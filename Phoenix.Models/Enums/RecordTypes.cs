﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Enums
{
    public enum RecordTypes
    {        
        System = 1,
        Custom = 2
    }
}
