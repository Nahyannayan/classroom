﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Common.Enums
{
    public enum BehaviourCategoryTypes
    {

        Acheivements = 1,
        Merits = 2,
        Demerit = 3
    }
    public enum ParameterMapping
    {

        PRESENT = 1,
        ABSENT = 2
    }
    public enum AttendanceTypeSession
    {
        Session1 = 1,
        Session2 = 2,
        Session1AndSession2 = 3
    }
    public enum AttendanceSelectType
    {
        Daily = 1,
        Room = 2,
        DailyAndRoom = 3
    }

}
