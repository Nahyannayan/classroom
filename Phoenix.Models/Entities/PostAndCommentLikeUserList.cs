﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
   public class PostAndCommentLikeUserList
    {
        public string UserProfileImage { get; set; }
        public string AvatarLogo { get; set; }

        public string CreatedByName { get; set; }
        public string Type { get; set; }
        public string Comment { get; set; }
        public string CreatedOn { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string TypeName { get; set; }
        public string FileId { get; set; }
        public int UserTypeId { get; set; }

    }
}
