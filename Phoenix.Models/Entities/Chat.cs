﻿using System;

namespace Phoenix.Models
{
    public class Chat
    {
        public Chat()
        {

        }
        public Chat(int _chatId)
        {
            ChatId = _chatId;
        }

        public long ChatId { get; set; }
        public long SchoolId { get; set; }
        public int ChatTypeId { get; set; }
        public string ChatType { get; set; }
        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public string SignalRGroupName { get; set; }
        public string ChatMessage { get; set; }

        public long FromUserId { get; set; }
        public string FromUser { get; set; }
        public long ToUserId { get; set; }
        public string ToUser { get; set; }

        public int MessageTypeId { get; set; }
        public string MessageType { get; set; }
        public string ContentPath { get; set; }
        public string FileName { get; set; }
        public DateTime MessageDateTime { get; set; }
        public bool IsActive { get; set; }
        public string FromUserProfileImage { get; set; }
        public string ToUserProfileImage { get; set; }
        public string SearchText { get; set; }
        public bool IsRead { get; set; }
        public bool IsDelivered { get; set; }
        public string UserIPAddress { get; set; }
        public string WebServerIPAddress { get; set; }
        public string GroupUserIds { get; set; }
        public string GroupUserProfileImages { get; set; }
        public int UserCount { get; set; }
        public bool IsOnline { get; set; }
    }

    public class InviteChat
    {
        public InviteChat()
        {

        }

        public InviteChat(long _id)
        {
            Id = _id;
        }
        public long Id { get; set; }
        public long SchoolId { get; set; }
        public long InviteGroupId { get; set; }
        public string InviteGroupName { get; set; }
        public string SignalRGroupName { get; set; }
        public long GroupUserId { get; set; }
        public string GroupUserName { get; set; }
        public string GroupUserProfileImage { get; set; }
        public DateTime? InvitedOn { get; set; }
        public long InvitedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime? GroupJoiningTime { get; set; }
        public bool IsActive { get; set; }
    }


    public class RecentUserActivity
    {
        public RecentUserActivity()
        {

        }
        public int Id { get; set; }
        public int userId { get; set; }
        public int? UserTypeId { get; set; }
        public int? IsFile { get; set; }
        public int? CreatedBy { get; set; }

        public string Title { get; set; }
        public string UserName { get; set; }
        public string Types { get; set; }
        public string ProfileImage { get; set; }
        public string ActivityType { get; set; }
        public DateTime MessageDateTime { get; set; }
        public string MessageHour { get; set; }

        public int IsGroup { get; set; }
        public int GroupId { get; set; }
        public string SignalRGroupName { get; set; }
        public long FileId { get; set; }
        public int ResourceFileTypeId { get; set; }
        public string PhysicalFilePath { get; set; }
    }

    public class UserChatLog
    {
        public UserChatLog()
        {

        }
        public string ConnectionId { get; set; }
        public long UserId { get; set; }
        public long SchoolId { get; set; }
        public string UserName { get; set; }
        public DateTime UserLoginTime { get; set; }
        public DateTime UserLogoutTime { get; set; }
        public bool IsActive { get; set; }
        public int  CurrentStatus { get; set; }
        public string SessionId { get; set; }
    }
}
