﻿using Newtonsoft.Json;
namespace Phoenix.Models
{
    public class WebRTCResponseView
    {
        [JsonProperty("responseCode")]
        public short ResponseCode { get; set; }
        [JsonProperty("responseDescription")]
        public string ResponseDescription { get; set; }
        [JsonProperty("authToken")]
        public string AuthToken { get; set; }
    }
}
