﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
   public class CertificateColumns
    {
        public int CertificateColumnId { get; set; }
        public string ColumnName { get; set; }
        public string ColumnDataQuery { get; set; }
        public bool IsActive { get; set; }
           
    }
}
