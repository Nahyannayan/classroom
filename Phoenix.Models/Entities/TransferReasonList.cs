﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class TransferReasonList
    {
        public string ReasonID { get; set; }
        public string Reason { get; set; }
    }

    public class TransferReasonListRoot : JasonResult
    {
        public List<TransferReasonList> data { get; set; }
    }
}
