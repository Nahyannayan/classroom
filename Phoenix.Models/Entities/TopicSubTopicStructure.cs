﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class TopicSubTopicStructure
    {
        public string SubSyllabusId { get; set; }
        public string SubSyllabusDescription { get; set; }
        public string ParentId { get; set; }
        public bool isMainSyllabus { get; set; }
        public bool isLesson { get; set; }
        public int SubjectId { get; set; }
        public bool IsSelected { get; set; }
    }

    public class Objective
    {
        public int ObjectiveId { get; set; }
        public string ObjectiveName { get; set; }
        public bool isSelected { get; set; }
        public int SubjectId { get; set; }
    }

    public class CourseUnit
    {
        public int CourseUnitId { get; set; }
        public string CourseUnitName { get; set; }
        public bool isSelected { get; set; }
        public int UnitId { get; set; }
    }

    public class GroupUnit
    {
        public int GroupCourseTopicId { get; set; }
        public int GroupId { get; set; }
        public string TopicName { get; set; }
        public bool IsActive { get; set; }
    }
}
