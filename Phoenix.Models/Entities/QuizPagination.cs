﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class QuizPagination
    {
        public List<QuizView> items { get; set; }
        public int totalCount { get; set; }
        public int filteredCount { get; set; }
        public int pageSize { get; set; }
    }
    public class QuizPaginationRequest
    {
        public long UserId { get; set; }
        public bool IsForm { get; set; }
        public string SearchString { get; set; }
        public int StartIndex { get; set; }
        public int PageSize { get; set; }
        public string SortedColumnsString { get; set; }
    }
}
