﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class SchoolInformation
    {
        public SchoolInformation() { }

        public SchoolInformation(int _schoolId)
        {

            SchoolId = _schoolId;
        }

        public long SchoolId { get; set; }

        public string SchoolName { get; set; }

        public string SchoolShortName { get; set; }

        public bool IsActive { get; set; }

        public int CountryId { get; set; }

        public string SchoolEmail { get; set; }

        public string StreetLine1 { get; set; }

        public string StreetLine2 { get; set; }

        public string TownName { get; set; }

        public string County { get; set; }

        public string PostCode { get; set; }

        public string Telepone { get; set; }

        public string Website { get; set; }

        public string PrincipleName { get; set; }

        public string PrimaryContactFirstName { get; set; }

        public string PrimaryContactLastName { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }
        public int TotalCount { get; set; }
        public bool IsClassroomActive { get; set; }
        public bool IsAdminApprovalForLessonPlan { get; set; }
        public string SchoolImage { get; set; }
    }
}
