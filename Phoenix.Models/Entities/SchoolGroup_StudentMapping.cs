﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
  public  class SchoolGroup_StudentMapping
    {
        public long mapping_id { get; set; }
        public long student_id { get; set; }

        public long group_id { get; set; }

        public long post_id { get; set; }

        public DateTime created_date { get; set; }
    }
}
