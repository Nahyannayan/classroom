﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class AssessmentLearning : Attachments
    {
        public long AssesmentLearningID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int DeliveryMode { get; set; }
        public long CourseId { get; set; }
        public long UserId { get; set; }
        public long UnitId { get; set; }
        public string AttachmentKey { get; set; }

    }

}
