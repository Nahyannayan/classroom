﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Observation
    {
        public Observation() { }
        public Observation(int _observationId)
        {
            ObservationId = _observationId;
            lstObservationFiles = new List<ObservationFile>();
            lstObservationStudents = new List<ObservationStudent>();
        }
        public int ObservationId { get; set; }
        public string ObservationTitle { get; set; }
        public string ObservationDesc { get; set; }        
        public DateTime StartDate { get; set; }
        public bool IsPublished { get; set; }
        public long CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public string StudentIdsToAdd { get; set; }
        public bool IsGradingEnable { get; set; }
        public int GradingTemplateId { get; set; }
        public List<ObservationFile> lstObservationFiles { get; set; }
        public List<ObservationStudent> lstObservationStudents { get; set; }
        public string SchoolGroups { get; set; }
        //public string Subjects { get; set; }
        public List<Objective> lstObjectives { get; set; }
        public List<SchoolGroup> lstSchoolGroups { get; set; }
        //public List<Subject> lstSubjects { get; set; }
        public int TotalCount { get; set; }
        public bool IsArchived { get; set; }
        public List<Observation> lstArchivedObservation { get; set; }
        public List<Observation> lstActiveObservation { get; set; }
        public string Courses { get; set; }
        public List<Course> lstCourses { get; set; }
        public long NoofStudent { get; set; }
    }
    public class ObservationFile
    {
        public int ObservationFileId { get; set; }
        public int ObservationId { get; set; }
        public string FileName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsNewFile { get; set; }
        public string UploadedFileName { get; set; }
        public short ResourceFileTypeId { get; set; }
        public int FileTypeId { get; set; }
        public string FileTypeName { get; set; }
        public double FileSizeInMB { get; set; }
        public string GoogleDriveFileId { get; set; }
        public string FileIcon { get; set; }
        public string PhysicalFilePath { get; set; }
    }

    public class ObservationStudent : Observation
    {
        public int ObservationStudentId { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public bool IsCompletedByStudent { get; set; }
        public bool CompleteMarkByTeacher { get; set; }
        public bool IsSeenByStudent { get; set; }
        public string UserImageFilePath { get; set; }
        public string StudentNumber { get; set; }
        public int TotalStudent { get; set; }

    }
}
