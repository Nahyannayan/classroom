﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class Standard
    {
        public Int64 StandardID { get; set; }
        public string CourseName { get; set; }
        public string Description { get; set; }
        public Int64 ParentID { get; set; }
        public Int64 AgeGroupID { get; set; }
        public Int64 StepsID { get; set; }
        public string ParentGroupName { get; set; }

        public Int64 Courseid { get; set; }
        public Int64 Termid { get; set; }

        public string TremName { get; set; }
        public long MainSyllabusId { get; set; }


    }

    public class StandardDetails
    {
        public Int64 StandardDetailID { get; set; }
        public Int64 StandardID { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public int Weight { get; set; }
        public int Points { get; set; }
        public string StandardGroupName { get; set; }
        public int ParentID { get; set; }

        public int MainSyllabusId { get; set; }
        //public int ParentID { get; set; }

        public int GroupID { get; set; }

        public int StepID { get; set; }

        public string COR_TITLE { get; set; }

    }
    public class SchoolTermList
    {
        public Int64 TermId { get; set; }
        public string TermName { get; set; }
    }
    public class MainSyllabusIdList
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }
    }

    public class ParentDetails
    {
        public Int64 PatentId { get; set; }
        public string Name { get; set; }
    }
}
