﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class GroupPrimaryTeacher
    {
        public GroupPrimaryTeacher()
        {

        }
        public GroupPrimaryTeacher(long _groupPrimaryTeacherMappingId)
        {
            GroupPrimaryTeacherMappingId = _groupPrimaryTeacherMappingId;
        }
        public long GroupPrimaryTeacherMappingId { get; set; }
        public long GroupId { get; set; }
        public long TeacherId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
