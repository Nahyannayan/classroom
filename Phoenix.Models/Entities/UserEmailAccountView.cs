﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class UserEmailAccountView
    {
        public long UserMailId { get; set; }
        public int UserId { get; set; }
        public string GoogleMailAddress { get; set; }
        public string MicorosoftMailAddress { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string StudentNumber { get; set; }
    }
}
