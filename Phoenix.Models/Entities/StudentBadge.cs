﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentBadge
    {
        public long userId { get; set; }
        public string schoolGroupIds { get; set; }
        public string badgesIds { get; set; }
        public long CreatedBy { get; set; }
    }
}
