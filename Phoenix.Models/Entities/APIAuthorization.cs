﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models { 
    public class APIAuthorization
    {
        public string grant_type { get; set; }
        public string username { get; set; }       
        public string password { get; set; }
        public string AppId { get; set; }

  
    }
}
