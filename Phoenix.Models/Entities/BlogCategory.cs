﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class BlogCategory
    {
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public string ColorCode { get; set; }
        public long SchoolId { get; set; }
    }
}
