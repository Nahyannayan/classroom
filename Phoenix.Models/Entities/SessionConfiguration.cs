﻿namespace Phoenix.Models
{
    public class SessionConfiguration
    {
        public long SchoolId { get; set; }
        public bool ZoomSessionEnabled { get; set; }
        public bool TeamsSessionEnabled { get; set; }
    }
}
