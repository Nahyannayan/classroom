﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class BlogCommentLike
    {
        public BlogCommentLike()
        {

        }

        public BlogCommentLike(int _blogCommentLikeId)
        {
            BlogCommentLikeId = _blogCommentLikeId;
        }

        public long BlogCommentLikeId { get; set; }
        public long CommentId { get; set; }
        public bool IsLike { get; set; }
        public int LikeTypeId { get; set; }
        public long LikedBy { get; set; }
        public string LikedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
