﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Subject
    {
        public Subject() { }
        public Subject(int _subjectId)
        {
            SubjectId = _subjectId;
        }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public string SubjectColorCode { get; set; }
        public bool IsActive { get; set; }
        public int SchoolId { get; set; }
        public string FormattedCreatedOn { get; set; }
        public int CreatedById { get; set; }
        public int SubjectTypeId { get; set; }
        public string SubjectTypeName { get; set; }
        public string CreatedByName { get; set; }
        public string Actions { get; set; }
    }
}
