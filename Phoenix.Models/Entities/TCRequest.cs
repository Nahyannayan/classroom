﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class TCRequest
    {
        public string StudentID { get; set; }
        public string LastAttDate { get; set; }
        public string LeaveDate { get; set; }
        public int TotalDays { get; set; }
        public int TotalPresentDays { get; set; }
        public int TransferReasonID { get; set; }
        public long TransferTypeID { get; set; }
        public Boolean ISGemsTC { get; set; }
        public long TransferBSUID { get; set; }
        public string TransferFeedback { get; set; }
        public string TransferZone { get; set; }
    }
}
