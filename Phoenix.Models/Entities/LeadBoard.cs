﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class LeadBoard
    {
        public long StudentID { get; set; }
        public string StudentName { get; set; }
        public string StudentImage { get; set; }
        public int RewardPoint { get; set; }
        public DateTime WorkLogDateTime { get; set; }
        public int Ranks { get; set; }
        public int RecordType { get; set; }
    }
}
