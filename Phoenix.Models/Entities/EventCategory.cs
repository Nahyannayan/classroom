﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class EventCategory
    {
        public EventCategory()
        {

        }
        public EventCategory(int _eventCategory)
        {
            Id = _eventCategory;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ColorCode { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public string TransactionMode { get; set; }
        public long SchoolId { get; set; }
        public string EventCategoryXml { get; set; }
    }
}
