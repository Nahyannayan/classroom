﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
   public class BlogCompleteData
    {
        public Blog _Blog { get; set; }
        public List<BlogComment> _BlogComment { get; set; }
        public List<string> AllowedFileExtension { get; set; }
        public List<string> AllowedImageExtension { get; set; }
        public List<RecentUserActivity> _RecentUserActivity { get; set; }
        public List<SchoolGroup> _SchoolGroup { get; set; }

    }

}
