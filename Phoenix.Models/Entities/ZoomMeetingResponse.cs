﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ZoomMeetingResponse
    {
        public long UserId { get; set; }
        public int SchoolGroupId { get; set; }
        public string uuid { get; set; }
        public string id { get; set; }
        public string host_id { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string topic { get; set; }
        public short type { get; set; }
        public string status { get; set; }
        public string ParticipantsJoinUrl { get; set; }
        public string timezone { get; set; }
        public string created_at { get; set; }
        public short duration { get; set; }
        public string start_url { get; set; }
        public string join_url { get; set; }
        public string password { get; set; }
        public string start_time { get; set; }
        public long CreatedBy { get; set; }
        public ZoomMeetingSettings settings { get; set; }
        public string registrant_id { get; set; }
        public ZoomMeetingParticipants[] participants { get; set; }
        public bool IsPartipantsExistWithSameName { get; set; }
        public bool IsPerStudent { get; set; }
        public string SelectedMembers { get; set; }
        public bool IsSSOEnabled { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public string token { get; set; }
    }

    public class ZoomMeetingSettings
    {
        public string alternative_hosts { get; set; }
        public string contact_name { get; set; }
        public string contact_email { get; set; }
        public short approval_type { get; set; }
        public bool endorce_login { get; set; }
    }


    public class ZoomMeetingParticipants
    {
        public string Id { get; set; }
        public long user_id { get; set; }
        public string user_name { get; set; }
        public string device { get; set; }
        public string ip_address { get; set; }
        public string location { get; set; }
        public string network_type { get; set; }
        public string data_center { get; set; }
        public string connection_type { get; set; }
        public DateTimeOffset join_time { get; set; }
        public bool share_application { get; set; }
        public bool share_desktop { get; set; }
        public bool share_whiteboard { get; set; }
        public bool recording { get; set; }
        public string pc_name { get; set; }
        public string domain { get; set; }
        public string mac_addr { get; set; }
        public string harddisk_id { get; set; }
        public string version { get; set; }
    }
}
