﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{

    public class StudentInfo
    {
        private string pStu_Id { get; set; }
        public string Stuauto_Id
        {
            get
            {
                return pStu_Id;
            }
            set
            {
                pStu_Id = value;
            }
        }

        private string pStu_BSU_Id { get; set; }
        public string Stu_BSU_Id
        {
            get
            {
                return pStu_BSU_Id;
            }
            set
            {
                pStu_BSU_Id = value;
            }
        }

        private string pStu_NO { get; set; }
        public string StuId
        {
            get
            {
                return pStu_NO;
            }
            set
            {
                pStu_NO = value;
            }
        }
        private string pStu_KNOWNNAME { get; set; }
        public string Stu_KNOWNNAME
        {
            get
            {
                return pStu_KNOWNNAME;
            }
            set
            {
                pStu_KNOWNNAME = value;
            }
        }
        private string pSTU_FullName { get; set; }
        public string STU_FullName
        {
            get
            {
                return pSTU_FullName;
            }
            set
            {
                pSTU_FullName = value;
            }
        }

        private string pSTU_FirstName { get; set; }
        public string STU_FirstName
        {
            get
            {
                return pSTU_FirstName;
            }
            set
            {
                pSTU_FirstName = value;
            }
        }

        private string pSTU_MidName { get; set; }
        public string STU_MidName
        {
            get
            {
                return pSTU_MidName;
            }
            set
            {
                pSTU_MidName = value;
            }
        }

        private string pSTU_LastName { get; set; }
        public string STU_LastName
        {
            get
            {
                return pSTU_LastName;
            }
            set
            {
                pSTU_LastName = value;
            }
        }

        private string pSTU_Name_Arabic { get; set; }
        public string STU_Name_Arabic
        {
            get
            {
                return pSTU_Name_Arabic;
            }
            set
            {
                pSTU_Name_Arabic = value;
            }
        }

        private string pSTU_ENQ_REF { get; set; }
        public string STU_ENQ_REF
        {
            get
            {
                return pSTU_ENQ_REF;
            }
            set
            {
                pSTU_ENQ_REF = value;
            }
        }
        private string pSTU_FEE_ID { get; set; }
        public string STU_FEE_ID
        {
            get
            {
                return pSTU_FEE_ID;
            }
            set
            {
                pSTU_FEE_ID = value;
            }
        }

        private string pSTU_MOE_ID { get; set; }
        public string STU_MOE_ID
        {
            get
            {
                return pSTU_MOE_ID;
            }
            set
            {
                pSTU_MOE_ID = value;
            }
        }

        private DateTime pSTU_REGN_Date { get; set; }
        public DateTime STU_REGN_Date
        {

            get
            {
                return pSTU_REGN_Date;
            }
            set
            {
                pSTU_REGN_Date = value;
            }
        }

        private string pSTU_COUNTRY_OF_BIRTH { get; set; }
        public string STU_COUNTRY_OF_BIRTH
        {
            get
            {
                return pSTU_COUNTRY_OF_BIRTH;
            }
            set
            {
                pSTU_COUNTRY_OF_BIRTH = value;
            }
        }

        private string pSTU_NATIONALITY { get; set; }
        public string STU_NATIONALITY
        {
            get
            {
                return pSTU_NATIONALITY;
            }
            set
            {
                pSTU_NATIONALITY = value;
            }
        }

        private DateTime pSTU_DOJ { get; set; }

        public DateTime STU_DOJ
        {

            get
            {
                return pSTU_DOJ;
            }
            set
            {
                pSTU_DOJ = value;
            }
        }
        private DateTime pSTU_DOB { get; set; }
        public DateTime STU_DOB
        {

            get
            {
                return pSTU_DOB;
            }
            set
            {
                pSTU_DOB = value;
            }
        }


        private string pSTU_GENDER { get; set; }
        public string STU_GENDER
        {
            get
            {
                return pSTU_GENDER;
            }
            set
            {
                pSTU_GENDER = value;
            }
        }

        private string pSTU_RELIGION { get; set; }
        public string STU_RELIGION
        {
            get
            {
                return pSTU_RELIGION;
            }
            set
            {
                pSTU_RELIGION = value;
            }
        }

        private string pSTU_HOUSE_ID { get; set; }
        public string STU_HOUSE_ID
        {
            get
            {
                return pSTU_HOUSE_ID;
            }
            set
            {
                pSTU_HOUSE_ID = value;
            }
        }

        private string pSTU_FEE_SPONSOR { get; set; }
        public string STU_FEE_SPONSOR
        {
            get
            {
                return pSTU_FEE_SPONSOR;
            }
            set
            {
                pSTU_FEE_SPONSOR = value;
            }
        }

        private string pSTU_EMG_CONTACT { get; set; }
        public string STU_EMG_CONTACT
        {
            get
            {
                return pSTU_EMG_CONTACT;
            }
            set
            {
                pSTU_EMG_CONTACT = value;
            }
        }
        private string pSTU_HEALTH { get; set; }
        public string STU_HEALTH
        {
            get
            {
                return pSTU_HEALTH;
            }
            set
            {
                pSTU_HEALTH = value;
            }
        }

        private string pSTU_PHYSICAL { get; set; }
        public string STU_PHYSICAL
        {
            get
            {
                return pSTU_HEALTH;
            }
            set
            {
                pSTU_HEALTH = value;
            }
        }

        private string pSTU_RCV_SPMEDICATION { get; set; }
        public string STU_RCV_SPMEDICATION
        {
            get
            {
                return pSTU_RCV_SPMEDICATION;
            }
            set
            {
                pSTU_RCV_SPMEDICATION = value;
            }
        }

        private string pSTU_SPMEDICATION_DETAILS { get; set; }
        public string STU_SPMEDICATION_DETAILS
        {
            get
            {
                return pSTU_SPMEDICATION_DETAILS;
            }
            set
            {
                pSTU_SPMEDICATION_DETAILS = value;
            }
        }

        private string pSTU_USE_TPT { get; set; }
        public string STU_USE_TPT
        {
            get
            {
                return pSTU_USE_TPT;
            }
            set
            {
                pSTU_USE_TPT = value;
            }
        }

        private string pSTU_PICKUP_BUSNO { get; set; }
        public string STU_PICKUP_BUSNO
        {
            get
            {
                return pSTU_PICKUP_BUSNO;
            }
            set
            {
                pSTU_PICKUP_BUSNO = value;
            }
        }

        private string pSTU_DROPOFF_BUSNO { get; set; }
        public string STU_DROPOFF_BUSNO
        {
            get
            {
                return pSTU_DROPOFF_BUSNO;
            }
            set
            {
                pSTU_DROPOFF_BUSNO = value;
            }
        }

        private string pSTU_PICKUP { get; set; }
        public string STU_PICKUP
        {
            get
            {
                return pSTU_PICKUP;
            }
            set
            {
                pSTU_PICKUP = value;
            }
        }

        private string pSTU_DROPOFF { get; set; }
        public string STU_DROPOFF
        {
            get
            {
                return pSTU_DROPOFF;
            }
            set
            {
                pSTU_DROPOFF = value;
            }
        }

        private string pSTU_PASPRTNAME { get; set; }
        public string STU_PASPRTNAME
        {
            get
            {
                return pSTU_PASPRTNAME;
            }
            set
            {
                pSTU_PASPRTNAME = value;
            }
        }

        private string pSTU_PASPRTNO { get; set; }
        public string STU_PASPRTNO
        {
            get
            {
                return pSTU_PASPRTNO;
            }
            set
            {
                pSTU_PASPRTNO = value;
            }
        }

        private string pSTU_PASPRTISSPLACE { get; set; }
        public string STU_PASPRTISSPLACE
        {
            get
            {
                return pSTU_PASPRTISSPLACE;
            }
            set
            {
                pSTU_PASPRTISSPLACE = value;
            }
        }

        private string pSTU_CURRSTATUS { get; set; }
        public string STU_CURRSTATUS
        {
            get
            {
                return pSTU_CURRSTATUS;
            }
            set
            {
                pSTU_CURRSTATUS = value;
            }
        }
        private string pSTU_SIBLING_ID { get; set; }
        public string STU_SIBLING_ID
        {
            get
            {
                return pSTU_SIBLING_ID;
            }
            set
            {
                pSTU_SIBLING_ID = value;
            }
        }

        private string pSTU_SECTION { get; set; }
        public string STU_SECTION
        {
            get
            {
                return pSTU_SECTION;
            }
            set
            {
                pSTU_SECTION = value;
            }
        }

        private string pSTU_ACADEMIC_YEAR { get; set; }
        public string STU_ACADEMIC_YEAR
        {
            get
            {
                return pSTU_ACADEMIC_YEAR;
            }
            set
            {
                pSTU_ACADEMIC_YEAR = value;
            }
        }


        private string pSTU_GRADE { get; set; }
        public string STU_GRADE
        {
            get
            {
                return pSTU_GRADE;
            }
            set
            {
                pSTU_GRADE = value;
            }
        }
        private string pSTU_STREAM { get; set; }
        public string STU_STREAM
        {
            get
            {
                return pSTU_STREAM;
            }
            set
            {
                pSTU_STREAM = value;
            }
        }

        private string pSTU_PASPRTISSPLACE_ARABIC { get; set; }
        public string STU_PASPRTISSPLACE_ARABIC
        {
            get
            {
                return pSTU_PASPRTISSPLACE_ARABIC;
            }
            set
            {
                pSTU_PASPRTISSPLACE_ARABIC = value;
            }
        }

        private string pSTU_PASPRT_ISS_DATE { get; set; }
        public string STU_PASPRT_ISS_DATE
        {
            get
            {
                return pSTU_PASPRT_ISS_DATE;
            }
            set
            {
                pSTU_PASPRT_ISS_DATE = value;
            }
        }
        private string pSTU_PASPRT_EXP_DATE { get; set; }
        public string STU_PASPRT_EXP_DATE
        {
            get
            {
                return pSTU_PASPRT_EXP_DATE;
            }
            set
            {
                pSTU_PASPRT_EXP_DATE = value;
            }
        }

        private string pSTU_VISA_NO { get; set; }
        public string STU_VISA_NO
        {
            get
            {
                return pSTU_VISA_NO;
            }
            set
            {
                pSTU_VISA_NO = value;
            }
        }

        private string pSTU_VISA_ISSUE_PACE { get; set; }
        public string STU_VISA_ISSUE_PACE
        {
            get
            {
                return pSTU_VISA_ISSUE_PACE;
            }
            set
            {
                pSTU_VISA_ISSUE_PACE = value;
            }
        }

        private string pSTU_VISA_ISSUE_DATE { get; set; }
        public string STU_VISA_ISSUE_DATE
        {
            get
            {
                return pSTU_VISA_ISSUE_DATE;
            }
            set
            {
                pSTU_VISA_ISSUE_DATE = value;
            }
        }

        private string pSTU_VISA_EXPIRY_DATE { get; set; }
        public string STU_VISA_EXPIRY_DATE
        {
            get
            {
                return pSTU_VISA_EXPIRY_DATE;
            }
            set
            {
                pSTU_VISA_EXPIRY_DATE = value;
            }
        }

        private string pSTU_VISA_ISSUING_AUTHORIT { get; set; }
        public string STU_VISA_ISSUING_AUTHORIT
        {
            get
            {
                return pSTU_VISA_ISSUING_AUTHORIT;
            }
            set
            {
                pSTU_VISA_ISSUING_AUTHORIT = value;
            }
        }

        private string pSTU_BLOODGROUP { get; set; }
        public string STU_BLOODGROUP
        {
            get
            {
                return pSTU_BLOODGROUP;
            }
            set
            {
                pSTU_BLOODGROUP = value;
            }
        }
        private string pSTU_HCNO { get; set; }
        public string STU_HCNO
        {
            get
            {
                return pSTU_HCNO;
            }
            set
            {
                pSTU_HCNO = value;
            }
        }
        private string pSTU_PRIMARYCONTACT { get; set; }
        public string STU_PRIMARYCONTACT
        {
            get
            {
                return pSTU_PRIMARYCONTACT;
            }
            set
            {
                pSTU_PRIMARYCONTACT = value;
            }
        }

        private string pSTU_PREFCONTACT { get; set; }
        public string STU_PREFCONTACT
        {
            get
            {
                return pSTU_PREFCONTACT;
            }
            set
            {
                pSTU_PREFCONTACT = value;
            }
        }

        private string pSTU_PHOTOPATH { get; set; }
        public string STU_PHOTOPATH
        {
            get
            {
                return pSTU_PHOTOPATH;
            }
            set
            {
                pSTU_PHOTOPATH = value;
            }
        }

        private bool pSTU_bRCVSMS { get; set; }

        private bool pSTU_bRCVMAIL { get; set; }

        private bool pSTU_bActive { get; set; }
        private string pSTU_JOIN_ACADEMIC_YEAR { get; set; }
        public string STU_JOIN_ACADEMIC_YEAR
        {
            get
            {
                return pSTU_JOIN_ACADEMIC_YEAR;
            }
            set
            {
                pSTU_JOIN_ACADEMIC_YEAR = value;
            }
        }

        private string pSTU_JOIN_GRADE { get; set; }
        public string STU_JOIN_GRADE
        {
            get
            {
                return pSTU_JOIN_GRADE;
            }
            set
            {
                pSTU_JOIN_GRADE = value;
            }
        }

        private string pSTU_JOIN_SECTION { get; set; }
        public string STU_JOIN_SECTION
        {
            get
            {
                return pSTU_JOIN_SECTION;
            }
            set
            {
                pSTU_JOIN_SECTION = value;
            }
        }
        private string pSTU_JOIN_STREAM { get; set; }
        public string STU_JOIN_STREAM
        {
            get
            {
                return pSTU_JOIN_STREAM;
            }
            set
            {
                pSTU_JOIN_STREAM = value;
            }
        }

        private string pSTU_FIRSTLANG { get; set; }
        public string STU_FIRSTLANG
        {
            get
            {
                return pSTU_FIRSTLANG;
            }
            set
            {
                pSTU_FIRSTLANG = value;
            }
        }
        private string pSTU_OTHLANG { get; set; }
        public string STU_OTHLANG
        {
            get
            {
                return pSTU_OTHLANG;
            }
            set
            {
                pSTU_OTHLANG = value;
            }
        }

        private string pSTU_ENG_READING { get; set; }
        public string STU_ENG_READING
        {
            get
            {
                return pSTU_ENG_READING;
            }
            set
            {
                pSTU_ENG_READING = value;
            }
        }

        private string pSTU_ENG_WRITING { get; set; }
        public string STU_ENG_WRITING
        {
            get
            {
                return pSTU_ENG_WRITING;
            }
            set
            {
                pSTU_ENG_WRITING = value;
            }
        }

        private string pSTU_ENG_SPEAKING { get; set; }
        public string STU_ENG_SPEAKING
        {
            get
            {
                return pSTU_ENG_SPEAKING;
            }
            set
            {
                pSTU_ENG_SPEAKING = value;
            }
        }
        private string pSTU_EMIRATES_ID { get; set; }
        public string STU_EMIRATES_ID
        {
            get
            {
                return pSTU_EMIRATES_ID;
            }
            set
            {
                pSTU_EMIRATES_ID = value;
            }
        }

        private DateTime _stU_EM_ID_EXP_DATE { get; set; }
        public DateTime stU_EM_ID_EXP_DATE
        {
            get
            {
                return _stU_EM_ID_EXP_DATE;
            }
            set
            {
                _stU_EM_ID_EXP_DATE = value;
            }
        }

        private string pstu_PremisesID { get; set; }
        public string stu_PremisesID
        {
            get
            {
                return pstu_PremisesID;
            }
            set
            {
                pstu_PremisesID = value;
            }
        }

        private string _olU_ID { get; set; }
        public string olU_ID
        {
            get
            {
                return _olU_ID;
            }
            set
            {
                _olU_ID = value;
            }
        }

        private string pCURRICULUM_DESCR { get; set; }
        public string CURRICULUM_DESCR
        {
            get
            {
                return pCURRICULUM_DESCR;
            }
            set
            {
                pCURRICULUM_DESCR = value;
            }
        }
        private string pSCHOOL_NAME { get; set; }
        public string SCHOOL_NAME
        {
            get
            {
                return pSCHOOL_NAME;
            }
            set
            {
                pSCHOOL_NAME = value;
            }
        }
        private string pPAR_NAME { get; set; }
        public string PAR_NAME
        {
            get
            {
                return pPAR_NAME;
            }
            set
            {
                pPAR_NAME = value;
            }
        }

        private string pPAR_EMAIL { get; set; }
        public string PAR_EMAIL
        {
            get
            {
                return pPAR_EMAIL;
            }
            set
            {
                pPAR_EMAIL = value;
            }
        }
        private string pPAR_MOBILE { get; set; }
        public string PAR_MOBILE
        {
            get
            {
                return pPAR_MOBILE;
            }
            set
            {
                pPAR_MOBILE = value;
            }
        }
        private string pCLASS_TEACHER { get; set; }

        public string CLASS_TEACHER
        {
            get
            {
                return pCLASS_TEACHER;
            }
            set
            {
                pCLASS_TEACHER = value;
            }
        }

        public bool STU_IS_INTERNAL_PUB { get; set; }
        public bool STU_IS_FAMILY_PHONE { get; set; }
        public bool STU_IS_MARKETING_PUB { get; set; }
        public bool STU_IS_FIELD_TRIPS { get; set; }

        public int CURRICULUM_ID { get; set; }

        public string STU_PLACE_OF_BIRTH { get; set; }
        public int STU_ACD_ID { get; set; }

        public string STU_EMAIL { get; set; }
        public string SCHOOL_CITY { get; set; }
        public string NextAcademicYear { get; set; }
        public string NextGrade { get; set; }

    }
    public class StudentInfoRoot
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public object responseCode { get; set; }
        public object message { get; set; }
        public List<StudentInfo> data { get; set; }

    }
}
