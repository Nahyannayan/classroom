﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.Models.Entities
{
    public class Attachment
    {
        public long AttachmentId { get; set; }
        public string AttachmentKey { get; set; }
        public long AttachedToId { get; set; }
        public AttachmentType AttachmentType { get; set; }
        public string AttachmentPath { get; set; }
        public string FileName { get; set; }
        public short ResourceFileTypeId { get; set; }
        public string GoogleDriveFileId { get; set; }
        public int FileTypeId { get; set; }
        public string FileExtension { get; set; }
        public TranModes Mode { get; set; }
    }
    public class Attachments
    {
        public Attachments()
        {
            AttachmentList = new List<Attachment>();
        }
        public List<Attachment> AttachmentList { get; set; }
        public DataTable GetAttachmentDT()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AttachmentId", typeof(long));
            dt.Columns.Add("AttachmentKey", typeof(string));
            dt.Columns.Add("AttachedToId", typeof(long));
            dt.Columns.Add("AttachmentType", typeof(int));
            dt.Columns.Add("AttachmentPath", typeof(string));

            dt.Columns.Add("FileName", typeof(string));
            dt.Columns.Add("ResourceFileTypeId", typeof(int));
            dt.Columns.Add("GoogleDriveFileId", typeof(string));
            dt.Columns.Add("FileExtension", typeof(string));

            dt.Columns.Add("Mode", typeof(int));
            AttachmentList.ForEach(x => {
                dt.Rows.Add(x.AttachmentId, x.AttachmentKey, x.AttachedToId, x.AttachmentType, x.AttachmentPath,
                    x.FileName, x.ResourceFileTypeId, x.GoogleDriveFileId, x.FileExtension, x.Mode);
            });
            return dt;
        }
        public string CloudFileList { get; set; }

        public string SystemImageList { get; set; }

        public bool IsSystemImageUpload { get; set; }
    }
    public enum AttachmentKey
    {
        Course,
        CourseImage,
        AssessmentEvidence,
        AssessmentLearning,
        StudentBehaviour,
        ProgressTracker
    }
    public enum AttachmentType
    {
        File,
        Link,
        SystemImage
    }
    public enum TranModes : byte
    {
        Insert = 1,
        Update = 2,
        Delete = 3,
        BulkInsert,
    }
}
