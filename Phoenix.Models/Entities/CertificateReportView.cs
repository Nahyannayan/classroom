﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class CertificateReportView
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string TemplateStatus { get; set; }
        public string FullName { get; set; }
        public string StatusChangedDate { get; set; }
        public string ApprovalSentOn { get; set; }
        public string RequesterName { get; set; }
    }
}
