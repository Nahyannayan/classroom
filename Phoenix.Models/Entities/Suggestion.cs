﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Suggestion
    {
        public Suggestion()
        { }
        public Suggestion(int _SuggestionId)
        {
            SuggestionId = _SuggestionId;
        }
        public int SuggestionId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int TypeId { get; set; }
        public string Type { get; set; }
        public string FileName { get; set; }
        public string Remark { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public int TotalCount { get; set; }
        public string PhysicalPath { get; set; }
    }
}
