﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class SchoolGroup
    {
        public SchoolGroup() { }
        public SchoolGroup(int _schoolGroupId)
        {
            SchoolGroupId = _schoolGroupId;
            lstActiveGroups = new List<SchoolGroup>();
            lstArchivedGroups = new List<SchoolGroup>();
        }

        public int SchoolGroupId { get; set; }
        public string SchoolGroupName { get; set; }
        public string SchoolGroupDescription { get; set; }
        public string TeacherGivenName { get; set; }
        public long? CourseId { get; set; }
        public string CourseTitle { get; set; }
        public int? SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string FormattedCreatedOn { get; set; }
        public long CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public bool IsActive { get; set; }
        public long SchoolId { get; set; }
        public string StudentIdsToAdd { get; set; }
        public string StudentIdsToRemove { get; set; }
        public string MembersToAdd { get; set; }
        public string MembersToRemove { get; set; }
        public int TotalCount { get; set; }
        public DateTime CreatedOn { get; set; }
        public int DDLSchoolGroupId { get; set; }
        public bool IsBespokeGroup { get; set; }
        public string SubjectCode { get; set; }
        public bool IsHomeTutorGroup { get; set; }
        public string GroupingName { get; set; }
        public bool IsManager { get; set; }
        public bool IsViewOnly { get; set; }
        public bool IsContribute { get; set; }
        public string BlogTitle { get; set; }
        public long BlogId { get; set; }
        public bool IsSeenByTeacher { get; set; }
        public bool IsSeenByStudent { get; set; }
        public bool IsSeenByParent { get; set; }
        public DateTime? ArchivedOn { get; set; }
        public int? ArchivedBy { get; set; }
        public string MeetingRoomId { get; set; }
        public long MeetingCreatedBy { get; set; }
        public string MeetingAttendeePassword { get; set; }
        public string TeamsMeetingId { get; set; }
        public string TeamsMeetingJoinUrl { get; set; }
        public string TeamsMeetingWebJoinUrl { get; set; }
        public short MeetingDuration { get; set; }
        public List<SchoolGroup> lstActiveGroups { get; set; }
        public List<SchoolGroup> lstArchivedGroups { get; set; }
        public string  AcademicYear { get; set; }
        public string GradeDisplay { get; set; }
        public int ModuleId { get; set; }
        public string Description { get; set; }
        public string GroupImage { get; set; }
        public int ObservationId { get; set; }

        public bool AutoSyncGroupMember { get; set; }
        public string Expression { get; set; }
        public string PrimayTeacherProfilePhoto { get; set; }
        public bool IsHidden { get; set; }

        public string ClassGroupIdsToHide { get; set; }
    }

    public class SchoolGroupType
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
    }
        

}
