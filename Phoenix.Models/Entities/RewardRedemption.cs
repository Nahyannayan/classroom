﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
  
    public class RewardRedemption 
    {
        public int persistentId { get; set; }
        public object logUserName { get; set; }
        public int available_points { get; set; }
        public int redeemable_points { get; set; }
        public double redeemable_amount { get; set; }
        public List<StudentsFeeDetails> studentsFeeDetails { get; set; }
        public List<CommonStudentFee> FeeDetails { get; set; }
    }

    public class RedemptionModel
    {
         public string UserName { get; set; }
         public string UserEMailID { get; set; }
         public string Source { get; set; }
         public RewardRedemption Rewards { get; set; } 
         public IEnumerable<PCStudentInfo> LstStudentsInfo { get; set; }
    }
    public class FeeDetailList
    {
        public bool blockPayNow { get; set; }
        public List<object> advanceDetails { get; set; }
        public bool advancePaymentAvailable { get; set; }
        public int feeID { get; set; }
        public string feeDescription { get; set; }
        public double dueAmount { get; set; }
        public double payAmount { get; set; }
        public double originalAmount { get; set; }
        public double discAmount { get; set; }
        public int activityRefID { get; set; }
        public object activityFeeCollType { get; set; }
    }

    public class StudentsFeeDetail
    {
        public string stU_NO { get; set; }
        public bool onlinePaymentAllowed { get; set; }
        public string userMessageforOnlinePaymentBlock { get; set; }
        public int paymentTypeID { get; set; }
        public double payingAmount { get; set; }
        public object ipAddress { get; set; }
        public double paymentProcessingCharge { get; set; }
        public object payMode { get; set; }
        public List<FeeDetailList> feeDetail { get; set; }
        public List<object> discountDetails { get; set; }
    }

   public class RedemptionHistory
    {
        public string Date { get; set; }
        public string RecNo { get; set; }
        public double Amount { get; set; }
        public string RefNo { get; set; }
        public string StudentNo { get; set; }
        public string StudentName { get; set; }
    }
    
    public class RedemptionResponse
    {
        public string RedemptionRefNo { get; set; }
        public double TotalAmount { get; set; }
        public OnlineFeePayment Response { get; set; }
    }   
}
