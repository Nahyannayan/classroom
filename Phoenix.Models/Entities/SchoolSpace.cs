﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class SchoolSpace
    {
        public SchoolSpace()
        {

        }

        public SchoolSpace(int _spaceId)
        {
            SpaceId = _spaceId;
        }
        public int SpaceId { get; set; }
        public int SchoolId { get; set; }
        public string SpaceName { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string IconCssClass { get; set; }
        public int CategoryId { get; set; }
        public bool UseWebsite { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public int FileCount { get; set; }
        public int FolderCount { get; set; }
    }
}
