﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class TransferBusinessunitList
    {
        public string SchoolId { get; set; }
        public string SchoolName { get; set; }
    }

    public class TransferBusinessunitListRoot: JasonResult
    {
        public List<TransferBusinessunitList> data { get; set; }
    }

}
