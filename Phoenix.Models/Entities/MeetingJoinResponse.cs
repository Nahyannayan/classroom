﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
   public class MeetingJoinResponse
    {
        public string MeetingURL { get; set; }
        public string UserToken { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
