﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class GroupUrl
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedByName { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedByName { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public bool IsAddMode { get; set; }

        public long FolderId { get; set; }
        public int ModuleId { get; set; }
    }
}
