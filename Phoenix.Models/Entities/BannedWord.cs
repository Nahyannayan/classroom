﻿

namespace Phoenix.Models
{
   
    public class BannedWord 
    {
        public BannedWord()
        {

        }

        public BannedWord(int _bannedWordId)
        {
            BannedWordId = _bannedWordId;
        }

        public BannedWord(string _previousWord, string _newWord)
        {
            BannedWordName = _newWord;
            PreviousBannedWord = _previousWord;
        }
        public int BannedWordId { get; set; }
        public string BannedWordName { get; set; }
        public string PreviousBannedWord { get; set; }
        public bool IsActive { get; set; }
        public int SchoolId { get; set; }
        public string FormattedCreatedOn { get; set; }
        public int CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public bool IsAddMode { get; set; }
    }
}
