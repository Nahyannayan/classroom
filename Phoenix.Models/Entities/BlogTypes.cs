﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class BlogTypes
    {
        public BlogTypes()
        {

        }

        public BlogTypes(int _blogTypeId)
        {
            BlogTypeId = _blogTypeId;
        }

        public int BlogTypeId { get; set; }
        public string BlogType { get; set; }
        public long SchoolId { get; set; }
    }
}
