﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models.Entities;
namespace Phoenix.Models
{
    public class StudentDashboard
    {

        public List<SchoolBadge> Badges { get; set; }
        public List<Blog> Blogs { get; set; }
        public List<SchoolGroup> SchoolGroups { get; set; }

        public List<AssignmentStudentDetails> AssignmentStudentDetail { get; set; }
        public IEnumerable<SchoolGroup> SchoolGroupBlog { get; set; }

        public List<SchoolBanner> Banners { get; set; }


        public StudentDashboard()
        {
            Blogs = new List<Blog>();
            SchoolGroups = new List<SchoolGroup>();
            AssignmentStudentDetail = new List<AssignmentStudentDetails>();
            SchoolGroupBlog = new List<SchoolGroup>();
            Badges = new List<SchoolBadge>();
        }
    }
}
