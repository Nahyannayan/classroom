﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentConsentInfoPost
    {
        public string stU_NO { get; set; }
        public bool stU_is_internal_pub { get; set; }
        public bool stU_is_family_phone { get; set; }
        public bool stU_is_marketing_pub { get; set; }
        public bool stU_is_field_Trips { get; set; }
        public int olU_ID { get; set; }
    }
}
