﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class GroupQuiz
    {
        public GroupQuiz()
        {
            lstGroupStudents = new List<GroupStudent>();
            GradeWiseTotalQuestionsReport = new GradeWiseTotalQuestionsReport();
            GradeWiseReport = new List<GradeWiseReport>();
            QuestionWiseReport = new List<QuestionWiseReport>();
        }
        public int GroupQuizId { get; set; }
        public int GroupId { get; set; }
        public int AssignmentId { get; set; }
        public int TaskId { get; set; }
        public int QuizId { get; set; }
        public string QuizName { get; set; }
        public string QuizImagePath { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedByName { get; set; }
        public long CreatedBy { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int GradingTemplateId { get; set; }
        public int QuizGradeId { get; set; }
        public string GradingTemplateTitle { get; set; }
        public string UpdatedByName { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public bool IsAddMode { get; set; }
        public bool IsForm { get; set; }
        public int QuizTime { get; set; }
        public bool IsSetTime { get; set; }
        public List<GroupStudent> lstGroupStudents { get; set; }
        public int ResourceId { get; set; }
        public string ResourceType { get; set; }
        public string GroupName { get; set; }
        public string QuizTiming { get; set; }
        public decimal QuizMarks { get; set; }
        public decimal TotalMarks { get; set; }
        public GenderWiseReport GenderWiseReport { get; set; }
        public GradeWiseTotalQuestionsReport GradeWiseTotalQuestionsReport { get; set; }
        public List<GradeWiseReport> GradeWiseReport { get; set; }
        public int TimeTaken { get; set; }
        public List<QuestionWiseReport> QuestionWiseReport { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartTime { get; set; }
        public long FolderId { get; set; }
        public int ModuleId { get; set; }
        public bool IsSolved { get; set; }
        public int TimeInSec { get; set; }
        public string  QuizOverAllTime { get; set; }
        public bool IsSetStartDate { get; set; }
        public bool IsStartedSetTimer { get; set; }
        public string  QuizStartDate { get; set; }
        public string QuizStartedTime { get; set; }
        public bool IsRandomQuestion { get; set; }
        public int MaxSubmit { get; set; }
        public int QuestionPaginationRange { get; set; }
        public bool IsHideScore { get; set; }
        public DateTime? ShowScoreDate { get; set; }
        public bool IsQuestionsExist { get; set; }

    }
    public class GroupStudent
    {
        public int GroupId { get; set; }
        public int GroupStudentId { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public bool IsCompletedByStudent { get; set; }
        public int GradingTemplateId { get; set; }
        public int QuizGradeId { get; set; }
        public string GradingColor { get; set; }
        public string ShortLabel { get; set; }
        public string GradingItemDescription { get; set; }
        public string GradingTemplateItemSymbol { get; set; }
        public string Percentage { get; set; }
        public int QuizResultId { get; set; }
        public bool CompleteMarkByTeacher { get; set; }
        public bool IsSeenByStudent { get; set; }
        public int ResourceId { get; set; }
        public int QuizId { get; set; }
        public decimal QuizMarks { get; set; }
        public decimal TotalMarks { get; set; }
        public int TimeTaken { get; set; }
    }

    public class GenderWiseReport
    {
        public int TotalStudents { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
    }

    public class GradeWiseTotalQuestionsReport
    {
        public int TotalQuestions { get; set; }
        public int QHavingGrade { get; set; }
        public int QNotHavingGrade { get; set; }
    }

    public class GradeWiseReport
    {
        public string GradeDisplay { get; set; }
        public int QuestionCount { get; set; }
    }

    public class QuestionWiseReport
    {
        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public string QuestionTypeName { get; set; }
        public int CorrectAnswered { get; set; }
        public int IncorrectAnswered { get; set; }
        public int TotalAnswered { get; set; }
        public int Percentage { get; set; }
    }
    public class QuizReport
    {
        public QuizReport()
        {
            QuizReportQuestions = new List<QuizReportQuestions>();
            QuizReportAnswers = new List<QuizReportAnswers>();
            QuizReportData = new List<ReportData>();

        }

        public List<QuizReportQuestions> QuizReportQuestions { get; set; }
        public List<QuizReportAnswers> QuizReportAnswers { get; set; }
        public List<ReportData> QuizReportData { get; set; }

    }
    public class ReportData
    {
        public int QuizResultId { get; set; }
        public string QuizName { get; set; }
        public string GroupName { get; set; }
        public string Resource { get; set; }
        public string TeacherName { get; set; }
        public string StudentName { get; set; }
        public string StudentYear { get; set; }
        public string Section { get; set; }
        public string IsCompleted { get; set; }
        public string QuizScore { get; set; }
        public string StartTime { get; set; }
        public string FinishTime { get; set; }
        public string Question { get; set; }
        public string QuestionMark { get; set; }
        public int TimeTaken { get; set; }
        public int QuizTime { get; set; }
        public string StudentNumber { get; set; }
    }
    public class QuizReportQuestions
    {
        public int QuizQuestionId { get; set; }
        public string QuestionText { get; set; }
    }
    public class QuizReportAnswers
    {
        public int QuizResultId { get; set; }
        public int QuizQuestionId { get; set; }
        public string QuestionText { get; set; }
        public decimal ObtainedMarks { get; set; }
        public int UserId { get; set; }
        public string Resourse { get; set; }
        public int ResourseId { get; set; }
    }
}
