﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class BlogLike
    {
        public BlogLike()
        {

        }

        public BlogLike(int _blogLikeId)
        {
            BlogLikeId = _blogLikeId;
        }

        public long BlogLikeId { get; set; }
        public long BlogId { get; set; }
        public bool IsLike { get; set; }
        public int LikeTypeId { get; set; }
        public long LikedBy { get; set; }
        public string LikedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
