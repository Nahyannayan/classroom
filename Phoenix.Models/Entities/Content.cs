﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Content
    {
        public Content()
        { }
        public Content(int _ContentId)
        {
            ContentId = _ContentId;
        }
        public long ContentId { get; set; }
        public long  UserId { get; set; }
        public long  SchoolId { get; set; }
        public string ContentName{ get; set; }
        public string Description { get; set; }
        public List<string> SubjectId { get; set; }
        public List<string> SubjectIds { get; set; }
        public int SelectedSubjectId { get; set; }
        public string SelectedSubjectName { get; set; }

        public int ContentType{ get; set; }
        public string RedirectUrl { get; set; }
        public bool IsApproved{ get; set; }
        public string ContentImage { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public string FileExtenstion { get; set; }
        public bool IsSharepointFile { get; set; }
        public string PhysicalPath { get; set; }
    }
}
