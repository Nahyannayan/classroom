﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class GetOptionDetails
    {
        public int IsSelect { get; set; }
        public int SubjectGradeID { get; set; }
        public string SubjectGradeDescription { get; set; }
    }

}
