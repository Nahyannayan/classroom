﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class GetStudentDiseases
    {
        public int diS_ID { get; set; }
        public bool diS_IS_INFECTIOUS { get; set; }
        public string diS_DESC { get; set; }
        public bool diS_ACTIVE { get; set; }
        public int isYChecked { get; set; }
        public int isNChecked { get; set; }
    }
    public class GetStudentDiseasesRoot
    {
        public List<GetStudentDiseases> GetStudentDiseases { get; set; }
    }
}
