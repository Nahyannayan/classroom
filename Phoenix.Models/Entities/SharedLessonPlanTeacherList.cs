﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
   public class SharedLessonPlanTeacherList
    {
        public int PlanSchemeDetailId { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserImageFilePath { get; set; }
    }
}
