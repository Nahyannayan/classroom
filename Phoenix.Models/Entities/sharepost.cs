﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
   public class sharepost
    {
        public long Id { get; set; }
        public long SchoolId { get; set; }

        public long GroupId { get; set; }
        public long PostId { get; set; }

        public bool ShareWithDepartmentWall { get; set; }

        public bool ShareWithCourseWall { get; set; }

        public DateTime SharingDate { get; set; }

        public long SharedByUserId { get; set; }

        public long SchoolLevelId { get; set; }
    }
}
