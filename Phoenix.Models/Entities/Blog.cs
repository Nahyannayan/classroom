﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Blog
    {
        public Blog()
        {

        }

        public Blog(int _blogId)
        {
            BlogId = _blogId;
        }
        public long BlogId { get; set; }
        public long SchoolId { get; set; }
        public int BlogTypeId { get; set; }
        public string BlogType { get; set; }
        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public string SubjectName { get; set; }
        public string TeacherGivenName { get; set; }
        public int EnableCommentById { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string EmbedUrl { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public string BlogImage { get; set; }
        public bool IsPublish { get; set; }
        public bool ModerationRequired { get; set; }
        public DateTime? CloseDiscussionDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public string CreatedByName { get; set; }
        public int UserTypeId { get; set; }
        public string UserProfileImage { get; set; }
        public string ColorCode { get; set; }
        public int TotalLikes { get; set; }
        public int TotalComments { get; set; }
        public int SortOrder { get; set; }
        public string Icon { get; set; }
        public bool IsFilesExist { get; set; }
        public bool IsLike { get; set; }
        public bool IsShared { get; set; }
        public DateTime BlogSharedOn { get; set; }
        public long BlogSharedBy { get; set; }
        public long ParentBlogId { get; set; }
        public string SelectedStudentIDs { get; set; }
        public int CommentApprovedById { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ScheduledTime { get; set; }
        public DateTime SortDate { get; set; }
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
        public string ResourceFileType { get; set; }
        public string GroupImage { get; set; }
        public bool IsVisibleToParent { get; set; }
        public string ResourceFileTypeId { get; set; }
        public string PhysicalFilePath { get; set; }
        public bool IsBlogEnabled { get; set; }
        public Int64 UpdateAvailableId { get; set; }
        public bool IsUpdateAvailable { get; set; }

    }
    public class ExemplarWallOrder
    {
        public DataTable dataTable { get; set; }
        public long UpdatedBy { get; set; }
    }

    public class Chatter
    {
        public Blog Blog { get; set; }
        public List<BlogComment> BlogComments { get; set; }
        public List<string> AllowedFileExtension { get; set; }
        public List<string> AllowedImageExtension { get; set; }



        public Chatter()
        {
            BlogComments = new List<BlogComment>();
        }
    }

    public class ShareBlogs
    {
        public ShareBlogs()
        {

        }

        public ShareBlogs(int _blogId)
        {
            BlogId = _blogId;
        }
        public long BlogId { get; set; }
        public string GroupIds { get; set; }
        public long GroupId { get; set; }
        public bool IsShared { get; set; }
        public long BlogSharedBy { get; set; }

    }

    public class BlogCloudFile
    {
        public int BlogCloudFileId { get; set; }
        public int BlogCloudId { get; set; }
        public string FileName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string FileExtension { get; set; }
        public string UploadedFileName { get; set; }
        public bool IsCloudFile { get; set; }
        public bool IsSharePointFile { get; set; }
        public bool IsSharedFile { get; set; }
        public string CloudFileId { get; set; }
        public string DriveId { get; set; }
        public string ParentFolderId { get; set; }
        public short ResourceFileTypeId { get; set; }
    }
}
