﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
   
    public class GetParentUpdateDetails
    {
        public string fatheR_NAME { get; set; }
        public string fatheR_FIRSTNAME { get; set; }
        public string fatheR_MIDNAME { get; set; }
        public string fatheR_LASTNAME { get; set; }
        public string fatheR_NATIONALITY { get; set; }
        public string fatheR_NATIONALITY_ID { get; set; }
        public string fatheR_COMMUNICATION_ADDRESS_LINE1 { get; set; }
        public string fatheR_COMMUNICATION_ADDRESS_LINE2 { get; set; }
        public string fatheR_POBOX { get; set; }
        public string fatheR_STREET { get; set; }
        public string fatheR_AREA { get; set; }
        public string fatheR_BLDG { get; set; }
        public string fatheR_APARTMENT { get; set; }
        public string fatheR_COUNTRY { get; set; }
        public string fatheR_COUNTRY_ID { get; set; }
        public string fatheR_CITY { get; set; }
        public string fatheR_STATE { get; set; }
        public string fatheR_STATE_ID { get; set; }
        public string fatheR_OFFICE_PHONE { get; set; }
        public string fatheR_RESIDENCE_PHONE { get; set; }
        public string fatheR_FAX { get; set; }
        public string fatheR_MOBILE { get; set; }
        public string fatheR_OCCUPATION { get; set; }
        public string fatheR_COMPANY { get; set; }
        public string fatheR_EMAIL { get; set; }
        public string fatheR_GEMS_STAFF { get; set; }
        public string motheR_NAME { get; set; }
        public string motheR_FIRSTNAME { get; set; }
        public string motheR_MIDNAME { get; set; }
        public string motheR_LASTNAME { get; set; }
        public string motheR_NATIONALITY { get; set; }
        public string motheR_NATIONALITY_ID { get; set; }
        public string motheR_COMMUNICATION_ADDRESS_LINE1 { get; set; }
        public string motheR_COMMUNICATION_ADDRESS_LINE2 { get; set; }
        public string motheR_POBOX { get; set; }
        public string motheR_STREET { get; set; }
        public string motheR_AREA { get; set; }
        public string motheR_BLDG { get; set; }
        public string motheR_APARTMENT { get; set; }
        public string motheR_COUNTRY { get; set; }
        public string motheR_COUNTRY_ID { get; set; }
        public string motheR_CITY { get; set; }
        public string motheR_STATE { get; set; }
        public string motheR_STATE_ID { get; set; }
        public string motheR_OFFICE_PHONE { get; set; }
        public string motheR_RESIDENCE_PHONE { get; set; }
        public string motheR_FAX { get; set; }
        public string motheR_MOBILE { get; set; }
        public string motheR_OCCUPATION { get; set; }
        public string motheR_COMPANY { get; set; }
        public string motheR_EMAIL { get; set; }
        public string motheR_GEMS_STAFF { get; set; }
        public string guardiaN_NAME { get; set; }
        public string guardiaN_FIRSTNAME { get; set; }
        public string guardiaN_MIDNAME { get; set; }
        public string guardiaN_LASTNAME { get; set; }
        public string guardiaN_NATIONALITY { get; set; }
        public string guardiaN_NATIONALITY_ID { get; set; }
        public string guardiaN_COMMUNICATION_ADDRESS_LINE1 { get; set; }
        public string guardiaN_COMMUNICATION_ADDRESS_LINE2 { get; set; }
        public string guardiaN_POBOX { get; set; }
        public string guardiaN_STREET { get; set; }
        public string guardiaN_AREA { get; set; }
        public string guardiaN_BLDG { get; set; }
        public string guardiaN_APARTMENT { get; set; }
        public string guardiaN_COUNTRY { get; set; }
        public string guardiaN_COUNTRY_ID { get; set; }
        public string guardiaN_CITY { get; set; }
        public string guardiaN_STATE { get; set; }
        public string guardiaN_STATE_ID { get; set; }
        public string guardiaN_OFFICE_PHONE { get; set; }
        public string guardiaN_RESIDENCE_PHONE { get; set; }
        public string guardiaN_FAX { get; set; }
        public string guardiaN_MOBILE { get; set; }
        public string guardiaN_OCCUPATION { get; set; }
        public string guardiaN_COMPANY { get; set; }
        public string guardiaN_EMAIL { get; set; }
        public string guardiaN_GEMS_STAFF { get; set; }
        public string fatheR_AREA_ID { get; set; }
        public string motheR_AREA_ID { get; set; }
        public string guardiaN_AREA_ID { get; set; }
    }

    public class GetParentUpdateDetailsRoot
    {
        public List<GetParentUpdateDetails> data { get; set; }
    }
}
