﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentProfileSections
    {
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public int StudentId { get; set; }
        public string Target { get; set; }
        public string Url { get; set; }
        public string ImageSrc { get; set; }
        public int EventType { get; set; }
        public bool IsEnabled { get; set; }
        public string SectionIds { get; set; }
        public long UserId { get; set; }
        public short TotalModulesCount { get; set; }
        public string SectionTitle { get; set; }

    }
}
