﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
 public   class NotificationTrigger
    {
        public int NotificationTriggerId { get; set; }
        public string TriggerName { get; set; }
        public string ModuleName { get; set; }
        public bool IsEmailNoficationEnable { get; set; }
        public bool IsPushNoficationEnable { get; set; }
        public bool IsEmailNotificationAvailable { get; set; }
        public bool IsPushNotificationAvailable { get; set; }

    }

    public class NotificationSettings
    {
        public NotificationSettings()
        {
            NotificationList = new List<NotificationTrigger>();
        }
        public List<NotificationTrigger> NotificationList { get; set; }
    }
}
