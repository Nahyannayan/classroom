﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentWithSection
    {
        public long StudentId { get; set; }
        public string StudentName { get; set; }
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public string GradeDisplay { get; set; }
        public long SchoolGradeId { get; set; }
        public long SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string SchoolShortName { get; set; }
        public string StudentImagePath { get; set; }
    }
}
