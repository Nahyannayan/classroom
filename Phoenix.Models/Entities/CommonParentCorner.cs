﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class CommonParentCorner
    {
        public string Name { get; set; }
    }
    public class PaymentHistory
    {
        public string RecNo { get; set; }
        public string ReceiptDate { get; set; }
        public string Decription { get; set; }
        public long RefNo { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
    }

    public class AccountStatementDetail
    {
        public string Date { get; set; }
        public string RefNo { get; set; }
        public string FeeDescription { get; set; }
        public string Narration { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public string AcademicYear { get; set; }
    }

    public class AccountStatementSummary
    {
        public string AcademicYear { get; set; }
        public string FeeDescription { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        //public string AdvanceDueLabel { get; set; }
    }

    public class PaymentHistoryRoot : CommonResponse
    {
        public List<PaymentHistory> data { get; set; }
    }
    public class AccountStatDetailRoot : CommonResponse
    {
        public List<AccountStatementDetail> data { get; set; }
    }
    public class AccountStatSummaryRoot : CommonResponse
    {
        public List<AccountStatementSummary> data { get; set; }
    }

    #region START Online Payment Response and Redirection
    public class PaymentSummary
    {
        public string IsPaymentSuccessful { get; set; }
        public string TransactionStatusMessage { get; set; }
        public string TransactionStatusCode { get; set; }
        public double TotalAmount { get; set; }
        public string BackUrl { get; set; }
        public string ReferenceNo { get; set; }
        public IEnumerable<PaymentSuccessList> SchoolFeePaymentResultDetails { get; set; }
        public IEnumerable<PaymentSuccessList> ActivityPaymentResultDetails { get; set; }
    }
    
    public class PaymentSuccessList : PCStudentInfo
    {
        public double PaidAmount { get; set; }
        public string ReceiptNumber { get; set; }
        public string PaymentTransactionRefId { get; set; }
        public string ReceiptURL { get; set; }
    }
    public class PaymentSummaryRoot : CommonResponse
    {
        public PaymentSummary data { get; set; }
    }
    #endregion
}
