﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class StudentSuggestion
    {
        public StudentSuggestion()
        {

        }
        public StudentSuggestion(int _studentSuggestionId)
        {
            StudentSuggestionId = _studentSuggestionId;
        }
        public int SchoolId { get; set; }
        public int StudentSuggestionId { get; set; }
        public string StudentSuggestionName { get; set; }
        public string StudentSuggestionNameXml { get; set; }
        public bool IsActive { get; set; }
        public string FormattedCreatedOn { get; set; }
        public int CreatedById { get; set; }
        public string CreatedByName { get; set; }
    }
}
