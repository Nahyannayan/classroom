﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentDetails
    {       
        public string StudentNo { get; set; }
        public string StudentName { get; set; }
        public string StudentID { get; set; }
        public string Grade { get; set; }
        public bool IsSelected { get; set; }
        public string LastDateForSubmition { get; set; }
        public IEnumerable<SubjectDetails> LstSubjectDetails { get; set; }
        public IEnumerable<ExamPaper> LstExamPapers { get; set; }
        public IEnumerable<PaperDetails> LstPaperDetails { get; set; }
        public IEnumerable<ExamPaper> SubjectwiseExamPaperDetailsList { get; set; }
        public double TotalAmount { get; set; }
        public int PaymentTypeID { get; set; }
        public double PaymentProcessingCharge { get; set; }
        public bool OnlinePaymentAllowed { get; set; }
    }
      
    public class SubjectDetails : CommonSubjectDetails
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }      
        public string ExamEndDate { get; set; }       
        public IEnumerable<PaperDetails> GetPaperList { get; set; }
    }
    public class PaperDetails : CommonSubjectDetails
    {      
        public bool IsApprovalRequired { get; set; }
        public int PaperSetupID { get; set; }
    }
   
    public class ExamPaper : CommonSubjectDetails
    {      
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }   
        public bool IsRemove { get; set; }
        public string ApprovalStatus { get; set; }      
        public string PaymentStatus { get; set; }
        public int ExamFeeId { get; set; }      
    }
    public class CommonSubjectDetails
    {
        public string ExamintionLevel { get; set; }
        public string ExamintionBoard { get; set; }
        public string PaperName { get; set; }
        public double PaperCost { get; set; }
        public int ExamPaperSetDetID { get; set; }
        public int ExamSetupID { get; set; }
        public string RegisterStartDate { get; set; }
        public string RegisterEndDate { get; set; }
    }
    public class StudentDetailsRoot : CommonResponse
    {
        public List<StudentDetails> data { get; set; }
    }
    public class SubjectDetailsRoot : CommonResponse
    {
        public List<SubjectDetails> data { get; set; }
    }
    public class ExamPaperRoot : CommonResponse
    {
        public List<ExamPaper> data { get; set; }
    }
}
