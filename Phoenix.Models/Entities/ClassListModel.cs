﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class ClassListModel
    {
        public ClassListModel()
        {
        }
        public ClassListModel(int _TimeTableId)
        {
            TimeTableID = _TimeTableId;
        }
        public int TimeTableID { get; set; }
        public string StudentName { get; set; }
        public string StudentNo { get; set; }
        public string StudentID { get; set; }
        public string StudentImageUrl { get; set; }
        public string StudentGrade { get; set; }
        public string StudentGradeDisplay { get; set; }
        public string StudentSection { get; set; }
        public string StudentFlagStatus { get; set; }
        public int Behaviourpoint { get; set; }
        public int PositivePoint { get; set; }
        public int NegativePoint { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool IsInReport { get; set; }
        public long MeritId { get; set; }
        public DateTime? IncidentDate { get; set; }
        public long StudentGroupId { get; set; }
        public long StudentSectionId { get; set; }
        public bool IsChangeGroupPermission { get; set; }
        public string ParentName { get; set; }
        public string ParentEmailId { get; set; }
        public string ParentMobileNumber { get; set; }
        public bool IsSEN { get; set; }
        public string HouseDesc { get; set; }
    }
    public class StudentOnReportMasterModel
    {
        public long Id { get; set; }
        public long AcademicYearId { get; set; }
        public long SchoolId { get; set; }
        public string GradeId { get; set; }
        public long? SectionId { get; set; }
        public string GroupId { get; set; }
        public long StudentId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
    }
    public class StudentOnReportModel
    {
        public long Id { get; set; }
        public long StudentOnReportMasterId { get; set; }
        public string PeriodNo { get; set; }
        public string PeriodName { get; set; }

        public string BehaviourDescription { get; set; }
        public string Description { get; set; }
        public string GroupId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; } = true;
        public string CourseName { get; set; }
    }
    public class StudentOnReportParameterModel
    {
        public long StudentId { get; set; }
        public long? StudentOnReportMasterId { get; set; } = null;
        public long AcademicYear { get; set; }
        public long SchoolId { get; set; }
        public string CreatedBy { get; set; } = null;
        public string GroupId { get; set; } = null;
    }
    public class BasicDetailModel
    {
        public BasicDetailModel()
        {
            AttendenceBySession = new List<AttendenceBySessionModel>();
        }
        public string StudentID { get; set; }
        public string StudentName { get; set; }
        public string StudentNo { get; set; }
        public string StudentImageUrl { get; set; }
        public string StudentGrade { get; set; }
        public string StudentGradeDisplay { get; set; }
        public string StudentSection { get; set; }
        public string StudentAge { get; set; }
        public string StudentBoard { get; set; }
        public string StudentBSUName { get; set; }
        public string StudentACDYEAR { get; set; }
        public string Nationality { get; set; }
        public string DOB { get; set; }
        public IEnumerable<AttendenceBySessionModel> AttendenceBySession { get; set; }
        public bool IsSEN { get; set; }
        public string HouseDesc { get; set; }
    }
    public class ParentDetailModel
    {
        public string ParentName { get; set; }
        public string ParentRelation { get; set; }
        public string ParentMobile { get; set; }
        public string ParentEmail { get; set; }
        public string ParentOccupation { get; set; }
        public string ParentCountry { get; set; }
        public string ParentCity { get; set; }
        public string ParentNationality { get; set; }
        public string ParentEmirates { get; set; }
        public string ParentAddress { get; set; }
        public string ParentCompany { get; set; }
    }
    public class MedicalDetailModel
    {
        public string Allergies { get; set; }
        public string SpMedication { get; set; }
        public string Physical { get; set; }
        public string Health { get; set; }
        public string Theraphy { get; set; }
        public string SenRemark { get; set; }
        public string EalRemark { get; set; }
        public string Musical { get; set; }
        public string Enrich { get; set; }
        public string Behaviour { get; set; }
        public string Sports { get; set; }
        public string VisualDisability { get; set; }
    }
    public class SiblingDetailModel
    {
        public string Photopath { get; set; }
        public string SiblingId { get; set; }
        public string SiblingNo { get; set; }
        public string SiblingName { get; set; }
        public string SiblingGradeDisplay { get; set; }
        public string SiblingSectionDescr { get; set; }
        public string SiblingDOB { get; set; }
        public string SiblingAge { get; set; }
        public string SiblingSchoolName { get; set; }
    }
    public class StudentDashboardModel
    {
        public string BehaviourPointTotal { get; set; }
        public string BehaviourPointDiff { get; set; }
        public string ActivitiesTotal { get; set; }
        public string ActTotalDiff { get; set; }
        public string Attencence { get; set; }
    }
    public class AchievementsDetailModel
    {

    }
    public class BehaviorDetailModel
    {
        public long IncidentId { get; set; }
        public DateTime IncidentDate { get; set; }
        public string IncidentType { get; set; }
        public DateTime RecordedOn { get; set; }
        public string ReportedBy { get; set; }
        public string IncidentTime { get; set; }
        public long ReportedById { get; set; }
        public long IncidentCategoryId { get; set; }
        public long IncidentSubCategoryId { get; set; }
        public string IncidentRemarks { get; set; }
    }
    public class AttendanceDetailModel
    {

    }
    public class AssessmentDetailModel
    {
        public string Subjects { get; set; }
        public string Term_1 { get; set; }
        public string Term_2 { get; set; }
        public string Term_3 { get; set; }
        public string Average { get; set; }
    }
    public class TransportDetailModel
    {
        public string TransportName { get; set; }
        public string PickupBusNo { get; set; }
        public string DropBusNo { get; set; }
        public string PickupLocation { get; set; }
        public string DropOffLocation { get; set; }
        public string ContactNo { get; set; }
        public string PickupTime { get; set; }
        public string DropOffTime { get; set; }
        public string ProviderSchoolName { get; set; }
    }
    public class AttendenceListModel
    {
        public DateTime AttDate { get; set; }
        public string StudentID { get; set; }
        public string DisplayCodeAM { get; set; }
        public string DisplayCodePM { get; set; }
        public string Description { get; set; }
        public string DescriptionAM { get; set; }
        public string DescriptionPM { get; set; }
        public string WeekDayName { get; set; }
        public int DayNo { get; set; }
        public int DayOfWeekNo { get; set; }
        public int AttDay { get; set; }
        public int WeekNo { get; set; }
        public string MonthName { get; set; }
    }
    public class StudentProfileModel
    {
        public StudentProfileModel()
        {
            BasicDetailModel = new BasicDetailModel();
            ParentDetailModel = new ParentDetailModel();
            MedicalDetailModel = new MedicalDetailModel();
            SiblingDetailModel = new List<SiblingDetailModel>();
            AchievementsDetailModel = new List<AchievementsDetailModel>();
            IncidentDetailModel = new List<BehaviorDetailModel>();
            AttendanceDetailModel = new List<AttendanceDetailModel>();
            TransportDetailModel = new TransportDetailModel();
            StudentDashboardModel = new StudentDashboardModel();
            AttendenceListModel = new List<AttendenceListModel>();
            AssessmentDetailModel = new List<AssessmentDetailModel>();
            BehaviourSubCategoryList = new List<BehaviourSubCategoryModel>();
            IncidentBehaviourPoint = string.Empty;
            WeekDayList = new List<WeekDayModel>();
            OverallPercentageModel = new OverallPercentageModel();
            BehaviourPointModel = new BehaviourPointModel();
        }
        public long Id { get; set; }
        public string IncidentBehaviourPoint { get; set; }
        public bool IsFullView { get; set; }
        public bool IsOpenBySearch { get; set; }
        public BasicDetailModel BasicDetailModel { get; set; }
        public ParentDetailModel ParentDetailModel { get; set; }
        public MedicalDetailModel MedicalDetailModel { get; set; }
        public IEnumerable<SiblingDetailModel> SiblingDetailModel { get; set; }
        public IEnumerable<AchievementsDetailModel> AchievementsDetailModel { get; set; }
        public IEnumerable<BehaviorDetailModel> IncidentDetailModel { get; set; }
        public IEnumerable<AttendanceDetailModel> AttendanceDetailModel { get; set; }
        public IEnumerable<AssessmentDetailModel> AssessmentDetailModel { get; set; }
        public TransportDetailModel TransportDetailModel { get; set; }
        public StudentDashboardModel StudentDashboardModel { get; set; }
        public BehaviourPointModel BehaviourPointModel { get; set; }
        public IEnumerable<AttendenceListModel> AttendenceListModel { get; set; }
        public IEnumerable<BehaviourSubCategoryModel> BehaviourSubCategoryList { get; set; }
        public IEnumerable<WeekDayModel> WeekDayList { get; set; }
        public OverallPercentageModel OverallPercentageModel { get; set; }
        public string PrevStudentName { get; set; }
        public string NextStudentName { get; set; }
    }
    public class BehaviourPointModel
    {
        public BehaviourPointModel()
        {
            BehaviourPoint = string.Empty;
            WeeklyBehaviourPoint = string.Empty;
            MonthlyBehaviourPoint = string.Empty;
        }
        public string BehaviourPoint { get; set; }
        public string WeeklyBehaviourPoint { get; set; }
        public string MonthlyBehaviourPoint { get; set; }
    }
    public class BehaviourSubCategoryModel
    {
        public long MeritId { get; set; }
        public long StudentId { get; set; }
        public long CategoryId { get; set; }
        public int CategoryScore { get; set; }
        public string CategoryName { get; set; }
        public string ImagePath { get; set; }
        public string UploadedPath { get; set; }
        public int MeritUploaded { get; set; }
        public long LevelMapping_ID { get; set; }
    }

    public class AttendanceProfileModel
    {
        public AttendanceProfileModel()
        {
            StudentDetail = new BasicDetailModel();
            AttendenceBySession = new List<AttendenceBySessionModel>();
            AttendenceSessionCode = new List<AttendenceSessionCodeModel>();
            AttendenceList = new List<AttendenceListModel>();
            AttendanceChart = new List<AttendanceChartModel>();
        }

        public BasicDetailModel StudentDetail { get; set; }
        public IEnumerable<AttendenceBySessionModel> AttendenceBySession { get; set; }

        public IEnumerable<AttendenceSessionCodeModel> AttendenceSessionCode { get; set; }

        public IEnumerable<AttendenceListModel> AttendenceList { get; set; }

        public IEnumerable<AttendanceChartModel> AttendanceChart { get; set; }

        public string AttendenceBySessionHTML { get; set; }
        public string AttendenceSessionCodeHTML { get; set; }
        public string AttendenceListHTML { get; set; }
        public string AttendanceChartHTML { get; set; }
        public string AttendanceType { get; set; }
        public decimal OverAllPercentage { get; set; }
        public int AttendanceCount { get; set; }
        public int TotalClassesHeld { get; set; }
    }

    public class AttendenceBySessionModel
    {
        public AttendenceBySessionModel()
        {

            At_AM = 0;
            At_PM = 0;
            WeekDay = string.Empty;

        }
        //public string Day { get; set; }
        //public decimal AM { get; set; }
        //public decimal PM { get; set; }

        public string AttendaceDate { get; set; }
        public decimal At_AM { get; set; }
        public decimal At_PM { get; set; }
        public string WeekDay { get; set; }

    }

    public class AttendenceSessionCodeModel
    {
        public AttendenceSessionCodeModel()
        {
            //Code = string.Empty;
            //Desc = string.Empty;
            //Sessions = 0;
            //Percentage = 0;
            PresentAM = 0;
            PresentPM = 0;
            UnauthorisedAbsentAM = 0;
            UnauthorisedAbsentPM = 0;
            AuthorisedAbsentAM = 0;
            AuthorisedAbsentPM = 0;
            NumberOfLateAM = 0;
            NumberOfLatePM = 0;
            OverallPrecent = 0;
        }
        //public string Code { get; set; }
        //public string Desc { get; set; }
        //public Decimal Sessions { get; set; }
        //public Decimal Percentage { get; set; }
        public decimal PresentAM { get; set; }
        public decimal PresentPM { get; set; }
        public decimal UnauthorisedAbsentAM { get; set; }
        public decimal UnauthorisedAbsentPM { get; set; }
        public decimal AuthorisedAbsentAM { get; set; }
        public decimal AuthorisedAbsentPM { get; set; }
        public decimal NumberOfLateAM { get; set; }
        public decimal NumberOfLatePM { get; set; }
        public decimal OverallPrecent { get; set; }
        public int AttendanceCount { get; set; }
        public int TotalClassesHeld { get; set; }
    }

    public class AttendanceChartModel
    {
        public int Id { get; set; }
        public string ShortMonthName { get; set; }
        public string LongMonthName { get; set; }
        public string TotalPresentDays { get; set; }
        public string CurrentYear { get; set; }
        public int TotalDaysInMonth { get; set; }
        public double MonthWisePercentage { get; set; }
    }
    public class ChangeGroupCourseModel
    {
        public long StudentId { get; set; }
        public long GroupTeacherMappingId { get; set; }
        public long AssignedSchoolGroupId { get; set; }
        public long SchoolGroupId { get; set; }
        public long CourseId { get; set; }
        public string CourseTitle { get; set; }
    }
    public class ChangeSchoolGroupModel
    {
        public long SchoolGroupId { get; set; }
        public string SchoolGroupName { get; set; }
        public string SchoolGroupDescription { get; set; }
        public long CourseId { get; set; }
    }
    public class ChangeGroupStudentModel
    {
        public ChangeGroupStudentModel()
        {
            ChangeGroupCourseList = new List<ChangeGroupCourseModel>();
            ChangeSchoolGroupList = new List<ChangeSchoolGroupModel>();
        }
        public long StudentID { get; set; }
        public string StudentName { get; set; }
        public string StudentNo { get; set; }
        public string StudentImageUrl { get; set; }
        public string StudentGrade { get; set; }
        public string StudentGradeDisplay { get; set; }
        public string StudentSection { get; set; }
        public IEnumerable<ChangeGroupCourseModel> ChangeGroupCourseList { get; set; }
        public IEnumerable<ChangeSchoolGroupModel> ChangeSchoolGroupList { get; set; }
        public DataTable CourseWiseChangeGroupDT()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("GroupTeacherMappingId", typeof(Int64));
            dt.Columns.Add("StudentId", typeof(Int64));
            dt.Columns.Add("AssignedSchoolGroupId", typeof(Int64));
            dt.Columns.Add("SchoolGroupId", typeof(Int64));
            dt.Columns.Add("CourseId", typeof(Int64));
            foreach (var item in ChangeGroupCourseList)
            {
                dt.Rows.Add(item.GroupTeacherMappingId, item.StudentId, item.AssignedSchoolGroupId, item.SchoolGroupId, item.CourseId);
            }
            return dt;
        }
    }

    public class WeekDayModel
    {
        public WeekDayModel()
        {
            TimeTableList = new List<TimeTableModel>();
        }
        public int WeekDayNumber { get; set; }
        public string WeekDayName { get; set; }
        public DateTime WeekDayDate { get; set; }
        public IEnumerable<TimeTableModel> TimeTableList { get; set; }
    }
    public class TimeTableModel
    {
        public int WeekDayNumber { get; set; }
        public DateTime TimetableDate { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string RoomNumber { get; set; }
        public string SchoolGroupName { get; set; }
        public string CourseName { get; set; }
        public string TeacherName { get; set; }
        public bool IsLiveLecture { get; set; }
    }
    public class OverallPercentageModel
    {
        public decimal OverAllPercentage { get; set; }
        public int TotalSchoolDays { get; set; }
        public int MarkedPresentDays { get; set; }
        public int NumberOfDays { get; set; }
    }
}
