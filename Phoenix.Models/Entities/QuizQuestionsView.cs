﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace Phoenix.Models
{
    public class QuizQuestionsView
    {
        public int QuizQuestionId { get; set; }
        public int QuestionTypeId { get; set; }
        public int QuizId { get; set; }
        public string QuizName { get; set; }
        public string QuestionText { get; set; }
        public string QuestionTypeName { get; set; }
        public bool IsVisible { get; set; }
        public bool IsRequired { get; set; }
        public int SortOrder { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public List<Answers> Answers { get; set; }
        public int Marks { get; set; }
        public List<Objective> lstObjectives { get; set; }
        public string Subjects { get; set; }
        public List<string> SubjectIds { get; set; }
        public List<string> SubjectId { get; set; }
        public List<Objective> lstObjective { get; set; }
        public int MaxSubmit { get; set; }
        public string QuestionImagePath { get; set; }
        public string QuizImagePath { get; set; }
        public string SortedQuestionIds { get; set; }
        public List<QuizQuestionFiles> lstDocuments { get; set; }
        public List<string> SortedQuizQuestionIds { get; set; }
        public string SelectedQuestionId { get; set; }
        public string DeselectedQuestionId { get; set; }
        public int TotalCount { get; set; }
        public int RowNum { get; set; }
        public string Courses { get; set; }
        public List<string> CourseIds { get; set; }
        public List<string> CourseId { get; set; }
        public List<Course> lstCourse { get; set; }
    }
    public class Answers
    {
        public int QuizQuestionId { get; set; }
        public int QuizAnswerId { get; set; }
        public string AnswerText { get; set; }
        public int SortOrder { get; set; }
        public bool IsCorrectAnswer { get; set; }
        public string ResourcePath { get; set; }
    }
    public class QuizResponse
    {
        public QuizResponse()
        {
            ResponseQuestionAnswer = new List<QuizAnswersView>();
            QuizFeedbacks = new List<File>();
            QuizQuestionAnswerFiles = new List<QuizQuestionAnswerFiles>();
        }
        public int QuizId { get; set; }
        public int QuizResponseId { get; set; }
        public decimal QuizMarks { get; set; }
        public int QuizStatus { get; set; }
        public int TotalMarks { get; set; }
        public int QuizResponseByUserId { get; set; }
        public int QuizResponseByTeacherId { get; set; }
        public List<QuizAnswersView> ResponseQuestionAnswer { get; set; }
        public int StudentId { get; set; }
        public int TaskId { get; set; }
        public int StudentTaskId { get; set; }
        public int SubmitAttempt { get; set; }
        public string TskId { get; set; }
        public int TimeTaken { get; set; }
        public string Feedback { get; set; }
        public string TeacherName { get; set; }
        public List<File> QuizFeedbacks { get; set; }
        public string FeedbackFrom { get; set; }
        public List<QuizQuestionAnswerFiles> QuizQuestionAnswerFiles { get; set; }
    }
    public class QuizQuestionFiles
    {
        public int QuizQuestionFileId { get; set; }
        public long QuizQuestionId { get; set; }
        public string FileName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long DocumentId { get; set; }
        public string FileExtension { get; set; }
        public string UploadedFileName { get; set; }
        public string FilePath { get; set; }
        public string PhysicalPath { get; set; }

    }
    public class QuizQuestionAnswerFiles
    {
        public long FileId { get; set; }
        public long ResourceId { get; set; }
        public string ResourceType { get; set; }
        public long QuizQuestionId { get; set; }
        public long StudentId { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public string FilePath { get; set; }
        public string PhysicalPath { get; set; }
        public DateTime CreatedOn { get; set; }

    }
    public class QuizResult
    {
        public QuizResult()
        {
            ResponseQuestionAnswer = new List<QuizAnswersView>();
            QuizFeedbacks = new List<File>();
            QuizQuestionAnswerFiles = new List<QuizQuestionAnswerFiles>();
        }
        public int QuizId { get; set; }
        public string ResourceType { get; set; }
        public int QuizResultId { get; set; }
        public decimal QuizMarks { get; set; }
        public int QuizStatus { get; set; }
        public decimal TotalMarks { get; set; }
        public int QuizResponseByUserId { get; set; }
        public int QuizResponseByTeacherId { get; set; }
        public string GradingColor { get; set; }
        public string ShortLabel { get; set; }
        public string GradingItemDescription { get; set; }
        public string GradingTemplateItemSymbol { get; set; }
        public string Percentage { get; set; }
        public int? GradingTemplateId { get; set; }
        public int? GradeId { get; set; }
        public List<QuizAnswersView> ResponseQuestionAnswer { get; set; }
        public List<File> QuizFeedbacks { get; set; }
        public int StudentId { get; set; }
        public int TaskId { get; set; }
        public int SubmitAttempt { get; set; }
        public int ResourceId { get; set; }
        public string TskId { get; set; }
        public string SortedAnswerOrder { get; set; }
        public int TimeTaken { get; set; }
        public int QuizTime { get; set; }
        public string Feedback { get; set; }
        public string TeacherName { get; set; }
        public string  QuizName { get; set; }
        public string StudentName { get; set; }
        public List<QuizQuestionAnswerFiles> QuizQuestionAnswerFiles { get; set; }
        public DateTime ResultDate { get; set; }
    }
    public class QuizFeedback
    {
        public QuizFeedback()
        {
            Files = new List<File>();
        }
        public string feedback { get; set; }
        public int quizId { get; set; }
        public long studentId { get; set; }
        public int QuizResultId { get; set; }
        public List<File> Files { get; set; }
        public long UserId { get; set; }
    }
    public class QuizAnswersView
    {
        public int QuizId { get; set; }
        public int ResourceId { get; set; }
        public int QuizResponseId { get; set; }
        public string ResourceType { get; set; }
        public int QuizAnswerId { get; set; }
        public int QuestionTypeId { get; set; }
        public int QuizQuestionId { get; set; }
        public string QuizAnswerText { get; set; }
        public string QuestionText { get; set; }
        public bool IsCorrectAnswer { get; set; }
        public int SortOrder { get; set; }
        public int CreatedBy { get; set; }
        public int Marks { get; set; }
        public string GradingColor { get; set; }
        public string ShortLabel { get; set; }
        public string GradingItemDescription { get; set; }
        public string GradingTemplateItemSymbol { get; set; }
        public string Percentage { get; set; }
        public int QuizMarks { get; set; }
        public int QuizStatus { get; set; }
        public decimal ObtainedMarks { get; set; }
    }
    public class ImportQuizView
    {
        public ImportQuizView()
        {
            quizQuestionsList = new List<QuizQuestionsView>();
            quizAnswersList = new List<Answers>();
        }
        public List<QuizQuestionsView> quizQuestionsList { get; set; }
        public List<Answers> quizAnswersList { get; set; }
        public int QuizId { get; set; }
        public int CreatedBy { get; set; }

    }
}
