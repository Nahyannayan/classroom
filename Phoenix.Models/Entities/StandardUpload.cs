﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class StandardExcelUploadData
    {
        public DataTable ExcelData { get; set; }
        public bool IsColumnMatch { get; set; }
    }
    public class StandardUploadModel
    {
        public StandardUploadModel()
        {
            StandardExcelList = new List<StandardExcelModel>();
        }
        public long CourseId { get; set; }
        public string Description { get; set; }
        public long SchoolId { get; set; }
        public long UserId { get; set; }
        public List<StandardExcelModel> StandardExcelList { get; set; }
        public DataTable StandardUploadDT
        {
            get
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("OBJ_ID", typeof(int));
                dt.Columns.Add("Unit", typeof(string));
                dt.Columns.Add("SubUnit", typeof(string));
                dt.Columns.Add("StandardCode", typeof(string));
                dt.Columns.Add("StandardDescription", typeof(string));
                dt.Columns.Add("UnitColorCode", typeof(string));
                dt.Columns.Add("UnitStartDate", typeof(DateTime));
                dt.Columns.Add("UnitEndDate", typeof(DateTime));
                dt.Columns.Add("StandardStartDate", typeof(DateTime));
                dt.Columns.Add("StandardEndDate", typeof(DateTime));

                int OBJ_ID = 1;

                StandardExcelList.ToList().ForEach(item =>
                        {
                            dt.Rows.Add(OBJ_ID,
                                item.Unit,
                                item.SubUnit,
                                item.StandardCode,
                                item.StandardDescription,
                                item.UnitColorCode,
                                Convert.ToDateTime(item.UnitStartDate),
                                Convert.ToDateTime(item.UnitEndDate),
                                Convert.ToDateTime(item.StandardStartDate),
                                Convert.ToDateTime(item.StandardEndDate)
                               );
                            OBJ_ID++;
                        });
                return dt;
            }

        }
    }
    public class StandardExcelModel
    {
        public string Unit { get; set; }
        public string SubUnit { get; set; }
        public string StandardCode { get; set; }
        public string StandardDescription { get; set; }
        public string UnitColorCode { get; set; }
        public DateTime UnitStartDate { get; set; }
        public DateTime UnitEndDate { get; set; }
        public DateTime StandardStartDate { get; set; }
        public DateTime StandardEndDate { get; set; }
    }
}
