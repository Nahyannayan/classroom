﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class SchoolBanner
    {     
        public Int64 SchoolBannerId { get; set; }
        public string BannerName { get; set; }
        public string ActualImageName { get; set; }
        public string UploadedImageName { get; set; }
        public DateTime BannerPublishDate { get; set; }
        public DateTime BannerEndDate { get; set; }
        public string ClickURL { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DisplayOrder { get; set; }     
        public string Schools { get; set; }   
        public string UserTypes { get; set; }
        public string SchoolBannerXml { get; set; }

    }
}
