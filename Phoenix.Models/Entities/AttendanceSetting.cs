﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    #region Attendance Calendar
    public class GradeAndSection
    {
        public long SchoolGradeId { get; set; }
        public string GradeDisplay { get; set; }
        public int SectionId { get; set; }
        public string SectionName { get; set; }
    }
    public class SchoolWeekEnd
    {
        public SchoolWeekEnd()
        {
            WEEKEND1 = "Friday";
            WEEKEND2 = "Saturday";
        }
        public string WEEKEND1 { get; set; }
        public string WEEKEND2 { get; set; }
    }
    public class AttendanceCalendar
    {
        public AttendanceCalendar()
        {
            SCH_DATE = DateTime.Today;
            SCH_DTFROM = DateTime.Today;
            SCH_DTTO = DateTime.Today;
            SCH_WEEKEND1_WORK = string.Empty;
            SCH_WEEKEND2_WORK = string.Empty;
            SCH_REMARKS = string.Empty;
            SCH_TYPE = string.Empty;
            SchoolWeekEnd = new SchoolWeekEnd();
            selectedSectionList = new List<string>();
            AcademicYearDetail = new AcademicYearDetail();
        }
        public long SCH_ID { set; get; }
        public DateTime SCH_DATE { set; get; }
        public long SchoolId { set; get; }
        public DateTime SCH_DTFROM { set; get; }
        public DateTime SCH_DTTO { set; get; }
        public string SCH_REMARKS { set; get; }
        public string SCH_TYPE { set; get; }
        public string SCH_WEEKEND1_WORK { set; get; }
        public bool SCH_bWEEKEND1_LOG_BOOK { set; get; }
        public string SCH_WEEKEND2_WORK { set; get; }
        public bool SCH_bWEEKEND2_LOG_BOOK { set; get; }
        public SchoolWeekEnd SchoolWeekEnd { get; set; }
        public List<string> selectedSectionList { get; set; }
        public DataTable selectedSectionListDT
        {
            get
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("GRD_ID", typeof(string));
                dt.Columns.Add("SCT_ID", typeof(Int64));
                selectedSectionList.ToList().ForEach(item =>
                {
                    if (item.Split('_').Count() == 2)
                    {
                        string GRD_ID = item.Split('_')[1];
                        long SCT_ID = Convert.ToInt64(item.Split('_')[0]);
                        dt.Rows.Add(
                                       GRD_ID,
                                       SCT_ID
                                    );
                    }
                });
                return dt;
            }
        }
        public string SelectedGradeSection { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public AcademicYearDetail AcademicYearDetail { get; set; }
        public string GradeDisplay { get; set; }
        public string SectionName { get; set; }
    }
    public class AcademicYearDetail
    {
        public AcademicYearDetail()
        {
            YearStartDate = DateTime.Today;
            YearEndDate = DateTime.Today;
        }
        public int SchoolAcademicYearId { set; get; }
        public string DisplayAcademicYear { set; get; }
        public int AcademicYearId { set; get; }
        public long SchoolId { set; get; }
        public long CurriculumId { set; get; }
        public DateTime YearStartDate { set; get; }
        public DateTime YearEndDate { set; get; }
    }
    #endregion

    #region Parameter Setting
    public class ParameterSetting
    {
        public long ParamaterId { get; set; }
        public long AcdId { get; set; }
        public long SchoolId { get; set; }
        public long ParameterTypeId { get; set; }
        public string ParameterTypeName { get; set; }
        public string ParameterDesc { get; set; }
        public string ReportParameterDesc { get; set; }
        public string ParameterDisplayCode { get; set; }
        public string ParameterDisplayOrder { get; set; }
    } 
    #endregion

    #region Leave approval permission
    public class LeaveApprovalPermissionModel
    {
        private DateTime _fromDate;
        private DateTime _toDate;
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long SchoolId { get; set; }
        public string GradeId { get; set; }
        public string GradeDescription { get; set; }
        public long SalId { get; set; }
        public long SAL_ACD_ID { get; set; }
        public DateTime FromDate { get => _fromDate; set { FromDateStr = value.ToString("dd-MMM-yyyy"); _fromDate = value; } }
        public DateTime ToDate { get => _toDate; set { ToDateStr = value.ToString("dd-MMM-yyyy"); _toDate = value; } }
        public string FromDateStr { get; set; }
        public string ToDateStr { get; set; }
        public int SAL_NDAYS { get; set; }
        public int GradeDisplayOrder { get; set; }
        public IEnumerable<dynamic> LeaveApprovals { get; set; } = new List<dynamic>();
        public int DivisionId { get; set; }
    }
    #endregion

    #region Attendace Period
    public class AttendanceType
    {
        public AttendanceType()
        {
            AttendacePeriodSettingList = new List<AttendacePeriodModel>();
            StartDate = DateTime.Today;
            EndDate = DateTime.Today;
        }
        public long AttendanceTypeId { get; set; }
        public long SchoolId { get; set; }
        public long AcdId { get; set; }
        public long AttendanceTypeSession { get; set; }
        public string AttendanceTypeSessionName { get; set; }
        public long AttendanceSelectType { get; set; }
        public string AttendanceSelectTypeName { get; set; }
        public long GradeId { get; set; }
        public string GradeName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string DataMode { get; set; }
        public long AttendanceSessionConfigurationId { get; set; }
        public long AttendanceConfigurationTypeID { get; set; }
        public List<AttendacePeriodModel> AttendacePeriodSettingList { get; set; }
    }
    public class AttendacePeriodModel
    {
        public AttendacePeriodModel()
        {
            Comment = string.Empty;
        }
        public long AttendancePeriodId { get; set; }
        public long PeriodGradeId { get; set; }
        public int PeriodNo { get; set; }
        public string Weightage { get; set; }
        public string Comment { get; set; }
    }
    #endregion
}
