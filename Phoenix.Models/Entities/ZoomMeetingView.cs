﻿using System;

namespace Phoenix.Models
{
    public class ZoomMeetingView
    {
        public string ZoomMeetingId { get; set; }
        public int SchoolGroupId { get; set; }
        public bool IsSSOEnabled { get; set; }
        public string MeetingName { get; set; }
        public string ZoomEmail { get; set; }
        public string MeetingPassword { get; set; }
        public DateTime MeetingDate { get; set; }
        public string MeetingTime { get; set; }
        public string MeetingDuration { get; set; }
        public int TimeZoneOffset { get; set; }
        public string FormattedMeetingDateTime { get; set; }
        public string ContactName { get; set; }
        public string AccessToken { get; set; }
        public DateTime MeetingDateTime { get; set; }
        public string MeetingURL { get; set; }
        public long CreatedBy { get; set; }
        public int TotalCount { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EndTime { get; set; }
        public bool IsRecurringMeeting { get; set; }
        public string RecurringDays { get; set; }
        public string MeetingEndDateTime { get; set; }
    }
}
