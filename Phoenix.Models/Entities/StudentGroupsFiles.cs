﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentGroupsFiles
    {
        public long FileId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int FileTypeId { get; set; }
        public string Icon { get; set; }
        public string SchoolGroup { get; set; }
        public string TeacherGivenName { get; set; }
        public string SchoolGroupDescription { get; set; }
        public int SchoolGroupId { get; set; }
        public int SchoolId { get; set; }      
        public bool IsCurrentYear { get; set; }
        public string SubjectName { get; set; }
        public bool IsBespokeGroup { get; set; }
        public DateTime CreatedOn { get; set; }

    }
}
