﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class SelectedCourse
    {
        public long OptionID { get; set; }
        public long SubjectGradeID { get; set; }
    }
}
