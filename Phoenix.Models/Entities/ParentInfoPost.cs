﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ParentInfoPost
    {
        public string stU_NO { get; set; }
        public string stU_PREFCONTACT { get; set; }
        public string stS_FFIRSTNAME { get; set; }
        public string stS_FMIDNAME { get; set; }
        public string stS_FLASTNAME { get; set; }
        public string stS_FCOMSTREET { get; set; }
        public string stS_FCOMAREA { get; set; }
        public string stS_FCOMBLDG { get; set; }
        public string stS_FCOMAPARTNO { get; set; }
        public string stS_FCOMPOBOX { get; set; }
        public string stS_FMOBILE { get; set; }
        public string stS_FEMAIL { get; set; }
        public string stS_FCOMPANY { get; set; }
        public int stS_F_COMP_ID { get; set; }
        public string stS_FOCC { get; set; }
        public string stS_FEMIR { get; set; }
        public string stS_FCOMCOUNTRY { get; set; }
        public int stS_FCOMAREA_ID { get; set; }
        public int stS_FCOMSTATE_ID { get; set; }
        public string stS_MFIRSTNAME { get; set; }
        public string stS_MMIDNAME { get; set; }
        public string stS_MLASTNAME { get; set; }
        public string stS_MCOMCOUNTRY { get; set; }
        public int stS_MCOMAREA_ID { get; set; }
        public int stS_MCOMSTATE_ID { get; set; }
        public string stS_MCOMSTREET { get; set; }
        public string stS_MCOMAREA { get; set; }
        public string stS_MCOMBLDG { get; set; }
        public string stS_MCOMAPARTNO { get; set; }
        public string stS_MCOMPOBOX { get; set; }
        public string stS_MMOBILE { get; set; }
        public string stS_MEMAIL { get; set; }
        public string stS_MCOMPANY { get; set; }
        public int stS_M_COMP_ID { get; set; }
        public string stS_MOCC { get; set; }
        public string stS_MEMIR { get; set; }
        public string stS_GFIRSTNAME { get; set; }
        public string stS_GMIDNAME { get; set; }
        public string stS_GLASTNAME { get; set; }
        public string stS_GCOMSTREET { get; set; }
        public string stS_GCOMAREA { get; set; }
        public string stS_GCOMBLDG { get; set; }
        public string stS_GCOMAPARTNO { get; set; }
        public string stS_GCOMPOBOX { get; set; }
        public string stS_GMOBILE { get; set; }
        public string stS_GEMAIL { get; set; }
        public string stS_GCOMPANY { get; set; }
        public int stS_G_COMP_ID { get; set; }
        public string stS_GOCC { get; set; }
        public string stS_GEMIR { get; set; }
        public string stS_GCOMCOUNTRY { get; set; }
        public int stS_GCOMAREA_ID { get; set; }
        public int stS_GCOMSTATE_ID { get; set; }
        public string stS_FEESPONSOR { get; set; }
        public int olU_ID { get; set; }
        public string stS_FOFFPHONE { get; set; }
        public string stS_FRESPHONE { get; set; }
        public string stS_FFAX { get; set; }
        public string stS_MOFFPHONE { get; set; }
        public string stS_MRESPHONE { get; set; }
        public string stS_MFAX { get; set; }
        public string stS_GOFFPHONE { get; set; }
        public string stS_GRESPHONE { get; set; }
        public string stS_GFAX { get; set; }
        public string stU_EMAIL { get; set; }
    }

}
