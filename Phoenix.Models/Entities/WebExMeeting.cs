﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Phoenix.Models
{
    [XmlRoot(ElementName = "response", Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public class WebExStatus
    {
        [XmlElement(ElementName = "result", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string Result { get; set; }
        [XmlElement(ElementName = "gsbStatus", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string GsbStatus { get; set; }
    }

    [XmlRoot(ElementName = "header", Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public class Header
    {
        [XmlElement(ElementName = "response", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public WebExStatus Response { get; set; }
    }

    [XmlRoot(ElementName = "iCalendarURL", Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    public class ICalendarURL
    {
        [XmlElement(ElementName = "host", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string Host { get; set; }
        [XmlElement(ElementName = "attendee", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string Attendee { get; set; }
    }

    [XmlRoot(ElementName = "bodyContent", Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public class WebExMeetingData
    {
        public int SchoolGroupId { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public short Duration { get; set; }
        public string MeetingName { get; set; }
        public long CreatedBy { get; set; }
        [XmlElement(ElementName = "meetingkey", Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string Meetingkey { get; set; }
        [XmlElement(ElementName = "meetingPassword", Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string MeetingPassword { get; set; }
        [XmlElement(ElementName = "iCalendarURL", Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public ICalendarURL ICalendarURL { get; set; }
        [XmlElement(ElementName = "guestToken", Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string GuestToken { get; set; }
    }


    [XmlRoot(ElementName = "body")]
    public class Body
    {
        [XmlElement(ElementName = "bodyContent", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public WebExMeetingData BodyContent { get; set; }
    }

    [XmlRoot(ElementName = "message", Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public class WebExMeeting
    {
        [XmlElement(ElementName = "header", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public Header Header { get; set; }
        [XmlElement(ElementName = "body", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public Body Body { get; set; }
    }
}
