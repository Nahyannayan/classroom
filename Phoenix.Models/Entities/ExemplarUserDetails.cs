﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
  public  class ExemplarUserDetails
    {
        public long? ExemplarWallId { get; set; }
        public long? SchoolId { get; set; }
        public string PostTitle { get; set; }
        public string PostDescription { get; set; }
        public string TaggedGroup { get; set; }
        public string TaggedStudent { get; set; }
        public string WinnerStudent { get; set; }
        public bool? IsDepartmentWall { get; set; }
        public bool? IsCourseWall { get; set; }
        public string SchoolLevelId { get; set; }
        public string ReferenceLink { get; set; }
        public string EmbededVideoLink { get; set; }
        public string AdditionalDocPath { get; set; }
        public string ParentSharableLink { get; set; }
        public string FileNames { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<DateTime> UpdatedOn { get; set; }
        public Nullable<long> DeletedBy { get; set; }
        public Nullable<DateTime> DeletedOn { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<long> UserId { get; set; }
        public int SortOrder { get; set; }
        public int IsPublish { get; set; }
        public int BlogTypeId { get; set; }
        public string DeleteReason { get; set; }
        public long? CourseId { get; set; }
        public bool? IsStudentWall { get; set; }
        public List<TaggedStudentList> studentList { get; set; }
        public int MyExeplarWork { get; set; }
        public bool ApproveStatus { get; set; }
        public string DepartmentName { get; set; }
        public TaggedGroup AssignedGroup { get; set; }
        public TaggedCourse AssignedCourse { get; set; }
    }

    public class TaggedGroup {
        public int SchoolGroupId { get; set; }
        public string SchoolGroupName { get; set; }
        public int ExemplarWallId { get; set; }
    }

    public class TaggedCourse
    {
        public int COR_ID { get; set; }
        public string Cor_Title { get; set; }
        public int ExemplarWallId { get; set; }
    }

    public class TaggedStudentList {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string UserImageFilePath { get; set; }
        public int ExemplarPostId { get; set; }
        public int MyExeplarWork { get; set; }

    }

}
