﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class TeacherList
    {
        public long Id { get; set; }
        public string TeacherName { get; set; }
    }
}
