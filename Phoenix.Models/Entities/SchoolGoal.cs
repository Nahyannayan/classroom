﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class SchoolGoal
    {
        public SchoolGoal()
        {

        }
        public SchoolGoal(int _GoalId)
        {
            GoalId = _GoalId;
        }
        public int GoalId { get; set; }
        public string GoalName { get; set; }
        public string GoalDescription { get; set; }
        public DateTime ExpectedDate { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public String CreatedByName { get; set; }
        public String CreatedOn { get; set; }
        public string FormattedCreatedOn { get; set; }
        public bool IsApproved { get; set; }
        public string ApprovedBy { get; set; }
    }
}
