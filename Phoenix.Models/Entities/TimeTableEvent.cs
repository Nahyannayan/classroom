﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class TimeTableEvent
    {
        //[{"periodDay":"0","fromTime":"7:40","toTime":"7:50","subjectDescription":"Class Teacher","roomNo":"6H","teacherName":"Rashiem Sachdeva"},
        //public int PeriodDay { get; set; }
        //public DateTime FromTime { get; set; }
        //public DateTime ToTime { get; set; }
        //public string SubjectDescription { get; set; }
        //public string RoomNumber { get; set; }
        //public string TeacherName { get; set; }

        public string periodDay { get; set; }
        public DateTime fromTime { get; set; }
        public DateTime toTime { get; set; }
        public string subjectDescription { get; set; }
        public string roomNo { get; set; }
        public string teacherName { get; set; }
    }

    public class ResCategory
    {
        public int categoryID { get; set; }
        public string schoolID { get; set; }
        public string categoryDescription { get; set; }
        public string categoryThumbNailUrl { get; set; }
        public List<Resource> resources { get; set; }
    }

    public class Resource
    {
        public int resourceID { get; set; }
        public string resourceTitle { get; set; }
        public string resourceDescription { get; set; }
        public DateTime resourceDateTime { get; set; }
        public string resourceAttachmentUrl { get; set; }
        public string resourceThumbNailUrl { get; set; }
        public string FileIcon { get; set; }
    }

    public class GalleryFile
    {
        public int FileID { get; set; }
        //public int FileParentID { get; set; }
        public string FileName { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsDirectory { get; set; }
        public string FileExtention { get; set; }
        public string FilePath { get; set; }
        public string SchoolId { get; set; }
    }

    public class Breadcrumb
    {
        public string MenuId { get; set; }
        public string MenuName { get; set; }
        public bool IsActive { get; set; }
    }
}
