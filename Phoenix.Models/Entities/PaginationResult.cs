﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class PaginationResult<T> where T : class
    {
        public IEnumerable<T> items { get; set; }
        public int totalCount { get; set; }
        public int filteredCount { get; set; }
        public int pageSize { get; set; }
    }
}
