﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phoenix.Models;

namespace Phoenix.Models
{
    public class MyFilesModel
    {
        public MyFilesModel()
        {
            Files = new List<File>();
            Folders = new List<Folder>();
        }
        public int ModuleId { get; set; }
        public int ParentFolderId { get; set; }
        public int SectionId { get; set; }
        public IEnumerable<File> Files { get; set; }
        public IEnumerable<Folder> Folders { get; set; }
    }
}