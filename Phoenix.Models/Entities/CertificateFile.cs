﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.Models
{
    public class CertificateFile
    {
        public int TemplateId { get; set; }
        public long UserId { get; set; }
        public long CertificateUserId { get; set; }
        public short CertificateTypeId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int FileTypeId { get; set; }
        public string CertificateDescription { get; set; }
        public bool IsAutoApproval { get; set; }
        public int CertificateId { get; set; }
        public string PhysicalFilePath { get; set; }
    }
}
