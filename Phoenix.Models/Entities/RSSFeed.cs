﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
   public class RSSFeed
    {
        public int Id { get; set; }
        public string RSSTitle { get; set; }
        public string RSSLink { get; set; }
        public string RSSDescription { get; set; }
       
    }
}
