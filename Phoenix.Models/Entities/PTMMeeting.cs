﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class PTMMeeting
    {
        public int MeetingId { get; set; }
        public string MeetingTitle { get; set; }
        public string MeetingDetails { get; set; }
        public string MeetingDescription { get; set; }
        public int MeetingCategoryId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public TimeSpan MeetingSlotInTime { get; set; }
        public TimeSpan BreakTimeBetweenSlots { get; set; }
        public string Grades_YearGroups { get; set; }
        public bool IsAutoAllocatedTimeSlots { get; set; }
        public bool IsRecurringMeeting { get; set; }
        public int UserId { get; set; }

    }
    public class MeetingSlots
    {
        public int Id { get; set; }
        public int MeetingId { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public int ParentId { get; set; }
        public int TeacherId { get; set; }
        public bool IsTeacherAccepted { get; set; }
        public int DeclinedReasonId { get; set; }
        public bool IsParentEmailSent { get; set; }
        public string IsParentPresent { get; set; }

    }
    public class TeacherLunchBreakSlot
    {
        public int Id { get; set; }
        public int TeacherId { get; set; }
        public int MeetingId { get; set; }
        public TimeSpan LunchStartTime { get; set; }
        public TimeSpan LunchEndTime { get; set; }
    }
    public class DeclineReasonList
    {
        public DeclineReasonList()
        {

        }
        public DeclineReasonList(int _DeclineReasonId)
        {
            DeclineReasonId = _DeclineReasonId;
        }
        public int DeclineReasonId { get; set; }
        public string DeclineReasonName { get; set; }
        public string DeclineReasonDescription { get; set; }
        public bool IsApproved { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedOn { get; set; }
    }

    public class DaysMapping
    {
        public int Id { get; set; }
        public int MeetingId { get; set; }
        public int Day { get; set; }

    }
    public class CategoryList
    {
        public CategoryList()
        {

        }
        public CategoryList(int _CategoryId)
        {
            CategoryId = _CategoryId;
        }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public bool IsApproved { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedOn { get; set; }
    }
}
