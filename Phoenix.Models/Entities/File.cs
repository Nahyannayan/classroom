﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.Models
{
    public class File
    {
        public File()
        {

        }
        public File(int _fileId)
        {
            FileId = _fileId;
        }
        public long FileId { get; set; }
        public long SchoolId { get; set; }
        public string Extension { get; set; }
        public int ModuleId { get; set; }
        public long SectionId { get; set; }
        public long FolderId { get; set; }
        public string FolderName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int FileTypeId { get; set; }
        public string FileTypeName { get; set; }
        public string Icon { get; set; }
        public bool PinToHome { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

        public double FileSizeInMB { get; set; }
        public short ResourceFileTypeId { get; set; }
        public string GoogleDriveFileId { get; set; }
        public string PhysicalFilePath { get; set; }
        public string FlatIcon { get; set; }
    }

    public class FileExplorer
    {
        public int ModuleId { get; set; }
        public long SectionId { get; set; }
        public long FolderId { get; set; }
        public List<Folder> Folders { get; set; }
        public List<File> Files { get; set; }
        public List<GroupCourseTopic> GroupCourseTopics { get; set; }
        public List<GroupUrl> GroupUrls { get; set; }
        public List<GroupQuiz> GroupQuizzes { get; set; }
        public List<GroupQuiz> Forms { get; set; }
        public List<AsyncLesson> AsyncLessons { get; set; }
        public List<Assignment> Assignments { get; set; }
        public FileExplorer()
        {
            Folders = new List<Folder>();
            Files = new List<File>();
            GroupCourseTopics = new List<GroupCourseTopic>();
            GroupUrls = new List<GroupUrl>();
            GroupQuizzes = new List<GroupQuiz>();
            Forms = new List<GroupQuiz>();
            AsyncLessons = new List<AsyncLesson>();
            Assignments = new List<Assignment>();
        }
    }
}
