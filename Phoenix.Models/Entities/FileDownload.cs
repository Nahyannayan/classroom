﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class FileDownload
    {
        public int FileId { get; set; }
        public string FilePath { get; set; }
        public string FolderName { get; set; }
        public string FileName { get; set; }

    }


}
