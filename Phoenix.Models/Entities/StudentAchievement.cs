﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentAchievement
    {
        public long AchievementId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string EventDate { get; set; }
        public long CreatedBy { get; set; }
        public long UpdatedBy { get; set; }
        public long DeletedBy { get; set; }
        public long StudentId { get; set; }
        public bool IsApproved { get; set; }
        public bool ShowOnPortfolio { get; set; }
        public int TotalCount { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string ThumbnailPath { get; set; }
        public bool IsCertificate { get; set; }
        public string PhysicalFilePath { get; set; }
        public long TeacherId { get; set; }
        public char Mode { get; set; }
        public List<AchievementFiles> lstAchievementFiles { get; set; }
    }

    public class AchievementFiles
    {
        public long AchievementId { get; set; }
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public long UserId { get; set; }
        public long CreatedBy { get; set; }
        public string PhysicalFilePath { get; set; }
    }
}


