﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class GetOption
    {
        public int OptionID { get; set; }
        public string OptionDesc { get; set; }
        public List<GetOptionDetails> OptionDetailList { get; set; }
    }
    //new property addedd
    public class StudentSubjectInfo
    {
        public string StudentNumber { get; set; }
        public string OptionGuidelines { get; set; }
        public string AcademicYear { get; set; }
        public string Grade { get; set; }
        public bool IsCourseAlreadySelected { get; set; }
        public List<GetOption> Getoption { get; set; }
    }
    public class GetOptionRoot
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public object responseCode { get; set; }
        public object message { get; set; }
        public List<GetOption> data { get; set; }
    }
    public class StudentListInfo
    {
        public string StudentFirstName { get; set; }
        public string StudentLastName { get; set; }
        public int SchooolId { get; set; }
        public string StudentNumber { get; set; }
        public int StudentId { get; set; }
        public int schoolAcademicYearid { get; set; }
    }
    public class SelectSubjectDashaboard
    {
        
        public StudentSubjectInfo Studentsubjectinfo { get; set; }
        public List<StudentListInfo> StudentlistInfo { get; set; }
    }
    public class SelectCoursesList
    {
        public string OptionID { get; set; }
        public int SubjectGradeID { get; set; }
        public string StudentId { get; set; }

    }


    public class SaveCourse
    {
        public string OptionID { get; set; }
        public int SubjectGradeID { get; set; }

    }
    public class SaveCourseModel
    {
        public string StudentID { get; set; }
        public string User { get; set; }
        public List<SaveCourse> SelectCoursesList { get; set; }

    }

}



