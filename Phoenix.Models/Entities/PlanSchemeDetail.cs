﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class PlanSchemeDetail
    {
        public PlanSchemeDetail()
        {

        }

        public PlanSchemeDetail(Int64 _planSchemeDetailId)
        {
            PlanSchemeDetailId = _planSchemeDetailId;
        }

        public Int64 PlanSchemeDetailId { get; set; }
        public Int64 TemplateId { get; set; }
        public string PlanSchemeName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int Status { get; set; }
        public string StatusText { get; set; }
        public bool IsActive { get; set; }
        public bool CanApprovePlan { get; set; }
        public Int64 CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string TemplateName { get; set; }
        public string SelectedTeacherIds { get; set; }
        public bool IsAdminApprovalRequired { get; set; }

        //Added By Shankar
        public string GroupList { get; set; }
        public string UnitList { get; set; }
        public long SchoolId { get; set; }
        public int TotalCount { get; set; }
        public bool IsRejected { get; set; }
        public List<SharedLessonPlanTeacherList> SharedLessonPlanTeacherList { get; set; }
        public string SharePointUploadFilePath { get; set; }
        public string ShareableLink { get; set; }

        

    }
}
