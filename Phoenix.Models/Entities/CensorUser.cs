﻿

namespace Phoenix.Models
{
   
    public class CensorUser 
    {
        public CensorUser()
        {

        }

        public CensorUser(int _CensorUserId)
        {
            CensorUserId = _CensorUserId;
        }
        public int CensorUserId { get; set; }
        public int AbuseContactInfoId { get; set; }
        public string CensorUserName { get; set; }
        public string ContactMessage { get; set; }
        public string ContactEmail { get; set; }

        public bool IsActive { get; set; }
        public int SchoolId { get; set; }
        public string FormattedCreatedOn { get; set; }
        public int CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public string Actions { get; set; }
    }
}
