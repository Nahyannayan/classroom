﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Phoenix.Common;
using Phoenix.Models.Entities;

namespace Phoenix.Models
{
    public class Unit
    {
        public long UnitId { get; set; }
        public long CourseId { get; set; }
        public string UnitName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string UnitColorCode { get; set; }
        public string CourseName { get; set; }

        public string StartWeek { get; set; }
        public string EndWeek { get; set; }
        public long ParentId { get; set; }
        public int NewAssingmentCount { get; set; }
    }



    public class UnitDetailsType :Attachments
    {
        public long UnitId { get; set; }
        public long UnitTypeId { get; set; }
        public long UnitDetailsId { get; set; }
        public string Title { get; set; }
        public string ControlType { get; set; }
        public string GroupName { get; set; }
        public int TypeOrder { get; set; }
        public bool HasAttachment { get; set; }
        public string AttachmentKey { get; set; }
        public string Description { get; set; }

        public string ContentDescription { get; set; }
        public bool IsRequired { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }

    public class UnitTopic
    {
        public int TopicID { get; set; }
        public string Topic_Description { get; set; }
        public int ParentID { get; set; }


    }

    public class UnitTopicStandardDetails
    {
        public int ParentID { get; set; }
        public int Sub_TOPIC_STM_ID { get; set; }
        [AllowHtml]
        public string SUB_TOPIC_DESCRIPTION { get; set; }
        [AllowHtml]
        public string PARENT_TOPIC_DESCRIPTION { get; set; }
        public int STD_ID { get; set; }
        [AllowHtml]
        public string STANDARD_DETAIL_DESCRIPTION { get; set; }
        public long SCMID { get; set; }
        public string StandardCode { get; set; }
        public int Targeted { get; set; }
        public int Assessed { get; set; }
    }

    public class UnitCalendar
    {

        public int Acd_Id { get; set; }
        public string YearStartDate { get; set; }
        public string YearEndDate { get; set; }
        public IEnumerable<Unit> UnitDetails { get; set; }
        public Course CourseDetails { get; set; }
        public IEnumerable<UnitWeek> WeekList {get;set;}
    }

    public class UnitWeek
    {
        public int Id { get; set; }
        public DateTime WeekStart { get; set; }
        public string WeekStartString
        {
            get
            {
                return WeekStart.ToString("dd-MMM-yyyy");
            }
        }
        public DateTime WeekEnd { get; set; }
        public string WeekEndString
        {
            get
            {
                return WeekEnd.ToString("dd-MMM-yyyy");
            }
        }
        public string Description { get; set; }
    }
    public class UnitDetailAttachment
    {
        public long UTD_ID { get; set; }
        public string AttachmentKey { get; set; }
    }
    

}
