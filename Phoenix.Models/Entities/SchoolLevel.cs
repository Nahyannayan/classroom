﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
   public class SchoolLevel
    {
        public long SchoolLevelId { get; set; }

        public long SchoolId { get; set; }
        public string SchoolLevelName { get; set; }

        
    }
}
