﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class SearchMyWall
    {
        public SearchMyWall()
        {

        }
        public SearchMyWall(int _searchId)
        {
            SearchId = _searchId;
        }

        public int SearchId { get; set; }
        public string SearchText { get; set; }       
        public long? SchoolGroupId { get; set; }
       
        public long? UserId { get; set; }
        
        public long? BlogId { get; set; }       

        public string SearchImage { get; set; }
    }
}
