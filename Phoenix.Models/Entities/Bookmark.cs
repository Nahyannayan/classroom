﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class Bookmark
    {
        public long BookmarkId { get; set; }
        public string BookmarkName { get; set; }
        public string BookmarkDescription { get; set; }
        public string URL { get; set; }
        public List<SchoolGroup> lstSchoolGroup { get; set; }
        public string ThumbnailImage { get; set; }
        public string SelectedSchoolGroupIds { get; set; }
        public int UserId { get; set; }
        public string CreatedByName { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
