﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class SuggestionCategory
    {
        public SuggestionCategory()
        {

        }
        public SuggestionCategory(int _suggestionCategory)
        {
            SuggestionCategoryId = _suggestionCategory;
        }
        public int SuggestionCategoryId { get; set; }
        public string SuggestionCategoryName { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedByName { get; set; }
        public int SchoolId { get; set; }
        public bool IsActive { get; set; }
        public int CreatedById { get; set; }
        public string FormattedCreatedOn { get; set; }
        public string SuggestionCategoryXml { get; set; }

    }
}
