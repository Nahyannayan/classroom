﻿using System;

namespace Phoenix.Models
{
   public class RecordedSessionView
    {
        public string MeetingId { get; set; }
        public string  SharableMeetingLink { get; set; }
        public string MeetingName { get; set; }
        public string HostName { get; set; }
        public int RecordingCount { get; set; }
        public string  MeetingDateTime { get; set; }
        public int TotalCount { get; set; }
        public DateTime MeetingDate { get; set; }
    }
}
