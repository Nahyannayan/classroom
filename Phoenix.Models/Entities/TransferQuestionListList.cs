﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class TransferQuestionListList
    {
        public int QuestionId { get; set; }
        public int QuestionSrNo { get; set; }
        public string Question { get; set; }
    }


    public class TransferQuestionListRoot : JasonResult
    {
        public List<TransferQuestionListList> data { get; set; }
    }
}
