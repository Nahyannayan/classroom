﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentCertificate
    {
        public short CertificateTypeId { get; set; }
        public string CertificateTypeName { get; set; }
        public int StudentId { get; set; }
        public long UserId { get; set; }
        public string FileName { get; set; }
        public int CertificateId { get; set; }
        public bool IsActive { get; set; }
        public bool IsApproved { get; set; }
        public string FilePath { get; set; }
        public string CertificateDescription { get; set; }
        public long ApprovedBy { get; set; }
        public bool ShowOnDashboard { get; set; }
        public string PhysicalFilePath { get; set; }
        public bool IsTeacherAssignedCertificate { get; set; }
        public string FileCssClass { get; set; }
    }
}
