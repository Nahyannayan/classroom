﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Themes
    {
        public int ThemeId { get; set; }
        public int SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string ThemeName { get; set; }
        public string ThemeCssFileName { get; set; }
        public string ThemePreviewImage { get; set; }
        public int SchoolGradeId { get; set; }
        public string SchoolGradeName { get; set; }
        public string SchoolGradeIds { get; set; }
    }
}
