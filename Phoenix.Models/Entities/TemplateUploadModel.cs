﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
//using Phoenix.Common.CustomAttributes;

namespace Phoenix.Models
{
   // [ResourceMappingRoot(Path = "TemplateUpload.TemplateUpload")]
    public class TemplateUploadModel
    {
        public List<Template_M> Template_M { get; set; }

        //[Display(Name = "Template Type"), Required(ErrorMessage = "Please select template type")]
        public string TEMPLATE_CODE { get; set; }
       
        public HttpPostedFileBase TemplateFile { get; set; }
    }
    public class StudentUploadTemplate
    {

        public string TSTU_SCHOOL_NAME { get; set; }
        public string TSTU_FEEID_OR_ADMISSION_NUMBER { get; set; }
        public string TSTU_SIBLIN_REF { get; set; }
        public string TSTU_FIRSTNAME { get; set; }
        public string TSTU_MIDNAME { get; set; }
        public string TSTU_LASTNAME { get; set; }
        public string TSTU_GENDER { get; set; }
        public string TSTU_NATIONALITY { get; set; }
        public string TSTU_DOB { get; set; }
        public string TSTU_ACY_DESCR { get; set; }
        public string TSTU_CLM_DESCR { get; set; }
        public string TSTU_GRD_ID { get; set; }
        public string TSTU_GRD_DESCR { get; set; }
        public string TSTU_SCT_DESCR { get; set; }
        public string TSTU_STREAM_DESCR { get; set; }
        public string TSTU_PRIMARYCONTACT { get; set; }
        public string TSTU_PRIMARYCONTACT_FNAME { get; set; }
        public string TSTU_PRIMARYCONTACT_SNAME { get; set; }
        public string TSTU_PRIMARYCONTACT_LNAME { get; set; }
        public string TSTU_PRIMARYCONTACT_MOBILE { get; set; }
        public string TSTU_PRIMARYCONTACT_EMAIL { get; set; }
        public string Return_Status { get; set; }
        public string Error_Msg { get; set; }

    }

    public class BusinessUnitUploadTemplate
    {

        public string TBSU_SHORTNAME { get; set; }
        public string TBSU_NAME { get; set; }
        public string TBSU_EMAIL { get; set; }
        public string TBSU_CITY { get; set; }

    }
    public class GRADE_WISE_SUBJECT_M
    {

        public string TGSM_SCHOOL_NAME { get; set; }
        public string TGSM_ACY_DESCR { get; set; }
        public string TGSM_CLM_DESCR { get; set; }
        public string TGSM_GRD_DESCR { get; set; }
        public string TGSM_STM_DESCR { get; set; }
        public string TGSM_SBJ_DESCR { get; set; }
        public string TGSM_OPTIONAL_SBJ { get; set; }
        public string TGSM_SBJ_TEACHER { get; set; }
        public string TGSM_PARENT_SBJ { get; set; }

        public string Return_Status { get; set; }
        public string Error_Msg { get; set; }


    }
    public class StaffUploadTemplate
    {

        public string TSTF_SCHOOL_NAME { get; set; }
        public string TSTF_NO { get; set; }
        public string TSTF_FIRSTNAME { get; set; }
        public string TSTF_MIDNAME { get; set; }
        public string TSTF_LASTNAME { get; set; }
        public string TSTF_GENDER { get; set; }
        public string TSTF_DOB { get; set; }
        public string TSTF_EMAIL { get; set; }
        public string TSTF_MOBILE { get; set; }
        public string TSTF_ROLE { get; set; }

        public string Return_Status { get; set; }
        public string Error_Msg { get; set; }

    }
    public class STUDENT_WISE_SUBJECT_MAPPING
    {
        public string TSSM_SCHOOL_NAME { get; set; }
        public string TSSM_FEEID_OR_ADMISSION_NUMBER { get; set; }
        public string TSSM_SBJ_ID { get; set; }
        public string Return_Status { get; set; }
        public string Error_Msg { get; set; }
    }
    public class Template_M
    {
        public string TEMPLATE_CODE { get; set; }
        public string TPT_NAME { get; set; }
    }

    public class Template_Model
    {
        public System.Web.HttpPostedFileBase UploadTemplateFile { get; set; }
        public List<Template_M> Template { get; set; }
    }

}
