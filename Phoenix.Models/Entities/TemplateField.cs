﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class TemplateField
    {
        public TemplateField()
        {

        }

        public TemplateField(Int64  _TemplateFieldId)
        {
            TemplateFieldId = _TemplateFieldId;
        }

        public Int64 TemplateFieldId { get; set; }
        public Int64 TemplateId { get; set; }
        public string Field { get; set; }
        public string Placeholder { get; set; }
        public bool IsLabel { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime UpdatedOn { get; set; }
        public Int64 UpdatedBy { get; set; }
        public int? CertificateColumnId { get; set; }
        public string CertificateColumnValue { get; set; }
        public string FieldValue { get; set; }
        public List<TemplateCollectionList> TimeTableList { get; set; }
        public List<TemplateCollectionList> GradeList { get; set; }
    }


    public class TemplateCollectionList {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
