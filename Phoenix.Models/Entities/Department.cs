﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Department
    {
        public long DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public long SchoolId { get; set; }
        public int CurriculumId { get; set; }
        public long[] CourseId { get; set; }
        public string CourseIds { get; set; }
        public string CoursesName { get; set; }
        public long CreatedBy { get; set; }
        public string DATAMODE { get; set; }
    }
}
