﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class GroupCourseTopic
    {
        public GroupCourseTopic()
        {

        }
        public GroupCourseTopic(long groupCourseTopicId)
        {
            GroupCourseTopicId = groupCourseTopicId;
        }
        public long GroupCourseTopicId { get; set; }
        public long GroupId { get; set; }
        public long TopicId { get; set; }
        public string  TopicName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsSubTopic { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime UpdatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public int SortOrder { get; set; }
    }
}
