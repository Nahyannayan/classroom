﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class TCAnswer
    {
        public long TransferQuestionID { get; set; }
        public string Answer { get; set; }
    }
}
