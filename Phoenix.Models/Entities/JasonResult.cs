﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class JasonResult
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public object responseCode { get; set; }
        public object message { get; set; }
    }
}
