﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Phoenix.Models
{
    [XmlRoot(ElementName ="results")]
    public class AdobeConnectMeeting
    {
        public string MeetingId { get; set; }
        public int SchoolGroupId { get; set; }
        [XmlElement(ElementName="date-begin")]
        public string StartDateTime { get; set; }
        [XmlElement(ElementName="date-end")]
        public string EndDateTime { get; set; }
        [XmlElement(ElementName="name")]
        public string MeetingName { get; set; }
        [XmlElement(ElementName="url-path")]
        public string MeetingUrlSegment { get; set; }
        [XmlAttribute("folder-id")]
        public string FolderId { get; set; }
        [XmlElement(ElementName="date-created")]
        public string CreatedDateTime { get; set; }
        public long CreatedBy { get; set; }
    }

    [XmlRoot(ElementName = "status")]
    public class Status
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "sco")]
    public class Sco
    {
        [XmlElement(ElementName = "date-begin")]
        public string Datebegin { get; set; }
        [XmlElement(ElementName = "date-created")]
        public string Datecreated { get; set; }
        [XmlElement(ElementName = "date-end")]
        public string Dateend { get; set; }
        [XmlElement(ElementName = "date-modified")]
        public string Datemodified { get; set; }
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "url-path")]
        public string Urlpath { get; set; }
        [XmlAttribute(AttributeName = "account-id")]
        public string Accountid { get; set; }
        [XmlAttribute(AttributeName = "disabled")]
        public string Disabled { get; set; }
        [XmlAttribute(AttributeName = "display-seq")]
        public string Displayseq { get; set; }
        [XmlAttribute(AttributeName = "folder-id")]
        public string Folderid { get; set; }
        [XmlAttribute(AttributeName = "icon")]
        public string Icon { get; set; }
        [XmlAttribute(AttributeName = "lang")]
        public string Lang { get; set; }
        [XmlAttribute(AttributeName = "max-retries")]
        public string Maxretries { get; set; }
        [XmlAttribute(AttributeName = "sco-id")]
        public string Scoid { get; set; }
        [XmlAttribute(AttributeName = "source-sco-id")]
        public string Sourcescoid { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
    }

    [XmlRoot(ElementName = "results")]
    public class Results
    {
        [XmlElement(ElementName = "status")]
        public Status Status { get; set; }
        [XmlElement(ElementName = "sco")]
        public Sco Sco { get; set; }
    }
}
