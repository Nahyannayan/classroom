﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class GetReportDetail
    {
        public int UserCount  { get; set; }
        public int TotalAssignmentCount  { get; set; }
        public int  SchoolTaskFileCount { get; set; }
        public int AssignmentFileCount { get; set; }
        public int TeacherFileCount { get; set; }
        public int StudentAssignmentFileCount { get; set; }
        public int StudentTaskFilesCount { get; set; }
        public int GroupFileCount { get; set; }
        public int MasterGroupFileCount { get; set; }
        public int QuizCount { get; set; }
        public int QuizFileCount { get; set; }
        public int BespokeGroupCreated { get; set; }
        public int ChatterCount { get; set; }
        public int ChatterCommentsCount { get; set; }
        public int Reportradio { get; set; }
        public int BeSpokeGroupFilesCount { get; set; }
        public string FilterName { get; set; }

    }
}



