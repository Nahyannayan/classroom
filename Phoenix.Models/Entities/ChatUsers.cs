﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ChatUsers
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int StudentId { get; set; }
        
        public int TeacherId { get; set; }
        public List<ChatUsers> StudentList { get; set; }
        public List<ChatUsers> TeacherList { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int UserTypeId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
