﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentAcademic
    {
        public StudentAcademic()
        {
            Documents = new List<AcademicDocuments>();
        }
        public List<AcademicDocuments> Documents { get; set; }
    }

    public class AcademicDocuments
    {
        public string AcademicTitle { get; set; }
        public long UserId { get; set; }
        public long FileId { get; set; }
        public string FilePath { get; set; }
        public short FileTypeId { get; set; }
        public string FileTypeName { get; set; }
        public string FileName { get; set; }
        public string DocumentType { get; set; }
        public string FileCssClass { get; set; }
        public bool ShowOnDashboard { get; set; }
        public int AssignmentId { get; set; }
        public int StudentId { get; set; }
        public string StudentAssignmentFileIds { get; set; }
        public short ResourceFileTypeId { get; set; }
    }
}
