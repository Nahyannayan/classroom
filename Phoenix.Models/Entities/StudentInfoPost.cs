﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentInfoPost
    {
        public string stU_NO { get; set; }
        public string stU_BSU_ID { get; set; }
        public string strGivenName { get; set; }
        public string gender { get; set; }
        public string strReligion { get; set; }
        public DateTime stU_DOB { get; set; }
        public string stU_COB { get; set; }
        public string stU_NATIONALITY { get; set; }
        public string stU_NATIONALITY1 { get; set; }
        public string stU_PASPRTNAME { get; set; }
        public string stU_PASPRTNO { get; set; }
        public string stU_PASPRTISSPLACE { get; set; }
        public DateTime stU_PASPRTISSDATE { get; set; }
        public DateTime stU_PASPRTEXPDATE { get; set; }
        public string stU_VISANO { get; set; }
        public string stU_VISAISSPLACE { get; set; }
        public DateTime stU_VISAISSDATE { get; set; }
        public DateTime stU_VISAEXPDATE { get; set; }
        public string stU_VISAISSAUTH { get; set; }
        public string stU_EmiratesID { get; set; }
        public DateTime stU_EM_ID_EXP_DATE { get; set; }
        public string premisesID { get; set; }
        public string stU_FIRSTLANG { get; set; }
        public string stU_OTHLANG { get; set; }
        public string emerGenyNumber { get; set; }
        public int olU_ID { get; set; }
        public string stu_phone { get; set; }
        public string stu_email { get; set; }
    }
}
