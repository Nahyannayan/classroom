﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ContactInfo
    {
        public ContactInfo()
        {
                
        }

        public ContactInfo(int _contactInfoId)
        {
            ContactInfoId = _contactInfoId;
        }

        public int ContactInfoId { get; set; }

        public string SchoolName { get; set; }

        public int SchoolId { get; set; }

        public string SchoolEmail { get; set; }

        public string PrincipalName { get; set; }

        public string PrimaryContactFirstName { get; set; }
        public string PrimaryContactLastName { get; set; }
        public string Address1 { get; set; }

        public string Address2 { get; set; }
        public string Town { get; set; }
        public int CountyId { get; set; }

        public int CountryId { get; set; }

        public string Telephone { get; set; }

        public string PostCode { get; set; }

        public string WebsiteUrl { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }

        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }

    }
}
