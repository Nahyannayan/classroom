﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Models
{
    public class GradingTemplate
    {
        public GradingTemplate()
        {

        }

        public GradingTemplate(int _gradingTemplateId)
        {
            GradingTemplateId = _gradingTemplateId;
        }
        public int GradingTemplateId { get; set; }
        public int SchoolId { get; set; }
        public string GradingTemplateTitle { get; set; }
        public string GradingTemplateDesc { get; set; }
        public bool AllowAdditionalGrade { get; set; }
        public string GradingTemplateLogo { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public bool IsEditable { get; set; }
        public string GradingTemplateXml { get; set; }
    }

    public class GradingTemplateItem
    {
        public GradingTemplateItem()
        {

        }
        public GradingTemplateItem(int _gradingTemplateItemId)
        {
            GradingTemplateItemId = _gradingTemplateItemId;
        }
        public int GradingTemplateItemId { get; set; }
        public int GradingTemplateId { get; set; }
        public string GradingColor { get; set; }
        public string ShortLabel { get; set; }
        public string GradingItemDescription { get; set; }
        public string GradingTemplateItemSymbol { get; set; }
        public int Percentage { get; set; }
        public DateTime CreatedOn { get; set; }
        public int SortOrder { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        // adding score from score to --syed | 13 aug 2020
        public decimal ScoreFrom { get; set; }
        public decimal ScoreTo { get; set; }
        public string GradingTemplateItemXml { get; set; }
    }
}
