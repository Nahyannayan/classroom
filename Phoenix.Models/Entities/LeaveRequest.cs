﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Common;

namespace Phoenix.Models.Entities
{
    public class StudentsLeaveDetails
    {
        public int StudentId { get; set; }
        public string StudentNo { get; set; }
        public string StudentName { get; set; }
        public bool IsSelected { get; set; }
        public bool IsSelLeaveType { get; set; }
        public IEnumerable<LeaveRequest> LstLeaveDetails { get; set; }
        public IEnumerable<LeaveType> LstLeaveTypes { get; set; }
    }
    public class LeaveRequest
    {
        public string Leave_Type { get; set; }
        public string From_Date { get; set; }
        public string To_Date { get; set; }
        public string Attendance_Leave_Type { get; set; }
        public string Remarks { get; set; }
        public string Approval_Status { get; set; }
        public int Enable_Button { get; set; }
        public string LeaveReasonId { get; set; }
        public long Ref_Id { get; set; }
        public string StudentNo { get; set; }
        public bool IsAddMode { get; set; }
        public string UploadedImageName { get; set; }
        public string ActualImageName { get; set; }
        public string Attachment_FILE_NAME { get; set; }
        public string Attachment_URL { get; set; }
        public string Approver_Remarks { get; set; }
        public int APD_ID { get; set; }
        public string APD_DESCR { get; set; }
        public string ActionMode { get; set; }
        public string StudentName { get; set; }

    }
    public class LeaveApplication
    {
        public long SLA_Id { get; set; }
        public string StudentNo { get; set; }
        public int StudentId { get; set; }
        public string SLA_Type { get; set; }
        public string FromDT { get; set; }
        public string ToDT { get; set; }
        public string Remark { get; set; }
        public int LeaveReasonId { get; set; }
        public bool BEdit { get; set; }
        public string AttachmentStatus { get; set; }
    }
      
    public class LeaveType
    {
        public int? Id { get; set; }
        public string Descr { get; set; }
    }
    public class LeaveTypeRoot
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public object responseCode { get; set; }
        public object message { get; set; }
        public List<LeaveType> data { get; set; }
    }
    public class ResultRespose
    {
        public string Message { get; set; }
        public string Code { get; set; }
        public bool IsSuccess { get; set; }
    }

    public enum AttachemtStatus{
        RETAIN,
        UPDATE,
        DELETE,
        INSERT,
    }
}
