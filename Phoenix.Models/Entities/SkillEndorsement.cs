﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class SkillEndorsement
    {
        public string[] SkillIds { get; set; }
        public string Message { get; set; }
        public long StudentUserId { get; set; }
        public string TeacherName { get; set; }
        public string SkillName { get; set; }
        public int SkillId { get; set; }
        public int TotalCount { get; set; }
        public short Rating { get; set; }
        public bool ShowOnDashboard { get; set; }
        public int SkillEndorsedId { get; set; }
        public char? Mode { get; set; }
        public long Student { get; set; }
        public long TeacherId { get; set; }
        public string CreatedOn { get; set; }
        public string TeacherImage { get; set; }
        public string SkillDescription { get; set; }
        public bool IsApproved { get; set; }
        public long CreatedBy { get; set; }
        public bool RequestEndorsement { get; set; }
    }
}
