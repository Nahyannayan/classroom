﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
   public class SchoolDepartment
    {
        public long SchoolDepartmentId { get; set; }

        public long SchoolId { get; set; }
        public long SchoolLevelId { get; set; }

        public string SchoolDepartmentName { get; set; }
    }
}
