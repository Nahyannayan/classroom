﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class BlogComment
    {
        public BlogComment()
        {

        }
        public BlogComment(int _commentId)
        {
            CommentId = _commentId;
        }
        public long CommentId { get; set; }
        public long BlogId { get; set; }
        public string Comment { get; set; }
        public bool IsPublish { get; set; }
        public DateTime PublishOn { get; set; }
        public long PublishBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public string UserProfileImage { get; set; }
        public int TotalLikes { get; set; }
        public bool IsLike { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public int UserTypeId { get; set; }
        public string FileId { get; set; }
        public string ResourceFileTypeId { get; set; }
        public string PhysicalFilePath { get; set; }
    }
}
