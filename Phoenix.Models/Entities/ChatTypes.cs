﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ChatTypes
    {
        public ChatTypes()
        {

        }
        public ChatTypes(int _chatTypeId)
        {
            ChatTypeId = _chatTypeId;
        }

        public int ChatTypeId { get; set; }
        public string ChatType { get; set; }
        public bool IsActive { get; set; }
    }
}
