﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ChatPermission
    {
        public string UserIds { get; set; }
        public long SchoolId { get; set; }
        public int PermissionId { get; set; }
        public bool Flag { get; set; }
       

    }
}
