﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ReportDashboard
    {
        public IEnumerable<LoginDetailData> Logins { get; set; }
        public IEnumerable<PieChartData> Assignments { get; set; }
        public IEnumerable<TableData> GroupAssignments { get; set; }
        public IEnumerable<TableData> GroupObservation { get; set; }
        public IEnumerable<ChatterTableData> GroupChatters { get; set; }
        public IEnumerable<TeacherAssignment> TeacherAssignments { get; set; }
        public IEnumerable<StudentAssignment> StudentAssignments { get; set; }
        public IEnumerable<QuizReportData> QuizReportData { get; set; }
        public IEnumerable<ChildQuizReportData> ChildQuizReportData { get; set; }
        public IEnumerable<Data> AbuseReport { get; set; }
        public IEnumerable<Data> AbuseReportGraph { get; set; }
        public IEnumerable<SchoolInformation> SchoolList { get; set; }

        public ReportDashboard()
        {
            Logins = new List<LoginDetailData>();
            Assignments = new List<PieChartData>();
            GroupAssignments = new List<TableData>();
            GroupObservation = new List<TableData>();
            GroupChatters = new List<ChatterTableData>();
            AbuseReport = new List<Data>();
            AbuseReportGraph = new List<Data>();
            TeacherAssignments = new List<TeacherAssignment>();
            StudentAssignments = new List<StudentAssignment>();
            QuizReportData = new List<QuizReportData>();
            SchoolList = new List<SchoolInformation>();
        }
    }

    public class LoginDetailData
    {
        public int SchoolId { get; set; }
        public int TotalLoginTillDate { get; set; }
        public int TeacherLogin { get; set; }
        public int ParentLogin { get; set; }
        public int StudentLogin { get; set; }
        public int TotalLogins { get; set; }
    }

    public class PieChartData
    {
        public int SchoolId { get; set; }
        public int Total { get; set; }
        public int CompletedByStudent { get; set; }
        public int CompletedByTeacher { get; set; }
    }

    public class TableData
    {
        public int SchoolGroupId { get; set; }
        public string SchoolGroupName { get; set; }
        public int TotalAssignments { get; set; }
        public int TotalObservation { get; set; }
        public int TotalFiles { get; set; }
        public int FileSize { get; set; }
    }

    public class ChatterTableData
    {
        public int SchoolGroupId { get; set; }
        public string SchoolGroupName { get; set; }
        public int TotalChatters { get; set; }
        public int TotalFiles { get; set; }
        public int TotalComments { get; set; }
    }

    public class TeacherAssignment
    {
        public int Id { get; set; }
        public string Teacher { get; set; }
        public int TotalAssignments { get; set; }
        public int TotalFiles { get; set; }
    }

    public class StudentAssignment
    {
        public int Id { get; set; }
        public string Student { get; set; }
        public int TotalAssignments { get; set; }
        public int TotalFiles { get; set; }
    }

    public class QuizReportData
    {
        public int Id { get; set; }
        public int QuizId { get; set; }
        public int GroupId { get; set; }
        public string TeacherName { get; set; }
        public string GroupName { get; set; }
        public string QuizName { get; set; }
        public int TotalQuiz { get; set; }
        public int NoOfStudents { get; set; }
    }

    public class ChildQuizReportData
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int QuizId { get; set; }
        public string GroupName { get; set; }
        public string QuizName { get; set; }
        public int NoOfStudents { get; set; }

    }

    public class Data
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }

    public class ReportsRequest
    {
        public long? SchoolId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long? UserId { get; set; }
        public long? GroupId { get; set; }
    }

    public class TimetableReportsRequest
    {
        public long? SchoolId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long? UserId { get; set; }
        public long? GroupId { get; set; }
        public string TeacherIds { get; set; }
        public string GroupIds { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool IsOnlineMeetingEvents { get; set; }
        public bool IsTeacher { get; set; }
    }

    public class SchoolGroupsReportRequest
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public long UserId { get; set; }
        public long SchoolId { get; set; }
        public string TeacherIds { get; set; }
        public string ClassGroupIds { get; set; }
        public string GroupTypeIds { get; set; }
        public string CourseIds { get; set; }
        public bool IsBeSpokeGroup { get; set; }
        public string SearchString { get; set; }
    }

    public class AssignmentReportCard
    {
        public IEnumerable<PieChartData> Assignments { get; set; }
        public IEnumerable<TableData> GroupAssignments { get; set; }
        public IEnumerable<TeacherAssignment> TeacherAssignments { get; set; }
        public IEnumerable<StudentAssignment> StudentAssignments { get; set; }
        public IEnumerable<SchoolInformation> SchoolList { get; set; }

        public AssignmentReportCard()
        {
            Assignments = new List<PieChartData>();
            GroupAssignments = new List<TableData>();
            TeacherAssignments = new List<TeacherAssignment>();
            StudentAssignments = new List<StudentAssignment>();
            SchoolList = new List<SchoolInformation>();
        }
    }

}
