﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AbuseReportLedger
    {
        public AbuseReportLedger()
        {
            Attachments = new List<string>();
        }

        public AbuseReportLedger(Int64 _abuseReportLedgerId)
        {
            AbuseReportLedgerId = _abuseReportLedgerId;
        }

        public Int64 AbuseReportLedgerId { get; set; }
        public int SafetyCategoryId { get; set; }
        public string Message { get; set; }
        public bool IsEmail { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 CreatedBy { get; set; }
        public DateTime UpdateOn { get; set; }
        public Int64 UpdateBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public Int64 DeletedBy { get; set; }
        public string Location { get; set; }
        public string AttachmentsURL { get; set; }
        public List<string> Attachments{ get; set; }
        public string DeviceSerialNumber { get; set; }
        public string DeviceIPAddress { get; set; }

    }
}
