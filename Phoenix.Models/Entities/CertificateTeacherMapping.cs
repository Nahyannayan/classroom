﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Common.Enums;

namespace Phoenix.Models
{
    public class CertificateTeacherMapping
    {
        public string TeacherName { get; set; }
        public long TeacherUserId { get; set; }
        public bool IsAssigned { get; set; }
        public long TemplateId { get; set; }
        public int TotalCount { get; set; }
        public int GradeId { get; set; }
        public string GradeName { get; set; }
        public string UserImageFilePath { get; set; }
        public string TeacherUserIds { get; set; }
        public long CreatedBy { get; set; }
        public long SchoolId { get; set; }
        public short TemplateTypeId { get; set; }
        public long DepartmentId { get; set; }
    }

}
