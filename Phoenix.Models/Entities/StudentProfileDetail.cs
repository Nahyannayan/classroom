﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentProfileDetail
    {
        public StudentProfileDetail()
        {
            StudentBadges = new List<SchoolBadge>();
        }
        public List<SchoolBadge> StudentBadges{ get; set; }
        public Student StudentDetails { get; set; }
    }
}
