﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class QuizView
    {
        public QuizView()
        {
            lstDocuments = new List<QuizFile>();
            lstObjectives = new List<Objective>();
            lstCourse = new List<Course>();
        }
        public int QuizId { get; set; }
        public string QuizName{ get; set; }
        public string Description{ get; set; }
        public int PassMarks { get; set; }
        public long SchoolId { get; set; }
        public bool IsActive { get; set; }
        public bool IsRandomQuestion { get; set; }
        public bool IsSetTime { get; set; }
        public long CreatedBy { get; set; }
        public long SharedBy { get; set; }
        public string CreatedByName { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsForm { get; set; }
        public int MaxSubmit { get; set; }
        public int QuestionPaginationRange { get; set; }
        public int QuizTime { get; set; }
        public int TaskCount { get; set; }
        public string QuizImagePath { get; set; }
        public int SectionId { get; set; }
        public string SectionType { get; set; }
        public int GroupId { get; set; }
        public List<QuizFile> lstDocuments { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string SelectedDate { get; set; }
        public string Subjects { get; set; }
        public List<string> SubjectIds { get; set; }
        public List<string> SubjectId { get; set; }
        public bool IsStarted { get; set; }
        public bool IsQuestionsExist { get; set; }

        public long FolderId { get; set; }
        public int ModuleId { get; set; }
        public string Courses { get; set; }
        public string CourseIds { get; set; }
        public List<string> CourseId { get; set; }
        public List<Course> lstCourse { get; set; }
        public List<Objective> lstObjectives { get; set; }
        public bool IsSafeQuiz { get; set; }
        public bool IsOTP { get; set; }
        public bool IsHideScore { get; set; }
        public DateTime? ShowScoreDate { get; set; }
        public bool IsLogAnswer { get; set; }
        public long GroupQuizId { get; set; }
        public string SharedByName { get; set; }
        public string ResourceName { get; set; }
        public string ResourceCourseName { get; set; }


    }
    public class QuizFile
    {
        public int QuizFileId { get; set; }
        public int QuizId { get; set; }
        public string FileName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string FileExtension { get; set; }
        public string UploadedFileName { get; set; }
        public string FilePath { get; set; }
        public string PhysicalPath { get; set; }

    }

    public class QuizDetails
    {
        public QuizDetails()
        {
            QuizAnswers = new List<QuizAnswer>();
        }
        public int QuizQuestionId { get; set; }
        public string QuestionText { get; set; }
        public string QuestionTypeName { get; set; }
        public int Marks { get; set; }
        public List<QuizAnswer> QuizAnswers { get; set; }
        public string IsCorrectAnswer { get; set; }
        public string Answers { get; set; }
    }

    public class QuizAnswer
    {
        public int QuizAnswerId { get; set; }
        public string AnswerText { get; set; }
        public bool IsCorrectAnswer { get; set; }
    }
}
