﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class TransferTypeListList
    {
        public string TransferTypeID { get; set; }
        public string TransferZone { get; set; }
    }

    public class TransferTypeListListRoot : JasonResult
    {
        public List<TransferTypeListList> data { get; set; }
    }
}
