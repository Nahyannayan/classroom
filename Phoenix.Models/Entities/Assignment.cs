﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Common;

namespace Phoenix.Models
{
    public class Assignment
    {
        public Assignment() { }
        public Assignment(int _assignmentId)
        {
            AssignmentId = _assignmentId;
            lstCourseUnit = new List<CourseUnit>();
            lstGroupTopicUnit = new List<GroupUnit>();
        }


        public int AssignmentId { get; set; }
        public int TaskId { get; set; }
        public string AssignmentTitle { get; set; }
        public int NoofAttachment { get; set; }
        public string AssignmentDesc { get; set; }
        public int? SubjectId { get; set; }
        public string SubjectName { get; set; }
        public int TaskCount { get; set; }
        public string TeacherName { get; set; }
        public int SubmissionType { get; set; }
        public char AssignmentType { get; set; }
        public short InstantMessageUserTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? DueDate { get; set; }
        public TimeSpan DueTime { get; set; }
        public DateTime? ArchiveDate { get; set; }
        public bool IsPublished { get; set; }
        public bool isChangeReverted { get; set; }
        public bool IsTaskOrderRequired { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public string StudentIdsToAdd { get; set; }
        public List<AssignmentTask> lstAssignmentTasks { get; set; }
        public bool IsInstantMessage { get; set; }
        public bool IsGradingEnable { get; set; }
        public int GradingTemplateId { get; set; }
        public List<AssignmentFile> lstAssignmentFiles { get; set; }
        public string SchoolGroups { get; set; }
        public string Courses { get; set; }
        public List<Objective> lstObjectives { get; set; }
        public List<CourseUnit> lstCourseUnit { get; set; }
        public List<GroupUnit> lstGroupTopicUnit { get; set; }
        public List<AssignmentStudent> lstAssignmentStudent { get; set; }
        public List<Assignment> lstActiveAssignments { get; set; }
        public List<Assignment> lstArchiveAssignments { get; set; }
        public bool IsPeerMarkingEnable { get; set; }
        public bool IsConsiderForFinalGrade { get; set; }
        public bool IsAssignmentMarksEnable { get; set; }
        public int PeerMarkingType { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedByOneDriveId { get; set; }
        public string CreatedByGmailId { get; set; }
        public bool IsInstantMailToParent { get; set; }
        public bool IsNotificationToParent { get; set; }
        public bool IsInstantNotificationMailToStudent { get; set; }
        public bool CreateSeperateFileCopy { get; set; }
        public bool IsConsiderForGrading { get; set; }
        public int AssignmentCategoryId { get; set; }
        public bool EnablePlagarism { get; set; }
        public string RandomId { get; set; }

        public int AssignmentStudentId { get; set; }

        public int NewAssignmentCount { get; set; }

        public int PendingAssignmentCount { get; set; }

        public int CompletedAssignmentCount { get; set; }

        public int OverdueAssignmentCount { get; set; }

        public long GroupId { get; set; } = 0;
        public long GroupCourseTopicId { get; set; } = 0;
        public bool IsQuizExist { get; set; }
        public decimal AssignmentMarks { get; set; }
    }
    public class AssignmentTask
    {
        public AssignmentTask()
        {
            QuizFeedbacks = new List<File>();
            Files = new List<File>();
        }
        public int AssignmentId { get; set; }
        public int StudentAssignmentId { get; set; }
        public int TaskId { get; set; }
        public int StudentId { get; set; }
        public string TaskTitle { get; set; }
        public string TaskDescription { get; set; }
        public int SortOrder { get; set; }
        public bool IsActive { get; set; }
        public List<string> CourseIds { get; set; }
        public List<string> CourseId { get; set; }
        public string Courses { get; set; }
        public int TaskGradingTemplateId { get; set; }
        public List<TaskFile> lstTaskFiles { get; set; }
        public bool IsDeleted { get; set; }
        public int? QuizId { get; set; }
        public int GroupQuizId { get; set; }
        public string QuizName { get; set; }
        public StudentTask objStudentTask { get; set; }
        public TaskPeerReview objTaskPeerReview { get; set; }
        public int TimeTaken { get; set; }
        public List<File> QuizFeedbacks { get; set; }
        public string Feedback { get; set; }
        public string TeacherName { get; set; }
        public List<File> Files { get; set; }
        public long StudentTaskId { get; set; }
        public long UserId { get; set; }
        public string FeedbackFrom { get; set; }
        public bool IsSetTime { get; set; }
        public bool IsRandomQuestion { get; set; }
        public int QuizTime { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartTime { get; set; }
        public int MaxSubmit { get; set; }
        public int QuestionPaginationRange { get; set; }
        public string RandomId { get; set; }
        public bool IsHideScore { get; set; }
        public DateTime? ShowScoreDate { get; set; }
        public string CreatedByOneDriveId { get; set; }
        public string CreatedByGmailId { get; set; }
        public decimal TaskMarks { get; set; }
        public bool IsQuestionsExist { get; set; }
    }

    public class StudentTask : GradingTemplateItem
    {
        public long AssignmentId { get; set; }
        public int StudentAssignmentId { get; set; }
        public long TaskId { get; set; }
        public long StudentTaskId { get; set; }
        public int StudentId { get; set; }
        public List<StudentTaskFile> lstStudentTaskFile { get; set; }
        public bool IsCompleteMarkByTEacher { get; set; }
        public bool IsCompleteMarkByStudent { get; set; }
        public int TaskSortOrder { get; set; }
        public bool IsEnable { get; set; }
        public string QuizMarks { get; set; }
        public QuizResponse objQuizResponse { get; set; }
        public QuizView objQuizView { get; set; }
        public int TimeTaken { get; set; }
        public decimal TaskSubmitMarks { get; set; }
        public decimal TaskTotalMarks { get; set; }

    }

    public class AssignmentReport
    {
        public AssignmentReport()
        {
            AssignmentStudentData = new List<AssignmentStudent>();
            QuizReportAnswers = new List<QuizReportAnswers>();
            QuizReportQuestions = new List<QuizReportQuestions>();

        }
        public string TaskTitle { get; set; }
        public string QuizName { get; set; }
        public int TaskId { get; set; }
        public List<AssignmentStudent> AssignmentStudentData { get; set; }
        public List<QuizReportAnswers> QuizReportAnswers { get; set; }
        public List<QuizReportQuestions> QuizReportQuestions { get; set; }
    }

    public class AssignmentStudent : GradingTemplateItem
    {
        public int SNO { get; set; }
        public int AssignmentId { get; set; }
        public int TaskId { get; set; }
        public int AssignmentStudentId { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public bool IsCompletedByStudent { get; set; }
        public bool CompleteMarkByTeacher { get; set; }
        public bool IsTaskCompleted { get; set; }
        public string QuizName { get; set; }
        public string QuizScore { get; set; }
        public bool IsSeenByStudent { get; set; }
        public DateTime CompletedOn { get; set; }
        public string SectionName { get; set; }
        public string DisplayAcademicYear { get; set; }
        public bool ByTeacher { get; set; }
        public string Comment { get; set; }
        public List<StudentAssignmentFile> lstStudentAsgFiles { get; set; }
        public Assignment objAssignment { get; set; }
        public List<AssignmentStudentObjective> lstStudentObjective { get; set; }
        public int TotalStudentTaskCount { get; set; }
        public int CompletedStudentTaskCount { get; set; }
        public decimal AssignmentSumbitMarks { get; set; }
        public decimal AssignmentTotakMarks { get; set; }
        public GradingTemplateItem objGradingTemplateItem { get; set; }
    }

    public class AssignmentStudentDetails
    {
        public AssignmentStudentDetails()
        {
            AssignmentSchoolGroups = new List<SchoolGroup>();
            AssignmentFiles = new List<AssignmentFile>();

        }
        public int NoofStudent { get; set; }
        public int NoofAttachment { get; set; }
        public long UserId { get; set; }

        public int NoofAssignment { get; set; }
        public int AssignmentId { get; set; }
        public int AssignmentStudentId { get; set; }
        public int StudentId { get; set; }
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string StudentName { get; set; }
        public string AssignmentTitle { get; set; }
        public string AssignmentDesc { get; set; }
        public string AssignmentSchoolGroup { get; set; }
        public DateTime ArchiveDate { get; set; }
        public DateTime DueDate { get; set; }
        public bool IsPublished { get; set; }
        public DateTime StartDate { get; set; }
        public int SubmissionType { get; set; }
        public int TaskId { get; set; }
        public int TotalCount { get; set; }
        public string TaskTitle { get; set; }
        public string TaskDesc { get; set; }
        public bool IsCompleted { get; set; }
        public int CompletedStudentCount { get; set; }
        public int NewAssignment { get; set; }
        public int CompletedAssignment { get; set; }
        public int PendingAssignment { get; set; }
        public int OverdueAssignment { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsSeenByStudent { get; set; }
        public bool CompleteMarkByTeacher { get; set; }
        public int AssignmentGradingTemplateId { get; set; }
        public int StudentAssignmentGradeId { get; set; }
        public List<SchoolGroup> AssignmentSchoolGroups { get; set; }
        public List<AssignmentFile> AssignmentFiles { get; set; }
        public string GradingColor { get; set; }
        public string ShortLabel { get; set; }
        public string GradingItemDescription { get; set; }
        public string GradingTemplateItemSymbol { get; set; }
        public int Percentage { get; set; }
        public int CompleteMarkbyStudentCount { get; set; }
        public int? GradingTemplateId { get; set; }
        public int AssignGradeCount { get; set; }
        public string SubjectName { get; set; }
        public bool ShowOnDashboard { get; set; }
        public string AssignmentStatus { get; set; }
    }

    public class AssignmentFile
    {
        public string AssignmentTitle { get; set; }
        public int AssignmentFileId { get; set; }
        public int AssignmentId { get; set; }
        public string FileName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long DocumentId { get; set; }
        public string FileExtension { get; set; }
        public string UploadedFileName { get; set; }
        public string ShareableLinkFileURL { get; set; }
        public string FilePath { get; set; }
        public bool IsCloudFile { get; set; }
        public bool IsSharePointFile { get; set; }
        public string CloudFileId { get; set; }
        public string DriveId { get; set; }
        public string ParentFolderId { get; set; }
        public short ResourceFileTypeId { get; set; }
        public DateTime UploadedOn { get; set; }
        public string PhysicalFilePath { get; set; }
        public bool ShowOnDashboard { get; set; }
    }
    public class TaskFile
    {
        public int TaskFileId { get; set; }
        public int TaskId { get; set; }
        public string FileName { get; set; }
        public int CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        public DateTime CreateOn { get; set; }
        public string FileExtension { get; set; }
        public string UploadedFileName { get; set; }
        public bool IsCloudFile { get; set; }
        public bool IsSharePointFile { get; set; }
        public bool IsSharedFile { get; set; }
        public string CloudFileId { get; set; }
        public string DriveId { get; set; }
        public string ParentFolderId { get; set; }
        public short ResourceFileTypeId { get; set; }
        public string UploadedSharepointFileUrl { get; set; }
        public string ShareableLinkFileURL { get; set; }
    }

    public class StudentTaskFile
    {
        public int StudentTaskFileId { get; set; }
        public int TaskId { get; set; }
        public int StudentId { get; set; }
        public string FileName { get; set; }
        public int CreatedBy { get; set; }
        public bool IsCloudFile { get; set; }
        public string FileExtension { get; set; }
        public bool IsSharedFile { get; set; }
        public string CloudFileId { get; set; }
        public string DriveId { get; set; }
        public string ParentFolderId { get; set; }
        public DateTime CreateOn { get; set; }
        public string UploadedFileName { get; set; }
        public string SharepointUploadedFileURL { get; set; }
        public short ResourceFileTypeId { get; set; }

    }
    public class StudentAssignmentFile
    {
        public long StudentAsgFileId { get; set; }
        public int AssignmentId { get; set; }
        public int StudentId { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string UploadedFileName { get; set; }
        public DateTime UploadedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public bool DeletedBy { get; set; }
        public bool IsCloudFile { get; set; }
        public string CloudFileId { get; set; }
        public string DriveId { get; set; }
        public string ParentFolderId { get; set; }
        public string PathtoDownload { get; set; }
        public bool IsCollaboratedFile { get; set; }
        public bool IsSharedFile { get; set; }
        public string SharepointUploadedFileURL { get; set; }
        public short ResourceFileTypeId { get; set; }
    }

    public class MyFilesTreeItem
    {
        public string FileId { get; set; }
        public string FileName { get; set; }
        public long ParentFolderId { get; set; }
        public string FilePath { get; set; }
        public bool isFolder { get; set; }

    }
    public class AssignmentPeerReview
    {
        public long PeerReviewId { get; set; }
        public long AssignmentId { get; set; }
        public long StudentId { get; set; }
        public long ReviewerId { get; set; }
        public long CalculatedMarks { get; set; }
        public string StudentName { get; set; }
        public long UserId { get; set; }
        public string ReviewerName { get; set; }
        public long AssignmentStudentId { get; set; }
        public bool IsReviewCompleted { get; set; }
        public bool IsStudentCompletedHomework { get; set; }
        public int TotalMistakesCount { get; set; }
    }

    public class DocumentReviewdetails
    {
        public long ReviewedFileId { get; set; }
        public long PeerReviewId { get; set; }
        public long StudentAsgFileId { get; set; }
        public string ReviewedFileName { get; set; }
        public decimal CalculatedMarks { get; set; }
        public int MistakesCount { get; set; }

    }

    public class AssignmentCounts
    {
        public int NewAssignentCount { get; set; }
        public int PendingAssignentCount { get; set; }
        public int OverDueAssignentCount { get; set; }
        public int CompletedAssignentCount { get; set; }
        public int TotalAssignmentCount { get; set; }
    }

    public class TaskPeerReview
    {
        public long TaskPeerReviewId { get; set; }
        public long StudentTaskId { get; set; }

        public long CalculatedMarks { get; set; }

        public bool IsTaskPeerReviewCompleted { get; set; }
    }

    public class TaskDocumentReviewdetails
    {
        public long ReviewTaskFileId { get; set; }
        public long TaskPeerReviewId { get; set; }
        public long StudentTaskFileId { get; set; }
        public string ReviewedFileName { get; set; }
        public decimal CalculatedMarks { get; set; }
        public int MistakesCount { get; set; }

    }

    public class AssignmentDetails : GradingTemplateItem
    {
        public int AssignmentId { get; set; }
        public string AssignmentTitle { get; set; }
        public int AssignmentStudentId { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public bool IsCompletedByStudent { get; set; }
        public bool CompleteMarkByTeacher { get; set; }
        public bool IsSeenByStudent { get; set; }
        public List<StudentAssignmentFile> lstStudentAsgFiles { get; set; }
        public Assignment objAssignment { get; set; }
    }
    public class AssignmentStudentObjective
    {
        public AssignmentStudentObjective()
        {
            ObjectiveFeedbacks = new List<File>();
            Files = new List<File>();
        }
        public int StudentObjectiveMarkId { get; set; }
        public int AssignmentStudentId { get; set; }
        public int ObjectiveId { get; set; }
        public int GradingTemplateItemId { get; set; }
        public string ObjectiveTitle { get; set; }
        public string GradingTemplateItemSymbol { get; set; }
        public string GradingColor { get; set; }
        public string ShortLabel { get; set; }
        public string Description { get; set; }
        public List<File> Files { get; set; }
        public string Feedback { get; set; }
        public int CreatedBy { get; set; }
        public List<File> ObjectiveFeedbacks { get; set; }
    }

    public class AssignmentFilter
    {
        public int page { get; set; }
        public int size { get; set; }
        public string assignmentType { get; set; }
        public string searchString { get; set; }
        public bool CreatedByMeOnly { get; set; }
        public string sortBy { get; set; }
        public string filterVal { get; set; }
        public string teacherVal { get; set; }
        public string courseVal { get; set; }
        public long schoolId { get; set; }

    }

    public class AssignmentCategoryDetails
    {
        public int AssigntmentCatgoryId { get; set; }
        public string CategoryTitle { get; set; }
        public string CategoryDescription { get; set; }
        public long SchoolId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ArchiveAssignment
    {
        public string AssignmentIds { get; set; }
        public long UpdatedBy { get; set; }
    }

}

