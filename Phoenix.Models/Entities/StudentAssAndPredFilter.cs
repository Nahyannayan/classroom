﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Phoenix.Models.Entities
{

    public class AllStudentFilters
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class FilStudent
    {
        /// <summary>
        /// Id of Student.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Name of Student.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Photo of Student.
        /// </summary>
        public string PhotoPath { get; set; }
    }

    public class StudentCountPieChartViewModel
    {
        public string category { get; set; }
        public double value { get; set; }
    }

    public class StudentFilterRes
    {
        /// <summary>
        /// Academic year filter for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AllFilters> AcademicYear { get; set; }

        /// <summary>
        /// Class filter for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AllFilters> Class { get; set; }

        /// <summary>
        /// Subject filter for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AllFilters> Subject { get; set; }

        /// <summary>
        /// Assessment filter for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AllFilters> Assessment { get; set; }

        /// <summary>
        /// Teacher filter for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AllFilters> Teacher { get; set; }
        public IEnumerable<FileStudent> FileStudent { get; set; }
    }

    public class StudentAssAndPredFilter
    {
        /// <summary>
        /// Academic year filter for StudentProgressTracker Dashboard.
        /// </summary>
        public SelectList AcademicYear { get; set; }

        /// <summary>
        /// Class filter for StudentProgressTracker Dashboard.
        /// </summary>
        public SelectList Class { get; set; }

        /// <summary>
        /// Subject filter for StudentProgressTracker Dashboard.
        /// </summary>
        public SelectList Subject { get; set; }

        /// <summary>
        /// Assessment filter for StudentProgressTracker Dashboard.
        /// </summary>
        public SelectList Assessment { get; set; }

        /// <summary>
        /// Teacher filter for StudentProgressTracker Dashboard.
        /// </summary>
        public SelectList Teacher { get; set; }
        public SelectList FileStudent { get; set; }
    }

    public class StudentChartRes
    {
        /// <summary>
        /// Average and prediction figures for StudentProgressTracker Dashboard.
        /// </summary>
        public AveragePredictionFigures AvgPredFig { get; set; }

        /// <summary>
        /// Average assessment and prediction figures by year for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AverageScoreAssAndPredByAcademicYear> AvgAssPredByYear { get; set; }

        /// <summary>
        /// Average marks by year and assessment for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AverageScoreAssAndPredByAcademicYear> AvgMarksByYearAndAss { get; set; }

        /// <summary>
        /// Prediction bucket for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<StudentPredictionBucket> PredBuck { get; set; }

        /// <summary>
        /// Prediction by subject for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<StudentPredictionBySubject> PredBySub { get; set; }
        public IEnumerable<StudentPredictionCountBySubject> PredBySubByStu { get; set; }
        public AvgPredScore ArgPredScore { get; set; }
    }

    #region Average Prediction Figures

    /// <summary>
    /// Model class for average and prediction figures of StudentProgressTracker dashboard.
    /// </summary>
    public class StudentAveragePredictionFigures
    {
        /// <summary>
        /// Overall average score.
        /// </summary>
        public decimal OverallAverage { get; set; }

        /// <summary>
        /// Prediction average score.
        /// </summary>
        public decimal PredictionAverage { get; set; }

        /// <summary>
        /// Overall average score for boys.
        /// </summary>
        public decimal OverallAverageBoys { get; set; }

        /// <summary>
        /// Prediction average score for boys.
        /// </summary>
        public decimal PredictionAverageBoys { get; set; }

        /// <summary>
        /// Overall average score for boys.
        /// </summary>
        public decimal OverallAverageGirls { get; set; }

        /// <summary>
        /// Prediction average score for boys.
        /// </summary>
        public decimal PredictionAverageGirls { get; set; }
    }

    #endregion Average Prediction Figures

    #region Average Assessment Prediction By Year

    /// <summary>
    /// Model class for average assessment and prediction by year chart of StudentProgressTracker dashboard.
    /// </summary>
    public class AverageScoreAssAndPredByAcademicYear
    {
        /// <summary>
        /// Academic Year.
        /// </summary>
        public string AcademicYear { get; set; }

        /// <summary>
        /// Average percentage score of category.
        /// </summary>
        public decimal AvgPercentage { get; set; }

        /// <summary>
        /// Category of chart.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Assessment of chart.
        /// </summary>
        public string Assessment { get; set; }

        /// <summary>
        /// Assessment Date of chart.
        /// </summary>
        public DateTime? AssessmentDate { get; set; }
        /// <summary>
        /// Interval for chart Y-axis values.
        /// </summary>
        public int Interval { get; set; }
    }

    #endregion Average Assessment Prediction By Year

    #region Prediction Bucket

    /// <summary>
    /// Model class for prediction bucket chart of StudentProgressTracker dashboard.
    /// </summary>
    public class StudentPredictionBucket
    {
        /// <summary>
        /// Percentage Range.
        /// </summary>
        public string PRange { get; set; }

        /// <summary>
        /// Count of students.
        /// </summary>
        public int StudentCount { get; set; }
    }

    #endregion Prediction Bucket

    #region Prediction By Subject

    /// <summary>
    /// Model class for prediction by subject chart of StudentProgressTracker dashboard.
    /// </summary>
    public class StudentPredictionBySubject
    {
        /// <summary>
        /// Subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Count of students.
        /// </summary>
        public int StudentCount { get; set; }
    }

    public class StudentPredictionCountBySubject
    {
        /// <summary>
        /// Subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Count of students.
        /// </summary>
        public int AvgPercentage { get; set; }
        
    }

    #endregion Prediction By Subject

    public class AvgPredScore
    {
        public decimal Score { get; set; }
    }

    #region Average Assessment Prediction By Year

    /// <summary>
    /// Model class for all assessment data by student table of Charts dashboard.
    /// </summary>
    public class AssessmentDataByStudent
    {
        /// <summary>
        /// Academic Year.
        /// </summary>
        public string AcademicYear { get; set; }

        /// <summary>
        /// Percentage.
        /// </summary>
        public decimal Percentage { get; set; }

        /// <summary>
        /// Subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Assessment.
        /// </summary>
        public string Assessment { get; set; }

        /// <summary>
        /// Assessment Date .
        /// </summary>
        public DateTime? AssessmentDate { get; set; }

        /// <summary>
        /// Teacher.
        /// </summary>
        public string Teacher { get; set; }
        public string Class { get; set; }
    }
    #endregion

}
