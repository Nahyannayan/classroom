﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class CourseCatalogue : Course
    {
        public CourseCatalogue()
        {
            CourseMappings = new List<CourseMapping>();
            Units = new List<CatalogueUnits>();
        }
        public List<CourseMapping> CourseMappings { get; set; }
        public List<CatalogueUnits> Units { get; set; }
        public List<CatalogueUnitDetails> UnitDetails { get; set; }
    }
    public class CatalogueUnits : Unit
    {
        public CatalogueUnits()
        {
            Units = new List<CatalogueUnits>();
            UnitDetails = new List<CatalogueUnitDetails>();
            AssingmentLessions = new List<CalalogueAssingmentLession>();
            Quizzes = new List<CalalogueQuiz>();
        }
        public List<CatalogueUnits> Units { get; set; }
        public List<CatalogueUnitDetails> UnitDetails { get; set; }
        public List<CalalogueAssingmentLession> AssingmentLessions { get; set; }
        public List<CalalogueQuiz> Quizzes { get; set; }
    }
    public class GroupCatalogue
    {
        public GroupCatalogue()
        {
            SchoolGradeName = new List<string>();
            CatalogueTeachers = new List<CatalogueTeachers>();
            TimeTableDatas = new List<TimeTableData>();
        }
        public long CourseId { get; set; }
        public string CourseTitle { get; set; }
        public int MaximumEnrollment { get; set; }
        public int StudentsEnrolled { get; set; }
        public long SchoolGroupId { get; set; }
        public string CourseImage { get; set; }
        public string SchoolGroupName { get; set; }
        public IEnumerable<string> SchoolGradeName { get; set; }
        public IEnumerable<CatalogueTeachers> CatalogueTeachers { get; set; }
        public IEnumerable<TimeTableData> TimeTableDatas { get; set; }
    }
    public class TimeTableData
    {
        public DateTime TimetableDate { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public string Period { get; set; }
        public string RoomNumber { get; set; }
        public int WeekDay { get; set; }
        public long SchoolGroupId { get; set; }
    }
    public class TimeTableWeekly
    {
        public long SchoolGroupId { get; set; }
        public string Period { get; set; }
        public DateTime StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public int WeekDay { get; set; }
        public DateTime WeekDate { get; set; }
        public string RoomNumber { get; set; }
    }
    public class CatalogueTeachers
    {
        public long TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string TeacherImage { get; set; }
    }
    public class CatalogueUnitDetails
    {
        public long UnitDetailId { get; set; }
        public long UnitId { get; set; }
        public string UnitDescription { get; set; }
        public string UnitType { get; set; }
        public string FilePath { get; set; }
        public string UnitName { get; set; }
    }
    public class CalalogueAssingmentLession
    {
        public long UnitId { get; set; }
        public long AssignmentId { get; set; }
        public string AssignmentTitle { get; set; }
        public long LessonId { get; set; }
        public string Description { get; set; }
        public long CreatedBy { get; set; }
        public int GroupsAssignedCount { get; set; }
        public string SchoolGroupNames { get; set; }
    }
    public class CalalogueQuiz
    {
        public long QuizId { get; set; }
        public string QuizName { get; set; }
        public long UnitId { get; set; }
        public long GroupId { get; set; }
    }
    public enum UnitType
    {
        Assignment = 1,
        Quiz = 2
    }
}
