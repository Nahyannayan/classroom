﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class SchoolReports
    {
        public SchoolReports()
        {
            ReportParameters = new List<ReportParameter>();
        }
        public int ReportDetailId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StoredProcedureName { get; set; }
        public string FileName { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long SchoolId { get; set; }
        public int ConfigType  { get; set; }
        public bool IsCrystalReport { get; set; }
        public bool IsReportEdit { get; set; }
        public List<ReportParameter> ReportParameters { get; set; }
        public DataTable GetReportParameterDT()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ReportParameterId", typeof(long));
            dt.Columns.Add("ReportDetailId", typeof(long));
            dt.Columns.Add("ParameterId", typeof(long));
            dt.Columns.Add("ParameterOrder", typeof(long));
            dt.Columns.Add("ParameterName", typeof(string));

            ReportParameters.ForEach(x =>
            {
                dt.Rows.Add(x.ReportParameterId, x.ReportDetailId, x.ParameterId, x.ParameterOrder, x.ParameterName);
            });
            return dt;
        }
    }
    public class ReportParameter
    {
        public int ReportParameterId { get; set; }
        public int ReportDetailId { get; set; }
        public int ParameterId { get; set; }
        public int? ParameterOrder { get; set; }
        public string ParameterName { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string ParameterType { get; set; }
    }
    public class ReportListModel
    {
        public long PrimaryReportId { get; set; }
        public long SecondaryReportId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public bool IsCrystalReport { get; set; }
    }
    public class ReportDetailModel
    {
        public ReportDetailModel()
        {
            ReportParameters = new List<ReportParameter>();
            ColumnFields = new List<dynamic>();
            ColumnFieldsDic = new Dictionary<string, object>();
        }
        public SchoolReports SchoolReports { get; set; }
        public IEnumerable<ReportParameter> ReportParameters { get; set; }
        public List<dynamic> ColumnFields { get; set; }
        public Dictionary<string, object> ColumnFieldsDic { get; set; }
        public SchoolInformation SchoolInfo { get; set; }
    }
    public class ReportFilterModel
    {
        public string Name { get; set; }
        public string value { get; set; }
    }
    public class ReportParameterRequestModel
    {
        public long ReportDetailId { get; set; }
        public string ReportParamterListString { get; set; }
        public long SchoolId { get; set; }
        public bool IsCrystalReport { get; set; }
    }
    public class ReportDataModel : ReportDetailModel
    {
        public string FileName { get; set; }
    }
}
