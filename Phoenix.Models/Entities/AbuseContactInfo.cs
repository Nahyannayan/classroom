﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AbuseContactInfo
    {
        public AbuseContactInfo()
        {

        }

        public AbuseContactInfo(Int64 _abuseContactInfoId)
        {
            AbuseContactInfoId = _abuseContactInfoId;
        }

        public Int64 AbuseContactInfoId { get; set; }
        public Int64 SchoolId { get; set; }
        public string ContactMessage { get; set; }
        public string ContactNumber { get; set; }
        public string ContactEmail { get; set; }
        public Int64 CensorNoticeRecieverId { get; set; }

        public bool IsActive { get; set; }
        public Int64 CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool EnableCallingPolice { get; set; }
        public string PolicePhoneNumber { get; set; }
        public bool IsSOSEnabledInMobile { get; set; }
        public bool IsReportAbuseEnabledInMobile { get; set; }
    }
}
