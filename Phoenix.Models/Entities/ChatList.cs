﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ChatList
    {
        public ChatList()
        {

        }
        public ChatList(int _chatListId)
        {
            ChatListId = _chatListId;
        }

        public long ChatListId { get; set; }

        public List<Chat> UserChatList { get; set; } = new List<Chat>();
    }
}
