﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class StudentHealthInfoPost
    {
        public StudentHealth stU_Health = new StudentHealth();
        public StudentDiseases stU_Disease = new StudentDiseases();
        public List<StduentInfection> stU_Infection = new List<StduentInfection>();

    }

    public class StudentHealth
    {
        public string stU_NO { get; set; }
        public string stU_BSU_ID { get; set; }
        public string healthCardNo { get; set; }
        public string bloodGroup { get; set; }
        public bool stU_bALLERGIES { get; set; }
        public string txtSTU_bAllergies { get; set; }
        public string stU_bRCVSPMEDICATION { get; set; }
        public string txtPMedication { get; set; }
        public bool stU_bPRESTRICTIONS { get; set; }
        public string txtSTU_bPRESTRICTIONS { get; set; }
        public bool stU_bHRESTRICTIONS { get; set; }
        public string txtbHealthAware { get; set; }
        public bool bVisualDisab { get; set; }
        public string txtVisual { get; set; }
        public bool stU_EduNeeds { get; set; }
        public string txtspclEduNeeds { get; set; }
        public int olU_ID { get; set; }
    }

    public class StudentDiseases
    {
        public string stU_NO { get; set; }
        public string diS_INF_DETAILS { get; set; }
        public bool diS_DIABETES { get; set; }
        public bool diS_HYPERTENSION { get; set; }
        public bool diS_STROKE { get; set; }
        public bool diS_TUBERCULOSIS { get; set; }
        public bool diS_OTHER { get; set; }
        public string diS_OTHER_DETAILS { get; set; }
    }

    public class StduentInfection
    {
        public string stU_NO { get; set; }
        public int diS_ID { get; set; }
        public bool diS_STATUS { get; set; }
    }

}
