﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class LessonCourse
    {
        public long UnitMasterId { get; set; }
        public long ParentId { get; set; }
        public string CourseName { get; set; }
        public long LessonId { get; set; }

        public long SubSyllabusId { get; set; }
        public string LessonDescription { get; set; }
        public long CorseId { get; set; }

        public string UnitMasterName { get; set; }

        public string CourseStateDate { get; set; }
        public string CourseEndDate { get; set; }

        public string SrartWeek { get; set; }
        public string Endweek { get; set; }
    }
}
