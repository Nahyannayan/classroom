﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Phoenix.Models.Entities
{
    public class StandardBankExcelUploadData
    {
        public DataTable ExcelData { get; set; }
        public bool IsColumnMatch { get; set; }
    }
    public class StandardBankUploadModel
    {
        public StandardBankUploadModel()
        {
            StandardBankExcelList = new List<StandardBankExcelModel>();
        }
        public long SchoolId { get; set; }
        public List<StandardBankExcelModel> StandardBankExcelList { get; set; }
        public DataTable StandardBankUploadDT
        {
            get
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("SB_Id", typeof(int));
                dt.Columns.Add("GroupName", typeof(string));
                dt.Columns.Add("UnitName", typeof(string));
                dt.Columns.Add("SubUnitName", typeof(string));
                dt.Columns.Add("StandardCode", typeof(string));
                dt.Columns.Add("StandardDescription", typeof(string));

                int SB_Id = 1;

                StandardBankExcelList.ToList().ForEach(item =>
                        {
                            dt.Rows.Add(SB_Id,
                                item.GroupName,
                                item.UnitName,
                                item.SubUnitName,
                                item.StandardCode,
                                item.StandardDescription
                                
                               );
                            SB_Id++;
                        });
                return dt;
            }

        }
    }
    public class StandardBankExcelModel
    {
        public string GroupName { get; set; }
        [AllowHtml]
        public string UnitName { get; set; }
        [AllowHtml]
        public string SubUnitName { get; set; }
        public string StandardCode { get; set; }
        [AllowHtml]
        public string StandardDescription { get; set; }
        public long SB_Id { get; set; }
    }
}
