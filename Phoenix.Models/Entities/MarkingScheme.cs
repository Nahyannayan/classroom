﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class MarkingScheme
    {
        public MarkingScheme()
        {

        }
        public MarkingScheme(int _markingSchemeId)
        {
            MarkingSchemeId = _markingSchemeId;
        }
        public int MarkingSchemeId { get; set; }
        public int SchoolId { get; set; }
        public string ColourCode { get; set; }
        public string MarkingName { get; set; }
        public string MarkingDesc { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public decimal Weight { get; set; }
        public decimal? CalculatedWeight { get; set; }
        public string MarkingSchemeXml { get; set; }
    }
}