﻿namespace Phoenix.Models
{
    public class AssignmentStudentSharedFiles
    {
        public int StudentSharedFileId { get; set; }
        public long AssignmentId { get; set; }
        public int StudentId { get; set; }
        public string FilePath { get; set; }
        public short ResourceFileTypeId { get; set; }
        public string FileName { get; set; }
        public bool IsCollaboratedFile { get; set; }
    }
}
