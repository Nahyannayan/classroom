﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Models
{
    
    public class StudentView
    {
        public int StudentId { get; set; }
        public string FirstName_1 { get; set; }
        public string FirstName_2 { get; set; }
        public string FirstName_3 { get; set; }
        public string LastName_1 { get; set; }
        public string LastName_2 { get; set; }
        public string LastName_3 { get; set; }
        public bool IsActive { get; set; }

    }
}
