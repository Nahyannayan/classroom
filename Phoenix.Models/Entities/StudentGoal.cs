﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentGoal
    {
        public long UserId { get; set; }
        public long GoalId { get; set; }
        public string GoalDescription { get; set; }
        public string GoalName { get; set; }
        public string ExpectedDate { get; set; }
        public long StudentId { get; set; }
        public bool ShowOnDashboard { get; set; }
        public int TotalCount { get; set; }
        public long CreatedUserId { get; set; }
        public bool IsApproved { get; set; }
        public long TeacherId { get; set; }
    }
}
