﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class SaveCourseSelection
    {
        public string StudentID { get; set; }
        public string User { get; set; }
        public List<SelectedCourse> SelectCoursesList { get; set; }
    }
}
