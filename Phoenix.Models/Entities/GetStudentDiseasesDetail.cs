﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class GetStudentDiseasesDetail
    {
       
        public int diS_DET_ID { get; set; }
        public int diS_STU_ID { get; set; }
        public string diS_INF_DETAILS { get; set; }
        public bool diS_DIABETES { get; set; }
        public bool diS_HYPERTENSION { get; set; }
        public bool diS_STROKE { get; set; }
        public bool diS_TUBERCULOSIS { get; set; }
        public bool diS_OTHER { get; set; }
        public string diS_OTHER_DETAILS { get; set; }
       
    }
    public class GetStudentDiseasesDetailRoot
    {
        public List<GetStudentDiseasesDetail> GetStudentDiseasesDetail { get; set; }
    }
}
