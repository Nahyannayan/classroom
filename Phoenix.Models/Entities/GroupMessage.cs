﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class GroupMessage
    {
        public int GroupMessageId { get; set; }
        public int ParentGroupId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

        public string Attachment { get; set; }
        public string MemberIds { get; set; }
        public string SelectedGroups { get; set; }
        public string NotifyTo { get; set; }
    }
}
