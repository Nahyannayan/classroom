﻿

namespace Phoenix.Models
{
   
    public class SafetyCategory 
    {
        public SafetyCategory()
        {

        }

        public SafetyCategory(int _safetyCategoryId)
        {
            SafetyCategoryId = _safetyCategoryId;
        }
        public int SafetyCategoryId { get; set; }
        public string SafetyCategoryName { get; set; }
        public string SafetyCategoryXml { get; set; }

        public bool IsActive { get; set; }
        public int SchoolId { get; set; }
        public string FormattedCreatedOn { get; set; }
        public int CreatedById { get; set; }
        public string CreatedByName { get; set; }
    }
}
