﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class UserFeedback
    {
        public string FeedbackRating { get; set; }
        public string Description { get; set; }
        public long CreatedBy { get; set; }
    }
}
