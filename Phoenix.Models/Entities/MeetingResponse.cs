﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Phoenix.Models
{
    [XmlRoot(ElementName = "response")]
    public class MeetingResponse
    {
        public string MeetingName { get; set; }
        public string StartDate { get; set; }
        public string EndTime { get; set; }
        public string StartTime { get; set; }
        public int SchoolGroupId { get; set; }
        [XmlElement(ElementName = "returncode")]
        public string Returncode { get; set; }

        [XmlElement(ElementName = "meetingID")]
        public string MeetingID { get; set; }

        [XmlElement(ElementName = "internalMeetingID")]
        public string InternalMeetingID { get; set; }

        [XmlElement(ElementName = "parentMeetingID")]
        public string ParentMeetingID { get; set; }

        [XmlElement(ElementName = "attendeePW")]
        public string AttendeePW { get; set; }

        [XmlElement(ElementName = "moderatorPW")]
        public string ModeratorPW { get; set; }

        [XmlElement(ElementName = "createTime")]
        public string CreateTime { get; set; }

        [XmlElement(ElementName = "voiceBridge")]
        public string VoiceBridge { get; set; }

        [XmlElement(ElementName = "dialNumber")]
        public string DialNumber { get; set; }

        [XmlElement(ElementName = "createDate")]
        public string CreateDate { get; set; }

        [XmlElement(ElementName = "hasUserJoined")]
        public string HasUserJoined { get; set; }

        [XmlElement(ElementName = "duration")]
        public string Duration { get; set; }

        [XmlElement(ElementName = "hasBeenForciblyEnded")]
        public string HasBeenForciblyEnded { get; set; }

        [XmlElement(ElementName = "messageKey")]
        public string MessageKey { get; set; }

        [XmlElement(ElementName = "message")]
        public string Message { get; set; }

        [XmlElement(ElementName = "meeting_id")]
        public string BBBMeetingId { get; set; }

        [XmlElement(ElementName = "user_id")]
        public string UserId { get; set; }

        [XmlElement(ElementName = "auth_token")]
        public string AuthToken { get; set; }

        [XmlElement(ElementName = "session_token")]
        public string SessionToken { get; set; }

        [XmlElement(ElementName = "url")]
        public string Url { get; set; }
        [XmlElement(ElementName = "running")]
        public bool IsRunning { get; set; }
        public long CreatedBy { get; set; }
        public short Mode { get; set; }
        public string OldMeetingId { get; set; }
    }

}
