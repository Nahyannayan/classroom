﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class ReportingTermModel
    {
        public ReportingTermModel()
        {
            StartDate = DateTime.Today;
            EndDate = DateTime.Today;
        }
        public long ReportingTermId { get; set; }
        public long SchoolId { get; set; }
        public long DivisionId { get; set; }
        public string DivisionName { get; set; }
        public string TermDescription { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Order { get; set; }
        public bool IsLock { get; set; }
        public bool IsDeleted { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string DATAMODE { get; set; }
        public long UserId { get; set; }
    }
    public class ReportingSubTermModel
    {
        public ReportingSubTermModel()
        {
            StartDate = DateTime.Today;
            EndDate = DateTime.Today;
        }
        public long ReportingSubTermId { get; set; }
        public long ReportingTermId { get; set; }
        public string TermDescription { get; set; }
        public string SubTermDescription { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Order { get; set; }
        public bool IsLock { get; set; }
        public bool IsDeleted { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string DATAMODE { get; set; }
        public long UserId { get; set; }
    }
}
