﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentAssessmentReportView
    {
        public int ReportId { get; set; }
        public string Title { get; set; }
        public bool ShowOnDashboard { get; set; }
        public string ReleaseDate { get; set; }
        public long UserId { get; set; }
        public string StudentNumber { get; set; }
        public string AcademicYearId { get; set; }
    }
}
