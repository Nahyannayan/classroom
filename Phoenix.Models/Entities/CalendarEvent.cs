﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class CalendarEvent
    {
        public string Cmd { get; set; }
        public string SuccessMessage { get; set; }

        public string ResponseCode { get; set; }
        public string Message { get; set; }
        public List<TimeTableEvent> data {get;set;}
    }

    public class ResourceCategory
    {
        public string schoolID { get; set; }
        public List<ResCategory> resourcesCategories { get; set; }
        public List<Resource> ResourceList{ get; set; }
    }

    public class ResourceCategoryRoot
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public object responseCode { get; set; }
        public object message { get; set; }
        public List<ResourceCategory> data { get; set; }
        public List<ResCategory> data1 { get; set; }
    }
}
