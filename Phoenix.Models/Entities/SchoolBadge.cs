﻿using System;
using System.Web.Mvc;

namespace Phoenix.Models.Entities
{
    public class SchoolBadge
    {
        public Int64 SchoolBadgeId { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public string ActualImageName { get; set; }
        public string UploadedImageName { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }

        public string SchoolId { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsSelected { get; set; }

        public string BadgesIds { get; set; }
        public long AssignedBy { get; set; }
        public long UserId { get; set; }

    }
}
