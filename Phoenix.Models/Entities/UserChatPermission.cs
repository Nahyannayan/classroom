﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class UserChatPermission
    {
        public int UserId { get; set; }
        public int SchoolId { get; set; }
        public int PermissionId { get; set; }
        public string PermissionCode { get; set; }
        public int GrantPermission { get; set; }
    }
}
