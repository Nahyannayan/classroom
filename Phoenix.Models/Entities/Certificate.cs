﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Certificate
    {
        public int CertificateId { get; set; }
        public string CertificateName { get; set; }
        public string CertificateDescription { get; set; }
        public string Status { get; set; }
        public int CreatedBy { get; set; }
        public bool IsAddMode { get; set; }
        public string FileName { get; set; }
    }
}
