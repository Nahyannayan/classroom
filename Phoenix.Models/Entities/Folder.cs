﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Folder
    {
        public Folder()
        {

        }
        public Folder(long _folderId)
        {
            FolderId = _folderId;
        }

        public long FolderId { get; set; }
        public long SchoolId { get; set; }
        public int ModuleId { get; set; }
        public long SectionId { get; set; }
        public string FolderName { get; set; }
        public long ParentFolderId { get; set; }
        public string ParentFolderName { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public string UpdatedByName { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int TotalCount { get; set; }
    }
}
