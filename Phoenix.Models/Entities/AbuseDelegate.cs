﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AbuseDelegate
    {
        public AbuseDelegate()
        {

        }

        public AbuseDelegate(Int64 _abuseDelegateId)
        {
            AbuseDelegateId = _abuseDelegateId;
        }
        public Int64 AbuseDelegateId { get; set; }
        public Int64 SchoolId { get; set; }
        public Int64 DelegateId { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; }
        public string DelegateName { get; set; }
        public Int64 CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
