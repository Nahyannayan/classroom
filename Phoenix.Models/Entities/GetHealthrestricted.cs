﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class GetHealthrestricted
    {
        public string _healthCardNo { get; set; }
        public string _bloodGroup { get; set; }
        public string _STU_bALLERGIES { get; set; }
        public string _txtSTU_bAllergies { get; set; }
        public string _STU_bRCVSPMEDICATION { get; set; }
        public string _txtPMedication { get; set; }
        public string _STU_bPRESTRICTIONS { get; set; }
        public string stU_PHYSICAL { get; set; }
        public string stU_bHRESTRICTIONS { get; set; }
        public string stU_HEALTH { get; set; }
        public string stU_bTHERAPHY { get; set; }
        public string stU_THERAPHY { get; set; }
        public string stU_bSEN { get; set; }
        public string _txtspclEduNeeds { get; set; }
        public string stU_bEAL { get; set; }
        public string stU_EAL_REMARK { get; set; }
        public string stU_bMUSICAL { get; set; }
        public string stU_MUSICAL { get; set; }
        public string stU_bENRICH { get; set; }
        public string stU_ENRICH { get; set; }
        public string stU_bBEHAVIOUR { get; set; }
        public string stU_BEHAVIOUR { get; set; }
        public string stU_bSPORTS { get; set; }
        public string stU_SPORTS { get; set; }
        public string stU_bVisual_disability { get; set; }
        public string stU_Visual_Disability { get; set; }
        public string grD_ID_JOIN { get; set; }
        public string scT_DESCR_JOIN { get; set; }
        public DateTime sdoj { get; set; }
        public string feE_ID { get; set; }
    }

    public class GetHealthrestrictedRoot
    {
        public List<GetHealthrestricted> data { get; set; }
    }
}
