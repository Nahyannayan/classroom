﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Phoenix.Models
{
    public class AssignmentComment
    {
        public AssignmentComment()
        {
            lstDocuments = new List<AssignmentFeedbackFiles>();
        }
        public int AssignmentCommentId { get; set; }
        public int AssignmentStudentId { get; set; }
        [AllowHtml]
        public string Comment { get; set; }
        public bool ByTeacher { get; set; }
        public DateTime CreatedOn { get; set; }
        public string TeacherName { get; set; }
        public string StudentName { get; set; }
        public string StudentImage { get; set; }
        public string TeacherImage { get; set; }
        public string TeacherAvatar { get; set; }
        public string AssignmentTitle { get; set; }
        public int StudentId { get; set; }
        public long CreatedBy { get; set; }
        public long DeletedBy { get; set; }
        public long CompletedBy { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsCommentSeenByStudent { get; set; }
        public string CommentIds { get; set; }
        public List<AssignmentFeedbackFiles> lstDocuments { get; set; }
    }
    public class AssignmentFeedbackFiles
    {
        public int AssignmentFeedbackFileId { get; set; }
        public long AssignmentStudentId { get; set; }
        public string FileName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long DocumentId { get; set; }
        public string FileExtension { get; set; }
        public string UploadedFileName { get; set; }

    }
}
