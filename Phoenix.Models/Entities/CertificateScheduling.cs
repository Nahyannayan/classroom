﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class CertificateScheduling
    {
        public long CertificateSchedulingId { get; set; }
        public string Description { get; set; }
        public int? Points { get; set; }
        public string CertificateType { get; set; }
        public long CertificateTypeId { get; set; }
        public long CurriculumId { get; set; }
        public bool IsEmail { get; set; } = true;
        public bool IsPrint { get; set; } = true;
        public bool IsEmailOther { get; set; }
        public long[] EmailOtherStaffId { get; set; }
        public int? ScheduleType { get; set; }
        public long CreatedBy { get; set; }
        public bool IsActive { get; set; } = true;
        public long SchoolId { get; set; }
        public string CertificateSchedulingXml { get; set; }
    }
    #region Student Point Category
    public class StudentPointCategory
    {
        public StudentPointCategory()
        {
            Certificates = new List<CertificateProcessLog>();
        }
        public string StudentImageurl { get; set; }
        public string StudentName { get; set; }
        public long StudentNo { get; set; }
        public long StudentID { get; set; }
        public string StudentGrade { get; set; }
        public string StudentSection { get; set; }
        public int StudentPoints { get; set; }
        public string CetificateDescription { get; set; }
        public long CertificateScheduleId { get; set; }
        public long CertificateId { get; set; }
        public long CertificateType { get; set; }
        public string StudentEmail { get; set; }
        public string OtherEmail { get; set; }
        public bool IsEmail { get; set; }
        public bool IsPrint { get; set; }
        public bool IsEmailOther { get; set; }
        public string FilePath { get; set; }
        public string FileArray { get; set; }
        public List<CertificateProcessLog> Certificates { get; set; }
    }
    public enum CertificateStudentAction
    {
        Print, Email, EmailOther
    }

    public class CertificateProcessLog
    {
        public long CertificateProcessLogId { get; set; }
        public long AcademicYearId { get; set; }
        public long SchoolId { get; set; }
        public long StudentId { get; set; }
        public long TemplateId { get; set; }
        public string TemplateName { get; set; }
        public CertificateStudentAction ActionType { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateOn { get; set; }
        public string FilePath { get; set; }
    }
    public class TemplateMappingData
    {
        public int TemplateId { get; set; }
        public List<long> UserIds { get; set; }
        public string DynamicParameters { get; set; }
    }
    public class StudentBehaviourCertificate
    {
        public long StudentId { get; set; }
        public string CertificatePath { get; set; }
    }
    #endregion
}
