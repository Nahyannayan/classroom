﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class MSTeamsReponse
    {
        public long schoolTeamMeetingId { get; set; }
        public int SchoolGroupId { get; set; }
        public string id { get; set; }
        public string joinUrl { get; set; }
        [JsonProperty("webLink")]
        public string joinWebUrl { get; set; }
        public string subject { get; set; }
        [JsonProperty("start")]
        public MeetingDateInfo Start { get; set; }
        [JsonProperty("end")]
        public MeetingDateInfo End { get; set; }
        public long CreatedBy { get; set; }
        public bool IsPerUser { get; set; }
        [JsonProperty("onlineMeeting")]
        public OnlineMeeting OnlineMeeting { get; set; }
        public string endDateTime { get; set; }
        public string startDateTime { get; set; }
    }

    public class OnlineMeeting
    {
        [JsonProperty("joinUrl")]
        public string JoinUrl { get; set; }
    }

    public class MeetingDateInfo
    {
        [JsonProperty("dateTime")]
        public string DateTime { get; set; }
        [JsonProperty("timeZone")]
        public string TimeZone { get; set; }
    }
}
