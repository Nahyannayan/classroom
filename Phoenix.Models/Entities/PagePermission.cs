﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.Models
{
    public class PagePermission
    {
        public long UserId { get; set; }
        public int ModuleId { get; set; }
        public bool CanView { get; set; }
        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
    }

    public class ModulePermission
    {
        public int ModuleId { get; set; }
        public string PermissionCategoryIds { get; set; }
    }


    public enum PermissionCategory
    {
        View = 1,
        AddEditDelete = 2,
        Add = 4,
        Update = 5,
        Delete = 6
    }

    public enum UserTypes
    {
        Student = 1,
        Parent = 2,
        Teacher = 3,
        SchoolAdmin = 4,
        SuperAdmin = 5
    }
}