﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class LessonPlanFilterModule
    {
        public string Group { get; set; }
        public int Count { get; set; }
        public long GroupId { get; set; }
        public string Type { get; set; }

    }

}
