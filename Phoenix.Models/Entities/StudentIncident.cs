﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentIncident
    {
        public long UserId { get; set; }
        public int StudentIncidentId { get; set; }
        public int IncidentId { get; set; }
        public long StudentId { get; set; }
        public string IncidentRemarks { get; set; }
        public string IncidentType { get; set; }
        public string IncidentDate { get; set; }
        public int SubBehaviourCategoryId { get; set; }
        public string SubBehaviourName { get; set; }
        public short BehaviourCategoryId { get; set; }
        public int TotalCount { get; set; }
        public string BehaviourCategoryName { get; set; }
        public bool ShowOnDashboard { get; set; }
    }
}
