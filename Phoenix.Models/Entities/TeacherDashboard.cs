﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models.Entities;
namespace Phoenix.Models
{
    public class TeacherDashboard
    {
        public TeacherDashboard()
        {
            Blogs = new List<Blog>();
            ClassGroups = new List<SchoolGroup>();
            OtherGroups = new List<SchoolGroup>();
            SchoolBanners = new List<SchoolBanner>();
        }
        public List<Blog> Blogs { get; set; }
        public List<SchoolGroup> ClassGroups { get; set; }
        public List<SchoolGroup> OtherGroups { get; set; }
        public List<SchoolBanner> SchoolBanners { get; set; }


    }
}
