﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class FeeDetailsVM : PCStudentInfo
    {      
        public bool IsAllowedOnlinePayment { get; set; }
        public int PaymentTypeId { get; set; }
        public int ProviderTypeId { get; set; }
        public double PayingAmount { get; set; }
        public string IpAddress { get; set; }
        public double PaymentProcessingCharge { get; set; }
        public string PayMode { get; set; }
        public IEnumerable<PaymentType> LstPaymentType { get; set; }
        public IEnumerable<FeeSchedule> LstFeeSchedule { get; set; }
        public double TotalOutstanding { get; set; }
        public IEnumerable<FeeDetail> FeeDet { get; set; }
        public IEnumerable<DiscountDetails> DiscDetail { get; set; }
        public IEnumerable<StudentsFeeDetails> StudentFeeDetls { get; set; }
        //public IEnumerable<PaymentHistory> LstPaymentHistory { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromDatePaymHist { get; set; }
        public DateTime ToDatePaymHist { get; set; }

    }
    public class PCStudentInfo
    {
        public string SchoolName { get; set; }
        public int SchoolId { get; set; }
        public string Grade { get; set; }
        public string Section { get; set; }
        public Int64 StudentId { get; set; }
        public string StudentNo { get; set; }
        public string StudentImg { get; set; }
        public string StudentName { get; set; }
        public bool IsSelected { get; set; }
        public string StudentSection { get; set; }
    }
    public class FeeDetailVMRoot 
    {
        public string Source { get; set; }        
        public List<FeeDetailsVM> data { get; set; }
    }
    public class AdvanceDetails
    {
        public int Id { get; set; }
        public string Month { get; set; }
        public double FeeAmount { get; set; }
        public string DiscPerc { get; set; }
        public double DiscAmount { get; set; }
        public double NetAmount { get; set; }
        public string Source { get; set; }
    }
    public class StudentsFeeDetails : CommonStudentFee
    {     
        public double DueAmount { get; set; }
        public double DiscAmount { get; set; }
        public double PayAmount { get; set; }
        public string Source { get; set; }
        public string StudentName { get; set; }
        public string IsPaySibling { get; set; }
        public IEnumerable<FeeDetail> FeeDetail { get; set; }
    }
    public class FeeDetailsRoot : CommonResponse
    {
        public List<StudentsFeeDetails> data { get; set; }
    }
    public class FeeDetail : CommonFeeDetails
    {        
        public string RangeLables { get; set; }
        public string RangeValues { get; set; }
        public double OutstandingAmount { get; set; }       
       
    }  
    public class FeeDetailRoot : CommonResponse
    {
        public List<FeeDetail> data { get; set; }
    }
    public class PaymentType
    {
        public int PaymentTypeID { get; set; }
        public int ProviderTypeID { get; set; }
        public string ProviderTypeDesc { get; set; }
        public string PaymentTypeImagePath { get; set; }
        public string ApplePayID { get; set; }
        public string ApplePayMerchantName { get; set; }
        public string GooglePayID { get; set; }
        public double ProcessingRate { get; set; }
        public bool IsSelected { get; set; }
        public string MoreInfo { get; set; }
    }
    public class PaymentTypeRoot : CommonResponse
    {
        public List<PaymentType> data { get; set; }
    }
    public class DiscountDetails
    {
        public string Month { get; set; }
        public double FeeAmount { get; set; }
        public double DiscPrec { get; set; }
        public double DiscAmount { get; set; }
        public double NetAmount { get; set; }
    }
    public class DiscountData
    {
        public double DiscountAmount { get; set; }
        public List<DiscountDetails> DiscountDetails { get; set; }
    }
    public class DiscountDetailsRoot : CommonResponse
    {
        public DiscountData data { get; set; }
    }
    public class FeeSchedule
    {
        public string StudentNo { get; set; }
        public string FeeDescription { get; set; }
        public string DueDate { get; set; }
        public double Amount { get; set; }
    }
    public class FeeScheduleRoot : CommonResponse
    {      
        public List<FeeSchedule> data { get; set; }
    }

    #region common online payment process
    public class OnlinePayResponse
    {
        public int PaymentRefNo { get; set; }
        public string PaymentRedirectURL { get; set; }
        public OnlineFeePayment Response { get; set; }
    }
    public class OnlineFeePayment : CommonResponse
    {
        public string ErrorCode { get; set; }
        public dynamic data { get; set; }
        public int? TransactionId { get; set; }
    }
    public class CommonStudentFee
    {
        public string STU_NO { get; set; }
        public string OnlinePaymentAllowed { get; set; }
        public string UserMessageforOnlinePaymentBlock { get; set; }
        public string PaymentTypeID { get; set; }
        public double PayingAmount { get; set; }
        public string IpAddress { get; set; }
        public double PaymentProcessingCharge { get; set; }
        public string PayMode { get; set; }       
        public IEnumerable<CommonFeeDetails> StudFeeDetails { get; set; }
        public IEnumerable<DiscountDetails> DiscountDetails { get; set; }
    }
    public class CommonFeeDetails
    {
        public bool BlockPayNow { get; set; }
        public bool AdvancePaymentAvailable { get; set; }
        public string FeeDescription { get; set; }
        public string FeeID { get; set; }
        public double DueAmount { get; set; }
        public double PayAmount { get; set; }
        public double DiscAmount { get; set; }
        public double OriginalAmount { get; set; }
        public int ActivityRefID { get; set; }
        public string ActivityFeeCollType { get; set; }
        public IEnumerable<AdvanceDetails> AdvanceDetails { get; set; }
    }
    public class CommonResponse
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public string responseCode { get; set; }
        public string message { get; set; }
    }
    #endregion
}
