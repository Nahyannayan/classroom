﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class MigrationDetail
    {
        public int SyncDetailId { get; set; }
        public string SyncDetail { get; set; }
        public string SyncCategory { get; set; }
        public int TotalSyncRecord { get; set; }
        public string SyncDate { get; set; }

    }
    public class SyncDetailById
    {
        public int SyncDetailId { get; set; }

        public long userid { get; set; }

        public long SchoolId { get; set; }

        public DateTime SyncDate { get; set; }

        public int TotalSyncRecord { get; set; }

    }
    public class MigrationDetailCount
    {
        public int SyncDetailId { get; set; }
        public int TotalSyncRecord { get; set; }
        public DateTime SyncDate { get; set; }

    }
}
