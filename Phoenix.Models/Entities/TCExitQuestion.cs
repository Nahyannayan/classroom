﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class TCExitQuestion
    {
        public string StudentID { get; set; }
        public Boolean IsOnlineTransfer { get; set; }
        public string Feedback { get; set; }
        public List<TCAnswer> AnswerList { get; set; }
    }
}
