﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
  public  class SchoolCourse
    {
        public long CourseId { get; set; }
        public long SchoolId { get; set; }

        public long SchoolLevelId { get; set; }

        public long DepartmentId { get; set; }

        public string CourseName { get; set; }
    }
}
