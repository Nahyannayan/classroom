﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentPortfolio
    {
        public StudentPortfolio()
        {
            SkillsList = new List<StudentSkills>();
            CertificateList = new List<StudentCertificate>();
            Academics = new List<AcademicDocuments>();
            StudentGoals = new List<StudentGoal>();
            SkillsEndorsed = new List<SkillEndorsement>();
            DashboardVideos = new List<string>();
            StudentAcheivements = new List<StudentIncident>();
            StudentAcheivementsNew = new List<StudentPortfolioAchievements>();
            StudentProfileSections = new List<StudentProfileSections>();
        }
        public Student StudentDetails { get; set; }
        public List<StudentSkills> SkillsList { get; set; }
        public List<StudentCertificate> CertificateList { get; set; }
        public List<AcademicDocuments> Academics { get; set; }
        public List<StudentGoal> StudentGoals { get; set; }
        public List<SkillEndorsement> SkillsEndorsed { get; set; }
        public List<string> DashboardVideos { get; set; }
        public List<StudentIncident> StudentAcheivements { get; set; }
        public List<StudentPortfolioAchievements> StudentAcheivementsNew { get; set; }
        public List<SchoolBadge> StudentBadges { get; set; }
        public List<StudentProfileSections> StudentProfileSections { get; set; }
        public List<StudentAssessmentReportView> StudentReports { get; set; }
    }

    public class AcademicInfo
    {

    }

    public class StudentSkills
    {
        public int SkillId { get; set; }
        public string SkillName { get; set; }
        public int StudentId { get; set; }
        public int StudentSkillId { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }

    }

    public class StudentPortfolioAchievements
    {
        public long AchievementId { get; set; }
        public string Title { get; set; }
        public string EventDate { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string PhysicalFilePath { get; set; }
        public string FileCssClass { get; set; }

    }
}
