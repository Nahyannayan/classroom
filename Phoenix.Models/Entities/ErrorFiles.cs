﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ErrorFiles
    {
        public long ErrorScreenId { get; set; }
        public string UploadedFileName { get; set; }
        public string FileName { get; set; }
        public DateTime CreatedOn { get; set; }
        public long ErrorLogId { get; set; }
    }
}
