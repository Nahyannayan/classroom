﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    class Incident
    {
    }
    public class IncidentListModel
    {
        private const string dateFormat = "dd-MMM-yyyy";
        private DateTime? _incident_Date;
        private DateTime _recorded_On;

        public IncidentListModel()
        {
            IncidentStudentLists = new List<IncidentStudentList>();
            IncidentWitnesses = new List<IncidentWitness>();
        }
        public long IncidentId { get; set; }
        public DateTime? Incident_Date { get => _incident_Date; set { _incident_Date = value; Incident_DateString = value.HasValue ? value.Value.ToString(dateFormat) : ""; } }
        public string Incident_DateString { get; set; }
        public IncidentType Incident_Type { get; set; }
        public DateTime Recorded_On { get => _recorded_On; set { _recorded_On = value; Recorded_OnString = value.ToString(dateFormat); } }
        public string Recorded_OnString { get; set; }
        public string Reported_By { get; set; }
        public string Incident_Time { get; set; }
        public long Reported_ById { get; set; }
        public string Incident_Remarks { get; set; }
        public int Incident_CategoryId { get; set; }
        public int Incident_SubCategoryId { get; set; }
        public long Incident_LevelId { get; set; }
        public double Points { get; set; }
        public int CurriculumId { get; set; }
        public string StudentDetail { get; set; }
        public IEnumerable<IncidentStudentList> IncidentStudentLists { get; set; }
        public IEnumerable<IncidentWitness> IncidentWitnesses { get; set; }
        public IList<SubjectCategory> SubCategory { get; set; }
        public string strIncident_Date { get; set; }
    }
    public class SubjectCategory
    {
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public bool Selected { get; set; }
        public int Points { get; set; }
    }


    public class IncidentStudentList
    {
        public long IncidentId { get; set; }
        public long STUDENT_ID { get; set; }
        public string STUDENT_NO { get; set; }
        public string STUDENT_NAME { get; set; }
        public string Student_Image_url { get; set; }
        public string SECTION { get; set; }
        public string GRADE { get; set; }
        public string STREAM { get; set; }
        public string SHIFT { get; set; }
        public string PRIMARY_CONTACT { get; set; }
        public string PARENTEMAIL { get; set; }
        public string PARENTMOBILE { get; set; }
        public string PARENT_NAME { get; set; }
        public string IsConfidential { get; set; }
        public long InvolvedId { get; set; }
        public int FollowupCount { get; set; }
        public long ActionId { get; set; }
        public long ActionTakenId { get; set; }
    }

    public class ChartModel
    {
        public string GRADE { get; set; }
        public string IncidentType { get; set; }
        public int IncidentCount { get; set; }
    }
    public class IncidentStaffList
    {
        public int EmpId { get; set; }
        public string EmpNo { get; set; }
        public string EmployeeName { get; set; }
    }
    public class IncidentEntry
    {
        public IncidentEntry()
        {
            StudentInvolved = new List<IncidentStudentInvolved>();
            Witness = new List<IncidentWitness>();
        }
        public int BehaviourId { get; set; }
        public int SchoolId { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime? IncidentDate { get; set; }
        public string IncidentTime { get; set; }
        public IncidentType IncidentType { get; set; }
        public long StaffId { get; set; }
        public string IncidentDesc { get; set; }
        public string DataMode { get; set; }
        public int CategoryId { get; set; }
        public int CurriculumId { get; set; }
        public long LevelMapping_ID { get; set; }
        #region Student Involved 
        public List<IncidentStudentInvolved> StudentInvolved { get; set; }
        public DataTable StudentInvolvedDT
        {
            get
            {
                var dt = this.CreateStudentDT();
                StudentInvolved.ForEach(e =>
                {
                    DataRow dr = dt.NewRow();
                    dr["INVOLVED_ID"] = e.InvolvedId;
                    dr["INVOLVED_STUDENT_ID"] = e.InvolvedStudentId;
                    dr["INVOLVED_CONFIDENTIAL"] = Convert.ToInt32(e.IsConfidential);
                    dt.Rows.Add(dr);
                });
                return dt;
            }
        }
        private DataTable CreateStudentDT()
        {
            var dt = new DataTable();
            dt.Columns.Add("INVOLVED_ID", typeof(long));
            dt.Columns.Add("INVOLVED_STUDENT_ID", typeof(long));
            dt.Columns.Add("INVOLVED_CONFIDENTIAL", typeof(int));
            return dt;
        }
        #endregion

        #region witness Involved 
        public List<IncidentWitness> Witness { get; set; }
        public DataTable WitnessDT
        {
            get
            {
                var dt = this.CreateWitnessDT();
                Witness.ForEach(e =>
                {
                    DataRow dr = dt.NewRow();
                    dr["WITNESS_ID"] = e.WitnessId;
                    dr["WITNESS_INVOLVED_ID"] = e.WitnessInvolvedId;
                    dr["WITNESS_TYPE"] = (int)e.WitnessType;
                    dr["WITNESS_REMARKS"] = e.WitnessRemark;
                    dt.Rows.Add(dr);
                });
                return dt;
            }
        }
        private DataTable CreateWitnessDT()
        {
            var dt = new DataTable();
            dt.Columns.Add("WITNESS_ID", typeof(long));
            dt.Columns.Add("WITNESS_INVOLVED_ID", typeof(long));
            dt.Columns.Add("WITNESS_TYPE", typeof(int));
            dt.Columns.Add("WITNESS_REMARKS", typeof(string));
            return dt;
        }
        #endregion
        public string strIncidentDate { get; set; }
    }
    public enum IncidentType { FA = 0, FI = 1 }
    public enum WitnessType { STF = 0, STU = 1 }
    public class IncidentWitness
    {
        public long WitnessId { get; set; }
        public long IncidentId { get; set; }
        public long WitnessInvolvedId { get; set; }
        public string WitnessInvolvedName { get; set; }
        public WitnessType WitnessType { get; set; }
        public string WitnessRemark { get; set; }
        public string WitnessDatamode { get; set; }
    }
    public class IncidentStudentInvolved
    {
        public long InvolvedId { get; set; }
        public long InvolvedStudentId { get; set; }
        public string DataMode { get; set; }
        public bool IsConfidential { get; set; }
    }

    public class IncidentDashBoardModel
    {
        public IEnumerable<IncidentListModel> IncidentLists { get; set; }
        public IEnumerable<ChartModel> PieChart { get; set; }
        public IEnumerable<ChartModel> LineChart { get; set; }
    }
    public class IncidentModel
    {
        public IncidentListModel Incident { get; set; }
        public IEnumerable<IncidentStudentList> StudentLists { get; set; }
        public IEnumerable<IncidentWitness> Witnesses { get; set; }
    }
    public class BehaviourAction
    {
        public long ActionId { get; set; }
        public bool ParentCalled { get; set; }
        public DateTime? ParentCalledDate { get; set; }
        public string ParentCalledComment { get; set; }
        public bool ParentInterviewed { get; set; }
        public DateTime? ParentInterviewedDate { get; set; }
        public string ParentInterviewedComment { get; set; }
        public bool NotesInStudentPlanner { get; set; }
        public DateTime? NotesInStudentPlannerDate { get; set; }
        public bool BreakDetentionGiven { get; set; }
        public DateTime? BreakDetentionGivenDate { get; set; }
        public bool AfterSchoolDetentionGiven { get; set; }
        public DateTime? AfterSchoolDetentionGivenDate { get; set; }
        public bool Suspension { get; set; }
        public DateTime? SuspensionDate { get; set; }
        public bool ReferredToStudentsCounsellor { get; set; }
        public DateTime? ReferredToStudentsCounsellorDate { get; set; }
        public Student StudentDetails { get; set; }
        public long IncidentId { get; set; }
        public long StudentId { get; set; }
        public long CategoryId { get; set; }
        public string DataMode { get; set; }
        public DateTime Entry_Date { get; set; }
        public double Score { get; set; }
    }
    public class BehaviourActionFollowup
    {
        public long ActionDetailsId { get; set; }
        public long StudentInvolvedId { get; set; }
        public long ActionId { get; set; }
        public DateTime ActionDate { get; set; }
        public string Action_Followup_Remarks { get; set; }
        public long Action_FollowupBy_Id { get; set; }
        public long Action_FollowupBy_Designation_Id { get; set; }
        public string Action_FollowupBy_EmpNo { get; set; }
        public string Action_FollowupBy_EmpName { get; set; }
        public string Action_FollowupBy_Designation { get; set; }
        public string Action_FollowupBy_DesignationGroup { get; set; }
        public long Action_CurrentUser_DesignationId { get; set; }
        public string DataMode { get; set; }
    }
    public class FollowUpStaff
    {
        public long EMPLOYEE_ID { get; set; }
        public string EMPLOYEE_NAME { get; set; }
    }
    public class FollowUpDesignation
    {
        public long DESIGNATION_ID { get; set; }
        public string DESIGNATION_DESCRIPTION { get; set; }
    }
    public class ActionDetails
    {
        public int ActionTypeId { get; set; }
        public string ActionTypeDesc { get; set; }
        public bool IsCommentRequired { get; set; }
        public long ReportId { get; set; }
        public long StudentInvolvedId { get; set; }
        public string ReportComment { get; set; }
        public bool ActionDone { get; set; }
        public DateTime? ActionDate { get; set; }
        public long CreatedBy { get; set; }
    }
    public class ActionModel
    {
        public ActionModel()
        {
            ActionDetails = new List<ActionDetails>();
        }
        public long IncidentId { get; set; }
        public long StudentId { get; set; }
        public IEnumerable<ActionDetails> ActionDetails { get; set; }
        public IncidentStudentList Student { get; set; }
        public long StudentInvolvedId { get; set; }
        public DateTime? ActionDate { get; set; }
    }
}
