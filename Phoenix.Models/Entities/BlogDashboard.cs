﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class BlogStudentDash
    {
        public BlogStudentDash() {
        }
        public long BlogId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CommentCount { get; set; }
        public DateTime CommentCreatedDate { get; set; }
        public DateTime FinalDate { get; set; }
        public int LikeCount { get; set; }
        public DateTime LikeCreatedDate { get; set; }
        public string UserName { get; set; }
        public string ProfileImage { get; set; }
        public string ActivityType { get; set; }

        public long CreatedBy { get; set; }
        public int UserTypeId { get; set; }
    }

    public class BlogTeacherDash
    {
        public BlogTeacherDash()
        {
        }
        public long BlogId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CommentCount { get; set; }
        public DateTime CommentCreatedDate { get; set; }
        public DateTime FinalDate { get; set; }
        public int LikeCount { get; set; }
        public DateTime LikeCreatedDate { get; set; }
        public string UserName { get; set; }
        public string ProfileImage { get; set; }
        public string ActivityType { get; set; }
        public string Comment { get; set; }
        public long CreatedBy { get; set; }
        public long GroupId { get; set; }
        public int UserTypeId { get; set; }
    }
}
