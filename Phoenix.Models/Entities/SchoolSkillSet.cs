﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class SchoolSkillSet
    {
        public SchoolSkillSet()
        {

        }
        public SchoolSkillSet(int _SkillId)
        {
            SkillId = _SkillId;
        }
        public int SkillId { get; set; }
        public int SchoolGradeId { get; set; }
        public string SkillName { get; set; }
        public string SkillDescription { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public String CreatedByName { get; set; }
        public String CreatedOn { get; set; }
        public string FormattedCreatedOn { get; set; }
        public bool IsApproved { get; set; }
        public string ApprovedBy { get; set; }
        public string SkillSetXml { get; set; }



    }
}
