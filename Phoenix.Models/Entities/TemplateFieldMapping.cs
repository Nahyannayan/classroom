﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class TemplateFieldMapping
    {
        public int TemplateId { get; set; }
        public int CertificateColumnId { get; set; }
        public string ColumnName { get; set; }
        public string ColumnData { get; set; }
        public string FieldName { get; set; }
        public string PlaceHolder { get; set; }
        public string CertificateTitle { get; set; }
        public short CertificateTypeId { get; set; }
        public string FileName { get; set; }
        public string ImageData { get; set; }
        public long UserId { get; set; }
    }
}
