﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class GradeBook
    {
        public GradeBook()
        {
            CourseIds = new List<long>();
            SchoolGradeIds = new List<long>();
            AssessmentTypeIds = new List<int>();
            AssignmentQuizSetups = new List<AssignmentQuizSetup>();
            Standardizeds = new List<StandardizedExaminationSetup>();
            GradeBookCourseDetailList = new List<GradeBookCourseDetail>();
            InternalAssessments = new List<InternalAssessment>();
        }
        public long GradeBookId { get; set; }
        public string GradeBookName { get; set; }
        public long SchoolId { get; set; }
        public List<long> CourseIds { get; set; }
        public List<long> SchoolGradeIds { get; set; }
        public List<int> AssessmentTypeIds { get; set; }
        public long Createdby { get; set; }
        public DateTime CreatedOn { get; set; }
        public List<AssignmentQuizSetup> AssignmentQuizSetups { get; set; }
        public List<StandardizedExaminationSetup> Standardizeds { get; set; }
        public List<GradeBookCourseDetail> GradeBookCourseDetailList { get; set; }
        public List<GradeBookGradeDetail> GradeBookGradeDetailList { get; set; }
        public List<InternalAssessment> InternalAssessments { get; set; }
        public TranModes Modes { get; set; }
        public int TotalCount { get; set; }
        public string GradeDisplay { get; set; }
        public DataTable GetAssignmentQuizSetupsDT()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AssignmentQuizSetupId", typeof(long));
            dt.Columns.Add("AssessmentTypeid", typeof(int));
            dt.Columns.Add("GradingTemplateId", typeof(int));
            dt.Columns.Add("CalculationType", typeof(int));
            dt.Columns.Add("Weightage", typeof(int));
            dt.Columns.Add("StartDate", typeof(DateTime));
            dt.Columns.Add("EndDate", typeof(DateTime));

            AssignmentQuizSetups.ForEach(x =>
            {
                dt.Rows.Add(x.AssignmentQuizSetupId, x.AssessmentTypeid, x.GradingTemplateId, x.CalculationType, x.Weightage, x.StartDate, x.EndDate);
            });

            return dt;
        }
        public DataTable GetStandardizedsDT()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AssessmentTypeid", typeof(int));
            dt.Columns.Add("CheckBoxDetailId", typeof(int));
            Standardizeds.ForEach(x =>
            {
                dt.Rows.Add(x.AssessmentTypeid, x.CheckBoxDetailId);
            });
            return dt;
        }
        public DataTable GetInternalAssessmentDT()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("InternalAssessmentId", typeof(long));
            dt.Columns.Add("GradebookId", typeof(long));
            dt.Columns.Add("RowName", typeof(string));
            dt.Columns.Add("Weightage", typeof(int));
            dt.Columns.Add("IsCalculatedField", typeof(bool));
            dt.Columns.Add("EntryType", typeof(int));
            dt.Columns.Add("FromMarks", typeof(int));
            dt.Columns.Add("ToMarks", typeof(int));
            dt.Columns.Add("DataEntryGradingTemplateId", typeof(int));
            dt.Columns.Add("Commentlength", typeof(int));
            dt.Columns.Add("AllowTeacherToAddColumn", typeof(bool));
            dt.Columns.Add("StartDate", typeof(DateTime));
            dt.Columns.Add("EndDate", typeof(DateTime));
            dt.Columns.Add("CalculatedGradingTemplateId", typeof(int));
            dt.Columns.Add("CalculatedFormulaId", typeof(int));
            dt.Columns.Add("ResultConsideredIds", typeof(string));
            dt.Columns.Add("Index", typeof(int));
            dt.Columns.Add("IsActive", typeof(bool));
            InternalAssessments.ForEach(x =>
            {
                x.ResultsToConsider.RemoveAll(r => r == "0");
                dt.Rows.Add(x.InternalAssessmentId, x.GradebookId, x.RowName, x.Weightage, x.IsCalculatedField,
                    x.RowType, x.FromMark, x.ToMark, x.DataEntryGradingTemplateId, x.CommentLength, x.AllowTeacherToAddColumn
                    , x.StartDate, x.EndDate, x.CalculatedGradingTemplateId, x.CalculatedFormulaId, string.Join(",", x.ResultsToConsider), x.Index,
                    x.IsActive);
            });

            return dt;
        }
    }
    public class AssignmentQuizSetup
    {
        public AssignmentQuizSetup()
        {
            StartDate = DateTime.Today;
            EndDate = DateTime.Today;
        }
        public long GradebookId { get; set; }
        public long AssignmentQuizSetupId { get; set; }
        public int AssessmentTypeid { get; set; }
        public int CalculationType { get; set; }
        public int GradingTemplateId { get; set; }
        public int? Weightage { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Index { get; set; }
    }
    public class StandardizedExaminationSetup
    {
        public int AssessmentTypeid { get; set; }
        public int CheckBoxDetailId { get; set; }
        public int[] ExaminationSetup { get; set; }
        public string ExternalExaminationIds { get; set; }
        public string SubExternalExaminationIds { get; set; }
    }
    public class GradeBookCourseDetail
    {
        public long GradebookId { get; set; }
        public long CourseId { get; set; }
        public string CourseTitle { get; set; }
    }
    public class GradeBookGradeDetail
    {
        public long GradebookId { get; set; }
        public long SchoolGradeId { get; set; }
        public string GradeDisplay { get; set; }
        public string CourseName { get; set; }
    }
    public class GradeAndCourse
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public bool IsCourse { get; set; }
    }
    public class GradebookGrade
    {
        public long Id { get; set; }
        public string Description { get; set; }
    }
    public class TeacherGradeBook
    {
        public TeacherGradeBook()
        {
            StudentLists = new List<GradeBookStudentList>();
            AssessmentType = new List<int>();
            Assignments = new List<GradeBookAssignmentQuiz>();
            Quizzes = new List<GradeBookAssignmentQuiz>();
            InternalAssessments = new List<InternalAssessment>();
            Standards = new List<StandardExamination>();
            Examinations = new List<StandardExamination>();
            GradingTemplates = new List<GradeBookGradingTemplate>();
            AssessmentScores = new List<InternalAssessmentScore>();
            GradebookRuleSetups = new List<GradebookRuleSetup>();
        }
        public IEnumerable<GradeBookStudentList> StudentLists { get; set; }
        public IEnumerable<int> AssessmentType { get; set; }
        public IEnumerable<GradeBookAssignmentQuiz> Assignments { get; set; }
        public IEnumerable<GradeBookAssignmentQuiz> Quizzes { get; set; }
        public List<InternalAssessment> InternalAssessments { get; set; }
        public IEnumerable<TeacherInternalAssessment> TeacherInternalAssessments { get; set; }
        public IEnumerable<InternalAssessmentScore> AssessmentScores { get; set; }
        public IEnumerable<StandardExamination> Standards { get; set; }
        public IEnumerable<StandardExamination> Examinations { get; set; }
        public IEnumerable<GradeBookGradingTemplate> GradingTemplates { get; set; }
        public IEnumerable<GradebookRuleSetup> GradebookRuleSetups { get; set; }
        public bool IsPreview { get; set; }
    }
    public class GradeBookStudentList
    {
        public long StudentId { get; set; }
        public string StudentName { get; set; }
        public string StudentImage { get; set; }
        public long SchoolUserId { get; set; }
    }
    public class GradeBookAssignmentQuiz
    {
        public long StudentId { get; set; }
        public long AssignmentQuizId { get; set; }
        public string AssignmentQuizTitle { get; set; }
        public float Score { get; set; }
        public string ShortLabel { get; set; }
        public string Status { get; set; }
        public string GradingColor { get; set; }
        public string GradingTemplateItemSymbol { get; set; }
        public bool IsTopRow { get; set; }
    }
    public class StandardExamination
    {
        public long StudentId { get; set; }
        public int AssessmentTypeId { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
    public class InternalAssessment : GradingTemplateItem
    {
        public InternalAssessment()
        {
            ResultsToConsider = new List<string>();
            TeacherInternalAssessments = new List<TeacherInternalAssessment>();
        }
        public long InternalAssessmentId { get; set; }
        public long GradebookId { get; set; }
        public string RowName { get; set; }
        public int RowType { get; set; }
        public int Weightage { get; set; }
        public bool? IsCalculatedField { get; set; }
        public int EntryType { get; set; }
        public int? FromMark { get; set; }
        public int? ToMark { get; set; }
        public int? DataEntryGradingTemplateId { get; set; }
        public int? CalculatedGradingTemplateId { get; set; }
        public int? CommentLength { get; set; }
        public bool AllowTeacherToAddColumn { get; set; }
        public bool IsRuleProcessingSetupEnabled { get; set; }
        public List<string> ResultsToConsider { get; set; }
        public string ResultConsideredId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Index { get; set; }
        public long? CalculatedFormulaId { get; set; }
        public string Formula { get; set; }
        public string FormulaDescription { get; set; }
        public List<TeacherInternalAssessment> TeacherInternalAssessments { get; set; }
        public int IsEdit { get; set; }
        public new bool IsActive { get; set; }
        public GradebookProgressTracker ProgressTracker { get; set; }
    }
    public class TeacherInternalAssessment
    {
        public long InternalAssessmentId { get; set; }
        public string RowName { get; set; }
        public int RowType { get; set; }
        public int? FromMark { get; set; }
        public int? ToMark { get; set; }
        public int? GradingTemplateId { get; set; }
        public int? CommentLength { get; set; }
        public long GroupId { get; set; }
        public long ParentInternalAssessmentId { get; set; }
        public long CreatedBy { get; set; }
        public int ColumnCount { get; set; }
        public List<long> StudentIds { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedOn { get; set; }
        public int Index { get; set; }
        public bool ForceDeleteAssessmentEvenIfDataExist { get; set; }
    }
    public class GradeBookGradingTemplate : GradingTemplateItem
    {
        public long InternalAssessmentId { get; set; }
        public bool IsForParent { get; set; }
    }
    public class InternalAssessmentScore
    {
        public long InternalAssessmentId { get; set; }
        public long StudentId { get; set; }
        public string Score { get; set; }
        public long ParentInternalAssessmentId { get; set; }
        public long CreatedBy { get; set; }
        public int ResultType { get; set; }
    }
    public class GradebookFormula
    {
        public long FormulaId { get; set; }
        public string Formula { get; set; }
        public string FormulaDescription { get; set; }
        public long SchoolId { get; set; }
        public string Assessments { get; set; }
        public IDictionary<string, string> Buttons { get; set; }
        public int MyProperty { get; set; }
    }
    public class GradebookFormulaButtons
    {
        public string BtnVal { get; set; }
        public string BtnText { get; set; }
    }
    public class GradebookRuleSetup
    {
        public GradebookRuleSetup()
        {
            RuleSetupMappings = new List<RuleSetupMapping>();
            TeacherInternalAssessments = new List<TeacherInternalAssessment>();
        }
        public long ProcessingRuleSetupId { get; set; }
        public long InternalAssessmentId { get; set; }
        public int CalculationTypeId { get; set; }
        public int BestOfCount { get; set; }
        public string Formula { get; set; }
        public long CreatedBy { get; set; }
        public bool IsDelete { get; set; }
        public List<RuleSetupMapping> RuleSetupMappings { get; set; }
        public List<TeacherInternalAssessment> TeacherInternalAssessments { get; set; }
    }
    public class RuleSetupMapping
    {
        public long SubInternalAssessmentId { get; set; }
        public int Weightage { get; set; }
        public long ProcessingRuleSetupId { get; set; }
    }
    public class GradebookProgressTracker
    {
        public GradebookProgressTracker()
        {
            Attainments = new List<ProgressTrackerAttainment>();
            Progresses = new List<ProgressTrackerProgress>();
            ProgressTrackerType = new List<int>();
        }
        public IEnumerable<int> ProgressTrackerType { get; set; }
        public IEnumerable<ProgressTrackerAttainment> Attainments { get; set; }
        public IEnumerable<ProgressTrackerProgress> Progresses { get; set; }
    }
    public class ProgressTrackerAttainment : GradeBookStudentList
    {
        public decimal Attainment { get; set; }
        public string AttainmentColor { get; set; }
        public string AttainmentDescriptor { get; set; }
    }
    public class ProgressTrackerProgress : GradeBookStudentList
    {
        public decimal Progress { get; set; }
        public string ProgressColor { get; set; }
        public string ProgressDescriptor { get; set; }
    }
}
