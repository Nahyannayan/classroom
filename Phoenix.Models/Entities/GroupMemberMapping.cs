﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class GroupMemberMapping
    {
        public int GroupTeacherMappingId { get; set; }
        public int GroupId { get; set; }
        public long MemberId { get; set; }
        public string MemberName { get; set; }
        public bool IsViewOnly { get; set; }
        public bool IsContribute { get; set; }
        public bool IsManager { get; set; }
        public bool ToBeDeleted { get; set; }

        public int UserTypeId { get; set; }
        public string UserProfileImage { get; set; }
        public bool IsGroupOwner { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long UpdatedBy { get; set; }
        public bool IsNew { get; set; }
        public bool IsDeleted { get; set; }

        public bool IsPrimaryTeacher { get; set; }
        public long GroupPrimaryTeacherMappingId { get; set; }

        public string ClassGroupIds { get; set; }
        public bool IsHost { get; set; }
    }
}
