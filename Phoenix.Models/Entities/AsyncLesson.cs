﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AsyncLesson
    {
        public AsyncLesson()
        {

        }
        public AsyncLesson(long _asyncLessonId)
        {
            AsyncLessonId = _asyncLessonId;
        }

        public long AsyncLessonId { get; set; }
        public string LessonTitle { get; set; }
        public string Description { get; set; }
        public long CourseId { get; set; }
        public string CourseName { get; set; }
        public long PlanSchemeId { get; set; }
        public string PlanSchemeName { get; set; }
        public string Students { get; set; }
        public long UnitId { get; set; }
        public string UnitName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long FolderId { get; set; }
        public long SectionId { get; set; }
        public long ModuleId { get; set; }
        public bool IsActive { get; set; }
        public bool IsPublish { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }

    public class AsyncLessonResourcesActivities
    {
        public AsyncLessonResourcesActivities()
        {

        }

        public AsyncLessonResourcesActivities(long _resourceActivityId)
        {
            ResourceActivityId = _resourceActivityId;
        }

        public long ResourceActivityId { get; set; }
        public long AsyncLessonId { get; set; }
        public int ModuleId { get; set; }
        public long SectionId { get; set; }
        public long FolderId { get; set; }
        public int StepNo { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public int FileTypeId { get; set; }
        public string Icon { get; set; }
        public int Status { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime UpdatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public decimal FileSizeInMB { get; set; }
        public string GoogleDriveFileId { get; set; }
        public int ResourceFileTypeId { get; set; }
        public string PhysicalPath { get; set; }
        //For Group Quiz
        public long QuizId { get; set; }
        public bool IsSetTime { get; set; }
        public int QuizTime { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartTime { get; set; }
        public bool IsSolved { get; set; }
        public int TimeInSec { get; set; }
        public string QuizOverAllTime { get; set; }
        public bool IsSetStartDate { get; set; }
        public bool IsStartedSetTimer { get; set; }
        public string QuizStartDate { get; set; }
        public string QuizStartedTime { get; set; }
        public bool IsRandomQuestion { get; set; }
        public int MaxSubmit { get; set; }
        public int QuestionPaginationRange { get; set; }
    }

    public class AsyncLessonStudentMapping
    {
        public AsyncLessonStudentMapping()
        {

        }

        public AsyncLessonStudentMapping(long _asyncLessonStudentMappingId)
        {
            AsyncLessonStudentMappingId = _asyncLessonStudentMappingId;
        }

        public long AsyncLessonStudentMappingId { get; set; }
        public long AsyncLessonId { get; set; }
        public long StudentId { get; set; }
        public string StudentName { get; set; }
        public int Status { get; set; }
        public int StepNo { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

        public string GoogleMailAddress { get; set; }
        public string MicorosoftMailAddress { get; set; }
        public string UserProfileImage { get; set; }
        public int DoubtCount { get; set; }
    }

    public class AsyncLessonComments
    {
        public AsyncLessonComments()
        {

        }
        public AsyncLessonComments(long _asyncLessonCommentId)
        {
            AsyncLessonCommentId = _asyncLessonCommentId;
        }

        public long AsyncLessonCommentId { get; set; }
        public long ResourceActiivtyId { get; set; }
        public string Name { get; set; }
        public long UserId { get; set; }
        public string Comment { get; set; }
        public long ParentCommentId { get; set; }
        public bool IsActive { get; set; }
        public bool IsResolved { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

    }

    public class UpdateAsyncLessonResourcesActivity
    {
        public long ResourceActivityId { get; set; }
        public int Status { get; set; }
        public long updatedBy { get; set; }
        public bool IsStatusUpdate { get; set; }
        public List<AsyncLessonResourcesActivities> ResourcesActivitiesList { get; set; }

        public UpdateAsyncLessonResourcesActivity()
        {
            ResourcesActivitiesList = new List<AsyncLessonResourcesActivities>();
        }
    }

    public class ViewAsyncLessonDetail
    {
        public AsyncLesson AsyncLesson { get; set; }
        public List<AsyncLessonResourcesActivities> ResourcesList { get; set; }
        public List<AsyncLessonStudentMapping> StudentList { get; set; }
        public List<AsyncLessonComments> CommentList { get; set; }
        public long AsyncLessonId { get; set; }
        public long GroupId { get; set; }

        public List<string> ImageExt { get; set; }
        public List<string> VideoExt { get; set; }
        public List<string> AudioExt { get; set; }
        public List<string> DocumentExt { get; set; }

        public ViewAsyncLessonDetail()
        {
            ResourcesList = new List<AsyncLessonResourcesActivities>();
            StudentList = new List<AsyncLessonStudentMapping>();
            CommentList = new List<AsyncLessonComments>();
            ImageExt = new List<string>();
            VideoExt = new List<string>();
            AudioExt = new List<string>();
            DocumentExt = new List<string>();  
        }
    }
}
