﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Standard { get; set; }
        public int StudentId { get; set; }
        public string StudentNumber { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string StudentImage { get; set; }
        public List<string> SchoolGroupId { get; set; }
        public string StudentInternalId { get; set; }
        public int TotalCount { get; set; }
        public string SchoolName { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string StudentName { get; set; }
        public string UserCoverFilePath { get; set; }
        public bool IsCoverPhoto { get; set; }
        public int StudentNotificationCount { get; set; }
        public string GradeDisplay { get; set; }
    }
}

