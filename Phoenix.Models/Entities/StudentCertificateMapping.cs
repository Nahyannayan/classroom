﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentCertificateMapping
    {
        public int TemplateId { get; set; }
        public string StudentIds { get; set; }
        public long CreatedBy { get; set; }
        public string RemovedStudentIds { get; set; }
        public string SchoolGroupIds { get; set; }
    }
}
