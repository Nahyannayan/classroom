﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class EnrollActivities
    {
        public string AcademicYear { get; set; }
        public int StudentId { get; set; }
        public string EventName { get; set; }
        public string Grade { get; set; }
        public string Coupons { get; set; }
        public string EventSchedule { get; set; }
        public string Amount { get; set; }
        public string Comment { get; set; }
        public string EventFromDate { get; set; }
        public string EventToDate { get; set; }
        public string RegisterByDate { get; set; }
        public string Location { get; set; }
    }
}
