﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Phoenix.Models
{
    public class Course : Attachments
    {
        public long CourseId { get; set; }
        public long AcademicYearId { get; set; }
        public long SchoolId { get; set; }
        public long CurriculumId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string CourseType { get; set; }
        public long DepartmentId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long CreatedBy { get; set; }
        public int TotalCount { get; set; }
        public string CourseImageName { get; set; }
        public int MaximumEnrollment { get; set; }
        public double? CourseHours { get; set; }
        //public long[] Groupids { get; set; }
        //public long[] StudentId { get; set; }
        //public long[] TeacherId { get; set; } 
        public int ObservationId { get; set; }

    }
    public class CourseMapping
    {
        public CourseMapping()
        {
            Students = new List<CourseStudentAndGrade>();
            GroupTeachers = new List<GroupTeacher>();
            GroupGrades = new List<GroupGrade>();
            GroupAssignTeachers = new List<GroupAssignTeacherDuration>();
        }
        public long CourseId { get; set; }
        public string CourseTitle { get; set; }
        public long[] TeacherIds { get; set; }
        public string TeacherName { get; set; }
        public long SchoolGroupId { get; set; }
        public long[] GradeIds { get; set; }
        public string SchoolGroupName { get; set; }
        public int CourseType { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public IEnumerable<CourseStudentAndGrade> Students { get; set; }
        public IEnumerable<GroupTeacher> GroupTeachers { get; set; }
        public IEnumerable<GroupGrade> GroupGrades { get; set; }
        public IEnumerable<GroupAssignTeacherDuration> GroupAssignTeachers { get; set; }
        public int TotalCount { get; set; }
        public string EditUrl { get; set; }
        public string CourseImage { get; set; }
    }
    public class CourseStudentAndGrade
    {
        public long StudentId { get; set; }
        public long GradeId { get; set; }
        public long TeacherId { get; set; }
        public long SchoolId { get; set; }
        public string SchoolName { get; set; }
    }
    public class GroupGrade
    {
        public long SchoolId { get; set; }
        public long SchoolGroupId { get; set; }
        public long SchoolGradeId { get; set; }
        public string GradeDisplay { get; set; }
        public string SchoolName { get; set; }
    }
    public class GroupTeacher
    {
        public long SchoolGroupId { get; set; }
        public long TeacherId { get; set; }
        public string TeacherName { get; set; }
        public bool IsCurrentSchool { get; set; }
        public bool IsAdmin { get; set; }
    }

    public class SchoolWiseGrade
    {
        public string TeacherId { get; set; }
        public string SchoolId { get; set; }
        public string SchoolShortNameAndGradeDisplay { get; set; }
        public long SchoolGradeId { get; set; }
        public string SchoolGradeDisplay { get; set; }
    }
    public class CrossSchoolPermission
    {
        public CrossSchoolPermission()
        {
            SchoolIds = string.Empty;
            SchoolGradeIds = string.Empty;
        }
        public long TCS_ID { get; set; }
        public long TeacherId { get; set; }
        public long SchoolId { get; set; }
        public string TeacherName { get; set; }
        public string SchoolIds { get; set; }
        public string SchoolGradeIds { get; set; }
        public string SchoolName { get; set; }
        public long UserId { get; set; }
        public bool IsDelete { get; set; } = false;
        public DateTime? AssignFrom { get; set; }
        public DateTime? AssignTo { get; set; }
    }
    public class GroupAssignTeacherDuration : CrossSchoolPermission
    {
        public bool IsCoreTeacher { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsUserFromCurrentSchool { get; set; }
    }
}
