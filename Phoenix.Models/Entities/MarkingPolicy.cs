﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class MarkingPolicy
    {     
        public Int64 MarkingPolicyId { get; set; }
        public string Symbol { get; set; }
        public string Description { get; set; }
        public Decimal Weight { get; set; }
        public string ColorCode { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public string SchoolId { get; set; }
     
    }
}
