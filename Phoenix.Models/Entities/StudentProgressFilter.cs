﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Phoenix.Models
{
    public class AllFilters
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class FileStudent
    {
        /// <summary>
        /// Id of Student.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Name of Student.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Photo of Student.
        /// </summary>
        public string PhotoPath { get; set; }
    }

    public class StudentCountDonutChartViewModel
    {
        public string category { get; set; }
        public double value { get; set; }
    }

    public class FilterRes
    {
        public FilterRes()
        {
            this.AcademicYear = new List<AllFilters>();
            this.Class = new List<AllFilters>();
            this.Subject = new List<AllFilters>();
            this.Assessment = new List<AllFilters>();
            this.Teacher = new List<AllFilters>();
        }

        /// <summary>
        /// Academic year filter for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AllFilters> AcademicYear { get; set; }

        /// <summary>
        /// Class filter for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AllFilters> Class { get; set; }

        /// <summary>
        /// Subject filter for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AllFilters> Subject { get; set; }

        /// <summary>
        /// Assessment filter for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AllFilters> Assessment { get; set; }

        /// <summary>
        /// Teacher filter for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AllFilters> Teacher { get; set; }
        
    }

    public class StudentProgressFilter
    {
        public StudentProgressFilter()
        {
            this.AcademicYear = new SelectList(Enumerable.Empty<SelectListItem>());
            this.Class = new SelectList(Enumerable.Empty<SelectListItem>());
            this.Subject = new SelectList(Enumerable.Empty<SelectListItem>());
            this.Assessment = new SelectList(Enumerable.Empty<SelectListItem>());
            this.Teacher = new SelectList(Enumerable.Empty<SelectListItem>());
        }

        /// <summary>
        /// Academic year filter for StudentProgressTracker Dashboard.
        /// </summary>
        public SelectList AcademicYear { get; set; }

        /// <summary>
        /// Class filter for StudentProgressTracker Dashboard.
        /// </summary>
        public SelectList Class { get; set; }

        /// <summary>
        /// Subject filter for StudentProgressTracker Dashboard.
        /// </summary>
        public SelectList Subject { get; set; }

        /// <summary>
        /// Assessment filter for StudentProgressTracker Dashboard.
        /// </summary>
        public SelectList Assessment { get; set; }

        /// <summary>
        /// Teacher filter for StudentProgressTracker Dashboard.
        /// </summary>
        public SelectList Teacher { get; set; }
    }

    public class ChartRes
    {
        public ChartRes()
        {
            this.AvgPredFig = new AveragePredictionFigures();
            this.AvgAssPredByYear = new List<AverageAssessmentPredictionByYear>();
            this.AvgMarksByYearAndAss = new List<AverageAssessmentPredictionByYear>();
            this.PredBuck = new List<PredictionBucket>();
            this.PredBySub = new List<PredictionBySubject>();
        }

        /// <summary>
        /// Average and prediction figures for StudentProgressTracker Dashboard.
        /// </summary>
        public AveragePredictionFigures AvgPredFig { get; set; }

        /// <summary>
        /// Average assessment and prediction figures by year for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AverageAssessmentPredictionByYear> AvgAssPredByYear { get; set; }

        /// <summary>
        /// Average marks by year and assessment for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<AverageAssessmentPredictionByYear> AvgMarksByYearAndAss { get; set; }

        /// <summary>
        /// Prediction bucket for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<PredictionBucket> PredBuck { get; set; }

        /// <summary>
        /// Prediction by subject for StudentProgressTracker Dashboard.
        /// </summary>
        public IEnumerable<PredictionBySubject> PredBySub { get; set; }
    }

    #region Average Prediction Figures

    /// <summary>
    /// Model class for average and prediction figures of StudentProgressTracker dashboard.
    /// </summary>
    public class AveragePredictionFigures
    {
        /// <summary>
        /// Overall average score.
        /// </summary>
        public decimal OverallAverage { get; set; }

        /// <summary>
        /// Prediction average score.
        /// </summary>
        public decimal PredictionAverage { get; set; }

        /// <summary>
        /// Overall average score for boys.
        /// </summary>
        public decimal OverallAverageBoys { get; set; }

        /// <summary>
        /// Prediction average score for boys.
        /// </summary>
        public decimal PredictionAverageBoys { get; set; }

        /// <summary>
        /// Overall average score for boys.
        /// </summary>
        public decimal OverallAverageGirls { get; set; }

        /// <summary>
        /// Prediction average score for boys.
        /// </summary>
        public decimal PredictionAverageGirls { get; set; }
    }

    #endregion Average Prediction Figures

    #region Average Assessment Prediction By Year

    /// <summary>
    /// Model class for average assessment and prediction by year chart of StudentProgressTracker dashboard.
    /// </summary>
    public class AverageAssessmentPredictionByYear
    {
        /// <summary>
        /// Academic Year.
        /// </summary>
        public string AcademicYear { get; set; }

        /// <summary>
        /// Average percentage score of category.
        /// </summary>
        public decimal AvgPercentage { get; set; }

        /// <summary>
        /// Category of chart.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Assessment of chart.
        /// </summary>
        public string Assessment { get; set; }

        /// <summary>
        /// Assessment Date of chart.
        /// </summary>
        public DateTime? AssessmentDate { get; set; }
        /// <summary>
        /// Interval for chart Y-axis values.
        /// </summary>
        public int Interval { get; set; }
    }

    #endregion Average Assessment Prediction By Year

    #region Prediction Bucket

    /// <summary>
    /// Model class for prediction bucket chart of StudentProgressTracker dashboard.
    /// </summary>
    public class PredictionBucket
    {
        /// <summary>
        /// Percentage Range.
        /// </summary>
        public string PRange { get; set; }

        /// <summary>
        /// Count of students.
        /// </summary>
        public int StudentCount { get; set; }
    }

    #endregion Prediction Bucket

    #region Prediction By Subject

    /// <summary>
    /// Model class for prediction by subject chart of StudentProgressTracker dashboard.
    /// </summary>
    public class PredictionBySubject
    {
        /// <summary>
        /// Subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Count of students.
        /// </summary>
        public int StudentCount { get; set; }
    }

    #endregion Prediction By Subject
}
