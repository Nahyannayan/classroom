﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Country
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryNationality { get; set; }
        public string CountryCode { get; set; }
        public string CountryGroup { get; set; }
    }
}
