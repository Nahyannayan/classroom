﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class VLEFileType
    {
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public string Extension { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public bool IsActive { get; set; }
        public string DocumentType { get; set; }
    }
}
