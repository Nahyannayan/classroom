﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class City
    {
        public int CityId { get; set; }

        public string CityName { get; set; }

        public string CityCode { get; set; }

        public string CityIATACode { get; set; }

        public string CountryID { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
