﻿using System;

namespace Phoenix.Models
{
    public class AccountingYear
    {
        public int AccountingYearID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }
}
