﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class PlanSchemeField
    {
        public long PlanSchemeDetailFieldId { get; set; }
        public long PlanSchemeId { get; set; }
        public string FieldId { get; set; }
        public string FieldValue { get; set; }
    }
}
