﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class StudentParents
    {
        public long ParentId { get; set; }
        public long SiblingId { get; set; }
        public string FFullName { get; set; }
        public string FMobile { get; set; }
        public string FEmail { get; set; }
        public string FPhoto { get; set; }
        public string MFullName { get; set; }
        public string MMobile { get; set; }
        public string MEmail { get; set; }
        public string MPhoto { get; set; }
    }
}
