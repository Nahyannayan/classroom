﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ProfileFilterVM
    {
        public int UserType { get; set; }
        public int CountryId { get; set; }
        public int CityId { get; set; }
        public int BusinessUnitId { get; set; }
        public int GradeId { get; set; }
        public string Designation { get; set; }
        public long UserId { get; set; }
        public string searchText { get; set; }
    }
}
