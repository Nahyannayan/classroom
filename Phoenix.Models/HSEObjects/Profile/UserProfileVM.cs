﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class UserProfileVM
    {
        public int UserType { get; set; }
        public long UserId { get; set; }
        public string FullName { get; set; }
        public string School { get; set; }
        public string Class { get; set; }
        public long SiblingId { get; set; }
        public string Siblings { get; set; }
        public string PhotoPath { get; set; }
        public string Designation { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
