﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class GCCode
    {
        public GCCode() {}
        public int ItemId { get; set; }
        public string GroupName { get; set; }
        public string ItemName { get; set; }
        public string CustomValue { get; set; }
        public bool IsActive { get; set; }
    }
}
