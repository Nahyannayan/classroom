﻿
namespace Phoenix.Models
{
    public class UserLocationMap
    {
        public long UserId {get;set;}
        public string UserName { get; set; }
        public long CountryId {get;set;}
        public string CountryName { get; set; }
        public long CityId {get;set;}
        public string CityName { get; set; }
        public long BusinessUnitld { get; set;}
        public string BusinessUnitName {get;set;}
        public string BusinessUnitShortName { get; set; }
    }
}
