﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class InvgCause
    {
        public InvgCause() { }

        public InvgCause(int id)
        {
            InvestigationID = id;
        }
        public int InvestigationID { get; set; }

        public int CauseID { get; set; }

        public int TaskId { get; set; }

        public string CauseName { get; set; }

        public string CauseRemarks { get; set; }

        public long LastModifiedBy { get; set; }

        public DateTime LastModifiedOn { get; set; }
    }
}