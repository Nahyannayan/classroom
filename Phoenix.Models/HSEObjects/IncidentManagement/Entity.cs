﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Entity
    {
        public Entity()
        {
        }

        public Entity(long _entityId)
        {
            EntityId = _entityId;
        }

        public long EntityId { get; set; }

        public int IncidentId { get; set; }

        public int EntityTypeId { get; set; }

        public long EntityPrimaryId { get; set; }

        public string Name { get; set; }

        public string ContactNo { get; set; }

        public string Gender { get; set; }

        public string Class { get; set; }

        public string Occupation { get; set; }

        public string CntrCompanyName { get; set; }

        public int Age { get; set; }

        public string TypeName { get; set; }

        public string SchoolBusinessUnitName { get; set; }

        public bool IsPersonInvolved { get; set; }

        public bool IsInjuryAdded { get; set; }

        public bool IsTreatmentAdded { get; set; }
        public EntityTreatment EntityTreatment { get; set; }
        public dynamic EntityInjury { get; set; }
    }
}
