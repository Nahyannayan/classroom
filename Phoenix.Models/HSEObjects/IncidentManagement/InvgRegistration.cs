﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace Phoenix.Models
{
    public class InvgRegistration
    {
        public InvgRegistration() { }

        public InvgRegistration(int id)
        {
            InvestigationID = id;
        }
        public int InvestigationID { get; set; }
        public int IncidentId { get; set; }
        public string IncidentSummary { get; set; }
        public string EvidenceSummary { get; set; }
        public bool IsSubmitted { get; set; }
        public long SubmittedBy { get; set; }
        public long ApprovedBy { get; set; }
        public string ApproverRemarks { get; set; }
        public long LastModifiedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public DateTime? SubmittedOn { get; set; }
        public DateTime? DueDate { get; set; }
        public bool IsSafeGuarding { get; set; }
        public bool IsApproved { get; set; }
        public DateTime? ApprovedOn { get; set; }
        public bool IsAssingedUser { get; set; }
        public List<InvgCauseCheckBoxList> UnSafeActCauses { get; set; }
        public List<InvgCauseCheckBoxList> UnSafeConditionCauses { get; set; }
        public List<InvgCauseCheckBoxList> SystemDeficinceCauses { get; set; }
        public List<InvgRootCause> RootCauses { get; set; }
    }

    public class InvgCauseCheckBoxList
    {
        public int CauseId { get; set; }
        public string CauseName { get; set; }
        public int TaskId { get; set; }
        public bool IsSelected { get; set; }
    }
}














