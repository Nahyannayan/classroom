﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class CauseOfIncident
    {
        public long ID { get; set; }
        public long IncidentId { get; set; }
        public string ImmediateCauses { get; set; }
        public string RootCauses { get; set; }
        public long LastModifiedByUserId { get; set; }
        
    }
}
