﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Assailant
    {
        public Assailant() { }

        public Assailant(long _incidentAssailantId) {
            IncidentAssailantId = _incidentAssailantId;
        }

        public long IncidentAssailantId { get; set; }

        public int IncidentId { get; set; }

        public bool IsAssailantInternal { get; set; }

        public long AssailantPrimarytId { get; set; }

        public int AssailantEntityTypeId { get; set; }

        public string AssailantName { get; set; }

        public string AssailantGender { get; set; }

        public string AssailantContact { get; set; }

        public string AssailantAddress { get; set; }

        public string OtherDetails { get; set; }

        public string AssailantEmail { get; set; }

        public bool IsAddMode { get; set; }

        public long VerbalWarningBy { get; set; }
        public long WrittenWarningBy { get; set; }
        public bool Retraining { get; set; }
        public bool Monitoring { get; set; }
    }
}
