﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class InvgCauseTaskMap
    {
        public InvgCauseTaskMap() { }

        public InvgCauseTaskMap(int _causeId, int _invgId)
        {
            CauseID = _causeId;
            InvestigationID = _invgId;
        }

        public int CauseID { get; set; }

        public int TaskID { get; set; }

        public int InvestigationID { get; set; }

        public long LastModifiedBy { get; set; }

        public DateTime LastModifiedBOn { get; set; }
    }
}
