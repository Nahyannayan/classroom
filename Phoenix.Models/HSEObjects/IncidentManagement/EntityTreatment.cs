﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class EntityTreatment
    {
        public EntityTreatment() { }

        public EntityTreatment(int _treatmentId)
        {
            TreatmentId = _treatmentId;
        }

        public int TreatmentId { get; set; }
        public int EntityId { get; set; }
        public int IncidentId { get; set; }
        public bool WasMedicalAttentionSought { get; set; }
        public string TakenToHospitalDoctor { get; set; }
        public string CheifComplaint { get; set; }
        public string Observation { get; set; }
        public string Treatment { get; set; }
        public bool ParentalContacted { get; set; }
        public string TreatedBy { get; set; }
        public string TreaterName { get; set; }
        public string Prescription { get; set; }
        public bool IsAmbulanceCalled { get; set; }
        public string CalledBy { get; set; }
        public DateTime? CalledTime { get; set; }
        public string AttatchFile { get; set; }
        public bool IsLTA { get; set; }
        public int DaysLost { get; set; }
    }
}
