﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class InvestigationInvestigator
    {
        public long Id { get; set; }
        public long RegistrationID { get; set; }
        public long UserID { get; set; }
        public List<string> SelectedInvestigatorList { get; set; }
        public string InvestigatorName { get; set; }
    }
}
