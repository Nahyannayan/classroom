﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class EntityInjury
    {
        public EntityInjury() { }

        public EntityInjury(int _entityInjuryId)
        {
            EntityInjuryId = _entityInjuryId;
        }

        public int EntityInjuryId { get; set; }
        public int EntityId { get; set; }
        public string Gender { get; set; }
        public int IncidentID { get; set; }
        public bool Head { get; set; }
        public bool Face { get; set; }
        public bool BackOfHead { get; set; }
        public bool RightShoulder { get; set; }
        public bool RightUpperArm { get; set; }
        public bool RightForeArm { get; set; }
        public bool RightHand { get; set; }
        public bool RightWrist { get; set; }
        public bool Chest { get; set; }
        public bool Stomach { get; set; }
        public bool Groin { get; set; }
        public bool RightThigh { get; set; }
        public bool RightKnee { get; set; }
        public bool RightLowerLeg { get; set; }
        public bool RightAnkel { get; set; }
        public bool RightFoot { get; set; }
        public bool LeftShoulder { get; set; }
        public bool LeftUpperArm { get; set; }
        public bool LeftForeArm { get; set; }
        public bool LeftHand { get; set; }
        public bool LeftWrist { get; set; }
        public bool UpperBack { get; set; }
        public bool MidToLowerBack { get; set; }
        public bool Waist { get; set; }
        public bool LeftThigh { get; set; }
        public bool LeftKnee { get; set; }
        public bool LeftLowerLeg { get; set; }
        public bool LeftAnkel { get; set; }
        public bool LeftFoot { get; set; }
        public bool RHIndexFinger { get; set; }
        public bool RHThumb { get; set; }
        public bool RHMiddleFinger { get; set; }
        public bool RHRingFinger { get; set; }
        public bool RHLittleFinger { get; set; }
        public bool RHPalm { get; set; }
        public bool RHBackOfHand { get; set; }
        public bool LHIndexFinger { get; set; }
        public bool LHThumb { get; set; }
        public bool LHMiddleFinger { get; set; }
        public bool LHRingFinger { get; set; }
        public bool LHLittleFinger { get; set; }
        public bool LHPalm { get; set; }
        public bool LHBackOfHand { get; set; }
        public bool RLLargeToe { get; set; }
        public bool RLToe2 { get; set; }
        public bool RLMiddleToe { get; set; }
        public bool RLToe4 { get; set; }
        public bool RLLittleToe { get; set; }
        public bool RLSole { get; set; }
        public bool RLTopFoot { get; set; }
        public bool RLHeel { get; set; }
        public bool LLLargeToe { get; set; }
        public bool LLToe2 { get; set; }
        public bool LLMiddleToe { get; set; }
        public bool LLToe4 { get; set; }
        public bool LLLittleToe { get; set; }
        public bool LLSole { get; set; }
        public bool LLTopFoot { get; set; }
        public bool LLHeel { get; set; }
        public bool ForeHead { get; set; }
        public bool RightEye { get; set; }
        public bool RightEar { get; set; }
        public bool RightCheek { get; set; }
        public bool Teeth { get; set; }
        public bool InnerMouth { get; set; }
        public bool Chin { get; set; }
        public bool LeftEye { get; set; }
        public bool LeftEar { get; set; }
        public bool LeftCheek { get; set; }
        public bool Nose { get; set; }
        public bool OuterMouth { get; set; }
        public bool Jaw { get; set; }
        public string InjuryDescription { get; set; }
    }
}
