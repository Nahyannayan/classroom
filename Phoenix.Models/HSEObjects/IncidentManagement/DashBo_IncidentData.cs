﻿using System;
using System.Collections.Generic;

namespace Phoenix.Models
{
    public class DashBo_IncidentData
    {
        public IncidentHeading heading { get; set; }
        public List<IncidentChart> chart { get; set; }
        public List<IncidentEntity> list { get; set; }
    }

    public class IncidentEntity
    {
        public long IncidentId { get; set; }
        public string School { get; set; }
        public string Category { get; set; }
        public string Severity { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Color { get; set; }
        public bool IsSubmit { get; set; }
        public string EncryptedId { get; set; }
    }

    public class IncidentHeading
    {
        public long Total { get; set; }
        public long Open { get; set; }
        public long Closed { get; set; }
    }

    public class IncidentChart
    {
        public string Category { get; set; }
        public long Incidents { get; set; }
        public string Color { get; set; }
    }
}
