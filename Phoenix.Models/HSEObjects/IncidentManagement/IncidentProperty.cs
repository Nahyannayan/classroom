﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class IncidentProperty
    {
        public IncidentProperty() { }

        public IncidentProperty(int _incidentPropertyId) {
            IncidentPropertyId = _incidentPropertyId;
        }

        public int IncidentPropertyId { get; set; }

        public int IncidentId { get; set; }

        public string Name { get; set; }

        public string PhoneNo { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }

        public bool IsEquipmentImpacted { get; set; }

        public bool StructuralDamageProperty { get; set; }

        public bool FurnishingDamageProperty { get; set; }

        public decimal EstimatedPropertyDamage { get; set; }

        public string OtherDetails { get; set; }

        public string ImageUpload { get; set; }

        public bool IsAddMode { get; set; }
    }
}

