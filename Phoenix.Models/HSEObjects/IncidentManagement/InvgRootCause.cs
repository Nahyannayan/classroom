﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class InvgRootCause
    {
        public long InvestigationID { get; set; }
        public int CauseID { get; set; }
        public string CauseType { get; set; }
        public int TaskId { get; set; }
        public string Title { get; set; }
        public string Recommendation { get; set; }
        public string Actions { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? EstimatedTime { get; set; }
        public string Priority { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime? CompletedDate { get; set; }
        public string DocumentIds { get; set; }
        public int IsDue { get; set; }
        public int IncidentId { get; set; }
        public string AssignTo { get; set; }
        public bool IsAssingedUser { get; set; }
        public bool IsSubmitted { get; set; }
    }
}
