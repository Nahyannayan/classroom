﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class UserActivityLogList
    {
        public UserActivityLogList()
        {
            //Registrations = new IEnumerable<Registration>();
        }
        public int NoOfPages { get; set; }

        public IEnumerable<UserActivityLog> UserActivityLog { get; set; }
    }
}
