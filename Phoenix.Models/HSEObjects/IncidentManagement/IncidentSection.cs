﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class IncidentSection
    {
        public IncidentSection() { }

        public IncidentSection(int sectiondId)
        {
            SectionId = sectiondId;
        }

        public int SectionId { get; set; }

        public string SectionName { get; set; }

        public string SectionDetails { get; set; }

        public bool IsActive { get; set; }

        public string SectionLocalizeXml { set; get; }
    }
}
