﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class UserActivityLogFilter
    {
        public int RegistrationID { get; set; }
        public DateTime? EventStartDateTime { get; set; }
        public DateTime? EventEndDateTime { get; set; }
        public int EventByUserID { get; set; }
        public string EventType { get; set; }
        public string ModuleName { get; set; }
    }
}
