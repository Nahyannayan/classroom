﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Registration
    {
        public Registration()
        {
            CategoryList = new HashSet<CategoryCheckBoxList>();
            SubCategoryList = new HashSet<SubCategoryCheckBoxList>();
        }

        public Registration(int _IncidentId)
        {
            IncidentId = _IncidentId;
        }


        public int IncidentId { get; set; }

        public string EncryptedId { get; set; }

        public int CountryId { get; set; }

        public string Corporate { get; set; }

        public int CityId { get; set; }

        public string HeadOffice { get; set; }

        public int BusinessUnitId { get; set; }

        public string School { get; set; }

        public int SubBusinessUnitId { get; set; }

        public string SubBusinessUnitName { get; set; }

        public int SectionId { get; set; }

        public string SectionName { get; set; }

        public int IncidentSeverityId { get; set; }

        public string Severity { get; set; }

        public string IncidentRegNo { get; set; }

        public DateTime IncidentDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public string IncidentDetails { get; set; }

        public bool IsReporterInternal { get; set; }

        public int ReportedById { get; set; }

        public long InternalReporterId { get; set; }

        public string ReporterName { get; set; }

        public string RepoterGender { get; set; }

        public string ReporterEmailAddress { get; set; }

        public string ReporterContact { get; set; }

        public string ReporterAddress1 { get; set; }

        public string ReporterAddress2 { get; set; }

        public string ReporterOccupation { get; set; }

        public bool IsPersonInvolved { get; set; }

        public bool IsConfidential { get; set; }

        public bool IsReminder { get; set; }

        public bool IsPopertyDamage { get; set; }

        public bool IsVehicleInvolve { get; set; }

        public bool IsAssailantInvole { get; set; }

        public bool IsAnyWitness { get; set; }

        public bool IsAgencyInvolved { get; set; }

        public bool IsIncidentClosed { get; set; }

        public string Category { get; set; }

        public string CategoryIds { get; set; }

        public string SubCategory { get; set; }

        public string SubCategoryIds { get; set; }

        public string InjuredPersons { get; set; }

        public string ReportedBy { get; set; }

        public string DocumentIds { get; set; }

        public int IsSubmit { get; set; }

        public DateTime? SubmitedDate { get; set; }

        public DateTime? EstCloseDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public bool IsInvestigationReq { get; set; }

        public bool UpdateCategory { get; set; }

        public Vehicle Vehicle { get; set; }

        public IncidentProperty Property { get; set; }

        public string IncidentDateDay { get; set; }

        public int IncidentDateHour { get; set; }

        public int InvestigationId { get; set; }

        public bool IsSafeGuarding { get; set; }

        public bool IsInvgSubmitted { get; set; }

        public long CreatedBy { get; set; }

        public long LastModifiedBy { get; set; }

        public string IncidentStatus { get; set; }

        public InvgRegistration InvgRegistration { get; set; }

        public IEnumerable<CategoryCheckBoxList> CategoryList { get; set; }

        public IEnumerable<SubCategoryCheckBoxList> SubCategoryList { get; set; }

        public long ClosedById { get; set; }

        public string ClosedBy { get; set; }
        public int InvestigationOutcomeId { get; set; }
        public string InvestigationOutcome { get; set; }
        public bool AddedToBarredList { get; set; }
        public bool IsInvestigationOutcomeReq { get; set; }
        public bool IsApproved { get; set; }
        public long ApprovedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string ApprovedByName { get; set; }
        public bool IsDraftNotification { get; set; }
        public bool IsDraftDelete { get; set; }
    }

    public class CategoryCheckBoxList
    {
        public CategoryCheckBoxList()
        {
            SubCategories= new HashSet<SubCategoryCheckBoxList>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public bool IsSelected { get; set; }
        public int Sequence { get; set; }
        public IEnumerable<SubCategoryCheckBoxList> SubCategories { get; set; }
    }

    public class SubCategoryCheckBoxList
    {
        public int SubCategoryId { get; set; }
        public int CategoryId { get; set; }
        public bool IsSelected { get; set; }
        public int Sequence { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
       
    }

}
