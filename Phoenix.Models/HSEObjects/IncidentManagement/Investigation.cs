﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Investigation
    {
        public int InvestigationId { get; set; }

        public long IncidentId { get; set; }
        
        public string ActionTakenDate { get; set; }
        public string ActionTakenTime { get; set; }

        public string ActionType { get; set; }
        public string ActionDescription { get; set; }

        public int AgencyId { get; set; }

        public long LastModifiedByUserId { get; set; }

        public DateTime LastModifiedOn { get; set; }

        public string Documents { get; set; }

        public string DeletedDocuments { get; set; }
        public string InvestigatorName { get; set; }

        public int InvgRegistrationId { get; set; }

        //Add Investigator

        public long RegistrationID { get; set; }
        public long UserID { get; set; }
    }
}
