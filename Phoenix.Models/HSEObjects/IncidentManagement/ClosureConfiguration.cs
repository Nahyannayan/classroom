﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ClosureConfiguration
    {
        public ClosureConfiguration() { }

        public ClosureConfiguration(int _clouserConfId)
        {
            ClosureConfId = _clouserConfId;
        }

        public int ClosureConfId { get; set; }

        public int CategoryId { get; set; }

        public string Category { get; set; }

        public int SeverityId { get; set; }

        public string Severity { get; set; }

        public int PeriodInDays { get; set; }

        public bool IsInvestigationReq { get; set; }

        public int SubCategoryId { get; set; }
        public bool IsInvestigationOutcomeReq { get; set; }
    }
}
