﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    /// <summary>
    /// Author : Athar Shaikh
    /// CreatedAt : 30-04-2019
    /// </summary>
    public class Vehicle
    {
        public Vehicle() { }

        public Vehicle(int _incidentVehicleId) {
            IncidentVehicleID = _incidentVehicleId;
        }
        public int IncidentVehicleID { get; set; }

        public int IncidentId { get; set; }

        public bool IsInternalVehicle { get; set; }

        public string MakeOfVehicle { get; set; }

        public string RegistrationNo { get; set; }

        public string VehicleInformation { get; set; }

        public string DriverName { get; set; }

        public string DriverContact { get; set; }

        public string DriverEmail { get; set; }

        public string DriverLicenseNo { get; set; }

        public string DriverExtraInfo { get; set; }

        public string OwnerName { get; set; }

        public string OwnerContact { get; set; }

        public string OwnerEmail { get; set; }

        public string OwnerLicenseNo { get; set; }

        public string OwnerExtraInfo { get; set; }

        public string VehicleImage { get; set; }

        public int VehiclePrimaryId { get; set; }

        public string IncidentRegNo { get; set; }

        public bool IsAddMode { get; set; }
    }
}
