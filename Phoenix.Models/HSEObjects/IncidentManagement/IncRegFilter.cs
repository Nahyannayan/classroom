﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class IncRegFilter
    {
        public long UserId { get; set; }

        public List<int> LocationIds { get; set; }

        public List<int> CategoryIds { get; set; }

        public List<int> SubCategoryIds { get; set; }

        public List<int> SeverityIds { get; set; }

        public List<int> InvestigationFilter { get; set; }

        public List<int> InjuredPersonTypes { get; set; }

        public int SortBy { get; set; }

        public int PageNo { get; set; }

        public int NoOfRecords { get; set; }

        public int CountryId { get; set; }

        public int CityId { get; set; }

        public string SearchText { get; set; }

        public long EntityPrimaryId { get; set; }

        public string IncidentStatus { get; set; }

        public List<int> Pages { get; set; }

    }

    public class PersonsCheckBoxList
    {
        public int PersonId { get; set; }

        public int EntityTypeId { get; set; }

        public string Name { get; set; }

        public string Details { get; set; }

        public bool IsSelected { get; set; }
    }
}
