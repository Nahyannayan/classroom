﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class InvgCauseMaster
    {
        public InvgCauseMaster() { }

        public InvgCauseMaster(int id)
        {
            CauseID = id;
        }
        public int CauseID { get; set; }

        public string CauseType { get; set; }

        public string CauseName { get; set; }

        public string CauseDescription { get; set; }

        public long LastModifiedBy { get; set; }

        public DateTime LastModifiedOn { get; set; }
    }
}
