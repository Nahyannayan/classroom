﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    /// <summary>
    /// Author : Athar Shaikh
    /// CreatedAt : 29-04-2019
    /// </summary>
    public class Witness
    {
        public Witness() { }

        public Witness(long _incidentWitnessId)
        {
            IncidentWitnessId = _incidentWitnessId;
        }
        public long IncidentWitnessId { get; set; }

        public int IncidentID { get; set; }

        public bool IsWitnessInterviewed { get; set; }

        public bool IsWitnessInternal { get; set; }

        public long WitnessPrimaryId { get; set; }

        public string WitnessName { get; set; }

        public string WitnessGender { get; set; }

        public string WitnessAddress { get; set; }

        public string WitnessContact { get; set; }

        public string WitnessEmail { get; set; }

        public string WitnessOccupation { get; set; }

        public string OtherDetails { get; set; }

        public bool IsAddMode { get; set; }
    }
}
