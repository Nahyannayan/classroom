﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Incidents
    {
        public Incidents()
        {
            Registrations = new List<Registration>();
        }
        public int NoOfPages { get; set; }

        public int TotalNumberOfRecords { get; set; }

        public List<Registration> Registrations { get; set; }
    }
}
