﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditMatrix
    {
        public int AuditMatrixID { get; set; }

        public int NotificationProfileID { get; set; }

        public int AuditID { get; set; }

        public string AuditName { get; set; }

        public int BusinessUnitId { get; set; }

        public string BusinessUnitName { get; set; }

        public int RoleID { get; set; }

        public string UserRoleName { get; set; }

        public bool OnAuditMapToLocation { get; set; }

        public bool OnAuditCompletion { get; set; }

        public bool OnAuditFail { get; set; }

        public bool IsDeleted { get; set; }
        public bool OnAuditReminder { get; set; }
        public bool OnAuditApprove { get; set; }
        public bool ForApproval { get; set; }
        public bool OnAuditReject  { get; set; }


    }
}
