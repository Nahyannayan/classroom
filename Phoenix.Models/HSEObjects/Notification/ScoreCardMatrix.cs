﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ScoreCardMatrix
    {
        public int ScoreCardMatrixID { get; set; }

        public int NotificationProfileID { get; set; }

        public int BusinessUnitId { get; set; }

        public string BusinessUnitName { get; set; }

        public int TargetProfileID { get; set; }

        public int AccountingYearID { get; set; }

        public string AccountingYear { get; set; }

        public int RoleID { get; set; }

        public string UserRoleName { get; set; }

        public bool OnMapToLocation { get; set; }

        public bool OnCompletion { get; set; }

        public bool AfterDue { get; set; }

        public bool IsDeleted { get; set; }

        public bool OnScoreCardReminder { get; set; }
        public bool OnApproved { get; set; }


    }
}
