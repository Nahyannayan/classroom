﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class IncidentMatrix
    {
        public int IncidentMatrixID { get; set; }
        public int NotificationProfileID { get; set; }
        public int IncidentCategoryID { get; set; }
        public string CategoryName { get; set; }
        public int InicidentSeverityID { get; set; }
        public string SeverityName { get; set; }
        public int BusinessUnitId { get; set; }
        public string BusinessUnitName { get; set; }
        public int RoleID { get; set; }
        public string UserRoleName { get; set; }
        public bool IsAddMode { get; set; }
    }
}
