﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class IncidentNotificationUser
    {
        public int NotificationProfileID { get; set; }
        public int IncidentMatrixID { get; set; }
        public int UserID { get; set; }
        public bool IsInclude { get; set; }
    }
}
