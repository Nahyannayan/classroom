﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class NotificationProfile
    {
        public int NotificationProfileID { get; set; }
        public string Module { get; set; }
        public string ProfileName { get; set; }
        public string ProfileDescription { get; set; }
        public bool IsEmailNotification { get; set; }
        public bool IsWebNotification { get; set; }
        public bool IsSMSNotification { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public bool IsAddMode { get; set; }
        public string NotificationProfileLocalizeXml { get; set; }
    }
}
