﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{

    public class EventsView
    {
        public EventsView()
        {
            auditEventsList = new List<AuditEvents>();
            taskEventsList = new List<TaskEvents>();
            scorecardEventsList = new List<ScorecardEvents>();
        }
        public List<AuditEvents> auditEventsList { get; set; }
        public List<TaskEvents> taskEventsList { get; set; }
        public List<ScorecardEvents> scorecardEventsList { get; set; }
    }
}
