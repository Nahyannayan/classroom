﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ReminderRegistration
    {
        public ReminderRegistration() { }

        public ReminderRegistration(int _reminderID)
        {
            ReminderID = _reminderID;
        }
        public int ReminderID { get; set; }

        public string ProfileName { get; set; }

        public string ReminderName { get; set; }

        public string ReminderDetails { get; set; }

        public bool IsBeforeDueDate { get; set; }

        public int BeforeDueDays { get; set; }

        public bool IsEveryDayBeforeDueDate { get; set; }

        public bool IsAfterDueDate { get; set; }

        public int AfterDueDays { get; set; }

        public bool IsEveryDayTillCompletion { get; set; }

        public bool DisableReminder { get; set; }

        public int LastModifiedBy { get; set; }

        public DateTime LastModifiedOn { get; set; }

        public string RegistrationLocalizeXml { get; set; }
    }
}
