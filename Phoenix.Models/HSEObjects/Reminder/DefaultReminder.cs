﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class DefaultReminder
    {
        public int Id { get; set; }

        public string ReminderType { get; set; }
    }
}
