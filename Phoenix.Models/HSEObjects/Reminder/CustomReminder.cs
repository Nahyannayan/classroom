﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class CustomReminder
    {
        public CustomReminder() { }

        public CustomReminder(int _customReminderID) {
            CustomReminderID = _customReminderID;
        }
        public int CustomReminderID { get; set; }

        public bool IsBeforeDueDate { get; set; }

        public int BeforeDueDays { get; set; }

        public bool IsEveryDayBeforeDueDate { get; set; }

        public bool IsAfterDueDate { get; set; }

        public int AfterDueDays { get; set; }

        public bool IsEveryDayTillCompletion { get; set; }

        public bool DisableReminder { get; set; }

        public int LastModifiedBy { get; set; }

        public DateTime LastModifiedOn { get; set; }

        public bool IsAddMode { get; set; }

        public string ProfileType { get; set; }

        public int ProfileId { get; set; }

        public bool IsBeforeStartDate { get; set; }

        public int BeforeStartDays { get; set; }
    }
}
