﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ScorecardEvents
    {
        public int Id { get; set; }
        public int TargetProfileId { get; set; }
        public int MonthActualId { get; set; }
        public long BusinessUnitId { get; set; }
        public DateTime ScheduledDate { get; set; }
        public int Status { get; set; }
        public bool IsNotified { get; set; }
        public string NotifiedTo { get; set; }
    }
}
