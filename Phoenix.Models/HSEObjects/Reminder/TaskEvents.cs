﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class TaskEvents
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public long UserId { get; set; }
        public DateTime ScheduledDate { get; set; }
        public int Status { get; set; }
        public bool IsNotified { get; set; }
        public string NotifiedTo { get; set; }
    }
}
