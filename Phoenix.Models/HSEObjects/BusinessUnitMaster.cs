﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class BusinessUnitMaster
    {
        public int BusinessUnitId { get; set; }
      
        public string BusinessUnitName { get; set; }

        public string BusinessUnitShortName { get; set; }

        public Boolean IsActive { get; set; }

        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }

        public string BusinessUnitEmail { get; set; }

        public string StreetLine1 { get; set; }

        public string StreetLine2 { get; set; }

        public string TownName { get; set; }

        public string PostCode { get; set; }

        public string Telepone { get; set; }

        public string Website { get; set; }

        public string PrincipleName { get; set; }

        public string PrimaryContactFirstName { get; set; }

        public string PrimaryContactLastName { get; set; }

        public DateTime CreatedOn { get; set; }

        public int CreatedBy { get; set; }

        public int BusinessUnitTypeID { get; set; }
        public string BusinessType { get; set; }
        public Boolean IsGEMSBU { get; set; }
        public Boolean IsAddMode { get; set; }
        public Boolean IsEdited { get; set; }
        public int UserId { get; set; }
        public string BusinessUnitLocalizeXml { get; set; }
    }
}
