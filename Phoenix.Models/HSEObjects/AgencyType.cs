﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Models
{
    public class AgencyType
    {
        public int AgencyTypeId { get; set; }

        public string AgencyName { get; set; }

        public int Sequence { get; set; }
    }
}
