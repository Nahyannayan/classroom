﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class DashboardData
    {
        public DashBo_IncidentData incidentData { get; set; }
        public DashBo_InspectionData inspectionData { get; set; }
        public DashBo_TaskData taskData { get; set; }
    }
}
