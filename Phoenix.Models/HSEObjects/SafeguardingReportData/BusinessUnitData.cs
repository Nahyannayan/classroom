﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.HSEObjects.SafeguardingReportData
{
    public class BusinessUnitData
    {
        public long BusinessUnitId { get; set; }

        public string BusinessUnitName { get; set; }

        public string BusinessUnitShortName { get; set; }

        public bool IsActive { get; set; }

        public string CountryName { get; set; }

        public string CityName { get; set; }
    }
}
