﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.HSEObjects.SafeguardingReportData
{
    public class EntityData
    {

        public EntityData()
        {
        }

        public EntityData(long _entityId)
        {
            EntityId = _entityId;
        }

        public long EntityId { get; set; }

        public int IncidentId { get; set; }

        public int EntityTypeId { get; set; }

        public string EntityTypeName { get; set; }

        public long EntityPrimaryId { get; set; }

        public string Name { get; set; }

        public string ContactNo { get; set; }

        public string Gender { get; set; }

        public string Class { get; set; }

        public string Occupation { get; set; }

        public string CntrCompanyName { get; set; }

        public int Age { get; set; }

        public string TypeName { get; set; }

       // public string SchoolBusinessUnitName { get; set; }        
        //public EntityTreatment EntityTreatment { get; set; }
       // public dynamic EntityInjury { get; set; }

        public bool IsPersonInvolved { get; set; }

        public bool IsInjuryAdded { get; set; }

        public bool IsTreatmentAdded { get; set; }

        #region Entity Treatment
        public string TakenToHospitalDoctor { get; set; }
        public string CheifComplaint { get; set; }
        public string Observation { get; set; }
        public string Treatment { get; set; }
        public string TreatedBy { get; set; }
        public string TreaterName { get; set; }
        public string Prescription { get; set; }
        public string CalledBy { get; set; }
        public DateTime? CalledTime { get; set; }
        public string AttatchFile { get; set; }
        public int DaysLost { get; set; }
        public bool WasMedicalAttentionSought { get; set; }       
        public bool ParentalContacted { get; set; }        
        public bool IsAmbulanceCalled { get; set; }       
        public bool IsLTA { get; set; }

        #endregion

        #region Entity Enjury
        public string InjuryDescription { get; set; }
        //public string Gender { get; set; }
        public bool Head { get; set; }
        public bool Face { get; set; }
        public bool BackOfHead { get; set; }
        public bool RightShoulder { get; set; }
        public bool RightUpperArm { get; set; }
        public bool RightForeArm { get; set; }
        public bool RightHand { get; set; }
        public bool RightWrist { get; set; }
        public bool Chest { get; set; }
        public bool Stomach { get; set; }
        public bool Groin { get; set; }
        public bool RightThigh { get; set; }
        public bool RightKnee { get; set; }
        public bool RightLowerLeg { get; set; }
        public bool RightAnkel { get; set; }
        public bool RightFoot { get; set; }
        public bool LeftShoulder { get; set; }
        public bool LeftUpperArm { get; set; }
        public bool LeftForeArm { get; set; }
        public bool LeftHand { get; set; }
        public bool LeftWrist { get; set; }
        public bool UpperBack { get; set; }
        public bool MidToLowerBack { get; set; }
        public bool Waist { get; set; }
        public bool LeftThigh { get; set; }
        public bool LeftKnee { get; set; }
        public bool LeftLowerLeg { get; set; }
        public bool LeftAnkel { get; set; }
        public bool LeftFoot { get; set; }
        public bool RHIndexFinger { get; set; }
        public bool RHThumb { get; set; }
        public bool RHMiddleFinger { get; set; }
        public bool RHRingFinger { get; set; }
        public bool RHLittleFinger { get; set; }
        public bool RHPalm { get; set; }
        public bool RHBackOfHand { get; set; }
        public bool LHIndexFinger { get; set; }
        public bool LHThumb { get; set; }
        public bool LHMiddleFinger { get; set; }
        public bool LHRingFinger { get; set; }
        public bool LHLittleFinger { get; set; }
        public bool LHPalm { get; set; }
        public bool LHBackOfHand { get; set; }
        public bool RLLargeToe { get; set; }
        public bool RLToe2 { get; set; }
        public bool RLMiddleToe { get; set; }
        public bool RLToe4 { get; set; }
        public bool RLLittleToe { get; set; }
        public bool RLSole { get; set; }
        public bool RLTopFoot { get; set; }
        public bool RLHeel { get; set; }
        public bool LLLargeToe { get; set; }
        public bool LLToe2 { get; set; }
        public bool LLMiddleToe { get; set; }
        public bool LLToe4 { get; set; }
        public bool LLLittleToe { get; set; }
        public bool LLSole { get; set; }
        public bool LLTopFoot { get; set; }
        public bool LLHeel { get; set; }
        public bool ForeHead { get; set; }
        public bool RightEye { get; set; }
        public bool RightEar { get; set; }
        public bool RightCheek { get; set; }
        public bool Teeth { get; set; }
        public bool InnerMouth { get; set; }
        public bool Chin { get; set; }
        public bool LeftEye { get; set; }
        public bool LeftEar { get; set; }
        public bool LeftCheek { get; set; }
        public bool Nose { get; set; }
        public bool OuterMouth { get; set; }
        public bool Jaw { get; set; }
        

        #endregion
    }
}
