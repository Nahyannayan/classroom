﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.HSEObjects.SafeguardingReportData
{
    public class AgenciesData
    {        
        public int IncidentId { get; set; }

        public int AgencyTypeId { get; set; }

        public string AgencyTypeName { get; set; }

        public string AgencyName { get; set; }

        public string ContactPerson { get; set; }

        public string ContactNo { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }

        public string Remark { get; set; }

        //public bool IsAddMode { get; set; }

        
    }
}
