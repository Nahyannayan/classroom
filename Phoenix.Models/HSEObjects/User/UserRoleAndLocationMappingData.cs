﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.HSEObjects.User
{
    public class UserRoleAndLocationMappingData
    {
        public UserRoleAndLocationMappingData()
        {
            UserRoles = new List<UserRoleMapping>();
            AuditUserLocationsList = new List<UserLocationMap>();
            CommonUserLocationsList = new List<UserLocationMap>();
        }
        public List<UserRoleMapping> UserRoles { get; set; }
        public List<UserLocationMap> AuditUserLocationsList { get; set; }
        public List<UserLocationMap> CommonUserLocationsList { get; set; }

    }
}
