﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Models
{
    public class UserLoginHistory
    {
        public long LoginId { get; set; }
        public string LoginUserName { get; set; }
        public int LoginMode { get; set; }
        public DateTime LoginTime { get; set; }
        public DateTime LogOutTime { get; set; }
        public bool LoginStatus { get; set; }
        public string LoginBrowser { get; set; }
        public string LoginIP { get; set; }
        public long LoginSessionId { get; set; }
        public string Type { get; set; } //Login or LogOut

    }

   
}
