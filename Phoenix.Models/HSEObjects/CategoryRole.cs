﻿namespace Phoenix.Models
{
    public class CategoryRole
    {
        public int CategoryId { get; set; }
        public int RoleId { get; set; }
    }
}
