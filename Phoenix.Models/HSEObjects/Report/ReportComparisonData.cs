﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class IncidentReportData
    {
        public IncidentReportData()
        {
            ReportComparisonData = new List<ReportComparisonData>();
            GroupByData = new List<GroupByChartData>();
        }

        public string ReportTitle { get; set; }

        public int ReportType { get; set; }

        public int SubReportType { get; set; }

        public string Col1Label { get; set; }

        public string Col2Label { get; set; }

        public string Col3Label { get; set; }

        public string Col4Label { get; set; }

        public string XAxisLabel { get; set; }

        public string YAxisLabel { get; set; }

        public int MajorUnitValue { get; set; }

        public List<ReportComparisonData> ReportComparisonData { get; set; }
        public ReportParameters ReportParameters { get; set; }
        public List<GroupByChartData> GroupByData { get; set; }
    }

    public class ReportComparisonData
    {
        public string RLable { get; set; }
        public string CLable { get; set; }
        public string CColor { get; set; }
        public int ReportCol1 { get; set; }
        public int ReportCol2 { get; set; }
        public int ReportCol3 { get; set; }
        public int ReportCol4 { get; set; }
        public int ReportCol5 { get; set; }
        public int ReportCol6 { get; set; }
    }

    public class GroupByChartData
    {
        public GroupByChartData()
        {
            GroupByData = new List<ReportComparisonData>();
        }
        public string GroupBy { get; set; }
        public string Color { get; set; }
        public List<ReportComparisonData> GroupByData { get; set; }
    }
}
