﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ReportParameters
    {
        public ReportParameters()
        {
            ReportName = new List<string>();
        }

        public string ReportType { get; set; }

        public int SubType { get; set; }

        public int UserId { get; set; }

        public int CountryId { get; set; }

        public int CityId { get; set; }

        public string BusinessUnits { get; set; }

        public string Category { get; set; }

        public int CategoryId { get; set; }

        public string Severity { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public string Param1 { get; set; }
        public string Param2 { get; set; }
        public string InjuredPersonType { get; set; }
        public string SubCategoryList { get; set; }

        public int OrderBy { get; set; }
        public bool NoStack { get; set; }

        public List<string> ReportName { get; set; }
    }
}
