﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditReport
    {
        public class AuditReportParam
        {
            public AuditReportParam()
            {
                Audits = new List<int>();
            }
            public string ReportName { get; set; }

            public string Location { get; set; }
            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            public IEnumerable<ModuleStructure> ModuleStructures { get; set; }
            public int? CountryId { get; set; }
            public int? CityId { get; set; }
            public int AuditType { get; set; }
            public string ModuleName { get; set; }
            public string GroupBy { get; set; }
            public bool ShowGroupBy { get; set; }
            public string TypeYear { get; set; }
            public dynamic AuditList { get; set; }
            public List<int> Audits { get; set; }
        }

        public class AuditReportField
        {
            public string Inspection_Id { get; set; }
            public string Audit_Id { get; set; }
            public string School { get; set; }
            public string Title { get; set; }
            public string Score { get; set; }
            public string Audit_Rating { get; set; }
            public string Start_Date { get; set; }
            public string End_Date { get; set; }
            public string Closed_Date { get; set; }
            public string Status { get; set; }
            public string Due_Date { get; set; }
            public string AdHoc { get; set; }
        }

        public class AuditsCompletedReport
        {
            public string Date { get; set; }
            public int AuditsCompleted { get; set; }
            public string GroupBy { get; set; }
        }

        public class ScheduledAuditsCompletionStatus
        {
            public string Location { get; set; }
            public int Successful { get; set; }
            public int Underway { get; set; }
            public int NotStarted { get; set; }
            public int Missed { get; set; }
        }
    }
}
