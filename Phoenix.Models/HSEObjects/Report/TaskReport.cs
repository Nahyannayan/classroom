﻿using System;
using System.Collections.Generic;
using Phoenix.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
   public  class TaskReport
    {
        public class TaskReportParam
        {
            public string TaskStatus { get; set; }
            public string TaskName { get; set; }
           
            public string Location { get; set; }
            public string Owner { get; set; }
            public string AssignTo { get; set; }
            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            public IEnumerable<ModuleStructure> ModuleStructures { get; set; }
            public int? CountryId { get; set; }
            public int? CityId { get; set; }
            public string ListBy { get; set; }
            public string OverDueBy { get; set; }
            public string TaskSource { get; set; }
            public int SchoolId { get; set; }
            public string ReportType { get; set; }
            public string ModuleName { get; set; }
            public List<string> AuditType { get; set; }
            public List<string> AuditList { get; set; }
            public bool IsAuditBasedReport { get; set; }
            public long UserId { get; set; }
        }

        public class TaskReportField
        {
            public int RegistrationId { get; set; }
            public string DateCreated { get; set; }
            public string Title { get; set; } //'QuestionName' when Audits are selected
            public string Source { get; set; }
            public string Status { get; set; }
            public string DueDate { get; set; } //'Action TimeLine' when Audits are selected
            public string Completed { get; set; }
            public string Location { get; set; } //used instead of BusinessUnitShortName when Audits are selected
            public string Owner { get; set; }
            public string AllocatedTo { get; set; } //'Action By' when Audits are selected
            public int AssignId { get; set; }

            /* New Fields for Inspection Tasks */
            public int InspectionID { get; set; }
            public string AuditName { get; set; }
            public string Severity { get; set; }
            public string Compliant { get; set; }
            public string Observation { get; set; }
            public string Recommendation { get; set; }
            public int PercentageDrop { get; set; }
            public string Closed { get; set; }
            public string Comments { get; set; }
            public string IsAdhoc { get; set; }
        }

        public class TaskStatusByUserReport
        {
            public string UserName { get; set; }
            public int TotalTask { get; set; }
            public int Complete { get; set; }
            public int Pending { get; set; }
            public int OverDue { get; set; }
            public float CompletePer { get; set; }
            public float PendingPer { get; set; }
            public float OverDuePer { get; set; }
            public string CreatedDate { get; set; }
            public string Location { get; set; }
            public string ListBy { get; set; }
           

        }

    }
}
