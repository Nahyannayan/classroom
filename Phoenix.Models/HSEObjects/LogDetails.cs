﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.HSEObjects
{
    public class LogDetails
    {
        public int LogDetailsId { get; set; }
        public string InputJson { get; set; }
        public string OutputJson { get; set; }
        public string BaseUrl { get; set; }
        public string Header { get; set; }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public string ExceptionMsg { get; set; }
        public DateTime LogDate { get; set; }
        public bool IsSuccessStatusCode { get; set; }
        public string userName { get; set; }
    }
}
