﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Catagory
    {
 
        public int DocCatID { get; set; }

        public string DocumentType { get; set; }

        public bool? IsDeleted { get; set; }

        public int? ParentID { get; set; }

        public long? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }
        public string CreatedByName { get; set; }
    }
}
