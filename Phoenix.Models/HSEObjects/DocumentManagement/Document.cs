﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public enum FileUploadTo
    {
        Drive = 0,
        Database = 1,
        SharePoint = 2
    }
    public class Document
    {
        public Int64 DocumentID { get; set; }

        public string DocumentName { get; set; }

        public string DocumentDescription { get; set; }

        public string RootDirectory { get; set; }

        public string FileLocation { get; set; }

        public int? FileTypeID { get; set; }

        public int? CategoryID { get; set; }

        public bool? IsApproved { get; set; }

        public bool? IsCompleted { get; set; }

        public long? CreatedBy { get; set; }

        public byte[] StreamCol { get; set; }

        public int? Size { get; set; }

        public bool? IsDeleted { get; set; }

        public DateTime? CreatedOn { get; set; }
        
        public FileUploadTo FileUPloadTo { get; set; }
        public string CreatedByName { get; set; }

    }
}
