﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class CatagoryBUMap
    {
        public CatagoryBUMap()
        {
            catagoryBUMapList = new List<CatagoryBUMapList>();
        }
        public int DocCatID { get; set; }
        public long BusinessUnitId { get; set; }
        public bool IsDeleted { get; set; }
        public long LastModifiedBy { get; set; }
        public string Source { get; set; }
        public List<CatagoryBUMapList> catagoryBUMapList { get; set; }
    }
    public class CatagoryBUMapList
    {
        public int DocCatID { get; set; }
        public long BusinessUnitId { get; set; }
        public bool IsDeleted { get; set; }
        public long LastModifiedBy { get; set; }

    }
}
