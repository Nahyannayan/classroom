﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Version
    {
        public int VersionID { get; set; }

        public int? DocumentId { get; set; }
        
        public string Name { get; set; }

        public int? FileTypeID { get; set; }

        public DateTime? StoreDate { get; set; }

        public int? Size { get; set; }

        public int? CategoryID { get; set; }

        public string StreamCol { get; set; }
    }
}
