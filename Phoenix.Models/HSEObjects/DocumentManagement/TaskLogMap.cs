﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class TaskLogMap
    {
        public long LogID { get; set; }

        public int? RegistrationID { get; set; }

        public bool? DocumentID { get; set; }
    }
}
