﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class CheckerApproval
    {
        public int RepositoryId { get; set; }

        public int? RequestedBy { get; set; }

        public int? RequestedTo { get; set; }

        public bool? Status { get; set; }

        public string RequestorRemark { get; set; }

        public string ApproverRemark { get; set; }

        public long? RegistrationID { get; set; }

        public int? DcoumentID { get; set; }
    }
}
