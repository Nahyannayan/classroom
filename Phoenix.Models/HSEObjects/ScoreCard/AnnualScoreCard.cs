﻿using System;

namespace Phoenix.Models
{
    public class AnnualScoreCard
    {
        public int MyScoreCardId { get; set; }
        public int BussinessUnitID { get; set; }
        public int AccountingYearID { get; set; }
        public string Remarks { get; set; }
        public DateTime CompletedOn { get; set; }
        public int CompletedBy { get; set; }
    }
}
