﻿using System;

namespace Phoenix.Models
{
    public class AnnualTargetLocMap
    {
        public AnnualTargetLocMap() { }

        public AnnualTargetLocMap(int _annualTagerLocMapId)
        {
            AnnualTargetMapID = _annualTagerLocMapId;
        }

        public int AnnualTargetMapID { get; set; }
        public int TargetProfileID    {get;set;}
        public int CountryID          {get;set;}
        public int CityID             {get;set;}
        public int BussinessUnitID    {get;set;}
        public DateTime LastModifiedOn {get;set;}
        public int LastModifedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
