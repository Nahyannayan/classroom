﻿using System;

namespace Phoenix.Models
{
    public class ScoreCardActivityLog
    {
        public int ActivityID { get; set; }
        public long RegistrationID { get; set; }
        public DateTime EventDateTime { get; set; }
        public int EventByUserID { get; set; }
        public string EventType { get; set; }
        public string ModelName { get; set; }
        public string ModelData { get; set; }
    }
}
