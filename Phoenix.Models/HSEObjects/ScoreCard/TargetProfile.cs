﻿using System;

namespace Phoenix.Models
{
    public class TargetProfile :AccountingYear
    {
        public int TargetProfileID { get; set; }
        //public int AccountingYearID { get; set; }
        public int StartDay { get; set; }
        public int ExpireDay { get; set; }
        public string Remarks { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public int LastModifedBy { get; set; }
        public string AccountingYear { get; set; }
    }
}
