﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.Models
{
    public class MonthActualTargetValueParameters
    {
        public int TargetProfileId { get; set; }
        public int BusinessUnitId { get; set; }
        public int MonthId { get; set; }
        public int KpiId { get; set; }
        public int ActualMonthId { get; set; }
        public string BusinessUnitIds { get; set; }
        public int AccountingYearId { get; set; }
        public int? CountryId { get; set; }
        public int? CityId { get; set; }
        public long UserId { get; set; }
    }

    public class MonthActualTargetProfileValue
    {
        public int MonthActualId { get; set; }
        public int AnnualTargetMapID { get; set; }
        public int MonthID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime DueDate { get; set; }
        public int TargetProfileID { get; set; }
        public int BussinessUnitID { get; set; }
        public string BusinessUnitName { get; set; }
        public string BusinessUnitShortName { get; set; }
        public decimal TargetValue { get; set; }
        public string Remarks { get; set; }
        public int KPIid { get; set; }
        public string KPIName { get; set; }
        public int? DisplayParentKPI { get; set; }
        public string DisplayParentKPIName { get; set; }
        public int KPIMonth { get; set; }
        public int? MTVKPIid { get; set; }
        public decimal? ActualValue { get; set; }
        public decimal YTDActualValue { get; set; }

        public string MTVRemarks { get; set; }
        public string Documents { get; set; }
        public decimal YealyTarget { get; set; }
        public decimal YTDTarget { get; set; }
        public bool ReadOnly { get; set; }
        public IEnumerable<FileData> FileList { get; set; }
        public string ComplianceType { get; set; }
        public int? SumParentKPI { get; set; }
        public int SequenceNo { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsUploadMandatory { get; set; }
        public bool IsSubmitted { get; set; }
        public bool IsApproved { get; set; }
        public int IsRevised { get; set; }
        public bool IsDue { get; set; }
        public decimal YearlyCompliance { get; set; }
        public decimal MonthlyCompliance { get; set; }
    }

    public class MonthActualTargetValueVM
    {
        public MonthActualTargetValueVM()
        {
            MonthActualTargetValue = new List<MonthActualTargetProfileValue>();
        }
        public int KPIID { get; set; }
        public string BusinessUnitShortName { get; set; }
        public string Title { get; set; }
        public IEnumerable<MonthActualTargetProfileValue> MonthActualTargetValue { get; set; }
        public int SequenceNo { get; set; }
        public string ReportType { get; set; }
    }

    public class FileData
    {
        public FileData()
        {
            FileContent = new byte[0];
        }
        public int RegistrationId { get; set; }
        public long? AttachmentId { get; set; }
        public int CategoryId { get; set; }
        public string AttachmentName { get; set; }
        public bool IsProcessed { get; set; }
        public HttpPostedFileBase AttachmentFile { get; set; }
        public byte[] FileContent { get; set; }
    }

}
