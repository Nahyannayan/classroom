﻿using System;

namespace Phoenix.Models
{
    public class KPIItem
    {
        public int KPIid { get; set; }
        public string KPIKey { get; set; }
        public string KPIName { get; set; }
        public bool DisplayParentKPI { get; set; }
        public int SumParentKPI { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public int LastModifedBy { get; set; }
        public bool IsManualEntry { get; set; }
        public bool IsActive { get; set; }
        public decimal DefaultTargetValue { get; set; }
        public bool IsCarryForward { get; set; }
        public int SequenceNo { get; set; }
        public string RegularExpression { get; set; }
    }
}
