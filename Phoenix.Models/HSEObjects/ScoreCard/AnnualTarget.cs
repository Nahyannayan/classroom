﻿using System;

namespace Phoenix.Models
{
    public class AnnualTarget
    {
        public int TargetProfileID { get; set; }
        public int KPIid { get; set; }
        public int? DisplayParentKPI { get; set; }
        public decimal TargetValue { get; set; }
        public string Remarks { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public int LastModifedBy { get; set; }
        public string KPIName { get; set; }
        public bool IsSelected { get; set; }
    }
}
