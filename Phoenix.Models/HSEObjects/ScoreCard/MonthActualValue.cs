﻿
using System;

namespace Phoenix.Models
{
    public class MonthActualValue
    {
        public int MonthActualId { get; set; }
        public int KPIid { get; set; }
        public int KPIMonth { get; set; }
        public decimal? ActualValue { get; set; }
        public string Remarks { get; set; }
        public string Documents { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public int LastModifedBy { get; set; }
        public decimal YealyTarget { get; set; }
        public decimal YTDTarget { get; set; }
        public decimal YTDActualValue { get; set; }
        public decimal TargetValue { get; set; }
        
        public decimal YearlyCompliance { get; set; }
        public decimal MonthlyCompliance { get; set; }
    }
}
