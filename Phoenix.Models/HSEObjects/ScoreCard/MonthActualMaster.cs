﻿using System;

namespace Phoenix.Models
{
    public class MonthActualMaster: AnnualTargetLocMap
    {
        public int MonthActualId { get; set; }
        //public int AnnualTargetMapID { get; set; }
        public int MonthID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime DueDate { get; set; }
        public bool IsSubmitted { get; set; }
        public DateTime? SubmittedOn { get; set; }
        public int SubmittedBy { get; set; }
        public bool IsApproved { get; set; }
        public DateTime? ApprovedOn { get; set; }
        public int ApprovedBy { get; set; }
        public DateTime RevisedDueDate { get; set; }
    }
}
