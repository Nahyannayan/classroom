﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class MonthActualTargetMasterParameters
    {
        public long UserId { get; set; }
        public int AccountingYearId { get; set; }
        public int TargetProfileId { get; set; }
        public string BusinessUnitId { get; set; }
        public int MonthId { get; set; }
        public int MonthActualId { get; set; }
    }

    public class MonthActualTargetProfile :BusinessUnitMaster
    {
        public int TargetProfileID { get; set; }
        public int AccountingYearID { get; set; }
        public int StartDay { get; set; }
        public int ExpireDay { get; set; }
        public string Remarks { get; set; }
        public string AccountingYear { get; set; }
        public int BussinessUnitID { get; set; }
        //public string BusinessUnitShortName { get; set; }
        public int MonthActualId { get; set; }
        public int MonthID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime DueDate { get; set; }
        public bool IsSubmitted { get; set; }
        public bool IsDue { get; set; }
        public bool IsApproved { get; set; }
        public int IsRevised { get; set; }
    }
    public class MonthActualRevise
    {
        public int MonthActualId { get; set; }
        public DateTime RevisedDueDate { get; set; }
        public int RevisedBy { get; set; }
        public int AnnualTargetMapID { get; set; }
    }

}
