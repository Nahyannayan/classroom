﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class BodyPart
    {
        public int BodyPartId { get; set; }

        public string BodyPartName { get; set; }

        public int Sequence { get; set; }

        public int Section { get; set; }

        public string Coordinates { get; set; }

        public bool IsHaveSubSection { get; set; }
    }
}
