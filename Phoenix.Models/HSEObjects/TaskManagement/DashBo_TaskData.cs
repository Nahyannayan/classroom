﻿using System;
using System.Collections.Generic;

namespace Phoenix.Models
{
    public class DashBo_TaskData
    {
        public TaskHeading heading { get; set; }
        public TaskChart chart { get; set; }
        public List<TaskEntity> list { get; set; }
    }

    public class TaskEntity
    {
        public long RegistrationID { get; set; }
        public long AssignId { get; set; }
        public string TaskName { get; set; }
        public int Status { get; set; }
        public string StartedOn { get; set; }
        public bool HasDocuments { get; set; }
        public string BusinessUnitShortName { get; set; }
    }

    public class TaskHeading
    {
        public long Open { get; set; }
        public long OverDue { get; set; }
        public long InProgress { get; set; }
    }

    public class TaskChart
    {
        public int High { get; set; }
        public decimal HighPer { get; set; }
        public int Medium { get; set; }
        public decimal MediumPer { get; set; }
        public int Low { get; set; }
        public decimal LowPer { get; set; }
    }
}
