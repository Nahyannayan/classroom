﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AssignDeAssignTask
    {
        public Int32 Assignid { get; set; }
        public Int32 Registrationid { get; set; }
        public Int32 TaskUserid { get; set; }
        public string TaskName { get; set; }
        public string TaskUser { get; set; }
        public string Description { get; set; }
        public string Priority { get; set; }
        public string DueDate { get; set; }
        public string StartDateTime { get; set; }
        public string EndTime { get; set; }
        public bool IsCompleted { get; set; }
        public string Location { get; set; }
       public string DocumentIds { get; set; }
        public int CreatedBy { get; set; }
    }



}
