﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class TaskActivityLog
    {
        public int ActivityID { get; set; }
        public long RegistrationID { get; set; }
        public DateTime EventDateTime { get; set; }
        public int EventByUserID { get; set; }
        public string EventType { get; set; }
        public string ModelName { get; set; }
        public string ModelData { get; set; }
    }
}
