﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class TaskFilterParam
    {
        public long UserId { get; set; }

        public string TaskType { get; set; }

        public string Priority { get; set; }

        public int? Status { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public long? AssignedToId { get; set; }

        public long? CreatedById { get; set; }

        public int PageNo { get; set; }

        public int NoOfRecords { get; set; }

        public int? TaskToShow { get; set; }

        public string SearchText { get; set; }
    }
}
