﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Phoenix.Models
{
    public class Tasks
    {
        public Tasks() { }

        public int NoOfPages { get; set; }
        public int TotalNoOfRecords { get; set; }

        public IEnumerable<Task> Registrations { get; set; }
    }
}
