﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class TaskReminder
    {
        public Int32 ReminderId { get; set; }

        public Int32 RegistrationId { get; set; }

        public string TaskName { get; set; }
        public string TimeType { get; set; }

        public DateTime date { get; set; }
        public string time { get; set; }
        public int IsAfterDueDate { get; set; }
        public Int16 Days { get; set; }
        public string Message { get; set; }
        public DateTime ReminderOn { get; set; }

    }
}
