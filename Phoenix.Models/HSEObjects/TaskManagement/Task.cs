﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Models
{
    public class Task
    {
        public Task()
        {
            TaskLogs = new HashSet<TaskLog>();
        }
        public int AssignId { get; set; }
        public Int32 RegistrationId { get; set; }
        public string TaskName { get; set; }
        public string Description { get; set; }
       public int IsDue { get; set; }
        public string    StartDate { get; set; }
        public string EndDate { get; set; }
        public string EstimatedTime { get; set; }
        public string Priority { get; set; }
        public string AssignTo { get; set; }
        public string TaskDonePercent { get; set; }
        public int IsCompleted { get; set; }
        public Int32 UserId { get; set; }
        public string NotifyUserIds { get; set; }
        public int TaskTypeId { get; set; }
        public string TaskType { get; set; }
        public int CreatedBy { get; set; }
        public int TaskIncidentId { get; set; }

        public int CountryId { get; set; }
        public int CityId { get; set; }
        public int BusinessUnitId { get; set; }
        public string Location { get; set; }
        public string TaskOwner { get; set; }
        public int InspectionId { get; set; }
        public int AuditQuestionId { get; set; }
        public string AssignToEmail { get; set; }
        public string DocumnetIds { get; set; }
        public string SchoolName { get; set; }
        public string TaskCompletedDate { get; set; }
        public string DeletedDocuments { get; set; }
        public long ParentBusinessUnitId { get; set; }
        public bool IsSafeguarding { get; set; }

        public IEnumerable<TaskLog> TaskLogs { get; set; }

        public bool IsTaskCompleted { get; set; }
        public bool IsStandBy { get; set; }

    }

   
}
