﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Models
{
   public  class TaskLog
    {
        public Int32 LogId { get; set; }
        public Int32 AssignId { get; set; }
        public Int32 RegistrationId { get; set; }
        public string TaskName { get; set; }
        public int TaskUserId { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string TaskUser { get; set; }
        public string TimeSpent { get; set; }
        public bool IsBillable { get; set; }
        public bool IsCompleted { get; set; }
        public int Billable { get; set; }
        public string DocumentIds { get; set; }
    }
}
