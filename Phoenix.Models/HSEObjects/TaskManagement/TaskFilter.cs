﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class TaskFilter
    {
        public int TaskType { get; set; }
        public string Priority { get; set; }
        public string FromToDate { get; set; }

        public string AssignTo { get; set; }
        public string CreatedBy { get; set; }
    }
}
