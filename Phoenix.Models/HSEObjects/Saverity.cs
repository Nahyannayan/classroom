﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Severity
    {
        public int IncidentSeverityId { get; set; }

        public string SeverityName { get; set; }

        public int Sequence { get; set; }

        public bool IsActive { get; set; }
    }
}
