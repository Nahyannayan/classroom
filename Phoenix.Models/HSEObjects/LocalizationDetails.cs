﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Phoenix.Models.HSEObjects
{
    public class LocalizationDetails
    {
        public string tableName { get; set; }
        public string columnName { get; set; }
        public string columnValue { get; set; }
        public string identityColumnName { get; set; }
        public Int32 identityColumnValue { get; set; }        
    }
}
