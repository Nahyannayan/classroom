﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Category
    {
        public Category() { }

        public Category(int _categoryId)
        {
            IncidentCategoryId = _categoryId;
            //SubCategories = new HashSet<SubCategory>();
        }

        public int IncidentCategoryId { get; set; }

        public string CategoryName { get; set; }

        public int Sequence { get; set; }

        public bool IsActive { get; set; }

        public string Color { get; set; }

        public bool IsSafeGuarding { get; set; }

        public int DraftNotificationDays { get; set; }
        public int DraftDeletionDays { get; set; }

        public IEnumerable<SubCategory> SubCategories { get; set; }
        public string CategoryLocalizeXml { get; set; }
    }
}
