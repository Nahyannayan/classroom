﻿using Phoenix.Models.HSEObjects.SafeguardingReportData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models.Entities
{
    public class SafeguardingData
    {
        public SafeguardingData()
        {
            BusinessUnit = new List<BusinessUnitData>();            
            Entity = new List<EntityData>();
            Assailant = new List<AssailantData>();
            Witness = new List<Witness>();
            Agencies = new List<AgenciesData>();            
            CategotySubCategoryMap = new List<CategorySubCategoryMap>();
        }

        public List<BusinessUnitData> BusinessUnit { get; set; }        
        public List<CategorySubCategoryMap> CategotySubCategoryMap { get; set; }
        public List<EntityData> Entity { get; set; }
        public List<AssailantData> Assailant { get; set; }
        public List<Witness> Witness { get; set; }
        public List<AgenciesData> Agencies { get; set; }
    }
}
