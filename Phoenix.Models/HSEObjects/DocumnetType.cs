﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class DocumnetType
    {
        public int DocumentTypeId { get; set; }

        public string DocumentName { get; set; }

        public int Sequence { get; set; }

        public bool IsActive { get; set; }
    }
}
