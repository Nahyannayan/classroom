﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class SubCategory
    {
        public SubCategory() { }

        public SubCategory(int _incidentSubcategoryId)
        {
            IncidentSubCategoryId = _incidentSubcategoryId;
        }

        public int IncidentSubCategoryId { get; set; }
        public int IncidentCategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public int Sequence { get; set; }
        public string ExtraDetails { get; set; }
        public string CategoryName { get; set; }
        public bool IsActive { get; set; }
        public bool IsVictimPersonMandatory { get; set; }
        public bool IsApprovalFlowEnabled { get; set; }
        public long LastModifiedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public string SubCategoryLocalizeXml{get;set;}
    }
}
