﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ControlDetails
    {
        public int ControlId { get; set; }
        public string ControlName { get; set; }
        public string ControlType { get; set; }
        public string ControlValue { get; set; }
        public bool IsAddTask { get; set; }
        public bool IsNegative { get; set; }
        public int ControlProfileID { get; set; }
        public bool IsSelected { get; set; }
        public bool IsDisplay { get; set; }
    }
}
