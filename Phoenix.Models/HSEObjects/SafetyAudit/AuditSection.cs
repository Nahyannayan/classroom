﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditSection
    {
        public AuditSection()
        {
            AuditQuestionAnswerList = new HashSet<AuditQuestionAnswer>();
        }
        public int AuditSectionID { get; set; }
        public string SectionName { get; set; }
        public string SectionDescription { get; set; }
        public int AuditAmmendID { get; set; }
        public int SectionSNo { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public IEnumerable<AuditQuestionAnswer> AuditQuestionAnswerList { get; set; }
    }
}
