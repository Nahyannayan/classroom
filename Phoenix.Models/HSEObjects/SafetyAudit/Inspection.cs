﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Inspection
    {
        public Inspection()
        {
            InspectionValues = new HashSet<InspectionValue>();
        }

        public int InspectionID { get; set; }

        public int AuditLocationMapID { get; set; }

        public string CreatedOn { get; set; }

        public string StartedOn { get; set; }

        public string EndAt { get; set; }
        public string CompletedAt { get; set; }

        public string Comments { get; set; }
        public int AuditAmmendID { get; set; }
        public long AssignedTo { get; set; }
        public string Documents { get; set; }

        public virtual ICollection<InspectionValue> InspectionValues { get; set; }

        public long AuditID { get; set; }
        public string AuditName { get; set; }
        public string BusinessUnitName { get; set; }
        public string BusinessUnitShortName { get; set; }
        public long BusinessUnitID { get; set; }
        public long CountryID { get; set; }
        public long CityID { get; set; }
        public string AssigneeName { get; set; }
        public string Score { get; set; }
        public string Percentage { get; set; }
        public string Remaining { get; set; }
        public string AuditRating { get; set; }
        public string TotalDrop { get; set; }
        

        public int Status { get; set; }
        public int IsDue { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public bool IsAdHoc { get; set; }
        public int AuditScheduleID { get; set; }
        public bool RequireApproval { get; set; }
        public int InspectionStatus { get; set; }
        public long CompletedBy { get; set; }
        public long ApprovedBy { get; set; }
        public string CompletedByName { get; set; }
        public string ApprovedByName { get; set; }
        public int ScoreCalcId { get; set; }

    }
}
