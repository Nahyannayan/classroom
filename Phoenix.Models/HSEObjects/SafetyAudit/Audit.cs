﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Audit
    {
        public int AuditID { get; set; }

        public string AuditName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public long ModifiedBy { get; set; }
        public string ModifiedOn { get; set; }
        public int ControlProfileId { get; set; }

        public int ScoreCalcId { get; set; }
        public bool MarkDefaultYes { get; set; }
        public int SafetyAuditSeverityId { get; set; }
        public int PositiveValue { get; set; }

        public int NegativeValue { get; set; }
        public bool HasAmmend { get; set; }
        public IEnumerable<AuditSeverityMap> auditSeverityMapList { get; set; }
    }
}
