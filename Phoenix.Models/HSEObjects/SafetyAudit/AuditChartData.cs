﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditChartData
    {
        public int Total { get; set; }
        public int Successful { get; set; }
        public int Missed { get; set; }
        public int Underway { get; set; }
        public int NotStarted { get; set; }
    }
}
