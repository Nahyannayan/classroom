﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class InspectionValue
    {
        public int InspectionValuesId { get; set; }

        public int InspectionID { get; set; }

        public int QuestionID { get; set; }
        public int AuditQuestionID { get; set; }
        
        public string Value { get; set; }

        public string Note { get; set; }

        public int Score { get; set; }

        public virtual Inspection Inspection { get; set; }

        public virtual Question Question { get; set; }

        public int TaskId { get; set; }
        public string Observation { get; set; }
        public string Recommendation { get; set; }
        public long ActionBy { get; set; }
        public string Documents { get; set; }
        public int ControlId { get; set; }

    }
}
