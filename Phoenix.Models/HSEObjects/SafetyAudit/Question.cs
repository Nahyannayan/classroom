﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class Question
    {
        
        public Question()
        {
            InspectionValues = new HashSet<InspectionValue>();
        }

        public int QuestionID { get; set; }

        public string QuestionName { get; set; }

        public string QuestionDescription { get; set; }
        public string Sno { get; set; }
        public string HelpText { get; set; }

        public string Sequence { get; set; }
        
        public int AuditAmmendID { get; set; }

        public virtual AnswerProfile AnswerProfile { get; set; }

        public virtual ICollection<InspectionValue> InspectionValues { get; set; }

        public virtual QuestionGroup QuestionGroup { get; set; }


    }
}
