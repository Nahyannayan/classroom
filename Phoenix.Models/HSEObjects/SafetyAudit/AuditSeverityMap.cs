﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditSeverityMap
    {
        public int AuditSeverityMapId { get; set; }
        public int AuditID { get; set; }
        public int SafetyAuditSeverityId { get; set; }
        public string SeverityName { get; set; }
        public int PositiveValue { get; set; }
        public int NegativeValue { get; set; }
        public long ModifiedBy { get; set; }
        public string ModifiedOn { get; set; }
    }
}
