﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditAmmend
    {
        public AuditAmmend()
        {
            AuditSectionList = new List<AuditSection>();
            auditRatingLookupList = new List<AuditRatingLookup>();
        }
        public long AuditAmmendID { get; set; }
        public long AuditID { get; set; }
        public long AmmedByUser { get; set; }
        public string AmmedOn { get; set; }
        public bool ApplyAudit { get; set; }
        public string AmmendRemarks { get; set; }
        public string EffectiveFrom { get; set; }
        public string VersionName { get; set; }
        public bool Status { get; set; }
        public bool IsMapped { get; set; }
        public IEnumerable<AuditSection> AuditSectionList { get; set; }


        public string BusinessUnitName { get; set; }
        public string StartedOn { get; set; }

        public string EndAt { get; set; }
        public string CompletedAt { get; set; }
        public string AuditName { get; set; }
        public string Percentage { get; set; }
        public string AuditRating{ get; set; }
        public string TotalDrop { get; set; }
        public int InspectionStatus { get; set; }
        public int IsDue { get; set; }
        public int ScoreCalcId { get; set; }
        public IEnumerable<Task> inspectionTaskList { get; set; }
        public string CompletedByName { get; set; }
        public string ApprovedByName { get; set; }
        public IEnumerable<AuditRatingLookup> auditRatingLookupList { get; set; }

    }
}
