﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class QuestionSection
    {
        public int QuestionSectionID { get; set; }
        public string SectionName { get; set; }
        public string SectionDescription { get; set; }
    }
}
