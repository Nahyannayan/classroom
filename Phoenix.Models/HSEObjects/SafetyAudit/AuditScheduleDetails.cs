﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditScheduleDetails
    {
        public int AuditScheduleDetailId { get; set; }
        public int AuditScheduleId { get; set; }
        public DateTime IterationStartDate { get; set; }
        public DateTime IterationEndDate { get; set; }
        public int AuditAmmendId { get; set; }
        public string VersionName { get; set; }
    }
}
