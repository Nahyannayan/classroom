﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditQuestionAnswer
    {
        public AuditQuestionAnswer()
        {
            ControlDetailsList = new List<ControlDetails>();
        }
        public int AuditQuestionID { get; set; }
        public int QuestionID { get; set; }

        public int SequenceNo { get; set; }

        public int Severity { get; set; }
        public int PositiveValue { get; set; }

        public int NegativeValue { get; set; }
        public int AuditSectionID { get; set; }
        public int ControlProfileID { get; set; }
        public int AuditAmmendID { get; set; }
        public string HelpText { get; set; }


        public string QuestionName { get; set; }
        public string QuestionDescription { get; set; }
        public string ControlProfileName { get; set; }
        public string AuditSectionName { get; set; }
        public string SeverityName { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }


        public List<ControlDetails> ControlDetailsList { get; set; }
        public InspectionValue InspectionValues { get; set; }
        public Task inspectionTask { get; set; }
        public TaskLog inspectionTaskLog { get; set; }

    }
}
