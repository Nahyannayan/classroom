﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditRoleMap
    {
        public int AuditId { get; set; }
        public string AuditName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        
    }
}
