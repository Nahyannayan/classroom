﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AnswerControl
    {
        
        public int AnswerID { get; set; }

        public int? AnswerProfileID { get; set; }
        
        public string ControlType { get; set; }

        public string ControlText { get; set; }

        public bool? IsDefault { get; set; }

        
        public string DefaultValue { get; set; }

        public bool? IsAddTask { get; set; }

        public int? NegativeRate { get; set; }

        public int? PositiveRate { get; set; }

        public virtual AnswerProfile AnswerProfile { get; set; }

    }
}
