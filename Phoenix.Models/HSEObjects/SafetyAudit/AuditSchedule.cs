﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditSchedule
    {
        public int AuditScheduleID { get; set; }
        public int SchoolAcademicYearID { get; set; }
        public long AuditID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Frequency { get; set; }
        public int Days { get; set; }
        public bool IsAdHoc { get; set; }
        public string FrequencyName { get; set; }
        public bool IsMapped { get; set; }
    }
}
