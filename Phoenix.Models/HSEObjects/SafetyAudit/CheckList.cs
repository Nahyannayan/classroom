﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class CheckList
    {
        public CheckList()
        {
            Inspections = new HashSet<Inspection>();
        }

        public int CheckListID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<Inspection> Inspections { get; set; }
    }
}
