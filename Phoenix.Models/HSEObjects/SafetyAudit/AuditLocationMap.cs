﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditLocationMap
    {
        public int AuditLocationMapID { get; set; }
        public int AuditScheduleID { get; set; }
        public int CountryID { get; set; }
        public int CityID { get; set; }
        public long BusinessUnitID { get; set; }
        public int AuditScheduleDetailID { get; set; }
        public bool IsDeleted { get; set; }
    }
}
