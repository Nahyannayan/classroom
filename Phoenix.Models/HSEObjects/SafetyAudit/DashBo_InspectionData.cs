﻿using System;
using System.Collections.Generic;

namespace Phoenix.Models
{
    public class DashBo_InspectionData
    {
        public InspectionHeading heading { get; set; }
        public InspectionChart chart { get; set; }
        public List<InspectionEntity> list { get; set; }
    }

    public class InspectionEntity
    {
        public long InspectionID { get; set; }
        public string AuditName { get; set; }
        public string BusinessUnitShortName { get; set; }
        public DateTime EndAt { get; set; }
        public DateTime CompletedAt { get; set; }
        public int Total { get; set; }
        public int Attempted { get; set; }
        public int Pending { get; set; }
        public int Status { get; set; }
    }

    public class InspectionHeading
    {
        public long Total { get; set; }
        public long Successful { get; set; }
        public long Failed { get; set; }
        public long Underway { get; set; }
        public long Upcoming { get; set; }
        public long NotStarted { get; set; }
    }

    public class InspectionChart
    {
        public long Successful { get; set; }
        public long Failed { get; set; }
        public long Underway { get; set; }
        public long Upcoming { get; set; }
        public long NotStarted { get; set; }
    }
}
