﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class QuestionGroup
    {
        public QuestionGroup()
        {
            Questions = new HashSet<Question>();
        }

        public int QuestionGroupID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<Question> Questions { get; set; }

    }
}
