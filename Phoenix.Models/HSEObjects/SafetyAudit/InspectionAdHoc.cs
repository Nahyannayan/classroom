﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class InspectionAdHoc
    {
        public int AuditScheduleID { get; set; }
        public long AdHocAuditID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int AdHocCountryId { get; set; }
        public int AdHocCityId { get; set; }

        public long AdHocBusinessUnitId { get; set; }
        public long AssignedTo { get; set; }
        public bool IsAddMode { get; set; }
        public int AuditAmmendID { get; set; }
        public bool RequireApproval { get; set; }
    }
}
