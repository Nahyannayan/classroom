﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class ControlProfile
    {
        public int ControlProfileId { get; set; }

        public string ControlProfileName { get; set; }
    }
}
