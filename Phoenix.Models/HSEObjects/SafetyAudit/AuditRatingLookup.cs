﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AuditRatingLookup
    {
        public int AuditRatingLookupId { get; set; }
        public int AuditAmmendId { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public int GCCodeValueId { get; set; }
        public string GCCodeName { get; set; }
        public long LastModifiedBy { get; set; }
    }
}
