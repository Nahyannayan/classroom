﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Models
{
    public class AnswerProfile
    {
        public AnswerProfile()
        {
            Questions = new HashSet<Question>();
        }

        public int AnswerProfileID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual AnswerControl AnswerControl { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
    }
}
