﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Models
{
    public class EntityType
    {
        public int EntityTypeId { get; set; }

        public string EntityTypeName { get; set; }

        public int Sequence { get; set; }
    }
}
