﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Models.DashBoard
{
    public class DashBoardGroups
    {
        public int SchoolGroupId { get; set; }
        public string SchoolGroupName { get; set; }
        public int? SubjectId { get; set; }
        public string SubjectName { get; set; }
        public long? CourseId { get; set; }
        public string CourseTitle { get; set; }
        public string MeetingRoomId { get; set; }
        public string GroupImage { get; set; }
        public DateTime UpdatedOn { get; set; }

      
    }
}
