﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Models.DashBoard
{
    public class DashBoardBanners
    {
        public string BannerName { get; set; }

        public string ActualImageName { get; set; }

    }
}
