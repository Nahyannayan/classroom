﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Models
{
    public class AssessmentConfigSetting
    {
        public AssessmentConfigSetting()
        {
            AssessmentColumnList = new List<AssessmentColumn>();
        }
        public long AssessmentMasterId { get; set; }
        public int AcademicYearId { get; set; }
        public string ASM_Description { get; set; }
        public string SchoolGradeIds { get; set; }
        public string SchoolGradeDisplay { get; set; }
        public long UserId { get; set; }
        public long SchoolId { get; set; }
        public int ASM_Order { get; set; }
        public bool ASM_ActiveStatus { get; set; }
        public string DATAMODE { get; set; }
        public IEnumerable<AssessmentColumn> AssessmentColumnList { get; set; }
        public int TotalCount { get; set; }
        public DataTable AssessmentColumnDT
        {
            get
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ASC_Id", typeof(long));
                dt.Columns.Add("ASC_CourseId", typeof(long));
                dt.Columns.Add("ASC_Description", typeof(string));
                dt.Columns.Add("ASC_ControlType", typeof(string));
                dt.Columns.Add("ASC_GradeTemplateMasterId", typeof(long));
                dt.Columns.Add("ASC_FromMark", typeof(decimal));
                dt.Columns.Add("ASC_ToMark", typeof(decimal));
                dt.Columns.Add("ASC_Order", typeof(int));
                dt.Columns.Add("ASC_ActiveStatus", typeof(bool));
                AssessmentColumnList.ToList().ForEach(item =>
                 {
                     dt.Rows.Add(
                                    item.ASC_Id,
                                    item.CourseId,
                                    item.ASC_Description,
                                    item.ControlType,
                                    item.GradeTemplateMasterId,
                                    item.FromMark,
                                    item.ToMark,
                                    item.ASC_Order,
                                    item.ASC_ActiveStatus
                                 );
                 });
                return dt;

            }

        }
    }
    public class AssessmentColumn
    {
        public long ASC_Id { get; set; }
        public long CourseId { get; set; }
        public string CourseName { get; set; }
        public string ASC_Description { get; set; }
        public int ControlType { get; set; }
        public long GradeTemplateMasterId { get; set; }
        public decimal FromMark { get; set; }
        public decimal ToMark { get; set; }
        public int ASC_Order { get; set; }
        public bool ASC_ActiveStatus { get; set; }
        public string AssessComment { get; set; }
    }
}
