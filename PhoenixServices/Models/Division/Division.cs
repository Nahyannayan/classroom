﻿using Phoenix.API.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Models
{
    public class DivisionDetails
    {
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public string GradeList { get; set; }
        public string DisplayGradeList { get; set; }
        public long CREATED_BY { get; set; }
        public long BSU_ID { set; get; }
        public string DataMode { get; set; }
        public int CurriculumId { get; set; }
    }
}
