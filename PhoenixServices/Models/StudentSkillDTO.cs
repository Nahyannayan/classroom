﻿using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Models
{
    public class StudentSkillDTO
    {
        public int SkillId { get; set; }
        public bool IsActive { get; set; }
        public bool IsApproved { get; set; }
        public string SkillName { get; set; }
        public long UserId { get; set; }
        public int StudentId { get; set; }
        public long CreatedUserId { get; set; }
        public string SkillDescription { get; set; }
        public TransactionModes Mode { get; set; }
    }
}
