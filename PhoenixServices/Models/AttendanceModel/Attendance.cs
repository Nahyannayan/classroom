﻿using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Models
{
    public class AttendanceDetails                       
    {
        public string Student_No { get; set; }
        public string Student_Name { get; set; }
        public string TDATE { get; set; }
        public string Status { get; set; }
        public string Student_Image_url { get; set; }
        public string AllowEdit { get; set; }
        public string ALG_ID { get; set; }
        public string STU_ID { get; set; }
        public string Student_XML { get; set; }
        public string current_ALG_ID { get; set; }
        public string APD_ID { get; set; }
        public string STATUS_DESCR { get; set; }
        public string Remarks { get; set; }
        public string transport_code { get; set; }
        public string transport_descr { get; set; }
        public long STREAM_ID { get; set; }
        public long SHIFT_ID { get; set; }
        public string ATT_SHORTCODE { get; set; }
        public long SectionId { get; set; }
    }

    public class RoomAttendance
    {

        public string Student_Name { get; set; }
        public long Student_ID { get; set; }
        public long TimeTableID { get; set; }
        public string Student_Image_url { get; set; }
        public string TransportDesc { get; set; }
        public string TransportCode { get; set; }

        public string STU_NO { get; set; }

        public string Grade { get; set; }
        public string SectionId { get; set; }
        public string Section { get; set; }

        public string DailyAttendance { get; set; }
        public bool IsInReport { get; set; }
        public long IsInReportID { get; set; }

    }

    public class SubjectRoomAttendance
    {

        public SubjectRoomAttendance()
        {
            objRoomAttendance = new List<RoomAttendance>();
            objTimeTable = new List<RoomAttendanceHeader>();
            objlstRoomRemarks = new List<StudentRoomAttendance>();
            objLstOptional = new List<RoomAttendanceHeader>();
            objlstOptionalHeaders = new List<RoomAttendanceHeader>();
            objRoomPeriodList = new List<RoomAttendanceHeader>();
        }
        public List<RoomAttendance> objRoomAttendance { get; set; }
        public List<RoomAttendanceHeader> objTimeTable { get; set; }

        public IList<ListItem> lstOtherCategories { get; set; }

        public List<StudentRoomAttendance> objlstRoomRemarks { get; set; }
        public List<RoomAttendanceHeader> objLstOptional { get; set; }

        public List<RoomAttendanceHeader> objlstOptionalHeaders { get; set; }

        public List<RoomAttendanceHeader> objRoomPeriodList { get; set; }
    }


    public class RoomAttendanceHeader
    {
        public string SGR_ID { get; set; }
        public string Subject { get; set; }

        public bool IsActive { get; set; }

        public bool IsOptional { get; set; }

        public int PeriodNo { get; set; }

        public string OptionalHeader { get; set; }
       
    }

    public class StudentRoomAttendance
    {
        public long logId { get; set; }
        public long DetailId { get; set; }
        public string RoomDate { get; set; }
        public long StudentId { get; set; }
        public long SGR_ID { get; set; }
        public long Subject { get; set; }
        public long PeriodNo { get; set; }
        public bool IsOptional { get; set; }
        public string Status { get; set; }

        public string Remark { get; set; }
        public string Description { get; set; }

        public string OptionalHeader { get; set; }


    }


    public class StudentLogAttendance
    {
        public StudentLogAttendance()
        {
            objStudentLog = new List<StudentRoomAttendance>();
            objStudentAttendance = new List<StudentRoomAttendance>();
        }
        public List<StudentRoomAttendance> objStudentLog { get; set; }
        public List<StudentRoomAttendance> objStudentAttendance { get; set; }

    }


    #region Attendance Settings

    #region Attendance Permission
    public class AttendancePermission
    {

        public long PermissionId { get; set; }
        public string EmployeeName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Grade { get; set; }
        public string Section { get; set; }
        public int AcademicYearId { get; set; }
        public string DisplayOrder { get; set; }
        public string Shift { get; set; }
        public string Stream { get; set; }
        public int StreamId { get; set; }
        public int ShiftId { get; set; }
        public int AuthorizedStaffId { get; set; }
        public string SectionId { get; set; }
        public int DivisionId { get; set; }
    }

    public class StaffDetails
    {
        public string EmployeeName { get; set; }
        public int EmployeeId { get; set; }


    }

    public class AttendanceStaff
    {
        public int SAD_ID { get; set; }
        public string EmployeeName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Description { get; set; }
    }

    #endregion

 
    public class GradeDetails
    {
        public int fad_id { get; set; }
        public string FAD_DT { get; set; }

        public DateTime FAD_DATE { get; set; }
        public string FAD_MONTH { get; set; }
        public string Grades { get; set; }
        public string GradesID { get; set; }

        public string FAD_DESCR { get; set; }
        public string FAD_CREATED_BY { get; set; }
        public int FAD_ACD_ID { get; set; }

        public int ACD_BSU_ID { get; set; }


        public string DataMode { get; set; }
    }
 
    #endregion

    #region Daily Attendance
    public class GradeSectionAccess
    {
        public string GradeId { get; set; }
        public string GradeDescription { get; set; }
        public int GradeDisplayOrder { get; set; }
        public long SectionId { get; set; }
        public string SectionDescription { get; set; }
    }

    public class AuthorizedStaffDetails
    {
        public long Id { get; set; }
        public long StaffId { get; set; }
        public long AcademicYearId { get; set; }
        public long  GradeId { get; set; }
        public long SectionId { get; set; }
        public string GradeName { get; set; }
        public string SectionName { get; set; }


    }
    public class AttendanceStudent
    {
        public string StudentImageUrl { get; set; }
        public string StudentName { get; set; }
        public string Nationality { get; set; }
        public long StudentNumber { get; set; }
        public long StudentId { get; set; }
        public long GradeId { get; set; }
        public long SectionId { get; set; }
        public long ParameterId { get; set; }
        public long RegisterId { get; set; }
        public long PhoenixALGID { get; set; }
        public string Status { get; set; }
        public DateTime AsOnDate { get; set; }
        public string Sessiontype { get; set; }
        public long AcademicYearId { get; set; }
        public string Remark { get; set; }
        public long SessionId { get; set; }
        public bool FromSession1 { get; set; }
        public bool IsSEN { get; set; }
    }
    #endregion


    
    #region Schedule Attendance Email
    public class ScheduleAttendanceEmail
    {
        public ScheduleAttendanceEmail()
        {
            SAE_FROMDT = DateTime.Now;
            SAE_TODT = DateTime.Now;
            SAE_TIME = DateTime.Now.ToString("HH:mm");
        }
        public long SAE_ID { get; set; }
        public long SAE_BSU_ID { get; set; }
        public string SAE_GRD_IDS { get; set; }
        public DateTime SAE_FROMDT { get; set; }
        public DateTime SAE_TODT { get; set; }
        public bool SAE_bENABLED { get; set; }
        public string SAE_TIME { get; set; }
        public string SAE_PARAMETERS { get; set; }
       
        public string SAE_EMAILCONTENT { get; set; }
        public string SAE_USR_ID { get; set; }
        public bool SAE_ALERT_BOTH_PARENT { get; set; }
        public int DivisionId { get; set; }

    }
    #endregion  Schedule Attendance Email

    public class AttendanceSessionType
    {
        public string AttendanceType { get; set; }
        public string AttendanceDescription { get; set; }

    }
    public class AttendanceBulkUpload
    {
        public AttendanceBulkUpload()
        {
            UploadDataModels = new List<AttendanceBulkUploadDataModel>();
        }
        public List<AttendanceBulkUploadDataModel> UploadDataModels { get; set; }
        public DataTable UploadDataModelsDT
        {
            get
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ADB_StudentNo", typeof(long));
                dt.Columns.Add("ADB_Date", typeof(string));
                UploadDataModels.ForEach(x =>
                {
                    DataRow dr = dt.NewRow();
                    dr["ADB_StudentNo"] = x.StudentNumber;
                    dr["ADB_Date"] = x.Date.ToString("dd-MMM-yyyy");
                    dt.Rows.Add(dr);
                });
                return dt;
            }
        }
        public string AttendanceType { get; set; }
        public long BSU_ID { get; set; }
        public long ACD_ID { get; set; }
        public long APD_ID { get; set; }
        public string Username { get; set; }
    }
    public class AttendanceBulkUploadDataModel
    {
        [DataTableField("ADB_StudentNo", 1, typeof(long))]
        public long StudentNumber { get; set; }
        [DataTableField("ADB_Date", 2, typeof(DateTime))]
        public DateTime Date { get; set; }
    }
    public class AttendanceBulkUploadValidationModel
    {
        public long ADB_StudentNo { get; set; }
        public long STATUS { get; set; }
    }
    public class AttendanceChartMain
    {
        public string ACD_YEAR_DESC { get; set; }
        public IEnumerable<AttendanceChart> AttendanceChart { get; set; }
    }

    public class AttendanceChart
    {
        public int Order { get; set; }
        public string TMONTH { get; set; }
        public string TOT_ATT { get; set; }
        public string ACD_DESC { get; set; }
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
        public double PresentPerc { get; set; }
    }

    public class MarkedAttendaceDetails
    {
        public MarkedAttendaceDetails()
        {
            ATTENDENCE_ANALYSIS_LST = new List<ATTENDENCE_ANALYSIS>();
            AttendenceList = new List<AttendenceList>();
            AttendenceHistory1 = new List<AttendanceChart>();
            AttendenceHistory2 = new List<AttendanceChart>();
            AttendenceHistory3 = new List<AttendanceChart>();
        }
        public BasicDetails StudentDetail { get; set; }
        public List<ATTENDENCE_ANALYSIS> ATTENDENCE_ANALYSIS_LST { get; set; }

        public List<AttendenceBySession> AttendenceBySession { get; set; }

        public List<AttendenceSessionCode> AttendenceSessionCode { get; set; }
        public List<AttendenceList> AttendenceList { get; set; }

        public List<AttendanceChart> AttendenceHistory1 { get; set; }
        public List<AttendanceChart> AttendenceHistory2 { get; set; }
        public List<AttendanceChart> AttendenceHistory3 { get; set; }

    }

    public class ATTENDENCE_ANALYSIS
    {
        public string CODE { get; set; }
        public string DESCR { get; set; }
        public string Type { get; set; }
        public string Per { get; set; }
    }
    public class AttendenceList
    {
        public DateTime AttDate { get; set; }
        public string STU_ID { get; set; }
        public string code { get; set; }
        public string Descriptions { get; set; }
        public string DayOfWeek { get; set; }
        public int day1 { get; set; }
        public int DayOfWeek1 { get; set; }
    }
    public class BasicDetails
    {
        public BasicDetails() { }
        //public BasicDetails(String _StudentId)
        //{
        //    Student_ID = _StudentId;
        //}
        public string Student_ID { get; set; }
        public string Student_Name { get; set; }
        public string Student_No { get; set; }
        public string Student_Image_url { get; set; }
        public string Student_Grade { get; set; }
        public string Student_Section { get; set; }
        public string Student_Age { get; set; }
        public string Student_Board { get; set; }
        public string Student_BSU_Name { get; set; }
        public string Student_ACD_YEAR { get; set; }

    }
    [AttributeUsage(AttributeTargets.Property)]
    public class DataTableFieldAttribute : Attribute
    {
        public DataTableFieldAttribute(string FieldName)
        {
            this.FieldName = FieldName;
        }
        public DataTableFieldAttribute(string FieldName, int order)
        {
            this.FieldName = FieldName;
            Order = order;
            DbType = typeof(object);
        }
        public DataTableFieldAttribute(string FieldName, int order, Type DbType)
        {
            this.FieldName = FieldName;
            Order = order;
            this.DbType = DbType;
        }
        public string FieldName { get; private set; }
        public int Order { get; set; }
        public Type DbType { get; private set; }
    }
    public class PropertyAndName
    {
        public string ColumnName { get; set; }
        public string PropertyName { get; set; }
        public int ColumnOrder { get; set; }
    }
    public class CurriculumRole
    {
        public int Id { get; set; }
        public int BSU_CLM_ID { get; set; }
        public bool IsSuperUser { get; set; }
        public int ACD_ID { get; set; }
        public long GSA_ID { get; set; }

        public int NEXT_ACD_ID { get; set; }
        public int PREV_ACD_ID { get; set; }
    }

    public class Curriculum
    {
        public int ACD_ID { get; set; }
        public string CLM_ID { get; set; }
        public string CLM_DESCR { get; set; }
    }
    public class GradesAccess
    {
        public string GRD_ID { get; set; }
        public string GRD_DESCR { get; set; }
        public string GRD_DISPLAY { get; set; }
        public int GRD_ORDER { get; set; }
        public string GRM_DISPLAY { get; set; }
    }
    public class CourseDetails
    {
        public int COR_ID { get; set; }
        public string COR_DESCR { get; set; }
    }
    public class GradeTemplateMaster
    {
        public int GTM_ID { get; set; }
        public string GTM_DESCR { get; set; }
    }
    public class AssessmentConfigList
    {
        public int Id { get; set; }
        public int GTM_Id { get; set; }
        public string TemPlateName { get; set; }
        public string ShortCode { get; set; }
        public string Description { get; set; }
        public string ColorCode { get; set; }
        public string Value { get; set; }
        public string Order { get; set; }
        public string GTD_Order { get; set; }

    }
    public class GradeTemplate
    {
        public int GradeTemplateDetailId { get; set; }
        public int GradeTemplateMasterId { get; set; }

        public string GradeTemplateMasterdesc { get; set; }
        public string TemPlateName { get; set; }
        public string ShortCode { get; set; }
        public string Description { get; set; }
        public string ColorCode { get; set; }
        public string Value { get; set; }
        public string GradeOrder { get; set; }
        // public string Order { get; set; }
        public int AcademicYearId { get; set; }


    }
    public class ClassAttendance
    {
        public int PeriodNo { get; set; }
        public int TimetableId { get; set; }
        public long AcademicYearId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime TimeTableDate { get; set; }
        public int WeekDay { get; set; }
        public int WeekNumber { get; set; }
        public long TeacherId { get; set; }

        public int isDailyWeekly { get; set; }
        public long SchoolGroupId { get; set; }

    }
    public class ClassAttendanceDetails
    {
        public string StudentImageUrl { get; set; }
        public string StudentName { get; set; }
        public string Nationality { get; set; }
        public long StudentNumber { get; set; }
        public long StudentId { get; set; }

        public long ParameterId { get; set; }
        public long RegisterId { get; set; }

        public DateTime AsOnDate { get; set; }

        public long AcademicYearId { get; set; }
        public string Remark { get; set; }

        public long SessionId { get; set; }

        public string DisplayDate { get; set; }

        public bool IsInReport { get; set; }
        public long IsInReportID { get; set; }

        public int PeriodNo { get; set; }
        public long DailyAttendanceParameterId { get; set; }
        public string DailyAttendanceParameterRemark { get; set; }
        public long SchoolGroupId { get; set; }
        public bool IsSEN { get; set; }
    }

    public class AttendanceWeekend
    {

        public long Id { get; set; }
        public long SchoolId { get; set; }
        public string SchoolWeekend { get; set; }
        public int SortOrder { get; set; }
    }

    public class GradeSessionType
    {
        public long GradeId { get; set; }
        public long SessionId { get; set; }
        public string SessionType { get; set; }

    }

    public class StaffCurriculumn
    {

        public long AcademicYearId { get; set; }
        public string CurriculumnName { get; set; }
    }

    public class TimeTableGroup
    {
        public long SchoolGroupId { get; set; }
        public string SchoolGroupName { get; set; }
        public int IsMemberBelongsToGroup { get; set; }
    }

    public class ParameterMappingList
    {
        public long Id { get; set; }
        public string Description { get; set; }
    }

    public class ParameterShortCutMapping
    {
        public long ParameterId { get; set; }
        public string ParameterDescription { get; set; }
    }

    public class AttendanceConfiguration
    {
        public long AcademicYearId { get; set; }
        public string TypeId { get; set; }
    }
    public class MergeServiceModelForGradeSection
    {

        public List<AttendanceConfiguration> AttendanceConfiguration { get; set; }
        public List<AuthorizedStaffDetails> AuthorizedStaffDetails { get; set; }
        public List<ParameterMappingList> ParameterMappingList { get; set; }
        public List<ParameterShortCutMapping> AttendanceParameterShortCut { get; set; }
        public List<GradeSessionType> GradeSessionType { get; set; }

    }

    public class ClassAttendanceHeaderNDetails
    {
        public List<ClassAttendance> ClassAttendanceHeader { get; set; }
        public List<ClassAttendanceDetails> ClassAttendanceDetails { get; set; }
    }
}
