﻿// <summary>
// Created By: Raj M. Gogri
// Created On: 02/Oct/2020
// </summary>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.API.Models.ChartsDashboard
{
    /// <summary>
    /// Static Wrapper class that contains chart model classes.
    /// </summary>
    public static class ChartModel
    {
        #region Filter

        /// <summary>
        /// Model class for academic years filter of Charts dashboard.
        /// </summary>
        public class AcademicYear
        {
            /// <summary>
            /// AcademicYear Id.
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// AcademicYear Name.
            /// </summary>
            [Column("Academic Year")]
            public string Name { get; set; }
        }

        /// <summary>
        /// Model class for classes filter of Charts dashboard.
        /// </summary>
        public class Class
        {
            /// <summary>
            /// Id of class.
            /// </summary>
            [Column("ClassId")]
            public string Id { get; set; }

            /// <summary>
            /// Name of class.
            /// </summary>
            [Column("Class")]
            public string Name { get; set; }
        }

        /// <summary>
        /// Model class for subject filter of Charts dashboard.
        /// </summary>
        public class FilSubject
        {
            /// <summary>
            /// Id of subject.
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Name of subject.
            /// </summary>
            [Column("Subject")]
            public string Name { get; set; }
        }

        /// <summary>
        /// Model class for Assessment filter of Charts dashboard.
        /// </summary>
        public class Assessment
        {
            /// <summary>
            /// Id of Assessment.
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Name of Assessment.
            /// </summary>
            [Column("Assessment")]
            public string Name { get; set; }
        }

        /// <summary>
        /// Model class for Teacher filter of Charts dashboard.
        /// </summary>
        public class Teacher
        {
            /// <summary>
            /// Id of Teacher.
            /// </summary>
            [Column("TeacherId")]
            public string Id { get; set; }

            /// <summary>
            /// Name of Teacher.
            /// </summary>
            [Column("Teacher")]
            public string Name { get; set; }
        }

        /// <summary>
        /// Model class for Student filter of Charts dashboard.
        /// </summary>
        public class FilStudent
        {
            /// <summary>
            /// Id of Student.
            /// </summary>
            [Column("STU_NO")]
            public string Id { get; set; }

            /// <summary>
            /// Name of Student.
            /// </summary>
            [Column("STU_NAME")]
            public string Name { get; set; }

            /// <summary>
            /// Photo of Student.
            /// </summary>
            [Column("STU_PHOTOPATH")]
            public string PhotoPath { get; set; }
        }

        /// <summary>
        /// Model class for request properties of Charts dashboard filters.
        /// </summary>
        public class FilterReq
        {
            /// <summary>
            /// Business unit id of school.
            /// </summary>
            public string BSUID { get; set; }
        }

        /// <summary>
        /// Model class for response properties of Charts dashboard filters.
        /// </summary>
        public class FilterRes
        {
            /// <summary>
            /// Academic year filter for Charts dashboard.
            /// </summary>
            public IEnumerable<AcademicYear> AcademicYear { get; set; }

            /// <summary>
            /// Class filter for Charts dashboard.
            /// </summary>
            public IEnumerable<Class> Class { get; set; }

            /// <summary>
            /// Subject filter for Charts dashboard.
            /// </summary>
            public IEnumerable<FilSubject> Subject { get; set; }

            /// <summary>
            /// Assessment filter for Charts dashboard.
            /// </summary>
            public IEnumerable<Assessment> Assessment { get; set; }

            /// <summary>
            /// Teacher filter for Charts dashboard.
            /// </summary>
            public IEnumerable<Teacher> Teacher { get; set; }
        }

        #endregion Filter

        #region Chart

        #region Average Prediction Figures

        /// <summary>
        /// Model class for average and prediction figures of Charts dashboard.
        /// </summary>
        public class AveragePredictionFigures
        {
            /// <summary>
            /// Overall average score.
            /// </summary>
            [Column("AVERAGE_OVERALL")]
            public decimal OverallAverage { get; set; }

            /// <summary>
            /// Prediction average score.
            /// </summary>
            [Column("AVERAGE_PREDICTION")]
            public decimal PredictionAverage { get; set; }

            /// <summary>
            /// Overall average score for boys.
            /// </summary>
            [Column("AVERAGE_OVERALL_B")]
            public decimal OverallAverageBoys { get; set; }

            /// <summary>
            /// Prediction average score for boys.
            /// </summary>
            [Column("AVERAGE_PREDICTION_B")]
            public decimal PredictionAverageBoys { get; set; }

            /// <summary>
            /// Overall average score for boys.
            /// </summary>
            [Column("AVERAGE_OVERALL_G")]
            public decimal OverallAverageGirls { get; set; }

            /// <summary>
            /// Prediction average score for boys.
            /// </summary>
            [Column("AVERAGE_PREDICTION_G")]
            public decimal PredictionAverageGirls { get; set; }
        }

        #endregion Average Prediction Figures

        #region Average Assessment Prediction By Year

        /// <summary>
        /// Model class for average assessment and prediction by year chart of Charts dashboard.
        /// </summary>
        public class AverageAssessmentPredictionByYear
        {
            /// <summary>
            /// Academic Year.
            /// </summary>
            [Column("Academic Year")]
            public string AcademicYear { get; set; }

            /// <summary>
            /// Average percentage score of category.
            /// </summary>
            public decimal AvgPercentage { get; set; }

            /// <summary>
            /// Category of chart.
            /// </summary>
            public string Category { get; set; }

            /// <summary>
            /// Assessment of chart.
            /// </summary>
            public string Assessment { get; set; }

            /// <summary>
            /// Assessment Date of chart.
            /// </summary>
            public DateTime? AssessmentDate { get; set; }

            /// <summary>
            /// Interval for chart Y-axis values.
            /// </summary>
            public int Interval { get; set; }
        }

        #endregion Average Assessment Prediction By Year

        #region Prediction Bucket

        /// <summary>
        /// Model class for prediction bucket chart of Charts dashboard.
        /// </summary>
        public class PredictionBucket
        {
            /// <summary>
            /// Percentage Range.
            /// </summary>
            [Column("P_Range")]
            public string PRange { get; set; }

            /// <summary>
            /// Count of students.
            /// </summary>
            [Column("STUDENTCOUNT")]
            public int StudentCount { get; set; }
        }

        #endregion Prediction Bucket

        #region Prediction By Subject

        /// <summary>
        /// Model class for prediction by subject chart of Charts dashboard.
        /// </summary>
        public class PredictionBySubject
        {
            /// <summary>
            /// Subject.
            /// </summary>
            [Column("SUBJECT")]
            public string Subject { get; set; }

            /// <summary>
            /// Count of students.
            /// </summary>
            [Column("STUDENTCOUNT")]
            public int StudentCount { get; set; }
        }

        #endregion Prediction By Subject

        #region Prediction Bucket By Student

        /// <summary>
        /// Model class for prediction bucket by student chart of Charts dashboard.
        /// </summary>
        public class PredictionBucketByStudent
        {
            /// <summary>
            /// Percentage Range.
            /// </summary>
            [Column("P_Range")]
            public string PRange { get; set; }

            /// <summary>
            /// Count of subjects.
            /// </summary>
            [Column("SUBJECTCOUNT")]
            public int SubjectCount { get; set; }
        }

        #endregion Prediction Bucket By Student

        #region Prediction By Subject By Student

        /// <summary>
        /// Model class for prediction by subject by student chart of dashboard.
        /// </summary>
        public class PredictionBySubjectByStudent
        {
            /// <summary>
            /// Subject.
            /// </summary>
            [Column("SUBJECT")]
            public string Subject { get; set; }

            /// <summary>
            /// Avg Percentage.
            /// </summary>
            public int AvgPercentage { get; set; }
        }

        #endregion Prediction By Subject By Student

        #region Average Assessment Prediction By Year

        /// <summary>
        /// Model class for all assessment data by student table of Charts dashboard.
        /// </summary>
        public class AssessmentDataByStudent
        {
            /// <summary>
            /// Academic Year.
            /// </summary>
            [Column("Academic Year")]
            public string AcademicYear { get; set; }

            /// <summary>
            /// Percentage.
            /// </summary>
            public decimal Percentage { get; set; }

            /// <summary>
            /// Subject.
            /// </summary>
            public string Subject { get; set; }

            /// <summary>
            /// Class.
            /// </summary>
            public string Class { get; set; }

            /// <summary>
            /// Assessment.
            /// </summary>
            public string Assessment { get; set; }

            /// <summary>
            /// Assessment Date .
            /// </summary>
            public DateTime? AssessmentDate { get; set; }

            /// <summary>
            /// Teacher.
            /// </summary>
            public string Teacher { get; set; }
        }

        #endregion Average Assessment Prediction By Year

        #region Average Prediction Score

        /// <summary>
        /// Model class for Average Prediction Score by student of Charts dashboard.
        /// </summary>
        public class AveragePredictionScore
        {
            /// <summary>
            /// Average Prediction Score.
            /// </summary>
            [Column("AVGPREDICTIONSCORE")]
            public decimal Score { get; set; }
        }

        #endregion Average Prediction Score

        /// <summary>
        /// Model class for request properties of Charts dashboard charts.
        /// </summary>
        public class ChartReq
        {
            /// <summary>
            /// Business unit id of school.
            /// </summary>

            [Required]
            public string BSUID { get; set; }

            /// <summary>
            /// Academic year filter(optional).
            /// </summary>
            public string AcademicYear { get; set; }

            /// <summary>
            /// Subject filter(optional).
            /// </summary>
            public string Subject { get; set; }

            /// <summary>
            /// Class filter(optional).
            /// </summary>
            public string ClassId { get; set; }

            /// <summary>
            /// Assessment filter(optional).
            /// </summary>
            public string Assessment { get; set; }

            /// <summary>
            /// Teacher filter(optional).
            /// </summary>
            public string Teacher { get; set; }

            /// <summary>
            /// Student filter(optional).
            /// </summary>
            public string StudentId { get; set; }
        }

        /// <summary>
        /// Model class for response properties of Charts dashboard charts.
        /// </summary>
        public class ChartRes
        {
            /// <summary>
            /// Average and prediction figures for Charts dashboard.
            /// </summary>
            public AveragePredictionFigures AvgPredFig { get; set; }

            /// <summary>
            /// Average assessment and prediction figures by year for Charts dashboard.
            /// </summary>
            public IEnumerable<AverageAssessmentPredictionByYear> AvgAssPredByYear { get; set; }

            /// <summary>
            /// Average marks by year and assessment for Charts dashboard.
            /// </summary>
            public IEnumerable<AverageAssessmentPredictionByYear> AvgMarksByYearAndAss { get; set; }

            /// <summary>
            /// Prediction bucket for Charts dashboard.
            /// </summary>
            public IEnumerable<PredictionBucket> PredBuck { get; set; }

            /// <summary>
            /// Prediction by subject for Charts dashboard.
            /// </summary>
            public IEnumerable<PredictionBySubject> PredBySub { get; set; }

            /// <summary>
            /// Prediction bucket by student for Charts dashboard.
            /// </summary>
            public IEnumerable<PredictionBucketByStudent> PredBuckByStu { get; set; }

            /// <summary>
            /// Prediction by subject for Charts dashboard.
            /// </summary>
            public IEnumerable<PredictionBySubjectByStudent> PredBySubByStu { get; set; }

            /// <summary>
            /// Average Prediction Score by Student for Charts dashboard.
            /// </summary>
            public AveragePredictionScore ArgPredScore { get; set; }
        }

        #endregion Chart
    }
}