﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Models;
using Phoenix.Common.Models.ViewModels;
using Phoenix.Common.Services;
using Phoenix.Common.ViewModels;
using Phoenix.Models;

namespace Phoenix.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    ////[Authorize]
    public class CommonController : ControllerBase
    {
        private readonly IEmailSettingsService _emailSettingsService;
        private readonly ILanguageSettingsService _languageSettingsService;
        private readonly ISystemImageFileSerivce _commonService;
        public CommonController(IEmailSettingsService EmailSettingsService, ILanguageSettingsService languageSettingsService,ISystemImageFileSerivce commonService)
        {
            _emailSettingsService = EmailSettingsService;
            _languageSettingsService = languageSettingsService;
            _commonService = commonService;
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 11 june 2019
        /// Description - To get email settings.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getEmailSettings")]
        [ProducesResponseType(typeof(EmailSettingsView), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetEmailSettings()
        {
            var modelList = await _emailSettingsService.GetEmailSettings();
            return Ok(modelList);
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 11 june 2019
        /// Description - To Get School Current Language settings.
        /// </summary>
        /// <param name="SchoolId">School Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSchoolCurrentLanguage")]
        [ProducesResponseType(typeof(SystemLanguage), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolCurrentLanguage(int SchoolId)
        {
            var modelList = await _languageSettingsService.GetSchoolCurrentLanguage(SchoolId);
            return Ok(modelList);
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 11 june 2019
        /// Description - To set School Current Language settings.
        /// </summary>
        /// <param name="languageId">language Id</param>
        /// <param name="schoolId">school Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("setSchoolCurrentLanguage")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult SetSchoolCurrentLanguage(int languageId, int schoolId)
        {
            var modelList = _languageSettingsService.SetSchoolCurrentLanguage(languageId,schoolId);
            return Ok(modelList);
        }
        /// <summary>
        /// Created By - Rohan Patil
        /// Created Date - 29 Feb 2020
        /// Description - To set User Current Language settings.
        /// </summary>
        /// <param name="languageId">language Id</param>
        /// <param name="userId">userId </param>
        /// <returns></returns>
        [HttpGet]
        [Route("setUserCurrentLanguage")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult SetUserCurrentLanguage(int languageId, long userId)
        {
            var result = _languageSettingsService.SetUserCurrentLanguage(languageId, userId);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Rohan Patil
        /// Created Date - 02 Mar 2020
        /// Description - To Get user Current Language settings.
        /// </summary>
        /// <param name="languageId">language Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getUserCurrentLanguage")]
        [ProducesResponseType(typeof(SystemLanguage), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserCurrentLanguage(int languageId)
        {
            var modelList = await _languageSettingsService.GetUserCurrentLanguage(languageId);
            return Ok(modelList);
        }
        /// <summary>
        /// Created By - Rohan Patil
        /// Created Date - 04 Mar 2020
        /// Description - To Get System Languages .
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getSystemLanguages")]
        [ProducesResponseType(typeof(IEnumerable<SystemLanguage>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSystemLanguages()
        {
            var list = await _languageSettingsService.GetSystemLanguages();
            return Ok(list);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 22 March 2020
        /// Description - Send Email to the user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SendEmail")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SendMail(SendMailView sendMail)
        {
            bool result = await _emailSettingsService.SendEmail(sendMail);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetSystemImageList")]
        [ProducesResponseType(typeof(IEnumerable<SystemImageViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSystemImageList()
        {
            var list = await _commonService.GetSystemImageList();
            return Ok(list);
        }

        [HttpGet]
        [Route("GetServerDateTime")]
        [ProducesResponseType(typeof(DateTime), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult GetServerDateTime()
        {
            var result = Convert.ToDateTime(DateTime.UtcNow);
            return Ok(result);
        }

    }
}