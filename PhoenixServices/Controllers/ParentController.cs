﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using Phoenix.Common.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace Phoenix.API.Controllers
{
    //NOTE: Please add version ID like v1 in api route.
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class ParentController : ControllerBase
    {
        private readonly IParentService _parentService;

        public ParentController(IParentService parentService)
        {
            _parentService = parentService;
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 07-JULY-2019
        /// Description : Get all student details for parent dashboard
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getstudentdetails")]
        [ProducesResponseType(typeof(ParentView), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentDetailsById(long id)
        {
            var user = await _parentService.GetStudentDetailsById(id);
            return Ok(user);
        }


        /// <summary>
        /// Author : Shankar Kadam
        /// Created Date : 20-August-2020
        /// Description : Get all student details for parent dashboard
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("AssignAccessPermission")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AssignAccessPermission(string StudentIdList)
        {
            var user = await _parentService.AssignAccessPermission(StudentIdList);
            return Ok(user);
        }
    }
}