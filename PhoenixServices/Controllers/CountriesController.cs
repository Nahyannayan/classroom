﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Phoenix.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryService _countryService;

        public CountriesController(ICountryService countryService)
        {
            _countryService = countryService;
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 1/May/2019
        /// Description: To get country list from country master table
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getcountries")]
        [ProducesResponseType(typeof(IEnumerable<Country>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCountries()
        {
            var result = await _countryService.GetCountries();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 1/May/2019
        /// Description: To get country by id from country master table
        /// </summary>
        /// <param name="id">country id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getcountrybyid")]
        [ProducesResponseType(typeof(Country), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCountryById(int id)
        {
            var result = await _countryService.GetCountryById(id);
            return Ok(result);
        }
    }
}