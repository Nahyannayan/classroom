﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System;
using Phoenix.Common.Models;
using Phoenix.API.Services;
using Phoenix.API.Models;
using SIMS.API.Models;

namespace Phoenix.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class GradesController : ControllerBase
    {
        private readonly IGradeService _GradeService;
        private readonly ICurriculumRoleService _curriculumRoleService;

        public GradesController( ICurriculumRoleService curriculumRoleService, IGradeService Grade_Service)
        {
            _GradeService = Grade_Service;
            _curriculumRoleService = curriculumRoleService;
        }

        [HttpGet]
        [Route("GetSubjectsByGrade")]
        [ProducesResponseType(typeof(GradesAccess), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubjectsByGrade(Int32 acd_id, string grd_id, string username = "", string IsSuperUser = "")
        {
            var result = await _GradeService.GetSubjectsByGrade(acd_id, grd_id, username, IsSuperUser);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetTeacherCourse")]
        [ProducesResponseType(typeof(CourseDetails), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTeacherCourse(long id, long Schoolid, long curriculumId)
        {
            var result = await _curriculumRoleService.GetTeacherCourse(id, Schoolid, curriculumId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GradeTemplateMaster")]
        [ProducesResponseType(typeof(GradeTemplateMaster), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GradeTemplateMaster(long acd_id)
        {
            var result = await _curriculumRoleService.GradeTemplateMaster(acd_id);
            return Ok(result);
        }
        
        [HttpGet]
        [Route("GetUserCurriculumRole")]
        [ProducesResponseType(typeof(CurriculumRole), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserCurriculumRole(string BSU_ID = "", string UserName = "")
        {
            var result = await _curriculumRoleService.GetUserCurriculumRole(BSU_ID, UserName);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetCourseGroupByCourse")]
        [ProducesResponseType(typeof(CourseGroupName), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseGroupByCourse(long courseId,long userId)
        {
            var result = await _GradeService.GetCourseGroupByCourse( courseId,  userId);
            return Ok(result);
        }
        //[HttpGet]
        //[Route("getselectlistitems")]
        //[ProducesResponseType(typeof(IEnumerable<ListItem>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
        //public async Task<IActionResult> GetSelectListItems(string listCode, string whereCondition, string whereConditionParamValues)
        //{
        //    var result = await _GradeService.GetSelectListItems(listCode, whereCondition, whereConditionParamValues);
        //    return Ok(result);
        //}

        //[HttpGet]
        //[Route("getacademicyearitems")]
        //[ProducesResponseType(typeof(IEnumerable<ListItem>), (int)HttpStatusCode.OK)]
        //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
        //public async Task<IActionResult> GetAcademicYearLists(string schoolId, int curriculumId, bool? IsCurrentCurriculum = null)
        //{
        //    var result = await _GradeService.GetAcademicYearLists(schoolId, curriculumId, IsCurrentCurriculum);
        //    return Ok(result);
        //}


    }
  
}