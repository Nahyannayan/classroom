﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;

namespace Phoenix.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SelectListController : ControllerBase
    {
        private readonly ISelectListService _selectListService;

        public SelectListController(ISelectListService selectListService)
        {
            _selectListService = selectListService;
        }

        /// <summary>
        /// Created By: Rohit Patil
        /// Created On: 07/May/2019
        /// Description: To get select list to bind the dropdown list
        /// </summary>       
        /// <returns></returns>
        [HttpGet]
        [Route("getselectlistitems")]
        [ProducesResponseType(typeof(IEnumerable<ListItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSelectListItems(string listCode, string whereCondition, string whereConditionParamValues,short languageId=1)
        {
            var result = await _selectListService.GetSelectListItemsAsync(listCode, whereCondition, whereConditionParamValues, languageId);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Mohd Ameeq
        /// Created On: 25/Nov/2020
        /// Description: To get select list of Assignment Category Master to bind the dropdown list
        /// </summary>       
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentCategoryMasterList")]
        [ProducesResponseType(typeof(IEnumerable<ListItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentCategoryMasterList()
        {
            var result = await _selectListService.GetAssignmentCategoryMasterList();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Mohd Ameeq
        /// Created On: 26/Nov/2020
        /// Description: To get select list of Assignment Category Mapping List by SchoolId
        /// </summary>       
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentCategoryMappingListBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentCategory>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentCategoryMappingListBySchoolId(long SchoolId)
        {
            var result = await _selectListService.GetAssignmentCategoryMappingListBySchoolId(SchoolId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 18 Apr 2019
        /// Description - To get subjects  filtered by user id.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsubjectlistitems")]
        [ProducesResponseType(typeof(IEnumerable<SubjectListItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubjectsByUserId(int languageId, int userId)
        {
            var subjectList = await _selectListService.GetSubjectListByUserId(languageId, userId);
            return Ok(subjectList);
        }
        /// <summary>
        /// Created By - sonali shinde
        /// Description - To get Year  filtered by user id.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAcademicYearByUserId")]
        [ProducesResponseType(typeof(IEnumerable<AcademicYearList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAcademicYearByUserId(int userId)
        {
            var subjectList = await _selectListService.GetAcademicYearListItems(userId);
            return Ok(subjectList);
        }
    }
}