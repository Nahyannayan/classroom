﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Services;
using Phoenix.Models;
using Phoenix.Common.Helpers;
using System.IO;
using File = Phoenix.Models.File;
using System.Security.Principal;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using Serilog;

namespace Phoenix.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class FileUploaderController : ControllerBase
    {
        #region Constant variable
        //Resources Folder
        public const string ResourcesDir = "/Resources";
        public const string ContentDir = "/Content";
        //Blog & File Module 
        public const string BlogDir = "/Resources/BlogContent";
        public const string FileDir = "/Resources/FileContent";
        public const string GroupImageDir = "/Resources/GroupImage";

        //Assignment Files constants
        public const string AssignmentFilesDir = "/Resources/AssignmentFiles";
        public const string AssignmentTaskFilesDir = "/Resources/AssignmentTaskFiles";

        //Observation Files constants
        public const string ObservationFilesDir = "/Resources/ObservationFiles";
        public const string ObservationTaskFilesDir = "/Resources/ObservationTaskFiles";
        //Error Screen Shots
        public const string ErrorScreenShotDir = "/Resources/ErrorScreens";
        public const string ErrorScreenShots = "/Resources/ErrorScreenShots";
        //Quiz File
        public const string QuizQuestion = "/Resources/Quiz/";
        //Suggestion File
        public const string SuggestionFile = "/SuggestionFiles";
        //Content Resource File
        public const string ContentResourceFiles = "/ContentResourceFiles";
        //Group Quiz Feedback File
        public const string GroupQuizFeedbackFiles = "/QuizFeedback/";
        //Assignment Task Quiz Feedback File
        public const string TaskQuizFeedbackFiles = "/TaskFeedback/";

        public const string ProgressTracker = "/Resources/ProgressTrackerFiles";
        public const string StudentCoverPath = "/Resources/Content/ProfilePhotos/Students/";

        #endregion

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
        int dwLogonType, int dwLogonProvider, out SafeAccessTokenHandle phToken);

        private readonly IAssignmentService _assignmentService;
        private readonly IFileService _fileService;
        private readonly IFolderService _folderService;
        private readonly IConfiguration _config;
        private readonly ISchoolGroupService _schoolGroupService;
        SafeAccessTokenHandle safeAccessTokenHandle;
        private readonly ILogger _logger;

        public FileUploaderController(IAssignmentService assignmentService, IFileService fileService,
                        IFolderService folderService, IConfiguration configuration, ILogger logger, ISchoolGroupService schoolGroupService)
        {
            _assignmentService = assignmentService;
            _fileService = fileService;
            _folderService = folderService;
            _config = configuration;
            _logger = logger;
            _schoolGroupService = schoolGroupService;
        }

        private void ImpersonateLogin(out SafeAccessTokenHandle safeAccessTokenHandle)
        {
            string userName = _config["Impersonate:userName"];
            string password = _config["Impersonate:password"];
            string domainName = _config["Impersonate:domainName"];

            const int LOGON32_PROVIDER_DEFAULT = 0;
            //This parameter causes LogonUser to create a primary token.     
            const int LOGON32_LOGON_INTERACTIVE = 2;
            // Call LogonUser to obtain a handle to an access token. 
            bool returnValue = LogonUser(userName, domainName, password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, out safeAccessTokenHandle);
            if (!returnValue)
            {
                int ret = Marshal.GetLastWin32Error();
                _logger.Error("LogonUser failed with error code", ret);
                throw new System.ComponentModel.Win32Exception(ret);
            }
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 11/December/2019
        /// Description: To upload file on file server and add file record in database
        /// Model is to insert in database and postedFile is actual postedfile from application to save on file server 
        /// </summary>
        /// <param name="file">postedfile</param>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploadfile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public IActionResult UploadFile(IFormFile file, [FromForm] File model)
        {
            var result = false;
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0)
            {
                //_logger.Information("fileupload started");
                try
                {
                    string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                    string fileContent = _config["FileServer:WritePath"] + FileDir;
                    string fileModule = _config["FileServer:WritePath"] + FileDir + "/Module_" + model.ModuleId;
                    string filePath = _config["FileServer:WritePath"] + FileDir + "/Module_" + model.ModuleId + "/User_" + model.CreatedBy;

                    //For impersonate on UAT & production server, please remove below commented lines
                    // _logger.Information("\n Impersonate Start ");
                    ImpersonateLogin(out safeAccessTokenHandle);

                    WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                    {
                        //_logger.Information("\n Impersonate success: ");
                        CommonHelper.CreateDestinationFolder(resourceDir);
                        CommonHelper.CreateDestinationFolder(fileContent);
                        CommonHelper.CreateDestinationFolder(fileModule);
                        CommonHelper.CreateDestinationFolder(filePath);
                        var _fileTypeList = _fileService.GetFileTypes("").Result;
                        var fileName = !string.IsNullOrWhiteSpace(file.FileName) ? file.FileName : model.FileName;

                        var extension = Path.GetExtension(fileName).Replace(".", "");

                        fileName = Guid.NewGuid() + "_" + fileName;
                        string relativePath = FileDir + "/Module_" + model.ModuleId + "/User_" + model.CreatedBy + "/" + fileName;

                        var extId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;

                        fileName = Path.Combine(filePath, fileName);
                        using (var stream = System.IO.File.Create(fileName))
                        {
                            file.CopyTo(stream);
                        }

                        model.FilePath = relativePath;
                        model.FileTypeId = extId;

                        result = _fileService.Insert(model);
                    });

                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Error while uploading files");
                }
            }


            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 9/January/2020
        /// Description: To upload observation file on file server 
        /// </summary>
        /// <param name="file">postedfile</param>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploadobservationfile")]
        [ProducesResponseType(typeof(ObservationFile), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public IActionResult UploadObservationFile(IFormFile file, [FromForm] long userId)
        {
            var result = new ObservationFile();
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0)
            {

                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string fileContent = _config["FileServer:WritePath"] + ObservationFilesDir;
                string filePath = _config["FileServer:WritePath"] + ObservationFilesDir + "/User_" + userId.ToString();

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                //ImpersonateLogin(out safeAccessTokenHandle);

                //WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                //{
                //    //_logger.Information("\n Impersonate success: ");
                //    CommonHelper.CreateDestinationFolder(resourceDir);
                //    CommonHelper.CreateDestinationFolder(fileContent);
                //    CommonHelper.CreateDestinationFolder(filePath);
                var fileName = file.FileName;


                //    fileName = Guid.NewGuid() + "_" + fileName;
                //    string relativePath = ObservationFilesDir + "/User_" + userId.ToString() + "/" + fileName;

                //    result.FileName = file.FileName;
                //    result.UploadedFileName = relativePath;

                //    fileName = Path.Combine(filePath, fileName);
                //    using (var stream = System.IO.File.Create(fileName))
                //    {
                //        file.CopyTo(stream);
                //    }
                using (var stream = System.IO.File.Create(fileName))
                {
                    file.CopyTo(stream);
                    // CommonSharepointHelper sh = new CommonSharepointHelper();
                    // Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint();
                    //string SharePTUploadedFilePath = sh.APIUploadFilesAsPerModule("OOS", "MyFiles", stream);

                }

                //  });

                return Ok(result);


            }
            else
            {
                return BadRequest();
            }

        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 9/January/2020
        /// Description: To upload assignment file on file server 
        /// </summary>
        /// <param name="file">postedfile</param>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploadassignmentfile")]
        [ProducesResponseType(typeof(AssignmentFile), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public IActionResult UploadAssignmentFile(IFormFile file, [FromForm] long userId)
        {
            var result = new AssignmentFile();
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0 && userId > 0)
            {

                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string fileContent = _config["FileServer:WritePath"] + AssignmentFilesDir;
                string filePath = _config["FileServer:WritePath"] + AssignmentFilesDir + "/User_" + userId.ToString();

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                ImpersonateLogin(out safeAccessTokenHandle);

                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    //_logger.Information("\n Impersonate success: ");
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    CommonHelper.CreateDestinationFolder(fileContent);
                    CommonHelper.CreateDestinationFolder(filePath);
                    var fileName = file.FileName;

                    var extension = Path.GetExtension(fileName).Replace(".", "");

                    fileName = Guid.NewGuid() + "_" + fileName;
                    string relativePath = AssignmentFilesDir + "/User_" + userId.ToString() + "/" + fileName;

                    result.FileName = file.FileName;
                    result.FileExtension = extension;
                    result.UploadedFileName = relativePath;

                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }
                });

                return Ok(result);


            }
            else
            {
                return BadRequest();
            }

        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 9/January/2020
        /// Description: To upload student assignment file on file server 
        /// </summary>
        /// <param name="file">postedfile</param> 
        /// <param name="assignmentId">assignmentId</param>
        /// <param name="userId">current login userId</param>
        /// <param name="studentId">studentId</param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploadstudentassignmentfile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public IActionResult UploadStudentssignmentFile(IFormFile file, [FromForm] int assignmentId, [FromForm] int userId, [FromForm] int studentId)
        {
            var studentAssignmentFiles = new List<StudentAssignmentFile>();
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0 && assignmentId > 0 && userId > 0 && studentId > 0)
            {

                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string fileContent = _config["FileServer:WritePath"] + AssignmentFilesDir;
                string filePath = _config["FileServer:WritePath"] + AssignmentFilesDir + "/User_" + userId.ToString();

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                ImpersonateLogin(out safeAccessTokenHandle);

                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    //_logger.Information("\n Impersonate success: ");
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    CommonHelper.CreateDestinationFolder(fileContent);
                    CommonHelper.CreateDestinationFolder(filePath);

                    //foreach (var file in files)
                    //{
                    var fileName = file.FileName;

                    var extension = Path.GetExtension(fileName).Replace(".", "");

                    fileName = Guid.NewGuid() + "_" + fileName;
                    string relativePath = AssignmentFilesDir + "/User_" + userId.ToString() + "/" + fileName;


                    studentAssignmentFiles.Add(new StudentAssignmentFile
                    {
                        FileName = file.FileName,
                        FileExtension = extension,
                        AssignmentId = assignmentId,
                        UploadedFileName = relativePath,
                        PathtoDownload = relativePath
                    });

                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }
                    //}
                });

                //var visitorInfo = new VisitorInfo();
                //string IpAddress = visitorInfo.GetIpAddress();
                //string HostIpAddress = visitorInfo.GetClientIPAddress();
                //string IpDetails = IpAddress + ":" + HostIpAddress;
                //string IpDetails = "172.25.160.3:::1";
                var result = _assignmentService.UploadStudentAssignmeentFiles(studentAssignmentFiles, studentId, assignmentId, "");
                return Ok(result);


            }
            else
            {
                return BadRequest();
            }

        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 9/January/2020
        /// Description: To upload task file on file server 
        /// </summary>
        /// <param name="file">postedfile</param>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploadtaskfile")]
        [ProducesResponseType(typeof(TaskFile), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public IActionResult UploadTaskFile(IFormFile file, [FromForm] long userId)
        {
            var result = new TaskFile();
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0 && userId > 0)
            {

                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string fileContent = _config["FileServer:WritePath"] + AssignmentTaskFilesDir;
                string filePath = _config["FileServer:WritePath"] + AssignmentTaskFilesDir + "/User_" + userId.ToString();

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                ImpersonateLogin(out safeAccessTokenHandle);

                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    //_logger.Information("\n Impersonate success: ");
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    CommonHelper.CreateDestinationFolder(fileContent);
                    CommonHelper.CreateDestinationFolder(filePath);
                    var fileName = file.FileName;

                    var extension = Path.GetExtension(fileName).Replace(".", "");

                    fileName = Guid.NewGuid() + "_" + fileName;
                    string relativePath = AssignmentTaskFilesDir + "/User_" + userId.ToString() + "/" + fileName;

                    result.FileName = file.FileName;
                    result.FileExtension = extension;
                    result.UploadedFileName = relativePath;

                    //fileName = string.Concat(filePath, fileName);
                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }
                });

                return Ok(result);


            }
            else
            {
                return BadRequest();
            }

        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 9/January/2020
        /// Description: To upload student task file on file server 
        /// </summary>
        /// <param name="file">postedfile</param>
        /// <param name="taskId">taskId</param>
        /// <param name="userId">current login userId</param>
        /// <param name="studentId">studentId</param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploadstudenttaskfile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public IActionResult UploadStudentTaskFile(IFormFile file, [FromForm] int taskId, [FromForm] int userId, [FromForm] int studentId)
        {
            var studentTaskFiles = new List<StudentTaskFile>();
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0 && taskId > 0 && userId > 0 && studentId > 0)
            {

                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string fileContent = _config["FileServer:WritePath"] + AssignmentTaskFilesDir;
                string filePath = _config["FileServer:WritePath"] + AssignmentTaskFilesDir + "/User_" + userId.ToString();

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                ImpersonateLogin(out safeAccessTokenHandle);

                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    //_logger.Information("\n Impersonate success: ");
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    CommonHelper.CreateDestinationFolder(fileContent);
                    CommonHelper.CreateDestinationFolder(filePath);

                    //foreach (var file in files)
                    //{

                    var fileName = file.FileName;

                    var extension = Path.GetExtension(fileName).Replace(".", "");

                    fileName = Guid.NewGuid() + "_" + fileName;
                    string relativePath = AssignmentTaskFilesDir + "/User_" + userId.ToString() + "/" + fileName;

                    studentTaskFiles.Add(new StudentTaskFile
                    {
                        FileName = file.FileName,
                        TaskId = taskId,
                        UploadedFileName = relativePath

                    });

                    //fileName = string.Concat(filePath, fileName);
                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }
                    // }
                });

                var result = _assignmentService.UploadStudentTaskFiles(studentTaskFiles, studentId, taskId, "");
                return Ok(result);
            }
            else
            {
                return BadRequest();
            }

        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 10/January/2020
        /// Description: To upload chatter image file on file server 
        /// </summary>
        /// <param name="file">postedfile</param>
        /// <param name="blogTypeId">userId</param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploadchatterImage")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public IActionResult UploadChatterImage(IFormFile file, [FromForm] int blogTypeId)
        {
            var result = string.Empty;
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0 && blogTypeId > 0)
            {

                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string fileContent = _config["FileServer:WritePath"] + BlogDir;
                string filePath = _config["FileServer:WritePath"] + BlogDir + "/Blog_" + blogTypeId.ToString();

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                ImpersonateLogin(out safeAccessTokenHandle);

                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    //_logger.Information("\n Impersonate success: ");
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    CommonHelper.CreateDestinationFolder(fileContent);
                    CommonHelper.CreateDestinationFolder(filePath);
                    var fileName = file.FileName;

                    var extension = Path.GetExtension(fileName).Replace(".", "");

                    fileName = Guid.NewGuid() + "_" + fileName;
                    string relativePath = BlogDir + "/Blog_" + blogTypeId.ToString() + "/" + fileName;

                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }

                    result = relativePath;
                });

                return Ok(result);


            }
            else
            {
                return BadRequest();
            }

        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 10/January/2020
        /// Description: To upload chatter image file on file server 
        /// </summary>
        /// <param name="file">postedfile</param>
        /// <param name="isParent">isParent</param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploadprofileimage")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public IActionResult UploadProfilemage(IFormFile file, [FromForm] bool isParent)
        {
            var result = string.Empty;
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0)
            {

                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string fileContent = _config["FileServer:WritePath"] + "/Content";
                string profilePhoto = _config["FileServer:WritePath"] + ResourcesDir + "/Content/ProfilePhotos";
                string filePath = _config["FileServer:WritePath"] + (isParent ? "" + ResourcesDir + "/Content/ProfilePhotos/Parents" : "" + ResourcesDir + "/Content/ProfilePhotos/Teachers");

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                ImpersonateLogin(out safeAccessTokenHandle);

                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    //_logger.Information("\n Impersonate success: ");
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    CommonHelper.CreateDestinationFolder(fileContent);
                    CommonHelper.CreateDestinationFolder(profilePhoto);
                    CommonHelper.CreateDestinationFolder(filePath);
                    var fileName = file.FileName;

                    var extension = Path.GetExtension(fileName).Replace(".", "");

                    fileName = string.Format("{0}{1}", Guid.NewGuid(), Path.GetExtension(file.FileName));
                    result = fileName;

                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }


                });

                return Ok(result);


            }
            else
            {
                return BadRequest();
            }

        }

        /// <summary>
        /// Created By: Vinayak Yenpure
        /// Created On: 30/June/2020
        /// Description: To upload suggestion file on file server 
        /// </summary>
        /// <param name="file">postedfile</param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploadsuggestionfile")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public string UploadSuggestionFile(IFormFile file)
        {
            var result = new AssignmentFile();
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0)
            {
                string GUID = Guid.NewGuid().ToString();
                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string content = resourceDir + ContentDir;
                string filePath = content + SuggestionFile;

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                ImpersonateLogin(out safeAccessTokenHandle);

                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    //_logger.Information("\n Impersonate success: ");
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    CommonHelper.CreateDestinationFolder(content);
                    CommonHelper.CreateDestinationFolder(filePath);
                    var fileName = String.Format("{0}{1}", GUID, Path.GetExtension(file.FileName));

                    //var extension = Path.GetExtension(fileName).Replace(".", "");

                    //fileName = Guid.NewGuid() + "_" + fileName;
                    string relativePath = filePath + fileName;

                    result.FileName = file.FileName;
                    result.FileExtension = Path.GetExtension(file.FileName);
                    result.UploadedFileName = relativePath;

                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }
                });

                return result.FileName;


            }
            else
            {
                return String.Empty;
            }

        }

        /// <summary>
        /// Created By: Vinayak Yenpure
        /// Created On: 30/June/2020
        /// Description: To upload suggestion file on file server 
        /// </summary>
        /// <param name="file">postedfile</param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploadcontentresource")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public string UploadContentResource(IFormFile file)
        {
            var result = new AssignmentFile();
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0)
            {
                string GUID = Guid.NewGuid().ToString();
                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string content = resourceDir + ContentDir;
                string filePath = content + ContentResourceFiles;

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                ImpersonateLogin(out safeAccessTokenHandle);

                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    //_logger.Information("\n Impersonate success: ");
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    CommonHelper.CreateDestinationFolder(content);
                    CommonHelper.CreateDestinationFolder(filePath);
                    var fileName = String.Format("{0}{1}", GUID, Path.GetExtension(file.FileName));

                    //var extension = Path.GetExtension(fileName).Replace(".", "");

                    //fileName = Guid.NewGuid() + "_" + fileName;
                    string relativePath = filePath + fileName;

                    result.FileName = file.FileName;
                    result.FileExtension = Path.GetExtension(file.FileName);
                    result.UploadedFileName = relativePath;

                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }
                });

                return result.FileName;


            }
            else
            {
                return String.Empty;
            }

        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 9/August/2020
        /// Description: To upload group images
        /// </summary>
        /// <param name="file"></param>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadGroupImage")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public string UploadGroupImage(IFormFile file, [FromForm] long schoolId, [FromForm] long userId)
        {
            string relativePath = string.Empty;
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0)
            {
                string GUID = Guid.NewGuid().ToString();
                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string groupImageDir = _config["FileServer:WritePath"] + GroupImageDir;
                string groupSchoolDir = _config["FileServer:WritePath"] + GroupImageDir + "/School_" + schoolId.ToString();
                string filePath = _config["FileServer:WritePath"] + GroupImageDir + "/School_" + schoolId.ToString() + "/User_" + userId.ToString();

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                ImpersonateLogin(out safeAccessTokenHandle);

                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    //_logger.Information("\n Impersonate success: ");
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    CommonHelper.CreateDestinationFolder(groupImageDir);
                    CommonHelper.CreateDestinationFolder(groupSchoolDir);
                    CommonHelper.CreateDestinationFolder(filePath);
                    var fileName = String.Format("{0}{1}", GUID, Path.GetExtension(file.FileName));

                    //var extension = Path.GetExtension(fileName).Replace(".", "");

                    //fileName = Guid.NewGuid() + "_" + fileName;
                    relativePath = GroupImageDir + "/School_" + schoolId.ToString() + "/User_" + userId.ToString() + "/" + fileName;

                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }
                });

                return relativePath;


            }
            else
            {
                return String.Empty;
            }

        }

        /// <summary>
        /// Created By: Vinayak Yenpure
        /// Created On: 30/June/2020
        /// Description: To upload quiz feedback
        /// </summary>
        /// <param name="file">postedfile</param>
        /// <param name="type">type</param>
        /// <returns></returns>
        [HttpPost]
        [Route("uploadQuizFeedback")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public string UploadQuizFeedback(IFormFile file, string type)
        {
            var result = new AssignmentFile();
            string resourcePath = String.Empty;
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0)
            {
                string directory = type == "Group" ? GroupQuizFeedbackFiles : type == "Task" ? TaskQuizFeedbackFiles : String.Empty;
                string GUID = Guid.NewGuid().ToString();
                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                //string content = resourceDir + ContentDir;
                string filePath = resourceDir + directory;

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                ImpersonateLogin(out safeAccessTokenHandle);

                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    //_logger.Information("\n Impersonate success: ");
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    //CommonHelper.CreateDestinationFolder(content);
                    CommonHelper.CreateDestinationFolder(filePath);
                    var fileName = String.Format("{0}{1}", GUID, Path.GetExtension(file.FileName));

                    //var extension = Path.GetExtension(fileName).Replace(".", "");

                    //fileName = Guid.NewGuid() + "_" + fileName;
                    string relativePath = filePath + fileName;
                    resourcePath = ResourcesDir + directory + fileName;
                    result.FileName = file.FileName;
                    result.FileExtension = Path.GetExtension(file.FileName);
                    result.UploadedFileName = relativePath;

                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }
                });

                return resourcePath;


            }
            else
            {
                return String.Empty;
            }

        }


        [HttpPost]
        [Route("UploadProgressTrackerAttachment")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public IActionResult UploadProgressTrackerAttachment(IFormFile file, long schoolId, long userId)
        {
            var result = string.Empty;
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0 && schoolId > 0 && userId > 0)
            {

                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string fileContent = _config["FileServer:WritePath"] + ProgressTracker;
                string filePath = _config["FileServer:WritePath"] + ProgressTracker + "/School_" + schoolId + "/User_" + userId;

                //For impersonate on UAT & production server, please remove below commented lines
                // _logger.Information("\n Impersonate Start ");
                ImpersonateLogin(out safeAccessTokenHandle);

                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    //_logger.Information("\n Impersonate success: ");
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    CommonHelper.CreateDestinationFolder(fileContent);
                    CommonHelper.CreateDestinationFolder(filePath);
                    var fileName = file.FileName;

                    var extension = Path.GetExtension(fileName).Replace(".", "");

                    fileName = Guid.NewGuid() + "_" + fileName;
                    string relativePath = ProgressTracker + "/School_" + schoolId.ToString() + "/User_" + userId.ToString() + "/" + fileName;

                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }

                    result = relativePath;
                });

                return Ok(result);


            }
            else
            {
                return BadRequest();
            }

        }


        /// <summary>
        /// CREATED BY - Rohit Patil
        /// CREATED DATE - 10-Oct-2020
        /// DESCRIPTION - Uploads student covert image file
        /// </summary>
        /// <param name="file"></param>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadStudentCoverImage")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Created)]
        [RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
        public IActionResult UploadStudentCoverImage(IFormFile file, [FromForm] long schoolId, [FromForm]long userId)
        {
            var result = string.Empty;
            //_logger.Information("\n File size: "+file.Length.ToString());
            if (file.Length > 0 && schoolId > 0 && userId > 0)
            {
                string resourceDir = _config["FileServer:WritePath"] + ResourcesDir;
                string fileContent = _config["FileServer:WritePath"] + StudentCoverPath;
                string filePath = _config["FileServer:WritePath"] + StudentCoverPath + schoolId.ToString() + "/" + userId.ToString() + "/";
                ImpersonateLogin(out safeAccessTokenHandle);
                WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                {
                    CommonHelper.CreateDestinationFolder(resourceDir);
                    CommonHelper.CreateDestinationFolder(resourceDir+"/"+"Content");
                    CommonHelper.CreateDestinationFolder(resourceDir + "/" + "Content"+"/ProfilePhotos");
                    CommonHelper.CreateDestinationFolder(resourceDir + "/" + "Content" + "/ProfilePhotos"+"/Students");
                    CommonHelper.CreateDestinationFolder(fileContent);
                    CommonHelper.CreateDestinationFolder(fileContent + "/" + schoolId.ToString());
                    CommonHelper.CreateDestinationFolder(filePath);
                    var fileName = file.FileName;
                    var extension = Path.GetExtension(fileName).Replace(".", "");
                    fileName = Guid.NewGuid() + "_" + fileName;
                    string relativePath = StudentCoverPath + "/" + schoolId.ToString() + "/" + userId.ToString() + "/" + fileName;
                    fileName = Path.Combine(filePath, fileName);
                    using (var stream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(stream);
                    }
                    result = relativePath;
                });
                return Ok(result);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}