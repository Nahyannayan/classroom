﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Models;
using Phoenix.API.Services;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Controllers
{
    //NOTE: Please add version ID like v1 in api route.
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class StudentController : ControllerBase
    {
        private readonly IStudentService _studentService;

        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        /// <summary>
        /// Added By: Deepak Singh
        /// Created: 23-Apri-2019
        /// Description: To Get all records
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getstudents")]
        [ProducesResponseType(typeof(StudentDTO), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<StudentDTO>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> StudentAsync(string ids = null)
        {
            if (!string.IsNullOrEmpty(ids))
            {
                var items = await GetStudentsByIdsAsync(ids);

                if (!items.Any())
                {
                    return BadRequest("ids value invalid. Must be comma-separated list of numbers");
                }

                return Ok(items);
            }

            var studentList = await _studentService.GetAll();

            var totalItems = studentList.LongCount();

            var itemsOnPage = studentList
                .OrderBy(c => c.FirstName)
                .ToList();


            return Ok(studentList);
        }

        private async Task<List<StudentDTO>> GetStudentsByIdsAsync(string ids)
        {
            var numIds = ids.Split(',').Select(id => (Ok: int.TryParse(id, out int x), Value: x));

            if (!numIds.All(nid => nid.Ok))
            {
                return new List<StudentDTO>();
            }

            var idsToSelect = numIds
                .Select(id => id.Value);

            //var items = await _catalogContext.CatalogItems.Where(ci => idsToSelect.Contains(ci.Id)).ToListAsync();
            var studentList = await _studentService.GetAll();
            var items = studentList.ToAsyncEnumerable<StudentDTO>().Where(r => idsToSelect.Contains(r.Id));

            return await items.ToList<StudentDTO>();
        }

        /// <summary>
        /// Added By: Deepak Singh
        /// Created: 23-Apri-2019
        /// Description: To record by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getstudent/{id:int}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(StudentDTO), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<StudentDTO>> StudentByIdAsync(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var student = await _studentService.Get(id);


            if (student != null)
            {
                return student;
            }

            return NotFound();
        }

        /// <summary>
        /// Added By: Deepak Singh
        /// Created: 23-Apri-2019
        /// Description: Add new records
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addstudent")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public ActionResult CreateStudentA([FromBody] StudentDTO student)
        {
            _studentService.Insert(student);
            return Ok();
        }

        /// <summary>
        /// Added By: Deepak Singh
        /// Created: 23-Apri-2019
        /// Description: Update student details
        /// </summary>
        /// <param name="studentToUpdate"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updatestudent/{id:int}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public ActionResult UpdateStudent([FromQuery] int id, [FromBody] StudentDTO studentToUpdate)
        {
            _studentService.Update(studentToUpdate);
            return Ok();
        }

        /// <summary>
        /// Added By: Deepak Singh
        /// Created: 23-Apri-2019
        /// Description: Delete student
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletestudent/{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public ActionResult DeleteStudent(int id)
        {
            _studentService.Delete(id);
            return Ok();
        }


        /// <summary>
        /// Author : Rohit Patil
        /// Created Date : 17-JUNE-2019
        /// Description : To fetch all Studnet detail by user id
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentByUserId")]
        [ProducesResponseType(typeof(StudentDetail), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentByUserId(long id)
        {
            var user = await _studentService.GetStudentByUserId(id);
            return Ok(user);
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 04-JULY-2019
        /// Description : Get all student details by parentID
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentByFamily")]
        [ProducesResponseType(typeof(IEnumerable<StudentDetail>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentByFamily(long id)
        {
            var user = await _studentService.GetStudentByFamily(id);
            return Ok(user);
        }

        [HttpGet]
        [Route("GetStudentDashboard")]
        [ProducesResponseType(typeof(StudentDashboard), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentDashboard(long id)
        {
            var user = await _studentService.GetStudentDashboard(id);
            return Ok(user);
        }

        /// <summary>
        /// Description : Returns student portfolio details like student details, skills, acheivements etc
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="languageId"></param>
        [HttpGet]
        [Route("getStudentPortfolioInformation")]
        [ProducesResponseType(typeof(StudentPortfolio), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentPortfolioInformation(int userId,short languageId=1)
        {
            var studentDetails = await _studentService.GetStudentPortfolioInformation(userId, languageId);
            return Ok(studentDetails);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date -  12 Feb 2020
        /// Description - Save Student Regarding me section
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveStudentDescription")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveStudentDescription([FromBody] Student model)
        {
            bool result = await _studentService.SaveStudentDescription(model);
            return Ok(result);
        }

        /// <summary>
        /// Created by - Rahul Verma
        /// Created Date -  8th June 2020
        /// Description - Save About Informations (Name,Picture,Mobile Number and Address) for Student
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveStudentAboutMe")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveStudentAboutMe([FromBody] SchoolBadge model)
        {
            bool result = await _studentService.SaveStudentAboutMe(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 29th July 2020
        /// Description - Gets student profile descriptions and badges.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentProfileDetails")]
        [ProducesResponseType(typeof(StudentProfileDetail), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentProfileDetails(long userId)
        {
            var details = await _studentService.GetStudentProfileDetails(userId);
            return Ok(details);
        }

        /// <summary>
        /// Created By - Rohit Pati
        /// Created Date - 29th July 2020
        /// Description - save student profile/cover images
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveStudentProfileImages")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveStudentProfileImages(Student model)
        {
            var result = await _studentService.SaveStudentProfileImages(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 03 Aug 2020
        /// Description - show/hide student portfolio modules
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateStudentPortfolioSectionDetails")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateStudentPortfolioSectionDetails(StudentProfileSections model)
        {
            var result = await _studentService.UpdateStudentPortfolioSectionDetails(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 22 NOV 2020
        /// Description - To check if the resource is deleted
        /// </summary>
        /// <param name="sourceId"></param>
        /// <param name="notificationType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("checkIfTheResourceDeleted")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> CheckIfTheResourceDeleted(int sourceId, string notificationType)
        {
            var result = _studentService.CheckIfTheResourceDeleted(sourceId, notificationType);
            return Ok(result);
        }
    }
}