﻿using Microsoft.Extensions.Configuration;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using System.IO;
using System.Security;

namespace Phoenix.API.Helpers
{
    public class SharePointAPIHelper
    {
        public string SharePointURL { get; set; }
        public string SharePointUser { get; set; }
        public string SharePointPassword { get; set; }
        private readonly IConfiguration _config;

        public SharePointAPIHelper(IConfiguration configuration)
        {
            _config = configuration;
        }
        public ClientContext LoginToSharepoint()
        {
            SharePointURL = _config["SharePointServer:SharepointURL"];
            SharePointUser = _config["SharePointServer:SharepointUser"];
            SharePointPassword = _config["SharePointServer:SharepointPassword"];
            ClientContext cxt = new ClientContext(SharePointURL);
            System.Security.SecureString s = new System.Security.SecureString();
            foreach (char c in SharePointPassword.ToCharArray())
                s.AppendChar(c);
            cxt.Credentials = new Microsoft.SharePoint.Client.SharePointOnlineCredentials(SharePointUser, SharePointPassword);
            Microsoft.SharePoint.Client.Web web = cxt.Web;
            cxt.Load(web);
            cxt.ExecuteQueryAsync();
            //string webtitle = web.Title;
            return cxt;
        }


        public static bool CreateFolder(ref ClientContext cxt, ref List list, string FolderNAme)
        {
            bool CreateFolder = false;
            list.EnableFolderCreation = true;
            list.Update();
            cxt.ExecuteQueryAsync();
            ListItemCreationInformation info = new ListItemCreationInformation();
            info.UnderlyingObjectType = FileSystemObjectType.Folder;
            info.LeafName = FolderNAme.Trim(); // Trim for spaces.Just extra check
            ListItem newItem = list.AddItem(info);
            newItem["Title"] = FolderNAme;
            newItem.Update();
            cxt.ExecuteQueryAsync();
            CreateFolder = true;
            return CreateFolder;
        }

        public static bool CreateLibrary(ref ClientContext cxt, string LibraryName)
        {
            bool CreateLibrary = false;
            ListCreationInformation lci = new ListCreationInformation();
            lci.Description = LibraryName;
            lci.Title = LibraryName;
            lci.TemplateType = System.Convert.ToInt32(ListTemplateType.DocumentLibrary);
            List newLib = cxt.Web.Lists.Add(lci);
            cxt.Load(newLib);
            cxt.ExecuteQueryAsync();
            CreateLibrary = true;
            return CreateLibrary;
        }

        public static bool IsLibraryExists(ref ClientContext clientContext, string listTitle)
        {
            bool IsLibraryExists = false;
            try
            {
                var web = clientContext.Web;
                ListCollection lists = web.Lists;
                clientContext.Load(lists);
                clientContext.ExecuteQueryAsync();
                foreach (List L in lists)
                {
                    if (L.Title.Equals(listTitle, StringComparison.CurrentCultureIgnoreCase))
                    {
                        IsLibraryExists = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return IsLibraryExists;
        }

        public static bool CheckFileExists(ref ClientContext ctx, string FilePath)
        {
            bool CheckFileExists = false;
            try
            {
                var web = ctx.Web;
                ctx.Load(web);
                string url = new Uri(FilePath).AbsolutePath;
                Microsoft.SharePoint.Client.File f = web.GetFileByServerRelativeUrl(url);
                ctx.Load(f);
                ctx.ExecuteQueryAsync();
                if (f.Exists)
                    CheckFileExists = true;
            }
            catch (Exception ex)
            {

            }
            return CheckFileExists;
        }

        public static string UploadFilesAsPerModule(ref ClientContext clientContext, string LibraryName, FileModulesConstants ModuleName, FileStream file)
        {
            bool UploadFile = false;
            bool LibraryExists = false;
            bool IsFolderExists = false;
            string ShareableLink = "";
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);

            if (!IsLibraryExists(ref clientContext, LibraryName))
                LibraryExists = CreateLibrary(ref clientContext, LibraryName); // Creates library if not exists
            else
                LibraryExists = true;
            if (LibraryExists)
            {
                List List = clientContext.Web.Lists.GetByTitle(LibraryName);
                clientContext.Load(List);
                clientContext.Load(List.RootFolder);
                clientContext.Load(List.RootFolder.Folders);
                clientContext.ExecuteQueryAsync();
                FolderCollection folders = List.RootFolder.Folders;
                foreach (Folder fldr in folders)
                {
                    if (fldr.Name == ModuleNametoUpload)
                    {
                        IsFolderExists = true;
                        break;
                    }
                }
                if (!IsFolderExists)
                {
                    if (CreateFolder(ref clientContext, ref List, ModuleNametoUpload))
                        IsFolderExists = true;
                }
                if (IsFolderExists)
                {
                    string DestinationFileName = "";
                    string ServerDomain = "";
                    ServerDomain = new Uri(clientContext.Url).GetLeftPart(UriPartial.Authority);
                    DestinationFileName = file.Name;
                    var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + ModuleName, DestinationFileName);
                    while (CheckFileExists(ref clientContext, fileUrl))
                    {
                        var random = new Random(DateTime.Now.Millisecond);
                        int randomNumber = random.Next(1, 500000);
                        DestinationFileName = randomNumber + "_" + DestinationFileName;
                        fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + ModuleName, DestinationFileName);
                    }
                    if ((clientContext.HasPendingRequest))
                        clientContext.ExecuteQueryAsync();
                   // Microsoft.SharePoint.Client.File.SaveBinary(clientContext, List.RootFolder.ServerRelativeUrl + "/" + ModuleName + "/" + DestinationFileName, file, false);
                    //ShareableLink = clientContext.Web.CreateAnonymousLinkForDocument(fileUrl, ExternalSharingDocumentOption.View);

                }
            }
            return ShareableLink;

        }




    }
}
