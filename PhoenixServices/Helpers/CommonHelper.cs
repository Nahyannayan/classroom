﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Phoenix.API.Helpers
{
    public static class CommonHelper
    {
        public static DataTable ToCustomDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            List<PropertyAndName> propertyAndName = new List<PropertyAndName>();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];

                DataTableFieldAttribute attributes = prop.Attributes.OfType<DataTableFieldAttribute>().Any() ? prop.Attributes.OfType<DataTableFieldAttribute>().FirstOrDefault() : null;
                if (attributes != null)
                {
                    table.Columns.Add(attributes.FieldName, attributes.DbType == null ? prop.PropertyType : attributes.DbType);
                    propertyAndName.Add(new PropertyAndName
                    {
                        ColumnName = attributes.FieldName,
                        PropertyName = prop.Name,
                        ColumnOrder = attributes.Order
                    });
                }
            }
            foreach (T item in data)
            {
                DataRow dr = table.NewRow();
                foreach (var name in propertyAndName)
                {
                    dr[name.ColumnName] = props[name.PropertyName].GetValue(item);
                }
                table.Rows.Add(dr);
            }
            if (propertyAndName.Any(x => x.ColumnOrder > 0))
            {
                propertyAndName = propertyAndName.OrderBy(x => x.ColumnOrder).ToList();
                for (int i = 0; i < propertyAndName.Count(); i++)
                {
                    table.Columns[propertyAndName[i].ColumnName].SetOrdinal(i);
                }
            }
            return table;
        }

        public static bool IsNullOrEmpty(this string str) => string.IsNullOrEmpty(str);

        public static string GetColumnAttribute(MemberInfo member)
        {
            if (member == null) return null;

            var attrib = (ColumnAttribute)Attribute.GetCustomAttribute(member, typeof(ColumnAttribute), false);
            return (attrib?.Name ?? member.Name).ToLower();
        }

        public static bool IsElite
        {
            get
            {
#if ELITE
                return true;
#else
                return false;
#endif
            }
        }

        public static bool IsNonGems
        {
            get
            {
#if NONGEMS
                return true;
#else
                return false;
#endif
            }
        }

        public static bool IsGems
        {
            get
            {
#if GEMS
                return true;
#else
                return false;
#endif
            }
        }
        public static bool StudentImageHasFullPath
        {
            get
            {
#if ELITE
                return true;
#else
                return false;
#endif
            }
        }
        public static string GemsStudentImagePath(string url)
        {
            if (string.IsNullOrEmpty(url)) return "";
            return url.ToLower().Replace("https://school.gemsoasis.com/OASISPHOTOSNEW/OASIS_HR/ApplicantPhoto/".ToLower(), "").Replace("https://school.gemsoasis.com/OASISPHOTOSNEW/OASIS_HR/ApplicantPhoto".ToLower(), "").Replace("https://phoenix.gemseducation.com/phoenixphotos/oasis_hr/applicantphoto/".ToLower(), "").Replace("https://phoenix.gemseducation.com/phoenixphotos/oasis_hr/applicantphoto".ToLower(), "");
        }
    }

    public class FileGenerator
    {
        public byte[] GenerateQuestionWiseReportXLSFile(string path, List<QuestionWiseReport> questionWiseReports)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());

                    // Constructing header
                    Row row = new Row();

                    row.Append(
                        ConstructCell("QuestionText", CellValues.String),
                        ConstructCell("QuestionTypeName", CellValues.String),
                        ConstructCell("CorrectAnswered", CellValues.String),
                        ConstructCell("InCorrectAnswered", CellValues.String),
                        ConstructCell("Percentage", CellValues.String));

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    // Inserting each value
                    foreach (var item in questionWiseReports)
                    {
                        if (item.QuestionText.Contains("[blank_space_") && item.QuestionText.Contains("]"))
                        {
                            string[] r = item.QuestionText.Contains(' ') ? item.QuestionText.Split(' ') : item.QuestionText.Split('.');
                            foreach (var field in r)
                            {
                                if (field.Contains("[blank_space_") && field.Contains("]"))
                                {
                                    int pFrom = field.IndexOf("[");
                                    int pTo = field.LastIndexOf("]");
                                    if (pTo + 1 == field.Length)
                                    {
                                        //int optionNumber = Convert.ToInt32(field.Substring(pFrom, pTo - pFrom));
                                        item.QuestionText = item.QuestionText.Replace(field, "___________");
                                    }
                                    else
                                    {
                                        string subStr = field.Substring(pFrom, pTo + 1);
                                        item.QuestionText = item.QuestionText.Replace(subStr, "___________");
                                    }
                                }
                            }
                        }

                        row = new Row();

                        row.Append(
                            ConstructCell(item.QuestionText, CellValues.String),
                            ConstructCell(item.QuestionTypeName, CellValues.String),
                            ConstructCell(Convert.ToString(item.CorrectAnswered), CellValues.String),
                            ConstructCell(Convert.ToString(item.IncorrectAnswered), CellValues.String),
                            ConstructCell(Convert.ToString(item.Percentage) + "%", CellValues.String));

                        sheetData.AppendChild(row);
                    }

                    worksheetPart.Worksheet.Save();
                }

                var data = System.IO.File.ReadAllBytes(path);

                System.IO.File.Delete(path);

                return data;
            }
            catch
            {
                throw;
            }
        }

        private Cell ConstructCell(string value, CellValues dataType)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType)
            };
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class DataTableFieldAttribute : Attribute
    {
        public DataTableFieldAttribute(string FieldName)
        {
            this.FieldName = FieldName;
        }

        public DataTableFieldAttribute(string FieldName, int order)
        {
            this.FieldName = FieldName;
            Order = order;
        }

        public DataTableFieldAttribute(string FieldName, int order, Type DbType)
        {
            this.FieldName = FieldName;
            Order = order;
            this.DbType = DbType;
        }

        public string FieldName { get; private set; }
        public int Order { get; set; }
        public Type DbType { get; private set; }
    }

    public class PropertyAndName
    {
        public string ColumnName { get; set; }
        public string PropertyName { get; set; }
        public int ColumnOrder { get; set; }
    }
}