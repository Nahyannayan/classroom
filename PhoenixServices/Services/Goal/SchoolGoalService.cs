﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class SchoolGoalService  : ISchoolGoalService
    {
        private readonly ISchoolGoalRepository _schoolGoalRepository;
        public SchoolGoalService(ISchoolGoalRepository schoolGoalRepository)
        {
            _schoolGoalRepository = schoolGoalRepository;
        }

        public async Task<IEnumerable<SchoolGoal>> GetSchoolGoals(int languageId)
        {
            return await _schoolGoalRepository.GetSchoolGoals(languageId);
        }

        public async Task<SchoolGoal> GetSchoolGoalById(int id)
        {
            return await _schoolGoalRepository.GetAsync(id);
        }

        public int InsertSchoolGoal(SchoolGoal entity)
        {
            return _schoolGoalRepository.UpdateSchoolGoal(entity, TransactionModes.Insert);
        }

        public int UpdateSchoolGoal(SchoolGoal entity)
        {
            return _schoolGoalRepository.UpdateSchoolGoal(entity, TransactionModes.Update);
        }

        public int DeleteSchoolGoal(int id)
        {
            SchoolGoal entity = new SchoolGoal(id);
            return _schoolGoalRepository.UpdateSchoolGoal(entity, TransactionModes.Delete);
        }
    }
}
