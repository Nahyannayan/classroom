﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ISchoolGoalService
    {
        Task<IEnumerable<SchoolGoal>> GetSchoolGoals(int languageId);
        Task<SchoolGoal> GetSchoolGoalById(int id);
        int InsertSchoolGoal(SchoolGoal entity);
        int UpdateSchoolGoal(SchoolGoal entity);
        int DeleteSchoolGoal(int id);
    }
}
