﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IStudentGoalsService
    {
        Task<List<StudentGoal>> GetAllStudentGoals(long id, int pageIndex, int pageSize, long currentUserId, string searchString);
        Task<bool> UpdateDashboardGoalsStatus(StudentGoal goalModel);
        Task<bool> UpdateAllDashboardGoalsStatus(List<StudentGoal> lstGoalModel);
        Task<bool> InsertStudentGoal(StudentGoal studentGoalModel);
        Task<bool> ApproveStudentGoal(StudentGoal studentGoal);
        Task<bool> DeleteStudentGoal(StudentGoal model);
        Task<StudentGoal> GetStudentGoalById(long goalId);
    }
}
