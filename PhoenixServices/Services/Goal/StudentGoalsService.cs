﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class StudentGoalsService : IStudentGoalsService
    {
        private readonly IStudentGoalsRepository _studentGoalsRepository;
        public StudentGoalsService(IStudentGoalsRepository studentGoalsRepository)
        {
            _studentGoalsRepository = studentGoalsRepository;
        }

        public async Task<bool> ApproveStudentGoal(StudentGoal studentGoal)
        {
            return await _studentGoalsRepository.ApproveStudentGoal(studentGoal);
        }

        public async Task<List<StudentGoal>> GetAllStudentGoals(long id, int pageIndex, int pageSize, long currentUserId, string searchString = "")
        {
            return await _studentGoalsRepository.GetAllStudentGoals(id, pageIndex, pageSize, currentUserId, searchString);
        }

        public async Task<bool> InsertStudentGoal(StudentGoal studentGoalModel)
        {
            return await _studentGoalsRepository.InsertStudentGoal(studentGoalModel);
        }

        public async Task<bool> UpdateDashboardGoalsStatus(StudentGoal goalModel)
        {
            return await _studentGoalsRepository.UpdateDashboardGoalsStatus(goalModel);
        }
        public async Task<bool> UpdateAllDashboardGoalsStatus(List<StudentGoal> lstGoalModel)
        {
            return await _studentGoalsRepository.UpdateAllDashboardGoalsStatus(lstGoalModel);
        }
        public async Task<bool> DeleteStudentGoal(StudentGoal model) => await _studentGoalsRepository.DeleteStudentGoal(model);

        public async Task<StudentGoal> GetStudentGoalById(long goalId) => await _studentGoalsRepository.GetStudentGoalById(goalId);
    }
}
