﻿using SIMS.API.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Phoenix.API.Repositories;
using Phoenix.API.Models;
using SIMS.API.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class ClassListService : IClassListService
    {
        private readonly IClassListRepository _ClassListRepository;

        public ClassListService(IClassListRepository ClassListRepository)
        {
            _ClassListRepository = ClassListRepository;
        }
        public async Task<IEnumerable<ClassListModel>> GetClassList(long SchoolId, long GroupId, long GradeId, long SectionId)
        {
            return await _ClassListRepository.GetClassList(SchoolId, GroupId, GradeId, SectionId);
        }
        public async Task<BasicDetailModel> GetStudentDetails(string stu_id)
        {
            return await _ClassListRepository.GetStudentDetails(stu_id);
        }
        public async Task<StudentProfileModel> GetStudentProfileDetail(long stu_id, long SchoolId, DateTime nowDate,short languageId)
        {
            return await _ClassListRepository.GetStudentProfileDetail(stu_id, SchoolId, nowDate, languageId);
        }
        public async Task<IEnumerable<WeekDayModel>> GetWeeklyTimeTableDetail(long StudentId, long SchoolId, int WeekCount,DateTime nowDate,short languageId)
        {
            return await _ClassListRepository.GetWeeklyTimeTableDetail(StudentId, SchoolId, WeekCount, nowDate, languageId);
        }
        public async Task<IEnumerable<ParentDetailModel>> GetQuickContactDetail(long StudentId, long SchoolId)
        {
            return await _ClassListRepository.GetQuickContactDetail(StudentId, SchoolId);
        }
        public async Task<TimeTableModel> GetStudentFindMeDetail(long StudentId, long SchoolId, DateTime nowDate)
        {
            return await _ClassListRepository.GetStudentFindMeDetail(StudentId, SchoolId, nowDate);
        }
        public async Task<IEnumerable<AttendanceChart>> GetAttendanceChart(string stu_id)
        {
            return await _ClassListRepository.GetAttendanceChart(stu_id);
        }
        public async Task<IEnumerable<AttendenceListModel>> GetAttendenceList(string stu_id, DateTime EndDate)
        {
            return await _ClassListRepository.GetAttendenceList(stu_id, EndDate);
        }
        public async Task<long> StudentOnReportMasterCU(StudentOnReportMasterModel studentOnReport)
        {
            return await _ClassListRepository.StudentOnReportMasterCU(studentOnReport);
        }
        public async Task<IEnumerable<StudentOnReportMasterModel>> GetStudentOnReportMasters(long studentId, long academicYearId, long schoolId)
        {
            return await _ClassListRepository.GetStudentOnReportMasters(studentId, academicYearId, schoolId);
        }
        public async Task<long> StudentOnReportDetailsCU(StudentOnReportModel studentOnReport)
        {
            return await _ClassListRepository.StudentOnReportDetailsCU(studentOnReport);
        }
        public async Task<IEnumerable<StudentOnReportModel>> GetStudentOnReportDetails(StudentOnReportParameterModel detailsParameter)
        {
            return await _ClassListRepository.GetStudentOnReportDetails(detailsParameter);
        }
        public async Task<IEnumerable<ClassListModel>> GetStudentPhotoPath(string BSU_ID, long RPF_ID, string STU_ID)
        {
            return await _ClassListRepository.GetStudentPhotoPath(BSU_ID, RPF_ID, STU_ID);
        }

        public async Task<AttendanceProfileModel> GetAttendanceProfileDetail(long StudentId, long SchoolId, string AttendanceDt, string AttendanceType)
        {
            return await _ClassListRepository.GetAttendanceProfileDetail(StudentId, SchoolId, AttendanceDt, AttendanceType);
        }
        public async Task<ChangeGroupStudentModel> GetCourseWiseSchoolGroups(long StudentId)
        {
            return await _ClassListRepository.GetCourseWiseSchoolGroups(StudentId);
        }
        public async Task<bool> ChangeCourseWiseStudentSchoolGroup(ChangeGroupStudentModel changeGroupStudentModel)
        {
            return await _ClassListRepository.ChangeCourseWiseStudentSchoolGroup(changeGroupStudentModel);
        }
        public async Task<IEnumerable<ClassListModel>> GetStudentListBySearch(long SchoolId, long UserId, string SearchString)
        {
            return await _ClassListRepository.GetStudentListBySearch(SchoolId, UserId, SearchString);
        }
    }
}
