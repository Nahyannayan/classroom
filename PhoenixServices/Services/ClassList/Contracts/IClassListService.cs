﻿using Phoenix.API.Models;
using Phoenix.Models.Entities;
using SIMS.API.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IClassListService
    {
        Task<IEnumerable<ClassListModel>> GetClassList(long SchoolId, long GroupId, long StudentGradeId, long StudentSectionId);
        Task<BasicDetailModel> GetStudentDetails(string stu_id);
        Task<StudentProfileModel> GetStudentProfileDetail(long stu_id, long SchoolId, DateTime nowDate,short languageId);
        Task<IEnumerable<ParentDetailModel>> GetQuickContactDetail(long StudentId, long SchoolId);
        Task<TimeTableModel> GetStudentFindMeDetail(long StudentId, long SchoolId, DateTime nowDate);
        Task<IEnumerable<WeekDayModel>> GetWeeklyTimeTableDetail(long StudentId, long SchoolId, int WeekCount,DateTime nowDate,short languageId);
        Task<IEnumerable<AttendanceChart>> GetAttendanceChart(string stu_id);
        Task<IEnumerable<AttendenceListModel>> GetAttendenceList(string stu_id,DateTime EndDate);
        Task<long> StudentOnReportMasterCU(StudentOnReportMasterModel studentOnReport);
        Task<IEnumerable<StudentOnReportMasterModel>> GetStudentOnReportMasters(long studentId, long academicYearId, long schoolId);
        Task<long> StudentOnReportDetailsCU(StudentOnReportModel studentOnReport);
        Task<IEnumerable<StudentOnReportModel>> GetStudentOnReportDetails(StudentOnReportParameterModel detailsParameter);
        Task<IEnumerable<ClassListModel>> GetStudentPhotoPath(string BSU_ID, long RPF_ID, string STU_ID);

        Task<AttendanceProfileModel> GetAttendanceProfileDetail(long StudentId, long SchoolId, string AttendanceDt, string AttendanceType);
        Task<ChangeGroupStudentModel> GetCourseWiseSchoolGroups(long StudentId);
        Task<bool> ChangeCourseWiseStudentSchoolGroup(ChangeGroupStudentModel changeGroupStudentModel);
        Task<IEnumerable<ClassListModel>> GetStudentListBySearch(long SchoolId, long UserId, string SearchString);
    }
}
