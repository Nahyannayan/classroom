﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class GradeBookSetupService : IGradeBookSetupService
    {
        private readonly IGradeBookSetupRepository gradeBookSetupRepository;
        public GradeBookSetupService(IGradeBookSetupRepository gradeBookSetupRepository)
        {
            this.gradeBookSetupRepository = gradeBookSetupRepository;
        }
        public async Task<long> SaveGradeBookForm(GradeBook gradeBook) => await gradeBookSetupRepository.SaveGradeBookForm(gradeBook);
        public async Task<long> DeleteGradeBookDetail(GradeBook gradeBook) => await gradeBookSetupRepository.DeleteGradeBookDetail(gradeBook);        
        public async Task<GradeBook> GetGradeBookDetail(long gradeBookId) => await gradeBookSetupRepository.GetGradeBookDetail(gradeBookId);
        public async Task<IEnumerable<GradeBook>> GetGradeBookDetailPagination(int curriculumId, long schoolId, int pageNum, int pageSize, string searchString, string GradeIds, string CourseIds, string sortBy) => await gradeBookSetupRepository.GetGradeBookDetailPagination(curriculumId, schoolId, pageNum, pageSize, searchString, GradeIds, CourseIds, sortBy);
        public async Task<IEnumerable<GradeAndCourse>> GetGradeAndCourseList(int curriculumId, long SchoolId) => await gradeBookSetupRepository.GetGradeAndCourseList(curriculumId, SchoolId);
        public async Task<bool> GradebookFormulaCU(GradebookFormula gradebookFormula) => await gradeBookSetupRepository.GradebookFormulaCU(gradebookFormula);
        public async Task<IEnumerable<GradebookFormula>> GetGradebookFormulas(long schoolId) => await gradeBookSetupRepository.GetGradebookFormulas(schoolId);
        public async Task<GradebookFormula> GetGradebookFormulaDetailById(long FormulaId) => await gradeBookSetupRepository.GetGradebookFormulaDetailById(FormulaId);
        public async Task<IEnumerable<GradebookGrade>> GetGradeListExceptGradebookGrade(long SchoolId, long GradebookId) => await gradeBookSetupRepository.GetGradeListExceptGradebookGrade(SchoolId, GradebookId);
        public async Task<string> ProcessingRuleSetupCU(GradebookRuleSetup gradebookRuleSetup) => await gradeBookSetupRepository.ProcessingRuleSetupCU(gradebookRuleSetup);
        public async Task<IEnumerable<GradebookRuleSetup>> GetGradebookRuleSetups(string assessmentIds) => await gradeBookSetupRepository.GetGradebookRuleSetups(assessmentIds);
        public async Task<bool> DeleteRuleProcessingSetup(long ruleSetupId) => await gradeBookSetupRepository.DeleteRuleProcessingSetup(ruleSetupId);
        public async Task<IEnumerable<GradingTemplateItem>> GetGradingTemplatesByIds(string ids) => await gradeBookSetupRepository.GetGradingTemplatesByIds(ids);
        public async Task<IEnumerable<GradeBookGradeDetail>> ValidateGradeAndCourse(string courseIds, string gradeIds) => await gradeBookSetupRepository.ValidateGradeAndCourse(courseIds, gradeIds);
    }
}
