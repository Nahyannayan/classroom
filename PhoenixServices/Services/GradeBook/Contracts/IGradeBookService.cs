﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IGradeBookService
    {
        Task<TeacherGradeBook> GetTeacherGradeBook(long SchoolGroupId, long teacherId);
        Task<long> TeacherGradebookCreateNewRow(TeacherInternalAssessment createNewRow);
        Task<bool> InternalAssessmentCU(List<InternalAssessmentScore> assessmentScore);
        Task<IEnumerable<GradeBookAssignmentQuiz>> GetAssignmentQuizScore(long schoolGroupId, bool isAssignment);
        Task<TeacherGradeBook> GetInternalAssessments(long schoolGroupId, long teacherId);
        Task<IEnumerable<StandardExamination>> GetStandardExaminations(long schoolGroupId, int assessmentTypeId);
        Task<GradebookProgressTracker> GetProgressTracker(long schoolGroupId, DateTime? startDate, DateTime? endDate);
        Task<TeacherInternalAssessment> GetSubInternalAssessment(long internalAssessmentId);
        Task<IEnumerable<InternalAssessmentScore>> GetInternalAssessmentScoreByAssessmentId(long internalAssessmentId);
    }
}
