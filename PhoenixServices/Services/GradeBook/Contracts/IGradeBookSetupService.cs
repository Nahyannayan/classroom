﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IGradeBookSetupService
    {
        Task<long> SaveGradeBookForm(GradeBook gradeBook);
        Task<long> DeleteGradeBookDetail(GradeBook gradeBook);
        Task<GradeBook> GetGradeBookDetail(long gradeBookId);
        Task<IEnumerable<GradeBook>> GetGradeBookDetailPagination(int curriculumId, long schoolId, int pageNum, int pageSize, string searchString, string GradeIds, string CourseIds, string sortBy);
        Task<IEnumerable<GradeAndCourse>> GetGradeAndCourseList(int curriculumId, long SchoolId);
        Task<bool> GradebookFormulaCU(GradebookFormula gradebookFormula);
        Task<IEnumerable<GradebookFormula>> GetGradebookFormulas(long schoolId);
        Task<GradebookFormula> GetGradebookFormulaDetailById(long FormulaId);
        Task<IEnumerable<GradebookGrade>> GetGradeListExceptGradebookGrade(long SchoolId, long GradebookId);
        Task<string> ProcessingRuleSetupCU(GradebookRuleSetup gradebookRuleSetup);
        Task<IEnumerable<GradebookRuleSetup>> GetGradebookRuleSetups(string assessmentIds);
        Task<bool> DeleteRuleProcessingSetup(long ruleSetupId);
        Task<IEnumerable<GradingTemplateItem>> GetGradingTemplatesByIds(string ids);
        Task<IEnumerable<GradeBookGradeDetail>> ValidateGradeAndCourse(string courseIds, string gradeIds);
    }
}
