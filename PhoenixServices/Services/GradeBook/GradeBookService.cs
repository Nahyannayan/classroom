﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class GradeBookService : IGradeBookService
    {
        private readonly IGradeBookRepository gradeBookRepository;
        public GradeBookService(IGradeBookRepository gradeBookRepository)
        {
            this.gradeBookRepository = gradeBookRepository;
        }
        public async Task<TeacherGradeBook> GetTeacherGradeBook(long SchoolGroupId, long teacherId) => await gradeBookRepository.GetTeacherGradeBook(SchoolGroupId, teacherId);
        public async Task<long> TeacherGradebookCreateNewRow(TeacherInternalAssessment createNewRow) => await gradeBookRepository.TeacherGradebookCreateNewRow(createNewRow);
        public async Task<bool> InternalAssessmentCU(List<InternalAssessmentScore> assessmentScore) => await gradeBookRepository.InternalAssessmentCU(assessmentScore);
        public async Task<IEnumerable<GradeBookAssignmentQuiz>> GetAssignmentQuizScore(long schoolGroupId, bool isAssignment)
            => await gradeBookRepository.GetAssignmentQuizScore(schoolGroupId, isAssignment);
        public async Task<TeacherGradeBook> GetInternalAssessments(long schoolGroupId, long teacherId)
            => await gradeBookRepository.GetInternalAssessments(schoolGroupId, teacherId);
        public async Task<IEnumerable<StandardExamination>> GetStandardExaminations(long schoolGroupId, int assessmentTypeId)
            => await gradeBookRepository.GetStandardExaminations(schoolGroupId, assessmentTypeId);
        public async Task<GradebookProgressTracker> GetProgressTracker(long schoolGroupId, DateTime? startDate, DateTime? endDate)
            => await gradeBookRepository.GetProgressTracker(schoolGroupId,startDate,endDate);
        public async Task<TeacherInternalAssessment> GetSubInternalAssessment(long internalAssessmentId) 
            => await gradeBookRepository.GetSubInternalAssessment(internalAssessmentId);
        public async Task<IEnumerable<InternalAssessmentScore>> GetInternalAssessmentScoreByAssessmentId(long internalAssessmentId)
            => await gradeBookRepository.GetInternalAssessmentScoreByAssessmentId(internalAssessmentId);
    }
}
