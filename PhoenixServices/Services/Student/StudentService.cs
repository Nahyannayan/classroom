﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository _studentRepository;

        public StudentService(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public void Delete(int id)
        {
            _studentRepository.DeleteAsync(id);
        }

        public async Task<StudentDTO> Get(int id)
        {
            return await _studentRepository.GetAsync(id);
        }

        public async Task<IEnumerable<StudentDTO>> GetAll()
        {
            return await _studentRepository.GetAllAsync();
        }



        public void Insert(StudentDTO student)
        {
            _studentRepository.InsertAsync(student);
        }

        public void Update(StudentDTO studentToUpdate)
        {
            _studentRepository.UpdateAsync(studentToUpdate);
        }

        public async Task<StudentDetail> GetStudentByUserId(long id)
        {
            return await _studentRepository.GetStudentByUserId(id);
        }
        public async Task<IEnumerable<StudentDetail>> GetStudentByFamily(long id)
        {
            return await _studentRepository.GetStudentByFamily(id);
        }
        public async Task<StudentDashboard> GetStudentDashboard(long id)
        {
            return await _studentRepository.GetStudentDashboard(id);
        }
        public async Task<StudentPortfolio> GetStudentPortfolioInformation(int userId,short languageId) => await _studentRepository.GetStudentPortfolioInformation(userId, languageId);

        public async Task<bool> SaveStudentDescription(Student model)
        {
            return await _studentRepository.SaveStudentDescription(model);
        }

        public async Task<bool> SaveStudentAboutMe(SchoolBadge model)
        => await _studentRepository.SaveStudentAboutMe(model);
        
        public async Task<StudentProfileDetail> GetStudentProfileDetails(long userId) => await _studentRepository.GetStudentProfileDetails(userId);
        public async Task<bool> SaveStudentProfileImages(Student model) => await _studentRepository.SaveStudentProfileImages(model);

        public async Task<bool> UpdateStudentPortfolioSectionDetails(StudentProfileSections model) => await _studentRepository.UpdateStudentPortfolioSectionDetails(model);

        public int CheckIfTheResourceDeleted(int sourceId, string notificationType)
        {
            return _studentRepository.CheckIfTheResourceDeleted(sourceId, notificationType);
        }
    }
}
