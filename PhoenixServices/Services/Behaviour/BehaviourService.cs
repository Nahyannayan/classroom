﻿using SIMS.API.Repositories;
using SIMS.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Phoenix.Models.Entities;

namespace SIMS.API.Services
{
    public class BehaviourService : IBehaviourService
    {
        private readonly IBehaviourRepository _BehaviourRepository;

        public BehaviourService(IBehaviourRepository BehaviourRepository)
        {
            _BehaviourRepository = BehaviourRepository;
        }
        public async Task<IEnumerable<ClassList>> GetStudentList(string username, int tt_id = 0, string grade = null, string section = null)
        {
            return await _BehaviourRepository.GetStudentList(username, tt_id, grade, section);
        }
        public async Task<IEnumerable<CourseGroupModel>> GetCourseGroupList(long TeacherId, long SchoolId)
        {
            return await _BehaviourRepository.GetCourseGroupList(TeacherId, SchoolId);
        }
        public async Task<IEnumerable<Behaviour>> LoadBehaviourByStudentId(string stu_id)
        {
            return await _BehaviourRepository.LoadBehaviourByStudentId(stu_id);
        }
        public async Task<IEnumerable<BehaviourDetails>> GetBehaviourById(int stu_id)
        {
            return await _BehaviourRepository.GetBehaviourById(stu_id);
        }
        public async Task<IEnumerable<BehaviourDetails>> InsertBehaviourDetails(BehaviourDetails entity, string bsu_id, string mode = "ADD")
        {
            return await _BehaviourRepository.InsertBehaviourDetails(entity, bsu_id, mode);
        }

        public async Task<IEnumerable<StudentBehaviour>> GetListOfStudentBehaviour()
        {
            return await _BehaviourRepository.GetListOfStudentBehaviour();
        }

        public bool InsertUpdateStudentBehavior(List<StudentBehaviourFiles> studentBehaviourFiles, long studentId = 0, int behaviorId = 0, string behaviourComment = "")
        {
            return _BehaviourRepository.InsertUpdateStudentBehavior(studentBehaviourFiles, studentId, behaviorId, behaviourComment);
        }

        public async Task<IEnumerable<StudentBehaviour>> GetStudentBehaviorByStudentId(long studentId = 0)
        {
            return await _BehaviourRepository.GetStudentBehaviorByStudentId(studentId);
        }

        public bool InsertBulkStudentBehaviour(List<StudentBehaviourFiles> studentBehaviourFiles, string bulkStudentds = "", int behaviourId = 0, string behaviourComment = "")
        {
            return _BehaviourRepository.InsertBulkStudentBehaviour(studentBehaviourFiles, bulkStudentds, behaviourId, behaviourComment);
        }

        public bool UpdateBehaviourTypes(int behaviourId = 0, string behaviourType = "", int behaviourPoint = 0, int categoryId = 0)
        {
            return _BehaviourRepository.UpdateBehaviourTypes(behaviourId, behaviourType, behaviourPoint, categoryId);
        }

        public async Task<IEnumerable<StudentBehaviourMerit>> GetFileDetailsByStudentId(long studentId)
        {
            return await _BehaviourRepository.GetFileDetailsByStudentId(studentId);
        }

        public async Task<IEnumerable<ClassList>> GetBehaviourClassList(string username, int tt_id = 0, string grade = null, string section = null, long GroupId = 0, bool IsFilterByGroup = false)
        {
            return await _BehaviourRepository.GetBehaviourClassList(username, tt_id, grade, section,GroupId, IsFilterByGroup);
        }

        public async Task<IEnumerable<ClassList>> GetBehaviourStudentListByGroupId(long groupId) =>
            await _BehaviourRepository.GetBehaviourStudentListByGroupId(groupId);

        public bool DeleteStudentBehaviourMapping(long studentId = 0, int behaviourId = 0)
        {
            return _BehaviourRepository.DeleteStudentBehaviourMapping(studentId, behaviourId);
        }

        public async Task<IEnumerable<SubCategories>> GetSubCategoriesByCategoryId(long categoryId, string BSU_ID, string GRD_ID, long GroupId,short languageId)
        {
            return await _BehaviourRepository.GetSubCategoriesByCategoryId(categoryId, BSU_ID, GRD_ID, GroupId, languageId);
        }

        public async Task<IEnumerable<StudentBehaviourMerit>> GetMeritDetails(long meritId)
        {
            return await _BehaviourRepository.GetMeritDetails( meritId);
        }
        public async Task<IEnumerable<SubCategories>> GetMeritCategoryByStudent(long schoolId, long studentId,short languageId)
        {
            return await _BehaviourRepository.GetMeritCategoryByStudent (schoolId, studentId, languageId);
        }
        public async Task<int> InsertMeritDemerit(string schoolId, int academicId,DateTime? incidentDate, MeritDemerit objMeritDemerit)
        {
            return await _BehaviourRepository.InsertMeritDemerit(schoolId, academicId,incidentDate, objMeritDemerit.objMeritDemerit, objMeritDemerit.objListOfCategories);
        }

        #region Student Points Category
        public async Task<IEnumerable<StudentPointCategory>> GetStudentPointCategory(long schoolId, long academicYearId, int scheduleType)
        {
            return await _BehaviourRepository.GetStudentPointCategory(schoolId, academicYearId,scheduleType);
        }
        public async Task<IEnumerable<StudentPointCategory>> GetStudentPointCategory(string sectionId, int? scheduleType, long? CertificateScheduleId, DateTime? date, long? studentId)
        {
            return await _BehaviourRepository.GetStudentPointCategory(sectionId, scheduleType, CertificateScheduleId, date, studentId);
        }
        public async Task<int> CertificateProcessLogCU(List<CertificateProcessLog> processLogs) => 
            await _BehaviourRepository.CertificateProcessLogCU(processLogs);
        #endregion

        #region StudentOnReport
        public async Task<IEnumerable<StudentOnReportMaster>> GetStudentOnReportMDetail(long studentId)
        {
            return await _BehaviourRepository.GetStudentOnReportMDetail(studentId);
        }
        public async Task<long> StudentOnReportMCU(StudentOnReportMaster studentOnReport)
        {
            return await _BehaviourRepository.StudentOnReportMCU(studentOnReport);
        }
        public async Task<IEnumerable<StudentOnReportDetail>> GetStudentOnReportDetails(StudentOnReportDetailsParameter detailsParameter)
        {
            return await _BehaviourRepository.GetStudentOnReportDetails(detailsParameter);
        }
        public async Task<long> StudentOnReportDetailsCU(StudentOnReportDetail studentOnReport)
        {
            return await _BehaviourRepository.StudentOnReportDetailsCU(studentOnReport);
        }
        #endregion
    }
}
