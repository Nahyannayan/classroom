﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Phoenix.Models.Entities;
using Phoenix.API.Repositories;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class IncidentService : IIncidentService
    {
        private readonly IIncidentRepository incidentRepository;

        public IncidentService(IIncidentRepository incidentRepository)
        {
            this.incidentRepository = incidentRepository;
        }
      
        #region Incident
        public async Task<IncidentDashBoardModel> GetIncidentList(long schoolId, long curriculumId, int month, bool isFA)
        {
            return await incidentRepository.GetIncidentList(schoolId, curriculumId, month, isFA);
        }
        public async Task<IncidentModel> GetIncident(long IncidentId)
        {
            return await incidentRepository.GetIncident(IncidentId);
        }
        public async Task<IEnumerable<IncidentWitness>> GetIncidentWitnesses(long IncidentId)
        {
            return await incidentRepository.GetIncidentWitnesses(IncidentId);
        }
        public async Task<IEnumerable<ChartModel>> GetIncidentChartByCategory(long schoolId, long academicYearId, int month, bool isCategory)
        {
            return await incidentRepository.GetIncidentChartByCategory(schoolId, academicYearId, month, isCategory);
        }

        public async Task<IEnumerable<IncidentStudentList>> GetStudentByIncidentId(long incidentId)
        {
            return await incidentRepository.GetStudentByIncidentId(incidentId);
        }

        public async Task<string> IncidentEntryCUD(IncidentEntry incidentEntry)
        {
            return await incidentRepository.IncidentEntryCUD(incidentEntry);
        }
        #region IncidentAction
        public async Task<IEnumerable<ActionDetails>> GetBehaviourAction(long incidentId, long studentId)
        {
            return await incidentRepository.GetBehaviourAction(incidentId, studentId);
        }

        public async Task<IEnumerable<BehaviourActionFollowup>> GetBehaviourActionFollowups(long incidentId, long actionId)
        {
            return await incidentRepository.GetBehaviourActionFollowups(incidentId, actionId);
        }

        public async Task<IEnumerable<FollowUpDesignation>> GetFollowUpDesignations(long schoolId, long incidentId, long UserId)
        {
            return await incidentRepository.GetFollowUpDesignations(schoolId, incidentId, UserId);
        }

        public async Task<IEnumerable<FollowUpStaff>> GetFollowUpStaffs(long schoolId, long designationId)
        {
            return await incidentRepository.GetFollowUpStaffs(schoolId, designationId);
        }

        public async Task<bool> ActionCUD(ActionModel behaviourAction)
        {
            return await incidentRepository.ActionCUD(behaviourAction);
        }

        public async Task<int> ActionFollowUpCUD(BehaviourActionFollowup behaviourActionFollowup)
        {
            return await incidentRepository.ActionFollowUpCUD(behaviourActionFollowup);
        }

        public async Task<IEnumerable<GroupTeacher>> GetSchoolTeachersBySchoolId(string SchoolId) =>
            await incidentRepository.GetSchoolTeachersBySchoolId(SchoolId);

        #endregion
        #endregion
    }
}
