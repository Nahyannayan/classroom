﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class BehaviourSetupService: IBehaviourSetupService
    {
        private readonly IBehaviourSetupRepository behaviourSetupRepository;

        public BehaviourSetupService(IBehaviourSetupRepository behaviourSetupRepository)
        {
            this.behaviourSetupRepository = behaviourSetupRepository;
        }
        public async Task<IEnumerable<BehaviourSetup>> GetSubCategoryList(long MainCategoryId, long SchoolId,short languageId)
        {
            return await behaviourSetupRepository.GetSubCategoryList(MainCategoryId, SchoolId, languageId);
        }
        public async Task<int> SaveSubCategory(BehaviourSetup behaviourSetup, string DATAMODE)
        {
            return await behaviourSetupRepository.SaveSubCategory(behaviourSetup, DATAMODE);
        }

        #region Action Hierarchy
        public async Task<IEnumerable<Designations>> GetDesignations(long schoolId)
        {
            return await behaviourSetupRepository.GetDesignations(schoolId);
        }

        public async Task<IEnumerable<DesignationsRouting>> GetDesignationsRoutings(long schoolId, long? designationFrom)
        {
            return await behaviourSetupRepository.GetDesignationsRoutings(schoolId, designationFrom);
        }

        public async Task<IEnumerable<DesignationsRouting>> DesignationBySchoolCUD(DesignationsRoutingCUD designationsRouting)
        {
            return await behaviourSetupRepository.DesignationBySchoolCUD(designationsRouting);
        }
        #endregion

        #region Certificate Schedule
        public async Task<IEnumerable<CertificateScheduling>> GetCertificateSchedulings(long? CertificateSchedulingId, long? curriculumId, long? schoolId, int? scheduleType = null, short languageId = 1)
        {
            return await behaviourSetupRepository.GetCertificateSchedulings(CertificateSchedulingId, curriculumId, schoolId, scheduleType, languageId);
        }

        public async Task<long> CertificateSchedulingCUD(CertificateScheduling certificateScheduling)
        {
            return await behaviourSetupRepository.CertificateSchedulingCUD(certificateScheduling);
        }
        #endregion
    }
}
