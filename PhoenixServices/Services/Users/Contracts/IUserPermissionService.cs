﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IUserPermissionService
    {
        Task<bool> IsPermissionAssigned(long userId, string moduleUrl, int userTypeId);
        Task<PagePermission> GetUserPermissions(long userId, string moduleUrl, string moduleCode, int userTypeId);
        Int32 AddMenuItem(MenuItem menuItem);
        Task<bool> IsCustomPermissionAssigned(int userId, string permissionCodes);
    }
}
