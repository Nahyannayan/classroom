﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IChatterPermissionService
    {
        Task<IEnumerable<UserChatPermission>> GetUserPermissions(int schoolId, int userId);
        Task<bool> UpdateChatterPermission(ChatPermission chats);
        Task<IEnumerable<ChatUsers>> GetUsersInGroup(int groupId);
    }
}
