﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Phoenix.API.Services
{
    public interface ILogInUserService
    {
        Task<LogInUser> GetLoginUserByUserName(string userName, string ipDetails);
        Task<IEnumerable<LogInUser>> GetUserList(int schoolId, short languageId);
        Task<LogInUser> GetLoginUserByUserNamePassword(string userName, string password, string ipDetails);
    }
}
