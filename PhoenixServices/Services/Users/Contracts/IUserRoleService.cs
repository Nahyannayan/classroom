﻿using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IUserRoleService
    {
        Task<IEnumerable<UserRole>> GetUserRoles();
        Task<UserRole> GetUserRoleById(int id, short languageId);
        Task<IEnumerable<UserRoleMapping>> GetAllUserRoleMappingData(int userid);
        Task<IEnumerable<UserRoleMapping>> GetAssignedUserMappingData(int systemlanguageid, int roleid);
        Task<IEnumerable<ModuleStructure>> GetModuleList(int systemlanguageid, string modulecode);
        Task<IEnumerable<ModuleStructure>> GetUserRolePermissionList();
        Task<IEnumerable<ModuleStructure>> GetRolePhoenixModuleData(int userroleid);
        Task<IEnumerable<ModuleStructure>> GetRoleMappingData(int roleId, int schoolId);
        Task<IEnumerable<ModuleStructure>> GetModuleStructureList(int systemlanguageid, int? moduleid, string modulecode);
        Task<IEnumerable<PermissionTypeView>> GetAllPermissionData(int userroleid, int userid, int moduleid, bool loadcustomepermission,int schoolId);
        Task<bool> UpdatePermissionTypeDataCUD(List<CustomPermissionEdit> MappingDetails, string operationtype, int? userId, short userRoleId, int schoolId);
        bool InsertUserRole(UserRole entity);
        bool UpdateUserRole(UserRole entity);
        bool DeleteUserRole(int id);
        Task<object> CheckUserRoleMapping(long? userId, short? roleId);
        Task<bool> InsertUserRoleMappingData(long userId, short roleId);
        Task<bool> DeleteUserRoleMappingData(long userId, short roleId);

        Task<IEnumerable<UserRole>> GetUserRolesBySchoolId(int schoolId,short languageId);
        Task<IEnumerable<User>> GetUsersForRole(int roleId, int schoolId);
    }
}
