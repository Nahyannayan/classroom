﻿using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.Models.HSEObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetAllUsers();

        Task<IEnumerable<UserFeelingView>> GetUserFeelings(int SystemLanguageId);

        Task<IEnumerable<UserProfileView>> GetProfileAvatars();

        Task<IEnumerable<User>> GetUserBySchool(int? id, int UserTypeId);
        Task<IEnumerable<User>> GetUsersBySchoolAndType(int? id, int UserTypeId,short languageId);
        Task<IEnumerable<UserNotificationView>> GetUserNotifications(int? userTypeId, long userId, long loginUserId);
        Task<IEnumerable<UserNotificationView>> GetAllNotifications(int? userTypeId, long userId, long loginUserId);

        Task<User> GetUserById(int id);

        Task<IEnumerable<User>> SearchUserByName(string name, int typeId = 0, int schoolId = 0);

        Task<IEnumerable<User>> GetUsersByRolesLocatonAllowed(string roles, long businessUnitId);

        bool UpdateUserFeeling(UserFeelingView userFeelingView);

        bool UpdateUserProfile(UserProfileView userProfileView);
        Task<bool> saveErrorLogger(ErrorLogger errorLogger);
        Task<IEnumerable<DBLogDetails>> GetDBLogdetails();
        bool PushNotificationLogs(string notificationType, int sourceId, long userId);
        Task<IEnumerable<User>> GetUserLog(int? schoolId, DateTime startDate, DateTime endDate, string loginType);
        Task<IEnumerable<UserErrorLogs>> GetErrorLogs(int? schoolId, DateTime startDate, DateTime endDate, string loginType);
        Task<IEnumerable<ErrorFiles>> GetErrorLogFiles(long ErrorLogId);

        Task<IEnumerable<User>> GetTeachersListBySchoolId( long schoolId = 0);
        Task<bool> InsertLogDetails(LogDetails logDetails);
        Task<IEnumerable<User>> GetUsersBySchoolAndTypeWithPaging(int pageNumber, int pageSize, int? schoolId, int UserTypeId);
        Task<bool> SaveUserFeedback(UserFeedback model);
    }
}
