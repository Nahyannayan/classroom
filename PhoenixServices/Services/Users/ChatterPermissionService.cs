﻿using Phoenix.API.Repositories;
using Phoenix.API.Services;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services.Users
{
    public class ChatterPermissionService : IChatterPermissionService
    {
        private readonly IChatterPermissionRepository _chatterPermissionRepository;

        public ChatterPermissionService(IChatterPermissionRepository chatterPermissionRepository)
        {
            _chatterPermissionRepository = chatterPermissionRepository;
        }

        public async Task<IEnumerable<UserChatPermission>> GetUserPermissions(int schoolId, int userId)
        {
            return await _chatterPermissionRepository.GetUserPermissions(schoolId,userId);
        }

        public async Task<bool> UpdateChatterPermission(ChatPermission chats)
        {   
            return await _chatterPermissionRepository.UpdateChatterPermission(chats);
        }

        public async Task<IEnumerable<ChatUsers>> GetUsersInGroup(int groupId)
        {
            return await _chatterPermissionRepository.GetUsersInGroup(groupId);
        }

    }
}
