﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.API.Repositories;

namespace Phoenix.API.Services
{
    public class UserPermissionService : IUserPermissionService
    {
        private readonly IUserPermissionRepository _userPermissionRepository;

        public UserPermissionService(IUserPermissionRepository userPermissionRepository)
        {
            _userPermissionRepository = userPermissionRepository;
        }

        public int AddMenuItem(MenuItem menuItem)
        {
            return _userPermissionRepository.AddMenuItem(menuItem);
        }

        public async Task<PagePermission> GetUserPermissions(long userId, string moduleUrl, string moduleCode, int userTypeId)
        {
           return await _userPermissionRepository.GetUserPermissions(userId, moduleUrl, moduleCode, userTypeId);
        }

        public async Task<bool> IsPermissionAssigned(long userId, string moduleUrl, int userTypeId)
        {
            return await _userPermissionRepository.IsPermissionAssigned(userId, moduleUrl, userTypeId);
        }
        public async Task<bool> IsCustomPermissionAssigned(int userId, string permissionCodes)
        {
            return await _userPermissionRepository.IsCustomPermissionAssigned(userId, permissionCodes);
        }
    }
}
