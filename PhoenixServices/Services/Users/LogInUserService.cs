﻿using Phoenix.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;

namespace Phoenix.API.Services
{
    public class LogInUserService : ILogInUserService
    {
        private readonly ILogInUserRepository _logInUserRepository;
        private IConfiguration _config;
        public LogInUserService(ILogInUserRepository logInUserRepository, IConfiguration config)
        {
            _logInUserRepository = logInUserRepository;
            _config = config;
        }

        public async Task<LogInUser> GetLoginUserByUserName(string userName,string ipDetails)
        {
            var loginUser = await _logInUserRepository.GetLogInUserByUserName(userName, ipDetails);
            if (loginUser != null)
            {
                loginUser.Token = GenerateJSONWebToken(loginUser);
            }
            else
            {
                loginUser = new LogInUser();
                loginUser.ErrorMessage = _config["ErrorMessage"];
            }
            return loginUser;
        }

        public async Task<IEnumerable<LogInUser>> GetUserList(int schoolId,short languageId)
        {
            return await _logInUserRepository.GetUserList(schoolId, languageId);
        }

        /// <summary>
        /// Deepak Singh, 19 August 2019, To get token for api authentication
        /// </summary>
        /// <param name="logInUser"></param>
        /// <returns></returns>
        private string GenerateJSONWebToken(LogInUser logInUser)
        {
            var key = _config["Jwt:Key"];
            var audiences = Convert.ToString(_config["Jwt:Audiences"]);

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);


            var claims = new List<Claim>
                            {
                                new Claim(JwtRegisteredClaimNames.Sub, logInUser.Id.ToString()),
                                new Claim(JwtRegisteredClaimNames.FamilyName, logInUser.LastName),
                                new Claim(JwtRegisteredClaimNames.GivenName, logInUser.FirstName),
                                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                                new Claim(JwtRegisteredClaimNames.Iss, _config["Jwt:Issuer"])
                             };
           
            foreach (var aud in audiences.Split(","))
            {
                claims.Add(new Claim(JwtRegisteredClaimNames.Aud, aud));
            }

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.UtcNow.AddHours(Convert.ToInt32(_config["Jwt:Hours"])),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        public async Task<LogInUser> GetLoginUserByUserNamePassword(string userName,string password, string ipDetails)
        {
            var loginUser = await _logInUserRepository.GetLogInUserByUserNamePassword(userName,password, ipDetails);
            if (loginUser != null)
            {
                loginUser.Token = GenerateJSONWebToken(loginUser);
            }
            else
            {
                loginUser = new LogInUser();
                loginUser.ErrorMessage = _config["ErrorMessage"];
            }
            return loginUser;
        }
    }
}
