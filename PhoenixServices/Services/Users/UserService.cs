﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.Models.HSEObjects;

namespace Phoenix.API.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 16-MAY-2019
        /// Description : To fetch all the users
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetAllUsers()
        {
            return await _userRepository.GetAllAsync();
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 18-June-2019
        /// Description : To fetch all user feelings
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<UserFeelingView>> GetUserFeelings(int SystemLanguageId)
        {
            return await _userRepository.GetUserFeelings(SystemLanguageId);
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 18-June-2019
        /// Description : To fetch all user profile avatar
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<UserProfileView>> GetProfileAvatars()
        {
            return await _userRepository.GetProfileAvatars();
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 18-June-2019
        /// Description : To Insert user feelings
        /// </summary>
        /// <returns></returns>
        public bool UpdateUserFeeling(UserFeelingView userFeelingView)
        {
            return _userRepository.UpdateUserFeeling(userFeelingView);
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 24-June-2019
        /// Description : To UpdateUserProfile
        /// </summary>
        /// <returns></returns>
        public bool UpdateUserProfile(UserProfileView userProfileView)
        {
            return _userRepository.UpdateUserProfile(userProfileView);
        }
        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 16-Oct-2019
        /// Description : To insert log for notification
        /// </summary>
        /// <returns></returns>
        public bool PushNotificationLogs(string notificationType, int sourceId, long userId)
        {
            return _userRepository.PushNotificationLogs(notificationType, sourceId, userId);
        }
        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 16-MAY-2019
        /// Description : To fetch all the users by schoolId
        /// </summary>
        /// <param name="id"></param>
        /// <param name="UserTypeId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetUserBySchool(int? id, int UserTypeId)
        {
            return await _userRepository.GetUsersBySchool(id, UserTypeId);
        }

        /// <summary>
        /// Author : Girish Sonawane 
        /// Created Date : 15/10/2019
        /// Description : To get user notifications
        /// </summary>
        /// <param name="userTypeId"></param>
        /// <param name="userId"></param>
        /// <param name="loginUserId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<UserNotificationView>> GetUserNotifications(int? userTypeId, long userId, long loginUserId)
        {
            return await _userRepository.GetUserNotifications(userTypeId, userId, loginUserId);
        }
        /// <summary>
        /// Author : Girish Sonawane 
        /// Created Date : 15/10/2019
        /// Description : To get all notifications
        /// </summary>
        /// <param name="userTypeId"></param>
        /// <param name="userId"></param>
        /// <param name="loginUserId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<UserNotificationView>> GetAllNotifications(int? userTypeId, long userId, long loginUserId)
        {
            return await _userRepository.GetAllNotifications(userTypeId, userId, loginUserId);
        }
        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 16-MAY-2019
        /// Description : To fetch all the user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<User> GetUserById(int id)
        {
            return await _userRepository.GetAsync(id);
        }

        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 19-MAY-2019
        /// Description : To fetch all the user name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="typeId"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<User>> SearchUserByName(string name, int typeId = 0, int schoolId = 0)
        {
            return await _userRepository.SearchUserByName(name, typeId, schoolId);
        }

        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 13-June-2019
        /// Description : To fetch user by role and location allowed to access
        /// </summary>
        /// <param name="roles"></param>
        /// <param name="bussinessUnitId">
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetUsersByRolesLocatonAllowed(string roles, long bussinessUnitId)
        {
            return await _userRepository.GetUsersByRolesLocatonAllowed(roles, bussinessUnitId);
        }

        /// <summary>
        /// Author : Mahesh Chikhale
        /// Created Date : 21-Oct-2019
        /// Description : To Insert Error Log
        /// </summary>
        /// <param name="objErrorLog"></param>
        /// <returns></returns>
        public async Task<bool> saveErrorLogger(ErrorLogger objErrorLog)
        {
            return await _userRepository.SaveErrorLogger(objErrorLog);
        }
        /// <summary>
        /// Author : Vinayak y
        /// Created Date : 15-JAN-2020
        /// Description : To fetch users by school id and user type without studentnumber
        /// </summary>
        /// <param name="id"></param>
        /// <param name="UserTypeId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetUsersBySchoolAndType(int? id, int UserTypeId,short languageId)
        {
            return await _userRepository.GetUsersBySchoolAndType(id, UserTypeId, languageId);
        }

        /// <summary>
        /// Author : Mukund Patil
        /// Created Date : 08-AUG-2020
        /// Description : To fetch users by school id and user type with paging
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="schoolId"></param>
        /// <param name="UserTypeId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetUsersBySchoolAndTypeWithPaging(int pageNumber, int pageSize, int? schoolId, int UserTypeId)
        {
            return await _userRepository.GetUsersBySchoolAndTypeWithPaging(pageNumber, pageSize, schoolId, UserTypeId);
        }

        /// <summary>
        /// Author :Mahesh Chikhale
        /// Created Date : 21-Oct -2019
        /// Description : To get DB log details after user reported error
        /// </summary>       
        /// <returns></returns>
        public async Task<IEnumerable<DBLogDetails>> GetDBLogdetails()
        {
            return await _userRepository.GetDBLogdetails();
        }

        public Task<IEnumerable<User>> GetUserLog(int? schoolId, DateTime startDate, DateTime endDate, string loginType)
        {
            return _userRepository.GetUserLog(schoolId, startDate, endDate, loginType);
        }
        /// <summary>
        /// Author :sonali shinde
        /// Created Date : 19-Feb -2019
        /// Description : To get logs
        /// </summary>       
        /// <returns></returns>
        public  Task<IEnumerable<UserErrorLogs>> GetErrorLogs(int? schoolId, DateTime startDate, DateTime endDate, string loginType)
        {
            return  _userRepository.GetErrorLogs(schoolId, startDate, endDate, loginType);
        }
        /// <summary>
        /// Author :sonali shinde
        /// Created Date : 20-Feb -2019
        /// Description : To get log files
        /// </summary>       
        /// <returns></returns>
        public async Task<IEnumerable<ErrorFiles>> GetErrorLogFiles(long ErrorLogId)
        {
            return await _userRepository.GetErrorLogFiles(ErrorLogId);
        }

        public Task<IEnumerable<UserErrorLogs>> GetErrorLogs()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// AUthor: Shankar Kadam
        /// Get Teachers List By School Id
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetTeachersListBySchoolId(long schoolId = 0) {
            return await _userRepository.GetTeachersListBySchoolId(schoolId);
        }

        /// <summary>
        /// Athor: tejalben
        /// Add log details.
        /// </summary>
        /// <param name="logDetails"></param>
        /// <returns></returns>
        public Task<bool> InsertLogDetails(LogDetails logDetails)
        {
            return _userRepository.InsertLogDetails(logDetails);
        }

        public async Task<bool> SaveUserFeedback(UserFeedback model)
        => await _userRepository.SaveUserFeedback(model);
    }
}
