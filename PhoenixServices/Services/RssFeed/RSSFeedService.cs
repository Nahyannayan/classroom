﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class RSSFeedService : IRSSFeedService
    {
        private readonly IRSSFeedRepository _rssFeedRepository;

        public RSSFeedService(IRSSFeedRepository rssFeedRepository)
        {
            _rssFeedRepository = rssFeedRepository;

        }
        public bool AddUpdateRSSFeed(RSSFeed entity)
        {
            return _rssFeedRepository.AddUpdateRSSFeed(entity);
        }

        public async Task<RSSFeed> GetRSSFeedById(int id)
        {
            return await _rssFeedRepository.GetRSSFeedById(id);
        }

        public async Task<IEnumerable<RSSFeed>> GetRSSFeedList()
        {
            return await _rssFeedRepository.GetRSSFeedList();
        }

      
    }
}
