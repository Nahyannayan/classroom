﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
   public  interface IRSSFeedService
    {
        Task<IEnumerable<RSSFeed>> GetRSSFeedList();

        bool AddUpdateRSSFeed(RSSFeed entity);

        Task<RSSFeed> GetRSSFeedById(int id);
    }
}
