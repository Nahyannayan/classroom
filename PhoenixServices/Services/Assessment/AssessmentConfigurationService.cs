﻿using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class AssessmentConfigurationService : IAssessmentConfigurationService
    {
        private readonly IAssessmentConfigurationRepository assessmentConfigurationRepository;
        public AssessmentConfigurationService(IAssessmentConfigurationRepository _assessmentConfig)
        {
            assessmentConfigurationRepository = _assessmentConfig;
        }

        #region Assessment Configuration
        public async Task<IEnumerable<AssessmentColumn>> GetAssessmentColumnDetail(long AssessmentMasterId)
        {
            return await assessmentConfigurationRepository.GetAssessmentColumnDetail(AssessmentMasterId);
        }
        public async Task<IEnumerable<AssessmentConfigSetting>> GetAssessmentConfigDetail(long AssessmentMasterId, long schoolId)
        {
            return await assessmentConfigurationRepository.GetAssessmentConfigDetail(AssessmentMasterId, schoolId);
        }
        public async Task<IEnumerable<AssessmentConfigSetting>> GetAssessmentConfigPagination(long schoolId, int pageNum, int pageSize, string searchString)
        {
            return await assessmentConfigurationRepository.GetAssessmentConfigPagination(schoolId, pageNum, pageSize, searchString);
        }
        public async  Task<IEnumerable<Course>> GetCourseListByGrade(string SchoolGradeIds)
        {
            return await assessmentConfigurationRepository.GetCourseListByGrade(SchoolGradeIds);
        }
        public async Task<bool> SaveAssessmentConfigDetail(AssessmentConfigSetting assessmentConfig)
        {
            return await assessmentConfigurationRepository.SaveAssessmentConfigDetail(assessmentConfig);
        }
        #endregion
    }
}
