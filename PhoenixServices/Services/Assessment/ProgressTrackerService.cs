﻿using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class ProgressTrackerService : IProgressTrackerService
    {
        private readonly IProgressTrackerRepository _iprogressTrackerRepository;
        private readonly IAttachmentRepository _attachmentRepository;
        public ProgressTrackerService(IProgressTrackerRepository iprogressTrackerRepository, IAttachmentRepository attachmentRepository)
        {
            _iprogressTrackerRepository = iprogressTrackerRepository;
            _attachmentRepository = attachmentRepository;

        }



        public async Task<IEnumerable<CourseTopics>> GetTopicsByCourseId(long Id)
        {
            return await _iprogressTrackerRepository.GetTopicsByCourseId(Id);
        }
        public async Task<IEnumerable<StudentList>> GetStudentList(string GRD_ID, Int32 ACD_ID, Int32 SGR_ID, Int32 SCT_ID)
        {
            return await _iprogressTrackerRepository.GetStudentList(GRD_ID, ACD_ID, SGR_ID, SCT_ID);
        }
        public async Task<IEnumerable<ProgressTrackerHeader>> GET_PROGRESS_TRACKER_HEADERS(long SBG_ID, string TOPIC_ID, long AGE_BAND_ID, string STEPS, long TSM_ID)
        {
            return await _iprogressTrackerRepository.GET_PROGRESS_TRACKER_HEADERS(SBG_ID, TOPIC_ID, AGE_BAND_ID, STEPS, TSM_ID);
        }
        public async Task<IEnumerable<ProgressTrackerDropdown>> BindProgressTrackerDropdown(long BSU_ID, string GRD_ID)
        {
            return await _iprogressTrackerRepository.BindProgressTrackerDropdown(BSU_ID, GRD_ID);
        }
        public Task<ProgressTrackerSettingMaster> BindProgressTrackerMasterSetting(long ACD_ID, long BSU_ID, string GRD_ID)
        {
            return _iprogressTrackerRepository.BindProgressTrackerMasterSetting(ACD_ID, BSU_ID, GRD_ID);
        }

        public async Task<IEnumerable<ProgressTrackerData>> GET_PROGRESS_TRACKER_DATA(long SBG_ID, string TOPIC_ID, long TSM_ID, long SGR_ID)
        {
            return await _iprogressTrackerRepository.GET_PROGRESS_TRACKER_DATA(SBG_ID, TOPIC_ID, TSM_ID, SGR_ID);
        }
        public bool AddEditProgressSetUP(long teacherId, ProgressTrackerSetup model)
        {
            return _iprogressTrackerRepository.AddEditProgressSetUP(teacherId, model);
        }
        public async Task<IEnumerable<ProgressTrackerSetup>> GetProgressSetup(long schoolId)
        {
            return await _iprogressTrackerRepository.GetProgressSetup(schoolId);
        }

        public async Task<IEnumerable<CourseGradeDisplay>> GetCourseGradeDisplay(long schoolId, string courseIds)
        {
            return await _iprogressTrackerRepository.GetCourseGradeDisplay(schoolId, courseIds);
        }

        public async Task<ProgressTrackerSetup> GetProgressSetupDetailsById(long Id)
        {
            return await _iprogressTrackerRepository.GetProgressSetupDetailsById(Id);
        }

        public async Task<IEnumerable<GradingTemplateItem>> GetProgressLessonGrading(long courseId, long schoolGroupId,short languageId)
        {
            return await _iprogressTrackerRepository.GetProgressLessonGrading(courseId, schoolGroupId, languageId);
        }

        public async Task<IEnumerable<AssessmentStudent>> StudentProgressTracker(long schoolGroupId)
        {
            return await _iprogressTrackerRepository.StudentProgressTracker(schoolGroupId);
        }

        public async Task<IEnumerable<AssessmetLessons>> GetLessonObjectivesByUnitIds(string subSyllabusIds)
        {
            return await _iprogressTrackerRepository.GetLessonObjectivesByUnitIds(subSyllabusIds);
        }

        public bool StudentProgressMapper(long userId, long groudId, List<AssessmentStudent> studentProgressMapper)
        {
            return _iprogressTrackerRepository.StudentProgressMapper(userId, groudId, studentProgressMapper);
        }

        public async Task<bool> SaveProgressTrackerEvidence(List<Attachment> attachments)
        {
            return await _iprogressTrackerRepository.SaveProgressTrackerEvidence(attachments);
        }

        public async Task<IEnumerable<Attachment>> GetProgressTrackerEvidence(long id, string key)
        {
            return await _iprogressTrackerRepository.GetProgressTrackerEvidence(id, key);
        }

        public async Task<IEnumerable<AssignmentObjectiveGrading>> GetObjectiveAssignmentGrading(long lessonId, long studentId)
        {
            return await _iprogressTrackerRepository.GetObjectiveAssignmentGrading(lessonId, studentId);
        }

        public async Task<IEnumerable<ProgressTrackerSetup>> ValidateProgressSetupCourseGrade(string courseIds, string gradeIds)
        {
            return await _iprogressTrackerRepository.ValidateProgressSetupCourseGrade(courseIds, gradeIds);
        }
    }
}
