﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class StudentSuggestionService : IStudentSuggestionService
    {
        private readonly IStudentSuggestionRepository _studentSuggestionRepository;

        public StudentSuggestionService(IStudentSuggestionRepository studentSuggestionRepository)
        {
            _studentSuggestionRepository = studentSuggestionRepository;
        }
        
        public async Task<IEnumerable<Suggestion>> GetStudentSuggestions()
        {
            return await _studentSuggestionRepository.GetStudentSuggestion();
        }

        public async Task<IEnumerable<Suggestion>> GetStudentSuggestionsByUserId(int id)
        {
            return await _studentSuggestionRepository.GetStudentSuggestionsByUserId(id);
        }
        public async Task<IEnumerable<Suggestion>> GetStudentSuggestionsBySchoolId(int id)
        {
            return await _studentSuggestionRepository.GetStudentSuggestionsBySchoolId(id);
        }
        public async Task<Suggestion> GetStudentSuggestionById(int id)
        {
            return await _studentSuggestionRepository.GetStudentSuggestionById(id);
        }

        public async Task<Suggestion> GetStudentSuggestionByIdAndUserId(int id, long userId)
        {
            return await _studentSuggestionRepository.GetStudentSuggestionByIdAndUserId(id, userId);
        }

        public int InsertStudentSuggestion(Suggestion entity)
        {
            return _studentSuggestionRepository.UpdateStudentSuggestion(entity,TransactionModes.Insert);
        }
        public async Task<IEnumerable<Suggestion>> PaginateStudentSuggestionByUserId(int userId, int pageNumber, int pageSize, string searchString = "", string sortBy="")
        {
            return await _studentSuggestionRepository.PaginateStudentSuggestionByUserId(userId, pageNumber, pageSize, searchString, sortBy);
        }
        public async Task<IEnumerable<Suggestion>> PaginateStudentSuggestionBySchoolId(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy = "",short langaugeId=1)
        {
            return await _studentSuggestionRepository.PaginateStudentSuggestionBySchoolId(schoolId, pageNumber, pageSize, searchString, sortBy, langaugeId);
        }
        public int UpdateStudentSuggestion(Suggestion entity)
        {
            return _studentSuggestionRepository.UpdateStudentSuggestion(entity, TransactionModes.Update);
        }

        public int DeleteStudentSuggestion(int id, long userId)
        {
            Suggestion entity = new Suggestion(id);
            entity.UserId = (int)userId;
            return _studentSuggestionRepository.UpdateStudentSuggestion(entity, TransactionModes.Delete);
        }
        public int AddUpdateThinkBoxUser(ThinkBox entity)
        {
            return _studentSuggestionRepository.AddUpdateThinkBoxUser(entity);
        }
        public async Task<IEnumerable<ThinkBox>> GetThinkBoxUsersBySchoolId(int Id)
        {
            return await  _studentSuggestionRepository.GetThinkBoxUsersBySchoolId(Id);
        }

        public void DeleteUserFromThinkBox(long userId,long schoolId)
        {
            _studentSuggestionRepository.DeleteUserFromThinkBox(userId, schoolId);
        }
        public bool HasThinkBoxUserPermission(long userId)
        {
            return _studentSuggestionRepository.HasThinkBoxUserPermission(userId);
        }
    }
}
