﻿using Phoenix.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class SubjectService : ISubjectService
    {
        private readonly ISubjectRepository _subjectRepository;

        public SubjectService(ISubjectRepository subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public async Task<IEnumerable<Subject>> GetSubjects(int languageId, int schoolId)
        {
            return await _subjectRepository.GetSubjects(languageId, schoolId);
        }
        public async Task<IEnumerable<Subject>> GetTopTenSubjects(int schoolId)
        {
            return await _subjectRepository.GetTopTenSubjects(schoolId);
        }

        public async Task<Subject> GetSubjectById(int id)
        {
            return await _subjectRepository.GetAsync(id);
        }
        public async Task<IEnumerable<Objective>> GetLessonsByTopicId(int id)
        {
            return await _subjectRepository.GetLessonsByTopicId(id);
        }

        public async Task<IEnumerable<GroupUnit>> GetSelectedUnitByGroupId(string id)
        {
            return await _subjectRepository.GetSelectedUnitByGroupId(id);
        }

        public bool InsertSubject(Subject entity)
        {
            return _subjectRepository.UpdateSubjectData(entity, TransactionModes.Insert);
        }

        public bool UpdateSubject(Subject entity)
        {
            return _subjectRepository.UpdateSubjectData(entity, TransactionModes.Update);
        }

        public bool DeleteSubject(int id)
        {
            Subject entity = new Subject(id);
            return _subjectRepository.UpdateSubjectData(entity, TransactionModes.Delete);
        }
        public async Task<IEnumerable<TopicSubTopicStructure>> GetTopicSubTopicStructure(int subjectId)
        {
            return await _subjectRepository.GetTopicSubTopicStructure(subjectId);
        }

        public async Task<IEnumerable<TopicSubTopicStructure>> GetUnitStructure(string courseIds)
        {
            return await _subjectRepository.GetUnitStructure(courseIds);
        }

        public async Task<IEnumerable<Objective>> GetSubtopicsObjective(int id, bool isMainSyllabus, int subjectId, bool isLesson)
        {
            return await _subjectRepository.GetSubtopicsObjective(id, isMainSyllabus, subjectId, isLesson);
        }
        public async Task<IEnumerable<Objective>> GetAssignmentsObjective(int assignmentId)
        {
            return await _subjectRepository.GetAssignmentsObjective(assignmentId);
        }

        public async Task<IEnumerable<CourseUnit>> GetAssignmentUnit(int assignmentId)
        {
            return await _subjectRepository.GetAssignmentUnit(assignmentId);
        }

        public async Task<IEnumerable<GroupUnit>> GetAssignmentCourseTopicUnit(int assignmentId)
        {
            return await _subjectRepository.GetAssignmentCourseTopicUnit(assignmentId);
        }

        public async Task<IEnumerable<TopicSubTopicStructure>> GetTopicSubTopicStructureByMultipleSubjectIds(string subjectIds)
        {
            return await _subjectRepository.GetTopicSubTopicStructureByMultipleSubjectIds(subjectIds);
        }

        public async Task<IEnumerable<Objective>> GetSubtopicObjectivesWithMultipleSubjects(int id, bool isMainSyllabus, string subjectIds, bool isLesson)
        {
            return await _subjectRepository.GetSubtopicsObjectiveWithMultipleSubject(id, isMainSyllabus, subjectIds, isLesson);
        }
    }
}
