﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class SuggestionCategoryService : ISuggestionCategoryService
    {
        private readonly ISuggestionCategoryRepository _suggestionCategoryService;
        public SuggestionCategoryService(ISuggestionCategoryRepository suggestionCategoryService)
        {
            _suggestionCategoryService = suggestionCategoryService;
        }

        public async Task<IEnumerable<SuggestionCategory>> GetSuggestionCategories(int languageId, int SchoolId)
        {
            return await _suggestionCategoryService.GetSuggestionCategory(languageId, SchoolId);
        }

        public async Task<SuggestionCategory> GetSuggestionCategoryById(int Id, short languageId)
        {
            return await _suggestionCategoryService.GetSuggestionCategoryById(Id,languageId);
        }

        public int InsertSuggestionCategory(SuggestionCategory entity)
        {
            return _suggestionCategoryService.UpdateSuggestionCategory(entity, TransactionModes.Insert);
        }

        public int UpdateSuggestionCategory(SuggestionCategory entity)
        {
            return _suggestionCategoryService.UpdateSuggestionCategory(entity, TransactionModes.Update);
        }
        public int DeleteSuggestionCategory(int Id)
        {
            SuggestionCategory entity = new SuggestionCategory(Id);
            return _suggestionCategoryService.UpdateSuggestionCategory(entity, TransactionModes.Delete);
        }
    }
}
