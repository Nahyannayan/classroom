﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IGradingTemplateService
    {
        Task<IEnumerable<GradingTemplate>> GetGradingTemplates(int languageId, int schoolId);
        Task<GradingTemplate> GetGradingTemplateById(int id, short languageId);
        int InsertGradingTemplate(GradingTemplate entity);
        int UpdateGradingTemplate(GradingTemplate entity);
        int DeleteGradingTemplate(int id);
    }
}
