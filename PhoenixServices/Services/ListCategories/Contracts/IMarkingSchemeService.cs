﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
   public interface IMarkingSchemeService
    {
        Task<IEnumerable<MarkingScheme>> GetMarkingSchemes(int schoolId, short languageId);
        Task<MarkingScheme> GetMarkingSchemeById(int id, short languageId);
        int InsertMarkingScheme(MarkingScheme entity);
        int UpdateMarkingScheme(MarkingScheme entity);
        int DeleteMarkingScheme(int id);
    }
}
