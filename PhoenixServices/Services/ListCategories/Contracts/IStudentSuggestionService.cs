﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IStudentSuggestionService
    {
        Task<IEnumerable<Suggestion>> GetStudentSuggestions();
        Task<IEnumerable<Suggestion>> GetStudentSuggestionsByUserId(int id);
        Task<IEnumerable<Suggestion>> GetStudentSuggestionsBySchoolId(int id);
        int InsertStudentSuggestion(Suggestion entity);
        int UpdateStudentSuggestion(Suggestion entity);
        int DeleteStudentSuggestion(int id, long userId);
        Task<Suggestion> GetStudentSuggestionById(int id);
        int AddUpdateThinkBoxUser(ThinkBox entity);
        Task<IEnumerable<ThinkBox>> GetThinkBoxUsersBySchoolId(int Id);
        void DeleteUserFromThinkBox(long userId, long schoolId);
        bool HasThinkBoxUserPermission(long userId);
        Task<IEnumerable<Suggestion>> PaginateStudentSuggestionByUserId(int userId, int pageNumber, int pageSize, string searchString = "", string sortBy = "");
        Task<IEnumerable<Suggestion>> PaginateStudentSuggestionBySchoolId(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy = "", short languageId=1);
        Task<Suggestion> GetStudentSuggestionByIdAndUserId(int id, long userId);
    }
}
