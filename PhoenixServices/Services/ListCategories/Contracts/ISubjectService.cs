﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ISubjectService
    {
        Task<IEnumerable<Subject>> GetSubjects(int languageId, int schoolId);
        Task<IEnumerable<Subject>> GetTopTenSubjects(int schoolId);
        Task<Subject> GetSubjectById(int id);
        bool InsertSubject(Subject entity);
        Task<IEnumerable<Objective>> GetLessonsByTopicId(int id);
        Task<IEnumerable<GroupUnit>> GetSelectedUnitByGroupId(string id);
        bool UpdateSubject(Subject entity);
        bool DeleteSubject(int id);
        Task<IEnumerable<TopicSubTopicStructure>> GetTopicSubTopicStructure(int subjectId);
        Task<IEnumerable<TopicSubTopicStructure>> GetUnitStructure(string courseIds);
        Task<IEnumerable<Objective>> GetSubtopicsObjective(int id, bool isMainSyllabus, int subjectId, bool isLesson);
        Task<IEnumerable<Objective>> GetAssignmentsObjective(int assignmentId);
        Task<IEnumerable<CourseUnit>> GetAssignmentUnit(int assignmentId);
        Task<IEnumerable<GroupUnit>> GetAssignmentCourseTopicUnit(int assignmentId);
        Task<IEnumerable<TopicSubTopicStructure>> GetTopicSubTopicStructureByMultipleSubjectIds(string subjectIds);
        Task<IEnumerable<Objective>> GetSubtopicObjectivesWithMultipleSubjects(int id, bool isMainSyllabus, string subjectIds, bool isLesson);

    }
}
