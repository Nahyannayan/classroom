﻿using Phoenix.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IGradingTemplateItemService
    {
        Task<IEnumerable<GradingTemplateItem>> GetGradingTemplateItems(int templateId,int SystemLanguageId);
        Task<GradingTemplateItem> GetGradingTemplateItemById(int id,int SystemLanguageId);
        int InsertGradingTemplateItem(GradingTemplateItem entity);
        int UpdateGradingTemplateItem(GradingTemplateItem entity);
        bool DeleteGradingTemplateItem(int id);
    }
}
