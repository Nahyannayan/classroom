﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ISuggestionCategoryService
    {
        Task<IEnumerable<SuggestionCategory>> GetSuggestionCategories(int languageId, int SchoolId);
        Task<SuggestionCategory> GetSuggestionCategoryById(int Id,short languageId);
        int InsertSuggestionCategory(SuggestionCategory entity);
        int UpdateSuggestionCategory(SuggestionCategory entity);
        int DeleteSuggestionCategory(int Id);
    }
}
