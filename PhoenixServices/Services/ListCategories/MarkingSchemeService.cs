﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class MarkingSchemeService:IMarkingSchemeService
    {
        private readonly IMarkingSchemeRepository _markingSchemeRepository;

        public MarkingSchemeService(IMarkingSchemeRepository markingSchemeRepository)
        {
            _markingSchemeRepository = markingSchemeRepository;
        }

        public int DeleteMarkingScheme(int id)
        {
            MarkingScheme entity = new MarkingScheme(id);
            return _markingSchemeRepository.UpdateMarkingScheme(entity, TransactionModes.Delete);
        }

        public async Task<MarkingScheme> GetMarkingSchemeById(int id,short languageId)
        {
            return await _markingSchemeRepository.GetMarkingSchemeById(id,languageId);
        }

        public async Task<IEnumerable<MarkingScheme>> GetMarkingSchemes(int schoolId,short languageId)
        {
            return await _markingSchemeRepository.GetMarkingSchemes(schoolId, languageId);
        }

        public int InsertMarkingScheme(MarkingScheme entity)
        {
            return _markingSchemeRepository.UpdateMarkingScheme(entity, TransactionModes.Insert);
        }

        public int UpdateMarkingScheme(MarkingScheme entity)
        {
            return _markingSchemeRepository.UpdateMarkingScheme(entity, TransactionModes.Update);
        }
    }
}
