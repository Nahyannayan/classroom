﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class GradingTemplateService : IGradingTemplateService
    {
        private readonly IGradingTemplateRepository _gradingTemplateRepository;

        public GradingTemplateService(IGradingTemplateRepository gradingTemplateRepository)
        {
            _gradingTemplateRepository = gradingTemplateRepository;
        }
        public async Task<IEnumerable<GradingTemplate>> GetGradingTemplates(int languageId, int schoolId)
        {
            return await _gradingTemplateRepository.GetGradingTemplates(languageId,schoolId);
        }

        public async Task<GradingTemplate> GetGradingTemplateById(int id,short languageId)
        {
            return await _gradingTemplateRepository.GetGradingTemplateById(id,languageId);
        }

        public int InsertGradingTemplate(GradingTemplate entity)
        {
            return _gradingTemplateRepository.UpdateGradingTemplateData(entity, TransactionModes.Insert);
        }

        public int UpdateGradingTemplate(GradingTemplate entity)
        {
            return _gradingTemplateRepository.UpdateGradingTemplateData(entity, TransactionModes.Update);
        }

        public int DeleteGradingTemplate(int id)
        {
            GradingTemplate entity = new GradingTemplate(id);
            return _gradingTemplateRepository.UpdateGradingTemplateData(entity, TransactionModes.Delete);
        }

    }
}
