﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class GradingTemplateItemService : IGradingTemplateItemService
    {
        private readonly IGradingTemplateItemRepository _gradingTemplateItemRepository;

        public GradingTemplateItemService(IGradingTemplateItemRepository gradingTemplateItemRepository)
        {
            _gradingTemplateItemRepository = gradingTemplateItemRepository;
        }

        public async Task<IEnumerable<GradingTemplateItem>> GetGradingTemplateItems(int templateId,int SystemLanguageId)
        {
            return await _gradingTemplateItemRepository.GetGradingTemplateItemsByTemplateId(templateId, SystemLanguageId); ;
        }

        public async Task<GradingTemplateItem> GetGradingTemplateItemById(int id,int SystemLanguageId)
        {
            return await _gradingTemplateItemRepository.GetGradingTemplateItembyId(id, SystemLanguageId);
        }

        public bool DeleteGradingTemplateItem(int id)
        {
            GradingTemplateItem entity = new GradingTemplateItem(id);
            return  _gradingTemplateItemRepository.UpdateGradingTemplateItemData(entity,TransactionModes.Delete) > 0;
        }
                             
        public int InsertGradingTemplateItem(GradingTemplateItem entity)
        {
            return _gradingTemplateItemRepository.UpdateGradingTemplateItemData(entity, TransactionModes.Insert);
        }

        public int UpdateGradingTemplateItem(GradingTemplateItem entity)
        {
            return _gradingTemplateItemRepository.UpdateGradingTemplateItemData(entity, TransactionModes.Update);
        }
    }
}
