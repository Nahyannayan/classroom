﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IMigrationJobService
    {
        Task<IEnumerable<MigrationDetail>> GetMigrationDetailList(long SchoolId);

        Task<bool> GetSyncDetailbyId(SyncDetailById obj);

        Task<IEnumerable<MigrationDetailCount>> GetMigrationDetailCount();
    }
}
