﻿using Phoenix.API.Repositories;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class MigrationJobService : IMigrationJobService
    {
        private readonly IMigrationJobRepository _IMigrationJobRepository;

        public MigrationJobService(IMigrationJobRepository iMigrationJobRepository)
        {
            _IMigrationJobRepository = iMigrationJobRepository;
        }
        public async Task<IEnumerable<MigrationDetail>> GetMigrationDetailList(long SchoolId)
        {
            return await _IMigrationJobRepository.GetMigrationDetailList(SchoolId);
        }
        //public async Task<IEnumerable<SyncDetailById>> GetSyncDetailbyId(SyncDetailById obj)
        //{
        //    return await _IMigrationJobRepository.GetSyncDetailbyId(obj);
        //}
        public async Task<bool> GetSyncDetailbyId(SyncDetailById obj)
           => await _IMigrationJobRepository.GetSyncDetailbyId(obj);

        public async Task<IEnumerable<MigrationDetailCount>> GetMigrationDetailCount()
        {
            return await _IMigrationJobRepository.GetMigrationDetailCount();
        }
    }
}
