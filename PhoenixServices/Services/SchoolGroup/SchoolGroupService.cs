﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class SchoolGroupService : ISchoolGroupService
    {
        private readonly ISchoolGroupRepository _schoolGroupRepository;
        private readonly IConfiguration _config;

        public SchoolGroupService(ISchoolGroupRepository schoolGroupRepository, IConfiguration config)
        {
            _schoolGroupRepository = schoolGroupRepository;
            _config = config;
        }
        public int DeleteSchoolGroup(int id, long UserId = 0)
        {
            SchoolGroup entity = new SchoolGroup(id);
            entity.CreatedById = UserId;
            return _schoolGroupRepository.UpdateSchoolGroupData(entity, TransactionModes.Delete);
        }

        public async Task<SchoolGroup> GetSchoolGroupById(int id)
        {
            return await _schoolGroupRepository.GetAsync(id);
        }

        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroups(int userId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "")
        {
            return await _schoolGroupRepository.GetSchoolGroups(userId, pageNumber, pageSize, IsBeSpokeGroup, searchString);
        }
        public async Task<IEnumerable<SchoolGroup>> GetOtherschoolgroups(int userId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "")
        {
            return await _schoolGroupRepository.GetOtherschoolgroups(userId, pageNumber, pageSize, IsBeSpokeGroup, searchString);
        }
        
        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroupsForAdminSpace(SchoolGroupsReportRequest model)
        {
            return await _schoolGroupRepository.GetSchoolGroupsForAdminSpace(model);
        }

        public async Task<IEnumerable<Course>> GetCoursesBySchoolId(long schoolId)
        {
            return await _schoolGroupRepository.GetCoursesBySchoolId(schoolId);
        }

        public int InsertSchoolGroup(SchoolGroup entity)
        {
            return _schoolGroupRepository.UpdateSchoolGroupData(entity, TransactionModes.Insert);
        }

        public int UpdateSchoolGroupData(SchoolGroup entity)
        {
            return _schoolGroupRepository.UpdateSchoolGroupData(entity, TransactionModes.Update);
        }

        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroupsByStudentId(long studentId)
        {
            return await _schoolGroupRepository.GetSchoolGroupsByStudentId(studentId);
        }

        public async Task<IEnumerable<SchoolGroup>> GetStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string searchString = "")
        {
            return await _schoolGroupRepository.GetStudentGroups(studentId, PageNumber, PageSize, IsBespokeGroup, searchString);
        }
        public async Task<IEnumerable<SchoolGroup>> GetOtherschoolStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string searchString = "")
        {
            return await _schoolGroupRepository.GetOtherschoolStudentGroups(studentId, PageNumber, PageSize, IsBespokeGroup, searchString);
        }
        
        public async Task<IEnumerable<SchoolGroup>> GetSubjectSchoolGroup(int subjectId, int schoolId)
        {
            return await _schoolGroupRepository.GetSubjectSchoolGroup(subjectId, schoolId);
        }

        public bool UpdateTeacherGivenName(SchoolGroup entity)
        {
            return _schoolGroupRepository.UpdateTeacherGivenName(entity);
        }

        public Task<IEnumerable<SchoolGroup>> GetSchoolGroupsBySchoolId(long schoolId)
        {
            return _schoolGroupRepository.GetSchoolGroupsBySchoolId(schoolId);
        }

        public Task<IEnumerable<SchoolGroup>> GetSchoolGroupsByUserId(long userId, bool isTeacher)
        {
            return _schoolGroupRepository.GetSchoolGroupsByUserId(userId, isTeacher);
        }

        public Task<IEnumerable<ListItem>> GetUnassignedMembers(short systemLanguageId, int schoolId, long userId, int? userTypeId, int schoolGroupId)
        {
            return _schoolGroupRepository.GetUnassignedMembers(systemLanguageId, schoolId, userId, userTypeId, schoolGroupId);
        }
        public Task<IEnumerable<ListItem>> GetUnassignedMembers(short systemLanguageId, int schoolId, long userId, int? userTypeId, int schoolGroupId, string selectedSchoolGrupIds)
        {
            return _schoolGroupRepository.GetUnassignedMembers(systemLanguageId, schoolId, userId, userTypeId, schoolGroupId, selectedSchoolGrupIds);
        }

        public bool AddUpdateGroupMember(List<GroupMemberMapping> groupMemberMappings)
        {
            return _schoolGroupRepository.AddUpdateGroupMember(groupMemberMappings);
        }

        public Task<IEnumerable<GroupMemberMapping>> GetAssignedMembers(int systemLanguageId, int schoolGroupId)
        {
            return _schoolGroupRepository.GetAssignedMembers(systemLanguageId, schoolGroupId);
        }

        public bool InsertGroupMessage(GroupMessage groupMessage)
        {
            return _schoolGroupRepository.InsertGroupMessage(groupMessage);
        }

        public Task<IEnumerable<SchoolGroup>> GetSchoolGroupsHavingBlogs(long id)
        {
            return _schoolGroupRepository.GetSchoolGroupsHavingBlogs(id);
        }

        public Task<IEnumerable<GroupMessage>> GetGroupMessageListByGroup(int groupId, long id)
        {
            return _schoolGroupRepository.GetGroupMessageListByGroup(groupId, id);
        }
        public bool UpdateLastSeen(long userId, int schoolGroupId, bool isParent)
        {
            return _schoolGroupRepository.UpdateLastSeen(userId, schoolGroupId, isParent);
        }

        #region Archive School Group
        public bool UpdateArchiveSchoolGroup(string schoolGroupIds, long teacherId)
        {
            return _schoolGroupRepository.UpdateArchiveSchoolGroup(schoolGroupIds, teacherId);
        }
        public Task<SchoolGroup> GetArchivedSchoolGroupsByUserId(long userId, bool isTeacher)
        {
            return _schoolGroupRepository.GetArchivedSchoolGroupsByUserId(userId, isTeacher);
        }
        public Task<SchoolGroup> GetActiveSchoolGroupsByUserId(long userId, bool isTeacher)
        {
            return _schoolGroupRepository.GetActiveSchoolGroupsByUserId(userId, isTeacher);
        }
        public async Task<IEnumerable<SchoolGroup>> GetArchivedSchoolGroups(int userId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "")
        {
            return await _schoolGroupRepository.GetArchivedSchoolGroups(userId, pageNumber, pageSize, IsBeSpokeGroup, searchString);
        }

        public async Task<bool> UpdateGroupMeeting(MeetingResponse model)
        {
            return await _schoolGroupRepository.UpdateGroupMeeting(model);
        }

        public async Task<bool> RemoveGroupMeeting(string meetingId)
        {
            return await _schoolGroupRepository.RemoveGroupMeeting(meetingId);
        }

        #endregion Archive School Group

        #region Teams Meeting

        public async Task<long> UpdateTeamsMeetingInfo(MSTeamsReponse model) => await _schoolGroupRepository.UpdateTeamsMeetingInfo(model);

        public async Task<IEnumerable<MSTeamsReponse>> GetAllTeamsMeetings(int schoolGroupId) => await _schoolGroupRepository.GetAllTeamsMeetings(schoolGroupId);

        public async Task<long> UpdateZoomMeetingInfo(ZoomMeetingResponse zoomMeetingResponse) => await _schoolGroupRepository.UpdateZoomMeetingInfo(zoomMeetingResponse);

        public async Task<bool> UpdateZoomMeetingParticipants(List<ZoomMeetingResponse> meetingParticipants, string removedParticipants) => await _schoolGroupRepository.UpdateZoomMeetingParticipants(meetingParticipants, removedParticipants);
        public async Task<IEnumerable<ZoomMeetingResponse>> GetAllZoomMeetings(int schoolGroupId, string meetingId, long userId) => await _schoolGroupRepository.GetAllZoomMeetings(schoolGroupId, meetingId, userId);
        #endregion

        #region Adobe Connect Meeting
        public async Task<bool> SaveGroupAdobeMeeting(AdobeConnectMeeting meetingInfo) => await _schoolGroupRepository.SaveGroupAdobeMeeting(meetingInfo);

        public async Task<IEnumerable<AdobeConnectMeeting>> GetAllAdobeMeetings(int schoolGroupId) => await _schoolGroupRepository.GetAllAdobeGroupMeetings(schoolGroupId);
        #endregion

        #region BBB Live Class Meetings

        public async Task<IEnumerable<MeetingResponse>> GetAllGroupBBBMeetings(int? schoolGroupId, string meetingId) => await _schoolGroupRepository.GetAllGroupBBBMeetings(schoolGroupId, meetingId);

        public async Task<IEnumerable<EventView>> GetCurrentGroupMeeting(long userId) => await _schoolGroupRepository.GetCurrentGroupMeeting(userId);

        #endregion

        #region Web Ex Meeting
        public async Task<IEnumerable<WebExMeetingData>> GetAllGroupWebExMeetings(int schoolGroupId) => await _schoolGroupRepository.GetAllGroupWebExMeetings(schoolGroupId);
        public async Task<bool> UpdateGroupWebExMeeting(WebExMeetingData meetingData) => await _schoolGroupRepository.UpdateGroupWebExMeeting(meetingData);
        #endregion


        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroupWithPagination(long userId, short pageSize, int pageNumber) => await _schoolGroupRepository.GetSchoolGroupWithPagination(userId, pageSize, pageNumber);

        public async Task<IEnumerable<SchoolLevel>> GetAllSchoolLevel(long schoolId) => await _schoolGroupRepository.GetAllSchoolLevel(schoolId);

        public async Task<IEnumerable<SchoolDepartment>> GetAllSchoolDepartment(long schoolId) => await _schoolGroupRepository.GetAllGetAllSchoolDepartment(schoolId);

        public async Task<IEnumerable<SchoolDepartment>> GetAllSchoolDepartmentBySchoolLevel(long schoolId, long schoolLevelId) => await _schoolGroupRepository.GetAllSchoolDepartmentBySchoolLevel(schoolId, schoolLevelId);


        public async Task<IEnumerable<SchoolCourse>> GetAllSchoolCoursesBySchoolLevelAndSchoolIdAndDepartmentId(long schoolId, long schoolLevelId, long departmentId) => await _schoolGroupRepository.GetAllSchoolCoursesBySchoolLevelAndSchoolIdAndDepartmentId(schoolId, schoolLevelId, departmentId);

        public async Task<IEnumerable<UserEmailAccountView>> GetSchoolGroupUserEmailAddress(string schoolGroupIds, string studentIds) => await _schoolGroupRepository.GetSchoolGroupUserEmailAddress(schoolGroupIds, studentIds);


        public string GetZoomMeetingAccessToken()
        {
            DateTime Expiry = DateTime.UtcNow.AddMinutes(20);

            string ApiKey = _config.GetValue<string>("SynchronousLesson:ApiKey");
            string ApiSecret = _config.GetValue<string>("SynchronousLesson:ApiSecret");

            int ts = (int)(Expiry - new DateTime(1970, 1, 1)).TotalSeconds;

            // Create Security key  using private key above:
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(ApiSecret));

            // length should be >256b
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Finally create a Token
            var header = new JwtHeader(credentials);

            //Zoom Required Payload
            var payload = new JwtPayload
        {
            { "iss", ApiKey},
            { "exp", ts },
        };

            var secToken = new JwtSecurityToken(header, payload);
            var handler = new JwtSecurityTokenHandler();

            // Token to String so you can use it in your client
            var tokenString = handler.WriteToken(secToken);
            return tokenString;
        }

        public async Task<IEnumerable<GroupMemberMapping>> GetMemberMailingDetails(string selectedMembers)
        => await _schoolGroupRepository.GetMemberMailingDetails(selectedMembers);

        public async Task<IEnumerable<RecordedSessionView>> GetGroupRecordedSessions(int groupId, int pageIndex, int pageSize, string searchText, string fromDate, string toDate)
        => await _schoolGroupRepository.GetGroupRecordedSessions(groupId, pageIndex, pageSize, searchText, fromDate, toDate);

        public async Task<IEnumerable<ZoomMeetingView>> GetAllLiveSessions(long userId, int pageIndex, int pageSize, string searchText, string type, string groupId)
         => await _schoolGroupRepository.GetAllLiveSessions(userId, pageIndex, pageSize, searchText, type, groupId);

        public async Task<bool> SaveUserMeetingJoinURL(ZoomMeetingResponse zoomMeetingResponse)
        => await _schoolGroupRepository.SaveUserMeetingJoinURL(zoomMeetingResponse);

        public async Task<IEnumerable<GroupMemberMapping>> GetOnlineEventParticipants(string onlineMeetingId, string selectedMembers)
       => await _schoolGroupRepository.GetOnlineEventParticipants(onlineMeetingId, selectedMembers);

        public async Task<bool> DeleteOnlineMeetingEvent(string onlineMeetingId) => await _schoolGroupRepository.DeleteOnlineMeetingEvent(onlineMeetingId);

        public async Task<bool> DeleteLiveSession(ZoomMeetingView model) => await _schoolGroupRepository.DeleteLiveSession(model);

        public async Task<bool> DeleteTeamsSession(MSTeamsReponse model) => await _schoolGroupRepository.DeleteTeamsSession(model);

        public bool InsertGroupPrimaryTeacher(GroupPrimaryTeacher model)
        {
            return _schoolGroupRepository.InsertGroupPrimaryTeacher(model);
        }

        public bool DeleteGroupPrimaryTeacher(long id)
        {
            return _schoolGroupRepository.DeleteGroupPrimaryTeacher(id);
        }

        public bool UpdateSchoolGroupsToHideClassGroups(SchoolGroup model)
        {
            return _schoolGroupRepository.UpdateSchoolGroupsToHideClassGroups(model);
        }

        public int CheckStudentGroupsAvailable(long studentId, int IsBespokeGroup = 0)
        {
            return _schoolGroupRepository.CheckStudentGroupsAvailable(studentId, IsBespokeGroup);
        }
        public IEnumerable<long> GetDisabledGroupList(long BlogId, long SchoolId) {
            return _schoolGroupRepository.GetDisabledGroupList(BlogId, SchoolId);
        }
        public int UpdateBlogWithNewUpdate(long UpdateBlogId, long UpdatFromBlogId, bool IsApproved, bool IsRejected)
        { 
            return _schoolGroupRepository.UpdateBlogWithNewUpdate(UpdateBlogId, UpdatFromBlogId, IsApproved, IsRejected);

        }
    }
}
