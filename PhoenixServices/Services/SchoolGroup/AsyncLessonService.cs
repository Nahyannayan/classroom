﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class AsyncLessonService : IAsyncLessonService
    {
        private readonly IAsyncLessonRepository _asyncLessonRepository;
        public AsyncLessonService(IAsyncLessonRepository asyncLessonRepository)
        {
            _asyncLessonRepository = asyncLessonRepository;
        }

        public int DeleteAsyncLesson(long id, long userId)
        {
            return _asyncLessonRepository.DeleteAsyncLesson(id, userId);
        }

        public int DeleteComment(long id, long userId)
        {
            return _asyncLessonRepository.DeleteComment(id, userId);
        }

        public int DeleteResourceActivity(long id, long userId)
        {
            return _asyncLessonRepository.DeleteResourceActivity(id, userId);
        }

        public Task<AsyncLesson> GetAsyncLessonById(long asynLessonId, bool? isActive)
        {
            return _asyncLessonRepository.GetAsyncLessonById(asynLessonId, isActive);
        }

        public Task<IEnumerable<AsyncLessonComments>> GetAsyncLessonComments(long? asyncLessonCommentId, long? resourceActivityId, long studentId, long asyncLessonId)
        {
            return _asyncLessonRepository.GetAsyncLessonComments(asyncLessonCommentId, resourceActivityId, studentId, asyncLessonId);
        }

        public Task<ViewAsyncLessonDetail> GetAsyncLessons(long? asyncLessonId, long? sectionId, long? moduleId, long? folderId, bool? isActive, int? IsIncludeResources, int? IsIncludeStudents)
        {
            return _asyncLessonRepository.GetAsyncLessons(asyncLessonId, sectionId, moduleId, folderId, isActive, IsIncludeResources, IsIncludeStudents);
        }

        public Task<IEnumerable<AsyncLessonStudentMapping>> GetAsyncLessonStudentMappingByAsyncLessonId(long asyncLessonId, long? userId, bool? isActive)
        {
            return _asyncLessonRepository.GetAsyncLessonStudentMappingByAsyncLessonId(asyncLessonId, userId, isActive);
        }

        public Task<IEnumerable<AsyncLessonResourcesActivities>> GetResourceActivity(long asyncLessonId, long userId, bool? isActive)
        {
            return _asyncLessonRepository.GetResourceActivity(asyncLessonId, userId, isActive);
        }

        public int InsertAsyncLesson(AsyncLesson entity)
        {
            return _asyncLessonRepository.InsertAsyncLesson(entity);
        }

        public int InsertComment(AsyncLessonComments entity)
        {
            return _asyncLessonRepository.InsertComment(entity);
        }

        public int InsertResourceActivity(List<AsyncLessonResourcesActivities> model)
        {
            return _asyncLessonRepository.InsertResourceActivity(model);
        }

        public int UpdateAsyncLesson(AsyncLesson entity)
        {
            return _asyncLessonRepository.UpdateAsyncLesson(entity);
        }

        public int UpdateComment(AsyncLessonComments entity)
        {
            return _asyncLessonRepository.UpdateComment(entity);
        }

        public int UpdateResourceActivity(AsyncLessonResourcesActivities entity)
        {
            return _asyncLessonRepository.UpdateResourceActivity(entity);
        }

        public int UpdateResourceActivityStatus(UpdateAsyncLessonResourcesActivity model)
        {
            return _asyncLessonRepository.UpdateResourceActivityStatus(model);
        }

        public long UpdateStudentMapping(AsyncLessonStudentMapping entity)
        {
            return _asyncLessonRepository.UpdateStudentMapping(entity);
        }
    }
}
