﻿using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IAsyncLessonService
    {
        int InsertAsyncLesson(AsyncLesson entity);
        int UpdateAsyncLesson(AsyncLesson entity);
        int DeleteAsyncLesson(long id, long userId);
        Task<ViewAsyncLessonDetail> GetAsyncLessons(long? asyncLessonId, long? sectionId, long? moduleId, long? folderId, bool? isActive, int? IsIncludeResources, int? IsIncludeStudents);
        Task<AsyncLesson> GetAsyncLessonById(long asynLessonId, bool? isActive);

        int InsertResourceActivity(List<AsyncLessonResourcesActivities> model);
        int UpdateResourceActivity(AsyncLessonResourcesActivities entity);
        int DeleteResourceActivity(long id, long userId);
        Task<IEnumerable<AsyncLessonResourcesActivities>> GetResourceActivity(long asyncLessonId, long userId, bool? isActive);
        int UpdateResourceActivityStatus(UpdateAsyncLessonResourcesActivity model);

        long UpdateStudentMapping(AsyncLessonStudentMapping entity);
        Task<IEnumerable<AsyncLessonStudentMapping>> GetAsyncLessonStudentMappingByAsyncLessonId(long asyncLessonId, long? userId, bool? isActive);

        int InsertComment(AsyncLessonComments entity);
        int UpdateComment(AsyncLessonComments entity);
        int DeleteComment(long id, long userId);
        Task<IEnumerable<AsyncLessonComments>> GetAsyncLessonComments(long? asyncLessonCommentId, long? resourceActivityId, long studentId, long asyncLessonId);
    }
}
