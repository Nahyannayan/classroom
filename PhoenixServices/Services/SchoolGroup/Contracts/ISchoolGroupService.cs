﻿using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
   public interface ISchoolGroupService
    {
        Task<IEnumerable<SchoolGroup>> GetSchoolGroups(int schoolId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "");
        Task<IEnumerable<SchoolGroup>> GetOtherschoolgroups(int schoolId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "");
        
        Task<SchoolGroup> GetSchoolGroupById(int id);
        Task<IEnumerable<SchoolGroup>> GetSchoolGroupsByStudentId(long studentId);
        Task<IEnumerable<SchoolGroup>> GetStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string SearchString="");
        Task<IEnumerable<SchoolGroup>> GetOtherschoolStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string SearchString = "");
        
        int InsertSchoolGroup(SchoolGroup entity);
        int UpdateSchoolGroupData(SchoolGroup entity);
        int DeleteSchoolGroup(int id,long UserId =0);
        Task<IEnumerable<SchoolGroup>> GetSubjectSchoolGroup(int subjectId,int schoolGroupId);
        bool UpdateTeacherGivenName(SchoolGroup entity);

        Task<IEnumerable<SchoolGroup>> GetSchoolGroupsBySchoolId(long schoolId);
        Task<IEnumerable<SchoolGroup>> GetSchoolGroupsByUserId(long userId, bool isTeacher);
        Task<IEnumerable<ListItem>> GetUnassignedMembers(short systemLanguageId,int schoolId, long userId, int? userTypeId, int schoolGroupId);
        Task<IEnumerable<ListItem>> GetUnassignedMembers(short systemLanguageId, int schoolId, long userId, int? userTypeId, int schoolGroupId,string selectedSchoolGroupIds);
        bool AddUpdateGroupMember(List<GroupMemberMapping> groupMemberMappings);
        Task<IEnumerable<GroupMemberMapping>> GetAssignedMembers(int systemLanguageId, int schoolGroupId);
        bool InsertGroupMessage(GroupMessage groupMessage);
        Task<IEnumerable<SchoolGroup>> GetSchoolGroupsHavingBlogs(long id);
        Task<IEnumerable<GroupMessage>> GetGroupMessageListByGroup(int groupId, long id);
        bool UpdateLastSeen(long UserId, int SchoolGroupId,bool isParent);
        bool UpdateArchiveSchoolGroup(string schoolGroupIds, long teacherId);
        Task<SchoolGroup> GetArchivedSchoolGroupsByUserId(long userId, bool isTeacher);
        Task<SchoolGroup> GetActiveSchoolGroupsByUserId(long userId, bool isTeacher);
        Task<IEnumerable<SchoolGroup>> GetArchivedSchoolGroups(int userId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "");
        Task<bool> UpdateGroupMeeting(MeetingResponse model);
        Task<bool> RemoveGroupMeeting(string meetingId);
        Task<long> UpdateTeamsMeetingInfo(MSTeamsReponse model);
        Task<IEnumerable<MSTeamsReponse>> GetAllTeamsMeetings(int schoolGroupId);
        Task<long> UpdateZoomMeetingInfo(ZoomMeetingResponse zoomMeetingResponse);
        Task<bool> UpdateZoomMeetingParticipants(List<ZoomMeetingResponse> meetingParticipants, string removedParticipants = "");
        Task<IEnumerable<GroupMemberMapping>> GetMemberMailingDetails(string selectedPlannerMemberId);
        Task<IEnumerable<ZoomMeetingResponse>> GetAllZoomMeetings(int schoolGroupId, string meetingId, long userId);
        Task<bool> SaveGroupAdobeMeeting(AdobeConnectMeeting meetingInfo);
        Task<IEnumerable<AdobeConnectMeeting>> GetAllAdobeMeetings(int schoolGroupId);
        Task<IEnumerable<MeetingResponse>> GetAllGroupBBBMeetings(int? schoolGroupId, string meetingId);
        Task<IEnumerable<EventView>> GetCurrentGroupMeeting(long userId);
        Task<IEnumerable<WebExMeetingData>> GetAllGroupWebExMeetings(int schoolGroupId);
        Task<bool> UpdateGroupWebExMeeting(WebExMeetingData meetingData);
        Task<IEnumerable<SchoolGroup>> GetSchoolGroupWithPagination(long userId, short pageSize, int pageNumber);
        Task<IEnumerable<SchoolLevel>> GetAllSchoolLevel(long schoolId);

        Task<IEnumerable<SchoolDepartment>> GetAllSchoolDepartment(long schoolId);

        Task<IEnumerable<SchoolDepartment>> GetAllSchoolDepartmentBySchoolLevel(long schoolId, long schoolLevelId);

        Task<IEnumerable<SchoolCourse>> GetAllSchoolCoursesBySchoolLevelAndSchoolIdAndDepartmentId(long schoolId, long schoolLevelId,long departmentlId);
        Task<IEnumerable<UserEmailAccountView>> GetSchoolGroupUserEmailAddress(string schoolGroupIds, string studentIds);

        string GetZoomMeetingAccessToken();
        Task<IEnumerable<RecordedSessionView>> GetGroupRecordedSessions(int groupId, int pageIndex, int pageSize, string searchText, string fromDate, string toDate);
        Task<IEnumerable<ZoomMeetingView>> GetAllLiveSessions(long userId, int pageIndex, int pageSize, string searchText, string type, string groupId);
        Task<bool> SaveUserMeetingJoinURL(ZoomMeetingResponse zoomMeetingResponse);
        Task<IEnumerable<GroupMemberMapping>> GetOnlineEventParticipants(string onlineMeetingId, string selectedMembers);
        Task<bool> DeleteOnlineMeetingEvent(string onlineMeetingId);
        Task<bool> DeleteLiveSession(ZoomMeetingView model);
        Task<IEnumerable<SchoolGroup>> GetSchoolGroupsForAdminSpace(SchoolGroupsReportRequest model);
        Task<IEnumerable<Course>> GetCoursesBySchoolId(long schoolId);
        Task<bool> DeleteTeamsSession(MSTeamsReponse model);

        bool InsertGroupPrimaryTeacher(GroupPrimaryTeacher model);
        bool DeleteGroupPrimaryTeacher(long id);
        bool UpdateSchoolGroupsToHideClassGroups(SchoolGroup model);
        int CheckStudentGroupsAvailable(long studentId, int IsBespokeGroup = 0);

        IEnumerable<long> GetDisabledGroupList(long BlogId, long SchoolId);
        int UpdateBlogWithNewUpdate(long UpdateBlogId, long UpdatFromBlogId, bool IsApproved, bool IsRejected);
    }
}
