﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class StudentListService : IStudentListService
    {
        private readonly IStudentListRepository _studentListRepository;

        public StudentListService(IStudentListRepository studentListRepository)
        {
            _studentListRepository = studentListRepository;
        }
        public async Task<IEnumerable<Student>> GetStudentsNotInGroup(int schoolId, int groupId)
        {
            return await _studentListRepository.GetStudentsNotInGroup(schoolId, groupId);
        }
        public async Task<IEnumerable<Student>> GetStudentsInGroup(int groupId)
        {
            return await _studentListRepository.GetStudentsInGroup(groupId);
        }
        public async Task<IEnumerable<Student>> GetStudentsInSelectedGroups(string groupIds)
        {
            return await _studentListRepository.GetStudentsInSelectedGroups(groupIds);
        }
        public async Task<IEnumerable<Course>> GetStudentCourses(long studentId)
        {
            return await _studentListRepository.GetStudentCourses(studentId);
        }
        public async Task<IEnumerable<Student>> GetStudentDetailsByIds(string studentIds)
        {
            return await _studentListRepository.GetStudentDetailsByIds(studentIds);
        }

        public async Task<Student> GetStudentDetailsByStudentId(int studentId)
        {
            return await _studentListRepository.GetStudentDetailsByStudentId(studentId);
        }

        public async Task<IEnumerable<StudentWithSection>> GetStudentByGradeIds(string gradeIds) =>
            await _studentListRepository.GetStudentByGradeIds(gradeIds);
        public async Task<IEnumerable<StudentWithSection>> GetStudentByGradeSection(long gradeId, long sectionId) =>
            await _studentListRepository.GetStudentByGradeSection(gradeId, sectionId);

        public async Task<IEnumerable<Student>> GetStudentDetailsByIdsPaginate(string studentIds, int page, int size) =>
             await _studentListRepository.GetStudentDetailsByIdsPaginate(studentIds, page, size);

        public async Task<IEnumerable<UserEmailAccountView>> GetParentsByStudentIds(string studentIds) =>
            await _studentListRepository.GetParentsByStudentIds(studentIds);

        public async Task<IEnumerable<Student>> GetStudentsWithStaffInGroup(int groupId)
        => await _studentListRepository.GetStudentsWithStaffInGroup(groupId);
        
    }
}
