﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class StudentAcademicService : IStudentAcademicService
    {
        private readonly IStudentAcademicRepository _studentAcademicRepository;
        public StudentAcademicService(IStudentAcademicRepository studentAcademicRepository)
        {
            _studentAcademicRepository = studentAcademicRepository;
        }

        public async Task<bool> DeleteAcademicFile(AcademicDocuments document)
        {
            return await _studentAcademicRepository.DeleteAcademicFile(document);
        }

        public async Task<StudentAcademic> GetStudentAcademicData(long id)
        {
            return await _studentAcademicRepository.GetStudentAcademicData(id);
        }

        public async Task<bool> InsertAcademicFile(File model, long id)
        {
            return await _studentAcademicRepository.InsertAcademicFile(model, id);
        }

        public async Task<bool> UpdateAcademicFileDashboardStatus(AcademicDocuments model)
        {
            return await _studentAcademicRepository.UpdateAcademicFileDashboardStatus(model);
        }

        public async Task<bool> UpdateAssignmentStatus(AssignmentStudentDetails model)
        {
            return await _studentAcademicRepository.UpdateAssignmentStatus(model);
        }

        public async Task<bool> UpdateAllAssignmentStatus(List<AssignmentStudentDetails> lstModel)
        {
            return await _studentAcademicRepository.UpdateAllAssignmentStatus(lstModel);
        }

        public async Task<bool> UpdateAssessmentReportStatus(List<StudentAssessmentReportView> lst) => await _studentAcademicRepository.UpdateAssessmentReportStatus(lst);

        public async Task<bool> DeleteAcademicAssignment(AssignmentStudentDetails model) => await _studentAcademicRepository.DeleteAcademicAssignment(model);

        public async Task<AcademicDocuments> GetStudentAcademicDetailById(int assignmentId, long userId) => await _studentAcademicRepository.GetStudentAcademicDetailById(assignmentId, userId);

        public async Task<IEnumerable<AssignmentFile>> GetStudentAcademicAssignments(long userId, string searchString)
            => await _studentAcademicRepository.GetStudentAcademicAssignments(userId, searchString);
    }
}
