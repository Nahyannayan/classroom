﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public interface IStudentAcademicService
    {
        Task<StudentAcademic> GetStudentAcademicData(long id);
        Task<bool> UpdateAssignmentStatus(AssignmentStudentDetails model);
        Task<bool> UpdateAllAssignmentStatus(List<AssignmentStudentDetails> lstModel);
        Task<bool> InsertAcademicFile(File model, long id);
        Task<bool> DeleteAcademicFile(AcademicDocuments document);
        Task<bool> UpdateAcademicFileDashboardStatus(AcademicDocuments model);
        Task<bool> UpdateAssessmentReportStatus(List<StudentAssessmentReportView> lst);
        Task<bool> DeleteAcademicAssignment(AssignmentStudentDetails model);
        Task<AcademicDocuments> GetStudentAcademicDetailById(int assignmentId, long userId);
        Task<IEnumerable<AssignmentFile>> GetStudentAcademicAssignments(long userId, string searchString);
    }
}
