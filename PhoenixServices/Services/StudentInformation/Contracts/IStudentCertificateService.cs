﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IStudentCertificateService
    {
        Task<IEnumerable<StudentCertificate>> GetAllStudentCertificates(long id);
        Task<bool> InsertCertificateFile(CertificateFile model);
        Task<bool> UpdateDeleteUserCertificate(StudentCertificate model, char mode);
        Task<IEnumerable<Certificate>> GetStudentCertificates(long id ,int isTeacher);
        Task<Certificate> GetStudentCertificatesById(long Id);
        int InsertCertificate(Certificate certificate);
        int UpdateCertificate(Certificate certificate);


    }
}
