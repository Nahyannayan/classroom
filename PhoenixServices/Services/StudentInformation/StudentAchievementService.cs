﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class StudentAchievementService : IStudentAchievementService
    {
        private readonly IStudentAchievementRepository _studentAchievementRepository;
        public StudentAchievementService(IStudentAchievementRepository studentAchievementRepository)
        {
            _studentAchievementRepository = studentAchievementRepository;
        }

        public async Task<IEnumerable<StudentIncident>> GetStudentAchievements(long userId, int pageIndex, int pageSize, string searchString, short type)
        {
            return await _studentAchievementRepository.GetStudentAchievements(userId, pageIndex, pageSize, searchString, type);
        }

        public async Task<IEnumerable<StudentAchievement>> GetStudentAchievementsWithFiles(long userId, int pageIndex, int pageSize, string searchString, long currentUserId)
        {
            return await _studentAchievementRepository.GetStudentAchievementsWithFiles(userId, pageIndex, pageSize, searchString, currentUserId);
        }

        public async Task<bool> InsertAchievement(StudentAchievement model)
        {
            return await _studentAchievementRepository.InsertAchievement(model);
        }
        public async Task<bool> DeleteAchievement(StudentAchievement model)
        {
            return await _studentAchievementRepository.DeleteAchievement(model);
        }
        public async Task<bool> UpdateAchievements(List<StudentAchievement> lstModel)
        {
            return await _studentAchievementRepository.UpdateAchievements(lstModel);
        }

        public async Task<bool> UpdateAchievementsPortfolioStatus(List<StudentAchievement> lstModel)
        {
            return await _studentAchievementRepository.UpdateAchievementsPortfolioStatus(lstModel);

        }

        public async Task<bool> UpdateAcheivementDashboardStatus(StudentIncident model)
        {
            return await _studentAchievementRepository.UpdateAcheivementDashboardStatus(model);
        }

        public async Task<bool> SaveStudentAcheivementFile(AchievementFiles model, TransactionModes mode) => await _studentAchievementRepository.SaveStudentAcheivementFile(model, mode);

        public async Task<StudentAchievement> GetStudentAcheivementById(long acheivementId) => await _studentAchievementRepository.GetStudentAcheivementById(acheivementId);

        public async Task<bool> DeleteAcheivement(StudentAchievement model) => await _studentAchievementRepository.DeleteAcheivement(model);

        public async Task<bool> UpdateAcheivementApprovalStatus(StudentAchievement model) => await _studentAchievementRepository.UpdateAcheivementApprovalStatus(model);
    }
}
