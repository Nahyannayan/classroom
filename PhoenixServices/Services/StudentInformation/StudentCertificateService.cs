﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class StudentCertificateService : IStudentCertificateService
    {
        private readonly IStudentCertificateRepository _studentCertificateRepository;
        public StudentCertificateService(IStudentCertificateRepository studentCertificateRepository)
        {
            _studentCertificateRepository = studentCertificateRepository;
        }
        public async Task<IEnumerable<StudentCertificate>> GetAllStudentCertificates(long id)
        {
            return await _studentCertificateRepository.GetAllStudentCertificates(id);
        }

        public async Task<bool> InsertCertificateFile(CertificateFile model)
        {
            return await _studentCertificateRepository.InsertCertificateFile(model);
        }

        public async Task<bool> UpdateDeleteUserCertificate(StudentCertificate model, char mode)
        {
            return await _studentCertificateRepository.UpdateDeleteUserCertificate(model, mode);
        }
        public async Task<IEnumerable<Certificate>> GetStudentCertificates(long id , int isTeacher)
        {
            return await _studentCertificateRepository.GetStudentCertificates(id, isTeacher);
        }
        public async Task <Certificate> GetStudentCertificatesById(long id)
        {
            return await _studentCertificateRepository.GetStudentCertificatesById(id);
        }
        public  int InsertCertificate(Certificate model)
        {
            return  _studentCertificateRepository.InsertCertificate(model, TransactionModes.Insert);
        }

        public int UpdateCertificate(Certificate model)
        {
            return _studentCertificateRepository.InsertCertificate(model, TransactionModes.Update);
        }
    }
}
