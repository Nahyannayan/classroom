﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class ContentLibraryService : IContentLibraryService
    {
        private readonly IContentLibraryRepository _contentLibraryRepository;

        public ContentLibraryService(IContentLibraryRepository contentLibraryRepository)
        {
            _contentLibraryRepository = contentLibraryRepository;
        }
        public Task<IEnumerable<ContentView>> GetContentLibrary(long SchoolId)
        {
            return _contentLibraryRepository.GetContentLibrary(SchoolId);
        }

        public Task<IEnumerable<SchoolLevels>> GetSchoolLevelsBySchoolId(long schoolId)
        {
            return _contentLibraryRepository.GetSchoolLevelsBySchoolId(schoolId);
        }
        public Task<IEnumerable<ContentView>> GetContentLProvider(long SchoolId, string divisionIds,string sortBy="")
        {
            return _contentLibraryRepository.GetContentLProvider(SchoolId, divisionIds, sortBy);
        }
        public async Task<IEnumerable<ContentView>> GetPaginateContentLibrary(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy="", string categoryIds = "")
        {
            return await _contentLibraryRepository.GetPaginateContentLibrary(schoolId, pageNumber, pageSize, searchString, sortBy, categoryIds);
        }
        public async Task<IEnumerable<ContentView>> GetStudentContentLibrary(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy="", string categoryIds = "")
        {
            return await _contentLibraryRepository.GetStudentContentLibrary(schoolId, pageNumber, pageSize, searchString, sortBy, categoryIds);
        }
        public int InsertContentResource(Content entity)
        {
            return _contentLibraryRepository.UpdateContentResource(entity, TransactionModes.Insert);
        }

        public int UpdateContentResource(Content entity)
        {
            return _contentLibraryRepository.UpdateContentResource(entity, TransactionModes.Update);
        }

        public int DeleteContentResource(int id, long userId)
        {
            Content entity = new Content(id);
            entity.UserId = userId;
            return _contentLibraryRepository.UpdateContentResource(entity, TransactionModes.Delete);
        }
        public async Task<Content> GetContentResourceById(int id)
        {
            return await _contentLibraryRepository.GetContentResourceById(id);
        }
        public async Task<IEnumerable<Content>> GetSubjectsByContentId(int id)
        {
            return await _contentLibraryRepository.GetSubjectsByContentId(id);
        }

        public bool ChangeContentLibraryStatus(int resourceId, bool status, long schoolId)
        {
            return _contentLibraryRepository.ChangeContentLibraryStatus(resourceId, status, schoolId);
        }
        public bool AssignContentResourceToSchool(int resourceId, bool status, long schoolId)
        {
            return _contentLibraryRepository.AssignContentResourceToSchool(resourceId, status, schoolId);
        }
    }
}
