﻿using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IContentLibraryService
    {
        Task<IEnumerable<ContentView>> GetContentLibrary(long SchoolId);
        Task<IEnumerable<ContentView>> GetContentLProvider(long SchoolId, string divisionIds, string sortBy="");
        int InsertContentResource(Content entity);
        int UpdateContentResource(Content entity);
        int DeleteContentResource(int id, long userId);
        Task<Content> GetContentResourceById(int id);
        Task<IEnumerable<Content>> GetSubjectsByContentId(int id);
        bool ChangeContentLibraryStatus(int resourceId, bool status, long schoolId);
        bool AssignContentResourceToSchool(int resourceId, bool status, long schoolId);
        Task<IEnumerable<ContentView>> GetPaginateContentLibrary(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy = "", string CategoryIds = "");
        Task<IEnumerable<ContentView>> GetStudentContentLibrary(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy="", string CategoryIds = "");
        Task<IEnumerable<SchoolLevels>> GetSchoolLevelsBySchoolId(long schoolId);
    }
}
