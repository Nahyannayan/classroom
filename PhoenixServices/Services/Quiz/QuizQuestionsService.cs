﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Common.Helpers;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class QuizQuestionsService : IQuizQuestionsService
    {
        private readonly IQuizQuestionsRepository _quizQuestionsRepository;
        public QuizQuestionsService(IQuizQuestionsRepository quizQuestionsRepository)
        {
            _quizQuestionsRepository = quizQuestionsRepository;
        }
        public bool QuizQuestionsInsert(QuizQuestionsView quizQuestionsView)
        {
            DataTable dt = new DataTable();
            DataTable dtQuizQuestionFiles = new DataTable();
            dt.Columns.Add("AnswerText");
            dt.Columns.Add("SortOrder");
            dt.Columns.Add("IsCorrectAnswer");
            dt.Columns.Add("ResourcePath");
            if (quizQuestionsView.Answers != null)
            {
                foreach (var item in quizQuestionsView.Answers)
                {
                    DataRow dr = dt.NewRow();
                    dr["AnswerText"] = item.AnswerText;
                    dr["SortOrder"] = item.SortOrder;
                    dr["IsCorrectAnswer"] = item.IsCorrectAnswer;
                    dr["ResourcePath"] = item.ResourcePath;
                    dt.Rows.Add(dr);
                }
            }
            dtQuizQuestionFiles.Columns.Add("QuizQuestionFileId", typeof(Int32));
            dtQuizQuestionFiles.Columns.Add("QuizQuestionId", typeof(Int64));
            dtQuizQuestionFiles.Columns.Add("FileName", typeof(string));
            dtQuizQuestionFiles.Columns.Add("UploadedFileName", typeof(string));
            dtQuizQuestionFiles.Columns.Add("FilePath", typeof(string));
            dtQuizQuestionFiles.Columns.Add("PhysicalPath", typeof(string));
            if (quizQuestionsView.lstDocuments.Count > 0)
            {
                foreach (var item in quizQuestionsView.lstDocuments)
                {
                    DataRow drQuizFile = dtQuizQuestionFiles.NewRow();
                    drQuizFile["QuizQuestionFileId"] = item.QuizQuestionFileId;
                    drQuizFile["QuizQuestionId"] = item.QuizQuestionId;
                    drQuizFile["FileName"] = item.FileName;
                    drQuizFile["UploadedFileName"] = item.FileName;
                    drQuizFile["FilePath"] = item.FilePath;
                    drQuizFile["PhysicalPath"] = item.PhysicalPath;
                    dtQuizQuestionFiles.Rows.Add(drQuizFile);
                }
            }
            return _quizQuestionsRepository.QuizQuestionsInsert(quizQuestionsView, dt, dtQuizQuestionFiles);
        }
        public int GetIdByQuestionType(string questionType)
        {
            return _quizQuestionsRepository.GetIdByQuestionType(questionType);
        }
        public bool ImportQuizQuestion(ImportQuizView importQuizView)
        {
            // Create Datatable for answers
            DataTable dtAnswers = new DataTable();
            dtAnswers.Columns.Add("QuizQuestionId");
            dtAnswers.Columns.Add("AnswerText");
            dtAnswers.Columns.Add("SortOrder");
            dtAnswers.Columns.Add("IsCorrectAnswer");
            dtAnswers.Columns.Add("ResourcePath");
            if (importQuizView.quizAnswersList != null)
            {
                foreach (var item in importQuizView.quizAnswersList)
                {
                    DataRow dr = dtAnswers.NewRow();
                    dr["QuizQuestionId"] = item.QuizQuestionId;
                    dr["AnswerText"] = item.AnswerText;
                    dr["SortOrder"] = item.SortOrder;
                    dr["IsCorrectAnswer"] = item.IsCorrectAnswer;
                    dr["ResourcePath"] = item.ResourcePath;
                    dtAnswers.Rows.Add(dr);
                }
            }

            // Create Datatable for Quiz questions
            DataTable dtQuestions = new DataTable();
            dtQuestions.Columns.Add("TempQuizQuestionId");
            dtQuestions.Columns.Add("QuizQuestionId");
            dtQuestions.Columns.Add("QuestionTypeId");
            dtQuestions.Columns.Add("QuestionText");
            dtQuestions.Columns.Add("IsRequired");
            dtQuestions.Columns.Add("SortOrder");
            dtQuestions.Columns.Add("Marks");
            if (importQuizView.quizAnswersList != null)
            {
                foreach (var item in importQuizView.quizQuestionsList)
                {
                    DataRow dr = dtQuestions.NewRow();
                    dr["TempQuizQuestionId"] = item.QuizQuestionId;
                    dr["QuizQuestionId"] = item.QuizQuestionId;
                    dr["QuestionTypeId"] = item.QuestionTypeId;
                    dr["QuestionText"] = item.QuestionText;
                    dr["IsRequired"] = item.IsRequired;
                    dr["SortOrder"] = item.SortOrder;
                    dr["Marks"] = item.Marks;
                    dtQuestions.Rows.Add(dr);
                }
            }

            return _quizQuestionsRepository.ImportQuizQuestion(dtQuestions, dtAnswers, importQuizView);
        }
        public bool InsertUpdatePoolQuestions(QuizQuestionsView quizQuestionsView)
        {
            return _quizQuestionsRepository.InsertUpdatePoolQuestions(quizQuestionsView);
        }

        public bool QuizQuestionsUpdate(QuizQuestionsView quizQuestionsView)
        {
            DataTable dt = new DataTable();
            DataTable dtQuizQuestionFiles = new DataTable();
            dt.Columns.Add("AnswerText");
            dt.Columns.Add("SortOrder");
            dt.Columns.Add("IsCorrectAnswer");
            dt.Columns.Add("ResourcePath");
            if (quizQuestionsView.Answers != null)
            {
                foreach (var item in quizQuestionsView.Answers)
                {
                    dt.Rows.Add(item.AnswerText, item.SortOrder, item.IsCorrectAnswer, item.ResourcePath);
                }
            }
            //if (quizQuestionsView.lstDocuments.Count > 0)
            //{
            dtQuizQuestionFiles.Columns.Add("QuizQuestionFileId", typeof(Int32));
            dtQuizQuestionFiles.Columns.Add("QuizQuestionId", typeof(Int64));
            dtQuizQuestionFiles.Columns.Add("FileName", typeof(string));
            dtQuizQuestionFiles.Columns.Add("UploadedFileName", typeof(string));
            dtQuizQuestionFiles.Columns.Add("FilePath", typeof(string));
            dtQuizQuestionFiles.Columns.Add("PhysicalPath", typeof(string));
            if (quizQuestionsView.lstDocuments.Count > 0)
            {
                foreach (var item in quizQuestionsView.lstDocuments)
                {
                    DataRow drQuizFile = dtQuizQuestionFiles.NewRow();
                    drQuizFile["QuizQuestionFileId"] = item.QuizQuestionFileId;
                    drQuizFile["QuizQuestionId"] = item.QuizQuestionId;
                    drQuizFile["FileName"] = item.FileName;
                    drQuizFile["UploadedFileName"] = item.UploadedFileName;
                    drQuizFile["FilePath"] = item.UploadedFileName;
                    drQuizFile["PhysicalPath"] = item.PhysicalPath;
                    dtQuizQuestionFiles.Rows.Add(drQuizFile);
                }
            }
            //}
            return _quizQuestionsRepository.QuizQuestionsUpdate(quizQuestionsView, dt, dtQuizQuestionFiles);
        }
        public bool QuizQuestionsDelete(QuizQuestionsView quizQuestionsView)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AnswerText");
            dt.Columns.Add("SortOrder");
            dt.Columns.Add("IsCorrectAnswer");
            if (quizQuestionsView.Answers != null)
            {
                foreach (var item in quizQuestionsView.Answers)
                {
                    dt.Rows.Add(item.AnswerText, item.SortOrder, item.IsCorrectAnswer);
                }
            }
            return _quizQuestionsRepository.QuizQuestionsDelete(quizQuestionsView, dt);
        }

        public int DeleteQuizQuestionFile(int quizQuestionFileId)
        {
            return _quizQuestionsRepository.DeleteQuizQuestionFile(quizQuestionFileId);
        }
        public int DeleteQuizQuestion(int quizQuestionId, int quizId)
        {
            return _quizQuestionsRepository.DeleteQuizQuestion(quizQuestionId, quizId);
        }
        public int DeleteMTPResource(int QuizAnswerId)
        {
            return _quizQuestionsRepository.DeleteMTPResource(QuizAnswerId);
        }
        public bool UpdateQuizAnswerData(QuizResponse quizResponse)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("QuizResponseId");
            dt.Columns.Add("QuizAnswerId");
            dt.Columns.Add("QuizQuestionId");
            dt.Columns.Add("QuizAnswerText");
            dt.Columns.Add("ObtainedMarks");
            if (quizResponse.ResponseQuestionAnswer != null)
            {
                foreach (var item in quizResponse.ResponseQuestionAnswer)
                {
                    DataRow dr = dt.NewRow();
                    dr["QuizResponseId"] = quizResponse.QuizResponseId;
                    dr["QuizAnswerId"] = item.QuizAnswerId;
                    dr["QuizQuestionId"] = item.QuizQuestionId;
                    dr["QuizAnswerText"] = item.QuizAnswerText;
                    dr["ObtainedMarks"] = decimal.Round(item.ObtainedMarks, 2, MidpointRounding.AwayFromZero);
                    dt.Rows.Add(dr);
                }
            }
            return _quizQuestionsRepository.UpdateQuizAnswerData(quizResponse, dt);
        }

        public Task<QuizResponse> GetQuizResponseByUserId(int quizId, int userId, int studentId, int taskId)
        {
            return _quizQuestionsRepository.GetQuizResponseByUserId(quizId, userId, studentId, taskId);
        }

        public Task<QuizQuestionsView> GetQuizQuestionById(int quizQuestionId)
        {
            return _quizQuestionsRepository.GetQuizQuestionById(quizQuestionId);
        }

        public Task<IEnumerable<QuizQuestionsView>> GetQuizQuestionsByQuizId(int quizId, bool IsTeacher,short languageId)
        {
            return _quizQuestionsRepository.GetQuizQuestionsByQuizId(quizId, IsTeacher, languageId);
        }
       
        public Task<IEnumerable<QuizQuestionsView>> GetQuizQuestionsPaginationByQuizId(int quizId, int PageNumber, int PageSize)
        {
            return _quizQuestionsRepository.GetQuizQuestionsPaginationByQuizId(quizId, PageNumber, PageSize);
        }
        public Task<IEnumerable<QuizQuestionsView>> GetAllQuizQuestions()
        {
            return _quizQuestionsRepository.GetAllQuizQuestions();
        }
        public Task<IEnumerable<QuizQuestionsView>> GetFilteredQuizQuestions(string courseIds, string lstObjectives)
        {
            return _quizQuestionsRepository.GetFilteredQuizQuestions(courseIds, lstObjectives);
        }
        public Task<IEnumerable<QuizQuestionFiles>> GetFilesByQuizQuestionId(int quizQuestionId)
        {
            return _quizQuestionsRepository.GetFilesByQuizQuestionId(quizQuestionId);
        }
        public Task<QuizQuestionFiles> GetFileByQuizQuestionFileId(int quizQuestionFileId)
        {
            return _quizQuestionsRepository.GetFileByQuizQuestionFileId(quizQuestionFileId);
        }
        public async Task<IEnumerable<Subject>> GetQuestionSubjects(int questionId)
        {
            return await _quizQuestionsRepository.GetQuestionSubjects(questionId);
        }

        public async Task<IEnumerable<Objective>> GetQuestionObjectives(int questionId)
        {
            return await _quizQuestionsRepository.GetQuestionObjectives(questionId);
        }

        public bool UpdateQuizCorrectAnswerByQuizAnswerId(QuizAnswersView quizAnswersView)
        {
            return _quizQuestionsRepository.UpdateQuizCorrectAnswerByQuizAnswerId(quizAnswersView);
        }

        public Task<IEnumerable<QuizAnswersView>> GetQuizAnswerByQuizQuestionId(int quizQuestionId)
        {
            return _quizQuestionsRepository.GetQuizAnswerByQuizQuestionId(quizQuestionId);
        }

        public bool SortQuestionByOrder(QuizQuestionsView quizQuestionsView)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("QuizQuestionId");
            if (quizQuestionsView.SortedQuizQuestionIds != null)
            {
                foreach (var item in quizQuestionsView.SortedQuizQuestionIds)
                {
                    DataRow dr = dt.NewRow();
                    dr["QuizQuestionId"] = Convert.ToInt64(item);
                    dt.Rows.Add(dr);
                }
            }
            return _quizQuestionsRepository.SortQuestionByOrder(quizQuestionsView, dt);
        }

    }
}
