﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class QuizService : IQuizService
    {
        private readonly IQuizRepository _IQuizRepository;
        public QuizService(IQuizRepository quizRepository)
        {
            _IQuizRepository = quizRepository;
        }
        public Task<IEnumerable<QuizView>> GetAllQuiz(long schoolId)
        {
            return _IQuizRepository.GetAllQuiz(schoolId);
        }

        public Task<QuizView> GetQuiz(int quizId, long schoolId)
        {
            return _IQuizRepository.GetQuiz(quizId, schoolId);
        }
        public Task<QuizView> GetQuizResourseDetail(int resourseId, string resourseType)
        {
            return _IQuizRepository.GetQuizResourseDetail(resourseId, resourseType);
        }
        public Task<QuizView> GetQuizDetails(int quizId, int quizResourceId, long schoolId)
        {
            return _IQuizRepository.GetQuizDetails(quizId, quizResourceId, schoolId);
        }
        public Task<QuizView> GetTaskQuizDetail(int quizId, long taskId, long schoolId)
        {
            return _IQuizRepository.GetTaskQuizDetail(quizId, taskId, schoolId);
        }
        public int LogQuizTime(int id,int resourceId,string resourceType, long userId, bool isStartQuiz)
        {
            return _IQuizRepository.LogQuizTime(id, resourceId, resourceType, userId, isStartQuiz);
        }
        public async Task<IEnumerable<Objective>> GetQuizObjectives(int quizId)
        {
            return await _IQuizRepository.GetQuizObjectives(quizId);
        }
        public Task<IEnumerable<QuizFile>> GetFilesByQuizId(int quizId)
        {
            return _IQuizRepository.GetFilesByQuizId(quizId);
        }
        public Task<QuizFile> GetFileByQuizFileId(int quizFileId)
        {
            return _IQuizRepository.GetFileByQuizFileId(quizFileId);
        }
        public Task<IEnumerable<QuizQuestionAnswerFiles>> GetQuizQuestionAnswerFiles(long resourceId, string resourceType, long questionId, long studentId)
        {
            return _IQuizRepository.GetQuizQuestionAnswerFiles(resourceId, resourceType, questionId, studentId);
        }
        public int DeleteQuizFile(int quizFileId)
        {
            return _IQuizRepository.DeleteQuizFile(quizFileId);
        }
        public int DeleteQuestionAnswerFile(int quizFileId, long userId)
        {
            return _IQuizRepository.DeleteQuestionAnswerFile(quizFileId, userId);
        }
        public async Task<IEnumerable<Subject>> GetQuizSubjectsGrade(int quizId)
        {
            return await _IQuizRepository.GetQuizSubjectsGrade(quizId);
        }
        public async Task<QuizReport> GetQuizReport(int quizId, long userId)
        {
            return await _IQuizRepository.GetQuizReport(quizId, userId);
        }
        public async Task<QuizReport> GetGroupQuizReport(int quizId, int groupQuizId)
        {
            return await _IQuizRepository.GetGroupQuizReport(quizId, groupQuizId);
        }

        public int CloneQuizData(int quizId)
        {
            return _IQuizRepository.CloneQuizData(quizId);
        }

        public int ShareQuizData(int quizId, string teacherIds, long SharedBy)
        {
            return _IQuizRepository.ShareQuizData(quizId, teacherIds, SharedBy);
        }

        public bool QuizInsert(QuizView model)
        {
            DataTable dtQuizFiles = new DataTable();
            dtQuizFiles.Columns.Add("QuizFileId", typeof(Int32));
            dtQuizFiles.Columns.Add("QuizId", typeof(Int64));
            dtQuizFiles.Columns.Add("FileName", typeof(string));
            dtQuizFiles.Columns.Add("UploadedFileName", typeof(string));
            dtQuizFiles.Columns.Add("FilePath", typeof(string));
            dtQuizFiles.Columns.Add("PhysicalPath", typeof(string));
            if (model.lstDocuments.Count > 0)
            {
                foreach (var item in model.lstDocuments)
                {
                    DataRow drQuizFile = dtQuizFiles.NewRow();
                    drQuizFile["QuizFileId"] = item.QuizFileId;
                    drQuizFile["QuizId"] = item.QuizId;
                    drQuizFile["FileName"] = item.FileName;
                    drQuizFile["UploadedFileName"] = item.FileName;
                    drQuizFile["FilePath"] = item.FilePath;
                    drQuizFile["PhysicalPath"] = item.PhysicalPath;
                    dtQuizFiles.Rows.Add(drQuizFile);
                }
            }
            return _IQuizRepository.QuizCUD(model, TransactionModes.Insert, dtQuizFiles);
        }
        public bool UploadQuestionAnswerFiles(List<QuizQuestionAnswerFiles> lstQuestionAnswerFiles)
        {
            DataTable dtQuestionAnswerFiles = new DataTable();
            dtQuestionAnswerFiles.Columns.Add("ResourceId", typeof(Int64));
            dtQuestionAnswerFiles.Columns.Add("ResourceType", typeof(string));
            dtQuestionAnswerFiles.Columns.Add("QuizQuestionId", typeof(Int64));
            dtQuestionAnswerFiles.Columns.Add("StudentId", typeof(Int64));
            dtQuestionAnswerFiles.Columns.Add("FileName", typeof(string));
            dtQuestionAnswerFiles.Columns.Add("Extension", typeof(string));
            dtQuestionAnswerFiles.Columns.Add("FilePath", typeof(string));
            dtQuestionAnswerFiles.Columns.Add("PhysicalPath", typeof(string));
            if (lstQuestionAnswerFiles.Any())
            {
                foreach (var item in lstQuestionAnswerFiles)
                {
                    DataRow drQuizFile = dtQuestionAnswerFiles.NewRow();
                    drQuizFile["ResourceId"] = item.ResourceId;
                    drQuizFile["ResourceType"] = item.ResourceType;
                    drQuizFile["QuizQuestionId"] = item.QuizQuestionId;
                    drQuizFile["StudentId"] = item.StudentId;
                    drQuizFile["FileName"] = item.FileName;
                    drQuizFile["Extension"] = item.Extension;
                    drQuizFile["FilePath"] = item.FilePath;
                    drQuizFile["PhysicalPath"] = item.PhysicalPath;
                    dtQuestionAnswerFiles.Rows.Add(drQuizFile);
                }
            }
            return _IQuizRepository.UploadQuestionAnswerFiles(dtQuestionAnswerFiles);
        }
        public bool QuizUpdate(QuizView model)
        {
            DataTable dtQuizFiles = new DataTable();
            dtQuizFiles.Columns.Add("QuizFileId", typeof(Int32));
            dtQuizFiles.Columns.Add("QuizId", typeof(Int64));
            dtQuizFiles.Columns.Add("FileName", typeof(string));
            dtQuizFiles.Columns.Add("UploadedFileName", typeof(string));
            dtQuizFiles.Columns.Add("FilePath", typeof(string));
            dtQuizFiles.Columns.Add("PhysicalPath", typeof(string));
            if (model.lstDocuments.Count > 0)
            {
                foreach (var item in model.lstDocuments)
                {
                    DataRow drQuizFile = dtQuizFiles.NewRow();
                    drQuizFile["QuizFileId"] = item.QuizFileId;
                    drQuizFile["QuizId"] = item.QuizId;
                    drQuizFile["FileName"] = item.FileName;
                    drQuizFile["UploadedFileName"] = item.UploadedFileName;
                    drQuizFile["FilePath"] = item.UploadedFileName;
                    drQuizFile["PhysicalPath"] = item.PhysicalPath;
                    dtQuizFiles.Rows.Add(drQuizFile);
                }
            }
            return _IQuizRepository.QuizCUD(model, TransactionModes.Update, dtQuizFiles);
        }
        public bool QuizDelete(QuizView model)
        {
            DataTable dtQuizFiles = new DataTable();
            dtQuizFiles.Columns.Add("QuizFileId", typeof(Int32));
            dtQuizFiles.Columns.Add("QuizId", typeof(Int64));
            dtQuizFiles.Columns.Add("FileName", typeof(string));
            dtQuizFiles.Columns.Add("UploadedFileName", typeof(string));
            dtQuizFiles.Columns.Add("FilePath", typeof(string));
            dtQuizFiles.Columns.Add("PhysicalPath", typeof(string));
            return _IQuizRepository.QuizCUD(model, TransactionModes.Delete, dtQuizFiles);
        }
        public bool ActiveDeactiveQuiz(int QuizId)
        {
            return _IQuizRepository.ActiveDeactiveQuiz(QuizId);
        }
        public Task<IEnumerable<QuizView>> GetAllQuizByUser(long Id, bool isForm)
        {
            return _IQuizRepository.GetAllQuizByUser(Id,isForm);
        }
        public async Task<QuizPagination> GetPaginateQuizByUser(QuizPaginationRequest quizView)
        {
            return await _IQuizRepository.GetPaginateQuizByUser(quizView);
        }
        public Task<IEnumerable<QuizView>> GetAllQuizByUserAndSource(long Id, long sourceId, string sourceType, bool isForm)
        {
            return _IQuizRepository.GetAllQuizByUserAndSource(Id, sourceId, sourceType, isForm);
        }
        public Task<IEnumerable<GroupQuiz>> GetAllQuizByStudentId(long studentId)
        {
            return _IQuizRepository.GetAllQuizByStudentId(studentId);
        }

        public Task<IEnumerable<QuizDetails>> GetQuizDetailsByQuizIdAndUserId(long userId, int quizId)
        {
            return _IQuizRepository.GetQuizDetailsByQuizIdAndUserId(userId,quizId);
        }

        public Task<IEnumerable<QuizAnswer>> GetAnswersByQuestionId(long userId, int questionId)
        {
            return _IQuizRepository.GetAnswersByQuestionId(userId, questionId);
        }

        public int GetQuizQuestionsCount(long userId, int quizId)
        {
            return _IQuizRepository.GetQuizQuestionsCount(userId, quizId);
        }

        public bool ActiveDeactiveSafeQuiz(int QuizId)
        {
            return  _IQuizRepository.ActiveDeactiveQuiz(QuizId);
        }

        public Task<QuizResult> GetQuizReportCardByUserId(int quizId, int userId, int resourceId, string resourceType)
        {
            return _IQuizRepository.GetQuizReportCardByUserId(quizId, userId, resourceId, resourceType);
        }

        public async Task<bool> IsSafeQuiz(int QuizId)
        {
            return await _IQuizRepository.isSafeTest(QuizId);
        }
    }
}
