﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IQuizQuestionsService
    {
        bool QuizQuestionsInsert(QuizQuestionsView quizQuestionsView);
        int GetIdByQuestionType(string questionType);
        bool ImportQuizQuestion(ImportQuizView importQuizView);
        bool QuizQuestionsUpdate(QuizQuestionsView quizQuestionsView);
        Task<QuizQuestionsView> GetQuizQuestionById(int quizQuestionId);
        Task<IEnumerable<QuizQuestionsView>> GetQuizQuestionsByQuizId(int quizId, bool IsTeacher, short languageId);
        Task<IEnumerable<QuizQuestionsView>> GetQuizQuestionsPaginationByQuizId(int quizId, int PageNumber, int PageSize);

        bool InsertUpdatePoolQuestions(QuizQuestionsView quizQuestionsView);
        Task<IEnumerable<QuizQuestionsView>> GetAllQuizQuestions();
        Task<IEnumerable<QuizQuestionFiles>> GetFilesByQuizQuestionId(int quizQuestionId);
        Task<IEnumerable<QuizQuestionsView>> GetFilteredQuizQuestions(string courseIds, string lstObjectives);
        bool UpdateQuizCorrectAnswerByQuizAnswerId(QuizAnswersView quizAnswersView);
        Task<IEnumerable<Objective>> GetQuestionObjectives(int questionId);
        Task<IEnumerable<Subject>> GetQuestionSubjects(int questionId);
        Task<QuizQuestionFiles> GetFileByQuizQuestionFileId(int quizQuestionFileId);
        Task<IEnumerable<QuizAnswersView>> GetQuizAnswerByQuizQuestionId(int quizQuestionId);
        bool SortQuestionByOrder(QuizQuestionsView quizQuestionsView);
        bool QuizQuestionsDelete(QuizQuestionsView quizQuestionsView);
        bool UpdateQuizAnswerData(QuizResponse quizResponse);
        int DeleteQuizQuestionFile(int quizQuestionFileId);
        int DeleteQuizQuestion(int quizQuestionId, int quizId);
        int DeleteMTPResource(int quizQuestionId);
        Task<QuizResponse> GetQuizResponseByUserId(int quizId, int userId, int studentId, int taskId);

    }
}
