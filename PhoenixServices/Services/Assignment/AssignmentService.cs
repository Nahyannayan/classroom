﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class AssignmentService : IAssignmentService
    {
        private readonly IAssignmentRepository _assignmentRepository;

        public AssignmentService(IAssignmentRepository assignmentRepository)
        {
            _assignmentRepository = assignmentRepository;
        }

        public async Task<Assignment> GetAssignments(int teacherId)
        {
            return await _assignmentRepository.GetAssignments(teacherId);
        }
        public async Task<Assignment> GetAssignmentById(int id)
        {
            return await _assignmentRepository.GetAsync(id);
        }

        public async Task<AssignmentStudent> GetAssignmentSubmitMarksByAssignmentStudentId(int assignmentStudentId)
        {
            return await _assignmentRepository.GetAssignmentSubmitMarksByAssignmentStudentId(assignmentStudentId);
        }

        public async Task<StudentTask> GetTaskSubmitMarksByTaskId(int taskId)
        {
            return await _assignmentRepository.GetTaskSubmitMarksByTaskId(taskId);
        }

        public async Task<AssignmentCategoryDetails> GetAssignmentCategoryMasterById(int id)
        {
            return await _assignmentRepository.GetAssignmentCategoryMasterById(id);
        }

        public async Task<Assignment> GetExcelReportDataAssignmentById(int id)
        {
            return await _assignmentRepository.GetExcelReportDataAssignmentById(id);
        }

        public int InsertAssignment(Assignment entity)
        {
            return _assignmentRepository.UpdateAssignmentData(entity, TransactionModes.Insert);
        }
        public int SaveAssignmentDetails(Assignment entity)
        {
            return _assignmentRepository.SaveAssignmentData(entity, TransactionModes.Insert);
        }


        public int UpdateAssignmentData(Assignment entity)
        {
            return _assignmentRepository.UpdateAssignmentData(entity, TransactionModes.Update);
        }

        public int DeleteAssignment(Assignment assignment)
        {
            return _assignmentRepository.UpdateAssignmentData(assignment, TransactionModes.Delete);
        }

        public bool DeleteAssignmentComment(AssignmentComment assignmentComment)
        {
            return _assignmentRepository.DeleteAssignmentComment(assignmentComment);
        }

        public async Task<IEnumerable<AssignmentStudent>> GetAssignmentStudent()
        {
            return await _assignmentRepository.GetAssignmentStudent();
        }

        public async Task<IEnumerable<AssignmentCategoryDetails>> GetAssignmentCategoryBySchoolId(long SchoolId)
        {
            return await _assignmentRepository.GetAssignmentCategoryBySchoolId(SchoolId);
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentStudentDetails(int TeacherId, AssignmentFilter loadAssignment)
        {
            return await _assignmentRepository.GetAssignmentStudentDetails(TeacherId, loadAssignment);
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAdminAssignmentStudentDetails(AssignmentFilter loadAssignment)
        {
            return await _assignmentRepository.GetAdminAssignmentStudentDetails(loadAssignment);
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetFilterSchoolGroupsById(int TeacherId, AssignmentFilter loadAssignment)
        {
            return await _assignmentRepository.GetFilterSchoolGroupsById(TeacherId, loadAssignment);
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetArchivedFilterSchoolGroupsById(int TeacherId, AssignmentFilter loadAssignment)
        {
            return await _assignmentRepository.GetArchivedFilterSchoolGroupsById(TeacherId, loadAssignment);
        }



        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentStudentDetailsById(int Id)
        {
            return await _assignmentRepository.GetAssignmentStudentDetailsById(Id);
        }
        public async Task<IEnumerable<AssignmentTask>> GetAssignmentTasksByAssignmentId(int assignmentId)
        {
            return await _assignmentRepository.GetAssignmentTasksByAssignmentId(assignmentId);
        }
        public async Task<AssignmentTask> GetTaskQuizDetailById(int taskId)
        {
            return await _assignmentRepository.GetTaskQuizDetailById(taskId);
        }
        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "")
        {
            return await _assignmentRepository.GetAssignmentByStudentId(PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy);
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetArchivedAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "")
        {
            return await _assignmentRepository.GetArchivedAssignmentByStudentId(PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy);
        }


        public async Task<IEnumerable<AssignmentStudentDetails>> GeStudentFilterSchoolGroupsById(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "", string filterVal = "")
        {
            return await _assignmentRepository.GeStudentFilterSchoolGroupsById(PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy, filterVal);
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetArchivedStudentFilterSchoolGroupsById(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "", string filterVal = "")
        {
            return await _assignmentRepository.GetArchivedStudentFilterSchoolGroupsById(PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy, filterVal);
        }



        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentIdWithoutPagination(long studentId, string assignmentType = "")
        {
            return await _assignmentRepository.GetAssignmentByStudentIdWithoutPagination(studentId, assignmentType);
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentIdWithDateRange(int PageNumber, int PageSize, long studentId, DateTime startDate, DateTime endDate, string SearchString = "", string assignmentType = "")
        {
            return await _assignmentRepository.GetAssignmentByStudentIdWithDateRange(PageNumber, PageSize, studentId, startDate, endDate, SearchString, assignmentType);
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentIdAndTeacherIdWithDateRange(int PageNumber, int PageSize, long studentId, long teacherId, DateTime startDate, DateTime endDate, string SearchString = "", string assignmentType = "")
        {
            return await _assignmentRepository.GetAssignmentByStudentIdAndTeacherIdWithDateRange(PageNumber, PageSize, studentId, teacherId, startDate, endDate, SearchString, assignmentType);
        }

        public async Task<IEnumerable<AssignmentReport>> GetAssignmentTaskQuizByAssignmentId(int assignmentId)
        {
            return await _assignmentRepository.GetAssignmentTaskQuizByAssignmentId(assignmentId);
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentsForMyPlanner(long studentId, DateTime startDate, DateTime endDate, string assignmentType = "")
        {
            return await _assignmentRepository.GetAssignmentsForMyPlanner(studentId, startDate, endDate, assignmentType);
        }

        public bool AssignGradeToStudentAssignment(int studentAssignmentId, int gradingTemplateItemId, int GradedBy)
        {
            return _assignmentRepository.AssignGradeToStudentAssignment(studentAssignmentId, gradingTemplateItemId, GradedBy);
        }


        public bool GetAssignmentPermissions(int assignmentId, long userId)
        {
            return _assignmentRepository.GetAssignmentPermissions(assignmentId, userId);
        }


        public bool AssignmentUnArchive(int assignmentId)
        {
            return _assignmentRepository.AssignmentUnArchive(assignmentId);
        }

        public bool AddEditAssignmentCategory(int AssignmentCategoryId, bool IsActive, long SchoolId)
        {
            return _assignmentRepository.AddEditAssignmentCategory(AssignmentCategoryId, IsActive, SchoolId);
        }

        public bool AddAssignmentCategoryMaster(string AssignmentCategoryTitle, string AssignmentCategoryDesc, int CategoryId, bool IsActive, long SchoolId)
        {
            return _assignmentRepository.AddAssignmentCategoryMaster(AssignmentCategoryTitle, AssignmentCategoryDesc, CategoryId, IsActive, SchoolId);
        }

        public bool SeenStudentAssignmentComment(AssignmentComment objAssignmentComment)
        {
            return _assignmentRepository.SeenStudentAssignmentComment(objAssignmentComment);
        }

        public async Task<IEnumerable<AssignmentFile>> GetAssignmentFilesByAssignmentId(int assignmentId)
        {
            return await _assignmentRepository.GetFilesByAssignmentId(assignmentId);
        }

        public async Task<IEnumerable<AssignmentFile>> GetSavedAssignmentFilesByAssignmentId(int assignmentId, long userId)
        {
            return await _assignmentRepository.GetSavedAssignmentFilesByAssignmentId(assignmentId, userId);
        }

        public async Task<bool> SaveAssignmentFiles(List<AssignmentFile> lstModel)
        {
            return await _assignmentRepository.SaveAssignmentFiles(lstModel);
        }


        public async Task<IEnumerable<TaskFile>> GetFilesbyTaskId(int taskId)
        {
            return await _assignmentRepository.GetFilesByTaskId(taskId);
        }
        public async Task<AssignmentStudent> GetStudentAsgHomeworkDetailsById(int assignmentStudentId, int IsSeenByStudent = 0)
        {
            return await _assignmentRepository.GetStudentAsgHomeworkDetailsById(assignmentStudentId, IsSeenByStudent);
        }
        public async Task<AssignmentDetails> GetStudentAssignmentDetailsById(int assignmentStudentId)
        {
            return await _assignmentRepository.GetStudentAssignmentDetailsById(assignmentStudentId);
        }
        public async Task<AssignmentTask> GetAssignmentTaskbyTaskId(int taskId)
        {
            return await _assignmentRepository.GetAssignmentTaskbyTaskId(taskId);
        }
        public async Task<IEnumerable<SchoolGroup>> GetSelectegGroupsByAssignmentId(int assignmentId)
        {
            return await _assignmentRepository.GetSelectegGroupsByAssignmentId(assignmentId);
        }
        public async Task<IEnumerable<Course>> GetSelectedCoursesByAssignmentId(int assignmentId)
        {
            return await _assignmentRepository.GetSelectedCoursesByAssignmentId(assignmentId);
        }

        public async Task<IEnumerable<AssignmentStudent>> GetStudentsByAssignmentId(int assignmentId, int GroupId)
        {
            return await _assignmentRepository.GetStudentsByAssignmentId(assignmentId, GroupId);
        }


        public async Task<IEnumerable<AssignmentStudent>> GetAssignmentCompletedStudentsByAssignmentId(int assignmentId)
        {
            return await _assignmentRepository.GetAssignmentCompletedStudentsByAssignmentId(assignmentId);
        }

        public async Task<AssignmentReport> GetExcelReportDataStudentsByAssignmentId(int assignmentId)
        {
            return await _assignmentRepository.GetExcelReportDataStudentsByAssignmentId(assignmentId);
        }

        public bool MarkAsComplete(int assignmentStudentId, bool isTeacher, int CompletedBy)
        {
            return _assignmentRepository.MarkAsComplete(assignmentStudentId, isTeacher, CompletedBy);
        }

        public async Task<GradingTemplateItem> SubmitAssignmentMarks(int assignmentStudentId, decimal submitMarks,int CompletedBy,int gradingTemplateId,int SystemLanguageId)
        {
            return await _assignmentRepository.SubmitAssignmentMarks(assignmentStudentId, submitMarks, CompletedBy, gradingTemplateId, SystemLanguageId);
        }

        public bool UploadStudentAssignmeentFiles(List<StudentAssignmentFile> lstStudentAssignmentFile, int studentId, int assignmentId, string IpDetails)
        {
            return _assignmentRepository.UploadStudentAssignmeentFiles(lstStudentAssignmentFile, studentId, assignmentId, IpDetails);
        }
        public async Task<IEnumerable<StudentAssignmentFile>> GetStudentAssignmentFiles(int studentId, int assignmentId)
        {
            return await _assignmentRepository.GetStudentAssignmentFiles(studentId, assignmentId);
        }
        public async Task<IEnumerable<AssignmentFeedbackFiles>> GetAssignmentFeedbackFiles(int assignmentStudentId, int assignmentCommentId)
        {
            return await _assignmentRepository.GetAssignmentFeedbackFiles(assignmentStudentId, assignmentCommentId);
        }

        public bool MarkAsIncomplete(int studentId, int assignmentId, bool isReSubmitAssignnment)
        {
            return _assignmentRepository.MarkAsInComplete(studentId, assignmentId, isReSubmitAssignnment);
        }

        //for student task file upload
        public bool UploadStudentTaskFiles(List<StudentTaskFile> lststudentTaskFiles, int studentId, int taskId, string IpDetails)
        {
            return _assignmentRepository.UploadStudentTaskFiles(lststudentTaskFiles, studentId, taskId, IpDetails);
        }

        public async Task<IEnumerable<StudentTaskFile>> GetStudentTaskFiles(int studentId, int taskid)
        {
            return await _assignmentRepository.GetStudentTaskFiles(studentId, taskid);
        }
        public async Task<StudentTask> GetStudentTaskDetails(int taskId, int studentId)
        {
            return await _assignmentRepository.GetStudentTaskDetails(taskId, studentId);
        }
        public async Task<AssignmentTask> GetStudentTaskDetailById(int taskId, int studentId, int assignmentStudentId, long studentTaskId)
        {
            return await _assignmentRepository.GetStudentTaskDetailById(taskId, studentId, assignmentStudentId, studentTaskId);
        }
        public bool InsertAssignmentComment(AssignmentComment entity)
        {
            DataTable dtAssignmentFeedbackFiles = new DataTable();
            int commentId = 0;

            dtAssignmentFeedbackFiles.Columns.Add("AssignmentFeedbackFileId", typeof(Int32));
            dtAssignmentFeedbackFiles.Columns.Add("AssignmentStudentId", typeof(Int64));
            dtAssignmentFeedbackFiles.Columns.Add("AssignmentCommentId", typeof(Int32));
            dtAssignmentFeedbackFiles.Columns.Add("FileName", typeof(string));
            dtAssignmentFeedbackFiles.Columns.Add("UploadedFileName", typeof(string));
            dtAssignmentFeedbackFiles.Columns.Add("FilePath", typeof(string));
            if (entity.lstDocuments != null)
            {
                if (entity.lstDocuments.Count > 0)
                {
                    foreach (var item in entity.lstDocuments)
                    {
                        DataRow drFeedbackFile = dtAssignmentFeedbackFiles.NewRow();
                        drFeedbackFile["AssignmentFeedbackFileId"] = item.AssignmentFeedbackFileId;
                        drFeedbackFile["AssignmentStudentId"] = item.AssignmentStudentId;
                        drFeedbackFile["AssignmentCommentId"] = commentId;
                        drFeedbackFile["FileName"] = item.FileName;
                        drFeedbackFile["UploadedFileName"] = item.UploadedFileName;
                        drFeedbackFile["FilePath"] = item.UploadedFileName;
                        dtAssignmentFeedbackFiles.Rows.Add(drFeedbackFile);
                    }
                }
            }

            return _assignmentRepository.InsertAssignmentComment(entity, dtAssignmentFeedbackFiles);
        }
        public async Task<IEnumerable<AssignmentComment>> GetAssignmentComments(int assignmentStudentId)
        {
            return await _assignmentRepository.GetAssignmentComments(assignmentStudentId);
        }

        public bool MarkAsCompleteTask(int studentTaskId, bool isTeacher, int CompletedBy)
        {
            return _assignmentRepository.MarkAsCompleteTask(studentTaskId, isTeacher, CompletedBy);
        }

        public async Task<GradingTemplateItem> SubmitTaskMarks(int studentTaskId,int StudentAssignmentId, decimal submitMarks, int CompletedBy, int gradingTemplateId, int SystemLanguageId)
        {
            return await _assignmentRepository.SubmitTaskMarks(studentTaskId, StudentAssignmentId, submitMarks, CompletedBy,  gradingTemplateId, SystemLanguageId);
        }

        public bool AssignGradeToStudentTask(int studentTaskId, int gradingTemplateItemId, int GradedBy)
        {
            return _assignmentRepository.AssignGradeToStudentTask(studentTaskId, gradingTemplateItemId, GradedBy);
        }
        public bool AssignGradeToStudentObjective(int studentObjectiveId, int gradingTemplateItemId)
        {
            return _assignmentRepository.AssignGradeToStudentObjective(studentObjectiveId, gradingTemplateItemId);
        }
        public async Task<IEnumerable<MyFilesTreeItem>> GetMyFilesStructure(long userId)
        {
            return await _assignmentRepository.GetMyFilesStructure(userId);
        }
        public async Task<IEnumerable<AssignmentFile>> GetAssignmentMyFile(long id, bool isFolder)
        {
            return await _assignmentRepository.GetAssignmentMyFile(id, isFolder);
        }

        public bool DeleteUploadedFiles(int FileId, int AssignmentFileId, long UserId)
        {
            return _assignmentRepository.DeleteUploadedFiles(FileId, AssignmentFileId, UserId);
        }


        public async Task<File> GetAssignmentFilebyAssignmentFileId(long id)
        {
            return await _assignmentRepository.GetAssignmentFilebyAssignmentFileId(id);
        }
        public async Task<File> GetTaskFile(long id)
        {
            return await _assignmentRepository.GetTaskFile(id);
        }
        public async Task<File> GetStudentTaskFile(long id)
        {
            return await _assignmentRepository.GetStudentTaskFile(id);
        }
        public async Task<File> GetStudentAssignmentFilebyStudentAsgFileId(long id)
        {
            return await _assignmentRepository.GetStudentAssignmentFilebyStudentAsgFileId(id);
        }

        public bool DeleteStudentAssignmentFile(int FileId, long UserId, bool IsTaskFile)
        {
            return _assignmentRepository.DeleteStudentAssignmentFile(FileId, UserId, IsTaskFile);
        }

        public bool InsertTaskFeedback(AssignmentTask assignmentTask)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SchoolId");
            dt.Columns.Add("ModuleId");
            dt.Columns.Add("SectionId");
            dt.Columns.Add("FileName");
            dt.Columns.Add("FilePath");
            dt.Columns.Add("FileTypeId");
            if (assignmentTask.Files != null)
            {
                foreach (var item in assignmentTask.Files)
                {
                    DataRow dr = dt.NewRow();
                    dr["SchoolId"] = item.SchoolId;
                    dr["ModuleId"] = item.ModuleId;
                    dr["SectionId"] = item.SectionId;
                    dr["FileName"] = item.FileName;
                    dr["FilePath"] = item.FilePath;
                    dr["FileTypeId"] = item.FileTypeId;
                    dt.Rows.Add(dr);
                }
            }
            return _assignmentRepository.InsertTaskFeedback(assignmentTask, dt);
        }
        public bool InsertStudentObjectiveFeedback(AssignmentStudentObjective studentobjective)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SchoolId");
            dt.Columns.Add("ModuleId");
            dt.Columns.Add("SectionId");
            dt.Columns.Add("FileName");
            dt.Columns.Add("FilePath");
            dt.Columns.Add("FileTypeId");
            if (studentobjective.Files != null)
            {
                foreach (var item in studentobjective.Files)
                {
                    DataRow dr = dt.NewRow();
                    dr["SchoolId"] = item.SchoolId;
                    dr["ModuleId"] = item.ModuleId;
                    dr["SectionId"] = item.SectionId;
                    dr["FileName"] = item.FileName;
                    dr["FilePath"] = item.FilePath;
                    dr["FileTypeId"] = item.FileTypeId;
                    dt.Rows.Add(dr);
                }
            }
            return _assignmentRepository.InsertStudentObjectiveFeedback(studentobjective, dt);
        }

        public bool UpdateActiveAssignment(long AssignmentId)
        {
            return _assignmentRepository.UpdateArchiveAssignment(AssignmentId);
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentListForReport(int pageNumber, int pageSize, string searchString, long userId, long schoolId, int schoolGroupId, DateTime startDate, DateTime endDate, string userType)
        {
            return await _assignmentRepository.GetAssignmentListForReport(pageNumber, pageSize, searchString, userId, schoolId, schoolGroupId, startDate, endDate, userType);
        }
        public async Task<IEnumerable<AssignmentStudentDetails>> GetArchivedAssignmentStudentDetails(int TeacherId, AssignmentFilter loadAssignment)
        {
            return await _assignmentRepository.GetArchivedAssignmentStudentDetails(TeacherId, loadAssignment);
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAdminArchivedAssignmentStudentDetails(AssignmentFilter loadAssignment)
        {
            return await _assignmentRepository.GetAdminArchivedAssignmentStudentDetails(loadAssignment);
        }
        public bool AddUpdatePeerReviewMapping(List<AssignmentPeerReview> lstPeerReviewMapping)
        {
            return _assignmentRepository.AddUpdatePeerReviewMapping(lstPeerReviewMapping);
        }

        public async Task<IEnumerable<AssignmentPeerReview>> GetPeerAssignmentDetails(long assignmentId, long userId)
        {
            return await _assignmentRepository.GetPeerAssignmentDetails(assignmentId, userId);
        }

        public async Task<IEnumerable<AssignmentPeerReview>> GetPeerMappingDetails(long assignmentId)
        {
            return await _assignmentRepository.GetPeerMappingDetails(assignmentId);
        }
        public int AddUpdateDocumentReviewDetails(List<DocumentReviewdetails> lstDocumentReview, long userId)
        {
            return _assignmentRepository.AddUpdateDocumentReviewDetails(lstDocumentReview, userId);
        }
        public async Task<IEnumerable<DocumentReviewdetails>> GetReviewedDocumentFiles(long peerReviewId)
        {
            return await _assignmentRepository.GetReviewedDocumentFiles(peerReviewId);
        }
        public async Task<DocumentReviewdetails> GetDocumentFileByStudAsgFileId(long stdAsgFileId)
        {
            return await _assignmentRepository.GetDocumentFileByStudAsgFileId(stdAsgFileId);
        }
        public async Task<AssignmentCounts> GetAssignmentStatusCounts(long id, bool isTeacher, string searchText)
        {
            return await _assignmentRepository.GetAssignmentStatusCounts(id, isTeacher, searchText);
        }
        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByUserId(int pageNumber, int pageSize, long userId, string searchString)
        {
            return await _assignmentRepository.GetAssignmentByUserId(pageNumber, pageSize, userId, searchString);
        }
        public bool MarkAsCompleteReview(long peerReviewId, long reviewedBy)
        {
            return _assignmentRepository.MarkAsCompleteReview(peerReviewId, reviewedBy);
        }
        public bool MarkAsCompleteTaskReview(long peerReviewId, long reviewedBy)
        {
            return _assignmentRepository.MarkAsCompleteTaskReview(peerReviewId, reviewedBy);
        }

        public async Task<AssignmentPeerReview> GetPeerAssignmentDetail(long peerReviewId)
        {
            return await _assignmentRepository.GetPeerAssignmentDetail(peerReviewId);
        }
        public async Task<AssignmentPeerReview> GetPeerAssignmentDetailByAssignmentStudentId(long assignmentStudentId)
        {
            return await _assignmentRepository.GetPeerAssignmentDetailByAssignmentStudentId(assignmentStudentId);
        }
        public int AddUpdateTaskDocumentReviewDetails(List<TaskDocumentReviewdetails> lstDocumentReview, long userId)
        {
            return _assignmentRepository.AddUpdateTaskDocumentReviewDetails(lstDocumentReview, userId);
        }
        public async Task<TaskPeerReview> GetPeerTaskDetail(long studentId, long taskId)
        {
            return await _assignmentRepository.GetPeerTaskDetail(studentId, taskId);
        }
        public async Task<IEnumerable<TaskPeerReview>> GetPeerTaskDetails(long assignmentId, long studentId)
        {
            return await _assignmentRepository.GetPeerTaskDetails(assignmentId, studentId);
        }
        public async Task<TaskDocumentReviewdetails> GetTaskDocumentFileByStudTaskFileId(long stdTaskFileId)
        {
            return await _assignmentRepository.GetTaskDocumentFileByStudTaskFileId(stdTaskFileId);
        }
        public async Task<IEnumerable<TaskDocumentReviewdetails>> GetTaskReviewedDocumentFiles(long taskPeerReviewId)
        {
            return await _assignmentRepository.GetTaskReviewedDocumentFiles(taskPeerReviewId);
        }
        public async Task<IEnumerable<Assignment>> GetGroupAssignments(long schoolGroupId)
        {
            return await _assignmentRepository.GetGroupAssignments(schoolGroupId);
        }
        public async Task<IEnumerable<AssignmentStudent>> GetStudentGroupAssignments(long schoolGroupId, long Userid)
        {
            return await _assignmentRepository.GetStudentGroupAssignments(schoolGroupId, Userid);
        }

        public async Task<IEnumerable<Assignment>> GetDashboardAssignmentOverview(long userId) => await _assignmentRepository.GetDashboardAssignmentOverview(userId);

        public async Task<IEnumerable<AssignmentStudentObjective>> GetStudentAssignmentObjectives(long StudentAssignmengtId)
        {
            return await _assignmentRepository.GetStudentAssignmentObjectives(StudentAssignmengtId);
        }

        public async Task<IEnumerable<File>> GetStudentAssignmentObjectiveAudiofeedback(long StudentAssignmengtId)
        {
            return await _assignmentRepository.GetStudentAssignmentObjectiveAudiofeedback(StudentAssignmengtId);
        }

        public Task<IEnumerable<QuizQuestionsView>> GetQuizQuestionsByTaskId(int taskId)
        {
            return _assignmentRepository.GetQuizQuestionsByTaskId(taskId);
        }
        public Task<GroupQuiz> GetAssignmentQuizDetailsByTaskId(int taskId)
        {
            return _assignmentRepository.GetAssignmentQuizDetailsByTaskId(taskId);
        }
        public async Task<bool> UploadStudentSharedCopyFiles(List<AssignmentStudentSharedFiles> copiedFiles) => await _assignmentRepository.UploadStudentSharedCopyFiles(copiedFiles);
        public int ShareAssignment(int assignmentId, string teacherIds) => _assignmentRepository.ShareAssignment(assignmentId, teacherIds);

        public async Task<bool> UndoAssignmentsArchive(ArchiveAssignment model) => await _assignmentRepository.UndoAssignmentsArchive(model);
    }

}
