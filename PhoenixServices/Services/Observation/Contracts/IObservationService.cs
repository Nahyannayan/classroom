﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IObservationService
    {
        bool InsertObservation(Observation entity);
        bool UpdateObservationData(Observation entity);
        bool DeleteObservation(Observation entity);      
        Task<Observation> GetObservationById(int id, long userId);
        Task<IEnumerable<ObservationFile>> GetObservationFilesById(int id);
        Task<IEnumerable<Objective>> GetObservationObjectives(int id);
        Task<IEnumerable<SchoolGroup>> GetSelectegGroupsByObservationId(int observationId);
        Task<IEnumerable<ObservationStudent>> GetObservationStudent(int id);
        Task<IEnumerable<ObservationFile>> GetObservationMyFiles(long id, bool isFolder);
        Task<IEnumerable<Observation>> GetObservationTeacherPaging(int PageNumber, int PageSize, long TeacherId, string searchString, string schoolGroupIds, string sortBy);
        Task<IEnumerable<ObservationStudent>> GetObservationStudentPaging(int pageNumber, int pageSize, long studentId, string searchString, string sortBy);
        bool DeleteObservationFile(int fileId, int observationId, long userId);
        Task<IEnumerable<Subject>> GetObservationSubjects(int observationId);
        Task<File> GetObservationFilebyObservationFileId(long id);
        Task<IEnumerable<Observation>> GetArchivedObservationTeacherPaging(int PageNumber, int PageSize, long TeacherId, string searchString);
        Task<Observation> GetObservationByUserID(int teacherId);
        bool UpdateActiveObservation(long observationId);
        Task<IEnumerable<Course>> GetSchoolCoursesByObservation(int observationId);
    }
}
