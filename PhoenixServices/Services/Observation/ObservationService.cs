﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class ObservationService : IObservationService
    {
        private readonly IObservationRepository _observationRepository;
        public ObservationService(IObservationRepository observationRepository)
        {
            _observationRepository = observationRepository;
        }
        public bool InsertObservation(Observation entity)
        {
            return _observationRepository.UpdateObservationData(entity, TransactionModes.Insert);
        }

        public bool UpdateObservationData(Observation entity)
        {
            return _observationRepository.UpdateObservationData(entity, TransactionModes.Update);
        }

        public bool DeleteObservation(Observation entity)
        {
            return _observationRepository.UpdateObservationData(entity, TransactionModes.Delete);
        }
        public async Task<Observation> GetObservationById(int id, long userId)
        {
            return await _observationRepository.GetObservationById(id, userId);
        }

        public async Task<IEnumerable<ObservationFile>> GetObservationFilesById(int id)
        {
            return await _observationRepository.GetObservationFilesById(id);
        }
        public async Task<IEnumerable<Objective>> GetObservationObjectives(int id)
        {
            return await _observationRepository.GetObservationObjectives(id);
        }
        public async Task<IEnumerable<SchoolGroup>> GetSelectegGroupsByObservationId(int id)
        {
            return await _observationRepository.GetSelectedGroupsByObservationId(id);
        }

        public async Task<IEnumerable<ObservationStudent>> GetObservationStudent(int observationId)
        {
            return await _observationRepository.GetObservationStudent(observationId);
        }

        public async Task<IEnumerable<ObservationFile>> GetObservationMyFiles(long id, bool isFolder)
        {
            return await _observationRepository.GetObservationMyFiles(id, isFolder);
        }

        public async Task<IEnumerable<Observation>> GetObservationTeacherPaging(int pageNumber, int pageSize, long teacherId, string searchString, string schoolGroupIds,string sortBy)
        {
            return await _observationRepository.GetObservationTeacherPaging(pageNumber, pageSize, teacherId, searchString, schoolGroupIds, sortBy);
        }

        public async Task<IEnumerable<ObservationStudent>> GetObservationStudentPaging(int pageNumber, int pageSize, long studentId, string searchString, string sortBy)
        {
            return await _observationRepository.GetObservationStudentPaging(pageNumber, pageSize, studentId, searchString, sortBy);
        }

        public bool DeleteObservationFile(int fileId, int observationId, long userId)
        {
            return _observationRepository.DeleteObservationFile(fileId, observationId, userId);
        }

        public async Task<IEnumerable<Subject>> GetObservationSubjects(int observationId)
        {
            return await _observationRepository.GetObservationSubjects(observationId);
        }

        public async Task<File> GetObservationFilebyObservationFileId(long id)
        {
            return await _observationRepository.GetObservationFilebyObservationFileId(id);
        }
        public async Task<IEnumerable<Observation>> GetArchivedObservationTeacherPaging(int pageNumber, int pageSize, long teacherId, string searchString)
        {
            return await _observationRepository.GetArchivedObservationTeacherPaging(pageNumber, pageSize, teacherId, searchString);
        }
        public async Task<Observation> GetObservationByUserID(int teacherId)
        {
            return await _observationRepository.GetObservationByUserID(teacherId);
        }
        public bool UpdateActiveObservation(long observationId)
        {
            return _observationRepository.UpdateActiveObservation(observationId);
        }
        public async Task<IEnumerable<Course>> GetSchoolCoursesByObservation(int observationId)
        {
            return await _observationRepository.GetSchoolCoursesByObservation(observationId);
        }
    }
}
