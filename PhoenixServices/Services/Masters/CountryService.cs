﻿using Phoenix.API.Repositories;
using Phoenix.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;

        public CountryService(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository;
        }

        public async Task<IEnumerable<Country>> GetCountries()
        {
            return await _countryRepository.GetAllAsync();
        }

        public async Task<Country> GetCountryById(int id)
        {
            return await _countryRepository.GetAsync(id);
        }
    }
}
