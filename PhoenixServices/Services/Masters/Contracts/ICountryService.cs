﻿using Phoenix.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ICountryService
    {
        Task<IEnumerable<Country>> GetCountries();
        Task<Country> GetCountryById(int id);
    }
}
