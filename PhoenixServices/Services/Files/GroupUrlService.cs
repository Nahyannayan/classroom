﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class GroupUrlService : IGroupUrlService
    {
        public IGroupUrlRepository _groupUrlRepository;
        public GroupUrlService(IGroupUrlRepository groupUrlRepository)
        {
            _groupUrlRepository= groupUrlRepository;
        }

        public Task<IEnumerable<GroupUrl>> GetAll()
        {
           return _groupUrlRepository.GetAll();
        }

        public async Task<GroupUrl> GetById(int id)
        {
            return await _groupUrlRepository.GetById(id);
        }

        public int AddUpdateGroupUrl(GroupUrl entity)
        {
            return _groupUrlRepository.AddUpdateGroupUrl(entity);
        }

        public async Task<IEnumerable<GroupUrl>> GetGroupUrlListByGroupId(int groupId)
        {
          return await _groupUrlRepository.GetGroupUrlListByGroupId(groupId);
        }
    }
}
