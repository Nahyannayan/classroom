﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class BookmarkService : IBookmarkService
    {
        private readonly IBookmarkRepository _bookmarkRepository;
        public BookmarkService(IBookmarkRepository bookmarkRepository)
        {
            _bookmarkRepository = bookmarkRepository;
        }

        public async Task<Bookmark> GetBookmark(long bookmarkId)
        {
            return await _bookmarkRepository.GetBookmark(bookmarkId);
        }

        public bool InserBookmark(Bookmark entity)
        {
            return _bookmarkRepository.AddUpdateBookmark(entity, TransactionModes.Insert);
        }

        public bool UpdateBookmark(Bookmark entity)
        {
            return _bookmarkRepository.AddUpdateBookmark(entity, TransactionModes.Update);
        }
        public async Task <IEnumerable<Bookmark>> GetBookmarks(int userId)
        {
            return await _bookmarkRepository.GetBookmarks(userId);
        }
        public async Task<IEnumerable<SchoolGroup>> GetBookmarkSchoolGroups(long bookmarkId)
        {
            return await _bookmarkRepository.GetBookmarkSchoolGroups(bookmarkId);
        }
        public bool DeleteBookmark(Bookmark entity)
        {
            return _bookmarkRepository.AddUpdateBookmark(entity, TransactionModes.Delete);
        }
    }
}
