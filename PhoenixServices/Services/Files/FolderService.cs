﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.API.Repositories;

namespace Phoenix.API.Services
{
    public class FolderService : IFolderService
    {
        private readonly IFolderRepository _folderRepository;
        public FolderService(IFolderRepository folderRepository)
        {
            _folderRepository = folderRepository;
        }

        public bool Delete(int id)
        {
            _folderRepository.DeleteAsync(id);
            return true;
        }

        public Task<IEnumerable<Folder>> GetAll()
        {
            return _folderRepository.GetAllAsync();
        }

        public Task<Folder> GetById(int id)
        {
            return _folderRepository.GetAsync(id);
        }

        public Task<IEnumerable<Folder>> GetFoldersByModuleId(int schoolId, int moduleId, bool? isActive)
        {
            return _folderRepository.GetFoldersByModuleId(schoolId, moduleId, isActive);
        }

        public Task<IEnumerable<Folder>> GetFoldersBySectionId(int sectionId, int moduleId, bool? isActive)
        {
            return _folderRepository.GetFoldersBySectionId(sectionId, moduleId, isActive);
        }

        public Task<IEnumerable<Folder>> GetFoldersByParentFolderId(int parentFolderId, int moduleId, bool? isActive)
        {
            return _folderRepository.GetFoldersByParentFolderId(parentFolderId, moduleId, isActive);
        }

        public Task<IEnumerable<Folder>> GetFoldersBySchoolId(int schoolId, bool? isActive)
        {
            return _folderRepository.GetFoldersBySchoolId(schoolId, isActive);
        }

        public Task<IEnumerable<Folder>> GetFoldersByUserId(int userId, int moduleId, bool? isActive)
        {
            return _folderRepository.GetFoldersByUserId(userId,moduleId, isActive);
        }

        public async Task<IEnumerable<FolderTree>> GetFolderTree(int folderId)
        {
            return await _folderRepository.GetFolderTree(folderId);
        }

        public int Insert(Folder entity)
        {
            return _folderRepository.Insert(entity);
        }

        public int Update(Folder entityToUpdate)
        {
            return _folderRepository.Update(entityToUpdate);
        }

        public int GetGroupPermission(int userId, int groupId)
        {
            return _folderRepository.GetGroupPermission(userId, groupId);
        }

        public bool Delete(long id, long userId)
        {
            return _folderRepository.Delete(id, userId);
        }
    }
}
