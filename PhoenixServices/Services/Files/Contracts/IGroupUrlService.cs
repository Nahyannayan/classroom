﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IGroupUrlService
    {
        Task<IEnumerable<GroupUrl>> GetAll();
        Task<GroupUrl> GetById(int id);

        int AddUpdateGroupUrl(GroupUrl entityToDelete);
        Task<IEnumerable<GroupUrl>> GetGroupUrlListByGroupId(int groupId);
        //Task<int> Insert(GroupUrl entity);
        //Task<int> Delete(GroupUrl entityToDelete);
        //Task<int> Update(GroupUrl entityToUpdate);
    }
}
