﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public interface IGroupCourseTopicService
    {
        Task<IEnumerable<GroupCourseTopic>> GetGroupCourseTopicsByGroupId(long groupId, bool? isActive);
        Task<IEnumerable<GroupCourseTopic>> GetGroupCourseTopics(long? groupCourseTopicId, long? groupId, long? topicId, long? userId, bool? isSubTopic, bool? isActive);
        Task<IEnumerable<GroupCourseTopic>> GetAll();
        Task<GroupCourseTopic> GetById(int id);
        int Insert(GroupCourseTopic entity);
        bool Delete(int id, long userId);
        int Update(GroupCourseTopic entityToUpdate);
        int UpdateUnitSortOrder(IEnumerable<GroupCourseTopic> model);
    }
}
