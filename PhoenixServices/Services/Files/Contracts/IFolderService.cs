﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public interface IFolderService
    {
        Task<IEnumerable<Folder>> GetFoldersByUserId(int userId, int moduleId, bool? isActive);
        Task<IEnumerable<Folder>> GetFoldersBySchoolId(int schoolId, bool? isActive);
        Task<IEnumerable<Folder>> GetFoldersByModuleId(int schoolId, int moduleId, bool? isActive);
        Task<IEnumerable<Folder>> GetFoldersByParentFolderId(int parentFolderId, int moduleId, bool? isActive);
        Task<IEnumerable<FolderTree>> GetFolderTree(int folderId);

        Task<IEnumerable<Folder>> GetFoldersBySectionId(int sectionId, int moduleId, bool? isActive);

        Task<IEnumerable<Folder>> GetAll();
        Task<Folder> GetById(int id);
        int Insert(Folder entity);
        bool Delete(int id);
        int Update(Folder entityToUpdate);
        int GetGroupPermission(int userId, int groupId);
        bool Delete(long id, long userId);
    }
}
