﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IGroupQuizService
    {
        int AddUpdateGroupQuiz(GroupQuiz entity);
        Task<IEnumerable<GroupQuiz>> GetGroupQuizListByGroupId(int groupId);
        Task<IEnumerable<GroupQuiz>> GetGroupQuizListByGroupIdAndDateRange(int groupId, DateTime startDate, DateTime endDate);
        Task<GroupQuiz> GetById(int id);
        bool InsertUpdateQuizAnswers(QuizResult quizResult);
        bool InsertQuizFeedback(QuizFeedback quizFeedback);
        bool LogQuizQuestionAnswer(QuizResult quizResult);
        bool UpdateQuizGrade(int gradeId, int gradingTemplateId, int quizResultId);
        Task<IEnumerable<QuizView>> GetQuizByCourse(string courseIds, int schoolId);
        Task<QuizResult> GetLogResultQuestionAnswer(int quizId, int userId, int resourceId, string resourceType);
        Task<QuizResult> GetQuizResultByUserId(int quizId, int userId, int resourceId);
        Task<GroupQuiz> GetQuizStudentDetailsByQuizId(int groupQuizId, int groupId, int selectedGroupId);
        Task<GroupQuiz> GetFormStudentDetailsByQuizId(int QuizId, int groupId);
    }
}
