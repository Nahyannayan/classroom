﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public interface IFileService
    {
        Task<IEnumerable<File>> GetFilesByUserId(int userId, int moduleId, bool? isActive);
        Task<IEnumerable<File>> GetFilesBySchoolId(int schoolId, bool? isActive);
        Task<IEnumerable<File>> GetFilesByModuleId(int schoolId, int moduleId, bool? isActive);
        Task<IEnumerable<File>> GetFilesByFolderId(int folderId, bool? isActive);
        Task<IEnumerable<FileManagementModule>> GetFileManagementModules();
        Task<IEnumerable<VLEFileType>> GetFileTypes(string DocumentType);
        Task<IEnumerable<StudentGroupsFiles>> GetStudentGroupsFiles(long userId);

        Task<IEnumerable<File>> GetFilesBySectionId(int sectionId, int moduleId, bool? isActive);

        Task<IEnumerable<File>> GetAll();
        Task<File> GetById(int id);
        bool Insert(File entity);
        bool Delete(int id, long userId);
        bool Update(File entityToUpdate);
        bool RenameFile(long fileId, string FileName, long updatedBy);
        Task<IEnumerable<File>> GetAllGroupFiles(int schoolGroupId);
        Task<int> ClearGroupFiles(SchoolGroup model);
        Task<bool> SaveCloudFiles(List<File> files);

        Task<IEnumerable<File>> GetFilesByIds(string Ids);
        Task<bool> FilesFolderBulkCreate(MyFilesModel model);
        Task<bool> FilesFolderBulkDelete(MyFilesModel model);

        Task<FileExplorer> GetGroupFileExplorer(int moduleId, long sectionId, long folderId, long studentUserId, bool isActive);
    }
}
