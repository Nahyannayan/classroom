﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IBookmarkService
    {
        Task<Bookmark> GetBookmark(long bookmarkId);
        bool InserBookmark(Bookmark entity);
        bool UpdateBookmark(Bookmark entity);
        bool DeleteBookmark(Bookmark entity);
        Task<IEnumerable<Bookmark>> GetBookmarks(int UserId);
        Task<IEnumerable<SchoolGroup>> GetBookmarkSchoolGroups(long bookmarkId);
    }
}
