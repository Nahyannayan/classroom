﻿using Phoenix.API.Repositories;
using Phoenix.Models;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class GroupQuizService: IGroupQuizService
    {
        public IGroupQuizRepository _groupQuizRepository;
        public GroupQuizService(IGroupQuizRepository groupQuizRepository)
        {
            _groupQuizRepository = groupQuizRepository;
        }

        public int AddUpdateGroupQuiz(GroupQuiz entity)
        {
            return _groupQuizRepository.AddUpdateGroupQuiz(entity);
        }

        public async Task<IEnumerable<GroupQuiz>> GetGroupQuizListByGroupId(int groupId)
        {
            return await _groupQuizRepository.GetGroupQuizListByGroupId(groupId);
        }

        public async Task<IEnumerable<GroupQuiz>> GetGroupQuizListByGroupIdAndDateRange(int groupId, DateTime startDate, DateTime endDate)
        {
            return await _groupQuizRepository.GetGroupQuizListByGroupIdAndDateRange(groupId,startDate,endDate);
        }

        public async Task<GroupQuiz> GetById(int id)
        {
            return await _groupQuizRepository.GetById(id);
        }
        public bool InsertUpdateQuizAnswers(QuizResult quizResult)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("QuizResultId");
            dt.Columns.Add("QuizAnswerId");
            dt.Columns.Add("QuizQuestionId");
            dt.Columns.Add("QuizAnswerText");
            dt.Columns.Add("ObtainedMarks");
            dt.Columns.Add("SortOrder");
            if (quizResult.ResponseQuestionAnswer != null)
            {
                foreach (var item in quizResult.ResponseQuestionAnswer)
                {
                    DataRow dr = dt.NewRow();
                    dr["QuizResultId"] = quizResult.QuizResultId;
                    dr["QuizAnswerId"] = item.QuizAnswerId;
                    dr["QuizQuestionId"] = item.QuizQuestionId;
                    dr["QuizAnswerText"] = item.QuizAnswerText;
                    dr["ObtainedMarks"] = decimal.Round(item.ObtainedMarks, 2, MidpointRounding.AwayFromZero);
                    dr["SortOrder"] = item.SortOrder;
                    dt.Rows.Add(dr);
                }
            }
            return _groupQuizRepository.InsertUpdateQuizAnswers(quizResult, dt);
        }
        public bool LogQuizQuestionAnswer(QuizResult quizResult)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ResourceId");
            dt.Columns.Add("ResourceType");
            dt.Columns.Add("QuizId");
            dt.Columns.Add("ResponseByUserId");
            dt.Columns.Add("QuizQuestionId");
            dt.Columns.Add("QuizAnswerId");
            dt.Columns.Add("QuizAnswerText");
            dt.Columns.Add("ObtainedMarks");
            if (quizResult.ResponseQuestionAnswer != null)
            {
                foreach (var item in quizResult.ResponseQuestionAnswer)
                {
                    DataRow dr = dt.NewRow();
                    dr["ResourceId"] = item.ResourceId;
                    dr["ResourceType"] = item.ResourceType;
                    dr["QuizId"] = item.QuizId;
                    dr["ResponseByUserId"] = quizResult.QuizResponseByUserId;
                    dr["QuizQuestionId"] = item.QuizQuestionId;
                    dr["QuizAnswerId"] = item.QuizAnswerId;
                    dr["QuizAnswerText"] = item.QuizAnswerText;
                    dr["ObtainedMarks"] = decimal.Round(item.ObtainedMarks, 2, MidpointRounding.AwayFromZero);
                    dt.Rows.Add(dr);
                }
            }
            return _groupQuizRepository.LogQuizQuestionAnswer(quizResult, dt);
        }
        public bool InsertQuizFeedback(QuizFeedback quizFeedback)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SchoolId");
            dt.Columns.Add("ModuleId");
            dt.Columns.Add("SectionId");
            dt.Columns.Add("FileName");
            dt.Columns.Add("FilePath");
            dt.Columns.Add("FileTypeId");
            if (quizFeedback.Files != null)
            {
                foreach (var item in quizFeedback.Files)
                {
                    DataRow dr = dt.NewRow();
                    dr["SchoolId"] = item.SchoolId;
                    dr["ModuleId"] = item.ModuleId;
                    dr["SectionId"] = item.SectionId;
                    dr["FileName"] = item.FileName;
                    dr["FilePath"] =item.FilePath;
                    dr["FileTypeId"] = item.FileTypeId;
                    dt.Rows.Add(dr);
                }
            }
            return _groupQuizRepository.InsertQuizFeedback(quizFeedback, dt);
        }

        public Task<QuizResult> GetQuizResultByUserId(int quizId, int userId,int resourceId)
        {
            return _groupQuizRepository.GetQuizResultByUserId(quizId, userId, resourceId);
        }
        public async Task<IEnumerable<QuizView>> GetQuizByCourse(string courseIds, int schoolId)
        {
            return await _groupQuizRepository.GetQuizByCourse(courseIds, schoolId);
        }
        public Task<QuizResult> GetLogResultQuestionAnswer(int quizId, int userId, int resourceId, string resourceType)
        {
            return _groupQuizRepository.GetLogResultQuestionAnswer(quizId, userId, resourceId, resourceType);
        }
        public bool UpdateQuizGrade(int gradeId, int gradingTemplateId, int quizResultId)
        {
            return _groupQuizRepository.UpdateQuizGrade(gradeId, gradingTemplateId, quizResultId);
        }
        
        public Task<GroupQuiz> GetQuizStudentDetailsByQuizId(int groupQuizId, int groupId, int selectedGroupId)
        {
            return _groupQuizRepository.GetQuizStudentDetailsByQuizId(groupQuizId, groupId, selectedGroupId);
        }
     
        public Task<GroupQuiz> GetFormStudentDetailsByQuizId(int QuizId, int groupId)
        {
            return _groupQuizRepository.GetFormStudentDetailsByQuizId(QuizId, groupId);
        }
    }
}
