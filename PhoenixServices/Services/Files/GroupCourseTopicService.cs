﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.API.Repositories;

namespace Phoenix.API.Services
{
    public class GroupCourseTopicService : IGroupCourseTopicService
    {
        private readonly IGroupCourseTopicRepository _groupCourseTopicRepository;
        public GroupCourseTopicService(IGroupCourseTopicRepository groupCourseTopicRepository)
        {
            _groupCourseTopicRepository = groupCourseTopicRepository;
        }

        public bool Delete(int id, long userId)
        {
            _groupCourseTopicRepository.Delete(id, userId);
            return true;
        }

        public async Task<IEnumerable<GroupCourseTopic>> GetAll()
        {
            return await _groupCourseTopicRepository.GetAllAsync();
        }

        public async Task<GroupCourseTopic> GetById(int id)
        {
            return await _groupCourseTopicRepository.GetAsync(id);
        }

        public async Task<IEnumerable<GroupCourseTopic>> GetGroupCourseTopics(long? groupCourseTopicId, long? groupId, long? topicId, long? userId, bool? isSubTopic, bool? isActive)
        {
            return await _groupCourseTopicRepository.GetGroupCourseTopics(groupCourseTopicId, groupId, topicId, userId, isSubTopic, isActive);
        }

        public async Task<IEnumerable<GroupCourseTopic>> GetGroupCourseTopicsByGroupId(long groupId, bool? isActive)
        {
            return await _groupCourseTopicRepository.GetGroupCourseTopicsByGroupId(groupId, isActive);
        }

        public int Insert(GroupCourseTopic entity)
        {
            return _groupCourseTopicRepository.Insert(entity);
           
        }

        public int Update(GroupCourseTopic entityToUpdate)
        {
          return  _groupCourseTopicRepository.Update(entityToUpdate);
          
        }

        public int UpdateUnitSortOrder(IEnumerable<GroupCourseTopic> model)
        {
            return _groupCourseTopicRepository.UpdateUnitSortOrder(model);
        }
    }
}
