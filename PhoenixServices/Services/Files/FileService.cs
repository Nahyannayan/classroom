﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;

namespace Phoenix.API.Services
{
    public class FileService : IFileService
    {
        private readonly IFileRepository _fileRepository;
        public FileService(IFileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }

        public bool Delete(int id, long userId)
        {
            return _fileRepository.Delete(id, userId);
             
        }

        public Task<IEnumerable<File>> GetAll()
        {
            return _fileRepository.GetAllAsync();
        }

        public Task<File> GetById(int id)
        {
            return _fileRepository.GetAsync(id);
        }

        public async Task<IEnumerable<FileManagementModule>> GetFileManagementModules()
        {
            return await _fileRepository.GetFileManagementModules();
        }

        public Task<IEnumerable<File>> GetFilesByFolderId(int folderId, bool? isActive)
        {
            return _fileRepository.GetFilesByFolderId(folderId, isActive);
        }

        public Task<IEnumerable<File>> GetFilesByModuleId(int schoolId, int moduleId, bool? isActive)
        {
            return _fileRepository.GetFilesByModuleId(schoolId, moduleId, isActive);
        }

        public Task<IEnumerable<File>> GetFilesBySectionId(int sectionId, int moduleId, bool? isActive)
        {
            return _fileRepository.GetFilesBySectionId(sectionId, moduleId, isActive);
        }

        public Task<IEnumerable<File>> GetFilesBySchoolId(int schoolId, bool? isActive)
        {
            return _fileRepository.GetFilesBySchoolId(schoolId, isActive);
        }

        public Task<IEnumerable<File>> GetFilesByUserId(int userId, int moduleId, bool? isActive)
        {
            return _fileRepository.GetFilesByUserId(userId, moduleId, isActive);
        }
        public Task<IEnumerable<StudentGroupsFiles>> GetStudentGroupsFiles(long userId)
        {
            return _fileRepository.GetStudentGroupsFiles(userId);
        }

        public async Task<IEnumerable<VLEFileType>> GetFileTypes(string DocumentType)
        {
            return await _fileRepository.GetFileTypes(DocumentType);
        }

        public bool Insert(File entity)
        {
            _fileRepository.Insert(entity);
            return true;
        }

        public bool Update(File entityToUpdate)
        {
            _fileRepository.UpdateAsync(entityToUpdate);
            return true;
        }
        public bool RenameFile(long fileId, string FileName, long updatedBy)
        {
            _fileRepository.RenameFile(fileId, FileName, updatedBy);
            return true;
        }

        public async Task<IEnumerable<File>> GetAllGroupFiles(int schoolGroupId)
        {
            return await _fileRepository.GetAllGroupFiles(schoolGroupId);
        }

        public async Task<int> ClearGroupFiles(SchoolGroup model)
        {
            return await _fileRepository.ClearGroupFiles(model);
        }
        public async Task<bool> SaveCloudFiles(List<File> files) => await _fileRepository.SaveCloudFiles(files);

        public async Task<IEnumerable<File>> GetFilesByIds(string Ids) 
        { 
            return await _fileRepository.GetFilesByIds(Ids); 
        }
        public async Task<bool> FilesFolderBulkCreate(MyFilesModel model) => await _fileRepository.FilesFolderBulkCD(model, TransactionModes.Insert);
        public async Task<bool> FilesFolderBulkDelete(MyFilesModel model) => await _fileRepository.FilesFolderBulkCD(model, TransactionModes.Delete);

        public async Task<FileExplorer> GetGroupFileExplorer(int moduleId, long sectionId, long folderId, long studentUserId, bool isActive)
        {
            return await _fileRepository.GetGroupFileExplorer(moduleId, sectionId, folderId, studentUserId, isActive);
        }
    }
}
