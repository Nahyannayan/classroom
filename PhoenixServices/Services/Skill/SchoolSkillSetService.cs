﻿using Phoenix.API.Repositories.Skill.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class SchoolSkillSetService : ISchoolSkillSetService
    {
        private readonly ISchoolSkillSetRepository _schoolSkillSetRepository;
        public SchoolSkillSetService(ISchoolSkillSetRepository schoolSkillSetRepository)
        {
            _schoolSkillSetRepository = schoolSkillSetRepository;
        }
       

        public async Task<IEnumerable<SchoolSkillSet>> GetSchoolSkillSet(int languageId, int SchoolGradeid)
        {
            return await _schoolSkillSetRepository.GetSchoolSkillSet(languageId,SchoolGradeid);
        }

        public async Task<SchoolSkillSet> GetSchoolSkillSetById(int id,short langaugeId)
        {
            return await _schoolSkillSetRepository.GetSchoolSkillSetById(id, langaugeId);
        }

        public int InsertSchoolSkillSet(SchoolSkillSet entity)
        {
            return _schoolSkillSetRepository.UpdateSchoolSkillSet(entity, TransactionModes.Insert);
        }

        public int UpdateSchoolSkillSet(SchoolSkillSet entity)
        {
            return _schoolSkillSetRepository.UpdateSchoolSkillSet(entity, TransactionModes.Update);
        }

        public int DeleteSchoolSkillSet(int id)
        {
            SchoolSkillSet entity = new SchoolSkillSet(id);
            return _schoolSkillSetRepository.UpdateSchoolSkillSet(entity,TransactionModes.Delete);
        }
    }
}
