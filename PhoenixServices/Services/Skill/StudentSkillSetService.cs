﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class StudentSkillSetService : IStudentSkillSetService
    {
        private readonly IStudentSkillSetRepository _studentSkillSetRepository;
        public StudentSkillSetService(IStudentSkillSetRepository studentSkillSetRepository)
        {
            _studentSkillSetRepository = studentSkillSetRepository;
        }
        public async Task<IEnumerable<StudentSkills>> GetStudentSkillsData(long id,short languageId)
        {
            return await _studentSkillSetRepository.GetStudentSkillsData(id, languageId);
        }

        public async Task<OperationDetails> InsertStudentSkill(StudentSkillDTO model)
        {
            return await _studentSkillSetRepository.InsertStudentSkill(model);
        }
        public async Task<OperationDetails> UpdateStudentSkillsData(int studentId, long id, int skillId, char mode)
        {
            return await _studentSkillSetRepository.UpdateStudentSkillsData(studentId, id, skillId, mode);
        }

        #region Skill Endorsement
        public async Task<bool> UpdateStudentSkillEndorsementDetails(SkillEndorsement model)
        {
            return await _studentSkillSetRepository.UpdateStudentSkillEndorsementDetails(model);
        }

        public async Task<IEnumerable<SkillEndorsement>> GetAllStudentEndorsedSkills(long userId, int pageIndex, int pageSize, string searchString = "", long currentUserId = 0)
        {
            return await _studentSkillSetRepository.GetAllStudentEndorsedSkills(userId, pageIndex, pageSize, searchString, currentUserId);
        }

        public async Task<bool> UpdateEndorseSkillStatusRating(SkillEndorsement model)
        {
            return await _studentSkillSetRepository.UpdateEndorseSkillStatusRating(model); ;
        }

        public async Task<bool> UpdateAllEndorsedSkillStatus(List<SkillEndorsement> lstModel)
        {
            return await _studentSkillSetRepository.UpdateAllEndorsedSkillStatus(lstModel); ;
        }

        public async Task<bool> DeleteEndorsedSkill(SkillEndorsement model) => await _studentSkillSetRepository.DeleteEndorsedSkill(model);

        public async Task<SkillEndorsement> GetStudentEndorsedSkillById(int skillEndorsementId) => await _studentSkillSetRepository.GetStudentEndorsedSkillById(skillEndorsementId);

        public async Task<bool> UpdateStudentEndorsementStatus(SkillEndorsement model) => await _studentSkillSetRepository.UpdateStudentEndorsementStatus(model);
        #endregion

    }
}
