﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ISchoolSkillSetService
    {
        Task<IEnumerable<SchoolSkillSet>> GetSchoolSkillSet(int languageId, int SchoolGradeid);
        Task<SchoolSkillSet> GetSchoolSkillSetById(int id, short langaugeId);
        int InsertSchoolSkillSet(SchoolSkillSet entity);
        int UpdateSchoolSkillSet(SchoolSkillSet entity);
        int DeleteSchoolSkillSet(int id);
    }
}
