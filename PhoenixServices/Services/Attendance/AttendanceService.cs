﻿using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using SIMS.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class AttendanceService : IAttendanceService
    {
        private readonly IAttendanceRepository _AttendanceRepository;

        public AttendanceService(IAttendanceRepository AttendanceRepository)
        {
            _AttendanceRepository = AttendanceRepository;
        }
        public async Task<IEnumerable<Models.AttendanceDetails>> GetAttendanceByIdAndDate(long acd_id, int ttm_id, string username, string entrydate, string grade = null, string section = null, string AttendanceType = "Session1")
        {
            return await _AttendanceRepository.GetAttendanceByIdAndDate(acd_id,ttm_id, username, entrydate, grade, section, AttendanceType);
        }
        public async Task<int> InsertAttendanceDetails(string student_xml, string entry_date, string username, int alg_id, int ttm_id = 0, string sct_id = "", string GRD_ID = "", long ACD_ID = 0, long BSU_ID = 0, long SHF_ID = 0, long STM_ID = 0, string AttendanceType = "")
        {
           return await _AttendanceRepository.InsertAttendanceDetails(student_xml, entry_date, username, alg_id, ttm_id, sct_id , GRD_ID, ACD_ID, BSU_ID, SHF_ID, STM_ID, AttendanceType);
        }

        public async Task<IEnumerable<ATTENDENCE_ANALYSIS>> Get_ATTENDENCE_ANALYSIS(String STU_ID)
        {
            return await _AttendanceRepository.Get_ATTENDENCE_ANALYSIS(STU_ID);
        }

        public async Task<IEnumerable<AttendenceBySession>> Get_AttendenceBySession(String STU_ID,DateTime EndDate)
        {
            return await _AttendanceRepository.Get_AttendenceBySession(STU_ID, EndDate);
        }

        public async Task<IEnumerable<AttendenceSessionCode>> Get_AttendenceSessionCode(String STU_ID, DateTime EndDate)
        {
            return await _AttendanceRepository.Get_AttendenceSessionCode(STU_ID, EndDate);
        }
        public async Task<IEnumerable<AttendanceChart>> Get_AttendanceChartMain(String STU_ID)
        {
            return await _AttendanceRepository.Get_AttendanceChartMain(STU_ID);
        }
        
        public async Task<IEnumerable<RoomAttendance>> GetRoomAttendanceDetails( long Coursegroupid, string entryDate)
        {
            return await _AttendanceRepository.GetRoomAttendanceDetails(Coursegroupid, entryDate);
        }

        public async Task<IEnumerable<RoomAttendanceHeader>> GetRoomAttendanceHeader(long SGR_ID, DateTime ENTRY_DATE)
        {
            return await _AttendanceRepository.GetRoomAttendanceHeader(SGR_ID, ENTRY_DATE);
        }

        public bool InsertUpdateRoomAttendance(string SchoolId, string UserId, long acd_id, int isDailyWeekly, long schoolGroupId, long teacherId,List<StudentRoomAttendance> objStudentRoomAttendance, DateTime entryDate)
        {
            return _AttendanceRepository.InsertUpdateRoomAttendance( SchoolId,  UserId, acd_id,  isDailyWeekly,  schoolGroupId,  teacherId, objStudentRoomAttendance,  entryDate);
        }

        public async Task<IEnumerable<StudentRoomAttendance>> GetRoomAttendanceRemarksList(string entryDate, string schoolId, string UserId, int GroupId)
        {
            return await _AttendanceRepository.GetRoomAttendanceRemarksList(entryDate, schoolId, UserId, GroupId);
        }

        public async Task<IEnumerable<GradeSectionAccess>> GetGradeSectionAccesses(long schoolId, long academicYear, string userName, string IsSuperUser, int gradeAccess, string gradeId)
        {
            return await _AttendanceRepository.GetGradeSectionAccesses(schoolId, academicYear, userName, IsSuperUser, gradeAccess, gradeId);
        }

        public async Task<IEnumerable<AttendanceSessionType>> GetAttendanceTypeByEntryDate(int acdId, string schoolId, DateTime AttendanceDt, string GrdId)
        {
            return await _AttendanceRepository.GetAttendanceTypeByEntryDate( acdId,  schoolId,  AttendanceDt,  GrdId);
        }

        public async Task<IEnumerable<AuthorizedStaffDetails>> GetGradeSectionByUserId(long Id,long currentAcademicYearId)
        {
            return await _AttendanceRepository.GetGradeSectionByUserId(Id, currentAcademicYearId);
        }

        public async Task<IEnumerable<AttendanceStudent>> GetDailyAttendanceDetails(long gradeId, long sectionId, long sessionTypeId, DateTime asOnDate, long schoolId)
        {
            return await _AttendanceRepository.GetDailyAttendanceDetails(gradeId, sectionId, sessionTypeId, asOnDate,  schoolId);
        }

        public bool SaveDailyAttendance(List<AttendanceStudent> objAttendanceStudent, long userId, int IsMobile)
        {
            return _AttendanceRepository.SaveDailyAttendance(objAttendanceStudent, userId, IsMobile);
        }

        public async Task<IEnumerable<ClassAttendance>> GetClassAttendanceHeader(long schoolId, long schoolGroupId, DateTime entryDate)
        {
            return await _AttendanceRepository.GetClassAttendanceHeader(schoolId, schoolGroupId, entryDate);
        }

        public async Task<IEnumerable<ClassAttendanceDetails>> GetClassAttendanceDetails(long schoolGroupId, DateTime entryDate)
        {
            return await _AttendanceRepository.GetClassAttendanceDetails( schoolGroupId, entryDate);
        }

        public async Task<IEnumerable<AttendanceWeekend>> GetWeekEndBySchoolId(long schoolId, long academicyearId, long schoolgradeId, long courseId, DateTime date, short languageId)
        {
            return await _AttendanceRepository.GetWeekEndBySchoolId(schoolId,  academicyearId,  schoolgradeId,  courseId,  date,  languageId);
        }

        public async Task<IEnumerable<GradeSessionType>> GetGradeSessionByGradeId(long schoolGradeId)
        {
            return await _AttendanceRepository.GetGradeSessionByGradeId(schoolGradeId);
        }

        public async Task<IEnumerable<StaffCurriculumn>> GetCurriculumByAuthorizedStaffId(long staffId)
        {
            return await _AttendanceRepository.GetCurriculumByAuthorizedStaffId(staffId);
        }

        public async Task<IEnumerable<TimeTableGroup>> GetGroupsfromTimeTable(long teacherId, DateTime courseDate)
        {
            return await _AttendanceRepository.GetGroupsfromTimeTable(teacherId, courseDate);
        }

        public async Task<IEnumerable<ClassAttendance>> GetPreviousClassAttendanceHeader(long schoolId, long schoolGroupId, DateTime entryDate)
        {
            return await _AttendanceRepository.GetPreviousClassAttendanceHeader(schoolId, schoolGroupId, entryDate);
        }

        public async Task<IEnumerable<ClassAttendanceDetails>> GetPreviousClassAttendanceDetails(long schoolGroupId, DateTime entryDate)
        {
            return await _AttendanceRepository.GetPreviousClassAttendanceDetails(schoolGroupId, entryDate);
        }

        public async Task<IEnumerable<ParameterMappingList>> GetParameterMappingByAcademicId(long academicYearId, short languageId)
        {
            return await _AttendanceRepository.GetParameterMappingByAcademicId(academicYearId, languageId);
        }

        public async Task<MergeServiceModelForGradeSection> GetRequiredParamBySchoolUserId(long schoolId, long id, long academicYearId, short languageId)
        {
            return await _AttendanceRepository.GetRequiredParamBySchoolUserId( schoolId,  id,  academicYearId,  languageId);
        }

        public async Task<ClassAttendanceHeaderNDetails> GetClassAttendanceHeaderNDetails(long schoolId, long schoolGroupId, DateTime entryDate)
        {
            return await _AttendanceRepository.GetClassAttendanceHeaderNDetails(schoolId, schoolGroupId, entryDate);
        }
    }
}
