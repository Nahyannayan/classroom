﻿using Phoenix.API.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IAttendanceService
    {
        Task<IEnumerable<AttendanceDetails>> GetAttendanceByIdAndDate(long acd_id, int ttm_id, string username, string entrydate, string grade = null, string section = null, string AttendanceType = "Session1");
        Task<int> InsertAttendanceDetails(string student_xml, string entry_date, string username, int alg_id, int ttm_id = 0, string sct_id = "", string GRD_ID = "", long ACD_ID = 0, long BSU_ID = 0, long SHF_ID = 0, long STM_ID = 0, string AttendanceType = "");
        Task<IEnumerable<ATTENDENCE_ANALYSIS>> Get_ATTENDENCE_ANALYSIS(String STU_ID);

        Task<IEnumerable<AttendenceBySession>> Get_AttendenceBySession(String STU_ID,DateTime EndDate);

        Task<IEnumerable<AttendenceSessionCode>> Get_AttendenceSessionCode(String STU_ID, DateTime EndDate);
        Task<IEnumerable<AttendanceChart>> Get_AttendanceChartMain(String STU_ID);
      
        Task<IEnumerable<RoomAttendance>> GetRoomAttendanceDetails(long Coursegroupid,string entryDate);
        Task<IEnumerable<RoomAttendanceHeader>> GetRoomAttendanceHeader(long SGR_ID, DateTime ENTRY_DATE);

        bool InsertUpdateRoomAttendance(string SchoolId, string UserId, long acd_id, int isDailyWeekly, long schoolGroupId, long teacherId, List<StudentRoomAttendance> objStudentRoomAttendance, DateTime entryDate);

        Task<IEnumerable<StudentRoomAttendance>> GetRoomAttendanceRemarksList(string entryDate, string schoolId, string UserId, int GroupId);

        Task<IEnumerable<GradeSectionAccess>> GetGradeSectionAccesses(long schoolId, long academicYear, string userName, string IsSuperUser, int gradeAccess, string gradeId);
        Task<IEnumerable<AttendanceSessionType>> GetAttendanceTypeByEntryDate(int acdId, string schoolId, DateTime AttendanceDt, string GrdId);
        Task<IEnumerable<AuthorizedStaffDetails>> GetGradeSectionByUserId(long Id,long currentAcademicYearId);
        Task<IEnumerable<AttendanceStudent>> GetDailyAttendanceDetails(long gradeId, long sectionId, long sessionTypeId, DateTime asOnDate, long schoolId);
        bool SaveDailyAttendance(List<AttendanceStudent> objAttendanceStudent, long userId,int IsMobile);

        Task<IEnumerable<ClassAttendance>> GetClassAttendanceHeader(long schoolId, long schoolGroupId, DateTime entryDate);
        Task<IEnumerable<ClassAttendanceDetails>> GetClassAttendanceDetails(long schoolGroupId, DateTime entryDate);

        Task<IEnumerable<AttendanceWeekend>> GetWeekEndBySchoolId(long schoolId, long academicyearId, long schoolgradeId, long courseId, DateTime date, short languageId);
        Task<IEnumerable<GradeSessionType>> GetGradeSessionByGradeId(long schoolGradeId);

        Task<IEnumerable<StaffCurriculumn>> GetCurriculumByAuthorizedStaffId(long staffId);
        Task<IEnumerable<TimeTableGroup>> GetGroupsfromTimeTable(long teacherId, DateTime courseDate);
        Task<IEnumerable<ClassAttendance>> GetPreviousClassAttendanceHeader(long schoolId, long schoolGroupId, DateTime entryDate);
        Task<IEnumerable<ClassAttendanceDetails>> GetPreviousClassAttendanceDetails(long schoolGroupId, DateTime entryDate);
        Task<IEnumerable<ParameterMappingList>> GetParameterMappingByAcademicId(long academicYearId, short languageId);
        Task<MergeServiceModelForGradeSection> GetRequiredParamBySchoolUserId(long schoolId, long id, long academicYearId, short languageId);
        Task<ClassAttendanceHeaderNDetails> GetClassAttendanceHeaderNDetails(long schoolId, long schoolGroupId, DateTime entryDate);
    }
}
