﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class EventsService : IEventsService
    {
        private readonly IEventsRepository _eventsRepository;
        public EventsService(IEventsRepository eventsRepository)
        {
            _eventsRepository = eventsRepository;
        }
        public int EventInsert(EventView eventView)
        {
            return _eventsRepository.EventCUD(eventView, TransactionModes.Insert);
        }
        public int CancelEvent(int eventId, long userId)
        {
            return _eventsRepository.CancelEvent(eventId, userId);
        }

        public bool DeleteEventFile(int eventId)
        {
            return _eventsRepository.DeleteEventFile(eventId);
        }

        public int EventUpdate(EventView eventView)
        {
            return _eventsRepository.EventCUD(eventView, TransactionModes.Update);
        }
        public bool AcceptEventRequest(int eventId, long userId)
        {
            return _eventsRepository.AcceptEventRequest(eventId, userId);
        }

        public bool AcceptEventRequestExternalUser(int eventId, string emailId)
        {
            return _eventsRepository.AcceptEventRequestExternalUser(eventId, emailId);
        }

        public Task<IEnumerable<EventView>> GetEvent(int id, long? userId,short languageId)
        {
            return _eventsRepository.GetEvent(id, userId, languageId);
        }
        public Task<IEnumerable<EventCategoryView>> GetEventCategory(long schoolId)
        {
            return _eventsRepository.GetEventCategory(schoolId);
        }
        public Task<IEnumerable<EventTypeView>> GetEventType()
        {
            return _eventsRepository.GetEventType();
        }
        public Task<IEnumerable<EventDurationView>> GetEventDuration()
        {
            return _eventsRepository.GetEventDuration();
        }
        public Task<IEnumerable<EventView>> GetMothlyEvents(long userId, DateTime fromDate, DateTime toDate, int? categoryId)
        {
            return _eventsRepository.GetMothlyEvents(userId, fromDate, toDate, categoryId);
        }

        public Task<IEnumerable<EventView>> GetAllMothlyEvents(long userId, DateTime fromDate, DateTime toDate, string eventCategoryIds)
        {
            return _eventsRepository.GetAllMothlyEvents(userId, fromDate, toDate, eventCategoryIds);
        }

        public Task<IEnumerable<WeeklyEventView>> GetWeeklyTimeTableEvents(long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher, bool isDashboardEvents)
        {
            return _eventsRepository.GetWeeklyTimeTableEvents(userId, schoolId, fromDate, toDate,isTeacher, isDashboardEvents);
        }

        public Task<IEnumerable<WeeklyEventView>> GetTodayOrWeeklyTimeTableEvents(long userId, long schoolId, int fromWeekDay, int toWeekDay, DateTime fromDate, DateTime toDate, bool isTeacher, bool isWeeklyEvent)
        {
            return _eventsRepository.GetTodayOrWeeklyTimeTableEvents(userId, schoolId,fromWeekDay,toWeekDay, fromDate, toDate, isTeacher, isWeeklyEvent);
        }

        public Task<PlannerTimetableView> GetPlannerTimetableData(long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            return _eventsRepository.GetPlannerTimetableData(userId, schoolId, fromDate, toDate, isTeacher);
        }

        public Task<PlannerTimetableView> GetPlannerTimetableDataWithPaging(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            return _eventsRepository.GetPlannerTimetableDataWithPaging(pageNumber,pageSize, userId, schoolId, fromDate, toDate, isTeacher);
        }

        public Task<IEnumerable<WeeklyEventView>> GetTimetableDataWithPaging(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            return _eventsRepository.GetTimetableDataWithPaging(pageNumber, pageSize, userId, schoolId, fromDate, toDate, isTeacher);
        }

        public Task<IEnumerable<EventView>> GetOnlineMeetingEvents(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isOnlineMeetingEvents,short languageId)
        {
            return _eventsRepository.GetOnlineMeetingEvents(pageNumber,pageSize,userId,schoolId, fromDate, toDate, isOnlineMeetingEvents, languageId);
        }

        public Task<IEnumerable<WeeklyEventView>> GetSchoolTimetableEvents(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            return _eventsRepository.GetSchoolTimetableEvents(pageNumber, pageSize, userId, schoolId, fromDate, toDate, isTeacher);
        }

        public Task<IEnumerable<WeeklyEventView>> GetTimetableEventsReport(TimetableReportsRequest model)
        {
            return _eventsRepository.GetTimetableEventsReport(model);
        }

        public Task<IEnumerable<EventView>> GetLiveSessionsReport(TimetableReportsRequest model)
        {
            return _eventsRepository.GetLiveSessionsReport(model);
        }

        public Task<EventUser> GetEventUserDetails(int eventId, long userId)
        {
            return _eventsRepository.GetEventUserDetails(eventId,userId);
        }

        public Task<IEnumerable<EventUser>> GetInternalEventUser(int eventId)
        {
            return _eventsRepository.GetInternalEventUser(eventId);
        }
        public Task<IEnumerable<EventUser>> GetExternalEventUser(int eventId)
        {
            return _eventsRepository.GetExternalEventUser(eventId);
        }
        public int CheckTimetableEventExists(string title, string meetingPassword, DateTime startDate, string startTime, long userId)
        {
            return _eventsRepository.CheckTimetableEventExists(title,meetingPassword,startDate,startTime, userId);
        }
    }
}
