﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class EventCategoryService : IEventCategoryService
    {
        private readonly IEventCategoryRepository _eventCategoryRepository;
        public EventCategoryService(IEventCategoryRepository eventCategoryRepository)
        {
            _eventCategoryRepository = eventCategoryRepository;
        }

        public Task<IEnumerable<EventCategoryView>> GetEventCategory(int id)
        {
            return _eventCategoryRepository.GetEventCategory(id);
        }

        public async Task<EventCategory> GetEventCategoryById(int Id,short languageId)
        {
            return await _eventCategoryRepository.GetEventCategoryById(Id, languageId);
        }

        public Task<IEnumerable<EventCategoryView>> GetAllEventCategoryBySchool(int schoolId,short languageId)
        {
            return _eventCategoryRepository.GetAllEventCategoryBySchool(schoolId, languageId);
        }

        public Task<IEnumerable<EventCategoryView>> GetEventCategoriesBySchoolId(int schoolId,short languageId)
        {
            return _eventCategoryRepository.GetEventCategoriesBySchoolId(schoolId, languageId);
        }

        public int InsertEventCategory(EventCategory entity)
        {
            return _eventCategoryRepository.UpdateEventCategory(entity, TransactionModes.Insert);
        }

        public int UpdateEventCategory(EventCategory entity)
        {
            return _eventCategoryRepository.UpdateEventCategory(entity, TransactionModes.Update);
        }

        public int DeleteEventCategory(int Id)
        {
            EventCategory entity = new EventCategory(Id);
            return _eventCategoryRepository.UpdateEventCategory(entity, TransactionModes.Delete);
        }

        public bool CheckEventCategoryAvailable(string categoryName, int id, long schoolId)
        {
            return _eventCategoryRepository.CheckEventCategoryAvailable(categoryName, id, schoolId);
        }

    }
}
