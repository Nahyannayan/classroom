﻿using Phoenix.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IDurationService
    {
        Task<IEnumerable<DurationView>> GetDuration(int id);
        bool UpdateDuration(DurationView durationView);
    }
}
