﻿using DbConnection;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IEventCategoryService 
    {
        Task<IEnumerable<EventCategoryView>> GetEventCategory(int id);
        Task<IEnumerable<EventCategoryView>> GetAllEventCategoryBySchool(int schoolId, short languageId);
        Task<IEnumerable<EventCategoryView>> GetEventCategoriesBySchoolId(int schoolId, short languageId);
        Task<EventCategory> GetEventCategoryById(int Id,short languageId);
        int InsertEventCategory(EventCategory entity);
        int UpdateEventCategory(EventCategory entity);
        int DeleteEventCategory(int Id);
        bool CheckEventCategoryAvailable(string categoryName, int id, long schoolId);
    }
}
