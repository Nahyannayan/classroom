﻿using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IEventsService
    {
        int EventInsert(EventView eventView);
        int EventUpdate(EventView eventView);
        int CancelEvent(int eventId, long userId);
        bool DeleteEventFile(int eventId);
        bool AcceptEventRequest(int eventId, long userId);
        bool AcceptEventRequestExternalUser(int eventId, string emailId);
        Task<IEnumerable<EventView>> GetEvent(int id, long? userId,short languageId);
        Task<IEnumerable<EventCategoryView>> GetEventCategory(long schoolId);
        Task<IEnumerable<EventTypeView>> GetEventType();
        Task<IEnumerable<EventDurationView>> GetEventDuration();
        Task<IEnumerable<EventView>> GetMothlyEvents(long userId, DateTime fromDate, DateTime toDate, int? categoryId);
        Task<IEnumerable<EventUser>> GetInternalEventUser(int eventId);
        Task<IEnumerable<EventUser>> GetExternalEventUser(int eventId);
        Task<IEnumerable<EventView>> GetAllMothlyEvents(long userId, DateTime fromDate, DateTime toDate, string eventCategoryIds);
        Task<IEnumerable<WeeklyEventView>> GetWeeklyTimeTableEvents(long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher, bool isDashboardEvents);
        Task<IEnumerable<WeeklyEventView>> GetTodayOrWeeklyTimeTableEvents(long userId, long schoolId, int fromWeekDay, int toWeekDay, DateTime fromDate, DateTime toDate, bool isTeacher, bool isWeeklyEvent);
        Task<EventUser> GetEventUserDetails(int eventId, long userId);
        Task<PlannerTimetableView> GetPlannerTimetableData(long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher);
        Task<IEnumerable<EventView>> GetOnlineMeetingEvents(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isOnlineMeetingEvents, short languageId);
        Task<IEnumerable<WeeklyEventView>> GetSchoolTimetableEvents(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher);
        Task<PlannerTimetableView> GetPlannerTimetableDataWithPaging(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher);
        Task<IEnumerable<WeeklyEventView>> GetTimetableDataWithPaging(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher);
        int CheckTimetableEventExists(string title, string meetingPassword, DateTime startDate, string startTime, long userId);
        Task<IEnumerable<WeeklyEventView>> GetTimetableEventsReport(TimetableReportsRequest model);
        Task<IEnumerable<EventView>> GetLiveSessionsReport(TimetableReportsRequest model);
    }
}
