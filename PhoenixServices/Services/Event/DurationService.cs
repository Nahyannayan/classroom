﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Common.Models;

namespace Phoenix.API.Services
{
    public class DurationService : IDurationService
    {
        private readonly IDurationRepository _durationRepository;
        public DurationService(IDurationRepository durationRepository)
        {
            _durationRepository = durationRepository;
        }

        public Task<IEnumerable<DurationView>> GetDuration(int id)
        {
            return _durationRepository.GetDuration(id);
        }

        public bool UpdateDuration(DurationView durationView)
        {
            return _durationRepository.UpdateDuration(durationView);
        }
    }
}
