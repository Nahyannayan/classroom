﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.API.Repositories;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class PlanSchemeService : IPlanSchemeService
    {
        private readonly IPlanSchemeDetailRepository _planSchemeDetailRepository;
        public PlanSchemeService(IPlanSchemeDetailRepository planSchemeDetailRepository)
        {
            _planSchemeDetailRepository = planSchemeDetailRepository;
        }

        public bool Delete(int id, int deletedBy)
        {
            _planSchemeDetailRepository.Delete(id, deletedBy);
            return true;
        }

        public Task<IEnumerable<PlanSchemeDetail>> GetAll()
        {
            return _planSchemeDetailRepository.GetAllAsync();
        }

        public Task<PlanSchemeDetail> GetById(int id)
        {
            return _planSchemeDetailRepository.GetAsync(id);
        }

        public Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemeBySchoolId(int schoolId, int? userId, int? status, bool? isActive,
            bool? SharedWithMe, bool? CreatedByMe,
            string MISGroupId, string OtherGroupId, string CourseId, string GradeId,
            int? pageIndex = null, int? PageSize = null, string searchString = "", string sortBy = "")
        {
            return _planSchemeDetailRepository.GetPlanSchemeBySchoolId(schoolId, userId, status, isActive, SharedWithMe, CreatedByMe, MISGroupId, OtherGroupId, CourseId,
                GradeId, pageIndex, PageSize, searchString, sortBy);
        }

        public Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemeDetail(int? planSchemeDetailId, int? templateId, int? schoolId, string templateType, int? status, int? userId, bool? isActive)
        {
            return _planSchemeDetailRepository.GetPlanSchemeDetail(planSchemeDetailId, templateId, schoolId, templateType, status, userId, isActive);
        }

        public int Insert(PlanSchemeDetail entity)
        {
            return _planSchemeDetailRepository.Insert(entity);
        }

        public int Update(PlanSchemeDetail entityToUpdate)
        {
            return _planSchemeDetailRepository.Update(entityToUpdate);
        }

        public int UpdatePlanSchemeStatus(PlanSchemeDetail entityToUpdate)
        {
            return _planSchemeDetailRepository.UpdatePlanSchemeStatus(entityToUpdate);
        }
        public Task<int> UpdateSharedLessonPlanDetial(long PlanSchemeId, string SelectedTeacherList, long UserId, string Operation)
        {
            return _planSchemeDetailRepository.UpdateSharedLessonPlanDetial(PlanSchemeId, SelectedTeacherList,UserId, Operation);
        }

        public Task<IEnumerable<SharedLessonPlanTeacherList>> GetSharedLessonPlanTeacherList(string PlanSchemeId)
        {
            return _planSchemeDetailRepository.GetSharedLessonPlanTeacherList(PlanSchemeId);
        }

        public Task<IEnumerable<TopicSubTopicStructure>> GetUnitStructure(string groupIds)
        {
            return _planSchemeDetailRepository.GetUnitStructure(groupIds);
        }
        public Task<IEnumerable<LessonPlanFilterModule>> GetLessonPlanFilterModule(long SchoolId, long TeacherId)
        {
            return _planSchemeDetailRepository.GetLessonPlanFilterModule(SchoolId, TeacherId);
        }
        public Task<IEnumerable<PlanSchemeDetail>> GetPendingForApprovalLessonPlan(long SchoolId)
        {
            return _planSchemeDetailRepository.GetPendingForApprovalLessonPlan(SchoolId);
        }
        public int SavePlanSchemeFields(List<PlanSchemeField> planSchemeField) {
            return _planSchemeDetailRepository.SavePlanSchemeFields(planSchemeField);
        }

        public Task<IEnumerable<PlanSchemeField>> GetPlanSchemeFields(long planSchemeId) {
            return _planSchemeDetailRepository.GetPlanSchemeFields(planSchemeId);
        }

        public async Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemesByUnitId(long unitId, int? status)
        {
            return await _planSchemeDetailRepository.GetPlanSchemesByUnitId(unitId, status);
        }
        public async Task<IEnumerable<SchoolGroup>> GetUserGroups(long Userid) { 
            return await _planSchemeDetailRepository.GetUserGroups(Userid);
        }

    }
}
