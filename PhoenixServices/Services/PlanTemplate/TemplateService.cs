﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class TemplateService : ITemplateService
    {
        private readonly ITemplateRepository _templateRepository;
        public TemplateService(ITemplateRepository templateRepository)
        {
            _templateRepository = templateRepository;
        }

        public async Task<bool> SaveTemplateCertificateStatus(Template model)
        {
            return await _templateRepository.SaveTemplateCertificateStatus(model);
        }

        public bool Delete(int id, int deletedBy)
        {
            _templateRepository.Delete(id, deletedBy);
            return true;
        }

        public Task<IEnumerable<Template>> GetAll()
        {
            return _templateRepository.GetAllAsync();
        }

        public async Task<Template> GetById(int id)
        {
            return await _templateRepository.GetTemplateById(id);
        }

        public async Task<IEnumerable<Student>> GetCertificateMappingStudents(long userId, int pageIndex, int pageSize, int templateId, string schoolGroupIds, string searchString)
        {
            return await _templateRepository.GetCertificateMappingStudents(userId, pageIndex, pageSize, templateId, schoolGroupIds, searchString);
        }

        public async Task<IEnumerable<Template>> GetStudentAssignedCertificates(long userId, PlanTemplateTypes templateType)
        {
            return await _templateRepository.GetStudentAssignedCertificates(userId, templateType);
        }

        public async Task<IEnumerable<CertificateTeacherMapping>> GetSchoolTemplateApproverDelegates(long schoolId, PlanTemplateTypes templateType, long? departmentId)=> 
            await _templateRepository.GetSchoolTemplateApproverDelegates(schoolId, templateType, departmentId);

        public Task<Template> GetTemplateDetail(int? templateId, int? schoolId, string templateType, string period, int? userId, bool? isActive, bool? includeTemplateField)
        {
            return _templateRepository.GetTemplateDetail(templateId, schoolId, templateType, period, userId, isActive, includeTemplateField);
        }

        public Task<TemplateField> GetTemplateFieldById(int templateFieldId)
        {
            return _templateRepository.GetTemplateFieldById(templateFieldId);
        }

        public Task<IEnumerable<TemplateField>> GetTemplateFieldByTemplateId(int templateId, bool isActive,long UserId, int groupid)
        {
            return _templateRepository.GetTemplateFieldByTemplateId(templateId, isActive, UserId, groupid);
        }

        public async Task<IEnumerable<TemplateFieldMapping>> GetTemplateFieldData(int templateId, long userId) => await _templateRepository.GetTemplateFieldData(templateId, userId);
        
        public Task<IEnumerable<Template>> GetTemplatesBySchoolId(int schoolId, int? userId, bool isActive, string TemplateType,int? status)
        {
            return _templateRepository.GetTemplatesBySchoolId(schoolId, userId, isActive, TemplateType, status);
        }

        public int Insert(Template entity)
        {
            return _templateRepository.Insert(entity);

        }

        public async Task<bool> SaveCertificateApprovalTeacher(CertificateTeacherMapping model)
        {
            return await _templateRepository.SaveCertificateApprovalTeacher(model);
        }

        public async Task<bool> SaveCertificateAssignedColumns(IEnumerable<TemplateField> fieldList)
        {
            return await _templateRepository.SaveCertificateAssignedColumns(fieldList);
        }

        public async Task<bool> SaveTemplateImageData(Template templateModel)
        {
            return await _templateRepository.SaveTemplateImageData(templateModel);
        }

        public async Task<bool> SaveTemplateStudentMapping(StudentCertificateMapping certificateMapping)
        {
            return await _templateRepository.SaveTemplateStudentMapping(certificateMapping);
        }

        public int Update(Template entityToUpdate)
        {
            return _templateRepository.Update(entityToUpdate);

        }

        public async Task<IEnumerable<TemplateFieldMapping>> GetTemplatePreviewData(int templateId, long schoolId)
        {
            return await _templateRepository.GetTemplatePreviewData(templateId, schoolId);
        }

        public async Task<IEnumerable<CertificateColumns>> GetCertificateColumns(int certificateColumnType)
        {
            return await _templateRepository.GetCertificateColumns(certificateColumnType);
        }
        public async Task<IEnumerable<TemplateCollectionList>> GetTimeTableList(long UserId, string SelectDate) { return await _templateRepository.GetTimeTableList( UserId,  SelectDate); }
        public async Task<IEnumerable<TemplateCollectionList>> GetGradeList(long Groupid,long SchoolId) { return await _templateRepository.GetGradeList( Groupid, SchoolId); }
        public async Task<IEnumerable<TemplateFieldMapping>> GetTemplateFieldData(int templateId, List<long> userIds, string dynamicParameter)
            => await _templateRepository.GetTemplateFieldData(templateId, userIds,dynamicParameter);

        public async Task<IEnumerable<CertificateReportView>> GetCertificateReportData(long schoolId) => await _templateRepository.GetCertificateReportData(schoolId);
    }
}
