﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Serilog;

namespace Phoenix.API.Services
{
    public interface IPlanSchemeService
    {
        Task<IEnumerable<PlanSchemeDetail>> GetAll();
        Task<PlanSchemeDetail> GetById(int id);
        int Insert( PlanSchemeDetail entity);
        int SavePlanSchemeFields(List<PlanSchemeField> planSchemeField);
        Task<IEnumerable<PlanSchemeField>> GetPlanSchemeFields(long planSchemeId);
        int Update(PlanSchemeDetail entityToUpdate);
        bool Delete(int id, int deletedBy);
        int UpdatePlanSchemeStatus(PlanSchemeDetail entityToUpdate);

        Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemeBySchoolId(int schoolId, int? userId, int? status, bool? isActive,
            bool? SharedWithMe, bool? CreatedByMe,
            string MISGroupId, string OtherGroupId, string CourseId, string GradeId,
            int? pageIndex = null, int? PageSize = null, string searchString = "", string sortBy = "");
        Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemeDetail(int? planSchemeDetailId, int? templateId, int? schoolId, string templateType, int? status, int? userId, bool? isActive);

        Task<int> UpdateSharedLessonPlanDetial(long PlanSchemeId, string SelectedTeacherList,long UserId, string Operation);
        Task<IEnumerable<SharedLessonPlanTeacherList>> GetSharedLessonPlanTeacherList(string PlanSchemeId);

         Task<IEnumerable<TopicSubTopicStructure>> GetUnitStructure(string groupIds);

        Task<IEnumerable<LessonPlanFilterModule>> GetLessonPlanFilterModule(long SchoolId, long TeacherId);
        Task<IEnumerable<PlanSchemeDetail>> GetPendingForApprovalLessonPlan(long SchoolId);

        Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemesByUnitId(long unitId, int? status);
        Task<IEnumerable<SchoolGroup>> GetUserGroups(long Userid);
    }
}
