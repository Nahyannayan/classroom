﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public interface ITemplateService

    {
        Task<IEnumerable<Template>> GetAll();
        Task<Template> GetById(int id);
        int Insert(Template entity);
        bool Delete(int id, int deletedBy);
        int Update(Template entityToUpdate);

        Task<IEnumerable<Template>> GetTemplatesBySchoolId(int schoolId, int? userId, bool isActive, string TemplateType, int? status);
        Task<Template> GetTemplateDetail(int? templateId, int? schoolId, string templateType, string period, int? userId, bool? isActive, bool? includeTemplateField);
        Task<TemplateField> GetTemplateFieldById(int templateFieldId);
        Task<IEnumerable<TemplateField>> GetTemplateFieldByTemplateId(int templateId, bool isActive, long UserId, int groupid);
        Task<bool> SaveTemplateImageData(Template templateModel);
        Task<IEnumerable<Student>> GetCertificateMappingStudents(long userId, int pageIndex, int pageSize, int templateId, string schoolGroupIds, string searchString);
        Task<bool> SaveTemplateStudentMapping(StudentCertificateMapping certificateMapping);
        Task<bool> SaveCertificateAssignedColumns(IEnumerable<TemplateField> fieldList);
        Task<IEnumerable<Template>> GetStudentAssignedCertificates(long userId, PlanTemplateTypes templateType);
        Task<IEnumerable<TemplateFieldMapping>> GetTemplateFieldData(int templateId, long userId);
        Task<bool> SaveTemplateCertificateStatus(Template model);
        Task<IEnumerable<CertificateTeacherMapping>> GetSchoolTemplateApproverDelegates(long schoolId, PlanTemplateTypes templateType, long? departmentId);
        Task<bool> SaveCertificateApprovalTeacher(CertificateTeacherMapping model);
        Task<IEnumerable<TemplateFieldMapping>> GetTemplatePreviewData(int templateId, long schoolId);
        Task<IEnumerable<CertificateColumns>> GetCertificateColumns(int certificateColumnType);
        Task<IEnumerable<TemplateCollectionList>> GetTimeTableList(long UserId, string SelectDate);
        Task<IEnumerable<TemplateCollectionList>> GetGradeList(long Groupid,long SchoolId);
        Task<IEnumerable<TemplateFieldMapping>> GetTemplateFieldData(int templateId, List<long> userIds, string dynamicParameter);
        Task<IEnumerable<CertificateReportView>> GetCertificateReportData(long schoolId);
    }
}
