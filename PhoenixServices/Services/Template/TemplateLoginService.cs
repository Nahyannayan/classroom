﻿using Phoenix.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;

namespace Phoenix.API.Services
{
    public class TemplateLoginService : ITemplateLoginService
    {
        private readonly ITemplateUserRepository _logInUserRepository;
        private IConfiguration _config;
        public TemplateLoginService(ITemplateUserRepository logInUserRepository, IConfiguration config)
        {
            _logInUserRepository = logInUserRepository;
            _config = config;
        }

        public async Task<LogInUser> GetLoginUserByUserName(string userName)
        {
            var loginUser = await _logInUserRepository.GetLogInUserByUserName(userName);
            loginUser.Token = GenerateJSONWebToken(loginUser);
            return loginUser;
        }

        public async Task<IEnumerable<LogInUser>> GetUserList(int schoolId)
        {
            return await _logInUserRepository.GetUserList(schoolId);
        }

        /// <summary>
        /// Deepak Singh, 19 August 2019, To get token for api authentication
        /// </summary>
        /// <param name="logInUser"></param>
        /// <returns></returns>
        private string GenerateJSONWebToken(LogInUser logInUser)
        {
            var key = _config["Jwt:Key"];
            var audiences = Convert.ToString(_config["Jwt:Audiences"]);

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);


            var claims = new List<Claim>
                            {
                                new Claim(JwtRegisteredClaimNames.Sub, logInUser.Id.ToString()),
                                new Claim(JwtRegisteredClaimNames.FamilyName, logInUser.LastName),
                                new Claim(JwtRegisteredClaimNames.GivenName, logInUser.FirstName),
                                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                                new Claim(JwtRegisteredClaimNames.Iss, _config["Jwt:Issuer"])
                             };
           
            foreach (var aud in audiences.Split(","))
            {
                claims.Add(new Claim(JwtRegisteredClaimNames.Aud, aud));
            }

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddHours(Convert.ToInt32(_config["Jwt:Hours"])),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public bool IsValidUser(string userName,string Password)
        {
            return  _logInUserRepository.IsValidUser(userName,Password);
        }

        public bool ResetPassword(string userName, string Password)
        {
            return  _logInUserRepository.ResetPassword(userName, Password);
        }

        public Task<IEnumerable<StudentUploadTemplate>> StudentTemplateBulkImport(List<StudentUploadTemplate> StudentList,long UserId)
        {
           return _logInUserRepository.StudentTemplateBulkImport(StudentList, UserId);
        }

        public Task<IEnumerable<StaffUploadTemplate>> StaffTemplateBulkImport(List<StaffUploadTemplate> StaffList, long UserId)
        {
          return  _logInUserRepository.StaffTemplateBulkImport(StaffList,UserId);
        }
        public Task<IEnumerable<GRADE_WISE_SUBJECT_M>> GradeWiseSubjectTemplateBulkImport(List<GRADE_WISE_SUBJECT_M> GradeWiseSubjectList, long UserId)
        {
          return  _logInUserRepository.GradeWiseSubjectTemplateBulkImport(GradeWiseSubjectList,UserId);
        }

        public Task<IEnumerable<STUDENT_WISE_SUBJECT_MAPPING>> StudentWiseSubjectTemplateBulkImport(List<STUDENT_WISE_SUBJECT_MAPPING> StudentWiseSubjectList,long UserId)
        {
           return _logInUserRepository.StudentWiseSubjectTemplateBulkImport(StudentWiseSubjectList,UserId);
        }
    }
}
