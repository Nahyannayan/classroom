﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Phoenix.API.Services
{
    public interface ITemplateLoginService
    {
        Task<LogInUser> GetLoginUserByUserName(string userName);
        Task<IEnumerable<LogInUser>> GetUserList(int schoolId);
        bool IsValidUser(string userName, string Password);
        Task<IEnumerable<StudentUploadTemplate>> StudentTemplateBulkImport(List<StudentUploadTemplate> StudentList,long UserId);
        Task<IEnumerable<StaffUploadTemplate>> StaffTemplateBulkImport(List<StaffUploadTemplate> StaffList, long UserId);
        Task<IEnumerable<GRADE_WISE_SUBJECT_M>> GradeWiseSubjectTemplateBulkImport(List<GRADE_WISE_SUBJECT_M> GradeWiseSubjectList, long UserId);
        Task<IEnumerable<STUDENT_WISE_SUBJECT_MAPPING>> StudentWiseSubjectTemplateBulkImport(List<STUDENT_WISE_SUBJECT_MAPPING> StudentWiseSubjectList, long UserId);

        bool ResetPassword(string userName, string Password);
    }
}

