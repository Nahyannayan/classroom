﻿using Phoenix.API.Repositories.PTM.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class PTMMeetingService : IPTMMeetingService
    {
        private readonly IPTMMeetingRepository _meetingRepository;
        public PTMMeetingService(IPTMMeetingRepository meetingRepository)
        {
            _meetingRepository = meetingRepository;
        }
        public Task<PTMMeeting> GetPTMMeetingById(int meetingId, short langaugeId)
        {
            throw new NotImplementedException();
        }

        public async Task<int> InsertPTMMeeting(PTMMeeting model)
        {
            return await _meetingRepository.InsertPTMMeetingAsync(model, TransactionModes.Insert);
        }

        public async Task<int> UpdatePTMMeeting(PTMMeeting model)
        {
            return await _meetingRepository.UpdatePTMMeetingAsync(model, TransactionModes.Update);
        }

        public async Task<int> DeletePTMMeeting(int MeetingId)
        {
            return 0;
            //return await _meetingRepository.DeleteAsync(MeetingId);
        }
    }
}
