﻿using Phoenix.API.Repositories.PTM.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Models;


namespace Phoenix.API.Services
{
    public class PTMDeclineReasonService : IPTMDeclineReasonService
    {
        private readonly IPTMDeclineReasonRepository _ptmDeclineReasonRepository;
        public PTMDeclineReasonService(IPTMDeclineReasonRepository ptmDeclineReasonRepository)
        {
            _ptmDeclineReasonRepository = ptmDeclineReasonRepository;
        }

        public async Task<IEnumerable<DeclineReasonList>> GetPTMDeclineReason(int languageId, int SchoolGradeid)
        {
            return await _ptmDeclineReasonRepository.GetPTMDeclineReason(languageId, SchoolGradeid);
        }

        public async Task<DeclineReasonList> GetPTMDeclineReasonById(int id, short langaugeId)
        {
            return await _ptmDeclineReasonRepository.GetPTMDeclineReasonById(id, langaugeId);
        }

        public int InsertPTMDeclineReason(DeclineReasonList entity)
        {
            return  _ptmDeclineReasonRepository.UpdatePTMDeclineReason(entity, TransactionModes.Insert);
        }

        public int UpdatePTMDeclineReason(DeclineReasonList entity)
        {
            return  _ptmDeclineReasonRepository.UpdatePTMDeclineReason(entity, TransactionModes.Update);
        }

        public int DeletePTMDeclineReason(int id)
        {
            //SchoolSkillSet entity = new SchoolSkillSet(id);
           // return await _ptmDeclineReasonRepository.UpdatePTMDeclineReason(entity, TransactionModes.Delete);

            DeclineReasonList entity = new DeclineReasonList(id);
            //CategoryList entity = null;
            return _ptmDeclineReasonRepository.UpdatePTMDeclineReason(entity, TransactionModes.Delete);
        }
    }
}
