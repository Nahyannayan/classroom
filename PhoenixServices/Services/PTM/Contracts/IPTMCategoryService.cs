﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IPTMCategoryService
    {
        Task<IEnumerable<CategoryList>> GetPTMCategory(int languageId, int SchoolGradeid);
        Task<CategoryList> GetPTMCategoryById(int id, short langaugeId);
        int InsertPTMCategory(CategoryList entity);
        int UpdatePTMCategory(CategoryList entity);
        int DeletePTMCategory(int id);
    }
}
