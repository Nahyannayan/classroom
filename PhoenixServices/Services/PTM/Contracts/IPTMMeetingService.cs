﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IPTMMeetingService
    {
        Task<PTMMeeting> GetPTMMeetingById(int meetingId, short langaugeId);
        Task<int> InsertPTMMeeting(PTMMeeting model);
        Task<int> UpdatePTMMeeting(PTMMeeting model);
        Task<int> DeletePTMMeeting(int MeetingId);
    }
}
