﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IPTMDeclineReasonService
    {
        Task<IEnumerable<DeclineReasonList>> GetPTMDeclineReason(int languageId, int SchoolGradeid);
        Task<DeclineReasonList> GetPTMDeclineReasonById(int id, short langaugeId);
        int InsertPTMDeclineReason(DeclineReasonList entity);
        int UpdatePTMDeclineReason(DeclineReasonList entity);
        int DeletePTMDeclineReason(int id);
    }
}
