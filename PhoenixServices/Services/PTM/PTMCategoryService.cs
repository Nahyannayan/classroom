﻿using Phoenix.API.Repositories.PTM.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class PTMCategoryService : IPTMCategoryService
    {
        private readonly IPTMCategoryRepository _ptmCategoryRepository;
        public PTMCategoryService(IPTMCategoryRepository ptmCategoryRepository)
        {
            _ptmCategoryRepository = ptmCategoryRepository;
        }


        public async Task<IEnumerable<CategoryList>> GetPTMCategory(int languageId, int SchoolGradeid)
        {
            return await _ptmCategoryRepository.GetPTMCategory(languageId, SchoolGradeid);
        }

        public async Task<CategoryList> GetPTMCategoryById(int id, short langaugeId)
        {
            return await _ptmCategoryRepository.GetPTMCategoryById(id, langaugeId);
        }

        public int InsertPTMCategory(CategoryList entity)
        {
            return _ptmCategoryRepository.UpdatePTMCategory(entity, TransactionModes.Insert);
        }

        public int UpdatePTMCategory(CategoryList entity)
        {
            return _ptmCategoryRepository.UpdatePTMCategory(entity, TransactionModes.Update);
        }

        public int DeletePTMCategory(int id)
        {
             CategoryList entity = new CategoryList (id);
            //CategoryList entity = null;
            return _ptmCategoryRepository.UpdatePTMCategory(entity, TransactionModes.Delete);
        }
    }
}
