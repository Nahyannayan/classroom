﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class TeacherDashboardService : ITeacherDashboardService
    {
        private readonly ITeacherDashboardRepository _teacherDashboardRepository;

        public TeacherDashboardService(ITeacherDashboardRepository teacherDashboardRepository)
        {
            _teacherDashboardRepository = teacherDashboardRepository;

        }
        public async Task<TeacherDashboard> GetTeacherDashboard(int id)
        {
            return await _teacherDashboardRepository.GetTeacherDashboard(id);
        }

        public async Task<List<Student>> GetStudentForTeacher(long id, string ids = "")
        {
            return await _teacherDashboardRepository.GetStudentForTeacher(id,ids);
        }

        public async Task<List<Student>> GetStudentForTeacherBySchoolGroup(int id, int pageNumber, int pageSize, string schoolGroupIds, string searchString,string sortBy)
        {
            return await _teacherDashboardRepository.GetStudentForTeacherBySchoolGroup(id, pageNumber, pageSize, schoolGroupIds, searchString, sortBy);
        }
    }
}
