﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ITeacherDashboardService
    {
      
        Task<TeacherDashboard> GetTeacherDashboard(int id);

        Task<List<Student>> GetStudentForTeacher(long id, string ids = "");
        Task<List<Student>> GetStudentForTeacherBySchoolGroup(int id, int pageNumber, int pageSize, string schoolGroupIds, string searchString, string sortBy);
    }
}
