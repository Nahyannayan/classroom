﻿using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IAbuseDelegateService
    {
        #region Abuse Contact Info
        Task<AbuseContactInfo> GetAbuseContactInfoBySchoolId(long schoolId);
        Task<IEnumerable<AbuseContactInfo>> GetAllAbuseContactInfoAsync();
        Task<AbuseContactInfo> GetAbuseContactInfoById(long id);
        void InsertAbuseContactInfo(AbuseContactInfo entity);
        Int64 UpdateAbuseContactInfoData(AbuseContactInfo entity, TransactionModes mode);
        void UpdateAbuseContactInfo(AbuseContactInfo entityToUpdate);
        void DeleteAbuseContactInfo(long id);
        #endregion

        #region Abuse Delegate
        Task<IEnumerable<AbuseDelegate>> GetAbuseDelegateBySchoolId(long schoolId);
        Task<IEnumerable<AbuseDelegate>> GetAllAbuseDelegateAsync();
        Task<AbuseDelegate> GetAbuseDelegateById(long id);
        void InsertAbuseDelegateAsync(AbuseDelegate entity);
        Int64 UpdateAbuseDelegateData(AbuseDelegate entity, TransactionModes mode);
        void UpdateAbuseDelegateAsync(AbuseDelegate entityToUpdate);
        void DeleteAbuseDelegateAsync(long id);
        Task<bool> SaveMobileSettings(AbuseContactInfo model);
        #endregion
    }
}
