﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ISafetyCategoriesService
    {
        Task<IEnumerable<SafetyCategory>> GetSafetyCategories(int languageId, int schoolId);
        Task<SafetyCategory> GetSafetyCategoryById(int id,short languageId);
        int InsertSafetyCategory(SafetyCategory entity);
        int UpdateSafetyCategory(SafetyCategory entity);
        int DeleteSafetyCategory(int id);
    }
}
