﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ICensorFilterService
    {
        Task<IEnumerable<CensorUser>> GetCensorUsers(int schoolId);
        Task<IEnumerable<BannedWord>> GetBannedWords(int schoolId);
        bool InsertBannedWord(BannedWord entity);
        bool UpdateBannedWord(BannedWord entity);
        bool DeleteBannedWord(BannedWord entity);
    }
}
