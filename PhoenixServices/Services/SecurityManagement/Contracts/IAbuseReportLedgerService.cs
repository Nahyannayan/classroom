﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public interface IAbuseReportLedgerService
    {
        Task<AbuseReportLedger> GetAbuseReportLedgerBySafetyCategoryId(long safetyCategoryId);
        Task<AbuseReportLedger> GetAbuseReportLedgerByAbuseReportLedgerId(long abuseReportLedgerId);
        Task<IEnumerable<AbuseReportLedger>> GetAllAbuseReportLedgerAsync();        
        void InsertAbuseReportLedger(AbuseReportLedger entity);
        Int64 UpdateAbuseReportLedgerData(AbuseReportLedger entity, TransactionModes mode);
        void UpdateAbuseReportLedger(AbuseReportLedger entityToUpdate);
        void DeleteAbuseReportLedger(long id);
        Task<IEnumerable<AbuseReportLedger>> GetAbuseReportListAsPerUser(int UserId);
    }
}
