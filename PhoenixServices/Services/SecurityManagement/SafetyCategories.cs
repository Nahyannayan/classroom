﻿using Phoenix.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class SafetyCategoriesService : ISafetyCategoriesService
    {
        private readonly ISafetyCategoriesRepository _safetyCategoriesRepository;

        public SafetyCategoriesService(ISafetyCategoriesRepository safetyCategoriesRepository)
        {
            _safetyCategoriesRepository = safetyCategoriesRepository;
        }


        public async Task<IEnumerable<SafetyCategory>> GetSafetyCategories(int languageId, int schoolId)
        {
            return await _safetyCategoriesRepository.GetSafetyCategories(languageId, schoolId);
        }

        public async Task<SafetyCategory> GetSafetyCategoryById(int id,short languageId)
        {
            return await _safetyCategoriesRepository.GetSafetyCategoryById(id,languageId);
        }

        public int InsertSafetyCategory(SafetyCategory entity)
        {
            return _safetyCategoriesRepository.UpdateSafetyCategoryData(entity, TransactionModes.Insert);
        }

        public int UpdateSafetyCategory(SafetyCategory entity)
        {
            return _safetyCategoriesRepository.UpdateSafetyCategoryData(entity, TransactionModes.Update);
        }

        public int DeleteSafetyCategory(int id)
        {
            SafetyCategory entity = new SafetyCategory(id);
            return _safetyCategoriesRepository.UpdateSafetyCategoryData(entity, TransactionModes.Delete);
        }
    }
}
