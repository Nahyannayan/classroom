﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.API.Repositories;

namespace Phoenix.API.Services
{
    public class AbuseDelegateService : IAbuseDelegateService
    {
        private readonly IAbuseDelegateRepository _abuseDelegateRepository;
        private readonly IAbuseContactInfoRepository _abuseContactInfoRepository;

        public AbuseDelegateService(IAbuseContactInfoRepository abuseContactInfoRepository,
            IAbuseDelegateRepository abuseDelegateRepository )
        {
            _abuseContactInfoRepository = abuseContactInfoRepository;
            _abuseDelegateRepository = abuseDelegateRepository;
        }

        public void DeleteAbuseDelegateAsync(long id)
        {
            _abuseDelegateRepository.DeleteAsync((int)id);
        }

        public void DeleteAbuseContactInfo(long id)
        {
            _abuseContactInfoRepository.DeleteAsync((int)id);
        }

        public Task<AbuseContactInfo> GetAbuseContactInfoBySchoolId(long schoolId)
        {
            return _abuseContactInfoRepository.GetAbuseContactInfo(schoolId);
        }

        public Task<IEnumerable<AbuseDelegate>> GetAbuseDelegateBySchoolId(long schoolId)
        {
            return _abuseDelegateRepository.GetAbuseDelegate(schoolId);
        }

        public Task<AbuseDelegate> GetAbuseDelegateById(long id)
        {
            return _abuseDelegateRepository.GetAsync((int)id);
        }

        public Task<IEnumerable<AbuseDelegate>> GetAllAbuseDelegateAsync()
        {
            return _abuseDelegateRepository.GetAllAsync();
        }

        public Task<IEnumerable<AbuseContactInfo>> GetAllAbuseContactInfoAsync()
        {
            return _abuseContactInfoRepository.GetAllAsync();
        }

        public Task<AbuseContactInfo> GetAbuseContactInfoById(long id)
        {
            return _abuseContactInfoRepository.GetAsync((int)id);
        }

        public void InsertAbuseDelegateAsync(AbuseDelegate entity)
        {
             _abuseDelegateRepository.InsertAsync(entity);
        }

        public void InsertAbuseContactInfo(AbuseContactInfo entity)
        {
            _abuseContactInfoRepository.InsertAsync(entity);
        }

        public long UpdateAbuseContactInfoData(AbuseContactInfo entity, TransactionModes mode)
        {
           return _abuseContactInfoRepository.UpdateAbuseContactInfoData(entity, mode);
        }

        public void UpdateAbuseDelegateAsync(AbuseDelegate entityToUpdate)
        {
            _abuseDelegateRepository.UpdateAsync(entityToUpdate);
        }

        public long UpdateAbuseDelegateData(AbuseDelegate entity, TransactionModes mode)
        {
            return _abuseDelegateRepository.UpdateAbuseDelegateData(entity, mode);
        }

        public void UpdateAbuseContactInfo(AbuseContactInfo entityToUpdate)
        {
            _abuseContactInfoRepository.UpdateAsync(entityToUpdate);
        }

        public async Task<bool> SaveMobileSettings(AbuseContactInfo model) => await _abuseContactInfoRepository.SaveMobileSettings(model);
    }
}
