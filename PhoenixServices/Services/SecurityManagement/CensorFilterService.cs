﻿using Phoenix.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class CensorFilterService : ICensorFilterService
    {
        private readonly ICensorFilterRepository _censorFilterRepository;

        public CensorFilterService(ICensorFilterRepository censorFilterRepository)
        {
            _censorFilterRepository = censorFilterRepository;
        }


        public async Task<IEnumerable<BannedWord>> GetBannedWords(int schoolId)
        {
            return await _censorFilterRepository.GetBannedWords(schoolId);
        }

        public bool InsertBannedWord(BannedWord entity)
        {
            return _censorFilterRepository.UpdateBannedWordData(entity, TransactionModes.Insert);
        }

        public bool UpdateBannedWord(BannedWord entity)
        {
            return _censorFilterRepository.UpdateBannedWordData(entity, TransactionModes.Update);
        }

        public bool DeleteBannedWord(BannedWord entity)
        {
            return _censorFilterRepository.UpdateBannedWordData(entity, TransactionModes.Delete);
        }

        public async Task<IEnumerable<CensorUser>> GetCensorUsers(int schoolId)
        {
            return await _censorFilterRepository.GetCensorUsers(schoolId);
        }
    }
}
