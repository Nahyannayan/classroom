﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class AbuseReportLedgerService : IAbuseReportLedgerService
    {
        private readonly IAbuseReportLedgerRepository abuseReportLedgerRepository;

        public AbuseReportLedgerService(IAbuseReportLedgerRepository abuseReportLedgerRepository)
        {
            this.abuseReportLedgerRepository = abuseReportLedgerRepository;
        }

        public void DeleteAbuseReportLedger(long id)
        {
            abuseReportLedgerRepository.DeleteAsync(Convert.ToInt32(id));
        }

        public Task<AbuseReportLedger> GetAbuseReportLedgerByAbuseReportLedgerId(long abuseReportLedgerId)
        {
            return abuseReportLedgerRepository.GetByAbuseReportLedgerIdAsync(Convert.ToInt32(abuseReportLedgerId));
        }

        public Task<AbuseReportLedger> GetAbuseReportLedgerBySafetyCategoryId(long safetyCategoryId)
        {
            return abuseReportLedgerRepository.GetAsync(Convert.ToInt32(safetyCategoryId));
        }

        public Task<IEnumerable<AbuseReportLedger>> GetAbuseReportListAsPerUser(int UserId)
        {
            return abuseReportLedgerRepository.GetAbuseReportListAsPerUser(UserId);
        }

        public Task<IEnumerable<AbuseReportLedger>> GetAllAbuseReportLedgerAsync()
        {
            return abuseReportLedgerRepository.GetAllAsync();
        }

        public void InsertAbuseReportLedger(AbuseReportLedger entity)
        {
            abuseReportLedgerRepository.InsertAsync(entity);
        }

        public void UpdateAbuseReportLedger(AbuseReportLedger entityToUpdate)
        {
            abuseReportLedgerRepository.UpdateAsync(entityToUpdate);
        }

        public long UpdateAbuseReportLedgerData(AbuseReportLedger entity, TransactionModes mode)
        {
            return abuseReportLedgerRepository.InsertUpdateAbuseReportLedgerData(entity, mode);
        }
    }
}
