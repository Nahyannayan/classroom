﻿// <summary>
// Created By: Raj M. Gogri
// Created On: 03/Oct/2020
// </summary>
using System.Collections.Generic;
using System.Threading.Tasks;
using static Phoenix.API.Models.ChartsDashboard.ChartModel;

namespace Phoenix.API.Services.ChartsDashboard
{
    /// <summary>
    /// Contract for StudentProgressTracker dashboard service that service class must implement.
    /// </summary>
    public interface IChartsDashboardService
    {
        /// <summary>
        /// Get filters for StudentProgressTracker dashboard.
        /// </summary>
        /// <param name="req">FilterReq request parameters to get filter for StudentProgressTracker dashboard.</param>
        /// <returns>FilterRes containing filters for StudentProgressTracker dashboard.</returns>
        Task<FilterRes> GetFiltersAync(FilterReq req);

        /// <summary>
        /// Get charts for StudentProgressTracker dashboard.
        /// </summary>
        /// <param name="req">ChartReq request parameters to get charts data for StudentProgressTracker dashboard.</param>
        /// <returns>ChartRes containing charts data for StudentProgressTracker dashboard.</returns>
        Task<ChartRes> GetStudentProgressTrackerChartAsync(ChartReq req);

        /// <summary>
        /// Get student filters for StudentAssessmentPrediction dashboard.
        /// </summary>
        /// <param name="req">ChartReq request parameters to get student filter for StudentAssessmentPrediction dashboard.</param>
        /// <returns>List of FilStudent.</returns>
        Task<IEnumerable<FilStudent>> GetStudentFiltersAync(ChartReq req);

        /// <summary>
        /// Get charts for StudentAssessmentPrediction dashboard.
        /// </summary>
        /// <param name="req">ChartReq request parameters to get charts data for StudentAssessmentPrediction dashboard.</param>
        /// <returns>ChartRes containing charts data for StudentProgressTracker dashboard.</returns>
        Task<ChartRes> GetStudentAssessmentPredictionChartAsync(ChartReq req);

        /// <summary>
        /// Get all assessment data for StudentAssessmentPrediction dashboard.
        /// </summary>
        /// <param name="req">ChartReq request parameters to get all assessment data for StudentAssessmentPrediction dashboard.</param>
        /// <returns>List of AssessmentDataByStudent.</returns>
        Task<IEnumerable<AssessmentDataByStudent>> GetAllAssessmentDataAync(ChartReq req);
    }
}