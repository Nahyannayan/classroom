﻿// <summary>
// Created By: Raj M. Gogri
// Created On: 03/Oct/2020
// </summary>
using Phoenix.API.Repositories.ChartsDashboard;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Phoenix.API.Models.ChartsDashboard.ChartModel;

namespace Phoenix.API.Services.ChartsDashboard
{
    public class ChartsDashboardService : IChartsDashboardService
    {
        /// <value>
        /// Readonly IChartsDashboardRepository repository field.
        /// </value>
        private readonly IChartsDashboardRepository repo;

        /// <summary>
        /// Parameterized constructor with IChartsDashboardRepository parameter.
        /// </summary>
        /// <param name="repo">IChartsDashboardRepository repository for StudentProgressTracker dashboard.</param>
        public ChartsDashboardService(IChartsDashboardRepository repo)
        {
            this.repo = repo;
        }

        public async Task<FilterRes> GetFiltersAync(FilterReq req)
        {
            var filters = await repo.GetFiltersAsync(req.BSUID);
            return new FilterRes()
            {
                AcademicYear = filters.Item1,
                Class = filters.Item2,
                Subject = filters.Item3,
                Assessment = filters.Item4,
                Teacher = filters.Item5
            };
        }

        public async Task<ChartRes> GetStudentProgressTrackerChartAsync(ChartReq req)
        {
            //var taskAvgPredFig = repo.GetAveragePredictionFiguresAsync(req.BSUID, req.AcademicYear, req.Subject, req.ClassId, req.Assessment, req.Teacher);
            //var taskAvgAssPredByYear = repo.GetAverageAssessmentAndPredictionByYearAsync(req.BSUID, req.AcademicYear, req.Subject, req.ClassId, req.Assessment, req.Teacher);
            //var taskAvgMarksByYearAndAss = repo.GetAverageMarksByYearAndAssessmentAsync(req.BSUID, req.AcademicYear, req.Subject, req.ClassId, req.Assessment, req.Teacher);
            //await Task.WhenAll(taskAvgPredFig, taskAvgAssPredByYear);
            var result = await repo.GetStudentProgressTrackerChartAsync(req.BSUID, req.AcademicYear, req.Subject, req.ClassId, req.Assessment, req.Teacher);
            return new ChartRes
            {
                AvgPredFig = result.Item1,
                AvgAssPredByYear = result.Item2,
                AvgMarksByYearAndAss = result.Item3,
                PredBuck = result.Item4,
                PredBySub = result.Item5
            };
        }

        public Task<IEnumerable<FilStudent>> GetStudentFiltersAync(ChartReq req)
        {
            return repo.GetStudentFiltersAsync(req.BSUID, req.AcademicYear, req.Subject, req.ClassId, req.Assessment, req.Teacher);
        }

        public async Task<ChartRes> GetStudentAssessmentPredictionChartAsync(ChartReq req)
        {
            var result = await repo.GetStudentAssessmentPredictionAsync(req.BSUID, req.AcademicYear, req.Subject, req.ClassId, req.Assessment, req.StudentId, req.Teacher);
            return new ChartRes
            {
                AvgAssPredByYear = result.Item1,
                AvgMarksByYearAndAss = result.Item2,
                PredBySubByStu = result.Item3,
                ArgPredScore = result.Item4
            };
        }

        public Task<IEnumerable<AssessmentDataByStudent>> GetAllAssessmentDataAync(ChartReq req)
        {
            return repo.GetAllAssessmentDataAsync(req.BSUID, req.AcademicYear, req.Subject, req.ClassId, req.Assessment, req.StudentId, req.Teacher);
        }
    }
}