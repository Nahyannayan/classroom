﻿using Phoenix.API.Repositories;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class CourseService : ICourseService
    {
        private readonly ICourseRepository courseRepository;

        public CourseService(ICourseRepository courseRepository)
        {
            this.courseRepository = courseRepository;
        }

        public async Task<OperationDetails> CourseCU(Course course) =>
            await courseRepository.CourseCU(course);
        public async Task<IEnumerable<Course>> GetCourses(long curriculumId, long schoolId, int PageSize, int PageNumber, string SearchString) =>
            await courseRepository.GetCourses(curriculumId, schoolId, PageSize, PageNumber, SearchString);
        public async Task<bool> IsCourseTitleUnique(string title) =>
            await courseRepository.IsCourseTitleUnique(title);
        public async Task<Course> GetCourseDetails(long courseId) =>
            await courseRepository.GetCourseDetails(courseId);
        public async Task<bool> CourseMappingCUD(CourseMapping course) =>
            await courseRepository.CourseMappingCUD(course);
        public async Task<IEnumerable<CourseMapping>> GetCourseMappings(long schoolId, long courseId, int pageNum = 1, int pageSize = 6, string searchString = "") =>
            await courseRepository.GetCourseMappings(schoolId, courseId, pageNum, pageSize, searchString);
        public async Task<CourseMapping> GetCourseMappingDetail(long courseGroupId) =>
            await courseRepository.GetCourseMappingDetail(courseGroupId);
        public async Task<IEnumerable<SchoolWiseGrade>> GetGradeListBySchoolId(string SchoolIds, long TeacherId) =>
           await courseRepository.GetGradeListBySchoolId(SchoolIds, TeacherId);
        public async Task<bool> SaveCrossSchoolPermission(CrossSchoolPermission crossSchoolPermission) =>
          await courseRepository.SaveCrossSchoolPermission(crossSchoolPermission);
        public async Task<IEnumerable<CrossSchoolPermission>> GetCrossSchoolPermissionList(long TeacherId, long SchoolId, long TCS_ID) =>
         await courseRepository.GetCrossSchoolPermissionList(TeacherId, SchoolId, TCS_ID);
        public async Task<IEnumerable<SchoolList>> GetSchoolListByTeacherId(string TeacherId) =>
         await courseRepository.GetSchoolListByTeacherId(TeacherId);


        public async Task<IEnumerable<GroupTeacher>> GetSchoolTeachersBySchoolId(string SchoolId, string TeacherId) =>
         await courseRepository.GetSchoolTeachersBySchoolId(SchoolId,TeacherId);

        public async Task<bool> IsGroupNameUnique(string title) =>
            await courseRepository.IsGroupNameUnique(title);
    }
}
