﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class StandardService : IStandardService
    {
        private readonly IStandardRepository _standardRepository;
        public StandardService(IStandardRepository standardRepository)
        {
            _standardRepository = standardRepository;
        }
        public async Task<Standard> GetStandardMasterDetailsById(Int64? StandardID)
        {
            return await _standardRepository.GetStandardMasterDetailsById(StandardID);
        }
        public OperationDetails SaveUpdateStandardMasterDetails(Standard model)
        {
            return  _standardRepository.SaveUpdateStandardMasterDetails(model);   
        }
        public async Task<IEnumerable<Standard>> GetStandardMasterList(int Acd_Id)
        {
            return await _standardRepository.GetStandardMasterList(Acd_Id);
        }
        public async Task<IEnumerable<ParentDetails>> GetParentList(Int64? SId)
        {
            return await _standardRepository.GetParentList(SId);
        }
       
        public async Task<IEnumerable<MainSyllabusIdList>> MainSyllabusIdList()
        {
            return await _standardRepository.MainSyllabusIdList();
        }

        
        public OperationDetails DeleteStandardMaster(Standard standard)
        {
            return _standardRepository.DeleteStandardMaster(standard);
        }

        public async Task<IEnumerable<Standard>> GetStandardGroupName(long UnitId,long SchoolId)
        {
            return await _standardRepository.GetStandardGroupName(UnitId,SchoolId);
        }
        public async Task<IEnumerable<SchoolTermList>> GetSchoolTerm(int acdidid)
        {
            return await _standardRepository.GetSchoolTerm(acdidid);
        }

        public async Task<int> BulkStandardUpload(StandardUploadModel standardUploadModel)
        {
            return await _standardRepository.BulkStandardUpload(standardUploadModel);
        }
        public async Task<int> BulkStandardBankUpload(StandardBankUploadModel standardUploadModel)
        {
            return await _standardRepository.BulkStandardBankUpload(standardUploadModel);
        }

        public async Task<IEnumerable<StandardBankExcelModel>> GetStandardBankList(long SchoolId,long SB_Id)
        {
            return await _standardRepository.GetStandardBankList(SchoolId,SB_Id);
        }

        public async Task<bool> AddEditStandardBank(string TransMode, string EditType,StandardBankExcelModel model)
        {
            return await _standardRepository.AddEditStandardBank(TransMode,  EditType,model);
        }

    }
}
