﻿using Phoenix.API.Repositories;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class CourseCatalogueService : ICourseCatalogueService
    {
        private readonly ICourseCatalogueRepository courseCatalogueRepository;

        public CourseCatalogueService(ICourseCatalogueRepository courseCatalogueRepository)
        {
            this.courseCatalogueRepository = courseCatalogueRepository;
        }
        public async Task<GroupCatalogue> GetCatalogueInfoUsingGroupId(long groupId) =>
            await courseCatalogueRepository.GetCatalogueInfoUsingGroupId(groupId);
        public async Task<IEnumerable<CourseCatalogue>> GetCourseCatalogueInformationByTeacher(long memberId, bool isStudent) =>
            await courseCatalogueRepository.GetCourseCatalogueInformationByTeacher(memberId, isStudent);
        public async Task<IEnumerable<CatalogueUnits>> GetCatalogueUnits(long courseId, long memberId, bool isStudent) =>
            await courseCatalogueRepository.GetCatalogueUnits(courseId, memberId, isStudent);
    }
}
