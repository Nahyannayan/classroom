﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
   public interface IAssessmentLearningService
    {
        Task<OperationDetails> SaveUpdateAssessmentLearningDetails(AssessmentLearning model,long UserId);
        Task<IEnumerable<AssessmentLearning>> GetAssessmentLearningList(long UnitId);
        Task<AssessmentLearning> GetAssessmentLearningDetailsById(long AssessmentId);
    }
}
