﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
   public interface IStandardService
    {
        Task<Standard> GetStandardMasterDetailsById(Int64? StandardID);
        OperationDetails SaveUpdateStandardMasterDetails(Standard model);
        Task<IEnumerable<Standard>> GetStandardMasterList(int Acd_Id);
        Task<IEnumerable<ParentDetails>> GetParentList(Int64? SId);
    
        Task<IEnumerable<MainSyllabusIdList>> MainSyllabusIdList();
        
        OperationDetails DeleteStandardMaster(Standard standard);
        Task<IEnumerable<Standard>> GetStandardGroupName(long UnitId,long SchoolId);
        Task<IEnumerable<SchoolTermList>> GetSchoolTerm(int acdid);
        Task<int> BulkStandardUpload(StandardUploadModel standardUploadModel);
        Task<int> BulkStandardBankUpload(StandardBankUploadModel standardUploadModel);
        Task<IEnumerable<StandardBankExcelModel>> GetStandardBankList(long SchoolId,long SB_Id);
        Task<bool> AddEditStandardBank(string TransMode, string EditType,StandardBankExcelModel standardBankModel);

    }
}
