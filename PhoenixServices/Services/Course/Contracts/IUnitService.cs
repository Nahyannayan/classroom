﻿using Phoenix.Common.Enums;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IUnitService
    {
        bool AddEditUnitMaster(Unit objUnitMaster, TransactionModes mode, string createdBy);
        Task<IEnumerable<Unit>> GetUnitMasterDetails();

        Task<Unit> GetUnitMasterDetailsByUnitId(long Id);
        Task<IEnumerable<Unit>> GetUnitMasterDetailsByCourseId(long courseId);

        Task<IEnumerable<UnitDetailsType>> GetUnitDetailsType(long unitId);
        Task<bool> AddEditUnitDetailsType(List<UnitDetailsType> objlstUnitType,long schoolId, string standardIds,string assessmentEvidenceIds, string lstOfAssessmentLearningIds,string lessonIdsToDelete, long CreatedBy);
        Task<IEnumerable<UnitTopic>> GetUnitTopicList(long MainSyllabusId);
        Task<IEnumerable<UnitTopicStandardDetails>> GetUnitTopicStandardDetails(long UnitMasterId,long GroupId, long UnitId, long StandardBankId,int PageNumber,string SearchString, long SchoolId);

        Task<IEnumerable<UnitWeek>> GetUnitWeekList(long SchoolId,short languageId);
        Task<UnitCalendar> GetUnitCalendarByCourseId(long SchoolId, long courseId);
        Task<IEnumerable<TopicTreeView>> GetStandardDetailsById(long unitId);
        bool DeleteUnitStandardDetailsById(long scmId);
        Task<IEnumerable<Unit>> GetCourseUnitMasterByCourseId(long schoolId, long courseId);
        Task<IEnumerable<UnitDetailsType>> GetUnitDetailsByCourseId(long courseId);

        Task<IEnumerable<Unit>> GetUnitMasterDetailsByGroupId(long groupId);
        Task<IEnumerable<Unit>> GetCourseDetailsAndUnitList(long courseId, long teacherId);

        Task<IEnumerable<Unit>> GetUnitsByGroup(long Id);
        Task<IEnumerable<Unit>> GetSubUnitsById(long Id);
        Task<IEnumerable<Attachment>> GetAttachmentDetailByUnitId(long UnitId);
    }
}
