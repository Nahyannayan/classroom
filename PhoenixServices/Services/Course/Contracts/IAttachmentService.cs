﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IAttachmentService
    {
        Task<bool> DeleteAttachment(long id);
        Task<Attachment> GetAttachment(long masterId = 0, string masterKey = "", long? attachmentId = null);
        Task<bool> AttachmentCU(List<Attachment> attachments);
    }
}
