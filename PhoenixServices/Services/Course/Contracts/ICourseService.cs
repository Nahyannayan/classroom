﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ICourseService
    {
        Task<OperationDetails> CourseCU(Course course);
        Task<IEnumerable<Course>> GetCourses(long curriculumId, long schoolId, int PageSize, int PageNumber, string SearchString);
        Task<bool> IsCourseTitleUnique(string title);
        Task<Course> GetCourseDetails(long courseId);
        Task<bool> CourseMappingCUD(CourseMapping course);
        Task<IEnumerable<CourseMapping>> GetCourseMappings(long schoolId, long courseId, int pageNum = 1, int pageSize = 6, string searchString = "");
        Task<CourseMapping> GetCourseMappingDetail(long courseGroupId);
        Task<IEnumerable<SchoolWiseGrade>> GetGradeListBySchoolId(string SchoolIds, long TeacherId);
        Task<bool> SaveCrossSchoolPermission(CrossSchoolPermission crossSchoolPermission);
        Task<IEnumerable<CrossSchoolPermission>> GetCrossSchoolPermissionList(long TeacherId, long SchoolId, long TCS_ID);
        Task<IEnumerable<SchoolList>> GetSchoolListByTeacherId(string TeacherId);
        Task<IEnumerable<GroupTeacher>> GetSchoolTeachersBySchoolId(string SchoolId, string TeacherId);
        Task<bool> IsGroupNameUnique(string title);


    }
}
