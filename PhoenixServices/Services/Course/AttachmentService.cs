﻿using Phoenix.API.Repositories;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class AttachmentService : IAttachmentService
    {
        private readonly IAttachmentRepository attachmentRepository;

        public AttachmentService(IAttachmentRepository attachmentRepository)
        {
            this.attachmentRepository = attachmentRepository;
        }
        public async Task<bool> DeleteAttachment(long id)
        {
            return await attachmentRepository.DeleteAttachment(id);
        }
        public async Task<Attachment> GetAttachment(long masterId = 0, string masterKey = "", long? attachmentId = null) =>
            await attachmentRepository.GetAttachment(masterId, masterKey, attachmentId);
        public async Task<bool> AttachmentCU(List<Attachment> attachments)
        {
            return await attachmentRepository.AttachmentCU(attachments);
        }
    }
}
