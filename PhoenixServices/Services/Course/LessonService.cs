﻿using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
namespace Phoenix.API.Services
{
    public class LessonService : ILessonService
    {
        private readonly ILessonRepository _iLessonRepository;
        public LessonService(ILessonRepository iLessonRepository)
        {
            _iLessonRepository = iLessonRepository;

        }

        public async Task<IEnumerable<UnitDetails>> GetUnitDetails()
        {
            return await _iLessonRepository.GetUnitDetails();
        }
        public async Task<IEnumerable<StandardDetailsValue>> GetStandardDetails()
        {
            return await _iLessonRepository.GetStandardDetails();
        }

        public async Task<IEnumerable<Lesson>> GetLessonDetails()
        {
            return await _iLessonRepository.GetLessonDetails();
        }
        public bool AddEditLesson(Lesson model)
        {
            return  _iLessonRepository.AddEditLesson(model);
        }
        public async Task<IEnumerable<LessonCourse>> GetTopicLessonDetail(long CourseId)
        {
            return await _iLessonRepository.GetTopicLessonDetail(CourseId);
        }
        public async Task<IEnumerable<UnitDetails>> GetUnitBasedonCourse(long CourseId)
        {
            return await _iLessonRepository.GetUnitBasedonCourse(CourseId);
        }


        #region School Unit Detail type Mapping
        public async Task<bool> SchoolUnitDetailsTypeCD(SchoolUnitDetailTypeMapping typeMapping)
            => await _iLessonRepository.SchoolUnitDetailsTypeCD(typeMapping);
        public async Task<IEnumerable<SchoolUnitDetailTypeMapping>> GetSchoolUnitDetailTypes(long schoolId, int divisionId)
            => await _iLessonRepository.GetSchoolUnitDetailTypes(schoolId, divisionId);
        public async Task<IEnumerable<SchoolUnitDetailType>> GetSchoolUnitDetailType(SchoolUnitDetailType schoolUnit)
            => await _iLessonRepository.GetSchoolUnitDetailType(schoolUnit);
        public async Task<bool> SaveSchoolUnitDetailType(SchoolUnitDetailType schoolUnit)
            => await _iLessonRepository.SaveSchoolUnitDetailType(schoolUnit);
        #endregion
    }

}
