﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
namespace Phoenix.API.Services
{
    public class UnitService : IUnitService
    {
        private readonly IUnitRepository _iUnitRepository;
        public UnitService(IUnitRepository iUnitRepository)
        {
            _iUnitRepository = iUnitRepository;

        }

        public async Task<bool> AddEditUnitDetailsType(List<UnitDetailsType> objlstUnitType,long schoolId, string standardIds,string assessmentEvidenceIds, string lstOfAssessmentLearningIds,string lessonIdsToDelete, long CreatedBy)
        {
            return await _iUnitRepository.AddEditUnitDetailsType(objlstUnitType,schoolId, standardIds, assessmentEvidenceIds, lstOfAssessmentLearningIds, lessonIdsToDelete, CreatedBy);
        }

        public bool AddEditUnitMaster(Unit objUnitMaster, TransactionModes mode, string createdBy)
        {
            return _iUnitRepository.AddEditUnitMaster(objUnitMaster, mode, createdBy);
        }

        public async Task<IEnumerable<UnitDetailsType>> GetUnitDetailsType(long unitId)
        {
            return await _iUnitRepository.GetUnitDetailsType(unitId);
        }

        public async Task<IEnumerable<Unit>> GetUnitMasterDetails()
        {
            return await _iUnitRepository.GetUnitMasterDetails();

        }

        public async Task<IEnumerable<Unit>> GetUnitMasterDetailsByCourseId(long courseId)
        {
            return await _iUnitRepository.GetUnitMasterDetailsByCourseId(courseId);
        }

        public async Task<Unit> GetUnitMasterDetailsByUnitId(long Id)
        {
            return await _iUnitRepository.GetUnitMasterDetailsByUnitId(Id);
        }

        public async Task<IEnumerable<UnitTopic>> GetUnitTopicList(long MainSyllabusId)
        {
            return await _iUnitRepository.GetUnitTopicList(MainSyllabusId);
        }


        public async Task<IEnumerable<UnitTopicStandardDetails>> GetUnitTopicStandardDetails(long UnitMasterId,long GroupId, long UnitId, long StandardBankId,int PageNumber,string SearchString, long SchoolId)
        {
            return await _iUnitRepository.GetUnitTopicStandardDetails(UnitMasterId, GroupId,  UnitId,  StandardBankId,PageNumber,SearchString, SchoolId);
        }

        public async Task<IEnumerable<UnitWeek>> GetUnitWeekList(long SchoolId,short languageId)
        {
            return await _iUnitRepository.GetUnitWeekList(SchoolId, languageId);
        }

        public async Task<UnitCalendar> GetUnitCalendarByCourseId(long SchoolId,long courseId)
        {
            return await _iUnitRepository.GetUnitCalendarByCourseId(SchoolId,courseId);
        }

        public async Task<IEnumerable<TopicTreeView>> GetStandardDetailsById(long unitId)
        {
            return await _iUnitRepository.GetStandardDetailsById(unitId);
        }

        public  bool DeleteUnitStandardDetailsById(long scmId)
        {
            return  _iUnitRepository.DeleteUnitStandardDetailsById(scmId);
        }

        public async Task<IEnumerable<Unit>> GetCourseUnitMasterByCourseId(long schoolId,long courseId)
        {
            return await _iUnitRepository.GetCourseUnitMasterByCourseId(schoolId,courseId);
        }

        public async Task<IEnumerable<UnitDetailsType>> GetUnitDetailsByCourseId(long courseId)
        {
            return await _iUnitRepository.GetUnitDetailsByCourseId(courseId);
        }

        public async Task<IEnumerable<Unit>> GetUnitMasterDetailsByGroupId(long groupId)
        {
            return await _iUnitRepository.GetUnitMasterDetailsByGroupId(groupId);
        }
        public async Task<IEnumerable<Unit>> GetCourseDetailsAndUnitList(long courseId, long teacherId) =>
            await _iUnitRepository.GetCourseDetailsAndUnitList(courseId, teacherId);

        public async Task<IEnumerable<Unit>> GetUnitsByGroup(long Id)
        {
            return await _iUnitRepository.GetUnitsByGroup(Id);
        }
        public async Task<IEnumerable<Unit>> GetSubUnitsById(long Id)
        {
            return await _iUnitRepository.GetSubUnitsById(Id);
        }
        public async Task<IEnumerable<Attachment>> GetAttachmentDetailByUnitId(long UnitId)
        {
            return await _iUnitRepository.GetAttachmentDetailByUnitId(UnitId);
        }
    }

}

