﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class AssessmentLearningService : IAssessmentLearningService
    {
        private readonly IAssessmentLearningRepository _AssessmentLearningRepository;
        public AssessmentLearningService(IAssessmentLearningRepository assessmentLearningRepository)
        {
            _AssessmentLearningRepository = assessmentLearningRepository;
        }
       
      public async Task<OperationDetails> SaveUpdateAssessmentLearningDetails(AssessmentLearning model,long UserId)
        {
            return  await _AssessmentLearningRepository.SaveUpdateAssessmentLearningDetails(model,UserId);   
        }
        public async Task<IEnumerable<AssessmentLearning>> GetAssessmentLearningList(long UnitId)
        {
            return await _AssessmentLearningRepository.GetAssessmentLearningList(UnitId);
        }
        public async Task<AssessmentLearning> GetAssessmentLearningDetailsById(long AssessmentId)
        {
            return await _AssessmentLearningRepository.GetAssessmentLearningDetailsById(AssessmentId);
        }
    }
}
