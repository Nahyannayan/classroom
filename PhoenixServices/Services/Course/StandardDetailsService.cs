﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class StandardDetailsService : IStandardDetailsService
    {
        private readonly IStandardDetailsRepository _standardDetailsRepository;
        public StandardDetailsService(IStandardDetailsRepository standardDetailsRepository)
        {
            _standardDetailsRepository = standardDetailsRepository;
        }
        public async Task<StandardDetails> GetStandardDetailsById(Int64? StandardDetailsID)
        {
            return await _standardDetailsRepository.GetStandardDetailsById(StandardDetailsID);
        }
        public OperationDetails SaveUpdateStandardDetails(StandardDetails model)
        {
            return _standardDetailsRepository.SaveUpdateStandardDetails(model);   
        }
        public async Task<IEnumerable<StandardDetails>> GetStandardDetailsList()
        {
            return await _standardDetailsRepository.GetStandardDetailsList();
        }

        public OperationDetails DeleteStandardDetails(StandardDetails standardDetails)
        {
            return _standardDetailsRepository.DeleteStandardDetails(standardDetails);
        }
    }
}
