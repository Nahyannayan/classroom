﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class AssessmentEvidenceService : IAssessmentEvidenceService
    {
        private readonly IAssessmentEvidenceRepository _AssessmentEvidenceRepository;
        public AssessmentEvidenceService(IAssessmentEvidenceRepository assessmentEvidenceRepository)
        {
            _AssessmentEvidenceRepository = assessmentEvidenceRepository;
        }
       
      public async Task<OperationDetails> SaveUpdateAssessmentEvidenceDetails(AssessmentEvidence model,long UserId)
        {
            return  await _AssessmentEvidenceRepository.SaveUpdateAssessmentEvidenceDetails(model,UserId);   
        }
        public async Task<IEnumerable<AssessmentEvidence>> GetAssessmentEvidenceList(long UnitId)
        {
            return await _AssessmentEvidenceRepository.GetAssessmentEvidenceList(UnitId);
        }
        public async Task<AssessmentEvidence> GetAssessmentEvidenceDetailsById(long AssessmentId)
        {
            return await _AssessmentEvidenceRepository.GetAssessmentEvidenceDetailsById(AssessmentId);
        }
    }
}
