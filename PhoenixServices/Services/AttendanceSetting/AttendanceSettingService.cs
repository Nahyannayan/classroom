﻿using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.API.Services;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
namespace Phoenix.API.Services
{
    public class AttendanceSettingService : IAttendanceSettingService
    {
        private readonly IAttendanceSettingRepository _IAttendancSettingRepository;
        public AttendanceSettingService(IAttendanceSettingRepository IAttendancSettingRepository)
        {

            _IAttendancSettingRepository = IAttendancSettingRepository;

        }
        public async Task<IEnumerable<GradeDetails>> GetGradeDetails()
        {
            return await _IAttendancSettingRepository.GetGradeDetails();
        }
        #region Attendance Calendar
        public async Task<IEnumerable<GradeAndSection>> GetGradeAndSectionList(long SchoolId)
        {
            return await _IAttendancSettingRepository.GetGradeAndSectionList(SchoolId);
        }
        public async Task<SchoolWeekEnd> GetSchoolWeekEnd(long BSU_ID)
        {
            return await _IAttendancSettingRepository.GetSchoolWeekEnd(BSU_ID);
        }
        public async Task<int> SaveCalendarEvent(AttendanceCalendar attendanceCalendar, string DATAMODE)
        {
            return await _IAttendancSettingRepository.SaveCalendarEvent(attendanceCalendar, DATAMODE);
        }
        public async Task<IEnumerable<AttendanceCalendar>> GetCalendarDetail(long SchoolId, long SCH_ID, bool IsListView)
        {
            return await _IAttendancSettingRepository.GetCalendarDetail(SchoolId, SCH_ID, IsListView);
        }
        public async Task<AcademicYearDetail> GetAcademicYearDetail(long SchoolId)
        {
            return await _IAttendancSettingRepository.GetAcademicYearDetail(SchoolId);
        }
        #endregion Attendance Calendar
        #region Parameter Setting
        public async Task<int> SaveParameterSetting(ParameterSetting parameterSetting, string DATAMODE)
        {
            return await _IAttendancSettingRepository.SaveParameterSetting(parameterSetting, DATAMODE);
        }
        public async Task<IEnumerable<ParameterSetting>> GetParameterSettingList(long BSU_ID)
        {
            return await _IAttendancSettingRepository.GetParameterSettingList(BSU_ID);
        }
        #endregion
        #region Leave approval permission
        public async Task<IEnumerable<LeaveApprovalPermissionModel>> GetLeaveApprovalPermission(long AcdId, long schoolId, int divisionId)
        {
            return await _IAttendancSettingRepository.GetLeaveApprovalPermission(AcdId, schoolId, divisionId);
        }
        public async Task<int> LeaveApprovalPermissionCU(LeaveApprovalPermissionModel leaveApproval)
        {
            return await _IAttendancSettingRepository.LeaveApprovalPermissionCU(leaveApproval);
        }
        #endregion

        #region att type
        public async Task<IEnumerable<AttendanceType>> GetAttendanceType(long BSU_ID)
        {
            return await _IAttendancSettingRepository.GetAttendanceType(BSU_ID);
        }
        public async Task<int> SaveAttendanceType(AttendanceType AttendanceType, string DATAMODE)
        {
            return await _IAttendancSettingRepository.SaveAttendanceType(AttendanceType, DATAMODE);
        }
        #endregion
        #region Attendance Period

        public async Task<IEnumerable<AttendacePeriodModel>> GetAttendancePeriodList(long gradeId)
        {
            return await _IAttendancSettingRepository.GetAttendancePeriodList(gradeId);
        }
        public async Task<IEnumerable<AttendanceType>> GetAttendanceTypeListBYId(long AttendanceConfigurationTypeID)
        {
            return await _IAttendancSettingRepository.GetAttendanceTypeListBYId(AttendanceConfigurationTypeID);
        }
        
        public bool AddUpdateAttendancePeriod(long schoolId, long academicId, string gradeId, long CreatedBy, List<AttendacePeriodModel> objAttendancePeriod)
        {
            return _IAttendancSettingRepository.AddUpdateAttendancePeriod(schoolId, academicId, gradeId, CreatedBy, objAttendancePeriod);
        }
        #endregion


    }
}
