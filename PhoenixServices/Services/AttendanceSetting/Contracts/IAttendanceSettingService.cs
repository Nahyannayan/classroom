﻿using Phoenix.API.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Phoenix.API.Services
{
    public interface IAttendanceSettingService
    {
        Task<IEnumerable<GradeDetails>> GetGradeDetails();
        #region Attendance Calendar
        Task<IEnumerable<GradeAndSection>> GetGradeAndSectionList(long SchoolId);
        Task<SchoolWeekEnd> GetSchoolWeekEnd(long BSU_ID);
        Task<int> SaveCalendarEvent(AttendanceCalendar attendanceCalendar, string DATAMODE);
        Task<IEnumerable<AttendanceCalendar>> GetCalendarDetail(long SchoolId, long SCH_ID, bool IsListView);
        Task<AcademicYearDetail> GetAcademicYearDetail(long SchoolId);
        #endregion Attendance Calendar
        #region Parameter Setting
        Task<int> SaveParameterSetting(ParameterSetting parameterSetting, string DATAMODE);
        Task<IEnumerable<ParameterSetting>> GetParameterSettingList(long BSU_ID);
        #endregion
        #region Leave approval permission
        Task<IEnumerable<LeaveApprovalPermissionModel>> GetLeaveApprovalPermission(long AcdId, long schoolId, int divisionId);
        Task<int> LeaveApprovalPermissionCU(LeaveApprovalPermissionModel leaveApproval);
        #endregion

        #region att type
        Task<IEnumerable<AttendanceType>> GetAttendanceType(long BSU_ID);
        Task<int> SaveAttendanceType(AttendanceType AttendanceType, string DATAMODE);
        #endregion
        #region Attendance Period
        Task<IEnumerable<AttendacePeriodModel>> GetAttendancePeriodList(long gradeId);
        Task<IEnumerable<AttendanceType>> GetAttendanceTypeListBYId(long AttendanceConfigurationTypeID);
      
        bool AddUpdateAttendancePeriod(long schoolId, long academicId, string gradeId, long CreatedBy, List<AttendacePeriodModel> objAttendancePeriod);
        #endregion

   
    }
}
