﻿using Phoenix.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class SurveyService: ISurveyService
    {
        private readonly ISurveyRepository surveyRepository;
        public SurveyService(ISurveyRepository surveyRepository)
        {
            this.surveyRepository = surveyRepository;
        }
    }
}
