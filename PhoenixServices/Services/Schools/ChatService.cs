﻿using Phoenix.Models;
using Phoenix.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class ChatService : IChatService
    {
        private readonly IChatRepository _ChatRepository;
        public ChatService(IChatRepository ChatRepository)
        {
            _ChatRepository = ChatRepository;
        }

        /// <summary>
        /// Delete Chat
        /// </summary>
        /// <param name="id"></param>
        public void DeleteAsync(int id)
        {
            _ChatRepository.DeleteAsync(id);
        }

        /// <summary>
        /// Get All Chats
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Chat>> GetAllAsync()
        {
            return _ChatRepository.GetAllAsync();
        }

        /// <summary>
        /// Get Chat by Chat Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Chat> GetAsync(int id)
        {
            return _ChatRepository.GetAsync(id);
        }

        /// <summary>
        /// Get Types of messages
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<MessageTypes>> GetMessageTypes()
        {
            return _ChatRepository.GetMessageTypes();
        }

        /// <summary>
        /// Get Chats based on types of messages
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="messageTypeId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Chat>> GetChatsByMessageTypeId(int schoolId, int messageTypeId)
        {
            return _ChatRepository.GetChatsByMessageTypeId(schoolId, messageTypeId);
        }

        /// <summary>
        /// Get chats based on school
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Chat>> GetChatsBySchoolId(int schoolId)
        {
            return _ChatRepository.GetChatsBySchoolId(schoolId);
        }


        /// <summary>
        /// Insert into the chat table
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int InsertAsync(Chat entity)
        {
            return _ChatRepository.Insert(entity);
        }

        public void UpdateAsync(Chat entityToUpdate)
        {
            _ChatRepository.UpdateAsync(entityToUpdate);
        }

        /// <summary>
        /// Get chats based on type of chat
        /// </summary>
        /// <param name="ChatTypeId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Chat>> GetChatsByChatTypeId(int ChatTypeId)
        {
            return _ChatRepository.GetChatsByChatTypeId(ChatTypeId);
        }

        /// <summary>
        /// get chat by group
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Chat>> GetChatsByGroupId(int groupId, int schoolId, int? userId)
        {
            return _ChatRepository.GetChatsByGroupId(groupId, schoolId, userId);
        }

        /// <summary>
        /// get all the chat types
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<ChatTypes>> GetChatTypes()
        {
            return _ChatRepository.GetChatTypes();
        }

        /// <summary>
        /// To get recent Chats of a user
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task<IEnumerable<ChatList>> GetRecentChats(int schoolId, int userId)
        {
            return _ChatRepository.GetRecentChats(schoolId,userId);
        }

        /// <summary>
        /// To get the User Activity by UserId
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userid"></param>
        /// <param name="enableChat"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public Task<IEnumerable<RecentUserActivity>> GetRecentUserActivity(int schoolId, int userid, bool enableChat, long groupId)
        {
            return _ChatRepository.GetRecentUserActivity(schoolId, userid, enableChat, groupId);
        }

        /// <summary>
        /// To get recent Chat user list of a user
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Chat>> GetRecentChatUserList(int schoolId, int userId)
        {
            return _ChatRepository.GetRecentChatUserList(schoolId, userId);
        }


        /// <summary>
        /// To get the chat history by user
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <param name="otherUser"></param>
        /// <param name="groupId"></param>
        /// <param name="pageNum"></param>
        /// <returns></returns>
        public Task<IEnumerable<Chat>> GetChatHistorybyUserId(long schoolId, long userId, long otherUser, long groupId,int? pageNum =1)
        {
            return _ChatRepository.GetChatHistorybyUserId(schoolId, userId, otherUser, groupId, pageNum);
        }

        /// <summary>
        /// To get the search result in chat
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <param name="userTypeId"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public Task<IEnumerable<Chat>> GetSearchInChat(int schoolId, int userId, int userTypeId, string text)
        {
            return _ChatRepository.GetSearchInChat(schoolId, userId, userTypeId, text);
        }

        /// <summary>
        /// To get the list of my contacts
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task<IEnumerable<LogInUser>> GetMyContactsList(int schoolId, int userId)
        {
            return _ChatRepository.GetMyContactsList(schoolId, userId);
        }

        #region Invite Chat
        /// <summary>
        /// To insert the invite user for group chat
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int InviteUserForChat(InviteChat entity)
        {
            return _ChatRepository.InviteUserForChat(entity);
        }

        /// <summary>
        ///  To update the user permission or group related information in quick chat
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int UpdateInviteUserGroupChat(InviteChat entity)
        {
            return _ChatRepository.UpdateInviteUserGroupChat(entity);
        }

        /// <summary>
        /// To Delete the user from Group Chat permanently
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int DeleteInviteUserGroupChat(InviteChat entity)
        {
            return _ChatRepository.DeleteInviteUserGroupChat(entity);
        }

        public string UpdateInviteChatGroupName(InviteChat entity)
        {
            return _ChatRepository.UpdateInviteChatGroupName(entity);
        }

        /// <summary>
        /// To get the latest group chat id
        /// </summary>
        /// <returns></returns>
        public int GetLatestInviteGroup()
        {
            return _ChatRepository.GetLatestInviteGroup();
        }

        /// <summary>
        /// To get the list of users in group chat
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public Task<IEnumerable<InviteChat>> GetGroupChatUsers(long groupId, long schoolId)
        {
            return _ChatRepository.GetGroupChatUsers(groupId, schoolId);
        }

        /// <summary>
        /// get Group Details based on various paramters
        /// </summary>
        /// <param name="groupid"></param>
        /// <param name="schoolid"></param>
        /// <param name="userid"></param>
        /// <param name="groupname"></param>
        /// <returns></returns>
        public Task<IEnumerable<InviteChat>> GetGroupDetails(long? groupid, long? schoolid, long? userid, string groupname)
        {
            return _ChatRepository.GetGroupDetails(groupid, schoolid, userid, groupname);
        }

        /// <summary>
        /// To get the invite group name of group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public string GetInviteGroupName(long groupId)
        {
            return _ChatRepository.GetInviteGroupName(groupId);
        }


        /// <summary>
        /// To get the list of all users in group chat
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public Task<IEnumerable<InviteChat>> GetAllGroupUsers(long schoolId)
        {
            return _ChatRepository.GetAllGroupUsers(schoolId);
        }

        #endregion


        public int DeleteChatConversation(long schoolId, long groupId, long fromUser, long toUser)
        {
            return _ChatRepository.DeleteChatConversation(schoolId, groupId, fromUser, toUser);
        }

        public int InsertChatLog(UserChatLog entity)
        {
            return _ChatRepository.InsertChatLog(entity);
        }

        public int DeleteChatlog(UserChatLog entity)
        {
            return _ChatRepository.DeleteChatlog(entity);
        }

        public int DeleteAllChatlog()
        {
            return _ChatRepository.DeleteAllChatlog();
        }

        public Task<IEnumerable<UserChatLog>> GetConnectionbyUserId(string connectionid, long? schoolId, long? userId, string sessionid)
        {
            return _ChatRepository.GetConnectionbyUserId(connectionid, schoolId, userId, sessionid);
        }

    }
}
