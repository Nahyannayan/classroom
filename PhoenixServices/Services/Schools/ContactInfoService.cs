﻿using Phoenix.API.Repositories;
using Phoenix.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Phoenix.Common.Enums;

namespace Phoenix.API.Services
{
    public class ContactInfoService : IContactInfoService
    {

        private readonly IContactInfoRepository _contactInfoRepository;
        public ContactInfoService(IContactInfoRepository contactInfoRepository)
        {
            _contactInfoRepository = contactInfoRepository;
        }

        public bool DeleteContactInfo(int id)
        {
            ContactInfo entity = new ContactInfo(id);
            return _contactInfoRepository.UpdateContactInfoData(entity, TransactionModes.Delete);
        }

        //To get all contact info
        public async Task<IEnumerable<ContactInfo>> GetContactInfos()
        {
            return await _contactInfoRepository.GetContactInfos();
        }

        //To get contact info by id
        public async Task<ContactInfo> GetContactInfoById(int id)
        {
            return await _contactInfoRepository.GetAsync(id);
        }

        //To get contact info by school id
        public async Task<IEnumerable<ContactInfo>> GetContactInfoBySchoolId(int schoolId)
        {
            return await _contactInfoRepository.GetContactInfoBySchoolId(schoolId);
        }

        //Insert contact info
        public bool InsertContactInfo(ContactInfo entity)
        {
            return _contactInfoRepository.UpdateContactInfoData(entity, TransactionModes.Insert);
        }

        //Update contact info
        public bool UpdateContactInfo(ContactInfo entity)
        {
            return _contactInfoRepository.UpdateContactInfoData(entity, TransactionModes.Update);
        }
    }
}
