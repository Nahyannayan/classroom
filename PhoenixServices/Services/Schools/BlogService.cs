﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.API.Repositories;
using System.Data;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class BlogService : IBlogService
    {
        private readonly IBlogRepository _blogRepository;
        public BlogService(IBlogRepository blogRepository)
        {
            _blogRepository = blogRepository;
        }
        public void DeleteAsync(int id)
        {
            _blogRepository.DeleteAsync(id);
        }

        public Task<IEnumerable<Blog>> GetAllAsync()
        {
            return _blogRepository.GetAllAsync();
        }

        public Task<Blog> GetAsync(int id)
        {
            return _blogRepository.GetAsync(id);
        }

        public Task<IEnumerable<BlogCategory>> GetBlogCategories(long? schoolId, short languageId)
        {
            return _blogRepository.GetBlogCategories(schoolId, languageId);
        }

        public Task<IEnumerable<Blog>> GetBlogsByCategoryId(int schoolId, int categoryId, bool? isPublish)
        {
            return _blogRepository.GetBlogsByCategoryId(schoolId, categoryId, isPublish);
        }

        public Task<IEnumerable<Blog>> GetBlogsBySchoolId(int schoolId, bool? isPublish)
        {
            return _blogRepository.GetBlogsBySchoolId(schoolId, isPublish);
        }

        public int InsertAsync(Blog entity)
        {
            //_blogRepository.InsertAsync(entity);
            return _blogRepository.Insert(entity);
        }
        public int Insert(Blog entity)
        {
            return _blogRepository.Insert(entity);
        }



        public int InsertMapAsync(SchoolGroup_StudentMapping mapModal)
        {
            return _blogRepository.InsertMap(mapModal);
        }

        public int InsertSharePost(sharepost shareModal)
        {
            return _blogRepository.InsertSharePost(shareModal);
        }



        public bool UpdateDiscloseDate(int id)
        {
            return _blogRepository.UpdateDiscloseDate(id);
        }
        public void UpdateAsync(Blog entityToUpdate)
        {
            _blogRepository.UpdateAsync(entityToUpdate);
        }
        public Task<IEnumerable<Blog>> GetTeacherBlogs(int schoolId, int pageNumber, int pageSize)
        {
            return _blogRepository.GetTeacherBlogs(schoolId, pageNumber, pageSize);
        }

        public void PublishBlog(int blogId, bool isPublish, int publishBy)
        {
            _blogRepository.PublishBlog(blogId, isPublish, publishBy);
        }

        public Task<IEnumerable<Blog>> GetBlogsByBlogTypeId(int blogTypeId, bool? isPublish)
        {
            return _blogRepository.GetBlogsByBlogTypeId(blogTypeId, isPublish);
        }

        public Task<IEnumerable<Blog>> GetBlogsByGroupId(int groupId, int schoolId, int? userId, bool? isPublish)
        {
            return _blogRepository.GetBlogsByGroupId(groupId, schoolId, userId, isPublish);
        }

        public Task<IEnumerable<Blog>> GetBlogsByGroupNBlogtypeId(int groupId, int blogTypeId, bool? isPublish)
        {
            return _blogRepository.GetBlogsByGroupNBlogtypeId(groupId, blogTypeId, isPublish);
        }

        public bool InsertLike(BlogLike blogLike)
        {
            return _blogRepository.InsertLike(blogLike);
        }

        public bool UpdatedLike(BlogLike blogLike)
        {
            return _blogRepository.UpdatedLike(blogLike);
        }

        public bool DeleteLike(int id)
        {
            return _blogRepository.DeleteLike(id);
        }

        public Task<IEnumerable<BlogLike>> GetBlogLikes(int? blogId, int? blogLikeId, int? likedBy, bool? isLike)
        {
            return _blogRepository.GetBlogLikes(blogId, blogLikeId, likedBy, isLike);
        }

        public Task<IEnumerable<BlogTypes>> GetBlogTypes(long? schoolId, short languageId)
        {
            return _blogRepository.GetBlogTypes(schoolId, languageId);
        }

        public int DeleteExemplar(Blog entity)
        {
            return _blogRepository.DeleteExemplar(entity);
        }

        public bool UpdateWallSortOrder(DataTable dataTable, long UpdatedBy)
        {
            return _blogRepository.UpdateWallSortOrder(dataTable, UpdatedBy);
        }

        public Task<IEnumerable<Chatter>> GetChattersWithComment(int? blogId, int schoolId, int? categoryId, int groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish)
        {
            return _blogRepository.GetChattersWithComment(blogId, schoolId, categoryId, groupId, userId, isTeacher, isPublish, commentId, PublishBy, isCommentPublish);
        }

        public async Task<IEnumerable<Blog>> GetUserExemplerWallData(long userId, int pageSize, int pageIndex) => await _blogRepository.GetUserExemplerWallData(userId, pageSize, pageIndex);

        public async Task<IEnumerable<SearchMyWall>> GetSearchOnMyWall(long schooId, string searchString, long userId, bool isTeacher) => await _blogRepository.GetSearchOnMyWall(schooId, searchString, userId, isTeacher);

        public async Task<IEnumerable<SearchMyWall>> GetSearchBlogsOnMyGroup(long schoolId, string searchString, long userId, bool isTeacher, long groupId) => await _blogRepository.GetSearchBlogsOnMyGroup(schoolId, searchString, userId, isTeacher, groupId);

        public Task<IEnumerable<Chatter>> GetMyWallData(int? blogId, int schoolId, int? categoryId, int? blogTypeId, int? groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish, int? LogedInUserId, int? pageNum)
        {
            return _blogRepository.GetMyWallData(blogId, schoolId, categoryId, blogTypeId, groupId, userId, isTeacher, isPublish, commentId, PublishBy, isCommentPublish, LogedInUserId, pageNum);
        }
        public Task<IEnumerable<Chatter>> GetMyWallDataOnSearch(long? blogId, long schoolId, int? categoryId, int? blogTypeId, long? groupId, long? userId, bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish, int? LogedInUserId, int? pageNum)
        {
            return _blogRepository.GetMyWallDataOnSearch(blogId, schoolId, categoryId, blogTypeId, groupId, userId, isTeacher, isPublish, commentId, PublishBy, isCommentPublish, LogedInUserId, pageNum);
        }

        public Task<IEnumerable<BlogStudentDash>> GetStudentDashBlogs(long userId, int schoolId)
        {
            return _blogRepository.GetStudentDashBlogs(userId, schoolId);
        }

        public Task<IEnumerable<BlogTeacherDash>> GetTeacherDashBlogs(long userId, int schoolId)
        {
            return _blogRepository.GetTeacherDashBlogs(userId, schoolId);
        }

        public void DeleteBlog(int id, long userId)
        {
            _blogRepository.DeleteBlog(id, userId);
        }

        public Task<IEnumerable<PostAndCommentLikeUserList>> GetPostAndCommentLikeUserList(long BlogId, long CommentSectioId, long UserId, bool IsCommentDetailDisplay)
        {
         return   _blogRepository.GetPostAndCommentLikeUserList(BlogId, CommentSectioId, UserId, IsCommentDetailDisplay);
        }
        public Task<IEnumerable<BlogCompleteData>> GetMyChatterData(long? blogId, long schoolId, int? categoryId, int? blogTypeId, long? groupId, long? userId,
       bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish, int? LogedInUserId, int? pageNum, bool EnableChat)
        {
            return _blogRepository.GetMyChatterData(blogId, schoolId, categoryId, blogTypeId, groupId, userId, isTeacher, isPublish, commentId, PublishBy, isCommentPublish,
                LogedInUserId, pageNum, EnableChat);
        }
    }
}
