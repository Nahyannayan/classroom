﻿using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class SchoolReportService : ISchoolReportService
    {
        private readonly ISchoolReportRepository schoolReportRepository;

        public SchoolReportService(ISchoolReportRepository schoolReportRepository)
        {
            this.schoolReportRepository = schoolReportRepository;
        }

        public void DeleteAsync(int id, long schoolId) {
            schoolReportRepository.DeleteAsync(id,schoolId);
        }
        public async Task<int> SchoolReportCU(SchoolReports schoolReports) =>
            await schoolReportRepository.SchoolReportCU(schoolReports);
        public async Task<IEnumerable<ReportListModel>> GetReportLists(long schoolId) =>
            await schoolReportRepository.GetReportLists(schoolId);
        public async Task<ReportDetailModel> GetReportDetail(long reportDetailId, bool isPreview, bool isCommonReport, long schoolId) =>
           await schoolReportRepository.GetReportDetail(reportDetailId, isPreview, isCommonReport, schoolId);
        public async Task<IEnumerable<ReportFilterModel>> GetReportFilter(string filterCode, string filterParameter) =>
            await schoolReportRepository.GetReportFilter(filterCode, filterParameter);
        public async Task<ReportDataModel> GetReportData(ReportParameterRequestModel requestModel) =>
            await schoolReportRepository.GetReportData(requestModel);
    }
}
