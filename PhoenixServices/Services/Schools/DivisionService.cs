﻿using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class DivisionService : IDivisionService
    {
        private readonly IDivisionRepositorycs _IDivisionRepositorycs;
        public DivisionService(IDivisionRepositorycs IDivisionRepositorycs)
        {

            _IDivisionRepositorycs = IDivisionRepositorycs;

        }
        public async Task<OperationDetails> SaveDivisionDetails(DivisionDetails DivisionDetails, string DATAMODE)
        {
            return await _IDivisionRepositorycs.SaveDivisionDetails(DivisionDetails, DATAMODE);
        }
        public async Task<IEnumerable<DivisionDetails>> GetDivisionDetails(long BSU_ID, int CurriculumId)
        {
            return await _IDivisionRepositorycs.GetDivisionDetails(BSU_ID, CurriculumId);
        }
        public async Task<IEnumerable<ReportingTermModel>> GetReportingTermDetail(long ReportingTermId, long SchoolId)
        {
            return await _IDivisionRepositorycs.GetReportingTermDetail(ReportingTermId, SchoolId);
        }
        public async Task<IEnumerable<ReportingSubTermModel>> GetReportingSubTermDetail(long ReportingTermId, long ReportingSubTermId)
        {
            return await _IDivisionRepositorycs.GetReportingSubTermDetail(ReportingTermId, ReportingSubTermId);
        }
        public async Task<bool> SaveReportingTermDetail(ReportingTermModel reportingTermModel)
        {
            return await _IDivisionRepositorycs.SaveReportingTermDetail(reportingTermModel);
        }
        public async Task<bool> LockUnlockTerm(long ReportingTermId, bool IsLock)
        {
            return await _IDivisionRepositorycs.LockUnlockTerm(ReportingTermId, IsLock);
        }
        public async Task<bool> SaveReportingSubTermDetail(ReportingSubTermModel reportingSubTermModel)
        {
            return await _IDivisionRepositorycs.SaveReportingSubTermDetail(reportingSubTermModel);
        }
        public async Task<bool> LockUnlockSubTerm(long ReportingSubTermId, bool IsLock)
        {
            return await _IDivisionRepositorycs.LockUnlockSubTerm(ReportingSubTermId, IsLock);
        }
    }
}
