﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.API.Repositories;

namespace Phoenix.API.Services
{
    public class BlogCommentService : IBlogCommentService
    {
        private readonly IBlogCommentRepository _blogCommentRepository;

        public BlogCommentService(IBlogCommentRepository blogCommentRepository)
        {
            _blogCommentRepository = blogCommentRepository;
        }

        public void DeleteAsync(int id)
        {
            _blogCommentRepository.DeleteAsync(id);
        }

        public void DeleteBlogComment(int id, long userId)
        {
            _blogCommentRepository.DeleteBlogComment(id, userId);
        }

        public bool DeleteCommentLike(int id)
        {
            return _blogCommentRepository.DeleteCommentLike(id);
        }

        public Task<IEnumerable<BlogComment>> GetAllAsync()
        {
            return _blogCommentRepository.GetAllAsync();
        }

        public Task<BlogComment> GetAsync(int id)
        {
            return _blogCommentRepository.GetAsync(id);
        }

        public Task<IEnumerable<BlogComment>> GetBlogCommentByBlogId(int blogId, bool? isPublish)
        {
            return _blogCommentRepository.GetBlogCommentByBlogId(blogId, isPublish);
        }

        public Task<IEnumerable<BlogComment>> GetBlogCommentByPublishBy(int publishId, bool? isPublish)
        {
            return _blogCommentRepository.GetBlogCommentByPublishBy(publishId, isPublish);
        }

        public Task<IEnumerable<BlogCommentLike>> GetBlogCommentLikes(int? commentId, int? blogCommentLikeId, int? likedBy, bool? isLike)
        {
            return _blogCommentRepository.GetBlogCommentLikes(commentId, blogCommentLikeId, likedBy, isLike);
        }

        public int Insert(BlogComment entity)
        {
            return _blogCommentRepository.Insert(entity);
        }

        public bool InsertCommentLike(BlogCommentLike blogCommentLike)
        {
            return _blogCommentRepository.InsertCommentLike(blogCommentLike);
        }

        public void UpdateAsync(BlogComment entityToUpdate)
        {
            _blogCommentRepository.UpdateAsync(entityToUpdate);
        }

        public bool UpdatedCommentLike(BlogCommentLike blogCommentLike)
        {
            return _blogCommentRepository.UpdatedCommentLike(blogCommentLike);
        }
    }
}
