﻿using Phoenix.API.Repositories;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Phoenix.API.Services
{
    public class BlogShareService : IBlogShareService
    {
        private readonly IBlogShareRepository _blogShareRepository;
        public BlogShareService(IBlogShareRepository blogShareRepository)
        {
            _blogShareRepository = blogShareRepository;
        }
        public void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ShareBlogs>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Task<ShareBlogs> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public int InsertAsync(ShareBlogs entity)
        {
            throw new NotImplementedException();
        }

        public void ShareBlogs(ShareBlogs entity)
        {
            _blogShareRepository.InsertAsync(entity);
        }

        public void UpdateAsync(ShareBlogs entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
