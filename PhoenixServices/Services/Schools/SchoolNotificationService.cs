﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public class SchoolNotificationService : ISchoolNotificationService
    {
        private readonly ISchoolNotificationRepository _schoolNotificationRepository;
        public SchoolNotificationService(ISchoolNotificationRepository schoolNotificationRepository)
        {
            _schoolNotificationRepository = schoolNotificationRepository;
        }
        public async Task<IEnumerable<DeploymentNotification>> GetSchoolDeploymentNotificationList()
        {
            return await _schoolNotificationRepository.GetSchoolDeploymentNotificationList();
        }

        public async Task<bool> SaveDeploymentNotificationData(DeploymentNotification model)
        {
            return await _schoolNotificationRepository.SaveDeploymentNotificationData(model);
        }
    }
}
