﻿using Phoenix.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Phoenix.Common.Helpers;

namespace Phoenix.API.Services
{
    public class SchoolService : ISchoolService
    {
        private readonly ISchoolRepository _schoolRepository;
        private readonly IConfiguration _config;

        public SchoolService(ISchoolRepository schoolRepository, IConfiguration config)
        {
            _schoolRepository = schoolRepository;
            _config = config;
        }

        public async Task<IEnumerable<SchoolInformation>> GetSchoolList()
        {
            return await _schoolRepository.GetSchoolList();
        }
        public async Task<IEnumerable<SchoolInformation>> GetAdminSchoolList()
        {
            return await _schoolRepository.GetAdminSchoolList();
        }

        public async Task<SchoolInformation> GetSchoolById(int id)
        {
            return await _schoolRepository.GetAsync(id);
        }

        public async Task<FileDownload> GetModuleFileDetails(int id, string filetype)
        {
            return await _schoolRepository.GetModuleFileDetails(id, filetype);
        }

        public async Task<ReportDashboard> GetReportDashboard(long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId)
        {
            return await _schoolRepository.GetReportDashboard(schoolId, startDate, endDate, userId, groupId);
        }

        public async Task<AssignmentReportCard> GetAssignmentReportData(long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId)
        {
            return await _schoolRepository.GetAssignmentReportData(schoolId, startDate, endDate, userId, groupId);
        }

        public async Task<IEnumerable<ChildQuizReportData>> GetChildQuizReportData(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            return await _schoolRepository.GetChildQuizReportData(schoolId, startDate, endDate, userId);
        }

        public async Task<IEnumerable<TeacherList>> GetSchoolTeachersBySchoolId(long schoolId)
        {
            return await _schoolRepository.GetSchoolTeachersBySchoolId(schoolId);
        }

        public async Task<IEnumerable<QuizReportData>> GetQuizReportDataWithDateRange(long? userId, long? schoolId, DateTime startDate, DateTime endDate)
        {
            return await _schoolRepository.GetQuizReportDataWithDateRange(userId, schoolId, startDate, endDate);
        }

        public async Task<GetReportDetail> GetReportDetails(DateTime startDate, DateTime endDate)
        {
            return await _schoolRepository.GetReportDetails(startDate, endDate);
        }

        public async Task<IEnumerable<SchoolInformation>> GetAllBusinesUnitSchools(int pageIndex, int pageSize, string searchString) => await _schoolRepository.GetAllBusinesUnitSchools(pageIndex, pageSize, searchString);
        public async Task<bool> UpdateSchoolClassroomStatus(SchoolInformation schoolInformation) => await _schoolRepository.UpdateSchoolClassroomStatus(schoolInformation);

        public async Task<IEnumerable<TableData>> GetSchoolGroupAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            return await _schoolRepository.GetSchoolGroupAssignmentReport(schoolId, startDate, endDate, userId);
        }
        public async Task<IEnumerable<ChatterTableData>> GetGroupChatterReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            return await _schoolRepository.GetGroupChatterReport(schoolId, startDate, endDate, userId);
        }
        public async Task<IEnumerable<TeacherAssignment>> GetTeacherAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            return await _schoolRepository.GetTeacherAssignmentReport(schoolId, startDate, endDate, userId);
        }
        public async Task<IEnumerable<StudentAssignment>> GetStudentAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            return await _schoolRepository.GetStudentAssignmentReport(schoolId, startDate, endDate, userId);
        }
        public async Task<IEnumerable<Data>> GetSchoolAbuseReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            return await _schoolRepository.GetSchoolAbuseReport(schoolId, startDate, endDate, userId);
        }

        public async Task<LoginDetailData> GetSchoolLoginReport(long userId, long schoolId, string startDate, string endDate)
        => await _schoolRepository.GetSchoolLoginReport(userId, schoolId, startDate, endDate);

        public async Task<SessionConfiguration> GetSchoolSessionStatus(long schoolId)
        => await _schoolRepository.GetSchoolSessionStatus(schoolId);

        public async Task<bool> SaveSchoolSessionStatus(SessionConfiguration model)
        => await _schoolRepository.SaveSchoolSessionStatus(model);


        #region FOR DEPARTMENT
        public async Task<bool> DepartmentCourseCU(Department model)
       => await _schoolRepository.DepartmentCourseCU(model);


        public async Task<IEnumerable<Department>> GetDepartmentCourse(long schoolId, long curriculumId, long? departmentId)
        {
            return await _schoolRepository.GetDepartmentCourse(schoolId, curriculumId, departmentId);
        }
        public async Task<bool> CanDeleteDepartment(long departmentId)
        {
            return await _schoolRepository.CanDeleteDepartment(departmentId);
        }
        #endregion

        #region Web RTC 
        public async Task<WebRTCResponseView> GetWebRTCAuthToken()
        {
            var model = new WebRTCResponseView();
            string userName = _config.GetValue<string>("WebRTC:AuthUser");
            string password = _config.GetValue<string>("WebRTC:AuthPassword");
            string authURL = _config.GetValue<string>("WebRTC:AuthorizationURL");
            string authToken = _config.GetValue<string>("WebRTC:AuthToken");
            using (var http = new HttpClient())
            {
                http.DefaultRequestHeaders.Add("AuthUser", userName);
                http.DefaultRequestHeaders.Add("AuthPassword", password);
                http.DefaultRequestHeaders.Add("AuthToken", authToken);
                var response = http.GetAsync(authURL).Result;
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                model = EntityMapper<string, WebRTCResponseView>.MapFromJson(jsonDataProviders);
                return model;
            }
        }

        public async Task<string> AddWebRTCEventUsers(int id, string title, IEnumerable<GroupMemberMapping> lst, string authToken)
        {
            var usrList = new List<dynamic>();
            lst.ToList().ForEach(x => usrList.Add(new { email = x.Email, host = x.IsHost }));
            var url = _config.GetValue<string>("WebRTC:EventUserURL");
            string userName = _config.GetValue<string>("WebRTC:AuthUser");
            string password = _config.GetValue<string>("WebRTC:AuthPassword");
            using (var http = new HttpClient())
            {
                http.DefaultRequestHeaders.Add("AuthUser", userName);
                http.DefaultRequestHeaders.Add("AuthPassword", password);
                http.DefaultRequestHeaders.Add("AuthToken", authToken);
                var response = http.PostAsJsonAsync(url, new { groupid = id, groupname = title, useremail  = usrList }).Result;
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                return jsonDataProviders;
            }
        }

        public async Task<IEnumerable<Student>> GetStudentVaultReportData(long schoolId, long teacherId) => await _schoolRepository.GetStudentVaultReportData(schoolId, teacherId);
        #endregion
    }
}
