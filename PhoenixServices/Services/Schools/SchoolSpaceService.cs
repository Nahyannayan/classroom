﻿using Phoenix.API.Repositories;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class SchoolSpaceService : ISchoolSpaceService
    {
        private readonly ISchoolSpaceRepository _schoolSpaceRepository;
        public SchoolSpaceService(ISchoolSpaceRepository schoolSpaceRepository)
        {
            _schoolSpaceRepository = schoolSpaceRepository;
        }

        public bool DeleteSchoolSpace(int id)
        {
            SchoolSpace schoolSpace = new SchoolSpace(id);
            return _schoolSpaceRepository.UpdateSchoolSpaceData(schoolSpace, TransactionModes.Delete);
        }

        public Task<IEnumerable<SchoolSpace>> GetSchoolSpacesBySchoolId(int schoolId)
        {
            return _schoolSpaceRepository.GetSchoolSpaceBySchoolId(schoolId);
        }

        public Task<SchoolSpace> GetSchoolSpaceById(int id)
        {
            return _schoolSpaceRepository.GetAsync(id);
        }

        public Task<IEnumerable<SchoolSpace>> GetSchoolSpaces()
        {
            return _schoolSpaceRepository.GetAllAsync();
        }

        public bool InsertSchoolSpace(SchoolSpace entity)
        {
            return _schoolSpaceRepository.UpdateSchoolSpaceData(entity, TransactionModes.Insert);
        }

        public bool UpdateSchoolSpace(SchoolSpace entity)
        {
            return _schoolSpaceRepository.UpdateSchoolSpaceData(entity, TransactionModes.Update);
        }
    }
}
