﻿using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class ThemesService : IThemesService
    {
        private readonly IThemesRepository _themesRepository;
        public ThemesService(IThemesRepository themesRepository)
        {
            _themesRepository = themesRepository;
        }

        public async Task<IEnumerable<Themes>> GetAllSchoolThemesById(int? id = null, short languageId=0)
        {
            return await _themesRepository.GetAllSchoolThemesById(id, languageId);
        }
        public bool InsertSchoolTheme(Themes model)
        {
            return _themesRepository.SchoolThemeCUD(model, TransactionModes.Insert);
        }

        public bool UpdateSchoolTheme(Themes model)
        {
            return _themesRepository.SchoolThemeCUD(model, TransactionModes.Update);
        }
        public Task<Themes> GetSchoolThemeById(int themeId, short languageId)
        {
            return _themesRepository.GetSchoolThemeById(themeId, languageId);
        }

        public async Task<IEnumerable<Themes>> GetAllSchoolThemesByCurriculum(int? schoolId, int? curriculumId, short languageId=0)
        {
            return await _themesRepository.GetAllSchoolThemesByCurriculum(schoolId, curriculumId, languageId);
        }

        public async Task<IEnumerable<Themes>> GetStudentGradeTheme(long userId)
        {
            return await _themesRepository.GetStudentGradeTheme(userId);
        }
    }
}
