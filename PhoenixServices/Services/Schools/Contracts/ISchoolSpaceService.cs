﻿using Phoenix.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ISchoolSpaceService
    {
        Task<IEnumerable<SchoolSpace>> GetSchoolSpaces();
        Task<IEnumerable<SchoolSpace>> GetSchoolSpacesBySchoolId(int schoolId);
        Task<SchoolSpace> GetSchoolSpaceById(int id);
        bool InsertSchoolSpace(SchoolSpace entity);
        bool UpdateSchoolSpace(SchoolSpace entity);
        bool DeleteSchoolSpace(int id);
    }
}
