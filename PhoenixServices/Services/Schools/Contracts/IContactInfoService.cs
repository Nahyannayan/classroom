﻿using Phoenix.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IContactInfoService
    {
        Task<IEnumerable<ContactInfo>> GetContactInfos();
        Task<IEnumerable<ContactInfo>> GetContactInfoBySchoolId(int schoolId);
        Task<ContactInfo> GetContactInfoById(int id);
        bool InsertContactInfo(ContactInfo entity);
        bool UpdateContactInfo(ContactInfo entity);
        bool DeleteContactInfo(int id);
    }
}
