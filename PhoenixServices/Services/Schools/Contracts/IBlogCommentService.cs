﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public interface IBlogCommentService
    {
        Task<IEnumerable<BlogComment>> GetBlogCommentByBlogId(int blogId, bool? isPublish);

        Task<IEnumerable<BlogComment>> GetBlogCommentByPublishBy(int publishId, bool? isPublish);
        Task<IEnumerable<BlogComment>> GetAllAsync();
        Task<BlogComment> GetAsync(int id);
        int Insert(BlogComment entity);
        void DeleteAsync(int id);
        
        void UpdateAsync(BlogComment entityToUpdate);

        bool InsertCommentLike(BlogCommentLike blogCommentLike);
        bool UpdatedCommentLike(BlogCommentLike blogCommentLike);
        bool DeleteCommentLike(int id);
        Task<IEnumerable<BlogCommentLike>> GetBlogCommentLikes(int? commentId, int? blogCommentLikeId, int? likedBy, bool? isLike);
        void DeleteBlogComment(int id, long userId);
    }
}
