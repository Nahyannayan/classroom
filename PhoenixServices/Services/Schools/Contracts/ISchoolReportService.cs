﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ISchoolReportService
    {
        void DeleteAsync(int id, long schoolId);
        Task<int> SchoolReportCU(SchoolReports schoolReports);
        Task<IEnumerable<ReportListModel>> GetReportLists(long schoolId);
        Task<ReportDetailModel> GetReportDetail(long reportDetailId, bool isPreview, bool isCommonReport, long schoolId);
        Task<IEnumerable<ReportFilterModel>> GetReportFilter(string filterCode, string filterParameter);
        Task<ReportDataModel> GetReportData(ReportParameterRequestModel requestModel);
    }
}
