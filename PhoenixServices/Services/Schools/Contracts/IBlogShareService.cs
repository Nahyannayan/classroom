﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IBlogShareService
    {
        Task<IEnumerable<ShareBlogs>> GetAllAsync();
        Task<ShareBlogs> GetAsync(int id);
        int InsertAsync(ShareBlogs entity);
        void DeleteAsync(int id);
        void UpdateAsync(ShareBlogs entityToUpdate);
        void ShareBlogs(ShareBlogs entity);
    }
}
