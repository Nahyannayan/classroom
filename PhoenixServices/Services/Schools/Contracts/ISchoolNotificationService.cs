﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;

namespace Phoenix.API.Services
{
    public interface ISchoolNotificationService
    {
        Task<IEnumerable<DeploymentNotification>> GetSchoolDeploymentNotificationList();
        Task<bool> SaveDeploymentNotificationData(DeploymentNotification model);
    }
}
