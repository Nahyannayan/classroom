﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public interface IBlogService
    {
        Task<IEnumerable<Blog>> GetBlogsBySchoolId(int schoolId, bool? isPublish);
        Task<IEnumerable<Blog>> GetBlogsByCategoryId(int schoolId, int categoryId, bool? isPublish);
        Task<IEnumerable<Blog>> GetAllAsync();
        Task<Blog> GetAsync(int id);
        int InsertAsync(Blog entity);
        int InsertMapAsync(SchoolGroup_StudentMapping mapModal);

        int InsertSharePost(sharepost shhareModal);

        int Insert(Blog entity);

        int DeleteExemplar(Blog entity);
        void DeleteAsync(int id);
        bool UpdateDiscloseDate(int id);
        void UpdateAsync(Blog entityToUpdate);
        Task<IEnumerable<BlogCategory>> GetBlogCategories(long? schoolId, short languageId);
        Task<IEnumerable<Blog>> GetTeacherBlogs(int schoolId, int pageNumber, int pageSize);
        void PublishBlog(int blogId, bool isPublish, int publishBy);
        Task<IEnumerable<Chatter>> GetChattersWithComment(int? blogId, int schoolId, int? categoryId, int groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish);

        Task<IEnumerable<Blog>> GetBlogsByBlogTypeId(int blogTypeId, bool? isPublish);
        Task<IEnumerable<Blog>> GetBlogsByGroupId(int groupId, int schoolId, int? userId, bool? isPublish);
        Task<IEnumerable<Blog>> GetBlogsByGroupNBlogtypeId(int groupId, int blogTypeId, bool? isPublish);

        bool InsertLike(BlogLike blogLike);
        bool UpdatedLike(BlogLike blogLike);
        bool DeleteLike(int id);
        Task<IEnumerable<BlogLike>> GetBlogLikes(int? blogId, int? blogLikeId, int? likedBy, bool? isLike);
        Task<IEnumerable<BlogTypes>> GetBlogTypes(long? schoolId, short languageId);
        bool UpdateWallSortOrder(DataTable dataTable, long UpdatedBy);
        Task<IEnumerable<Blog>> GetUserExemplerWallData(long userId, int pageSize, int pageIndex);
        Task<IEnumerable<SearchMyWall>> GetSearchOnMyWall(long schoolId, string searchString, long userId, bool isTeacher);
        Task<IEnumerable<SearchMyWall>> GetSearchBlogsOnMyGroup(long schoolId, string searchString, long userId, bool isTeacher,long groupId);

        Task<IEnumerable<Chatter>> GetMyWallDataOnSearch(long? blogId, long schoolId, int? categoryId, int? blogTypeId, long? groupId, long? userId, bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish,int? LogedInUserId, int? pageNum);
        Task<IEnumerable<Chatter>> GetMyWallData(int? blogId, int schoolId, int? categoryId, int? blogTypeId, int? groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish,int? LogedInUserId, int? pageNum);
        Task<IEnumerable<BlogStudentDash>> GetStudentDashBlogs(long userId, int schoolId);
        Task<IEnumerable<BlogTeacherDash>> GetTeacherDashBlogs(long userId, int schoolId);
        Task<IEnumerable<PostAndCommentLikeUserList>> GetPostAndCommentLikeUserList(long BlogId, long CommentSectioId, long UserId, bool IsCommentDetailDisplay);

        Task<IEnumerable<BlogCompleteData>> GetMyChatterData(long? blogId, long schoolId, int? categoryId, int? blogTypeId, long? groupId, long? userId,
       bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish, int? LogedInUserId, int? pageNum, bool EnableChat);
        void DeleteBlog(int id, long userId);
    }
}
