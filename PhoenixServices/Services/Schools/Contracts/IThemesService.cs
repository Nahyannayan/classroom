﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IThemesService
    {
        Task<IEnumerable<Themes>> GetAllSchoolThemesById(int? id = null, short languageId=0);
        Task<IEnumerable<Themes>> GetAllSchoolThemesByCurriculum(int? schoolId, int? curriculumId, short languageId);
        bool InsertSchoolTheme(Themes model);
        bool UpdateSchoolTheme(Themes model);
        Task<Themes> GetSchoolThemeById(int themeId, short languageId);
        Task<IEnumerable<Themes>> GetStudentGradeTheme(long userId);
    }
}
