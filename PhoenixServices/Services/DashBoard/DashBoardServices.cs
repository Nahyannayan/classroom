﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models.DashBoard;

namespace Phoenix.API.Services
{
    public class DashBoardServices : IDashBoardServices
    {

        private readonly IDashBoardGroupsRepository _dashBoardGroupeRepository;
        private readonly ITeachersDashboardBannerRepository _dashBoardBanners;

        public DashBoardServices(IDashBoardGroupsRepository dashBoardGroupeRepository, ITeachersDashboardBannerRepository dashBoardBanners)
        {
            _dashBoardGroupeRepository = dashBoardGroupeRepository;
            _dashBoardBanners = dashBoardBanners;
        }

        public async Task<IEnumerable<DashBoardGroups>> getOtherGroups(int id)
        {
           var result=await _dashBoardGroupeRepository.getOtherGroups(id);
            return result;
        }

        public async Task<IEnumerable<DashBoardBanners>> getSchoolBanners(int id)
        {
            var result = await _dashBoardBanners.GetTeacherDashboardBanners(id);
            return result;
        }

        public async Task<IEnumerable<DashBoardGroups>> getSchoolGroups(int id)
        {
            var result = await _dashBoardGroupeRepository.getSchoolGroups(id);
            return result;
        }
    }
}
