﻿using Phoenix.API.Models.DashBoard;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
   public interface IDashBoardServices
    {
        Task<IEnumerable<DashBoardBanners>> getSchoolBanners(int id);
        Task<IEnumerable<DashBoardGroups>> getSchoolGroups(int id);
        Task<IEnumerable<DashBoardGroups>> getOtherGroups(int id);


    }
}
