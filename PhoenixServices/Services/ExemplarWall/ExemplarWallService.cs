﻿using Phoenix.API.Repositories.ExemplarWall.Contracts;
using Phoenix.API.Services.ExemplarWall.Contracts;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;

namespace Phoenix.API.Services.ExemplarWall
{
    public class ExemplarWallService : IExemplarWallService
    {

        private readonly IExemplarWallRepository _exemplarWallRepository;
        public ExemplarWallService(IExemplarWallRepository exemplarWallRepository)
        {
            _exemplarWallRepository = exemplarWallRepository;
        }

        public bool AddUpdateExemplarWall(ExemplarWallModel entity, TransactionModes mode)
        {
            return _exemplarWallRepository.AddUpdateExemplarWall(entity, mode);
        }
        public bool ShareExemplarWall(sharepost entity, TransactionModes mode)
        {
            return _exemplarWallRepository.ShareExemplarWall(entity, mode);
        }

        public bool DeleteExemplarWall(ExemplarWallModel entity)
        {
            return _exemplarWallRepository.DeleteExemplarWall(entity);
        }

        public Task<ExemplarWallModel> GetExemplarWall(long ExemplarWallId)
        {
            return _exemplarWallRepository.GetExemplarWall(ExemplarWallId);
        }

        public Task<IEnumerable<ExemplarWallModel>> GetExemplarWallDetailBySchoolId(long SchoolId)
        {
            return _exemplarWallRepository.GetExemplarWallDetailBySchoolId(SchoolId);
        }

        public Task<IEnumerable<ExemplarUserDetails>> GetBlogsByGroupNBlogtypeId(int SchoolLevel, int Department, int CourseId, int SchoolId, int groupId, int blogTypeId,
            int? isPublish, Int64 UserId, Int64 CreatedBy, string SearchText, bool MyExemplarWork)
        {
            return _exemplarWallRepository.GetBlogsByGroupNBlogtypeId(SchoolLevel, Department, CourseId, SchoolId, groupId, blogTypeId, isPublish, UserId, CreatedBy, SearchText, MyExemplarWork);
        }

        public Task<IEnumerable<SchoolLevel>> GetSchoolLevelBySchoolId(long schoolId, short languageId)
        {
            return _exemplarWallRepository.GetSchoolLevelBySchoolId(schoolId, languageId);
        }
        public Task<IEnumerable<SchoolDepartment>> GetSchoolDepartmentList(long schoolId, short languageId)
        {
            return _exemplarWallRepository.GetSchoolDepartmentList(schoolId, languageId);
        }
        public Task<IEnumerable<SchoolGroup>> GetSchoolGroupBasedOnSchoolLevel(long LevelId, long UserId, long CourseId, long SchoolId)
        {
            return _exemplarWallRepository.GetSchoolGroupBasedOnSchoolLevel(LevelId, UserId, CourseId, SchoolId);
        }
        public Task<IEnumerable<ExemplarWallModel>> GetPendingForApprovalExemplarPost(long SchoolId)
        {
            return _exemplarWallRepository.GetPendingForApprovalExemplarPost(SchoolId);
        }
        public Task<int> UpdateExemplarPostStatusFromPendingToApprove(long ExemplarPostId)
        {
            return _exemplarWallRepository.UpdateExemplarPostStatusFromPendingToApprove(ExemplarPostId);
        }

        public async Task<bool> GetApprovalStatusDetails(long SchoolId, string Opearion, string FlagType, bool Value)
        {
            return await _exemplarWallRepository.GetApprovalStatusDetails(SchoolId, Opearion, FlagType, Value);
        }

        public async Task<IEnumerable<Phoenix.Models.Course>> GetCourseByDepartment(long DepartmentId)
        {
            return await _exemplarWallRepository.GetCourseByDepartment(DepartmentId);
        }
    }
}
