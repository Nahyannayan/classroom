﻿using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services.ExemplarWall.Contracts
{
    public interface IExemplarWallService
    {
        Task<ExemplarWallModel> GetExemplarWall(long ExemplarWallId);
        bool AddUpdateExemplarWall(ExemplarWallModel entity, TransactionModes mode);
        bool ShareExemplarWall(sharepost entity, TransactionModes mode);
        bool DeleteExemplarWall(ExemplarWallModel entity);
        Task<IEnumerable<ExemplarWallModel>> GetExemplarWallDetailBySchoolId(long SchoolId);
        Task<IEnumerable<ExemplarUserDetails>> GetBlogsByGroupNBlogtypeId(int SchoolLevel, int Department, int CourseId, int SchoolId, int groupId, int blogTypeId, 
            int? isPublish, Int64 UserId, Int64 CreatedBy, string SearchText,bool MyExemplarWork);
        Task<IEnumerable<SchoolLevel>> GetSchoolLevelBySchoolId(long schoolId, short languageId);
        Task<IEnumerable<SchoolDepartment>> GetSchoolDepartmentList(long schoolId, short languageId);

        Task<IEnumerable<SchoolGroup>> GetSchoolGroupBasedOnSchoolLevel(long LevelId, long UserId, long CourseId, long SchoolId);

        Task<IEnumerable<ExemplarWallModel>> GetPendingForApprovalExemplarPost(long SchoolId);
        Task<int> UpdateExemplarPostStatusFromPendingToApprove(long ExemplarPostId);

       Task<bool> GetApprovalStatusDetails(long SchoolId, string Opearion, string FlagType, bool Value);
        Task<IEnumerable<Phoenix.Models.Course>> GetCourseByDepartment(long DepartmentId);

    }
}
