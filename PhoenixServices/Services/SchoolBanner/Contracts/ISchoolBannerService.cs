﻿using Phoenix.API.Models;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ISchoolBannerService
    {
        Task<IEnumerable<SchoolBanner>> GetSchoolBannersByPage(int pageNumber, int pageSize, string searchString, int userId, int schoolId);
        Task<IEnumerable<SchoolBanner>> GetSchoolBanners(long userId, int schoolId);
        OperationDetails InsertBannerData(SchoolBanner entity);
        OperationDetails UpdateBannerData(SchoolBanner entity, TransactionModes mode);
        Task<IEnumerable<string>> GetBannerSchoolIds(long bannerId);
        Task<SchoolBanner> GetSchoolBannerDetails(long schoolBannerId,long userId);
        int GetTopOrderForDisplayBanner(long bannerId);
        Task<IEnumerable<SchoolBanner>> GetUserBanners(long userId);
    }
}
