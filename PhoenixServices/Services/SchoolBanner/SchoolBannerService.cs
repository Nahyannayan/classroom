﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class SchoolBannerService : ISchoolBannerService
    {
        private readonly ISchoolBannerRepository _schoolBannerRepository;

        public SchoolBannerService(ISchoolBannerRepository schoolbannerRepository)
        {
            _schoolBannerRepository = schoolbannerRepository;
        }

        public async Task<IEnumerable<SchoolBanner>> GetSchoolBannersByPage(int pageNumber, int pageSize, string searchString, int userId, int schoolId)
        {
            return await _schoolBannerRepository.GetSchoolBannersByPage(pageNumber,pageSize,searchString,userId,schoolId);
        }
        public async Task<IEnumerable<SchoolBanner>> GetSchoolBanners(long userId, int schoolId)
        {
            return await _schoolBannerRepository.GetSchoolBanners(userId, schoolId);
        }

        public OperationDetails InsertBannerData(SchoolBanner entity)
        {
            return _schoolBannerRepository.InsertBannerData(entity);
        }
        public OperationDetails UpdateBannerData(SchoolBanner entity, TransactionModes mode)
        {
            return _schoolBannerRepository.UpdateBannerData(entity, mode);
        }
        public async Task<IEnumerable<string>> GetBannerSchoolIds(long bannerId)
        {
            return await _schoolBannerRepository.GetBannerSchoolIds(bannerId);
        }

        public async Task<SchoolBanner> GetSchoolBannerDetails(long schoolBannerId, long userId)
        {
            return await _schoolBannerRepository.GetSchoolBannerDetails(schoolBannerId, userId);
        }
        public int GetTopOrderForDisplayBanner(long userId)
        {
            return  _schoolBannerRepository.GetTopOrderForDisplayBanner(userId);
        }
        public async Task<IEnumerable<SchoolBanner>> GetUserBanners(long userId)
        {
            return await _schoolBannerRepository.GetUserBanners(userId);
        }
    }
}