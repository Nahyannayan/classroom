﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Services
{
    public class SchoolBadgeService : ISchoolBadgeService
    {
        private readonly ISchoolBadgeRepository _schoolBadgeRepository;

        public SchoolBadgeService(ISchoolBadgeRepository schoolBadgeRepository)
        {
            _schoolBadgeRepository = schoolBadgeRepository;
        }

        public async Task<IEnumerable<SchoolBadge>> GetSchoolBadgesByPage(int pageNumber, int pageSize, string searchString, int userId, int schoolId)
        {
            return await _schoolBadgeRepository.GetSchoolBadgesByPage(pageNumber, pageSize, searchString, userId, schoolId);
        }
        public async Task<IEnumerable<SchoolBadge>> GetSchoolBadges(long userId, int schoolId)
        {
            return await _schoolBadgeRepository.GetSchoolBadges(userId, schoolId);
        }

        public OperationDetails InsertBadgeData(SchoolBadge entity)
        {
            return _schoolBadgeRepository.InsertBadgeData(entity);
        }
        public OperationDetails UpdateBadgeData(SchoolBadge entity, TransactionModes mode)
        {
            return _schoolBadgeRepository.UpdateBadgeData(entity, mode);
        }
        public async Task<IEnumerable<string>> GetBadgeSchoolIds(long BadgeId)
        {
            return await _schoolBadgeRepository.GetBadgeSchoolIds(BadgeId);
        }

        public async Task<SchoolBadge> GetSchoolBadgeDetails(long schoolBadgeId, long userId)
        {
            return await _schoolBadgeRepository.GetSchoolBadgeDetails(schoolBadgeId, userId);
        }
        public int GetTopOrderForDisplayBadge(long userId)
        {
            return _schoolBadgeRepository.GetTopOrderForDisplayBadge(userId);
        }
        public async Task<IEnumerable<SchoolBadge>> GetUserBadges(long userId)
        {
            return await _schoolBadgeRepository.GetUserBadges(userId);
        }

        public async Task<bool> SaveStudentBadges(SchoolBadge model)
        {
            return await _schoolBadgeRepository.SaveStudentBadges(model);
        }

        public async Task<IEnumerable<SchoolBadge>> GetBadgesBySchoolId(int schoolId)
        {
            return await _schoolBadgeRepository.GetBadgesBySchoolId(schoolId);
        }

        public async Task<bool> SaveGroupWiseStudentBadges(StudentBadge studentBadge)
        {
            return await _schoolBadgeRepository.SaveGroupWiseStudentBadges(studentBadge);
        }
    }
}