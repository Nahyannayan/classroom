﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services.Setting.Contract
{
    public interface ISettingService
    {
        Task<IEnumerable<NotificationTrigger>> GetEnableDisableNotificationListForParent(long parentId);

        int SaveNotificationSettingForParent(long parentId, NotificationSettings model);
    }
}
