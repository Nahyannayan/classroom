﻿using Phoenix.API.Repositories.Setting;
using Phoenix.API.Repositories.Setting.Contract;
using Phoenix.API.Services.Setting.Contract;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services.Setting
{
    public class SettingService : ISettingService
    {
        public ISettingRepository _settingRepository;
        public SettingService(ISettingRepository _SettingRepository)
        {
            _settingRepository = _SettingRepository;
        }
        public Task<IEnumerable<NotificationTrigger>> GetEnableDisableNotificationListForParent(long parentId) {
            return _settingRepository.GetEnableDisableNotificationListForParent( parentId);
        }

        public int SaveNotificationSettingForParent(long parentId, NotificationSettings model)
        {
            return _settingRepository.SaveNotificationSettingForParent(parentId, model);
        }
    }
}
