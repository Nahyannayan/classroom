﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Phoenix.API.Repositories;
using Phoenix.Common.ViewModels;

namespace Phoenix.API.Services
{
    public class EmailSettingsService : IEmailSettingsService
    {
        private readonly IEmailSettingsRepository _emailSettingsRepository;
        private readonly IHostingEnvironment _env;
        public EmailSettingsService(IEmailSettingsRepository emailSettingsRepository, IHostingEnvironment env)
        {
            _emailSettingsRepository = emailSettingsRepository;
            _env = env;
        }
        public Task<EmailSettingsView> GetEmailSettings()
        {
            return _emailSettingsRepository.GetEmailSettings();
        }

        public async Task<bool> SendEmail(SendMailView sendMail)
        {
            try
            {
                var webRoot = _env.WebRootPath;
                var emailHtmlTempate = System.IO.File.ReadAllText(webRoot + "/templates/RequestDemo.html");
                emailHtmlTempate = emailHtmlTempate.Replace("{MESSAGE}", sendMail.MSG);
                emailHtmlTempate = emailHtmlTempate.Replace("{ORGANIZATION}", sendMail.Organization);
                emailHtmlTempate = emailHtmlTempate.Replace("{EMAILADDRESS}", sendMail.FROM_EMAIL);
                emailHtmlTempate = emailHtmlTempate.Replace("{NAME}", sendMail.SenderName);
                emailHtmlTempate = emailHtmlTempate.Replace("{CONTACTNUMBER}", sendMail.ContactNumber);
                var _emailSettings = await _emailSettingsRepository.GetEmailSettings();
                var _smtp = new SmtpClient();
                MailMessage message = new MailMessage();
                message.From = new MailAddress(_emailSettings.SenderEmail, _emailSettings.SenderName);
                message.To.Add("support@phoenixclassroom.com");
                message.Subject = "Lead - Phoenix Classroom";
                message.Body = emailHtmlTempate;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.Normal;
                _smtp.Credentials = new System.Net.NetworkCredential(_emailSettings.SenderEmail, _emailSettings.SenderPassword);
                _smtp.Port = Convert.ToInt32(_emailSettings.PortNo);
                _smtp.Host = _emailSettings.SMTPClient;
                _smtp.EnableSsl = _emailSettings.IsSSLEnabled; ;
                _smtp.Send(message);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }
    }
}
