﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.API.Repositories;
using SIMS.API.Repositories;
namespace Phoenix.API.Services
{
    public class CurriculumRoleService : ICurriculumRoleService
    {
        private ICurriculumRoleRepository _curriculumRoleRepository;

        public CurriculumRoleService(ICurriculumRoleRepository curriculumRoleRepository)
        {
            _curriculumRoleRepository = curriculumRoleRepository;

        }

        public Task<CurriculumRole> GetUserCurriculumRole(string BSU_ID = "", string UserName = "")
        {
            return _curriculumRoleRepository.GetUserCurriculumRole(BSU_ID,UserName);
        }

        public async Task<IEnumerable<Curriculum>> GetCurriculum(long BSU_ID, int? CLM_ID)
        {
            return await _curriculumRoleRepository.GetCurriculum(BSU_ID, CLM_ID);
        }
        public Task<IEnumerable<CourseDetails>> GetTeacherCourse(long id, long Schoolid, long curriculumId)
        {
            return _curriculumRoleRepository.GetTeacherCourse(id, Schoolid, curriculumId);
        }
        public Task<IEnumerable<GradeTemplateMaster>> GradeTemplateMaster(long acd_id)
        {
            return _curriculumRoleRepository.GradeTemplateMaster(acd_id);
        }

        
    }
}
