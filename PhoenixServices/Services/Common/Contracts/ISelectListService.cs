﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ISelectListService
    {
        Task<IEnumerable<ListItem>> GetSelectListItemsAsync(string listCode, string whereCondition, string whereConditionParamValues, short languageId);
        Task<IEnumerable<SubjectListItem>> GetSubjectListByUserId(int languageId, int userId);
        Task<IEnumerable<AcademicYearList>> GetAcademicYearListItems(int userId);
        Task<IEnumerable<ListItem>> GetAssignmentCategoryMasterList();
        Task<IEnumerable<AssignmentCategory>> GetAssignmentCategoryMappingListBySchoolId(long SchoolId);
    }
}
