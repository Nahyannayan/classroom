﻿using Phoenix.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ILanguageSettingsService
    {
        Task<SystemLanguage> GetSchoolCurrentLanguage(int SchoolId);
        bool SetSchoolCurrentLanguage(int languageId, int SchoolId);
        bool SetUserCurrentLanguage(int languageId, long userId);
        Task<SystemLanguage> GetUserCurrentLanguage(int languageId);
        Task<IEnumerable<SystemLanguage>> GetSystemLanguages();
    }
}
