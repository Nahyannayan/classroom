﻿
using Phoenix.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ICurriculumRoleService
    {
        Task<CurriculumRole> GetUserCurriculumRole(string BSU_ID = "", string UserName = "");
        Task<IEnumerable<Curriculum>> GetCurriculum(long BSU_ID, int? CLM_ID);

        Task<IEnumerable<CourseDetails>> GetTeacherCourse(long id, long Schoolid, long curriculumId);
        Task<IEnumerable<GradeTemplateMaster>> GradeTemplateMaster(long acd_id);
        
        

    }
}
