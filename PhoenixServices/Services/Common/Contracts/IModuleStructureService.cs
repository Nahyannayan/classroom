﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IModuleStructureService
    {
        Task<IEnumerable<ModuleStructure>> GetPhoenixModuleStructure(int systemLanguageId,
            long userId, string applicationCode, string traverseDirection, string moduleUrl, string moduleCode, bool excludeParent,
            string excludeModuleCodes, bool? showInMenu);
    }
}
