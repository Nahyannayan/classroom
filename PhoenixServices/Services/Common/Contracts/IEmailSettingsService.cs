﻿using Phoenix.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IEmailSettingsService
    {
        Task<EmailSettingsView> GetEmailSettings();
        Task<bool> SendEmail(SendMailView sendMail);
    }
}
