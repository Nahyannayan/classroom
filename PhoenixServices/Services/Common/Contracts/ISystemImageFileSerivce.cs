﻿using Phoenix.Common.Models.ViewModels;
using Phoenix.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface ISystemImageFileSerivce
    {
        Task<IEnumerable<SystemImageViewModel>> GetSystemImageList();
    }
}
