﻿using Phoenix.API.Repositories;
using Phoenix.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class LanguageSettingsService : ILanguageSettingsService
    {
        private readonly ILanguageSettingsRepository _languageSettingsRepository;
        public LanguageSettingsService(ILanguageSettingsRepository languageSettingsRepository)
        {
            _languageSettingsRepository = languageSettingsRepository;
        }
        public Task<SystemLanguage> GetSchoolCurrentLanguage(int SchoolId)
        {
            return _languageSettingsRepository.GetSchoolCurrentLanguage(SchoolId);
        }

        public bool SetSchoolCurrentLanguage(int languageId, int schoolId)
        {
            return _languageSettingsRepository.SetSchoolCurrentLanguage(languageId, schoolId);
        }
        public bool SetUserCurrentLanguage(int languageId, long userId)
        {
            return _languageSettingsRepository.SetUserCurrentLanguage(languageId, userId);
        }
        public Task<SystemLanguage> GetUserCurrentLanguage(int languageId)
        {
            return _languageSettingsRepository.GetUserCurrentLanguage(languageId);
        }
        public Task<IEnumerable<SystemLanguage>> GetSystemLanguages()
        {
            return _languageSettingsRepository.GetSystemLanguages();
        }
    }
}
