﻿using Phoenix.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Models;
using Phoenix.Common.Helpers;

namespace Phoenix.API.Services
{
    public class SelectListService : ISelectListService
    {
        private readonly ISelectListRepository _selectListRepository;

        public SelectListService(ISelectListRepository selectListRepository)
        {
            _selectListRepository = selectListRepository;
        }

        public async Task<IEnumerable<ListItem>> GetSelectListItemsAsync(string listCode, string whereCondition, string whereConditionParamValues, short languageId)
        {
            return await _selectListRepository.GetSelectListItems(listCode, whereCondition, whereConditionParamValues, languageId);
        }
        public async Task<IEnumerable<SubjectListItem>> GetSubjectListByUserId(int languageId, int userId)
        {
            return await _selectListRepository.GetSubjectsListByUserId(languageId, userId);
        }
        public async Task<IEnumerable<AcademicYearList>> GetAcademicYearListItems(int userId)
        {
            return await _selectListRepository.GetAcademicYearByUserId(userId);
        }

        public async Task<IEnumerable<ListItem>> GetAssignmentCategoryMasterList()
        {
            return await _selectListRepository.GetAssignmentCategoryMasterList();
        }

        public async Task<IEnumerable<AssignmentCategory>> GetAssignmentCategoryMappingListBySchoolId(long SchoolId)
        {
            return await _selectListRepository.GetAssignmentCategoryMappingListBySchoolId(SchoolId);
        }
    }
}
