﻿using Phoenix.API.Repositories;
using Phoenix.Common.Models.ViewModels;
using Phoenix.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class SystemImageFileSerivce : ISystemImageFileSerivce
    {
        private readonly ISystemImageFileRepository _systemImageFileRepository;
        public SystemImageFileSerivce(ISystemImageFileRepository systemImageFileRepository)
        {
            _systemImageFileRepository = systemImageFileRepository;
        }
        public async Task<IEnumerable<SystemImageViewModel>> GetSystemImageList()
        {
            return await _systemImageFileRepository.GetSystemImageList();
        }
    }
}
