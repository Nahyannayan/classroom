﻿using Phoenix.Models;
using Phoenix.API.Repositories;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Models;
using Phoenix.Common.Helpers;

namespace Phoenix.API.Services
{
    public class ModuleStructureService : IModuleStructureService
    {
        private readonly IModuleStructureRepository _moduleStructureRepository;

        public ModuleStructureService(IModuleStructureRepository ModuleStructureRepository)
        {
            _moduleStructureRepository = ModuleStructureRepository;
        }

        public async Task<IEnumerable<ModuleStructure>> GetPhoenixModuleStructure(int systemLanguageId,
            long userId, string applicationCode, string traverseDirection, string moduleUrl, string moduleCode, bool excludeParent,
            string excludeModuleCodes, bool? showInMenu)
        {
            return await _moduleStructureRepository.GetPhoenixModuleStructure(systemLanguageId, userId, applicationCode, traverseDirection, moduleUrl, 
                moduleCode, excludeParent, excludeModuleCodes, showInMenu);
        }
    }
}
