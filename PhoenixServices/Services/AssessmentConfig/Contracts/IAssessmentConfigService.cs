﻿using Phoenix.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IAssessmentConfigService
    {
        bool AddEditAssessmentConfig(List<GradeTemplate> obj,string Description, string MasterId, long GTM_Id, long SchoolId);
        bool AddEditGradeSlab(List<GradeSlab> obj, string Description, string MasterIds, long GradeSlabMasterId, long Acd_Id);
        Task<IEnumerable<GradeTemplate>> GetAssessmentConfigList(long Acd_Id);
         Task<IEnumerable<GradeTemplate>> GetAssessmentConfigMasterList(long SchoolId);
        

        Task<IEnumerable<GradeSlab>> GetGradeSlabList(long Acd_Id);

        Task<IEnumerable<GradeSlabMaster>> GetGradeSlabMasterList(long SchoolId);

    }
}
