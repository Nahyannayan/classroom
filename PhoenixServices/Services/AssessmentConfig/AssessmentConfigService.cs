﻿using Phoenix.API.Models;
using Phoenix.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.API.Services
{
    public class AssessmentConfigService : IAssessmentConfigService
    {
        private readonly IAssessmentConfigRepository _IAssessmentConfigRepository;
      
        public AssessmentConfigService(IAssessmentConfigRepository iAssessmentConfigRepository)
        {
            _IAssessmentConfigRepository = iAssessmentConfigRepository;

        }
        public bool AddEditAssessmentConfig(List<GradeTemplate> obj,string Description, string MasterId, long GTM_Id,long SchoolId)
        {
            return _IAssessmentConfigRepository.AddEditAssessmentConfig(obj, Description,  MasterId, GTM_Id, SchoolId);
        }
        public bool AddEditGradeSlab(List<GradeSlab> obj, string Description, string MasterIds, long GradeSlabMasterId, long Acd_Id)
        {
            return _IAssessmentConfigRepository.AddEditGradeSlab(obj, Description, MasterIds, GradeSlabMasterId, Acd_Id);
        }
      
        public async Task<IEnumerable<GradeTemplate>> GetAssessmentConfigList(long Acd_Id)
        {
            return await _IAssessmentConfigRepository.GetAssessmentConfigList(Acd_Id);
        }
        public async Task<IEnumerable<GradeTemplate>> GetAssessmentConfigMasterList(long SchoolId)
        {
            return await _IAssessmentConfigRepository.GetAssessmentConfigMasterList(SchoolId);
        }
        
        public async Task<IEnumerable<GradeSlab>> GetGradeSlabList(long Acd_Id)
        {
            return await _IAssessmentConfigRepository.GetGradeSlabList(Acd_Id);
        }
        public async Task<IEnumerable<GradeSlabMaster>> GetGradeSlabMasterList(long SchoolId)
        {
            return await _IAssessmentConfigRepository.GetGradeSlabMasterList(SchoolId);
        }
       

    }
}