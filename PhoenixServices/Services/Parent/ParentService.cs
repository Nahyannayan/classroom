﻿using Phoenix.API.Repositories;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public class ParentService:IParentService
    {
        private readonly IParentRepository _parentRepository;
        public ParentService(IParentRepository parentRepository)
        {
            _parentRepository = parentRepository;
        }
        public async Task<ParentView> GetStudentDetailsById(long id)
        {
            return await _parentRepository.GetStudentDetailsById(id);
        }
        public async Task<int> AssignAccessPermission(string StudentIdList) {
            return await _parentRepository.AssignAccessPermission(StudentIdList);
        }
    }
}
