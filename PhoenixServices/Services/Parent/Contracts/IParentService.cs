﻿using Phoenix.Models;
using Phoenix.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Services
{
    public interface IParentService
    {
        Task<ParentView> GetStudentDetailsById(long id);

        Task<int> AssignAccessPermission(string StudentIdList);
    }
}
