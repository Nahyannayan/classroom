﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using Phoenix.Common.ViewModels;
using Phoenix.Common.Models;

namespace Phoenix.API.Repositories
{
    public interface IUserRoleRepository: IGenericRepository<UserRole>
    {
        Task<IEnumerable<UserRole>> GetUserRoles();
        Task<IEnumerable<UserRoleMapping>> GetAllUserRoleMappingData(int userid);
        Task<IEnumerable<UserRoleMapping>> GetAssignedUserMappingData(int systemlanguageid, int roleid);
        Task<IEnumerable<ModuleStructure>> GetModuleList(int systemlanguageid, string modulecode);
        Task<IEnumerable<ModuleStructure>> GetUserRolePermissionList();
        Task<IEnumerable<ModuleStructure>> GetRolePhoenixModuleData(int userroleid);
        Task<IEnumerable<ModuleStructure>> GetRoleMappingData(int roleId, int schoolId);
        Task<IEnumerable<ModuleStructure>> GetModuleStructureList(int systemlanguageid, int? ModuleId, string ModuleCode);
        bool UpdateUserRoleData(UserRole entity, TransactionModes mode);
        Task<IEnumerable<PermissionTypeView>> GetAllPermissionData(int userRoleId, int userId, int moduleId, bool loadCustomePermission, int schoolId);
        Task<bool> UpdatePermissionTypeDataCUD(List<CustomPermissionEdit> MappingDetails, string operationtype, int? userId, short userRoleId,int schoolId);
        Task<object> CheckUserRoleMapping(long? userId, short? roleId);
        Task<bool> InsertUserRoleMappingData(long userId, short roleId);
        Task<bool> DeleteUserRoleMappingData(long userId, short roleId);

        Task<IEnumerable<UserRole>> GetUserRolesBySchoolId(int schoolId, short languageId);
        Task<IEnumerable<User>> GetUsersForRole(int roleId, int schoolId);
        Task<UserRole> GetUserRoleById(int id, short languageId);
    }
}
