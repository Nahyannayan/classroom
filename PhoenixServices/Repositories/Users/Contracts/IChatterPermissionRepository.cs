﻿using DbConnection;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IChatterPermissionRepository : IGenericRepository<ChatPermission>
    {
        Task<IEnumerable<UserChatPermission>> GetUserPermissions(int schoolId, int userId);
        Task<bool> UpdateChatterPermission(ChatPermission chats);
        Task<IEnumerable<ChatUsers>> GetUsersInGroup(int groupId);
    }
}
