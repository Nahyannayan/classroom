﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface ILogInUserRepository: IGenericRepository<LogInUser>
    {
        Task<LogInUser> GetLogInUserByUserName(string UserName, string ipDetails);
        Task<IEnumerable<LogInUser>> GetUserList(int schoolId,short languageId);
        Task<LogInUser> GetLogInUserByUserNamePassword(string UserName,string Password, string ipDetails);
    }
}
