﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Phoenix.Common.ViewModels;
using Phoenix.Common.Models;
using Phoenix.Common.DataAccess;

namespace Phoenix.API.Repositories
{
    public class UserRoleRepository : SqlRepository<UserRole>, IUserRoleRepository
    {
        private readonly IConfiguration _config;

        public UserRoleRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<UserRole>> GetUserRoles()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<UserRole>("Admin.GetUserRoleData", CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<UserRole>> GetUserRolesBySchoolId(int schoolId,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserRoleId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<UserRole>("Admin.GetUserRoleData", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public bool UpdateUserRoleData(UserRole entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetUserRolesParameters(entity, mode);
                conn.Query<int>("Admin.UserRoleCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<UserRole> GetUserRoleById(int id,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserRoleId", id, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryFirstOrDefaultAsync<UserRole>("Admin.GetUserRoleData", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        #region Private Methods
        private DynamicParameters GetUserRolesParameters(UserRole entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@UserRoleId", entity.UserRoleId, DbType.Int32);
            parameters.Add("@UserRoleName", entity.UserRoleName, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int32);
            parameters.Add("@UserRoleXml", entity.UserRoleXml, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        #region Generated Methods

        public async override Task<UserRole> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserRoleId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<UserRole>("Admin.GetUserRoleData", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<UserRole>> GetAllAsync()
        {
            throw new NotImplementedException();
        }


        public override void InsertAsync(UserRole entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(UserRole entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<UserRoleMapping>> GetAllUserRoleMappingData(int userid)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userid, DbType.Int32);
                return await conn.QueryAsync<UserRoleMapping>("Admin.GetAllUserRoleMappingData", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<UserRoleMapping>> GetAssignedUserMappingData(int systemlanguageid, int roleid)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SystemLanguageId", systemlanguageid, DbType.Int32);
                parameters.Add("@RoleId", roleid, DbType.Int32);
                return await conn.QueryAsync<UserRoleMapping>("Admin.GetAssignedUserMappingData", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ModuleStructure>> GetModuleList(int systemlanguageid, string modulecode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SystemLanguageId", systemlanguageid, DbType.Int32);
                parameters.Add("@ModuleCode", modulecode, DbType.String);
                return await conn.QueryAsync<ModuleStructure>("Admin.GetModuleList", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ModuleStructure>> GetUserRolePermissionList()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<ModuleStructure>("Admin.GetUserRolePermissionList", CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ModuleStructure>> GetRolePhoenixModuleData(int userroleid)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserRoleId", userroleid, DbType.Int32);
                return await conn.QueryAsync<ModuleStructure>("Admin.GetRolePhoenixModuleData", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ModuleStructure>> GetRoleMappingData(int roleId, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@RoleId", roleId, DbType.Int32);
                parameters.Add("@RoleId", schoolId, DbType.Int32);
                return await conn.QueryAsync<ModuleStructure>("Admin.GetRoleMappingData", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ModuleStructure>> GetModuleStructureList(int systemlanguageid, int? moduleid, string modulecode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SystemLanguageId", systemlanguageid, DbType.Int16);
                parameters.Add("@ModuleId", moduleid, DbType.Int32);
                parameters.Add("@ModuleCode", modulecode, DbType.String);
                return await conn.QueryAsync<ModuleStructure>("Admin.GetModuleStructureList", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<PermissionTypeView>> GetAllPermissionData(int userRoleId, int userId, int moduleId, bool loadCustomePermission, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserRoleId", userRoleId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@ModuleId", moduleId, DbType.Int16);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@LoadCustomPermission", loadCustomePermission, DbType.Boolean);

                return await conn.QueryAsync<PermissionTypeView>("Admin.GetAllPermissionData", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<bool> UpdatePermissionTypeDataCUD(List<CustomPermissionEdit> MappingDetails, string operationtype, int? userId, short userRoleId, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("GrantPermission");
                if (MappingDetails != null)
                {
                    foreach (var item in MappingDetails)
                    {
                        dt.Rows.Add(item.PermissionId, item.GrantPermission);
                    }

                }
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@UserRoleId", userRoleId, DbType.Int16);
                parameters.Add("@OperationType", operationtype, DbType.String);
                parameters.Add("@PermissionTypeData", dt, DbType.Object);
                parameters.Add("@schoolId", schoolId, DbType.Int32);
                parameters.Add("@output", 0, DbType.Int32, ParameterDirection.Output);


                if (userId == null)
                {
                    conn.Query<int>("[Admin].[BatchUpdatePermissionTypesCUD]", parameters, commandType: CommandType.StoredProcedure);
                    return parameters.Get<int>("output") > 0;
                }
                else
                {
                    conn.Query<int>("[Admin].[BatchUpdateUserCustomPermissionCUD]", parameters, commandType: CommandType.StoredProcedure);
                    return parameters.Get<int>("output") > 0;
                }
            }
        }

        public async Task<object> CheckUserRoleMapping(long? userId, short? roleId)
        {
            using (var conn = GetOpenConnection())
            {
                string query = "SELECT COUNT(RoleId) FROM [Admin].[UserRoleMapping] WHERE UserId = @UserId and RoleId = @RoleId";
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int16);
                parameters.Add("@RoleId", roleId, DbType.Int16);

                return await conn.ExecuteScalarAsync(query, parameters, commandType: CommandType.Text);
            }
        }
        public async Task<bool> InsertUserRoleMappingData(long userId, short roleId)
        {
           
            using (var conn = GetOpenConnection())
            {
                //string query = "Insert into [Admin].[UserRoleMapping] (UserId,RoleId) values (@UserId,@RoleId) ";
                //var parameters = new DynamicParameters();
                //parameters.Add("@UserId", userId, DbType.Int64);
                //parameters.Add("@RoleId", roleId, DbType.Int64);
                //return await conn.ExecuteAsync(query, parameters, commandType: CommandType.Text)>0;

                var parameters = new DynamicParameters();
                parameters.Add("@TransMode", (int)TransactionModes.Insert, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@RoleId", roleId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync<Int32>("Admin.UserRoleMappingCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("output") > 0;
            }
        }
        public async Task<bool> DeleteUserRoleMappingData(long userId, short roleId)
        {
            using (var conn = GetOpenConnection())
            {
                //string query = string.Join("\n", 
                //"Delete from [Admin].[UserRoleMapping] WHERE UserId = @UserId and RoleId = @RoleId");

                //var parameters = new DynamicParameters();
                //parameters.Add("@UserId", userId, DbType.Int64);
                //parameters.Add("@RoleId", roleId, DbType.Int64);

                //return await conn.ExecuteAsync(query, parameters, commandType: CommandType.Text) > 0;

                var parameters = new DynamicParameters();
                parameters.Add("@TransMode", (int)TransactionModes.Delete, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@RoleId", roleId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync<Int32>("Admin.UserRoleMappingCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("output") > 0;
            }
        }

        public async Task<IEnumerable<User>> GetUsersForRole(int roleId, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@RoleId", roleId, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);

                return await conn.QueryAsync<User>("Admin.GetUsersForRole", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        #endregion
    }
}
