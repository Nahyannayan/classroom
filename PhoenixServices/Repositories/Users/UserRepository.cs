﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.Models.HSEObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;


namespace Phoenix.API.Repositories
{
    public class UserRepository : SqlRepository<User>, IUserRepository
    {
        private readonly IConfiguration _config;

        public UserRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 16-MAY-2019
        /// Description : To fetch all the users
        /// </summary>
        /// <returns></returns>
        public override async Task<IEnumerable<User>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<User>("dbo.GetUsers", CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 18-JUNE-2019
        /// Description : To fetch user feelings 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<UserFeelingView>> GetUserFeelings(int SystemLanguageId)
        {
            using (var conn = GetOpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@SystemLanguageId", SystemLanguageId, DbType.Int32);
                return await conn.QueryAsync<UserFeelingView>("Admin.GetUserFeelings", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 18-JUNE-2019
        /// Description : To fetch user profile avatar 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<UserProfileView>> GetProfileAvatars()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<UserProfileView>("Admin.GetProfileAvatars", CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 18-JUNE-2019
        /// Description : To update user feelings 
        /// </summary>
        /// <returns></returns>
        public bool UpdateUserFeeling(UserFeelingView userFeelingView)
        {
            using (var conn = GetOpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@FeelingId", userFeelingView.FeelId, DbType.Int64);
                parameters.Add("@UserTypeId", userFeelingView.UserTypeId, DbType.Int64);
                parameters.Add("@UserId", userFeelingView.UserId, DbType.Int64);
                parameters.Add("@strCurrentStatus", userFeelingView.CurrentStatus, DbType.String);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Admin.UpdateUserFeeling", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("Output") > 0;
            }
        }
        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 24-JUNE-2019
        /// Description : To UpdateUserProfile
        /// </summary>
        /// <returns></returns>
        public bool UpdateUserProfile(UserProfileView userProfileView)
        {
            using (var conn = GetOpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@AvatarId", userProfileView.AvatarId, DbType.Int32);
                parameters.Add("@UserId", Convert.ToInt64(userProfileView.UserId), DbType.Int64);
                parameters.Add("@ProfilePhoto", userProfileView.ProfilePhoto, DbType.String);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Admin.UpdateUserProfile", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("Output") > 0;
            }
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 16-OCT-2019
        /// Description : To Insert Notification Log
        /// </summary>
        /// <returns></returns>
        public bool PushNotificationLogs(string notificationType, int sourceId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@SourceId", sourceId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@NotificationType", notificationType, DbType.String);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Admin.PushNotificationLogs", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("Output") > 0;
            }
        }
        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 16-MAY-2019
        /// Description : To fetch users by school id
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="UserTypeId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetUsersBySchool(int? schoolId, int UserTypeId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@UserTypeId", UserTypeId, DbType.Int32);
                return await conn.QueryAsync<User>("Admin.GetUsers", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Author : Girish Sonawane 
        /// Created Date : 15/10/2019
        /// Description : To get user notifications
        /// </summary>
        /// <param name="userTypeId"></param>
        /// <param name="userId"></param>
        /// <param name="loginUserId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<UserNotificationView>> GetUserNotifications(int? userTypeId, long userId, long loginUserId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@UserTypeId", userTypeId, DbType.Int32);
                parameters.Add("@loginUserId", loginUserId, DbType.Int32);
                return await conn.QueryAsync<UserNotificationView>("dbo.GetUserNotifications", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        /// <summary>
        /// Author : Girish Sonawane 
        /// Created Date : 15/10/2019
        /// Description : To get user notifications
        /// </summary>
        /// <param name="userTypeId"></param>
        /// <param name="userId"></param>
        /// <param name="loginUserId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<UserNotificationView>> GetAllNotifications(int? userTypeId, long userId, long loginUserId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@UserTypeId", userTypeId, DbType.Int32);
                parameters.Add("@loginUserId", loginUserId, DbType.Int32);
                return await conn.QueryAsync<UserNotificationView>("dbo.GetAllNotifications", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 16-MAY-2019
        /// Description : To fetch user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task<User> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<User>("dbo.[GetUsersById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 19-MAY-2019
        /// Description : To fetch user by name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="typeId"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<User>> SearchUserByName(string name, int typeId = 0, int schoolId = 0)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId, DbType.Int32);
                parameters.Add("@typeId", typeId, DbType.Int32);
                parameters.Add("@Name", name, DbType.String);
                return await conn.QueryAsync<User>("dbo.UserSearchByName", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 13-JUNE-2019
        /// Description : To fetch user by role and location allowed to access
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetUsersByRolesLocatonAllowed(string roles, long businessUnitId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Roles", roles, DbType.String);
                parameters.Add("@BusinessUnitId", businessUnitId, DbType.Int32);
                return await conn.QueryAsync<User>("[dbo].[GetUsersByRolesandLocations]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Author :Mahesh Chikhale
        /// Created Date : 21-Oct-2019
        /// Description : To save Error logged by User
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveErrorLogger(ErrorLogger objErrorLog)
        {
            using (var conn = GetOpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@ErrorText", objErrorLog.ErrorMessage, DbType.String);
                parameters.Add("@UserId", objErrorLog.UserId, DbType.Int64);
                parameters.Add("@ModuleUrl", objErrorLog.ModuleUrl, DbType.String);
                parameters.Add("@UserIpAddress", objErrorLog.UserIpAddress, DbType.String);
                parameters.Add("@WebServerIpAddress", objErrorLog.WebServerIpAddress, DbType.String);
                parameters.Add("@ReportIssueId", objErrorLog.ReportIssueId, DbType.Int32);
                parameters.Add("@ErrorFrequency", objErrorLog.ErrorFrequency, DbType.String);
                parameters.Add("@ErrorDateTime", objErrorLog.ErrorDateTime, DbType.String);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                DataTable dtErrorFiles = new DataTable();
                dtErrorFiles.Columns.Add("TaskId", typeof(Int64));
                dtErrorFiles.Columns.Add("FileName", typeof(string));
                dtErrorFiles.Columns.Add("UploadedFileName", typeof(string));
                dtErrorFiles.Columns.Add("RefTaskId", typeof(Int64));
                dtErrorFiles.Columns.Add("ResourceFileTypeId", typeof(int));
                dtErrorFiles.Columns.Add("ShareableLinkFileURL", typeof(string));
                foreach (var item in objErrorLog.lstTaskFiles)
                {
                    dtErrorFiles.Rows.Add(item.TaskId, item.FileName, item.UploadedFileName, 0, 0, string.Empty);
                }
                parameters.Add("@ErrorFiles", dtErrorFiles, DbType.Object);
                await conn.QueryAsync<int>("Admin.SaveErrorLogs", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("Output") > 0;
            }
        }
        /// <summary>
        /// Author : vinayk y 
        /// Created Date : 15-JAN-2020
        /// Description : To fetch users by school id and user type without studentnumber
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="UserTypeId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetUsersBySchoolAndType(int? schoolId, int UserTypeId,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@UserTypeId", UserTypeId, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<User>("Admin.GetUsersBySchoolAndType", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Author : Mukund Patil 
        /// Created Date : 08-AUGUST-2020
        /// Description : To fetch users by school id and user type with paging
        /// </summary>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="schoolId">schoolId</param>
        /// <param name="UserTypeId">UserTypeId</param>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetUsersBySchoolAndTypeWithPaging(int pageNumber, int pageSize, int? schoolId, int UserTypeId)
        {
            var list = new List<User>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@UserTypeId", UserTypeId, DbType.Int32);

                var result = await conn.QueryMultipleAsync("[Admin].[GetUsersBySchoolAndTypeWithPaging]", parameters, null, null, CommandType.StoredProcedure);
                var users = await result.ReadAsync<User>();
                var usersCount = await result.ReadAsync<int>();
                list = users.ToList();
                int totalCount = usersCount.FirstOrDefault();
                if (list.Any())
                {
                    list.ForEach(x => { x.TotalCount = totalCount; });
                }
                return list;
            }
        }


        /// <summary>
        /// Author :Mahesh Chikhale
        /// Created Date : 21-Oct-2019
        /// Description : To get DB log details
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<DBLogDetails>> GetDBLogdetails()
        {

            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<DBLogDetails>("[dbo].[sp_who3]", null, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<UserErrorLogs>> GetErrorLogs(int? schoolId, DateTime startDate, DateTime endDate, string loginType)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@LoginType", loginType, DbType.String);
                //return await conn.QueryAsync<User>("[admin].[GetUserLog]", parameters, commandType: CommandType.StoredProcedure);
                return await conn.QueryAsync<UserErrorLogs>("[admin].[GetUserLog]", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ErrorFiles>> GetErrorLogFiles(long ErrorLogId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ErrorLogId", ErrorLogId, DbType.Int64);
                return await conn.QueryAsync<ErrorFiles>("Admin.GetUserLogsFiles", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<User>> GetTeachersListBySchoolId(long schoolId = 0)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<User>("DE.GetTeachersList ", parameters, commandType: CommandType.StoredProcedure);
            }

        }

        public async Task<bool> InsertLogDetails(LogDetails logDetails)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@InputJson", logDetails.InputJson, DbType.String);
                parameters.Add("@OutputJson", logDetails.OutputJson, DbType.String);
                parameters.Add("@BaseUrl", logDetails.BaseUrl, DbType.String);
                parameters.Add("@Header", logDetails.Header, DbType.String);
                parameters.Add("@StatusCode", logDetails.StatusCode, DbType.String);
                parameters.Add("@StatusMessage", logDetails.StatusMessage, DbType.String);
                parameters.Add("@ExceptionMsg", logDetails.ExceptionMsg, DbType.String);
                parameters.Add("@LogDate", DateTime.Now, DbType.DateTime);
                parameters.Add("@IsSuccessStatusCode", logDetails.IsSuccessStatusCode, DbType.Byte);
                parameters.Add("@UserName", logDetails.userName, DbType.String);
                await conn.QueryFirstOrDefaultAsync<bool>("InsertLogDetails", parameters, commandType: CommandType.StoredProcedure);
                return true;
            }
        }

        #region Common method
        public override void InsertAsync(User entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(User entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To get list of users for dashboard
        /// Total Teacher Logins
        /// Total Student Logins
        /// Total Parent Logins
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="loginType"></param>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetUserLog(int? schoolId, DateTime startDate, DateTime endDate, string loginType)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@LoginType", loginType, DbType.String);
                return await conn.QueryAsync<User>("[admin].[GetUserLog]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<bool> SaveUserFeedback(UserFeedback model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FeedbackRating", model.FeedbackRating, DbType.String);
                parameters.Add("@Description", model.Description, DbType.String);
                parameters.Add("@CreatedBy", model.CreatedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync<int>("[Admin].[SaveUserFeedback]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        #endregion
    }
}

