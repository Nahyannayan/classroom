﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class LogInUserRepository : SqlRepository<LogInUser>, ILogInUserRepository
    {
        private readonly IConfiguration _config;

        public LogInUserRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<LogInUser>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<LogInUser> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<LogInUser> GetLogInUserByUserName(string UserName,string ipDetails)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserName", UserName, DbType.String);
                parameters.Add("@ipDetails", ipDetails, DbType.String);
                return await conn.QueryFirstOrDefaultAsync<LogInUser>("Admin.GetLoginUserByUserName", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<LogInUser>> GetUserList(int schoolId,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<LogInUser>("Admin.GetUserList", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<LogInUser> GetLogInUserByUserNamePassword(string UserName,string Password,string ipDetails)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserName", UserName, DbType.String);
                parameters.Add("@Password", Password, DbType.String);
                parameters.Add("@ipDetails", ipDetails, DbType.String);
                var result= await conn.QueryFirstOrDefaultAsync<LogInUser>("Admin.GetLoginUserByUserNamePassword", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }

        }

        public override void InsertAsync(LogInUser entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(LogInUser entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
