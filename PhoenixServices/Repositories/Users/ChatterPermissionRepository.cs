﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Repositories;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class ChatterPermissionRepository : SqlRepository<ChatPermission> , IChatterPermissionRepository
    {
        private readonly IConfiguration _config;

        public ChatterPermissionRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<UserChatPermission>> GetUserPermissions(int schoolId, int userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
           
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int32);
              
                return await conn.QueryAsync<UserChatPermission>("Admin.GetUserChatPermission", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<bool> UpdateChatterPermission(ChatPermission chats)
        {
            using (var conn = GetOpenConnection())
            {
                var result = string.Join(",",chats.UserIds);
                var parameters = new DynamicParameters(); 
                parameters.Add("@UserIds", result, DbType.String);
                parameters.Add("@SchoolId", chats.SchoolId, DbType.Int32);
                parameters.Add("@PermissionId", chats.PermissionId, DbType.Int32);
                parameters.Add("@Allow", chats.Flag, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync<ChatPermission>("Admin.UpdateChatPermission", parameters, commandType: CommandType.StoredProcedure);
                //return parameters.Get<Int32>("output") > 0;
                return true;

            }
        }

        public async Task<IEnumerable<ChatUsers>> GetUsersInGroup(int groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", groupId, DbType.Int32);
                return await conn.QueryAsync<ChatUsers>("School.GetChatUsers", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<ChatPermission>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<ChatPermission> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(ChatPermission entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(ChatPermission entityToUpdate)
        {
            throw new NotImplementedException();
        }

        

    }
}
