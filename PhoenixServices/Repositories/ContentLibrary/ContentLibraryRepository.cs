﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;

namespace Phoenix.API.Repositories
{
    public class ContentLibraryRepository : SqlRepository<ContentView>, IContentLibraryRepository
    {
        private readonly IConfiguration _config;

        public ContentLibraryRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<ContentView>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<ContentView> GetAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override void InsertAsync(ContentView entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(ContentView entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ContentView>> GetContentLibrary(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<ContentView>("Admin.GetContentLibrary", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SchoolLevels>> GetSchoolLevelsBySchoolId(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<SchoolLevels>("[School].[GetSchoolLevelsBySchoolId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ContentView>> GetContentLProvider(long schoolId, string divisionIds, string sortBy="")
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DivisionId");
            dt.TableName = "tblDivisionId";
            if (!string.IsNullOrEmpty(divisionIds))
            {
                List<int> lstdivisionIds = divisionIds.Split(',').Select(int.Parse).ToList();
                foreach (var item in lstdivisionIds)
                {
                    dt.Rows.Add(item);
                }
            }

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@SortBy", String.IsNullOrEmpty(sortBy) ? String.Empty : sortBy, DbType.String);
                parameters.Add("@DivisionIds", dt, DbType.Object);
                return await conn.QueryAsync<ContentView>("[Admin].[GetContentProvider]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ContentView>> GetPaginateContentLibrary(int schoolId, int pageNumber, int pageSize, string searchString = "",string  sortBy="", string categoryIds = "")
        {
            var list = new List<ContentView>();
            using (var conn = GetOpenConnection())
            {
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@SchoolId", schoolId, DbType.Int32);
                    parameters.Add("@PageNum", pageNumber, DbType.Int32);
                    parameters.Add("@PageSize", pageSize, DbType.Int32);
                    parameters.Add("@SearchString", String.IsNullOrEmpty(searchString) ? String.Empty : searchString, DbType.String);
                    parameters.Add("@SortBy", String.IsNullOrEmpty(sortBy) ? String.Empty : sortBy, DbType.String);
                    parameters.Add("@CategoryIds", String.IsNullOrEmpty(categoryIds) ? String.Empty : categoryIds, DbType.String);
                    var result = await conn.QueryMultipleAsync("[School].[PaginateContentLibrary]", parameters, null, null, CommandType.StoredProcedure);
                    var ContentLibrary = await result.ReadAsync<ContentView>();
                    var ContentLibraryCount = await result.ReadAsync<int>();
                    list = ContentLibrary.ToList();
                    int totalCount = ContentLibraryCount.FirstOrDefault();
                    if (list.Any())
                    {
                        list.ForEach(x => { x.TotalCount = totalCount; });
                    }
                }
                catch (Exception ex) 
                {

                    throw ex;
                }
              
                return list;
            }

        }
        public async Task<IEnumerable<ContentView>> GetStudentContentLibrary(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy="", string categoryIds = "")
        {
            var list = new List<ContentView>();
            using (var conn = GetOpenConnection())
            {
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@SchoolId", schoolId, DbType.Int32);
                    parameters.Add("@PageNum", pageNumber, DbType.Int32);
                    parameters.Add("@PageSize", pageSize, DbType.Int32);
                    parameters.Add("@SearchString", String.IsNullOrEmpty(searchString) ? String.Empty : searchString, DbType.String);
                    parameters.Add("@SortBy", String.IsNullOrEmpty(sortBy) ? String.Empty : sortBy, DbType.String);
                    parameters.Add("@CategoryIds", String.IsNullOrEmpty(categoryIds) ? String.Empty : categoryIds, DbType.String);
                    var result = await conn.QueryMultipleAsync("[School].[GetStudentContentLibrary]", parameters, null, null, CommandType.StoredProcedure);
                    var ContentLibrary = await result.ReadAsync<ContentView>();
                    var ContentLibraryCount = await result.ReadAsync<int>();
                    list = ContentLibrary.ToList();
                    int totalCount = ContentLibraryCount.FirstOrDefault();
                    if (list.Any())
                    {
                        list.ForEach(x => { x.TotalCount = totalCount; });
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }

                return list;
            }

        }

        public int UpdateContentResource(Content entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetContentResourceParameters(entity, mode);
                conn.Query<int>("[Admin].[ContentResourceCUD]", parameters, commandType: CommandType.StoredProcedure); // Sp modification
                return parameters.Get<int>("output");
            }
        }
        private DynamicParameters GetContentResourceParameters(Content entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@ContentId", entity.ContentId, DbType.Int32);
            parameters.Add("@UserId", entity.UserId, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int32);
            parameters.Add("@SubjectIds", entity.SubjectIds==null ?String.Empty:entity.SubjectIds.FirstOrDefault(), DbType.String);
            parameters.Add("@ContentName", entity.ContentName, DbType.String);
            parameters.Add("@Description", entity.Description, DbType.String);
            parameters.Add("@ContentType", (int)ContentType.ContentResource, DbType.Int32);
            parameters.Add("@RedirectUrl", entity.RedirectUrl, DbType.String);
            parameters.Add("@ContentImage", entity.ContentImage, DbType.String);
            parameters.Add("@PhysicalPath", entity.PhysicalPath, DbType.String);
            parameters.Add("@FileExtenstion", entity.FileExtenstion, DbType.String);
            parameters.Add("@IsSharepointFile", entity.IsSharepointFile, DbType.Boolean);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        public async Task<Content> GetContentResourceById(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ContentId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<Content>("[Admin].[GetContentById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Content>> GetSubjectsByContentId(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ContentId", id, DbType.Int32);
                return await conn.QueryAsync<Content>("[Admin].[GetSubjectsByContentId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public bool ChangeContentLibraryStatus(int resourceId, bool status, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ContentId", resourceId, DbType.Int32);
                parameters.Add("@Status", status, DbType.Boolean);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                conn.Query<int>("[Admin].[ChangeContentLibraryStatus]", parameters, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
        public bool AssignContentResourceToSchool(int resourceId, bool status, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ContentId", resourceId, DbType.Int32);
                parameters.Add("@Status", status, DbType.Boolean);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                conn.Query<int>("[Admin].[AssignContentResourceToSchool]", parameters, commandType: CommandType.StoredProcedure);
                return true;
            }
        }

    }
}
