﻿using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IContentLibraryRepository
    {
        Task<IEnumerable<ContentView>> GetContentLibrary(long schoolId);
        Task<IEnumerable<ContentView>> GetContentLProvider(long schoolId, string divisionIds, string sortBy = "");
        int UpdateContentResource(Content entity, TransactionModes mode);
        Task<Content> GetContentResourceById(int id);
        Task<IEnumerable<Content>> GetSubjectsByContentId(int id);
        bool ChangeContentLibraryStatus(int resourceId, bool status, long schoolId);
        bool AssignContentResourceToSchool(int resourceId, bool status, long schoolId);
        Task<IEnumerable<ContentView>> GetPaginateContentLibrary(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy = "", string categoryIds = "");
        Task<IEnumerable<ContentView>> GetStudentContentLibrary(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy="", string categoryIds = "");
        Task<IEnumerable<SchoolLevels>> GetSchoolLevelsBySchoolId(long schoolId);
    }
}
