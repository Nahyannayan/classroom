﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Models;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Models;

namespace Phoenix.API.Repositories
{
    public interface IStudentSkillSetRepository
    {
        Task<IEnumerable<StudentSkills>> GetStudentSkillsData(long id, short languageId);
        Task<OperationDetails> UpdateStudentSkillsData(int studentId, long id, int skillId, char mode);
        Task<OperationDetails> InsertStudentSkill(StudentSkillDTO model);
        Task<bool> UpdateStudentSkillEndorsementDetails(SkillEndorsement model);
        Task<IEnumerable<SkillEndorsement>> GetAllStudentEndorsedSkills(long userId, int pageIndex, int pageSize, string searchString ="", long currentUserId = 0);
        Task<bool> UpdateEndorseSkillStatusRating(SkillEndorsement model);
        Task<bool> UpdateAllEndorsedSkillStatus(List<SkillEndorsement> lstModel);
        Task<bool> DeleteEndorsedSkill(SkillEndorsement model);
        Task<SkillEndorsement> GetStudentEndorsedSkillById(int skillEndorsementId);
        Task<bool> UpdateStudentEndorsementStatus(SkillEndorsement model);
    }
}
