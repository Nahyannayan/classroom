﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories.Skill.Contracts
{
    public interface ISchoolSkillSetRepository : IGenericRepository<SchoolSkillSet>
    {
        Task<IEnumerable<SchoolSkillSet>> GetSchoolSkillSet(int languageId, int SchoolGradeId);
        int UpdateSchoolSkillSet(SchoolSkillSet entity, TransactionModes mode);
        Task<SchoolSkillSet> GetSchoolSkillSetById(int id, short languageId);
    }
}
