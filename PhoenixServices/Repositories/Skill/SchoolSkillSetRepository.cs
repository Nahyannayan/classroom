﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Repositories.Skill.Contracts;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class SchoolSkillSetRepository : SqlRepository<SchoolSkillSet>, ISchoolSkillSetRepository
    {
        private readonly IConfiguration _config;
        public SchoolSkillSetRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<SchoolSkillSet>> GetSchoolSkillSet(int languageId, int SchoolGradeId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@LanguageId", languageId, DbType.Int32);
                parameters.Add("@SchoolGradeId", SchoolGradeId, DbType.Int32);        
                return await conn.QueryAsync<SchoolSkillSet>("[School].[GetSkillSet]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int UpdateSchoolSkillSet(SchoolSkillSet entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSchoolSkillSetParameters(entity, mode);
                conn.Query<int>("[School].[SkillSetCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<SchoolSkillSet> GetSchoolSkillSetById(int id,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SkillId", id, DbType.Int32);
                parameters.Add("@LanguageId", languageId, DbType.Int16);
                return await conn.QueryFirstOrDefaultAsync<SchoolSkillSet>("[School].[GetSkillSetById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        #region Private Methods  
        private DynamicParameters GetSchoolSkillSetParameters(SchoolSkillSet entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SchoolGradeId", entity.SchoolGradeId, DbType.Int32);
            parameters.Add("@SkillId", entity.SkillId, DbType.Int32);
            parameters.Add("@SkillName", entity.SkillName, DbType.String);
            parameters.Add("@SkillSetXml", entity.SkillSetXml, DbType.String);
            parameters.Add("@SkillDescription", entity.SkillDescription, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@IsApproved", entity.IsApproved, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedBy, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        #region Generated Methods

        public override Task<IEnumerable<SchoolSkillSet>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async override Task<SchoolSkillSet> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SkillId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<SchoolSkillSet>("[School].[GetSkillSetById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

      
        public override void InsertAsync(SchoolSkillSet entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SchoolSkillSet entityToUpdate)
        {
            throw new NotImplementedException();
        }

      
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

       

        #endregion
    }
}
