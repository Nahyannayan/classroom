﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;

namespace Phoenix.API.Repositories
{
    public class StudentSkillSetRepository : SqlRepository<StudentSkills>, IStudentSkillSetRepository
    {
        private readonly IConfiguration _config;
        public StudentSkillSetRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        #region public methods
        public async Task<IEnumerable<StudentSkills>> GetStudentSkillsData(long id,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<StudentSkills>("[Student].[GetStudentSkillSetData]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<OperationDetails> UpdateStudentSkillsData(int studentId, long userId, int skillId, char mode)
        {
            var op = new OperationDetails();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@StudentId", studentId, DbType.Int32);
                parameters.Add("@SkillId", skillId, DbType.Int32);
                parameters.Add("@TransactionMode", mode, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.StudentSkillCUD", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = true;
                }
                else
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = false;
                }
                return op;
            }
        }

        public async Task<OperationDetails> InsertStudentSkill(StudentSkillDTO model)
        {
            var op = new OperationDetails();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@CreatedByUserId", model.CreatedUserId, DbType.Int64);
                parameters.Add("@SkillName", model.SkillName, DbType.String);
                parameters.Add("@Desciption", model.SkillDescription, DbType.String);
                parameters.Add("@IsActive", model.IsActive, DbType.Boolean);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.InsertStudentSkill", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = true;
                }
                else
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = false;
                }
            }
            return op;
        }


        public async Task<bool> DeleteEndorsedSkill(SkillEndorsement model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SkillEndorsedId", model.SkillEndorsedId, DbType.Int64);
                parameters.Add("@UpdatedBy", model.CreatedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.DeleteSkillEndorsed", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        #endregion

        #region Student Skill Endorsement
        public async Task<bool> UpdateStudentSkillEndorsementDetails(SkillEndorsement model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.StudentUserId, DbType.Int64);
                parameters.Add("@TeacherId", model.TeacherId, DbType.Int64);
                parameters.Add("@Message", model.Message, DbType.String);
                parameters.Add("@EndorsedSkillId", model.SkillEndorsedId, DbType.Int32);
                parameters.Add("@SkillIds", string.Join(',', model.SkillIds), DbType.String);
                parameters.Add("@CreatedBy", model.CreatedBy, DbType.Int64);
                parameters.Add("@RequestEndorsement", model.RequestEndorsement, DbType.Boolean);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.SaveStudentSkillEndorsed", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                return result;
            }

        }

        public async Task<IEnumerable<SkillEndorsement>> GetAllStudentEndorsedSkills(long userId, int pageIndex, int pageSize, string searchString, long currentUserId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@PageNum", pageIndex, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@CurrentUserId", currentUserId, DbType.Int64);
                parameters.Add("@SearchString", searchString, DbType.String);
                var result = await conn.QueryMultipleAsync("Student.GetAllStudentEndorsedSkills", parameters, null, null, CommandType.StoredProcedure);
                var studentSkills = await result.ReadAsync<SkillEndorsement>();
                var totalGoalsCount = await result.ReadAsync<int>();
                if (studentSkills.Any())
                {
                    studentSkills.ToList().ForEach(x => { x.TotalCount = totalGoalsCount.FirstOrDefault(); });
                }
                return studentSkills;
            }
        }

        public async Task<bool> UpdateEndorseSkillStatusRating(SkillEndorsement model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.StudentUserId, DbType.Int64);
                parameters.Add("@SkillEndorsedId", model.SkillEndorsedId, DbType.String);
                parameters.Add("@Rating", model.Rating, DbType.Int16);
                parameters.Add("@Mode", model.Mode, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.UpdateEndorsedSkillRatingStatus", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                return result;
            }
        }

        public async Task<bool> UpdateAllEndorsedSkillStatus(List<SkillEndorsement> LstModel)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                foreach (var item in LstModel)
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@UserId", item.StudentUserId, DbType.Int64);
                    parameters.Add("@SkillEndorsedId", item.SkillEndorsedId, DbType.String);
                    parameters.Add("@ShowOnDashboard", item.ShowOnDashboard, DbType.Boolean);
                    parameters.Add("@Rating", item.Rating, DbType.Int16);
                    parameters.Add("@Mode", item.Mode, DbType.String);//D means update Dashboard Status
                    parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    conn.Query<int>("Student.UpdateEndorsedSkillRatingStatus", parameters, commandType: CommandType.StoredProcedure);
                    if (parameters.Get<int>("output") > 0)
                        result = true;

                }
            }
            return result;
        }

        public async Task<SkillEndorsement> GetStudentEndorsedSkillById(int skillEndorsementId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SkillEndorsementId", skillEndorsementId, DbType.Int32);
                return await conn.QueryFirstAsync<SkillEndorsement>("[Student].[GetStudentEndorsedSkillById]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<bool> UpdateStudentEndorsementStatus(SkillEndorsement model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EndorsedSkillId", model.SkillEndorsedId, DbType.Int32);
                parameters.Add("@SkillDescription", model.Message, DbType.String);
                parameters.Add("@Mode", model.Mode, DbType.String);
                parameters.Add("@Rating", model.Rating, DbType.Int16);
                parameters.Add("@UpdatedBy", model.CreatedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.UpdateStudentEndorsedSkill", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        #endregion

        #region Auto generated method
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<StudentSkills>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<StudentSkills> GetAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override void InsertAsync(StudentSkills entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(StudentSkills entityToUpdate)
        {
            throw new NotImplementedException();
        }




        #endregion
    }
}
