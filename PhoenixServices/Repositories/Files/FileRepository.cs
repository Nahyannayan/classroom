﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;


namespace Phoenix.API.Repositories
{
    public class FileRepository : SqlRepository<File>, IFileRepository
    {
        private readonly IConfiguration _config;
        public FileRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetFileParameters(new File(id), TransactionModes.Delete);
                conn.Query<Int32>("FMS.FilesCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public bool Delete(int id, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var model = new File(id);
                model.UpdatedBy = userId;
                var parameters = GetFileParameters(model, TransactionModes.Delete);
                conn.Query<Int32>("FMS.FilesCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public async override Task<IEnumerable<File>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@ModuleId", DBNull.Value, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", DBNull.Value, DbType.Boolean);
                return await conn.QueryAsync<File>("FMS.GetFiles", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async override Task<File> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileId", id, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@ModuleId", DBNull.Value, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", DBNull.Value, DbType.Boolean);
                return await conn.QueryFirstOrDefaultAsync<File>("FMS.GetFiles", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<FileManagementModule>> GetFileManagementModules()
        {
            using (var conn = GetOpenConnection())
            {

                // return conn.QueryAsync<FileManagementModule>("FMS.GetFileManagementModules",null,null,null,CommandType.StoredProcedure);
                return await conn.QueryAsync<FileManagementModule>("FMS.GetFileManagementModules", null, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<File>> GetFilesByFolderId(int folderId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@ModuleId", DBNull.Value, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@FolderId", folderId, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<File>("FMS.GetFiles", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<File>> GetFilesByModuleId(int schoolId, int moduleId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@ModuleId", moduleId, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<File>("FMS.GetFiles", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<File>> GetFilesBySectionId(int sectionId, int moduleId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@ModuleId", moduleId, DbType.Int32);
                parameters.Add("@SectionId", sectionId, DbType.Int32);
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<File>("FMS.GetFiles", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<File>> GetFilesBySchoolId(int schoolId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@ModuleId", DBNull.Value, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<File>("FMS.GetFiles", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<StudentGroupsFiles>> GetStudentGroupsFiles(long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<StudentGroupsFiles>("FMS.GetStudentGroupsFiles", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<File>> GetFilesByUserId(int userId, int moduleId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@ModuleId", moduleId, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<File>("FMS.GetFiles", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<VLEFileType>> GetFileTypes(string DocumentType)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                if (!string.IsNullOrEmpty(DocumentType))
                {
                    parameters.Add("@DocumentType", DocumentType, DbType.String);
                }
                //return conn.QueryAsync<VLEFileType>("FMS.GetFileTypes", null, null, null, CommandType.StoredProcedure);
                return await conn.QueryAsync<VLEFileType>("FMS.GetFileTypes", parameters, null, null, commandType: CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(File entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetFileParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("FMS.FilesCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }
        public int Insert(File entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetFileParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("FMS.FilesCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public override void UpdateAsync(File entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetFileParameters(entityToUpdate, TransactionModes.Insert);
                conn.Query<Int32>("FMS.FilesCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }
        public bool RenameFile(long fileId, string FileName, long updatedBy)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileId", fileId, DbType.Int64);
                parameters.Add("@FileName", FileName, DbType.String);
                parameters.Add("@CreatedBy", updatedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("FMS.FilesRename", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<File>> GetAllGroupFiles(int schoolGroupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int32);
                return await conn.QueryAsync<File>("School.GetAllSchoolGroupFiles", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        #region Cloud Storage Files 

        public async Task<bool> SaveCloudFiles(List<File> files)
        {
            using (var conn = GetOpenConnection())
            {
                var dt = new DataTable();
                dt.Columns.Add("FileName", typeof(string));
                dt.Columns.Add("CreatedBy", typeof(long));
                dt.Columns.Add("ModuleId", typeof(int));
                dt.Columns.Add("SectionId", typeof(long));
                dt.Columns.Add("FolderId", typeof(long));
                dt.Columns.Add("FilePath", typeof(string));
                dt.Columns.Add("FileTypeId", typeof(int));
                dt.Columns.Add("ResourceFileTypeId", typeof(short));
                dt.Columns.Add("DriveFileId", typeof(string));

                foreach (var item in files)
                {
                    dt.Rows.Add(item.FileName, item.CreatedBy, item.ModuleId, item.SectionId, item.FolderId, item.FilePath, item.FileTypeId, item.ResourceFileTypeId, item.GoogleDriveFileId);
                }
                var parameters = new DynamicParameters();
                parameters.Add("@FilesData", dt, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("FMS.InsertCloudFiles", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        #endregion
        #region bulk Files Get & CD

        public async Task<IEnumerable<File>> GetFilesByIds(string Ids)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileIds", Ids, DbType.String);
                return await conn.QueryAsync<File>("FMS.GetFilesByIds", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<bool> FilesFolderBulkCD(MyFilesModel model, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var fileDt = new DataTable();
                fileDt.Columns.Add("FileId", typeof(int));
                fileDt.Columns.Add("FileName", typeof(string));
                fileDt.Columns.Add("CreatedBy", typeof(long));
                fileDt.Columns.Add("ModuleId", typeof(int));
                fileDt.Columns.Add("SectionId", typeof(long));
                fileDt.Columns.Add("FolderId", typeof(long));
                fileDt.Columns.Add("FilePath", typeof(string));
                fileDt.Columns.Add("FileTypeId", typeof(int));
                fileDt.Columns.Add("ResourceFileTypeId", typeof(short));
                fileDt.Columns.Add("DriveFileId", typeof(string));
                fileDt.Columns.Add("FileSizeInMB", typeof(double));
                fileDt.Columns.Add("PhysicalFilePath", typeof(string));

                var folderDt = new DataTable();
                folderDt.Columns.Add("FolderId", typeof(int));
                folderDt.Columns.Add("FolderName", typeof(string));

                foreach (var item in model.Files)
                {
                    fileDt.Rows.Add(item.FileId,item.FileName, item.CreatedBy, item.ModuleId, item.SectionId, item.FolderId, item.FilePath, item.FileTypeId, item.ResourceFileTypeId, item.GoogleDriveFileId, item.FileSizeInMB,item.PhysicalFilePath);
                }
                foreach (var item in model.Folders)
                {
                    folderDt.Rows.Add(item.FolderId, item.FolderName);
                }
                var parameters = new DynamicParameters();
                parameters.Add("@TransMode", (int)mode, DbType.Int16);
                parameters.Add("@FilesData", fileDt, DbType.Object);
                parameters.Add("@FoldersData", folderDt, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("FMS.FilesFoldersBulkCD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        #endregion

        #region Private Methods
        private DynamicParameters GetFileParameters(File entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@FileId", entity.FileId, DbType.Int64);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@ModuleId", entity.ModuleId, DbType.Int64);
            parameters.Add("@SectionId", entity.SectionId, DbType.Int64);
            parameters.Add("@FolderId", entity.FolderId, DbType.Int64);
            parameters.Add("@FileName", entity.FileName, DbType.String);
            parameters.Add("@FilePath", entity.FilePath, DbType.String);
            parameters.Add("@FileTypeId", entity.FileTypeId, DbType.Int32);
            parameters.Add("@PinToHome", entity.PinToHome, DbType.Boolean);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            parameters.Add("@FileSizeInMb", entity.FileSizeInMB, DbType.Double);
            parameters.Add("@ResourceFileTypeId", entity.ResourceFileTypeId, DbType.Int16);
            parameters.Add("@PhysicalFilePath", entity.PhysicalFilePath, DbType.String);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        public async Task<int> ClearGroupFiles(SchoolGroup model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", model.SchoolGroupId, DbType.Int32);
                parameters.Add("@UpdatedBy", model.CreatedById, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("School.ClearGroupFiles", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        public async Task<FileExplorer> GetGroupFileExplorer(int moduleId, long sectionId, long folderId, long studentUserId, bool isActive)
        {
            var fileExplorer = new FileExplorer();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ModuleId", moduleId, DbType.Int32);
                parameters.Add("@SectionId", sectionId, DbType.Int64);
                parameters.Add("@FolderId", folderId, DbType.Int64);
                parameters.Add("@StudentUserId", studentUserId, DbType.Int64);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                var result = await conn.QueryMultipleAsync("[FMS].[GetGroupFileExplorer]", parameters, commandType: CommandType.StoredProcedure);

                fileExplorer.SectionId = sectionId;
                fileExplorer.ModuleId = moduleId;
                fileExplorer.FolderId = folderId;
                if (folderId == 0)
                {
                    fileExplorer.GroupCourseTopics.AddRange(result.Read<GroupCourseTopic>());
                }
                fileExplorer.Folders.AddRange(result.Read<Folder>());
                fileExplorer.Files.AddRange(result.Read<File>());
                fileExplorer.GroupUrls.AddRange(result.Read<GroupUrl>());
                fileExplorer.GroupQuizzes.AddRange(result.Read<GroupQuiz>());
                fileExplorer.Forms.AddRange(result.Read<GroupQuiz>());
                fileExplorer.AsyncLessons.AddRange(result.Read<AsyncLesson>());
                if (folderId == 0)
                    fileExplorer.Assignments.AddRange(result.Read<Assignment>());


                return fileExplorer;
            }
        }

        #endregion
    }
}
