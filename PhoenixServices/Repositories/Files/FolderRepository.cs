﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;

namespace Phoenix.API.Repositories
{
    public class FolderRepository : SqlRepository<Folder>, IFolderRepository
    {
        private readonly IConfiguration _config;
        public FolderRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetFolderParameters(new Folder(id), TransactionModes.Delete);
                conn.Query<Int32>("FMS.FolderCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public bool Delete(long id, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var model = new Folder(id);
                model.UpdatedBy = userId;
                var parameters = GetFolderParameters(model, TransactionModes.Delete);
                conn.Query<Int32>("FMS.FolderCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public async override Task<IEnumerable<Folder>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@ModuleId", DBNull.Value, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@ParentFolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", DBNull.Value, DbType.Boolean);
                return await conn.QueryAsync<Folder>("FMS.GetFolders", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async override Task<Folder> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FolderId", id, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@ModuleId", DBNull.Value, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@ParentFolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", DBNull.Value, DbType.Boolean);
                return await conn.QueryFirstOrDefaultAsync<Folder>("FMS.GetFolders", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Folder>> GetFoldersByModuleId(int schoolId, int moduleId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@ModuleId", moduleId, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@ParentFolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<Folder>("FMS.GetFolders", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Folder>> GetFoldersBySectionId(int sectionId, int moduleId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@ModuleId", moduleId, DbType.Int32);
                parameters.Add("@SectionId", sectionId, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@ParentFolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<Folder>("FMS.GetFolders", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Folder>> GetFoldersByParentFolderId(int parentFolderId, int moduleId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@ModuleId", moduleId, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@ParentFolderId", parentFolderId, DbType.Int32);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<Folder>("FMS.GetFolders", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Folder>> GetFoldersBySchoolId(int schoolId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@ModuleId", DBNull.Value, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@ParentFolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<Folder>("FMS.GetFolders", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Folder>> GetFoldersByUserId(int userId, int moduleId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@ModuleId", moduleId, DbType.Int32);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@ParentFolderId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<Folder>("FMS.GetFolders", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<FolderTree>> GetFolderTree(int folderId)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@ParentFolderId", folderId, DbType.Int32);
                return await conn.QueryAsync<FolderTree>("FMS.GetFolderTree", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(Folder entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetFolderParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("FMS.FolderCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public int Insert(Folder entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetFolderParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("FMS.FolderCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public override void UpdateAsync(Folder entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetFolderParameters(entityToUpdate, TransactionModes.Update);
                conn.Query<Int32>("FMS.FolderCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public int Update(Folder entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetFolderParameters(entityToUpdate, TransactionModes.Update);
                conn.Query<Int32>("FMS.FolderCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        #region Private Methods
        private DynamicParameters GetFolderParameters(Folder entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@FolderId", entity.FolderId, DbType.Int64);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@ModuleId", entity.ModuleId, DbType.Int64);
            parameters.Add("@SectionId", entity.SectionId, DbType.Int64);
            parameters.Add("@FolderName", entity.FolderName, DbType.String);
            parameters.Add("@ParentFolderId", entity.ParentFolderId, DbType.Int64);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        public int GetGroupPermission(int userId, int groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                return conn.QueryFirst<int>("School.GetUserPermissionOnGroup", parameters, commandType: CommandType.StoredProcedure);
                 
            }
        }
        #endregion
    }
}
