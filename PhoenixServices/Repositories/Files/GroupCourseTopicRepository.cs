﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;

namespace Phoenix.API.Repositories
{
    public class GroupCourseTopicRepository : SqlRepository<GroupCourseTopic>, IGroupCourseTopicRepository
    {
        private readonly IConfiguration _config;
        public GroupCourseTopicRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetGroupCourseTopicParameters(new GroupCourseTopic(id), TransactionModes.Delete);
                conn.Query<Int32>("FMS.GroupCourseTopicsCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public override void InsertAsync(GroupCourseTopic entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(GroupCourseTopic entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var model =  new GroupCourseTopic(id);
                model.UpdatedBy = userId;
                model.StartDate = DateTime.Now;
                model.EndDate = DateTime.Now;
                var parameters = GetGroupCourseTopicParameters(model, TransactionModes.Delete);
                conn.Query<Int32>("FMS.GroupCourseTopicsCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public async override Task<IEnumerable<GroupCourseTopic>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupCourseTopicId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int32);
                parameters.Add("@TopicId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsSubTopic", DBNull.Value, DbType.Boolean);
                parameters.Add("@IsActive", DBNull.Value, DbType.Boolean);
                return await conn.QueryAsync<GroupCourseTopic>("FMS.GetGroupCourseTopics", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async override Task<GroupCourseTopic> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupCourseTopicId", id, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int32);
                parameters.Add("@TopicId", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsSubTopic", DBNull.Value, DbType.Boolean);
                parameters.Add("@IsActive", DBNull.Value, DbType.Boolean);
                return await conn.QueryFirstOrDefaultAsync<GroupCourseTopic>("FMS.GetGroupCourseTopics", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<GroupCourseTopic>> GetGroupCourseTopics(long? groupCourseTopicId, long? groupId, long? topicId, long? userId, bool? isSubTopic, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupCourseTopicId", groupCourseTopicId, DbType.Int64);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@TopicId", topicId, DbType.Int64);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@IsSubTopic", isSubTopic, DbType.Boolean);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<GroupCourseTopic>("FMS.GetGroupCourseTopics", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<GroupCourseTopic>> GetGroupCourseTopicsByGroupId(long groupId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupCourseTopicId", DBNull.Value, DbType.Int64);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@TopicId", DBNull.Value, DbType.Int64);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                parameters.Add("@IsSubTopic", DBNull.Value, DbType.Boolean);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<GroupCourseTopic>("FMS.GetGroupCourseTopics", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        
        public int Insert(GroupCourseTopic entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetGroupCourseTopicParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("FMS.GroupCourseTopicsCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public int Update(GroupCourseTopic entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetGroupCourseTopicParameters(entityToUpdate, TransactionModes.Update);
                conn.Query<Int32>("FMS.GroupCourseTopicsCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public int UpdateUnitSortOrder(IEnumerable<GroupCourseTopic> model)
        {
            using (var conn = GetOpenConnection())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ResourceActivityId", typeof(long));
                dt.Columns.Add("SortOrder", typeof(int));
                foreach (var item in model)
                {
                    dt.Rows.Add(item.GroupCourseTopicId, item.SortOrder);
                }

                var parameters = new DynamicParameters();
                parameters.Add("@ResourcesSortOrder", dt, DbType.Object);
                parameters.Add("@UpdatedBy", model.FirstOrDefault().UpdatedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("FMS.UpdateGroupCourseTopicOrder", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("output");
                return result;
            }
        }

        #region Private Methods
        private DynamicParameters GetGroupCourseTopicParameters(GroupCourseTopic entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@GroupCourseTopicId", entity.GroupCourseTopicId, DbType.Int64);
            parameters.Add("@GroupId", entity.GroupId, DbType.Int64);
            parameters.Add("@TopicId", entity.TopicId, DbType.Int64);
            parameters.Add("@TopicName", entity.TopicName, DbType.String);
            parameters.Add("@StartDate", entity.StartDate, DbType.DateTime);
            parameters.Add("@EndDate", entity.EndDate, DbType.DateTime);
            parameters.Add("@IsSubTopic", entity.IsSubTopic, DbType.Boolean);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            parameters.Add("@SortOrder", entity.SortOrder, DbType.Int32);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        
        #endregion
    }
}
