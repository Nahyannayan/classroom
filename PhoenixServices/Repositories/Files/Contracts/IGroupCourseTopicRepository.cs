﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IGroupCourseTopicRepository : IGenericRepository<GroupCourseTopic>
    {
        bool Delete(int id, long userId);
        int Insert(GroupCourseTopic entity);
        int Update(GroupCourseTopic entityToUpdate);
        Task<IEnumerable<GroupCourseTopic>> GetGroupCourseTopicsByGroupId(long groupId, bool? isActive);
        Task<IEnumerable<GroupCourseTopic>> GetGroupCourseTopics(long? groupCourseTopicId, long? groupId, long? topicId, long? userId, bool? isSubTopic, bool? isActive);

        int UpdateUnitSortOrder(IEnumerable<GroupCourseTopic> model);
    }
}
