﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IGroupQuizRepository
    {

        int AddUpdateGroupQuiz(GroupQuiz entityToUpdate);
        Task<IEnumerable<GroupQuiz>> GetGroupQuizListByGroupId(int groupId);
        Task<IEnumerable<GroupQuiz>> GetGroupQuizListByGroupIdAndDateRange(int groupId, DateTime startDate, DateTime endDate);
        Task<GroupQuiz> GetById(int id);
        Task<QuizResult> GetQuizResultByUserId(int quizId, int userId, int resourceId);
        Task<GroupQuiz> GetQuizStudentDetailsByQuizId(int groupQuizId, int groupId, int selectedGroupId);
        Task<IEnumerable<QuizView>> GetQuizByCourse(string courseIds, int schoolId);
        Task<QuizResult> GetLogResultQuestionAnswer(int quizId, int userId, int resourceId, string resourceType);
        bool InsertUpdateQuizAnswers(QuizResult quizResult, DataTable Quizanswers);
        bool InsertQuizFeedback(QuizFeedback quizFeedback, DataTable recordings);
        bool LogQuizQuestionAnswer(QuizResult quizResult, DataTable Quizanswers);
        bool UpdateQuizGrade(int gradeId, int gradingTemplateId, int quizResultId);
        Task<GroupQuiz> GetFormStudentDetailsByQuizId(int QuizId, int groupId);
    }
}
