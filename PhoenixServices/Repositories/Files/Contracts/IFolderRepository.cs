﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Phoenix.API.Repositories
{
    public interface IFolderRepository : IGenericRepository<Folder>
    {
        Task<IEnumerable<Folder>> GetFoldersByUserId(int userId, int moduleId, bool? isActive);
        Task<IEnumerable<Folder>> GetFoldersBySchoolId(int schoolId, bool? isActive);
        Task<IEnumerable<Folder>> GetFoldersByModuleId(int schoolId, int moduleId, bool? isActive);
        Task<IEnumerable<Folder>> GetFoldersByParentFolderId(int parentFolderId, int moduleId, bool? isActive);
        Task<IEnumerable<FolderTree>> GetFolderTree(int folderId);
        Task<IEnumerable<Folder>> GetFoldersBySectionId(int sectionId, int moduleId, bool? isActive);

        int Insert(Folder entity);
        int Update(Folder entityToUpdate);
        int GetGroupPermission(int userId, int groupId);
        bool Delete(long id, long userId);
    }
}
