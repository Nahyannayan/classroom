﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IFileRepository : IGenericRepository<File>
    {
        Task<IEnumerable<File>> GetFilesByUserId(int userId, int moduleId, bool? isActive);
        Task<IEnumerable<File>> GetFilesBySchoolId(int schoolId, bool? isActive);
        Task<IEnumerable<File>> GetFilesByModuleId(int schoolId, int moduleId, bool? isActive);
        Task<IEnumerable<File>> GetFilesByFolderId(int folderId, bool? isActive);
        Task<IEnumerable<File>> GetFilesBySectionId(int sectionId, int moduleId, bool? isActive);
        Task<IEnumerable<StudentGroupsFiles>> GetStudentGroupsFiles(long userId);

        Task<FileExplorer> GetGroupFileExplorer(int moduleId, long sectionId, long folderId, long studentUserId, bool isActive);

        Task<IEnumerable<FileManagementModule>> GetFileManagementModules();
        Task<IEnumerable<VLEFileType>> GetFileTypes(string DocumentType);
        bool RenameFile(long fileId, string FileName, long updatedBy);
        Task<IEnumerable<File>> GetAllGroupFiles(int schoolGroupId);
        Task<int> ClearGroupFiles(SchoolGroup model);
        Task<bool> SaveCloudFiles(List<File> files);
        Task<IEnumerable<File>> GetFilesByIds(string Ids);
        Task<bool> FilesFolderBulkCD(MyFilesModel model, TransactionModes mode);
		
		int Insert(File file);
        bool Delete(int id, long userId);
    }
}
