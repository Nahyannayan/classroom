﻿
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
   public interface IGroupUrlRepository
    {
        Task<IEnumerable<GroupUrl>> GetAll();
        Task<IEnumerable<GroupUrl>> GetGroupUrlListByGroupId(int groupId);
        Task<GroupUrl> GetById(int id);
        //Task<int> Insert(GroupUrl entity);
        //Task<int> Delete(GroupUrl entityToDelete);
        //Task<int> Update(GroupUrl entityToUpdate);
        int AddUpdateGroupUrl(GroupUrl entityToUpdate);
    }
}
