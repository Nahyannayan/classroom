﻿using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IBookmarkRepository
    {
        Task<Bookmark> GetBookmark(long bookmarkId);
        bool AddUpdateBookmark(Bookmark entity, TransactionModes mode);
        Task<IEnumerable<Bookmark>> GetBookmarks(int userId);
        Task<IEnumerable<SchoolGroup>> GetBookmarkSchoolGroups(long bookmarkId);
    }
}
