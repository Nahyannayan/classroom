﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Phoenix.Models;
using System.Data;
using Phoenix.Common.Enums;
using Microsoft.Extensions.Configuration;

namespace Phoenix.API.Repositories
{
    public class GroupUrlRepository : SqlRepository<GroupUrl>,IGroupUrlRepository
    {
        private readonly IConfiguration _config;
        public GroupUrlRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<GroupUrl>> GetAll()
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<GroupUrl>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<GroupUrl> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<GroupUrl> GetById(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Int32);
                return await conn.QueryFirstAsync<GroupUrl>("[School].[GetSchoolGroupUrlById]",parameters,null,null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(GroupUrl entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(GroupUrl entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public int AddUpdateGroupUrl(GroupUrl entityToUpdate)
        {
            DynamicParameters parameters;
            if (entityToUpdate.DeletedBy!=0)
                parameters = GetGroupUrlParameters(entityToUpdate, TransactionModes.Delete);
            else
                parameters = GetGroupUrlParameters(entityToUpdate, TransactionModes.Update);
            using (var conn = GetOpenConnection())
            {

                conn.Query<Int32>("[School].[GroupUrlCUD]", parameters, commandType: CommandType.StoredProcedure);
                return  parameters.Get<Int32>("Output");
            }
        }

        public async Task<IEnumerable<GroupUrl>> GetGroupUrlListByGroupId(int groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupId", groupId, DbType.Int32);
                var result= await conn.QueryAsync<GroupUrl>("[School].[GetSchoolGroupUrlByGroupId]", parameters, null, null, CommandType.StoredProcedure);
                return result;
            }
        }
        #region Private Methods
        private DynamicParameters GetGroupUrlParameters(GroupUrl entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@Id", entity.Id, DbType.Int32);
            parameters.Add("@GroupId", entity.GroupId, DbType.Int64);
            parameters.Add("@Title", entity.Title, DbType.String);
            parameters.Add("@Link", entity.Link, DbType.String);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            parameters.Add("@DeletedBy", entity.DeletedBy, DbType.Int64);
            parameters.Add("@FolderId", entity.FolderId, DbType.Int64);
            parameters.Add("ModuleId", entity.ModuleId, DbType.Int64);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        #endregion

    }
}
