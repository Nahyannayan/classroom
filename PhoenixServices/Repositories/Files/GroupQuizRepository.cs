﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class GroupQuizRepository : SqlRepository<GroupQuiz>, IGroupQuizRepository
    {
        private readonly IConfiguration _config;
        public GroupQuizRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }


        public int AddUpdateGroupQuiz(GroupQuiz entityToUpdate)
        {
            DynamicParameters parameters;
            if (entityToUpdate.DeletedBy != 0)
                parameters = GetGroupQuizParameters(entityToUpdate, TransactionModes.Delete);
            else
                parameters = GetGroupQuizParameters(entityToUpdate, TransactionModes.Update);
            using (var conn = GetOpenConnection())
            {
                conn.Query<Int32>("[School].[GroupQuizCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }
        private DynamicParameters GetGroupQuizParameters(GroupQuiz entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@GroupQuizId", entity.GroupQuizId, DbType.Int32);
            parameters.Add("@GroupId", entity.GroupId, DbType.Int64);
            parameters.Add("@QuizId", entity.QuizId, DbType.Int32);
            parameters.Add("@GradingTemplateId", entity.GradingTemplateId, DbType.Int32);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            parameters.Add("@DeletedBy", entity.DeletedBy, DbType.Int64);
            parameters.Add("@FolderId", entity.FolderId, DbType.Int64);
            parameters.Add("@ModuleId", entity.ModuleId, DbType.Int32);
            parameters.Add("@IsSetTime", entity.IsSetTime, DbType.Boolean);
            parameters.Add("@IsHideScore", entity.IsHideScore, DbType.Boolean);
            parameters.Add("@IsRandomQuestion", entity.IsRandomQuestion, DbType.Boolean);
            parameters.Add("@QuizTime", entity.QuizTime, DbType.Int32);
            parameters.Add("@StartDate", entity.StartDate, DbType.DateTime);
            parameters.Add("@UpdatedOn", entity.UpdatedOn, DbType.DateTime);
            parameters.Add("@ShowScoreDate", entity.ShowScoreDate, DbType.DateTime);
            parameters.Add("@StartTime", entity.StartTime, DbType.String);
            parameters.Add("@MaxSubmit", entity.MaxSubmit, DbType.Int32);
            parameters.Add("@QuestionPaginationRange", entity.QuestionPaginationRange, DbType.Int32);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        public async Task<IEnumerable<GroupQuiz>> GetGroupQuizListByGroupId(int groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupId", groupId, DbType.Int32);
                var result = await conn.QueryAsync<GroupQuiz>("[School].[GetSchoolGroupQuizByGroupId]", parameters, null, null, CommandType.StoredProcedure);
                return result;
            }
        }

        public async Task<IEnumerable<GroupQuiz>> GetGroupQuizListByGroupIdAndDateRange(int groupId, DateTime startDate, DateTime endDate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupId", groupId, DbType.Int32);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                var result = await conn.QueryAsync<GroupQuiz>("[School].[GetSchoolGroupQuizByGroupIdAndDateRange]", parameters, null, null, CommandType.StoredProcedure);
                return result;
            }
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<GroupQuiz>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<GroupQuiz> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(GroupQuiz entity)
        {
           
        }

        public override void UpdateAsync(GroupQuiz entityToUpdate)
        {
            
        }

        public async Task<GroupQuiz> GetById(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Int32);
                return await conn.QueryFirstAsync<GroupQuiz>("[School].[GetSchoolGroupQuizById]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public bool InsertUpdateQuizAnswers(QuizResult quizResult, DataTable Quizanswers)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizResult.QuizId, DbType.Int32);
                parameters.Add("@QuizResultId", quizResult.QuizResultId, DbType.Int32);
                parameters.Add("@QuizResponseByUserId", quizResult.QuizResponseByUserId, DbType.Int32);
                parameters.Add("@QuizResponseByTeacherId", quizResult.QuizResponseByTeacherId, DbType.Int32);
                parameters.Add("@QuizAnswerTable", Quizanswers, DbType.Object);
                parameters.Add("@StudentId", quizResult.StudentId, DbType.Int32);
                parameters.Add("@TaskId", quizResult.TaskId, DbType.Int32);//MAKE AS RESOURCE ID
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[InsertUpdateQuizAnswers]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public bool LogQuizQuestionAnswer(QuizResult quizResult, DataTable Quizanswers)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizResult.QuizId, DbType.Int32);
                parameters.Add("@QuizResultId", quizResult.QuizResultId, DbType.Int32);
                parameters.Add("@QuizResponseByUserId", quizResult.QuizResponseByUserId, DbType.Int32);
                parameters.Add("@QuizResponseByTeacherId", quizResult.QuizResponseByTeacherId, DbType.Int32);
                parameters.Add("@QuizAnswerTable", Quizanswers, DbType.Object);
                parameters.Add("@StudentId", quizResult.StudentId, DbType.Int32);
                parameters.Add("@TaskId", quizResult.TaskId, DbType.Int32);//MAKE AS RESOURCE ID
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[InsertUpdateLogQuizAnswer]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public bool InsertQuizFeedback(QuizFeedback quizFeedback, DataTable recordings)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Feedback", quizFeedback.feedback, DbType.String);
                parameters.Add("@QuizResultId", quizFeedback.QuizResultId, DbType.Int32);
                parameters.Add("@UserId", quizFeedback.UserId, DbType.Int32);
                parameters.Add("@RecordingsTable", recordings, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[InsertQuizFeedback]", parameters, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
        public bool UpdateQuizGrade(int gradeId, int gradingTemplateId, int quizResultId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GradeId", gradeId, DbType.Int32);
                parameters.Add("@GradingTemplateId", gradingTemplateId, DbType.Int32);
                parameters.Add("@QuizResultId", quizResultId, DbType.Int32);
                conn.Query<int>("[School].[UpdateQuizGrade]", parameters, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
     
        public async Task<QuizResult> GetQuizResultByUserId(int quizId, int userId, int resourceId)
        {
            using (var conn = GetOpenConnection())
            {
                QuizResult quizResponse = new QuizResult();
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@ResourceId", resourceId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[School].[GetQuizResultByUserId]", parameters, null, null, CommandType.StoredProcedure);
                var quizResponseList = await result.ReadAsync<QuizResult>();
                var questionAnswerList = await result.ReadAsync<QuizAnswersView>();
                var questionFeedbackList = await result.ReadAsync<File>();
                var questionAnswerFiles= await result.ReadAsync<QuizQuestionAnswerFiles>();
                if (quizResponseList.Any())
                {
                    quizResponse = quizResponseList.FirstOrDefault();
                    quizResponse.ResponseQuestionAnswer = questionAnswerList.ToList();
                    quizResponse.QuizFeedbacks = questionFeedbackList.ToList();
                    quizResponse.QuizQuestionAnswerFiles = questionAnswerFiles.ToList();
                }
                return quizResponse;
            }
        }
        public async Task<IEnumerable<QuizView>> GetQuizByCourse(string courseIds, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CourseIds", courseIds==null?String.Empty:courseIds, DbType.String);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<QuizView>("[School].[GetQuizByCourse]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<QuizResult> GetLogResultQuestionAnswer(int quizId, int userId, int resourceId, string resourceType)
        {
            using (var conn = GetOpenConnection())
            {
                QuizResult quizResponse = new QuizResult();
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@ResourceId", resourceId, DbType.Int32);
                parameters.Add("@ResourceType", resourceType, DbType.String);
                var result = await conn.QueryMultipleAsync("[School].[GetLogResultQuestionAnswer]", parameters, null, null, CommandType.StoredProcedure);
                var quizResponseList = await result.ReadAsync<QuizResult>();
                var questionAnswerList = await result.ReadAsync<QuizAnswersView>();
                var questionFeedbackList = await result.ReadAsync<File>();
                if (questionAnswerList.Any())
                {
                    quizResponse.ResponseQuestionAnswer = questionAnswerList.ToList();
                }
                return quizResponse;
            }
        }

        public async Task<GroupQuiz> GetQuizStudentDetailsByQuizId(int groupQuizId, int groupId, int selectedGroupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupQuizId", groupQuizId, DbType.Int32);
                parameters.Add("@GroupId", groupId, DbType.Int32);
                parameters.Add("@SelectedGroupId", selectedGroupId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[School].[GetQuizStudentDetailsByQuizId]", parameters, null, null, CommandType.StoredProcedure);
                var quiz = await result.ReadAsync<GroupQuiz>();
                var quizStudentList = await result.ReadAsync<GroupStudent>();
                var genderWiseReport = await result.ReadAsync<GenderWiseReport>();
                var gradeWiseReport = await result.ReadAsync<GradeWiseReport>();
                var gradeWiseTotalQuestionsReport = await result.ReadAsync<GradeWiseTotalQuestionsReport>();
                var questionWiseReport = await result.ReadAsync<QuestionWiseReport>();
                var quizDetails = quiz.FirstOrDefault();
                quizDetails.lstGroupStudents = quizStudentList.ToList();
                quizDetails.GenderWiseReport = genderWiseReport.FirstOrDefault();
                quizDetails.GradeWiseReport = gradeWiseReport.ToList();
                quizDetails.GradeWiseTotalQuestionsReport = gradeWiseTotalQuestionsReport.FirstOrDefault();
                quizDetails.QuestionWiseReport = questionWiseReport.ToList();
                return quizDetails;
            }
        }


        public async Task<GroupQuiz> GetFormStudentDetailsByQuizId(int QuizId, int groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", QuizId, DbType.Int32);
                parameters.Add("@GroupId", groupId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[School].[GetFormStudentDetailsByQuizId]", parameters, null, null, CommandType.StoredProcedure);
                var quiz = await result.ReadAsync<GroupQuiz>();
                var quizStudentList = await result.ReadAsync<GroupStudent>();
                var quizDetails = quiz.FirstOrDefault();
                quizDetails.lstGroupStudents = quizStudentList.ToList();
                return quizDetails;
            }
        }

    }
}
