﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class BookmarkRepository : SqlRepository<Bookmark>, IBookmarkRepository
    {
        private readonly IConfiguration _config;
        public BookmarkRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public bool AddUpdateBookmark(Bookmark entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBookmarkParameters(entity, mode);
                conn.Query<int>("School.BookmarkCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }   
        }


        public async Task<Bookmark> GetBookmark(long bookmarkId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BookmarkId", bookmarkId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<Bookmark>("[School].[GetBookmark]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Bookmark>> GetBookmarks(int userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int32);
                return await conn.QueryAsync<Bookmark>("[School].[GetBookmarks]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolGroup>> GetBookmarkSchoolGroups(long bookmarkId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BookmarkId", bookmarkId, DbType.Int32);
                return await conn.QueryAsync<SchoolGroup>("School.GetBookmarkSharedGroups", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

    


        public override Task<IEnumerable<Bookmark>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<Bookmark> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(Bookmark entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(Bookmark entityToUpdate)
        {
            throw new NotImplementedException();
        }

        #region Private Methods
        private DynamicParameters GetBookmarkParameters(Bookmark entity, TransactionModes mode)
        {         
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@BookmarkId", entity.BookmarkId, DbType.Int32);
            parameters.Add("@BookmarkName", entity.BookmarkName, DbType.String);
            parameters.Add("@BookmarkDescription", entity.BookmarkDescription, DbType.String);
            parameters.Add("@URL", entity.URL, DbType.String);
            parameters.Add("@ThumbnailImage", entity.ThumbnailImage, DbType.Int32);
            parameters.Add("@UserId", entity.UserId, DbType.Int32);
            parameters.Add("@SchoolGroupIds",entity.SelectedSchoolGroupIds, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion
    }
}
