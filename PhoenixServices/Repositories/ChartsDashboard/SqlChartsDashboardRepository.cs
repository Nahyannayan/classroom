﻿// <summary>
// Created By: Raj M. Gogri
// Created On: 03/Oct/2020
// </summary>
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using static Phoenix.API.Models.ChartsDashboard.ChartModel;

namespace Phoenix.API.Repositories.ChartsDashboard.StudentProgressTracker
{
    /// <summary>
    /// Sql repository class for Charts dashboard database operations.
    /// </summary>
    public class SqlChartsDashboardRepository : SqlRepository<Models.ChartsDashboard.ChartModel.FilterReq>, IChartsDashboardRepository
    {
        public SqlChartsDashboardRepository(IConfiguration configuration) : base(configuration)
        {
            var aymap = new CustomPropertyTypeMap(typeof(
                AcademicYear), (type, columnName)
      => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(AcademicYear), aymap);

            var clmap = new CustomPropertyTypeMap(typeof(
               Class), (type, columnName)
     => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(Class), clmap);

            var subMap = new CustomPropertyTypeMap(typeof(
              FilSubject), (type, columnName)
    => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(FilSubject), subMap);

            var assMap = new CustomPropertyTypeMap(typeof(
              Assessment), (type, columnName)
    => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(Assessment), assMap);

            var teaMap = new CustomPropertyTypeMap(typeof(Phoenix.API.Models.ChartsDashboard.ChartModel.Teacher), (type, columnName)
    => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(Phoenix.API.Models.ChartsDashboard.ChartModel.Teacher), teaMap);

            var avgPredMap = new CustomPropertyTypeMap(typeof(AveragePredictionFigures), (type, columnName)
    => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(AveragePredictionFigures), avgPredMap);

            var avgAssPredByYearMap = new CustomPropertyTypeMap(typeof(AverageAssessmentPredictionByYear), (type, columnName)
    => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(AverageAssessmentPredictionByYear), avgAssPredByYearMap);

            var predBuck = new CustomPropertyTypeMap(typeof(PredictionBucket), (type, columnName)
   => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(PredictionBucket), predBuck);

            var predBySub = new CustomPropertyTypeMap(typeof(PredictionBySubject), (type, columnName)
   => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(PredictionBySubject), predBySub);

            var stuFilter = new CustomPropertyTypeMap(typeof(FilStudent), (type, columnName)
   => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(FilStudent), stuFilter);

            var assdatabystu = new CustomPropertyTypeMap(typeof(AssessmentDataByStudent), (type, columnName)
   => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(AssessmentDataByStudent), assdatabystu);

            var avgPredScore = new CustomPropertyTypeMap(typeof(AveragePredictionScore), (type, columnName)
  => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(AveragePredictionScore), avgPredScore);

            var predBuckByStu = new CustomPropertyTypeMap(typeof(PredictionBucketByStudent), (type, columnName)
  => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(PredictionBucketByStudent), predBuckByStu);

            var predBuckBySubByStu = new CustomPropertyTypeMap(typeof(PredictionBySubjectByStudent), (type, columnName)
  => type.GetProperties().FirstOrDefault(prop => CommonHelper.GetColumnAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(PredictionBySubjectByStudent), predBuckBySubByStu);
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<Models.ChartsDashboard.ChartModel.FilterReq>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<Models.ChartsDashboard.ChartModel.FilterReq> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<Tuple<IEnumerable<AcademicYear>, IEnumerable<Class>, IEnumerable<FilSubject>, IEnumerable<Assessment>, IEnumerable<Models.ChartsDashboard.ChartModel.Teacher>>> GetFiltersAsync(string BSUID)
        {
            using (var conn = OpenConnectionForChartDashboardDB())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSUID", BSUID, DbType.String);
                var result = await conn.QueryMultipleAsync("[CLASSROOM].[GETSUMMARYFILTERS]", parameters, null, null, CommandType.StoredProcedure);
                var AcademicYears = (await result.ReadAsync<AcademicYear>()).Select(ay =>
                {
                    ay.Id = ay.Name;
                    return ay;
                });

                var Classes = await result.ReadAsync<Class>();

                var Subjects = (await result.ReadAsync<FilSubject>()).Select(sub =>
                {
                    sub.Id = sub.Name;
                    return sub;
                });

                var Assessments = (await result.ReadAsync<Assessment>()).Select(ass =>
                {
                    ass.Id = ass.Name;
                    return ass;
                });

                var Teachers = await result.ReadAsync<Phoenix.API.Models.ChartsDashboard.ChartModel.Teacher>();

                return Tuple.Create(AcademicYears, Classes, Subjects, Assessments, Teachers);
            }
        }

        //public Task<AveragePredictionFigures> GetAveragePredictionFiguresAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string teacher)
        //{
        //    using (var conn = OpenConnectionForChartDashboardDB())
        //    {
        //        var parameters = new DynamicParameters();
        //        parameters.Add("@BSUID", BSUID, DbType.String);
        //        parameters.Add("@ACADEMICYEAR", academicYear, DbType.String);
        //        parameters.Add("@SUBJECT", subject, DbType.String);
        //        parameters.Add("@TEACHER", teacher, DbType.String);
        //        parameters.Add("@CLASSID", classid, DbType.String);
        //        parameters.Add("@ASSESSMENT", assessment, DbType.String);

        //        return conn.QueryFirstAsync<AveragePredictionFigures>("[CLASSROOM].[GETOVERALLPREDICTIONDATA]", parameters, null, null, CommandType.StoredProcedure);
        //    }
        //}

        //public Task<IEnumerable<AverageAssessmentPredictionByYear>> GetAverageAssessmentAndPredictionByYearAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string teacher)
        //{
        //    using (var conn = OpenConnectionForChartDashboardDB())
        //    {
        //        var parameters = new DynamicParameters();
        //        parameters.Add("@BSUID", BSUID, DbType.String);
        //        parameters.Add("@ACADEMICYEAR", academicYear, DbType.String);
        //        parameters.Add("@SUBJECT", subject, DbType.String);
        //        parameters.Add("@TEACHER", teacher, DbType.String);
        //        parameters.Add("@CLASSID", classid, DbType.String);
        //        parameters.Add("@ASSESSMENT", assessment, DbType.String);

        //        return conn.QueryAsync<AverageAssessmentPredictionByYear>("[CLASSROOM].[GETAVERAGEASSESSMENTBYYEAR]", parameters, null, null, CommandType.StoredProcedure);
        //    }
        //}

        //public Task<IEnumerable<AverageAssessmentPredictionByYear>> GetAverageMarksByYearAndAssessmentAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string teacher)
        //{
        //    using (var conn = OpenConnectionForChartDashboardDB())
        //    {
        //        var parameters = new DynamicParameters();
        //        parameters.Add("@BSUID", BSUID, DbType.String);
        //        parameters.Add("@ACADEMICYEAR", academicYear, DbType.String);
        //        parameters.Add("@SUBJECT", subject, DbType.String);
        //        parameters.Add("@TEACHER", teacher, DbType.String);
        //        parameters.Add("@CLASSID", classid, DbType.String);

        //        return conn.QueryAsync<AverageAssessmentPredictionByYear>("[CLASSROOM].[GETAVERAGEMARKSBYYEARANDASSESSMENT]", parameters, null, null, CommandType.StoredProcedure);
        //    }
        //}

        public async Task<Tuple<AveragePredictionFigures, IEnumerable<AverageAssessmentPredictionByYear>, IEnumerable<AverageAssessmentPredictionByYear>, IEnumerable<PredictionBucket>, IEnumerable<PredictionBySubject>>> GetStudentProgressTrackerChartAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string teacher)
        {
            using (var conn = OpenConnectionForChartDashboardDB())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSUID", BSUID, DbType.String);
                parameters.Add("@ACADEMICYEAR", academicYear, DbType.String);
                parameters.Add("@SUBJECT", subject, DbType.String);
                parameters.Add("@TEACHER", teacher, DbType.String);
                parameters.Add("@CLASSID", classid, DbType.String);
                parameters.Add("@ASSESSMENT", assessment, DbType.String);

                var taskAvgPredFig = conn.QueryFirstAsync<AveragePredictionFigures>("[CLASSROOM].[GETOVERALLPREDICTIONDATA]", parameters, null, null, CommandType.StoredProcedure);

                var taskAvgAssPredByYear = conn.QueryAsync<AverageAssessmentPredictionByYear>("[CLASSROOM].[GETAVERAGEASSESSMENTBYYEAR]", parameters, null, null, CommandType.StoredProcedure);

                var taskPredSub = conn.QueryAsync<PredictionBySubject>("[CLASSROOM].[GETPREDICTIONDATA_BYSUBJECT]", parameters, null, null, CommandType.StoredProcedure);

                var parametersWithoutAss = new DynamicParameters();
                parametersWithoutAss.Add("@BSUID", BSUID, DbType.String);
                parametersWithoutAss.Add("@ACADEMICYEAR", academicYear, DbType.String);
                parametersWithoutAss.Add("@SUBJECT", subject, DbType.String);
                parametersWithoutAss.Add("@TEACHER", teacher, DbType.String);
                parametersWithoutAss.Add("@CLASSID", classid, DbType.String);

                var taskAvgMarksByYearAndAss = conn.QueryAsync<AverageAssessmentPredictionByYear>("[CLASSROOM].[GETAVERAGEMARKSBYYEARANDASSESSMENT]", parametersWithoutAss, null, null, CommandType.StoredProcedure);

                var taskPredBuck = conn.QueryAsync<PredictionBucket>("[CLASSROOM].[GETPREDICTIONBUCKETS]", parametersWithoutAss, null, null, CommandType.StoredProcedure);

                await Task.WhenAll(taskAvgPredFig, taskAvgAssPredByYear, taskAvgMarksByYearAndAss, taskPredBuck);

                return Tuple.Create(taskAvgPredFig.Result, taskAvgAssPredByYear.Result, taskAvgMarksByYearAndAss.Result, taskPredBuck.Result, taskPredSub.Result);
            }
        }

        public async Task<IEnumerable<FilStudent>> GetStudentFiltersAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string teacher)
        {
            using (var conn = OpenConnectionForChartDashboardDB())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSUID", BSUID, DbType.String);
                parameters.Add("@ACADEMICYEAR", academicYear, DbType.String);
                parameters.Add("@SUBJECT", subject, DbType.String);
                parameters.Add("@CLASSID", classid, DbType.String);
                parameters.Add("@ASSESSMENT", assessment, DbType.String);
                parameters.Add("@TEACHER", teacher, DbType.String);

                return await conn.QueryAsync<FilStudent>("[CLASSROOM].[GETSTUDENTFILTERS]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<Tuple<IEnumerable<AverageAssessmentPredictionByYear>, IEnumerable<AverageAssessmentPredictionByYear>, IEnumerable<PredictionBySubjectByStudent>, AveragePredictionScore>> GetStudentAssessmentPredictionAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string student, string teacher)
        {
            using (var conn = OpenConnectionForChartDashboardDB())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSUID", BSUID, DbType.String);
                parameters.Add("@ACADEMICYEAR", academicYear, DbType.String);
                parameters.Add("@SUBJECT", subject, DbType.String);
                parameters.Add("@STUDENTID", student, DbType.String);
                parameters.Add("@CLASSID", classid, DbType.String);
                parameters.Add("@ASSESSMENT", assessment, DbType.String);
                parameters.Add("@TEACHER", teacher, DbType.String);

                var taskAvgAssPredByYear = conn.QueryAsync<AverageAssessmentPredictionByYear>("[CLASSROOM].[GETAVERAGEASSESSMENTBYYEAR_BYSTUDENT]", parameters, null, null, CommandType.StoredProcedure);

                var taskPredSub = conn.QueryAsync<PredictionBySubjectByStudent>("[CLASSROOM].[GETPREDICTIONDATA_BYSUBJECT_BYSTUDENT]", parameters, null, null, CommandType.StoredProcedure);

                var parametersWithoutAss = new DynamicParameters();
                parametersWithoutAss.Add("@BSUID", BSUID, DbType.String);
                parametersWithoutAss.Add("@ACADEMICYEAR", academicYear, DbType.String);
                parametersWithoutAss.Add("@SUBJECT", subject, DbType.String);
                parametersWithoutAss.Add("@STUDENTID", student, DbType.String);
                parametersWithoutAss.Add("@CLASSID", classid, DbType.String);
                parametersWithoutAss.Add("@TEACHER", teacher, DbType.String);

                var taskAvgMarksByYearAndAss = conn.QueryAsync<AverageAssessmentPredictionByYear>("[CLASSROOM].[GETAVERAGEMARKSBYYEARANDASSESSMENT_BYSTUDENT]", parametersWithoutAss, null, null, CommandType.StoredProcedure);

                //var taskPredBuck = conn.QueryAsync<PredictionBucket>("[CLASSROOM].[GETPREDICTIONBUCKETS_BYSTUDENT]", parametersWithoutAss, null, null, CommandType.StoredProcedure);

                var taskPredScore = GetAveragePredictionScore(BSUID, academicYear, subject, classid, assessment, student, teacher);

                await Task.WhenAll(taskAvgAssPredByYear, taskAvgMarksByYearAndAss, taskPredSub, taskPredScore);

                return Tuple.Create(taskAvgAssPredByYear.Result, taskAvgMarksByYearAndAss.Result, taskPredSub.Result, taskPredScore.Result);
            }
        }

        public async Task<IEnumerable<AssessmentDataByStudent>> GetAllAssessmentDataAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string student, string teacher)
        {
            using (var conn = OpenConnectionForChartDashboardDB())
            {
                var parametersWithoutAss = new DynamicParameters();
                parametersWithoutAss.Add("@BSUID", BSUID, DbType.String);
                parametersWithoutAss.Add("@ACADEMICYEAR", academicYear, DbType.String);
                parametersWithoutAss.Add("@SUBJECT", subject, DbType.String);
                parametersWithoutAss.Add("@STUDENTID", student, DbType.String);
                parametersWithoutAss.Add("@CLASSID", classid, DbType.String);
                parametersWithoutAss.Add("@TEACHER", teacher, DbType.String);

                return await conn.QueryAsync<AssessmentDataByStudent>("[CLASSROOM].[GETALLASSESSMENTDATA_BYSTUDENT]", parametersWithoutAss, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<AveragePredictionScore> GetAveragePredictionScore(string BSUID, string academicYear, string subject, string classid, string assessment, string student, string teacher)
        {
            using (var conn = OpenConnectionForChartDashboardDB())
            {
                var parametersWithoutAss = new DynamicParameters();
                parametersWithoutAss.Add("@BSUID", BSUID, DbType.String);
                parametersWithoutAss.Add("@ACADEMICYEAR", academicYear, DbType.String);
                parametersWithoutAss.Add("@SUBJECT", subject, DbType.String);
                parametersWithoutAss.Add("@STUDENTID", student, DbType.String);
                parametersWithoutAss.Add("@CLASSID", classid, DbType.String);
                parametersWithoutAss.Add("@TEACHER", teacher, DbType.String);

                return await conn.QueryFirstAsync<AveragePredictionScore>("[CLASSROOM].[GETAVERAGEPREDICYTIONSCORE_BYSTUDENT]", parametersWithoutAss, null, null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(Models.ChartsDashboard.ChartModel.FilterReq entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(Models.ChartsDashboard.ChartModel.FilterReq entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}