﻿// <summary>
// Created By: Raj M. Gogri
// Created On: 02/Oct/2020
// </summary>
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Phoenix.API.Models.ChartsDashboard.ChartModel;

namespace Phoenix.API.Repositories.ChartsDashboard
{
    /// <summary>
    /// Contract for Charts dashboard repository(data source) that repository class must implement.
    /// </summary>
    public interface IChartsDashboardRepository
    {
        /// <summary>
        /// Get filters for Charts dashboard.
        /// </summary>
        /// <param name="BSUID">Business unit id of school.</param>
        /// <returns>Tuple with filters for Charts dashboard.</returns>
        Task<Tuple<IEnumerable<AcademicYear>, IEnumerable<Class>, IEnumerable<FilSubject>, IEnumerable<Assessment>, IEnumerable<Models.ChartsDashboard.ChartModel.Teacher>>> GetFiltersAsync(string BSUID);

        ///// <summary>
        ///// Get average prediction figures for Charts dashboard.
        ///// </summary>
        ///// <param name="BSUID">Business unit id of school.</param>
        ///// <param name="academicYear">Academic year filter.</param>
        ///// <param name="subject">Subject filter.</param>
        ///// <param name="classid">Class filter.</param>
        ///// <param name="assessment">Assessment filter.</param>
        ///// <param name="teacher">Teacher filter.</param>
        ///// <returns>AveragePredictionFigures with average and prediction data for Charts dashboard.</returns>
        //Task<AveragePredictionFigures> GetAveragePredictionFiguresAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string teacher);

        ///// <summary>
        ///// Get average assessment and prediction figures by year for Charts dashboard chart.
        ///// </summary>
        ///// <param name="BSUID">Business unit id of school.</param>
        ///// <param name="academicYear">Academic year filter.</param>
        ///// <param name="subject">Subject filter.</param>
        ///// <param name="classid">Class filter.</param>
        ///// <param name="assessment">Assessment filter.</param>
        ///// <param name="teacher">Teacher filter.</param>
        ///// <returns>List of AverageAssessmentPredictionByYear.</returns>
        //Task<IEnumerable<AverageAssessmentPredictionByYear>> GetAverageAssessmentAndPredictionByYearAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string teacher);

        ///// <summary>
        ///// Get average marks by year and assessment for Charts dashboard chart.
        ///// </summary>
        ///// <param name="BSUID">Business unit id of school.</param>
        ///// <param name="academicYear">Academic year filter.</param>
        ///// <param name="subject">Subject filter.</param>
        ///// <param name="classid">Class filter.</param>
        ///// <param name="assessment">Assessment filter.</param>
        ///// <param name="teacher">Teacher filter.</param>
        ///// <returns>List of AverageAssessmentPredictionByYear.</returns>
        //Task<IEnumerable<AverageAssessmentPredictionByYear>> GetAverageMarksByYearAndAssessmentAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string teacher);

        /// <summary>
        /// Get charts for StudentProgressTracker Chart dashboard.
        /// </summary>
        /// <param name="BSUID">Business unit id of school.</param>
        /// <param name="academicYear">Academic year filter.</param>
        /// <param name="subject">Subject filter.</param>
        /// <param name="classid">Class filter.</param>
        /// <param name="assessment">Assessment filter.</param>
        /// <param name="teacher">Teacher filter.</param>
        /// <returns>Tuple with charts data for Charts dashboard.</returns>
        Task<Tuple<AveragePredictionFigures, IEnumerable<AverageAssessmentPredictionByYear>, IEnumerable<AverageAssessmentPredictionByYear>, IEnumerable<PredictionBucket>, IEnumerable<PredictionBySubject>>> GetStudentProgressTrackerChartAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string teacher);

        /// <summary>
        /// Get student filters for Charts dashboard.
        /// </summary>
        /// <param name="BSUID">Business unit id of school.</param>
        /// <param name="academicYear">Academic year filter.</param>
        /// <param name="subject">Subject filter.</param>
        /// <param name="classid">Class filter.</param>
        /// <param name="assessment">Assessment filter.</param>
        /// <param name="teacher">Teacher filter.</param>
        /// <returns>List of FilStudent.</returns>
        Task<IEnumerable<FilStudent>> GetStudentFiltersAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string teacher);

        /// <summary>
        /// Get charts for StudentAssessmentPrediction Chart dashboard.
        /// </summary>
        /// <param name="BSUID">Business unit id of school.</param>
        /// <param name="academicYear">Academic year filter.</param>
        /// <param name="subject">Subject filter.</param>
        /// <param name="classid">Class filter.</param>
        /// <param name="assessment">Assessment filter.</param>
        /// <param name="student">Student filter.</param>
        /// <param name="teacher">Teacher filter.</param>
        /// <returns>Tuple with charts data for Charts dashboard.</returns>
        Task<Tuple<IEnumerable<AverageAssessmentPredictionByYear>, IEnumerable<AverageAssessmentPredictionByYear>, IEnumerable<PredictionBySubjectByStudent>, AveragePredictionScore>> GetStudentAssessmentPredictionAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string student, string teacher);

        /// <summary>
        /// Get all assessment data of table for student predication and assessment chart dashboard .
        /// </summary>
        /// <param name="BSUID">Business unit id of school.</param>
        /// <param name="academicYear">Academic year filter.</param>
        /// <param name="subject">Subject filter.</param>
        /// <param name="classid">Class filter.</param>
        /// <param name="assessment">Assessment filter.</param>
        /// <param name="student">Student filter.</param>
        /// <param name="teacher">Teacher filter.</param>
        /// <returns>List of AssessmentDataByStudent.</returns>
        Task<IEnumerable<AssessmentDataByStudent>> GetAllAssessmentDataAsync(string BSUID, string academicYear, string subject, string classid, string assessment, string student, string teacher);

        /// <summary>
        /// Get average prediction score for student predication and assessment chart dashboard .
        /// </summary>
        /// <param name="BSUID">Business unit id of school.</param>
        /// <param name="academicYear">Academic year filter.</param>
        /// <param name="subject">Subject filter.</param>
        /// <param name="classid">Class filter.</param>
        /// <param name="assessment">Assessment filter.</param>
        /// <param name="student">Student filter.</param>
        /// <param name="teacher">Teacher filter.</param>
        /// <returns>AveragePredictionScore.</returns>
        Task<AveragePredictionScore> GetAveragePredictionScore(string BSUID, string academicYear, string subject, string classid, string assessment, string student, string teacher);
    }
}