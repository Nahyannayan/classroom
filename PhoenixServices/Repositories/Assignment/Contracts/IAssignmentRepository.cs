﻿using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IAssignmentRepository
    {

        Task<Assignment> GetAssignments(int teacherId);
        Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "");
        Task<IEnumerable<AssignmentStudentDetails>> GetArchivedAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "");
        Task<IEnumerable<AssignmentStudentDetails>> GeStudentFilterSchoolGroupsById(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "", string filterVal = "");
        Task<IEnumerable<AssignmentStudentDetails>> GetArchivedStudentFilterSchoolGroupsById(int PageNumber, int PageSize, long studentId, string SearchString = "", string assignmentType = "", string sortBy = "", string filterVal = "");
        Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentIdWithoutPagination(long studentId, string assignmentType = "");
        Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentIdWithDateRange(int PageNumber, int PageSize, long studentId, DateTime startDate, DateTime endDate, string SearchString = "", string assignmentType = "");
        Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentIdAndTeacherIdWithDateRange(int PageNumber, int PageSize, long studentId, long teacherId, DateTime startDate, DateTime endDate, string searchString = "", string assignmentType = "");
        Task<Assignment> GetAsync(int id);
        Task<AssignmentStudent> GetAssignmentSubmitMarksByAssignmentStudentId(int assignmentStudentId);
        Task<StudentTask> GetTaskSubmitMarksByTaskId(int taskId);
        Task<AssignmentCategoryDetails> GetAssignmentCategoryMasterById(int id);
        Task<Assignment> GetExcelReportDataAssignmentById(int id);
        int UpdateAssignmentData(Assignment entity, TransactionModes mode);
        bool DeleteAssignmentComment(AssignmentComment entity);

        int SaveAssignmentData(Assignment entity, TransactionModes mode);

        Task<IEnumerable<AssignmentStudent>> GetAssignmentStudent();
        Task<IEnumerable<AssignmentCategoryDetails>> GetAssignmentCategoryBySchoolId(long SchoolId);

        Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentStudentDetails(int TeacherId, AssignmentFilter loadAssignment);

        Task<IEnumerable<AssignmentStudentDetails>> GetAdminAssignmentStudentDetails(AssignmentFilter loadAssignment);
        Task<IEnumerable<AssignmentStudentDetails>> GetFilterSchoolGroupsById(int TeacherId, AssignmentFilter loadAssignment);
        Task<IEnumerable<AssignmentStudentDetails>> GetArchivedFilterSchoolGroupsById(int TeacherId, AssignmentFilter loadAssignment);
        Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentStudentDetailsById(int Id);
        Task<IEnumerable<AssignmentTask>> GetAssignmentTasksByAssignmentId(int assignmentId);
        Task<AssignmentTask> GetTaskQuizDetailById(int taskId);
        bool AssignGradeToStudentAssignment(int studentAssignmentId, int gradingTemplateItemId, int GradedBy);
        bool GetAssignmentPermissions(int assignmentId, long userId);
        bool AssignmentUnArchive(int assignmentId);
        bool AddEditAssignmentCategory(int AssignmentCategoryId, bool IsActive,long SchoolId);
        bool AddAssignmentCategoryMaster(string AssignmentCategoryTitle, string AssignmentCategoryDesc, int CategoryId, bool IsActive, long SchoolId);
        bool SeenStudentAssignmentComment(AssignmentComment objAssignmentComment);
        Task<IEnumerable<AssignmentFile>> GetFilesByAssignmentId(int assignmentId);
        Task<IEnumerable<AssignmentFile>> GetSavedAssignmentFilesByAssignmentId(int assignmentId, long userId);
        Task<bool> SaveAssignmentFiles(List<AssignmentFile> lstModel);
        Task<IEnumerable<TaskFile>> GetFilesByTaskId(int taskId);
        Task<AssignmentStudent> GetStudentAsgHomeworkDetailsById(int assignmentStudentId, int IsSeenByStudent = 0);
        Task<AssignmentTask> GetAssignmentTaskbyTaskId(int taskId);
        Task<IEnumerable<SchoolGroup>> GetSelectegGroupsByAssignmentId(int assignmentId);
        Task<IEnumerable<Course>> GetSelectedCoursesByAssignmentId(int assignmentId);
        Task<IEnumerable<AssignmentStudent>> GetStudentsByAssignmentId(int assignmentId,int GroupId);
        Task<IEnumerable<AssignmentStudent>> GetAssignmentCompletedStudentsByAssignmentId(int assignmentId);
        Task<AssignmentReport> GetExcelReportDataStudentsByAssignmentId(int assignmentId);
        bool MarkAsComplete(int assignmentStudentId, bool isTeacher, int CompletedBy);
        Task<GradingTemplateItem> SubmitAssignmentMarks(int assignmentStudentId, decimal submitMarks,int CompletedBy,int gradingTemplateId,int SystemLanguageId);
        bool UploadStudentAssignmeentFiles(List<StudentAssignmentFile> lstStudentAssignmentFiles, int studentId, int assignmentId, string IpDetails);
        Task<IEnumerable<StudentAssignmentFile>> GetStudentAssignmentFiles(int studentId, int assignmentId);
        Task<IEnumerable<AssignmentFeedbackFiles>> GetAssignmentFeedbackFiles(int assignmentStudentId, int assignmentCommentId);
        bool MarkAsInComplete(int studentId, int assignmentId, bool isReSubmitAssignnment);
        bool UploadStudentTaskFiles(List<StudentTaskFile> lststudentTaskFiles, int studentId, int taskId, string IpDetails);

        Task<IEnumerable<StudentTaskFile>> GetStudentTaskFiles(int studentId, int taskid);
        Task<StudentTask> GetStudentTaskDetails(int taskId, int studentId);
        bool InsertAssignmentComment(AssignmentComment entity, DataTable assignmentFeedbackFile);
        Task<AssignmentTask> GetStudentTaskDetailById(int taskId, int studentId, int assignmentStudentId, long studentTaskId);
        Task<IEnumerable<AssignmentComment>> GetAssignmentComments(int assignmentStudentId);
        bool MarkAsCompleteTask(int studentTaskId, bool isTeacher, int CompletedBy);
        Task<GradingTemplateItem> SubmitTaskMarks(int studentTaskId,int StudentAssignmentId, decimal submitMarks, int CompletedBy, int gradingTemplateId, int SystemLanguageId);
        bool AssignGradeToStudentTask(int studentTaskId, int gradingTemplateItemId, int GradedBy);
        bool AssignGradeToStudentObjective(int assignmentStudentId, int gradingTemplateItemId);
        Task<IEnumerable<MyFilesTreeItem>> GetMyFilesStructure(long userId);
        Task<IEnumerable<AssignmentFile>> GetAssignmentMyFile(long id, bool isFolder);
        bool DeleteUploadedFiles(int FileId, int AssignmentFileId, long UserId);
        bool DeleteStudentAssignmentFile(int FileId, long UserId, bool IsTaskFile);
        Task<File> GetAssignmentFilebyAssignmentFileId(long id);
        Task<File> GetTaskFile(long id);
        Task<File> GetStudentTaskFile(long id);
        Task<File> GetStudentAssignmentFilebyStudentAsgFileId(long id);
        bool UpdateArchiveAssignment(long AssignmentId);
        bool InsertTaskFeedback(AssignmentTask assignmentTask, DataTable recordings);
        bool InsertStudentObjectiveFeedback(AssignmentStudentObjective assignmentTask, DataTable recordings);
        Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentListForReport(int pageNumber, int pageSize, string searchString, long userId, long schoolId, int schoolGroupId, DateTime startDate, DateTime endDate, string userType);

        Task<IEnumerable<AssignmentStudentDetails>> GetArchivedAssignmentStudentDetails(int TeacherId, AssignmentFilter loadAssignment);
        Task<IEnumerable<AssignmentStudentDetails>> GetAdminArchivedAssignmentStudentDetails(AssignmentFilter loadAssignment);
        Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByUserId(int pageNumber, int pageSize, long userId, string searchString);

        bool AddUpdatePeerReviewMapping(List<AssignmentPeerReview> lstPeerReviewMapping);
        Task<IEnumerable<AssignmentPeerReview>> GetPeerAssignmentDetails(long assignmentId, long userId);
        Task<IEnumerable<AssignmentPeerReview>> GetPeerMappingDetails(long assignmentId);
        int AddUpdateDocumentReviewDetails(List<DocumentReviewdetails> lstDocumentReview, long userId);
        Task<IEnumerable<DocumentReviewdetails>> GetReviewedDocumentFiles(long peerReviewId);
        Task<DocumentReviewdetails> GetDocumentFileByStudAsgFileId(long stdAsgFileId);
        Task<AssignmentCounts> GetAssignmentStatusCounts(long id, bool isTeacher, string searchText);
        bool MarkAsCompleteReview(long peerReviewId, long reviewedBy);
        bool MarkAsCompleteTaskReview(long peerReviewId, long reviewedBy);
        Task<AssignmentPeerReview> GetPeerAssignmentDetail(long peerReviewId);
        int AddUpdateTaskDocumentReviewDetails(List<TaskDocumentReviewdetails> lsttaskDocumentReview, long userId);
        Task<TaskPeerReview> GetPeerTaskDetail(long studentId, long taskId);
        Task<IEnumerable<TaskPeerReview>> GetPeerTaskDetails(long assignmentId, long studentId);
        Task<TaskDocumentReviewdetails> GetTaskDocumentFileByStudTaskFileId(long stdTaskFileId);
        Task<IEnumerable<TaskDocumentReviewdetails>> GetTaskReviewedDocumentFiles(long taskPeerReviewId);
        Task<AssignmentPeerReview> GetPeerAssignmentDetailByAssignmentStudentId(long assignmentStudentId);
        Task<IEnumerable<Assignment>> GetGroupAssignments(long schoolGroupId);
        Task<IEnumerable<AssignmentStudent>> GetStudentGroupAssignments(long schoolGroupId, long StudentId);
        Task<AssignmentDetails> GetStudentAssignmentDetailsById(int assignmentStudentId);
        Task<IEnumerable<Assignment>> GetDashboardAssignmentOverview(long userId);
        Task<IEnumerable<AssignmentStudentObjective>> GetStudentAssignmentObjectives(long StudentAssignmengtId);
        Task<IEnumerable<File>> GetStudentAssignmentObjectiveAudiofeedback(long studentObjectiveMarkId);
        Task<bool> UploadStudentSharedCopyFiles(List<AssignmentStudentSharedFiles> copiedFiles);
        Task<IEnumerable<QuizQuestionsView>> GetQuizQuestionsByTaskId(int taskId);
        int ShareAssignment(int assignmentId, string teacherIds);
        Task<GroupQuiz> GetAssignmentQuizDetailsByTaskId(int taskId);
        Task<IEnumerable<AssignmentReport>> GetAssignmentTaskQuizByAssignmentId(int assignmentId);
        Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentsForMyPlanner(long studentId, DateTime startDate, DateTime endDate, string assignmentType = "");
        Task<bool> UndoAssignmentsArchive(ArchiveAssignment model);
    }
}
