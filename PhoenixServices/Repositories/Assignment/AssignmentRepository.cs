﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class AssignmentRepository : SqlRepository<Assignment>, IAssignmentRepository
    {
        private readonly IConfiguration _config;
        public AssignmentRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<Assignment> GetAssignments(int teacherId)
        {
            Assignment objAssignment = new Assignment();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TeacherId", teacherId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("Assignment.GetAssignments", parameters, null, null, CommandType.StoredProcedure);
                var listOfArchiveAssignments = await result.ReadAsync<Assignment>();
                var listOfActiveAssignments = await result.ReadAsync<Assignment>();
                objAssignment.lstArchiveAssignments = listOfArchiveAssignments.ToList();
                objAssignment.lstActiveAssignments = listOfActiveAssignments.ToList();
                return objAssignment;
            }
        }

        public async Task<IEnumerable<AssignmentStudent>> GetAssignmentStudent()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<AssignmentStudent>("Assignment.GetAssignmentStudents", commandType: CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<AssignmentCategoryDetails>> GetAssignmentCategoryBySchoolId(long SchoolId)
        {
            var objAssignmentCategoryDetails = new List<AssignmentCategoryDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[Assignment].[GetAssignmentCategoryBySchoolId]", parameters, null, null, CommandType.StoredProcedure);
                var lstAssignmentCategory = await result.ReadAsync<AssignmentCategoryDetails>();
                objAssignmentCategoryDetails = lstAssignmentCategory.ToList();
                return objAssignmentCategoryDetails;
            }
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentStudentDetails(int TeacherId, AssignmentFilter loadAssignment)
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", loadAssignment.page, DbType.Int32);
                parameters.Add("@PageSize", loadAssignment.size, DbType.Int32);
                parameters.Add("@intTeacherId", TeacherId, DbType.Int32);
                parameters.Add("@SearchString", loadAssignment.searchString == null ? "" : loadAssignment.searchString, DbType.String);
                parameters.Add("@AssignmentType", loadAssignment.assignmentType == null ? "" : loadAssignment.assignmentType, DbType.String);
                parameters.Add("CreatedByMeOnly", loadAssignment.CreatedByMeOnly, DbType.Boolean);
                parameters.Add("@SortBy", string.IsNullOrEmpty(loadAssignment.sortBy) ? (object)DBNull.Value : loadAssignment.sortBy, DbType.String);
                //parameters.Add("@TeacherId", string.IsNullOrEmpty(loadAssignment.teacherVal) ? (object)DBNull.Value : loadAssignment.teacherVal, DbType.String);
                //return await conn.QueryAsync<AssignmentStudentDetails>("[Assignment].[GetAssignmentByPage]", parameters, null, null, CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("[Assignment].[GetAssignmentByPage]", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }


        public async Task<IEnumerable<AssignmentStudentDetails>> GetAdminAssignmentStudentDetails(AssignmentFilter loadAssignment)
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", loadAssignment.page, DbType.Int32);
                parameters.Add("@PageSize", loadAssignment.size, DbType.Int32);
                parameters.Add("@SearchString", loadAssignment.searchString == null ? "" : loadAssignment.searchString, DbType.String);
                parameters.Add("@AssignmentType", loadAssignment.assignmentType == null ? "" : loadAssignment.assignmentType, DbType.String);
                parameters.Add("@CreatedByMeOnly", loadAssignment.CreatedByMeOnly, DbType.Boolean);
                parameters.Add("@SortBy", string.IsNullOrEmpty(loadAssignment.sortBy) ? (object)DBNull.Value : loadAssignment.sortBy, DbType.String);
                parameters.Add("@teacherIds", string.IsNullOrEmpty(loadAssignment.teacherVal) ? (object)DBNull.Value : loadAssignment.teacherVal, DbType.String);
                parameters.Add("@GroupIds", string.IsNullOrEmpty(loadAssignment.filterVal) ? (object)DBNull.Value : loadAssignment.filterVal, DbType.String);
                parameters.Add("@CourseIds", string.IsNullOrEmpty(loadAssignment.courseVal) ? (object)DBNull.Value : loadAssignment.courseVal, DbType.String);
                parameters.Add("@SchoolId", loadAssignment.schoolId, DbType.Int64);
                var result = await conn.QueryMultipleAsync("[Assignment].[GetAdminAssignmentPage]", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                return assignmentStudentDetails;
            }
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetFilterSchoolGroupsById(int TeacherId, AssignmentFilter loadAssignment)
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", loadAssignment.page, DbType.Int32);
                parameters.Add("@PageSize", loadAssignment.size, DbType.Int32);
                parameters.Add("@intTeacherId", TeacherId, DbType.Int32);
                parameters.Add("@SearchString", loadAssignment.searchString == null ? "" : loadAssignment.searchString, DbType.String);
                parameters.Add("@AssignmentType", loadAssignment.assignmentType == null ? "" : loadAssignment.assignmentType, DbType.String);
                parameters.Add("CreatedByMeOnly", loadAssignment.CreatedByMeOnly, DbType.Boolean);
                parameters.Add("@SortBy", string.IsNullOrEmpty(loadAssignment.sortBy) ? (object)DBNull.Value : loadAssignment.sortBy, DbType.String);
                parameters.Add("@SchoolGroupId", string.IsNullOrEmpty(loadAssignment.filterVal) ? (object)DBNull.Value : loadAssignment.filterVal, DbType.String);
                //parameters.Add("@TeacherId", string.IsNullOrEmpty(loadAssignment.teacherVal) ? (object)DBNull.Value : loadAssignment.teacherVal, DbType.String);
                //return await conn.QueryAsync<AssignmentStudentDetails>("[Assignment].[GetAssignmentByPage]", parameters, null, null, CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("[Assignment].[FilterSchoolGroupsById]", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }


        public async Task<IEnumerable<AssignmentStudentDetails>> GetArchivedFilterSchoolGroupsById(int TeacherId, AssignmentFilter loadAssignment)
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", loadAssignment.page, DbType.Int32);
                parameters.Add("@PageSize", loadAssignment.size, DbType.Int32);
                parameters.Add("@intTeacherId", TeacherId, DbType.Int32);
                parameters.Add("@SearchString", loadAssignment.searchString == null ? "" : loadAssignment.searchString, DbType.String);
                parameters.Add("@AssignmentType", loadAssignment.assignmentType == null ? "" : loadAssignment.assignmentType, DbType.String);
                parameters.Add("CreatedByMeOnly", loadAssignment.CreatedByMeOnly, DbType.Boolean);
                parameters.Add("@SortBy", string.IsNullOrEmpty(loadAssignment.sortBy) ? (object)DBNull.Value : loadAssignment.sortBy, DbType.String);
                parameters.Add("@SchoolGroupId", string.IsNullOrEmpty(loadAssignment.filterVal) ? (object)DBNull.Value : loadAssignment.filterVal, DbType.String);
                //parameters.Add("@TeacherId", string.IsNullOrEmpty(loadAssignment.teacherVal) ? (object)DBNull.Value : loadAssignment.teacherVal, DbType.String);
                //return await conn.QueryAsync<AssignmentStudentDetails>("[Assignment].[GetAssignmentByPage]", parameters, null, null, CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("[Assignment].[ArchivedFilterSchoolGroupsById]", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }


        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentStudentDetailsById(int Id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", Id, DbType.Int32);
                return await conn.QueryAsync<AssignmentStudentDetails>("[Assignment].[GetAssignmentStudentDetailsById]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int UpdateAssignmentData(Assignment entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAssignmentParameters(entity, mode);
                conn.Query<int>("Assignment.AssignmentCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }


        public bool DeleteAssignmentComment(AssignmentComment entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentCommentId", entity.AssignmentCommentId, DbType.Int32);
                parameters.Add("@DeletedBy", entity.DeletedBy, DbType.Int32);
                parameters.Add("@Result", dbType: DbType.Boolean, direction: ParameterDirection.Output);
                conn.Query<int>("Assignment.DeleteAssignmentComment", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("Result");
            }
        }

        public int ShareAssignment(int assignmentId, string teacherIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                parameters.Add("teacherIds", teacherIds, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[Assignment].[ShareAssignmentWithTeachers]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public int SaveAssignmentData(Assignment entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAssignmentDetailsParameters(entity, mode);
                conn.Query<int>("Assignment.SaveAssignmentDetails", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        public async override Task<Assignment> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<Assignment>("[Assignment].[GetAssignmentByID]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<AssignmentStudent> GetAssignmentSubmitMarksByAssignmentStudentId(int assignmentStudentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentStudentId", assignmentStudentId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<AssignmentStudent>("[Assignment].[GetAssignmentSubmitMarksByAssignmentStudentId]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<StudentTask> GetTaskSubmitMarksByTaskId(int taskId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TaskId", taskId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<StudentTask>("[Assignment].[GetTaskSubmitMarksByTaskId]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<Assignment> GetExcelReportDataAssignmentById(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<Assignment>("[Assignment].[GetExcelReportDataAssignmentByID]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<AssignmentCategoryDetails> GetAssignmentCategoryMasterById(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CategoryId", id, DbType.Int32);
                //parameters.Add("@SchoolId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<AssignmentCategoryDetails>("[Assignment].[GetAssignmentCategoryMasterById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AssignmentTask>> GetAssignmentTasksByAssignmentId(int assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                return await conn.QueryAsync<AssignmentTask>("[Assignment].[GetTasksByAssignmentId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<AssignmentTask> GetTaskQuizDetailById(int taskId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TaskId", taskId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<AssignmentTask>("Assignment.GetTaskQuizDetailById", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public bool AssignGradeToStudentAssignment(int studentAssignmentId, int gradingTemplateItemId, int GradedBy)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssignmentStudentId", studentAssignmentId, DbType.Int32);
            parameters.Add("@GradingTemplateitemId", gradingTemplateItemId, DbType.Int32);
            parameters.Add("@GradedBy", GradedBy, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("Assignment.AssignGradeToStudentAssignment", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public bool GetAssignmentPermissions(int assignmentId, long userId)
        {
            int cUserId = Convert.ToInt32(userId);
            var parameters = new DynamicParameters();
            parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
            parameters.Add("@UserId", cUserId, DbType.Int32);
            parameters.Add("@Result", dbType: DbType.Boolean, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("Assignment.GetAssignmentPermissions", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("Result");
            }
        }


        public bool AssignmentUnArchive(int assignmentId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
            parameters.Add("@Result", dbType: DbType.Boolean, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("Assignment.AssignmentUnArchived", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("Result");
            }
        }

        public bool AddEditAssignmentCategory(int AssignmentCategoryId, bool IsActive, long SchoolId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssignmentCategoryId", AssignmentCategoryId, DbType.Int32);
            parameters.Add("@IsActive", IsActive, DbType.Boolean);
            parameters.Add("@SchoolId", SchoolId, DbType.Int32);
            parameters.Add("@Result", dbType: DbType.Boolean, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("Assignment.AddEditAssignmentCategory", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("Result");
            }
        }


        public bool AddAssignmentCategoryMaster(string AssignmentCategoryTitle, string AssignmentCategoryDesc, int CategoryId, bool IsActive,long SchoolId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssignmentCategoryTitle", AssignmentCategoryTitle, DbType.String);
            parameters.Add("@AssignmentCategoryDesc", AssignmentCategoryDesc, DbType.String);
            parameters.Add("@CategoryId", CategoryId, DbType.Int32);
            parameters.Add("@IsActive", IsActive, DbType.Boolean);
            parameters.Add("@SchoolId", SchoolId, DbType.Int32);
            parameters.Add("@Result", dbType: DbType.Boolean, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("Assignment.AddAssignmentCategoryMaster", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("Result");
            }
        }


        public bool SeenStudentAssignmentComment(AssignmentComment objAssignmentComment)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssignmentCommentIds", objAssignmentComment.CommentIds, DbType.String);
            parameters.Add("@IsCommentSeenByStudent", objAssignmentComment.IsCommentSeenByStudent, DbType.Boolean);
            parameters.Add("@ByTeacher", objAssignmentComment.ByTeacher, DbType.Boolean);
            parameters.Add("@Result", dbType: DbType.Boolean, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("Assignment.SeenStudentAssignmentComment", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("Result");
            }
        }

        public async Task<IEnumerable<AssignmentFile>> GetFilesByAssignmentId(int assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                return await conn.QueryAsync<AssignmentFile>("[Assignment].[GetAssignmentFilesbyAssignmentId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AssignmentFile>> GetSavedAssignmentFilesByAssignmentId(int assignmentId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<AssignmentFile>("[Assignment].[GetSavedAssignmentFilesbyAssignmentId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<bool> SaveAssignmentFiles(List<AssignmentFile> lstModel)
        {
            var assignmentFileIds = string.Join(",", lstModel.Select(e => e.AssignmentFileId));
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentFileIds", assignmentFileIds, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.InsertAssignmentFiles", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;

            }
        }
        public async Task<IEnumerable<TaskFile>> GetFilesByTaskId(int taskId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TaskId", taskId, DbType.Int32);
                return await conn.QueryAsync<TaskFile>("[Assignment].[GetTaskFilesByTaskId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<AssignmentStudent> GetStudentAsgHomeworkDetailsById(int assignmentStudentId, int IsSeenByStudent = 0)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentStudentId", assignmentStudentId, DbType.Int32);
                parameters.Add("@IsSeenByStudent", IsSeenByStudent, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<AssignmentStudent>("Assignment.GetStudentAsgHomeworkDetailsById", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<AssignmentDetails> GetStudentAssignmentDetailsById(int assignmentStudentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentStudentId", assignmentStudentId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<AssignmentDetails>("[Assignment].[GetStudentAssignmentDetailsById]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<AssignmentTask> GetAssignmentTaskbyTaskId(int taskId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TaskId", taskId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<AssignmentTask>("Assignment.GetAssignmentTaskByTaskId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SchoolGroup>> GetSelectegGroupsByAssignmentId(int assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                return await conn.QueryAsync<SchoolGroup>("[Assignment].[GetSchoolGroupsByAssignmentId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Course>> GetSelectedCoursesByAssignmentId(int assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                return await conn.QueryAsync<Course>("[Assignment].[GetSchoolCoursesByAssignmentId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<AssignmentStudent>> GetStudentsByAssignmentId(int assignmentId, int GroupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                parameters.Add("@GroupId", GroupId, DbType.Int32);
                return await conn.QueryAsync<AssignmentStudent>("[Assignment].[GetAssignmentStudentsByAssignmentId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AssignmentStudent>> GetAssignmentCompletedStudentsByAssignmentId(int assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                return await conn.QueryAsync<AssignmentStudent>("[Assignment].[GetAssignmentCompletedStudentsByAssignmentId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<AssignmentReport> GetExcelReportDataStudentsByAssignmentId(int assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var AssignmentReport = new AssignmentReport();
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                //return await conn.QueryAsync<AssignmentStudent>("[Assignment].[GetExcelAssignmentStudentsByAssignmentId]", parameters, null, null, CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("[Assignment].[GetExcelAssignmentStudentsByAssignmentId]", parameters, null, null, CommandType.StoredProcedure);
                var AssignmentStudentData = await result.ReadAsync<AssignmentStudent>();
                var QuizReportQuestions = await result.ReadAsync<QuizReportQuestions>();
                var QuizReportAnswers = await result.ReadAsync<QuizReportAnswers>();
                AssignmentReport.AssignmentStudentData = AssignmentStudentData.ToList();
                AssignmentReport.QuizReportQuestions = QuizReportQuestions.ToList();
                AssignmentReport.QuizReportAnswers = QuizReportAnswers.ToList();
                return AssignmentReport;
            }
        }

        public async Task<bool> UploadStudentSharedCopyFiles(List<AssignmentStudentSharedFiles> files)
        {
            DataTable ds = new DataTable();
            ds.Columns.Add("AssignmentId", typeof(long));
            ds.Columns.Add("StudentId", typeof(int));
            ds.Columns.Add("FilePath", typeof(string));
            ds.Columns.Add("FileName", typeof(string));
            ds.Columns.Add("ResourceFileTypeId", typeof(short));
            ds.Columns.Add("IsCollaboratedFile", typeof(bool));
            foreach (var item in files)
            {
                ds.Rows.Add(item.AssignmentId, item.StudentId, item.FilePath, item.FileName, item.ResourceFileTypeId, item.IsCollaboratedFile);
            }
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileData", ds, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Assignment.InsertStudentSharedCopyFiles", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        //public async Task<AssignmentCounts> GetAssignmentStatusCounts(long id, bool isTeacher, string searchText)
        //{
        //    using (var conn = GetOpenConnection())
        //    {
        //        var parameters = new DynamicParameters();
        //        parameters.Add("@Id", id, DbType.Int64);
        //        parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
        //        parameters.Add("@SearchText", searchText, DbType.String);
        //        return await conn.QueryFirstOrDefaultAsync<AssignmentCounts>("Assignment.GetAssignmentStatusCount", parameters, null, null, CommandType.StoredProcedure);
        //    }
        //}


        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<Assignment>> GetAllAsync()
        {
            throw new NotImplementedException();
        }


        public override void InsertAsync(Assignment entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(Assignment entityToUpdate)
        {
            throw new NotImplementedException();
        }

        #region Private Methods
        private DynamicParameters GetAssignmentParameters(Assignment entity, TransactionModes mode)
        {
            var userId = mode != TransactionModes.Delete ? entity.CreatedById : entity.DeletedBy;
            entity.DueDate = entity.DueDate != null ? entity.DueDate.Value.Date.Add(entity.DueTime != null ? entity.DueTime : TimeSpan.Zero) : (DateTime?)null;
            //entity.DueDate = entity.DueDate.Add(entity.DueTime);
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@AssignmentId", entity.AssignmentId, DbType.Int32);
            parameters.Add("@AssignmentTitle", entity.AssignmentTitle, DbType.String);
            parameters.Add("@AssignmentDesc", entity.AssignmentDesc, DbType.String);

            parameters.Add("@SubmissionType", entity.SubmissionType, DbType.Int32);
            parameters.Add("@StartDate", entity.StartDate, DbType.DateTime);
            parameters.Add("@DueDate", entity.DueDate, DbType.DateTime);
            parameters.Add("@ArchiveDate", entity.ArchiveDate, DbType.DateTime);
            parameters.Add("@IsPublished", entity.IsPublished, DbType.Boolean);
            parameters.Add("@isChangeReverted", entity.isChangeReverted, DbType.Boolean);
            parameters.Add("@IsTaskOrderRequired", entity.IsTaskOrderRequired, DbType.Boolean);
            parameters.Add("@UserId", userId, DbType.Int32);
            parameters.Add("@StudentIds", entity.StudentIdsToAdd, DbType.String);
            parameters.Add("@SchoolGroupIds", entity.SchoolGroups, DbType.String);
            parameters.Add("@InstantMessage", entity.IsInstantMessage, DbType.Boolean);
            parameters.Add("@IsGradingEnable", entity.IsGradingEnable, DbType.Boolean);
            parameters.Add("@GradingTemplateId", entity.GradingTemplateId, DbType.Int32);
            parameters.Add("@IsPeerMarkingEnable", entity.IsPeerMarkingEnable, DbType.Boolean);
            parameters.Add("@IsConsiderForFinalGrade", entity.IsConsiderForFinalGrade, DbType.Boolean);
            parameters.Add("@IsAssignmentMarksEnable", entity.IsAssignmentMarksEnable, DbType.Boolean);
            parameters.Add("@PeerMarkingType", entity.PeerMarkingType, DbType.Int32);
            parameters.Add("@IsInstantMailToParent", entity.IsInstantMailToParent, DbType.Boolean);
            parameters.Add("@InstantMessageUserTypeId", entity.InstantMessageUserTypeId, DbType.Int16);
            parameters.Add("@CourseIds", entity.Courses, DbType.String);
            parameters.Add("@IsInstantMailToParent", entity.IsInstantMailToParent, DbType.Boolean);
            parameters.Add("@IsNotificationToParent", entity.IsNotificationToParent, DbType.Boolean);
            parameters.Add("@IsInstantNotificationMailToStudent", entity.IsInstantNotificationMailToStudent, DbType.Boolean);
            parameters.Add("@IsShareCopyOfDriveAttachments", entity.CreateSeperateFileCopy, DbType.Boolean);
            parameters.Add("@IsConsiderForGrading", entity.IsConsiderForGrading, DbType.Boolean);
            parameters.Add("@AssignmentCategoryId", entity.AssignmentCategoryId, DbType.Int32);
            parameters.Add("@EnablePlagarism", entity.EnablePlagarism, DbType.Boolean);
            parameters.Add("@AssignmentMarks", entity.AssignmentMarks, DbType.Decimal);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);

            if (mode != TransactionModes.Delete)
            {
                if (entity.lstAssignmentTasks.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("AutoID", typeof(Int64));
                    dt.Columns.Add("AssignmentId", typeof(Int64));
                    dt.Columns.Add("TaskId", typeof(Int64));
                    dt.Columns.Add("TaskName", typeof(string));
                    dt.Columns.Add("TaskDecr", typeof(string));
                    dt.Columns.Add("GradingTemplateId", typeof(Int32));
                    dt.Columns.Add("SortOrder", typeof(Int32));
                    dt.Columns.Add("IsActive", typeof(Boolean));
                    dt.Columns.Add("IsDeleteds", typeof(Boolean));
                    dt.Columns.Add("QuizId", typeof(Int32));
                    dt.Columns.Add("IsSetTime", typeof(Boolean));
                    dt.Columns.Add("IsRandomQuestion", typeof(Boolean));
                    dt.Columns.Add("QuizTime", typeof(Int32));
                    dt.Columns.Add("StartDate", typeof(DateTime));
                    dt.Columns.Add("StartTime", typeof(string));
                    dt.Columns.Add("MaxSubmit", typeof(Int32));
                    dt.Columns.Add("QuestionPaginationRange", typeof(Int32));
                    dt.Columns.Add("CourseId", typeof(string));
                    dt.Columns.Add("IsHideScore", typeof(Boolean));
                    dt.Columns.Add("ShowScoreDate", typeof(DateTime));
                    dt.Columns.Add("TaskMarks", typeof(Decimal));

                    DataTable dtTaskFiles = new DataTable();
                    dtTaskFiles.Columns.Add("TaskId", typeof(Int64));
                    dtTaskFiles.Columns.Add("FileName", typeof(string));
                    dtTaskFiles.Columns.Add("UploadedFileName", typeof(string));
                    dtTaskFiles.Columns.Add("RefTaskID", typeof(Int64));
                    dtTaskFiles.Columns.Add("ResouseFileTypeId", typeof(Int64));
                    dtTaskFiles.Columns.Add("ShareableLinkFileURL", typeof(string));
                    int i = 1;
                    foreach (var item in entity.lstAssignmentTasks)
                    {
                        dt.Rows.Add(i, item.AssignmentId, item.TaskId, item.TaskTitle, item.TaskDescription, item.TaskGradingTemplateId, item.SortOrder, item.IsActive, item.IsDeleted, item.QuizId, item.IsSetTime, item.IsRandomQuestion, item.QuizTime, item.StartDate, item.StartTime, item.MaxSubmit, item.QuestionPaginationRange, item.Courses, item.IsHideScore, item.ShowScoreDate,item.TaskMarks);
                        if (item.lstTaskFiles != null)
                        {
                            foreach (var item1 in item.lstTaskFiles)
                            {
                                dtTaskFiles.Rows.Add(item.TaskId, item1.FileName, item1.UploadedFileName, i, item1.ResourceFileTypeId, item1.ShareableLinkFileURL);
                            }
                        }

                        i = i + 1;
                    }
                    parameters.Add("@TaskDetails", dt, DbType.Object);
                    parameters.Add("@TaskFiles", dtTaskFiles, DbType.Object);
                }
                if (entity.lstAssignmentFiles.Count > 0)
                {
                    DataTable dtAssignmentFiles = new DataTable();
                    dtAssignmentFiles.Columns.Add("AssignmentFileId", typeof(Int64));
                    dtAssignmentFiles.Columns.Add("AssignmentId", typeof(Int64));
                    dtAssignmentFiles.Columns.Add("FileName", typeof(string));
                    dtAssignmentFiles.Columns.Add("UploadedFileName", typeof(string));
                    dtAssignmentFiles.Columns.Add("ResourceFileTypeId", typeof(short));
                    dtAssignmentFiles.Columns.Add("ShareableLinkFileURL", typeof(string));
                    if (entity.lstAssignmentFiles != null)
                    {
                        foreach (var item in entity.lstAssignmentFiles)
                        {
                            dtAssignmentFiles.Rows.Add(item.AssignmentFileId, entity.AssignmentId, item.FileName, item.UploadedFileName, item.ResourceFileTypeId, item.ShareableLinkFileURL);
                        }
                    }
                    parameters.Add("@AssignmentFileDetails", dtAssignmentFiles, DbType.Object);

                }
                if (entity.lstObjectives != null)
                {
                    parameters.Add("@isInsertObjective", 0, DbType.Boolean);
                }
                if (entity.lstCourseUnit != null)
                {
                    parameters.Add("@isInsertCourseUnit", 0, DbType.Boolean);
                }
                if (entity.lstGroupTopicUnit != null)
                {
                    parameters.Add("@isInsertGroupUnit", 0, DbType.Boolean);
                }
                if (entity.lstObjectives.Count > 0)
                {
                    DataTable dtObjective = new DataTable();
                    dtObjective.Columns.Add("ObjectiveId", typeof(Int64));
                    dtObjective.Columns.Add("IsSelected", typeof(Boolean));
                    if (entity.lstObjectives != null)
                    {
                        foreach (var item in entity.lstObjectives)
                        {
                            dtObjective.Rows.Add(item.ObjectiveId, item.isSelected);
                        }
                    }
                    parameters.Add("@isInsertObjective", 1, DbType.Boolean);
                    parameters.Add("@Objective", dtObjective, DbType.Object);
                }
                if (entity.lstCourseUnit.Count > 0)
                {
                    DataTable dtCourseUnit = new DataTable();
                    dtCourseUnit.Columns.Add("CourseUnitId", typeof(Int64));
                    dtCourseUnit.Columns.Add("IsSelected", typeof(Boolean));
                    if (entity.lstCourseUnit != null)
                    {
                        foreach (var item in entity.lstCourseUnit)
                        {
                            dtCourseUnit.Rows.Add(item.CourseUnitId, item.isSelected);
                        }
                    }
                    parameters.Add("@isInsertCourseUnit", 1, DbType.Boolean);
                    parameters.Add("@CourseUnit", dtCourseUnit, DbType.Object);
                }
                if (entity.lstGroupTopicUnit.Count > 0)
                {
                    DataTable dtGroupTopicUnit = new DataTable();
                    dtGroupTopicUnit.Columns.Add("GroupTopicId", typeof(Int64));
                    dtGroupTopicUnit.Columns.Add("GroupId", typeof(Int32));
                    if (entity.lstGroupTopicUnit != null)
                    {
                        foreach (var item in entity.lstGroupTopicUnit)
                        {
                            dtGroupTopicUnit.Rows.Add(item.GroupCourseTopicId, item.GroupId);
                        }
                    }
                    parameters.Add("@isInsertGroupUnit", 1, DbType.Boolean);
                    parameters.Add("@GroupTopicUnit", dtGroupTopicUnit, DbType.Object);
                }
            }
            return parameters;
        }

        private DynamicParameters GetAssignmentDetailsParameters(Assignment entity, TransactionModes mode)
        {
            var userId = mode != TransactionModes.Delete ? entity.CreatedById : entity.DeletedBy;
            entity.DueDate = entity.DueDate != null ? entity.DueDate.Value.Date.Add(entity.DueTime != null ? entity.DueTime : TimeSpan.Zero) : (DateTime?)null;
            //entity.DueDate = entity.DueDate.Add(entity.DueTime);
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@AssignmentId", entity.AssignmentId, DbType.Int32);
            parameters.Add("@AssignmentTitle", entity.AssignmentTitle, DbType.String);
            parameters.Add("@AssignmentDesc", entity.AssignmentDesc, DbType.String);
            parameters.Add("@AssignmentDesc", entity.AssignmentDesc, DbType.String);
            parameters.Add("@SubmissionType", entity.SubmissionType, DbType.Int32);
            parameters.Add("@StartDate", entity.StartDate, DbType.DateTime);
            parameters.Add("@DueDate", entity.DueDate, DbType.DateTime);
            parameters.Add("@ArchiveDate", entity.ArchiveDate, DbType.DateTime);
            parameters.Add("@IsPublished", entity.IsPublished, DbType.Boolean);
            parameters.Add("@IsTaskOrderRequired", entity.IsTaskOrderRequired, DbType.Boolean);
            parameters.Add("@UserId", userId, DbType.Int32);
            parameters.Add("@StudentIds", entity.StudentIdsToAdd, DbType.String);
            parameters.Add("@SchoolGroupIds", entity.SchoolGroups, DbType.String);
            parameters.Add("@CourseIds", entity.SchoolGroups, DbType.String);
            parameters.Add("@IsGradingEnable", entity.IsGradingEnable, DbType.Boolean);
            parameters.Add("@GradingTemplateId", entity.GradingTemplateId, DbType.Int32);
            parameters.Add("@IsPeerMarkingEnable", entity.IsPeerMarkingEnable, DbType.Boolean);
            parameters.Add("@PeerMarkingType", entity.PeerMarkingType, DbType.Int32);
            parameters.Add("@IsInstantMailToParent", entity.IsInstantMailToParent, DbType.Boolean);
            parameters.Add("@IsNotificationToParent", entity.IsNotificationToParent, DbType.Boolean);
            parameters.Add("@IsInstantNotificationMailToStudent", entity.IsInstantNotificationMailToStudent, DbType.Boolean);
            parameters.Add("@IsShareCopyOfDriveAttachments", entity.CreateSeperateFileCopy, DbType.Boolean);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string searchString = "", string assignmentType = "", string sortBy = "")
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", PageNumber, DbType.Int32);
                parameters.Add("@PageSize", PageSize, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                parameters.Add("@SortBy", string.IsNullOrEmpty(sortBy) ? (object)DBNull.Value : sortBy, DbType.String);
                parameters.Add("@AssignmentType", assignmentType == null ? "" : assignmentType, DbType.String);
                var result = await conn.QueryMultipleAsync("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetArchivedAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string searchString = "", string assignmentType = "", string sortBy = "")
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", PageNumber, DbType.Int32);
                parameters.Add("@PageSize", PageSize, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                parameters.Add("@SortBy", string.IsNullOrEmpty(sortBy) ? (object)DBNull.Value : sortBy, DbType.String);
                parameters.Add("@AssignmentType", assignmentType == null ? "" : assignmentType, DbType.String);
                var result = await conn.QueryMultipleAsync("Assignment.GetArchivedAssignmentByStudentId", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }


        public async Task<IEnumerable<AssignmentStudentDetails>> GeStudentFilterSchoolGroupsById(int PageNumber, int PageSize, long studentId, string searchString = "", string assignmentType = "", string sortBy = "", string filterVal = "")
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", PageNumber, DbType.Int32);
                parameters.Add("@PageSize", PageSize, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                parameters.Add("@SortBy", string.IsNullOrEmpty(sortBy) ? (object)DBNull.Value : sortBy, DbType.String);
                parameters.Add("@SchoolGroupId", string.IsNullOrEmpty(filterVal) ? (object)DBNull.Value : filterVal, DbType.String);
                parameters.Add("@AssignmentType", assignmentType == null ? "" : assignmentType, DbType.String);
                var result = await conn.QueryMultipleAsync("Assignment.FilterStudentSchoolGroupsById", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }
        public async Task<IEnumerable<AssignmentStudentDetails>> GetArchivedStudentFilterSchoolGroupsById(int PageNumber, int PageSize, long studentId, string searchString = "", string assignmentType = "", string sortBy = "", string filterVal = "")
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", PageNumber, DbType.Int32);
                parameters.Add("@PageSize", PageSize, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                parameters.Add("@SortBy", string.IsNullOrEmpty(sortBy) ? (object)DBNull.Value : sortBy, DbType.String);
                parameters.Add("@SchoolGroupId", string.IsNullOrEmpty(filterVal) ? (object)DBNull.Value : filterVal, DbType.String);
                parameters.Add("@AssignmentType", assignmentType == null ? "" : assignmentType, DbType.String);
                var result = await conn.QueryMultipleAsync("Assignment.ArchivedFilterStudentSchoolGroupsById", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }


        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentIdWithoutPagination(long studentId, string assignmentType = "")
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@AssignmentType", assignmentType == null ? "" : assignmentType, DbType.String);
                var result = await conn.QueryMultipleAsync("Assignment.GetStudentAssignments", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByUserId(int PageNumber, int PageSize, long userId, string searchString = "")
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", PageNumber, DbType.Int32);
                parameters.Add("@PageSize", PageSize, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                var result = await conn.QueryMultipleAsync("Assignment.GetAssignmentsByUserId", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentIdWithDateRange(int PageNumber, int PageSize, long studentId, DateTime startDate, DateTime endDate, string searchString = "", string assignmentType = "")
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", PageNumber, DbType.Int32);
                parameters.Add("@PageSize", PageSize, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                parameters.Add("@AssignmentType", assignmentType == null ? "" : assignmentType, DbType.String);
                var result = await conn.QueryMultipleAsync("[Assignment].[GetAssignmentsByStudentIdWithDateRange]", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentByStudentIdAndTeacherIdWithDateRange(int PageNumber, int PageSize, long studentId, long teacherId, DateTime startDate, DateTime endDate, string searchString = "", string assignmentType = "")
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", PageNumber, DbType.Int32);
                parameters.Add("@PageSize", PageSize, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@intTeacherId", teacherId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                parameters.Add("@AssignmentType", assignmentType == null ? "" : assignmentType, DbType.String);
                var result = await conn.QueryMultipleAsync("[Assignment].[GetAssignmentsByStudentIdAndTeacherIdWithDateRange]", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }

        public async Task<IEnumerable<AssignmentReport>> GetAssignmentTaskQuizByAssignmentId(int assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                return await conn.QueryAsync<AssignmentReport>("[School].[GetAssignmentTaskQuizDetailsByAssignmentId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentsForMyPlanner(long studentId, DateTime startDate, DateTime endDate, string assignmentType = "")
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@AssignmentType", assignmentType == null ? "" : assignmentType, DbType.String);
                var result = await conn.QueryMultipleAsync("[Assignment].[GetAssignmentsForMyPlannerWithDateRange]", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                //if (assignmentStudentDetails.Any())
                //{
                //    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                //}

                return assignmentStudentDetails;
            }
        }

        public bool MarkAsComplete(int assignmentStudentId, bool isTeacher, int CompletedBy)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                parameters.Add("@AssignmentStudentId", assignmentStudentId, DbType.Int64);
                parameters.Add("@CompletedBy", CompletedBy, DbType.Int32);
                conn.Query<int>("Assignment.MarkAsComplete", parameters, commandType: CommandType.StoredProcedure);
                return true;
            }
        }

        public async Task<GradingTemplateItem> SubmitAssignmentMarks(int assignmentStudentId, decimal submitMarks,int CompletedBy,int gradingTemplateId,int SystemLanguageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentStudentId", assignmentStudentId, DbType.Int64);
                parameters.Add("@submitAssignmentMarks", submitMarks, DbType.Decimal);
                parameters.Add("@CompletedBy", CompletedBy, DbType.Int32);
                parameters.Add("@GradingTemplateId", gradingTemplateId, DbType.Int32);
                parameters.Add("@SystemLanguageId", SystemLanguageId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<GradingTemplateItem>("Assignment.SubmitAssignmentMarks", parameters, commandType: CommandType.StoredProcedure);
                //conn.Query<int>("Assignment.SubmitAssignmentMarks", parameters, commandType: CommandType.StoredProcedure);
                //return true;
            }
        }

        public bool UploadStudentAssignmeentFiles(List<StudentAssignmentFile> lstStudentAssignmentFile, int studentId, int assignmentId, string IpDetails)
        {

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                DataTable dt = new DataTable();
                dt.Columns.Add("FileName", typeof(string));
                dt.Columns.Add("UploadedFileName", typeof(string));
                dt.Columns.Add("SharepointUploadedFileURL", typeof(string));
                dt.Columns.Add("ResourceFileTypeId", typeof(int));
                dt.Columns.Add("CreateOn", typeof(DateTime));
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                foreach (var item in lstStudentAssignmentFile)
                {
                    dt.Rows.Add(item.FileName, item.PathtoDownload, item.SharepointUploadedFileURL, item.ResourceFileTypeId, item.UploadedOn);
                }
                parameters.Add("@StudentAssignmentFiles", dt, DbType.Object);
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@AssignmentId", assignmentId, DbType.Int64);
                parameters.Add("@IpDetails", IpDetails, DbType.String);

                conn.Query<int>("[Assignment].[InsertStudentAssignmentFiles]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<StudentAssignmentFile>> GetStudentAssignmentFiles(int studentId, int assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int32);
                return await conn.QueryAsync<StudentAssignmentFile>("[Assignment].[GetStudentUploadedFiles]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<AssignmentFeedbackFiles>> GetAssignmentFeedbackFiles(int assignmentStudentId, int assignmentCommentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentStudentId", assignmentStudentId, DbType.Int64);
                parameters.Add("@AssignmentCommentId", assignmentCommentId, DbType.Int32);
                return await conn.QueryAsync<AssignmentFeedbackFiles>("[Assignment].[GetAssignmentFeedbackFiles]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public bool MarkAsInComplete(int stundetId, int assignmentId, bool isReSubmitAssignnment)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", stundetId, DbType.Int32);
                parameters.Add("@AssignmentId", assignmentId, DbType.Int64);
                parameters.Add("@isReSubmitAssignnment", isReSubmitAssignnment, DbType.Boolean);
                conn.Query<int>("[Assignment].[MarkAsInComplete]", parameters, commandType: CommandType.StoredProcedure);
                return true;
            }
        }


        public bool UploadStudentTaskFiles(List<StudentTaskFile> lstStudentTaskFile, int studentId, int taskId, string IpDetails)
        {

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                DataTable dt = new DataTable();
                dt.Columns.Add("FileName", typeof(string));
                dt.Columns.Add("UploadedFileName", typeof(string));
                dt.Columns.Add("SharepointUploadedFileURL", typeof(string));
                dt.Columns.Add("ResourceFileTypeId", typeof(int));
                dt.Columns.Add("CreateOn", typeof(DateTime));
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                foreach (var item in lstStudentTaskFile)
                {
                    dt.Rows.Add(item.FileName, item.UploadedFileName, item.SharepointUploadedFileURL, item.ResourceFileTypeId, item.CreateOn);
                }
                parameters.Add("@StudentAssignmentFiles", dt, DbType.Object);
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@TaskId", taskId, DbType.Int64);
                parameters.Add("@IpDetails", IpDetails, DbType.String);

                conn.Query<int>("[Assignment].[InsertStudentTaskFiles]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        //[Assignment].[GetStudentTaskFiles] GetStudentTaskFiles

        public async Task<IEnumerable<StudentTaskFile>> GetStudentTaskFiles(int studentId, int taskid)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@taskid", taskid, DbType.Int32);
                parameters.Add("@studentId", studentId, DbType.Int32);
                return await conn.QueryAsync<StudentTaskFile>("[Assignment].[GetStudentTaskFiles]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<StudentTask> GetStudentTaskDetails(int taskId, int studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TaskId", taskId, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<StudentTask>("Assignment.GetStudentTaskdetails", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<AssignmentTask> GetStudentTaskDetailById(int taskId, int studentId, int studentAssignmentId, long studentTaskId)
        {
            using (var conn = GetOpenConnection())
            {
                AssignmentTask taskResponse = new AssignmentTask();
                var parameters = new DynamicParameters();
                parameters.Add("@TaskId", taskId, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int32);
                parameters.Add("@StudentAssignmentId", studentAssignmentId, DbType.Int32);
                parameters.Add("@StudentTaskId", studentTaskId, DbType.Int64);

                var result = await conn.QueryMultipleAsync("[School].[GetStudentTaskDetailById]", parameters, null, null, CommandType.StoredProcedure);
                var assignmentTask = await result.ReadAsync<AssignmentTask>();
                var taskFilesList = await result.ReadAsync<File>();
                if (assignmentTask.Any())
                {
                    taskResponse = assignmentTask.FirstOrDefault();
                    taskResponse.QuizFeedbacks = taskFilesList.ToList();
                }
                return taskResponse;
            }
        }
        public bool InsertAssignmentComment(AssignmentComment entity, DataTable assignmentFeedbackFile)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentCommentId", entity.AssignmentCommentId, DbType.Int64);
                parameters.Add("@AssignmentStudentId", entity.AssignmentStudentId, DbType.Int64);
                parameters.Add("@Comment", entity.Comment, DbType.String);
                parameters.Add("@ByTeacher", entity.ByTeacher, DbType.Boolean);
                parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int32);
                parameters.Add("@AssignmentFeedbackFile", assignmentFeedbackFile, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Assignment.InsertAssignmentComment", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<AssignmentComment>> GetAssignmentComments(int assignmentStudentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentStudentId", assignmentStudentId, DbType.Int32);
                return await conn.QueryAsync<AssignmentComment>("[Assignment].[GetAssignmentComment]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public bool MarkAsCompleteTask(int studentTaskId, bool isTeacher, int CompletedBy)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentTaskId", studentTaskId, DbType.Int64);
                parameters.Add("@ByTeacher", isTeacher, DbType.Boolean);
                parameters.Add("@CompletedBy", CompletedBy, DbType.Int32);
                parameters.Add("@IsSuccess", dbType: DbType.Boolean, direction: ParameterDirection.Output);
                conn.Query<int>("Assignment.MarkAsCompleteTask", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("IsSuccess");
            }
        }

        public async Task<GradingTemplateItem> SubmitTaskMarks(int studentTaskId,int StudentAssignmentId, decimal submitMarks, int CompletedBy, int gradingTemplateId, int SystemLanguageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentTaskId", studentTaskId, DbType.Int64);
                parameters.Add("@StudentAssignmentId", StudentAssignmentId, DbType.Int32);
                parameters.Add("@SubmitMarks", submitMarks, DbType.Decimal);
                parameters.Add("@CompletedBy", CompletedBy, DbType.Int32);
                parameters.Add("@GradingTemplateId", gradingTemplateId, DbType.Int32);
                parameters.Add("@SystemLanguageId", SystemLanguageId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<GradingTemplateItem>("Assignment.SubmitTaskMarks", parameters, commandType: CommandType.StoredProcedure);
                //conn.Query<int>("Assignment.SubmitTaskMarks", parameters, commandType: CommandType.StoredProcedure);
                //return parameters.Get<bool>("IsSuccess");
            }
        }

        public bool AssignGradeToStudentTask(int studentTaskId, int gradingTemplateItemId, int GradedBy)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentTaskId", studentTaskId, DbType.Int64);
                parameters.Add("@GradingTemplateitemId", gradingTemplateItemId, DbType.Int32);
                parameters.Add("@GradedBy", GradedBy, DbType.Int32);
                parameters.Add("@IsSuccess", dbType: DbType.Boolean, direction: ParameterDirection.Output);
                conn.Query<int>("Assignment.AssignGradeToStudentTask", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("IsSuccess");
            }
        }


        public bool AssignGradeToStudentObjective(int studentObectiveId, int gradingTemplateItemId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentObectiveId", studentObectiveId, DbType.Int64);
                parameters.Add("@GradingTemplateitemId", gradingTemplateItemId, DbType.Int32);
                parameters.Add("@IsSuccess", dbType: DbType.Boolean, direction: ParameterDirection.Output);
                conn.Query<int>("Assignment.AssignGradeToStudentObjective", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("IsSuccess");
            }
        }
        public async Task<IEnumerable<MyFilesTreeItem>> GetMyFilesStructure(long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<MyFilesTreeItem>("Assignment.GetUserMyFilesStructure", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<AssignmentFile>> GetAssignmentMyFile(long id, bool isFolder)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileId", id, DbType.Int64);
                parameters.Add("@IsFolder", isFolder, DbType.Boolean);
                return await conn.QueryAsync<AssignmentFile>("Assignment.GetMyFileAsAssignmentFile", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public bool DeleteUploadedFiles(int FileId, int AssignmentFileId, long UserId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@assignmentfileId", FileId, DbType.Int32);
                parameters.Add("@assignmentId", AssignmentFileId, DbType.Int32);
                parameters.Add("@deleteby", UserId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Assignment.DeleteUploadedFiles", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public bool DeleteStudentAssignmentFile(int FileId, long UserId, bool IsTaskFile)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StdAssignmentfileId", FileId, DbType.Int32);
                parameters.Add("@deleteby", UserId, DbType.Int64);
                parameters.Add("@IsTaskFile", IsTaskFile, DbType.Boolean);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Assignment.DeleteStudentAssignmentFile", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<File> GetAssignmentFilebyAssignmentFileId(long id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentFileId", id, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<File>("Assignment.GetAssignmentFilebyAssignmentFileId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<File> GetTaskFile(long id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TaskFileId", id, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<File>("Assignment.GetTaskFile", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<File> GetStudentTaskFile(long id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentTaskFileId", id, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<File>("Assignment.GetStudentTaskFile", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<File> GetStudentAssignmentFilebyStudentAsgFileId(long id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentAsgFileId", id, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<File>("Assignment.GetStudentAssignmentFilebyStudentAsgFileId", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public bool InsertTaskFeedback(AssignmentTask assignmentTask, DataTable recordings)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Feedback", assignmentTask.Feedback, DbType.String);
                parameters.Add("@StudentTaskId", assignmentTask.StudentTaskId, DbType.Int32);
                parameters.Add("@UserId", assignmentTask.UserId, DbType.Int32);
                parameters.Add("@RecordingsTable", recordings, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[InsertTaskFeedback]", parameters, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
        public bool InsertStudentObjectiveFeedback(AssignmentStudentObjective studentObjective, DataTable recordings)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Feedback", studentObjective.Feedback, DbType.String);
                parameters.Add("@StudentObjectiveMarkId", studentObjective.StudentObjectiveMarkId, DbType.Int32);
                parameters.Add("@UserId", studentObjective.CreatedBy, DbType.Int32);
                parameters.Add("@RecordingsTable", recordings, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[InsertObjectiveFeedback]", parameters, commandType: CommandType.StoredProcedure);
                return true;
            }
        }

        public bool UpdateArchiveAssignment(long assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("Assignment.UpdateArchiveAssignment", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<IEnumerable<AssignmentStudentDetails>> GetAssignmentListForReport(int pageNumber, int pageSize, string searchString, long userId, long schoolId, int schoolGroupId, DateTime startDate, DateTime endDate, string userType)
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int32);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                parameters.Add("@UserType", userType == null ? "" : userType, DbType.String);
                var result = await conn.QueryMultipleAsync("Admin.GetAssignmentListForReport", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                return assignmentStudentDetails;
            }
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetArchivedAssignmentStudentDetails(int TeacherId, AssignmentFilter loadAssignment)
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", loadAssignment.page, DbType.Int32);
                parameters.Add("@PageSize", loadAssignment.size, DbType.Int32);
                parameters.Add("@intTeacherId", TeacherId, DbType.Int32);
                parameters.Add("@SearchString", loadAssignment.searchString == null ? "" : loadAssignment.searchString, DbType.String);
                parameters.Add("@AssignmentType", loadAssignment.assignmentType == null ? "" : loadAssignment.assignmentType, DbType.String);
                parameters.Add("@SortBy", string.IsNullOrEmpty(loadAssignment.sortBy) ? (object)DBNull.Value : loadAssignment.sortBy, DbType.String);
                //parameters.Add("@TeacherId", string.IsNullOrEmpty(loadAssignment.teacherVal) ? (object)DBNull.Value : loadAssignment.teacherVal, DbType.String);
                //return await conn.QueryAsync<AssignmentStudentDetails>("[Assignment].[GetAssignmentByPage]", parameters, null, null, CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("[Assignment].[GetArchivedAssignmentByPage]", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }

        public async Task<IEnumerable<AssignmentStudentDetails>> GetAdminArchivedAssignmentStudentDetails(AssignmentFilter loadAssignment)
        {
            var assignmentStudentDetails = new List<AssignmentStudentDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", loadAssignment.page, DbType.Int32);
                parameters.Add("@PageSize", loadAssignment.size, DbType.Int32);
                parameters.Add("@SearchString", loadAssignment.searchString == null ? "" : loadAssignment.searchString, DbType.String);
                parameters.Add("@AssignmentType", loadAssignment.assignmentType == null ? "" : loadAssignment.assignmentType, DbType.String);
                parameters.Add("@SortBy", string.IsNullOrEmpty(loadAssignment.sortBy) ? (object)DBNull.Value : loadAssignment.sortBy, DbType.String);
                parameters.Add("@teacherIds", string.IsNullOrEmpty(loadAssignment.teacherVal) ? (object)DBNull.Value : loadAssignment.teacherVal, DbType.String);
                parameters.Add("@GroupIds", string.IsNullOrEmpty(loadAssignment.filterVal) ? (object)DBNull.Value : loadAssignment.teacherVal, DbType.String);
                parameters.Add("@CourseIds", string.IsNullOrEmpty(loadAssignment.courseVal) ? (object)DBNull.Value : loadAssignment.teacherVal, DbType.String);
                parameters.Add("@SchoolId", loadAssignment.schoolId, DbType.Int64);
                //return await conn.QueryAsync<AssignmentStudentDetails>("[Assignment].[GetAssignmentByPage]", parameters, null, null, CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("[Assignment].[GetAdminArchiveAssignmentPage]", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<AssignmentStudentDetails>();
                var assignmentCount = await result.ReadAsync<int>();
                assignmentStudentDetails = assignmentDetails.ToList();
                int totalCount = assignmentCount.FirstOrDefault();
                if (assignmentStudentDetails.Any())
                {
                    assignmentStudentDetails.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return assignmentStudentDetails;
            }
        }

        public bool AddUpdatePeerReviewMapping(List<AssignmentPeerReview> lstPeerReviewMapping)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                DataTable dt = new DataTable();
                dt.Columns.Add("PeerReviewId", typeof(Int64));
                dt.Columns.Add("AssignmentId", typeof(Int64));
                dt.Columns.Add("StudentUserId", typeof(Int64));
                dt.Columns.Add("ReviewerId", typeof(Int64));
                foreach (var item in lstPeerReviewMapping)
                {
                    dt.Rows.Add(item.PeerReviewId, item.AssignmentId, item.UserId, item.ReviewerId);
                }
                parameters.Add("@PeerReviewMapping", dt, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Assignment.AddUpdatePeerReviewMapping", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<AssignmentPeerReview>> GetPeerAssignmentDetails(long assignmentId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@assignmentId", assignmentId, DbType.Int64);
                parameters.Add("@userId", userId, DbType.Int64);
                return await conn.QueryAsync<AssignmentPeerReview>("Assignment.GetPeerAssignmentDetails", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AssignmentPeerReview>> GetPeerMappingDetails(long assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int64);
                return await conn.QueryAsync<AssignmentPeerReview>("[Assignment].[GetPeerMappingDetails]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int AddUpdateDocumentReviewDetails(List<DocumentReviewdetails> lstDocumentReview, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                DataTable dt = new DataTable();
                dt.Columns.Add("ReviewedFileId", typeof(Int64));
                dt.Columns.Add("PeerReviewId", typeof(Int64));
                dt.Columns.Add("StudentAsgFileId", typeof(Int64));
                dt.Columns.Add("ReviewedFileName", typeof(string));
                dt.Columns.Add("CalculatedMarks", typeof(decimal));
                dt.Columns.Add("MistakesCount", typeof(int));
                foreach (var item in lstDocumentReview)
                {
                    dt.Rows.Add(item.ReviewedFileId, item.PeerReviewId, item.StudentAsgFileId, item.ReviewedFileName, item.CalculatedMarks, item.MistakesCount);
                }
                parameters.Add("@DocumentReviewDetails", dt, DbType.Object);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("Assignment.AddUpdateDocumentReview", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        public async Task<IEnumerable<DocumentReviewdetails>> GetReviewedDocumentFiles(long peerReviewId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PeerReviewId", peerReviewId, DbType.Int64);
                return await conn.QueryAsync<DocumentReviewdetails>("Assignment.GetReviewedDocumentFiles", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<DocumentReviewdetails> GetDocumentFileByStudAsgFileId(long stdAsgFileId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@stdAsgFileId", stdAsgFileId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<DocumentReviewdetails>("Assignment.GetReviewedDocumentFileBsStdAsgFileId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<AssignmentCounts> GetAssignmentStatusCounts(long id, bool isTeacher, string searchText)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Int64);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                parameters.Add("@SearchText", searchText, DbType.String);
                return await conn.QueryFirstOrDefaultAsync<AssignmentCounts>("Assignment.GetAssignmentStatusCount", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public bool MarkAsCompleteReview(long peerReviewId, long reviewedBy)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PeerReviewId", peerReviewId, DbType.Int64);
                parameters.Add("@ReviwedBy", reviewedBy, DbType.Int64);
                conn.Query<int>("Assignment.MarkAsCompleteReview", parameters, commandType: CommandType.StoredProcedure);
                return true;
            }
        }

        public bool MarkAsCompleteTaskReview(long peerReviewId, long reviewedBy)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PeerReviewId", peerReviewId, DbType.Int64);
                parameters.Add("@ReviwedBy", reviewedBy, DbType.Int64);
                conn.Query<int>("[Assignment].[MarkAsCompleteTaskReview]", parameters, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
        public async Task<AssignmentPeerReview> GetPeerAssignmentDetail(long peerReviewId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PeerReviewId", peerReviewId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<AssignmentPeerReview>("[Assignment].[GetPeerAssignmentDetail]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int AddUpdateTaskDocumentReviewDetails(List<TaskDocumentReviewdetails> lstTaskDocumentReview, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                DataTable dt = new DataTable();
                dt.Columns.Add("ReviewTaskFileId", typeof(Int64));
                dt.Columns.Add("PeerReviewId", typeof(Int64));
                dt.Columns.Add("StudentTaskFileId", typeof(Int64));
                dt.Columns.Add("ReviewedFileName", typeof(string));
                dt.Columns.Add("CalculatedMarks", typeof(decimal));
                dt.Columns.Add("MistakesCount", typeof(int));
                foreach (var item in lstTaskDocumentReview)
                {
                    dt.Rows.Add(item.ReviewTaskFileId, item.TaskPeerReviewId, item.StudentTaskFileId, item.ReviewedFileName, item.CalculatedMarks, item.MistakesCount);
                }
                parameters.Add("@TaskDocumentReviewDetails", dt, DbType.Object);
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("Assignment.AddUpdateTaskReviewDocumentDetails", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<TaskPeerReview> GetPeerTaskDetail(long studentId, long taskId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@TaskId", taskId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<TaskPeerReview>("[Assignment].[GetPeerTaskDetail]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<TaskPeerReview>> GetPeerTaskDetails(long assignmentId, long studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@assignmentId", assignmentId, DbType.Int64);
                parameters.Add("@StudentId", studentId, DbType.Int64);
                return await conn.QueryAsync<TaskPeerReview>("Assignment.GetPeerTaskDetails", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<TaskDocumentReviewdetails> GetTaskDocumentFileByStudTaskFileId(long stdTaskFileId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@stdTaskFileId", stdTaskFileId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<TaskDocumentReviewdetails>("[Assignment].[GetTaskReviewedDocumentFileByStdTaskFileId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<TaskDocumentReviewdetails>> GetTaskReviewedDocumentFiles(long taskPeerReviewId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TaskPeerReviewId", taskPeerReviewId, DbType.Int64);
                return await conn.QueryAsync<TaskDocumentReviewdetails>("[Assignment].[GetTaskReviewedDocumentFiles]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<AssignmentPeerReview> GetPeerAssignmentDetailByAssignmentStudentId(long assignmentStudentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentStudentId", assignmentStudentId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<AssignmentPeerReview>("[Assignment].[GetPeerAssignmentDetailByAssignmentStudentId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Assignment>> GetGroupAssignments(long schoolGroupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int64);
                return await conn.QueryAsync<Assignment>("Assignment.GetSchoolGroupAssignments", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AssignmentStudent>> GetStudentGroupAssignments(long schoolGroupId, long UserId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int64);
                parameters.Add("@UserId", UserId, DbType.Int64);
                return await conn.QueryAsync<AssignmentStudent>("Assignment.GetStudentGroupAssignments", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Assignment>> GetDashboardAssignmentOverview(long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);

                return await conn.QueryAsync<Assignment>("Assignment.GetAssignmentOverviewData", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<AssignmentStudentObjective>> GetStudentAssignmentObjectives(long StudentAssignmengtId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentStudentId", StudentAssignmengtId, DbType.Int32);
                return await conn.QueryAsync<AssignmentStudentObjective>("[Assignment].[GetStudentAssignmentObjective]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<QuizQuestionsView>> GetQuizQuestionsByTaskId(int taskId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TaskId", taskId, DbType.Int32);
                return await conn.QueryAsync<QuizQuestionsView>("[school].[GetQuizQuestionsByTaskId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<File>> GetStudentAssignmentObjectiveAudiofeedback(long studentObjectiveMarkId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentObjectiveMarkId", studentObjectiveMarkId, DbType.Int32);
                return await conn.QueryAsync<File>("[Assignment].[GetStudentObjectiveAudioFeedback]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<GroupQuiz> GetAssignmentQuizDetailsByTaskId(int taskId)
        {
            using (var conn = GetOpenConnection())
            {
                var quizDetails = new GroupQuiz();
                var parameters = new DynamicParameters();
                parameters.Add("@TaskId", taskId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[School].[GetAssignmentQuizDetailsByTaskId]", parameters, null, null, CommandType.StoredProcedure);
                //var quiz = await result.ReadAsync<GroupQuiz>();
                //var quizStudentList = await result.ReadAsync<GroupStudent>();
                var genderWiseReport = await result.ReadAsync<GenderWiseReport>();
                var gradeWiseReport = await result.ReadAsync<GradeWiseReport>();
                var gradeWiseTotalQuestionsReport = await result.ReadAsync<GradeWiseTotalQuestionsReport>();
                var questionWiseReport = await result.ReadAsync<QuestionWiseReport>();
                //quizDetails.lstGroupStudents = quizStudentList.ToList();
                quizDetails.GenderWiseReport = genderWiseReport.FirstOrDefault();
                quizDetails.GradeWiseReport = gradeWiseReport.ToList();
                quizDetails.GradeWiseTotalQuestionsReport = gradeWiseTotalQuestionsReport.FirstOrDefault();
                quizDetails.QuestionWiseReport = questionWiseReport.ToList();
                return quizDetails;
            }
        }

        public async Task<bool> UndoAssignmentsArchive(ArchiveAssignment model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentIds", model.AssignmentIds, DbType.String);
                parameters.Add("@UpdatedBy", model.UpdatedBy, DbType.Int64);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync<int>("Assignment.UndoArchivedAssignments", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("Output") > 0;
            }
        }

        #endregion

        //#region Private Methods
        //private DynamicParameters GetAssignmentParameters(Assignment entity, TransactionModes mode)
        //{
        //    var userId = mode != TransactionModes.Delete ? entity.CreatedById : entity.DeletedBy;
        //    entity.DueDate = entity.DueDate != null ? entity.DueDate.Value.Date.Add(entity.DueTime != null ? entity.DueTime : TimeSpan.Zero) : (DateTime?)null;
        //    //entity.DueDate = entity.DueDate.Add(entity.DueTime);
        //    var parameters = new DynamicParameters();
        //    parameters.Add("@TransMode", (int)mode, DbType.Int32);
        //    parameters.Add("@AssignmentId", entity.AssignmentId, DbType.Int32);
        //    parameters.Add("@AssignmentTitle", entity.AssignmentTitle, DbType.String);
        //    parameters.Add("@AssignmentDesc", entity.AssignmentDesc, DbType.String);
        //    parameters.Add("@AssignmentDesc", entity.AssignmentDesc, DbType.String);
        //    parameters.Add("@SubjectId", entity.SubjectId, DbType.Int32);
        //    parameters.Add("@SubmissionType", entity.SubmissionType, DbType.Int32);
        //    parameters.Add("@StartDate", entity.StartDate, DbType.DateTime);
        //    parameters.Add("@DueDate", entity.DueDate, DbType.DateTime);
        //    parameters.Add("@ArchiveDate", entity.ArchiveDate, DbType.DateTime);
        //    parameters.Add("@IsPublished", entity.IsPublished, DbType.Boolean);
        //    parameters.Add("@IsTaskOrderRequired", entity.IsTaskOrderRequired, DbType.Boolean);
        //    parameters.Add("@UserId", userId, DbType.Int32);
        //    parameters.Add("@StudentIds", entity.StudentIdsToAdd, DbType.String);
        //    parameters.Add("@TeacherIds", entity.TeacherIdsToassign, DbType.String);
        //    parameters.Add("@SchoolGroupIds", entity.SchoolGroups, DbType.String);
        //    parameters.Add("@InstantMessage", entity.IsInstantMessage, DbType.Boolean);
        //    parameters.Add("@IsGradingEnable", entity.IsGradingEnable, DbType.Boolean);
        //    parameters.Add("@GradingTemplateId", entity.GradingTemplateId, DbType.Int32);
        //    parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
        //    if (mode != TransactionModes.Delete)
        //    {
        //        if (entity.lstAssignmentTasks.Count > 0)
        //        {
        //            DataTable dt = new DataTable();
        //            dt.Columns.Add("AutoID", typeof(Int64));
        //            dt.Columns.Add("AssignmentId", typeof(Int64));
        //            dt.Columns.Add("TaskId", typeof(Int64));
        //            dt.Columns.Add("TaskName", typeof(string));
        //            dt.Columns.Add("TaskDecr", typeof(string));
        //            dt.Columns.Add("GradingTemplateId", typeof(Int32));
        //            dt.Columns.Add("SortOrder", typeof(Int32));
        //            dt.Columns.Add("IsActive", typeof(Boolean));
        //            dt.Columns.Add("IsDeleteds", typeof(Boolean));
        //            dt.Columns.Add("QuizId", typeof(Int32));

        //            DataTable dtTaskFiles = new DataTable();
        //            dtTaskFiles.Columns.Add("TaskId", typeof(Int64));
        //            dtTaskFiles.Columns.Add("FileName", typeof(string));
        //            dtTaskFiles.Columns.Add("UploadedFileName", typeof(string));
        //            dtTaskFiles.Columns.Add("RefTaskID", typeof(Int64));
        //            int i = 1;
        //            foreach (var item in entity.lstAssignmentTasks)
        //            {
        //                dt.Rows.Add(i, item.AssignmentId, item.TaskId, item.TaskTitle, item.TaskDescription, item.TaskGradingTemplateId, item.SortOrder, item.IsActive, item.IsDeleted, item.QuizId);
        //                if (item.lstTaskFiles != null)
        //                {
        //                    foreach (var item1 in item.lstTaskFiles)
        //                    {
        //                        dtTaskFiles.Rows.Add(item.TaskId, item1.FileName, item1.UploadedFileName, i);
        //                    }
        //                }

        //                i = i + 1;
        //            }
        //            parameters.Add("@TaskDetails", dt, DbType.Object);
        //            parameters.Add("@TaskFiles", dtTaskFiles, DbType.Object);
        //        }
        //        if (entity.lstAssignmentFiles.Count > 0)
        //        {
        //            DataTable dtAssignmentFiles = new DataTable();
        //            dtAssignmentFiles.Columns.Add("AssignmentFileId", typeof(Int64));
        //            dtAssignmentFiles.Columns.Add("AssignmentId", typeof(Int64));
        //            dtAssignmentFiles.Columns.Add("FileName", typeof(string));
        //            dtAssignmentFiles.Columns.Add("UploadedFileName", typeof(string));
        //            if (entity.lstAssignmentFiles != null)
        //            {
        //                foreach (var item in entity.lstAssignmentFiles)
        //                {
        //                    dtAssignmentFiles.Rows.Add(item.AssignmentFileId, entity.AssignmentId, item.FileName, item.UploadedFileName);
        //                }
        //            }
        //            parameters.Add("@AssignmentFileDetails", dtAssignmentFiles, DbType.Object);

        //        }
        //        if (entity.lstObjectives != null)
        //        {
        //            parameters.Add("@isInsertObjective", 0, DbType.Boolean);
        //        }
        //        if (entity.lstObjectives.Count > 0)
        //        {
        //            DataTable dtObjective = new DataTable();
        //            dtObjective.Columns.Add("ObjectiveId", typeof(Int64));
        //            dtObjective.Columns.Add("IsSelected", typeof(Boolean));
        //            if (entity.lstObjectives != null)
        //            {
        //                foreach (var item in entity.lstObjectives)
        //                {
        //                    dtObjective.Rows.Add(item.ObjectiveId, item.isSelected);
        //                }
        //            }
        //            parameters.Add("@isInsertObjective", 1, DbType.Boolean);
        //            parameters.Add("@Objective", dtObjective, DbType.Object);
        //        }
        //    }
        //    return parameters;
        //}

        //#endregion
    }
}
