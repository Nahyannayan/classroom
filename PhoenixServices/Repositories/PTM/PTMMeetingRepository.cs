﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Repositories.PTM.Contracts;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories.PTM
{
    public class PTMMeetingRepository : SqlRepository<PTMMeeting>, IPTMMeetingRepository
    {
        private readonly IConfiguration _config;
        public PTMMeetingRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<PTMMeeting>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<PTMMeeting> GetAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override void InsertAsync(PTMMeeting entity)
        {
            throw new NotImplementedException();
        }
        public override void UpdateAsync(PTMMeeting entityToUpdate)
        {
            throw new NotImplementedException();
        }
        public Task<PTMMeeting> GetPTMMeetingByIdAsync(int id, short languageId)
        {
            throw new NotImplementedException();
        }
        public async Task<int> InsertPTMMeetingAsync(PTMMeeting entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSchoolSkillSetParameters(entity, mode);
                await conn.QueryAsync<int>("PTM.PTMeetingCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<int> UpdatePTMMeetingAsync(PTMMeeting entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSchoolSkillSetParameters(entity, mode);
                await conn.QueryAsync<int>("PTM.PTMeetingCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        private DynamicParameters GetSchoolSkillSetParameters(PTMMeeting entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@MeetingId", entity.MeetingId, DbType.Int32);
            parameters.Add("@MeetingTitle", entity.MeetingTitle, DbType.String);
            parameters.Add("@MeetingDetails", entity.MeetingDetails, DbType.String);
            parameters.Add("@MeetingDescription", entity.MeetingDescription, DbType.String);
            parameters.Add("@MeetingCategoryId", entity.MeetingCategoryId, DbType.Int32);
            parameters.Add("@StartDate", entity.StartDate, DbType.DateTime);
            parameters.Add("@EndDate", entity.EndDate, DbType.DateTime);
            parameters.Add("@MeetingSlotInTime", entity.MeetingSlotInTime, DbType.Time);
            parameters.Add("@BreakTimeBetweenSlots", entity.BreakTimeBetweenSlots, DbType.Time);
            parameters.Add("@Grades_YearGroups", entity.Grades_YearGroups, DbType.String);
            parameters.Add("@IsAutoAllocatedTimeSlots", entity.IsAutoAllocatedTimeSlots, DbType.Boolean);
            parameters.Add("@IsRecurringMeeting", entity.IsRecurringMeeting, DbType.Boolean);
            parameters.Add("@UserId", entity.UserId, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
    }
}
