﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Repositories.PTM.Contracts;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories.PTM
{
    public class PTMCategoryRepository :  SqlRepository<CategoryList>, IPTMCategoryRepository
    {
        private readonly IConfiguration _config;
        public PTMCategoryRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<CategoryList>> GetPTMCategory(int languageId, int SchoolGradeId)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@LanguageId", languageId, DbType.Int32);
                //parameters.Add("@SchoolGradeId", SchoolGradeId, DbType.Int32);
                try
                {
                    var result1 = await conn.QueryAsync<CategoryList>("[PTM].[GetPTMCategory]", parameters, null, null, CommandType.StoredProcedure);
                    return result1;
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex);

                }
                return null;
                
            }
        }

        public int UpdatePTMCategory(CategoryList entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSchoolSkillSetParameters(entity, mode);
                conn.Query<int>("[PTM].[PTMCategoryCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<CategoryList> GetPTMCategoryById(int id, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CategoryId", id, DbType.Int32);
                //parameters.Add("@LanguageId", languageId, DbType.Int16);
                return await conn.QueryFirstOrDefaultAsync<CategoryList>("[PTM].[GetPTMCategoryById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        #region Private Methods  
        private DynamicParameters GetSchoolSkillSetParameters(CategoryList entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@CategoryId", entity.CategoryId, DbType.Int32);
            parameters.Add("@CategoryName", entity.CategoryName, DbType.String);
            parameters.Add("@CategoryDescription", entity.CategoryDescription, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@IsApproved", entity.IsApproved, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedBy, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        #region Generated Methods

        public override Task<IEnumerable<CategoryList>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async override Task<CategoryList> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SkillId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<CategoryList>("[School].[GetSkillSetById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        public override void InsertAsync(CategoryList entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(CategoryList entityToUpdate)
        {
            throw new NotImplementedException();
        }


        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }



        #endregion
    }
}
