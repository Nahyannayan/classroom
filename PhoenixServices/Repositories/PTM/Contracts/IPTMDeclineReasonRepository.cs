﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories.PTM.Contracts
{
    public interface IPTMDeclineReasonRepository : IGenericRepository<DeclineReasonList>
    {
        Task<IEnumerable<DeclineReasonList>> GetPTMDeclineReason(int languageId, int SchoolGradeId);
        int UpdatePTMDeclineReason(DeclineReasonList entity, TransactionModes mode);
        Task<DeclineReasonList> GetPTMDeclineReasonById(int id, short languageId);
    }
}
