﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories.PTM.Contracts
{
    public interface IPTMCategoryRepository : IGenericRepository<CategoryList>
    {
        Task<IEnumerable<CategoryList>> GetPTMCategory(int languageId, int SchoolGradeId);
        int UpdatePTMCategory(CategoryList entity, TransactionModes mode);
        Task<CategoryList> GetPTMCategoryById(int id, short languageId);
    }
}
