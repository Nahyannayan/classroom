﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories.PTM.Contracts
{
    public interface IPTMMeetingRepository : IGenericRepository<PTMMeeting>
    {
        Task<int> UpdatePTMMeetingAsync(PTMMeeting entity, TransactionModes mode);
        Task<PTMMeeting> GetPTMMeetingByIdAsync(int id, short languageId); 
        Task<int> InsertPTMMeetingAsync(PTMMeeting entity, TransactionModes mode);
    }
}
