﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Repositories.PTM.Contracts;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories.PTM
{
    public class PTMDeclineReasonRepository : SqlRepository<DeclineReasonList>, IPTMDeclineReasonRepository
    {
        private readonly IConfiguration _config;
        public PTMDeclineReasonRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<DeclineReasonList>> GetPTMDeclineReason(int languageId, int SchoolGradeId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@LanguageId", languageId, DbType.Int32);
                // parameters.Add("@SchoolGradeId", SchoolGradeId, DbType.Int32);
                try
                {
                    return await conn.QueryAsync<DeclineReasonList>("[PTM].[GetPTMDeclineReason]", parameters, null, null, CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);

                }
                return null;
            }
        }

        public int UpdatePTMDeclineReason(DeclineReasonList entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSchoolSkillSetParameters(entity, mode);
                conn.Query<int>("[PTM].[PTMDeclineReasonCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");

            }
        }
        public async Task<DeclineReasonList> GetPTMDeclineReasonById(int id, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@DeclineReasonId", id, DbType.Int32);
               // parameters.Add("@LanguageId", languageId, DbType.Int16);
                return await conn.QueryFirstOrDefaultAsync<DeclineReasonList>("[PTM].[GetPTMDeclineReasonById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        #region Private Methods  
        private DynamicParameters GetSchoolSkillSetParameters(DeclineReasonList entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@DeclineReasonId", entity.DeclineReasonId, DbType.Int32);
            parameters.Add("@DeclineReasonName", entity.DeclineReasonName, DbType.String);
            parameters.Add("@DeclineReasonDescription", entity.DeclineReasonDescription, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@IsApproved", entity.IsApproved, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedBy, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        #region Generated Methods

        public override Task<IEnumerable<DeclineReasonList>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async override Task<DeclineReasonList> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SkillId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<DeclineReasonList>("[School].[GetSkillSetById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        public override void InsertAsync(DeclineReasonList entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(DeclineReasonList entityToUpdate)
        {
            throw new NotImplementedException();
        }


        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }



        #endregion
    }
}
