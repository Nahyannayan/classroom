﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public class SubjectRepository : SqlRepository<Subject>, ISubjectRepository
    {
        private readonly IConfiguration _config;

        public SubjectRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<Subject>> GetSubjects(int languageId, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@LanguageId", languageId, DbType.Int32);
                return await conn.QueryAsync<Subject>("School.GetSubjects", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Subject>> GetTopTenSubjects(int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<Subject>("School.GetTopTenSubjects", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public bool UpdateSubjectData(Subject entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSubjectsParameters(entity, mode);
                conn.Query<int>("School.SubjectCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<IEnumerable<TopicSubTopicStructure>> GetTopicSubTopicStructure(int subjectId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SubjectId", subjectId, DbType.Int32);
                return await conn.QueryAsync<TopicSubTopicStructure>("[School].[GetTopicSubTopicTree]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Objective>> GetLessonsByTopicId(int Id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TopicId", Id, DbType.Int32);
                return await conn.QueryAsync<Objective>("[School].[GetLessonsByTopicId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<GroupUnit>> GetSelectedUnitByGroupId(string Id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupIds", Id, DbType.String);
                return await conn.QueryAsync<GroupUnit>("[School].[GetSelectedUnitByGroupId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<TopicSubTopicStructure>> GetUnitStructure(string courseIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CourseIds", courseIds, DbType.String);
                return await conn.QueryAsync<TopicSubTopicStructure>("[School].[GetTopicSubTopicTreeWithMultipleCourses]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<TopicSubTopicStructure>> GetTopicSubTopicStructureByMultipleSubjectIds(string subjectIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SubjectIds", subjectIds, DbType.String);
                return await conn.QueryAsync<TopicSubTopicStructure>("[School].[GetTopicSubTopicTreeWithMultipleSubjects]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Objective>> GetSubtopicsObjective(int id, bool isMainSyllabus, int subjectId, bool isLesson)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                if (isMainSyllabus)
                {
                    parameters.Add("@Id", "x" + id.ToString(), DbType.String);
                }
                else
                {
                    parameters.Add("@Id", id.ToString(), DbType.String);
                }

                parameters.Add("@isMainSyllabus", isMainSyllabus, DbType.Boolean);
                parameters.Add("@SubjectId", subjectId, DbType.Int32);
                parameters.Add("@IsLesson", isLesson, DbType.Int32);
                return await conn.QueryAsync<Objective>("School.GetSubtopicObjectives", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Objective>> GetAssignmentsObjective(int assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                return await conn.QueryAsync<Objective>("Assignment.GetAssignmentObjective", parameters, null, null, CommandType.StoredProcedure);
            }
        }


        public async Task<IEnumerable<CourseUnit>> GetAssignmentUnit(int assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                return await conn.QueryAsync<CourseUnit>("Assignment.GetAssignmentUnit", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<GroupUnit>> GetAssignmentCourseTopicUnit(int assignmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                return await conn.QueryAsync<GroupUnit>("Assignment.GetAssignmentCourseTopicUnit", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Objective>> GetSubtopicsObjectiveWithMultipleSubject(int id, bool isMainSyllabus, string subjectId, bool isLesson)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                if (isMainSyllabus)
                {
                    parameters.Add("@Id", "x" + id.ToString(), DbType.String);
                }
                else
                {
                    parameters.Add("@Id", id.ToString(), DbType.String);
                }

                parameters.Add("@isMainSyllabus", isMainSyllabus, DbType.Boolean);
                parameters.Add("@SubjectIds", subjectId, DbType.String);
                parameters.Add("@IsLesson", isLesson, DbType.Int32);
                return await conn.QueryAsync<Objective>("[School].[GetSubtopicObjectivesWithMultipleSubjects]", parameters, null, null, CommandType.StoredProcedure);
            }
        }


        #region Private Methods
        private DynamicParameters GetSubjectsParameters(Subject entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int32);
            parameters.Add("@SubjectId", entity.SubjectId, DbType.Int32);
            parameters.Add("@SubjectName", entity.SubjectName, DbType.String);
            parameters.Add("@SubjectCode", entity.SubjectCode, DbType.String);
            parameters.Add("@SubjectColorCode", entity.SubjectColorCode, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedById, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        #region Generated Methods

        public async override Task<Subject> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SubjectId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<Subject>("School.GetSubjectById", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<Subject>> GetAllAsync()
        {
            throw new NotImplementedException();
        }


        public override void InsertAsync(Subject entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(Subject entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
