﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;


namespace Phoenix.API.Repositories
{
    public class MarkingSchemeRepository : SqlRepository<MarkingScheme>,IMarkingSchemeRepository
    {
        private readonly IConfiguration _config;

        public MarkingSchemeRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<MarkingScheme>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async override Task<MarkingScheme> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@MarkingSchemeId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<MarkingScheme>("School.GetMarkingSchemeById", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<MarkingScheme>> GetMarkingSchemes(int schoolId, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<MarkingScheme>("School.GetMarkingSchemes", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int UpdateMarkingScheme(MarkingScheme entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetMarkingSchemeParameters(entity, mode);
                conn.Query<int>("School.MarkingSchemeCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<MarkingScheme> GetMarkingSchemeById(int id,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@MarkingSchemeId", id, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryFirstOrDefaultAsync<MarkingScheme>("School.GetMarkingSchemeById", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #region Private Methods
        private DynamicParameters GetMarkingSchemeParameters(MarkingScheme entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int32);
            parameters.Add("@MarkingSchemeId", entity.MarkingSchemeId, DbType.Int32);
            parameters.Add("@ColourCode", entity.ColourCode, DbType.String);
            parameters.Add("@MarkingName", entity.MarkingName, DbType.String);
            parameters.Add("@MarkingDesc", entity.MarkingDesc, DbType.String);         
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedBy, DbType.Int32);
            parameters.Add("@Weight", entity.Weight, DbType.Decimal);
            parameters.Add("@MarkingSchemeXml", entity.MarkingSchemeXml, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion
        public override void InsertAsync(MarkingScheme entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(MarkingScheme entityToUpdate)
        {
            throw new NotImplementedException();
        }


    }
}
