﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class StudentSuggestionRepository : SqlRepository<StudentSuggestion>,IStudentSuggestionRepository
    {
        private readonly IConfiguration _config;
        public StudentSuggestionRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<Suggestion>> GetStudentSuggestion()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
               //need to change the sp name
                return await conn.QueryAsync<Suggestion>("School.GetSafetyCategories", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Suggestion>> GetStudentSuggestionsByUserId(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int32);
                return await conn.QueryAsync<Suggestion>("[Student].[GetStudentSuggestionByUserId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<Suggestion> GetStudentSuggestionById(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SuggestionId", id, DbType.Int32);
                //Need to change the SP
                return await conn.QueryFirstOrDefaultAsync<Suggestion>("[Student].[GetStudentSuggestionById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<Suggestion> GetStudentSuggestionByIdAndUserId(int id, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SuggestionId", id, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
               
                return await conn.QueryFirstOrDefaultAsync<Suggestion>("[Student].[GetStudentSuggestionByIdAndUserId]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public int UpdateStudentSuggestion(Suggestion entity, TransactionModes mode)
        { 
            using (var conn = GetOpenConnection())
            {
                var parameters = GetStudentSuggestionParameters(entity, mode);
                conn.Query<int>("[Student].[SuggestionsCUD]", parameters, commandType: CommandType.StoredProcedure); // Sp modification
                return parameters.Get<int>("output");
            }
        }

        public async Task<IEnumerable<Suggestion>> PaginateStudentSuggestionByUserId(int userId, int pageNumber, int pageSize, string searchString = "", string sortBy = "")
        {
            var list = new List<Suggestion>();
            using (var conn = GetOpenConnection())
            {
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@UserId", userId, DbType.Int32);
                    parameters.Add("@PageNum", pageNumber, DbType.Int32);
                    parameters.Add("@PageSize", pageSize, DbType.Int32);
                    parameters.Add("@SearchString", String.IsNullOrEmpty(searchString) ? String.Empty : searchString, DbType.String);
                    parameters.Add("@SortBy", String.IsNullOrEmpty(sortBy) ? String.Empty : sortBy, DbType.String);
                    var result = await conn.QueryMultipleAsync("[School].[PaginateStudentSuggestionByUserId]", parameters, null, null, CommandType.StoredProcedure);
                    var StudentSuggestion = await result.ReadAsync<Suggestion>();
                    var StudentSuggestionCount = await result.ReadAsync<int>();
                    list = StudentSuggestion.ToList();
                    int totalCount = StudentSuggestionCount.FirstOrDefault();
                    if (list.Any())
                    {
                        list.ForEach(x => { x.TotalCount = totalCount; });
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                return list;
            }
        }
        public async Task<IEnumerable<Suggestion>> PaginateStudentSuggestionBySchoolId(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy = "",short langaugeId=1)
        {
            var list = new List<Suggestion>();
            using (var conn = GetOpenConnection())
            {
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@SchoolId", schoolId, DbType.Int32);
                    parameters.Add("@PageNum", pageNumber, DbType.Int32);
                    parameters.Add("@PageSize", pageSize, DbType.Int32);
                    parameters.Add("@SearchString", String.IsNullOrEmpty(searchString) ? String.Empty : searchString, DbType.String);
                    parameters.Add("@SortBy", String.IsNullOrEmpty(sortBy) ? String.Empty : sortBy, DbType.String);
                    parameters.Add("@languageId", langaugeId, DbType.Int16);
                    var result = await conn.QueryMultipleAsync("[School].[PaginateStudentSuggestionBySchoolId]", parameters, null, null, CommandType.StoredProcedure);
                    var StudentSuggestion = await result.ReadAsync<Suggestion>();
                    var StudentSuggestionCount = await result.ReadAsync<int>();
                    list = StudentSuggestion.ToList();
                    int totalCount = StudentSuggestionCount.FirstOrDefault();
                    if (list.Any())
                    {
                        list.ForEach(x => { x.TotalCount = totalCount; });
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                return list;
            }
        }

        #region Private Methods  
        private DynamicParameters GetStudentSuggestionParameters(Suggestion entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SuggestionId", entity.SuggestionId, DbType.Int32);
            parameters.Add("@UserId", entity.UserId, DbType.Int32);
            parameters.Add("@Title", entity.Title, DbType.String);
            parameters.Add("@Description", entity.Description, DbType.String);
            parameters.Add("@TypeId", entity.TypeId, DbType.Int32);
            parameters.Add("@FileName", entity.FileName, DbType.String);
            parameters.Add("@PhysicalPath", entity.PhysicalPath, DbType.String);
            parameters.Add("@Remark", entity.Remark, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        #region Generated Methods

        public async override Task<StudentSuggestion> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SafetyCategoryId", id, DbType.Int32); // need to change parameter and Stored Procedure
                return await conn.QueryFirstOrDefaultAsync<StudentSuggestion>("School.GetSafetyCategoryById", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<StudentSuggestion>> GetAllAsync()
        {
            throw new NotImplementedException();
        }


        public override void InsertAsync(StudentSuggestion entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(StudentSuggestion entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Think Box
        public int AddUpdateThinkBoxUser(ThinkBox entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", entity.SchoolId, DbType.Int32);
                parameters.Add("@UserId", entity.UserId, DbType.Int64);
                conn.Query<int>("[Admin].[AddUpdateThinkBoxUser]", parameters, commandType: CommandType.StoredProcedure); // Sp modification
                return parameters.Get<int>("output");
            }
        }
        public async Task<IEnumerable<ThinkBox>> GetThinkBoxUsersBySchoolId(long Id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", Id, DbType.Int64);
                return await conn.QueryAsync<ThinkBox>("[Admin].[GetUsersBySchoolId]", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public void DeleteUserFromThinkBox(long userId, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId",schoolId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                conn.Query<int>("[Admin].[DeleteUserFromThinkBox]", parameters, commandType: CommandType.StoredProcedure); // Sp modification
            }
        }
        public bool HasThinkBoxUserPermission(long userId)
        {
            using (var conn = GetOpenConnection())
            {
                bool isPermissionAssigned = false;
                var parameters = new DynamicParameters();
                parameters.Add("@UserID", userId, DbType.Int64);
                isPermissionAssigned = conn.QueryFirstOrDefault<bool>("SELECT [Admin].[HasThinkBoxUserPermission](@UserID)", parameters, commandType: CommandType.Text);
                return isPermissionAssigned;
            }
        }
        #endregion
        #region Suggestion List(Admin)
        public async Task<IEnumerable<Suggestion>> GetStudentSuggestionsBySchoolId(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", id, DbType.Int32);
                return await conn.QueryAsync<Suggestion>("[Student].[GetStudentSuggestionBySchoolId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        #endregion
    }
}
