﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class SuggestionCategoryRepository : SqlRepository<SuggestionCategory>, ISuggestionCategoryRepository
    {
        private readonly IConfiguration _config;
        public SuggestionCategoryRepository(IConfiguration configuration):base(configuration)
        {
            _config = configuration;
        }
    
        public async Task<IEnumerable<SuggestionCategory>> GetSuggestionCategory(int languageId, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@LanguageId", languageId, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);           
                return await conn.QueryAsync<SuggestionCategory>("[Admin].[GetSuggestionCategory]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int UpdateSuggestionCategory(SuggestionCategory entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSuggestionCategoryParameters(entity, mode);              
                conn.Query<int>("Admin.SuggestionCategoryCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<SuggestionCategory> GetSuggestionCategoryById(int id,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SuggestionCategoryId", id, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                //Need to change the SP
                return await conn.QueryFirstOrDefaultAsync<SuggestionCategory>("[Admin].[GetSuggestionCategoryById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #region Private Methods
        private DynamicParameters GetSuggestionCategoryParameters(SuggestionCategory entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int32);
            parameters.Add("@SuggestionCategoryId", entity.SuggestionCategoryId, DbType.Int32);
            parameters.Add("@SuggestionCategoryName", entity.SuggestionCategoryName, DbType.String);
            parameters.Add("@SuggestionCategoryXml", entity.SuggestionCategoryXml, DbType.String);//update after xml implementation
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedById, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        #region Generated Methods

        public async override Task<SuggestionCategory> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SuggestionCategoryId", id, DbType.Int32);
                //Need to change the SP
                return await conn.QueryFirstOrDefaultAsync<SuggestionCategory>("[Admin].[GetSuggestionCategoryById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<SuggestionCategory>> GetAllAsync()
        {
            throw new NotImplementedException();
        }


        public override void InsertAsync(SuggestionCategory entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SuggestionCategory entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
