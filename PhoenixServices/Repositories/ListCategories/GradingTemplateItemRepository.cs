﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class GradingTemplateItemRepository : SqlRepository<GradingTemplateItem>, IGradingTemplateItemRepository
    {
        private readonly IConfiguration _config;

        public GradingTemplateItemRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<GradingTemplateItem>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async override Task<GradingTemplateItem> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GradingTemplateItemId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<GradingTemplateItem>("School.GetGradingTemplateItembyId", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<GradingTemplateItem>> GetGradingTemplateItems(int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<GradingTemplateItem>("School.GetGradingTemplateItemsBySchoolId", parameters, null, null, CommandType.StoredProcedure);
            }
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<GradingTemplateItem>> GetGradingTemplateItemsByTemplateId(int schoolId,int SystemLanguageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", schoolId, DbType.Int32);
                parameters.Add("@SystemLanguageId", SystemLanguageId, DbType.Int32);
                return await conn.QueryAsync<GradingTemplateItem>("School.GetGradingTemplateItemsByTemplateId", parameters, null, null, CommandType.StoredProcedure);
            }
            throw new NotImplementedException();
        }
        public async Task<GradingTemplateItem> GetGradingTemplateItembyId(int id, int SystemLanguageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GradingTemplateItemId", id, DbType.Int32);
                parameters.Add("@SystemLanguageId", SystemLanguageId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<GradingTemplateItem>("School.GetGradingTemplateItembyId", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(GradingTemplateItem entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(GradingTemplateItem entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public int UpdateGradingTemplateItemData(GradingTemplateItem entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetGradingTemplateItemParameters(entity, mode);
                conn.Query<int>("School.GradingTemplateItemCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }


        #region Private Methods
        private DynamicParameters GetGradingTemplateItemParameters(GradingTemplateItem entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@GradingTemplateItemId", entity.GradingTemplateItemId, DbType.Int32);
            parameters.Add("@GradingTemplateId", entity.GradingTemplateId, DbType.Int32);
            parameters.Add("@GradingColor", entity.GradingColor, DbType.String);
            parameters.Add("@ShortLabel", entity.ShortLabel, DbType.String);
            parameters.Add("@GradingItemDescription", entity.GradingItemDescription, DbType.String);
            parameters.Add("@GradingTemplateItemSymbol", entity.GradingTemplateItemSymbol, DbType.String);
            parameters.Add("@Percentage", entity.Percentage, DbType.String);
            parameters.Add("@SortOrder", entity.SortOrder, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@IsDeleted", entity.IsDeleted, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedBy, DbType.Int32);
            parameters.Add("@ScoreFrom", entity.ScoreFrom, DbType.Double);
            parameters.Add("@ScoreTo", entity.ScoreTo, DbType.Double);
            parameters.Add("@GradingTemplateItemXml", entity.GradingTemplateItemXml, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion
    }
}
