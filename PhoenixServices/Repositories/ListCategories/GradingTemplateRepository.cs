﻿using Phoenix.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Phoenix.Common.Enums;

namespace Phoenix.API.Repositories
{
    public class GradingTemplateRepository : SqlRepository<GradingTemplate>, IGradingTemplateRepository
    {
        private readonly IConfiguration _config;

        public GradingTemplateRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<GradingTemplate>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async override Task<GradingTemplate> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GradingTemplateId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<GradingTemplate>("School.GetGradingTemplateById", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<GradingTemplate>> GetGradingTemplates(int languageId,int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@LanguageId", languageId, DbType.Int32);
                return await conn.QueryAsync<GradingTemplate>("School.GetGradingTemplates", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int UpdateGradingTemplateData(GradingTemplate entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetGradingTemplateParameters(entity, mode);
                conn.Query<int>("School.GradingTemplateCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<GradingTemplate> GetGradingTemplateById(int id,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GradingTemplateId", id, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryFirstOrDefaultAsync<GradingTemplate>("School.GetGradingTemplateById", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #region Private Methods
        private DynamicParameters GetGradingTemplateParameters(GradingTemplate entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int32);
            parameters.Add("@GradingTemplateId", entity.GradingTemplateId, DbType.Int32);
            parameters.Add("@GradingTemplateTitle", entity.GradingTemplateTitle, DbType.String);
            parameters.Add("@GradingTemplateDesc", entity.GradingTemplateDesc, DbType.String);
            parameters.Add("@AllowAdditionalGrade", entity.AllowAdditionalGrade, DbType.Boolean);
            parameters.Add("@GradingTemplateLogo", entity.GradingTemplateLogo, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedBy, DbType.Int32);
            parameters.Add("@GradingTemplateXml", entity.GradingTemplateXml, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        public override void InsertAsync(GradingTemplate entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(GradingTemplate entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
