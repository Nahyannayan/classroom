﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IGradingTemplateItemRepository
    {
        Task<IEnumerable<GradingTemplateItem>> GetGradingTemplateItems(int schoolId);
        Task<IEnumerable<GradingTemplateItem>> GetGradingTemplateItemsByTemplateId(int schoolId, int SystemLanguageId);
        Task<GradingTemplateItem> GetAsync(int id);
        int UpdateGradingTemplateItemData(GradingTemplateItem entity, TransactionModes mode);
        Task<GradingTemplateItem> GetGradingTemplateItembyId(int id, int SystemLanguageId);
    }
}
