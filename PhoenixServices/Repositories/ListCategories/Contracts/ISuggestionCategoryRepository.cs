﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface ISuggestionCategoryRepository : IGenericRepository<SuggestionCategory>
    {
        Task<IEnumerable<SuggestionCategory>> GetSuggestionCategory(int languageId, int schoolId);
        int UpdateSuggestionCategory(SuggestionCategory entity, TransactionModes mode);
        Task<SuggestionCategory> GetSuggestionCategoryById(int id, short languageId);
    }
}
