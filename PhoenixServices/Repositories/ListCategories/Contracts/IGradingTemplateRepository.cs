﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IGradingTemplateRepository
    {
        Task<IEnumerable<GradingTemplate>> GetGradingTemplates(int languageId, int schoolId);
        Task<GradingTemplate> GetAsync(int id);
        int UpdateGradingTemplateData(GradingTemplate entity, TransactionModes mode);
        Task<GradingTemplate> GetGradingTemplateById(int id, short languageId);
    }
}
