﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using Phoenix.Common.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public interface ISubjectRepository : IGenericRepository<Subject>
    {
        Task<IEnumerable<Subject>> GetSubjects(int languageId, int schoolId);
        Task<IEnumerable<Subject>> GetTopTenSubjects(int schoolId);
        bool UpdateSubjectData(Subject entity, TransactionModes mode);
        Task<IEnumerable<TopicSubTopicStructure>> GetTopicSubTopicStructure(int subjectId);
        Task<IEnumerable<Objective>> GetLessonsByTopicId(int Id);
        Task<IEnumerable<GroupUnit>> GetSelectedUnitByGroupId(string Id); 
        Task<IEnumerable<TopicSubTopicStructure>> GetUnitStructure(string courseIds);
        Task<IEnumerable<Objective>> GetSubtopicsObjective(int id, bool isMainSyllabus, int subjectId, bool isLesson);
        Task<IEnumerable<Objective>> GetAssignmentsObjective(int assignmentId);
        Task<IEnumerable<CourseUnit>> GetAssignmentUnit(int assignmentId);
        Task<IEnumerable<GroupUnit>> GetAssignmentCourseTopicUnit(int assignmentId);
        Task<IEnumerable<TopicSubTopicStructure>> GetTopicSubTopicStructureByMultipleSubjectIds(string subjectIds);
        Task<IEnumerable<Objective>> GetSubtopicsObjectiveWithMultipleSubject(int id, bool isMainSyllabus, string subjectIds, bool isLesson);
    }
}
