﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IStudentSuggestionRepository : IGenericRepository<StudentSuggestion>
    {
        Task<IEnumerable<Suggestion>> GetStudentSuggestion();
        int UpdateStudentSuggestion(Suggestion entity, TransactionModes mode);
        Task<IEnumerable<Suggestion>> GetStudentSuggestionsByUserId(int id);
        Task<IEnumerable<Suggestion>> GetStudentSuggestionsBySchoolId(int id);
        Task<Suggestion> GetStudentSuggestionById(int id);
        int AddUpdateThinkBoxUser(ThinkBox entity);
        Task<IEnumerable<ThinkBox>> GetThinkBoxUsersBySchoolId(long Id);
        void DeleteUserFromThinkBox(long userId, long schoolId);
        bool HasThinkBoxUserPermission(long userId);
        Task<IEnumerable<Suggestion>> PaginateStudentSuggestionByUserId(int userId, int pageNumber, int pageSize, string searchString = "", string sortBy = "");
        Task<IEnumerable<Suggestion>> PaginateStudentSuggestionBySchoolId(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy = "",short languageId=1);
        Task<Suggestion> GetStudentSuggestionByIdAndUserId(int id, long userId);
    }
}
