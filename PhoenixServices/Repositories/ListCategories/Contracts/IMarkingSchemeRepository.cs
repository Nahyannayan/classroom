﻿using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IMarkingSchemeRepository
    {
        Task<IEnumerable<MarkingScheme>> GetMarkingSchemes(int schoolId, short languageId);        
        Task<MarkingScheme> GetAsync(int id);
        int UpdateMarkingScheme(MarkingScheme entity, TransactionModes mode);
        Task<MarkingScheme> GetMarkingSchemeById(int id, short languageId);
    }
}
