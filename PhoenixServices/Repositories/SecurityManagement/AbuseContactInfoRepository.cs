﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;

namespace Phoenix.API.Repositories
{
    public class AbuseContactInfoRepository : SqlRepository<AbuseContactInfo>, IAbuseContactInfoRepository
    {
        private readonly IConfiguration _config;

        public AbuseContactInfoRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            UpdateAbuseContactInfoData(new AbuseContactInfo(id), TransactionModes.Delete);
        }

        public async Task<AbuseContactInfo> GetAbuseContactInfo(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryFirstAsync<AbuseContactInfo>("School.GetAbuseContactInfo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async override Task<IEnumerable<AbuseContactInfo>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                return await conn.QueryAsync<AbuseContactInfo>("School.GetAbuseContactInfo", null, commandType: CommandType.StoredProcedure);
            }
        }

        public async override Task<AbuseContactInfo> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AbuseContactInfoId", id, DbType.Int32);
                return await conn.QueryFirstAsync<AbuseContactInfo>("School.GetAbuseContactInfo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(AbuseContactInfo entity)
        {
            UpdateAbuseContactInfoData(entity, TransactionModes.Insert);
        }

        public async Task<bool> SaveMobileSettings(AbuseContactInfo model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters =new DynamicParameters();
                parameters.Add("@AbuseContactInfoId", model.AbuseContactInfoId, DbType.Int64);
                parameters.Add("@EnableCallingPolice", model.EnableCallingPolice, DbType.Boolean);
                parameters.Add("@PolicePhoneNumber", model.PolicePhoneNumber, DbType.String);
                parameters.Add("@EnableSOS", model.IsSOSEnabledInMobile, DbType.Boolean);
                parameters.Add("@EnableReportAbuse", model.IsReportAbuseEnabledInMobile, DbType.Boolean);
                parameters.Add("@SchoolId", model.SchoolId, DbType.Int64);
                parameters.Add("@UpdatedBy", model.UpdatedBy, DbType.Int64);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync<int>("School.SaveAbuseDelegateMobileSettings", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("Output") > 0;
            }
        }

        public Int64 UpdateAbuseContactInfoData(AbuseContactInfo entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAbuseContactInfoParameters(entity, mode);
                conn.Query<Int64>("School.AbuseContactInfoCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int64>("Output");
            }
        }

        public override void UpdateAsync(AbuseContactInfo entityToUpdate)
        {
            UpdateAbuseContactInfoData(entityToUpdate, TransactionModes.Update);
        }

        #region Private Methods
        private DynamicParameters GetAbuseContactInfoParameters(AbuseContactInfo entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@AbuseContactInfoId", entity.AbuseContactInfoId, DbType.Int64);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@ContactMessage", entity.ContactMessage, DbType.String);
            parameters.Add("@ContactNumber", entity.ContactNumber, DbType.String);
            parameters.Add("@ContactEmail", entity.ContactEmail, DbType.String);
            parameters.Add("@CensorNoticeRecieverId", entity.CensorNoticeRecieverId, DbType.Int64);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            //parameters.Add("@EnableCallingPolice", entity.EnableCallingPolice, DbType.Boolean);
            //parameters.Add("@PolicePhoneNumber", entity.PolicePhoneNumber, DbType.String);
            //parameters.Add("@EnableSOS", entity.IsSOSEnabledInMobile, DbType.Boolean);
            //parameters.Add("@EnableReportAbuse", entity.IsReportAbuseEnabledInMobile, DbType.Boolean);
            parameters.Add("@Output", dbType: DbType.Int64, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion
    }
}
