﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;


namespace Phoenix.API.Repositories
{
    public class AbuseDelegateRepository : SqlRepository<AbuseDelegate>, IAbuseDelegateRepository
    {
        private readonly IConfiguration _config;

        public AbuseDelegateRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            UpdateAbuseDelegateData(new AbuseDelegate(id), TransactionModes.Delete);
        }

        public async Task<IEnumerable<AbuseDelegate>> GetAbuseDelegate(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AbuseDelegateId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<AbuseDelegate>("School.GetAbuseDelegate", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async override Task<IEnumerable<AbuseDelegate>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AbuseDelegateId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int64);
                return await conn.QueryAsync<AbuseDelegate>("School.GetAbuseDelegate", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async override Task<AbuseDelegate> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AbuseDelegateId", id, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int64);
                return await conn.QueryFirstAsync<AbuseDelegate>("School.GetAbuseDelegate", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(AbuseDelegate entity)
        {
            UpdateAbuseDelegateData(entity, TransactionModes.Insert);
        }

        public Int64 UpdateAbuseDelegateData(AbuseDelegate entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAbuseDelegateParameters(entity, mode);
                conn.Query<Int32>("School.AbuseDelegateCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public override void UpdateAsync(AbuseDelegate entityToUpdate)
        {
            UpdateAbuseDelegateData(entityToUpdate, TransactionModes.Update);
        }

        #region Private Methods
        private DynamicParameters GetAbuseDelegateParameters(AbuseDelegate entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@AbuseDelegateId", entity.AbuseDelegateId, DbType.Int64);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@DelegateId", entity.DelegateId, DbType.Int64);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion
    }
}
