﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;

namespace Phoenix.API.Repositories
{
    public class SafetyCategoriesRepository : SqlRepository<SafetyCategory>, ISafetyCategoriesRepository
    {
        private readonly IConfiguration _config;

        public SafetyCategoriesRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<SafetyCategory>> GetSafetyCategories(int languageId, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@LanguageId", languageId, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<SafetyCategory>("School.GetSafetyCategories", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int UpdateSafetyCategoryData(SafetyCategory entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSafetyCategoriesParameters(entity, mode);
                conn.Query<int>("School.SafefyCategoryCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<SafetyCategory> GetSafetyCategoryById(int id,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SafetyCategoryId", id, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryFirstOrDefaultAsync<SafetyCategory>("School.GetSafetyCategoryById", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #region Private Methods
        private DynamicParameters GetSafetyCategoriesParameters(SafetyCategory entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int32);
            parameters.Add("@SafetyCategoryId", entity.SafetyCategoryId, DbType.Int32);
            parameters.Add("@SafetyCategoryName", entity.SafetyCategoryName, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedById, DbType.Int32);
            parameters.Add("@SafetyCategoryXml", entity.SafetyCategoryXml, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        #region Generated Methods

        public async override Task<SafetyCategory> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SafetyCategoryId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<SafetyCategory>("School.GetSafetyCategoryById", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<SafetyCategory>> GetAllAsync()
        {
            throw new NotImplementedException();
        }


        public override void InsertAsync(SafetyCategory entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SafetyCategory entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
