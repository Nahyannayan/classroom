﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Phoenix.API.Repositories
{
    public interface IAbuseDelegateRepository : IGenericRepository<AbuseDelegate>
    {
        Task<IEnumerable<AbuseDelegate>> GetAbuseDelegate(long schoolId);
        Int64 UpdateAbuseDelegateData(AbuseDelegate entity, TransactionModes mode);
    }
}
