﻿using Phoenix.Models;
using DbConnection;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface ICensorFilterRepository: IGenericRepository<CensorUser>
    {
        #region Banned Words
        Task<IEnumerable<BannedWord>> GetBannedWords(int schoolId);
        bool UpdateBannedWordData(BannedWord entity, TransactionModes mode);
        #endregion

        #region Censor User
        Task<IEnumerable<CensorUser>> GetCensorUsers(int schoolId);
        #endregion

    }
}
