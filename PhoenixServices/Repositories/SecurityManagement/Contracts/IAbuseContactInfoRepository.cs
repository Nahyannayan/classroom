﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System.Threading.Tasks;
using System;

namespace Phoenix.API.Repositories
{
    public interface IAbuseContactInfoRepository : IGenericRepository<AbuseContactInfo>
    {
        Task<AbuseContactInfo> GetAbuseContactInfo(long schoolId);
        Int64 UpdateAbuseContactInfoData(AbuseContactInfo entity, TransactionModes mode);
        Task<bool> SaveMobileSettings(AbuseContactInfo model);
    }
}
