﻿using Phoenix.Models;
using DbConnection;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface ISafetyCategoriesRepository: IGenericRepository<SafetyCategory>
    {
        Task<IEnumerable<SafetyCategory>> GetSafetyCategories(int languageId, int schoolId);
        int UpdateSafetyCategoryData(SafetyCategory entity, TransactionModes mode);
        Task<SafetyCategory> GetSafetyCategoryById(int id, short languageId);

    }
}
