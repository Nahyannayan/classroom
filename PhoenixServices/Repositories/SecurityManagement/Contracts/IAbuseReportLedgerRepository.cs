﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IAbuseReportLedgerRepository : IGenericRepository<AbuseReportLedger>
    {
        Task<AbuseReportLedger> GetByAbuseReportLedgerIdAsync(int AbuseReportLedgerId);
        Task<IEnumerable<AbuseReportLedger>> GetAbuseReportListAsPerUser(int UserId);
        Int64 InsertUpdateAbuseReportLedgerData(AbuseReportLedger entity, TransactionModes mode);
    }
}
