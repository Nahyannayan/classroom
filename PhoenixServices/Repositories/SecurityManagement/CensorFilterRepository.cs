﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;

namespace Phoenix.API.Repositories
{
    public class CensorFilterRepository : SqlRepository<CensorUser>, ICensorFilterRepository 
    {
        private readonly IConfiguration _config;

        public CensorFilterRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<BannedWord>> GetBannedWords(int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<BannedWord>("School.GetBannedWords", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public bool UpdateBannedWordData(BannedWord entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBannedWordParameters(entity, mode);
                conn.Query<int>("School.BannedWordCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<CensorUser>> GetCensorUsers(int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<CensorUser>("School.GetCensoredUserDetailedList", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #region Private Methods
        private DynamicParameters GetBannedWordParameters(BannedWord entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int32);
            parameters.Add("@PreviousBannedWord", entity.PreviousBannedWord, DbType.String);
            parameters.Add("@BannedWord", entity.BannedWordName, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedById, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        #region Generated Methods      

        public override void InsertAsync(CensorUser entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(CensorUser entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<CensorUser>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<CensorUser> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
