﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;

namespace Phoenix.API.Repositories
{
    public class AbuseReportLedgerRepository : SqlRepository<AbuseReportLedger>, IAbuseReportLedgerRepository
    {
        public AbuseReportLedgerRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public override void DeleteAsync(int id)
        {
            InsertUpdateAbuseReportLedgerData(new AbuseReportLedger(id), TransactionModes.Delete);
        }

        public async override Task<IEnumerable<AbuseReportLedger>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryFirstAsync<IEnumerable<AbuseReportLedger>>("School.GetAbuseReportLedger", null, commandType: CommandType.StoredProcedure);
            }
        }

        public async override Task<AbuseReportLedger> GetAsync(int SafetyCategoryId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SafetyCategoryId", SafetyCategoryId, DbType.Int32);
                return await conn.QueryFirstAsync<AbuseReportLedger>("School.GetAbuseReportLedger", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AbuseReportLedger>> GetAbuseReportListAsPerUser(int UserId)
        {
            IEnumerable<AbuseReportLedger> lst = new List<AbuseReportLedger>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", UserId, DbType.Int32);
                lst = await conn.QueryAsync<AbuseReportLedger>("School.GetReportAbuseListById", parameters, commandType: CommandType.StoredProcedure);
                lst.ToList().ForEach(x =>
                {
                    if (!string.IsNullOrEmpty(x.AttachmentsURL))
                        x.Attachments = x.AttachmentsURL.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
                });
                return lst;
            }
        }
        public async Task<AbuseReportLedger> GetByAbuseReportLedgerIdAsync(int AbuseReportLedgerId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AbuseReportLedgerId", AbuseReportLedgerId, DbType.Int64);
                return await conn.QueryFirstAsync<AbuseReportLedger>("School.GetAbuseReportLedger", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(AbuseReportLedger entity)
        {
            InsertUpdateAbuseReportLedgerData(entity, TransactionModes.Insert);
        }

        public long InsertUpdateAbuseReportLedgerData(AbuseReportLedger entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAbuseContactInfoParameters(entity, mode);
                conn.Query<Int64>("School.AbuseReportLedgerCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int64>("Output");
            }
        }

        public override void UpdateAsync(AbuseReportLedger entityToUpdate)
        {
            InsertUpdateAbuseReportLedgerData(entityToUpdate, TransactionModes.Update);
        }


        #region Private Methods
        private DynamicParameters GetAbuseContactInfoParameters(AbuseReportLedger entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@AbuseReportLedgerId", entity.AbuseReportLedgerId, DbType.Int64);
            parameters.Add("@SafetyCategoryId", entity.SafetyCategoryId, DbType.Int64);
            parameters.Add("@Message", entity.Message, DbType.String);
            parameters.Add("@IsEmail", entity.IsEmail, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@DeviceSerialNumber", entity.DeviceSerialNumber, DbType.String);
            parameters.Add("@DeviceIPAddress", entity.DeviceIPAddress, DbType.String);
            parameters.Add("@UpdatedBy", entity.UpdateBy, DbType.Int64);
            parameters.Add("@Location", entity.Location, DbType.String);
            parameters.Add("@Attachments", string.Join(",", entity.Attachments), DbType.String);
            parameters.Add("@Output", dbType: DbType.Int64, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion
    }
}
