﻿using DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.API.Models;
using Phoenix.Common.Helpers;

namespace Phoenix.API.Repositories
{
    public class LessonRepository : SqlRepository<UnitDetails>, ILessonRepository
    {
        private readonly IConfiguration _config;
        public LessonRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<UnitDetails>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<UnitDetails> GetAsync(int id)
        {
            throw new NotImplementedException();
        }



        public override void InsertAsync(UnitDetails entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(UnitDetails entityToUpdate)
        {
            throw new NotImplementedException();
        }
        public async Task<IEnumerable<UnitDetails>> GetUnitDetails()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                return await conn.QueryAsync<UnitDetails>("[Course].[GetUTMDetail]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<StandardDetailsValue>> GetStandardDetails()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                return await conn.QueryAsync<StandardDetailsValue>("[Course].[GetStandardDetail]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Lesson>> GetLessonDetails()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                return await conn.QueryAsync<Lesson>("[Course].[GetLessonPlannerDetail]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<LessonCourse>> GetTopicLessonDetail(long CourseId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CourseId", CourseId, DbType.Int32);
                return await conn.QueryAsync<LessonCourse>("[Course].[GetCourseTopicLessondetails]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<UnitDetails>> GetUnitBasedonCourse(long CourseId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CourseId", CourseId, DbType.Int32);
                return await conn.QueryAsync<UnitDetails>("[Course].[GetUnitBasedonCourse]", parameters, null, null, CommandType.StoredProcedure);
            }
        }


        public bool AddEditLesson(Lesson model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();

                parameters.Add("@TransMode", model.TransMode, DbType.Int32);
                parameters.Add("@LSP_ID", model.LessonId, DbType.Int32);


                //parameters.Add("@LSP_UTM_ID", model.UnitId, DbType.Int32);
                parameters.Add("@STD_ID", model.StandardId, DbType.String);
                if (model.TransMode == 0)
                {
                    parameters.Add("@LSP_START_DATE", DateTime.Now, DbType.DateTime);
                    parameters.Add("@LSP_END_DATE", DateTime.Now, DbType.DateTime);

                }
                else
                {
                    parameters.Add("@LSP_START_DATE", model.StartDate, DbType.DateTime);
                    parameters.Add("@LSP_END_DATE", model.EndDate, DbType.DateTime);
                }

                parameters.Add("@StandardCode", model.StandardCode, DbType.String);
                parameters.Add("@LSP_DETAILS", model.Details, DbType.String);
                parameters.Add("@Points", 0, DbType.Int32);
                parameters.Add("@Weight", 0, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[Course].[LessonCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
                //await conn.QueryFirstOrDefaultAsync<int>("[Course].[LessonCUD]", parameters, null, null, CommandType.StoredProcedure);
                //return parameters.Get<int>("Output");
            }
        }
        #region School Unit Detail type Mapping

        public async Task<bool> SchoolUnitDetailsTypeCD(SchoolUnitDetailTypeMapping typeMapping)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", typeMapping.SchoolId);
                parameters.Add("@unitTypeIds", string.Join(",", typeMapping.UnitDetailsTypeIds ?? new List<int>().ToArray()));
                parameters.Add("@divisionId", typeMapping.DivisionId);
                parameters.Add("@output", dbType: DbType.Boolean, direction: ParameterDirection.Output);
                await conn.QueryAsync("Course.SchoolUnitDetailsTypeCD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("output");
            }
        }

        public async Task<IEnumerable<SchoolUnitDetailTypeMapping>> GetSchoolUnitDetailTypes(long schoolId, int divisionId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId, DbType.Int32);
                parameters.Add("@divisionId", divisionId);
                return await conn.QueryAsync<SchoolUnitDetailTypeMapping>("Course.GetSchoolUnitDetailsType", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolUnitDetailType>> GetSchoolUnitDetailType(SchoolUnitDetailType schoolUnit)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolUnit.SchoolId);
                parameters.Add("@untiDetailid", schoolUnit.UnitDetailTypeId);
                return await conn.QueryAsync<SchoolUnitDetailType>("Course.GetSchoolUnitDetailType", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<bool> SaveSchoolUnitDetailType(SchoolUnitDetailType schoolUnit)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                var UnitCourseIds = schoolUnit.UnitCourseId != null ? string.Join(',', schoolUnit.UnitCourseId) : string.Empty;
                parameters.Add("@UnitDetailTypeId", schoolUnit.UnitDetailTypeId);
                parameters.Add("@Title", schoolUnit.Title);
                parameters.Add("@ControlType", schoolUnit.ControlType);
                parameters.Add("@UnitGroupName", schoolUnit.UnitGroupName);
                parameters.Add("@UnitGroupId", schoolUnit.UnitGroupId);
                parameters.Add("@AttachmentRequired", schoolUnit.AttachmentRequired);
                parameters.Add("@AttachmentKey", schoolUnit.AttachmentKey);
                parameters.Add("@Description", schoolUnit.Description);
                parameters.Add("@Order", schoolUnit.Order);
                parameters.Add("@SchoolId", schoolUnit.SchoolId);
                parameters.Add("@IsActive", schoolUnit.IsActive);
                parameters.Add("@UnitCourseIds", UnitCourseIds);
                parameters.Add("@output", dbType: DbType.Boolean, direction: ParameterDirection.Output);
                await conn.QueryAsync<SchoolUnitDetailType>("Course.SchoolUnitDetailTypeCUD", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<bool>("output");
            }
        }
        #endregion
    }
}
