﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class CourseRepository : SqlRepository<Course>, ICourseRepository
    {
        private readonly IAttachmentRepository attachmentRepository;

        public CourseRepository(IConfiguration configuration, IAttachmentRepository attachmentRepository) : base(configuration)
        {
            this.attachmentRepository = attachmentRepository;
        }
        #region Generated Methods
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<Course>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<Course> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(Course entity)
        {

        }

        public override void UpdateAsync(Course entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Public Methods
        public async Task<OperationDetails> CourseCU(Course course)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetCourseParameters(course);
                await conn.QueryAsync("Course.CoursesCU", parameters, commandType: CommandType.StoredProcedure);
                var id = parameters.Get<long>("output");
                course.AttachmentList.ForEach(x => x.AttachedToId = id);
                var result = await attachmentRepository.AttachmentCU(course.AttachmentList);
                var operationDetails = new OperationDetails();
                operationDetails.Success = result;
                operationDetails.InsertedRowId = Convert.ToInt32(id);
                return operationDetails;
            }
        }
        public async Task<IEnumerable<Course>> GetCourses(long curriculumId, long schoolId, int PageSize, int PageNumber, string SearchString)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@curriculumId", curriculumId);
                parameters.Add("@schoolId", schoolId);
                parameters.Add("@PageNum", PageNumber);
                parameters.Add("@PageSize", PageSize);
                parameters.Add("@searchString", SearchString);
                var result = await conn.QueryMultipleAsync("Course.getCourses", parameters, commandType: CommandType.StoredProcedure);
                var courses = await result.ReadAsync<Course>();
                var coursesDataList = courses.ToList();
                var totalCount = await result.ReadFirstOrDefaultAsync<int>();
                coursesDataList.ForEach(x => x.TotalCount = totalCount);
                return coursesDataList;
            }
        }
        public async Task<Course> GetCourseDetails(long courseId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@id", courseId);
                parameters.Add("@key", $"{AttachmentKey.Course.EnumToString()},{AttachmentKey.CourseImage.EnumToString()}");
                var result = await conn.QueryMultipleAsync("Course.getCourseDetails", parameters, commandType: CommandType.StoredProcedure);

                var courses = await result.ReadFirstOrDefaultAsync<Course>();
                if (courses != null)
                {
                    var attachments = await result.ReadAsync<Attachment>();
                    courses.AttachmentList = attachments.ToList();
                }
                return courses;
            }
        }
        public async Task<bool> IsCourseTitleUnique(string title)
        {
            using (var conn = GetOpenConnection())
            {
                var parameter = new DynamicParameters();
                parameter.Add("@title", title);
                var query = "SELECT [COR_ID] FROM [Course].[Courses] where [COR_TITLE] = @title";
                var result = await conn.QueryAsync(query, parameter, commandType: CommandType.Text);
                return !result.Any();
            }
        }
        public async Task<bool> CourseMappingCUD(CourseMapping course)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetCourseMappingParameter(course);
                await conn.QueryAsync("Course.CourseMappingCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("output");
            }
        }
        public async Task<IEnumerable<CourseMapping>> GetCourseMappings(long schoolId, long courseId, int pageNum = 1, int pageSize = 6, string searchString = "")
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId);
                parameters.Add("@courseId", courseId);
                parameters.Add("@pageNum", pageNum);
                parameters.Add("@pageSize", pageSize);
                parameters.Add("@searchString", searchString);
                var result = await conn.QueryMultipleAsync("Course.GetCourseMappingList", parameters, commandType: CommandType.StoredProcedure);
                var mapping = await result.ReadAsync<CourseMapping>();
                var mappingData = mapping.ToList();
                var grades = await result.ReadAsync<GroupGrade>();
                var teachers = await result.ReadAsync<GroupTeacher>();
                var totalCount = await result.ReadFirstOrDefaultAsync<int>();
                mappingData.ForEach(y =>
                {
                    y.GroupGrades = grades.Where(x => x.SchoolGroupId == y.SchoolGroupId);
                    y.GroupTeachers = teachers.Where(x => x.SchoolGroupId == y.SchoolGroupId);
                    y.TotalCount = totalCount;
                });
                return mapping;
            }
        }
        public async Task<CourseMapping> GetCourseMappingDetail(long courseGroupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@courseMappingId", courseGroupId);
                var result = await conn.QueryMultipleAsync("Course.GetCourseMappingDetails", parameters, commandType: CommandType.StoredProcedure);
                var mapping = await result.ReadFirstOrDefaultAsync<CourseMapping>();
                var students = await result.ReadAsync<CourseStudentAndGrade>();
                //var teachers = await result.ReadAsync<long>();
                var teachers = await result.ReadAsync<GroupAssignTeacherDuration>();
                if (mapping != null)
                {
                    mapping.Students = students;
                    //mapping.TeacherIds = teachers.ToArray();
                    mapping.GroupAssignTeachers = teachers;
                }
                return mapping;
            }
        }

        public async Task<bool> IsGroupNameUnique(string title)
        {
            using (var conn = GetOpenConnection())
            {
                var parameter = new DynamicParameters();
                parameter.Add("@title", title);
                var query = "SELECT SchoolGroupId FROM School.SchoolGroups where SchoolGroupName = @title";
                var result = await conn.QueryAsync(query, parameter, commandType: CommandType.Text);
                return !result.Any();
            }
        }
        #endregion

        #region Teacher Cross School Permission
        public async Task<IEnumerable<SchoolWiseGrade>> GetGradeListBySchoolId(string SchoolIds, long TeacherId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolIds", SchoolIds, DbType.String);
                parameters.Add("@TeacherId", TeacherId, DbType.Int64);
                return await conn.QueryAsync<SchoolWiseGrade>("Course.GetGradeListBySchoolId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<bool> SaveCrossSchoolPermission(CrossSchoolPermission crossSchoolPermission)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TCS_ID", crossSchoolPermission.TCS_ID, DbType.Int64);
                parameters.Add("@TeacherId", crossSchoolPermission.TeacherId, DbType.Int64);
                parameters.Add("@SchoolIds", crossSchoolPermission.SchoolIds, DbType.String);
                parameters.Add("@SchoolId", crossSchoolPermission.SchoolId, DbType.Int64);
                parameters.Add("@AssignFrom", crossSchoolPermission.AssignFrom);
                parameters.Add("@AssignTo", crossSchoolPermission.AssignTo);
                parameters.Add("@CreatedBy", crossSchoolPermission.UserId, DbType.Int64);
                parameters.Add("@IsDelete", crossSchoolPermission.IsDelete);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("Course.SaveCrossSchoolPermission", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<IEnumerable<CrossSchoolPermission>> GetCrossSchoolPermissionList(long TeacherId, long SchoolId, long TCS_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TeacherId", TeacherId, DbType.Int64);
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                parameters.Add("@TCS_ID", TCS_ID);
                return await conn.QueryAsync<CrossSchoolPermission>("Course.GetCrossSchoolPermissionList", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SchoolList>> GetSchoolListByTeacherId(string TeacherId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TeacherId", TeacherId, DbType.String);
                return await conn.QueryAsync<SchoolList>("[Course].[GetSchoolListByTeacherId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        //public async Task<IEnumerable<SchoolWiseGrade>> GeteGradeListPassByschoolIds(string SchoolId, string TeacherId)
        //{
        //    using (var conn = GetOpenConnection())
        //    {
        //        var parameters = new DynamicParameters();
        //        parameters.Add("@SchoolId", SchoolId, DbType.String);
        //        parameters.Add("@TeacherId", TeacherId, DbType.String);
        //        return await conn.QueryAsync<SchoolWiseGrade>("[Course].[GeteGradeListPassByschoolIds]", parameters, null, null, CommandType.StoredProcedure);
        //    }
        //}

        public async Task<IEnumerable<GroupTeacher>> GetSchoolTeachersBySchoolId(string SchoolId, string TeacherId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.String);
                return await conn.QueryAsync<GroupTeacher>("Course.[GetSchoolTeachersBySchoolId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }


        #endregion

        #region private methods
        private DynamicParameters GetCourseParameters(Course course)
        {
            var parameter = new DynamicParameters();
            parameter.Add("@cor_id", course.CourseId);
            parameter.Add("@curriculumId", course.CurriculumId);
            parameter.Add("@schoolId", course.SchoolId);
            parameter.Add("@cor_title", course.Title);
            parameter.Add("@cor_description", course.Description);
            parameter.Add("@cor_type", course.CourseType);
            parameter.Add("@cor_start_date", course.StartDate);
            parameter.Add("@cor_end_date", course.EndDate);
            parameter.Add("@cor_created_by", course.CreatedBy);
            parameter.Add("@cor_maxenrollment", course.MaximumEnrollment);
            parameter.Add("@cor_coursehours", course.CourseHours, DbType.Double);
            parameter.Add("@DepartmentId", course.DepartmentId, DbType.Int64);
            parameter.Add("@output", direction: ParameterDirection.Output, dbType: DbType.Int64);

            return parameter;
        }
        private DynamicParameters GetCourseMappingParameter(CourseMapping courseMapping)
        {
            var parameter = new DynamicParameters();
            parameter.Add("@courseId", courseMapping.CourseId);
            //parameter.Add("@teacherId", string.Join(',', courseMapping.TeacherIds));
            parameter.Add("@groupName", courseMapping.SchoolGroupName);
            parameter.Add("@SchoolGroupId", courseMapping.SchoolGroupId);
            parameter.Add("@gradeIds", string.Join(',', courseMapping.GradeIds));
            parameter.Add("@createdBy", courseMapping.CreatedBy);
            DataTable studentIds = new DataTable();
            studentIds.Columns.Add("PrimaryId", typeof(long));
            studentIds.Columns.Add("TypeId", typeof(long));
            foreach (var item in courseMapping.Students)
            {
                studentIds.Rows.Add(item.StudentId, item.GradeId);
            }
            parameter.Add("@studentId", studentIds, DbType.Object);

            DataTable teachers = new DataTable();
            teachers.Columns.Add("TeacherId", typeof(long));
            teachers.Columns.Add("AssignFrom", typeof(DateTime));
            teachers.Columns.Add("AssignTo", typeof(DateTime));
            teachers.Columns.Add("IsCoreTeacher", typeof(bool));

            foreach (var item in courseMapping.GroupAssignTeachers)
            {
                teachers.Rows.Add(item.TeacherId, item.AssignFrom, item.AssignTo, item.IsCoreTeacher);
            }

            parameter.Add("@teacherId", teachers, DbType.Object);
            parameter.Add("@output", direction: ParameterDirection.Output, dbType: DbType.Boolean);
            return parameter;
        }
        #endregion


    }
}
