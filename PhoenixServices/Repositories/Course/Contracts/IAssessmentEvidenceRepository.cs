﻿using DbConnection;
using Phoenix.API.Models;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
  public interface IAssessmentEvidenceRepository
    {
        Task<OperationDetails> SaveUpdateAssessmentEvidenceDetails(AssessmentEvidence model,long UserId);
        Task<IEnumerable<AssessmentEvidence>> GetAssessmentEvidenceList(long UnitId);
        Task<AssessmentEvidence> GetAssessmentEvidenceDetailsById(long AssessmentId);
        
    }
}
