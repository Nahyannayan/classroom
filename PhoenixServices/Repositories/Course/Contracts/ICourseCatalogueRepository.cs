﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface ICourseCatalogueRepository
    {
        Task<GroupCatalogue> GetCatalogueInfoUsingGroupId(long groupId);
        Task<IEnumerable<CourseCatalogue>> GetCourseCatalogueInformationByTeacher(long memberId, bool isStudent);
        Task<IEnumerable<CatalogueUnits>> GetCatalogueUnits(long courseId, long memberId, bool isStudent);
    }
}
