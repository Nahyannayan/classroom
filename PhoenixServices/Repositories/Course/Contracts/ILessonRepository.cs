﻿using Phoenix.API.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface ILessonRepository
    {
        Task<IEnumerable<UnitDetails>> GetUnitDetails();
        Task<IEnumerable<StandardDetailsValue>> GetStandardDetails();

        Task<IEnumerable<Lesson>> GetLessonDetails();

        Task<IEnumerable<LessonCourse>> GetTopicLessonDetail(long CourseId);
        Task<IEnumerable<UnitDetails>> GetUnitBasedonCourse(long CourseId);
        bool AddEditLesson(Lesson model);

        #region School Unit Detail type Mapping
        Task<bool> SchoolUnitDetailsTypeCD(SchoolUnitDetailTypeMapping typeMapping);
        Task<IEnumerable<SchoolUnitDetailTypeMapping>> GetSchoolUnitDetailTypes(long schoolId, int divisionId);
        Task<IEnumerable<SchoolUnitDetailType>> GetSchoolUnitDetailType(SchoolUnitDetailType schoolUnit);
        Task<bool> SaveSchoolUnitDetailType(SchoolUnitDetailType schoolUnit);
        #endregion
    }
}
