﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IAttachmentRepository
    {
        Task<bool> AttachmentCU(List<Attachment> attachments);
        Task<bool> DeleteAttachment(long id);
        Task<Attachment> GetAttachment(long masterId = 0, string masterKey = "", long? attachmentId = null);
    }
}
