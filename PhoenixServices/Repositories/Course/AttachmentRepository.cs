﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class AttachmentRepository : SqlRepository<Attachments>, IAttachmentRepository
    {
        public AttachmentRepository(IConfiguration configuration) : base(configuration)
        {
        }

        #region Inbuild Code
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        
        public override Task<IEnumerable<Attachments>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<Attachments> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(Attachments entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(Attachments entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion

        public async Task<bool> AttachmentCU(List<Attachment> attachments)
        {
            var list = new Attachments { AttachmentList = attachments };
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@attachment", list.GetAttachmentDT(), DbType.Object);
                parameters.Add("@output", direction: ParameterDirection.Output, dbType: DbType.Boolean);
                await conn.QueryAsync("dbo.AttachmentCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("output");
            }
        }

        public async Task<bool> DeleteAttachment(long id)
        {
            var list = new Attachments { AttachmentList = new List<Attachment>() { new Attachment { AttachmentId = id, Mode = TranModes.Delete } } };
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@attachment", list.GetAttachmentDT(), DbType.Object);
                parameters.Add("@output", direction: ParameterDirection.Output, dbType: DbType.Boolean);
                await conn.QueryFirstOrDefaultAsync("dbo.AttachmentCUD", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<bool>("output");
            }
        }

        public async Task<Attachment> GetAttachment(long masterId = 0,string masterKey = "", long? attachmentId = null)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@id", masterId);
                parameters.Add("@key", masterKey);
                parameters.Add("@attachmentId", attachmentId);
                return await conn.QueryFirstOrDefaultAsync<Attachment>("dbo.getattachmentbymasterkeyid", parameters, null, null, CommandType.StoredProcedure);                
            }
        }
    }
}
