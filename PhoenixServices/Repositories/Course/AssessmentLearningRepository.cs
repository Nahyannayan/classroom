﻿using DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
namespace Phoenix.API.Repositories
{
    public class AssessmentLearningRepository : SqlRepository<AssessmentLearning>, IAssessmentLearningRepository
    {
        private readonly IConfiguration _config;
        private readonly IAttachmentRepository attachmentRepository;

        public AssessmentLearningRepository(IConfiguration configuration, IAttachmentRepository attachmentRepository) : base(configuration)
        {
            _config = configuration;
            this.attachmentRepository = attachmentRepository;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<AssessmentLearning>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<AssessmentLearning> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(AssessmentLearning entity)
        {
            throw new NotImplementedException();
        }

        public async Task<OperationDetails> SaveUpdateAssessmentLearningDetails(AssessmentLearning model, long UserId)
        {
            OperationDetails op = new OperationDetails();

            var parameters = new DynamicParameters();
            parameters.Add("@AssesmentLearningID", model.AssesmentLearningID, DbType.Int64);
            parameters.Add("@Description", model.Description, DbType.String);
            parameters.Add("@Title", model.Title, DbType.String);
            parameters.Add("@DeliveryMode", model.DeliveryMode, DbType.Int32);
            parameters.Add("@UnitId", model.UnitId, DbType.Int64);
            parameters.Add("@UserId", UserId, DbType.Int64);
            parameters.Add("@output", dbType: DbType.Int64, direction: ParameterDirection.Output);

            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("COURSE.AssessmentLearningCU", parameters, commandType: CommandType.StoredProcedure);

                var id = parameters.Get<long>("output");
                model.AttachmentList.ForEach(x => x.AttachedToId = id);
                var result = await attachmentRepository.AttachmentCU(model.AttachmentList);
                var operationDetails = new OperationDetails();
                operationDetails.Success = result;
                operationDetails.InsertedRowId = Convert.ToInt32(id);
                return operationDetails;

                
                
            }
        }

        public async Task<IEnumerable<AssessmentLearning>>  GetAssessmentLearningList(long UnitId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UnitId", UnitId, DbType.Int64);
                parameters.Add("@key", $"{AttachmentKey.AssessmentLearning.EnumToString()}");
                var result = await conn.QueryMultipleAsync("Course.GetAssessmentLearningListByUnitId", parameters, commandType: CommandType.StoredProcedure);

                var AssessmentLearning = await result.ReadAsync<AssessmentLearning>();
                if (AssessmentLearning != null)
                {
                    var attachments = await result.ReadAsync<Attachment>();
                    foreach (var item in AssessmentLearning)
                    {
                        var attachmentList = attachments.Where(x => x.AttachedToId == item.AssesmentLearningID).ToList();
                        item.AttachmentList = attachmentList.ToList();
                    }
                }
                return AssessmentLearning;

            }
        }

        public async Task<AssessmentLearning> GetAssessmentLearningDetailsById(long AssessmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@assessmentLearningId", AssessmentId, DbType.Int64);
                parameters.Add("@key", $"{AttachmentKey.AssessmentLearning.EnumToString()}");
                var result = await conn.QueryMultipleAsync("Course.GetAssessmentLearningDetailsById", parameters, commandType: CommandType.StoredProcedure);

                var AssessmentLearning = await result.ReadFirstOrDefaultAsync<AssessmentLearning>();
                if (AssessmentLearning != null)
                {
                    var attachments = await result.ReadAsync<Attachment>();
                    AssessmentLearning.AttachmentList = attachments.ToList();
                }
                return AssessmentLearning;
            }
        }

        public override void UpdateAsync(AssessmentLearning entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
