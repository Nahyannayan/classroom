﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class CourseCatalogueRepository : SqlRepository<CourseCatalogue>, ICourseCatalogueRepository
    {
        public CourseCatalogueRepository(IConfiguration configuration) : base(configuration)
        {
        }

        #region Generated Methods
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<CourseCatalogue>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<CourseCatalogue> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(CourseCatalogue entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(CourseCatalogue entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Public Methods
        public async Task<GroupCatalogue> GetCatalogueInfoUsingGroupId(long groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@groupid", groupId);
                var result = await conn.QueryMultipleAsync("Course.GetCatalogueInfoUsingGroupId", parameters, commandType: CommandType.StoredProcedure);
                var groupCatalogue = await result.ReadFirstOrDefaultAsync<GroupCatalogue>();
                if(groupCatalogue != null)
                {
                    groupCatalogue.SchoolGradeName = await result.ReadAsync<string>();
                    groupCatalogue.CatalogueTeachers = await result.ReadAsync<CatalogueTeachers>();
                    groupCatalogue.TimeTableDatas = await result.ReadAsync<TimeTableData>();
                    //groupCatalogue.TableWeeklies = await result.ReadAsync<TimeTableWeekly>();
                }
                return groupCatalogue;
            }
        }
        public async Task<IEnumerable<CourseCatalogue>> GetCourseCatalogueInformationByTeacher(long memberId, bool isStudent)
        {
            using (var conn = GetOpenConnection())
            {
                SqlMapper.GridReader result;
                var parameters = new DynamicParameters();
                if (isStudent)
                {
                    parameters.Add("@studentId", memberId);
                    result = await conn.QueryMultipleAsync("Course.GetCourseCatalogueInformationByStudent", parameters, commandType: CommandType.StoredProcedure);                    
                }
                else
                {
                    parameters.Add("@teacherId", memberId);
                    result = await conn.QueryMultipleAsync("Course.GetCourseCatalogueInformationByTeacher", parameters, commandType: CommandType.StoredProcedure);
                }                
                var courseCatalogues = await result.ReadAsync<CourseCatalogue>();
                var courseMappings = await result.ReadAsync<CourseMapping>();
                //var units = await result.ReadAsync<CatalogueUnits>();
                //var unitDetails = await result.ReadAsync<CatalogueUnitDetails>();
                //var unitAssingmentAndLesson = await result.ReadAsync<CalalogueAssingmentLession>();
                //var unitQuizzes = await result.ReadAsync<CalalogueQuiz>();
                var courseData = courseCatalogues.AsList();
                if (courseData != null)
                {
                    courseData.ForEach(x =>
                    {
                        x.CourseMappings = courseMappings.Where(y => y.CourseId == x.CourseId).ToList();
                        //x.Units = units.Where(y => y.CourseId == x.CourseId).ToList();
                        //x.Units.ForEach(unit =>
                        //{
                        //    unit.UnitDetails = unitDetails.Where(y => y.UnitId == unit.UnitId).ToList();
                        //    unit.AssingmentLessions = unitAssingmentAndLesson.Where(y => y.UnitId == unit.UnitId).ToList();
                        //    unit.Quizzes = unitQuizzes.Where(y => y.UnitId == unit.UnitId).ToList();
                        //});                        
                    });
                }
                return courseData;
            }
        }
        public async Task<IEnumerable<CatalogueUnits>> GetCatalogueUnits(long courseId,long memberId, bool isStudent)
        {
            using (var conn = GetOpenConnection())
            {
                SqlMapper.GridReader result;
                var parameters = new DynamicParameters();
                parameters.Add("@courseId", courseId);
                if (isStudent)
                {
                    parameters.Add("@studentId", memberId);
                    result = await conn.QueryMultipleAsync("Course.GetStudentUnitDetails", parameters, commandType: CommandType.StoredProcedure);
                }
                else
                {
                    parameters.Add("@teacherId", memberId);
                    result = await conn.QueryMultipleAsync("Course.GetTeacherUnitDetails", parameters, commandType: CommandType.StoredProcedure);
                }
                var units = await result.ReadAsync<CatalogueUnits>();
                var unitDetails = await result.ReadAsync<CatalogueUnitDetails>();
                var unitAssingmentAndLesson = await result.ReadAsync<CalalogueAssingmentLession>();
                var unitQuizzes = await result.ReadAsync<CalalogueQuiz>();
                var unit = units.AsList();
                if (unit.Any())
                {
                    unit.ForEach(x =>
                    {
                        x.UnitDetails = unitDetails.Where(y => y.UnitId == x.UnitId).ToList();
                        x.AssingmentLessions = unitAssingmentAndLesson.Where(y => y.UnitId == x.UnitId).ToList();
                        x.Quizzes = unitQuizzes.Where(y => y.UnitId == x.UnitId).ToList();
                    });
                }
                return unit;
            }
        }
        #endregion

        #region Private Methods

        #endregion
    }
}
