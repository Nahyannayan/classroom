﻿using DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Microsoft.CodeAnalysis.Differencing;

namespace Phoenix.API.Repositories
{
    public class StandardRepository : SqlRepository<Standard>, IStandardRepository
    {
        private readonly IConfiguration _config;
        public StandardRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<Standard>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<Standard> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<Standard> GetStandardMasterDetailsById(Int64? StandardID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StandardID", StandardID, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<Standard>("Course.GetStandardMasterDetailsById", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(Standard entity)
        {
            throw new NotImplementedException();
        }

        public OperationDetails SaveUpdateStandardMasterDetails(Standard model)
        {
            OperationDetails op = new OperationDetails();

            var parameters = new DynamicParameters();
            parameters.Add("@SyllabusId", model.StandardID, DbType.Int64);
            //parameters.Add("@GroupName", model.GroupName, DbType.String);
            parameters.Add("@Description", model.Description, DbType.String);
            //parameters.Add("@ParentID", model.ParentID, DbType.Int64);
            parameters.Add("@TermID", model.Termid, DbType.Int64);
            parameters.Add("@CourseId", model.Courseid, DbType.Int64);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("COURSE.STANDARDMASTERCU", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = true;
                }
                else
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = false;
                }
                return op;
            }
        }
       
        public async Task<IEnumerable<ParentDetails>> GetParentList(Int64? SId)
        {
            List<Standard> lstStandard = new List<Standard>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StandardID", SId, DbType.Int32);
                return await conn.QueryAsync<ParentDetails>("[Course].[GetStandardParentList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Standard>> GetStandardMasterList(int Acd_Id)
        {
            List<Standard> lstStandard = new List<Standard>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Acd_Id", Acd_Id, DbType.Int32);
                return await conn.QueryAsync<Standard>("Course.GetStandardMasterList", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<MainSyllabusIdList>> MainSyllabusIdList()
        {
            List<Standard> lstStandard = new List<Standard>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                // parameters.Add("@IsParent", Isparent, DbType.Boolean);
                return await conn.QueryAsync<MainSyllabusIdList>("[Course].[MainSyllabusIdList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public OperationDetails DeleteStandardMaster(Standard standard)
        {
            OperationDetails op = new OperationDetails();

            var parameters = new DynamicParameters();
            //parameters.Add("@StandardID", model.StandardID, DbType.Int64);
            //parameters.Add("@GroupName", model.GroupName, DbType.String);
            //parameters.Add("@Description", model.Description, DbType.String);
            //parameters.Add("@ParentID", model.ParentID, DbType.Int64);
            //parameters.Add("@AgeGroupID", model.AgeGroupID, DbType.Int64);
            //parameters.Add("@StepsID", model.StepsID, DbType.Int64);
            //parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("COURSE.DeleteStandardMaster", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = true;
                }
                else
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = false;
                }
                return op;
            }
        }

        public override void UpdateAsync(Standard entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Standard>> GetStandardGroupName(long UnitId,long SchoolId)
        {
            List<Standard> lstStandard = new List<Standard>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UnitMasterId", UnitId, DbType.Int64);
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                return await conn.QueryAsync<Standard>("Course.GetStandardGroupName", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SchoolTermList>> GetSchoolTerm(int acdid)
        {
            List<SchoolTermList> lstStandard = new List<SchoolTermList>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@acdid", acdid, DbType.Int32);
                return await conn.QueryAsync<SchoolTermList>("Course.GetSchoolTerm", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<int> BulkStandardUpload(StandardUploadModel standardUploadModel)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", standardUploadModel.SchoolId, DbType.Int64);
                parameters.Add("@CourseId", standardUploadModel.CourseId, DbType.Int64);
                parameters.Add("@UserId", standardUploadModel.UserId, DbType.Int64);
                parameters.Add("@StandardUploadDT", standardUploadModel.StandardUploadDT, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("Course.BulkStandardUpload", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        public async Task<int> BulkStandardBankUpload(StandardBankUploadModel standardUploadModel)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", standardUploadModel.SchoolId, DbType.Int64);
               // parameters.Add("@UserId", standardUploadModel.UserId, DbType.Int64);
                parameters.Add("@StandardBankUploadDT", standardUploadModel.StandardBankUploadDT, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("Course.BulkStandardBankUpload", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<IEnumerable<StandardBankExcelModel>> GetStandardBankList(long SchoolId,long SB_Id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                parameters.Add("@SB_Id", SB_Id, DbType.Int64);
                return await conn.QueryAsync<StandardBankExcelModel>("Course.GetStandardBankList", parameters, null, null, CommandType.StoredProcedure);
            }
        }


        public async Task<bool> AddEditStandardBank(string TransMode, string EditType,StandardBankExcelModel standardBankModel)
        {
            using (var conn = GetOpenConnection())
            {
        
                var parameters = new DynamicParameters();
                parameters.Add("@SB_Id", standardBankModel.SB_Id, DbType.Int64);
                parameters.Add("@TransMode", TransMode, DbType.String);
                parameters.Add("@EditType", EditType, DbType.String);
                
                    if (EditType == "UnitEdit" || EditType =="UnitDelete")
                    {
                        parameters.Add("@StandardDescription", standardBankModel.UnitName, DbType.String);
                    }
                    else if (EditType == "SubUnitEdit" || EditType =="SubUnitDelete")
                    {
                        parameters.Add("@StandardDescription", standardBankModel.SubUnitName, DbType.String);
                    }
                    else parameters.Add("@StandardDescription", standardBankModel.StandardDescription, DbType.String);
                

                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("Course.StandardBankDU", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        
        }
    }
}
