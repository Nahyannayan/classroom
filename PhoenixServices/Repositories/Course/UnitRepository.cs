﻿using DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models.Entities;
using Phoenix.Common.Helpers.Extensions;
namespace Phoenix.API.Repositories
{
    public class UnitRepository : SqlRepository<Unit>, IUnitRepository
    {
        private readonly IConfiguration _config;
        private readonly IAttachmentRepository attachmentRepository;

        public UnitRepository(IConfiguration configuration, IAttachmentRepository attachmentRepository) : base(configuration)
        {
            _config = configuration;
            this.attachmentRepository = attachmentRepository;
        }

        public async Task<bool> AddEditUnitDetailsType(List<UnitDetailsType> objlstUnitType, long schoolId,
            string standardIds, string assessmentEvidenceIds, string assessmentLearningIds, string lessonIdsToDelete, long CreatedBy)
        {
            DataTable dt = new DataTable();
            var attachments = objlstUnitType.SelectMany(x => x.AttachmentList).ToList();
            dt.Columns.Add("UnitTypeId", typeof(long));
            dt.Columns.Add("UnitMasterId", typeof(long));

            dt.Columns.Add("UnitTypeDescription", typeof(string));
            dt.Columns.Add("UnitDetailsId", typeof(long));
            dt.Columns.Add("AttachmentKey", typeof(string));

            foreach (var item in objlstUnitType)
            {
                dt.Rows.Add(item.UnitTypeId, item.UnitId, item.ContentDescription, item.UnitDetailsId, item.AttachmentKey);
            }
            var parameters = new DynamicParameters();
            parameters.Add("@tblUnitDetails", dt, DbType.Object);
            parameters.Add("@standardIds", standardIds, DbType.String);
            parameters.Add("@assessmentEvidenceIds", assessmentEvidenceIds, DbType.String);
            parameters.Add("@assessmentEvidencekey", $"{AttachmentKey.AssessmentEvidence.EnumToString()}");
            parameters.Add("@assessmentLearningkey", $"{AttachmentKey.AssessmentLearning.EnumToString()}");
            parameters.Add("@assessmentLearningIds", assessmentLearningIds, DbType.String);
            parameters.Add("@lessonIdsToDelete", lessonIdsToDelete, DbType.String);
            parameters.Add("@schoolId", schoolId, DbType.Int64);
            parameters.Add("@CreatedBy", CreatedBy);
            using (var conn = GetOpenConnection())
            {
                var result = await conn.QueryAsync<UnitDetailAttachment>("Course.UnitDetailsCUD", parameters, commandType: CommandType.StoredProcedure);

                foreach (var attachment in attachments)
                {
                    var res = result.Where(x => x.AttachmentKey == attachment.AttachmentKey).FirstOrDefault();
                    if (res != null)
                    {
                        attachment.AttachedToId = res.UTD_ID;
                    }

                }

                return await attachmentRepository.AttachmentCU(attachments);

            }
        }

        public bool AddEditUnitMaster(Unit objUnitMaster, TransactionModes mode, string createdBy)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@UnitId", objUnitMaster.UnitId, DbType.Int64);
            parameters.Add("@ParentId", objUnitMaster.ParentId, DbType.Int64);
            parameters.Add("@CourseId", objUnitMaster.CourseId, DbType.Int64);
            parameters.Add("@UnitName ", objUnitMaster.UnitName, DbType.String);
            parameters.Add("@StartDate", objUnitMaster.StartDate, DbType.DateTime);
            parameters.Add("@EndDate", objUnitMaster.EndDate, DbType.DateTime);
            parameters.Add("@UnitColorCode", objUnitMaster.UnitColorCode, DbType.String);
            parameters.Add("@CreatedBy", createdBy, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("Course.UnitMasterCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<Unit>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<Unit> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<UnitDetailsType>> GetUnitDetailsType(long unitId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@unitId", unitId, DbType.Int64);

                //var result = await conn.QueryMultipleAsync<UnitDetailsType>("Course.GetUnitDetailsType", parameters, commandType: CommandType.StoredProcedure);
                //if (result != null)
                //{
                //    var attachments = await result.ReadAsync<Attachment>();
                //    courses.AttachmentList = attachments.ToList();
                //}

                var result = await conn.QueryMultipleAsync("Course.GetUnitDetailsType", parameters, commandType: CommandType.StoredProcedure);

                var unitDetailsType = await result.ReadAsync<UnitDetailsType>();
                if (unitDetailsType != null)
                {
                    var attachments = await result.ReadAsync<Attachment>();
                    foreach (var item in unitDetailsType)
                    {
                        var attachmentList = attachments.Where(x => x.AttachedToId == item.UnitDetailsId).ToList();
                        item.AttachmentList = attachmentList;
                    }
                }
                return unitDetailsType;


            }
        }

        public async Task<IEnumerable<Unit>> GetUnitMasterDetails()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<Unit>("Course.GetUnitMasterDetails", commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Unit>> GetUnitMasterDetailsByCourseId(long courseId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CourseId", courseId, DbType.Int64);

                return await conn.QueryAsync<Unit>("Course.GetUnitMasterDetailsByCourseId", parameters, commandType: CommandType.StoredProcedure);

            }
        }
        public async Task<UnitCalendar> GetUnitCalendarByCourseId(long SchoolId, long courseId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CourseId", courseId, DbType.Int64);
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);

                var result = await conn.QueryMultipleAsync("Course.GetUnitCalendarByCourseId", parameters, commandType: CommandType.StoredProcedure);
                var unitCalendar = await result.ReadFirstOrDefaultAsync<UnitCalendar>();
                if (courseId != 0 && unitCalendar != null)
                {
                    var weekList = await result.ReadAsync<UnitWeek>();
                    unitCalendar.WeekList = weekList;
                    var unitDetails = await result.ReadAsync<Unit>();
                    unitCalendar.UnitDetails = unitDetails;
                }
                return unitCalendar;
            }
        }

        public async Task<Unit> GetUnitMasterDetailsByUnitId(long Id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", Id, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<Unit>("Course.GetUnitMasterDetailsByUnitId", parameters, commandType: CommandType.StoredProcedure);

            }
        }

        public override void InsertAsync(Unit entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(Unit entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<UnitTopic>> GetUnitTopicList(long MainSyllabusId)
        {

            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@MainSyllabusId", MainSyllabusId, DbType.String);
                return await conn.QueryAsync<UnitTopic>("Course.GetUnitTopicList", parameters, commandType: CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<UnitTopicStandardDetails>> GetUnitTopicStandardDetails(
            long UnitMasterId, long GroupId, long UnitId, long StandardBankId, int PageNumber, string SearchString, long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StandardBankId", StandardBankId, DbType.Int64);
                parameters.Add("@GroupId", GroupId, DbType.Int64);
                parameters.Add("@UnitId", UnitId, DbType.Int64);
                parameters.Add("@UnitMasterId", UnitMasterId, DbType.Int64);
                parameters.Add("@PageNumber", PageNumber, DbType.Int32);
                parameters.Add("@SearchString", SearchString, DbType.String);
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                return await conn.QueryAsync<UnitTopicStandardDetails>("COURSE.GETUNITTOPICSTANDARDDETAILS", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<UnitWeek>> GetUnitWeekList(long SchoolId, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.String);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<UnitWeek>("COURSE.GetWeekStartListBySchool", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<TopicTreeView>> GetStandardDetailsById(long unitId)
        {

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UnitMasterId", unitId, DbType.Int64);
                return await conn.QueryAsync<TopicTreeView>("COURSE.GetUnitStandardDetailsById", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public bool DeleteUnitStandardDetailsById(long scmId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SCM_ID", scmId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Course.DeleteUnitStandardDetailsById", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;

            }
        }
        public async Task<IEnumerable<Unit>> GetCourseUnitMasterByCourseId(long schoolId, long courseId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CourseId", courseId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<Unit>("Course.GetCourseUnitMasterByCourseId", parameters, commandType: CommandType.StoredProcedure);

            }
        }
        public async Task<IEnumerable<UnitDetailsType>> GetUnitDetailsByCourseId(long courseId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CourseId", courseId, DbType.Int64);
                //   return await conn.QueryAsync<UnitDetailsType>("Course.GetUnitDetailsByCourseId", parameters, commandType: CommandType.StoredProcedure);


                var result = await conn.QueryMultipleAsync("Course.GetUnitDetailsByCourseId", parameters, commandType: CommandType.StoredProcedure);

                var unitDetailsType = await result.ReadAsync<UnitDetailsType>();
                if (unitDetailsType != null)
                {
                    var attachments = await result.ReadAsync<Attachment>();
                    foreach (var item in unitDetailsType)
                    {
                        var attachmentList = attachments.Where(x => x.AttachedToId == item.UnitDetailsId).ToList();
                        item.AttachmentList = attachmentList;
                    }
                }
                return unitDetailsType;

            }
        }

        public async Task<IEnumerable<Unit>> GetUnitMasterDetailsByGroupId(long groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupId", groupId, DbType.Int64);

                return await conn.QueryAsync<Unit>("Course.GetUnitMasterByGroupId", parameters, commandType: CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<Unit>> GetCourseDetailsAndUnitList(long courseId, long teacherId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CourseId", courseId, DbType.Int64);
                parameters.Add("@teacherId", teacherId, DbType.Int64);
                return await conn.QueryAsync<Unit>("Course.GetCourseDetailsAndUnitList", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Unit>> GetUnitsByGroup(long Id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", Id, DbType.Int64);
                return await conn.QueryAsync<Unit>("Course.GetUnitsByGroup", parameters, commandType: CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<Unit>> GetSubUnitsById(long Id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", Id, DbType.Int64);
                return await conn.QueryAsync<Unit>("Course.GetSubUnitsById", parameters, commandType: CommandType.StoredProcedure);

            }
        }
        public async Task<IEnumerable<Attachment>> GetAttachmentDetailByUnitId(long UnitId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UnitId", UnitId, DbType.Int64);
                return await conn.QueryAsync<Attachment>("Course.GetAttachmentDetailByUnitId", parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}

