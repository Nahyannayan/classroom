﻿using DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
namespace Phoenix.API.Repositories
{
    public class AssessmentEvidenceRepository : SqlRepository<AssessmentEvidence>, IAssessmentEvidenceRepository
    {
        private readonly IConfiguration _config;
        private readonly IAttachmentRepository attachmentRepository;

        public AssessmentEvidenceRepository(IConfiguration configuration, IAttachmentRepository attachmentRepository) : base(configuration)
        {
            _config = configuration;
            this.attachmentRepository = attachmentRepository;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<AssessmentEvidence>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<AssessmentEvidence> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(AssessmentEvidence entity)
        {
            throw new NotImplementedException();
        }

        public async Task<OperationDetails> SaveUpdateAssessmentEvidenceDetails(AssessmentEvidence model, long UserId)
        {
            OperationDetails op = new OperationDetails();

            var parameters = new DynamicParameters();
            parameters.Add("@AssesmentEvidenceID", model.AssesmentEvidenceID, DbType.Int64);
            parameters.Add("@Description", model.Description, DbType.String);
            parameters.Add("@Title", model.Title, DbType.String);
            parameters.Add("@AssessmentTypeId", model.AssessmentTypeId, DbType.Int32);
            parameters.Add("@UnitId", model.UnitId, DbType.Int64);
            parameters.Add("@UserId", UserId, DbType.Int64);
            parameters.Add("@output", dbType: DbType.Int64, direction: ParameterDirection.Output);

            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("COURSE.AssessmentEvidenceCU", parameters, commandType: CommandType.StoredProcedure);

                var id = parameters.Get<long>("output");
                model.AttachmentList.ForEach(x => x.AttachedToId = id);
                var result = await attachmentRepository.AttachmentCU(model.AttachmentList);
                var operationDetails = new OperationDetails();
                operationDetails.Success = result;
                operationDetails.InsertedRowId = Convert.ToInt32(id);
                return operationDetails;

                
                
            }
        }

        public async Task<IEnumerable<AssessmentEvidence>>  GetAssessmentEvidenceList(long UnitId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UnitId", UnitId, DbType.Int64);
                parameters.Add("@key", $"{AttachmentKey.AssessmentEvidence.EnumToString()}");
                var result = await conn.QueryMultipleAsync("Course.GetAssessmentEvidenceListByUnitId", parameters, commandType: CommandType.StoredProcedure);

                var AssessmentEvidence = await result.ReadAsync<AssessmentEvidence>();
                if (AssessmentEvidence != null)
                {
                    var attachments = await result.ReadAsync<Attachment>();
                    foreach (var item in AssessmentEvidence)
                    {
                        var attachmentList = attachments.Where(x => x.AttachedToId == item.AssesmentEvidenceID).ToList();
                        item.AttachmentList = attachmentList.ToList();
                    }
                }
                return AssessmentEvidence;

            }
        }

        public async Task<AssessmentEvidence> GetAssessmentEvidenceDetailsById(long AssessmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssessmentId", AssessmentId, DbType.Int64);
                parameters.Add("@key", $"{AttachmentKey.AssessmentEvidence.EnumToString()}");
                var result = await conn.QueryMultipleAsync("Course.GetAssessmentEvidenceDetailsById", parameters, commandType: CommandType.StoredProcedure);

                var AssessmentEvidence = await result.ReadFirstOrDefaultAsync<AssessmentEvidence>();
                if (AssessmentEvidence != null)
                {
                    var attachments = await result.ReadAsync<Attachment>();
                    AssessmentEvidence.AttachmentList = attachments.ToList();
                }
                return AssessmentEvidence;
            }
        }

        public override void UpdateAsync(AssessmentEvidence entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
