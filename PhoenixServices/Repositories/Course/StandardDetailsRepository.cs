﻿using DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;

namespace Phoenix.API.Repositories
{
    public class StandardDetailsRepository :SqlRepository<StandardDetails>, IStandardDetailsRepository
    {
        private readonly IConfiguration _config;
        public StandardDetailsRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<StandardDetails>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<StandardDetails> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<StandardDetails> GetStandardDetailsById(Int64? StandardDetailsID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StandardDetailsID", StandardDetailsID, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<StandardDetails>("COURSE.GETSTANDARDDETAILSBYID", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(StandardDetails entity)
        {
            throw new NotImplementedException();
        }

        public OperationDetails SaveUpdateStandardDetails(StandardDetails model)
        {
            OperationDetails op = new OperationDetails();

            var parameters = new DynamicParameters();
            parameters.Add("@SubSyllabusId", model.StandardDetailID, DbType.Int64);
            parameters.Add("@MainSyllabusId", model.StandardID, DbType.Int64);
            parameters.Add("@Description", model.Description, DbType.String);
            parameters.Add("@ParentId", model.ParentID, DbType.Int32);
            parameters.Add("@GroupId", 0, DbType.Int32);
            parameters.Add("@StepId", 0, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("COURSE.STANDARDDETAILSCU", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = true;
                }
                else
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = false;
                }
                return op;
            }
        }

        public async Task<IEnumerable<StandardDetails>> GetStandardDetailsList()
        {
            List<StandardDetails> lstStandard = new List<StandardDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                return await conn.QueryAsync<StandardDetails>("COURSE.GETSTANDARDDETAILSLIST", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public OperationDetails DeleteStandardDetails(StandardDetails standardDetails)
        {
            OperationDetails op = new OperationDetails();

            var parameters = new DynamicParameters();
            //parameters.Add("@StandardID", model.StandardID, DbType.Int64);
            //parameters.Add("@GroupName", model.GroupName, DbType.String);
            //parameters.Add("@Description", model.Description, DbType.String);
            //parameters.Add("@ParentID", model.ParentID, DbType.Int64);
            //parameters.Add("@AgeGroupID", model.AgeGroupID, DbType.Int64);
            //parameters.Add("@StepsID", model.StepsID, DbType.Int64);
            //parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("COURSE.DeleteStandardDetails", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = true;
                }
                else
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = false;
                }
                return op;
            }
        }


        public override void UpdateAsync(StandardDetails entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
