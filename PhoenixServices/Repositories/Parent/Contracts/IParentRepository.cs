﻿using DbConnection;
using Phoenix.API.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IParentRepository : IGenericRepository<ParentView>
    {
        Task<ParentView> GetStudentDetailsById(long id);
        Task<int> AssignAccessPermission(string StudentIdList);
    }
}
