﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.DataAccess;
using Phoenix.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class ParentRepository: SqlRepository<ParentView>, IParentRepository
    {
        private readonly IConfiguration _config;
        private DataHelper _dataHelper = null;
        public ParentRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        /// <summary>
        /// Author: Girish Sonawane
        /// CreatedAt: 07-July-2019
        /// Get student details fro bind it on dashboard. 
        /// </summary>
        public async Task<ParentView> GetStudentDetailsById(long id)
        {
            var parentView = new ParentView();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", id, DbType.Int32);
                var result = await conn.QueryMultipleAsync("School.GetParentDashoardData", parameters, null, null, CommandType.StoredProcedure);
                var studentDetails = await result.ReadAsync<StudentDetail>();
                var badges = await result.ReadAsync<SchoolBadge>();
                
                parentView.studentDetail = studentDetails.FirstOrDefault();
                parentView.Badges = badges.ToList();

                return parentView;
            }
        }
        public async Task<int> AssignAccessPermission(string StudentIdList) {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentList", StudentIdList+",", DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                var result = await conn.QueryAsync("school.UpdateStudentAccessibalePermission", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }

        }
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<ParentView>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<ParentView> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

  

        public override void InsertAsync(ParentView entity)
        {
            throw new NotImplementedException();
        }

     

        public override void UpdateAsync(ParentView entityToUpdate)
        {
            throw new NotImplementedException();
        }

       
    }
}
