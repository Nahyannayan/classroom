﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public class PlanSchemeDetailRepository : SqlRepository<PlanSchemeDetail>, IPlanSchemeDetailRepository
    {
        private readonly IConfiguration _config;
        public PlanSchemeDetailRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public void Delete(int id, int deletedBy)
        {
            using (var conn = GetOpenConnection())
            {
                var entity = new PlanSchemeDetail(id);
                entity.UpdatedBy = deletedBy;
                var parameters = GetPlanSchemeDetailParameters(entity, TransactionModes.Delete);
                conn.Query<Int32>("DE.PlanSchemeDetailCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public override void DeleteAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetPlanSchemeDetailParameters(new PlanSchemeDetail(id), TransactionModes.Delete);
                conn.Query<Int32>("DE.PlanSchemeDetailCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public async override Task<IEnumerable<PlanSchemeDetail>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PlanSchemeDetailId", DBNull.Value, DbType.Int64);
                parameters.Add("@TemplateId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int64);
                parameters.Add("@TemplateType", DBNull.Value, DbType.String);
                parameters.Add("@Status", DBNull.Value, DbType.String);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                parameters.Add("@IsActive", DBNull.Value, DbType.Boolean);
                return await conn.QueryAsync<PlanSchemeDetail>("DE.GetPlanSchemeDetail", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async override Task<PlanSchemeDetail> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PlanSchemeDetailId", id, DbType.Int64);
                parameters.Add("@TemplateId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int64);
                parameters.Add("@TemplateType", DBNull.Value, DbType.String);
                parameters.Add("@Status", DBNull.Value, DbType.String);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                parameters.Add("@IsActive", DBNull.Value, DbType.Boolean);
                var result = await conn.QueryMultipleAsync("DE.GetPlanSchemeDetail", parameters, null, null, CommandType.StoredProcedure);
                var PlanSchemeDetail = await result.ReadAsync<PlanSchemeDetail>();
                var PlanSchemeDetailCount = await result.ReadAsync<int>();
                var list = PlanSchemeDetail.ToList();
                int totalCount = PlanSchemeDetailCount.FirstOrDefault();
                if (list.Any())
                {
                    list.ForEach(x => { x.TotalCount = totalCount; });
                }
                return PlanSchemeDetail.FirstOrDefault();
            }
        }

        public async Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemeBySchoolId(int schoolId, int? userId, int? status, bool? isActive,
            bool? SharedWithMe, bool? CreatedByMe,
            string MISGroupId, string OtherGroupId, string CourseId, string GradeId,
            int? pageIndex = null, int? PageSize = null, string searchString = "", string sortBy = "")
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PlanSchemeDetailId", DBNull.Value, DbType.Int64);
                parameters.Add("@TemplateId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@TemplateType", DBNull.Value, DbType.String);
                parameters.Add("@Status", status, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                //Filter Parameters
                parameters.Add("@SharedWithMe", SharedWithMe, DbType.Boolean);
                parameters.Add("@CreatedByMe", CreatedByMe, DbType.Boolean);
                parameters.Add("@MISGroupId", MISGroupId, DbType.String);
                parameters.Add("@OtherGroupId", OtherGroupId, DbType.String);
                parameters.Add("@CourseId", CourseId, DbType.String);
                parameters.Add("@GradeId", GradeId, DbType.String);
                //Search & Pagination Parameter
                parameters.Add("@PageNum", pageIndex, DbType.Int32);
                parameters.Add("@PageSize", PageSize, DbType.Int32);
                parameters.Add("@SearchString", searchString, DbType.String);
                parameters.Add("@SortBy", sortBy, DbType.String);
                var result = await conn.QueryMultipleAsync("DE.GetPlanSchemeDetail", parameters, null, null, CommandType.StoredProcedure);
                var PlanSchemeDetail = await result.ReadAsync<PlanSchemeDetail>();
                var PlanSchemeDetailCount = await result.ReadAsync<int>();
                var SharedPlanTeacherList = await result.ReadAsync<SharedLessonPlanTeacherList>();
                var list = PlanSchemeDetail.ToList();
                int totalCount = PlanSchemeDetailCount.FirstOrDefault();
                if (list.Any())
                {
                    list.ForEach(x =>
                    {
                        x.TotalCount = totalCount;
                        x.SharedLessonPlanTeacherList = SharedPlanTeacherList.Where(p => p.PlanSchemeDetailId == x.PlanSchemeDetailId).ToList(); });
                }
                return PlanSchemeDetail;
            }
        }

        public async Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemeDetail(int? planSchemeDetailId, int? templateId, int? schoolId, string templateType,
            int? status, int? userId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PlanSchemeDetailId", planSchemeDetailId, DbType.Int64);
                parameters.Add("@TemplateId", templateId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@TemplateType", templateType, DbType.String);
                parameters.Add("@Status", status, DbType.String);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                var result = await conn.QueryMultipleAsync("DE.GetPlanSchemeDetail", parameters, null, null, CommandType.StoredProcedure);

               var PlanSchemeDetail = await result.ReadAsync<PlanSchemeDetail>();
                var PlanSchemeDetailCount = await result.ReadAsync<int>();
                var SharedPlanTeacherList = await result.ReadAsync<SharedLessonPlanTeacherList>();
                var GroupId = await result.ReadAsync<long>();
                var list = PlanSchemeDetail.ToList();
                int totalCount = PlanSchemeDetailCount.FirstOrDefault();
                if (list.Any())
                {
                    list.ForEach(x => { x.TotalCount = totalCount; });
                }
                PlanSchemeDetail.FirstOrDefault().GroupList = "";
                PlanSchemeDetail.FirstOrDefault().GroupList = Convert.ToString(GroupId.FirstOrDefault());
                return PlanSchemeDetail;

            }
        }

        public int Insert(PlanSchemeDetail entity)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = GetPlanSchemeDetailParameters(entity, entity.PlanSchemeDetailId > 0 ? TransactionModes.Update : TransactionModes.Insert);
                conn.Query<Int32>("DE.PlanSchemeDetailCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public async Task<IEnumerable<PlanSchemeField>> GetPlanSchemeFields(long planSchemeId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PlanSchemeId", planSchemeId, DbType.Int64);
                return await conn.QueryAsync<PlanSchemeField>("DE.GetPlanSchemeFields", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public int SavePlanSchemeFields(List<PlanSchemeField> planSchemeField)
        {
            using (var conn = GetOpenConnection())
            {
                long PlanSchemeId = 0;
                var parameters = new DynamicParameters();
                if (planSchemeField.Count > 0)
                {
                    PlanSchemeId = planSchemeField.FirstOrDefault().PlanSchemeId;
                    DataTable dtTemplateField = new DataTable();
                    dtTemplateField.Columns.Add("PlanSchemeDetailField", typeof(Int64));
                    dtTemplateField.Columns.Add("PlanSchemeId", typeof(string));
                    dtTemplateField.Columns.Add("FieldId", typeof(string));
                    dtTemplateField.Columns.Add("FieldValue", typeof(string));

                    foreach (var item in planSchemeField)
                    {
                        dtTemplateField.Rows.Add(item.PlanSchemeDetailFieldId, item.PlanSchemeId, item.FieldId, item.FieldValue);
                    }

                    parameters.Add("@DTPlanSchemeDetailField", dtTemplateField, DbType.Object);
                    parameters.Add("@PlanSchemeId", PlanSchemeId, DbType.Int64);
                    parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                }
                conn.Query<Int32>("DE.SavePlanSchemeFields", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public override void InsertAsync(PlanSchemeDetail entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetPlanSchemeDetailParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("DE.PlanSchemeDetailCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public int Update(PlanSchemeDetail entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetPlanSchemeDetailParameters(entityToUpdate, TransactionModes.Update);
                conn.Query<Int32>("DE.PlanSchemeDetailCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public override void UpdateAsync(PlanSchemeDetail entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetPlanSchemeDetailParameters(entityToUpdate, TransactionModes.Update);
                conn.Query<Int32>("DE.PlanSchemeDetailCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public int UpdatePlanSchemeStatus(PlanSchemeDetail entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PlanSchemeDetailId", entityToUpdate.PlanSchemeDetailId, DbType.Int64);
                parameters.Add("@Status", entityToUpdate.Status, DbType.String);
                parameters.Add("@UserId", entityToUpdate.UpdatedBy, DbType.Int64);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<Int32>("DE.UpdatePlanSchemeDetailStatus", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public async Task<int> UpdateSharedLessonPlanDetial(long PlanSchemeId, string SelectedTeacherList, long UserId, string Operation)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PlanSchemeId", PlanSchemeId, DbType.Int64);
                parameters.Add("@SelectedTeacherList", SelectedTeacherList + ",", DbType.String);
                parameters.Add("@Operation", Operation, DbType.String);
                parameters.Add("@UserId", UserId, DbType.Int64);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("DE.UpdateSharedLessonPlanDetial", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");

            }
        }

        public async Task<IEnumerable<SharedLessonPlanTeacherList>> GetSharedLessonPlanTeacherList(string PlanSchemeId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PlanSchemeId", PlanSchemeId, DbType.String);
                return await conn.QueryAsync<SharedLessonPlanTeacherList>("DE.GetSharedLessonPlanTeacherList", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<LessonPlanFilterModule>> GetLessonPlanFilterModule(long SchoolId, long TeacherId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                parameters.Add("@Teacher", TeacherId, DbType.Int64);
                return await conn.QueryAsync<LessonPlanFilterModule>("[DE].GetLessonPlanFilterModule ", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<TopicSubTopicStructure>> GetUnitStructure(string groupIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupIds", groupIds, DbType.String);
                return await conn.QueryAsync<TopicSubTopicStructure>("[DE].[GetTopicSubTopicTreeWithMultipleGroups]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolGroup>> GetUserGroups(long Userid)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", Userid, DbType.Int64);
                return await conn.QueryAsync<SchoolGroup>("DE.GetUserGroups", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<PlanSchemeDetail>> GetPendingForApprovalLessonPlan(long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.String);
                return await conn.QueryAsync<PlanSchemeDetail>("[DE].GetPendingForApprovalLessonPlan", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        #region Private Method
        private DynamicParameters GetPlanSchemeDetailParameters(PlanSchemeDetail entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@PlanSchemeDetailId", entity.PlanSchemeDetailId, DbType.Int64);
            parameters.Add("@PlanSchemeName", entity.PlanSchemeName, DbType.String);
            parameters.Add("@TemplateId", entity.TemplateId, DbType.Int64);
            parameters.Add("@FileName", entity.FileName, DbType.String);
            parameters.Add("@FilePath", entity.FilePath, DbType.String);
            parameters.Add("@Status", entity.Status, DbType.Int32);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@SharedTeacherId", entity.SelectedTeacherIds, DbType.String);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);

            //if (!string.IsNullOrEmpty(entity.GroupList) && entity.GroupList.Contains(",")) { entity.GroupList = entity.GroupList.Substring(0, (entity.GroupList.Length - 1)); }
            parameters.Add("@ParentGroupId", entity.GroupList, DbType.String);
            parameters.Add("@UnitId", entity.UnitList, DbType.String);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@IsRejected", entity.IsRejected, DbType.Boolean);
            parameters.Add("@SharePointUploadFilePath", entity.SharePointUploadFilePath, DbType.String);
            parameters.Add("@ShareableLink", entity.ShareableLink, DbType.String);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);

            return parameters;
        }

        public async Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemesByUnitId(long unitId, int? status)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UnitId", unitId, DbType.Int64);
                parameters.Add("@Status", status, DbType.Int32);
                return await conn.QueryAsync<PlanSchemeDetail>("DE.GetPlanSchemesByUnitId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        #endregion
    }
}
