﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public interface IPlanSchemeDetailRepository : IGenericRepository<PlanSchemeDetail>
    {
        int Insert(PlanSchemeDetail entity);
        int Update(PlanSchemeDetail entityToUpdate);
        void Delete(int id, int deletedBy);
        int UpdatePlanSchemeStatus(PlanSchemeDetail entityToUpdate);

        Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemeBySchoolId(int schoolId, int? userId, int? status, bool? isActive,
            bool? SharedWithMe, bool? CreatedByMe,
            string MISGroupId, string OtherGroupId, string CourseId, string GradeId,
            int? pageIndex = null, int? PageSize = null, string searchString = "", string sortBy = "");
        Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemeDetail(int? planSchemeDetail, int? templateId, int? schoolId, string templateType, int? status ,  int? userId, bool? isActive);
        Task<int> UpdateSharedLessonPlanDetial(long PlanSchemeId, string SelectedTeacherList, long UserId, string Operation);
        Task<IEnumerable<SharedLessonPlanTeacherList>> GetSharedLessonPlanTeacherList(string PlanSchemeId);
        Task<IEnumerable<TopicSubTopicStructure>> GetUnitStructure(string groupIds);
        Task<IEnumerable<LessonPlanFilterModule>> GetLessonPlanFilterModule(long SchoolId, long TeacherId);
        Task<IEnumerable<PlanSchemeDetail>> GetPendingForApprovalLessonPlan(long SchoolId);
        int SavePlanSchemeFields(List<PlanSchemeField> planSchemeField);
        Task<IEnumerable<PlanSchemeField>> GetPlanSchemeFields(long planSchemeId);

        Task<IEnumerable<PlanSchemeDetail>> GetPlanSchemesByUnitId(long unitId, int? status);

        Task<IEnumerable<SchoolGroup>> GetUserGroups(long Userid);


    }
}
