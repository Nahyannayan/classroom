﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public class TemplateRepository : SqlRepository<Template>, ITemplateRepository
    {
        private readonly IConfiguration _config;
        public TemplateRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetTemplateParameters(new Template(id), TransactionModes.Delete);
                conn.Query<Int32>("DE.TemplateCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public void Delete(int id, int deletedBy)
        {
            using (var conn = GetOpenConnection())
            {
                var entity = new Template(id);
                entity.UpdatedBy = deletedBy;
                var parameters = GetTemplateParameters(entity, TransactionModes.Delete);
                conn.Query<Int32>("DE.TemplateCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public async override Task<IEnumerable<Template>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int64);
                parameters.Add("@TemplateType", DBNull.Value, DbType.String);
                parameters.Add("@Period", DBNull.Value, DbType.String);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                parameters.Add("@IsActive", DBNull.Value, DbType.Boolean);
                return await conn.QueryAsync<Template>("DE.GetTemplate", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<Template> GetTemplateById(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", id, DbType.Int64);
                var result = await conn.QueryMultipleAsync("DE.GetTemplate", parameters, null, null, CommandType.StoredProcedure);
                var templateList = await result.ReadAsync<Template>();
                return templateList.FirstOrDefault();
            }
        }

        public async override Task<Template> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var model = new Template();
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", id, DbType.Int64);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int64);
                parameters.Add("@TemplateType", DBNull.Value, DbType.String);
                parameters.Add("@Period", DBNull.Value, DbType.String);
                parameters.Add("@Status", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                parameters.Add("@IsActive", DBNull.Value, DbType.Boolean);
                parameters.Add("@IncludeTemplateField", DBNull.Value, DbType.Boolean);
                return await conn.QueryFirstOrDefaultAsync<Template>("DE.GetTemplate", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Template>> GetTemplatesBySchoolId(int schoolId, int? userId, bool isActive, string TemplateType, int? status)
        {
            using (var conn = GetOpenConnection())
            {
                var model = new Template();
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@TemplateType", TemplateType, DbType.String);
                parameters.Add("@Period", DBNull.Value, DbType.String);
                parameters.Add("@Status", status.HasValue ? status.Value : (object)DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", userId.HasValue ? userId.Value : (object)DBNull.Value, DbType.Int64);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                parameters.Add("@IncludeTemplateField", false, DbType.Boolean);
                var result = await conn.QueryMultipleAsync("DE.GetTemplate", parameters, null, null, CommandType.StoredProcedure);
                var templateList = await result.ReadAsync<Template>();
                return templateList;
            }
        }

        public async Task<Template> GetTemplateDetail(int? templateId, int? schoolId, string templateType, string period, int? userId, bool? isActive, bool? includeTemplateField)
        {
            using (var conn = GetOpenConnection())
            {
                var model = new Template();
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", templateId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@TemplateType", templateType, DbType.String);
                parameters.Add("@Period", period, DbType.String);
                parameters.Add("@Status", DBNull.Value, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                parameters.Add("@IncludeTemplateField", includeTemplateField, DbType.Boolean);
                var result = await conn.QueryMultipleAsync("DE.GetTemplate", parameters, null, null, CommandType.StoredProcedure);

                model = result.ReadFirstOrDefault<Template>();
                if (includeTemplateField != null && includeTemplateField == true)
                {
                    var templateFieldList = await result.ReadAsync<TemplateField>();
                    model.TemplateFields = templateFieldList.ToList();
                }
                return model;
            }
        }

        public async Task<TemplateField> GetTemplateFieldById(int templateFieldId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", DBNull.Value, DbType.Int64);
                parameters.Add("@TemplateFieldId", templateFieldId, DbType.Int64);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int64);
                parameters.Add("@IsActive", DBNull.Value, DbType.Boolean);
                return await conn.QueryFirstOrDefaultAsync<TemplateField>("DE.GetTemplateField", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<TemplateField>> GetTemplateFieldByTemplateId(int templateId, bool isActive,long UserId, int groupid)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", templateId, DbType.Int64);
                parameters.Add("@TemplateFieldId", DBNull.Value, DbType.Int64);
                parameters.Add("@UserId", UserId, DbType.Int64);
                parameters.Add("@GroupId", groupid, DbType.Int64);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<TemplateField>("DE.GetTemplateField", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(Template entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetTemplateParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("DE.TemplateCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public int Insert(Template entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetTemplateParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("DE.TemplateCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public override void UpdateAsync(Template entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetTemplateParameters(entityToUpdate, TransactionModes.Update);
                conn.Query<Int32>("DE.TemplateCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public int Update(Template entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetTemplateParameters(entityToUpdate, TransactionModes.Update);
                conn.Query<Int32>("DE.TemplateCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }


        public async Task<IEnumerable<TemplateCollectionList>> GetTimeTableList(long UserId, string SelectDate) {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", UserId, DbType.Int32);
                parameters.Add("@SelectDate", SelectDate, DbType.String);
                return await conn.QueryAsync<TemplateCollectionList>("DE.GetDailyTimeTableForLessonPlan", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<TemplateCollectionList>> GetGradeList(long Groupid, long SchoolId) {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Groupid", Groupid, DbType.Int64);
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                return await conn.QueryAsync<TemplateCollectionList>("DE.GetGradeByGroupId", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        #region Private Methods
        private DynamicParameters GetTemplateParameters(Template entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@TemplateId", entity.TemplateId, DbType.Int64);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@Title", entity.Title, DbType.String);
            parameters.Add("@Description", entity.Description, DbType.String);
            parameters.Add("@TemplateType", entity.TemplateType, DbType.String);
            parameters.Add("@Period", entity.Period, DbType.String);
            parameters.Add("@Status", entity.Status, DbType.Int32);
            parameters.Add("@FileName", entity.FileName, DbType.String);
            parameters.Add("@FilePath", entity.FilePath, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            parameters.Add("@IsApprovalRequired", entity.IsApprovalRequired, DbType.Boolean);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);

            if (entity.TemplateFields.Count > 0 && mode != TransactionModes.Delete)
            {
                DataTable dtTemplateField = new DataTable();
                dtTemplateField.Columns.Add("TemplateFieldId", typeof(Int64));
                dtTemplateField.Columns.Add("Field", typeof(string));
                dtTemplateField.Columns.Add("Placeholder", typeof(string));
                dtTemplateField.Columns.Add("IsLabel", typeof(bool));
                dtTemplateField.Columns.Add("IsActive", typeof(bool));
                dtTemplateField.Columns.Add("CertificateColumnId", typeof(int));

                foreach (var item in entity.TemplateFields)
                {
                    dtTemplateField.Rows.Add(item.TemplateFieldId, item.Field, item.Placeholder, item.IsLabel, item.IsActive,item.CertificateColumnId);
                }

                parameters.Add("@TemplateField", dtTemplateField, DbType.Object);
            }

            return parameters;
        }

        public async Task<bool> SaveTemplateImageData(Template templateModel)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", templateModel.TemplateId, DbType.Int64);
                parameters.Add("@FileName", templateModel.FileName, DbType.String);
                parameters.Add("@FilePath", templateModel.FilePath, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.SaveTemplateFileData", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public async Task<IEnumerable<Student>> GetCertificateMappingStudents(long userId, int pageIndex, int pageSize, int templateId, string schoolGroupIds, string searchString)
        {
            var studentList = new List<Student>();
            DataTable dt = new DataTable();
            dt.Columns.Add("GroupStudentId");
            dt.TableName = "tblStudentGroupId";
            if (!string.IsNullOrEmpty(schoolGroupIds))
            {
                List<int> groupIds = schoolGroupIds.Split(',').Select(int.Parse).ToList();
                foreach (var item in groupIds)
                {
                    dt.Rows.Add(item);
                }
            }
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNumber", pageIndex, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@TemplateId", templateId, DbType.Int64);
                parameters.Add("@SchoolGroupIds", dt, DbType.Object);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                var result = await conn.QueryMultipleAsync("DE.GetCertificateMappingStudents", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<Student>();
                var totalStudentCount = await result.ReadAsync<int>();
                studentList = assignmentDetails.ToList();
                int totalCount = totalStudentCount.FirstOrDefault();
                if (studentList.Any())
                {
                    studentList.ForEach(x => { x.TotalCount = totalCount; });
                }
                return studentList;
            }
        }

        public async Task<bool> SaveTemplateStudentMapping(StudentCertificateMapping certificateMapping)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", certificateMapping.TemplateId, DbType.Int32);
                parameters.Add("@StudentIds", certificateMapping.StudentIds, DbType.String);
                parameters.Add("@RemovedStudentIds", certificateMapping.RemovedStudentIds, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                parameters.Add("@SchoolGroupIds", certificateMapping.SchoolGroupIds, DbType.String);
                parameters.Add("@AssignedBy", certificateMapping.CreatedBy, DbType.Int64);
                conn.Query<int>("DE.SaveTemplateStudentMappingData", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public async Task<bool> SaveCertificateAssignedColumns(IEnumerable<TemplateField> fieldList)
        {
            var dt = new DataTable();
            dt.Columns.Add("CertificateColumnId", typeof(int));
            dt.Columns.Add("TemplateId", typeof(long));
            dt.Columns.Add("TemplateFieldId", typeof(long));
            foreach (var item in fieldList)
            {
                dt.Rows.Add(item.CertificateColumnId, item.TemplateId, item.TemplateFieldId);
            }

            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FieldsData", dt, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("DE.SaveCertificateAssignedColumns", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public async Task<IEnumerable<Template>> GetStudentAssignedCertificates(long userId, PlanTemplateTypes templateType)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@TemplateTypeId", (short)templateType, DbType.Int16);
                var result = await conn.QueryMultipleAsync("DE.GetStudentAssignedCertificates", parameters, null, null, CommandType.StoredProcedure);
                var tmpList = await result.ReadAsync<Template>();
                return tmpList.ToList();
            }
        }

        public async Task<IEnumerable<TemplateFieldMapping>> GetTemplateFieldData(int templateId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@TemplateId", templateId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("DE.GetTemplateFieldMappingData", parameters, null, null, CommandType.StoredProcedure);
                var tmpList = await result.ReadAsync<TemplateFieldMapping>();
                
                return tmpList.ToList();
            }
        }

        public async Task<bool> SaveTemplateCertificateStatus(Template model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", model.TemplateId, DbType.Int32);
                parameters.Add("@UpdatedBy", model.UpdatedBy, DbType.Int64);
                parameters.Add("@Status", model.TeacherApprovalStatus, DbType.Int32);
                parameters.Add("@RejectedComment", model.RejectedComment, DbType.String);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<Int32>("DE.SaveTemplateCertificateStatus", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public async Task<IEnumerable<CertificateTeacherMapping>> GetSchoolTemplateApproverDelegates(long schoolId, PlanTemplateTypes templateType, long? departmentId)
        {
            var teacherList = new List<CertificateTeacherMapping>();

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@TemplateTypeId", (short)templateType, DbType.Int16);
                //parameters.Add("@DepartmentId", departmentId.HasValue ? departmentId.Value : (object)DBNull.Value, DbType.Int64);
                var result = await conn.QueryMultipleAsync("DE.GetSchoolTemplateApproverDelegates", parameters, null, null, CommandType.StoredProcedure);
                var lst = await result.ReadAsync<CertificateTeacherMapping>();
                teacherList = lst.ToList();
                return teacherList;
            }
        }

        public async Task<bool> SaveCertificateApprovalTeacher(CertificateTeacherMapping model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CreatedBy", model.CreatedBy, DbType.Int64);
                parameters.Add("@SchoolId", model.SchoolId, DbType.Int64);
                parameters.Add("@TeacherIds", model.TeacherUserIds, DbType.String);
                parameters.Add("@TemplateTypeId", model.TemplateTypeId, DbType.Int16);
                //parameters.Add("@DepartmentId", model.DepartmentId, DbType.Int64);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<Int32>("DE.SaveTemplateApprovalPermission", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public async Task<IEnumerable<TemplateFieldMapping>> GetTemplatePreviewData(int templateId, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateId", templateId, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                var result = await conn.QueryMultipleAsync("DE.GetCertificatePreviewData", parameters, null, null, CommandType.StoredProcedure);
                var tmpList = await result.ReadAsync<TemplateFieldMapping>();
                return tmpList.ToList();
            }
        }


        public async Task<IEnumerable<CertificateColumns>> GetCertificateColumns(int certificateColumnType) {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TemplateColumnType", certificateColumnType, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[DE].[GetCertificateColumnsList]", parameters, null, null, CommandType.StoredProcedure);
                var tmpList = await result.ReadAsync<CertificateColumns>();
                return tmpList.ToList();
            }
        }

        public async Task<IEnumerable<TemplateFieldMapping>> GetTemplateFieldData(int templateId, List<long> userIds,string dynamicParameter)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", string.Join(",", userIds));
                parameters.Add("@TemplateId", templateId, DbType.Int32);
                parameters.Add("@dynamicParameter", dynamicParameter);
                return await conn.QueryAsync<TemplateFieldMapping>("DE.GetTemplateFieldMappingDataMultipleUserId", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<CertificateReportView>> GetCertificateReportData(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<CertificateReportView>("DE.GetCertificateReportData", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        #endregion
    }
}
