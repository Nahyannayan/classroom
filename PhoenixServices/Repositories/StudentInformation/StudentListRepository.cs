﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class StudentListRepository : SqlRepository<Student>, IStudentListRepository
    {
        private readonly IConfiguration _config;

        public StudentListRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<Student>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<Student> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Student>> GetStudentsNotInGroup(int schoolId,int groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@SchoolGroupId", groupId, DbType.Int32);
                return await conn.QueryAsync<Student>("School.GetStudentsNotInGroup", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Student>> GetStudentsInGroup(int groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", groupId, DbType.Int32);
                return await conn.QueryAsync<Student>("School.GetStudentsInGroup", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Student>> GetStudentsInSelectedGroups(string groupIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupIds", groupIds, DbType.String);
                return await conn.QueryAsync<Student>("School.GetStudentsInMultipleGroups", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Course>> GetStudentCourses(long studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", studentId, DbType.String);
                return await conn.QueryAsync<Course>("School.GetStudentCourses", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Student>> GetStudentDetailsByIds(string StudentIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentIds", StudentIds, DbType.String);
                return await conn.QueryAsync<Student>("School.GetStudentDetailsByStudentIds", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<Student> GetStudentDetailsByStudentId(int studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", studentId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<Student>("[School].[GetStudentDetailsByStudentId]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<StudentWithSection>> GetStudentByGradeIds(string gradeIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@gradeIds", gradeIds, DbType.String);
                return await conn.QueryAsync<StudentWithSection>("[School].GetStudentByGradeIds", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<StudentWithSection>> GetStudentByGradeSection(long gradeId, long sectionId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@gradeId", gradeId, DbType.Int64);
                parameters.Add("@section", sectionId, DbType.Int64);
                return await conn.QueryAsync<StudentWithSection>("[School].GetStudentByGradeSection", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(Student entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(Student entityToUpdate)
        {
            throw new NotImplementedException();
        }
        public async Task<IEnumerable<Student>> GetStudentDetailsByIdsPaginate(string StudentIds,int page,int size)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentIds", StudentIds, DbType.String);
                parameters.Add("@PageNum", page, DbType.Int32);
                parameters.Add("@PageSize", size, DbType.Int32);
                return await conn.QueryAsync<Student>("School.GetStudentDetailsByStudentIdsPaginate", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<UserEmailAccountView>> GetParentsByStudentIds(string studentIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentIds", studentIds, DbType.String);
                return await conn.QueryAsync<UserEmailAccountView>("Student.GetParentByStudentId", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Student>> GetStudentsWithStaffInGroup(int groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", groupId, DbType.Int32);
                return await conn.QueryAsync<Student>("[School].[GetStudentsWithStaffInGroup]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
    }
}
