﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;

namespace Phoenix.API.Repositories
{
    public class StudentAcademicRepository : SqlRepository<StudentAcademic>, IStudentAcademicRepository
    {

        private readonly IConfiguration _config;
        public StudentAcademicRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        #region Abstract method implementation
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<StudentAcademic>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<StudentAcademic> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(StudentAcademic entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(StudentAcademic entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region public methods
        public async Task<StudentAcademic> GetStudentAcademicData(long id)
        {
            StudentAcademic academicDetails = new StudentAcademic();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int32);
                var result = await conn.QueryMultipleAsync("Student.GetStudentAcademicDocuments", parameters, null, null, CommandType.StoredProcedure);
                var academicDocuments = await result.ReadAsync<AcademicDocuments>();
                academicDetails.Documents = academicDocuments.ToList();
                return academicDetails;
            }
        }

        public async Task<bool> UpdateAssignmentStatus(AssignmentStudentDetails model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@AssignmentId", model.AssignmentId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.UpdateAssignmentDisplayStatus", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public async Task<bool> UpdateAllAssignmentStatus(List<AssignmentStudentDetails> lstModel)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                foreach (var item in lstModel)
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@UserId", item.UserId, DbType.Int64);
                    parameters.Add("@ShowOnDashboard", item.ShowOnDashboard, DbType.Boolean);
                    parameters.Add("@AssignmentId", item.AssignmentId, DbType.Int32);
                    parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    conn.Query<int>("Student.UpdateAssignmentDisplayStatus", parameters, commandType: CommandType.StoredProcedure);
                    if (parameters.Get<int>("output") > 0)
                        result = true;
                    else
                        result = false;
                }
            }
            return result;
        }

        public async Task<bool> InsertAcademicFile(File model, long id)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int64);
                parameters.Add("@CreatedBy", model.CreatedBy, DbType.Int32);
                parameters.Add("@FileName", model.FileName, DbType.String);
                parameters.Add("@FilePath", model.FilePath, DbType.String);
                parameters.Add("@FileTypeId", model.FileTypeId, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.InsertStudentAcademicFiles", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public async Task<bool> DeleteAcademicFile(AcademicDocuments document)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", document.UserId, DbType.Int64);
                parameters.Add("@FileId", document.FileId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.DeleteAcademicFile", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;

            }
            return result;
        }

        public async Task<bool> UpdateAcademicFileDashboardStatus(AcademicDocuments model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@FileId", model.FileId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.UpdateAcademicFileDashboardStatus", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;

        }

        public async Task<bool> UpdateAssessmentReportStatus(List<StudentAssessmentReportView> lst)
        {
            var dt = new DataTable();
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("ShowOnDasboard", typeof(bool));
            dt.Columns.Add("UserId", typeof(long));
            dt.Columns.Add("Title", typeof(string));
            dt.Columns.Add("ReleaseDate", typeof(string));
            dt.Columns.Add("AcademicYearId", typeof(string));
            dt.Columns.Add("StudentNumber", typeof(string));
            lst.ForEach(x => dt.Rows.Add(x.ReportId, x.ShowOnDashboard, x.UserId, x.Title, x.ReleaseDate, x.AcademicYearId, x.StudentNumber));
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Data", dt, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.UpdateAssessmentReportStatus", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<bool> DeleteAcademicAssignment(AssignmentStudentDetails model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", model.AssignmentId, DbType.Int32);
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.DeleteStudentPortfolioAssignment", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<AcademicDocuments> GetStudentAcademicDetailById(int assignmentId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AssignmentId", assignmentId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryFirstAsync<AcademicDocuments>("Student.GetStudentAcademicDetailById", parameters, commandType: CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<AssignmentFile>> GetStudentAcademicAssignments(long userId, string searchString)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SearchString", searchString, DbType.String);
                return await conn.QueryAsync<AssignmentFile>("Student.GetStudentAcademicAssignments", parameters, commandType: CommandType.StoredProcedure);

            }
        }
        #endregion
    }
}
