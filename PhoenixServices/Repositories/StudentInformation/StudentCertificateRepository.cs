﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public class StudentCertificateRepository : SqlRepository<StudentCertificate>, IStudentCertificateRepository
    {
        private readonly IConfiguration _config;
        public StudentCertificateRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<StudentCertificate>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<StudentCertificate>> GetAllStudentCertificates(long id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int64);
                return await conn.QueryAsync<StudentCertificate>("Student.GetAllStudentCertificates", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<bool> InsertCertificateFile(CertificateFile model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@CertificateUserId", model.CertificateUserId, DbType.Int32);
                parameters.Add("@CertificateId", model.CertificateId, DbType.Int32);
                parameters.Add("@CertificateTypeId", model.CertificateTypeId, DbType.Int32);
                parameters.Add("@CertificateDescription", model.CertificateDescription, DbType.String);
                parameters.Add("@FileName", model.FileName, DbType.String);
                parameters.Add("@FilePath", model.FilePath, DbType.String);
                parameters.Add("@PhysicalFilePath", model.PhysicalFilePath, DbType.String);
                parameters.Add("@TemplateId", model.TemplateId, DbType.Int32);
                parameters.Add("@IsAutoApprove", model.IsAutoApproval, DbType.Boolean);
                parameters.Add("@FileTypeId", model.FileTypeId, DbType.Int16);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.InsertStudentCertificate", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;

        }

        public async Task<bool> UpdateDeleteUserCertificate(StudentCertificate model, char mode)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@CertificateId", model.CertificateId, DbType.Int32);
                parameters.Add("@ApprovedBy", model.ApprovedBy, DbType.Int64);
                parameters.Add("@TransMode", mode, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.UpdateDeleteUserCertificate", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }
        public override Task<StudentCertificate> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(StudentCertificate entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(StudentCertificate entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Certificate>> GetStudentCertificates(long id, int isTeacher)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();

                parameters.Add("@TeacherId", id, DbType.Int64);
                parameters.Add("@IsTeacher", isTeacher, DbType.Int32);
                return await conn.QueryAsync<Certificate>("[School].[GetCertificates]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<Certificate> GetStudentCertificatesById(long id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();

                parameters.Add("@ID", id, DbType.Int64);

                return await conn.QueryFirstAsync<Certificate>("[School].[GetCertificatesById]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int InsertCertificate(Certificate entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSchoolCertificateParameters(entity, mode);
                conn.Query<int>("[School].[CertificateCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        private DynamicParameters GetSchoolCertificateParameters(Certificate entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@CertificateId", entity.CertificateId, DbType.Int32);
            parameters.Add("@CertificateName", entity.CertificateName, DbType.String);
            parameters.Add("@CertificateDescription", entity.CertificateDescription, DbType.String);
            parameters.Add("@Status", entity.Status, DbType.String);
            parameters.Add("@UserId", entity.CreatedBy, DbType.Int32);
            parameters.Add("@FileName", entity.FileName, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

    }
}
