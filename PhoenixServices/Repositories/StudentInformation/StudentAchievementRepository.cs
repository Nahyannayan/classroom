﻿using Autofac.Core;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class StudentAchievementRepository : SqlRepository<StudentIncident>, IStudentAchievementRepository
    {
        private readonly IConfiguration _config;
        public StudentAchievementRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        #region Abstract method implementation
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<StudentIncident>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<StudentIncident> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(StudentIncident entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(StudentIncident entityToUpdate)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Public Methods
        public async Task<IEnumerable<StudentIncident>> GetStudentAchievements(long userId, int pageIndex, int pageSize, string searchString, short type)
        {
            IEnumerable<StudentIncident> acheivementList = new List<StudentIncident>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@PageIndex", pageIndex, DbType.Int64);
                parameters.Add("@PageSize", pageSize, DbType.Int64);
                parameters.Add("@SearchString", searchString, DbType.String);
                parameters.Add("@type", type, DbType.Int16);
                var result = await conn.QueryMultipleAsync("Student.GetStudentAcheivementMerits", parameters, null, null, CommandType.StoredProcedure);
                acheivementList = await result.ReadAsync<StudentIncident>();
                var totalGoalsCount = await result.ReadAsync<int>();
                if (acheivementList.Any())
                {
                    acheivementList.ToList().ForEach(x => { x.TotalCount = totalGoalsCount.FirstOrDefault(); });
                }
                return acheivementList;
            }
        }

        public async Task<IEnumerable<StudentAchievement>> GetStudentAchievementsWithFiles(long userId, int pageIndex, int pageSize, string searchString, long currentUserId)
        {
            IEnumerable<StudentAchievement> acheivementList = new List<StudentAchievement>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@PageIndex", pageIndex, DbType.Int64);
                parameters.Add("@PageSize", pageSize, DbType.Int64);
                parameters.Add("@SearchString", searchString, DbType.String);
                parameters.Add("@CurrentUserId", currentUserId, DbType.Int64);
                var result = await conn.QueryMultipleAsync("Student.GetStudentAchievements", parameters, null, null, CommandType.StoredProcedure);
                acheivementList = await result.ReadAsync<StudentAchievement>();
                var files = await result.ReadAsync<AchievementFiles>();
                if (acheivementList.Any())
                    acheivementList.First().lstAchievementFiles = files.ToList();
                #endregion
                var totalAchievementCount = await result.ReadAsync<int>();
                if (acheivementList.Any())
                {
                    acheivementList.ToList().ForEach(x => { x.TotalCount = totalAchievementCount.FirstOrDefault(); });
                }
                return acheivementList;
            }
        }
        public async Task<bool> InsertAchievement(StudentAchievement model)
        {
            bool result = false;
            DataTable dtAchievement = new DataTable();
            dtAchievement.Columns.Add("FileName", typeof(string));
            dtAchievement.Columns.Add("FilePath", typeof(string));
            dtAchievement.Columns.Add("@PhysicalFilePath", typeof(string));
            foreach (var item in model.lstAchievementFiles)
            {
                dtAchievement.Rows.Add(item.FileName, item.FilePath, item.PhysicalFilePath);
            }
            using (var conn = GetOpenConnection())
            {
                var EventDate = Convert.ToDateTime(model.EventDate);
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.StudentId, DbType.Int64);
                parameters.Add("@AcheivementId", model.AchievementId, DbType.Int64);
                parameters.Add("@Title", model.Title, DbType.String);
                parameters.Add("@TeacherId", model.TeacherId, DbType.Int64);
                parameters.Add("@Description", model.Description, DbType.String);
                parameters.Add("@EventDate", EventDate, DbType.DateTime);
                parameters.Add("@CreatedBy", model.CreatedBy, DbType.Int64);
                parameters.Add("@AchievementFiles", dtAchievement, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.AchievementOperations", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;

            }
            return result;
        }

        public async Task<bool> DeleteAchievement(StudentAchievement model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AchievementId", model.AchievementId, DbType.Int64);
                parameters.Add("@DeletedBy", model.DeletedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.AchievementOperations", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }
        public async Task<bool> UpdateAchievements(List<StudentAchievement> lstModel)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                foreach (var item in lstModel)
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@AchievementId", item.AchievementId, DbType.Int64);
                    parameters.Add("@ShowOnPortfolio", item.ShowOnPortfolio, DbType.Boolean);
                    parameters.Add("@UdpatedBy", item.UpdatedBy, DbType.Int64);
                    parameters.Add("@Files", item.lstAchievementFiles, DbType.Object);
                    parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    conn.Query<int>("Student.AchievementOperations", parameters, commandType: CommandType.StoredProcedure);
                    if (parameters.Get<int>("output") > 0)
                        result = true;
                    else
                        result = false;
                }
            }
            return result;
        }

        public async Task<bool> UpdateAchievementsPortfolioStatus(List<StudentAchievement> model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                foreach (var item in model)
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@UserId", item.StudentId, DbType.Int64);
                    parameters.Add("@AchievementId", item.AchievementId, DbType.Int64);
                    parameters.Add("@ShowOnPortfolio", item.ShowOnPortfolio, DbType.Boolean);
                    parameters.Add("@UpdatedBy", item.UpdatedBy, DbType.Int64);
                    parameters.Add("@IsCertificate", item.IsCertificate, DbType.Boolean);
                    parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    conn.Query<int>("Student.UpdateAchievementsPortfolioStatus", parameters, commandType: CommandType.StoredProcedure);
                    if (parameters.Get<int>("output") > 0)
                        result = true;
                    else
                        result = false;
                }
            }
            return result;
        }


        public async Task<bool> UpdateAcheivementDashboardStatus(StudentIncident model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@StudentIncidentId", model.StudentIncidentId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.UpdateStudentAcheivementDashboardStatus", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
            }
            return result;
        }

        public async Task<bool> SaveStudentAcheivementFile(AchievementFiles model, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Mode", (short)mode, DbType.Int16);
                parameters.Add("@FileId", model.FileId, DbType.Int64);
                parameters.Add("@FilePath", model.FilePath, DbType.String);
                parameters.Add("@AchievementId", model.AchievementId, DbType.Int64);
                parameters.Add("@FileName", model.FileName, DbType.String);
                parameters.Add("@PhysicalFilePath", model.PhysicalFilePath, DbType.String);
                parameters.Add("@CreatedBy", model.CreatedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.SaveStudentAcheivementFile", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;

            }
        }

        public async Task<StudentAchievement> GetStudentAcheivementById(long acheivementId)
        {
            var model = new StudentAchievement();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AcheivementId", acheivementId, DbType.Int64);
                var result = await conn.QueryMultipleAsync("Student.GetStudentAcheivementById", parameters, null, null, CommandType.StoredProcedure);
                model = await result.ReadFirstAsync<StudentAchievement>();
                var attachments = await result.ReadAsync<AchievementFiles>();
                model.lstAchievementFiles = attachments.ToList();
                return model;
            }
        }

        public async Task<bool> DeleteAcheivement(StudentAchievement model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AcheivementId", model.AchievementId, DbType.Int64);
                parameters.Add("@UpdatedBy", model.CreatedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.DeleteStudentAcheivement", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<bool> UpdateAcheivementApprovalStatus(StudentAchievement model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AcheivementId", model.AchievementId, DbType.Int64);
                parameters.Add("@Mode", model.Mode, DbType.String);
                parameters.Add("@UpdatedBy", model.CreatedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.UpdateAcheivementApprovalStatus", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
    }
}
