﻿using Phoenix.API.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
   public interface IStudentListRepository
    {
        Task<IEnumerable<Student>> GetStudentsNotInGroup(int schoolId,int groupId);
        Task<IEnumerable<Student>> GetStudentsInGroup(int groupId);
        Task<IEnumerable<Student>> GetStudentsInSelectedGroups(string groupIds);
        Task<IEnumerable<Student>> GetStudentDetailsByIds(string studentIds);
        Task<IEnumerable<Course>> GetStudentCourses(long studentId);
        Task<Student> GetStudentDetailsByStudentId(int studentId);
        Task<IEnumerable<StudentWithSection>> GetStudentByGradeIds(string gradeIds);
        Task<IEnumerable<StudentWithSection>> GetStudentByGradeSection(long gradeId, long sectionId);
        Task<IEnumerable<Student>> GetStudentDetailsByIdsPaginate(string StudentIds, int page, int size);
        Task<IEnumerable<UserEmailAccountView>> GetParentsByStudentIds(string studentIds);
        Task<IEnumerable<Student>> GetStudentsWithStaffInGroup(int groupId);
    }
}
