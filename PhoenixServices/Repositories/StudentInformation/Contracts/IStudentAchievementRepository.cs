﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Models;

namespace Phoenix.API.Repositories
{
    public interface IStudentAchievementRepository
    {
        Task<IEnumerable<StudentIncident>> GetStudentAchievements(long userId, int pageIndex, int pageSize, string searchString, short type);
        Task<IEnumerable<StudentAchievement>> GetStudentAchievementsWithFiles(long userId, int pageIndex, int pageSize, string searchString, long currentUserId);
        Task<bool> InsertAchievement(StudentAchievement model);
        Task<bool> DeleteAchievement(StudentAchievement model);
        Task<bool> UpdateAchievements(List<StudentAchievement> model);
        Task<bool> UpdateAchievementsPortfolioStatus(List<StudentAchievement> lstModel);
        Task<bool> UpdateAcheivementDashboardStatus(StudentIncident model);
        Task<bool> SaveStudentAcheivementFile(AchievementFiles model, TransactionModes mode);
        Task<StudentAchievement> GetStudentAcheivementById(long acheivementId);
        Task<bool> DeleteAcheivement(StudentAchievement model);
        Task<bool> UpdateAcheivementApprovalStatus(StudentAchievement model);
    }
}
