﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Models;


namespace Phoenix.API.Repositories
{
    public interface IStudentCertificateRepository
    {
        Task<IEnumerable<StudentCertificate>> GetAllStudentCertificates(long id);
        Task<bool> InsertCertificateFile(CertificateFile certificateFile);
        Task<bool> UpdateDeleteUserCertificate(StudentCertificate model, char mode);
        Task<IEnumerable<Certificate>> GetStudentCertificates(long id, int isTeacher);
        Task<Certificate> GetStudentCertificatesById(long id);
        int InsertCertificate(Certificate model,TransactionModes modes);
        
    }
}
