﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DbConnection;
using Phoenix.API.Models.DashBoard;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public interface ITeachersDashboardBannerRepository : IGenericRepository<DashBoardBanners>
    {
       Task<IEnumerable<DashBoardBanners>> GetTeacherDashboardBanners(int id);
        
    }
}
