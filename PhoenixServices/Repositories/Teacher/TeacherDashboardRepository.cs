﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models.Entities;
namespace Phoenix.API.Repositories
{
    public class TeacherDashboardRepository : SqlRepository<TeacherDashboard>, ITeacherDashboardRepository
    {
        private readonly IConfiguration _config;

        public TeacherDashboardRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<TeacherDashboard>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<TeacherDashboard> GetAsync(int id)
        {
            throw new NotImplementedException();
        }



        public override void InsertAsync(TeacherDashboard entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(TeacherDashboard entityToUpdate)
        {
            throw new NotImplementedException();
        }


        public async Task<TeacherDashboard> GetTeacherDashboard(int id)
        {
            TeacherDashboard teacherDashboard = new TeacherDashboard();

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int32);
                var result = await conn.QueryMultipleAsync("School.TeacherDashoard", parameters, null, null, CommandType.StoredProcedure);

                teacherDashboard.SchoolBanners.AddRange(result.Read<SchoolBanner>());
                teacherDashboard.ClassGroups.AddRange(result.Read<SchoolGroup>());
                teacherDashboard.OtherGroups.AddRange(result.Read<SchoolGroup>());
                return teacherDashboard;
            }
        }

        public async Task<List<Student>> GetStudentForTeacher(long id, string ids = "")
        {
            List<Student> objStudent = new List<Student>();
            DataTable dt = new DataTable();
            dt.Columns.Add("GroupStudentId");
            dt.TableName = "tblStudentGroupId";
            if (!string.IsNullOrEmpty(ids))
            {
                List<int> groupIds = ids.Split(',').Select(int.Parse).ToList();
                foreach (var item in groupIds)
                {
                    dt.Rows.Add(item);
                }
            }

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TeacherId", id, DbType.Int64);
                parameters.Add("@tblStudentGroupId", dt, DbType.Object);
                // return await conn.QueryFirstOrDefaultAsync<TeacherDashboard>("School.TeacherDashoard", parameters, null, null, CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("School.GetStudentForTeacher", parameters, null, null, CommandType.StoredProcedure);
                var student = await result.ReadAsync<Student>();

                return student.ToList();
            }
        }

        public async Task<List<Student>> GetStudentForTeacherBySchoolGroup(int id, int pageNumber, int pageSize, string schoolGroupIds, string searchString, string sortBy)
        {
            var studentList = new List<Student>();
            DataTable dt = new DataTable();
            dt.Columns.Add("GroupStudentId");
            dt.TableName = "tblStudentGroupId";
            if (!string.IsNullOrEmpty(schoolGroupIds))
            {
                List<int> groupIds = schoolGroupIds.Split(',').Select(int.Parse).ToList();
                foreach (var item in groupIds)
                {
                    DataRow _group = dt.NewRow();
                    _group["GroupStudentId"] = item;
                    dt.Rows.Add(_group);
                }
            }
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNumber", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@TeacherId", id, DbType.Int64);
                parameters.Add("@SchoolGroupIds", dt, DbType.Object);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                parameters.Add("@SortBy", sortBy, DbType.String);
                var result = await conn.QueryMultipleAsync("School.GetStudentForTeacherBySchoolGroup", parameters, null, null, CommandType.StoredProcedure);
                var assignmentDetails = await result.ReadAsync<Student>();
                var totalStudentCount = await result.ReadAsync<int>();
                studentList = assignmentDetails.ToList();
                int totalCount = totalStudentCount.FirstOrDefault();
                if (studentList.Any())
                {
                    studentList.ForEach(x => { x.TotalCount = totalCount; });
                }
                return studentList;
            }
        }
    }
}
