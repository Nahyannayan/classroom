﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models.Entities;
using Microsoft.SharePoint.Client;
using Phoenix.API.Models.DashBoard;

namespace Phoenix.API.Repositories.Teacher
{
    public class TeachersDashboardBannerRepository : SqlRepository<SchoolBanner>, ITeachersDashboardBannerRepository
    {
        private readonly IConfiguration _config;

        public TeachersDashboardBannerRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<SchoolBanner>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<SchoolBanner> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<DashBoardBanners>> GetTeacherDashboardBanners(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int32);
                var result = await conn.QueryAsync<DashBoardBanners>("School.TeachersDashBoardBanners", parameters, null, null, CommandType.StoredProcedure);
                return result;
            }
        }

        public void InsertAsync(DashBoardBanners entity)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(SchoolBanner entity)
        {
            throw new NotImplementedException();
        }

        public void UpdateAsync(DashBoardBanners entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SchoolBanner entityToUpdate)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<DashBoardBanners>> IGenericRepository<DashBoardBanners>.GetAllAsync()
        {
            throw new NotImplementedException();
        }

        Task<DashBoardBanners> IGenericRepository<DashBoardBanners>.GetAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
