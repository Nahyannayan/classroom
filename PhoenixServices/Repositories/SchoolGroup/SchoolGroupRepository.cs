﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class SchoolGroupRepository : SqlRepository<SchoolGroup>, ISchoolGroupRepository
    {
        private readonly IConfiguration _config;
        public SchoolGroupRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<SchoolGroup>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async override Task<SchoolGroup> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<SchoolGroup>("School.GetSchoolGroupDetailsByGroupId", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroups(int userId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "")
        {
            var list = new List<SchoolGroup>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@IsBespokeGroup", IsBeSpokeGroup, DbType.Int32);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                var result = await conn.QueryMultipleAsync("[School].[GetSchoolGroups]", parameters, null, null, CommandType.StoredProcedure);
                var schoolGroups = await result.ReadAsync<SchoolGroup>();
                var schoolGroupsCount = await result.ReadAsync<int>();
                list = schoolGroups.ToList();
                int totalCount = schoolGroupsCount.FirstOrDefault();
                if (list.Any())
                {
                    list.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return list;
            }

        }
        /// <summary>
        /// add by ashish for GetOtherschoolgroups
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="IsBeSpokeGroup"></param>
        /// <param name="searchString"></param>
        /// <returns></returns>
        public async Task<IEnumerable<SchoolGroup>> GetOtherschoolgroups(int userId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "")
        {
            var list = new List<SchoolGroup>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@IsBespokeGroup", IsBeSpokeGroup, DbType.Int32);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                var result = await conn.QueryMultipleAsync("[School].[GetOtherschoolGroup]", parameters, null, null, CommandType.StoredProcedure);
                var schoolGroups = await result.ReadAsync<SchoolGroup>();
                var schoolGroupsCount = await result.ReadAsync<int>();
                list = schoolGroups.ToList();
                int totalCount = schoolGroupsCount.FirstOrDefault();
                if (list.Any())
                {
                    list.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return list;
            }

        }
        

        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroupsForAdminSpace(SchoolGroupsReportRequest model)
        {
            var list = new List<SchoolGroup>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", model.PageNumber, DbType.Int32);
                parameters.Add("@PageSize", model.PageSize, DbType.Int32);
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@SchoolId", model.SchoolId, DbType.Int64);
                parameters.Add("@TeacherIds", model.TeacherIds, DbType.String);
                parameters.Add("@ClassGroupIds", model.ClassGroupIds, DbType.String);
                parameters.Add("@GroupTypeIds", model.GroupTypeIds, DbType.String);
                parameters.Add("@CourseIds", model.CourseIds, DbType.String);
                parameters.Add("@IsBespokeGroup", model.IsBeSpokeGroup, DbType.Boolean);
                parameters.Add("@SearchString", model.SearchString == null ? "" : model.SearchString, DbType.String);
                var result = await conn.QueryMultipleAsync("[School].[GetSchoolGroupsForSchoolAdmin]", parameters, null, null, CommandType.StoredProcedure);
                var schoolGroups = await result.ReadAsync<SchoolGroup>();
                var schoolGroupsCount = await result.ReadAsync<int>();
                list = schoolGroups.ToList();
                int totalCount = schoolGroupsCount.FirstOrDefault();
                if (list.Any())
                {
                    list.ForEach(x => { x.TotalCount = totalCount; });
                }
                return list;
            }

        }

        public async Task<IEnumerable<Course>> GetCoursesBySchoolId(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<Course>("[School].[GetCoursesBySchoolId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroupsByStudentId(long studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", studentId, DbType.Int64);
                return await conn.QueryAsync<SchoolGroup>("[School].[GetSchoolGroupsByStudentId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        /// <summary>
        /// Add by ashish for GetOtherschoolStudentGroups
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="IsBespokeGroup"></param>
        /// <param name="searchString"></param>
        /// <returns></returns>

        public async Task<IEnumerable<SchoolGroup>> GetOtherschoolStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string searchString = "")
        {
            var list = new List<SchoolGroup>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", PageNumber, DbType.Int32);
                parameters.Add("@PageSize", PageSize, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int32);
                parameters.Add("@IsBespokeGroup", IsBespokeGroup, DbType.Int32);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                //return await conn.QueryAsync<SchoolGroup>("[School].[GetStudentGroups]", parameters, null, null, CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("[Student].[GetOtherschoolStudentGroups]", parameters, null, null, CommandType.StoredProcedure);
                var schoolGroups = await result.ReadAsync<SchoolGroup>();
                var schoolGroupsCount = await result.ReadAsync<int>();
                list = schoolGroups.ToList();
                int totalCount = schoolGroupsCount.FirstOrDefault();
                if (list.Any())
                {
                    list.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return list;
            }
        }




        public async Task<IEnumerable<SchoolGroup>> GetStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string searchString = "")
        {
            var list = new List<SchoolGroup>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", PageNumber, DbType.Int32);
                parameters.Add("@PageSize", PageSize, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int32);
                parameters.Add("@IsBespokeGroup", IsBespokeGroup, DbType.Int32);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                //return await conn.QueryAsync<SchoolGroup>("[School].[GetStudentGroups]", parameters, null, null, CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("[School].[GetStudentGroups]", parameters, null, null, CommandType.StoredProcedure);
                var schoolGroups = await result.ReadAsync<SchoolGroup>();
                var schoolGroupsCount = await result.ReadAsync<int>();
                list = schoolGroups.ToList();
                int totalCount = schoolGroupsCount.FirstOrDefault();
                if (list.Any())
                {
                    list.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return list;
            }
        }

        public int CheckStudentGroupsAvailable(long studentId, int IsBespokeGroup = 0)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@IsBespokeGroup", IsBespokeGroup, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[CheckStudentGroupsAvailable]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public IEnumerable<long> GetDisabledGroupList(long BlogId, long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", BlogId, DbType.Int64);
                parameters.Add("@SchoolId", SchoolId, DbType.Int32);
                var result= conn.Query<long>("[School].[GetGroupHierarchyByBlogId]", parameters, commandType: CommandType.StoredProcedure);
                return result;
            }
        }
        public int UpdateBlogWithNewUpdate(long UpdateBlogId, long UpdatFromBlogId, bool IsApproved, bool IsRejected)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UpdateBlogId", UpdateBlogId, DbType.Int64);
                parameters.Add("@UpdatFromBlogId", UpdatFromBlogId, DbType.Int64);
                parameters.Add("@IsApproved", IsApproved, DbType.Boolean);
                parameters.Add("@IsRejected", IsRejected, DbType.Boolean);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[UpdateBlogs]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }

        }
        public override void InsertAsync(SchoolGroup entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SchoolGroup entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public int UpdateSchoolGroupData(SchoolGroup entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSchoolGroupsParameters(entity, mode);

                conn.Query<int>("School.SchoolGroupsDetailsCUD", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("output");
                return result;

            }
        }
        public async Task<IEnumerable<SchoolGroup>> GetSubjectSchoolGroup(int subjectId, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SubjectId", subjectId, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<SchoolGroup>("[School].[GetSubjectSchoolGroup]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        #region Private Methods
        private DynamicParameters GetSchoolGroupsParameters(SchoolGroup entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SchoolGroupId", entity.SchoolGroupId, DbType.Int32);
            parameters.Add("@SchoolGroupName", entity.SchoolGroupName, DbType.String);
            parameters.Add("@SchoolGroupDescription", entity.SchoolGroupDescription, DbType.String);
            parameters.Add("@SubjectId", entity.SubjectId, DbType.Int32);
            // parameters.Add("@StudentIdsToAdd", entity.StudentIdsToAdd, DbType.String);
            // parameters.Add("@StudentIdsToRemove", entity.StudentIdsToRemove, DbType.String);
            // parameters.Add("@MembersidToAdd", entity.MembersToAdd, DbType.String);
            // parameters.Add("@MembersidToRemove", entity.MembersToRemove, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.String);
            parameters.Add("@UserId", entity.CreatedById, DbType.Int32);
            //   parameters.Add("@ddlschoolgroupId", entity.DDLSchoolGroupId, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@GroupImage", entity.GroupImage, DbType.String);
            parameters.Add("@Description", entity.Description, DbType.String);
            parameters.Add("@AutoSyncGroupMember", entity.AutoSyncGroupMember, DbType.Boolean);
            parameters.Add("@IsBespokeGroup", entity.IsBespokeGroup, DbType.Boolean);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        #endregion

        public bool UpdateTeacherGivenName(SchoolGroup entity)
        {
            using (var conn = GetOpenConnection())
            {
                string query = string.Join("\n",
             "UPDATE School.SchoolGroups SET TeacherGivenName=@TeacherGivenName  where SchoolGroupId=@SchoolGroupId");

                var parameters = new DynamicParameters();
                parameters.Add("@TeacherGivenName", entity.TeacherGivenName, DbType.String);
                parameters.Add("@SchoolGroupId", entity.SchoolGroupId, DbType.Int32);

                return conn.Execute(query, parameters, commandType: CommandType.Text) > 0;
            }
        }

        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroupsBySchoolId(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<SchoolGroup>("School.GetSchoolGroupBySchoolId", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroupsByUserId(long userId, bool isTeacher)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                return await conn.QueryAsync<SchoolGroup>("School.GetSchoolGroupByUserId", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolGroup>> GetGroupsByBespoke()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<SchoolGroup>("[School].[GetCurrentYearGroups]", null, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ListItem>> GetUnassignedMembers(short systemLanguageId, int schoolId, long userId, int? userTypeId, int schoolGroupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SystemLanguageId", 1, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@UserTypeId", userTypeId, DbType.Int32);
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int32);
                return await conn.QueryAsync<ListItem>("[School].[GetUnAssignedGroupMembers]", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ListItem>> GetUnassignedMembers(short systemLanguageId, int schoolId, long userId, int? userTypeId, int schoolGroupId, string selectedSchoolGroupIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SystemLanguageId", 1, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@UserTypeId", userTypeId, DbType.Int32);
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int32);
                parameters.Add("@SelectedSchoolGroupIds", selectedSchoolGroupIds, DbType.String);
                return await conn.QueryAsync<ListItem>("[School].[GetUnAssignedGroupMembers]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public bool AddUpdateGroupMember(List<GroupMemberMapping> groupMemberMappings)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AutoID", typeof(Int64));
            dt.Columns.Add("GroupTeacherMappingId", typeof(Int64));
            dt.Columns.Add("GroupId", typeof(Int64));
            dt.Columns.Add("MemberId", typeof(string));
            dt.Columns.Add("IsViewOnly", typeof(string));
            dt.Columns.Add("IsContribute", typeof(Int32));
            dt.Columns.Add("IsManager", typeof(Int32));
            dt.Columns.Add("ToBeDeleted", typeof(Boolean));

            using (var conn = GetOpenConnection())
            {
                int i = 1;
                foreach (var memberToAdd in groupMemberMappings)
                {
                    dt.Rows.Add(i, memberToAdd.GroupTeacherMappingId, memberToAdd.GroupId, memberToAdd.MemberId, memberToAdd.IsViewOnly, memberToAdd.IsContribute, memberToAdd.IsManager, memberToAdd.ToBeDeleted);
                    i = i + 1;
                }
                var parameters = new DynamicParameters();
                parameters.Add("@GroupMembers", dt, DbType.Object);
                parameters.Add("@UpdatedBy", groupMemberMappings != null ? groupMemberMappings.FirstOrDefault().UpdatedBy : (object)DBNull.Value, DbType.Int64);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.GroupMemberMappingCUD", parameters, commandType: CommandType.StoredProcedure);
            }
            return true;

        }

        public async Task<IEnumerable<GroupMemberMapping>> GetAssignedMembers(int systemLanguageId, int schoolGroupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SystemLanguageId", systemLanguageId, DbType.Int32);
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int32);
                return await conn.QueryAsync<GroupMemberMapping>("[School].[GetAssignedGroupMembers]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public bool InsertGroupMessage(GroupMessage groupMessage)
        {
            using (var conn = GetOpenConnection())
            {
                //string query = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@ParentGroupId", groupMessage.ParentGroupId, DbType.Int32);
                parameters.Add("@Title", groupMessage.Title, DbType.String);
                parameters.Add("@Message", groupMessage.Message, DbType.String);
                parameters.Add("@Attachment", groupMessage.Attachment, DbType.String);
                parameters.Add("@MemberIds", groupMessage.MemberIds, DbType.String);
                parameters.Add("@SelectedGroups", groupMessage.SelectedGroups, DbType.String);
                parameters.Add("@NotifyTo", groupMessage.NotifyTo, DbType.String);
                parameters.Add("@CreatedBy", groupMessage.CreatedBy, DbType.Int64);

                return conn.Execute("School.InsertGroupMessage", parameters, commandType: CommandType.StoredProcedure) > 0;
            }
        }

        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroupsHavingBlogs(long id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserID", id, DbType.Int64);
                // parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                return await conn.QueryAsync<SchoolGroup>("[School].[GetSchoolGroupsHavingBlogs]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<GroupMessage>> GetGroupMessageListByGroup(int groupId, long id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupId", groupId, DbType.Int32);
                parameters.Add("@UserId", id, DbType.Int64);
                // parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                return await conn.QueryAsync<GroupMessage>("[School].[GetGroupMessageListByGroup]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public bool UpdateLastSeen(long userId, int schoolGroupId, bool isParent)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int32);
                parameters.Add("@IsParent", isParent, DbType.Boolean);
                return conn.Execute("School.UpdateLastSeenofGroup", parameters, commandType: CommandType.StoredProcedure) > 0;
            }
        }

        #region Archive School Group

        public bool UpdateArchiveSchoolGroup(string schoolGroupIds, long teacherId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupIds", schoolGroupIds, DbType.String);
                parameters.Add("@TeacherId", teacherId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("School.UpdateArchiveSchoolGroup", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<SchoolGroup> GetArchivedSchoolGroupsByUserId(long userId, bool isTeacher)
        {
            SchoolGroup objGroup = new SchoolGroup();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                var result = await conn.QueryMultipleAsync("School.GetArchiveSchoolGroupByUserId", parameters, commandType: CommandType.StoredProcedure);
                var listOfArchivedGroup = await result.ReadAsync<SchoolGroup>();
                objGroup.lstArchivedGroups = listOfArchivedGroup.ToList();
                return objGroup;
            }
        }
        public async Task<SchoolGroup> GetActiveSchoolGroupsByUserId(long userId, bool isTeacher)
        {
            SchoolGroup objGroup = new SchoolGroup();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                var result = await conn.QueryMultipleAsync("School.GetSchoolGroupByUserId", parameters, commandType: CommandType.StoredProcedure);
                var listOfActiveSchoolGroup = await result.ReadAsync<SchoolGroup>();
                objGroup.lstActiveGroups = listOfActiveSchoolGroup.ToList();
                return objGroup;
            }
        }
        public async Task<IEnumerable<SchoolGroup>> GetArchivedSchoolGroups(int userId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "")
        {
            var list = new List<SchoolGroup>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@IsBespokeGroup", IsBeSpokeGroup, DbType.Int32);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                var result = await conn.QueryMultipleAsync("[School].[GetArchivedSchoolGroups]", parameters, null, null, CommandType.StoredProcedure);
                var schoolGroups = await result.ReadAsync<SchoolGroup>();
                var schoolGroupsCount = await result.ReadAsync<int>();
                list = schoolGroups.ToList();
                int totalCount = schoolGroupsCount.FirstOrDefault();
                if (list.Any())
                {
                    list.ForEach(x => { x.TotalCount = totalCount; });
                }
                //var result = await conn.QueryMultipleAsync<AssignmentStudentDetails>("Assignment.GetAssignmentsByStudentId", parameters, null, null, CommandType.StoredProcedure);
                return list;
            }

        }

        public async Task<bool> UpdateGroupMeeting(MeetingResponse model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", model.SchoolGroupId, DbType.Int32);
                parameters.Add("@Mode", model.Mode, DbType.Int16);
                parameters.Add("@CreatedBy", model.CreatedBy, DbType.Int64);
                parameters.Add("@MeetingId", model.MeetingID, DbType.String);
                parameters.Add("@StartDate", model.StartDate, DbType.String);
                parameters.Add("@StartTime", model.StartTime, DbType.String);
                parameters.Add("@EndTime", model.EndTime, DbType.String);
                parameters.Add("@MeetingDuration", model.Duration, DbType.Int16);
                parameters.Add("@MeetingName", model.MeetingName, DbType.String);
                parameters.Add("@AttendeePassword", model.AttendeePW, DbType.String);
                parameters.Add("@ModeratorPassword", model.ModeratorPW, DbType.String);
                parameters.Add("@OldMeetingId", model.OldMeetingId, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("School.UpdateSchoolGroupMeetingInfo", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<bool> RemoveGroupMeeting(string meetingId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", DBNull.Value, DbType.Int64);
                parameters.Add("@MeetingId", meetingId, DbType.String);
                parameters.Add("@Mode", TransactionModes.Delete, DbType.Int16);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("School.UpdateSchoolGroupMeetingInfo", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        #endregion Archive School Group

        #region Teams Meeting

        public async Task<long> UpdateTeamsMeetingInfo(MSTeamsReponse model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", model.SchoolGroupId, DbType.Int64);
                parameters.Add("@TeamsMeetingId", model.id, DbType.String);
                parameters.Add("@JoinUrl", model.OnlineMeeting.JoinUrl, DbType.String);
                parameters.Add("@WebJoinUrl", model.joinWebUrl, DbType.String);
                parameters.Add("@MeetingStartDateTime", model.Start.DateTime, DbType.String);
                parameters.Add("@MeetingEndDateTime", model.End.DateTime, DbType.String);
                parameters.Add("@LessonName", model.subject, DbType.String);
                parameters.Add("@CreatedBy", model.CreatedBy, DbType.Int64);
                parameters.Add("@IsPerUser", model.IsPerUser, DbType.Boolean);
                parameters.Add("@output", dbType: DbType.Int64, direction: ParameterDirection.Output);
                conn.Query<bool>("School.UpdateTeamsMeetingInfo", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<long>("output");
            }
        }

        public async Task<IEnumerable<MSTeamsReponse>> GetAllTeamsMeetings(int schoolGroupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int32);
                return await conn.QueryAsync<MSTeamsReponse>("[School].[GetAllTeamsMeeting]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<long> UpdateZoomMeetingInfo(ZoomMeetingResponse zoomMeetingResponse)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", zoomMeetingResponse.SchoolGroupId, DbType.Int32);
                parameters.Add("@ZoomMeetingId", zoomMeetingResponse.id, DbType.String);
                parameters.Add("@JoinUrl", zoomMeetingResponse.join_url, DbType.String);
                parameters.Add("@StartUrl", zoomMeetingResponse.start_url, DbType.String);
                parameters.Add("@MeetingStartDateTime", zoomMeetingResponse.start_time, DbType.String);
                parameters.Add("@MeetingName", zoomMeetingResponse.topic, DbType.String);
                parameters.Add("@Password", zoomMeetingResponse.password, DbType.String);
                parameters.Add("@Duration", Convert.ToInt16(zoomMeetingResponse.duration), DbType.Int16);
                parameters.Add("@CreatedBy", zoomMeetingResponse.CreatedBy, DbType.Int64);
                parameters.Add("@IsSSOMeeting", zoomMeetingResponse.IsSSOEnabled, DbType.Boolean);
                parameters.Add("@IsPerUser", zoomMeetingResponse.IsPerStudent, DbType.Boolean);
                parameters.Add("@output", dbType: DbType.Int64, direction: ParameterDirection.Output);
                conn.Query<bool>("School.UpdateZoomMeetingInfo", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<long>("output");
            }
        }

        public async Task<bool> UpdateZoomMeetingParticipants(List<ZoomMeetingResponse> meetingParticipants, string removedParticipants = "")
        {
            var ds = new DataTable();
            ds.Columns.Add("UserId", typeof(long));
            ds.Columns.Add("JoinUrl", typeof(string));
            ds.Columns.Add("MeetingId", typeof(string));
            foreach (var meetingInfo in meetingParticipants)
            {
                ds.Rows.Add(meetingInfo.UserId, meetingInfo.join_url, meetingInfo.id);
            }
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@MeetingInfo", ds, DbType.Object);
                parameters.Add("@RemovedParticipants", removedParticipants, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("School.UpdateGroupZoomParticipantsInfo", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<ZoomMeetingResponse>> GetAllZoomMeetings(int schoolGroupId, string meetingId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int32);
                parameters.Add("@MeetingId", meetingId, DbType.String);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<ZoomMeetingResponse>("[School].[GetAllGroupZoomMeetings]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #endregion

        #region Adobe Connect Meeting

        public async Task<bool> SaveGroupAdobeMeeting(AdobeConnectMeeting meetingInfo)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", meetingInfo.SchoolGroupId, DbType.Int32);
                parameters.Add("@MeetingId", meetingInfo.MeetingId, DbType.Int32);
                parameters.Add("@StartDateTime", meetingInfo.StartDateTime, DbType.String);
                parameters.Add("@EndDateTime", meetingInfo.EndDateTime, DbType.String);
                parameters.Add("@MeetingUrlSegment", meetingInfo.MeetingUrlSegment, DbType.String);
                parameters.Add("@MeetingName", meetingInfo.MeetingName, DbType.String);
                parameters.Add("@CreatedBy", meetingInfo.CreatedBy, DbType.Int64);
                parameters.Add("@CreatedDate", meetingInfo.CreatedDateTime, DbType.String);
                parameters.Add("@FolderId", meetingInfo.FolderId, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("School.SaveAdobeMeetingInfo", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<AdobeConnectMeeting>> GetAllAdobeGroupMeetings(int schoolGroupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int32);
                return await conn.QueryAsync<AdobeConnectMeeting>("School.GetAllAdobeGroupMeetings", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        #endregion

        #region BBB Live Class Meetings

        public async Task<IEnumerable<MeetingResponse>> GetAllGroupBBBMeetings(int? schoolGroupId, string meetingId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", schoolGroupId.HasValue ? schoolGroupId.Value : (object)DBNull.Value, DbType.Int32);
                parameters.Add("@MeetingId", !string.IsNullOrEmpty(meetingId) ? meetingId : (object)DBNull.Value, DbType.String);
                return await conn.QueryAsync<MeetingResponse>("School.GetAllBlueButtonGroupMeetings", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<EventView>> GetCurrentGroupMeeting(long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<EventView>("School.GetCurrentGroupMeeting", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #endregion

        #region Web-Ex Group Meetings

        public async Task<bool> UpdateGroupWebExMeeting(WebExMeetingData meetingData)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", meetingData.SchoolGroupId, DbType.Int32);
                parameters.Add("@MeetingId", meetingData.Meetingkey, DbType.String);
                parameters.Add("@MeetingName", meetingData.MeetingName, DbType.String);
                parameters.Add("@StartDate", meetingData.StartDate, DbType.String);
                parameters.Add("@StartTime", meetingData.StartTime, DbType.String);
                parameters.Add("@Duration", meetingData.Duration, DbType.Int16);
                parameters.Add("@CreatedBy", meetingData.CreatedBy, DbType.Int64);
                parameters.Add("@MeetingPassword", meetingData.MeetingPassword, DbType.String);
                parameters.Add("@HostCalendarUrl", meetingData.ICalendarURL.Host, DbType.String);
                parameters.Add("@AttendeeCalendarUrl", meetingData.ICalendarURL.Attendee, DbType.String);
                parameters.Add("@GuestToken", meetingData.GuestToken, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("School.SaveGroupWebExMeetings", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<WebExMeetingData>> GetAllGroupWebExMeetings(int schoolGroupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupId", schoolGroupId, DbType.Int32);
                return await conn.QueryAsync<WebExMeetingData>("School.GetAllGroupWebExMeeting", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #endregion


        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroupWithPagination(long userId, short pageSize, int pageNumber)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@PageIndex", pageNumber, DbType.Int32);
                return await conn.QueryAsync<SchoolGroup>("Student.GetAllSchoolGroupWithPagination", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SchoolLevel>> GetAllSchoolLevel(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId, DbType.Int64);

                return await conn.QueryAsync<SchoolLevel>("Student.GetAllSchoolLevel", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolDepartment>> GetAllGetAllSchoolDepartment(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId, DbType.Int64);

                return await conn.QueryAsync<SchoolDepartment>("Student.GetAllSchoolDepartment", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SchoolDepartment>> GetAllSchoolDepartmentBySchoolLevel(long schoolId, long schoolLevelId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId, DbType.Int64);

                return await conn.QueryAsync<SchoolDepartment>("Student.GetAllSchoolDepartment", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolCourse>> GetAllSchoolCoursesBySchoolLevelAndSchoolIdAndDepartmentId(long schoolId, long schoolLevelId, long departmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId, DbType.Int64);

                return await conn.QueryAsync<SchoolCourse>("Student.GetAllSchoolCoursesBySchoolLevelAndSchoolIdAndDepartmentId", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        public async Task<IEnumerable<UserEmailAccountView>> GetSchoolGroupUserEmailAddress(string schoolGroupIds, string studentIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGroupIds", schoolGroupIds, DbType.String);
                parameters.Add("@StudentIds", studentIds, DbType.String);
                return await conn.QueryAsync<UserEmailAccountView>("FMS.GetSchoolGroupUserEmailAccounts", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<GroupMemberMapping>> GetMemberMailingDetails(string selectedMembers)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SelectedMembers", selectedMembers, DbType.String);
                return await conn.QueryAsync<GroupMemberMapping>("School.GetMemberMailingDetails", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<RecordedSessionView>> GetGroupRecordedSessions(int groupId, int pageIndex, int pageSize, string searchText, string fromDate, string toDate)
        {
            IEnumerable<RecordedSessionView> lst = new List<RecordedSessionView>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupId", groupId, DbType.Int32);
                parameters.Add("@PageIndex", pageIndex, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@SearchText", searchText, DbType.String);
                parameters.Add("@FromDate", fromDate, DbType.String);
                parameters.Add("@ToDate", toDate, DbType.String);
                var result = await conn.QueryMultipleAsync("School.GetGroupRecordedSessions", parameters, commandType: CommandType.StoredProcedure);
                var recordingList = await result.ReadAsync<RecordedSessionView>();
                var recordingCount = await result.ReadFirstAsync<int>();
                lst = recordingList.ToList();
                lst.ToList().ForEach(x => x.TotalCount = recordingCount);
                return lst;
            }
        }

        public async Task<IEnumerable<ZoomMeetingView>> GetAllLiveSessions(long userId, int pageIndex, int pageSize, string searchText, string type, string groupId)
        {
            IEnumerable<ZoomMeetingView> lst = new List<ZoomMeetingView>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@PageIndex", pageIndex, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@Type", type, DbType.String);
                parameters.Add("@GroupId", string.IsNullOrEmpty(groupId) ? 0 : Convert.ToInt64(groupId), DbType.Int64); ;
                parameters.Add("@SearchText", searchText, DbType.String);
                var result = await conn.QueryMultipleAsync("School.GetAllLiveSessions", parameters, commandType: CommandType.StoredProcedure);
                var sessionList = await result.ReadAsync<ZoomMeetingView>();
                var sessionsCount = await result.ReadFirstAsync<int>();
                lst = sessionList.ToList();
                lst.ToList().ForEach(x => x.TotalCount = sessionsCount);
                return lst;
            }
        }

        public async Task<bool> SaveUserMeetingJoinURL(ZoomMeetingResponse model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@MeetingURL", model.join_url, DbType.String);
                parameters.Add("@MeetingId", model.id, DbType.String);
                parameters.Add("@MemberId", model.UserId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("School.SaveUserSynchronousMeetingURL", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<GroupMemberMapping>> GetOnlineEventParticipants(string onlineMeetingId, string selectedMembers)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SelectedUsers", selectedMembers, DbType.String);
                parameters.Add("@OnlineMeetingId", onlineMeetingId, DbType.String);
                return await conn.QueryAsync<GroupMemberMapping>("School.GetEventParticipantsInfo", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<bool> DeleteOnlineMeetingEvent(string onlineMeetingId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@MeetingId", onlineMeetingId, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("School.DeleteOnlineMeetingEvent", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<bool> DeleteLiveSession(ZoomMeetingView model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@MeetingId", model.ZoomMeetingId, DbType.String);
                parameters.Add("@UserId", model.CreatedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("School.DeleteLiveSession", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<bool> DeleteTeamsSession(MSTeamsReponse model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolTeamMeetingId", model.schoolTeamMeetingId, DbType.Int64);
                parameters.Add("@UserId", model.CreatedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("School.DeleteTeamsSession", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public bool InsertGroupPrimaryTeacher(GroupPrimaryTeacher model)
        {
            using(var conn = GetOpenConnection())
            {
                var parameters = GetGroupPrimaryTeacherParameters(model, TransactionModes.Insert);

                conn.Query<int>("School.GroupPrimaryTeacherMappingCUD", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("Output");
                return result > 0;
            }
        }

        public bool DeleteGroupPrimaryTeacher(long id)
        {
            using (var conn = GetOpenConnection())
            {
                var model = new GroupPrimaryTeacher(id);
                var parameters = GetGroupPrimaryTeacherParameters(model, TransactionModes.Delete);

                conn.Query<int>("School.GroupPrimaryTeacherMappingCUD", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("Output");
                return result > 0;
            }
        }

        private DynamicParameters GetGroupPrimaryTeacherParameters(GroupPrimaryTeacher entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@GroupPrimaryTeacherMappingId", entity.GroupPrimaryTeacherMappingId, DbType.Int64);
            parameters.Add("@GroupId", entity.GroupId, DbType.Int64);
            parameters.Add("@TeacherId", entity.TeacherId, DbType.Int64);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        public bool UpdateSchoolGroupsToHideClassGroups(SchoolGroup model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupIds", model.ClassGroupIdsToHide, DbType.String);
                parameters.Add("@UpdatedBy", model.CreatedById, DbType.Int64);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.UpdateSchoolGroupsToHide", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("Output");
                return result > 0;
            }
        }
    }
}
