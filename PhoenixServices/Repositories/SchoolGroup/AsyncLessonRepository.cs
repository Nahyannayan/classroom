﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class AsyncLessonRepository : SqlRepository<AsyncLesson>, IAsyncLessonRepository
    {
        private readonly IConfiguration _config;
        public AsyncLessonRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        #region Async Lesson 
        public override void DeleteAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public override Task<IEnumerable<AsyncLesson>> GetAllAsync()
        {
            throw new System.NotImplementedException();
        }

        public override Task<AsyncLesson> GetAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public override void InsertAsync(AsyncLesson entity)
        {
            throw new System.NotImplementedException();
        }

        public override void UpdateAsync(AsyncLesson entityToUpdate)
        {
            throw new System.NotImplementedException();
        }
    

        public int InsertAsyncLesson(AsyncLesson entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAsyncLessonParameters(entity, TransactionModes.Insert);

                conn.Query<int>("School.AsyncLessonCUD", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("output");
                return result;

            }
        }

        public int UpdateAsyncLesson(AsyncLesson entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAsyncLessonParameters(entity, TransactionModes.Update);

                conn.Query<int>("School.AsyncLessonCUD", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("output");
                return result;

            }
        }

        public int DeleteAsyncLesson(long id, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var entity = new AsyncLesson();
                entity.AsyncLessonId = id;
                entity.UpdatedBy = userId;
                entity.StartDate = DateTime.Today;
                entity.EndDate = DateTime.Today;
                var parameters = GetAsyncLessonParameters(entity, TransactionModes.Delete);
                conn.Query<int>("School.AsyncLessonCUD", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("output");
                return result;

            }
        }

        public async Task<ViewAsyncLessonDetail> GetAsyncLessons(long? asyncLessonId, long? sectionId, long? moduleId, long? folderId, bool? isActive, int? IsIncludeResources, int? IsIncludeStudents)
        {
            var model = new ViewAsyncLessonDetail();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AsyncLessonId", asyncLessonId, DbType.Int64);
                parameters.Add("@SectionId", sectionId, DbType.Int64);
                parameters.Add("@ModuleId", moduleId, DbType.Int64);
                parameters.Add("@FolderId", folderId, DbType.Int64);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                parameters.Add("@IncludeResources", IsIncludeResources, DbType.Int32);
                parameters.Add("@ListStudents", IsIncludeStudents, DbType.Int32);
                //return await conn.QueryAsync<AsyncLesson>("School.GetAsyncLesson", parameters, commandType: CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("School.GetAsyncLesson", parameters, commandType: CommandType.StoredProcedure);
                model.AsyncLessonId = asyncLessonId.HasValue ? asyncLessonId.Value : 0;
                model.AsyncLesson = result.ReadFirstOrDefault<AsyncLesson>();
                if(IsIncludeResources.HasValue && IsIncludeResources.Value == 1)
                {
                    model.ResourcesList.AddRange(result.Read<AsyncLessonResourcesActivities>());
                }
                if (IsIncludeStudents.HasValue && IsIncludeStudents.Value == 1)
                {
                    model.StudentList.AddRange(result.Read<AsyncLessonStudentMapping>());
                }

                return model;
            }
        }

        public async Task<AsyncLesson> GetAsyncLessonById(long asynLessonId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AsyncLessonId", asynLessonId, DbType.Int64);
                parameters.Add("@SectionId", DBNull.Value, DbType.Int64);
                parameters.Add("@ModuleId", DBNull.Value, DbType.Int64);
                parameters.Add("@FolderId", DBNull.Value, DbType.Int64);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                parameters.Add("@IncludeResources", 0, DbType.Int32);
                parameters.Add("@ListStudents", 0, DbType.Int32);
                var result = await conn.QueryMultipleAsync("School.GetAsyncLesson", parameters, commandType: CommandType.StoredProcedure);

                return result.ReadFirstOrDefault<AsyncLesson>();
            }
        }

        private DynamicParameters GetAsyncLessonParameters(AsyncLesson entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int64);
            parameters.Add("@AsyncLessonId", entity.AsyncLessonId, DbType.Int64);
            parameters.Add("@LessonTitle", entity.LessonTitle, DbType.String);
            parameters.Add("@Description", entity.Description, DbType.String);
            parameters.Add("@CourseId", entity.CourseId, DbType.Int64);
            parameters.Add("@PlanSchemeId", entity.PlanSchemeId, DbType.Int64);
            parameters.Add("@Students", entity.Students, DbType.String);
            parameters.Add("@UnitId", entity.UnitId, DbType.Int64);
            parameters.Add("@StartDate", entity.StartDate, DbType.DateTime);
            parameters.Add("@EndDate", entity.EndDate, DbType.DateTime);
            parameters.Add("@FolderId", entity.FolderId, DbType.Int64);
            parameters.Add("@SectionId", entity.SectionId, DbType.Int64);
            parameters.Add("@ModuleId", entity.ModuleId, DbType.Int64);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@IsPublish", entity.IsPublish, DbType.Boolean);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int32);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        #endregion

        #region Resource Activity
        public int InsertResourceActivity(List<AsyncLessonResourcesActivities> model)
        {
            using (var conn = GetOpenConnection())
            {
                var result = 0; //need to bulk insert in future by using datatable
                foreach (var entity in model)
                {
                    var parameters = GetResourceActivityParameters(entity, TransactionModes.Insert);

                    conn.Query<int>("FMS.AsyncLessonResourceActivityCUD", parameters, commandType: CommandType.StoredProcedure);
                    result = parameters.Get<int>("output");
                }
                return result;

            }
        }

        public int UpdateResourceActivity(AsyncLessonResourcesActivities entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetResourceActivityParameters(entity, TransactionModes.Update);

                conn.Query<int>("FMS.AsyncLessonResourceActivityCUD", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("output");
                return result;

            }
        }

        public int DeleteResourceActivity(long id, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var entity = new AsyncLessonResourcesActivities();
                entity.ResourceActivityId = id;
                entity.UpdatedBy = userId;

                var parameters = GetResourceActivityParameters(entity, TransactionModes.Delete);
                conn.Query<int>("FMS.AsyncLessonResourceActivityCUD", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("output");
                return result;

            }
        }

        public async Task<IEnumerable<AsyncLessonResourcesActivities>> GetResourceActivity(long asyncLessonId, long userId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AsyncLessonId", asyncLessonId, DbType.Int64);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<AsyncLessonResourcesActivities>("FMS.GetAsyncLessonResourceActivity", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public int UpdateResourceActivityStatus(UpdateAsyncLessonResourcesActivity model)
        {
            using (var conn = GetOpenConnection())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ResourceActivityId", typeof(long));
                dt.Columns.Add("SortOrder", typeof(int));
                foreach (var item in model.ResourcesActivitiesList)
                {
                    dt.Rows.Add(item.ResourceActivityId, item.StepNo);
                }

                var parameters = new DynamicParameters();
                parameters.Add("@ResourceActivityId", model.ResourceActivityId, DbType.Int64);
                parameters.Add("@Status", model.Status, DbType.Int32);
                parameters.Add("@UpdatedBy", model.updatedBy, DbType.Int64);
                parameters.Add("@IsStatusUpdate", model.IsStatusUpdate, DbType.Boolean);
                parameters.Add("@ResourcesSortOrder", dt, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("FMS.UpdateAsyncLessonResourceActivityStatus", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("output");
                return result;
            }
        }

        private DynamicParameters GetResourceActivityParameters(AsyncLessonResourcesActivities entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int64);
            parameters.Add("@ResourceActivityId", entity.ResourceActivityId, DbType.Int64);
            parameters.Add("@AsyncLessonId", entity.AsyncLessonId, DbType.Int64);
            parameters.Add("@ModuleId", entity.ModuleId, DbType.Int32);
            parameters.Add("@SectionId", entity.SectionId, DbType.Int64);
            parameters.Add("@FolderId", entity.FolderId, DbType.Int64);
            parameters.Add("@StepNo", entity.StepNo, DbType.Int32);
            parameters.Add("@Name", entity.Name, DbType.String);
            parameters.Add("@Path", entity.Path, DbType.String);
            parameters.Add("@FileTypeId", entity.FileTypeId, DbType.Int32);
            parameters.Add("@Status", entity.Status, DbType.Int32);
            parameters.Add("@IsActive", entity.IsActive, DbType.String);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int32);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int32);
            parameters.Add("@FileSizeInMB", entity.FileSizeInMB, DbType.Decimal);
            parameters.Add("@GoogleDriveFileId", entity.GoogleDriveFileId, DbType.String);
            parameters.Add("@ResourceFileTypeId", entity.ResourceFileTypeId, DbType.Int32);
            parameters.Add("@PhysicalPath", entity.PhysicalPath, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        #region Student Mapping
        public long UpdateStudentMapping(AsyncLessonStudentMapping entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AsyncLessonStudentMappingId", entity.AsyncLessonStudentMappingId, DbType.Int64);
                parameters.Add("@Status", entity.Status, DbType.Int32);
                parameters.Add("@StepNo", entity.StepNo, DbType.Int32);
                parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.UpdateAsyncLessonStudentMapping", parameters, commandType: CommandType.StoredProcedure);
                return entity.AsyncLessonStudentMappingId;
            }
        }

        public async Task<IEnumerable<AsyncLessonStudentMapping>> GetAsyncLessonStudentMappingByAsyncLessonId(long asyncLessonId,long? userId, bool? isActive)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AsyncLessonId", asyncLessonId, DbType.Int64);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@IsActive", isActive, DbType.Boolean);
                return await conn.QueryAsync<AsyncLessonStudentMapping>("School.GetAsyncLessonStudentMappingByAsyncLessonId", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        #endregion

        #region Comments
        public int InsertComment(AsyncLessonComments entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAsyncLessonCommentsParameters(entity, TransactionModes.Insert);

                conn.Query<int>("School.AsyncLessonCommentsCUD", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("output");
                return result;

            }
        }

        public int UpdateComment(AsyncLessonComments entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAsyncLessonCommentsParameters(entity, TransactionModes.Update);

                conn.Query<int>("School.AsyncLessonCommentsCUD", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("output");
                return result;

            }
        }

        public int DeleteComment(long id, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var entity = new AsyncLessonComments();
                entity.AsyncLessonCommentId = id;
                entity.UpdatedBy = userId;

                var parameters = GetAsyncLessonCommentsParameters(entity, TransactionModes.Delete);
                conn.Query<int>("School.AsyncLessonCommentsCUD", parameters, commandType: CommandType.StoredProcedure);
                var result = parameters.Get<int>("output");
                return result;

            }
        }

        public async Task<IEnumerable<AsyncLessonComments>> GetAsyncLessonComments(long? asyncLessonCommentId, long? resourceActivityId, long studentId, long asyncLessonId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AsyncLessonCommentId", asyncLessonCommentId, DbType.Int64);
                parameters.Add("@ResourceActivityId", resourceActivityId, DbType.Int64);
                parameters.Add("@StudentId", studentId, DbType.Int64);
                parameters.Add("@AsyncLessonId", asyncLessonId, DbType.Int64);
                return await conn.QueryAsync<AsyncLessonComments>("School.GetAsyncLessonComments", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        private DynamicParameters GetAsyncLessonCommentsParameters(AsyncLessonComments entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int64);
            parameters.Add("@AsyncLessonCommentId", entity.AsyncLessonCommentId, DbType.Int64);
            parameters.Add("@ResourceActiivtyId", entity.ResourceActiivtyId, DbType.Int64);
            parameters.Add("@UserId", entity.UserId, DbType.Int64);
            parameters.Add("@Comment", entity.Comment, DbType.String);
            parameters.Add("@ParentCommentId", entity.ParentCommentId, DbType.Int64);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@IsResolved", entity.IsResolved, DbType.Boolean);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int32);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        
        #endregion
    }
}