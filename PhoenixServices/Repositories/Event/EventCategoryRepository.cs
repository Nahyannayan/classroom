﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class EventCategoryRepository : SqlRepository<EventCategory>, IEventCategoryRepository
    {
        private readonly IConfiguration _config;

        public EventCategoryRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        #region Generated Methods

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<EventCategory>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async override Task<EventCategory> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EventCategoryId", id, DbType.Int32);
                //Need to change the SP
                return await conn.QueryFirstOrDefaultAsync<EventCategory>("[Admin].[GetEventCategoryById]", parameters, commandType: CommandType.StoredProcedure);
            }

        }

        public override void InsertAsync(EventCategory entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(EventCategory entityToUpdate)
        {
            throw new NotImplementedException();
        }

        #endregion

        public async Task<EventCategory> GetEventCategoryById(int id, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EventCategoryId", id, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                //Need to change the SP
                return await conn.QueryFirstOrDefaultAsync<EventCategory>("[Admin].[GetEventCategoryById]", parameters, commandType: CommandType.StoredProcedure);
            }

        }
        public async Task<IEnumerable<EventCategoryView>> GetEventCategory(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Int32);
                return await conn.QueryAsync<EventCategoryView>("Admin.GetEventCategory", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int UpdateEventCategory(EventCategory entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetEventCategoryParameters(entity, mode);
                conn.Query<int>("[Admin].[EventCategoryCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        public async Task<IEnumerable<EventCategoryView>> GetAllEventCategoryBySchool(long schoolId,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<EventCategoryView>("Admin.GetAllEventCategoryBySchool", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<EventCategoryView>> GetEventCategoriesBySchoolId(long schoolId,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<EventCategoryView>("[Admin].[GetEventCategoriesBySchoolId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public bool CheckEventCategoryAvailable(string categoryName, int id, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                bool isCategoryAvailable = false;
                var parameters = new DynamicParameters();
                parameters.Add("@CategoryName", categoryName, DbType.String);
                parameters.Add("@Id", id, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[CheckEventCategoryName]", parameters, commandType: CommandType.StoredProcedure);
                int outputParam = parameters.Get<int>("output");
                if (outputParam == 1)
                    isCategoryAvailable = true;
                return isCategoryAvailable;
            }
        }

        #region Private Methods
        private DynamicParameters GetEventCategoryParameters(EventCategory entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@EventCategoryId", entity.Id, DbType.Int32);
            parameters.Add("@EventCategoryName", entity.Name, DbType.String);
            parameters.Add("@EventCategoryDescription", entity.Description, DbType.String);
            parameters.Add("@ColorCode", entity.ColorCode, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedBy, DbType.Int32);
            parameters.Add("@EventCategoryXML", entity.EventCategoryXml, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

       
        #endregion
    }
}
