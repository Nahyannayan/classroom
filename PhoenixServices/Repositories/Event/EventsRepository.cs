﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;

namespace Phoenix.API.Repositories
{
    public class EventsRepository : SqlRepository<EventView>, IEventsRepository
    {
        private readonly IConfiguration _config;

        public EventsRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }
        public int CancelEvent(int eventId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EventId", eventId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[CancelEventById]", parameters, commandType: CommandType.StoredProcedure);
                
                return parameters.Get<int>("output");
            }
        }

        public bool DeleteEventFile(int eventId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EventId", eventId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[DeleteEventFile]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public int EventCUD(EventView eventView, TransactionModes modes)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EventId", eventView.EventId, DbType.Int32);
                parameters.Add("@TransMode", (int)modes, DbType.Int32);
                parameters.Add("@SchoolId", eventView.SchoolId, DbType.Int64);
                parameters.Add("@UserId", eventView.UserId, DbType.Int64);
                parameters.Add("@StartDate", eventView.StartDate, DbType.DateTime);
                parameters.Add("@EndDate", eventView.EndDate, DbType.DateTime);
                parameters.Add("@StartTime", eventView.StartTime, DbType.String);
                parameters.Add("@EndTime", eventView.EndTime, DbType.String);
                parameters.Add("@DurationId", eventView.DurationId, DbType.Int32);
                parameters.Add("@EventCategoryId", eventView.EventCategoryId, DbType.Int32);
                parameters.Add("@Title", eventView.Title, DbType.String);
                parameters.Add("@Venue", eventView.Venue, DbType.String);
                parameters.Add("@Description", eventView.Description, DbType.String);
                parameters.Add("@EventTypeId", eventView.EventTypeId, DbType.Int32);
                parameters.Add("@ResourceFile", eventView.ResourceFile, DbType.String);
                parameters.Add("@Extension", eventView.Extension, DbType.String);
                parameters.Add("@FileName", eventView.FileName, DbType.String);
                parameters.Add("@EventPriority", eventView.EventPriority, DbType.String);
                parameters.Add("@IsTeacherVisible", eventView.IsTeacherVisible, DbType.Boolean);
                parameters.Add("@EventRepeatTypeId", eventView.EventRepeatTypeId, DbType.Int32);
                parameters.Add("@EventRepeatTimes", eventView.EventRepeatTimes, DbType.String);
                parameters.Add("@SelectedPlannerMemberId", eventView.SelectedPlannerMemberId, DbType.String);
                parameters.Add("@ExternalEventMember", eventView.ExternalEmails, DbType.String);
                parameters.Add("@IsCopyMessage", eventView.IsCopyMessage, DbType.Boolean);
                parameters.Add("@IsActive", eventView.IsActive, DbType.Boolean);
                parameters.Add("@UpdatedBy", eventView.UpdatedBy, DbType.Int64);
                parameters.Add("@MeetingTypeId", eventView.OnlineMeetingType, DbType.Int16);
                parameters.Add("@MeetingMasterId", eventView.MeetingMasterId, DbType.Int64);
                parameters.Add("@PhysicalFilePath", eventView.PhysicalFilePath, DbType.String);
                parameters.Add("@OnlineMeetingId", string.IsNullOrEmpty(eventView.OnlineMeetingId) ? (object)DBNull.Value : eventView.OnlineMeetingId, DbType.String);
                parameters.Add("@IsRecurringEvent", eventView.IsRecurringEvent, DbType.Boolean);
                parameters.Add("@RecurringDays", eventView.RecurringEventDays, DbType.String);
                parameters.Add("@EventPassword", eventView.EventPassword, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.EventsCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        public async Task<IEnumerable<EventCategoryView>> GetEventCategory(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<EventCategoryView>("School.GetEventCategoryBySchoolId", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<EventTypeView>> GetEventType()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<EventTypeView>("School.GetEventType", null, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<EventDurationView>> GetEventDuration()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<EventDurationView>("School.GetEventDuration", null, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<EventView>> GetMothlyEvents(long userId, DateTime fromDate, DateTime toDate, int? categoryId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@StartDate", fromDate, DbType.DateTime);
                parameters.Add("@EndDate", toDate, DbType.DateTime);
                parameters.Add("@CategoryId", categoryId, DbType.Int32);
                return await conn.QueryAsync<EventView>("[School].[GetMonthlyEvents]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<EventView>> GetAllMothlyEvents(long userId, DateTime fromDate, DateTime toDate, string eventCategoryIds)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("EventCategoryId");
            dt.TableName = "tblEventCategoryId";
            if (!string.IsNullOrEmpty(eventCategoryIds))
            {
                List<int> categoryIds = eventCategoryIds.Split(',').Select(int.Parse).ToList();
                foreach (var item in categoryIds)
                {
                    dt.Rows.Add(item);
                }
            }

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@StartDate", fromDate, DbType.DateTime);
                parameters.Add("@EndDate", toDate, DbType.DateTime);
                parameters.Add("@EventCategoryIds", dt, DbType.Object);
                return await conn.QueryAsync<EventView>("[School].[GetAllMonthlyEvents]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<WeeklyEventView>> GetWeeklyTimeTableEvents(long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher, bool isDashboardEvents)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", fromDate, DbType.DateTime);
                parameters.Add("@EndDate", toDate, DbType.DateTime);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                parameters.Add("@IsDashboardEvents", isDashboardEvents, DbType.Boolean);
                return await conn.QueryAsync<WeeklyEventView>("[School].[GetWeeklyTimeTableEvents]", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<WeeklyEventView>> GetTodayOrWeeklyTimeTableEvents(long userId, long schoolId, int fromWeekDay, int toWeekDay, DateTime fromDate, DateTime toDate, bool isTeacher, bool isWeeklyEvent)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@FromWeekDay", fromWeekDay, DbType.Int32);
                parameters.Add("@ToWeekDay", toWeekDay, DbType.Int32);
                parameters.Add("@StartDate", fromDate, DbType.DateTime);
                parameters.Add("@EndDate", toDate, DbType.DateTime);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                parameters.Add("@IsWeeklyEvent", isWeeklyEvent, DbType.Boolean);
                return await conn.QueryAsync<WeeklyEventView>("[School].[GetTodayOrWeeklyTimeTableEvents]", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public async Task<PlannerTimetableView> GetPlannerTimetableData(long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            PlannerTimetableView plannerTimetable = new PlannerTimetableView();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", fromDate, DbType.DateTime);
                parameters.Add("@EndDate", toDate, DbType.DateTime);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);

                var result = await conn.QueryMultipleAsync("[School].[GetPlannerTimetableData]", parameters, commandType: CommandType.StoredProcedure);
                plannerTimetable.PlannerEventData = await result.ReadAsync<EventView>();
                plannerTimetable.WeeklyEventData = await result.ReadAsync<WeeklyEventView>();

                return plannerTimetable;
            }
        }

        public async Task<PlannerTimetableView> GetPlannerTimetableDataWithPaging(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            PlannerTimetableView plannerTimetable = new PlannerTimetableView();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", fromDate, DbType.DateTime);
                parameters.Add("@EndDate", toDate, DbType.DateTime);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);

                var result = await conn.QueryMultipleAsync("[School].[GetPlannerTimetableDataWithPaging]", parameters, null, null, CommandType.StoredProcedure);
                plannerTimetable.PlannerEventData = await result.ReadAsync<EventView>();
                plannerTimetable.WeeklyEventData = await result.ReadAsync<WeeklyEventView>();

                return plannerTimetable;
            }
        }

        public async Task<IEnumerable<WeeklyEventView>> GetTimetableDataWithPaging(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", fromDate, DbType.DateTime);
                parameters.Add("@EndDate", toDate, DbType.DateTime);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);

                var result = await conn.QueryMultipleAsync("[School].[GetTimetableDataWithPaging]", parameters, null, null, CommandType.StoredProcedure);
                var eventList = await result.ReadAsync<WeeklyEventView>();

                return eventList;
            }
        }

        public async Task<IEnumerable<EventView>> GetOnlineMeetingEvents(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isOnlineMeetingEvents, short languageId)
        {
            var list = new List<EventView>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", fromDate, DbType.DateTime);
                parameters.Add("@EndDate", toDate, DbType.DateTime);
                parameters.Add("@IsOnlineMeetingEvents", isOnlineMeetingEvents, DbType.Boolean);
                parameters.Add("@languageId", languageId, DbType.Int16);

                var result = await conn.QueryMultipleAsync("[School].[GetOnlineMeetingEvents]", parameters, null, null, CommandType.StoredProcedure);
                var events = await result.ReadAsync<EventView>();
                var eventsCount = await result.ReadAsync<int>();
                list = events.ToList();
                int totalCount = eventsCount.FirstOrDefault();
                if (list.Any())
                {
                    list.ForEach(x => { x.TotalCount = totalCount; });
                }
                return list;
            }
        }

        public async Task<IEnumerable<WeeklyEventView>> GetSchoolTimetableEvents(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            var weeklylist = new List<WeeklyEventView>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", fromDate, DbType.DateTime);
                parameters.Add("@EndDate", toDate, DbType.DateTime);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);

                var result = await conn.QueryMultipleAsync("[School].[GetTimetableEvents]", parameters, null, null, CommandType.StoredProcedure);
                var events = await result.ReadAsync<WeeklyEventView>();
                var eventsCount = await result.ReadAsync<int>();
                weeklylist = events.ToList();
                int totalCount = eventsCount.FirstOrDefault();
                if (weeklylist.Any())
                {
                    weeklylist.ForEach(x => { x.TotalCount = totalCount; });
                }
                return weeklylist;
            }
        }

        public async Task<IEnumerable<WeeklyEventView>> GetTimetableEventsReport(TimetableReportsRequest model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", model.PageIndex, DbType.Int32);
                parameters.Add("@PageSize", model.PageSize, DbType.Int32);
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@SchoolId", model.SchoolId, DbType.Int64);
                parameters.Add("@TeacherIds", model.TeacherIds, DbType.String);
                parameters.Add("@GroupIds", model.GroupIds, DbType.String);
                parameters.Add("@StartDate", model.StartDate, DbType.DateTime);
                parameters.Add("@EndDate", model.EndDate, DbType.DateTime);
                parameters.Add("@IsTeacher", model.IsTeacher, DbType.Boolean);

                var result = await conn.QueryMultipleAsync("[School].[GetTimetableEventsReport]", parameters, null, null, CommandType.StoredProcedure);
                var eventsList = await result.ReadAsync<WeeklyEventView>();

                return eventsList;
            }
        }

        public async Task<IEnumerable<EventView>> GetLiveSessionsReport(TimetableReportsRequest model)
        {

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", model.PageIndex, DbType.Int32);
                parameters.Add("@PageSize", model.PageSize, DbType.Int32);
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@SchoolId", model.SchoolId, DbType.Int64);
                parameters.Add("@TeacherIds", model.TeacherIds, DbType.String);
                parameters.Add("@GroupIds", model.GroupIds, DbType.String);
                parameters.Add("@StartDate", model.StartDate, DbType.DateTime);
                parameters.Add("@EndDate", model.EndDate, DbType.DateTime);
                parameters.Add("@IsOnlineMeetingEvents", model.IsOnlineMeetingEvents, DbType.Boolean);

                var result = await conn.QueryMultipleAsync("[School].[GetOnlineMeetingEventsReport]", parameters, null, null, CommandType.StoredProcedure);
                var eventsList = await result.ReadAsync<EventView>();

                return eventsList;
            }
        }

        public bool AcceptEventRequest(int eventId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@EventId", eventId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.AcceptEventRequest", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public bool AcceptEventRequestExternalUser(int eventId, string emailId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EventId", eventId, DbType.Int32);
                parameters.Add("@EmailId", emailId, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[AcceptExternalRequest]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public override Task<IEnumerable<EventView>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<EventView> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<EventView>> GetEvent(int id, long? userId, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EventId", id, DbType.Int32);
                parameters.Add("@UserId", userId.HasValue ? userId.Value : (object)DBNull.Value, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<EventView>("School.GetEvent", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<EventUser> GetEventUserDetails(int eventId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EventId", eventId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<EventUser>("[School].[GetEventUserDetails]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<EventUser>> GetInternalEventUser(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EventId", id, DbType.Int32);
                return await conn.QueryAsync<EventUser>("School.GetInternalEventUser", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<EventUser>> GetExternalEventUser(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EventId", id, DbType.Int32);
                return await conn.QueryAsync<EventUser>("School.GetExternalEventUser", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int CheckTimetableEventExists(string title, string meetingPassword, DateTime startDate, string startTime, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Title", title, DbType.String);
                parameters.Add("@MeetingPassword", meetingPassword, DbType.String);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@StartTime", startTime, DbType.String);
                parameters.Add("@CreatedBy", userId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[CheckTimetableEventExists]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        public override void InsertAsync(EventView entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(EventView entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
