﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Models;

namespace Phoenix.API.Repositories
{
    public class DurationRepository : SqlRepository<DurationView>, IDurationRepository
    {
        private readonly IConfiguration _config;

        public DurationRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<DurationView>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<DurationView> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(DurationView entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(DurationView entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDuration(DurationView durationView)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", durationView.Id, DbType.Int32);
                parameters.Add("@Duration", durationView.Duration, DbType.String);
                parameters.Add("@UpdatedBy", durationView.UpdatedBy, DbType.Int32);
                parameters.Add("@TransMode", durationView.TransactionMode, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Admin.DurationCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<DurationView>> GetDuration(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, DbType.Int32);
                return await conn.QueryAsync<DurationView>("Admin.GetDuration", parameters, null, null, CommandType.StoredProcedure);
            }
        }

    }
}
