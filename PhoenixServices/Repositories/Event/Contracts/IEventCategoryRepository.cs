﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IEventCategoryRepository : IGenericRepository<EventCategory>
    {
        Task<IEnumerable<EventCategoryView>> GetEventCategory(int id);
        Task<IEnumerable<EventCategoryView>> GetAllEventCategoryBySchool(long schoolId,short languageId);
        Task<IEnumerable<EventCategoryView>> GetEventCategoriesBySchoolId(long schoolId, short languageId);
        int UpdateEventCategory(EventCategory entity, TransactionModes mode);
        bool CheckEventCategoryAvailable(string categoryName, int id, long schoolId);
        Task<EventCategory> GetEventCategoryById(int id, short languageId);
    }
}
