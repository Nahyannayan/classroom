﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IDurationRepository
    {
        Task<IEnumerable<DurationView>> GetDuration(int id);
        bool UpdateDuration(DurationView durationView);
    }
}
