﻿using DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.API.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public class AttendanceSettingRepository : SqlRepository<GradeDetails>, IAttendanceSettingRepository
    {
        private readonly IConfiguration _config;

        public AttendanceSettingRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        #region Built In Function
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override Task<IEnumerable<GradeDetails>> GetAllAsync()
        {
            throw new NotImplementedException();
        }
        public override Task<GradeDetails> GetAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override void InsertAsync(GradeDetails entity)
        {
            throw new NotImplementedException();
        }
        public override void UpdateAsync(GradeDetails entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion

        public async Task<IEnumerable<GradeDetails>> GetGradeDetails()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<GradeDetails>("SIMS.AttendanceSettings_GetGradeDetails", null, null, null, CommandType.StoredProcedure);
            }
        }

        #region Attendance Calendar
        public async Task<IEnumerable<GradeAndSection>> GetGradeAndSectionList(long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                return await conn.QueryAsync<GradeAndSection>("[Attendance].[GetGradeAndSectionList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<SchoolWeekEnd> GetSchoolWeekEnd(long BSU_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSU_ID", BSU_ID, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<SchoolWeekEnd>("SIMS.AttendanceSettings_GetSchoolWeekEnd", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<int> SaveCalendarEvent(AttendanceCalendar attendanceCalendar, string DATAMODE)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SCH_ID", attendanceCalendar.SCH_ID, DbType.Int64);
                parameters.Add("@SchoolId", attendanceCalendar.SchoolId, DbType.Int64);
                parameters.Add("@SCH_DTFROM", attendanceCalendar.SCH_DTFROM, DbType.DateTime);
                parameters.Add("@SCH_DTTO", attendanceCalendar.SCH_DTTO, DbType.DateTime);
                parameters.Add("@SCH_REMARKS", attendanceCalendar.SCH_REMARKS, DbType.String);
                parameters.Add("@SCH_TYPE", attendanceCalendar.SCH_TYPE, DbType.String);
                parameters.Add("@Att_GradeSectionDT", attendanceCalendar.selectedSectionListDT, DbType.Object);
                parameters.Add("@DATAMODE", DATAMODE, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("[Attendance].[SaveCalendarDetail]", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<IEnumerable<AttendanceCalendar>> GetCalendarDetail(long SchoolId, long SCH_ID, bool IsListView)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                parameters.Add("@SCH_ID", SCH_ID, DbType.Int64);
                parameters.Add("@IsListView", IsListView, DbType.Boolean);
                return await conn.QueryAsync<AttendanceCalendar>("[Attendance].[GetCalendarDetail]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<AcademicYearDetail> GetAcademicYearDetail(long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<AcademicYearDetail>("[Attendance].[GetAcademicYearDetail]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        #endregion Attendance Calendar

        #region Parameter Setting
        public async Task<int> SaveParameterSetting(ParameterSetting parameterSetting, string DATAMODE)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ParamaterId", parameterSetting.ParamaterId, DbType.Int64);
                parameters.Add("@SchoolId", parameterSetting.SchoolId, DbType.Int64);
                parameters.Add("@ParameterTypeId", parameterSetting.ParameterTypeId, DbType.Int64);
                parameters.Add("@ParameterDesc", parameterSetting.ParameterDesc, DbType.String);
                parameters.Add("@ReportParameterDesc", parameterSetting.ReportParameterDesc, DbType.String);
                parameters.Add("@ParameterDisplayCode", parameterSetting.ParameterDisplayCode, DbType.String);
                parameters.Add("@ParameterDisplayOrder", parameterSetting.ParameterDisplayOrder, DbType.String);
                parameters.Add("@DATAMODE", DATAMODE, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("[Attendance].[AttendanceSettings_SaveParameterSetting]", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<IEnumerable<ParameterSetting>> GetParameterSettingList(long BSU_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", BSU_ID, DbType.Int32);
                return await conn.QueryAsync<ParameterSetting>("[Attendance].[AttendanceSettings_GetParameterSettingList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        #endregion

        #region Leave approval permission
        public async Task<IEnumerable<LeaveApprovalPermissionModel>> GetLeaveApprovalPermission(long AcdId, long schoolId, int divisionId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ACD_ID", AcdId);
                parameters.Add("@BSU_ID", schoolId);
                parameters.Add("@DivisionId", divisionId);

                return await conn.QueryAsync<LeaveApprovalPermissionModel>("SIMS.AttendanceSettings_GetLeaveApprovalPermissionByAcdId", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<int> LeaveApprovalPermissionCU(LeaveApprovalPermissionModel leaveApproval)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SAL_ID", leaveApproval.SalId);
                parameters.Add("@SAL_ACD_ID", leaveApproval.SAL_ACD_ID);
                parameters.Add("@SAL_GRD_ID", leaveApproval.GradeId);
                parameters.Add("@SAL_BSU_ID", leaveApproval.SchoolId);
                parameters.Add("@SAL_EMP_ID", leaveApproval.EmployeeId);
                parameters.Add("@SAL_FROMDT", leaveApproval.FromDate);
                parameters.Add("@SAL_TODT", leaveApproval.ToDate);
                parameters.Add("@SAL_NDAYS", leaveApproval.SAL_NDAYS);
                parameters.Add("@SAL_DivisionId", leaveApproval.DivisionId);
                parameters.Add("@bEdit", leaveApproval.SalId > 0);
                return await conn.ExecuteAsync("SIMS.LeaveApprovalPermissionCU", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        #endregion

        #region Attendance Type
        public async Task<IEnumerable<AttendanceType>> GetAttendanceType(long BSU_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", BSU_ID, DbType.Int32);
                return await conn.QueryAsync<AttendanceType>("[Attendance].[Getattendancetypelist]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<int> SaveAttendanceType(AttendanceType AttendanceType, string DATAMODE)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id", typeof(long));
                dt.Columns.Add("PeriodNumber", typeof(int));
                dt.Columns.Add("Weightage", typeof(decimal));
                 dt.Columns.Add("Remark", typeof(string));
                dt.Columns.Add("GradeId", typeof(string));
                foreach (var item in AttendanceType.AttendacePeriodSettingList)
                {
                    dt.Rows.Add(item.AttendancePeriodId, item.PeriodNo, item.Weightage, item.Comment, item.PeriodGradeId);
                }
                parameters.Add("@tblAttendancePeriod", dt, DbType.Object);

                parameters.Add("@AttendanceSessionConfigurationId", AttendanceType.AttendanceSessionConfigurationId, DbType.Int64);
                parameters.Add("@AttendanceConfigurationTypeID", AttendanceType.AttendanceConfigurationTypeID, DbType.Int64);

                parameters.Add("@ConfigurationTypeId", AttendanceType.AttendanceSelectType, DbType.Int64);

                parameters.Add("@ASC_SchoolGradeId", AttendanceType.GradeId, DbType.Int64);
                parameters.Add("@ASC_SessionTypeId", AttendanceType.AttendanceTypeSession, DbType.Int64);
                parameters.Add("@ASC_StartDate", AttendanceType.StartDate, DbType.DateTime);
                parameters.Add("@ASC_EndDate", AttendanceType.EndDate, DbType.DateTime);
                parameters.Add("@schoolId", AttendanceType.SchoolId, DbType.String);
                parameters.Add("@DataMode", DATAMODE, DbType.String);
                
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("[Attendance].[Attendancesetting_attendanceperiodcud]", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<IEnumerable<AttendacePeriodModel>> GetAttendancePeriodList(long gradeId)
        {

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GradeId", gradeId);
                return await conn.QueryAsync<AttendacePeriodModel>("[Attendance].[GetAttendancePeriodList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<AttendanceType>> GetAttendanceTypeListBYId(long AttendanceConfigurationTypeID)
        {

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GradeId", AttendanceConfigurationTypeID);
                return await conn.QueryAsync<AttendanceType>("[Attendance].[GetAttendancePeriodList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public bool AddUpdateAttendancePeriod(long schoolId, long academicId, string gradeId, long CreatedBy, List<AttendacePeriodModel> objAttendancePeriod)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(long));
            dt.Columns.Add("SchoolId", typeof(long));
            dt.Columns.Add("AcademicId", typeof(long));
            dt.Columns.Add("PeriodNumber", typeof(int));
            dt.Columns.Add("Weightage", typeof(decimal));
            dt.Columns.Add("Remark", typeof(string));
            dt.Columns.Add("GradeId", typeof(string));
            foreach (var item in objAttendancePeriod)
            {
                dt.Rows.Add(item.AttendancePeriodId, schoolId, academicId, item.PeriodNo, item.Weightage, item.Comment, gradeId);
            }
            var parameters = new DynamicParameters();
            parameters.Add("@tblAttendancePeriod", dt, DbType.Object);
            parameters.Add("@CreatedBy", CreatedBy);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("SIMS.AttendanceSetting_AttendancePeriodCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        #endregion     
    }
}
