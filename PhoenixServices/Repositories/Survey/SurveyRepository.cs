﻿using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class SurveyRepository : SqlRepository<SurveyModel>, ISurveyRepository
    {
        private readonly IConfiguration _config;
        public SurveyRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        #region Built In Functions

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<SurveyModel>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<SurveyModel> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(SurveyModel entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SurveyModel entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion Built In Functions
    }
}
