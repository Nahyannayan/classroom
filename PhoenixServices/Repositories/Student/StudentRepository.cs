﻿using DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.API.Services;

namespace Phoenix.API.Repositories
{
    public class StudentRepository : SqlRepository<StudentDTO>, IStudentRepository
    {
        private readonly IConfiguration _config;

        public StudentRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        /// <summary>
        /// Author: Deepak Singh
        /// CreatedAt: 17-April-2019
        /// To Delete Student by Id
        /// </summary>
        /// <param name="id"></param>
        public override async void DeleteAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var sql = "DELETE FROM Student WHERE Id = @Id";
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, System.Data.DbType.Int32);
                await conn.QueryFirstOrDefaultAsync<StudentDTO>(sql, parameters);
            }
        }

        /// <summary>
        /// Author: Deepak Singh
        /// CreatedAt: 17-April-2019
        /// Get all students
        /// </summary>
        /// <returns></returns>
        public override async Task<IEnumerable<StudentDTO>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                //deependra
                var sql = "SELECT SI.*,SD.*,SD.StudentName as UserName FROM Student.StudentInfo SI inner join [Student].StudentDetails SD on SI.StudentId = SD.StudentId";
                return await conn.QueryAsync<StudentDTO>(sql);
            }
        }


        /// <summary>
        /// Author: Deepak Singh
        /// CreatedAt: 17-April-2019
        /// Get student by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task<StudentDTO> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var sql = "SELECT * FROM Student WHERE Id = @Id";
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, System.Data.DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<StudentDTO>(sql, parameters);
            }
        }


        /// <summary>
        /// Author: Deepak Singh
        /// CreatedAt: 17-April-2019
        /// Add student
        /// </summary>
        /// <param name="entity">student object to add</param>
        public override async void InsertAsync(StudentDTO entity)
        {
            using (var conn = GetOpenConnection())
            {
                var sql = "INSERT INTO Student(FirstName, LastName, Standard) "
                    + "VALUES(@FirstName, @LastName,@Standard)";

                var parameters = new DynamicParameters();
                parameters.Add("@FirstName", entity.FirstName, System.Data.DbType.String);
                parameters.Add("@LastName", entity.LastName, System.Data.DbType.String);
                parameters.Add("@Standard", entity.Standard, System.Data.DbType.String);

                await conn.QueryAsync(sql, parameters);
            }
        }


        /// <summary>
        /// Author: Deepak Singh
        /// CreatedAt: 17-April-2019
        /// Update student by Id
        /// </summary>
        /// <param name="entityToUpdate">student object to update</param>
        public override async void UpdateAsync(StudentDTO entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var existingEntity = await GetAsync(entityToUpdate.Id);

                var sql = "UPDATE Student "
                    + "SET ";

                var parameters = new DynamicParameters();
                if (existingEntity.FirstName != entityToUpdate.FirstName)
                {
                    sql += "FirstName=@FirstName, LastName = @LastName, Standard=@Standard";
                    parameters.Add("@FirstName", entityToUpdate.FirstName, DbType.String);
                    parameters.Add("@LastName", entityToUpdate.FirstName, DbType.String);
                    parameters.Add("@Standard", entityToUpdate.FirstName, DbType.String);
                }

                sql = sql.TrimEnd(',');

                sql += " WHERE Id=@Id";
                parameters.Add("@Id", entityToUpdate.Id, DbType.Int32);

                await conn.QueryAsync(sql, parameters);
            }
        }

        public async Task<StudentDetail> GetStudentByUserId(long id)
        {
            using (var conn = GetOpenConnection())
            {
                var sql = "dbo.GetStudentByUserId";
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, System.Data.DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<StudentDetail>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }
        /// <summary>
        /// Author: Girish Sonawane
        /// CreatedAt: 04-July-2019
        /// Get student list which is child of provided parent id 
        /// </summary>
        public async Task<IEnumerable<StudentDetail>> GetStudentByFamily(long id)
        {
            using (var conn = GetOpenConnection())
            {
                var sql = "School.GetStudentByFamily";
                var parameters = new DynamicParameters();
                parameters.Add("@Id", id, System.Data.DbType.Int64);
                return await conn.QueryAsync<StudentDetail>(sql, parameters, commandType: CommandType.StoredProcedure);
                // return await conn.QueryFirstOrDefaultAsync<IEnumerable<StudentDetail>>(sql, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<StudentDashboard> GetStudentDashboard(long id)
        {
            StudentDashboard objStudent = new StudentDashboard();

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int64);
                // return await conn.QueryFirstOrDefaultAsync<TeacherDashboard>("School.TeacherDashoard", parameters, null, null, CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("School.StudentDashboard", parameters, null, null, CommandType.StoredProcedure);
                var badges = await result.ReadAsync<SchoolBadge>();
                objStudent.Badges = badges.ToList();
                return objStudent;
            }
        }

        public async Task<StudentPortfolio> GetStudentPortfolioInformation(int userId,short languageId)
        {
            StudentPortfolio studentDetails = new StudentPortfolio();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                var result = await conn.QueryMultipleAsync("Student.GetStudentPortfolioInformation", parameters, null, null, CommandType.StoredProcedure);
                var studentInfo = await result.ReadAsync<Student>();
                var skillSets = await result.ReadAsync<StudentSkills>();
                var certificates = await result.ReadAsync<StudentCertificate>();
                var academicDetails = await result.ReadAsync<AcademicDocuments>();
                var studentGoals = await result.ReadAsync<StudentGoal>();
                var skillsEndorsed = await result.ReadAsync<SkillEndorsement>();
                var dashboardVideos = await result.ReadAsync<string>();
                var studentAcheivements = await result.ReadAsync<StudentIncident>();
                var studentAcheivementsNew = await result.ReadAsync<StudentPortfolioAchievements>();
                var studentBadges = await result.ReadAsync<SchoolBadge>();
                var studentProfileSections = await result.ReadAsync<StudentProfileSections>();
                var studentReports = await result.ReadAsync<StudentAssessmentReportView>();
                studentDetails.StudentDetails = studentInfo.Count() != 0 ? studentInfo.First() : null;
                studentDetails.SkillsList = skillSets.ToList();
                studentDetails.CertificateList = certificates.ToList();
                studentDetails.Academics = academicDetails.ToList();
                studentDetails.StudentGoals = studentGoals.ToList();
                studentDetails.SkillsEndorsed = skillsEndorsed.ToList();
                studentDetails.DashboardVideos = dashboardVideos.ToList();
                studentDetails.StudentAcheivements = studentAcheivements.ToList();
                studentDetails.StudentAcheivementsNew = studentAcheivementsNew.ToList();
                studentDetails.StudentBadges = studentBadges.ToList();
                studentDetails.StudentProfileSections = studentProfileSections.ToList();
                studentDetails.StudentReports = studentReports.ToList();
                return studentDetails;
            }
        }

        public async Task<bool> SaveStudentDescription(Student model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.Id, DbType.Int64);
                parameters.Add("@Description", model.Description, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.SaveStudentDescription", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public async Task<bool> SaveStudentAboutMe(SchoolBadge model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@BadgeIds", model.BadgesIds, DbType.String);
                parameters.Add("@StudentDescription", model.Description, DbType.String);
                parameters.Add("@UpdatedBy", model.AssignedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.SaveStudentAboutMe", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public async Task<StudentProfileDetail> GetStudentProfileDetails(long userId)
        {
            var model = new StudentProfileDetail();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                var result = await conn.QueryMultipleAsync("Student.GetStudentProfileDetails", parameters, null, null, CommandType.StoredProcedure);
                var studentInfo = await result.ReadFirstAsync<Student>();
                var badges = await result.ReadAsync<SchoolBadge>();
                model.StudentDetails = studentInfo;
                model.StudentBadges = badges.ToList();
                return model;
            }
        }

        public async Task<bool> SaveStudentProfileImages(Student model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.Id, DbType.Int64);
                if (model.IsCoverPhoto)
                    parameters.Add("@CoverImagePath", model.StudentImage, DbType.String);
                else
                    parameters.Add("@ProfileImagePath", model.StudentImage, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.SaveStudentProfileImages", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                return result;
            }
        }

        public async Task<bool> UpdateStudentPortfolioSectionDetails(StudentProfileSections model)
        {

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SectionsIds", model.SectionIds, DbType.String);
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.SaveStudentProfileSection", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;

            }
        }

        public int CheckIfTheResourceDeleted(int sourceId, string notificationType)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SourceId", sourceId, DbType.Int32);
                parameters.Add("@NotificationType", notificationType, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[CheckIfTheResourceDeleted]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
    }
}
