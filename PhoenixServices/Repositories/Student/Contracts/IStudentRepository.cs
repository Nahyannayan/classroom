﻿using DbConnection;
using Phoenix.API.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IStudentRepository: IGenericRepository<StudentDTO>
    {
        Task<StudentDetail> GetStudentByUserId(long id);
        Task<IEnumerable<StudentDetail>> GetStudentByFamily(long id);
        Task<StudentDashboard> GetStudentDashboard(long id);
        Task<StudentPortfolio> GetStudentPortfolioInformation(int userId,short languageId);
        Task<bool> SaveStudentDescription(Student model);
        Task<bool> SaveStudentAboutMe(SchoolBadge model);
        Task<StudentProfileDetail> GetStudentProfileDetails(long userId);
        Task<bool> SaveStudentProfileImages(Student model);
        Task<bool> UpdateStudentPortfolioSectionDetails(StudentProfileSections model);
        int CheckIfTheResourceDeleted(int sourceId, string notificationType);
    }
}
