﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class AttendanceRepository : SqlRepository<AttendanceDetails>, IAttendanceRepository
    {
        private readonly IConfiguration _config;

        public AttendanceRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<AttendanceDetails>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<AttendanceDetails> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<AttendanceDetails>> GetAttendanceByIdAndDate(long acd_id, int ttm_id, string username, string entrydate, string grade = null, string section = null, string AttendanceType = "Session1")
        {

            List<AttendanceDetails> Objattendances = new List<AttendanceDetails>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ACD_ID", acd_id, DbType.Int32);
                parameters.Add("@TTM_ID", ttm_id, DbType.Int32);
                parameters.Add("@GRD_ID", grade, DbType.String);
                parameters.Add("@SCT_ID", section, DbType.String);
                parameters.Add("@USR_NAME", username, DbType.String);
                parameters.Add("@DTE", entrydate, DbType.String);
                parameters.Add("@AttendanceType", AttendanceType, DbType.String);
                var result = await conn.QueryMultipleAsync("SIMS.GET_ATTENDANCE_DETS", parameters, null, null, CommandType.StoredProcedure);
                var arrOfSection = section.Split(',');

                foreach (var arr in arrOfSection)
                {
                    var listOfAddendance = await result.ReadAsync<AttendanceDetails>();

                    Objattendances.AddRange(listOfAddendance);
                }

                return Objattendances;
            }
        }
        public async Task<int> InsertAttendanceDetails(string student_xml, string entry_date, string username, int alg_id, int ttm_id = 0, string sct_id = "", string GRD_ID = "", long ACD_ID = 0, long BSU_ID = 0, long SHF_ID = 0, long STM_ID = 0, string AttendanceType = "")
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@STUDENT_XML", student_xml, DbType.String);
                parameters.Add("@ENTRY_DATE", entry_date, DbType.String);
                parameters.Add("@TTM_ID", ttm_id, DbType.Int32);
                parameters.Add("@SCT_ID", sct_id, DbType.String);
                parameters.Add("@USR_NAME", username, DbType.String);
                parameters.Add("@ALG_ID", alg_id, DbType.Int32);
                parameters.Add("@GRD_ID", GRD_ID, DbType.String);
                parameters.Add("@ACD_ID", ACD_ID, DbType.Int32);
                parameters.Add("@BSU_ID", BSU_ID, DbType.Int32);
                parameters.Add("@SHF_ID", SHF_ID, DbType.Int32);
                parameters.Add("@STM_ID", STM_ID, DbType.Int32);
                parameters.Add("@AttendanceType", AttendanceType, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("SIMS.InsertAttendanceDetails", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }


        public async Task<IEnumerable<ATTENDENCE_ANALYSIS>> Get_ATTENDENCE_ANALYSIS(String STU_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@STU_ID", STU_ID, DbType.String);
                return await conn.QueryAsync<ATTENDENCE_ANALYSIS>("SIMS.GET_ATTENDENCE_ANALYSIS", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AttendenceBySession>> Get_AttendenceBySession(String STU_ID, DateTime EndDate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@STU_ID", STU_ID, DbType.String);
                parameters.Add("@END_DT", EndDate, DbType.String);

                return await conn.QueryAsync<AttendenceBySession>("SIMS.GET_AttendenceBySession", parameters, null, null, CommandType.StoredProcedure);
            }
        }


        public async Task<IEnumerable<AttendenceSessionCode>> Get_AttendenceSessionCode(String STU_ID, DateTime EndDate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@STU_ID", STU_ID, DbType.String);
                parameters.Add("@END_DT", EndDate, DbType.String);
                return await conn.QueryAsync<AttendenceSessionCode>("SIMS.GET_ATENDENCESESSIONCODE", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AttendanceChart>> Get_AttendanceChartMain(String STU_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@STU_ID", STU_ID, DbType.String);
                return await conn.QueryAsync<AttendanceChart>("SIMS.GET_ATTENDENCE_BY_ACD_ID", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(AttendanceDetails entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(AttendanceDetails entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<RoomAttendance>> GetRoomAttendanceDetails(long Coursegroupid, string entryDate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();

                parameters.Add("@Coursegroupid", Coursegroupid, DbType.String);

                parameters.Add("@ENTRY_DATE", Convert.ToDateTime(entryDate), DbType.DateTime);
                return await conn.QueryAsync<RoomAttendance>("Attendance.GetRoomAttendanceDetails", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<RoomAttendanceHeader>> GetRoomAttendanceHeader(long SGR_ID, DateTime ENTRY_DATE)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Coursegroupid", SGR_ID, DbType.Int64);
                parameters.Add("@ENTRY_DATE", ENTRY_DATE, DbType.DateTime);

                return await conn.QueryAsync<RoomAttendanceHeader>("[Attendance].[GetRoomAttendanceHeader]", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public bool InsertUpdateRoomAttendance(string SchoolId, string UserId, long acd_id, int isDailyWeekly, long schoolGroupId, long teacherId,  List<StudentRoomAttendance> objStudentRoomAttendance, DateTime entryDate)
        {


            DataTable dtRoomAttendanceDetails = new DataTable();
            dtRoomAttendanceDetails.Columns.Add("Id", typeof(long)); //cA_ID
            dtRoomAttendanceDetails.Columns.Add("StudentId", typeof(long));
            dtRoomAttendanceDetails.Columns.Add("RoomDate", typeof(DateTime));//ATT_DATE
            dtRoomAttendanceDetails.Columns.Add("GroupId", typeof(int)); //GroupID
            dtRoomAttendanceDetails.Columns.Add("UserId", typeof(string));//StaffID
            dtRoomAttendanceDetails.Columns.Add("AcdId", typeof(string));//ACDID
            dtRoomAttendanceDetails.Columns.Add("SchoolId", typeof(string));//SCLID
            dtRoomAttendanceDetails.Columns.Add("PeriodId", typeof(int));//CA_PERIOD 
            dtRoomAttendanceDetails.Columns.Add("StatusId", typeof(string)); //PMID
            dtRoomAttendanceDetails.Columns.Add("Remarks", typeof(string));
            dtRoomAttendanceDetails.Columns.Add("Att_Type", typeof(int));
            //StatusId :-PMID
            //Remarks: Remarks
            foreach (var item in objStudentRoomAttendance)
            {
                dtRoomAttendanceDetails.Rows.Add(item.DetailId, item.StudentId, item.RoomDate, item.SGR_ID, UserId, acd_id, SchoolId, item.PeriodNo, item.Status, item.Remark,0);
            }

            var parameters = new DynamicParameters();
            // parameters.Add("@tblRoomAttendanceLog", dtRoomAttendanceLog, DbType.Object);
            parameters.Add("@tblRoomAttendanceDetails", dtRoomAttendanceDetails, DbType.Object);
            parameters.Add("@isDaillyWeekly", isDailyWeekly, DbType.Int32);
            parameters.Add("@academicyearId", acd_id, DbType.Int64);
            parameters.Add("@schoolGroupId", schoolGroupId, DbType.Int64);
            parameters.Add("@entryDate", entryDate, DbType.DateTime);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("Attendance.RoomAttendanceDetailsCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<StudentRoomAttendance>> GetRoomAttendanceRemarksList(string entryDate, string schoolId, string UserId, int GroupId)
        {

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@entryDate", Convert.ToDateTime(entryDate), DbType.DateTime);
                parameters.Add("@schoolId", schoolId, DbType.String);
                parameters.Add("@UserId", UserId, DbType.String);
                parameters.Add("@GroupId", GroupId, DbType.Int32);
                return await conn.QueryAsync<StudentRoomAttendance>("Attendance.GetRoomAttendanceRemarksList", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<GradeSectionAccess>> GetGradeSectionAccesses(long schoolId, long academicYear, string userName, string IsSuperUser, int gradeAccess, string gradeId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSU_ID", schoolId);
                parameters.Add("@ACD_ID", academicYear);
                parameters.Add("@UserName", userName);
                parameters.Add("@IsSuperUser", IsSuperUser);
                parameters.Add("@GRD_ACCESS", gradeAccess);
                parameters.Add("@GRD_ID", gradeId);
                return await conn.QueryAsync<GradeSectionAccess>("[SIMS].GradeSectionAccessForAttendance", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AttendanceSessionType>> GetAttendanceTypeByEntryDate(int acdId, string schoolId, DateTime AttendanceDt, string GrdId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@acdId", acdId, DbType.Int32);
                parameters.Add("@schoolId", schoolId, DbType.String);
                parameters.Add("@AttendanceDt", AttendanceDt, DbType.DateTime);
                parameters.Add("@GrdId", GrdId, DbType.String);


                return await conn.QueryAsync<AttendanceSessionType>("SIMS.GetAttendanceTypeByEntryDate", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<AuthorizedStaffDetails>> GetGradeSectionByUserId(long Id,long currentAcademicYearId)
        {
            var parameters = new DynamicParameters();
            using (var con = GetOpenConnection())
            {
                parameters.Add("@Id", Id, DbType.Int64);
                parameters.Add("@currentAcademicYearId", currentAcademicYearId, DbType.Int64);

                return await con.QueryAsync<AuthorizedStaffDetails>("Attendance.GetGradeSectionByUserId", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<AttendanceStudent>> GetDailyAttendanceDetails(long gradeId, long sectionId, long sessionTypeId, DateTime asOnDate, long schoolId)
        {
            var parameters = new DynamicParameters();
            using (var con = GetOpenConnection())
            {
                parameters.Add("@GRADEID", gradeId, DbType.Int64);
                parameters.Add("@SECTIONID", sectionId, DbType.Int64);
                parameters.Add("@SESSIONID", sessionTypeId, DbType.Int64);
                parameters.Add("@ASONDATE", asOnDate, DbType.DateTime);
                parameters.Add("@schoolId", schoolId, DbType.Int64);
                return await con.QueryAsync<AttendanceStudent>("Attendance.GetDailyAttendanceDetails", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public bool SaveDailyAttendance(List<AttendanceStudent> objAttendanceStudent, long userId, int IsMobile)
        {
            DataTable dtDailyAttendanceDetails = new DataTable();
            dtDailyAttendanceDetails.Columns.Add("Id", typeof(long));
            dtDailyAttendanceDetails.Columns.Add("StudentId", typeof(long));
            dtDailyAttendanceDetails.Columns.Add("AcademicYearId", typeof(long));
            dtDailyAttendanceDetails.Columns.Add("ParameterId", typeof(long));
            dtDailyAttendanceDetails.Columns.Add("Remark", typeof(string));
            dtDailyAttendanceDetails.Columns.Add("AttendanceDate", typeof(DateTime));
            dtDailyAttendanceDetails.Columns.Add("AttendanceSessionId", typeof(long));
            dtDailyAttendanceDetails.Columns.Add("PhoenixALGID", typeof(long));
            foreach (var item in objAttendanceStudent)
            {
                dtDailyAttendanceDetails.Rows.Add(item.RegisterId, item.StudentId, item.AcademicYearId,
                item.ParameterId, item.Remark, item.AsOnDate,item.SessionId,item.PhoenixALGID);
            }

            var parameters = new DynamicParameters();
            parameters.Add("@tblDailyAttendance", dtDailyAttendanceDetails, DbType.Object);
            parameters.Add("@userId", userId, DbType.Int64);
            parameters.Add("@IsMobile", IsMobile, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("Attendance.DailyAttendanceCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<ClassAttendance>> GetClassAttendanceHeader(long schoolId, long schoolGroupId, DateTime entryDate)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@schoolId", schoolId, DbType.Int64);
            parameters.Add("@schoolGroupId", schoolGroupId, DbType.Int64);
            parameters.Add("@entryDate", entryDate, DbType.DateTime);
           // parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<ClassAttendance>("Attendance.GetClassAttendanceHeader", parameters, null, null, CommandType.StoredProcedure);
            }

        }

        public async Task<IEnumerable<ClassAttendanceDetails>> GetClassAttendanceDetails(long schoolGroupId, DateTime entryDate)
        {
            var parameters = new DynamicParameters();
           
            parameters.Add("@schoolGroupId", schoolGroupId, DbType.Int64);
            parameters.Add("@entryDate", entryDate, DbType.DateTime);
            // parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<ClassAttendanceDetails>("Attendance.GetClassAttendanceDetails", parameters, null, null, CommandType.StoredProcedure);
            }

        }

        public async Task<IEnumerable<AttendanceWeekend>> GetWeekEndBySchoolId(long schoolId, long academicyearId, long schoolgradeId,long courseId, DateTime date, short languageId)
        {
            var parameters = new DynamicParameters();

            parameters.Add("@schoolId", schoolId, DbType.Int64);
            parameters.Add("@academicyearId", academicyearId, DbType.Int64);
            parameters.Add("@schoolgradeId", schoolgradeId, DbType.Int64);
            parameters.Add("@courseId", courseId, DbType.Int64);
            parameters.Add("@date", date, DbType.DateTime);
            parameters.Add("@languageId", languageId, DbType.Int16);
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<AttendanceWeekend>("Attendance.GetWeekEndBySchoolId", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<GradeSessionType>> GetGradeSessionByGradeId(long schoolGradeId)
        {
            var parameters = new DynamicParameters();

            parameters.Add("@schoolGradeId", schoolGradeId, DbType.Int64);
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<GradeSessionType>("Attendance.GetGradeSessionByGradeId", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<StaffCurriculumn>> GetCurriculumByAuthorizedStaffId(long staffId)
        {
            var parameters = new DynamicParameters();

            parameters.Add("@staffId", staffId, DbType.Int64);
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<StaffCurriculumn>("Attendance.GetCurriculumByAuthorizedStaffId", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<TimeTableGroup>> GetGroupsfromTimeTable(long teacherId, DateTime courseDate)
        {
            var parameters = new DynamicParameters();

            parameters.Add("@teacherId", teacherId, DbType.Int64);
            parameters.Add("@courseDate", courseDate, DbType.DateTime);
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<TimeTableGroup>("Attendance.GetGroupsfromTimeTable", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ClassAttendance>> GetPreviousClassAttendanceHeader(long schoolId, long schoolGroupId, DateTime entryDate)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@schoolId", schoolId, DbType.Int64);
            parameters.Add("@schoolGroupId", schoolGroupId, DbType.Int64);
            parameters.Add("@entryDate", entryDate, DbType.DateTime);
            // parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<ClassAttendance>("Attendance.GetPreviousClassAttendanceHeader", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ClassAttendanceDetails>> GetPreviousClassAttendanceDetails(long schoolGroupId, DateTime entryDate)
        {
            var parameters = new DynamicParameters();

            parameters.Add("@schoolGroupId", schoolGroupId, DbType.Int64);
            parameters.Add("@entryDate", entryDate, DbType.DateTime);
            // parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<ClassAttendanceDetails>("Attendance.GetPreviousClassAttendanceDetails", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ParameterMappingList>> GetParameterMappingByAcademicId(long academicYearId, short languageId)
        {
            var parameters = new DynamicParameters();

            parameters.Add("@academicYearId", academicYearId, DbType.Int64);
            parameters.Add("@languageId", languageId, DbType.Int16);
            // parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<ParameterMappingList>("Attendance.GetParameterMappingByAcademicId", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<MergeServiceModelForGradeSection> GetRequiredParamBySchoolUserId(long schoolId, long id, long academicYearId, short languageId)
        {
            MergeServiceModelForGradeSection objMergeServiceModelForGradeSection = new MergeServiceModelForGradeSection();
            var parameters = new DynamicParameters();
            parameters.Add("@schoolId", schoolId, DbType.Int64);
            parameters.Add("@id", id, DbType.Int64);
            parameters.Add("@academicYearId", academicYearId, DbType.Int64);
            parameters.Add("@languageId", languageId, DbType.Int32);
            using (var conn= GetOpenConnection())
            {
                var result = await conn.QueryMultipleAsync("[Attendance].[GetRequiredParamBySchoolUserId]", parameters, null, null, CommandType.StoredProcedure);

                var attendanceConfiguration = await result.ReadAsync<AttendanceConfiguration>();
                var authorizedStaffDetails = await result.ReadAsync<AuthorizedStaffDetails>();
                var parameterMappingList = await result.ReadAsync<ParameterMappingList>();
                var attendanceParameterShortCut = await result.ReadAsync<ParameterShortCutMapping>();
                var gradeSessionType = await result.ReadAsync<GradeSessionType>();
                objMergeServiceModelForGradeSection.AttendanceConfiguration = attendanceConfiguration.ToList();
                objMergeServiceModelForGradeSection.AuthorizedStaffDetails = authorizedStaffDetails.ToList();
                objMergeServiceModelForGradeSection.ParameterMappingList = parameterMappingList.ToList();
                objMergeServiceModelForGradeSection.AttendanceParameterShortCut = attendanceParameterShortCut.ToList();
                objMergeServiceModelForGradeSection.GradeSessionType = gradeSessionType.ToList();

                return objMergeServiceModelForGradeSection;
            }
        }

        public async Task<ClassAttendanceHeaderNDetails> GetClassAttendanceHeaderNDetails(long schoolId, long schoolGroupId, DateTime entryDate)
        {
            ClassAttendanceHeaderNDetails objClassAttendanceHeaderNDetails = new ClassAttendanceHeaderNDetails();
            var parameters = new DynamicParameters();

            parameters.Add("@schoolId", schoolId, DbType.Int64);
            parameters.Add("@schoolGroupId", schoolGroupId, DbType.Int64);
            parameters.Add("@entryDate", entryDate, DbType.DateTime);
            using (var conn = GetOpenConnection())
            {
                var result = await conn.QueryMultipleAsync("[Attendance].[GetClassAttendanceHeaderNDetails]", parameters, null, null, CommandType.StoredProcedure);

                var classHeader = await result.ReadAsync<ClassAttendance>();
                var classDetails = await result.ReadAsync<ClassAttendanceDetails>();

                objClassAttendanceHeaderNDetails.ClassAttendanceHeader = classHeader.ToList();
                objClassAttendanceHeaderNDetails.ClassAttendanceDetails = classDetails.ToList();
               

                return objClassAttendanceHeaderNDetails;
            }
        }
    }
}
