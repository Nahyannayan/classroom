﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.API.Repositories.ExemplarWall.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories.ExemplarWall
{
    public class ExemplarWallRepository : SqlRepository<ExemplarWallModel>, IExemplarWallRepository
    {
        private readonly IConfiguration _config;
        public ExemplarWallRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public bool AddUpdateExemplarWall(ExemplarWallModel entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetExemplarWallParameters(entity, mode);
                conn.Query<int>("SCHOOL.ExemplarWallCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public bool ShareExemplarWall(sharepost entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetShareExemplarWallParameters(entity, mode);
                conn.Query<int>("SCHOOL.ShareExemplarWall", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public bool DeleteExemplarWall(ExemplarWallModel entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetExemplarWallParameters(entity, TransactionModes.Delete);
                conn.Query<int>("SCHOOL.ExemplarWallCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<ExemplarWallModel>> GetExemplarWallDetailBySchoolId(long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                return await conn.QueryAsync<ExemplarWallModel>("School.GetExemplarWallDetailBySchoolId", parameters, null, null, CommandType.StoredProcedure);

            }
        }
        public async Task<ExemplarWallModel> GetExemplarWall(long ExemplarWallId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ExmplarDetailId", ExemplarWallId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<ExemplarWallModel>("[School].[GetExmplarDetailById]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Phoenix.Models.Course>> GetCourseByDepartment(long DepartmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@departmentId", DepartmentId, DbType.Int64);
                return await conn.QueryAsync<Course>("school.GetCourseByDepartment", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ExemplarUserDetails>> GetBlogsByGroupNBlogtypeId(int SchoolLevel, int Department, int CourseId, int SchoolId, int groupId,
            int blogTypeId, int? isPublish, Int64 UserId, Int64 CreatedBy, string SearchText, bool MyExemplarWork)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolLevel", SchoolLevel, DbType.Int32);
                parameters.Add("@Department", Department, DbType.Int32);
                parameters.Add("@CourseId", CourseId, DbType.Int32);
                parameters.Add("@SchoolId", SchoolId, DbType.Int32);
                parameters.Add("@SelGroupId", groupId, DbType.String);
                parameters.Add("@UserId", UserId, DbType.Int64);
                parameters.Add("@CreatedBy", CreatedBy, DbType.Int64);
                parameters.Add("@SearchText", SearchText, DbType.String);
                parameters.Add("@MyExemplarWork", MyExemplarWork, DbType.Boolean);
                var result = await conn.QueryMultipleAsync("School.GetExemplarPostDetails", parameters, commandType: CommandType.StoredProcedure);
                var exemplarUserDetails = await result.ReadAsync<ExemplarUserDetails>();
                var userDetails = await result.ReadAsync<TaggedStudentList>();
                var AssignedGroup = await result.ReadAsync<TaggedGroup>();
                var AssignedCourse = await result.ReadAsync<TaggedCourse>();
                var exemplarPostlist = exemplarUserDetails.ToList();
                var userDetailslist = userDetails.ToList();
                if (exemplarPostlist.Any())
                {
                    exemplarPostlist.ForEach(x => {
                        x.studentList = userDetailslist.Where(p => p.ExemplarPostId == x.ExemplarWallId).ToList();
                        x.AssignedCourse = AssignedCourse.Where(p => p.ExemplarWallId == x.ExemplarWallId).FirstOrDefault();
                        x.AssignedGroup = AssignedGroup.Where(p => p.ExemplarWallId == x.ExemplarWallId).FirstOrDefault();
                    });
                }
                return exemplarPostlist;
            }
        }

        public async Task<IEnumerable<SchoolLevel>> GetSchoolLevelBySchoolId(long schoolId, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int32);
                return await conn.QueryAsync<SchoolLevel>("School.GetSchoolLevel", parameters, commandType: CommandType.StoredProcedure);

            }
        }
        public async Task<IEnumerable<SchoolDepartment>> GetSchoolDepartmentList(long schoolId, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int32);
                return await conn.QueryAsync<SchoolDepartment>("School.GetSchoolDepartment", parameters, commandType: CommandType.StoredProcedure);

            }
        }
        public async Task<IEnumerable<SchoolGroup>> GetSchoolGroupBasedOnSchoolLevel(long LevelId, long UserId, long CourseId,long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@LevelId", LevelId, DbType.Int64);
                parameters.Add("@UserId", UserId, DbType.Int64);
                parameters.Add("@CourseId", CourseId, DbType.Int64);
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                return await conn.QueryAsync<SchoolGroup>("School.GetGroupIdByCourseIdORUserId ", parameters, commandType: CommandType.StoredProcedure);

            }
        }

        public override void InsertAsync(ExemplarWallModel entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(ExemplarWallModel entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<ExemplarWallModel>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<ExemplarWallModel> GetAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        #region Private Method

        private DynamicParameters GetExemplarWallParameters(ExemplarWallModel entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            //parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@ExemplarWallId", entity.ExemplarWallId, DbType.Int64);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@PostTitle", entity.PostTitle, DbType.String);
            parameters.Add("@PostDescription", entity.PostDescription, DbType.String);
            parameters.Add("@TaggedGroup", entity.TaggedGroup, DbType.String);
            parameters.Add("@TaggedStudent", entity.TaggedStudent, DbType.String);
            parameters.Add("@WinnerStudent", entity.WinnerStudent, DbType.String);
            parameters.Add("@IsDepartmentWall", entity.IsDepartmentWall, DbType.Boolean);
            parameters.Add("@IsCourseWall", entity.IsCourseWall, DbType.Boolean);
            parameters.Add("@SchoolLevelId", entity.SchoolLevelId, DbType.String);
            parameters.Add("@ReferenceLink", entity.ReferenceLink, DbType.String);
            parameters.Add("@EmbededVideoLink", entity.EmbededVideoLink, DbType.String);
            parameters.Add("@AdditionalDocPath", entity.AdditionalDocPath, DbType.String);
            parameters.Add("@ParentSharableLink", entity.ParentSharableLink, DbType.String);
            parameters.Add("@FileNames", entity.FileNames, DbType.String);
            parameters.Add("@UserId", entity.UserId, DbType.Int64);
            parameters.Add("@IsDeleted", entity.IsDeleted, DbType.Boolean);

            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int32);
            parameters.Add("@SortOrder", entity.SortOrder, DbType.Int32);
            parameters.Add("@IsPublish", entity.IsPublish, DbType.Int32);
            parameters.Add("@BlogTypeId", entity.BlogTypeId, DbType.Int32);
            parameters.Add("@DeleteReason", entity.DeleteReason, DbType.String);
            parameters.Add("@IsRejected", entity.IsRejected, DbType.Boolean);
            //parameters.Add("@CourseId", entity.CourseId, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        private DynamicParameters GetShareExemplarWallParameters(sharepost entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            //parameters.Add("@TransMode", (int)mode, DbType.Int32);

            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@GroupId", entity.GroupId, DbType.Int64);
            parameters.Add("@PostId", entity.PostId, DbType.Int64);
            parameters.Add("@ShareWithDepartmentWall", entity.ShareWithDepartmentWall, DbType.Boolean);
            parameters.Add("@ShareWithCourseWall", entity.ShareWithCourseWall, DbType.Boolean);
            //parameters.Add("@SharingDate", entity.SharingDate, DbType.DateTime);
            parameters.Add("@SharedByUserId", entity.SharedByUserId, DbType.Int64);
            parameters.Add("@SchoolLevelId", entity.SchoolLevelId, DbType.Int64);

            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        public async Task<IEnumerable<ExemplarWallModel>> GetPendingForApprovalExemplarPost(long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                return await conn.QueryAsync<ExemplarWallModel>("[DE].GetPendingForApprovalExemplarPost", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<int> UpdateExemplarPostStatusFromPendingToApprove(long ExemplarPostId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ExemplarPostId", ExemplarPostId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                var result = await conn.QueryAsync("[DE].UpdateExemplarPostStatusFromPendingToApprove", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        public async Task<bool> GetApprovalStatusDetails(long SchoolId, string Opearion, string FlagType, bool Value)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                parameters.Add("@Opearion", Opearion, DbType.String);
                parameters.Add("@Type", FlagType, DbType.String);
                parameters.Add("@Value", Value, DbType.Boolean);
                parameters.Add("@Output", dbType: DbType.Boolean, direction: ParameterDirection.Output);
                var result=await conn.QueryAsync<bool>("[SCHOOL].SetApprovalFlag", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<bool>("Output");
            }
        }

        #endregion

    }
}
