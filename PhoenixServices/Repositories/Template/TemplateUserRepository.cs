﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Reflection;

namespace Phoenix.API.Repositories
{
    public class TemplateUserRepository : SqlRepository<LogInUser>, ITemplateUserRepository
    {
        private readonly IConfiguration _config;

        public TemplateUserRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<LogInUser>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<LogInUser> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<LogInUser> GetLogInUserByUserName(string UserName)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserName", UserName, DbType.String);
                return await conn.QueryFirstOrDefaultAsync<LogInUser>("Admin.GetLoginUserByUserName", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<LogInUser>> GetUserList(int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<LogInUser>("Admin.GetUserList", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(LogInUser entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(LogInUser entityToUpdate)
        {
            throw new NotImplementedException();
        }
        public bool IsValidUser(string userName, string Password)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserName", userName, DbType.String);
                parameters.Add("@Password", Password, DbType.String);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Admin.IsValidUser", parameters, commandType: CommandType.StoredProcedure);
                return  parameters.Get<int>("Output") > 0;
            }
        }

        public bool ResetPassword(string userName, string Password)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserName", userName, DbType.String);
                parameters.Add("@Password", Password, DbType.String);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Admin.ResetPassword", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("Output") > 0;
            }
        }

        public async Task<IEnumerable<StudentUploadTemplate>> StudentTemplateBulkImport(List<StudentUploadTemplate> StudentList,long UserId)
        {
            DataTable dt = new DataTable();

            dt = ToDataTable(StudentList);
            using (var conn = GetOpenConnectionForStageDB())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@typTEMPLATE_STUDENT", dt,DbType.Object);
                parameters.Add("@CREATED_BY", UserId);
                return await conn.QueryAsync<StudentUploadTemplate>("dbo.UPLOAD_TEMPLATE_STUDENT_DATA", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<StaffUploadTemplate>> StaffTemplateBulkImport(List<StaffUploadTemplate> StaffList, long UserId)
        {
            DataTable dt = new DataTable();

            dt = ToDataTable(StaffList);

            using (var conn = GetOpenConnectionForStageDB())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@typTEMPLATE_STAFF", dt, DbType.Object);
                parameters.Add("@CREATED_BY", UserId);
                return await conn.QueryAsync<StaffUploadTemplate>("dbo.UPLOAD_TEMPLATE_STAFF_DATA", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<GRADE_WISE_SUBJECT_M>> GradeWiseSubjectTemplateBulkImport(List<GRADE_WISE_SUBJECT_M> GradeWiseSubjectList, long UserId)
        {
            DataTable dt = new DataTable();

            dt = ToDataTable(GradeWiseSubjectList);

            using (var conn = GetOpenConnectionForStageDB())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@typTEMPLATE_GRADE_WISE_SUBJECT_M", dt, DbType.Object);
                parameters.Add("@CREATED_BY", UserId);
                return await conn.QueryAsync<GRADE_WISE_SUBJECT_M>("dbo.UPLOAD_TEMPLATE_GRADE_WISE_SUBJECT_M", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<STUDENT_WISE_SUBJECT_MAPPING>> StudentWiseSubjectTemplateBulkImport(List<STUDENT_WISE_SUBJECT_MAPPING> StudentWiseSubjectList, long UserId)
        {
            DataTable dt = new DataTable();

            dt = ToDataTable(StudentWiseSubjectList);

            using (var conn = GetOpenConnectionForStageDB())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@typTEMPLATE_STUDENT_WSIE_SUBJECT", dt,DbType.Object);
                parameters.Add("@CREATED_BY", UserId);
                return await conn.QueryAsync<STUDENT_WISE_SUBJECT_MAPPING>("dbo.UPLOAD_TEMPLATE_STUDENT_WSIE_SUBJECT", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public DataTable ToDataTable<T>(List<T> items)
        {

            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in Props)

            {

                //Setting column names as Property names

                dataTable.Columns.Add(prop.Name);

            }

            foreach (T item in items)

            {

                var values = new object[Props.Length];

                for (int i = 0; i < Props.Length; i++)

                {

                    //inserting property values to datatable rows

                    values[i] = Props[i].GetValue(item, null);

                }

                dataTable.Rows.Add(values);

            }

            //put a breakpoint here and check datatable

            return dataTable;
        }

       
    }
}
