﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface ITemplateUserRepository : IGenericRepository<LogInUser>
    {
        Task<LogInUser> GetLogInUserByUserName(string UserName);
        Task<IEnumerable<LogInUser>> GetUserList(int schoolId);
        bool IsValidUser(string userName, string Password);
        Task<IEnumerable<StudentUploadTemplate>> StudentTemplateBulkImport(List<StudentUploadTemplate> StudentList, long UserId);
        Task<IEnumerable<StaffUploadTemplate>> StaffTemplateBulkImport(List<StaffUploadTemplate> StaffList, long UserId);
        Task<IEnumerable<GRADE_WISE_SUBJECT_M>> GradeWiseSubjectTemplateBulkImport(List<GRADE_WISE_SUBJECT_M> GradeWiseSubjectList, long UserId);
        Task<IEnumerable<STUDENT_WISE_SUBJECT_MAPPING>> StudentWiseSubjectTemplateBulkImport(List<STUDENT_WISE_SUBJECT_MAPPING> StudentWiseSubjectList, long UserId);

        bool ResetPassword(string userName, string Password);
    }
}
