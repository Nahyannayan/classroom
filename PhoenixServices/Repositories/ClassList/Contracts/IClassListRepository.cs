﻿using DbConnection;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Phoenix.API.Models;
using SIMS.API.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public interface IClassListRepository : IGenericRepository<ClassListModel>
    {
        Task<IEnumerable<ClassListModel>> GetClassList(long SchoolId, long GroupId, long GradeId, long SectionId);
        Task<BasicDetailModel> GetStudentDetails(string stu_id);
        Task<StudentProfileModel> GetStudentProfileDetail(long STU_ID, long SchoolId, DateTime nowDate,short languageId);
        Task<IEnumerable<ParentDetailModel>> GetQuickContactDetail(long StudentId, long SchoolId);
        Task<TimeTableModel> GetStudentFindMeDetail(long StudentId, long SchoolId,DateTime nowDate);
        Task<IEnumerable<WeekDayModel>> GetWeeklyTimeTableDetail(long StudentId, long SchoolId, int WeekCount,DateTime nowDate,short languageId);
        Task<IEnumerable<AttendanceChart>> GetAttendanceChart(string STU_ID);
        Task<IEnumerable<AttendenceListModel>> GetAttendenceList(string STU_ID, DateTime EndDate);
        Task<long> StudentOnReportMasterCU(StudentOnReportMasterModel studentOnReport);
        Task<IEnumerable<StudentOnReportMasterModel>> GetStudentOnReportMasters(long studentId, long academicYearId, long schoolId);
        Task<long> StudentOnReportDetailsCU(StudentOnReportModel studentOnReport);
        Task<IEnumerable<StudentOnReportModel>> GetStudentOnReportDetails(StudentOnReportParameterModel detailsParameter);
        Task<IEnumerable<ClassListModel>> GetStudentPhotoPath(string BSU_ID, long RPF_ID, string STU_ID);
        Task<AttendanceProfileModel> GetAttendanceProfileDetail(long StudentId, long SchoolId, string AttendanceDt, string AttendanceType);
        Task<ChangeGroupStudentModel> GetCourseWiseSchoolGroups(long StudentId);
        Task<bool> ChangeCourseWiseStudentSchoolGroup(ChangeGroupStudentModel changeGroupStudentModel);
        Task<IEnumerable<ClassListModel>> GetStudentListBySearch(long SchoolId,long UserId,string SearchString);
    }
}
