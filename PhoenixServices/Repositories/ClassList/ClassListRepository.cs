﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Models.Entities;
using SIMS.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class ClassListRepository : SqlRepository<ClassListModel>, IClassListRepository
    {
        private readonly IConfiguration _config;
        public ClassListRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override Task<IEnumerable<ClassListModel>> GetAllAsync()
        {
            throw new NotImplementedException();
        }
        public override Task<ClassListModel> GetAsync(int id)
        {
            throw new NotImplementedException();
        }
        public async Task<IEnumerable<ClassListModel>> GetClassList(long SchoolId, long GroupId, long GradeId, long SectionId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                parameters.Add("@GroupId", GroupId, DbType.Int64);
                parameters.Add("@GradeId", GradeId, DbType.Int64);
                parameters.Add("@SectionId", SectionId, DbType.Int64);
                return await conn.QueryAsync<ClassListModel>("ClassList.GETCLASSLIST", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<BasicDetailModel> GetStudentDetails(string STU_ID)
        {
            var option = "STUDENT";
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@STU_ID", STU_ID, DbType.String);
                parameters.Add("@Option", option, DbType.String);
                return await conn.QueryFirstOrDefaultAsync<BasicDetailModel>("SIMS.GET_STUDENT_PROFILE", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<StudentProfileModel> GetStudentProfileDetail(long STU_ID, long SchoolId, DateTime nowDate, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var studentProfile = new StudentProfileModel();
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", STU_ID);
                parameters.Add("@SchoolId", SchoolId);
                parameters.Add("@languageId", languageId);
                var result = await conn.QueryMultipleAsync("ClassList.GetStudentProfileDetail", parameters, commandType: CommandType.StoredProcedure);
                var BasicDetails = await result.ReadFirstOrDefaultAsync<BasicDetailModel>();
                var ParentDetails = await result.ReadFirstOrDefaultAsync<ParentDetailModel>();
                var SiblingDetails = await result.ReadAsync<SiblingDetailModel>();
                var IncidentDetailModel = await result.ReadAsync<BehaviorDetailModel>();
                var BehaviourSubCategoryList = await result.ReadAsync<BehaviourSubCategoryModel>();
                var BehaviourPoint = await result.ReadFirstOrDefaultAsync<BehaviourPointModel>();
                var overallPercentageModel = await result.ReadFirstOrDefaultAsync<OverallPercentageModel>();

                if (BasicDetails != null)
                {
                    studentProfile.BasicDetailModel = BasicDetails ?? new BasicDetailModel();
                    studentProfile.ParentDetailModel = ParentDetails ?? new ParentDetailModel();
                    studentProfile.SiblingDetailModel = SiblingDetails ?? new List<SiblingDetailModel>();
                    studentProfile.IncidentDetailModel = IncidentDetailModel ?? new List<BehaviorDetailModel>();
                    studentProfile.BehaviourSubCategoryList = BehaviourSubCategoryList ?? new List<BehaviourSubCategoryModel>();
                    studentProfile.BehaviourPointModel = BehaviourPoint ?? new BehaviourPointModel();
                    studentProfile.OverallPercentageModel = overallPercentageModel ?? new OverallPercentageModel();
                    studentProfile.WeekDayList = await GetWeeklyTimeTableDetail(STU_ID, SchoolId, 0, nowDate, languageId);
                }
                return studentProfile;
            }
        }
        public async Task<IEnumerable<ParentDetailModel>> GetQuickContactDetail(long StudentId, long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", StudentId);
                parameters.Add("@SchoolId", SchoolId);
                var ParentDetails = await conn.QueryAsync<ParentDetailModel>("ClassList.GetQuickContactDetail", parameters, commandType: CommandType.StoredProcedure);
                ParentDetails = ParentDetails ?? new List<ParentDetailModel>();
                return ParentDetails;
            }
        }
        public async Task<TimeTableModel> GetStudentFindMeDetail(long StudentId, long SchoolId, DateTime nowDate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", StudentId, DbType.Int64);
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                parameters.Add("@nowDate", nowDate, DbType.DateTime);
                var findMeModel = await conn.QueryFirstOrDefaultAsync<TimeTableModel>("ClassList.GetStudentFindMeDetail", parameters, commandType: CommandType.StoredProcedure);
                findMeModel = findMeModel ?? new TimeTableModel();
                return findMeModel;
            }
        }
        public async Task<IEnumerable<WeekDayModel>> GetWeeklyTimeTableDetail(long StudentId, long SchoolId, int WeekCount, DateTime nowDate, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", StudentId);
                parameters.Add("@SchoolId", SchoolId);
                parameters.Add("@WeekCount", WeekCount);
                parameters.Add("@nowDate", nowDate);
                parameters.Add("@languageId", languageId);
                var result = await conn.QueryMultipleAsync("ClassList.GetWeeklyTimeTableDetail", parameters, commandType: CommandType.StoredProcedure);

                var WeekDayList = await result.ReadAsync<WeekDayModel>();
                WeekDayList = WeekDayList ?? new List<WeekDayModel>();
                var TimeTableList = await result.ReadAsync<TimeTableModel>();
                TimeTableList = TimeTableList ?? new List<TimeTableModel>();

                if (WeekDayList != null)
                {
                    WeekDayList.ToList().ForEach(x =>
                    {
                        x.TimeTableList = TimeTableList.ToList().Where(a => a.WeekDayNumber == x.WeekDayNumber);
                    });
                }
                return WeekDayList;
            }
        }
        public async Task<IEnumerable<AttendanceChart>> GetAttendanceChart(string STU_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@STU_ID", STU_ID, DbType.String);
                //  return await conn. <IEnumerable<ActivitiesDetails>>("SIMS.GET_ACTIVITY_BY_STU_ID", parameters, null, null, CommandType.StoredProcedure);
                return await conn.QueryAsync<AttendanceChart>("SIMS.GETSTUDDASHBOARD_ATT_PATTENBY_ID", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<AttendenceListModel>> GetAttendenceList(string STU_ID, DateTime EndDate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@STU_ID", STU_ID, DbType.String);
                parameters.Add("@ENDDATE", EndDate, DbType.DateTime);
                return await conn.QueryAsync<AttendenceListModel>("SIMS.GET_STUDENT_ATTENDENCE_HISTORY", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public override void InsertAsync(ClassListModel entity)
        {
            throw new NotImplementedException();
        }
        public override void UpdateAsync(ClassListModel entityToUpdate)
        {
            throw new NotImplementedException();
        }
        public async Task<long> StudentOnReportMasterCU(StudentOnReportMasterModel studentOnReport)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SRM_ID", studentOnReport.Id);
                parameters.Add("@SRM_ACD_ID", studentOnReport.AcademicYearId);
                parameters.Add("@SRM_BSU_ID", studentOnReport.SchoolId);
                parameters.Add("@SRM_GRD_ID", studentOnReport.GradeId);
                parameters.Add("@SRM_SCT_ID", studentOnReport.SectionId);
                parameters.Add("@SRM_GROUP_ID", studentOnReport.GroupId);
                parameters.Add("@SRM_STU_ID", studentOnReport.StudentId);
                parameters.Add("@SRM_FROM_DATE", studentOnReport.FromDate);
                parameters.Add("@SRM_TO_DATE", studentOnReport.ToDate);
                parameters.Add("@SRM_DESC", studentOnReport.Description);
                parameters.Add("@SRM_CREATED_BY", studentOnReport.CreatedBy);
                parameters.Add("@SRM_IsActive", studentOnReport.IsActive);
                parameters.Add("@OUTPUT", dbType: DbType.Int64, direction: ParameterDirection.Output);
                await conn.QueryAsync("SIMS.StudentOnReportMasterCU", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<long>("OUTPUT");
            }
        }
        public async Task<IEnumerable<StudentOnReportMasterModel>> GetStudentOnReportMasters(long studentId, long academicYearId, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SRM_STU_ID", studentId);
                parameters.Add("@SRM_ACD_ID", academicYearId);
                parameters.Add("@SRM_BSU_ID", schoolId);
                return await conn.QueryAsync<StudentOnReportMasterModel>("SIMS.GetStudentOnReportMaster", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<long> StudentOnReportDetailsCU(StudentOnReportModel studentOnReport)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SRD_ID", studentOnReport.Id);
                parameters.Add("@SRD_SRM_ID", studentOnReport.StudentOnReportMasterId);
                parameters.Add("@SRD_PERIOD_NO", studentOnReport.PeriodNo);
                parameters.Add("@SRD_DESC", studentOnReport.Description);
                parameters.Add("@SRD_GROUP_ID", studentOnReport.GroupId);
                parameters.Add("@SRD_CREATED_BY", studentOnReport.CreatedBy);
                parameters.Add("@SRD_CREATED_ON", studentOnReport.CreatedOn);
                parameters.Add("@IsActive", studentOnReport.IsActive);
                parameters.Add("@OUTPUT", dbType: DbType.Int64, direction: ParameterDirection.Output);
                await conn.QueryAsync("SIMS.StudentOnReportDetailCU", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<long>("OUTPUT");
            }
        }
        public async Task<IEnumerable<StudentOnReportModel>> GetStudentOnReportDetails(StudentOnReportParameterModel detailsParameter)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SRM_STU_ID", detailsParameter.StudentId);
                parameters.Add("@SRM_ACD_ID", detailsParameter.AcademicYear);
                parameters.Add("@SRM_BSU_ID", detailsParameter.SchoolId);
                parameters.Add("@SRD_SRM_ID", detailsParameter.StudentOnReportMasterId);
                parameters.Add("@SRD_CREATED_BY", detailsParameter.CreatedBy);
                parameters.Add("@SRD_GROUP_ID", detailsParameter.GroupId);
                return await conn.QueryAsync<StudentOnReportModel>("SIMS.GetStudentOnReportDetail", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ClassListModel>> GetStudentPhotoPath(string BSU_ID, long RPF_ID, string STU_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();

                parameters.Add("@STU_ID", STU_ID, DbType.String);
                parameters.Add("@RPF_ID", RPF_ID, DbType.Int64);
                parameters.Add("@BSU_ID", BSU_ID, DbType.String);
                return await conn.QueryAsync<ClassListModel>("RPT.GET_STUDENT_PHOTO_PATH", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<AttendanceProfileModel> GetAttendanceProfileDetail(long StudenId, long SchoolId, string AttendanceDt, string AttendanceType)
        {
            using (var conn = GetOpenConnection())
            {
                var attendanceProfile = new AttendanceProfileModel();
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", StudenId);
                parameters.Add("@SchoolId", SchoolId);
                parameters.Add("@Att_DT", AttendanceDt);
                parameters.Add("@AttendanceType", AttendanceType);

                var result = await conn.QueryMultipleAsync("ClassList.GetAttendanceProfileDetail",
                    parameters, commandType: CommandType.StoredProcedure);

                var BasicDetails = await result.ReadFirstOrDefaultAsync<BasicDetailModel>();
                var AttendanceBySession = await result.ReadAsync<AttendenceBySessionModel>();
                var AttendenceSessionCode = await result.ReadAsync<AttendenceSessionCodeModel>();
                var AttendanceChartList = await result.ReadAsync<AttendanceChartModel>();
                var AttendanceList = await result.ReadAsync<AttendenceListModel>();

                if (BasicDetails != null)
                {
                    attendanceProfile.StudentDetail = BasicDetails ?? new BasicDetailModel();
                    attendanceProfile.StudentDetail.AttendenceBySession = AttendanceBySession ?? new List<AttendenceBySessionModel>();
                    attendanceProfile.AttendanceChart = AttendanceChartList ?? new List<AttendanceChartModel>();
                    attendanceProfile.AttendenceList = AttendanceList ?? new List<AttendenceListModel>();
                    attendanceProfile.AttendenceSessionCode = AttendenceSessionCode ?? new List<AttendenceSessionCodeModel>();
                }
                return attendanceProfile;
            }
        }
        public async Task<ChangeGroupStudentModel> GetCourseWiseSchoolGroups(long StudentId)
        {
            using (var conn = GetOpenConnection())
            {
                var changeGroupStudent = new ChangeGroupStudentModel();
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", StudentId);
                var result = await conn.QueryMultipleAsync("ClassList.GetCourseWiseSchoolGroups",
                    parameters, commandType: CommandType.StoredProcedure);

                changeGroupStudent = await result.ReadFirstOrDefaultAsync<ChangeGroupStudentModel>();
                changeGroupStudent = changeGroupStudent ?? new ChangeGroupStudentModel();
                var ChangeGroupCourseList = await result.ReadAsync<ChangeGroupCourseModel>();
                var ChangeSchoolGroupList = await result.ReadAsync<ChangeSchoolGroupModel>();
                if (ChangeGroupCourseList != null)
                {
                    changeGroupStudent.ChangeGroupCourseList = ChangeGroupCourseList ?? new List<ChangeGroupCourseModel>();
                    changeGroupStudent.ChangeSchoolGroupList = ChangeSchoolGroupList ?? new List<ChangeSchoolGroupModel>();

                }
                return changeGroupStudent;
            }
        }

        public async Task<bool> ChangeCourseWiseStudentSchoolGroup(ChangeGroupStudentModel changeGroupStudentModel)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CourseWiseChangeGroupDT", changeGroupStudentModel.CourseWiseChangeGroupDT(), DbType.Object);
                parameters.Add("@OUTPUT", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync("ClassList.ChangeStudentSchoolGroup", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("OUTPUT") > 0;
            }
        }
        public async Task<IEnumerable<ClassListModel>> GetStudentListBySearch(long SchoolId, long UserId, string SearchString)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId);
                parameters.Add("@UserId", UserId);
                parameters.Add("@SearchString", SearchString);
                return await conn.QueryAsync<ClassListModel>("ClassList.GetStudentListBySearch", parameters, null, null, CommandType.StoredProcedure);
            }
        }
    }
}
