﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public class GradeBookRepository : SqlRepository<GradeDetails>, IGradeBookRepository
    {
        private readonly IConfiguration _config;
        public GradeBookRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        #region Built In Functions
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override Task<IEnumerable<GradeDetails>> GetAllAsync()
        {
            throw new NotImplementedException();
        }
        public override Task<GradeDetails> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(GradeDetails entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(GradeDetails entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion Built In Functions

        #region Teacher Grade Book 
        public async Task<TeacherGradeBook> GetTeacherGradeBook(long SchoolGroupId, long teacherId)
        {
            using (var conn = GetOpenConnection())
            {
                var gradeBooks = new TeacherGradeBook();
                var parameters = new DynamicParameters();
                parameters.Add("@groupId", SchoolGroupId);
                parameters.Add("@teacherId", teacherId);
                var result = await conn.QueryMultipleAsync("Gradebook.GetTeacherGradeBook", parameters, commandType: CommandType.StoredProcedure);
                gradeBooks.StudentLists = await result.ReadAsync<GradeBookStudentList>();
                gradeBooks.AssessmentType = await result.ReadAsync<int>();
                gradeBooks.Assignments = await result.ReadAsync<GradeBookAssignmentQuiz>();
                gradeBooks.Quizzes = await result.ReadAsync<GradeBookAssignmentQuiz>();
                //gradeBooks.InternalAssessments = await result.ReadAsync<InternalAssessment>();
                //gradeBooks.TeacherInternalAssessments = await result.ReadAsync<TeacherInternalAssessment>();
                //gradeBooks.AssessmentScores = await result.ReadAsync<InternalAssessmentScore>();
                //gradeBooks.Standards = await result.ReadAsync<StandardExamination>();
                //gradeBooks.Examinations = await result.ReadAsync<StandardExamination>();
                //gradeBooks.GradingTemplates = await result.ReadAsync<GradeBookGradingTemplate>();
                return gradeBooks;
            }
        }

        public async Task<long> TeacherGradebookCreateNewRow(TeacherInternalAssessment createNewRow)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@internalAssessmentId", createNewRow.InternalAssessmentId);
                parameters.Add("@ParentInternalAssessmentId", createNewRow.ParentInternalAssessmentId);
                parameters.Add("@createdBy", createNewRow.CreatedBy);
                parameters.Add("@RowName", createNewRow.RowName);
                parameters.Add("@RowType", createNewRow.RowType);
                parameters.Add("@FromMark", createNewRow.FromMark);
                parameters.Add("@ToMark", createNewRow.ToMark);
                parameters.Add("@GradingTemplateId", createNewRow.GradingTemplateId);
                parameters.Add("@CommentLength", createNewRow.CommentLength);
                parameters.Add("@IsDelete", createNewRow.IsDelete);
                parameters.Add("@IsForceDelete", createNewRow.ForceDeleteAssessmentEvenIfDataExist);
                return await conn.QueryFirstOrDefaultAsync<long>("Gradebook.AddNewRow", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<bool> InternalAssessmentCU(List<InternalAssessmentScore> assessmentScore)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                DataTable dt = new DataTable();
                dt.Columns.Add("InternalAssessmentId", typeof(long));
                dt.Columns.Add("StudentId", typeof(long));
                dt.Columns.Add("Score", typeof(string));
                dt.Columns.Add("ParentInternalAssessmentId", typeof(long));
                dt.Columns.Add("CreatedBy", typeof(long));
                assessmentScore.ForEach(x =>
                {
                    dt.Rows.Add(x.InternalAssessmentId, x.StudentId, x.Score, x.ParentInternalAssessmentId, x.CreatedBy);
                });
                parameters.Add("@studentScores", dt, DbType.Object);
                return await conn.QuerySingleOrDefaultAsync<bool>("Gradebook.InternalAssessmentCU", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<GradeBookAssignmentQuiz>> GetAssignmentQuizScore(long schoolGroupId,bool isAssignment)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@groupId", schoolGroupId);
                parameters.Add("@isAssignment", isAssignment);
                return await conn.QueryAsync<GradeBookAssignmentQuiz>("Gradebook.GetAssignmentQuizScore", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<TeacherGradeBook> GetInternalAssessments(long schoolGroupId, long  teacherId)
        {
            using (var conn = GetOpenConnection())
            {
                var gradeBooks = new TeacherGradeBook();
                var parameters = new DynamicParameters();
                parameters.Add("@groupId", schoolGroupId);
                parameters.Add("@teacherid", teacherId);
                var result = await conn.QueryMultipleAsync("Gradebook.GetInternalAssessment", parameters, commandType: CommandType.StoredProcedure);
                var internalAssessments = await result.ReadAsync<InternalAssessment>();                
                var teacherInternalAssessments = await result.ReadAsync<TeacherInternalAssessment>();                
                gradeBooks.AssessmentScores = await result.ReadAsync<InternalAssessmentScore>();
                gradeBooks.GradingTemplates = await result.ReadAsync<GradeBookGradingTemplate>();
                gradeBooks.InternalAssessments = internalAssessments.ToList();
                gradeBooks.InternalAssessments.ForEach(x =>
                {
                    x.TeacherInternalAssessments = teacherInternalAssessments.Where(y => y.ParentInternalAssessmentId == x.InternalAssessmentId).ToList();
                });
                return gradeBooks;
            }
        }
        public async Task<IEnumerable<StandardExamination>> GetStandardExaminations(long schoolGroupId, int assessmentTypeId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@groupId", schoolGroupId);
                parameters.Add("@assessmentType", assessmentTypeId);
                return await conn.QueryAsync<StandardExamination>("Gradebook.GetStandardizedAssessmentOrExternalExamination", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<GradebookProgressTracker> GetProgressTracker(long schoolGroupId, DateTime? startDate, DateTime? endDate)
        {
            using (var conn = GetOpenConnection())
            {
                var progressTracker = new GradebookProgressTracker();
                var parameters = new DynamicParameters();
                parameters.Add("@groupId", schoolGroupId);
                parameters.Add("@startdate", startDate);
                parameters.Add("@enddate", endDate);
                var result = await conn.QueryMultipleAsync("Gradebook.[GetAttainmentProgressByGroup]", parameters, commandType: CommandType.StoredProcedure);
                progressTracker.ProgressTrackerType = await result.ReadAsync<int>();
                progressTracker.Attainments = await result.ReadAsync<ProgressTrackerAttainment>();
                progressTracker.Progresses = await result.ReadAsync<ProgressTrackerProgress>();
                return progressTracker;
            }
        }
        public async Task<TeacherInternalAssessment> GetSubInternalAssessment(long internalAssessmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@internalAssessmentId", internalAssessmentId);
                return await conn.QueryFirstOrDefaultAsync<TeacherInternalAssessment>("Gradebook.GetSubInternalAssessment", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<InternalAssessmentScore>> GetInternalAssessmentScoreByAssessmentId(long internalAssessmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@interanlAssessmentId", internalAssessmentId);
                return await conn.QueryAsync<InternalAssessmentScore>("Gradebook.GetInternalAssessmentScoreByAssessmentId", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        #endregion

    }
}
