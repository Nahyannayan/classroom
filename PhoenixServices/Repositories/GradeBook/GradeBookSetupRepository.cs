﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public class GradeBookSetupRepository : SqlRepository<GradeDetails>, IGradeBookSetupRepository
    {
        private readonly IConfiguration _config;
        public GradeBookSetupRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        #region Built In Functions
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override Task<IEnumerable<GradeDetails>> GetAllAsync()
        {
            throw new NotImplementedException();
        }
        public override Task<GradeDetails> GetAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override void InsertAsync(GradeDetails entity)
        {
            throw new NotImplementedException();
        }
        public override void UpdateAsync(GradeDetails entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion Built In Functions

        #region GradeBookForm
        public async Task<long> SaveGradeBookForm(GradeBook gradeBook)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetGradebookParameters(gradeBook);
                await conn.QueryAsync("Gradebook.GradebookFormCUD", parameters, commandType: CommandType.StoredProcedure);
                var id = parameters.Get<long>("output");
                return id;
            }
        }
        public async Task<long> DeleteGradeBookDetail(GradeBook gradeBook)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetGradebookParameters(gradeBook);
                await conn.QueryAsync("Gradebook.GradebookFormCUD", parameters, commandType: CommandType.StoredProcedure);
                var id = parameters.Get<long>("output");
                return id;
            }
        }
        private DynamicParameters GetGradebookParameters(GradeBook gradeBook)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", gradeBook.Modes);
            parameters.Add("@gradebookId", gradeBook.GradeBookId);
            parameters.Add("@gradebookName", gradeBook.GradeBookName);
            parameters.Add("@schoolId", gradeBook.SchoolId);
            parameters.Add("@createdby", gradeBook.Createdby);
            parameters.Add("@courseIds", string.Join(',', gradeBook.CourseIds));
            parameters.Add("@gradeIds", string.Join(',', gradeBook.SchoolGradeIds));
            parameters.Add("@assessmentTypeIds", string.Join(',', gradeBook.AssessmentTypeIds));
            parameters.Add("@assignmentQuiz", gradeBook.GetAssignmentQuizSetupsDT(), DbType.Object);
            parameters.Add("@standardizedSetup", gradeBook.GetStandardizedsDT(), DbType.Object);
            parameters.Add("@internalAssessment", gradeBook.GetInternalAssessmentDT(), DbType.Object);
            parameters.Add("@output", direction: ParameterDirection.Output, dbType: DbType.Int64);
            return parameters;
        }
        public async Task<GradeBook> GetGradeBookDetail(long gradeBookId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@gradebookId", gradeBookId);
                var result = await conn.QueryMultipleAsync("Gradebook.GetGradeBookDetail", parameters, commandType: CommandType.StoredProcedure);
                var gradeBooks = await result.ReadSingleOrDefaultAsync<GradeBook>();
                var courseId = await result.ReadAsync<long>();
                var schoolGradeId = await result.ReadAsync<long>();
                var assessmentTypeId = await result.ReadAsync<int>();
                var assignmentQuiz = await result.ReadAsync<AssignmentQuizSetup>();
                var standardizeds = await result.ReadAsync<StandardizedExaminationSetup>();
                var internalAssessment = await result.ReadAsync<InternalAssessment>();
                if (gradeBooks != null)
                {
                    gradeBooks.CourseIds = courseId.ToList();
                    gradeBooks.SchoolGradeIds = schoolGradeId.ToList();
                    gradeBooks.AssessmentTypeIds = assessmentTypeId.ToList();
                    gradeBooks.AssignmentQuizSetups = assignmentQuiz.ToList();
                    gradeBooks.Standardizeds = standardizeds.ToList();
                    gradeBooks.InternalAssessments = internalAssessment.ToList();
                }
                return gradeBooks;
            }
        }
        #endregion

        #region Grade Book Detail Pagination
        public async Task<IEnumerable<GradeBook>> GetGradeBookDetailPagination(int curriculumId, long schoolId, int pageNum, int pageSize, string searchString, string GradeIds, string CourseIds, string sortBy)
        {
            using (var conn = GetOpenConnection())
            {
                var parameter = new DynamicParameters();
                parameter.Add("@SchoolId", schoolId);
                parameter.Add("@curriculumid", curriculumId);
                parameter.Add("@PageNum ", pageNum);
                parameter.Add("@PageSize ", pageSize);
                parameter.Add("@SearchString ", searchString);
                parameter.Add("@GradeIds ", GradeIds);
                parameter.Add("@CourseIds ", CourseIds);
                parameter.Add("@sortBy ", sortBy);
                var result = await conn.QueryMultipleAsync("[Gradebook].[GetGradeBookDetailPagination]", parameter, commandType: CommandType.StoredProcedure);
                var GradeBookList = await result.ReadAsync<GradeBook>();
                var totalCount = await result.ReadFirstOrDefaultAsync<int>();
                var assignmentQuiz = await result.ReadAsync<AssignmentQuizSetup>();
                var GradeBookCourseDetailList = await result.ReadAsync<GradeBookCourseDetail>();
                var GradeBookGradeDetailList = await result.ReadAsync<GradeBookGradeDetail>();
                foreach (var item in GradeBookList)
                {
                    item.GradeBookGradeDetailList = GradeBookGradeDetailList.Where(g => g.GradebookId == item.GradeBookId).ToList();
                    item.GradeBookCourseDetailList = GradeBookCourseDetailList.Where(g => g.GradebookId == item.GradeBookId).ToList();
                    item.AssignmentQuizSetups = assignmentQuiz.Where(g => g.GradebookId == item.GradeBookId).ToList();
                    item.TotalCount = totalCount;
                }
                return GradeBookList;
            }
        }
        public async Task<IEnumerable<GradeAndCourse>> GetGradeAndCourseList(int curriculumId, long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                parameters.Add("@curriculumId", curriculumId);
                return await conn.QueryAsync<GradeAndCourse>("[Gradebook].[GetGradeAndCourseList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<GradebookGrade>> GetGradeListExceptGradebookGrade(long SchoolId, long GradebookId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                parameters.Add("@GradebookId", GradebookId, DbType.Int64);
                return await conn.QueryAsync<GradebookGrade>("[Gradebook].[GetGradeListExceptGradebookGrade]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        #endregion

        #region Grade book Formula
        public async Task<bool> GradebookFormulaCU(GradebookFormula gradebookFormula)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@formulaId", gradebookFormula.FormulaId);
                parameters.Add("@formula", gradebookFormula.Formula);
                parameters.Add("@formulaDesc", gradebookFormula.FormulaDescription);
                parameters.Add("@schoolId", gradebookFormula.SchoolId);
                parameters.Add("@assessments", gradebookFormula.Assessments);
                return await conn.QueryFirstOrDefaultAsync<bool>("Gradebook.GradeBookFormulaCU", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<GradebookFormula>> GetGradebookFormulas(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId);
                return await conn.QueryAsync<GradebookFormula>("Gradebook.GetGradebookformula", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<GradebookFormula> GetGradebookFormulaDetailById(long FormulaId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FormulaId", FormulaId);
                var result = await conn.QueryMultipleAsync("Gradebook.GetGradebookFormulaDetailById", parameters, commandType: CommandType.StoredProcedure);
                var gradebookFormula = await result.ReadFirstOrDefaultAsync<GradebookFormula>();
                gradebookFormula = gradebookFormula ?? new GradebookFormula();
                var buttons = await result.ReadAsync<GradebookFormulaButtons>();
                if (buttons != null)
                {
                    gradebookFormula.Buttons = buttons.ToDictionary(x => x.BtnText, x => "$" + x.BtnVal);
                }
                return gradebookFormula;
            }
        }
        #endregion

        #region Grade book Rule Processing Setup
        public async Task<string> ProcessingRuleSetupCU(GradebookRuleSetup gradebookRuleSetup)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@setupId", gradebookRuleSetup.ProcessingRuleSetupId);
                parameters.Add("@InternalAssessmentId", gradebookRuleSetup.InternalAssessmentId);
                parameters.Add("@Formula", gradebookRuleSetup.Formula);
                parameters.Add("@bestofcount", gradebookRuleSetup.BestOfCount);
                parameters.Add("@CalculationTypeId", gradebookRuleSetup.CalculationTypeId);
                parameters.Add("@CreatedBy", gradebookRuleSetup.CreatedBy);
                DataTable dt = new DataTable();
                dt.Columns.Add("PrimaryId", typeof(long));
                dt.Columns.Add("TypeId", typeof(long));
                foreach (var item in gradebookRuleSetup.RuleSetupMappings)
                {
                    dt.Rows.Add(item.SubInternalAssessmentId, item.Weightage);
                }
                parameters.Add("@subAssessment", dt, DbType.Object);
                parameters.Add("@output", direction: ParameterDirection.Output, dbType: DbType.String, size: int.MaxValue);
                await conn.QueryAsync("Gradebook.ProcessingRuleSetupCU", parameters, commandType: CommandType.StoredProcedure);
                var id = parameters.Get<string>("output");
                return id;
            }
        }

        public async Task<IEnumerable<GradebookRuleSetup>> GetGradebookRuleSetups(string assessmentIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@internalAssessmentId", assessmentIds);
                var result = await conn.QueryMultipleAsync("Gradebook.GetProcessingRuleSetup", parameters, commandType: CommandType.StoredProcedure);
                var ruleSetups = await result.ReadAsync<GradebookRuleSetup>();
                var teacherAssessment = await result.ReadAsync<TeacherInternalAssessment>();
                var mapping = await result.ReadAsync<RuleSetupMapping>();
                var ruleSetupsList = ruleSetups.ToList();
                if (teacherAssessment.Any() && !ruleSetupsList.Any())
                {
                    ruleSetupsList = new List<GradebookRuleSetup>() {
                        new GradebookRuleSetup{ TeacherInternalAssessments = teacherAssessment.ToList(), RuleSetupMappings = mapping.ToList()} };
                }
                else
                {
                    ruleSetupsList.ForEach(x =>
                    {
                        x.TeacherInternalAssessments = teacherAssessment.Where(e => e.ParentInternalAssessmentId == x.InternalAssessmentId).ToList();
                        x.RuleSetupMappings = mapping.Where(y => y.ProcessingRuleSetupId == x.ProcessingRuleSetupId).ToList();
                    });
                }
                return ruleSetupsList;
            }
        }
        public async Task<bool> DeleteRuleProcessingSetup(long ruleSetupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@setupId", ruleSetupId);
                parameters.Add("@output", direction: ParameterDirection.Output, dbType: DbType.Boolean);
                await conn.QueryFirstOrDefaultAsync<bool>("[Gradebook].[DeleteRuleProcessingSetup]", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<bool>("output");
            }
        }
        #endregion

        public async Task<IEnumerable<GradingTemplateItem>> GetGradingTemplatesByIds(string ids)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ids", ids, DbType.String);
                return await conn.QueryAsync<GradingTemplateItem>("[Gradebook].[GetGradingTemplates]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<GradeBookGradeDetail>> ValidateGradeAndCourse(string courseIds, string gradeIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGradeIds", gradeIds);
                parameters.Add("@CourseIds", courseIds);
                return await conn.QueryAsync<GradeBookGradeDetail>("[GradeBook].[ValidateGradeAndCourseCombination]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
    }
}
