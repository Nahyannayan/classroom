﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories.Setting.Contract
{
    public interface ISettingRepository
    {
        Task<IEnumerable<NotificationTrigger>> GetEnableDisableNotificationListForParent(long parentId);
        int SaveNotificationSettingForParent(long parentId, NotificationSettings model);
    }
}
