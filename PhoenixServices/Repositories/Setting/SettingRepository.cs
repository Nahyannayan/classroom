﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Repositories.Setting.Contract;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories.Setting
{
    public class SettingRepository : SqlRepository<NotificationTrigger>, ISettingRepository
    {
        private readonly IConfiguration _config;
        public SettingRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<NotificationTrigger>> GetEnableDisableNotificationListForParent(long parentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ParentId", parentId, DbType.Int64);
                return await conn.QueryAsync<NotificationTrigger>("dbo.GetEnableDisableNotificationListForParent", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int SaveNotificationSettingForParent(long parentId, NotificationSettings model)
        {
            var parameters = new DynamicParameters();
            if (model.NotificationList.Count > 0 )
            {
                DataTable dtTemplateField = new DataTable();
                dtTemplateField.Columns.Add("ParentId", typeof(Int64));
                dtTemplateField.Columns.Add("NotificationTriggerId", typeof(Int64));
                dtTemplateField.Columns.Add("TriggerName", typeof(string));
                dtTemplateField.Columns.Add("ModuleName", typeof(string));
                dtTemplateField.Columns.Add("IsEmailNotificatioEnable", typeof(bool));
                dtTemplateField.Columns.Add("IsPushNotificatioEnable", typeof(bool));
   

                foreach (var item in model.NotificationList)
                {
                    dtTemplateField.Rows.Add(parentId, item.NotificationTriggerId, item.TriggerName, item.ModuleName, item.IsEmailNoficationEnable, item.IsPushNoficationEnable );
                }

                parameters.Add("@NotificationSetting", dtTemplateField, DbType.Object);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            }
            using (var conn = GetOpenConnection())
            {
                 conn.Query<Int32>("dbo.SaveNotificatioSettingForParent", parameters, commandType: CommandType.StoredProcedure);
                return  parameters.Get<Int32>("Output");
            }
        }
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<NotificationTrigger>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<NotificationTrigger> GetAsync(int id)
        {
            throw new NotImplementedException();
        }


        public override void InsertAsync(NotificationTrigger entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(NotificationTrigger entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
