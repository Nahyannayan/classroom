﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class ObservationRepository : SqlRepository<Observation>, IObservationRepository
    {
        private readonly IConfiguration _config;
        public ObservationRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        #region Overided Methos from SqlRepository
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<Observation>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<Observation> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(Observation entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(Observation entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion
        #region Observation methods
        public bool UpdateObservationData(Observation entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetObservationParameters(entity, mode);
                conn.Query<int>("[Observation].[ObservationCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<Observation> GetObservationById(int id, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ObservationId", id, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                var result= await conn.QueryMultipleAsync("[Observation].[GetObservationByID]", parameters, commandType: CommandType.StoredProcedure);
                var observationDetails = await result.ReadFirstAsync<Observation>();
                var observationFiles = await result.ReadAsync<ObservationFile>();
                var observationObjective = await result.ReadAsync<Objective>();
                observationDetails.lstObservationFiles = observationFiles.ToList();
                observationDetails.lstObjectives = observationObjective.ToList();
                return observationDetails;
            }
        }
        public async Task<IEnumerable<ObservationFile>> GetObservationFilesById(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ObservationId", id, DbType.Int32);
                return await conn.QueryAsync<ObservationFile>("[Observation].[GetObservationFilesbyId]", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Objective>> GetObservationObjectives(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ObservationId", id, DbType.Int32);
                return await conn.QueryAsync<Objective>("[Observation].[GetObservationObjective]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolGroup>> GetSelectedGroupsByObservationId(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ObservationId", id, DbType.Int32);
                return await conn.QueryAsync<SchoolGroup>("[Observation].[GetSchoolGroupsByObservationId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ObservationStudent>> GetObservationStudent(int observationId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ObservationId", observationId, DbType.Int32);
                return await conn.QueryAsync<ObservationStudent>("[Observation].[GetObservationStudentsByObservationId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ObservationFile>> GetObservationMyFiles(long id, bool isFolder)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileId", id, DbType.Int64);
                parameters.Add("@IsFolder", isFolder, DbType.Boolean);
                return await conn.QueryAsync<ObservationFile>("[Observation].[GetMyFileAsObservationFile]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Observation>> GetObservationTeacherPaging(int pageNumber, int pageSize, long teacherId, string searchString, string schoolGroupIds,string sortBy)
        {
            var observationDetail = new List<Observation>(); DataTable dt = new DataTable();
            dt.Columns.Add("GroupStudentId");
            dt.TableName = "tblStudentGroupId";
            if (!string.IsNullOrEmpty(schoolGroupIds))
            {
                List<int> groupIds = schoolGroupIds.Split(',').Select(int.Parse).ToList();
                foreach (var item in groupIds)
                {
                    dt.Rows.Add(item);
                }
            }
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@intTeacherId", teacherId, DbType.Int32);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                parameters.Add("@schoolGroupIds", dt, DbType.Object);
                parameters.Add("@sortBy", sortBy, DbType.String);
                var result = await conn.QueryMultipleAsync("[Observation].[GetObservationByPage]", parameters, null, null, CommandType.StoredProcedure);
                var observationDetails = await result.ReadAsync<Observation>();
                var observationFiles = await result.ReadAsync<ObservationFile>();
                var schoolGroups = await result.ReadAsync<SchoolGroup>();
                var courses = await result.ReadAsync<Course>();
                var observationCount = await result.ReadAsync<int>();
                observationDetail = observationDetails.ToList();
                var totalCount = observationCount.FirstOrDefault();
                if (observationDetail.Any())
                {
                    observationDetail.ForEach(x => {
                        x.TotalCount = totalCount;
                        x.lstObservationFiles = observationFiles.Where(f => x.ObservationId == f.ObservationId).ToList();
                        x.lstSchoolGroups = schoolGroups.Where(f => x.ObservationId == f.ObservationId).ToList();
                        x.lstCourses = courses.Where(f => x.ObservationId == f.ObservationId).ToList();
                    });
                }
                return observationDetail;
            }
        }

        public async Task<IEnumerable<ObservationStudent>> GetObservationStudentPaging(int pageNumber, int pageSize, long studentId, string searchString,string sortBy)
        {
            var observationDetail = new List<ObservationStudent>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@intStudentId", studentId, DbType.Int32);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                parameters.Add("@sortBy", sortBy, DbType.String);
                var result = await conn.QueryMultipleAsync("[Observation].[GetStudentObservationByPage]", parameters, null, null, CommandType.StoredProcedure);
                var observationDetails = await result.ReadAsync<ObservationStudent>();
                var observationFiles = await result.ReadAsync<ObservationFile>();
                var observationCount = await result.ReadAsync<int>();
                observationDetail = observationDetails.ToList();
                int totalCount = observationCount.FirstOrDefault();
                if (observationDetail.Any())
                {
                    observationDetail.ForEach(x => { x.TotalCount = totalCount;x.lstObservationFiles = observationFiles.Where(f => x.ObservationId == f.ObservationId).ToList(); });
                }
                return observationDetail;

            }
        }

        public bool DeleteObservationFile(int fileId, int observationId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@observationfileId", fileId, DbType.Int32);
                parameters.Add("@observationId", observationId, DbType.Int32);
                parameters.Add("@deleteby", userId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Observation.DeleteUploadedFiles", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<Subject>> GetObservationSubjects(int observationId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ObservationId", observationId, DbType.Int32);
                return await conn.QueryAsync<Subject>("Observation.GetSubjects", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<File> GetObservationFilebyObservationFileId(long id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ObservationFileId", id, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<File>("Observation.GetObservationFilebyObservationFileId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Course>> GetSchoolCoursesByObservation(int observationId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ObservationId", observationId, DbType.Int32);
                return await conn.QueryAsync<Course>("Observation.GetSchoolCoursesByObservation", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        #endregion
        #region Private Methods
        private DynamicParameters GetObservationParameters(Observation entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@ObservationId", entity.ObservationId, DbType.Int32);
            parameters.Add("@ObservationTitle", entity.ObservationTitle, DbType.String);
            parameters.Add("@ObservationDesc", entity.ObservationDesc, DbType.String);
            parameters.Add("@CourseIds", entity.Courses, DbType.String);
            parameters.Add("@StartDate", entity.StartDate, DbType.DateTime);
            parameters.Add("@IsPublished", entity.IsPublished, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedById, DbType.Int32);
            parameters.Add("@StudentIds", entity.StudentIdsToAdd, DbType.String);
            parameters.Add("@SchoolGroupIds", entity.SchoolGroups, DbType.String);
            parameters.Add("@IsGradingEnable", entity.IsGradingEnable, DbType.Boolean);
            parameters.Add("@GradingTemplateId", entity.GradingTemplateId, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            if (entity.lstObservationFiles != null)
            {
                if (entity.lstObservationFiles.Count > 0)
                {
                    DataTable dtObservationFiles = new DataTable();
                    dtObservationFiles.Columns.Add("ObservationFileId", typeof(Int64));
                    dtObservationFiles.Columns.Add("ObservationId", typeof(Int64));
                    dtObservationFiles.Columns.Add("FileName", typeof(string));
                    dtObservationFiles.Columns.Add("UploadedFileName", typeof(string));
                    dtObservationFiles.Columns.Add("FileTypeId", typeof(Int32));
                    dtObservationFiles.Columns.Add("FileSizeInMB", typeof(double));
                    dtObservationFiles.Columns.Add("GoogleDriveFileId", typeof(string));
                    dtObservationFiles.Columns.Add("ResourceFileTypeId", typeof(Int16));
                    dtObservationFiles.Columns.Add("PhysicalFilePath", typeof(string));
                    if (entity.lstObservationFiles != null)
                    {
                        foreach (var item in entity.lstObservationFiles)
                        {
                            dtObservationFiles.Rows.Add(item.ObservationFileId, entity.ObservationId, item.FileName, item.UploadedFileName,item.FileTypeId,item.FileSizeInMB,item.GoogleDriveFileId,item.ResourceFileTypeId,item.PhysicalFilePath);
                        }
                    }
                    parameters.Add("@ObservationFileDetails", dtObservationFiles, DbType.Object);

                }
            }

            if (entity.lstObjectives == null)
            {
                parameters.Add("@isInsertObjective", 0, DbType.Boolean);
            }
            else
            {
                if (entity.lstObjectives.Count > 0)
                {
                    DataTable dtObjective = new DataTable();
                    dtObjective.Columns.Add("ObjectiveId", typeof(Int64));
                    dtObjective.Columns.Add("IsSelected", typeof(Boolean));
                    if (entity.lstObjectives != null)
                    {
                        foreach (var item in entity.lstObjectives)
                        {
                            dtObjective.Rows.Add(item.ObjectiveId, item.isSelected);
                        }
                    }
                    parameters.Add("@isInsertObjective", 1, DbType.Boolean);
                    parameters.Add("@Objective", dtObjective, DbType.Object);
                }
                else {
                    parameters.Add("@isInsertObjective", 0, DbType.Boolean);
                }
            }
            return parameters;
        }

        #endregion
        #region ArchivedObservation
        public async Task<IEnumerable<Observation>> GetArchivedObservationTeacherPaging(int pageNumber, int pageSize, long teacherId, string searchString)
        {
            var observationDetail = new List<Observation>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@intTeacherId", teacherId, DbType.Int32);
                parameters.Add("@SearchString", searchString == null ? "" : searchString, DbType.String);
                var result = await conn.QueryMultipleAsync("[Observation].[GetArchivedObservationByPage]", parameters, null, null, CommandType.StoredProcedure);
                var observationDetails = await result.ReadAsync<Observation>();
                var observationCount = await result.ReadAsync<int>();
                observationDetail = observationDetails.ToList();
                int totalCount = observationCount.FirstOrDefault();
                if (observationDetail.Any())
                {
                    observationDetail.ForEach(x => { x.TotalCount = totalCount; });
                }
                return observationDetail;
            }
        }

        public async Task<Observation> GetObservationByUserID(int teacherId)
        {
            Observation objObservation = new Observation();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TeacherId", teacherId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[Observation].[GetObservationByUserID]", parameters, null, null, CommandType.StoredProcedure);
                var listOfActiveObservation = await result.ReadAsync<Observation>();
                var listOfArchivedObservation = await result.ReadAsync<Observation>();
                objObservation.lstActiveObservation = listOfActiveObservation.ToList();
                objObservation.lstArchivedObservation = listOfArchivedObservation.ToList();
                return objObservation;
            }
        }
        public bool UpdateActiveObservation(long ObservationId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ObservationId", ObservationId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<bool>("[Observation].[UpdateArchiveObservation]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        #endregion
    }
}
