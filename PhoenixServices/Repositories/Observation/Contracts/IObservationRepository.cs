﻿using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IObservationRepository
    {
        bool UpdateObservationData(Observation entity, TransactionModes mode);
        Task<Observation> GetObservationById(int id, long userId);
        Task<IEnumerable<ObservationFile>> GetObservationFilesById(int id);
        Task<IEnumerable<Objective>> GetObservationObjectives(int id);
        Task<IEnumerable<SchoolGroup>> GetSelectedGroupsByObservationId(int id);
        Task<IEnumerable<ObservationStudent>> GetObservationStudent(int observationId);
        Task<IEnumerable<ObservationFile>> GetObservationMyFiles(long id, bool isFolder);
        Task<IEnumerable<Observation>> GetObservationTeacherPaging(int pageNumber, int pageSize, long teacherId, string searchString, string schoolGroupIds,string sortBy);
        Task<IEnumerable<ObservationStudent>> GetObservationStudentPaging(int pageNumber, int pageSize, long studentId, string searchString,string sortBy);
        bool DeleteObservationFile(int fileId, int observationId, long userId);
        Task<IEnumerable<Subject>> GetObservationSubjects(int observationId);
        Task<File> GetObservationFilebyObservationFileId(long id);
        Task<IEnumerable<Observation>> GetArchivedObservationTeacherPaging(int pageNumber, int pageSize, long teacherId, string searchString);
        Task<Observation> GetObservationByUserID(int teacherId);
        bool UpdateActiveObservation(long ObservationId);
        Task<IEnumerable<Course>> GetSchoolCoursesByObservation(int observationId);
    }
}
