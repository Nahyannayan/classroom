﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class CountryRepository: SqlRepository<Country>, ICountryRepository
    {
        private readonly IConfiguration _config;

        public CountryRepository(IConfiguration configuration): base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override async Task<IEnumerable<Country>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                return await conn.QueryAsync<Country>("Admin.GetCountries", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override async Task<Country> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CountryId", id, DbType.Int32);
                return await conn.QueryFirstAsync<Country>("Admin.GetCountries", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(Country entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(Country entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
