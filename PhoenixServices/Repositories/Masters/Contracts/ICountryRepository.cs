﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Phoenix.API.Repositories
{
    public interface ICountryRepository: IGenericRepository<Country>
    {
    }
}
