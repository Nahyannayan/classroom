﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models.DashBoard;
using Phoenix.API.Repositories;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class DashBoardGroupsRepository : SqlRepository<DashBoardGroups>, IDashBoardGroupsRepository
    {

        private readonly IConfiguration _config;

        public DashBoardGroupsRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<DashBoardGroups>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<DashBoardGroups> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<DashBoardGroups>> getOtherGroups(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int32);
                var result = await conn.QueryAsync<DashBoardGroups>("School.TeachersDashBoardOtherGroups", parameters, null, null, CommandType.StoredProcedure);
                return result;
            }
        }

        public async Task<IEnumerable<DashBoardGroups>> getSchoolGroups(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int32);
                var result = await conn.QueryAsync<DashBoardGroups>("School.TeachersDashBoardSchoolGroups", parameters, null, null, CommandType.StoredProcedure);
                return result;
            }
        }

        public override void InsertAsync(DashBoardGroups entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(DashBoardGroups entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
