﻿using DbConnection;
using Phoenix.API.Models.DashBoard;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
   public interface IDashBoardGroupsRepository : IGenericRepository<DashBoardGroups>
    {
        Task<IEnumerable<DashBoardGroups>> getSchoolGroups(int id);
        Task<IEnumerable<DashBoardGroups>> getOtherGroups(int id);

    }
}
