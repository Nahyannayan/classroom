﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IQuizQuestionsRepository
    {
        bool QuizQuestionsInsert(QuizQuestionsView quizQuestionsView, DataTable answers, DataTable quizQuestionFile);
        int GetIdByQuestionType(string questionType);
        bool ImportQuizQuestion(DataTable quizQuestions, DataTable Answers, ImportQuizView importQuizView);
        bool QuizQuestionsUpdate(QuizQuestionsView quizQuestionsView, DataTable answers, DataTable quizQuestionFile);
        Task<QuizQuestionsView> GetQuizQuestionById(int quizQuestionId);
        bool InsertUpdatePoolQuestions(QuizQuestionsView quizQuestionsView);
        Task<IEnumerable<QuizQuestionsView>> GetQuizQuestionsByQuizId(int quizId, bool IsTeacher, short languageId);
        Task<IEnumerable<QuizQuestionsView>> GetQuizQuestionsPaginationByQuizId(int quizId, int PageNumber, int PageSize);
  
        Task<IEnumerable<QuizQuestionsView>> GetAllQuizQuestions();
        Task<IEnumerable<QuizQuestionFiles>> GetFilesByQuizQuestionId(int quizQuestionId);
        Task<IEnumerable<QuizQuestionsView>> GetFilteredQuizQuestions(string courseIds, string lstObjectives);
        bool UpdateQuizCorrectAnswerByQuizAnswerId(QuizAnswersView quizAnswersView);
        Task<IEnumerable<Objective>> GetQuestionObjectives(int questionId);
        Task<IEnumerable<Subject>> GetQuestionSubjects(int questionId);
        Task<QuizQuestionFiles> GetFileByQuizQuestionFileId(int quizQuestionFileId);
        bool SortQuestionByOrder(QuizQuestionsView quizAnswersView, DataTable sortedIds);
        Task<IEnumerable<QuizAnswersView>> GetQuizAnswerByQuizQuestionId(int quizQuestionId);
        bool QuizQuestionsDelete(QuizQuestionsView quizQuestionsView, DataTable answers);
        int DeleteQuizQuestionFile(int quizQuestionFileId);
        int DeleteQuizQuestion(int quizQuestionId, int quizId);
        bool UpdateQuizAnswerData(QuizResponse quizResponse, DataTable Quizanswers);
        int DeleteMTPResource(int QuizAnswerId);
        Task<QuizResponse> GetQuizResponseByUserId(int quizId, int userId,int studentId, int taskId);
    }
}
