﻿using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IQuizRepository
    {
        Task<IEnumerable<QuizView>> GetAllQuiz(long schoolId);
        Task<QuizView> GetQuiz(int quizId,long schoolId);
        int DeleteQuestionAnswerFile(int quizFileId, long userId);
        Task<QuizView> GetQuizDetails(int quizId, int quizResourceId, long schoolId);
        Task<QuizView> GetQuizResourseDetail(int resourseId, string resourseType);
        bool QuizCUD(QuizView model, TransactionModes mode, DataTable QuizFiles);
        bool UploadQuestionAnswerFiles(DataTable QuestionAnswerFiles);
        int LogQuizTime(int id, int resourceId, string resourceType, long userId, bool IsStartQuiz);
        Task<IEnumerable<QuizFile>> GetFilesByQuizId(int quizId);
        Task<IEnumerable<Subject>> GetQuizSubjectsGrade(int quizId);
        Task<IEnumerable<QuizQuestionAnswerFiles>> GetQuizQuestionAnswerFiles(long resourceId, string resourceType, long questionId, long studentId);
        int DeleteQuizFile(int quizFileId);
        bool ActiveDeactiveQuiz(int QuizId);
        Task<QuizFile> GetFileByQuizFileId(int quizFileId);
        Task<QuizReport> GetQuizReport(int quizId, long userId);
        Task<QuizReport> GetGroupQuizReport(int quizId, int groupQuizId);
        Task<IEnumerable<GroupQuiz>> GetAllQuizByStudentId(long studentId);
        Task<QuizPagination> GetPaginateQuizByUser(QuizPaginationRequest quizView);
        Task<IEnumerable<QuizView>> GetAllQuizByUser(long Id, bool isForm);
        Task<IEnumerable<QuizView>> GetAllQuizByUserAndSource(long Id, long sourceId, string sourceType, bool isForm);
        Task<IEnumerable<QuizDetails>> GetQuizDetailsByQuizIdAndUserId(long userId, int quizId);
        Task<QuizView> GetTaskQuizDetail(int quizId, long taskId, long schoolId);
        Task<IEnumerable<QuizAnswer>> GetAnswersByQuestionId(long userId, int questionId);
        int GetQuizQuestionsCount(long userId, int quizId);
        Task<IEnumerable<Objective>> GetQuizObjectives(int quizId);
        Task<bool> ActiveDeactiveOTP(int QuizId);
        Task<bool> ActiveDeactiveSafeQuiz(int QuizId);
        Task<bool> isSafeTest(int QuizId);
        int CloneQuizData(int quizId);
        int ShareQuizData(int quizId, string teacherIds,  long SharedBy);
        Task<QuizResult> GetQuizReportCardByUserId(int quizId, int userId, int resourceId, string resourceType);
    }
}
