﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;

namespace Phoenix.API.Repositories
{
    public class QuizQuestionsRepository : SqlRepository<QuizQuestionsView>, IQuizQuestionsRepository
    {
        private readonly IConfiguration _config;

        public QuizQuestionsRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override Task<IEnumerable<QuizQuestionsView>> GetAllAsync()
        {
            throw new NotImplementedException();
        }
        public override Task<QuizQuestionsView> GetAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override void InsertAsync(QuizQuestionsView entity)
        {
            throw new NotImplementedException();
        }
        public override void UpdateAsync(QuizQuestionsView entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public bool QuizQuestionsInsert(QuizQuestionsView quizQuestionsView, DataTable answers, DataTable quizQuestionFile)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizQuestionsView.QuizId, DbType.Int32);
                parameters.Add("@QuestionTypeId", quizQuestionsView.QuestionTypeId, DbType.Int32);
                parameters.Add("@QuestionText", quizQuestionsView.QuestionText, DbType.String);
                parameters.Add("@IsRequired", quizQuestionsView.IsRequired, DbType.Boolean);
                parameters.Add("@IsVisible", quizQuestionsView.IsVisible, DbType.Boolean);
                parameters.Add("@CreatedBy", quizQuestionsView.CreatedBy, DbType.Int32);
                parameters.Add("@AnswerTable", answers, DbType.Object);
                parameters.Add("@QuizQuestionFile", quizQuestionFile, DbType.Object);
                parameters.Add("@Marks", quizQuestionsView.Marks, DbType.Int32);
                parameters.Add("@QuestionImagePath", quizQuestionsView.QuestionImagePath, DbType.String);
                parameters.Add("@CourseIds", quizQuestionsView.Courses, DbType.String);
                if (quizQuestionsView.lstObjectives == null)
                {
                    parameters.Add("@isInsertObjective", 0, DbType.Boolean);
                }
                else
                {
                    if (quizQuestionsView.lstObjectives.Count > 0)
                    {
                        DataTable dtObjective = new DataTable();
                        dtObjective.Columns.Add("ObjectiveId", typeof(Int64));
                        dtObjective.Columns.Add("IsSelected", typeof(Boolean));
                        if (quizQuestionsView.lstObjectives != null)
                        {
                            foreach (var item in quizQuestionsView.lstObjectives)
                            {
                                dtObjective.Rows.Add(item.ObjectiveId, item.isSelected);
                            }
                        }
                        parameters.Add("@isInsertObjective", 1, DbType.Boolean);
                        parameters.Add("@Objective", dtObjective, DbType.Object);
                    }
                    else
                    {
                        parameters.Add("@isInsertObjective", 0, DbType.Boolean);
                    }
                }
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("school.InsertQuizQuestion", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public int GetIdByQuestionType(string questionType)
        {
            int result = 0;
            try
            {
                using (var conn = GetOpenConnection())
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@QuestionType", questionType, DbType.String);
                    parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    conn.Query<int>("[School].[GetIdByQuestionType]", parameters, commandType: CommandType.StoredProcedure);
                    return parameters.Get<int>("output");
                }
            }
            catch (Exception)
            {
                return result;
            }
        }
        public bool ImportQuizQuestion(DataTable quizQuestions, DataTable Answers, ImportQuizView importQuizView)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", importQuizView.QuizId, DbType.Int32);
                parameters.Add("@CreatedBy", importQuizView.CreatedBy, DbType.Int32);
                parameters.Add("@QuestionTable", quizQuestions, DbType.Object);
                parameters.Add("@AnswerTable", Answers, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.ImportQuizQuestion", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public bool InsertUpdatePoolQuestions(QuizQuestionsView quizQuestionsView)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SelectedQuestionIds", quizQuestionsView.SelectedQuestionId, DbType.String);
                parameters.Add("@DeselectedQuestionId", quizQuestionsView.DeselectedQuestionId, DbType.String);
                parameters.Add("@QuizId", quizQuestionsView.QuizId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[InsertUpdatePoolQuestions]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public bool QuizQuestionsUpdate(QuizQuestionsView quizQuestionsView, DataTable answers, DataTable quizQuestionFile)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizQuestionId", quizQuestionsView.QuizQuestionId, DbType.Int32);
                parameters.Add("@QuizId", quizQuestionsView.QuizId, DbType.Int32);
                parameters.Add("@QuestionTypeId", quizQuestionsView.QuestionTypeId, DbType.Int32);
                parameters.Add("@QuestionText", quizQuestionsView.QuestionText, DbType.String);
                parameters.Add("@IsRequired", quizQuestionsView.IsRequired, DbType.Boolean);
                parameters.Add("@IsVisible", quizQuestionsView.IsVisible, DbType.Boolean);
                parameters.Add("@UpdatedBy", quizQuestionsView.UpdatedBy, DbType.Int32);
                parameters.Add("@AnswerTable", answers, DbType.Object);
                parameters.Add("@QuizQuestionFile", quizQuestionFile, DbType.Object);
                parameters.Add("@Marks", quizQuestionsView.Marks, DbType.Int32);
                parameters.Add("@QuestionImagePath", quizQuestionsView.QuestionImagePath, DbType.String);
                parameters.Add("@CourseIds", quizQuestionsView.Courses, DbType.String);
                if (quizQuestionsView.lstObjectives == null)
                {
                    parameters.Add("@isInsertObjective", 0, DbType.Boolean);
                }
                else
                {
                    if (quizQuestionsView.lstObjectives.Count > 0)
                    {
                        DataTable dtObjective = new DataTable();
                        dtObjective.Columns.Add("ObjectiveId", typeof(Int64));
                        dtObjective.Columns.Add("IsSelected", typeof(Boolean));
                        if (quizQuestionsView.lstObjectives != null)
                        {
                            foreach (var item in quizQuestionsView.lstObjectives)
                            {
                                dtObjective.Rows.Add(item.ObjectiveId, item.isSelected);
                            }
                        }
                        parameters.Add("@isInsertObjective", 1, DbType.Boolean);
                        parameters.Add("@Objective", dtObjective, DbType.Object);
                    }
                    else
                    {
                        parameters.Add("@isInsertObjective", 0, DbType.Boolean);
                    }
                }
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("school.UpdateQuizQuestionById", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<QuizQuestionsView> GetQuizQuestionById(int quizQuestionId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizQuestionId", quizQuestionId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[school].[GetQuizQuestionById]", parameters, null, null, CommandType.StoredProcedure);
                var quizQuestionList = await result.ReadAsync<QuizQuestionsView>();
                var answerList = await result.ReadAsync<Answers>();
                var courseList = await result.ReadAsync<Course>();
                var quizQuestion = quizQuestionList.FirstOrDefault();
                quizQuestion.Answers = answerList.ToList();
                quizQuestion.lstCourse = courseList.ToList();
                return quizQuestion;
            }
        }

        public async Task<IEnumerable<QuizQuestionsView>> GetQuizQuestionsByQuizId(int quizId, bool IsTeacher,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@IsTeacher", IsTeacher, DbType.Boolean);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<QuizQuestionsView>("[school].[GetQuestionsByQuizId]", parameters, null, null, CommandType.StoredProcedure);
                
            }
        }
   
        public async Task<IEnumerable<QuizQuestionsView>> GetQuizQuestionsPaginationByQuizId(int quizId, int PageNumber, int PageSize)
        {
            using (var conn = GetOpenConnection())
            {
                var quizQuestionsView = new List<QuizQuestionsView>();
                var parameters = new DynamicParameters();
                parameters.Add("@PageNum", PageNumber, DbType.Int32);
                parameters.Add("@PageSize", PageSize, DbType.Int32);
                parameters.Add("@QuizId", quizId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[School].[GetPagingQuestionsByQuizId]", parameters, null, null, CommandType.StoredProcedure);
                var quizQuestionsDetails = await result.ReadAsync<QuizQuestionsView>();
                var objCount = await result.ReadAsync<int>();
                quizQuestionsView = quizQuestionsDetails.ToList();
                int totalCount = objCount.FirstOrDefault();
                if (quizQuestionsView.Any())
                {
                    quizQuestionsView.ForEach(x => { x.TotalCount = totalCount; });
                }
                return quizQuestionsView;
                //return await conn.QueryAsync<QuizQuestionsView>("[School].[GetPagingQuestionsByQuizId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<QuizQuestionsView>> GetAllQuizQuestions()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<QuizQuestionsView>("[School].[GetAllQuizQuestions]", CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<QuizQuestionsView>> GetFilteredQuizQuestions(string courseIds, string lstObjectives)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CourseIds", courseIds, DbType.String);
                parameters.Add("@Objectives", lstObjectives, DbType.String);
                return await conn.QueryAsync<QuizQuestionsView>("[School].[GetFilteredQuizQuestions]", parameters, null, null, CommandType.StoredProcedure);

            }
        }
        public async Task<IEnumerable<QuizQuestionFiles>> GetFilesByQuizQuestionId(int quizQuestionId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizQuestionId", quizQuestionId, DbType.Int64);
                return await conn.QueryAsync<QuizQuestionFiles>("[School].[GetFilesByQuizQuestionId]", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public async Task<QuizQuestionFiles> GetFileByQuizQuestionFileId(int quizQuestionFileId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizQuestionFileId", quizQuestionFileId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<QuizQuestionFiles>("[School].[GetFileByQuizQuestionFileId]", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<Subject>> GetQuestionSubjects(int questionId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizQuestionId", questionId, DbType.Int32);
                return await conn.QueryAsync<Subject>("[School].[GetQuestionSubjects]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Objective>> GetQuestionObjectives(int questionId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizQuestionId", questionId, DbType.Int32);
                return await conn.QueryAsync<Objective>("[School].[GetQuestionObjective]", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public bool UpdateQuizCorrectAnswerByQuizAnswerId(QuizAnswersView quizAnswersView)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizAnswerId", quizAnswersView.QuizAnswerId, DbType.Int32);
                parameters.Add("@IsCorrect", quizAnswersView.IsCorrectAnswer, DbType.Boolean);
                parameters.Add("@QuestionType", quizAnswersView.QuestionTypeId, DbType.String);
                parameters.Add("@QuizQuestionId", quizAnswersView.QuizQuestionId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("school.STP_UpdateQuizCorrectAnswerByQuizAnswerId", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public bool SortQuestionByOrder(QuizQuestionsView quizQuestionsView, DataTable sortedIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizQuestionsView.QuizId, DbType.Int64);
                parameters.Add("@QuizQuestionSortedTable", sortedIds, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[SortQuestionByOrder]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<QuizAnswersView>> GetQuizAnswerByQuizQuestionId(int quizQuestionId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizQuestionId", quizQuestionId, DbType.Int32);
                return await conn.QueryAsync<QuizAnswersView>("[school].[stp_GetQuizAnswerByQuizQuestionId]", parameters, null, null, CommandType.StoredProcedure);

            }
        }
        public bool QuizQuestionsDelete(QuizQuestionsView quizQuestionsView, DataTable answers)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizQuestionId", quizQuestionsView.QuizQuestionId, DbType.Int32);
                parameters.Add("@DeletedBy", quizQuestionsView.UpdatedBy, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("school.DeleteQuizQuestionById", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public int DeleteQuizQuestionFile(int quizQuestionFileId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizQuestionFileId", quizQuestionFileId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[DeleteQuizQuestionFileById]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public int DeleteQuizQuestion(int quizQuestionId, int quizId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizQuestionId", quizQuestionId, DbType.Int32);
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[DeleteQuizQuestion]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        public bool UpdateQuizAnswerData(QuizResponse quizResponse ,DataTable Quizanswers)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizResponse.QuizId, DbType.Int32);
                parameters.Add("@QuizResponseId", quizResponse.QuizResponseId, DbType.Int32);
                parameters.Add("@QuizResponseByUserId", quizResponse.QuizResponseByUserId, DbType.Int32);
                parameters.Add("@QuizResponseByTeacherId", quizResponse.QuizResponseByTeacherId, DbType.Int32);
                parameters.Add("@QuizAnswerTable", Quizanswers, DbType.Object);
                parameters.Add("@StudentId", quizResponse.StudentId, DbType.Int32);
                parameters.Add("@TaskId", quizResponse.TaskId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[InsertQuizAnswer]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<QuizResponse> GetQuizResponseByUserId(int quizId,int userId, int studentId,int taskId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@StudentId", studentId, DbType.Int32);
                parameters.Add("@TaskId", taskId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[school].[GetQuizResponseByUserId]", parameters, null, null, CommandType.StoredProcedure);
                var quizResponseList = await result.ReadAsync<QuizResponse>();
                var questionAnswerList = await result.ReadAsync<QuizAnswersView>();
                var quizResponse = quizResponseList.FirstOrDefault();
                var questionAnswerFiles = await result.ReadAsync<QuizQuestionAnswerFiles>();
                if (quizResponse != null)
                {
                    quizResponse.ResponseQuestionAnswer = questionAnswerList.Any() ? questionAnswerList.ToList() : new List<QuizAnswersView>();
                    quizResponse.QuizQuestionAnswerFiles = questionAnswerFiles.ToList();
                }
                else
                {
                    quizResponse = new QuizResponse();
                }
                return quizResponse;
            }
        }
        public int DeleteMTPResource(int QuizAnswerId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizAnswerId", QuizAnswerId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[DeleteMTPResource]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
    }
}
