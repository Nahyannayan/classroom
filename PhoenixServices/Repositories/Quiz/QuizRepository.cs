﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.DataAccess;
using Phoenix.Common.Enums;
using Phoenix.Models;

namespace Phoenix.API.Repositories
{
    public class QuizRepository : SqlRepository<QuizView>, IQuizRepository
    {
        private readonly IConfiguration _config;

        public QuizRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        #region SQL
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<QuizView>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<QuizView> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(QuizView entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(QuizView entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion SQL

        #region Quiz
        public async Task<IEnumerable<QuizView>> GetAllQuiz(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<QuizView>("[school].[GetAllQuiz]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Objective>> GetQuizObjectives(int quizId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                return await conn.QueryAsync<Objective>("[School].[GetQuizObjectives]", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public int LogQuizTime(int id, int resourceId, string resourceType, long userId, bool isStartQuiz)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", id, DbType.Int32);
                parameters.Add("@ResourceId", resourceId, DbType.Int32);
                parameters.Add("@ResourceType", resourceType, DbType.String);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@IsStartQuiz", isStartQuiz, DbType.Boolean);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[InsertUpdateQuizTime]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        public int CloneQuizData(int quizId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[CloneQuizData]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");

            }
        }


        public int ShareQuizData(int quizId, string teacherIds, long SharedBy)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@TeacherIds", teacherIds, DbType.String);
                parameters.Add("@SharedBy", SharedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[ShareQuizWithTeachers]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        public bool QuizCUD(QuizView model, TransactionModes mode, DataTable QuizFiles)
        {
            if (mode == TransactionModes.Delete)
            {
                model.StartDate = DateTime.Now;
                model.EndDate = DateTime.Now;
            }
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", model.QuizId, DbType.Int32);
                parameters.Add("@TransMode", (int)mode, DbType.Int32);
                parameters.Add("@QuizName", model.QuizName, DbType.String);
                parameters.Add("@Description", model.Description, DbType.String);
                parameters.Add("@SchoolId", model.SchoolId, DbType.Int64);
                parameters.Add("@PassMarks", model.PassMarks, DbType.Int32);
                parameters.Add("@IsActive", model.IsActive, DbType.Boolean);
                parameters.Add("@IsRandomQuestion", model.IsRandomQuestion, DbType.Boolean);
                parameters.Add("@IsSetTime", model.IsSetTime, DbType.Boolean);
                parameters.Add("@UpdatedBy", model.UpdatedBy, DbType.Int32);
                parameters.Add("@IsForm", model.IsForm, DbType.Boolean);
                parameters.Add("@MaxSubmit", model.MaxSubmit, DbType.Int32);
                parameters.Add("@QuestionPaginationRange", model.QuestionPaginationRange, DbType.Int32);
                parameters.Add("@QuizTime", model.QuizTime, DbType.Int32);
                parameters.Add("@QuizImagePath", model.QuizImagePath, DbType.String);
                parameters.Add("@SectionId", model.SectionId, DbType.Int32);
                parameters.Add("@SectionType", model.SectionType, DbType.String);
                parameters.Add("@StartDate", model.StartDate, DbType.DateTime);
                parameters.Add("@StartTime", model.StartTime, DbType.String);
                parameters.Add("@EndDate", model.EndDate, DbType.DateTime);
                parameters.Add("@EndTime", model.EndTime, DbType.String);
                parameters.Add("@CourseIds", model.CourseIds, DbType.String);
                parameters.Add("@QuizFileDetails", QuizFiles, DbType.Object);
                parameters.Add("@FolderId", model.FolderId, DbType.Int64);
                parameters.Add("@ModuleId", model.ModuleId, DbType.Int32);
                if (model.lstObjectives == null)
                {
                    parameters.Add("@isInsertObjective", 0, DbType.Boolean);
                }
                else
                {
                    if (model.lstObjectives.Count > 0)
                    {
                        DataTable dtObjective = new DataTable();
                        dtObjective.Columns.Add("ObjectiveId", typeof(Int64));
                        dtObjective.Columns.Add("IsSelected", typeof(Boolean));
                        if (model.lstObjectives != null)
                        {
                            foreach (var item in model.lstObjectives)
                            {
                                dtObjective.Rows.Add(item.ObjectiveId, item.isSelected);
                            }
                        }
                        parameters.Add("@isInsertObjective", 1, DbType.Boolean);
                        parameters.Add("@Objective", dtObjective, DbType.Object);
                    }
                    else
                    {
                        parameters.Add("@isInsertObjective", 0, DbType.Boolean);
                    }
                }
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("school.QuizCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public bool UploadQuestionAnswerFiles(DataTable QuestionAnswerFiles)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuestionAnswerFileDetails", QuestionAnswerFiles, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("school.UploadQuestionAnswerFiles", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public bool ActiveDeactiveQuiz(int QuizId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", QuizId, DbType.Int32);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[ActiveDeactiveQuiz]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("Output") > 0;
            }
        }
        public async Task<IEnumerable<QuizFile>> GetFilesByQuizId(int quizId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int64);
                return await conn.QueryAsync<QuizFile>("[School].[GetFilesByQuizId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<QuizFile> GetFileByQuizFileId(int quizFileId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizFileId", quizFileId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<QuizFile>("[School].[GetFileByQuizFileId]", parameters, null, null, CommandType.StoredProcedure);

            }
        }
        public async Task<IEnumerable<QuizQuestionAnswerFiles>> GetQuizQuestionAnswerFiles(long resourceId, string resourceType, long questionId, long studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ResourceId", resourceId, DbType.Int64);
                parameters.Add("@ResourceType", resourceType, DbType.String);
                parameters.Add("@QuestionId", questionId, DbType.Int64);
                parameters.Add("@StudentId", studentId, DbType.Int64);
                return await conn.QueryAsync<QuizQuestionAnswerFiles>("[School].[GetQuizQuestionAnswerFiles]", parameters, null, null, CommandType.StoredProcedure);

            }
        }
        public async Task<IEnumerable<Subject>> GetQuizSubjectsGrade(int quizId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                return await conn.QueryAsync<Subject>("[School].[GetQuizSubjectsGrade]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<QuizReport> GetQuizReport(int quizId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var QuizReport = new QuizReport();
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                var result = await conn.QueryMultipleAsync("[School].[GetQuizReport]", parameters, null, null, CommandType.StoredProcedure);
                var QuizReportData = await result.ReadAsync<ReportData>();
                var QuizReportQuestions = await result.ReadAsync<QuizReportQuestions>();
                var QuizReportAnswers = await result.ReadAsync<QuizReportAnswers>();
                QuizReport.QuizReportData = QuizReportData.ToList();
                QuizReport.QuizReportQuestions = QuizReportQuestions.ToList();
                QuizReport.QuizReportAnswers = QuizReportAnswers.ToList();
                return QuizReport;
            }
        }
        public async Task<QuizReport> GetGroupQuizReport(int quizId, int groupQuizId)
        {
            using (var conn = GetOpenConnection())
            {
                var QuizReport = new QuizReport();
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@GroupQuizId", groupQuizId, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[School].[GetGroupWiseQuizReport]", parameters, null, null, CommandType.StoredProcedure);
                var QuizReportData = await result.ReadAsync<ReportData>();
                var QuizReportQuestions = await result.ReadAsync<QuizReportQuestions>();
                var QuizReportAnswers = await result.ReadAsync<QuizReportAnswers>();
                QuizReport.QuizReportData = QuizReportData.ToList();
                QuizReport.QuizReportQuestions = QuizReportQuestions.ToList();
                QuizReport.QuizReportAnswers = QuizReportAnswers.ToList();
                return QuizReport;
            }
        }
        public int DeleteQuizFile(int quizFileId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizFileId", quizFileId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[DeleteQuizFileById]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public int DeleteQuestionAnswerFile(int quizFileId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@FileId", quizFileId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[DeleteQuestionAnswerFile]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<QuizView> GetQuiz(int quizId, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                //return await conn.QueryFirstOrDefaultAsync<QuizView>("[school].[GetQuiz]", parameters, null, null, CommandType.StoredProcedure);
                var result = await conn.QueryMultipleAsync("[school].[GetQuiz]", parameters, null, null, CommandType.StoredProcedure);
                var quiz = await result.ReadAsync<QuizView>();
                var courseList = await result.ReadAsync<Course>();
                var quizDetails = quiz.FirstOrDefault();
                quizDetails.lstCourse = courseList.ToList();
                return quizDetails;
            }
        }
        public async Task<QuizView> GetQuizResourseDetail(int resourseId, string resourseType)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ResourseId", resourseId, DbType.Int32);
                parameters.Add("@ResourseType", resourseType, DbType.String);
                return await conn.QueryFirstOrDefaultAsync<QuizView>("[School].[GetQuizResourseDetail]", parameters, null, null, CommandType.StoredProcedure);
                //var result = await conn.QueryMultipleAsync("[school].[GetQuiz]", parameters, null, null, CommandType.StoredProcedure);
                //var quiz = await result.ReadAsync<QuizView>();
                //var courseList = await result.ReadAsync<Course>();
                //var quizDetails = quiz.FirstOrDefault();
                //quizDetails.lstCourse = courseList.ToList();
                //return quizDetails;
            }
        }
        public async Task<QuizView> GetQuizDetails(int quizId, int quizResourceId, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@QuizResourceId", quizResourceId, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<QuizView>("[School].[GetQuizDetails]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<QuizView> GetTaskQuizDetail(int quizId, long taskId, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@TaskId", taskId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<QuizView>("[School].[GetTaskQuizDetail]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<QuizView>> GetAllQuizByUser(long Id, bool isForm)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", Id, DbType.Int64);
                parameters.Add("@IsForm", isForm, DbType.Boolean);
                return await conn.QueryAsync<QuizView>("[school].[GetAllQuizByUser]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        //public async Task<QuizPagination> GetPaginateQuizByUser(QuizPaginationRequest constraints)
        //{
        //    using (var conn = GetOpenConnection())
        //    {
        //        var quizPagination = new QuizPagination();
        //        IList<DataParameter> selectParameters = new List<DataParameter>();
        //        var searchString = constraints == null || string.IsNullOrEmpty(constraints.SearchString) ? string.Empty : constraints.SearchString;
        //        var rowStart = constraints == null ? DBNull.Value : (object)(constraints.StartIndex + 1);
        //        var rowEnd = constraints == null ? DBNull.Value : (object)(constraints.StartIndex + constraints.PageSize);
        //        var sortedColumnName = constraints == null ? DBNull.Value : (object)(constraints.SortedColumnsString);

        //        var parameters = new DynamicParameters();
        //        parameters.Add("@UserId", constraints.UserId, DbType.Int64);
        //        parameters.Add("@IsForm", constraints.IsForm, DbType.Boolean);
        //        parameters.Add("@SearchString", searchString, DbType.String);
        //        parameters.Add("@RowStart", rowStart, DbType.Int32);
        //        parameters.Add("@RowEnd", rowEnd, DbType.Int32);
        //        parameters.Add("@SortedColumnName", sortedColumnName, DbType.String);
        //        var result = await conn.QueryMultipleAsync("School.GetPaginateQuizByUser", parameters, null, null, CommandType.StoredProcedure);
        //        var count = await result.ReadAsync<int>();
        //        var quizView = await result.ReadAsync<QuizView>();
        //        quizPagination.items = quizView.ToList();
        //        //quizPagination.totalCount = count;
        //        return quizPagination;
        //    }

        //}

        public async Task<QuizPagination> GetPaginateQuizByUser(QuizPaginationRequest constraints)
        {
            using (var conn = GetOpenConnection())
            {
                var quizPagination = new QuizPagination();
                IList<DataParameter> selectParameters = new List<DataParameter>();
                var searchString = constraints == null || string.IsNullOrEmpty(constraints.SearchString) ? string.Empty : constraints.SearchString;
                var rowStart = constraints == null ? DBNull.Value : (object)(constraints.StartIndex + 1);
                var rowEnd = constraints == null ? DBNull.Value : (object)(constraints.StartIndex + constraints.PageSize);
                var sortedColumnName = constraints == null ? DBNull.Value : (object)(constraints.SortedColumnsString);

                var parameters = new DynamicParameters();
                parameters.Add("@UserId", constraints.UserId, DbType.Int64);
                parameters.Add("@IsForm", constraints.IsForm, DbType.Boolean);
                parameters.Add("@SearchString", searchString, DbType.String);
                parameters.Add("@RowStart", rowStart, DbType.Int32);
                parameters.Add("@RowEnd", rowEnd, DbType.Int32);
                parameters.Add("@SortedColumnName", sortedColumnName, DbType.String);
                var result = await conn.QueryMultipleAsync("School.GetPaginateQuizByUser", parameters, null, null, CommandType.StoredProcedure);
                var count = await result.ReadAsync<int>();
                var quizView = await result.ReadAsync<QuizView>();
                quizPagination.items = quizView.ToList();
                quizPagination.totalCount = count.FirstOrDefault();
                return quizPagination;
            }
        }

        public async Task<IEnumerable<GroupQuiz>> GetAllQuizByStudentId(long studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StudentId", studentId, DbType.Int64);
                var result = await conn.QueryAsync<GroupQuiz>("[School].[GetAllQuizByStudentId]", parameters, null, null, CommandType.StoredProcedure);
                return result;

            }
        }
        public async Task<IEnumerable<QuizView>> GetAllQuizByUserAndSource(long Id, long sourceId, string sourceType, bool isForm)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", Id, DbType.Int64);
                parameters.Add("@SectionId", sourceId, DbType.Int64);
                parameters.Add("@SectionType", sourceType, DbType.String);
                parameters.Add("@IsForm", isForm, DbType.Boolean);
                return await conn.QueryAsync<QuizView>("[School].[GetAllQuizByUserAndSource]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<QuizDetails>> GetQuizDetailsByQuizIdAndUserId(long userId, int quizId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@QuizId", quizId, DbType.Int32);
                return await conn.QueryAsync<QuizDetails>("[School].[GetQuizDetailsByQuizIdAndUserId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<QuizAnswer>> GetAnswersByQuestionId(long userId, int questionId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@QuestionId", questionId, DbType.Int32);
                return await conn.QueryAsync<QuizAnswer>("[School].[GetAnswersByQuestionId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int GetQuizQuestionsCount(long userId, int quizId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[GetQuizQuestionsCount]", parameters, commandType: CommandType.StoredProcedure);
                int outputParam = parameters.Get<int>("output");

                return outputParam;
            }
        }

        public async Task<bool> ActiveDeactiveOTP(int QuizId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", QuizId, DbType.Int32);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync<int>("[School].[ActiveDeactiveOTP]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("Output") > 0;
            }
        }
        public async Task<bool> ActiveDeactiveSafeQuiz(int QuizId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", QuizId, DbType.Int32);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync("[School].[ActiveDeactiveSafeBrowser]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("Output") > 0;
            }
        }

        public async Task<bool> isSafeTest(int QuizId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", QuizId, DbType.Int32);
                bool IsSafeQuiz= await conn.ExecuteScalarAsync<bool>("School.IsSafeQuiz", parameters, commandType: CommandType.StoredProcedure);
                return IsSafeQuiz;
            }
        }

        public async Task<QuizResult> GetQuizReportCardByUserId(int quizId, int userId, int resourceId, string resourceType)
        {
            using (var conn = GetOpenConnection())
            {
                QuizResult quizResponse = new QuizResult();
                var parameters = new DynamicParameters();
                parameters.Add("@QuizId", quizId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@ResourceId", resourceId, DbType.Int32);
                parameters.Add("@ResourceType", resourceType, DbType.String);
                var result = await conn.QueryMultipleAsync("[School].[GetQuizReportCardByUserId]", parameters, null, null, CommandType.StoredProcedure);
                var quizResponseList = await result.ReadAsync<QuizResult>();
                var questionAnswerList = await result.ReadAsync<QuizAnswersView>();
                if (quizResponseList.Any())
                {
                    quizResponse = quizResponseList.FirstOrDefault();
                    quizResponse.ResponseQuestionAnswer = questionAnswerList.ToList();
                }
                return quizResponse;
            }
        }


        #endregion Quiz
    }
}
