﻿using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IRSSFeedRepository
    {

        Task<IEnumerable<RSSFeed>> GetRSSFeedList();

        bool AddUpdateRSSFeed(RSSFeed entity);

        Task<RSSFeed> GetRSSFeedById(int Id);
    }
}
