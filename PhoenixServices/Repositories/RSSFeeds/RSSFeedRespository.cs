﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Repositories;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class RSSFeedRespository : SqlRepository<RSSFeed>, IRSSFeedRepository
    {
        private readonly IConfiguration _config;

        public RSSFeedRespository(IConfiguration configuration):base(configuration)
        {
            _config = configuration;

        }

        #region SqlRepository
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<RSSFeed>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<RSSFeed> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(RSSFeed entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(RSSFeed entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion 


        public bool AddUpdateRSSFeed(RSSFeed entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetRSSParameters(entity);
                conn.Query<int>("School.AddUpdateRSSFeed", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<IEnumerable<RSSFeed>> GetRSSFeedList()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<RSSFeed>("School.GetAllRSSFeed", commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<RSSFeed> GetRSSFeedById(int Id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", Id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<RSSFeed>("School.RSSFeedById", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        #region Private Methods
        private DynamicParameters GetRSSParameters(RSSFeed entity)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@RSSTitle", entity.RSSTitle, DbType.String);
            parameters.Add("@RSSLink", entity.RSSLink, DbType.String);
            parameters.Add("@RSSDescription", entity.RSSDescription, DbType.String);
            parameters.Add("@CreatedBy", 1, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

       
        #endregion
    }
}
