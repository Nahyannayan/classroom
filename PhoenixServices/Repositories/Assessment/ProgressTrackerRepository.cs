﻿using DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.API.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public class ProgressTrackerRepository : SqlRepository<ProgressTracker>, IProgressTrackerRepository
    {
        private readonly IConfiguration _config;
        public ProgressTrackerRepository(IConfiguration config) : base(config)
        {
            _config = config;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<ProgressTracker>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<ProgressTracker> GetAsync(int id)
        {
            throw new NotImplementedException();
        }



        public override void InsertAsync(ProgressTracker entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(ProgressTracker entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CourseTopics>> GetTopicsByCourseId(long Id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@courseId", Id, DbType.Int64);
                return await conn.QueryAsync<CourseTopics>("Assessment.GetTopicsByCourseId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<StudentList>> GetStudentList(string GRD_ID, Int32 ACD_ID, Int32 SGR_ID, Int32 SCT_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GRD_ID", GRD_ID, DbType.String);
                parameters.Add("@ACD_ID", ACD_ID, DbType.Int32);
                parameters.Add("@SGR_ID", SGR_ID, DbType.Int32);
                parameters.Add("@SCT_ID", SCT_ID, DbType.Int32);
                return await conn.QueryAsync<StudentList>("SIMS.GetStudentList", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ProgressTrackerHeader>> GET_PROGRESS_TRACKER_HEADERS(long SBG_ID, string TOPIC_ID, long AGE_BAND_ID, string STEPS, long TSM_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SBG_ID", SBG_ID, DbType.Int64);
                parameters.Add("@TOPIC_ID", TOPIC_ID, DbType.String);
                parameters.Add("@AGE_BAND_ID", AGE_BAND_ID, DbType.Int64);
                parameters.Add("@STEPS", STEPS, DbType.String);
                parameters.Add("@TSM_ID", TSM_ID, DbType.Int64);
                return await conn.QueryAsync<ProgressTrackerHeader>("[ASSESSMENT].GET_PROGRESS_TRACKER_HEADERS", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ProgressTrackerData>> GET_PROGRESS_TRACKER_DATA(long SBG_ID, string TOPIC_ID, long TSM_ID, long SGR_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SBG_ID", SBG_ID, DbType.Int64);
                parameters.Add("@TOPIC_ID", TOPIC_ID, DbType.String);
                parameters.Add("@TSM_ID", TSM_ID, DbType.Int64);
                parameters.Add("@SGR_ID", SGR_ID, DbType.Int64);
                return await conn.QueryAsync<ProgressTrackerData>("ASSESSMENT.GET_PROGRESS_TRACKER_DATA", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<ProgressTrackerSettingMaster> BindProgressTrackerMasterSetting(long ACD_ID, long BSU_ID, string GRD_ID)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@DAM_ACD_ID", ACD_ID, DbType.Int64);
            parameters.Add("@DAM_BSU_ID", BSU_ID, DbType.Int64);
            parameters.Add("@DAM_GRD_IDS", GRD_ID, DbType.String);
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryFirstOrDefaultAsync<ProgressTrackerSettingMaster>("ASSESSMENT.BindProgressTrackerMasterSetting", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ProgressTrackerDropdown>> BindProgressTrackerDropdown(long BSU_ID, string GRD_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSU_ID", BSU_ID, DbType.Int64);
                parameters.Add("@GRD_ID", GRD_ID, DbType.String);
                return await conn.QueryAsync<ProgressTrackerDropdown>("ASSESSMENT.BindProgressTrackerDropdown", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public bool AddEditProgressSetUP(long teacherId, ProgressTrackerSetup model)
        {
            DataTable tblProgressRule = new DataTable();
            tblProgressRule.Columns.Add("Id", typeof(long));
            tblProgressRule.Columns.Add("StartDate", typeof(DateTime));
            tblProgressRule.Columns.Add("EndDate", typeof(DateTime));
            tblProgressRule.Columns.Add("Steps", typeof(long));
            tblProgressRule.Columns.Add("GradingTemplateId", typeof(long));
            tblProgressRule.Columns.Add("ProgressSetupId", typeof(long));
            tblProgressRule.Columns.Add("IsDeleted", typeof(bool));
            foreach (var item in model.ProgressSetupRules)
            {
                tblProgressRule.Rows.Add(item.ProgressRuleId, item.StartDate, item.EndDate, item.Steps, item.GradingTemplateId,model.Id,item.IsDeleted);
            }

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();

                //parameters.Add("@TransMode", model.TransMode, DbType.Int32);
                parameters.Add("@Id", model.Id, DbType.Int64);
                parameters.Add("@schoolId", model.SchoolId, DbType.Int64);
                parameters.Add("@CourseIds", model.CourseIds, DbType.String);
                parameters.Add("@GradeIds", model.GradeIds, DbType.String);
                parameters.Add("@GradeTemplateId", model.GradeTemplateId, DbType.Int64);
                parameters.Add("@GradeSlabId", model.GradeSlabId, DbType.Int64);
                parameters.Add("@ShowCodeAsHeader", model.ShowCodeAsHeader, DbType.Boolean);
                parameters.Add("@ShowAsDropdown", model.ShowAsDropDown, DbType.Boolean);
                parameters.Add("@tblProgressRule", tblProgressRule, DbType.Object);
                parameters.Add("@ModifiedBy", teacherId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[ASSESSMENT].[ProgressSetupCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<IEnumerable<ProgressTrackerSetup>> GetProgressSetup(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<ProgressTrackerSetup>("[ASSESSMENT].[GetProgressSetupDetails]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<CourseGradeDisplay>> GetCourseGradeDisplay(long schoolId, string courseIds)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@courseIds", courseIds, DbType.String);
                parameters.Add("@schoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<CourseGradeDisplay>("[Assessment].[GetCourseGradeDisplay]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<ProgressTrackerSetup> GetProgressSetupDetailsById(long Id)
        {
            ProgressTrackerSetup objProgressTrackerSetup = new ProgressTrackerSetup();
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@Id", Id, DbType.Int64);
                var result = await conn.QueryMultipleAsync("[ASSESSMENT].[GetProgressSetupDetailsById]", parameters, null, null, CommandType.StoredProcedure);
                objProgressTrackerSetup = await result.ReadFirstOrDefaultAsync<ProgressTrackerSetup>();
                var progressSetupList = await result.ReadAsync<ProgressSetupRules>();
                objProgressTrackerSetup.ProgressSetupRules = progressSetupList.ToList();
                return objProgressTrackerSetup;
            }
        }

        public async Task<IEnumerable<GradingTemplateItem>> GetProgressLessonGrading(long courseId, long schoolGroupId,short languageId)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@courseId", courseId, DbType.Int64);
                parameters.Add("@schoolGroupId", schoolGroupId, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<GradingTemplateItem>("[Assessment].[GetProgressLessonGrading]", parameters, null, null, CommandType.StoredProcedure);
            }

        }

        public async Task<IEnumerable<AssessmentStudent>> StudentProgressTracker(long schoolGroupId)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@schoolGroupId", schoolGroupId, DbType.Int64);
                return await conn.QueryAsync<AssessmentStudent>("[Assessment].[StudentProgressTracker]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AssessmetLessons>> GetLessonObjectivesByUnitIds(string subSyllabusIds)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@subSyllabusIds", subSyllabusIds, DbType.String);
                return await conn.QueryAsync<AssessmetLessons>("[Assessment].[GetLessonObjectivesByUnitIds]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public bool StudentProgressMapper(long userId, long groupId, List<AssessmentStudent> studentProgressMapper)
        {
            DataTable tblStudentProgressMapper = new DataTable();
            tblStudentProgressMapper.Columns.Add("Id", typeof(long));
            tblStudentProgressMapper.Columns.Add("SchoolGroupId", typeof(long));
            tblStudentProgressMapper.Columns.Add("LessonId", typeof(long));
            tblStudentProgressMapper.Columns.Add("StudentId", typeof(string));
            tblStudentProgressMapper.Columns.Add("GradingTemplateItemId", typeof(string));
            tblStudentProgressMapper.Columns.Add("ProgressDate", typeof(DateTime));
            foreach (var item in studentProgressMapper)
            {
                tblStudentProgressMapper.Rows.Add(item.Id, groupId, item.LessonId, item.StudentId, item.GradingTemplateId, DateTime.Now);
            }
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@userId", userId, DbType.Int64);
                parameters.Add("@tblStudentProgressMapper", tblStudentProgressMapper, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[Assessment].[StudentProgressTrackerCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<bool> SaveProgressTrackerEvidence(List<Attachment> attachments)
        {
            var list = new Attachments { AttachmentList = attachments };
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@attachment", list.GetAttachmentDT(), DbType.Object);
                parameters.Add("@output", direction: ParameterDirection.Output, dbType: DbType.Boolean);
                await conn.QueryAsync("dbo.AttachmentCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("output");
            }
        }

        public async Task<IEnumerable<Attachment>> GetProgressTrackerEvidence(long id, string key)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                parameters.Add("@key", key);
              
                return await conn.QueryAsync<Attachment>("dbo.getattachmentbymasterkeyid", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AssignmentObjectiveGrading>> GetObjectiveAssignmentGrading(long lessonId, long studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@lessonId", lessonId);
                parameters.Add("@studentId", studentId);

                return await conn.QueryAsync<AssignmentObjectiveGrading>("Assessment.GetObjectiveAssignmentGrading", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ProgressTrackerSetup>> ValidateProgressSetupCourseGrade(string courseIds, string gradeIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@courseIds", courseIds);
                parameters.Add("@gradeIds", gradeIds);

                return await conn.QueryAsync<ProgressTrackerSetup>("Assessment.ValidateProgressSetupCourseGrade", parameters, null, null, CommandType.StoredProcedure);
            }
        }
    }
}
