﻿using Phoenix.API.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IAssessmentConfigurationRepository
    {
        #region Assessment Configuration
        Task<IEnumerable<AssessmentColumn>> GetAssessmentColumnDetail(long AssessmentMasterId);
        Task<IEnumerable<AssessmentConfigSetting>> GetAssessmentConfigDetail(long AssessmentMasterId, long schoolId);
        Task<IEnumerable<AssessmentConfigSetting>> GetAssessmentConfigPagination(long schoolId, int pageNum, int pageSize, string searchString);
        Task<bool> SaveAssessmentConfigDetail(AssessmentConfigSetting assessmentConfig);
        Task<IEnumerable<Course>> GetCourseListByGrade(string SchoolGradeIds);
        #endregion
    }
}
