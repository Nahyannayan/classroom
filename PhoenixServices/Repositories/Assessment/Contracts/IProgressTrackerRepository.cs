﻿using Phoenix.API.Models;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IProgressTrackerRepository
    {
        Task<IEnumerable<CourseTopics>> GetTopicsByCourseId(long Id);
        Task<IEnumerable<StudentList>> GetStudentList(string GRD_ID, Int32 ACD_ID, Int32 SGR_ID, Int32 SCT_ID);
        Task<IEnumerable<ProgressTrackerHeader>> GET_PROGRESS_TRACKER_HEADERS(long SBG_ID, string TOPIC_ID, long AGE_BAND_ID, string STEPS, long TSM_ID);
        Task<IEnumerable<ProgressTrackerData>> GET_PROGRESS_TRACKER_DATA(long SBG_ID, string TOPIC_ID, long TSM_ID, long SGR_ID);
        Task<ProgressTrackerSettingMaster> BindProgressTrackerMasterSetting(long ACD_ID, long BSU_ID, string GRD_ID);
        Task<IEnumerable<ProgressTrackerDropdown>> BindProgressTrackerDropdown(long BSU_ID, string GRD_ID);

        bool AddEditProgressSetUP(long teacherId,ProgressTrackerSetup model);

        Task<IEnumerable<ProgressTrackerSetup>> GetProgressSetup(long schoolId);
        Task<IEnumerable<CourseGradeDisplay>> GetCourseGradeDisplay(long schoolId,string courseIds);

        Task<ProgressTrackerSetup> GetProgressSetupDetailsById(long Id);

        Task<IEnumerable<GradingTemplateItem>> GetProgressLessonGrading(long courseId, long schoolGroupId,short languageId);
        Task<IEnumerable<AssessmentStudent>> StudentProgressTracker(long schoolGroupId);
        Task<IEnumerable<AssessmetLessons>> GetLessonObjectivesByUnitIds(string subSyllabusIds);

        bool StudentProgressMapper(long userId, long groudId,List<AssessmentStudent> studentProgressMapper);
       Task<bool> SaveProgressTrackerEvidence(List<Attachment> attachments);

        Task<IEnumerable<Attachment>> GetProgressTrackerEvidence(long id,string key);
        Task<IEnumerable<AssignmentObjectiveGrading>> GetObjectiveAssignmentGrading(long lessonId, long studentId);
        Task<IEnumerable<ProgressTrackerSetup>> ValidateProgressSetupCourseGrade(string courseIds, string gradeIds);
    }
}
