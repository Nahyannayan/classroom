﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.API.Models;

namespace Phoenix.API.Repositories
{
    public class AssessmentConfigurationRepository : SqlRepository<AssessmentSetting>, IAssessmentConfigurationRepository
    {
        private readonly IConfiguration _config;
        public AssessmentConfigurationRepository(IConfiguration config) : base(config)
        {
            _config = config;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<AssessmentSetting>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<AssessmentSetting> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(AssessmentSetting entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(AssessmentSetting entityToUpdate)
        {
            throw new NotImplementedException();
        }

        #region Assessment Configuration
        public async Task<IEnumerable<AssessmentColumn>> GetAssessmentColumnDetail(long AssessmentMasterId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameter = new DynamicParameters();
                parameter.Add("@AssessmentMasterId", AssessmentMasterId);
                return await conn.QueryAsync<AssessmentColumn>("[ASSESSMENT].[GetAssessmentColumnDetail]", parameter, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AssessmentConfigSetting>> GetAssessmentConfigDetail(long AssessmentMasterId, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameter = new DynamicParameters();
                parameter.Add("@SchoolId", schoolId);
                parameter.Add("@AssessmentMasterId", AssessmentMasterId);
                return await conn.QueryAsync<AssessmentConfigSetting>("[ASSESSMENT].[GetAssessmentConfigDetail]", parameter, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<AssessmentConfigSetting>> GetAssessmentConfigPagination(long schoolId, int pageNum, int pageSize, string searchString)
        {
            using (var conn = GetOpenConnection())
            {
                var parameter = new DynamicParameters();
                parameter.Add("@SchoolId", schoolId);
                parameter.Add("@PageNum ", pageNum);
                parameter.Add("@PageSize ", pageSize);
                parameter.Add("@SearchString ", searchString);
                var result = await conn.QueryMultipleAsync("[ASSESSMENT].[GetAssessmentConfigPagination]", parameter, commandType: CommandType.StoredProcedure);
                var CongfigList = await result.ReadAsync<AssessmentConfigSetting>();
                var CongfigDataList = CongfigList.ToList();
                var totalCount = await result.ReadFirstOrDefaultAsync<int>();
                CongfigDataList.ForEach(x => x.TotalCount = totalCount);
                return CongfigDataList;
            }
        }
        public async Task<IEnumerable<Course>> GetCourseListByGrade(string SchoolGradeIds)
        {
            using (var conn = GetOpenConnection())
            {
                var parameter = new DynamicParameters();
                parameter.Add("@SchoolGradeIds", SchoolGradeIds);
                return await conn.QueryAsync<Course>("Assessment.GetCourseListByGrade", parameter, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<bool> SaveAssessmentConfigDetail(AssessmentConfigSetting assessmentConfig)
        {
            using (var conn = GetOpenConnection())
            {
                var parameter = new DynamicParameters();
                parameter.Add("@AssessmentMasterId", assessmentConfig.AssessmentMasterId, DbType.Int64);
                parameter.Add("@ASM_Description", assessmentConfig.ASM_Description, DbType.String);
                parameter.Add("@SchoolGradeIds", assessmentConfig.SchoolGradeIds, DbType.String);
                parameter.Add("@SchoolId", assessmentConfig.SchoolId, DbType.Int64);
                parameter.Add("@ASM_Order", assessmentConfig.ASM_Order, DbType.Int32);
                parameter.Add("@ASM_ActiveStatus", assessmentConfig.ASM_ActiveStatus, DbType.Boolean);
                parameter.Add("@UserId", assessmentConfig.UserId, DbType.Int64);
                parameter.Add("@AssessmentConfigDT", assessmentConfig.AssessmentColumnDT, DbType.Object);
                parameter.Add("@DATAMODE", assessmentConfig.DATAMODE, DbType.String);
                parameter.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 50);
                await conn.QueryAsync("Assessment.AssessmentConfigDetailCUD", parameter, null, null, CommandType.StoredProcedure);
                return parameter.Get<int>("output") > 0;
            }
        }
        #endregion
    }
}
