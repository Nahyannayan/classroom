﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public class BehaviourSetupRepository: SqlRepository<BehaviourSetup>, IBehaviourSetupRepository
    {
        private readonly IConfiguration _config;
        public BehaviourSetupRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        public async Task<IEnumerable<BehaviourSetup>> GetSubCategoryList(long MainCategoryId, long SchoolId,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@MainCategoryId", MainCategoryId, DbType.Int64);
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<BehaviourSetup>("Behaviour.GetSubCategoryList", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<int> SaveSubCategory(BehaviourSetup behaviourSetup, string DATAMODE)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@MainCategoryID", behaviourSetup.MainCategoryID, DbType.Int64);
                parameters.Add("@SubCategoryID", behaviourSetup.SubCategoryID, DbType.Int64);
                parameters.Add("@GradeIds", behaviourSetup.GradeIds, DbType.String);
                parameters.Add("@SchoolId", behaviourSetup.SchoolId, DbType.String);
                parameters.Add("@SubCategoryName", behaviourSetup.SubCategoryName, DbType.String);
                parameters.Add("@CategoryScore", behaviourSetup.CategoryScore, DbType.Decimal);
                parameters.Add("@CategoryImagePath", behaviourSetup.CategoryImagePath, DbType.String);
                parameters.Add("@IsDeletedImage", behaviourSetup.IsDeletedImage, DbType.String);
                parameters.Add("@IsDeletedImage", behaviourSetup.IsDeletedImage, DbType.String);
                parameters.Add("@DATAMODE", DATAMODE, DbType.String);
                parameters.Add("@BehaviourCategoryXml", behaviourSetup.BehaviourCategoryXml, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("[Behaviour].[SaveSubCategory]", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        #region Built In Functions
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override Task<IEnumerable<BehaviourSetup>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<BehaviourSetup> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override void InsertAsync(BehaviourSetup entity)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override void UpdateAsync(BehaviourSetup entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Action Hierarchy
        public async Task<IEnumerable<Designations>> GetDesignations(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.String);
                return await conn.QueryAsync<Designations>("Behaviour.GetDesignationBySchoolId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<DesignationsRouting>> GetDesignationsRoutings(long schoolId, long? designationFrom)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@DesignationId", designationFrom, DbType.Int64);
                return await conn.QueryAsync<DesignationsRouting>("Behaviour.GetDesignationRoutingBySchool", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<DesignationsRouting>> DesignationBySchoolCUD(DesignationsRoutingCUD designationsRouting)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@DesignationDT", designationsRouting.RoutingsDT, DbType.Object);
                return await conn.QueryAsync<DesignationsRouting>("Behaviour.DesignationBySchoolCUD", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        #endregion

        #region Certificate Schedule
        public async Task<IEnumerable<CertificateScheduling>> GetCertificateSchedulings(long? CertificateSchedulingId, long? curriculumId, long? schoolId, int? scheduleType = null, short languageId=1)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CertificateSchedulingId", CertificateSchedulingId, DbType.Int64);
                parameters.Add("@curriculumId", curriculumId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@scheduleType", scheduleType);
                parameters.Add("@languageId", languageId,DbType.Int16);
                var  result = await conn.QueryMultipleAsync("Behaviour.[GetCertificateScheduling]", parameters, null, null, CommandType.StoredProcedure);
                var schedules = await result.ReadAsync<CertificateScheduling>();
                var schedule = schedules.AsList();
                if(schedule.Any() && CertificateSchedulingId != null)
                {
                    var staff = await result.ReadAsync<long>();
                    schedule.FirstOrDefault().EmailOtherStaffId = staff.ToArray();
                }
                return schedule;
            }
        }

        public async Task<long> CertificateSchedulingCUD(CertificateScheduling certificateScheduling)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CertificateSchedulingId", certificateScheduling.CertificateSchedulingId);
                parameters.Add("@Description", certificateScheduling.Description);
                parameters.Add("@Points", certificateScheduling.Points);
                parameters.Add("@CertificateType", certificateScheduling.CertificateTypeId);
                parameters.Add("@CurriculumId", certificateScheduling.CurriculumId);
                parameters.Add("@SchoolId", certificateScheduling.SchoolId);
                parameters.Add("@IsEmail", certificateScheduling.IsEmail);
                parameters.Add("@IsPrint", certificateScheduling.IsPrint);
                parameters.Add("@IsEmailOther", certificateScheduling.IsEmailOther);
                parameters.Add("@EmailOtherStaffId", certificateScheduling.EmailOtherStaffId == null ? null : string.Join("|", certificateScheduling.EmailOtherStaffId));
                parameters.Add("@ScheduleType", certificateScheduling.ScheduleType);
                parameters.Add("@CreatedBy", certificateScheduling.CreatedBy);
                parameters.Add("@IsActive", certificateScheduling.IsActive);
                parameters.Add("@CertificateSchedulingXml", certificateScheduling.CertificateSchedulingXml);
                parameters.Add("@output", dbType: DbType.Int64, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<long>("Behaviour.[CertificateSchedulingCUD]", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<long>("output");
            }
        }
        #endregion
    }
}
