﻿using DbConnection;
using SIMS.API.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Phoenix.Models.Entities;

namespace SIMS.API.Repositories
{
    public interface IBehaviourRepository : IGenericRepository<Behaviour>
    {
        Task<IEnumerable<ClassList>> GetStudentList(string username, int tt_id = 0, string grade = null, string section = null);
        Task<IEnumerable<Behaviour>> LoadBehaviourByStudentId(string stu_id);
        Task<IEnumerable<BehaviourDetails>> GetBehaviourById(int id);
        Task<IEnumerable<BehaviourDetails>> InsertBehaviourDetails(BehaviourDetails entity, string bsu_id, string mode = "ADD");
        Task<IEnumerable<StudentBehaviour>> GetListOfStudentBehaviour();
        bool InsertUpdateStudentBehavior(List<StudentBehaviourFiles> studentBehaviourFiles, long studentId = 0, int behaviorId = 0, string behaviourComment = "");
        Task<IEnumerable<StudentBehaviour>> GetStudentBehaviorByStudentId(long studentId = 0);
        bool InsertBulkStudentBehaviour(List<StudentBehaviourFiles> studentBehaviourFiles, string bulkStudentds = "", int behaviourId = 0, string behaviourComment = "");
        bool UpdateBehaviourTypes(int behaviourId = 0, string behaviourType = "", int behaviourPoint = 0, int categoryId = 0);

        Task<IEnumerable<StudentBehaviourMerit>> GetFileDetailsByStudentId(long studentId);

        Task<IEnumerable<ClassList>> GetBehaviourClassList(string username, int tt_id = 0, string grade = null, string section = null, long GroupId = 0, bool IsFilterByGroup = false);

        Task<IEnumerable<ClassList>> GetBehaviourStudentListByGroupId(long groupId);
        bool DeleteStudentBehaviourMapping(long studentId = 0, int behaviourId = 0);
        Task<IEnumerable<SubCategories>> GetSubCategoriesByCategoryId(long categoryId, string BSU_ID, string GRD_ID, long GroupId,short languageId);
        Task<IEnumerable<StudentBehaviourMerit>> GetMeritDetails(long meritId);
        Task<IEnumerable<SubCategories>> GetMeritCategoryByStudent(long schoolId, long studentId,short languageId);
        Task<IEnumerable<CourseGroupModel>> GetCourseGroupList(long TeacherId, long SchoolId);
        Task<int> InsertMeritDemerit(string schoolId, int academicId, DateTime? incidentDate, StudentBehaviourMerit objBehaviourMerit, List<CategoryDetails> objCategories);

        #region Student Points Category
        Task<IEnumerable<StudentPointCategory>> GetStudentPointCategory(long schoolId, long academicYearId, int scheduleType);
        Task<IEnumerable<StudentPointCategory>> GetStudentPointCategory(string sectionId, int? scheduleType, long? CertificateScheduleId, DateTime? date, long? studentId);
        Task<int> CertificateProcessLogCU(List<CertificateProcessLog> processLogs);
        #endregion

        #region StudentOnReport
        Task<IEnumerable<StudentOnReportMaster>> GetStudentOnReportMDetail(long studentId);
        Task<long> StudentOnReportMCU(StudentOnReportMaster studentOnReport);

        Task<IEnumerable<StudentOnReportDetail>> GetStudentOnReportDetails(StudentOnReportDetailsParameter detailsParameter);
        Task<long> StudentOnReportDetailsCU(StudentOnReportDetail studentOnReport);
        #endregion
    }
}
