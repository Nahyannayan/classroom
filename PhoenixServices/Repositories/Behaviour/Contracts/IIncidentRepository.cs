﻿
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IIncidentRepository
    {
        #region Incident
        /// <summary>
        /// To get the list of incident
        /// </summary>
        /// <param name="schoolId">School Id to get Incident</param>
        /// <returns></returns>
        Task<IncidentDashBoardModel> GetIncidentList(long schoolId, long curriculumId, int month, bool isFA);

        /// <summary>
        /// to get the list of student under incident
        /// </summary>
        /// <param name="IncidentId">Incident Id</param>
        /// <returns></returns>
        Task<IncidentModel> GetIncident(long IncidentId);
        Task<IEnumerable<IncidentWitness>> GetIncidentWitnesses(long IncidentId);
        Task<IEnumerable<ChartModel>> GetIncidentChartByCategory(long schoolId, long academicYearId, int month, bool isCategory);
        Task<IEnumerable<IncidentStudentList>> GetStudentByIncidentId(long incidentId);
        Task<string> IncidentEntryCUD(IncidentEntry incidentEntry);
        #region IncidentAction
        Task<IEnumerable<ActionDetails>> GetBehaviourAction(long incidentId, long studentId);
        Task<IEnumerable<BehaviourActionFollowup>> GetBehaviourActionFollowups(long incidentId, long actionId);
        Task<IEnumerable<FollowUpDesignation>> GetFollowUpDesignations(long schoolId, long incidentId,long UserId);
        Task<IEnumerable<FollowUpStaff>> GetFollowUpStaffs(long schoolId, long designationId);
        Task<bool> ActionCUD(ActionModel behaviourAction);
        Task<int> ActionFollowUpCUD(BehaviourActionFollowup behaviourActionFollowup);
        Task<IEnumerable<GroupTeacher>> GetSchoolTeachersBySchoolId(string SchoolId);
        #endregion
        #endregion
    }
}
