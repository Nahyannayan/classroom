﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public interface IBehaviourSetupRepository
    {
        Task<IEnumerable<BehaviourSetup>> GetSubCategoryList(long MainCategoryId, long SchoolId,short languageId);
        Task<int> SaveSubCategory(BehaviourSetup behaviourSetup, string DATAMODE);
        #region Action Hierarchy
        Task<IEnumerable<Designations>> GetDesignations(long schoolId);
        Task<IEnumerable<DesignationsRouting>> GetDesignationsRoutings(long schoolId, long? designationFrom);
        Task<IEnumerable<DesignationsRouting>> DesignationBySchoolCUD(DesignationsRoutingCUD designationsRouting);
        #endregion

        #region Certificate Schedule
        Task<IEnumerable<CertificateScheduling>> GetCertificateSchedulings(long? CertificateSchedulingId, long? curriculumId, long? schoolId, int? scheduleType = null, short languageId = 1);
        Task<long> CertificateSchedulingCUD(CertificateScheduling certificateScheduling);
        #endregion
    }
}
