﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using SIMS.API.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Phoenix.API.Helpers;
using System.Linq;
using Phoenix.API.Repositories;
using Phoenix.Models.Entities;

namespace SIMS.API.Repositories
{
    public class BehaviourRepository : SqlRepository<Behaviour>, IBehaviourRepository
    {
        private readonly IConfiguration _config;
        private readonly IAttachmentRepository attachmentRepository;
        public BehaviourRepository(IConfiguration configuration, IAttachmentRepository attachmentRepository) : base(configuration)
        {
            _config = configuration;
            this.attachmentRepository = attachmentRepository;
        }

        #region Built In Function
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override Task<IEnumerable<Behaviour>> GetAllAsync()
        {
            throw new NotImplementedException();
        }
        public override Task<Behaviour> GetAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override void InsertAsync(Behaviour entity)
        {
            throw new NotImplementedException();
        }
        public override void UpdateAsync(Behaviour entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion

        public async Task<IEnumerable<ClassList>> GetStudentList(string username, int tt_id = 0, string grade = null, string section = null)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@USER_NAME", username, DbType.String);
                parameters.Add("@TTM_ID", tt_id, DbType.Int32);
                parameters.Add("@GRD_ID", grade, DbType.String);
                parameters.Add("@SCT_ID", section, DbType.String);
                return await conn.QueryAsync<ClassList>("SIMS.GETCLASSLIST", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<Behaviour>> LoadBehaviourByStudentId(string stu_id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@STU_ID", stu_id, DbType.String);
                return await conn.QueryAsync<Behaviour>("SIMS.LoadBehaviourByStudentId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<BehaviourDetails>> GetBehaviourById(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ID", id, DbType.Int32);
                return await conn.QueryAsync<BehaviourDetails>("SIMS.GetBehaviourByid", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<BehaviourDetails>> InsertBehaviourDetails(BehaviourDetails entity, string bsu_id, string mode = "ADD")
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Behaviour_ID", (mode == "ADD" ? 0 : entity.Behaviour_ID), DbType.Int32);
                parameters.Add("@Category_ID", entity.Category_ID, DbType.Int32);
                parameters.Add("@SubCategory_ID", entity.SubCategory_ID, DbType.Int32);
                parameters.Add("@Points", entity.Points, DbType.Int32);
                parameters.Add("@Activity_ID", entity.Activity_ID, DbType.Int32);
                parameters.Add("@Location_ID", entity.Location_ID, DbType.Int32);
                parameters.Add("@Incident_Date", entity.Incident_Date.Value, DbType.DateTime);
                parameters.Add("@Incident_Time", entity.Incident_Time, DbType.String);
                parameters.Add("@Period", entity.Period, DbType.String);
                parameters.Add("@Comments", entity.Comments, DbType.String);
                parameters.Add("@Recorded_Date", entity.Recorded_Date.Value, DbType.DateTime);
                parameters.Add("@Status_ID", entity.Status_ID, DbType.Int32);
                parameters.Add("@Recorded_By", entity.Recorded_By, DbType.String);
                parameters.Add("@DocPath", entity.DocPath, DbType.String);
                parameters.Add("@OtherStaff_ID", entity.OtherStaff_ID, DbType.Int32);
                parameters.Add("@Followup_Date", entity.Followup_Date.Value, DbType.DateTime);
                parameters.Add("@ReferredTo", entity.ReferredTo, DbType.String);
                parameters.Add("@FollowupComments", entity.FollowupComments, DbType.String);
                parameters.Add("@BSU_ID", bsu_id, DbType.String);

                return await conn.QueryAsync<BehaviourDetails>("SIMS.InsertBehaviourDetails", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<StudentBehaviour>> GetListOfStudentBehaviour()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<StudentBehaviour>("SIMS.GetListOfStudentBehaviourType", null, null, null, CommandType.StoredProcedure);
            }
        }
        public bool InsertUpdateStudentBehavior(List<StudentBehaviourFiles> studentBehaviourFiles, long studentId = 0, int behaviorId = 0, string behaviourComment = "")
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("StudentId", typeof(long));
            dt.Columns.Add("BehaviourId", typeof(int));
            dt.Columns.Add("FileName", typeof(string));
            dt.Columns.Add("UploadedFilePath", typeof(string));
            foreach (var item in studentBehaviourFiles)
            {
                dt.Rows.Add(item.StudentId, item.BehaviourId, item.FileName, item.UploadedFilePath);
            }
            var parameters = new DynamicParameters();
            parameters.Add("@StudentId", studentId, DbType.Int32);
            parameters.Add("@BehaviorId", behaviorId, DbType.Int32);
            parameters.Add("@behaviourComment", behaviourComment, DbType.String);
            parameters.Add("@studentbehaviourfiles", dt, DbType.Object);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("SIMS.InsertUpdateStudentBehaviorMapping", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<IEnumerable<StudentBehaviour>> GetStudentBehaviorByStudentId(long studentId = 0)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();

                parameters.Add("@studentId", studentId, DbType.Int64);
                return await conn.QueryAsync<StudentBehaviour>("SIMS.GetStudentBehaviorByStudentId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public bool InsertBulkStudentBehaviour(List<StudentBehaviourFiles> studentBehaviourFiles, string bulkStudentds = "", int behaviourId = 0, string behaviourComment = "")
        {
            var parameters = new DynamicParameters();
            DataTable dt = new DataTable();
            dt.Columns.Add("StudentId", typeof(long));
            dt.Columns.Add("BehaviourId", typeof(int));
            dt.Columns.Add("FileName", typeof(string));
            dt.Columns.Add("UploadedFilePath", typeof(string));
            foreach (var item in studentBehaviourFiles)
            {
                dt.Rows.Add(item.StudentId, item.BehaviourId, item.FileName, item.UploadedFilePath);
            }
            parameters.Add("@bulkStudentds", bulkStudentds, DbType.String);
            parameters.Add("@behaviourId", behaviourId, DbType.Int32);
            parameters.Add("@behaviourComment", behaviourComment, DbType.String);
            parameters.Add("@studentbehaviourfiles", dt, DbType.Object);

            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("SIMS.InsertBulkStudentBehaviour", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public bool UpdateBehaviourTypes(int behaviourId = 0, string behaviourType = "", int behaviourPoint = 0, int categoryId = 0)
        {
            var parameters = new DynamicParameters();

            parameters.Add("@behaviourId", behaviourId, DbType.Int32);
            parameters.Add("@strBehaviourType", behaviourType, DbType.String);
            parameters.Add("@behaviourPoint", behaviourPoint, DbType.Int32);
            parameters.Add("@categoryId", categoryId, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("SIMS.AddEditBehaviourtypeList", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<IEnumerable<StudentBehaviourMerit>> GetFileDetailsByStudentId(long studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();

                parameters.Add("@studentId", studentId, DbType.Int64);
                return await conn.QueryAsync<StudentBehaviourMerit>("SIMS.GetFileDetailsByStudentId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ClassList>> GetBehaviourClassList(string username, int tt_id = 0, string grade = null, string section = null, long GroupId = 0, bool IsFilterByGroup = false)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@USER_NAME", username, DbType.String);
                parameters.Add("@TTM_ID", tt_id, DbType.Int32);
                parameters.Add("@GRD_ID", grade, DbType.Int32);
                parameters.Add("@SectionId", section, DbType.Int32);
                parameters.Add("@GroupId", GroupId, DbType.Int64);
                parameters.Add("@IsFilterByGroup", IsFilterByGroup, DbType.Boolean);
                return await conn.QueryAsync<ClassList>("Behaviour.GetBehaviourClassList", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ClassList>> GetBehaviourStudentListByGroupId(long groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@groupId", groupId);
                return await conn.QueryAsync<ClassList>("Behaviour.GetBehaviourStudentListByGroupId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public bool DeleteStudentBehaviourMapping(long studentId = 0, int behaviourId = 0)
        {

            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@studentId", studentId, DbType.Int64);
                parameters.Add("@BehaviourId", behaviourId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Behaviour.DeleteStudentBehaviourMapping", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<IEnumerable<SubCategories>> GetSubCategoriesByCategoryId(long categoryId, string BSU_ID, string GRD_ID,long GroupId,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@categoryId", categoryId, DbType.Int64);
                parameters.Add("@BSU_ID", Convert.ToInt64(BSU_ID), DbType.Int64);
                parameters.Add("@GRD_ID", GRD_ID, DbType.Int32);
                parameters.Add("@GroupId", GroupId, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int16);

                return await conn.QueryAsync<SubCategories>("Behaviour.GetSubCategoriesByCategoryId", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<StudentBehaviourMerit>> GetMeritDetails(long meritId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@meritid", meritId, DbType.Int64);

                return await conn.QueryAsync<StudentBehaviourMerit>("Behaviour.GetMeritDetails", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SubCategories>> GetMeritCategoryByStudent(long schoolId, long studentId,short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId, DbType.Int32);
                parameters.Add("@studentId", studentId, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<SubCategories>("Behaviour.GetMeritCategoryByStudent", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<CourseGroupModel>> GetCourseGroupList(long TeacherId, long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TeacherId", TeacherId, DbType.Int64);
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                return await conn.QueryAsync<CourseGroupModel>("Behaviour.GetCourseGroupList", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<int> InsertMeritDemerit(string schoolId, int academicId, DateTime? incidentDate, StudentBehaviourMerit objBehaviourMerit, List<CategoryDetails> objCategories)
        {
            string[] split = objBehaviourMerit.StudentIds[1].Split(',');

            var parameters = new DynamicParameters();
            DataTable dt = new DataTable();
            dt.Columns.Add("StudentId", typeof(long));
            dt.Columns.Add("CategoryId", typeof(int));
            dt.Columns.Add("SubCategoryId", typeof(int));
            dt.Columns.Add("LevelMappingId", typeof(long));

            foreach (var studentId in split)
            {
                if (objCategories != null)
                {
                    foreach (var item in objCategories)
                    {
                        dt.Rows.Add(Convert.ToInt64(studentId), item.CategoryId, item.SubcategoryId, item.LevelMappingId);
                    }
                }
            }
            parameters.Add("@UserId", objBehaviourMerit.UserId, DbType.Int64);
            parameters.Add("@schoolId", schoolId, DbType.String);
            parameters.Add("@MeritId", objBehaviourMerit.MeritId, DbType.Int64);
            parameters.Add("@MeritType", objBehaviourMerit.MeritType, DbType.String);
            parameters.Add("@MeritUploadId", objBehaviourMerit.MeritUploaded, DbType.Int64);
            parameters.Add("@MeritRemark", objBehaviourMerit.MeritRemarks, DbType.String);
            parameters.Add("@tblCategory", dt, DbType.Object);
            parameters.Add("@StudentId", objBehaviourMerit.StudentId, DbType.Int64);
            parameters.Add("@IncidentDate", objBehaviourMerit.IncidentDate, DbType.DateTime);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                await conn.QueryAsync<int>("Behaviour.MeritDemeritCUD", parameters, commandType: CommandType.StoredProcedure);
                var id = parameters.Get<int>("output");
                if (id > 0 && objBehaviourMerit.AttachmentList.Any())
                {
                    objBehaviourMerit.AttachmentList.FirstOrDefault().AttachedToId = id;
                    var result = await attachmentRepository.AttachmentCU(objBehaviourMerit.AttachmentList);
                }
                return id;
            }
        }
        #region Student Point Category
        public async Task<IEnumerable<StudentPointCategory>> GetStudentPointCategory(long schoolId, long academicYearId, int scheduleType)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@sectionId", schoolId, DbType.Int64);
                parameters.Add("@ACD_ID", academicYearId, DbType.Int64);
                parameters.Add("@SCHEDULE_TYPE", scheduleType, DbType.Int32);
                var result = await conn.QueryMultipleAsync("Behaviour.[GetStudentPointsCategory]", parameters, null, null, CommandType.StoredProcedure);

                var studentPointCategory = await result.ReadAsync<StudentPointCategory>();
                var certificates = await result.ReadAsync<CertificateProcessLog>();
                var studentPoints = new List<StudentPointCategory>();
                studentPoints = studentPointCategory.AsList();
                studentPoints.ForEach(x =>
                {
                    x.Certificates = certificates.AsList().FindAll(y => y.StudentId == x.StudentID);
                });
                return studentPoints;
            }
        }
        public async Task<IEnumerable<StudentPointCategory>> GetStudentPointCategory(string sectionId, int? scheduleType, long? CertificateScheduleId,DateTime? date, long? studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@sectionId", sectionId);
                parameters.Add("@SCHEDULE_TYPE", scheduleType);
                parameters.Add("@CertificateScheduleId", CertificateScheduleId);
                parameters.Add("@StudentId", studentId);
                parameters.Add("@date", date);
                var result = await conn.QueryMultipleAsync("Behaviour.[GetStudentPointsCategory]", parameters, null, null, CommandType.StoredProcedure);
                var studentPointCategory = await result.ReadAsync<StudentPointCategory>();
                var certificates = await result.ReadAsync<CertificateProcessLog>();
                var studentPoints = new List<StudentPointCategory>();
                studentPoints = studentPointCategory.AsList();
                studentPoints.ForEach(x =>
                {
                    x.Certificates = certificates.AsList().FindAll(y => y.StudentId == x.StudentID);
                });
                return studentPoints;
            }
        }
        public async Task<int> CertificateProcessLogCU(List<CertificateProcessLog> processLogs)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                DataTable dt = new DataTable();
                dt.Columns.Add("CertificateScheduleId", typeof(long));
                dt.Columns.Add("StudentId", typeof(long));
                dt.Columns.Add("ActionType", typeof(int));
                dt.Columns.Add("FilePath", typeof(string));

                foreach (var item in processLogs)
                {
                    dt.Rows.Add(item.TemplateId, item.StudentId, item.ActionType, item.FilePath);
                }

                parameters.Add("@certificateProcesslog", dt, DbType.Object);
                parameters.Add("@createdby", processLogs.FirstOrDefault().CreatedBy);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync("BEHAVIOUR.CertificateProcessLogCU", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        #endregion

        #region StudentOnReport
        public async Task<IEnumerable<StudentOnReportMaster>> GetStudentOnReportMDetail(long studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SRM_STU_ID", studentId);
                return await conn.QueryAsync<StudentOnReportMaster>("[Behaviour].[GetStudentOnReportMaster]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<long> StudentOnReportMCU(StudentOnReportMaster studentOnReport)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SRM_ID", studentOnReport.Id);
                parameters.Add("@SRM_SCT_ID", studentOnReport.SectionId);
                parameters.Add("@SRM_GROUP_ID", studentOnReport.GroupId);
                parameters.Add("@SRM_STU_ID", studentOnReport.StudentId);
                parameters.Add("@SRM_FROM_DATE", studentOnReport.FromDate);
                parameters.Add("@SRM_TO_DATE", studentOnReport.ToDate);
                parameters.Add("@SRM_DESC", studentOnReport.Description);
                parameters.Add("@SRM_CREATED_BY", studentOnReport.CreatedBy);
                parameters.Add("@SRM_IsActive", studentOnReport.IsActive);
                parameters.Add("@OUTPUT", dbType: DbType.Int64, direction: ParameterDirection.Output);
                await conn.QueryAsync("[Behaviour].[StudentOnReportMasterCU]", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<long>("OUTPUT");
            }
        }
        public async Task<IEnumerable<StudentOnReportDetail>> GetStudentOnReportDetails(StudentOnReportDetailsParameter detailsParameter)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SRM_STU_ID", detailsParameter.StudentId);
                parameters.Add("@SRD_SRM_ID", detailsParameter.StudentOnReportMasterId);
                parameters.Add("@SRD_CREATED_BY", detailsParameter.CreatedBy);
                parameters.Add("@SchoolGroupId", detailsParameter.GroupId);
                return await conn.QueryAsync<StudentOnReportDetail>("[Behaviour].[GetStudentOnReportDetail]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<long> StudentOnReportDetailsCU(StudentOnReportDetail studentOnReport)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SRD_ID", studentOnReport.Id);
                parameters.Add("@SRD_SRM_ID", studentOnReport.StudentOnReportMasterId);
                parameters.Add("@SRD_PERIOD_NO", studentOnReport.PeriodNo);
                parameters.Add("@SRD_DESC", studentOnReport.Description);
                parameters.Add("@SRD_GROUP_ID", studentOnReport.GroupId);
                parameters.Add("@SRD_CREATED_BY", studentOnReport.CreatedBy);
                parameters.Add("@SRD_CREATED_ON", DateTime.Now);
                parameters.Add("@OUTPUT", dbType: DbType.Int64, direction: ParameterDirection.Output);
                await conn.QueryAsync("[Behaviour].[StudentOnReportDetailCU]", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<long>("OUTPUT");
            }
        }
        #endregion


    }
}
