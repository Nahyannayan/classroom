﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class IncidentRepository : SqlRepository<IncidentListModel>, IIncidentRepository
    {
        private readonly IConfiguration _config;

        public IncidentRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        #region Incident
        public async Task<IncidentDashBoardModel> GetIncidentList(long schoolId, long curriculumId, int month, bool isFA)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId, DbType.Int64);
                parameters.Add("@curriculumId", curriculumId, DbType.Int64);
                parameters.Add("@month", month, DbType.Int32);
                var result =  await conn.QueryMultipleAsync("Behaviour.GetIncidentList", parameters, null, null, CommandType.StoredProcedure);
                var incidentDashBoard = new IncidentDashBoardModel
                {
                    IncidentLists = await result.ReadAsync<IncidentListModel>(),
                    PieChart = await result.ReadAsync<ChartModel>(),
                    LineChart = await result.ReadAsync<ChartModel>()
                };
                return incidentDashBoard;
            }
        }

        public async Task<IncidentModel> GetIncident(long IncidentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@incidentId", IncidentId, DbType.Int64);
                var result = await conn.QueryMultipleAsync("Behaviour.GetIncident", parameters, null, null, CommandType.StoredProcedure);
                var incident = new IncidentModel
                {
                    Incident = await result.ReadFirstAsync<IncidentListModel>(),
                    StudentLists = await result.ReadAsync<IncidentStudentList>(),
                    Witnesses = await result.ReadAsync<IncidentWitness>()
                };
                return incident;
            }
        }
        public async Task<IEnumerable<IncidentWitness>> GetIncidentWitnesses(long IncidentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BM_ID", IncidentId, DbType.Int64);
                return await conn.QueryAsync<IncidentWitness>("SIMS.GetIncidentWitnessById", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ChartModel>> GetIncidentChartByCategory(long schoolId, long academicYearId, int month, bool isCategory)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSU_ID", schoolId, DbType.Int64);
                parameters.Add("@ACD_ID", academicYearId, DbType.Int64);
                parameters.Add("@MONTH_NUMBER", month, DbType.Int32);
                if (isCategory)
                    return await conn.QueryAsync<ChartModel>("SIMS.IncidentChartByCategory", parameters, null, null, CommandType.StoredProcedure);
                else
                    return await conn.QueryAsync<ChartModel>("SIMS.IncidentChartByGrade", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<IncidentStudentList>> GetStudentByIncidentId(long incidentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@incidentId", incidentId, DbType.Int64);
                return await conn.QueryAsync<IncidentStudentList>("Behaviour.GetStudentByIncidentId", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<string> IncidentEntryCUD(IncidentEntry incidentEntry)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BMID", incidentEntry.BehaviourId, DbType.Int64);
                parameters.Add("@schoolId", incidentEntry.SchoolId, DbType.Int64);
                parameters.Add("@INCIDENT_DATE", incidentEntry.IncidentDate, DbType.DateTime);
                parameters.Add("@INCIDENT_TYPE", (int)incidentEntry.IncidentType, DbType.Int32);
                parameters.Add("@STAFF_ID", incidentEntry.StaffId, DbType.Int64);
                parameters.Add("@INCIDENT_DESC", incidentEntry.IncidentDesc, DbType.String);
                parameters.Add("@CATEGORYID", incidentEntry.CategoryId, DbType.Int32);
                parameters.Add("@CurriculumId", incidentEntry.CurriculumId, DbType.Int32);
                parameters.Add("@WITNESS_DATA", incidentEntry.WitnessDT, DbType.Object);
                parameters.Add("@INVOLVED_DATA", incidentEntry.StudentInvolvedDT, DbType.Object);
                parameters.Add("@LevelMapping_ID", incidentEntry.LevelMapping_ID, DbType.Int32);
                parameters.Add("@Output", dbType: DbType.String, size: int.MaxValue, direction: ParameterDirection.Output);
                await conn.QueryAsync<string>("Behaviour.IncidentCUD", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<string>("Output");
            }
        }
        #region IncidentAction
        public async Task<IEnumerable<ActionDetails>> GetBehaviourAction(long incidentId, long studentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@incidentId", incidentId, DbType.Int64);
                parameters.Add("@studentId", studentId, DbType.Int64);
                return await conn.QueryAsync<ActionDetails>("Behaviour.GetStudentIncidentAction", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<BehaviourActionFollowup>> GetBehaviourActionFollowups(long incidentId, long actionId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BM_ID", incidentId, DbType.Int64);
                parameters.Add("@ACTION_ID", actionId, DbType.Int64);
                return await conn.QueryAsync<BehaviourActionFollowup>("Behaviour.GetActionFollowupDetails", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<FollowUpDesignation>> GetFollowUpDesignations(long schoolId, long incidentId, long UserId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSU_ID", schoolId, DbType.Int64);
                parameters.Add("@BM_ID", incidentId, DbType.Int64);
                parameters.Add("@UserId", UserId, DbType.Int64);
                return await conn.QueryAsync<FollowUpDesignation>("Behaviour.BindFollowupDesignation", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<FollowUpStaff>> GetFollowUpStaffs(long schoolId, long designationId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSU_ID", schoolId, DbType.Int64);
                parameters.Add("@DES_ID", designationId, DbType.Int64);
                return await conn.QueryAsync<FollowUpStaff>("Behaviour.BindFollowupStaffList", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        private string GetYesNo(bool isTrue) => isTrue ? "Yes" : "No";

        public async Task<bool> ActionCUD(ActionModel behaviourAction)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                DataTable dt = new DataTable();
                dt.Columns.Add("ActionTypeId", typeof(long));
                dt.Columns.Add("ActionComment", typeof(string));
                dt.Columns.Add("ActionDate", typeof(DateTime));
                dt.Columns.Add("ActionDone", typeof(bool));
                dt.Columns.Add("CreatedBy", typeof(long));
                dt.Columns.Add("StudentInvolvedid", typeof(long));
                foreach (var item in behaviourAction.ActionDetails)
                {
                    dt.Rows.Add(item.ActionTypeId, item.ReportComment, item.ActionDate, item.ActionDone, item.CreatedBy, item.StudentInvolvedId);
                }
                parameters.Add("@action", dt, DbType.Object);
                parameters.Add("@output", dbType: DbType.Boolean, direction: ParameterDirection.Output);
                await conn.ExecuteAsync("Behaviour.ActionCUD", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<bool>("output");
            }
        }

        public async Task<int> ActionFollowUpCUD(BehaviourActionFollowup behaviourActionFollowup)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ACTION_DETAIL_ID", behaviourActionFollowup.ActionDetailsId);
                parameters.Add("@INCIDENT_ID", behaviourActionFollowup.StudentInvolvedId);
                parameters.Add("@ACTION_ID", behaviourActionFollowup.ActionId);
                parameters.Add("@ACTION_DATE", behaviourActionFollowup.ActionDate);
                parameters.Add("@STAFF_ID", behaviourActionFollowup.Action_CurrentUser_DesignationId);
                parameters.Add("@ACTION_DESCR", behaviourActionFollowup.Action_Followup_Remarks);
                parameters.Add("@ACTION_COMMENTS", behaviourActionFollowup.Action_Followup_Remarks);
                parameters.Add("@ACTION_FORWARD_TO", behaviourActionFollowup.Action_FollowupBy_Id);
                parameters.Add("@ACTION_FORWARD_DATE", DateTime.Now);
                parameters.Add("@DESIGNATION_ID", behaviourActionFollowup.Action_FollowupBy_Designation_Id);
                parameters.Add("@DATAMODE", behaviourActionFollowup.DataMode);
                return await conn.ExecuteAsync("Behaviour.ActionDetailsCUD", parameters, null, null, CommandType.StoredProcedure);

            }
        }

        public async Task<IEnumerable<GroupTeacher>> GetSchoolTeachersBySchoolId(string SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.String);
                return await conn.QueryAsync<GroupTeacher>("Behaviour.[GetTeachersBySchoolId]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        #endregion

        #endregion
        #region BuiltIn Function
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<IncidentListModel>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<IncidentListModel> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(IncidentListModel entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(IncidentListModel entityToUpdate)
        {
            throw new NotImplementedException();
        } 
        #endregion
    }
}
