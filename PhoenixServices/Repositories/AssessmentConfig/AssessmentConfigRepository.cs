﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.API.Models;
using DbConnection;

namespace Phoenix.API.Repositories
{
    public class AssessmentConfigRepository : SqlRepository<GradeTemplate>, IAssessmentConfigRepository
    {
        private readonly IConfiguration _config;
        public AssessmentConfigRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        public bool AddEditAssessmentConfig(List<GradeTemplate> obj, string Description, string MasterId, long GradeTemplateMasterId, long SchoolId)
        {

            var parameters = new DynamicParameters();
            DataTable dtAssessmentConfig = new DataTable();
            dtAssessmentConfig.Columns.Add("Id", typeof(long));
            dtAssessmentConfig.Columns.Add("TemPlateName", typeof(string));
            dtAssessmentConfig.Columns.Add("ShortCode", typeof(string));
            dtAssessmentConfig.Columns.Add("Description", typeof(string));
            dtAssessmentConfig.Columns.Add("ColorCode", typeof(string));
            dtAssessmentConfig.Columns.Add("GTD_Value", typeof(string));

            dtAssessmentConfig.Columns.Add("GTD_Order", typeof(int));


            foreach (var item in obj)
            {
                dtAssessmentConfig.Rows.Add(item.GradeTemplateDetailId, item.TemPlateName, item.ShortCode, item.Description, item.ColorCode, item.Value, item.GradeOrder);
            }
            parameters.Add("@AssessmentConfigCUD", dtAssessmentConfig, DbType.Object);
           
            parameters.Add("@Description", Description, DbType.String);
            parameters.Add("@MasterIds", MasterId, DbType.String);
            parameters.Add("@GTM_Id", GradeTemplateMasterId, DbType.Int32);
            parameters.Add("@SchoolId", SchoolId, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("[ASSESSMENT].[AssessmentConfigrationCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }

        }

        public bool AddEditGradeSlab(List<GradeSlab> obj, string Description, string MasterIds, long GradeSlabMasterId, long Acd_Id)
        {

            var parameters = new DynamicParameters();
            DataTable dtAssessmentConfig = new DataTable();
            dtAssessmentConfig.Columns.Add("Id", typeof(long));
            dtAssessmentConfig.Columns.Add("Code", typeof(string));
            dtAssessmentConfig.Columns.Add("Description", typeof(string));
            dtAssessmentConfig.Columns.Add("Color", typeof(string));
            dtAssessmentConfig.Columns.Add("FromMark", typeof(int));

            dtAssessmentConfig.Columns.Add("ToMark", typeof(int));


            foreach (var item in obj)
            {
                dtAssessmentConfig.Rows.Add(item.GradeSlabId, item.Code, item.Description, item.Colour, item.FromMark, item.ToMark);
            }
            parameters.Add("@AssessmentConfigCUD", dtAssessmentConfig, DbType.Object);

            parameters.Add("@Description", Description, DbType.String);
            parameters.Add("@MasterIds", MasterIds, DbType.String);
            parameters.Add("@GradeSlabMasterId", GradeSlabMasterId, DbType.Int32);
            parameters.Add("@@Acd_Id", Acd_Id, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            using (var conn = GetOpenConnection())
            {
                conn.Query<int>("[ASSESSMENT].[AssessmentGradeSlabCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }


        }


        public async Task<IEnumerable<GradeTemplate>> GetAssessmentConfigList(long Acd_Id)

        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Acd_Id", Acd_Id, DbType.Int32);
                return await conn.QueryAsync<GradeTemplate>("[ASSESSMENT].[GetAssessmentConfigList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
         public async Task<IEnumerable<GradeTemplate>> GetAssessmentConfigMasterList(long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int32);
                return await conn.QueryAsync<GradeTemplate>("[ASSESSMENT].[GetAssessmentGradeMasterList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        
        public async Task<IEnumerable<GradeSlab>> GetGradeSlabList(long Acd_Id)

        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Acd_Id", Acd_Id, DbType.Int32);
                return await conn.QueryAsync<GradeSlab>("[ASSESSMENT].[GetAssessmentGradeSlabList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<GradeSlabMaster>> GetGradeSlabMasterList(long SchoolId)

        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int32);
                return await conn.QueryAsync<GradeSlabMaster>("[ASSESSMENT].[GetAssessmentGradeSlabMasterList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<GradeTemplate>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<GradeTemplate> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(GradeTemplate entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(GradeTemplate entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
