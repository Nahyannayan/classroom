﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class LanguageSettingsRepository : SqlRepository<SystemLanguage>, ILanguageSettingsRepository
    {
        private readonly IConfiguration _config;
        public LanguageSettingsRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        public override Task<IEnumerable<SystemLanguage>> GetAllAsync()
        {
            throw new NotImplementedException();
        }
        public override Task<SystemLanguage> GetAsync(int id)
        {
            throw new NotImplementedException();
        }
        public override void InsertAsync(SystemLanguage entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SystemLanguage entityToUpdate)
        {
            throw new NotImplementedException();
        }
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }
        public async Task<SystemLanguage> GetSchoolCurrentLanguage(int SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                string query = "Admin.GetSchoolCurrentLanguage";
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<SystemLanguage>(query, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public bool SetSchoolCurrentLanguage(int languageId, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                bool response = false;
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int32);
                response = conn.QueryFirstOrDefault<bool>("Admin.SetSchoolCurrentLanguage", parameters, commandType: CommandType.StoredProcedure);
                return response;
            }
        }
        public bool SetUserCurrentLanguage(int languageId, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@userId", userId, DbType.Int64);
                parameters.Add("@SystemLanguageId", languageId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Admin.SetUserCurrentLanguage", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<SystemLanguage> GetUserCurrentLanguage(int languageId)
        {
            using (var conn = GetOpenConnection())
            {
                string query = "Admin.GetUserCurrentLanguage";
                var parameters = new DynamicParameters();
                parameters.Add("@LanguageId", languageId, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<SystemLanguage>(query, parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SystemLanguage>> GetSystemLanguages()
        {
            using (var conn = GetOpenConnection())
            {
                string query = "Admin.GetSystemLanguages";
                return await conn.QueryAsync<SystemLanguage>(query, null, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
