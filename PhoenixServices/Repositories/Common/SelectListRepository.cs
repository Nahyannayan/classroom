﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;

namespace Phoenix.API.Repositories
{
    public class SelectListRepository : SqlRepository<ListItem>, ISelectListRepository
    {
        private readonly IConfiguration _config;

        public SelectListRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<ListItem>> GetSelectListItems(string listCode, string whereCondition, string whereConditionParamValues, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SelectListCode", listCode, DbType.String);
                parameters.Add("@SelectListCode", listCode, DbType.String);
                parameters.Add("@WhereCondition", whereCondition, DbType.String);
                parameters.Add("@WhereConditionParamValues", whereConditionParamValues, DbType.String);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<ListItem>("Admin.GetSelectListItems", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SubjectListItem>> GetSubjectsListByUserId(int languageId, int userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@LanguageId", languageId, DbType.Int32);
                return await conn.QueryAsync<SubjectListItem>("School.GetSubjectsByUserId", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AcademicYearList>> GetAcademicYearByUserId(int userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@teacherId", userId, DbType.Int32);

                return await conn.QueryAsync<AcademicYearList>("Assignment.GetAcademicYearList", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ListItem>> GetAssignmentCategoryMasterList()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                return await conn.QueryAsync<ListItem>("[Assignment].[GetAssignmentCategoryMasterList]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<AssignmentCategory>> GetAssignmentCategoryMappingListBySchoolId(long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                return await conn.QueryAsync<AssignmentCategory>("[Assignment].[GetAssignmentCategoryMappingListBySchoolId]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #region Generated Methods

        public async override Task<ListItem> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<ListItem>> GetAllAsync()
        {
            throw new NotImplementedException();
        }


        public override void InsertAsync(ListItem entity)
        {
            throw new NotImplementedException();
        }


        public override void UpdateAsync(ListItem entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
