﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Phoenix.Common.ViewModels;
using Phoenix.Common.Models;
using Phoenix.Common.DataAccess;
using Phoenix.Common.Helpers;

namespace Phoenix.API.Repositories
{
    public class EmailSettingsRepository : SqlRepository<EmailSettingsView>, IEmailSettingsRepository
    {
        private readonly IConfiguration _config;

        public EmailSettingsRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<EmailSettingsView>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<EmailSettingsView> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<EmailSettingsView> GetEmailSettings()
        {
            using (var conn = GetOpenConnection())
            {
                string query = "SELECT TOP 1 AuthorizedName, SenderName, SenderEmail, SenderPassword, SMTPClient, PortNo, IsSSLEnabled FROM Admin.EmailSettings";

                return await conn.QueryFirstOrDefaultAsync<EmailSettingsView>(query, commandType: CommandType.Text);
            }
        }


        public override void InsertAsync(EmailSettingsView entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(EmailSettingsView entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
