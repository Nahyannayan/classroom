﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class CurriculumRoleRepository : SqlRepository<CurriculumRole>, ICurriculumRoleRepository
    {
        private readonly IConfiguration _config;

        public CurriculumRoleRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<CurriculumRole>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<CurriculumRole> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<CurriculumRole> GetUserCurriculumRole(string BSU_ID = "", string UserName = "")
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSU_ID", BSU_ID, DbType.String);
                parameters.Add("@UserName", UserName, DbType.String);
                return await conn.QueryFirstOrDefaultAsync<CurriculumRole>("SIMS.GetACDID_CLMID_IsSuperUser", parameters, null, null, CommandType.StoredProcedure);
            }

        }
        public async Task<IEnumerable<CourseDetails>> GetTeacherCourse(long id, long Schoolid, long curriculumId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@teacherid", id, DbType.Int64);
                parameters.Add("@SchoolId", Schoolid, DbType.Int64);
                parameters.Add("@curriculumId", curriculumId, DbType.Int64);
                return await conn.QueryAsync<CourseDetails>("[Attendance].[GetTeacherCourse]", parameters, null, null, CommandType.StoredProcedure);
            }

        }
        public async Task<IEnumerable<GradeTemplateMaster>> GradeTemplateMaster(long acd_id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Acd_Id", acd_id, DbType.Int32);
                return await conn.QueryAsync<GradeTemplateMaster>("[ASSESSMENT].[GetGradeTemplateMasterDetail]", parameters, null, null, CommandType.StoredProcedure);
            }

        }
        

        public async Task<IEnumerable<Curriculum>> GetCurriculum(long BSU_ID, int? CLM_ID)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSU_ID", BSU_ID, DbType.String);
                parameters.Add("@CLM_ID", CLM_ID, DbType.Int32);
                return await conn.QueryAsync<Curriculum>("[SIMS].[GetCurriculum]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(CurriculumRole entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(CurriculumRole entityToUpdate)
        {
            throw new NotImplementedException();
        }


    }
}
