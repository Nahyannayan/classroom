﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Models.ViewModels;
using Phoenix.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class SystemImageFileRepository : SqlRepository<SystemImageViewModel>, ISystemImageFileRepository
    {
        private readonly IConfiguration _config;
        public SystemImageFileRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<SystemImageViewModel>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<SystemImageViewModel> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<SystemImageViewModel>> GetSystemImageList()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<SystemImageViewModel>("Course.GetSystemImageList", null, null, null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(SystemImageViewModel entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SystemImageViewModel entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
