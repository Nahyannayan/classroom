﻿using Phoenix.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IEmailSettingsRepository
    {
        Task<EmailSettingsView> GetEmailSettings();
    }
}
