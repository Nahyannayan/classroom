﻿using DbConnection;

using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Phoenix.API.Models;

namespace Phoenix.API.Repositories
{
    public interface ICurriculumRoleRepository
    {
        Task<CurriculumRole> GetUserCurriculumRole(string BSU_ID = "", string UserName="");
        Task<IEnumerable<Curriculum>> GetCurriculum(long BSU_ID, int? CLM_ID);
        Task<IEnumerable<CourseDetails>> GetTeacherCourse(long id, long Schoolid,long curriculumId);
        Task<IEnumerable<GradeTemplateMaster>> GradeTemplateMaster(long acd_id);
        

    }
}
