﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using Phoenix.Common.Models;
using Phoenix.Common.Helpers;

namespace Phoenix.API.Repositories
{
    public interface ISelectListRepository : IGenericRepository<ListItem>
    {
        Task<IEnumerable<ListItem>> GetSelectListItems(string listCode, string whereCondition, string whereConditionParamValues, short languageId);
        Task<IEnumerable<SubjectListItem>> GetSubjectsListByUserId(int languageId, int userId);
        Task<IEnumerable<AcademicYearList>> GetAcademicYearByUserId(int userId);
        Task<IEnumerable<ListItem>> GetAssignmentCategoryMasterList();
        Task<IEnumerable<AssignmentCategory>> GetAssignmentCategoryMappingListBySchoolId(long SchoolId);
    }
}
