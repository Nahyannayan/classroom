﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;

namespace Phoenix.API.Repositories
{
    public class SchoolNotificationRepository : SqlRepository<DeploymentNotification>, ISchoolNotificationRepository
    {
        private readonly IConfiguration _config;
        public SchoolNotificationRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }



        public async Task<bool> SaveDeploymentNotificationData(DeploymentNotification model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@DeploymentNotificationId", model.DeploymentNotificationId, DbType.Int64);
                parameters.Add("@NotificationMessage", model.NotificationMessage, DbType.String);
                parameters.Add("@IsActive", model.IsActive, DbType.Boolean);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.SaveDeploymentNotification", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public async Task<IEnumerable<DeploymentNotification>> GetSchoolDeploymentNotificationList()
        {
            IEnumerable<DeploymentNotification> lst = new List<DeploymentNotification>();

            using (var conn = GetOpenConnection())
            {
                string query = @"SELECT DeploymentNotificationId, NotificationMessage
		            ,IsActive FROM School.DeploymentNotification";
                var parameters = new DynamicParameters();
                var result = await conn.QueryMultipleAsync(query, parameters, null, null, CommandType.Text);
                lst = await result.ReadAsync<DeploymentNotification>();
                return lst;
            }
        }

        #region Abstract methods
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<DeploymentNotification>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<DeploymentNotification> GetAsync(int id)
        {
            throw new NotImplementedException();
        }


        public override void InsertAsync(DeploymentNotification entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(DeploymentNotification entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
