﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class DivisionRepository : SqlRepository<DivisionDetails>, IDivisionRepositorycs
    {
        private readonly IConfiguration _config;
        public DivisionRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<DivisionDetails>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<DivisionDetails> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(DivisionDetails entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(DivisionDetails entityToUpdate)
        {
            throw new NotImplementedException();
        }
        public async Task<OperationDetails> SaveDivisionDetails(DivisionDetails DivisionDetails, string DATAMODE)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@DivisionId", DivisionDetails.DivisionId, DbType.Int64);
                parameters.Add("@DivisionName", DivisionDetails.DivisionName, DbType.String);
                parameters.Add("@GradeId", DivisionDetails.GradeList, DbType.String);
                parameters.Add("@SchoolId", DivisionDetails.BSU_ID, DbType.Int64);
                parameters.Add("@CurriculumId", DivisionDetails.CurriculumId, DbType.Int64);
                parameters.Add("@CreatedBy", DivisionDetails.CREATED_BY, DbType.Int64);
                parameters.Add("@DATAMODE", DATAMODE, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("SCHOOL.SaveDivisionDetails", parameters, null, null, CommandType.StoredProcedure);
                var id = parameters.Get<int>("output");

                var operationDetails = new OperationDetails();
                operationDetails.Success = id > 0;
                operationDetails.InsertedRowId = Convert.ToInt32(id);
                return operationDetails;
            }
        }
        public async Task<IEnumerable<DivisionDetails>> GetDivisionDetails(long BSU_ID, int CurriculumId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BSU_ID", BSU_ID, DbType.Int64);
                parameters.Add("@CurriculumId", CurriculumId, DbType.Int32);

                return await conn.QueryAsync<DivisionDetails>("SCHOOL.GetDivisionDetails", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ReportingTermModel>> GetReportingTermDetail(long ReportingTermId, long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ReportingTermId", ReportingTermId, DbType.Int64);
                parameters.Add("@SchoolId", SchoolId, DbType.Int64);
                return await conn.QueryAsync<ReportingTermModel>("SCHOOL.GetReportingTermDetail", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<ReportingSubTermModel>> GetReportingSubTermDetail(long ReportingTermId, long ReportingSubTermId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ReportingTermId", ReportingTermId, DbType.Int64);
                parameters.Add("@ReportingSubTermId", ReportingSubTermId, DbType.Int64);
                return await conn.QueryAsync<ReportingSubTermModel>("SCHOOL.GetReportingSubTermDetail", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<bool> SaveReportingTermDetail(ReportingTermModel reportingTermModel)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@ReportingTermId", reportingTermModel.ReportingTermId, DbType.Int64);
                parameters.Add("@DivisionId", reportingTermModel.DivisionId, DbType.Int64);
                parameters.Add("@TermDescription", reportingTermModel.TermDescription, DbType.String);
                parameters.Add("@StartDate", reportingTermModel.StartDate, DbType.DateTime);
                parameters.Add("@EndDate", reportingTermModel.EndDate, DbType.DateTime);
                parameters.Add("@IsLock", reportingTermModel.IsLock, DbType.Boolean);
                parameters.Add("@OperatedBy", reportingTermModel.UserId, DbType.Int64);
                parameters.Add("@SchoolId", reportingTermModel.SchoolId, DbType.Int64);
                parameters.Add("@DATAMODE", reportingTermModel.DATAMODE, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("SCHOOL.SaveReportingTermDetail", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<bool> LockUnlockTerm(long ReportingTermId, bool IsLock)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ReportingTermId", ReportingTermId, DbType.Int64);
                parameters.Add("@IsLock", IsLock, DbType.Boolean);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("SCHOOL.LockUnlockTerm", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<bool> SaveReportingSubTermDetail(ReportingSubTermModel reportingSubTermModel)
        {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@ReportingTermId", reportingSubTermModel.ReportingTermId, DbType.Int64);
                parameters.Add("@ReportingSubTermId", reportingSubTermModel.ReportingSubTermId, DbType.Int64);
                parameters.Add("@SubTermDescription", reportingSubTermModel.SubTermDescription, DbType.String);
                parameters.Add("@StartDate", reportingSubTermModel.StartDate, DbType.DateTime);
                parameters.Add("@EndDate", reportingSubTermModel.EndDate, DbType.DateTime);
                parameters.Add("@IsLock", reportingSubTermModel.IsLock, DbType.Boolean);
                parameters.Add("@OperatedBy", reportingSubTermModel.UserId, DbType.Int64);
                parameters.Add("@DATAMODE", reportingSubTermModel.DATAMODE, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("SCHOOL.SaveReportingSubTermDetail", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<bool> LockUnlockSubTerm(long ReportingSubTermId, bool IsLock)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ReportingSubTermId", ReportingSubTermId, DbType.Int64);
                parameters.Add("@IsLock", IsLock, DbType.Boolean);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryFirstOrDefaultAsync<int>("SCHOOL.LockUnlockSubTerm", parameters, null, null, CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
    }
}
