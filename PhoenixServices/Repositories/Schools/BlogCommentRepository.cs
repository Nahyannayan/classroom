﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;

namespace Phoenix.API.Repositories
{
    public class BlogCommentRepository : SqlRepository<BlogComment>, IBlogCommentRepository
    {
        private readonly IConfiguration _config;
        public BlogCommentRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogCommentParameters(new BlogComment(id), TransactionModes.Delete);
                conn.Query<Int32>("School.BlogCommentCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }


        public void DeleteBlogComment(int id, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var model = new BlogComment(id);
                model.UpdatedBy = userId;
                var parameters = GetBlogCommentParameters(model, TransactionModes.Delete);
                conn.Query<Int32>("School.BlogCommentCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public override async Task<IEnumerable<BlogComment>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", DBNull.Value, DbType.Int32);
                parameters.Add("@CommentId", DBNull.Value, DbType.Int32);
                parameters.Add("@PublishBy", DBNull.Value, DbType.Int32);
                parameters.Add("@IsPublish", DBNull.Value, DbType.Boolean);
                return await conn.QueryAsync<BlogComment>("School.GetBlogComments", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override async Task<BlogComment> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", DBNull.Value, DbType.Int32);
                parameters.Add("@CommentId", id, DbType.Int32);
                parameters.Add("@PublishBy", DBNull.Value, DbType.Int32);
                parameters.Add("@IsPublish", DBNull.Value, DbType.Boolean);
                return await conn.QueryFirstOrDefaultAsync<BlogComment>("School.GetBlogComments", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<BlogComment>> GetBlogCommentByPublishBy(int publishId, bool? IsPublish)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", DBNull.Value, DbType.Int32);
                parameters.Add("@CommentId", DBNull.Value, DbType.Int32);
                parameters.Add("@PublishBy", publishId, DbType.Int32);
                parameters.Add("@IsPublish", IsPublish, DbType.Boolean);
                return await conn.QueryAsync<BlogComment>("School.GetBlogComments", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<BlogComment>> GetBlogCommentByBlogId(int blogId, bool? IsPublish)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", blogId, DbType.Int32);
                parameters.Add("@CommentId", DBNull.Value, DbType.Int32);
                parameters.Add("@PublishBy", DBNull.Value, DbType.Int32);
                parameters.Add("@IsPublish", IsPublish, DbType.Boolean);
                return await conn.QueryAsync<BlogComment>("School.GetBlogComments", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(BlogComment entity)
        {
            throw new NotImplementedException();
        }

        public int Insert(BlogComment entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogCommentParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("School.BlogCommentCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public override void UpdateAsync(BlogComment entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogCommentParameters(entityToUpdate, TransactionModes.Update);
                conn.Query<Int32>("School.BlogCommentCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        #region Private Methods
        private DynamicParameters GetBlogCommentParameters(BlogComment entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@CommentId", entity.CommentId, DbType.Int64);
            parameters.Add("@BlogId", entity.BlogId, DbType.Int64);
            parameters.Add("@Comment", entity.Comment, DbType.String);
            parameters.Add("@IsPublish", entity.IsPublish, DbType.Boolean);
            parameters.Add("@PublishBy", entity.PublishBy, DbType.Int64);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        private DynamicParameters GetBlogCommentLikeParameters(BlogCommentLike entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@BlogCommentLikeId", entity.BlogCommentLikeId, DbType.Int64);
            parameters.Add("@CommentId", entity.CommentId, DbType.Int64);
            parameters.Add("@IsLike", entity.IsLike, DbType.Boolean);
            parameters.Add("@LikeTypeId", entity.LikeTypeId, DbType.Int32);
            parameters.Add("@LikedBy", entity.LikedBy, DbType.Int64);

            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        #endregion

        public bool InsertCommentLike(BlogCommentLike blogCommentLike)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogCommentLikeParameters(blogCommentLike, TransactionModes.Insert);
                conn.Query<Int32>("School.BlogCommentLikeCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public bool UpdatedCommentLike(BlogCommentLike blogCommentLike)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogCommentLikeParameters(blogCommentLike, TransactionModes.Update);
                conn.Query<Int32>("School.BlogCommentLikeCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public bool DeleteCommentLike(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogCommentLikeParameters(new BlogCommentLike(id), TransactionModes.Insert);
                conn.Query<Int32>("School.BlogCommentLikeCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public async Task<IEnumerable<BlogCommentLike>> GetBlogCommentLikes(int? commentId, int? blogCommentLikeId, int? likedBy, bool? isLike)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();

                parameters.Add("@CommentId", commentId, DbType.Int32);
                parameters.Add("@BlogCommentLikeId", blogCommentLikeId, DbType.Int32);
                parameters.Add("@LikedBy", likedBy, DbType.Int32);
                parameters.Add("@IsLike", isLike, DbType.Boolean);
                return await conn.QueryAsync<BlogCommentLike>("School.GetBlogCommentLikes", parameters, commandType: CommandType.StoredProcedure);
            }
        }

    }
}
