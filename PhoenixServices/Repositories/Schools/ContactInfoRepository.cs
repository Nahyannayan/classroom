﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;

namespace Phoenix.API.Repositories
{
    public class ContactInfoRepository : SqlRepository<ContactInfo>, IContactInfoRepository
    {
        private readonly IConfiguration _config;

        public ContactInfoRepository(IConfiguration configuration): base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<ContactInfo>> GetContactInfos()
        {
            using (var conn = GetOpenConnection())
            {
                //int schoolId = 0;
                var parameters = new DynamicParameters();
                //parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<ContactInfo>("School.GetContactInfo", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ContactInfo>> GetContactInfoBySchoolId(int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<ContactInfo>("School.GetContactInfo", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public bool UpdateContactInfoData(ContactInfo entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetContactInfoParameters(entity, mode);
                conn.Query<int>("School.SchoolContactInfoCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        private DynamicParameters GetContactInfoParameters(ContactInfo entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@ContactInfoId", entity.ContactInfoId, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int32);
            parameters.Add("@SchoolName", entity.SchoolName, DbType.String);
            parameters.Add("@SchoolEmail", entity.SchoolEmail, DbType.String);
            parameters.Add("@PrincipalName", entity.PrincipalName, DbType.String);
            parameters.Add("@PrimaryContactFirstName", entity.PrimaryContactFirstName, DbType.String);
            parameters.Add("@PrimaryContactLastName", entity.PrimaryContactLastName, DbType.String);
            parameters.Add("@Address1", entity.Address1, DbType.String);
            parameters.Add("@Address2", entity.Address2, DbType.String);
            parameters.Add("@Town", entity.Town, DbType.String);
            parameters.Add("@CountyId", entity.CountyId, DbType.Int32);
            parameters.Add("@CountryId", entity.CountryId, DbType.Int32);
            parameters.Add("@PostCode", entity.PostCode, DbType.String);
            parameters.Add("@Telephone", entity.Telephone, DbType.String);
            parameters.Add("@WebsiteUrl", entity.WebsiteUrl, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int32);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        #region Default repository methods
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async override Task<IEnumerable<ContactInfo>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async override Task<ContactInfo> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ContactInfoId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<ContactInfo>("School.GetContactInfoById", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public  override void InsertAsync(ContactInfo entity)
        {
            throw new NotImplementedException();
        }

        public  override void UpdateAsync(ContactInfo entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
