﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using DbConnection;
using Phoenix.Common.Enums;

namespace Phoenix.API.Repositories
{
    public interface IBlogCommentRepository: IGenericRepository<BlogComment>
    {
        Task<IEnumerable<BlogComment>> GetBlogCommentByBlogId(int blogId, bool? IsPublish);

        Task<IEnumerable<BlogComment>> GetBlogCommentByPublishBy(int publishId, bool? IsPublish);

        bool InsertCommentLike(BlogCommentLike blogCommentLike);

        int Insert(BlogComment entity);

        bool DeleteCommentLike(int id);

        


        Task<IEnumerable<BlogCommentLike>> GetBlogCommentLikes(int? commentId, int? blogCommentLikeId, int? likedBy, bool? isLike);
        void DeleteBlogComment(int id, long userId);
        bool UpdatedCommentLike(BlogCommentLike blogCommentLike);
    }
}
