﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IThemesRepository : IGenericRepository<Themes>
    {
        Task<IEnumerable<Themes>> GetAllSchoolThemesById(int? schoolId = null, short languageId=0);
        bool SchoolThemeCUD(Themes model, TransactionModes mode);
        Task<Themes> GetSchoolThemeById(int themeId, short languageId);
        Task<IEnumerable<Themes>> GetAllSchoolThemesByCurriculum(int? schoolId, int? curriculumId, short languageId);
        Task<IEnumerable<Themes>> GetStudentGradeTheme(long userId);
    }
}
