﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using DbConnection;
using Phoenix.Common.Enums;

namespace Phoenix.API.Repositories
{
    public interface IContactInfoRepository : IGenericRepository<ContactInfo>
    {
        Task<IEnumerable<ContactInfo>> GetContactInfos();
        Task<IEnumerable<ContactInfo>> GetContactInfoBySchoolId(int schoolId);
        bool UpdateContactInfoData(ContactInfo entity, TransactionModes mode);
    }
}
