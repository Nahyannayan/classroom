﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;
using DbConnection;
using Phoenix.Common.Enums;
using System.Data;

namespace Phoenix.API.Repositories
{
    public interface IChatRepository : IGenericRepository<Chat>
    {
        int Insert(Chat entity);
        Task<IEnumerable<Chat>> GetChatsBySchoolId(int schoolId);
        Task<IEnumerable<Chat>> GetChatsByMessageTypeId(int schoolId, int messagTypeIdh);
        Task<IEnumerable<Chat>> GetChatsByChatTypeId(int ChatTypeId);
        Task<IEnumerable<Chat>> GetChatsByGroupId(int groupId, int schoolId, int? userId);
        Task<IEnumerable<MessageTypes>> GetMessageTypes();
        Task<IEnumerable<ChatTypes>> GetChatTypes();
        Task<IEnumerable<ChatList>> GetRecentChats(int schoolId, int userId);
        Task<IEnumerable<Chat>> GetRecentChatUserList(int schoolId, int userId);
        Task<IEnumerable<RecentUserActivity>> GetRecentUserActivity(int schoolId, int userId, bool enableChat, long groupId);
        Task<IEnumerable<Chat>> GetChatHistorybyUserId(long schoolId, long userId, long otherUser, long groupId,int? pageNum);
        Task<IEnumerable<Chat>> GetSearchInChat(int schoolId, int userId, int userTypeId, string text);
        Task<IEnumerable<LogInUser>> GetMyContactsList(int schoolId, int userId);
        int InviteUserForChat(InviteChat entity);
        int UpdateInviteUserGroupChat(InviteChat entity);
        int DeleteInviteUserGroupChat(InviteChat entity);
        string UpdateInviteChatGroupName(InviteChat entity);
        int GetLatestInviteGroup();
        Task<IEnumerable<InviteChat>> GetGroupChatUsers(long groupId, long schoolId);
        Task<IEnumerable<InviteChat>> GetGroupDetails(long? groupid, long? schoolid, long? userid, string groupname);
        string GetInviteGroupName(long groupId);
        Task<IEnumerable<InviteChat>> GetAllGroupUsers(long schoolId);
        int DeleteChatConversation(long schoolId, long groupId, long fromUser, long toUser);
        int InsertChatLog(UserChatLog entity);
        int DeleteChatlog(UserChatLog entity);
        int DeleteAllChatlog();
        Task<IEnumerable<UserChatLog>> GetConnectionbyUserId(string connectionid, long? schoolId, long? userId, string sessionid);
    }
}
