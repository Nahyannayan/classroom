﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace Phoenix.API.Repositories
{
    public interface ISchoolRepository : IGenericRepository<SchoolInformation>
    {
        Task<IEnumerable<SchoolInformation>> GetSchoolList();
        Task<IEnumerable<SchoolInformation>> GetAdminSchoolList();
        Task<FileDownload> GetModuleFileDetails(int id, string filetype);
        Task<ReportDashboard> GetReportDashboard(long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId);
        Task<IEnumerable<ChildQuizReportData>> GetChildQuizReportData(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        Task<IEnumerable<TeacherList>> GetSchoolTeachersBySchoolId(long schoolId);
        Task<IEnumerable<QuizReportData>> GetQuizReportDataWithDateRange(long? userId, long? schoolId, DateTime startDate, DateTime endDate);
        Task<GetReportDetail> GetReportDetails(DateTime startDate, DateTime endDate);
        Task<IEnumerable<SchoolInformation>> GetAllBusinesUnitSchools(int pageIndex, int pageSize, string searchString);
        Task<bool> UpdateSchoolClassroomStatus(SchoolInformation schoolInformation);
        Task<IEnumerable<TableData>> GetSchoolGroupAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        Task<IEnumerable<ChatterTableData>> GetGroupChatterReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        Task<IEnumerable<TeacherAssignment>> GetTeacherAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        Task<IEnumerable<StudentAssignment>> GetStudentAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        Task<IEnumerable<Data>> GetSchoolAbuseReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId);
        Task<LoginDetailData> GetSchoolLoginReport(long userId, long schoolId, string startDate, string endDate);
        Task<AssignmentReportCard> GetAssignmentReportData(long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId);
        Task<SessionConfiguration> GetSchoolSessionStatus(long schoolId);
        Task<bool> SaveSchoolSessionStatus(SessionConfiguration model);

        #region FFOR DEPARTMENT
        Task<bool> DepartmentCourseCU(Department model);
        Task<IEnumerable<Department>> GetDepartmentCourse(long schoolId, long curriculumId, long? departmentId);
        Task<bool> CanDeleteDepartment(long departmentId);
        Task<IEnumerable<Student>> GetStudentVaultReportData(long schoolId, long teacherId);

        #endregion
    }
}
