﻿using DbConnection;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IBlogShareRepository : IGenericRepository<ShareBlogs>
    {
        int Insert(ShareBlogs entity);
        int ShareBlogs(ShareBlogs entity);
    }
}
