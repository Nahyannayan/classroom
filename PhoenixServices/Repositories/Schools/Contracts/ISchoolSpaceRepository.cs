﻿using DbConnection;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface ISchoolSpaceRepository : IGenericRepository<SchoolSpace>
    {
        Task<IEnumerable<SchoolSpace>> GetSchoolSpaceBySchoolId(int schoolId);
        bool UpdateSchoolSpaceData(SchoolSpace entity, TransactionModes mode);
    }
}
