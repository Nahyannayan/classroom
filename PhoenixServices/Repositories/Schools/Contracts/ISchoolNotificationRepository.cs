﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models;

namespace Phoenix.API.Repositories
{
    public interface ISchoolNotificationRepository
    {
        Task<IEnumerable<DeploymentNotification>> GetSchoolDeploymentNotificationList();
        Task<bool> SaveDeploymentNotificationData(DeploymentNotification model);
    }
}
