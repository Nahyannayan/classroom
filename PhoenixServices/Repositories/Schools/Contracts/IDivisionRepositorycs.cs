﻿using Phoenix.API.Models;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IDivisionRepositorycs
    {
        Task<OperationDetails> SaveDivisionDetails(DivisionDetails DivisionDetails, string DATAMODE);
        Task<IEnumerable<DivisionDetails>> GetDivisionDetails(long BSU_ID, int CurriculumId);
        Task<IEnumerable<ReportingTermModel>> GetReportingTermDetail(long ReportingTermId, long SchoolId);
        Task<IEnumerable<ReportingSubTermModel>> GetReportingSubTermDetail(long ReportingTermId, long ReportingSubTermId);
        Task<bool> SaveReportingTermDetail(ReportingTermModel reportingTermModel);
        Task<bool> LockUnlockTerm(long ReportingTermId, bool IsLock);
        Task<bool> SaveReportingSubTermDetail(ReportingSubTermModel reportingSubTermModel);
        Task<bool> LockUnlockSubTerm(long ReportingSubTermId, bool IsLock);
    }
}
