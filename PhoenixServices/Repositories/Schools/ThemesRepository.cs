﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class ThemesRepository : SqlRepository<Themes>, IThemesRepository
    {
        private readonly IConfiguration _config;

        public ThemesRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<Themes>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<Themes> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(Themes entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(Themes entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Themes>> GetAllSchoolThemesById(int? schoolId = null, short languageId=0)
        {
            using (var conn = GetOpenConnection())
            {
                //int schoolId = 0;
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<Themes>("School.GetAllSchoolThemesById", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<Themes> GetSchoolThemeById(int schoolGradeId, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolGradeId", schoolGradeId, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryFirstOrDefaultAsync<Themes>("[School].[GetSchoolThemeById]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public bool SchoolThemeCUD(Themes model, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ThemeId", model.ThemeId, DbType.Int32);
                parameters.Add("@TransMode", (int)mode, DbType.Int32);
                parameters.Add("@SchoolId", model.SchoolId, DbType.Int64);
                parameters.Add("@SchoolGradeIds", model.SchoolGradeIds, DbType.String);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.SchoolThemeCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<IEnumerable<Themes>> GetAllSchoolThemesByCurriculum(int? schoolId, int? curriculumId, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                //int schoolId = 0;
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@CurriculumId", curriculumId, DbType.Int32);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<Themes>("[School].[GetAllSchoolThemesByCurriculum]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Themes>> GetStudentGradeTheme(long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<Themes>("[School].[GetStudentGradeTheme]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
    }
}
