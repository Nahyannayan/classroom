﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;

namespace Phoenix.API.Repositories
{
    public class SchoolSpaceRepository : SqlRepository<SchoolSpace>, ISchoolSpaceRepository
    {
        private readonly IConfiguration _config;

        public SchoolSpaceRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async override Task<IEnumerable<SchoolSpace>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SpaceId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                return await conn.QueryAsync<SchoolSpace>("School.GetSchoolSpacesBySchool", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async override Task<SchoolSpace> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SpaceId", id , DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<SchoolSpace>("School.GetSchoolSpacesBySchool", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolSpace>> GetSchoolSpaceBySchoolId(int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SpaceId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<SchoolSpace>("School.GetSchoolSpacesBySchool", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(SchoolSpace entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SchoolSpace entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public bool UpdateSchoolSpaceData(SchoolSpace entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSchoolSpaceParameters(entity, mode);
                conn.Query<int>("School.SchoolSpaceCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("Output") > 0;
            }
        }

        private DynamicParameters GetSchoolSpaceParameters(SchoolSpace entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SpaceId", entity.SpaceId, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int32);
            parameters.Add("@SpaceName", entity.SpaceName, DbType.String);
            parameters.Add("@Description", entity.Description, DbType.String);
            parameters.Add("@UseWebsite", entity.UseWebsite, DbType.String);
            parameters.Add("@IsActive", entity.IsActive, DbType.String);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.String);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.String);
            parameters.Add("@CategoryId", entity.CategoryId, DbType.Int32);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
    }
}
