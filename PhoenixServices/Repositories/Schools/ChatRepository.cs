﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;

namespace Phoenix.API.Repositories
{
    public class ChatRepository : SqlRepository<Chat>, IChatRepository
    {
        private readonly IConfiguration _config;
        public ChatRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        /// <summary>
        /// To Delete method to delete the chat which is in not in use currently
        /// </summary>
        /// <param name="id"></param>
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to get all the chats
        /// </summary>
        /// <returns></returns>
        public override async Task<IEnumerable<Chat>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ChatId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int64);
                parameters.Add("@MessageTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@ChatTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int64);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                return await conn.QueryAsync<Chat>("School.GetChats", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Method to get chat based on ChatId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task<Chat> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ChatId", id, DbType.Int64);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int64);
                parameters.Add("@MessageTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@ChatTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int64);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<Chat>("School.GetChats", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Method to get Types of messages in chat system
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<MessageTypes>> GetMessageTypes()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                return await conn.QueryAsync<MessageTypes>("School.GetMessageTypes", null, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Method to get Chats based on Type of messages
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="messagtypeId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Chat>> GetChatsByMessageTypeId(int schoolId, int messagtypeId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ChatId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@MessageTypeId", messagtypeId, DbType.Int32);
                parameters.Add("@ChatTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int64);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                return await conn.QueryAsync<Chat>("School.GetChats", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Method to get chats based on school
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Chat>> GetChatsBySchoolId(int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ChatId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@MessageTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@ChatTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int64);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                return await conn.QueryAsync<Chat>("School.GetChats", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Method to get chats bases on type of chat wheather its private or group chat
        /// </summary>
        /// <param name="ChatTypeId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Chat>> GetChatsByChatTypeId(int ChatTypeId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ChatId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int64);
                parameters.Add("@MessageTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@ChatTypeId", ChatTypeId, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int64);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                return await conn.QueryAsync<Chat>("School.GetChats", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Method to get Chats for a particular group
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Chat>> GetChatsByGroupId(int groupId, int schoolId, int? userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ChatId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@MessageTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@ChatTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@UserId", DBNull.Value, DbType.Int64);
                return await conn.QueryAsync<Chat>("School.GetChats", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Method to insert Chat into the database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Insert(Chat entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetChatParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("School.ChatsCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }


        /// <summary>
        /// Asynchronous method to insert the chat into the database
        /// </summary>
        /// <param name="entity"></param>
        public override void InsertAsync(Chat entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to get Chat related parameters
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        private DynamicParameters GetChatParameters(Chat entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@ChatId", entity.ChatId, DbType.Int64);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@ChatTypeId", entity.ChatTypeId, DbType.Int32);
            parameters.Add("@GroupId", entity.GroupId, DbType.Int64);

            parameters.Add("@ChatMessage", EncryptDecryptHelper.Encrypt(entity.ChatMessage), DbType.String);
            parameters.Add("@FromUser", entity.FromUserId, DbType.Int64);
            parameters.Add("@ToUser", entity.ToUserId, DbType.Int64);
            parameters.Add("@MessageDateTime", entity.MessageDateTime, DbType.DateTime);
            parameters.Add("@ContentPath", entity.ContentPath, DbType.String);
            parameters.Add("@FileName", entity.FileName, DbType.String);
            parameters.Add("@MessageTypeId", entity.MessageTypeId, DbType.Int32);
            parameters.Add("@IsRead", entity.IsRead, DbType.Boolean);
            parameters.Add("@UserIPAddress", entity.UserIPAddress, DbType.String);
            parameters.Add("@WebServerIPAddress", entity.WebServerIPAddress, DbType.String);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        private DynamicParameters GetChatLogParameters(UserChatLog entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@ConnectionId", entity.ConnectionId, DbType.String);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@UserId", entity.UserId, DbType.Int64);
            parameters.Add("@UserName", entity.UserName, DbType.String);
            parameters.Add("@UserLoginTime", entity.UserLoginTime, DbType.DateTime);
            parameters.Add("@UserLogoutTime", entity.UserLogoutTime, DbType.DateTime);
            parameters.Add("@CurrentStatus", entity.CurrentStatus, DbType.Int32);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@SessionId", entity.SessionId, DbType.String);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        /// <summary>
        /// parameter setting for Invite Group Chat
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        private DynamicParameters GetInviteChatParameters(InviteChat entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@InviteChatId", entity.Id, DbType.Int64);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@GroupId", entity.InviteGroupId, DbType.Int64);
            parameters.Add("@GroupName", entity.InviteGroupName, DbType.String);
            parameters.Add("@SignalRGroupName", entity.SignalRGroupName, DbType.String);
            parameters.Add("@GroupUserId", entity.GroupUserId, DbType.Int64);
            parameters.Add("@InvitedOn", entity.InvitedOn, DbType.DateTime);
            parameters.Add("@InvitedBy", entity.InvitedBy, DbType.Int64);
            parameters.Add("@UpdatedOn", entity.UpdatedOn, DbType.DateTime);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            parameters.Add("@GroupJoiningTime", entity.GroupJoiningTime, DbType.DateTime);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

      private DynamicParameters GetUpdateGroupNameParameters(InviteChat entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@GroupId", entity.InviteGroupId, DbType.Int64);
            parameters.Add("@GroupName", entity.InviteGroupName, DbType.String);
            parameters.Add("@SignalRGroupName", entity.SignalRGroupName, DbType.String);
            parameters.Add("@UpdatedOn", entity.UpdatedOn, DbType.DateTime);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@UpdatedGroupName", dbType: DbType.String, direction: ParameterDirection.Output, size:4000);
            return parameters;
        }

        /// <summary>
        /// Method to get all the chat types
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ChatTypes>> GetChatTypes()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                return await conn.QueryAsync<ChatTypes>("School.GetChatTypes", null, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// To get the recent chat of a user - NOT IN USE
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ChatList>> GetRecentChats(int schoolId, int userid)
        {
            List<ChatList> userChatList = new List<ChatList>();
            ChatList chatList = new ChatList();

            IEnumerable<Chat> userChats = null;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ChatId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@MessageTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@ChatTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int64);
                parameters.Add("@UserId", userid, DbType.Int64);
                parameters.Add("@OtherUser", DBNull.Value, DbType.Int64);
                userChats = await conn.QueryAsync<Chat>("School.GetChatHistory", parameters, commandType: CommandType.StoredProcedure);
            }

            // logic to extract chat history for each different users
            foreach (Chat eachchat in userChats)
            {
                if (chatList.ChatListId == 0 || (chatList.ChatListId != eachchat.FromUserId && chatList.ChatListId != eachchat.ToUserId))
                {
                    if (chatList.ChatListId != 0)
                    {
                        var localChatList = chatList;
                        userChatList.Add(localChatList);
                        chatList = new ChatList();
                    }

                    if (eachchat.FromUserId != userid)
                        chatList.ChatListId = eachchat.FromUserId;
                    else
                        chatList.ChatListId = eachchat.ToUserId;
                }
                chatList.UserChatList.Add(eachchat);
            }
            // To add last element
            userChatList.Add(chatList);
            // reverse the user chat with the latest chat in first order
            userChatList.Reverse();

            return userChatList;
        }

        /// <summary>
        /// To Get the Chat history by user
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <param name="otherUser"></param>
        /// <param name="groupId"></param>
        /// <param name="pageNum"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Chat>> GetChatHistorybyUserId(long schoolId, long userId, long otherUser, long groupId,int? pageNum = 1)
        {
            IEnumerable<Chat> data = null;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ChatId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@MessageTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@ChatTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@OtherUser", otherUser, DbType.Int64);
                parameters.Add("@PageNum", pageNum, DbType.Int32);
                data = await conn.QueryAsync<Chat>("School.GetChatHistory", parameters, commandType: CommandType.StoredProcedure);
            }
            foreach (var chat in data)
            {
                chat.ChatMessage = EncryptDecryptHelper.Decrypt(chat.ChatMessage);
            }
            return data.Reverse();
        }

        /// <summary>
        /// To Get the recent chat user lists
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Chat>> GetRecentChatUserList(int schoolId, int userid)
        {
            IEnumerable<Chat> recentChatuserList = null;
            List<Chat> distinctRecentChatUserList = new List<Chat>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@UserId", userid, DbType.Int64);
                recentChatuserList = await conn.QueryAsync<Chat>("School.GetRecentChatUser", parameters, commandType: CommandType.StoredProcedure);
            }
            recentChatuserList = recentChatuserList.Reverse();
            foreach(var item in recentChatuserList)
            {
                if(item.GroupId == 0 || (item.GroupId > 0 && !distinctRecentChatUserList.Any(i => i.GroupId == item.GroupId)))
                {
                    distinctRecentChatUserList.Add(item);
                }
            }
            return distinctRecentChatUserList;
        }

        /// <summary>
        ///To get the User Activity by UserId
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userid"></param>
        /// <param name="groupId"></param>
        /// <param name="enableChat"></param>
        /// <returns></returns>
        public async Task<IEnumerable<RecentUserActivity>> GetRecentUserActivity(int schoolId, int userid, bool enableChat, long groupId)
        {
            IEnumerable<RecentUserActivity> recentUserActivityList = null;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@UserId", userid, DbType.Int64);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@EnableChat", enableChat, DbType.Boolean);
                recentUserActivityList = await conn.QueryAsync<RecentUserActivity>("School.GetRecentUserActivity", parameters, commandType: CommandType.StoredProcedure);
            }
            return recentUserActivityList;
        }
        /// <summary>
        /// To get the search result in chat
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <param name="userTypeId"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Chat>> GetSearchInChat(int schoolId, int userId, int userTypeId, string text)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@UserTypeId", userTypeId, DbType.Int64);
                parameters.Add("@Text", text, DbType.String);
                return await conn.QueryAsync<Chat>("School.SearchInChat", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// To get the list of my contacts
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<LogInUser>> GetMyContactsList(int schoolId, int userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<LogInUser>("School.GetMyContacts", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// To Update Chat which is in not in use currently
        /// </summary>
        /// <param name="entityToUpdate"></param>
        public override void UpdateAsync(Chat entityToUpdate)
        {
            throw new NotImplementedException();
        }

        #region Invite Chat
        /// <summary>
        /// To insert the invite user for group chat
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int InviteUserForChat(InviteChat entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetInviteChatParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("School.InviteChatCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        /// <summary>
        /// To update the user permission or group related information in quick chat
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int UpdateInviteUserGroupChat(InviteChat entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetInviteChatParameters(entity, TransactionModes.Update);
                conn.Query<Int32>("School.InviteChatCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        /// <summary>
        /// To Delete the user from Group Chat permanently
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int DeleteInviteUserGroupChat(InviteChat entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetInviteChatParameters(entity, TransactionModes.Delete);
                conn.Query<Int32>("School.InviteChatCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        /// <summary>
        /// To Update the group Name of Invite chat group
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string UpdateInviteChatGroupName(InviteChat entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetUpdateGroupNameParameters(entity, TransactionModes.Update);
                conn.Query<string>("School.UpdateInviteChatGroupName", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<string>("UpdatedGroupName");
            }
        }


        /// <summary>
        /// To get the latest group chat id
        /// </summary>
        /// <returns></returns>
        public int GetLatestInviteGroup()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@LatestGroupId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.GetLatestGroupId ", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("LatestGroupId");
            }
        }

        /// <summary>
        /// To get the list of users in group chat
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<InviteChat>> GetGroupChatUsers(long groupId, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<InviteChat>("School.GetGroupChatUsers", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// get Group Details based on various paramters
        /// </summary>
        /// <param name="groupid"></param>
        /// <param name="schoolid"></param>
        /// <param name="userid"></param>
        /// <param name="groupname"></param>
        /// <returns></returns>
        public async Task<IEnumerable<InviteChat>> GetGroupDetails(long? groupid, long? schoolid, long? userid, string groupname)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupId", groupid, DbType.Int64);
                parameters.Add("@SchoolId", schoolid, DbType.Int64);
                parameters.Add("@UserId", userid, DbType.Int64);
                parameters.Add("@GroupName", groupname, DbType.String);
                return await conn.QueryAsync<InviteChat>("School.GetGroupDetails", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// To get the invite group name of group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public string GetInviteGroupName(long groupId)
        {
            string groupName = "";
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@InviteGroupId", groupId, DbType.Int64);
                parameters.Add("@LatestInviteGroupName", dbType: DbType.String, direction: ParameterDirection.Output,size:4000);
                conn.Query<string>("School.GetLatestInviteGroupName", parameters, commandType: CommandType.StoredProcedure);
                groupName = parameters.Get<string>("LatestInviteGroupName");
            }
            return groupName;
        }

        /// <summary>
        /// To get the list of all users in group chat
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<InviteChat>> GetAllGroupUsers(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryAsync<InviteChat>("School.GetAllGroupUsers", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public int DeleteChatConversation(long schoolId, long groupId, long fromUser, long toUser)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@FromUser", fromUser, DbType.Int64);
                parameters.Add("@ToUser", toUser, DbType.Int64);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<Int32>("School.DeleteChatConversation", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }


        #endregion
        public int InsertChatLog(UserChatLog entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetChatLogParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("School.ChatsLogCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<dynamic>("Output");
            }
        }

        public int DeleteChatlog(UserChatLog entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetChatLogParameters(entity, TransactionModes.Delete);
                conn.Query<Int32>("School.ChatsLogCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public int DeleteAllChatlog()
        {
            using (var conn = GetOpenConnection())
            {
                conn.Query<Int32>("School.DeleteChatLogs", null, commandType: CommandType.StoredProcedure);
                return 1;
            }
        }

        public async Task<IEnumerable<UserChatLog>> GetConnectionbyUserId(string connectionid, long? schoolId, long? userId,string sessionid)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ConnectionId", connectionid, DbType.String);
                parameters.Add("@SessionId", sessionid, DbType.String);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<UserChatLog>("School.GetChatLogs", parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
