﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Linq;

namespace Phoenix.API.Repositories
{
    public class SchoolRepository : SqlRepository<SchoolInformation>, ISchoolRepository
    {
        private readonly IConfiguration _config;

        public SchoolRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<SchoolInformation>> GetSchoolList()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<SchoolInformation>("Admin.GetSchoolList", commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SchoolInformation>> GetAdminSchoolList()
        {
            using (var conn = GetOpenConnection())
            {
                return await conn.QueryAsync<SchoolInformation>("[Admin].[GetAdminSchoolList]", commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<GetReportDetail> GetReportDetails(DateTime startDate, DateTime endDate)
        {
            GetReportDetail GetReportDetail = new GetReportDetail();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                return await conn.QueryFirstOrDefaultAsync<GetReportDetail>("[School].[GetReportDetails]", parameters, commandType: CommandType.StoredProcedure);


            }

        }

        public async Task<IEnumerable<SchoolInformation>> GetAllBusinesUnitSchools(int pageIndex, int pageSize, string searchString)
        {
            IEnumerable<SchoolInformation> schoolList = new List<SchoolInformation>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PageIndex", pageIndex, DbType.Int64);
                parameters.Add("@PageSize", pageSize, DbType.Int64);
                parameters.Add("@SearchString", searchString, DbType.String);

                var result = await conn.QueryMultipleAsync("[Admin].[GetBusinessUnitSchoolList]", parameters, null, null, CommandType.StoredProcedure);
                schoolList = await result.ReadAsync<SchoolInformation>();
                var totalGoalsCount = await result.ReadAsync<int>();
                if (schoolList.Any())
                {
                    schoolList.ToList().ForEach(x => { x.TotalCount = totalGoalsCount.FirstOrDefault(); });
                }
                return schoolList;
            }
        }

        #region Generated Methods

        public async override Task<SchoolInformation> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<SchoolInformation>("Admin.GetSchoolById", parameters, commandType: CommandType.StoredProcedure);


            }
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<SchoolInformation>> GetAllAsync()
        {
            throw new NotImplementedException();
        }


        public override void InsertAsync(SchoolInformation entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SchoolInformation entityToUpdate)
        {
            throw new NotImplementedException();
        }

        public async Task<FileDownload> GetModuleFileDetails(int id, string filetype)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@id", id, DbType.Int32);
                parameters.Add("@strType", filetype, DbType.String);
                return await conn.QueryFirstOrDefaultAsync<FileDownload>("[School].[GetModuleFileDetails]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<ReportDashboard> GetReportDashboard(long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId)
        {
            ReportDashboard reportDashboard = new ReportDashboard();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                var result = await conn.QueryMultipleAsync("[Admin].[GetReportDashboardData]", parameters, commandType: CommandType.StoredProcedure);
                reportDashboard.Logins = await result.ReadAsync<LoginDetailData>();
                reportDashboard.Assignments = await result.ReadAsync<PieChartData>();
                reportDashboard.GroupAssignments = await result.ReadAsync<TableData>();
                reportDashboard.GroupObservation = await result.ReadAsync<TableData>();
                reportDashboard.AbuseReport = await result.ReadAsync<Data>();
                reportDashboard.AbuseReportGraph = await result.ReadAsync<Data>();
                reportDashboard.GroupChatters = await result.ReadAsync<ChatterTableData>();
                reportDashboard.TeacherAssignments = await result.ReadAsync<TeacherAssignment>();
                reportDashboard.StudentAssignments = await result.ReadAsync<StudentAssignment>();
                reportDashboard.QuizReportData = await result.ReadAsync<QuizReportData>();

                return reportDashboard;
            }
        }

        public async Task<AssignmentReportCard> GetAssignmentReportData(long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId)
        {
            AssignmentReportCard assignmentReport = new AssignmentReportCard();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                var result = await conn.QueryMultipleAsync("[Admin].[GetAssignmentCardReportData]", parameters, commandType: CommandType.StoredProcedure);
                assignmentReport.Assignments = await result.ReadAsync<PieChartData>();
                assignmentReport.GroupAssignments = await result.ReadAsync<TableData>();
                assignmentReport.TeacherAssignments = await result.ReadAsync<TeacherAssignment>();
                assignmentReport.StudentAssignments = await result.ReadAsync<StudentAssignment>();

                return assignmentReport;
            }
        }

        public async Task<IEnumerable<ChildQuizReportData>> GetChildQuizReportData(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@UserId", userId, DbType.Int64);

                return await conn.QueryAsync<ChildQuizReportData>("[Admin].[GetChildQuizReportdata]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<TeacherList>> GetSchoolTeachersBySchoolId(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);

                return await conn.QueryAsync<TeacherList>("[School].[GetSchoolTeachersBySchoolId]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<QuizReportData>> GetQuizReportDataWithDateRange(long? userId, long? schoolId, DateTime startDate, DateTime endDate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();

                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);

                return await conn.QueryAsync<QuizReportData>("[Admin].[GetQuizReportDataWithDateRange]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<bool> UpdateSchoolClassroomStatus(SchoolInformation schoolInformation)
        {
            bool result = false;
            var parameters = new DynamicParameters();
            using (var conn = GetOpenConnection())
            {
                parameters.Add("@SchoolId", schoolInformation.SchoolId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.UpdateClassroomStatus", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
            }
            return result;
        }

        public async Task<IEnumerable<TableData>> GetSchoolGroupAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<TableData>("Admin.GetSchoolGroupAssignmentReport", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<ChatterTableData>> GetGroupChatterReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<ChatterTableData>("Admin.GetGroupChatterReportDashboard", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<TeacherAssignment>> GetTeacherAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<TeacherAssignment>("Admin.GetTeacherAssignmentReportDashboard", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<StudentAssignment>> GetStudentAssignmentReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<StudentAssignment>("Admin.GetStudentAssignmentReportDashboard", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Data>> GetSchoolAbuseReport(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<Data>("Admin.GetAbuseReportDashboardData", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<LoginDetailData> GetSchoolLoginReport(long userId, long schoolId, string startDate, string endDate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@StartDate", startDate, DbType.DateTime);
                parameters.Add("@EndDate", endDate, DbType.DateTime);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryFirstAsync<LoginDetailData>("Admin.GetSchoolLoginReport", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        #endregion

        #region Live Session Configuration
        public async Task<SessionConfiguration> GetSchoolSessionStatus(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                return await conn.QueryFirstAsync<SessionConfiguration>("School.GetSchoolSessionStatus", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<bool> SaveSchoolSessionStatus(SessionConfiguration model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", model.SchoolId, DbType.Int64);
                parameters.Add("@TeamsMeetingEnabled", model.TeamsSessionEnabled, DbType.Boolean);
                parameters.Add("@ZoomMeetingEnabled", model.ZoomSessionEnabled, DbType.Boolean);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync<int>("School.SaveSchoolSessionStatus", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        #endregion

        #region Course Department
        public async Task<bool> DepartmentCourseCU(Department department)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@departmentId", department.DepartmentId);
                parameters.Add("@departmentTitle", department.DepartmentName);
                parameters.Add("@schoolId", department.SchoolId);
                parameters.Add("@curriculumId", department.CurriculumId);
                parameters.Add("@createdby", department.CreatedBy);
                parameters.Add("@DATAMODE", department.DATAMODE);
                //parameters.Add("@courseIds", string.Join(",", department.CourseId));
                parameters.Add("@courseIds", string.Empty);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await conn.QueryAsync<int>("School.DepartmentCourseCU", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }
        public async Task<IEnumerable<Department>> GetDepartmentCourse(long schoolId, long curriculumId, long? departmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@schoolId", schoolId, DbType.Int64);
                parameters.Add("@curriculumId", curriculumId, DbType.Int64);
                parameters.Add("@departmentId", departmentId, DbType.Int64);
                return await conn.QueryAsync<Department>("[School].[GetDepartmentCourse]", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<bool> CanDeleteDepartment(long departmentId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@departmentId", departmentId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<bool>("[School].[CanDeleteDepartment]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Student>> GetStudentVaultReportData(long schoolId, long teacherId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@TeacherId", teacherId, DbType.Int64);
                return await conn.QueryAsync<Student>("School.GetStudentVaultReportData", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        #endregion
    }
}
