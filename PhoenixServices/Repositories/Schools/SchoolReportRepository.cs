﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class SchoolReportRepository : SqlRepository<SchoolReports>, ISchoolReportRepository
    {
        private readonly IConfiguration _config;
        public SchoolReportRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }
        #region GeneratedMethods
        public override void DeleteAsync(int id)
        {
            throw new System.NotImplementedException();
        }
        public void DeleteAsync(int id, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@reportDetailId", id);
                parameters.Add("@schoolid", schoolId);
                conn.Execute("Report.DeleteSchoolReport", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override Task<IEnumerable<SchoolReports>> GetAllAsync()
        {
            throw new System.NotImplementedException();
        }

        public override Task<SchoolReports> GetAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public override void InsertAsync(SchoolReports entity)
        {
            throw new System.NotImplementedException();
        }

        public override void UpdateAsync(SchoolReports entityToUpdate)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region Public Method
        public async Task<int> SchoolReportCU(SchoolReports schoolReports)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@reportDetailId", schoolReports.ReportDetailId);
                parameters.Add("@Name", schoolReports.Name);
                parameters.Add("@Description", schoolReports.Description);
                parameters.Add("@StoredProcedureName", schoolReports.StoredProcedureName);
                parameters.Add("@FileName", schoolReports.FileName);
                parameters.Add("@CreatedBy", schoolReports.CreatedBy);
                parameters.Add("@schoolid", schoolReports.SchoolId);
                parameters.Add("@isReportEdit", schoolReports.IsReportEdit);
                parameters.Add("@configType", schoolReports.ConfigType);                
                parameters.Add("@ReportParameterCUD", schoolReports.GetReportParameterDT(), DbType.Object);
                parameters.Add("@output", direction: ParameterDirection.Output, dbType: DbType.Int32);
                await conn.QueryAsync("Report.SaveReportDetails", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }
        public async Task<IEnumerable<ReportListModel>> GetReportLists(long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var paramteres = new DynamicParameters();
                paramteres.Add("@schoolid", schoolId);
                return await conn.QueryAsync<ReportListModel>("Report.GetReportList", paramteres, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<ReportDetailModel> GetReportDetail(long reportDetailId, bool isPreview, bool isCommonReport, long schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var reportDetails = new ReportDetailModel();
                var paramteres = new DynamicParameters();
                paramteres.Add("@reportdetailid", reportDetailId);
                paramteres.Add("@isPreviewDataRequired", isPreview);
                paramteres.Add("@isCommonReport", isCommonReport);
                paramteres.Add("@schoolId", schoolId);
                var result = await conn.QueryMultipleAsync("Report.GetReportDetail", paramteres, commandType: CommandType.StoredProcedure);
                reportDetails.SchoolReports = await result.ReadFirstOrDefaultAsync<SchoolReports>();
                reportDetails.ReportParameters = await result.ReadAsync<ReportParameter>();
                if (!result.IsConsumed)
                {
                    var columnNames = await result.ReadAsync<string>();
                    var count = 0;
                    while (!result.IsConsumed)
                    {
                        var data = await result.ReadAsync();
                        reportDetails.ColumnFieldsDic.Add(columnNames.AsList()[count++], data);
                    }
                }
                return reportDetails;
            }
        }
        public async Task<IEnumerable<ReportFilterModel>> GetReportFilter(string filterCode, string filterParameter)
        {
            using (var conn = GetOpenConnection())
            {
                var paramteres = new DynamicParameters();
                paramteres.Add("@FilterCode", filterCode);
                paramteres.Add("@FilterParamter", filterParameter);
                return await conn.QueryAsync<ReportFilterModel>("Report.Get_ReportFilterBind", paramteres, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<ReportDataModel> GetReportData(ReportParameterRequestModel requestModel)
        {
            using (var conn = GetOpenConnection())
            {
                var reportData = new ReportDataModel();
                var paramteres = new DynamicParameters();
                paramteres.Add("@ReportDetailId", requestModel.ReportDetailId);
                paramteres.Add("@FilterParamter", requestModel.ReportParamterListString);
                paramteres.Add("@SchoolId", requestModel.SchoolId);
                var result = await conn.QueryMultipleAsync("Report.GetReportData", paramteres, commandType: CommandType.StoredProcedure);                
                reportData.FileName = await result.ReadFirstOrDefaultAsync<string>();
                if (!result.IsConsumed)
                {
                    var columnNames = await result.ReadAsync<string>();
                    var count = 0;
                    while (!result.IsConsumed)
                    {
                        var data = await result.ReadAsync();
                        reportData.ColumnFieldsDic.Add(columnNames.AsList()[count++], data);
                    }
                }
                return reportData;
            }
        }

       
        #endregion
    }
}
