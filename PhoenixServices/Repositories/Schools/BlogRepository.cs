﻿using Dapper;
using Phoenix.Models;
using DbConnection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Phoenix.Common.Enums;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public class BlogRepository : SqlRepository<Blog>, IBlogRepository
    {
        private readonly IConfiguration _config;
        public BlogRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var entity = new Blog(id);
                entity.FromDate = entity.ToDate = entity.BlogSharedOn = entity.CreatedOn = entity.SortDate = DateTime.Now;
                entity.CloseDiscussionDate = null;
                var parameters = GetBlogParameters(entity, TransactionModes.Delete);
                conn.Query<Int32>("School.BlogsCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }


        public void DeleteBlog(int id, long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var entity = new Blog(id);
                entity.FromDate = entity.ToDate = entity.BlogSharedOn = entity.CreatedOn = entity.SortDate = DateTime.Now;
                entity.UpdatedBy = userId;
                entity.CloseDiscussionDate = null;
                var parameters = GetBlogParameters(entity, TransactionModes.Delete);
                conn.Query<Int32>("School.BlogsCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public async Task<IEnumerable<PostAndCommentLikeUserList>> GetPostAndCommentLikeUserList(long BlogId, long CommentSectioId, long UserId, bool IsCommentDetailDisplay) {
            using (var conn = GetOpenConnection())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", BlogId, DbType.Int64);
                parameters.Add("@CommentSectioId", CommentSectioId, DbType.Int64);
                parameters.Add("@UserId", UserId, DbType.Int64);
                parameters.Add("@IsCommentDetailDisplay", IsCommentDetailDisplay, DbType.Boolean);
                return await conn.QueryAsync<PostAndCommentLikeUserList>("School.GetPostAndCommentLikeUserList", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public bool UpdateDiscloseDate(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", id, DbType.Int32);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<Int32>("School.CloseDiscussionUpdate", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }
        public override async Task<IEnumerable<Blog>> GetAllAsync()
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@CategoryId", DBNull.Value, DbType.Int32);
                parameters.Add("@BlogTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsPublish", DBNull.Value, DbType.Boolean);
                parameters.Add("@UserId", DBNull.Value, DbType.Boolean);
                return await conn.QueryAsync<Blog>("School.GetBlogs", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public override async Task<Blog> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", id, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@CategoryId", DBNull.Value, DbType.Int32);
                parameters.Add("@BlogTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsPublish", DBNull.Value, DbType.Boolean);
                parameters.Add("@UserId", DBNull.Value, DbType.Boolean);
                return await conn.QueryFirstOrDefaultAsync<Blog>("School.GetBlogByBlogId", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<BlogCategory>> GetBlogCategories(long? schooldId, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schooldId, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<BlogCategory>("School.GetBlogCategory", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Blog>> GetBlogsByCategoryId(int schoolId, int categoryId, bool? isPublish)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@CategoryId", categoryId, DbType.Int32);
                parameters.Add("@BlogTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsPublish", isPublish, DbType.Boolean);
                parameters.Add("@UserId", DBNull.Value, DbType.Boolean);
                return await conn.QueryAsync<Blog>("School.GetBlogs", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void PublishBlog(int blogId, bool isPublish, int publishBy)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", blogId, DbType.Int32);
                parameters.Add("@IsPublish", isPublish, DbType.Boolean);
                parameters.Add("@PublishBy", publishBy, DbType.Int32);
                conn.Query("School.PublishBlogById", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Blog>> GetBlogsBySchoolId(int schoolId, bool? isPublish)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@CategoryId", DBNull.Value, DbType.Int32);
                parameters.Add("@BlogTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsPublish", isPublish, DbType.Boolean);
                parameters.Add("@UserId", DBNull.Value, DbType.Boolean);
                return await conn.QueryAsync<Blog>("School.GetBlogs", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Blog>> GetBlogsByBlogTypeId(int blogTypeId, bool? isPublish)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@CategoryId", DBNull.Value, DbType.Int32);
                parameters.Add("@BlogTypeId", blogTypeId, DbType.Int32);
                parameters.Add("@GroupId", DBNull.Value, DbType.Int32);
                parameters.Add("@IsPublish", isPublish, DbType.Boolean);
                parameters.Add("@UserId", DBNull.Value, DbType.Boolean);
                return await conn.QueryAsync<Blog>("School.GetBlogs", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Blog>> GetBlogsByGroupId(int groupId, int schoolId, int? userId, bool? isPublish)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", DBNull.Value, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@CategoryId", DBNull.Value, DbType.Int32);
                parameters.Add("@BlogTypeId", DBNull.Value, DbType.Int32);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@IsPublish", isPublish, DbType.Boolean);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryAsync<Blog>("School.GetBlogs", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Blog>> GetBlogsByGroupNBlogtypeId(int groupId, int blogTypeId, bool? isPublish)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", DBNull.Value, DbType.Int32);
                parameters.Add("@SchoolId", DBNull.Value, DbType.Int32);
                parameters.Add("@CategoryId", DBNull.Value, DbType.Int32);
                parameters.Add("@BlogTypeId", blogTypeId, DbType.Int32);
                parameters.Add("@GroupId", groupId, DbType.Int32);
                parameters.Add("@IsPublish", isPublish, DbType.Boolean);
                parameters.Add("@UserId", DBNull.Value, DbType.Boolean);
                return await conn.QueryAsync<Blog>("School.GetBlogs", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Currently using this for blog insertion
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Insert(Blog entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("School.BlogsCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }

        public int InsertMap(SchoolGroup_StudentMapping mapModal)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetStudentGroupMaoParameters(mapModal, TransactionModes.Insert);
                conn.Query<Int32>("School.SP_SchoolGroup_StudentMapping", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }
        public int InsertSharePost(sharepost shareModal)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSharePostParameter(shareModal, TransactionModes.Insert);
                conn.Query<Int32>("School.SP_saveSharedPost", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }




        public int DeleteExemplar(Blog entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", entity.BlogId, DbType.Int64);
                parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<Int32>("School.DeleteExemplar", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output");
            }
        }
        public override void InsertAsync(Blog entity)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogParameters(entity, TransactionModes.Insert);
                conn.Query<Int32>("School.BlogsCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        public override void UpdateAsync(Blog entityToUpdate)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogParameters(entityToUpdate, TransactionModes.Update);
                conn.Query<Int32>("School.BlogsCUD", parameters, commandType: CommandType.StoredProcedure);
                parameters.Get<Int32>("Output");
            }
        }

        #region Private Methods
        private DynamicParameters GetBlogParameters(Blog entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@BlogId", entity.BlogId, DbType.Int64);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.Int64);
            parameters.Add("@BlogTypeId", entity.BlogTypeId, DbType.Int32);
            parameters.Add("@GroupId", entity.GroupId, DbType.Int64);
            parameters.Add("@EnableCommentById", entity.EnableCommentById, DbType.Int64);
            parameters.Add("@Title", entity.Title, DbType.String);
            parameters.Add("@Summary", entity.Summary, DbType.String);
            parameters.Add("@Description", entity.Description, DbType.String);
            parameters.Add("@EmbedUrl", entity.EmbedUrl, DbType.String);
            parameters.Add("@CategoryId", entity.CategoryId, DbType.Int32);
            parameters.Add("@BlogImage", entity.BlogImage, DbType.String);
            parameters.Add("@IsPublish", entity.IsPublish, DbType.Boolean);
            parameters.Add("@CreatedBy", entity.CreatedBy, DbType.Int64);
            parameters.Add("@UpdatedBy", entity.UpdatedBy, DbType.Int64);
            // Added on 10-11-2019 - ModerationIsRequired Field
            parameters.Add("@ModerationRequired", entity.ModerationRequired, DbType.Boolean);
            parameters.Add("@CloseDiscussionDate", entity.CloseDiscussionDate, DbType.DateTime);
            ////Added on 02-Jul-2020 for Lastest changes of Blog UI
            parameters.Add("@SelectedStudentIDs", entity.SelectedStudentIDs, DbType.String);
            parameters.Add("@CommentApprovedById", entity.CommentApprovedById, DbType.Int32);
            parameters.Add("@FromDate", entity.FromDate, DbType.DateTime);
            parameters.Add("@ToDate", entity.ToDate, DbType.DateTime);
            parameters.Add("@ScheduledTime", entity.ScheduledTime, DbType.DateTime);
            parameters.Add("@IsVisibleToParent", entity.IsVisibleToParent, DbType.Boolean);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        private DynamicParameters GetStudentGroupMaoParameters(SchoolGroup_StudentMapping mapModal, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            //parameters.Add("@mapping_id", (int)mode, DbType.Int32);
            parameters.Add("@student_id", mapModal.student_id, DbType.Int64);
            parameters.Add("@group_id", mapModal.group_id, DbType.Int64);
            parameters.Add("@post_id", mapModal.post_id, DbType.Int64);
            parameters.Add("@create_date", mapModal.created_date, DbType.DateTime);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        private DynamicParameters GetSharePostParameter(sharepost shareModal, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            //parameters.Add("@mapping_id", (int)mode, DbType.Int32);
            parameters.Add("@SchoolId", shareModal.SchoolId, DbType.Int64);
            parameters.Add("@GroupId", shareModal.GroupId, DbType.Int64);
            parameters.Add("@PostId", shareModal.PostId, DbType.Int64);
            parameters.Add("@ShareWithDepartmentWall", shareModal.ShareWithDepartmentWall, DbType.Boolean);
            parameters.Add("@ShareWithCourseWall", shareModal.ShareWithCourseWall, DbType.Boolean);
            parameters.Add("@SharingDate", shareModal.SharingDate, DbType.DateTime);
            parameters.Add("@SharedByUserId", shareModal.SharedByUserId, DbType.Int64);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        private DynamicParameters GetBlogLikeParameters(BlogLike entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@BlogLikeId", entity.BlogLikeId, DbType.Int64);
            parameters.Add("@BlogId", entity.BlogId, DbType.Int64);
            parameters.Add("@isLike", entity.IsLike, DbType.Boolean);
            parameters.Add("@LikeTypeId", entity.LikeTypeId, DbType.Int64);
            parameters.Add("@LikedBy", entity.LikedBy, DbType.Int64);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        #endregion

        public async Task<IEnumerable<Blog>> GetTeacherBlogs(int schoolId, int pageNumber, int pageSize)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@PageNum", pageNumber, DbType.Int32);
                parameters.Add("@pageSize", pageSize, DbType.Int32);
                return await conn.QueryAsync<Blog>("School.GetTeacherBlogs", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public bool InsertLike(BlogLike blogLike)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogLikeParameters(blogLike, TransactionModes.Insert);
                conn.Query<Int32>("School.BlogLikeCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public bool UpdatedLike(BlogLike blogLike)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogLikeParameters(blogLike, TransactionModes.Update);
                conn.Query<Int32>("School.BlogLikeCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public bool DeleteLike(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetBlogLikeParameters(new BlogLike(id), TransactionModes.Delete);
                conn.Query<Int32>("School.BlogLikeCUD", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public async Task<IEnumerable<BlogLike>> GetBlogLikes(int? blogId, int? blogLikeId, int? likedBy, bool? isLike)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", blogId, DbType.Int64);
                parameters.Add("@BlogLikeId", blogLikeId, DbType.Int64);
                parameters.Add("@LikedBy", likedBy, DbType.Int64);
                parameters.Add("@IsLike", isLike, DbType.Boolean);
                return await conn.QueryAsync<BlogLike>("School.GetBlogLikes", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<BlogTypes>> GetBlogTypes(long? schooldId, short languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schooldId, DbType.Int64);
                parameters.Add("@languageId", languageId, DbType.Int16);
                return await conn.QueryAsync<BlogTypes>("School.GetBlogTypes", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public bool UpdateWallSortOrder(DataTable dataTable, long UpdatedBy)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SortOrderTable", dataTable, DbType.Object);
                parameters.Add("UpdatedBy", UpdatedBy, DbType.Int64);
                parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<Int32>("School.UpdateWallSortOrder", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<Int32>("Output") > 0;
            }
        }

        public async Task<IEnumerable<Chatter>> GetChattersWithComment(int? blogId, int schoolId, int? categoryId, int groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish)
        {
            var blogList = new List<Blog>();
            var blogCommentList = new List<BlogComment>();
            var chatters = new List<Chatter>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", blogId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@CategoryId", categoryId, DbType.Int64);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@IsPublish", isPublish, DbType.Boolean);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                parameters.Add("@CommentId", commentId, DbType.Int64);
                parameters.Add("@PublishBy", PublishBy, DbType.Int64);
                parameters.Add("@IsCommentPublish", isCommentPublish, DbType.Boolean);
                var result = await conn.QueryMultipleAsync("[School].[GetChatters]", parameters, commandType: CommandType.StoredProcedure);
                blogList.AddRange(result.Read<Blog>());
                blogCommentList.AddRange(result.Read<BlogComment>());

                if (blogList != null && blogList.Count() > 0)
                {
                    for (int i = 0; i < blogList.Count(); i++)
                    {
                        var chatter = new Chatter();
                        chatter.Blog = blogList.ElementAt(i);
                        var bId = chatter.Blog.BlogId;

                        var commentList = blogCommentList.Where(r => r.BlogId == bId).ToList();
                        chatter.BlogComments.AddRange(commentList);

                        chatters.Add(chatter);
                    }
                }

                return chatters;
            }
        }

        public async Task<IEnumerable<Blog>> GetUserExemplerWallData(long userId, int pageSize, int pageIndex)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@PageIndex", pageIndex, DbType.Int32);
                return await conn.QueryAsync<Blog>("School.GetAllExemplarWallPosts", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SearchMyWall>> GetSearchOnMyWall(long schooId, string searchString,long userId , bool isTeacher)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schooId, DbType.Int64);
                parameters.Add("@SearchString", searchString, DbType.String);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                return await conn.QueryAsync<SearchMyWall>("School.SearchOnMyWall", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SearchMyWall>> GetSearchBlogsOnMyGroup(long schooId, string searchString, long userId, bool isTeacher,long groupId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schooId, DbType.Int64);
                parameters.Add("@SearchString", searchString, DbType.String);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                return await conn.QueryAsync<SearchMyWall>("School.SearchBlogsOnMyGroups", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// In Use to get the mywall data list
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="schoolId"></param>
        /// <param name="categoryId"></param>
        /// <param name="blogTypeId"></param>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <param name="isTeacher"></param>
        /// <param name="isPublish"></param>
        /// <param name="commentId"></param>
        /// <param name="PublishBy"></param>
        /// <param name="isCommentPublish"></param>
        /// <param name="LogedInUserId"></param>
        /// <param name="pageNum"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Chatter>> GetMyWallData(int? blogId, int schoolId, int? categoryId,int? blogTypeId , int? groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish,int ?LogedInUserId,int? pageNum)
        {
            var blogList = new List<Blog>();
            var blogCommentList = new List<BlogComment>();
            var myWallBlogs = new List<Chatter>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", blogId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@CategoryId", categoryId, DbType.Int64);
                parameters.Add("@BlogTypeId", blogTypeId, DbType.Int32);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@IsPublish", isPublish, DbType.Boolean);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@LogedInUserId", LogedInUserId, DbType.Int64);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                parameters.Add("@CommentId", commentId, DbType.Int64);
                parameters.Add("@PublishBy", PublishBy, DbType.Int64);
                parameters.Add("@IsCommentPublish", isCommentPublish, DbType.Boolean);
                parameters.Add("@PageNum", pageNum, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[School].[GetMyWallBlogs]", parameters, commandType: CommandType.StoredProcedure);
                blogList.AddRange(result.Read<Blog>());
                blogCommentList.AddRange(result.Read<BlogComment>());

                if (blogList != null && blogList.Count() > 0)
                {
                    for (int i = 0; i < blogList.Count(); i++)
                    {
                        var wallBlogs = new Chatter();
                        wallBlogs.Blog = blogList.ElementAt(i);
                        var bId = wallBlogs.Blog.BlogId;

                        var commentList = blogCommentList.Where(r => r.BlogId == bId).ToList();
                        wallBlogs.BlogComments.AddRange(commentList);

                        myWallBlogs.Add(wallBlogs);
                    }
                }
                return myWallBlogs;
            }
        }

        public async Task<IEnumerable<Chatter>> GetMyWallDataOnSearch(long? blogId, long schoolId, int? categoryId, int? blogTypeId, long? groupId, long? userId, bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish, int? LogedInUserId, int? pageNum)
        {
            var blogList = new List<Blog>();
            var blogCommentList = new List<BlogComment>();
            var myWallBlogs = new List<Chatter>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", blogId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@CategoryId", categoryId, DbType.Int32);
                parameters.Add("@BlogTypeId", blogTypeId, DbType.Int32);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@IsPublish", isPublish, DbType.Boolean);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@LogedInUserId", LogedInUserId, DbType.Int64);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                parameters.Add("@CommentId", commentId, DbType.Int64);
                parameters.Add("@PublishBy", PublishBy, DbType.Int64);
                parameters.Add("@IsCommentPublish", isCommentPublish, DbType.Boolean);
                parameters.Add("@PageNum", pageNum, DbType.Int32);
                var result = await conn.QueryMultipleAsync("[School].[GetBlogsOnSearch]", parameters, commandType: CommandType.StoredProcedure);
                blogList.AddRange(result.Read<Blog>());
                blogCommentList.AddRange(result.Read<BlogComment>());

                if (blogList != null && blogList.Count() > 0)
                {
                    for (int i = 0; i < blogList.Count(); i++)
                    {
                        var wallBlogs = new Chatter();
                        wallBlogs.Blog = blogList.ElementAt(i);
                        var bId = wallBlogs.Blog.BlogId;

                        var commentList = blogCommentList.Where(r => r.BlogId == bId).ToList();
                        wallBlogs.BlogComments.AddRange(commentList);

                        myWallBlogs.Add(wallBlogs);
                    }
                }
                return myWallBlogs;
            }
        }

        public async Task<IEnumerable<BlogStudentDash>> GetStudentDashBlogs(long userId, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<BlogStudentDash>("School.GetStudentChatterBlogs", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<BlogTeacherDash>> GetTeacherDashBlogs(long userId, int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int32);
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<BlogTeacherDash>("School.GetTeacherChatterBlogs", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// In Use to get the mywall data list
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="schoolId"></param>
        /// <param name="categoryId"></param>
        /// <param name="blogTypeId"></param>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <param name="isTeacher"></param>
        /// <param name="isPublish"></param>
        /// <param name="commentId"></param>
        /// <param name="PublishBy"></param>
        /// <param name="isCommentPublish"></param>
        /// <param name="LogedInUserId"></param>
        /// <param name="pageNum"></param>
        /// <param name="EnableChat"></param>
        /// <returns></returns>
        public async Task<IEnumerable<BlogCompleteData>> GetMyChatterData(long? blogId, long schoolId, int? categoryId, int? blogTypeId, long? groupId, long? userId, 
            bool isTeacher, bool? isPublish, int? commentId, int? PublishBy, bool? isCommentPublish, int? LogedInUserId, int? pageNum, bool EnableChat)
        {
            var blogList = new List<Blog>();
            var blogCommentList = new List<BlogComment>();
            var blogGroupList = new List<SchoolGroup>();
            var blogRecentActivityList = new List<RecentUserActivity>();
            var myWallBlogs = new List<BlogCompleteData>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BlogId", blogId, DbType.Int64);
                parameters.Add("@SchoolId", schoolId, DbType.Int64);
                parameters.Add("@CategoryId", categoryId, DbType.Int64);
                parameters.Add("@BlogTypeId", blogTypeId, DbType.Int32);
                parameters.Add("@GroupId", groupId, DbType.Int64);
                parameters.Add("@IsPublish", isPublish, DbType.Boolean);
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@LogedInUserId", LogedInUserId, DbType.Int64);
                parameters.Add("@IsTeacher", isTeacher, DbType.Boolean);
                parameters.Add("@CommentId", commentId, DbType.Int64);
                parameters.Add("@PublishBy", PublishBy, DbType.Int64);
                parameters.Add("@IsCommentPublish", isCommentPublish, DbType.Boolean);
                parameters.Add("@PageNum", pageNum, DbType.Int32);
                parameters.Add("@EnableChat", EnableChat, DbType.Boolean);

                var result = await conn.QueryMultipleAsync("[School].[GetMyWallBlogCompletePageData]", parameters, commandType: CommandType.StoredProcedure);
                blogList.AddRange(result.Read<Blog>());
                blogCommentList.AddRange(result.Read<BlogComment>());
                blogGroupList.AddRange(result.Read<SchoolGroup>());
                blogRecentActivityList.AddRange(result.Read<RecentUserActivity>());
                if (blogList != null && blogList.Count() > 0)
                {
                    for (int i = 0; i < blogList.Count(); i++)
                    {
                        var wallBlogs = new BlogCompleteData();
                        wallBlogs._Blog = blogList.ElementAt(i);
                        if (myWallBlogs.Count == 0)
                        {
                            wallBlogs._RecentUserActivity = blogRecentActivityList;
                            wallBlogs._SchoolGroup = blogGroupList;
                        }
                        var bId = wallBlogs._Blog.BlogId;

                        var commentList = blogCommentList.Where(r => r.BlogId == bId).ToList();
                        wallBlogs._BlogComment = new List<BlogComment>();
                        wallBlogs._BlogComment.AddRange(commentList);

                        myWallBlogs.Add(wallBlogs);
                    }
                }

                return myWallBlogs;
            }
        }


    }
}