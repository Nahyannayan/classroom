﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class BlogShareRepository : SqlRepository<ShareBlogs>, IBlogShareRepository
    {
        private readonly IConfiguration _config;
        public BlogShareRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<ShareBlogs>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<ShareBlogs> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public int Insert(ShareBlogs entity)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(ShareBlogs entity)
        {
            ShareBlogs(entity);
        }

        public int ShareBlogs(ShareBlogs entity)
        {
            List<string> groupids = new List<string>();
            int output = 0;
            try
            {
                if (entity.GroupIds.Contains(","))
                {
                    groupids = entity.GroupIds.Split(',').ToList();
                }
                else
                    groupids.Add(entity.GroupIds);



                foreach (var groupid in groupids)
                {
                    entity.GroupId = Convert.ToInt64(groupid);
                    using (var conn = GetOpenConnection())
                    {
                        var parameters = GetShareBlogsParameters(entity, TransactionModes.Insert);
                        conn.Query<Int32>("School.BlogsShareCUD", parameters, commandType: CommandType.StoredProcedure);
                        output = parameters.Get<Int32>("Output");
                    }
                }
                return output;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public override void UpdateAsync(ShareBlogs entityToUpdate)
        {
            throw new NotImplementedException();
        }


        private DynamicParameters GetShareBlogsParameters(ShareBlogs entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@BlogId", entity.BlogId, DbType.Int64);
            parameters.Add("@GroupId", entity.GroupId, DbType.Int64);
            parameters.Add("@IsShared", entity.IsShared, DbType.Boolean);
            parameters.Add("@BlogSharedBy", entity.BlogSharedBy, DbType.Int64);
            parameters.Add("@Output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
    }
}
