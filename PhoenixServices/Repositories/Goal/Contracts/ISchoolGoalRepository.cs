﻿using DbConnection;
using Phoenix.Common.Enums;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface ISchoolGoalRepository : IGenericRepository<SchoolGoal>
    {
        Task<IEnumerable<SchoolGoal>> GetSchoolGoals(int languageId);
        int UpdateSchoolGoal(SchoolGoal entity, TransactionModes mode);
    }
}
