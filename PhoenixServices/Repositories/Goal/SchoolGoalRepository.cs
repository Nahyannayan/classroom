﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Common.Enums;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class SchoolGoalRepository : SqlRepository<SchoolGoal>, ISchoolGoalRepository
    {
        private readonly IConfiguration _config;
        public SchoolGoalRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<SchoolGoal>> GetSchoolGoals(int languageId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@LanguageId", languageId, DbType.Int32);
               
                return await conn.QueryAsync<SchoolGoal>("[School].[GetGoals]", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public int UpdateSchoolGoal(SchoolGoal entity, TransactionModes mode)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = GetSchoolGoalParameters(entity, mode);
                conn.Query<int>("[School].[GoalCUD]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output");
            }
        }

        #region Private Methods  
        private DynamicParameters GetSchoolGoalParameters(SchoolGoal entity, TransactionModes mode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TransMode", (int)mode, DbType.Int32);
            parameters.Add("@GoalId", entity.GoalId, DbType.Int32);
            parameters.Add("@GoalName", entity.GoalName, DbType.String);
            parameters.Add("@GoalDescription", entity.GoalDescription, DbType.String);
            parameters.Add("@ExpectedDate", entity.ExpectedDate, DbType.DateTime);
            parameters.Add("@IsActive", entity.IsActive, DbType.Boolean);
            parameters.Add("@IsApproved", entity.IsApproved, DbType.Boolean);
            parameters.Add("@UserId", entity.CreatedBy, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion

        #region Generated Methods

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<SchoolGoal>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async override Task<SchoolGoal> GetAsync(int id)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GoalId", id, DbType.Int32);
                return await conn.QueryFirstOrDefaultAsync<SchoolGoal>("[School].[GetGoalById]", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public override void InsertAsync(SchoolGoal entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SchoolGoal entityToUpdate)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
