﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Repositories
{
    public class StudentGoalsRepository : SqlRepository<StudentGoal>, IStudentGoalsRepository
    {
        private readonly IConfiguration _config;
        public StudentGoalsRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        #region Abstract methods
        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<StudentGoal>> GetAllAsync()
        {
            throw new NotImplementedException();
        }


        public override Task<StudentGoal> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(StudentGoal entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(StudentGoal entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region public methods
        public async Task<List<StudentGoal>> GetAllStudentGoals(long id, int pageIndex, int pageSize, long currentUserId, string searchString = "")
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", id, DbType.Int32);
                parameters.Add("@LoginUserId", currentUserId, DbType.Int64);
                parameters.Add("@PageNum", pageIndex, DbType.Int32);
                parameters.Add("@PageSize", pageSize, DbType.Int32);
                parameters.Add("@SearchString", searchString, DbType.String);
                var result = await conn.QueryMultipleAsync("Student.GetStudentGoals", parameters, null, null, CommandType.StoredProcedure);
                var studentGoals = await result.ReadAsync<StudentGoal>();
                var totalGoalsCount = await result.ReadAsync<int>();
                if (studentGoals.Any())
                {
                    studentGoals.ToList().ForEach(x => { x.TotalCount = totalGoalsCount.FirstOrDefault(); });
                }
                return studentGoals.ToList();
            }
        }

        public async Task<bool> UpdateDashboardGoalsStatus(StudentGoal goalModel)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", goalModel.UserId, DbType.Int64);
                parameters.Add("@UpdatedBy", goalModel.CreatedUserId, DbType.Int64);
                parameters.Add("@GoalId", goalModel.GoalId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.UpdateStudentGoalsDisplayStatus", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public async Task<bool> UpdateAllDashboardGoalsStatus(List<StudentGoal> lstGoalModel)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                foreach (var goalModel in lstGoalModel)
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@UserId", goalModel.UserId, DbType.Int64);
                    parameters.Add("@UpdatedBy", goalModel.CreatedUserId, DbType.Int64);
                    parameters.Add("@GoalId", goalModel.GoalId, DbType.Int32);
                    parameters.Add("@ShowOnDashboard", goalModel.ShowOnDashboard, DbType.Boolean);
                    parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    conn.Query<int>("Student.UpdateStudentGoalsDisplayStatus", parameters, commandType: CommandType.StoredProcedure);
                    if (parameters.Get<int>("output") > 0)
                        result = true;
                    else
                        result = false;
                }
            }
            return result;
        }

        public async Task<bool> InsertStudentGoal(StudentGoal studentGoalModel)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", studentGoalModel.UserId, DbType.Int64);
                parameters.Add("@CreatedBy", studentGoalModel.CreatedUserId, DbType.Int64);
                parameters.Add("@GoalId", studentGoalModel.GoalId, DbType.Int64);
                parameters.Add("@TeacherId", studentGoalModel.TeacherId, DbType.Int64);
                parameters.Add("@GoalName", studentGoalModel.GoalName, DbType.String);
                parameters.Add("@GoalDescription", studentGoalModel.GoalDescription, DbType.String);
                parameters.Add("@ExpectedDate", studentGoalModel.ExpectedDate, DbType.Date);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.SaveStudentGoal", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public async Task<bool> ApproveStudentGoal(StudentGoal studentGoal)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", studentGoal.UserId, DbType.Int64);
                parameters.Add("@GoalId", studentGoal.GoalId, DbType.Int32);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.ApproveStudentGoal", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
            }
            return result;
        }

        public async Task<bool> DeleteStudentGoal(StudentGoal model)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UpdatedBy", model.UserId, DbType.Int64);
                parameters.Add("@GoalId", model.GoalId, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.DeleteStudentGoal", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("output") > 0;
            }
        }

        public async Task<StudentGoal> GetStudentGoalById(long goalId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GoalId", goalId, DbType.Int64);
                return await conn.QuerySingleAsync<StudentGoal>("Student.GetStudentGoalById", parameters, commandType: CommandType.StoredProcedure);
            }
        }
        #endregion
    }
}
