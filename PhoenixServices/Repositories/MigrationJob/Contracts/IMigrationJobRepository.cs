﻿using Phoenix.API.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface IMigrationJobRepository
    {
        Task<IEnumerable<MigrationDetail>> GetMigrationDetailList(long schoolId);


        //Task<IEnumerable<SyncDetailById>> GetSyncDetailbyId(SyncDetailById obj);
        Task<bool> GetSyncDetailbyId(SyncDetailById obj);
        Task<IEnumerable<MigrationDetailCount>> GetMigrationDetailCount();

    }
}
