﻿using Dapper;
using DbConnection;
using Microsoft.Extensions.Configuration;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public class MigrationJobRepository : SqlRepository<MigrationDetail>, IMigrationJobRepository
    {
        private readonly IConfiguration _config;
        public MigrationJobRepository(IConfiguration config) : base(config)
        {
            _config = config;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<MigrationDetail>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<MigrationDetail> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(MigrationDetail entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(MigrationDetail entityToUpdate)
        {
            throw new NotImplementedException();
        }
        public async Task<IEnumerable<MigrationDetail>> GetMigrationDetailList(long SchoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", SchoolId, DbType.Int32);
                return await conn.QueryAsync<MigrationDetail>("[Admin].[GetOnDemandSyncList]", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<MigrationDetailCount>> GetMigrationDetailCount()
        {
            using (var conn = GetOpenConnection())
            {
                //var parameters = new DynamicParameters();
                //parameters.Add("@SyncDetailId", SyncDetailId, DbType.Int32);
                return await conn.QueryAsync<MigrationDetailCount>("[Admin].[GetOnDemandSyncCountList]", null, null, null, CommandType.StoredProcedure);
            }
        }

        //public async Task<IEnumerable<SyncDetailById>> GetSyncDetailbyId(SyncDetailById obj)
        //{
        //    using (var conn = GetOpenConnection())
        //    {
        //        var parameters = new DynamicParameters();
        //        parameters.Add("@SyncDetailId", obj.SyncDetailId);
        //        parameters.Add("@SchoolId", obj.SchoolId);
        //        parameters.Add("@userId", obj.userid);
        //        return await conn.QueryAsync<SyncDetailById>("[Admin].[GetSyncData]", parameters, null, null, CommandType.StoredProcedure);
        //    }
        //}
        public async Task<bool> GetSyncDetailbyId(SyncDetailById obj)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SyncDetailId", obj.SyncDetailId);
                parameters.Add("@SchoolId", obj.SchoolId);
                parameters.Add("@userId", obj.userid);
                parameters.Add("@output", dbType: DbType.Boolean, direction: ParameterDirection.Output);
                await conn.QueryAsync("[Admin].[GetSyncData]", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<bool>("output");
            }
        }
    }
}
