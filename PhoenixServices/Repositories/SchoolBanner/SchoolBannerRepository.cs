﻿using DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;

namespace Phoenix.API.Repositories
{
    public class SchoolBannerRepository : SqlRepository<SchoolBanner>, ISchoolBannerRepository
    {
        private readonly IConfiguration _config;
        public SchoolBannerRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<SchoolBanner>> GetSchoolBannersByPage(int pageNumber, int pageSize, string searchString, int userId, int schoolId)
        {
            List<SchoolBanner> lstSchoolBanners = new List<SchoolBanner>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@UserId", schoolId, DbType.Int32);
                return await conn.QueryAsync<SchoolBanner>("School.GetSchoolBannersByPage", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SchoolBanner>> GetSchoolBanners(long userId, int schoolId)
        {
            List<SchoolBanner> lstSchoolBanners = new List<SchoolBanner>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int32);
                return await conn.QueryAsync<SchoolBanner>("School.GetSchoolBanners", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public OperationDetails InsertBannerData(SchoolBanner entity)
        {
            OperationDetails op = new OperationDetails();
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAssignmentParameters(entity, TransactionModes.Insert);
                conn.Query<int>("School.BannerInsert", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = true;
                }
                else
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = false;
                }
                return op;
            }
        }

        public OperationDetails UpdateBannerData(SchoolBanner entity,TransactionModes mode)
        {
            OperationDetails op = new OperationDetails();
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAssignmentParameters(entity,mode);
                conn.Query<int>("School.UpdateSchoolBanner", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = true;

                }
                else
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = false;
                }
                return op;
            }
        }

      

        public int GetTopOrderForDisplayBanner(long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@TopBannerOrder", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.GetTopSchoolBannerOrder ", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("TopBannerOrder");
            }
        }

        public async Task<IEnumerable<string>> GetBannerSchoolIds(long bannerId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BannerId", bannerId, DbType.Int64);
                return await conn.QueryAsync<string>("School.GetBannerSchoolIds", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolBanner>> GetUserBanners(long UserId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", UserId, DbType.Int64);
                return await conn.QueryAsync<SchoolBanner>("School.GetUserBanner", parameters, null, null, CommandType.StoredProcedure);
            }
        }
       
        public async Task<SchoolBanner> GetSchoolBannerDetails(long schoolBannerId, long userId)
        {
            List<string> lstSchoolIds = new List<string>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolBannerId", schoolBannerId, DbType.Int64);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<SchoolBanner>("School.GetSchoolBannerDetails", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        #region sql repo methods
        public async override Task<SchoolBanner> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<SchoolBanner>> GetAllAsync()
        {
            throw new NotImplementedException();
        }


        public override void InsertAsync(SchoolBanner entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SchoolBanner entityToUpdate)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Private methods
        private DynamicParameters GetAssignmentParameters(SchoolBanner entity, TransactionModes mode)
        {
            var userId = mode != TransactionModes.Delete ? entity.CreatedBy : entity.DeletedBy;

            //entity.DueDate = entity.DueDate.Add(entity.DueTime);
            var parameters = new DynamicParameters();
            if ((int)mode == 2 || (int)mode == 3)
            {
                parameters.Add("@SchoolBannerId", entity.SchoolBannerId, DbType.Int64);
                parameters.Add("@TranMode", (int)mode, DbType.Int32);
            }

            parameters.Add("@BannerName", entity.BannerName, DbType.String);
            parameters.Add("@ActualImageName", entity.ActualImageName, DbType.String);
            parameters.Add("@UploadedImageName", entity.UploadedImageName, DbType.String);
            parameters.Add("@BannerPubliseDate", entity.BannerPublishDate, DbType.DateTime);
            parameters.Add("@BannerEndDate", entity.BannerEndDate, DbType.DateTime);
            parameters.Add("@ClickURL", entity.ClickURL, DbType.String);
            parameters.Add("@UserId", userId, DbType.Int32);
            parameters.Add("@SchoolIds", entity.Schools, DbType.String);
            parameters.Add("@DisplayOrder", entity.DisplayOrder, DbType.Int32);
            parameters.Add("@UserTypeIds", entity.UserTypes, DbType.String);
            parameters.Add("@SchoolBannerXml", entity.SchoolBannerXml, DbType.String);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        #endregion
    }
}
