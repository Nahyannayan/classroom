﻿using DbConnection;
using Phoenix.API.Models;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.API.Repositories
{
    public interface ISchoolBadgeRepository
    {
        Task<IEnumerable<SchoolBadge>> GetSchoolBadgesByPage(int pageNumber, int pageSize, string searchString, int userId, int schoolId);
        Task<IEnumerable<SchoolBadge>> GetSchoolBadges(long userId, int schoolId);
        OperationDetails InsertBadgeData(SchoolBadge entity);
        OperationDetails UpdateBadgeData(SchoolBadge entity, TransactionModes mode);
        Task<IEnumerable<string>> GetBadgeSchoolIds(long BadgeId);
        Task<SchoolBadge> GetSchoolBadgeDetails(long schoolBadgeId, long userId);
        int GetTopOrderForDisplayBadge(long userId);
        Task<IEnumerable<SchoolBadge>> GetUserBadges(long UserId);
        Task<bool> SaveStudentBadges(SchoolBadge model);
        Task<IEnumerable<SchoolBadge>> GetBadgesBySchoolId(int schoolId);
        Task<bool> SaveGroupWiseStudentBadges(StudentBadge studentBadge);
    }
}
