﻿using DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;

namespace Phoenix.API.Repositories
{
    public class SchoolBadgeRepository : SqlRepository<SchoolBadge>, ISchoolBadgeRepository
    {
        private readonly IConfiguration _config;
        public SchoolBadgeRepository(IConfiguration configuration) : base(configuration)
        {
            _config = configuration;
        }

        public async Task<IEnumerable<SchoolBadge>> GetSchoolBadgesByPage(int pageNumber, int pageSize, string searchString, int userId, int schoolId)
        {
            List<SchoolBadge> lstSchoolBadges = new List<SchoolBadge>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@UserId", schoolId, DbType.Int32);
                return await conn.QueryAsync<SchoolBadge>("School.GetSchoolBadgesByPage", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public async Task<IEnumerable<SchoolBadge>> GetSchoolBadges(long userId, int schoolId)
        {
            List<SchoolBadge> lstSchoolBadges = new List<SchoolBadge>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                parameters.Add("@UserId", userId, DbType.Int32);
                return await conn.QueryAsync<SchoolBadge>("School.GetSchoolBadges", parameters, null, null, CommandType.StoredProcedure);
            }
        }
        public OperationDetails InsertBadgeData(SchoolBadge entity)
        {
            OperationDetails op = new OperationDetails();
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAssignmentParameters(entity, TransactionModes.Insert);
                conn.Query<int>("School.BadgeInsert", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = true;
                }
                else
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = false;
                }
                return op;
            }
        }

        public OperationDetails UpdateBadgeData(SchoolBadge entity, TransactionModes mode)
        {
            OperationDetails op = new OperationDetails();
            using (var conn = GetOpenConnection())
            {
                var parameters = GetAssignmentParameters(entity, mode);
                conn.Query<int>("School.UpdateSchoolBadge", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = true;

                }
                else
                {
                    op.InsertedRowId = parameters.Get<int>("output");
                    op.Success = false;
                }
                return op;
            }
        }



        public int GetTopOrderForDisplayBadge(long userId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", userId, DbType.Int64);
                parameters.Add("@TopBadgeOrder", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("School.GetTopSchoolBadgeOrder ", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("TopBadgeOrder");
            }
        }

        public async Task<IEnumerable<string>> GetBadgeSchoolIds(long BadgeId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@BadgeId", BadgeId, DbType.Int64);
                return await conn.QueryAsync<string>("School.GetBadgeSchoolIds", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolBadge>> GetUserBadges(long UserId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", UserId, DbType.Int64);
                return await conn.QueryAsync<SchoolBadge>("School.GetUserBadge", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<SchoolBadge>> GetBadgesBySchoolId(int schoolId)
        {
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolId", schoolId, DbType.Int32);
                return await conn.QueryAsync<SchoolBadge>("School.GetBadgesBySchoolId", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<SchoolBadge> GetSchoolBadgeDetails(long schoolBadgeId, long userId)
        {
            List<string> lstSchoolIds = new List<string>();
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SchoolBadgeId", schoolBadgeId, DbType.Int64);
                parameters.Add("@UserId", userId, DbType.Int64);
                return await conn.QueryFirstOrDefaultAsync<SchoolBadge>("School.GetSchoolBadgeDetails", parameters, null, null, CommandType.StoredProcedure);
            }
        }

        public async Task<bool> SaveStudentBadges(SchoolBadge model)
        {
            bool result = false;
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserId", model.UserId, DbType.Int64);
                parameters.Add("@BadgesIds", model.BadgesIds, DbType.String);
                parameters.Add("@AssignedBy", model.AssignedBy, DbType.Int64);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("Student.SaveStudentBadges", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
            }
            return result;
        }

        public async Task<bool> SaveGroupWiseStudentBadges(StudentBadge studentBadge)
        {
            bool result = false;

            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            dt.Columns.Add("GroupStudentId");
            dt.TableName = "tblStudentGroupId";
            dt2.Columns.Add("StudentBadgeId");
            dt2.TableName = "tblStudentBadgeIds";
            if (!string.IsNullOrEmpty(studentBadge.schoolGroupIds))
            {
                List<int> groupIds = studentBadge.schoolGroupIds.Split(',').Select(int.Parse).ToList();
                foreach (var item in groupIds)
                {
                    dt.Rows.Add(item);
                }
            }
            if (!string.IsNullOrEmpty(studentBadge.badgesIds))
            {
                List<int> badgeIds = studentBadge.badgesIds.Split(',').Select(int.Parse).ToList();
                foreach (var item in badgeIds)
                {
                    dt2.Rows.Add(item);
                }
            }
            using (var conn = GetOpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TeacherId", studentBadge.CreatedBy, DbType.Int64);
                parameters.Add("@SchoolGroupIds", dt, DbType.Object);
                parameters.Add("@UserId", studentBadge.userId, DbType.Int64);
                parameters.Add("@StudentBadgeIds", dt2, DbType.Object);
                parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("[School].[SaveGroupWiseSchoolBadges]", parameters, commandType: CommandType.StoredProcedure);
                if (parameters.Get<int>("output") > 0)
                    result = true;
            }
            return result;
        }


        #region Private methods
        private DynamicParameters GetAssignmentParameters(SchoolBadge entity, TransactionModes mode)
        {
            var userId = mode != TransactionModes.Delete ? entity.CreatedBy : entity.DeletedBy;

            var parameters = new DynamicParameters();
            if ((int)mode == 2 || (int)mode == 3)
            {
                parameters.Add("@SchoolBadgeId", entity.SchoolBadgeId, DbType.Int64);
                parameters.Add("@TranMode", (int)mode, DbType.Int32);
            }
            parameters.Add("@BadgeTitle", entity.Title, DbType.String);
            parameters.Add("@Description", entity.Description, DbType.String);
            parameters.Add("@ActualImageName", entity.ActualImageName, DbType.String);
            parameters.Add("@UploadedImageName", entity.UploadedImageName, DbType.String);
            parameters.Add("@UserId", userId, DbType.Int32);
            parameters.Add("@SchoolId", entity.SchoolId, DbType.String);
            parameters.Add("@DisplayOrder", entity.DisplayOrder, DbType.Int32);
            parameters.Add("@output", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }

        public override void DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<SchoolBadge>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<SchoolBadge> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public override void InsertAsync(SchoolBadge entity)
        {
            throw new NotImplementedException();
        }

        public override void UpdateAsync(SchoolBadge entityToUpdate)
        {
            throw new NotImplementedException();
        }


        #endregion
    }
}
