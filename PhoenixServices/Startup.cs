﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutofacSerilogIntegration;
using ElmahCore.Mvc;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.IdentityModel.Tokens;
using Phoenix.API.Filters;
using Phoenix.API.Repositories;
using Phoenix.API.Repositories.ChartsDashboard;
using Phoenix.API.Repositories.ChartsDashboard.StudentProgressTracker;
using Phoenix.API.Repositories.ExemplarWall;
using Phoenix.API.Repositories.ExemplarWall.Contracts;
using Phoenix.API.Repositories.PTM;
using Phoenix.API.Repositories.PTM.Contracts;
using Phoenix.API.Repositories.Setting;
using Phoenix.API.Repositories.Setting.Contract;
using Phoenix.API.Repositories.Skill.Contracts;
using Phoenix.API.Services;
using Phoenix.API.Services.ChartsDashboard;
using Phoenix.API.Services.ExemplarWall;
using Phoenix.API.Services.ExemplarWall.Contracts;
using Phoenix.API.Services.Setting;
using Phoenix.API.Services.Setting.Contract;
using Phoenix.API.Services.Users;
using Serilog;
using SIMS.API.Repositories;
using SIMS.API.Services;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                    });
            })// Make sure you call this previous to AddMvc
                    .AddCustomMVC(Configuration)
                    .AddCustomDbContext(Configuration)
                    .AddCustomOptions(Configuration)
                    .AddElmah()
                    .AddCustomHealthCheck(Configuration)
                    .AddSwagger(Configuration)
                    .AddLogging()
                    .AddAuthentication(Configuration);

            // Now register our services with Autofac container
            var container = new ContainerBuilder();
            container.Populate(services);
            container.RegisterLogger();
            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //else
            //{
            //    app.UseExceptionHandler();
            //    app.UseHsts();
            //}
            app.UseSerilogRequestLogging();
            app.UseHttpsRedirection();
            // Make sure you call this before calling app.UseMvc()
            app.UseCors("AllowAll");
            app.UseAuthentication();
            app.UseMvc();

            var pathBase = Configuration["PATH_BASE"];

            if (!string.IsNullOrEmpty(pathBase))
            {
                //loggerFactory.CreateLogger<Startup>().LogDebug("Using PATH BASE '{pathBase}'", pathBase);
                app.UsePathBase(pathBase);
            }

            app.UseHealthChecks("/hc", new HealthCheckOptions()
            {
                Predicate = _ => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            app.UseHealthChecks("/liveness", new HealthCheckOptions
            {
                Predicate = r => r.Name.Contains("self")
            });

            app.UseMvcWithDefaultRoute();
            app.UseElmah();

            app.UseSwagger()
              .UseSwaggerUI(c =>
              {
                  c.SwaggerEndpoint($"{ (!string.IsNullOrEmpty(pathBase) ? pathBase : string.Empty) }/swagger/v1/swagger.json", "Phoenix Service API'S V1");
              });
        }
    }

    public static class CustomExtensionMethods
    {
        public static IServiceCollection AddCustomMVC(this IServiceCollection services, IConfiguration configuration)
        {
            //TO allow specific domain
            //services.AddCors(options => options.AddPolicy("ApiCorsPolicy", builder =>
            //{
            //    builder.WithOrigins("http://localhost:4200").AllowAnyMethod().AllowAnyHeader();
            //})); 

            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
            })
               .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
               .AddControllersAsServices();

            services.Configure<IISServerOptions>(options =>
            {
                options.AutomaticAuthentication = false;
                //options.ForwardClientCertificate = false;
            });

            //To redirect from http to https
            services.AddHsts(options =>
            {
                options.IncludeSubDomains = true;
                options.Preload = true;
                options.MaxAge = TimeSpan.FromDays(120);

                var handler = new HttpClientHandler
                {
                    SslProtocols = SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls
                };

                HttpClient client = new HttpClient(handler);
            });

            //Load Balancer
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            return services;
        }

        public static IServiceCollection AddCustomDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            //Repository DI configuration
            services.AddTransient<IStudentRepository, StudentRepository>();
            services.AddTransient<IContactInfoRepository, ContactInfoRepository>();
            services.AddTransient<IThemesRepository, ThemesRepository>();
            services.AddTransient<ICountryRepository, CountryRepository>();
            services.AddTransient<ILogInUserRepository, LogInUserRepository>();
            services.AddTransient<IAbuseContactInfoRepository, AbuseContactInfoRepository>();
            services.AddTransient<IAbuseDelegateRepository, AbuseDelegateRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserPermissionRepository, UserPermissionRepository>();
            services.AddTransient<IEmailSettingsRepository, EmailSettingsRepository>();
            services.AddTransient<IBlogRepository, BlogRepository>();
            services.AddTransient<IBlogCommentRepository, BlogCommentRepository>();
            services.AddTransient<IBlogShareRepository, BlogShareRepository>();
            //Chat Service
            services.AddTransient<IChatRepository, ChatRepository>();
            services.AddTransient<IFolderRepository, FolderRepository>();
            services.AddTransient<IFileRepository, FileRepository>();
            services.AddTransient<IAbuseReportLedgerRepository, AbuseReportLedgerRepository>();
            services.AddTransient<IStudentSkillSetRepository, StudentSkillSetRepository>();
            services.AddTransient<IStudentCertificateRepository, StudentCertificateRepository>();
            services.AddTransient<IStudentAcademicRepository, StudentAcademicRepository>();
            services.AddTransient<IStudentGoalsRepository, StudentGoalsRepository>();
            services.AddTransient<ITemplateRepository, TemplateRepository>();
            services.AddTransient<IStudentAchievementRepository, StudentAchievementRepository>();
            services.AddTransient<ISchoolNotificationRepository, SchoolNotificationRepository>();
            services.AddTransient<IPlanSchemeDetailRepository, PlanSchemeDetailRepository>();
            services.AddTransient<ILanguageSettingsRepository, LanguageSettingsRepository>();
            services.AddTransient<IGroupCourseTopicRepository, GroupCourseTopicRepository>();

            services.AddTransient<ILanguageSettingsRepository, LanguageSettingsRepository>();
            //Services DI Configuration
            services.AddTransient<IStudentService, StudentService>();
            services.AddTransient<IContactInfoService, ContactInfoService>();
            services.AddTransient<IThemesService, ThemesService>();
            services.AddTransient<ICountryService, CountryService>();
            services.AddTransient<ILogInUserService, LogInUserService>();
            services.AddTransient<IAbuseDelegateService, AbuseDelegateService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserPermissionService, UserPermissionService>();
            services.AddTransient<IEmailSettingsService, EmailSettingsService>();
            services.AddTransient<IBlogCommentService, BlogCommentService>();
            services.AddTransient<IBlogService, BlogService>();
            services.AddTransient<IBlogShareService, BlogShareService>();
            //Chat service
            services.AddTransient<IChatService, ChatService>();
            services.AddTransient<IFolderService, FolderService>();
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IAbuseReportLedgerService, AbuseReportLedgerService>();
            services.AddTransient<IStudentSkillSetService, StudentSkillSetService>();
            services.AddTransient<IStudentCertificateService, StudentCertificateService>();
            services.AddTransient<IStudentAcademicService, StudentAcademicService>();
            services.AddTransient<IStudentGoalsService, StudentGoalsService>();
            services.AddTransient<IStudentAchievementService, StudentAchievementService>();
            services.AddTransient<ITemplateService, TemplateService>();
            services.AddTransient<ISchoolNotificationService, SchoolNotificationService>();
            services.AddTransient<IPlanSchemeService, PlanSchemeService>();
            services.AddTransient<ILanguageSettingsService, LanguageSettingsService>();
            services.AddTransient<IGroupCourseTopicService, GroupCourseTopicService>();

            // School
            services.AddTransient<ISchoolRepository, SchoolRepository>();
            services.AddTransient<ISchoolService, SchoolService>();
            services.AddTransient<ISchoolSpaceRepository, SchoolSpaceRepository>();
            services.AddTransient<ISchoolSpaceService, SchoolSpaceService>();

            // Safety Categories
            services.AddTransient<ISafetyCategoriesRepository, SafetyCategoriesRepository>();
            services.AddTransient<ISafetyCategoriesService, SafetyCategoriesService>();

            //School Skill Set
            services.AddTransient<ISchoolSkillSetRepository, SchoolSkillSetRepository>();
            services.AddTransient<ISchoolSkillSetService, SchoolSkillSetService>();

            //School Goal
            services.AddTransient<ISchoolGoalRepository, SchoolGoalRepository>();
            services.AddTransient<ISchoolGoalService, SchoolGoalService>();

            // Student Suggestion
            services.AddTransient<IStudentSuggestionService, StudentSuggestionService>();
            services.AddTransient<IStudentSuggestionRepository, StudentSuggestionRepository>();

            // Content Library
            services.AddTransient<IContentLibraryService, ContentLibraryService>();
            services.AddTransient<IContentLibraryRepository, ContentLibraryRepository>();

            // Suggestion Category
            services.AddTransient<ISuggestionCategoryService, SuggestionCategoryService>();
            services.AddTransient<ISuggestionCategoryRepository, SuggestionCategoryRepository>();

            // Subject
            services.AddTransient<ISubjectRepository, SubjectRepository>();
            services.AddTransient<ISubjectService, SubjectService>();

            // Grading Template
            services.AddTransient<IGradingTemplateRepository, GradingTemplateRepository>();
            services.AddTransient<IGradingTemplateService, GradingTemplateService>();

            // Grading Template Item
            services.AddTransient<IGradingTemplateItemRepository, GradingTemplateItemRepository>();
            services.AddTransient<IGradingTemplateItemService, GradingTemplateItemService>();

            // Censor Filters
            services.AddTransient<ICensorFilterRepository, CensorFilterRepository>();
            services.AddTransient<ICensorFilterService, CensorFilterService>();

            // User Roles
            services.AddTransient<IUserRoleRepository, UserRoleRepository>();
            services.AddTransient<IUserRoleService, UserRoleService>();

            // User Roles
            services.AddTransient<ISelectListRepository, SelectListRepository>();
            services.AddTransient<ISelectListService, SelectListService>();

            // Chatter Permissions
            services.AddTransient<IChatterPermissionRepository, ChatterPermissionRepository>();
            services.AddTransient<IChatterPermissionService, ChatterPermissionService>();

            // Marking Scheme
            services.AddTransient<IMarkingSchemeRepository, MarkingSchemeRepository>();
            services.AddTransient<IMarkingSchemeService, MarkingSchemeService>();

            // Users
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserService, UserService>();

            // Module Structure
            services.AddTransient<IModuleStructureRepository, ModuleStructureRepository>();
            services.AddTransient<IModuleStructureService, ModuleStructureService>();
            //  services.AddTransient<IGradingTemplateRepository, GradingTemplateRepository>();
            //  services.AddTransient<IGradingTemplateService, GradingTemplateService>();

            //Assignment
            services.AddTransient<IAssignmentRepository, AssignmentRepository>();
            services.AddTransient<IAssignmentService, AssignmentService>();

            //Observation
            services.AddTransient<IObservationRepository, ObservationRepository>();
            services.AddTransient<IObservationService, ObservationService>();

            //SchoolGroup
            services.AddTransient<ISchoolGroupRepository, SchoolGroupRepository>();
            services.AddTransient<ISchoolGroupService, SchoolGroupService>();

            //StudentInformation
            services.AddTransient<IStudentListRepository, StudentListRepository>();
            services.AddTransient<IStudentListService, StudentListService>();

            //Event
            services.AddTransient<IDurationRepository, DurationRepository>();
            services.AddTransient<IDurationService, DurationService>();
            services.AddTransient<IEventCategoryRepository, EventCategoryRepository>();
            services.AddTransient<IEventCategoryService, EventCategoryService>();
            services.AddTransient<IEventsService, EventsService>();
            services.AddTransient<IEventsRepository, EventsRepository>();

            //services.AddTransient<IEventCategoryService, EventCategoryService>();

            //RSSFeed
            services.AddTransient<IRSSFeedRepository, RSSFeedRespository>();
            services.AddTransient<IRSSFeedService, RSSFeedService>();

            //Terminology Editor
            services.AddTransient<ITerminologyEditorRepository, TerminologyEditorRepository>();
            services.AddTransient<ITerminologyEditorService, TerminologyEditorService>();

            //teacher dashboard
            services.AddTransient<ITeacherDashboardRepository, TeacherDashboardRepository>();
            services.AddTransient<ITeacherDashboardService, TeacherDashboardService>();

            //Parent
            services.AddTransient<IParentRepository, ParentRepository>();
            services.AddTransient<IParentService, ParentService>();

            //Quiz
            services.AddTransient<IQuizRepository, QuizRepository>();
            services.AddTransient<IQuizService, QuizService>();
            services.AddTransient<IQuizQuestionsRepository, QuizQuestionsRepository>();
            services.AddTransient<IQuizQuestionsService, QuizQuestionsService>();

            //GroupUrl
            services.AddTransient<IGroupUrlService, GroupUrlService>();
            services.AddTransient<IGroupUrlRepository, GroupUrlRepository>();

            //GroupQuiz
            services.AddTransient<IGroupQuizService, GroupQuizService>();
            services.AddTransient<IGroupQuizRepository, GroupQuizRepository>();

            //Bookmark
            services.AddTransient<IBookmarkService, BookmarkService>();
            services.AddTransient<IBookmarkRepository, BookmarkRepository>();

            //School banner
            services.AddTransient<ISchoolBannerService, SchoolBannerService>();
            services.AddTransient<ISchoolBannerRepository, SchoolBannerRepository>();

            //School badge
            services.AddTransient<ISchoolBadgeService, SchoolBadgeService>();
            services.AddTransient<ISchoolBadgeRepository, SchoolBadgeRepository>();

            //Behaviour
            services.AddTransient<IBehaviourService, BehaviourService>();
            services.AddTransient<IBehaviourRepository, BehaviourRepository>();

            //Behaviour Setup
            services.AddTransient<IBehaviourSetupService, BehaviourSetupService>();
            services.AddTransient<IBehaviourSetupRepository, BehaviourSetupRepository>();

            //School Marking Policy
            //services.AddTransient<IMarkingPolicyService, MarkingPolicyService>();
            //services.AddTransient<IMarkingPolicyRepository, MarkingPolicyRepository>();

            //Async Lesson
            services.AddTransient<IAsyncLessonRepository, AsyncLessonRepository>();
            services.AddTransient<IAsyncLessonService, AsyncLessonService>();

            #region Course

            services.AddTransient<ICourseRepository, CourseRepository>();
            services.AddTransient<ICourseService, CourseService>();
            //Unit
            services.AddTransient<IUnitRepository, UnitRepository>();
            services.AddTransient<IUnitService, UnitService>();

            //Leason
            // services.AddTransient<ILessonRepository, LessonRepository>();
            // services.AddTransient<ILessonService, LessonService>();
            //Standard
            services.AddTransient<IStandardRepository, StandardRepository>();
            services.AddTransient<IStandardService, StandardService>();

            services.AddTransient<IAssessmentEvidenceRepository, AssessmentEvidenceRepository>();
            services.AddTransient<IAssessmentEvidenceService, AssessmentEvidenceService>();

            services.AddTransient<IAssessmentLearningRepository, AssessmentLearningRepository>();
            services.AddTransient<IAssessmentLearningService, AssessmentLearningService>();

            //Standard Details
            services.AddTransient<IStandardDetailsRepository, StandardDetailsRepository>();
            services.AddTransient<IStandardDetailsService, StandardDetailsService>();

            //Leason
            services.AddTransient<ILessonRepository, LessonRepository>();
            services.AddTransient<ILessonService, LessonService>();

            //Attachment
            services.AddTransient<IAttachmentRepository, AttachmentRepository>();
            services.AddTransient<IAttachmentService, AttachmentService>();

            //

            #endregion Course

            services.AddTransient<IAssessmentConfigRepository, AssessmentConfigRepository>();
            services.AddTransient<IAssessmentConfigService, AssessmentConfigService>();
            //Repository DI configuration Attendance

            #region

            services.AddTransient<IAttendanceRepository, AttendanceRepository>();
            services.AddTransient<IGradeService, GradeService>();
            services.AddTransient<IGradeRepository, GradeRepository>();
            services.AddTransient<ISurveyService, SurveyService>();
            services.AddTransient<ISurveyRepository, SurveyRepository>();
            //Services DI Configuration
            services.AddTransient<IAttendanceService, AttendanceService>();
            services.AddTransient<IAttendanceSettingService, AttendanceSettingService>();
            services.AddTransient<IAttendanceSettingRepository, AttendanceSettingRepository>();
            services.AddTransient<IClassListService, ClassListService>();
            services.AddTransient<IClassListRepository, ClassListRepository>();
            services.AddTransient<ICurriculumRoleService, CurriculumRoleService>();

            services.AddTransient<ICurriculumRoleRepository, CurriculumRoleRepository>();

            #endregion

            #region Behaviour

            services.AddTransient<IIncidentService, IncidentService>();
            services.AddTransient<IIncidentRepository, IncidentRepository>();

            #endregion

            #region ProgressTracker

            services.AddTransient<IProgressTrackerRepository, ProgressTrackerRepository>();
            services.AddTransient<IProgressTrackerService, ProgressTrackerService>();

            #endregion

            #region Assessment

            services.AddTransient<IAssessmentRepository, AssessmentRepository>();
            services.AddTransient<IAssessmentService, AssessmentService>();

            #endregion Assessment

            #region Assessment  Configuration

            services.AddTransient<IAssessmentConfigurationRepository, AssessmentConfigurationRepository>();
            services.AddTransient<IAssessmentConfigurationService, AssessmentConfigurationService>();

            #endregion

            #region Course Catalogue

            services.AddTransient<ICourseCatalogueService, CourseCatalogueService>();
            services.AddTransient<ICourseCatalogueRepository, CourseCatalogueRepository>();

            #endregion

            #region Division

            services.AddTransient<IDivisionRepositorycs, DivisionRepository>();
            services.AddTransient<IDivisionService, DivisionService>();

            #endregion

            #region Grade Book

            services.AddTransient<IGradeBookRepository, GradeBookRepository>();
            services.AddTransient<IGradeBookService, GradeBookService>();
            services.AddTransient<IGradeBookSetupRepository, GradeBookSetupRepository>();
            services.AddTransient<IGradeBookSetupService, GradeBookSetupService>();

            #endregion Grade Book

            #region DashBoard

            services.AddTransient<ITeacherDashboardRepository, TeacherDashboardRepository>();
            services.AddTransient<IDashBoardGroupsRepository, IDashBoardGroupsRepository>();
            services.AddTransient<IDashBoardServices, DashBoardServices>();

            #endregion DashBoard

            //Exemplar
            services.AddTransient<IExemplarWallRepository, ExemplarWallRepository>();
            services.AddTransient<IExemplarWallService, ExemplarWallService>();
            //System Image
            services.AddTransient<ISystemImageFileRepository, SystemImageFileRepository>();
            services.AddTransient<ISystemImageFileSerivce, SystemImageFileSerivce>();

            //Setting
            services.AddTransient<ISettingService, SettingService>();
            services.AddTransient<ISettingRepository, SettingRepository>();
            #region PTM
            services.AddTransient<IPTMCategoryService, PTMCategoryService>();
            services.AddTransient<IPTMDeclineReasonService, PTMDeclineReasonService>();
            services.AddTransient<IPTMMeetingService, PTMMeetingService>();

            services.AddTransient<IPTMCategoryRepository, PTMCategoryRepository>();
            services.AddTransient<IPTMDeclineReasonRepository, PTMDeclineReasonRepository>();
            services.AddTransient<IPTMMeetingRepository, PTMMeetingRepository>();
            #endregion

            #region SchoolReport

            services.AddTransient<ISchoolReportService, SchoolReportService>();
            services.AddTransient<ISchoolReportRepository, SchoolReportRepository>();

            #endregion

            #region Chart Dashboards
            services.AddScoped<IChartsDashboardRepository, SqlChartsDashboardRepository>();
            services.AddScoped<IChartsDashboardService, ChartsDashboardService>();
            #endregion

            #region Migration Job
            services.AddTransient<IMigrationJobService, MigrationJobService>();
            services.AddScoped<IMigrationJobRepository, MigrationJobRepository>();
            #endregion
            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "Phoenix Services 2.0 - Phoenix HTTP API",
                    Version = "v1",
                    Description = "The Phoenix services HTTP API.",
                    TermsOfService = "Terms Of Service"
                });

                //options.AddSecurityDefinition("oauth2", new OAuth2Scheme
                //{
                //    Type = "oauth2",
                //    Flow = "implicit",
                //    AuthorizationUrl = $"{configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                //    TokenUrl = $"{configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                //    Scopes = new Dictionary<string, string>()
                //    {
                //        { "phoenix", "Phoenix Service API" }
                //    }
                //});
                options.AddSecurityDefinition("Bearer",
               new ApiKeyScheme
               {
                   In = "header",
                   Description = "Please enter into field the word 'Bearer' following by space and JWT",
                   Name = "Authorization",
                   Type = "apiKey"
               });
                options.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                { "Bearer", Enumerable.Empty<string>() }
                });

                options.OperationFilter<AuthorizeCheckOperationFilter>();
                options.EnableAnnotations();
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });

            return services;
        }

        public static IServiceCollection AddCustomOptions(this IServiceCollection services, IConfiguration configuration)
        {
            //services.Configure<CatalogSettings>(configuration);
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetails = new ValidationProblemDetails(context.ModelState)
                    {
                        Instance = context.HttpContext.Request.Path,
                        Status = StatusCodes.Status400BadRequest,
                        Detail = "Please refer to the errors property for additional details."
                    };

                    return new BadRequestObjectResult(problemDetails)
                    {
                        ContentTypes = { "application/problem+json", "application/problem+xml" }
                    };
                };
            });

            return services;
        }

        public static IServiceCollection AddCustomHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            var hcBuilder = services.AddHealthChecks();

            hcBuilder
                .AddCheck("self", () => HealthCheckResult.Healthy())
                .AddSqlServer(
                    configuration["ConnectionStrings:phoenixConStr"],
                    name: "StudentDTO-check",
                    tags: new string[] { "phoenixSqlServerStatus" });

            return services;
        }

        public static IServiceCollection AddAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            var audiences = Convert.ToString(configuration["Jwt:Audiences"]);
            var audList = new List<string>();

            foreach (var aud in audiences.Split(","))
            {
                audList.Add(aud);
            }

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                //options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidIssuer = configuration["Jwt:Issuer"],
                    ValidAudiences = audList,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"])),
                    ClockSkew = TimeSpan.FromSeconds(0)
                };

                options.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }
                        return Task.CompletedTask;
                    }
                };
            });

            return services;
        }
    }
}