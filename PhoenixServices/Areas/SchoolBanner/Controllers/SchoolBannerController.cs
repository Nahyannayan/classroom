﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Web;
using Phoenix.Common.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;

namespace Phoenix.API.Areas.Users.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SchoolBannerController : ControllerBase
    {
        private readonly ISchoolBannerService _schoolBannerService;

        public SchoolBannerController(ISchoolBannerService schoolBannerService)
        {
            _schoolBannerService = schoolBannerService;
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 08 Jan 2020
        /// Description - To get School Banner By School Id
        /// </summary>
        ///  <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchString"></param>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolBannersByPage")]
        [ProducesResponseType(typeof(IEnumerable<Assignment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolBannersByPage(int pageNumber, int pageSize, string searchString, int userId, int schoolId)
        {
            var schoolBannerList = await _schoolBannerService.GetSchoolBannersByPage(pageNumber, pageSize, searchString, userId, schoolId);
            return Ok(schoolBannerList);
        }


        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 08 Jan 2020
        /// Description - To get School Banner By School Id
        /// </summary>       
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolBanners")]
        [ProducesResponseType(typeof(IEnumerable<SchoolBanner>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolBanners(long userId, int schoolId)
        {
            var schoolBannerList = await _schoolBannerService.GetSchoolBanners(userId, schoolId);
            return Ok(schoolBannerList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 08 Jan 2020
        /// Description - To insert school banner
        /// </summary>
        /// <param name="schoolBanner"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertSchoolBanner")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertSchoolBanner([FromBody]SchoolBanner schoolBanner)
        {
            var result = _schoolBannerService.InsertBannerData(schoolBanner);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 08 Jan 2020
        /// Description - To insert school banner
        /// </summary>
        /// <param name="schoolBanner"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateSchoolBanner")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateSchoolBanner([FromBody]SchoolBanner schoolBanner)
        {
            var result = _schoolBannerService.UpdateBannerData(schoolBanner,TransactionModes.Update);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 08 Jan 2020
        /// Description - To delete school banner
        /// </summary>
        /// <param name="schoolBanner"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteSchoolBanner")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteSchoolBanner([FromBody]SchoolBanner schoolBanner)
        {
            var result = _schoolBannerService.UpdateBannerData(schoolBanner, TransactionModes.Delete);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 10 Jan 2020
        /// Description - To Get Schools Selected for Banner
        /// </summary>
        /// <param name="SchoolBannerId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetBannerSchoolIds")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetBannerSchoolIds(Int64 SchoolBannerId)
        {
            var schoolBannerList = await _schoolBannerService.GetBannerSchoolIds(SchoolBannerId);
            return Ok(schoolBannerList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 10 Jan 2020
        /// Description - To get School Banner Details
        /// </summary>       
        /// <param name="userId"></param>  
        /// <param name="schoolBannerId"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolBannerDetails")]
        [ProducesResponseType(typeof(SchoolBanner), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolBannerDetails(long schoolBannerId, long userId)
        {
            var schoolbanner = await _schoolBannerService.GetSchoolBannerDetails(schoolBannerId, userId);
            return Ok(schoolbanner);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 10 Jan 2020
        /// Description - To get School Banner Details
        /// </summary>       
        /// <param name="userId"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetTopOrderForDisplayBanner")]
        [ProducesResponseType(typeof(SchoolBanner), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTopOrderForDisplayBanner(long userId)
        {
            var order =  _schoolBannerService.GetTopOrderForDisplayBanner(userId);
            return Ok(order);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 10 Jan 2020
        /// Description - To get School Banner Details
        /// </summary>       
        /// <param name="userId"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetUserBanners")]
        [ProducesResponseType(typeof(IEnumerable<SchoolBanner>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserBanners(long userId)
        {
            var schoolbanner = await _schoolBannerService.GetUserBanners(userId);
            return Ok(schoolbanner);
        }
    }
}