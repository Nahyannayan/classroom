﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.Goal.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SchoolGoalController : Controller
    {
        private readonly ISchoolGoalService _schoolGoalService;
        public SchoolGoalController(ISchoolGoalService schoolGoalService)
        {
            _schoolGoalService = schoolGoalService;
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 19 Jan 2020
        /// Description - To Get all School Goals.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getschoolgoals")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGoal>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGoals(int languageId)
        {
            var GoalList = await _schoolGoalService.GetSchoolGoals(languageId);
            return Ok(GoalList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 19 Jan 2020
        /// Description - To Get  School Goal by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getschoolgoalById/{id:int}")]
        [ProducesResponseType(typeof(SchoolGoal), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGoalById(int id)
        {
            var Suggestion = await _schoolGoalService.GetSchoolGoalById(id);
            return Ok(Suggestion);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 19 Jan 2020
        /// Description - To Insert School Goal.
        /// </summary>
        /// <param name="schoolGoal"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertSchoolGoal")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertSchoolGoal([FromBody]SchoolGoal schoolGoal)
        {
            var result = _schoolGoalService.InsertSchoolGoal(schoolGoal);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 19 Jan 2020
        /// Description - To Update School Goal.
        /// </summary>
        /// <param name="schoolGoal"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateSchoolGoal")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateSchoolGoal([FromBody]SchoolGoal schoolGoal)
        {
            var result = _schoolGoalService.UpdateSchoolGoal(schoolGoal);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 19 Jan 2020
        /// Description - To Delete School Goal.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteSchoolGoal/{id:int}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteSchoolGoal(int id)
        {
            var result = _schoolGoalService.DeleteSchoolGoal(id);
            return Ok(result);
        }


    }
}
