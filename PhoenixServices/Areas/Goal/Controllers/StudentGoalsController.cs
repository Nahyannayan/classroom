﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.Goal.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class StudentGoalsController : ControllerBase
    {
        private readonly IStudentGoalsService _studentGoalsService;
        public StudentGoalsController(IStudentGoalsService studentGoalsService)
        {
            _studentGoalsService = studentGoalsService;
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 2nd Feb 2020
        /// Description - Get all goals of students with server side pagination
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name=""></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllStudentGoals")]
        [ProducesResponseType(typeof(List<StudentGoal>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllStudentGoals([FromQuery] long id, [FromQuery] int pageIndex, [FromQuery] int pageSize, [FromQuery] long currentUserId, [FromQuery] string searchString = "")
        {
            var studentGoals = await _studentGoalsService.GetAllStudentGoals(id, pageIndex, pageSize, currentUserId, searchString);
            return Ok(studentGoals);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 3rd Febuary 2020
        /// Description - Toggle show/hide student goals in student portfolio page.
        /// </summary>
        /// <param name="goalModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateDashboardGoalsStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateDashboardGoalsStatus(StudentGoal goalModel)
        {
            bool result = await _studentGoalsService.UpdateDashboardGoalsStatus(goalModel);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rahul Verma
        /// Created Date - 20th July 2020
        /// Description - Student Portfolio=> Edit Student Goals => Save all checked Goals to show/hide on student portfolio page
        /// </summary>
        /// <param name="lstGoalModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAllDashboardGoalsStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAllDashboardGoalsStatus(List<StudentGoal> lstGoalModel)
        {
            bool result = await _studentGoalsService.UpdateAllDashboardGoalsStatus(lstGoalModel);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Creaetd Date - 3rd Feb 2020
        /// Description - Insert new student goals
        /// </summary>
        /// <param name="studentGoalModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertStudentGoals")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> InsertStudentGoal(StudentGoal studentGoalModel)
        {
            bool result = await _studentGoalsService.InsertStudentGoal(studentGoalModel);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 17th Feb 2020
        /// Description - Approve student goals
        /// </summary>
        /// <param name="studentGoal"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ApproveStudentGoal")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ApproveStudentGoal(StudentGoal studentGoal)
        {
            bool result = await _studentGoalsService.ApproveStudentGoal(studentGoal);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 30th July 2020
        /// Description - deletes student goals
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteStudentGoal")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteStudentGoal([FromBody] StudentGoal model)
        {
            var result = await _studentGoalsService.DeleteStudentGoal(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 30th July 2020
        /// Description - retreives student goal by id
        /// </summary>
        /// <param name="goalId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentGoalById")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentGoalById(long goalId)
        {
            var result = await _studentGoalsService.GetStudentGoalById(goalId);
            return Ok(result);
        }
    }
}