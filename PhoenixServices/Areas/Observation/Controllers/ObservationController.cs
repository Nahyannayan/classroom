﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.Observations.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class ObservationController : ControllerBase
    {
        private readonly IObservationService _observationService;
        public ObservationController(IObservationService ObservationService)
        {
            _observationService = ObservationService;
        }

        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 26 August 2019
        /// Description - To insert observation
        /// </summary>
        /// <param name="observation"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertObservation")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertObservation([FromBody]Observation observation)
        {
            var result = _observationService.InsertObservation(observation);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date -  26 August 2019
        /// Description - To update observation.
        /// </summary>
        /// <param name="Observation"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateObservation")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateObservation([FromBody]Observation observation)
        {
            var result = _observationService.UpdateObservationData(observation);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 26 August 2019
        /// Description - To delete Observation.
        /// </summary>
        /// <param name="observation"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteObservation")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteObservation([FromBody]Observation observation)
        {
            var result = _observationService.DeleteObservation(observation);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 26 August 2019
        /// Description - To get observation by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetObservationById/{id:int}/{userId:long}")]
        [ProducesResponseType(typeof(Observation), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetObservationById(int id, long userId)
        {
            var Observation = await _observationService.GetObservationById(id, userId);
            return Ok(Observation);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 26 August 2019
        /// Description - To get files uploaded with Observation
        /// </summary>
        /// <param name="observationId"></param>        
        /// <returns>ObservationFileList</returns>
        [HttpGet]
        [Route("GetObservationFilesById")]
        [ProducesResponseType(typeof(IEnumerable<ObservationStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetObservationFilesByObservationId(int observationId)
        {
            var ObservationFileList = await _observationService.GetObservationFilesById(observationId);
            return Ok(ObservationFileList);
        }


        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 26 August 2019
        /// Description - To get files uploaded with Observation
        /// </summary>
        /// <param name="observationId"></param>        
        /// <returns>ObservationFileList</returns>
        [HttpGet]
        [Route("GetObservationObjectives")]
        [ProducesResponseType(typeof(IEnumerable<Objective>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetObservationObjectives(int observationId)
        {
            var ObservationFileList = await _observationService.GetObservationObjectives(observationId);
            return Ok(ObservationFileList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date -26 August 2019
        /// Description - To get school groups associated with Observation
        /// </summary>
        /// <param name="observationId"></param>        
        /// <returns>schoolGroupList</returns>
        [HttpGet]
        [Route("GetSelectegGroupsByObservationId")]
        [ProducesResponseType(typeof(IEnumerable<ObservationStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSelectegGroupsByObservationId(int observationId)
        {
            var schoolGroupList = await _observationService.GetSelectegGroupsByObservationId(observationId);
            return Ok(schoolGroupList);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 26 August 2019
        /// Description - To get school groups associated with Observation
        /// </summary>
        /// <param name="observationId"></param>        
        /// <returns>schoolGroupList</returns>
        [HttpGet]
        [Route("GetObservationStudent")]
        [ProducesResponseType(typeof(IEnumerable<ObservationStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetObservationStudent(int observationId)
        {
            var schoolGroupList = await _observationService.GetObservationStudent(observationId);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 21 Aug 2019
        /// Description - to get file as Observation File from My File
        /// </summary>
        /// <param name="fileId"></param>          
        /// <param name="isFolder"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetObservationMyFile")]
        [ProducesResponseType(typeof(IEnumerable<ObservationFile>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetObservationMyFile(long fileId, bool isFolder)
        {
            var myFiles = await _observationService.GetObservationMyFiles(fileId, isFolder);
            return Ok(myFiles);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 21 Aug 2019
        /// Description - to get file as Observation teacher using teacher id
        /// </summary>
        /// <param name="pageNumber"></param>          
        /// <param name="pageSize"></param>  
        /// <param name="teacherId"></param>  
        /// <param name="searchString"></param>  
        /// <param name="schoolGroupIds"></param>  
        /// <param name="sortBy"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetObservationTeacherPaging")]
        [ProducesResponseType(typeof(IEnumerable<Observation>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetObservationTeacherPaging(int pageNumber, int pageSize, long teacherId, string searchString, string schoolGroupIds,string sortBy)
        {
            var myFiles = await _observationService.GetObservationTeacherPaging(pageNumber, pageSize, teacherId, searchString, schoolGroupIds, sortBy);
            return Ok(myFiles);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 29 Aug 2019
        /// Description - to get file as Observation student using student id
        /// </summary>
        /// <param name="pageNumber"></param>          
        /// <param name="pageSize"></param>  
        /// <param name="studentId"></param>  
        /// <param name="searchString"></param>  
        /// <param name="sortBy"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetObservationStudentPaging")]
        [ProducesResponseType(typeof(IEnumerable<ObservationStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetObservationStudentPaging(int pageNumber, int pageSize, long studentId, string searchString, string sortBy)
        {
            var myFiles = await _observationService.GetObservationStudentPaging(pageNumber, pageSize, studentId, searchString, sortBy);
            return Ok(myFiles);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 4 Sept 2019
        /// Description - To delete Observation files.
        /// </summary>
        /// <param name="fileId"></param>          
        /// <param name="observationId"></param>  
        /// <param name="userId"></param>         
        /// <returns></returns>
        [HttpGet]
        [Route("DeleteObservationFile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> DeleteObservationFile(int fileId, int observationId, long userId)
        {
            var result = _observationService.DeleteObservationFile(fileId, observationId, userId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 09 Sept 2019
        /// Description - To get school groups associated with Observation
        /// </summary>
        /// <param name="observationId"></param>        
        /// <returns>schoolGroupList</returns>
        [HttpGet]
        [Route("GetObservationSubjects")]
        [ProducesResponseType(typeof(IEnumerable<Subject>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetObservationSubjects(int observationId)
        {
            var schoolGroupList = await _observationService.GetObservationSubjects(observationId);
            return Ok(schoolGroupList);
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 04 Nov 2019
        /// Description - to get file record by Observationfileid
        /// </summary>
        /// <param name="id"></param>          
        /// <returns></returns>
        [HttpGet]
        [Route("GetObservationFilebyObservationFileId")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetObservationFilebyObservationFileId(long id)
        {
            var myFiles = await _observationService.GetObservationFilebyObservationFileId(id);
            return Ok(myFiles);
        }
        /// <summary>
        /// Created By - Sonali Shinde
        /// Created Date - 14 Jan 2020
        /// Description - to get file as Archived Observation teacher using teacher id
        /// </summary>
        /// <param name="pageNumber"></param>          
        /// <param name="pageSize"></param>  
        /// <param name="teacherId"></param>  
        /// <param name="searchString"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetArchivedObservationTeacherPaging")]
        [ProducesResponseType(typeof(IEnumerable<Observation>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetArchivedObservationTeacherPaging(int pageNumber, int pageSize, long teacherId, string searchString)
        {
            var myFiles = await _observationService.GetArchivedObservationTeacherPaging(pageNumber, pageSize, teacherId, searchString);
            return Ok(myFiles);
        }

        /// <summary>
        /// Created By - Sonali Shinde
        /// Created Date - 16 Jan 2020
        /// Description - To get  Observations by teacher Id.
        /// </summary>
        /// <param name="teacherId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetObservationsByUserId")]
        [ProducesResponseType(typeof(IEnumerable<Observation>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetObservationsByUserId(int teacherId)
        {
            var assignmentList = await _observationService.GetObservationByUserID(teacherId);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - sonali shinde
        /// Created Date - 16 Jan 2020
        /// Description - To update active observation.
        /// </summary>
        /// <param name="observationId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateActiveObservation")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateActiveObservation(long observationId)
        {
            var result = _observationService.UpdateActiveObservation(observationId);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Rohan P.
        /// Created Date - 09 Sept 2019
        /// Description - To get school groups associated with Observation
        /// </summary>
        /// <param name="observationId"></param>        
        /// <returns>courseList</returns>
        [HttpGet]
        [Route("GetSchoolCoursesByObservation")]
        [ProducesResponseType(typeof(IEnumerable<Subject>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolCoursesByObservation(int observationId)
        {
            var schoolCourseList = await _observationService.GetSchoolCoursesByObservation(observationId);
            return Ok(schoolCourseList);
        }
    }
}