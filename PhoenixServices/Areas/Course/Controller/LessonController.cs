﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Models;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.Course.Controller
{

    [Route("api/v1/[controller]")]
    [ApiController]
    public class LessonController : ControllerBase
    {
        private readonly ILessonService _ILessonService;
        public LessonController(ILessonService ILessonService)
        {
            _ILessonService = ILessonService;
        }
        [HttpGet]
        [Route("GetUnitDetails")]
        [ProducesResponseType(typeof(IEnumerable<UnitDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitDetails()
        {
            var Unit = await _ILessonService.GetUnitDetails();
            return Ok(Unit);
        }
        [HttpGet]
        [Route("GetStandardDetails")]
        [ProducesResponseType(typeof(IEnumerable<StandardDetailsValue>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStandardDetails()
        {
            var Standard = await _ILessonService.GetStandardDetails();
            return Ok(Standard);
        }

        [HttpGet]
        [Route("GetLessonDetails")]
        [ProducesResponseType(typeof(IEnumerable<Lesson>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetLessonDetails()
        {
            var Lesson = await _ILessonService.GetLessonDetails();
            return Ok(Lesson);
        }
        [HttpGet]
        [Route("GetTopicLessonDetail")]
        [ProducesResponseType(typeof(IEnumerable<LessonCourse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTopicLessonDetail(long CourseId)
        {
            var Lesson = await _ILessonService.GetTopicLessonDetail(CourseId);
            return Ok(Lesson);
        }
        [HttpGet]
        [Route("GetUnitBasedonCourse")]
        [ProducesResponseType(typeof(IEnumerable<UnitDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitBasedonCourse(long CourseId)
        {
            var Lesson = await _ILessonService.GetUnitBasedonCourse(CourseId);
            return Ok(Lesson);
        }
        

        [HttpPost]
        [Route("AddEditLesson")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]

        public async Task<IActionResult> AddEditLesson(Lesson model)
        {
            var result = _ILessonService.AddEditLesson(model);
            return Ok(result);

        }
        //[HttpPost]
        //[Route("AddEditLesson")]
        //[ProducesResponseType((int)HttpStatusCode.Created)]
        //public async Task<IActionResult> AddEditLesson(Lesson model)
        //{
        //    var Unit = await _ILessonService.AddEditLesson(model);
        //    return Ok(Unit);
        //}
        #region School Unit Detail type Mapping
        [HttpPost]
        [Route("SchoolUnitDetailsTypeCD")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SchoolUnitDetailsTypeCD(SchoolUnitDetailTypeMapping typeMapping)
        {
            var result = await _ILessonService.SchoolUnitDetailsTypeCD(typeMapping);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetSchoolUnitDetailTypes")]
        [ProducesResponseType(typeof(IEnumerable<SchoolUnitDetailTypeMapping>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolUnitDetailTypes(long schoolId, int divisionId)
        {
            var result = await _ILessonService.GetSchoolUnitDetailTypes(schoolId,divisionId);
            return Ok(result);
        }
        [HttpPost]
        [Route("GetSchoolUnitDetailType")]
        [ProducesResponseType(typeof(IEnumerable<SchoolUnitDetailType>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolUnitDetailType(SchoolUnitDetailType schoolUnit)
        {
            var result = await _ILessonService.GetSchoolUnitDetailType(schoolUnit);
            return Ok(result);
        }
        [HttpPost]
        [Route("SaveSchoolUnitDetailType")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveSchoolUnitDetailType(SchoolUnitDetailType schoolUnit)
        {
            var result = await _ILessonService.SaveSchoolUnitDetailType(schoolUnit);
            return Ok(result);
        }
        #endregion
    }
}