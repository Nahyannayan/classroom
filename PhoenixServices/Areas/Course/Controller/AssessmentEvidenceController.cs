﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Web;
using Phoenix.Common.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;

namespace Phoenix.API.Areas.Course.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AssessmentEvidenceController : ControllerBase
    {

        private readonly IAssessmentEvidenceService _assessmentEvidenceService;

        public AssessmentEvidenceController(IAssessmentEvidenceService assessmentEvidenceService)
        {
            _assessmentEvidenceService = assessmentEvidenceService;
        }

      
        [HttpPost]
        [Route("SaveUpdateAssessmentEvidenceDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> SaveUpdateAssessmentEvidenceDetails([FromBody] AssessmentEvidence assessmentEvidence,long UserId)
        {
            var result = await _assessmentEvidenceService.SaveUpdateAssessmentEvidenceDetails(assessmentEvidence,UserId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAssessmentEvidenceList")]
        [ProducesResponseType(typeof(AssessmentEvidence), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAssessmentEvidenceList(long UnitId)
        {
            var result = await _assessmentEvidenceService.GetAssessmentEvidenceList(UnitId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAssessmentEvidenceDetailsById")]
        [ProducesResponseType(typeof(AssessmentEvidence), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAssessmentEvidenceDetailsById(long AssessmentId)
        {
            var result = await _assessmentEvidenceService.GetAssessmentEvidenceDetailsById(AssessmentId);
            return Ok(result);
        }

        

    }
}