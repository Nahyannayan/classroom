﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.Course.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UnitController : ControllerBase
    {
        private readonly IUnitService _iUnitService;

        public UnitController(IUnitService iUnitService)
        {
            _iUnitService = iUnitService;
        }
        [HttpPost]
        [Route("AddEditUnitMaster")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]

        public async Task<IActionResult> AddEditUnitMaster(Unit objUnitMaster, TransactionModes mode, string createdBy)
        {
            var result = _iUnitService.AddEditUnitMaster(objUnitMaster, mode, createdBy);
            return Ok(result);

        }

        [HttpGet]
        [Route("GetUnitMasterDetails")]
        [ProducesResponseType(typeof(IEnumerable<Unit>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitMasterDetails()
        {
            var objUnitmasterList = await _iUnitService.GetUnitMasterDetails();
            return Ok(objUnitmasterList);
        }


        [HttpGet]
        [Route("GetUnitMasterDetailsByCourseId")]
        [ProducesResponseType(typeof(IEnumerable<Unit>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitMasterDetailsByCourseId(long courseId)
        {
            var objUnitmasterList = await _iUnitService.GetUnitMasterDetailsByCourseId(courseId);
            return Ok(objUnitmasterList);
        }


        [HttpGet]
        [Route("GetUnitMasterDetailsByUnitId")]
        [ProducesResponseType(typeof(Unit), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitMasterDetailsByUnitId(long Id)
        {
            var objUnitmasterList = await _iUnitService.GetUnitMasterDetailsByUnitId(Id);
            return Ok(objUnitmasterList);
        }

        [HttpGet]
        [Route("GetUnitDetailsType")]
        [ProducesResponseType(typeof(UnitDetailsType), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitDetailsType(long unitId)
        {
            var objUnitDetailsTypeList = await _iUnitService.GetUnitDetailsType(unitId);
            return Ok(objUnitDetailsTypeList);
        }

        [HttpPost]
        [Route("AddEditUnitDetailsType")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]

        public async Task<IActionResult> AddEditUnitDetailsType(List<UnitDetailsType> objlstUnitType,long schoolId, string standardIds,string assessmentEvidenceIds, string assessmentLearningIds,string lessonIdsToDelete, long CreatedBy)
        {
            var result = await _iUnitService.AddEditUnitDetailsType(objlstUnitType, schoolId, standardIds, assessmentEvidenceIds, assessmentLearningIds, lessonIdsToDelete, CreatedBy);
            return Ok(result);

        }
        [HttpGet]
        [Route("GetUnitTopicList")]
        [ProducesResponseType(typeof(UnitTopic), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitTopicList(long MainSyllabusId)
        {
            var result = await _iUnitService.GetUnitTopicList(MainSyllabusId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetUnitTopicStandardDetails")]
        [ProducesResponseType(typeof(UnitTopicStandardDetails), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitTopicStandardDetails(long UnitMasterId,long GroupId,long UnitId,long StandardBankId,int PageNumber,string SearchString="", long SchoolId=0)
        {
            var result = await _iUnitService.GetUnitTopicStandardDetails(UnitMasterId, GroupId,  UnitId,  StandardBankId,PageNumber, SearchString, SchoolId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetUnitWeekList")]
        [ProducesResponseType(typeof(UnitWeek), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitWeekList(long SchoolId,short languageId=1)
        {
            var objUnitWeekList = await _iUnitService.GetUnitWeekList(SchoolId, languageId);
            return Ok(objUnitWeekList);
        }

        [HttpGet]
        [Route("GetUnitCalendarByCourseId")]
        [ProducesResponseType(typeof(UnitCalendar), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitCalendar(long SchoolId, long courseId)
        {
            var objUnitCalendar = await _iUnitService.GetUnitCalendarByCourseId(SchoolId, courseId);
            return Ok(objUnitCalendar);
        }
        [HttpGet]
        [Route("GetStandardDetailsById")]
        [ProducesResponseType(typeof(TopicTreeView), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStandardDetailsById(long unitId)
        {
            var objStandardDetails = await _iUnitService.GetStandardDetailsById(unitId);
            return Ok(objStandardDetails);
        }

        [HttpGet]
        [Route("DeleteUnitStandardDetailsById")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteUnitStandardDetailsById(long scmId)
        {
            OperationDetails op = new OperationDetails();
            var result = _iUnitService.DeleteUnitStandardDetailsById(scmId);
            op.Success = result ? true : false;
            return Ok(op);
        }
        [HttpGet]
        [Route("GetCourseUnitMasterByCourseId")]
        [ProducesResponseType(typeof(IEnumerable<Unit>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseUnitMasterByCourseId(long schoolId,long courseId)
        {
            var objUnitmasterList = await _iUnitService.GetCourseUnitMasterByCourseId(schoolId,courseId);
            return Ok(objUnitmasterList);
        }
        [HttpGet]
        [Route("GetUnitDetailsByCourseId")]
        [ProducesResponseType(typeof(IEnumerable<UnitDetailsType>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitDetailsByCourseId(long courseId)
        {
            var objUnitDetailsTypeList = await _iUnitService.GetUnitDetailsByCourseId(courseId);
            return Ok(objUnitDetailsTypeList);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// CreatedOn :28-June-2020
        /// Descr: To get unit master detail by group Id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUnitMasterByGroupId")]
        [ProducesResponseType(typeof(IEnumerable<Unit>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitMasterByGroupId(long groupId)
        {
            var result = await _iUnitService.GetUnitMasterDetailsByGroupId(groupId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetCourseDetailsAndUnitList")]
        [ProducesResponseType(typeof(IEnumerable<Unit>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseDetailsAndUnitList(long courseId, long teacherId)
        {
            var objUnitmasterList = await _iUnitService.GetCourseDetailsAndUnitList(courseId, teacherId);
            return Ok(objUnitmasterList);
        }

        [HttpGet]
        [Route("GetUnitsByGroup")]
        [ProducesResponseType(typeof(IEnumerable<Unit>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitsByGroup(long Id)
        {
            var objUnitmasterList = await _iUnitService.GetUnitsByGroup(Id);
            return Ok(objUnitmasterList);
        }

        [HttpGet]
        [Route("GetSubUnitsById")]
        [ProducesResponseType(typeof(IEnumerable<Unit>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubUnitsById(long Id)
        {
            var objUnitmasterList = await _iUnitService.GetSubUnitsById(Id);
            return Ok(objUnitmasterList);
        }
        [HttpGet]
        [Route("GetAttachmentDetailByUnitId")]
        [ProducesResponseType(typeof(IEnumerable<Attachment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAttachmentDetailByUnitId(long UnitId)
        {
            var result = await _iUnitService.GetAttachmentDetailByUnitId(UnitId);
            return Ok(result);
        }
    }
}