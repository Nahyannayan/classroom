﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Helpers;
using Phoenix.Models;

namespace Phoenix.API.Areas.Course.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CourseCatalogueController : ControllerBase
    {
        private readonly ICourseCatalogueService courseCatalogueService;

        public CourseCatalogueController(ICourseCatalogueService courseCatalogueService)
        {
            this.courseCatalogueService = courseCatalogueService;
        }

        [HttpGet]
        [Route("GetCatalogueInfoUsingGroupId")]
        [ProducesResponseType(typeof(GroupCatalogue), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCatalogueInfoUsingGroupId(long groupId)
        {
            var result = await courseCatalogueService.GetCatalogueInfoUsingGroupId(groupId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetCourseCatalogueInformationByTeacher")]
        [ProducesResponseType(typeof(IEnumerable<Phoenix.Models.CourseCatalogue>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseCatalogueInformationByTeacher(long memberId, bool isStudent)
        {
            var result = await courseCatalogueService.GetCourseCatalogueInformationByTeacher(memberId, isStudent);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetCatalogueUnits")]
        [ProducesResponseType(typeof(IEnumerable<CatalogueUnits>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCatalogueUnits(long courseId, long memberId, bool isStudent)
        {
            var result = await courseCatalogueService.GetCatalogueUnits(courseId, memberId, isStudent);
            return Ok(result);
        }
    }
}