﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.Course.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AttachmentController : ControllerBase
    {
        private readonly IAttachmentService attachmentService;

        public AttachmentController(IAttachmentService attachmentService)
        {
            this.attachmentService = attachmentService;
        }

        [HttpGet]
        [Route("DeleteAttachment")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteAttachment(long id)
        {
            var modelList = await attachmentService.DeleteAttachment(id);
            return Ok(modelList);
        }

        [HttpGet]
        [Route("GetAttachment")]
        [ProducesResponseType(typeof(Attachment), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAttachment(long masterId = 0, string masterKey = "", long? attachmentId = null)
        {
            var result = await attachmentService.GetAttachment(masterId, masterKey, attachmentId);
            return Ok(result);
        }
    }
}