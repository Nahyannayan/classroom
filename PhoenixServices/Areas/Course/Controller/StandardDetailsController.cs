﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Web;
using Phoenix.Common.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;

namespace Phoenix.API.Areas.Course.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class StandardDetailsController : ControllerBase
    {

        private readonly IStandardDetailsService _standardDetailsService;

        public StandardDetailsController(IStandardDetailsService standardDetailsService)
        {
            _standardDetailsService = standardDetailsService;
        }

        [HttpGet]
        [Route("GetStandardDetailsById")]
        [ProducesResponseType(typeof(Standard), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStandardDetailsById(Int64? StandardDetailsID)
        {
            var standardModel = await _standardDetailsService.GetStandardDetailsById(StandardDetailsID);
            return Ok(standardModel);
        }
        
        [HttpPost]
        [Route("SaveUpdateStandardDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> SaveUpdateStandardDetails([FromBody] StandardDetails model)
        {
            var result = _standardDetailsService.SaveUpdateStandardDetails(model);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStandardDetailsList")]
        [ProducesResponseType(typeof(IEnumerable<StandardDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStandardDetailsList()
        {
            var standardModel = await _standardDetailsService.GetStandardDetailsList();
            return Ok(standardModel);
        }

        [HttpPost]
        [Route("DeleteStandardDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteStandardDetails([FromBody]StandardDetails standard)
        {
            var result = _standardDetailsService.DeleteStandardDetails(standard);
            return Ok(result);
        }

    }
}