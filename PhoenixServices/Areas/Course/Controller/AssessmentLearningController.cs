﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Web;
using Phoenix.Common.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;

namespace Phoenix.API.Areas.Course.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AssessmentLearningController : ControllerBase
    {

        private readonly IAssessmentLearningService _assessmentLearningService;

        public AssessmentLearningController(IAssessmentLearningService assessmentLearningService)
        {
            _assessmentLearningService = assessmentLearningService;
        }

      
        [HttpPost]
        [Route("SaveUpdateAssessmentLearningDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> SaveUpdateAssessmentLearningDetails([FromBody] AssessmentLearning assessmentLearning,long UserId)
        {
            var result = await _assessmentLearningService.SaveUpdateAssessmentLearningDetails(assessmentLearning,UserId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAssessmentLearningList")]
        [ProducesResponseType(typeof(AssessmentLearning), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAssessmentLearningList(long UnitId)
        {
            var result = await _assessmentLearningService.GetAssessmentLearningList(UnitId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAssessmentLearningDetailsById")]
        [ProducesResponseType(typeof(AssessmentLearning), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAssessmentLearningDetailsById(long AssessmentId)
        {
            var result = await _assessmentLearningService.GetAssessmentLearningDetailsById(AssessmentId);
            return Ok(result);
        }

        

    }
}