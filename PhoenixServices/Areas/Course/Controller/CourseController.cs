﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Helpers;
using Phoenix.Models;

namespace Phoenix.API.Areas.Course.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly ICourseService courseService;
        private readonly ISelectListService selectListService;
        private readonly ISchoolService schoolService;

        public CourseController(ICourseService courseService, ISelectListService selectListService, ISchoolService schoolService)
        {
            this.courseService = courseService;
            this.selectListService = selectListService;
            this.schoolService = schoolService;
        }

        [HttpPost]
        [Route("CourseCU")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CourseCU(Phoenix.Models.Course course)
        {
            var result = await courseService.CourseCU(course);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetCourses")]
        [ProducesResponseType(typeof(IEnumerable<Phoenix.Models.Course>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourses(long curriculumId, long schoolId, int PageSize, int PageNumber, string SearchString)
        {
            var result = await courseService.GetCourses(curriculumId, schoolId, PageSize, PageNumber, SearchString);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetCourseDetails")]
        [ProducesResponseType(typeof(Phoenix.Models.Course), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseDetails(long courseId)
        {
            var result = await courseService.GetCourseDetails(courseId);
            return Ok(result);
        }
        [HttpGet]
        [Route("IsCourseTitleUnique")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> IsCourseTitleUnique(string title)
        {
            var result = await courseService.IsCourseTitleUnique(title);
            return Ok(result);
        }

        [HttpPost]
        [Route("CourseMappingCUD")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CourseMappingCUD(CourseMapping courseMapping)
        {
            var result = await courseService.CourseMappingCUD(courseMapping);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetCourseMappings")]
        [ProducesResponseType(typeof(IEnumerable<CourseMapping>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseMappings(long schoolId, long courseId, int pageNum = 1, int pageSize = 6, string searchString = "")
        {
            var result = await courseService.GetCourseMappings(schoolId, courseId, pageNum, pageSize, searchString);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetCourseMappingDetail")]
        [ProducesResponseType(typeof(CourseMapping), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseMappingDetail(long groupId)
        {
            var result = await courseService.GetCourseMappingDetail(groupId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetGradeListBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<SchoolWiseGrade>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradeListBySchoolId(string SchoolIds, long TeacherId)
        {
            var result = await courseService.GetGradeListBySchoolId(SchoolIds, TeacherId);
            return Ok(result);
        }
        [HttpPost]
        [Route("SaveCrossSchoolPermission")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveCrossSchoolPermission(CrossSchoolPermission crossSchoolPermission)
        {
            var result = await courseService.SaveCrossSchoolPermission(crossSchoolPermission);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetCrossSchoolPermissionList")]
        [ProducesResponseType(typeof(IEnumerable<CrossSchoolPermission>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCrossSchoolPermissionList(long TeacherId, long SchoolId,long TCS_ID = 0)
        {
            var result = await courseService.GetCrossSchoolPermissionList(TeacherId, SchoolId, TCS_ID);
            return Ok(result);
        }
        [HttpPost]
        [Route("GetSchoolListByTeacherId")]
        [ProducesResponseType(typeof(IEnumerable<SchoolList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolListByTeacherId(SchoolWiseGrade grade)
        {
            var result = await courseService.GetSchoolListByTeacherId(grade.TeacherId);
            return Ok(result);
        }

        [HttpPost]
        [Route("GeteGradeListPassByschoolIds")]
        [ProducesResponseType(typeof(IEnumerable<GroupTeacher>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolTeachersBySchoolId(SchoolWiseGrade grade)
        {
            var result = await courseService.GetSchoolTeachersBySchoolId(grade.SchoolId, grade.TeacherId);
            return Ok(result);
        }

        [HttpGet]
        [Route("IsGroupNameUnique")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> IsGroupNameUnique(string title)
        {
            var result = await courseService.IsGroupNameUnique(title);
            return Ok(result);
        }

    }
}