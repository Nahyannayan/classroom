﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Web;
using Phoenix.Common.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;

namespace Phoenix.API.Areas.Course.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class StandardController : ControllerBase
    {

        private readonly IStandardService _standardService;

        public StandardController(IStandardService standardService)
        {
            _standardService = standardService;

        }

        [HttpGet]
        [Route("GetStandardMasterDetailsById")]
        [ProducesResponseType(typeof(Standard), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStandardMasterDetailsById(Int64? StandardID)
        {
            var standardModel = await _standardService.GetStandardMasterDetailsById(StandardID);
            return Ok(standardModel);
        }

        [HttpPost]
        [Route("SaveUpdateStandardMasterDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> SaveUpdateStandardMasterDetails([FromBody] Standard standard)
        {
            var result =  _standardService.SaveUpdateStandardMasterDetails(standard);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStandardMasterList")]
        [ProducesResponseType(typeof(IEnumerable<Standard>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStandardMasterList(int Acd_Id)
        {
            var standardModel = await _standardService.GetStandardMasterList(Acd_Id);
            return Ok(standardModel);
        }
        [HttpGet]
        [Route("GetParentList")]
        [ProducesResponseType(typeof(IEnumerable<ParentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetParentList(Int64? SId)
        {
            var standardModel = await _standardService.GetParentList(SId);
            return Ok(standardModel);
        }

       
        [HttpGet]
        [Route("MainSyllabusIdList")]
        [ProducesResponseType(typeof(IEnumerable<MainSyllabusIdList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> MainSyllabusIdList()
        {
            var standardModel = await _standardService.MainSyllabusIdList();
            return Ok(standardModel);
        }
        
        [HttpPost]
        [Route("DeleteStandardMaster")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteStandardMaster([FromBody]Standard standard)
        {
            var result = _standardService.DeleteStandardMaster(standard);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStandardGroupName")]
        [ProducesResponseType(typeof(IEnumerable<Standard>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStandardGroupName(long UnitId,long SchoolId)
        {
            var standardModel = await _standardService.GetStandardGroupName(UnitId,SchoolId);
            return Ok(standardModel);
        }


        [HttpGet]
        [Route("GetSchoolTerm")]
        [ProducesResponseType(typeof(IEnumerable<SchoolTermList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolTerm(int acdid)
        {
            var standardModel = await _standardService.GetSchoolTerm(acdid);
            return Ok(standardModel);
        }

        [HttpPost]
        [Route("BulkStandardUpload")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> BulkStandardUpload(StandardUploadModel standardUploadModel)
        {
            var result = await _standardService.BulkStandardUpload(standardUploadModel);
            return Ok(result);
        }

        [HttpPost]
        [Route("BulkStandardBankUpload")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> BulkStandardBankUpload(StandardBankUploadModel standardUploadModel)
        {
            var result = await _standardService.BulkStandardBankUpload(standardUploadModel);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStandardBankList")]
        [ProducesResponseType(typeof(IEnumerable<StandardBankUploadModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStandardBankList(long SchoolId,long SB_Id=0)
        {
            var result = await _standardService.GetStandardBankList(SchoolId,SB_Id);
            return Ok(result);
        }


        [HttpPost]
        [Route("AddEditStandardBank")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> AddEditStandardBank(string TransMode, string EditType,StandardBankExcelModel model)
        {
            var result = await _standardService.AddEditStandardBank(TransMode,  EditType,model);
            return Ok(result);

        }

    }
}