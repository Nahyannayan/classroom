﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services.Setting;
using Phoenix.API.Services.Setting.Contract;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.Setting.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SettingController : ControllerBase
    {
        private ISettingService _iSettingService;
        public SettingController(ISettingService _SettingService)
        {
            _iSettingService = _SettingService;
        }


        /// <summary>
        /// Author: Shankar Kadam
        /// Added On: 19/10/2020
        /// Description: Get Enable/Disable Notification List For Parent
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetEnableDisableNotificationListForParent")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetEnableDisableNotificationListForParent(long parentId)
        {
            var result = await _iSettingService.GetEnableDisableNotificationListForParent(parentId);
            return Ok(result);
        }

        [HttpPost]
        [Route("SaveNotificationSettingForParent")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public  ActionResult SaveNotificationSettingForParent([FromQuery]long parentId,[FromBody] NotificationSettings model)
        {
            var result =  _iSettingService.SaveNotificationSettingForParent(parentId, model);
            return Ok(result);
        }
    }
}
