﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.StudentInformation.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class StudentListController : ControllerBase
    {
        private readonly IStudentListService _studentListService;

        public StudentListController(IStudentListService studentListService)
        {
            _studentListService = studentListService;
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 22 Apr 2019
        /// Description - To get all student by schoolid
        /// </summary>     
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentsInGroup/{groupId:int}")]
        [ProducesResponseType(typeof(IEnumerable<Student>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudensInGroup(int groupId)
        {
            var studentList = await _studentListService.GetStudentsInGroup(groupId);
            return Ok(studentList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 22 Apr 2019
        /// Description - To get all student by schoolid
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentsNotInGroup/{schoolId:int}/{groupId:int}")]
        [ProducesResponseType(typeof(IEnumerable<Student>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentsNotInGroup(int schoolId, int groupId)
        {
            var categoryList = await _studentListService.GetStudentsNotInGroup(schoolId, groupId);
            return Ok(categoryList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 20 June 2019
        /// Description - To get all student by schoolid
        /// </summary>
        /// <param name="groupIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetStudentsInSelectedGroups")]
        [ProducesResponseType(typeof(IEnumerable<Student>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentsInSelectedGroups([FromBody] string groupIds)
        {
            var studentList = await _studentListService.GetStudentsInSelectedGroups(groupIds);
            return Ok(studentList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 20 June 2019
        /// Description - To get all courses for students
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetStudentCourses")]
        [ProducesResponseType(typeof(IEnumerable<Phoenix.Models.Course>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentCourses([FromBody] long studentId)
        {
            var studentList = await _studentListService.GetStudentCourses(studentId);
            return Ok(studentList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 20 June 2019
        /// Description - To get all student by schoolid
        /// </summary>
        /// <param name="studentIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetStudentDetailsByIds")]
        [ProducesResponseType(typeof(IEnumerable<Student>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentDetailsByIds([FromBody] string studentIds)
        {
            var studentList = await _studentListService.GetStudentDetailsByIds(studentIds);
            return Ok(studentList);
        }


        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 30 APR 2020
        /// Description - To get student details by studentId
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentDetailsByStudentId")]
        [ProducesResponseType(typeof(Student), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentDetailsByStudentId(int studentId)
        {
            var student = await _studentListService.GetStudentDetailsByStudentId(studentId);
            return Ok(student);
        }

        /// <summary>
        /// Created By - Ashwin Dubey
        /// Created Date - 27-May-2020
        /// Description - To get student list by gradeIds
        /// </summary>
        /// <param name="gradeIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentByGradeIds")]
        [ProducesResponseType(typeof(IEnumerable<StudentWithSection>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentByGradeIds(string gradeIds)
        {
            var student = await _studentListService.GetStudentByGradeIds(gradeIds);
            return Ok(student);
        }

        /// <summary>
        /// Created By -    Ashwin Dubey
        /// Created Date -  04-April-2020
        /// Description -   To get student list by gradeIds
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentByGradeSection")]
        [ProducesResponseType(typeof(IEnumerable<StudentWithSection>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentByGradeSection(long gradeId, long sectionId)
        {
            var student = await _studentListService.GetStudentByGradeSection(gradeId, sectionId);
            return Ok(student);
        }
        /// <summary>
        /// Created By - Rohan p
        /// Created Date - 19 July 2020
        /// Description - To get all student by schoolid paginate
        /// </summary>
        /// <param name="studentIds"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetStudentDetailsByIdsPaginate")]
        [ProducesResponseType(typeof(IEnumerable<Student>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentDetailsByIdsPaginate([FromBody] string studentIds, int page, int size)
        {
            var studentList = await _studentListService.GetStudentDetailsByIdsPaginate(studentIds, page, size);
            return Ok(studentList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 22 July 2020
        /// Description - gets parent details of multiple students
        /// </summary>
        /// <param name="studentIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetParentsByStudentIds")]
        [ProducesResponseType(typeof(IEnumerable<UserEmailAccountView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetParentsByStudentIds(string studentIds)
        {
            var result = await _studentListService.GetParentsByStudentIds(studentIds);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 12 Nov 2020
        /// Description - To get all users of a group
        /// </summary>     
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentsWithStaffInGroup/{groupId:int}")]
        [ProducesResponseType(typeof(IEnumerable<Student>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentsWithStaffInGroup(int groupId)
        {
            var studentList = await _studentListService.GetStudentsWithStaffInGroup(groupId);
            return Ok(studentList);
        }
    }
}