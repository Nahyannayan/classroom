﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.StudentInformation.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class StudentProfileSectionsController : Controller
    {
        private readonly IStudentService _studentService;

        public StudentProfileSectionsController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        /// <summary>
        /// Created By - Rahul Verma
        /// Created Date - 7th July 2020
        /// Description - To get different profile sections for a student
        /// </summary>     
        /// <param name="studentId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentsProfileSections")]
        [ProducesResponseType(typeof(IEnumerable<StudentProfileSections>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentsProfileSections(int studentId,short languageId=1)
        {
            var studentList = await _studentService.GetStudentPortfolioInformation(studentId, languageId);
            return Ok(studentList.StudentProfileSections);
        }
    }
}