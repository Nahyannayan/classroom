﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.StudentInformation.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class StudentCertificatesController : ControllerBase
    {
        private readonly IStudentCertificateService _studentCertificateService;
        public StudentCertificatesController(IStudentCertificateService studentCertificateService)
        {
            _studentCertificateService = studentCertificateService;
        }

        /// <summary>
        /// Created By Rohit Patil - 28th Jan 2020
        /// Return all student certificates like Academic and extra curricular.
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        [Route("GetAllStudentCertificates/{id:long}")]
        [ProducesResponseType(typeof(IEnumerable<StudentCertificate>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllStudentCertificates(long id)
        {
            var studentList = await _studentCertificateService.GetAllStudentCertificates(id);
            return Ok(studentList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 29 January 2020
        /// Description - Insert new student certficates.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertCertificateFile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> InsertCertificateFile([FromBody] CertificateFile model)
        {
            bool result = await _studentCertificateService.InsertCertificateFile(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 29 January 2020
        /// Description - Delete or update active status of student certificate
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateDeleteUserCertificate")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> UpdateDeleteUserCertificate([FromBody]StudentCertificate model, [FromQuery]char mode)
        {
            bool result = await _studentCertificateService.UpdateDeleteUserCertificate(model, mode);
            return Ok(result);

        }

     
        /// <summary>
        /// Created By Sonali Shinde - 28th Jan 2020
        /// Return all student certificates .
        /// </summary>
      
        [HttpGet]
        [Route("GetStudentCertificates")]
        [ProducesResponseType(typeof(IEnumerable<Certificate>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentCertificates(long userId, int isTeacher)
        {
            var studentList = await _studentCertificateService.GetStudentCertificates(userId, isTeacher);
            return Ok(studentList);
        }

        [HttpGet]
        [Route("GetStudentCertificateByID")]
        [ProducesResponseType(typeof(Certificate), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult> GetStudentCertificateByID(long Id)
        {
            var studentList = await _studentCertificateService.GetStudentCertificatesById(Id);
            return Ok(studentList);
        }

        /// <summary>
        /// Created By - sonali Shinde 
        /// Created Date - 10 Feb 2020
        /// Description - To Update School certificate.
        /// </summary>
        /// <param name="certificate"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertCertificate")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult>InsertCertificate([FromBody]Certificate certificate)
        {
            var result = _studentCertificateService.InsertCertificate(certificate);
            return Ok(result);
        }
        /// <summary>
        /// Created By - sonali shinde
        /// Created Date - 15 Feb 2020
        /// Description - To Update certificate
        /// </summary>
        /// <param name="schoolSkillSet"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateCertificate")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateCertificate([FromBody]Certificate schoolSkillSet)
        {
            var result = _studentCertificateService.UpdateCertificate(schoolSkillSet);
            return Ok(result);
        }
        /// <summary>
        /// Created By - sonali shinde
        /// Created Date - 15 Feb 2020
        /// Description - To Update certificate
        /// </summary>
        /// <param name="schoolSkillSet"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ApproveCertificate")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> ApproveCertificate([FromBody]Certificate schoolSkillSet)
        {
            var result = _studentCertificateService.UpdateCertificate(schoolSkillSet);
            return Ok(result);
        }
    }
}