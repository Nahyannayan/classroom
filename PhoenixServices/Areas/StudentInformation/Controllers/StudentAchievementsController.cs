﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.StudentInformation.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class StudentAchievementsController : ControllerBase
    {
        private readonly IStudentAchievementService _studentAcheivementService;

        public StudentAchievementsController(IStudentAchievementService studentAcheivementService)
        {
            _studentAcheivementService = studentAcheivementService;
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 6th Feb 2020
        /// Description - Return student merits and achievements
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentAchievements")]
        [ProducesResponseType(typeof(IEnumerable<StudentIncident>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAchievements([FromQuery] long userId, [FromQuery] int pageIndex, [FromQuery] int pageSize, [FromQuery] string searchString, [FromQuery] short type)
        {
            IEnumerable<StudentIncident> studentIncidents = await _studentAcheivementService.GetStudentAchievements(userId, pageIndex, pageSize, searchString, type);
            return Ok(studentIncidents);
        }

        /// <summary>
        /// Created By - Rahul Verma
        /// Created Date - 21st July 2020
        /// Description - Return students achievements with files ( or certificates)
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentAchievementsWithFiles")]
        [ProducesResponseType(typeof(IEnumerable<StudentIncident>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAchievementsWithFiles([FromQuery] long userId, [FromQuery] int pageIndex, [FromQuery] int pageSize, [FromQuery] string searchString, [FromQuery] long currentUserId)
        {
            IEnumerable<StudentAchievement> studentIncidents = await _studentAcheivementService.GetStudentAchievementsWithFiles(userId, pageIndex, pageSize, searchString, currentUserId);
            return Ok(studentIncidents);
        }

        /// <summary>
        /// Created By - Rahul Verma
        /// Created Date - 21st July 2020
        /// Description - Insert student achievements with files
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertAchievement")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> InsertAchievement([FromBody] StudentAchievement model)
        {
            bool result = await _studentAcheivementService.InsertAchievement(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rahul Verma
        /// Created Date - 21st July 2020
        /// Description - Delete user achievements
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("DeleteAchievement")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteAchievement([FromBody] StudentAchievement model)
        {
            bool result = await _studentAcheivementService.DeleteAchievement(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rahul Verma
        /// Created Date - 21st July 2020
        /// Description - Update respective achievements
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAchievements")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAchievements(List<StudentAchievement> model)
        {
            bool result = await _studentAcheivementService.UpdateAchievements(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rahul Verma
        /// Created Date - 21st July 2020
        /// Description - Toggle Checkbox to show/hide respective achievement on portfolio
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAchievementsPortfolioStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAchievementsPortfolioStatus(List<StudentAchievement> model)
        {
            bool result = await _studentAcheivementService.UpdateAchievementsPortfolioStatus(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 6th Jan 2020
        /// Description - Toggle show on dashboard status of merits and acheivements
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAcheivementDashboardStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAcheivementDashboardStatus(StudentIncident model)
        {
            bool result = await _studentAcheivementService.UpdateAcheivementDashboardStatus(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 31st July 2020
        /// Desciption- Deletes student acheivement file
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteAcheivementFile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteAcheivementFile(AchievementFiles model)
        {
            var result = await _studentAcheivementService.SaveStudentAcheivementFile(model, TransactionModes.Delete);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 31st July 2020
        /// Desciption- Insert student acheivement file
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertAchievementFile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> InsertAchievementFile(AchievementFiles model)
        {
            var result = await _studentAcheivementService.SaveStudentAcheivementFile(model, TransactionModes.Insert);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStudentAcheivementById")]
        [ProducesResponseType(typeof(StudentAchievement), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAcheivementById(long acheivementId)
        {
            var result = await _studentAcheivementService.GetStudentAcheivementById(acheivementId);
            return Ok(result);
        }

        [HttpPost]
        [Route("DeleteAcheivement")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteAcheivement(StudentAchievement model)
        {
            var result = await _studentAcheivementService.DeleteAcheivement(model);
            return Ok(result);
        }


        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 11 Aug 2020
        /// Desciprion :- approve/reject student acheivement
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAcheivementApprovalStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAcheivementApprovalStatus(StudentAchievement model)
        {
            var result = await _studentAcheivementService.UpdateAcheivementApprovalStatus(model);
            return Ok(result);
        }
    }
}