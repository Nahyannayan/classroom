﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.StudentInformation.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class StudentAcademicController : ControllerBase
    {
        #region Private variables
        private readonly IStudentAcademicService _studentAcademicService;
        #endregion

        public StudentAcademicController(IStudentAcademicService studentAcademicService)
        {
            _studentAcademicService = studentAcademicService;
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 30th Jan 2020
        /// Description - Returns Studenrt Academic assignment and other files like dashboard video.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        [Route("GetStudentAcademicData/{id:long}")]
        [ProducesResponseType(typeof(StudentAcademic), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAcademicData(long id)
        {
            var studentAcademicData = new StudentAcademic();
            studentAcademicData = await _studentAcademicService.GetStudentAcademicData(id);
            return Ok(studentAcademicData);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 30th January 2020
        /// Description- Update Show on dashboard status of assignments from portfolio academics page.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAssignmentStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAssignmentStatus([FromBody] AssignmentStudentDetails model)
        {
            bool result = await _studentAcademicService.UpdateAssignmentStatus(model);
            return Ok(result);
        }

        /// <summary>
        /// Created by - Rahul Verma
        /// Created Date - 30 July 2020
        /// Description- Update Show on dashboard status of all selected assignments from portfolio edit academics page.
        /// </summary>
        /// <param name="lstModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAllAssignmentStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAllAssignmentStatus([FromBody] List<AssignmentStudentDetails> lstModel)
        {
            bool result = await _studentAcademicService.UpdateAllAssignmentStatus(lstModel);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 30th Jan 2020
        /// Description - Insert student academic file videos/others
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertAcademicFile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> InsertAcademicFile([FromBody] File model, [FromQuery] long id)
        {
            bool result = await _studentAcademicService.InsertAcademicFile(model, id);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 1 Feb 2020
        /// Description - Delete user academic file
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("DeleteAcademicFile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteAcademicFile([FromBody] AcademicDocuments document)
        {
            bool result = await _studentAcademicService.DeleteAcademicFile(document);
            return Ok(result);
        }


        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 12 Feb 2020
        /// Description - Show hide academic file dashboard files
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAcademicFileDashboardStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAcademicFileDashboardStatus([FromBody] AcademicDocuments model)
        {
            bool result = await _studentAcademicService.UpdateAcademicFileDashboardStatus(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 3rd August 2020
        /// Description - update show on dashboard of student assessment report
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAssessmentReportStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAssessmentReportStatus(List<StudentAssessmentReportView> lst)
        {
            var result = await _studentAcademicService.UpdateAssessmentReportStatus(lst);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date  - 3rd Aug 2020
        /// Description - deletes student portfolio assignment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteAcademicAssignment")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteAcademicAssignment(AssignmentStudentDetails model)
        {
            var result = await _studentAcademicService.DeleteAcademicAssignment(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 04 Aug 2020
        /// Description - get academic assignments
        /// </summary>
        /// <param name="assignmentId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentAcademicDetailById")]
        [ProducesResponseType(typeof(AcademicDocuments), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAcademicDetailById(int assignmentId, long userId)
        {
            var result = await _studentAcademicService.GetStudentAcademicDetailById(assignmentId, userId);
            return Ok(result);
        }

        /// <summary>
        /// CREATED BY - Rohit Patil
        /// Created Date - 19th Nov 2020
        /// Description - get student saved assignment in SDP academic 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="searchString"></param>
        /// <returns></returns>
        [HttpGet("GetStudentAcademicAssignments")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentFile>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetStudentAcademicAssignments(long userId, string searchString = "")
        {
            var result = await _studentAcademicService.GetStudentAcademicAssignments(userId, searchString);
            return Ok(result);
        }


    }
}