﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.Users.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ChatterPermissionController : ControllerBase
    {
        private readonly IChatterPermissionService _chatterPermissionService;
        public ChatterPermissionController(IChatterPermissionService chatterPermissionService)
        {
            _chatterPermissionService = chatterPermissionService;
        }

        [HttpGet]
        [Route("getUserPermissions/{schoolId:int}/{userId:int}")]
        public async Task<IActionResult> GetUserPermissions(int schoolId, int userId)
        {
            var modelList = await _chatterPermissionService.GetUserPermissions(schoolId,userId);
            return Ok(modelList);
        }
        [HttpPost]
        [Route("UpdateChatterPermission")]
        public async Task<IActionResult> UpdateChatterPermission([FromBody] ChatPermission chats)
        {
            var model = await _chatterPermissionService.UpdateChatterPermission(chats);
            return Ok(model);
        }
        [HttpGet]
        [Route("GetUsersInGroup/{groupId:int}")]
        public async Task<IActionResult> GetUsersInGroup(int groupId)
        {
            var userList = await _chatterPermissionService.GetUsersInGroup(groupId);
            return Ok(userList);
        }
    }
}
