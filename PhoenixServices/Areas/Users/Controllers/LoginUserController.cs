﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Web;
using Phoenix.Common.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace Phoenix.API.Areas.Users.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LoginUserController : ControllerBase
    {
        private readonly ILogInUserService _logInUserService;
       
        public LoginUserController(ILogInUserService logInUserService)
        {
            _logInUserService = logInUserService;
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 05/May/2019
        /// Description: To get login user details by username
        /// </summary>
        /// <param name="userName">username</param>
        /// <param name="ipDetails">ipDetails</param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        [Route("getloginuserbyusername")]
        [ProducesResponseType(typeof(IEnumerable<LogInUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        public async Task<IActionResult> GetUserRoles(string userName,string ipDetails)
        {
            var decryptUserName = EncryptDecryptHelper.DecryptUrl(userName);
            var result = await _logInUserService.GetLoginUserByUserName(decryptUserName, ipDetails);

            return Ok(result);
        }


        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 12/December/2019
        /// Description: To get login user details by username: Pass username in base64 encrypted string
        /// </summary>
        /// <param name="userName">username</param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        [Route("getloginuserbyencryptedusername")]
        [ProducesResponseType(typeof(IEnumerable<LogInUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        public async Task<IActionResult> GetLoginUser(string userName)
        {
            var decodedUserName = EncryptDecryptHelper.DecodeBase64(userName);
            var result = await _logInUserService.GetLoginUserByUserName(decodedUserName,"");

            return Ok(result);
        }

        /// <summary>
        /// CreatedBy: Rohit Patil
        /// CreatedOn: 05/May/2019
        /// Description: To get all user details filtered by school
        /// </summary>
        /// <param name="schoolId">schoolId</param>
        /// <param name="languageId">languageId</param>
        /// <returns></returns>
        [HttpGet,Authorize]
        [Route("getuserlist")]
        [ProducesResponseType(typeof(IEnumerable<LogInUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserList(int schoolId,short languageId=1)
        {           
            var result = await _logInUserService.GetUserList(schoolId, languageId);
            return Ok(result);
        }

        /// <summary>
        /// CreatedBy: Rohan patil
        /// CreatedOn: 17/Mar/20202
        /// Description: To get login user details by username and password
        /// </summary>
        /// <param name="userName">username</param>
        /// <param name="password">password</param>
        /// <param name="ipDetails">ipDetails</param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        [Route("getloginuserbyusernamepassword")]
        [ProducesResponseType(typeof(IEnumerable<LogInUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        public async Task<IActionResult> GetLoginUserByUserNamePassword(string userName,string password,string ipDetails)
        {
            var decryptUserName = EncryptDecryptHelper.DecryptUrl(userName);
            var decryptPassword = EncryptDecryptHelper.DecryptUrl(password);
            var result = await _logInUserService.GetLoginUserByUserNamePassword(decryptUserName,decryptPassword, ipDetails);

            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Date: 22-June-2020
        /// Desc: To validate token
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("gettokenvalidation")]
        [ProducesResponseType(typeof(bool),(int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        public async Task<IActionResult> TokenValidation()
        {
            var result = User.Identity.IsAuthenticated;
            return Ok(result);
        }

    }
}