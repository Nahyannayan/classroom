﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.Models.HSEObjects;

namespace Phoenix.API.Areas.Users.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _UserService;

        public UsersController(IUserService UserService)
        {
            _UserService = UserService;
        }

        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 16-MAY-2019
        /// Description : To fetch all the users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getallusers")]
        [ProducesResponseType(typeof(IEnumerable<User>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllUsersAsync()
        {
            var users = await _UserService.GetAllUsers();
            return Ok(users);
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 18-JUNE-2019
        /// Description : To fetch all the user feelings
        /// </summary>
        /// <param name="SystemLanguageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getalluserfeelings")]
        [ProducesResponseType(typeof(IEnumerable<UserFeelingView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllUserFeelingsAsync(int SystemLanguageId)
        {
            var userFeelings = await _UserService.GetUserFeelings(SystemLanguageId);
            return Ok(userFeelings);
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 23-JUNE-2019
        /// Description : To fetch all the user avatar
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getprofileavatars")]
        [ProducesResponseType(typeof(IEnumerable<UserProfileView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetProfileAvatars()
        {
            var profileAvatars = await _UserService.GetProfileAvatars();
            return Ok(profileAvatars);
        }

        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 13-June-2019
        /// Description :  To fetch user by role and location allowed to access
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getusersbyrolelocation")]
        [ProducesResponseType(typeof(IEnumerable<User>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUsersByRoleLocationsAsync(string roles, int businessUnitId = 0)
        {
            var users = await _UserService.GetUsersByRolesLocatonAllowed(roles, businessUnitId);
            return Ok(users);
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 18-JUNE-2019
        /// Description : To fetch Update user feelings
        /// </summary>
        /// <param name="userFeelingView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateuserfeeling")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateUserFeeling([FromBody] UserFeelingView userFeelingView)
        {
            var result = _UserService.UpdateUserFeeling(userFeelingView);
            return Ok(result);
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 24-JUNE-2019
        /// Description : To UpdateUserProfile
        /// </summary>
        /// <param name="userFeelingView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateuserprofile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateUserProfile([FromBody] UserProfileView userProfileView)
        {
            var result = _UserService.UpdateUserProfile(userProfileView);
            return Ok(result);
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// Created Date : 16-OCT-2019
        /// Description : Insert notofication log
        /// </summary>
        /// <param name="notificationType"></param>
        /// <param name="sourceId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("pushnotificationlogs")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> PushNotificationLogs(string notificationType, int sourceId, long userId)
        {
            var result = _UserService.PushNotificationLogs(notificationType, sourceId, userId);
            return Ok(result);
        }

        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 16-MAY-2019
        /// Description : To fetch all users filtered by school id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userTypeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getusersbyschool")]
        [ProducesResponseType(typeof(IEnumerable<User>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUsersBySchool(int? id, int userTypeId)
        {
            var users = await _UserService.GetUserBySchool(id, userTypeId);
            return Ok(users);
        }

        /// <summary>
        /// Author : Girish Sonawane 
        /// Created Date : 15/10/2019
        /// Description : To get user notifications
        /// </summary>
        /// <param name="userTypeId"></param>
        /// <param name="userId"></param>
        /// <param name="loginUserId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getusernotifications")]
        [ProducesResponseType(typeof(IEnumerable<UserNotificationView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserNotifications(int? userTypeId, long userId, long loginUserId)
        {
            var notifications = await _UserService.GetUserNotifications(userTypeId, userId, loginUserId);
            return Ok(notifications);
        }

        /// <summary>
        /// Author : Girish Sonawane 
        /// Created Date : 15/10/2019
        /// Description : To get all notifications
        /// </summary>
        /// <param name="userTypeId"></param>
        /// <param name="userId"></param>
        /// <param name="loginUserId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getallnotifications")]
        [ProducesResponseType(typeof(IEnumerable<UserNotificationView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllNotifications(int? userTypeId, long userId, long loginUserId)
        {
            var notifications = await _UserService.GetAllNotifications(userTypeId, userId, loginUserId);
            return Ok(notifications);
        }

        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 16-MAY-2019
        /// Description : To fetch all user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getuserbyid")]
        [ProducesResponseType(typeof(User), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserById(int id)
        {
            var user = await _UserService.GetUserById(id);
            return Ok(user);
        }

        /// <summary>
        /// Author : Athar Shaikh
        /// Created Date : 19-MAY-2019
        /// Description : To search users by name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="typId"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("searchuserbyname")]
        [ProducesResponseType(typeof(IEnumerable<User>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SearchUserByName(string name, int typId = 0, int schoolId = 0)
        {
            var user = await _UserService.SearchUserByName(name, typId, schoolId);
            return Ok(user);
        }
        /// <summary>
        /// Author : Mahesh Chikhale
        /// Created Date : 21th Oct 2019
        /// Description : To fetch Update user feelings
        /// </summary>
        /// <param name="errorLogger"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("saveErrorLogger")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> saveErrorLogger([FromBody] ErrorLogger errorLogger)
        {
            var result = await _UserService.saveErrorLogger(errorLogger);
            return Ok(result);
        }


        /// <summary>
        /// Author : vinayak y 
        /// Created Date : 15-JAN-2020
        /// Description : To fetch users by school id and user type without studentnumber
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userTypeId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getUsersBySchoolAndType")]
        [ProducesResponseType(typeof(IEnumerable<User>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUsersBySchoolAndType(int? id, int userTypeId, short languageId = 1)
        {
            var users = await _UserService.GetUsersBySchoolAndType(id, userTypeId, languageId);
            return Ok(users);
        }

        /// <summary>
        /// Author : Mukund Patil 
        /// Created Date : 08-AUG-2020
        /// Description : To fetch users by school id and user type with paging
        /// </summary>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="pageSize">pageSize</param>
        /// <param name="schoolId">schoolId</param>
        /// <param name="userTypeId">UserTypeId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getUsersBySchoolAndTypeWithPaging")]
        [ProducesResponseType(typeof(IEnumerable<User>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUsersBySchoolAndTypeWithPaging(int pageNumber, int pageSize, int? schoolId, int userTypeId)
        {
            var users = await _UserService.GetUsersBySchoolAndTypeWithPaging(pageNumber, pageSize, schoolId, userTypeId);
            return Ok(users);
        }


        /// <summary>
        /// Author : Mahesh Chikhale
        /// Created Date : 21-Oct-2019
        /// Description : To get DB process details after error logged by user
        /// </summary>

        /// <returns></returns>
        [HttpGet]
        [Route("GetDBLogdetails")]
        [ProducesResponseType(typeof(IEnumerable<User>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDBLogDetails()
        {
            var users = await _UserService.GetDBLogdetails();
            return Ok(users);
        }
        [HttpGet]
        [Route("GetUserLog")]
        [ProducesResponseType(typeof(IEnumerable<User>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserLog(int? schoolId, DateTime startDate, DateTime endDate, string loginType)
        {
            var users = await _UserService.GetUserLog(schoolId, startDate, endDate, loginType);
            return Ok(users);
        }

        /// <summary>
        /// Author : sonali Shinde
        /// Created Date : 21-Feb-2020
        /// Description : To get  error logs
        /// </summary>

        /// <returns></returns>
        [HttpGet]
        [Route("GetErrorLogs")]
        [ProducesResponseType(typeof(IEnumerable<UserErrorLogs>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetErrorLogs(int? schoolId, DateTime startDate, DateTime endDate, string loginType)
        {
            var usersLogs = await _UserService.GetErrorLogs(schoolId, startDate, endDate, loginType);
            return Ok(usersLogs);
        }
        /// <summary>
        /// Author : sonali Shinde
        /// Created Date : 21-Feb-2020
        /// Description : To get  error log files
        /// </summary>
        /// <param name="ErrorLogId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetErrorLogfiles")]
        [ProducesResponseType(typeof(IEnumerable<ErrorFiles>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetErrorLogFiles(long ErrorLogId)
        {
            var usersLogs = await _UserService.GetErrorLogFiles(ErrorLogId);
            return Ok(usersLogs);
        }
        /// <summary>
        /// Author : Shankar Kadam
        /// Created Date : 08-July-2020
        /// Description : Get Teachers List By SchoolId to share lesson plan document with other 
        /// </summary>
        /// <param name="schoolId "></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTeachersListBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<User>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTeachersListBySchoolId(long schoolId)
        {
            var usersLogs = await _UserService.GetTeachersListBySchoolId(schoolId);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (usersLogs != null && usersLogs.Count() > 0)
                {
                    usersLogs = usersLogs.Select(a =>
                    {
                        a.UserImageFilePath = Helpers.CommonHelper.GemsStudentImagePath(a.UserImageFilePath); return a;
                    });
                }
            }
            return Ok(usersLogs);
        }

        /// <summary>
        /// Author : Tejalben
        /// Created Date : 27-July-2020
        /// Description : Add log details 
        /// </summary>
        /// <param name="logDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertLogDetails")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> InsertLogDetails([FromBody] LogDetails logDetails)
        {
            var result = await _UserService.InsertLogDetails(logDetails);
            return Ok(result);
        }

        /// <summary>
        /// CREATED BY - Rohit Patil
        /// Created Date - 12 Sept 2020
        /// Description - save user feedback
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveUserFeedback")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveUserFeedback(UserFeedback model)
        {
            var result = await _UserService.SaveUserFeedback(model);
            return Ok(result);
        }
    }
}
