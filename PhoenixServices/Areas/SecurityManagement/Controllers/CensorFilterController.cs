﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.SecurityManagement.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class CensorFilterController : ControllerBase
    {
        private readonly ICensorFilterService _censorFilterService;

        public CensorFilterController(ICensorFilterService censorFilterService)
        {
            _censorFilterService = censorFilterService;
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 1 May 2019
        /// Description - To get all censor users filtered by school.
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getcensorusers")]
        [ProducesResponseType(typeof(IEnumerable<CensorUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCensorUsers(int schoolId)
        {
            var userList = await _censorFilterService.GetCensorUsers(schoolId);
            return Ok(userList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 1 May 2019
        /// Description - To get all banned words filtered by school.
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getbannedwords")]
        [ProducesResponseType(typeof(IEnumerable<BannedWord>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBannedWords(int schoolId)
        {
            var wordList = await _censorFilterService.GetBannedWords(schoolId);
            return Ok(wordList);
        }
        

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 1 May 2019
        /// Description - To insert banned word
        /// </summary>
        /// <param name="BannedWord"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertBannedWord")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertBannedWord([FromBody]BannedWord BannedWord)
        {
            var result = _censorFilterService.InsertBannedWord(BannedWord);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 1 May 2019
        /// Description - To update banned word.
        /// </summary>
        /// <param name="BannedWord"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateBannedWord")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateBannedWord([FromBody]BannedWord BannedWord)
        {
            var result = _censorFilterService.UpdateBannedWord(BannedWord);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 1 May 2019
        /// Description - To delete banned word.
        /// </summary>
        /// <param name="entity">BannedWord </param>
        /// <returns></returns>
        [HttpPost]
        [Route("deleteBannedWord")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteBannedWord(BannedWord entity)
        {
            var result = _censorFilterService.DeleteBannedWord(entity);
            return Ok(result);
        }
    }
}