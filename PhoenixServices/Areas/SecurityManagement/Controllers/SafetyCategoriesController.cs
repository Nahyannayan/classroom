﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Phoenix.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Microsoft.AspNetCore.Authorization;

namespace Phoenix.API.Areas.SecurityManagement.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SafetyCategoriesController : ControllerBase
    {
        private readonly ISafetyCategoriesService _safetyCategoriesService;

        public SafetyCategoriesController(ISafetyCategoriesService safetyCategoriesService)
        {
            _safetyCategoriesService = safetyCategoriesService;
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 18 Apr 2019
        /// Description - To get all safety categories filtered by school.
        /// </summary>
        /// <param name="languageId"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsafetycategories")]      
        [ProducesResponseType(typeof(IEnumerable<SafetyCategory>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSafetyCategories(int languageId, int schoolId)
        {            
            var categoryList = await _safetyCategoriesService.GetSafetyCategories(languageId, schoolId);
            return Ok(categoryList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19 Apr 2019
        /// Description - To get safety category by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsafetycategorybyid")]
        [ProducesResponseType(typeof(SafetyCategory), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSafetyCategoryById(int id,short languageId=1)
        {
            var category = await _safetyCategoriesService.GetSafetyCategoryById(id, languageId);
            return Ok(category);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19 Apr 2019
        /// Description - To insert safety category
        /// </summary>
        /// <param name="safetyCategory"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertsafetycategory")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertSafetyCategory([FromBody]SafetyCategory safetyCategory)
        {
            var result = _safetyCategoriesService.InsertSafetyCategory(safetyCategory);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19 Apr 2019
        /// Description - To update safety category.
        /// </summary>
        /// <param name="safetyCategory"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatesafetycategory")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateSafetyCategory([FromBody]SafetyCategory safetyCategory)
        {
            var result = _safetyCategoriesService.UpdateSafetyCategory(safetyCategory);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19 Apr 2019
        /// Description - To delete safety category.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletesafetycategory/{id:int}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteSafetyCategory(int id)
        {
            var result = _safetyCategoriesService.DeleteSafetyCategory(id);
            return Ok(result);
        }
    }
}