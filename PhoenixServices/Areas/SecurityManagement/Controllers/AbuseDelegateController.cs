﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.SecurityManagement.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class AbuseDelegateController : ControllerBase
    {
        private readonly IAbuseDelegateService _abuseDelegateService;
        private readonly IAbuseReportLedgerService abuseReportLedgerService;

        public AbuseDelegateController(IAbuseDelegateService abuseDelegateService,
            IAbuseReportLedgerService abuseReportLedgerService)
        {
            _abuseDelegateService = abuseDelegateService;
            this.abuseReportLedgerService = abuseReportLedgerService;
        }

        #region Abuse delegate
        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To get all abuse delegate 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAbuseDelegates")]
        [ProducesResponseType(typeof(IEnumerable<AbuseDelegate>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAbuseDelegates()
        {
            var result = await _abuseDelegateService.GetAllAbuseDelegateAsync();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To get abuse delegate  by school Id
        /// </summary>
        /// <param name="schoolId">school id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getabusedelegatesbyschoolid")]
        [ProducesResponseType(typeof(IEnumerable<AbuseDelegate>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAbuseDelegateBySchoolId(int schoolId)
        {
            var result = await _abuseDelegateService.GetAbuseDelegateBySchoolId(schoolId);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To get abuse delegate by space Id
        /// </summary>
        /// <param name="id">space id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAbuseDelegatebyid")]
        [ProducesResponseType(typeof(IEnumerable<AbuseDelegate>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAbuseDelegateById(int id)
        {
            var result = await _abuseDelegateService.GetAbuseDelegateById(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To add abuse delegate
        /// </summary>
        /// <param name="AbuseDelegate">abuse delegate entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("addAbuseDelegate")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult AddSchooSpace([FromBody] AbuseDelegate AbuseDelegate)
        {
            _abuseDelegateService.InsertAbuseDelegateAsync(AbuseDelegate);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To update abuse delegate
        /// </summary>
        /// <param name="AbuseDelegate">abuse delegate entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateAbuseDelegate")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult UpdateAbuseDelegate([FromBody] AbuseDelegate AbuseDelegate)
        {
            _abuseDelegateService.UpdateAbuseDelegateAsync(AbuseDelegate);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To delete abuse delegate by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteAbuseDelegate")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteAbuseDelegate(int id)
        {
            _abuseDelegateService.DeleteAbuseDelegateAsync(id);
            return Ok(true);
        }
        #endregion

        #region Abuse contact info
        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To get all abuse contact info 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAbuseContactInfos")]
        [ProducesResponseType(typeof(IEnumerable<AbuseContactInfo>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAbuseContactInfos()
        {
            var result = await _abuseDelegateService.GetAllAbuseContactInfoAsync();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To get abuse contact info  by school Id
        /// </summary>
        /// <param name="schoolId">school id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAbuseContactInfobyschoolid")]
        [ProducesResponseType(typeof(IEnumerable<AbuseContactInfo>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAbuseContactInfosBySchoolId(int schoolId)
        {
            var result = await _abuseDelegateService.GetAbuseContactInfoBySchoolId(schoolId);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To get abuse contact info by space Id
        /// </summary>
        /// <param name="id">space id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAbuseContactInfobyid")]
        [ProducesResponseType(typeof(IEnumerable<AbuseContactInfo>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAbuseContactInfoById(int id)
        {
            var result = await _abuseDelegateService.GetAbuseContactInfoById(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To add abuse contact info
        /// </summary>
        /// <param name="AbuseContactInfo">abuse contact info entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("addAbuseContactInfo")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult AddSchooSpace([FromBody] AbuseContactInfo AbuseContactInfo)
        {
            _abuseDelegateService.InsertAbuseContactInfo(AbuseContactInfo);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To update abuse contact info
        /// </summary>
        /// <param name="AbuseContactInfo">abuse contact info entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateAbuseContactInfo")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult UpdateAbuseContactInfo([FromBody] AbuseContactInfo AbuseContactInfo)
        {
            _abuseDelegateService.UpdateAbuseContactInfo(AbuseContactInfo);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To delete abuse contact info by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deleteAbuseContactInfo")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteAbuseContactInfo(int id)
        {
            _abuseDelegateService.DeleteAbuseContactInfo(id);
            return Ok(true);
        }

        #endregion

        #region Abuse report ledger
        /// <summary>
        /// Created By: Ashwin Dubey
        /// Created On: 14/November/2019
        /// Description: To get all abuse report ledgers 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAbuseReportLedgers")]
        [ProducesResponseType(typeof(IEnumerable<AbuseReportLedger>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAbuseReportLedgers()
        {
            var result = await abuseReportLedgerService.GetAllAbuseReportLedgerAsync();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Ashwin Dubey
        /// Created On: 14/November/2019
        /// Description: To get abuse report ledgers  by school Id
        /// </summary>
        /// <param name="safetyCategoryId">school id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAbuseReportLedgerbysafetycategoryid")]
        [ProducesResponseType(typeof(IEnumerable<AbuseReportLedger>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAbuseReportLedgersBySchoolId(int safetyCategoryId)
        {
            var result = await abuseReportLedgerService.GetAbuseReportLedgerBySafetyCategoryId(safetyCategoryId);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Ashwin Dubey
        /// Created On: 14/November/2019
        /// Description: To get abuse report ledgers by space Id
        /// </summary>
        /// <param name="id">space id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAbuseReportLedgerbyabusereportledgerid")]
        [ProducesResponseType(typeof(IEnumerable<AbuseReportLedger>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAbuseReportLedgerById(int id)
        {
            var result = await abuseReportLedgerService.GetAbuseReportLedgerByAbuseReportLedgerId(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Ashwin Dubey
        /// Created On: 14/November/2019
        /// Description: To add abuse report ledgers
        /// </summary>
        /// <param name="AbuseReportLedger">abuse report ledgers entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("addAbuseReportLedger")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult AddSchooSpace([FromBody] AbuseReportLedger AbuseReportLedger)
        {
            abuseReportLedgerService.InsertAbuseReportLedger(AbuseReportLedger);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Ashwin Dubey
        /// Created On: 14/November/2019
        /// Description: To update abuse report ledgers
        /// </summary>
        /// <param name="AbuseReportLedger">abuse report ledgers entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateAbuseReportLedger")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult UpdateAbuseReportLedger([FromBody] AbuseReportLedger AbuseReportLedger)
        {
            abuseReportLedgerService.UpdateAbuseReportLedger(AbuseReportLedger);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Ashwin Dubey
        /// Created On: 14/November/2019
        /// Description: To delete abuse report ledgers by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteAbuseReportLedger")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> DeleteAbuseReportLedger(int id)
        {
            abuseReportLedgerService.DeleteAbuseReportLedger(id);
            return Ok(true);
        }


        /// <summary>
        /// Created By: Rohit Karayat
        /// Created On: 20/December/2019
        /// Description: To get abuse report  by UserId
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAbuseReportListByUser")]
        [ProducesResponseType(typeof(IEnumerable<AbuseReportLedger>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAbuseReportListByUser(int id)
        {
            var AbuseReportList = await abuseReportLedgerService.GetAbuseReportListAsPerUser(id);
            return Ok(AbuseReportList);
        }

        /// <summary>
        /// CREATED BY - Rohit Patil
        /// CREATED Date - 17 Sept 2020
        /// DESCRIPTION - Send text message to mobile number
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SendTextMessage")]
        [ProducesResponseType(typeof(IEnumerable<bool>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SendTextMessage([FromBody] TextMessageView model)
        {
            var result = true;
            return Ok(result);
        }

        #endregion

        #region Mobile Settings

        /// <summary>
        /// CREATED BY - Rohit Patil
        /// CREATED DATE - 26th OCt 2020
        /// DESCRIPTION - Save abuse delegation mobile settings
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveMobileSettings")]
        [ProducesResponseType(typeof(IEnumerable<bool>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveMobileSettings(AbuseContactInfo model)
        {
            var result = await _abuseDelegateService.SaveMobileSettings(model);
            return Ok(result);
        }
        #endregion
    }
}