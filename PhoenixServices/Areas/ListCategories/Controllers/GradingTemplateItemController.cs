﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.ListCategories.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class GradingTemplateItemController : ControllerBase
    {
        private readonly IGradingTemplateItemService _gradingTemplateItemService;

        public GradingTemplateItemController(IGradingTemplateItemService gradingTemplateItemService)
        {
            _gradingTemplateItemService = gradingTemplateItemService;
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 22 Apr 2019
        /// Description - To get all grading templates
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="SystemLanguageId">SystemLanguageId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGradingTemplateItems")]
        [ProducesResponseType(typeof(IEnumerable<GradingTemplateItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradingTemplateItems(int id, int SystemLanguageId)
        {
            var categoryList = await _gradingTemplateItemService.GetGradingTemplateItems(id, SystemLanguageId);
            return Ok(categoryList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 25 Apr 2019
        /// Description - To get safety category by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="SystemLanguageId">SystemLanguageId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGradingTemplateItemById")]
        [ProducesResponseType(typeof(GradingTemplateItem), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradingTemplateItemById(int id, int SystemLanguageId)
        {
            var category = await _gradingTemplateItemService.GetGradingTemplateItemById(id, SystemLanguageId);
            return Ok(category);
        }



        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 22 Apr 2019
        /// Description - To insert grading template
        /// </summary>
        /// <param name="GradingTemplateItem"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertGradingTemplateItem")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertGradingTemplateItem([FromBody]GradingTemplateItem GradingTemplateItem)
        {
            var result = _gradingTemplateItemService.InsertGradingTemplateItem(GradingTemplateItem);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 22 Apr 2019
        /// Description - To update grading Template
        /// </summary>
        /// <param name="GradingTemplateItem"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateGradingTemplateItem")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateGradingTemplateItem([FromBody]GradingTemplateItem GradingTemplateItem)
        {
            var result = _gradingTemplateItemService.UpdateGradingTemplateItem(GradingTemplateItem);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 22 Apr 2019
        /// Description - To delete grading template.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteGradingTemplateItem/{id:int}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteGradingTemplateItem(int id)
        {
            var result = _gradingTemplateItemService.DeleteGradingTemplateItem(id);
            return Ok(result);
        }
    }

   
}