﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.ListCategories.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class MarkingSchemeController : ControllerBase
    {
        private readonly IMarkingSchemeService _markingSchemeService;

        public MarkingSchemeController(IMarkingSchemeService markingSchemeService)
        {
            _markingSchemeService = markingSchemeService;
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 07 May 2019
        /// Description - To get all marking Schemas
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMarkingSchemas")]
        [ProducesResponseType(typeof(IEnumerable<MarkingScheme>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMarkingSchemas(int schoolId,short languageId=1)
        {
            var markingSchemes = await _markingSchemeService.GetMarkingSchemes(schoolId, languageId);
            return Ok(markingSchemes);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 07 may 2019
        /// Description - To get marking scheme by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMarkingSchemeById")]
        [ProducesResponseType(typeof(MarkingScheme), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMarkingSchemeById(int id, short languageId = 1)
        {
            var category = await _markingSchemeService.GetMarkingSchemeById(id, languageId);
            return Ok(category);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 07 May 2019
        /// Description - To insert marking scheme
        /// </summary>
        /// <param name="markingScheme">markingScheme</param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertMarkingScheme")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertMarkingScheme([FromBody]MarkingScheme markingScheme)
        {
            var result = _markingSchemeService.InsertMarkingScheme(markingScheme);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date -  07 May 2019
        /// Description - To update marking Scheme
        /// </summary>
        /// <param name="markingScheme"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateMarkingScheme")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateMarkingScheme([FromBody]MarkingScheme markingScheme)
        {
            var result = _markingSchemeService.UpdateMarkingScheme(markingScheme);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 07 May 2019
        /// Description - To deletemarking scheme.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteMarkingScheme/{id:int}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteMarkingScheme(int id)
        {
            var result = _markingSchemeService.DeleteMarkingScheme(id);
            return Ok(result > 0);
        }

    }
}