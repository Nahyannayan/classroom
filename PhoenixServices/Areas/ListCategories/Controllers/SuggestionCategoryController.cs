﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.ListCategories.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SuggestionCategoryController : ControllerBase
    {
        private readonly ISuggestionCategoryService _suggestionCategoryService;
        public SuggestionCategoryController(ISuggestionCategoryService suggestionCategoryService)
        {
            _suggestionCategoryService = suggestionCategoryService;
        }

        /// <summary>
        /// Created By - Rohit Karayat
        /// Created Date - 12 Dec 2019
        /// Description - To Get all Suggestion Category filtered by school.
        /// </summary>
        /// <param name="languageId"></param>
        /// <param name="SchoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsuggestioncategories")]
        [ProducesResponseType(typeof(IEnumerable<SuggestionCategory>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSuggestionCategory(int languageId,int SchoolId)
        {
            var category = await _suggestionCategoryService.GetSuggestionCategories(languageId, SchoolId);
            return Ok(category);
        }

        /// <summary>
        /// Created By - Rohit Karayat
        /// Created Date - 12 Dec 2019
        /// Description - To Get  Suggestion Category by Id.
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsuggestioncategorybyid")]
        [ProducesResponseType(typeof(SuggestionCategory), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSuggestionCategoryById(int Id,short languageId)
        {
            var category = await _suggestionCategoryService.GetSuggestionCategoryById(Id, languageId);
            return Ok(category);
        }

        /// <summary>
        /// Created By - Rohit Karayat
        /// Created Date - 12 Dec 2019
        /// Description - To insert Suggestion Category.
        /// </summary>
        /// <param name="suggestionCategory"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertsuggestioncategory")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertSuggestionCategory([FromBody]SuggestionCategory suggestionCategory)
        {
            var result =  _suggestionCategoryService.InsertSuggestionCategory(suggestionCategory);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Karayat
        /// Created Date - 12 Dec 2019
        /// Description - To Update Suggestion Category.
        /// </summary>
        /// <param name="suggestionCategory"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateSuggestionCategory")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateSuggestionCategory([FromBody]SuggestionCategory suggestionCategory)
        {
            var result = _suggestionCategoryService.UpdateSuggestionCategory(suggestionCategory);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Karayat
        /// Created Date - 12 Dec 2019
        /// Description - To Delete Suggestion Category by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteSuggestionCategory/{id:int}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteSuggestionCategory(int id)
        {
            var result = _suggestionCategoryService.DeleteSuggestionCategory(id);
            return Ok(result);
        }
    }
}