﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Microsoft.AspNetCore.Http;
using Phoenix.Common.Helpers;
using System.Web;

namespace Phoenix.API.Areas.ListCategories.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class StudentSuggestionController : Controller
    {
        private readonly IStudentSuggestionService _studentSuggestionService;
        public StudentSuggestionController(IStudentSuggestionService studentSuggestionService)
        {
            _studentSuggestionService = studentSuggestionService;
        }

        /// <summary>
        ///  Description - To get all Student Suggestion list.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getstudentsuggestion")]
        [ProducesResponseType(typeof(IEnumerable<Suggestion>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentSuggestion()
        {
            var StudentList = await _studentSuggestionService.GetStudentSuggestions();
            return Ok(StudentList);
        }

        /// <summary>
        /// Created By: Vinayak Y
        /// Created On: 16/Dec/2019
        /// Description: Add DeleteSudentSuggestion by id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentSuggestionsByUserId")]
        [ProducesResponseType(typeof(IEnumerable<Suggestion>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentSuggestionsByUserId(int id)
        {
            var Suggestion = await _studentSuggestionService.GetStudentSuggestionsByUserId(id);
            return Ok(Suggestion);
        }

        /// <summary>
        /// Created By: Vinayak Y
        /// Created On: 16/Dec/2019
        /// Description: Add DeleteSudentSuggestion by id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentSuggestionById")]
        [ProducesResponseType(typeof(Suggestion), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentSuggestionById(int id)
        {
            var Suggestion = await _studentSuggestionService.GetStudentSuggestionById(id);
            return Ok(Suggestion);
        }

        /// <summary>
        /// Created By: Mukund Patil
        /// Created On: 22-08-2020
        /// Description: To get the student suggestion by Id and userId
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentSuggestionByIdAndUserId")]
        [ProducesResponseType(typeof(Suggestion), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentSuggestionByIdAndUserId(int id, long userId)
        {
            var Suggestion = await _studentSuggestionService.GetStudentSuggestionByIdAndUserId(id, userId);
            return Ok(Suggestion);
        }

        /// <summary>
        /// Created By - Vinayak Yenpure
        /// Created Date - 09 July 2020
        /// Description - To get paginate suggestions for student
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchString"></param>
        /// <param name="sortBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("paginateStudentSuggestionByUserId")]
        [ProducesResponseType(typeof(IEnumerable<Suggestion>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> PaginateStudentSuggestionByUserId(int userId, int pageNumber, int pageSize, string searchString = "", string sortBy="")
        {
            var SuggestionList = await _studentSuggestionService.PaginateStudentSuggestionByUserId(userId, pageNumber, pageSize, searchString, sortBy);
            return Ok(SuggestionList);
        }

        /// <summary>
        /// Created By - Vinayak Yenpure
        /// Created Date - 09 July 2020
        /// Description - To get paginate suggestions for teacher or admin
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchString"></param>
        /// <param name="sortBy"></param>
        /// <param name="langaugeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("paginateStudentSuggestionBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<Suggestion>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> PaginateStudentSuggestionBySchoolId(int schoolId, int pageNumber, int pageSize, string searchString = "", string sortBy = "", short langaugeId=1)
        {
            var SuggestionList = await _studentSuggestionService.PaginateStudentSuggestionBySchoolId(schoolId, pageNumber, pageSize, searchString, sortBy, langaugeId);
            return Ok(SuggestionList);
        }

        /// <summary>
        /// Created By: Vinayak Y
        /// Created On: 16/Dec/2019
        /// Description: Add DeleteSudentSuggestion by id
        /// </summary>
        /// <param name="studentSuggestion"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertStudentSuggestion")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertSudentSuggestion([FromBody]Suggestion studentSuggestion)
        {
            var result =  _studentSuggestionService.InsertStudentSuggestion(studentSuggestion);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Vinayak Y
        /// Created On: 16/Dec/2019
        /// Description: Add DeleteSudentSuggestion by id
        /// </summary>
        /// <param name="studentSuggestion"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertStudentSuggestionM")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertSudentSuggestionM([FromBody]Suggestion studentSuggestion, IFormFile file)
        {
            var result = _studentSuggestionService.InsertStudentSuggestion(studentSuggestion);
            //SharePointHelper sh = new SharePointHelper();
           // Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint();
            //string SharePTUploadedFilePath = SharePointHelper.UploadFilesAsPerModule(ref cx, SessionHelper.CurrentSession.SchoolCode, FileModulesConstants.MyFiles, model.PostedFile);

            return Ok(result);
        }

        /// <summary>
        /// Created By: Vinayak Y
        /// Created On: 16/Dec/2019
        /// Description: Add DeleteSudentSuggestion by id
        /// </summary>
        /// <param name="studentSuggestion"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateSudentSuggestion")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateSudentSuggestion([FromBody]Suggestion studentSuggestion)
        {
            var result = _studentSuggestionService.UpdateStudentSuggestion(studentSuggestion);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Vinayak Y
        /// Created On: 16/Dec/2019
        /// Description: Add DeleteSudentSuggestion by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteSudentSuggestion")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteSudentSuggestion(int id, long userId)
        {
            var result = _studentSuggestionService.DeleteStudentSuggestion(id, userId);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Vinayak Y
        /// Created On: 16/Dec/2019
        /// Description: Add AddUpdateThinkBoxUser by id
        /// </summary>
        /// <param name="thinkBox"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addupdatethinkboxuser")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddUpdateThinkBoxUser([FromBody]ThinkBox thinkBox)
        {
            var result = _studentSuggestionService.AddUpdateThinkBoxUser(thinkBox);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 16/May/2019
        /// Description: To get abuse delegate  by school Id
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getThinkBoxUsersBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<AbuseDelegate>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetThinkBoxUsersBySchoolId(int Id)
        {
            var result = await _studentSuggestionService.GetThinkBoxUsersBySchoolId(Id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: girish s
        /// Created On: 16/Dec/2019
        /// Description: To delete abuse user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteUserFromThinkBox")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteUserFromThinkBox(long userId, long schoolId)
        {
            _studentSuggestionService.DeleteUserFromThinkBox(userId, schoolId);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Vinayak Y
        /// Created On: 16/Dec/2019
        /// Description: Get GetStudentSuggestionsBySchoolId by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentSuggestionsBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<Suggestion>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentSuggestionsBySchoolId(int id)
        {
            var Suggestion = await _studentSuggestionService.GetStudentSuggestionsBySchoolId(id);
            return Ok(Suggestion);
        }

        /// <summary>
        /// Created By: girish s
        /// Created On: 16/Dec/2019
        /// Description: has ThinkBox UserPermission by user id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("hasThinkBoxUserPermission")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult HasThinkBoxUserPermission(long userId)
        {
            bool result = _studentSuggestionService.HasThinkBoxUserPermission(userId);
            return Ok(result);
        }

    }
}