﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.ListCategories.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SubjectController : ControllerBase
    {
        private readonly ISubjectService _subjectService;

        public SubjectController(ISubjectService SubjectService)
        {
            _subjectService = SubjectService;
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 18 Apr 2019
        /// Description - To get all subjects filtered by school.
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsubjects")]
        [ProducesResponseType(typeof(IEnumerable<Subject>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubjects(int languageId, int schoolId)
        {
            var categoryList = await _subjectService.GetSubjects(languageId,schoolId);
            return Ok(categoryList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 17 May 2019
        /// Description - To get top ten subjects filtered by school.
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("gettoptensubjects")]
        [ProducesResponseType(typeof(IEnumerable<Subject>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTopTenSubjects(int schoolId)
        {
            var categoryList = await _subjectService.GetTopTenSubjects(schoolId);
            return Ok(categoryList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19 Apr 2019
        /// Description - To get safety category by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsubjectbyid/{id:int}")]
        [ProducesResponseType(typeof(Subject), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubjectById(int id)
        {
            var category = await _subjectService.GetSubjectById(id);
            return Ok(category);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19 Apr 2019
        /// Description - To get Lesson Details By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getLessonsByTopicId/{id:int}")]
        [ProducesResponseType(typeof(IEnumerable<Objective>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetLessonsByTopicId(int id)
        {
            var category = await _subjectService.GetLessonsByTopicId(id);
            return Ok(category);
        }

        /// <summary>
        /// Created By- Mohd Ameeq
        /// Created Date - 30 Nov 2020
        /// Description - To get Units based on GroupId
        /// </summary>
        /// <param name="groupIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSelectedUnitByGroupId")]
        [ProducesResponseType(typeof(IEnumerable<GroupUnit>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSelectedUnitByGroupId(string groupIds)
        {
            var category = await _subjectService.GetSelectedUnitByGroupId(groupIds);
            return Ok(category);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19 Apr 2019
        /// Description - To insert safety category
        /// </summary>
        /// <param name="Subject"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertsubject")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertSubject([FromBody]Subject subject)
        {
            var result = _subjectService.InsertSubject(subject);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19 Apr 2019
        /// Description - To update safety category.
        /// </summary>
        /// <param name="subject"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatesubject")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateSubject([FromBody]Subject subject)
        {
            var result = _subjectService.UpdateSubject(subject);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19 Apr 2019
        /// Description - To delete safety category.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletesubject/{id:int}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteSubject(int id)
        {
            var result = _subjectService.DeleteSubject(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 28 July 2019
        /// Description - To get topic subtopic structure
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTopicSubTopicStructure")]
        [ProducesResponseType(typeof(Subject), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTopicSubTopicStructure(int subjectId)
        {
            var structure = await _subjectService.GetTopicSubTopicStructure(subjectId);
            return Ok(structure);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 06 July 2020
        /// Description - To get topic subtopic structure
        /// </summary>
        /// <param name="courseIds"></param>
        /// <param name="schoolGroupIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUnitStructure")]
        [ProducesResponseType(typeof(Subject), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitStructure(string courseIds)
        {
            var structure = await _subjectService.GetUnitStructure(courseIds);
            return Ok(structure);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 28 July 2019
        /// Description - To get topic subtopic structure
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isMainSyllabus"></param>
        /// <param name="subjectId"></param>
        /// <param name="isLesson"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSubtopicObjectives")]
        [ProducesResponseType(typeof(Subject), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubtopicObjectives(int id, bool isMainSyllabus, int subjectId,bool isLesson)
        {
            var objectives = await _subjectService.GetSubtopicsObjective(id,isMainSyllabus, subjectId, isLesson);
            return Ok(objectives);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 28 July 2019
        /// Description - To get objective for assignment
        /// </summary>
        /// <param name="assignmentId"></param>       
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentObjective")]
        [ProducesResponseType(typeof(Subject), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentObjective(int assignmentId)
        {
            var objectives = await _subjectService.GetAssignmentsObjective(assignmentId);
            return Ok(objectives);
        }

        
        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 13 Oct 2020
        /// Description - To get unit for assignment
        /// </summary>
        /// <param name="assignmentId"></param>       
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentUnit")]
        [ProducesResponseType(typeof(Subject), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentUnit(int assignmentId)
        {
            var assignmentUnits = await _subjectService.GetAssignmentUnit(assignmentId);
            return Ok(assignmentUnits);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 02 Dec 2020
        /// Description - To get units for assignment Group Course Topic list
        /// </summary>
        /// <param name="assignmentId"></param>       
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentCourseTopicUnit")]
        [ProducesResponseType(typeof(Subject), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentCourseTopicUnit(int assignmentId)
        {
            var assignmentUnits = await _subjectService.GetAssignmentCourseTopicUnit(assignmentId);
            return Ok(assignmentUnits);
        }



        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 28 July 2019
        /// Description - To get topic subtopic structure
        /// </summary>
        /// <param name="subjectIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTopicSubTopicStructureByMultipleSubjectIds")]
        [ProducesResponseType(typeof(Subject), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTopicSubTopicStructureByMultipleSubjectIds(string subjectIds)
        {
            var structure = await _subjectService.GetTopicSubTopicStructureByMultipleSubjectIds(subjectIds);
            return Ok(structure);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 28 July 2019
        /// Description - To get topic subtopic structure with multiple subjects
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isMainSyllabus"></param>
        /// <param name="subjectIds"></param>
        /// <param name="isLesson"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSubtopicObjectivesWithMultipleSubjects")]
        [ProducesResponseType(typeof(Subject), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubtopicObjectivesWithMultipleSubjects(int id, bool isMainSyllabus, string subjectIds, bool isLesson)
        {
            var objectives = await _subjectService.GetSubtopicObjectivesWithMultipleSubjects(id, isMainSyllabus, subjectIds, isLesson);
            return Ok(objectives);
        }
        
    }
}