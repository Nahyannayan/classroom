﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.ListCategories.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class GradingTemplateController : ControllerBase
    {
        private readonly IGradingTemplateService _gradingTemplateService;

        public GradingTemplateController(IGradingTemplateService gradingTemplateService)
        {
            _gradingTemplateService = gradingTemplateService;
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 22 Apr 2019
        /// Description - To get all grading templates
        /// </summary>
        /// <param name="languageId"></param>
        /// /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGradingTemplates")]
        [ProducesResponseType(typeof(IEnumerable<GradingTemplate>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradingTemplates(int languageId, int schoolId)
        {
            var categoryList = await _gradingTemplateService.GetGradingTemplates(languageId, schoolId);
            return Ok(categoryList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 25 Apr 2019
        /// Description - To get safety category by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGradingTemplateById")]
        [ProducesResponseType(typeof(GradingTemplate), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradingTemplateById(int id,short languageId=1)
        {
            var category = await _gradingTemplateService.GetGradingTemplateById(id, languageId);
            return Ok(category);
        }


        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 22 Apr 2019
        /// Description - To insert grading template
        /// </summary>
        /// <param name="gradingTemplate"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertGradingTemplate")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertGradingTemplate([FromBody]GradingTemplate gradingTemplate)
        {
            var result = _gradingTemplateService.InsertGradingTemplate(gradingTemplate);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 22 Apr 2019
        /// Description - To update grading Template
        /// </summary>
        /// <param name="gradingTemplate"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateGradingTemplate")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateGradingTemplate([FromBody]GradingTemplate gradingTemplate)
        {
            var result = _gradingTemplateService.UpdateGradingTemplate(gradingTemplate);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 22 Apr 2019
        /// Description - To delete grading template.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteGradingTemplate/{id:int}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteGradingTemplate(int id)
        {
            var result = _gradingTemplateService.DeleteGradingTemplate(id);
            return Ok(result);
        }
    }

   
}