﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.API.Models;
namespace Phoenix.API.Areas.AssessmentConfig.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AssessmentConfigController : ControllerBase
    {
        private readonly IAssessmentConfigService _IAssessmentConfigService;
        public AssessmentConfigController(IAssessmentConfigService AssessmentConfigService)
        {
            _IAssessmentConfigService = AssessmentConfigService;
        }

        [HttpPost]
        [Route("AddEditAssessmentConfig")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]

        public async Task<IActionResult> AddEditAssessmentConfig(List<GradeTemplate> obj, string Description, string MasterId, long GTM_Id, long SchoolId)
        {
            var result = _IAssessmentConfigService.AddEditAssessmentConfig(obj, Description, MasterId, GTM_Id, SchoolId);
            return Ok(result);

        }

        [HttpPost]
        [Route("AddEditGradeSlab")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]

        public async Task<IActionResult> AddEditGradeSlab(List<GradeSlab> obj, string Description, string MasterIds, long GradeSlabMasterId, long Acd_Id)
        {
            var result = _IAssessmentConfigService.AddEditGradeSlab(obj, Description, MasterIds, GradeSlabMasterId, Acd_Id);
            return Ok(result);

        }


        [HttpGet]
        [Route("GetAssessmentConfigList")]
        [ProducesResponseType(typeof(IEnumerable<AssessmentConfigList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssessmentConfigList(long Acd_Id)
        {
            var Lesson = await _IAssessmentConfigService.GetAssessmentConfigList(Acd_Id);
            return Ok(Lesson);
        }
        [HttpGet]
        [Route("GetAssessmentConfigMasterList")]
        [ProducesResponseType(typeof(IEnumerable<AssessmentConfigList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssessmentConfigMasterList(long SchoolId)
        {
            var Lesson = await _IAssessmentConfigService.GetAssessmentConfigMasterList(SchoolId);
            return Ok(Lesson);
        }


        
        [HttpGet]
        [Route("GetGradeSlabList")]
        [ProducesResponseType(typeof(IEnumerable<GradeSlab>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradeSlabList(long Acd_Id)
        {
            var Lesson = await _IAssessmentConfigService.GetGradeSlabList(Acd_Id);
            return Ok(Lesson);
        }
        [HttpGet]
        [Route("GetGradeSlabMasterList")]
        [ProducesResponseType(typeof(IEnumerable<GradeSlabMaster>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradeSlabMasterList(long SchoolId)
        {
            var Lesson = await _IAssessmentConfigService.GetGradeSlabMasterList(SchoolId);
            return Ok(Lesson);
        }

    }
}