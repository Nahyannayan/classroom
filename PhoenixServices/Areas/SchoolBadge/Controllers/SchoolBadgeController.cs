﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Web;
using Phoenix.Common.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;

namespace Phoenix.API.Areas.SchoolBadges.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SchoolBadgeController : ControllerBase
    {
        private readonly ISchoolBadgeService _schoolBadgeService;

        public SchoolBadgeController(ISchoolBadgeService schoolBadgeService)
        {
            _schoolBadgeService = schoolBadgeService;
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 08 Jan 2020
        /// Description - To get School Badge By School Id
        /// </summary>
        ///  <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchString"></param>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolBadgesByPage")]
        [ProducesResponseType(typeof(IEnumerable<Assignment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolBadgesByPage(int pageNumber, int pageSize, string searchString, int userId, int schoolId)
        {
            var schoolBadgeList = await _schoolBadgeService.GetSchoolBadgesByPage(pageNumber, pageSize, searchString, userId, schoolId);
            return Ok(schoolBadgeList);
        }


        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 08 Jan 2020
        /// Description - To get School Badge By School Id
        /// </summary>       
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolBadges")]
        [ProducesResponseType(typeof(IEnumerable<SchoolBadge>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolBadges(long userId, int schoolId)
        {
            var schoolBadgeList = await _schoolBadgeService.GetSchoolBadges(userId, schoolId);
            return Ok(schoolBadgeList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 08 Jan 2020
        /// Description - To insert school Badge
        /// </summary>
        /// <param name="schoolBadge"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertSchoolBadge")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertSchoolBadge([FromBody]SchoolBadge schoolBadge)
        {
            var result = _schoolBadgeService.InsertBadgeData(schoolBadge);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 08 Jan 2020
        /// Description - To insert school Badge
        /// </summary>
        /// <param name="schoolBadge"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateSchoolBadge")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateSchoolBadge([FromBody]SchoolBadge schoolBadge)
        {
            var result = _schoolBadgeService.UpdateBadgeData(schoolBadge, TransactionModes.Update);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 08 Jan 2020
        /// Description - To delete school Badge
        /// </summary>
        /// <param name="schoolBadge"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteSchoolBadge")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteSchoolBadge([FromBody]SchoolBadge schoolBadge)
        {
            var result = _schoolBadgeService.UpdateBadgeData(schoolBadge, TransactionModes.Delete);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 10 Jan 2020
        /// Description - To Get Schools Selected for Badge
        /// </summary>
        /// <param name="SchoolBadgeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetBadgeSchoolIds")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetBadgeSchoolIds(Int64 SchoolBadgeId)
        {
            var schoolBadgeList = await _schoolBadgeService.GetBadgeSchoolIds(SchoolBadgeId);
            return Ok(schoolBadgeList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 10 Jan 2020
        /// Description - To get School Badge Details
        /// </summary>       
        /// <param name="userId"></param>  
        /// <param name="schoolBadgeId"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolBadgeDetails")]
        [ProducesResponseType(typeof(SchoolBadge), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolBadgeDetails(long schoolBadgeId, long userId)
        {
            var schoolBadge = await _schoolBadgeService.GetSchoolBadgeDetails(schoolBadgeId, userId);
            return Ok(schoolBadge);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 10 Jan 2020
        /// Description - To get School Badge Details
        /// </summary>       
        /// <param name="userId"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetTopOrderForDisplayBadge")]
        [ProducesResponseType(typeof(SchoolBadge), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTopOrderForDisplayBadge(long userId)
        {
            var order = _schoolBadgeService.GetTopOrderForDisplayBadge(userId);
            return Ok(order);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 10 Jan 2020
        /// Description - To get School Badge Details
        /// </summary>       
        /// <param name="userId"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetUserBadges")]
        [ProducesResponseType(typeof(IEnumerable<SchoolBadge>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserBadges(long userId)
        {
            var schoolBadge = await _schoolBadgeService.GetUserBadges(userId);
            return Ok(schoolBadge);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 9th Feb 2020
        /// Description - assign/deassign badges to students by teacher
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveStudentBadges")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveStudentBadges(SchoolBadge model)
        {
            bool result = await _schoolBadgeService.SaveStudentBadges(model);
            return Ok();
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 27 FEB 2020
        /// Description - To get School Badges by schoolId
        /// </summary>       
        /// <param name="schoolId"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetBadgesBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<SchoolBadge>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBadgesBySchoolId(int schoolId)
        {
            var schoolBadges = await _schoolBadgeService.GetBadgesBySchoolId(schoolId);
            return Ok(schoolBadges);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 29 FEB 2020
        /// Description - save group wise student badges
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="schoolGroupIds">
        /// <param name="badgesIds"/></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveGroupWiseStudentBadges")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveGroupWiseStudentBadges(StudentBadge studentBadge)
        {
            bool result = await _schoolBadgeService.SaveGroupWiseStudentBadges(studentBadge);
            return Ok();
        }
    }
}