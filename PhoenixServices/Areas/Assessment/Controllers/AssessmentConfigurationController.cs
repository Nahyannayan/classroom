﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Models;
using Phoenix.API.Services;

namespace Phoenix.API.Areas.Assessment.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AssessmentConfigurationController : ControllerBase
    {
        private readonly IAssessmentConfigurationService assessmentConfiguration;

        public AssessmentConfigurationController(IAssessmentConfigurationService _assessmentConfiguration)
        {
            this.assessmentConfiguration = _assessmentConfiguration;
        }

        #region Assessment Configuration
        [HttpGet]
        [Route("GetAssessmentColumnDetail")]
        [ProducesResponseType(typeof(IEnumerable<AssessmentColumn>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssessmentColumnDetail(long AssessmentMasterId)
        {
            var result = await assessmentConfiguration.GetAssessmentColumnDetail(AssessmentMasterId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAssessmentConfigDetail")]
        [ProducesResponseType(typeof(IEnumerable<AssessmentConfigSetting>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssessmentConfigDetail(long AssessmentMasterId, long schoolId)
        {
            var result = await assessmentConfiguration.GetAssessmentConfigDetail(AssessmentMasterId, schoolId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetAssessmentConfigPagination")]
        [ProducesResponseType(typeof(IEnumerable<AssessmentConfigSetting>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssessmentConfigPagination(long schoolId, int pageNum, int pageSize, string searchString)
        {
            var result = await assessmentConfiguration.GetAssessmentConfigPagination(schoolId, pageNum, pageSize, searchString);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetCourseListByGrade")]
        [ProducesResponseType(typeof(IEnumerable<Phoenix.Models.Course>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseListByGrade(string SchoolGradeIds)
        {
            var result = await assessmentConfiguration.GetCourseListByGrade(SchoolGradeIds);
            return Ok(result);
        }
        [HttpPost]
        [Route("SaveAssessmentConfigDetail")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveAssessmentConfigDetail(AssessmentConfigSetting assessmentConfig)
        {
            var result = await assessmentConfiguration.SaveAssessmentConfigDetail(assessmentConfig);
            return Ok(result);
        }
       
        #endregion
    }
}