﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.API.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.Assessment.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProgressTrackerController : ControllerBase
    {
        private readonly IProgressTrackerService _iprogressTrackerService;

        public ProgressTrackerController(IProgressTrackerService iprogressTrackerService)
        {
            _iprogressTrackerService = iprogressTrackerService;
        }

        [HttpGet]
        [Route("GetTopicsByCourseId")]
        [ProducesResponseType(typeof(IEnumerable<CourseTopics>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTopicsByCourseId(long Id)
        {
            var result = await _iprogressTrackerService.GetTopicsByCourseId(Id);
            return Ok(result);
        }

        [HttpPost]
        [Route("AddEditProgressSetUP")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]

        public async Task<IActionResult> AddEditProgressSetUP(long teacherId, ProgressTrackerSetup model)
        {
            var result = _iprogressTrackerService.AddEditProgressSetUP(teacherId, model);
            return Ok(result);

        }
        [HttpGet]
        [Route("GetProgressSetup")]
        [ProducesResponseType(typeof(IEnumerable<ProgressTrackerSetup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetProgressSetup(long schoolId)
        {
            var progressSetup = await _iprogressTrackerService.GetProgressSetup(schoolId);
            return Ok(progressSetup);
        }


        [HttpGet]
        [Route("GetCourseGradeDisplay")]
        [ProducesResponseType(typeof(IEnumerable<CourseGradeDisplay>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseGradeDisplay(long schoolId, string courseIds)
        {
            var courseGradeDisplay = await _iprogressTrackerService.GetCourseGradeDisplay(schoolId, courseIds);
            return Ok(courseGradeDisplay);
        }

        [HttpGet]
        [Route("GetProgressSetupDetailsById")]
        [ProducesResponseType(typeof(ProgressTrackerSetup), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetProgressSetupDetailsById(long Id)
        {
            var progressSetupByid = await _iprogressTrackerService.GetProgressSetupDetailsById(Id);
            return Ok(progressSetupByid);
        }

        [HttpGet]
        [Route("GetProgressLessonGrading")]
        [ProducesResponseType(typeof(IEnumerable<GradingTemplateItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetProgressLessonGrading(long courseId, long schoolGroupId, short languageId = 1)
        {
            var GetProgressLessonGrade = await _iprogressTrackerService.GetProgressLessonGrading(courseId, schoolGroupId, languageId);
            return Ok(GetProgressLessonGrade);
        }


        [HttpGet]
        [Route("StudentProgressTracker")]
        [ProducesResponseType(typeof(IEnumerable<AssessmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> StudentProgressTracker(long schoolGroupId)
        {
            var studentProgressTracker = await _iprogressTrackerService.StudentProgressTracker(schoolGroupId);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (studentProgressTracker != null && studentProgressTracker.Count() > 0)
                {
                    studentProgressTracker = studentProgressTracker.Select(a =>
                    {
                        a.StudentImageUrl = Helpers.CommonHelper.GemsStudentImagePath(a.StudentImageUrl); return a;
                    });
                }
            }
            return Ok(studentProgressTracker);
        }



        [HttpGet]
        [Route("GetLessonObjectivesByUnitIds")]
        [ProducesResponseType(typeof(IEnumerable<AssessmetLessons>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetLessonObjectivesByUnitIds(string subSyllabusIds)
        {
            var studentProgressTracker = await _iprogressTrackerService.GetLessonObjectivesByUnitIds(subSyllabusIds);
            return Ok(studentProgressTracker);
        }


        [HttpPost]
        [Route("StudentProgressMapper")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]

        public async Task<IActionResult> StudentProgressMapper(long userId, long groudId, List<AssessmentStudent> studentProgressMapper)
        {
            var result = _iprogressTrackerService.StudentProgressMapper(userId, groudId, studentProgressMapper);
            return Ok(result);

        }


        [HttpPost]
        [Route("SaveProgressTrackerEvidence")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]

        public async Task<IActionResult> SaveProgressTrackerEvidence(List<Attachment> attachments)
        {
            var result = await _iprogressTrackerService.SaveProgressTrackerEvidence(attachments);
            return Ok(result);

        }

        [HttpGet]
        [Route("GetProgressTrackerEvidence")]
        [ProducesResponseType(typeof(IEnumerable<Attachment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetProgressTrackerEvidence(long id, string key)
        {
            var getProgressTrackerEvidence = await _iprogressTrackerService.GetProgressTrackerEvidence(id, key);
            return Ok(getProgressTrackerEvidence);
        }

        [HttpGet]
        [Route("GetObjectiveAssignmentGrading")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentObjectiveGrading>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetObjectiveAssignmentGrading(long lessonId, long studentId)
        {
            var getProgressTrackerEvidence = await _iprogressTrackerService.GetObjectiveAssignmentGrading(lessonId, studentId);
            return Ok(getProgressTrackerEvidence);
        }

        [HttpGet]
        [Route("ValidateProgressSetupCourseGrade")]
        [ProducesResponseType(typeof(IEnumerable<ProgressTrackerSetup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ValidateProgressSetupCourseGrade(string courseIds, string gradeIds)
        {
            var getValidateProgressSetupCourseGrade = await _iprogressTrackerService.ValidateProgressSetupCourseGrade(courseIds, gradeIds);
            return Ok(getValidateProgressSetupCourseGrade);
        }
    }
}