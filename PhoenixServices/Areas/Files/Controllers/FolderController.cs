﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;


namespace Phoenix.API.Areas.Files.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class FoldersController : ControllerBase
    {
        private readonly IFolderService _folderService;

        public FoldersController(IFolderService folderService)
        {
            _folderService = folderService;
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all Folders
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getfolders")]
        [ProducesResponseType(typeof(IEnumerable<Folder>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFolders()
        {
            var result = await _folderService.GetAll();
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all Folder by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfolderbyid/{id:int}")]
        [ProducesResponseType(typeof(Folder), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFolderById(int id)
        {
            var result = await _folderService.GetById(id);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all Folders by schoolId and moduleId
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="moduleId"></param>
        /// <param name="isActive">optional</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfoldersbymoduleId/{schoolId:int}/{moduleId:int}/{isactive:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<Folder>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFoldersByModuleId(int schoolId, int moduleId, bool? isActive = null)
        {
            var result = await _folderService.GetFoldersByModuleId(schoolId, moduleId, isActive);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all Folders by schoolId and moduleId
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="moduleId"></param>
        /// <param name="isActive">optional</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfoldersbysectionId/{sectionId:int}/{moduleId:int}/{isactive:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<Folder>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFoldersBySectionId(int sectionId, int moduleId, bool? isActive = null)
        {
            var result = await _folderService.GetFoldersBySectionId(sectionId, moduleId, isActive);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all Folders by parent folder id
        /// </summary>
        /// <param name="parentFolderId"></param>
        /// <param name="moduleId"></param>
        /// <param name="isActive">optional</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfoldersbyparentfolderid/{parentfolderid:int}/{moduleId:int}/{isactive:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<Folder>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFoldersByParentFolderId(int parentFolderId, int moduleId, bool? isActive = null)
        {
            var result = await _folderService.GetFoldersByParentFolderId(parentFolderId,moduleId, isActive);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all Folders by school id
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfoldersbyschoolid/{schoolid:int}/{isactive:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<Folder>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFoldersBySchoolId(int schoolId, bool? isActive = null)
        {
            var result = await _folderService.GetFoldersBySchoolId(schoolId, isActive);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all Folders by user id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="moduleId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfoldersbyuserid/{userid:int}/{moduleId:int}/{isactive:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<Folder>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFoldersByUserId(int userId,int moduleId, bool? isActive = null)
        {
            var result = await _folderService.GetFoldersByUserId(userId, moduleId, isActive);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To add new folder
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addfolder")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult AddFolder([FromBody]Folder folder)
        {
            var result = _folderService.Insert(folder);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get update folder
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatefolder")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult UpdateFolder([FromBody]Folder folder)
        {
            var result = _folderService.Update(folder);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get delete folder
        /// </summary>
        /// <param name="id"></param> 
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletefolder/{id:long}/{userId:long}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteFolder(long id, long userId)
        {
            var result = _folderService.Delete(id,userId);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 07/July/2019
        /// Description: To get all Folder tree structure
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfoldertree/{folderid:int}")]
        [ProducesResponseType(typeof(IEnumerable<FolderTree>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFolderTree(int folderId)
        {
            var result = await _folderService.GetFolderTree(folderId);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 30/August/2019
        /// Description: To get permission for group for user by userId N groupId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getgrouppermission/{userid:int}/{groupid:int}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult GetGroupPermission(int userId,int groupId)
        {
            var result = _folderService.GetGroupPermission(userId, groupId);
            return Ok(result);
        }
    }
}