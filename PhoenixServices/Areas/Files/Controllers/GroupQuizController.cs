﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Helpers;
using Phoenix.API.Services;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using SelectPdf;
using Phoenix.Common.Helpers.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Phoenix.API.Areas.Files.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class GroupQuizController : ControllerBase
    {
        private readonly IGroupQuizService _groupQuizService;
        private readonly IRazorViewEngine _razorViewEngine;
        private readonly ITempDataProvider _tempDataProvider;
        private readonly IServiceProvider _serviceProvider;

        public GroupQuizController(IGroupQuizService groupQuizService, IRazorViewEngine razorViewEngine,
                ITempDataProvider tempDataProvider,
                IServiceProvider serviceProvider)
        {
            _groupQuizService = groupQuizService;
            _razorViewEngine = razorViewEngine;
            _tempDataProvider = tempDataProvider;
            _serviceProvider = serviceProvider;

        }

        /// <summary>
        /// Description - To add or update Quiz.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("addgroupquiz")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddGroupQuiz([FromBody]GroupQuiz groupQuiz)
        {
            //if (groupQuiz.IsSetTime)
            //{
            //    if (groupQuiz.StartDate != null)
            //    {
            //        string iString = groupQuiz.StartDate.Value.ToString("dd-MM-yyyy") + " " + groupQuiz.StartTime;
            //        DateTime startDateTime = Convert.ToDateTime(iString);
            //        DateTime? testUTC = startDateTime.ConvertLocalToUTC();
            //        groupQuiz.StartDate = testUTC;
            //        groupQuiz.StartTime = Convert.ToDateTime(groupQuiz.StartDate).ToString("h:mm tt");
            //    }
            //}
            var result = _groupQuizService.AddUpdateGroupQuiz(groupQuiz);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Mukund Patil
        /// Created On: 25 FEB 2020
        /// Description: To get  Group Quiz List ByGroupId.
        /// </summary>
        /// <param name="groupId">groupId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGroupQuizListByGroupId")]
        [ProducesResponseType(typeof(IEnumerable<GroupQuiz>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> GetGroupQuizListByGroupId(int groupId)
        {
            var result = await _groupQuizService.GetGroupQuizListByGroupId(groupId);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Mukund Patil
        /// Created On: 25 FEB 2020
        /// Description: To get group quiz list by groupId and date range
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGroupQuizListByGroupIdAndDateRange")]
        [ProducesResponseType(typeof(IEnumerable<GroupQuiz>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> GetGroupQuizListByGroupIdAndDateRange(int groupId, DateTime startDate, DateTime endDate)
        {
            var result = await _groupQuizService.GetGroupQuizListByGroupIdAndDateRange(groupId, startDate, endDate);
            return Ok(result);
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// CreatedOn : 30-10-2019
        /// Insert QUIZ Answer
        /// </summary>
        /// <param name="quizResult">quizResult</param>
        [HttpPost]
        [Route("insertupdatequizanswers")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertUpdateQuizAnswers([FromBody] QuizResult quizResult)
        {
            var result = _groupQuizService.InsertUpdateQuizAnswers(quizResult);
            return Ok(result);
        }

        /// <summary>
        /// Author : Girish Sonawane
        /// CreatedOn : 30-10-2019
        /// Insert quiz question answer in temp table
        /// </summary>
        /// <param name="quizResult">quizResult</param>
        [HttpPost]
        [Route("logquizquestionanswer")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> LogQuizQuestionAnswer([FromBody] QuizResult quizResult)
        {
            var result = _groupQuizService.LogQuizQuestionAnswer(quizResult);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Girish S
        /// Created Date - 24 feb 2019
        /// Description - To save feedback as per quiz
        /// </summary>
        /// <param name="quizFeedback">quizFeedback</param>
        [HttpPost]
        [Route("InsertQuizFeedback")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertQuizFeedback([FromBody] QuizFeedback quizFeedback)
        {
            var result = _groupQuizService.InsertQuizFeedback(quizFeedback);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Girish S
        /// Created Date - 24 DEC 2019
        /// Description - To UpdateQuizGrade
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("updateQuizGrade")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateQuizGrade(int gradeId, int gradingTemplateId, int quizResultId)
        {
            var result = _groupQuizService.UpdateQuizGrade(gradeId, gradingTemplateId, quizResultId);
            return Ok(result);
        }


        /// <summary>
        /// Created By - girish sonawane
        /// Created Date - 30-10-2019
        /// Description - To get Quiz by Quiz Id.
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGroupQuizById")]
        [ProducesResponseType(typeof(GroupQuiz), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> GetGroupQuizById(int id)
        {
            var result = await _groupQuizService.GetById(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vinayak Yenpure
        /// Created Date - 29-04-2020
        /// Description - To Get Quiz By Course
        /// </summary>
        /// <param name="courseIds">courseIds</param>
        /// <param name="schoolId">schoolId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizByCourse")]
        [ProducesResponseType(typeof(IEnumerable<QuizView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizByCourse(string courseIds, int schoolId)
        {
            var quizList = await _groupQuizService.GetQuizByCourse(courseIds, schoolId);
            return Ok(quizList);
        }

        /// <summary>
        /// Created By - girish sonawane
        /// Created Date - 30-10-2019
        /// Description - To get Quiz result list by Quiz Id and User id.
        /// </summary>
        /// <param name="quizId">QuizId</param>
        /// <param name="userId">userId</param>
        /// <param name="resourceId">resourceId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizResultByUserId")]
        [ProducesResponseType(typeof(QuizResult), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizResultByUserId(int quizId, int userId, int resourceId)
        {
            var quizResponse = await _groupQuizService.GetQuizResultByUserId(quizId, userId, resourceId);
            return Ok(quizResponse);
        }

        /// <summary>
        /// Created By - vinayak yenpure
        /// Created Date - 27-4-2020
        /// Description - To get Get Log Result QuestionAnswer
        /// </summary>
        /// <param name="quizId">QuizId</param>
        /// <param name="userId">userId</param>
        /// <param name="resourceId">resourceId</param>
        /// <param name="resourceType">resourceType</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetLogResultQuestionAnswer")]
        [ProducesResponseType(typeof(QuizResult), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetLogResultQuestionAnswer(int quizId, int userId, int resourceId, string resourceType)
        {
            var quizResponse = await _groupQuizService.GetLogResultQuestionAnswer(quizId, userId, resourceId, resourceType);
            return Ok(quizResponse);
        }

        /// <summary>
        /// Created By - girish sonawane
        /// Created Date - 30-10-2019
        /// Description - To get Quiz result list by Quiz Id and User id.
        /// </summary>
        /// <param name="groupQuizId">groupQuizId</param>
        /// <param name="groupId">groupId</param>
        /// <param name="selectedGroupId">selectedGroupId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizStudentDetailsByQuizId")]
        [ProducesResponseType(typeof(GroupQuiz), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizStudentDetailsByQuizId(int groupQuizId, int groupId, int selectedGroupId)
        {
            var quizResponse = await _groupQuizService.GetQuizStudentDetailsByQuizId(groupQuizId, groupId, selectedGroupId);
            return Ok(quizResponse);
        }


     


        /// <summary>
        /// Created By - girish sonawane
        /// Created Date - 30-10-2019
        /// Description - To get Quiz result list by Quiz Id and User id.
        /// </summary>
        /// <param name="QuizId">QuizId</param>
        /// <param name="groupId">groupId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetFormStudentDetailsByQuizId")]
        [ProducesResponseType(typeof(GroupQuiz), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFormStudentDetailsByQuizId(int QuizId, int groupId)
        {
            var quizResponse = await _groupQuizService.GetFormStudentDetailsByQuizId(QuizId, groupId);
            return Ok(quizResponse);
        }

       /// <summary>
       /// To update group quiz record
       /// </summary>
       /// <param name="groupQuiz">group quiz object</param>
       /// <returns></returns>
        [HttpPost]
        [Route("UpdateGroupQuiz")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateGroupQuiz([FromBody]GroupQuiz groupQuiz)
        {
            var result = _groupQuizService.AddUpdateGroupQuiz(groupQuiz);
            return Ok(result);
        }

        #region Download functionality
        /// <summary>
        /// To download Quiz Report PDF
        /// </summary>
        /// <param name="groupQuizId">groupQuizId</param>
        /// <param name="groupId">groupId</param>
        /// <param name="selectedGroupId">selectedGroupId</param>
        /// <returns></returns>
        [HttpPost]
        [Route("downloadQuizReportPDF")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<string> downloadQuizReportPDF(int groupQuizId, int groupId, int selectedGroupId)
        {
            var viewName = "_QuestionWiseReport";
            GroupQuiz quizDetails = new GroupQuiz();
            var questionWiseReport = _groupQuizService.GetQuizStudentDetailsByQuizId(groupQuizId, groupId, selectedGroupId);
            quizDetails = questionWiseReport.Result;
            var model = quizDetails.QuestionWiseReport;
            var httpContext = new DefaultHttpContext { RequestServices = _serviceProvider };
            var actionContext = new ActionContext(httpContext, new RouteData(), new ActionDescriptor());

            using (var sw = new StringWriter())
            {
                var viewResult = _razorViewEngine.FindView(actionContext, viewName, false);

                if (viewResult.View == null)
                {
                    throw new ArgumentNullException($"{viewName} does not match any available view");
                }

                var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
                {
                    Model = model
                };

                var viewContext = new ViewContext(
                    actionContext,
                    viewResult.View,
                    viewDictionary,
                    new TempDataDictionary(actionContext.HttpContext, _tempDataProvider),
                    sw,
                    new HtmlHelperOptions()
                );

                await viewResult.View.RenderAsync(viewContext);
                return sw.ToString();
            }
        }
 
        /// <summary>
        /// To update group quiz record
        /// </summary>
        /// <param name="groupQuizId">groupQuizId</param>
        /// <param name="groupId">groupId</param>
        /// <param name="selectedGroupId">selectedGroupId</param>
        /// <returns></returns>
        [HttpPost]
        [Route("downloadQuizReportDetails")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public HttpResponseMessage downloadPoolFormat(int groupQuizId, int groupId, int selectedGroupId)
        {
            FileGenerator fileGenerator = new FileGenerator();
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write("Hello, World!");
            writer.Flush();
            stream.Position = 0;

            var questionWiseReport = _groupQuizService.GetQuizStudentDetailsByQuizId(groupQuizId, groupId, selectedGroupId);
            GroupQuiz quizDetails = new GroupQuiz();

            quizDetails = questionWiseReport.Result;
         
            byte[] bytes;
            var path = "Content/Files/EmptyReportFile.xls";

            bytes = fileGenerator.GenerateQuestionWiseReportXLSFile(path, quizDetails.QuestionWiseReport);
            string downloadFileName = "QuestionWiseReport.xls";
            var FileContent= File(bytes, "application/vnd.ms-excel", downloadFileName);

          

            //converting Pdf file into bytes array  
            //var dataBytes = File.ReadAllBytes(reqBook);
            //adding bytes to memory stream   
            var dataStream = new MemoryStream(bytes);

            HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            httpResponseMessage.Content = new StreamContent(dataStream);
            httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            httpResponseMessage.Content.Headers.ContentDisposition.FileName = downloadFileName;
            httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

            return httpResponseMessage;
        }


        #endregion


    }
}