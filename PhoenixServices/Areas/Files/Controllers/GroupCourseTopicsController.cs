﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.Files.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class GroupCourseTopicsController : ControllerBase
    {
        private readonly IGroupCourseTopicService _groupCourseTopicService;
        public GroupCourseTopicsController(IGroupCourseTopicService groupCourseTopicService)
        {
            _groupCourseTopicService = groupCourseTopicService;
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 24 June 2020
        /// Descr: To get all course topices for all groups of all schools
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getallgroupcoursetopics")]
        [ProducesResponseType(typeof(IEnumerable<GroupCourseTopic>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllGroupCourseTopics()
        {
            var result = await _groupCourseTopicService.GetAll();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 24 June 2020
        /// Descr: To get all group course topics by groupcoursetopicid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getgroupcoursetopicbyid/{id:int}")]
        [ProducesResponseType(typeof(GroupCourseTopic), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGroupCourseTopicById(int id)
        {
            var result = await _groupCourseTopicService.GetById(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 24 June 2020
        /// Descr: To get all group course topics by groupid
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getgroupcoursetopicbygroupid/{groupId:int}/{isactive:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<GroupCourseTopic>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGroupCourseTopicByGroupId(long groupId, bool? isActive = null)
        {
            var result = await _groupCourseTopicService.GetGroupCourseTopicsByGroupId(groupId, isActive);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 24 June 2020
        /// Descr: To get all group course topics by different parameters
        /// </summary>
        /// <param name="groupCourseTopicId"></param>
        /// <param name="groupId"></param>
        /// <param name="topicId"></param>
        /// <param name="userId"></param>
        /// <param name="isSubTopic"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getgroupcoursetopics")]
        [ProducesResponseType(typeof(IEnumerable<GroupCourseTopic>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGroupCourseTopics(long? groupCourseTopicId, long? groupId, long? topicId, long? userId, bool? isSubTopic, bool? isActive = null)
        {
            var result = await _groupCourseTopicService.GetGroupCourseTopics(groupCourseTopicId,groupId,topicId,userId,isSubTopic,isActive);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 24 June 2020
        /// Descr: To insert into groupcoursetopic
        /// </summary>
        /// <param name="groupCourseTopic"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addgroupcoursetopic")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult AddGroupCourseTopic([FromBody]GroupCourseTopic groupCourseTopic)
        {
            var result = _groupCourseTopicService.Insert(groupCourseTopic);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 24 June 2020
        /// Descr: To update groupcoursetopic
        /// </summary>
        /// <param name="groupCourseTopic"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updategroupcoursetopic")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult Update([FromBody]GroupCourseTopic groupCourseTopic)
        {
            var result = _groupCourseTopicService.Update(groupCourseTopic);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 24 June 2020
        /// Descr: To delete groupcoursetopic by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteGroupCourseTopic/{id:int}/{userId:long}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteGroupCourseTopic(int id,long userId)
        {
            var result = _groupCourseTopicService.Delete(id, userId);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 29 Sept 2020
        /// Descr: To update group course topic (Unit) sort order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateunitsortorder")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult Update([FromBody]IEnumerable<GroupCourseTopic> model)
        {
            var result = _groupCourseTopicService.UpdateUnitSortOrder(model);
            return Ok(result);
        }
    }
}