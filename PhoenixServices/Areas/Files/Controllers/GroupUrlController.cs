﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.Files.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class GroupUrlController : ControllerBase
    {
        private readonly IGroupUrlService _groupUrlService;

        public GroupUrlController(IGroupUrlService groupUrlService)
        {
            _groupUrlService = groupUrlService;

        }
        /// <summary>
        /// Description - To add or update RSSFeed.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("AddGroupUrl")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddGroupUrl([FromBody]GroupUrl groupUrl)
        {
            var result = _groupUrlService.AddUpdateGroupUrl(groupUrl);
            return Ok(result);
        }
        /// <summary>
        /// Description - To add or update RSSFeed.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateGroupUrl")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateGroupUrl([FromBody]GroupUrl groupUrl)
        {
            var result = _groupUrlService.AddUpdateGroupUrl(groupUrl);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetGroupUrlListByGroupId")]
        [ProducesResponseType(typeof(IEnumerable<GroupUrl>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> GetGroupUrlListByGroupId(int groupId)
        {
            var result = await _groupUrlService.GetGroupUrlListByGroupId(groupId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetGroupUrlById")]
        [ProducesResponseType(typeof(GroupUrl), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> GetGroupUrlById(int id)
        {
            var result =await _groupUrlService.GetById(id);
            return Ok(result);
        }

    }
}