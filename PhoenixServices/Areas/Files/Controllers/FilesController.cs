﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.Files.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class FilesController : ControllerBase
    {
        private readonly IFileService _fileService;

        public FilesController(IFileService fileService)
        {
            _fileService = fileService;
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get file types
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getfiletypes")]
        [ProducesResponseType(typeof(IEnumerable<VLEFileType>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFileTypes(string DocumentType)
        {
            var result = await _fileService.GetFileTypes(DocumentType);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get file management modules
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getfilemanagementmodule")]
        [ProducesResponseType(typeof(IEnumerable<FileManagementModule>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFileManagementModules()
        {
            var result = await _fileService.GetFileManagementModules();
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all files
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getfiles")]
        [ProducesResponseType(typeof(IEnumerable<File>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFiles()
        {
            var result = await _fileService.GetAll();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all file by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfilebyid/{id:int}")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFileById(int id)
        {
            var result = await _fileService.GetById(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all file by folder id 
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfilesbyfolderid/{folderid:int}/{isactive:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<File>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFilesByFolderId(int folderId, bool? isActive = null)
        {
            var result = await _fileService.GetFilesByFolderId(folderId, isActive);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all file by schoolId and moduleId
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="moduleId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfilesbymoduleid/{schoolid:int}/{moduleid:int}/{isactive:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<File>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFilesByModuleId(int schoolId, int moduleId, bool? isActive = null)
        {
            var result = await _fileService.GetFilesByModuleId(schoolId, moduleId, isActive);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 18/July/2019
        /// Description: To get all file by sectionId and moduleId
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="moduleId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfilesbysectionid/{sectionid:int}/{moduleid:int}/{isactive:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<File>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFilesBySectionId(int sectionId, int moduleId, bool? isActive = null)
        {
            var result = await _fileService.GetFilesBySectionId(sectionId, moduleId, isActive);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all file by schoolId 
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfilesbyschoolid/{schoolid:int}/{isactive:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<File>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFilesBySchoolId(int schoolId, bool? isActive = null)
        {
            var result = await _fileService.GetFilesBySchoolId(schoolId, isActive);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get all file by schoolId 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="moduleId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getfilesbyuserid/{userid:int}/{moduleid:int}/{isactive:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<File>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFilesByUserId(int userId, int moduleId, bool? isActive = null)
        {
            var result = await _fileService.GetFilesByUserId(userId, moduleId, isActive);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To add new file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addfile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult AddFile([FromBody]File file)
        {
            var result = _fileService.Insert(file);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To update new file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatefile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult UpdateFile([FromBody]File file)
        {
            var result = _fileService.Update(file);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To get delete file
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletefile/{id:int}/{userId:long}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteFile(int id, long userId)
        {
            var result = _fileService.Delete(id, userId);
            return Ok(result);
        }
        /// <summary>
        /// Created By: Rohit Patil
        /// Created On: 25/Aug/2019
        /// Description: To get all file by Student Id 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentGroupsFiles")]
        [ProducesResponseType(typeof(IEnumerable<StudentGroupsFiles>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentGroupsFiles(long userId)
        {
            var result = await _fileService.GetStudentGroupsFiles(userId);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 04/July/2019
        /// Description: To update new file
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="fileName"></param>
        ///  <param name="updatedBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("FileRename")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult RenameFile(long fileId, string fileName, long updatedBy)
        {
            var result = _fileService.RenameFile(fileId, fileName, updatedBy);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 5th April 2020
        /// Description - returns all files under a school group
        /// </summary>
        /// <param name="schoolGroupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllGroupFiles")]
        [ProducesResponseType(typeof(IEnumerable<File>), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> GetAllGroupFiles(int schoolGroupId)
        {
            var result = await _fileService.GetAllGroupFiles(schoolGroupId);
            return Ok(result);
        }

        /// <summary>
        /// Creaetd By - Rohit Patil
        /// Created Date - 5th April 2020
        /// Description - clear all group files/folders
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ClearGroupFiles")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> ClearGroupFiles([FromBody]SchoolGroup model)
        {
            int result = await _fileService.ClearGroupFiles(model);
            return Ok(result);
        }
        #region Cloud Storage Files Save
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 14th May 2020
        /// Description- save selected cloud files
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveCloudFiles")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> SaveCloudFiles([FromBody]List<File> files)
        {
            bool result = await _fileService.SaveCloudFiles(files);
            return Ok(result);
        }
        #endregion



        #region Bulk Files CD

        /// <summary>
        /// Created By: Rohan Patil
        /// Created On: 02/July/2020
        /// Description: To get files by IDs
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getfilesbyids")]
        [ProducesResponseType(typeof(IEnumerable<File>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFilesByIds(string Ids)
        {
            var result = await _fileService.GetFilesByIds(Ids);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Rohan Patil
        /// Created On: 02/July/2020
        /// Description- Delete bulk files and folders
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("FilesFolderBulkDelete")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> FilesFolderBulkDelete([FromBody]MyFilesModel model)
        {
            bool result = await _fileService.FilesFolderBulkDelete(model);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Rohan Patil
        /// Created On: 02/July/2020
        /// Description- Create bulk files and folders
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("FilesFolderBulkCreate")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> FilesFolderBulkCreate([FromBody] MyFilesModel model)
        {
            bool result = await _fileService.FilesFolderBulkCreate(model);
            return Ok(result);
        }
        #endregion
        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn : 1 July 2020
        /// Desc: To get group detail explorer
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="sectionId"></param>
        /// <param name="folderId"></param>
        /// <param name="studentUserId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGroupFileExplorer")]
        [ProducesResponseType(typeof(FileExplorer), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> GetGroupFileExplorer(int moduleId,long sectionId,long folderId, long studentUserId, bool isActive)
        {
            var result = await _fileService.GetGroupFileExplorer(moduleId, sectionId, folderId, studentUserId, isActive);
            return Ok(result);
        }

    }
}