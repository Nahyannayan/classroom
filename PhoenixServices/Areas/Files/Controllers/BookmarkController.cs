﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.Files.Controllers
{
    [Route("api/v1/[controller]")]    
    [ApiController]
    //[Authorize]
    public class BookmarkController : ControllerBase
    {
        private readonly IBookmarkService _bookmarkService;
        public BookmarkController(IBookmarkService bookmarkService)
        {
            _bookmarkService = bookmarkService;
        }
       
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 20 Dec 2019
        /// Description - To add bookmark.
        /// </summary>
        /// <param name="bookmark"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddBookmark")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddBookmark([FromBody]Bookmark bookmark)
        {
            var result = _bookmarkService.InserBookmark(bookmark);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 20 Dec 2019
        /// Description - To update bookmark.
        /// </summary>
        /// <param name="bookmark"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateBookmark")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateBookmark([FromBody]Bookmark bookmark)
        {
            var result = _bookmarkService.UpdateBookmark(bookmark);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 20 Dec 2019
        /// Description - To update bookmark.
        /// </summary>
        /// <param name="bookmarkId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteBookmark")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> DeleteBookmark(long bookmarkId)
        {
            var bookmark = new Bookmark();
            bookmark.BookmarkId = bookmarkId;
            var result = _bookmarkService.DeleteBookmark(bookmark);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 20 Dec 2019
        /// Description - To get bookmarks per userId.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getBookmarks")]
        [ProducesResponseType(typeof(IEnumerable<Bookmark>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBookmarks(int userId)
        {
            var bookmarkList = await _bookmarkService.GetBookmarks(userId);
            return Ok(bookmarkList);
        }
        /// <summary>
        /// Created By: Mahesh Chikhale
        /// Created On: 12/Dec/2019
        /// Description: To get bookmark
        /// </summary>
        /// <param name="bookmarkId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getBookmark")]
        [ProducesResponseType(typeof(Bookmark), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBookmark(long bookmarkId)
        {
            var result = await _bookmarkService.GetBookmark(bookmarkId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 20 Dec 2019
        /// Description - To get selected groups for Bookmark
        /// </summary>
        /// <param name="bookmarkId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getBookmarkSchoolGroups")]
        [ProducesResponseType(typeof(IEnumerable<Bookmark>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBookmarkSchoolGroups(long bookmarkId)
        {
            var bookmarkList = await _bookmarkService.GetBookmarkSchoolGroups(bookmarkId);
            return Ok(bookmarkList);
        }
    }
}