﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Web;
using Phoenix.Common.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Phoenix.Models.Entities;
using Phoenix.Common.Enums;
using Phoenix.API.Models.DashBoard;

namespace Phoenix.API.Areas.DashBoard
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class DashBoardController : ControllerBase
    {
        private readonly IDashBoardServices _dashBoardService;
        public DashBoardController(IDashBoardServices dashBoardService)
        {
            _dashBoardService = dashBoardService;
        }

        /// <summary>
        /// Created By - Nikhil Jahagirdar
        /// Created Date - 29 August 2020
        /// Description - To get all DashBoard Banners.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("DashBoardBanners")]
        [ProducesResponseType(typeof(IEnumerable<DashBoardBanners>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDashBoardBanners(int id)
        {
            var banners = await _dashBoardService.getSchoolBanners(id);
            return Ok(banners);
        }

        /// <summary>
        /// Created By - Nikhil Jahagirdar
        /// Created Date - 29 August 2020
        /// Description - To get all DashBoard School Groups.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("DashBoardSchoolGroups")]
        [ProducesResponseType(typeof(IEnumerable<DashBoardGroups>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDashBoardSchoolGroups(int id)
        {
            var banners = await _dashBoardService.getSchoolGroups(id);
            return Ok(banners);
        }

        /// <summary>
        /// Created By - Nikhil Jahagirdar
        /// Created Date - 29 August 2020
        /// Description - To get all DashBoard Other Groups.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("DashBoardOtherGroups")]
        [ProducesResponseType(typeof(IEnumerable<DashBoardGroups>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDashBoardOtherGroups(int id)
        {
            var banners = await _dashBoardService.getOtherGroups(id);
            return Ok(banners);
        }
    }
}
