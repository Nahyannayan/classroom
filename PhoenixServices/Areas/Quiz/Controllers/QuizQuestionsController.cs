﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using SelectPdf;

namespace Phoenix.API.Areas.Quiz.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class QuizQuestionsController : ControllerBase
    {
        private readonly IQuizQuestionsService _IQuizQuestionsService;
        public QuizQuestionsController(IQuizQuestionsService quizQuestionsService)
        {
            _IQuizQuestionsService = quizQuestionsService;
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 25 July 2019
        /// Description - To insert Quiz Questions
        /// </summary>
        /// <param name="quizQuestionsView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("QuizQuestionsinsert")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> QuizQuestionsInsert([FromBody]QuizQuestionsView quizQuestionsView)
        {
            var result = _IQuizQuestionsService.QuizQuestionsInsert(quizQuestionsView);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vinayak Yenpure
        /// Created Date - 30 March 2020
        /// Description - To import quiz questions
        /// </summary>
        /// <param name="importQuizView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ImportQuizQuestion")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> ImportQuizQuestion([FromBody]ImportQuizView importQuizView)
        {
            var result = _IQuizQuestionsService.ImportQuizQuestion(importQuizView);
            return Ok(result);
        }

        /// <summary>
        /// Created By - vinayak y
        /// Created Date - 31-03-2020
        /// Description - To Get Id By QuestionType
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetIdByQuestionType")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetIdByQuestionType(string questionType)
        {
            var result = _IQuizQuestionsService.GetIdByQuestionType(questionType);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vinayak Y 
        /// Created Date - 04 JAN 2020
        /// Description - To Insert Update PoolQuestions
        /// </summary>
        /// <param name="quizQuestionsView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertUpdatePoolQuestions")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertUpdatePoolQuestions([FromBody]QuizQuestionsView quizQuestionsView)
        {
            var result = _IQuizQuestionsService.InsertUpdatePoolQuestions(quizQuestionsView);
            return Ok(result);
        }

        /// <summary>
        /// Created By - vinayak y
        /// Created Date - 29 JAN 2020
        /// Description - To delete quiz question file
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteQuizQuestionFile")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteQuizQuestionFile(int id)
        {
            var result = _IQuizQuestionsService.DeleteQuizQuestionFile(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By - vinayak y
        /// Created Date - 29 JAN 2020
        /// Description - To delete quiz question 
        /// </summary>
        /// <param name="quizQuestionId"></param>
        /// <param name="quizId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteQuizQuestion")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteQuizQuestion(int quizQuestionId, int quizId)
        {
            var result = _IQuizQuestionsService.DeleteQuizQuestion(quizQuestionId, quizId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - vinayak y
        /// Created Date - 29 JAN 2020
        /// Description - To delete quiz question file
        /// </summary>
        /// <param name="Id"> Id </param>
        /// <returns></returns>
        [HttpGet]
        [Route("DeleteMTPResource")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteMTPResource(int Id)
        {
            var result = _IQuizQuestionsService.DeleteMTPResource(Id);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 25 July 2019
        /// Description - To update Quiz Questions
        /// </summary>
        /// <param name="quizQuestionsView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("quizquestionsupdate")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> QuizQuestionsUpdate([FromBody]QuizQuestionsView quizQuestionsView)
        {
            var result = _IQuizQuestionsService.QuizQuestionsUpdate(quizQuestionsView);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 25 July 2019
        /// Description - To get Question by id.
        /// </summary>
        /// <param name="questionId">questionId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getQuizQuestionsById")]
        [ProducesResponseType(typeof(QuizQuestionsView), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizQuestionsById(int questionId)
        {
            var quizQuestion = await _IQuizQuestionsService.GetQuizQuestionById(questionId);
            return Ok(quizQuestion);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 28 July 2019
        /// Description - To get Quiz Question list by Quiz Id.
        /// </summary>
        /// <param name="quizId">QuizId</param>
        /// <param name="IsTeacher">IsTeacher</param>
        /// <param name="languageId">languageId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizQuestionsByQuizId")]
        [ProducesResponseType(typeof(IEnumerable<QuizQuestionsView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizQuestionsByQuizId(int quizId, bool IsTeacher,short languageId=1)
        {
            var quizQuestion = await _IQuizQuestionsService.GetQuizQuestionsByQuizId(quizId, IsTeacher, languageId);
            return Ok(quizQuestion);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 28 July 2019
        /// Description - To get Quiz Question list by Quiz Id.
        /// </summary>
        /// <param name="quizId">QuizId</param>
        /// <param name="PageNumber">PageNumber</param>
        /// <param name="PageSize">PageSize</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizQuestionsPaginationByQuizId")]
        [ProducesResponseType(typeof(IEnumerable<QuizQuestionsView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizQuestionsPaginationByQuizId(int quizId, int PageNumber, int PageSize)
        {
            var quizQuestion = await _IQuizQuestionsService.GetQuizQuestionsPaginationByQuizId(quizId, PageNumber, PageSize);
            return Ok(quizQuestion);
        }

        /// <summary>
        /// Created By - Vinayak Y
        /// Created Date - 04-02-2020
        /// Description - To get All Quiz Question list .
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllQuizQuestions")]
        [ProducesResponseType(typeof(IEnumerable<QuizQuestionsView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllQuizQuestions()
        {
            var quizQuestion = await _IQuizQuestionsService.GetAllQuizQuestions();
            return Ok(quizQuestion);
        }

        /// <summary>
        /// Created By - Vinayak Y
        /// Created Date - 04-02-2020
        /// Description - To get All Quiz Question list .
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetFilteredQuizQuestions")]
        [ProducesResponseType(typeof(IEnumerable<QuizQuestionsView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFilteredQuizQuestions(string courseIds, string lstObjectives)
        {
            var quizQuestion = await _IQuizQuestionsService.GetFilteredQuizQuestions(courseIds, lstObjectives);
            return Ok(quizQuestion);
        }

        /// <summary>
        /// Created By - Vinayak y
        /// Created Date - 03 FEB 2020
        /// Description - To get objectives of question 
        /// </summary>
        /// <param name="questionId"></param>        
        [HttpGet]
        [Route("GetQuestionObjectives")]
        [ProducesResponseType(typeof(IEnumerable<Objective>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuestionObjectives(int questionId)
        {
            var ObservationFileList = await _IQuizQuestionsService.GetQuestionObjectives(questionId);
            return Ok(ObservationFileList);
        }

        /// <summary>
        /// Created By - Vinayak Y
        /// Created Date - 03 JAN 2020
        /// Description - To get school groups associated with question
        /// </summary>
        /// <param name="questionId"></param>        
        [HttpGet]
        [Route("GetQuestionSubjects")]
        [ProducesResponseType(typeof(IEnumerable<Subject>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuestionSubjects(int questionId)
        {
            var schoolGroupList = await _IQuizQuestionsService.GetQuestionSubjects(questionId);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By - vinayak y 
        /// Created Date - 26 JAN 2020
        /// Description - To get Quiz Question files by Quiz question Id.
        /// </summary>
        /// <param name="quizQuestionId">quizQuestionId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetFilesByQuizQuestionsId")]
        [ProducesResponseType(typeof(IEnumerable<QuizQuestionFiles>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFilesByQuizQuestionId(int quizQuestionId)
        {
            var quizQuestionFiles = await _IQuizQuestionsService.GetFilesByQuizQuestionId(quizQuestionId);
            return Ok(quizQuestionFiles);
        }


        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 29 July 2019
        /// Description - To update correct quiz answer
        /// </summary>
        /// <param name="quizAnswersView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateQuizCorrectAnswerByQuizAnswerId")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateQuizCorrectAnswerByQuizAnswerId([FromBody]QuizAnswersView quizAnswersView)
        {
            var result = _IQuizQuestionsService.UpdateQuizCorrectAnswerByQuizAnswerId(quizAnswersView);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vinayak Y
        /// Created Date - 30 JAN 2020
        /// Description - To update quiz uestion sort
        /// </summary>
        /// <param name="quizQuestion"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SortQuestionByOrder")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> SortQuestionByOrder([FromBody]QuizQuestionsView quizQuestion)
        {
            var result = _IQuizQuestionsService.SortQuestionByOrder(quizQuestion);
            return Ok(result);
        }

        /// <summary>
        /// Created By - vinayak y 
        /// Created Date - 10 JAN 2020
        /// Description - To get Quiz Question file by Quiz question file Id.
        /// </summary>
        /// <param name="quizQuestionFileId">quizQuestionFileId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetFileByQuizQuestionFileId")]
        [ProducesResponseType(typeof(QuizQuestionFiles), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFileByQuizQuestionFileId(int quizQuestionFileId)
        {
            var quizQuestionFiles = await _IQuizQuestionsService.GetFileByQuizQuestionFileId(quizQuestionFileId);
            return Ok(quizQuestionFiles);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 29 July 2019
        /// Description - To get Quiz Question's answer list by Quiz Id.
        /// </summary>
        /// <param name="quizQuestionId">QuizId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizAnswerByQuizQuestionId")]
        [ProducesResponseType(typeof(IEnumerable<QuizAnswersView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizAnswerByQuizQuestionId(int quizQuestionId)
        {
            var quizQuestion = await _IQuizQuestionsService.GetQuizAnswerByQuizQuestionId(quizQuestionId);
            return Ok(quizQuestion);
        }
        /// <summary>
        /// Created By - Rohan Patil
        /// Created Date - 1 Aug 2019
        /// Description - To delete Quiz Question by id
        /// </summary>
        /// <param name="quizQuestionsView">quizQuestionsView</param>
        /// <returns></returns>
        [HttpPost]
        [Route("QuizQuestionsDelete")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteQuizQuestion([FromBody]QuizQuestionsView quizQuestionsView)
        {
            var result = _IQuizQuestionsService.QuizQuestionsDelete(quizQuestionsView);
            return Ok(result);
        }
        /// <summary>
        /// Author : Rohan Patil
        /// CreatedOn : 5-08-2019
        /// Insert QUIZ Answer
        /// </summary>
        /// <param name="quizResponse">quizResponse</param>
        [HttpPost]
        [Route("insertquizanswer")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateQuizAnswerData([FromBody] QuizResponse quizResponse)
        {
            var result = _IQuizQuestionsService.UpdateQuizAnswerData(quizResponse);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Rohan Patil
        /// Created Date - 4 Aug 2019
        /// Description - To get Quiz Response list by Quiz Id and User id.
        /// </summary>
        /// <param name="quizId"></param>
        /// <param name="userId"></param>
        /// <param name="studentId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizResponseByUserId")]
        [ProducesResponseType(typeof(QuizResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizResponseByUserId(int quizId,int userId, int studentId, int taskId)
        {
            var quizResponse = await _IQuizQuestionsService.GetQuizResponseByUserId(quizId, userId, studentId,taskId);
            return Ok(quizResponse);
        }
        #region Download functionality
        /// <summary>
        /// Author : Rohan Patil
        /// CreatedOn : 5-08-2019
        /// downloadPoolFormat
        /// </summary>
        [HttpPost]
        [Route("downloadPoolFormat")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public HttpResponseMessage downloadPoolFormat()
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write("Hello, World!");
            writer.Flush();
            stream.Position = 0;
       
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            var myHtml = "<style>h1 {font-size:12px;}</style><h1>Test</h1><p style='font-weight:bold'>Test Bold</p>";

            HtmlToPdf converter = new HtmlToPdf();
            PdfDocument doc = converter.ConvertHtmlString(myHtml);
            doc.Save("output.pdf");
            doc.Close();


            return result;
        }
        
        #endregion

    }
}