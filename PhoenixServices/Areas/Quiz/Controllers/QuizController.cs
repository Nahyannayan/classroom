﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Models;
using Phoenix.Models;

namespace Phoenix.API.Areas.Quiz.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class QuizController : ControllerBase
    {
        private readonly IQuizService _IQuizService;
        public QuizController(IQuizService quizService)
        {
            _IQuizService = quizService;
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 24 July 2019
        /// Description - To get all Quiz.
        /// </summary>
        /// <param name="schoolId">School Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getallquiz")]
        [ProducesResponseType(typeof(IEnumerable<QuizView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllQuiz(long schoolId)
        {
            var quizList = await _IQuizService.GetAllQuiz(schoolId);
            return Ok(quizList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 25 July 2019
        /// Description - To get Quiz by id.
        /// </summary>
        /// <param name="quizId">QuizId</param>
        /// <param name="schoolId">School Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getquiz")]
        [ProducesResponseType(typeof(QuizView), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuiz(int quizId,long schoolId)
        {
            var quizList = await _IQuizService.GetQuiz(quizId,schoolId);
            return Ok(quizList);
        }

        /// <summary>
        /// Created By - Vinayak Yenpure
        /// Created Date - 30 Dec 2012
        /// Description - Get Quiz Resourse Detail.
        /// </summary>
        /// <param name="resourseId">ResourseId</param>
        /// <param name="resourseType">ResourseType</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizResourseDetail")]
        [ProducesResponseType(typeof(QuizView), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizResourseDetail(int resourseId, string resourseType)
        {
            var quizList = await _IQuizService.GetQuizResourseDetail(resourseId, resourseType);
            return Ok(quizList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 25 July 2019
        /// Description - To get Quiz Details by id for quiz page.
        /// </summary>
        /// <param name="quizId">QuizId</param>
        /// <param name="schoolId">School Id</param>
        /// <param name="taskId">TaskId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getTaskQuizDetail")]
        [ProducesResponseType(typeof(QuizView), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTaskQuizDetail(int quizId, long taskId, long schoolId)
        {
            var quizList = await _IQuizService.GetTaskQuizDetail(quizId, taskId, schoolId);
            return Ok(quizList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 25 July 2019
        /// Description - To get Quiz Details by id for quiz page.
        /// </summary>
        /// <param name="quizId">QuizId</param>
        /// <param name="schoolId">School Id</param>
        /// <param name="quizResourceId">QuizResourceId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getquizdetails")]
        [ProducesResponseType(typeof(QuizView), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizDetails(int quizId, int quizResourceId, long schoolId)
        {
            var quizList = await _IQuizService.GetQuizDetails(quizId, quizResourceId, schoolId);
            return Ok(quizList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 24 July 2019
        /// Description - To insert Quiz
        /// </summary>
        /// <param name="quizView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("quizinsert")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> QuizInsert([FromBody]QuizView quizView)
        {
            var result = _IQuizService.QuizInsert(quizView);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vinayak Y
        /// Created Date - 03 JAN 2020
        /// Description - To get subject grade for quiz
        /// </summary>
        /// <param name="quizId"></param>        
        [HttpGet]
        [Route("GetQuizSubjectsGrade")]
        [ProducesResponseType(typeof(IEnumerable<Subject>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizSubjectsGrade(int quizId)
        {
            var schoolGroupList = await _IQuizService.GetQuizSubjectsGrade(quizId);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By - Vinayak Y
        /// Created Date - 03 JAN 2020
        /// Description - To get quiz report
        /// </summary>
        /// <param name="quizId"></param>   
        /// <param name="userId"></param>   
        [HttpGet]
        [Route("GetQuizReport")]
        [ProducesResponseType(typeof(QuizReport), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizReport(int quizId, long userId)
        {
            var schoolGroupList = await _IQuizService.GetQuizReport(quizId, userId);
            return Ok(schoolGroupList);
        }


        /// <summary>
        /// Created By - Vinayak Y
        /// Created Date - 03 JAN 2020
        /// Description - To get group wise quiz report
        /// </summary>
        /// <param name="quizId"></param>   
        /// <param name="groupQuizId"></param>   
        [HttpGet]
        [Route("GetGroupQuizReport")]
        [ProducesResponseType(typeof(QuizReport), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGroupQuizReport(int quizId, int groupQuizId)
        {
            var schoolGroupList = await _IQuizService.GetGroupQuizReport(quizId, groupQuizId);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By - Vinayak Yenpure
        /// Created Date - 05-10-2020
        /// Description - To insert Quiz question answer files
        /// </summary>
        /// <param name="lstQuizAnswerFiles"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadQuestionAnswerFiles")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UploadQuestionAnswerFiles([FromBody]List<QuizQuestionAnswerFiles> lstQuizAnswerFiles)
        {
            var result = _IQuizService.UploadQuestionAnswerFiles(lstQuizAnswerFiles);
            return Ok(result);
        }

        /// <summary>
        /// Created By - vinayak y 
        /// Created Date - 26 JAN 2020
        /// Description - To get Quiz files by Quiz Id.
        /// </summary>
        /// <param name="quizId">quizId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetFilesByQuizId")]
        [ProducesResponseType(typeof(IEnumerable<QuizFile>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFilesByQuizId(int quizId)
        {
            var quizFiles = await _IQuizService.GetFilesByQuizId(quizId);
            return Ok(quizFiles);
        }

        /// <summary>
        /// Created By - vinayak y 
        /// Created Date - 10 JAN 2020
        /// Description - To get Quiz file by quiz file id.
        /// </summary>
        /// <param name="quizFileId">quizFileId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetFileByQuizFileId")]
        [ProducesResponseType(typeof(IEnumerable<QuizFile>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFileByQuizFileId(int quizFileId)
        {
            var quizFiles = await _IQuizService.GetFileByQuizFileId(quizFileId);
            return Ok(quizFiles);
        }

        /// <summary>
        /// Created By - vinayak y 
        /// Created Date - 05-10-2020
        /// Description - To Get Quiz Question Answer Files
        /// </summary>
        /// <param name="resourceId">resourceId</param>
        /// <param name="resourceType">resourceType</param>
        /// <param name="questionId">questionId</param>
        /// <param name="studentId">studentId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizQuestionAnswerFiles")]
        [ProducesResponseType(typeof(IEnumerable<QuizQuestionAnswerFiles>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizQuestionAnswerFiles(long resourceId, string resourceType, long questionId, long studentId)
        {
            var quizFiles = await _IQuizService.GetQuizQuestionAnswerFiles(resourceId, resourceType, questionId, studentId);
            return Ok(quizFiles);
        }

        /// <summary>
        /// Created By - Vinayak y
        /// Created Date - 07-FEB-2020
        /// Description - To Log quiz time by userId
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="userId">userId</param>
        /// <param name="resourceId">resourceId</param>
        /// <param name="resourceType">resourceType</param>
        /// <param name="isStartQuiz">isStartQuiz</param>
        /// <returns></returns>
        [HttpGet]
        [Route("LogQuizTime")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> LogQuizTime(int id,int resourceId, string resourceType, long userId, bool isStartQuiz)
        {
            var result = _IQuizService.LogQuizTime(id, resourceId, resourceType, userId, isStartQuiz);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vinayak y
        /// Created Date - 03 FEB 2020
        /// Description - To get objectives of quiz 
        /// </summary>
        /// <param name="quizId"></param>        
        [HttpGet]
        [Route("GetQuizObjectives")]
        [ProducesResponseType(typeof(IEnumerable<Objective>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizObjectives(int quizId)
        {
            var ObservationFileList = await _IQuizService.GetQuizObjectives(quizId);
            return Ok(ObservationFileList);
        }

        /// <summary>
        /// Created By - vinayak y 
        /// Created Date - 26 JAN 2020
        /// Description - To delete quiz file by Id.
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteQuizFile")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteQuizFile(int Id)
        {
            var result = _IQuizService.DeleteQuizFile(Id);
            return Ok(result);
        }

        /// <summary>
        /// Created By - vinayak y 
        /// Created Date - 26 JAN 2020
        /// Description - To Delete Question Answer File By Id
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteQuestionAnswerFile")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteQuestionAnswerFile(int Id, long userId)
        {
            var result = _IQuizService.DeleteQuestionAnswerFile(Id, userId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 24 July 2019
        /// Description - To insert Quiz
        /// </summary>
        /// <param name="quizView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("quizupdate")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> QuizUpdate([FromBody]QuizView quizView)
        {
            var result = _IQuizService.QuizUpdate(quizView);
            return Ok(result);
        }
        /// <summary>
        /// Description - To delete Quiz.
        /// </summary>
        /// <param name="quizView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("quizdelete")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> Quizdelete([FromBody]QuizView quizView)
        {
            var result = _IQuizService.QuizDelete(quizView);
            return Ok(result);
        }
        /// <summary>
        /// Description - To active deactive Quiz.
        /// </summary>
        /// <param name="quizView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ActiveDeactiveQuiz")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> ActiveDeactiveQuiz([FromBody]QuizView quizView)
        {
            var result = _IQuizService.ActiveDeactiveQuiz(quizView.QuizId);
            return Ok(result);
        }
        /// <summary>
        /// Description - To get quiz details by user id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isForm"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getallquizbyuser")]
        [ProducesResponseType(typeof(IEnumerable<QuizView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllQuizByUser(long id, bool isForm)
        {
            var quizList = await _IQuizService.GetAllQuizByUser(id,isForm);
            return Ok(quizList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 24 July 2019
        /// Description - To insert Quiz
        /// </summary>
        /// <param name="quizView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getpaginatequizbyuser")]
        [ProducesResponseType(typeof(QuizPagination), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetPaginateQuizByUser([FromBody]QuizPaginationRequest quizView)
        {
            var result = await _IQuizService.GetPaginateQuizByUser(quizView);
            return Ok(result);
        }

        /// <summary>
        /// Created By - girish sonawane
        /// Created Date - 19-02-2020
        /// Description - To get all quiz by student id.
        /// </summary>
        /// <param name="studentId">studentId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllQuizByStudentId")]
        [ProducesResponseType(typeof(IEnumerable<GroupQuiz>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllQuizByStudentId(long studentId)
        {
            var quizResponse = await _IQuizService.GetAllQuizByStudentId(studentId);
            return Ok(quizResponse);
        }
        /// <summary>
        /// Description - To get quiz details by user id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sourceId"></param>
        /// <param name="sourceType"></param>
        /// <param name="isForm"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getallquizbyuserandsource")]
        [ProducesResponseType(typeof(IEnumerable<QuizView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllQuizByUserAndSource(long id,long sourceId,string sourceType, bool isForm)
        {
            var quizList = await _IQuizService.GetAllQuizByUserAndSource(id, sourceId, sourceType, isForm);
            return Ok(quizList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 08 MAR 2020
        /// Description - To get quiz details by userId, quizId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="quizId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizDetailsByQuizIdAndUserId")]
        [ProducesResponseType(typeof(IEnumerable<QuizDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizDetailsByQuizIdAndUserId(long userId, int quizId)
        {
            var quizResponse = await _IQuizService.GetQuizDetailsByQuizIdAndUserId(userId, quizId);
            return Ok(quizResponse);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 08 MAR 2020
        /// Description - To get quiz question answers by questionId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAnswersByQuestionId")]
        [ProducesResponseType(typeof(IEnumerable<QuizAnswer>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAnswersByQuestionId(long userId, int questionId)
        {
            var quizResponse = await _IQuizService.GetAnswersByQuestionId(userId, questionId);
            return Ok(quizResponse);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 10 MAR 2020
        /// Description - To get quiz questions count
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="quizId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizQuestionsCount")]
        [ProducesResponseType(typeof(Int32), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizQuestionsCount(long userId, int quizId)
        {
            var quizResponse =  _IQuizService.GetQuizQuestionsCount(userId, quizId);
            return Ok(quizResponse);
        }

        /// <summary>
        /// Created By - vinayak yenpure
        /// Created Date - 28-12-2020
        /// Description - To get Quiz result list by Quiz Id and User id.
        /// </summary>
        /// <param name="quizId">QuizId</param>
        /// <param name="userId">userId</param>
        /// <param name="resourceId">resourceId</param>
        /// <param name="resourceType">resourceType</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizReportCardByUserId")]
        [ProducesResponseType(typeof(QuizResult), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizReportCardByUserId(int quizId, int userId, int resourceId, string resourceType)
        {
            var quizResponse = await _IQuizService.GetQuizReportCardByUserId(quizId, userId, resourceId, resourceType);
            return Ok(quizResponse);
        }

        /// <summary>
        /// Description - To active deactive Quiz.
        /// </summary>
        /// <param name="quizView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ActiveDeactiveSafeQuiz")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> ActiveDeactiveSafeQuiz([FromBody] QuizView quizView)
        {
            var result = _IQuizService.ActiveDeactiveQuiz(quizView.QuizId);
            return Ok(result);
        }
        [HttpPost]
        [Route("ActiveDeactiveOTP")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> ActiveDeactiveOTP([FromBody] QuizView quizView)
        {
            var result = _IQuizService.ActiveDeactiveQuiz(quizView.QuizId);
            return Ok(result);
        }
        [HttpPost]
        [Route("IsSafeQuiz")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> IsSafeQuiz(int QuizId)
        {
            var result =await _IQuizService.IsSafeQuiz(QuizId);
            return Ok(result);
        }


        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 29 SEPT 2020
        /// Description - To clone the quiz data
        /// </summary>
        /// <param name="quizId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("cloneQuizData")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> CloneQuizData(int quizId)
        {
            var result = _IQuizService.CloneQuizData(quizId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 06 OCT 2020
        /// Description - To share the quiz with other teachers
        /// </summary>
        /// <param name="quizId"></param>
        /// <param name="teacherIds"></param>
        /// <param name="SharedBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("shareQuizData")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> ShareQuizData(int quizId, string teacherIds, long SharedBy)
        {
            var result = _IQuizService.ShareQuizData(quizId, teacherIds, SharedBy);
            return Ok(result);
        }


    }
}