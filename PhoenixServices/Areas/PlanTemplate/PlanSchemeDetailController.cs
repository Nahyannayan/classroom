﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.PlanTemplate
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class PlanSchemeDetailController : ControllerBase
    {
        private IPlanSchemeService _planSchemeService;
        public PlanSchemeDetailController(IPlanSchemeService planSchemeService)
        {
            _planSchemeService = planSchemeService;
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/Feb/2020
        /// Description: To get all Plan Scheme detail
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getallplanschemedetail")]
        [ProducesResponseType(typeof(IEnumerable<PlanSchemeDetail>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllPlanSchemeDetail()
        {
            var result = await _planSchemeService.GetAll();
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        if (a.SharedLessonPlanTeacherList != null)
                        {
                            a.SharedLessonPlanTeacherList = a.SharedLessonPlanTeacherList.Select(s =>
                            {
                                s.UserImageFilePath = Helpers.CommonHelper.GemsStudentImagePath(s.UserImageFilePath);
                                return s;
                            }).ToList();
                        }
                        return a;
                    });
                }
            }
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/Feb/2020
        /// Description: To get Plan Scheme by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getplanschemedetailbyid/{id:int}")]
        [ProducesResponseType(typeof(PlanSchemeDetail), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPlanSchemeDetailById(int id)
        {
            var result = await _planSchemeService.GetById(id);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null)
                {
                    if (result.SharedLessonPlanTeacherList != null)
                    {
                        result.SharedLessonPlanTeacherList = result.SharedLessonPlanTeacherList.Select(s =>
                            {
                                s.UserImageFilePath = Helpers.CommonHelper.GemsStudentImagePath(s.UserImageFilePath);
                                return s;
                            }).ToList();
                    }
                }
            }
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/Feb/2020
        /// Description: To get Plan Scheme by schoolId, userId, status and isActive
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userId">nullable</param>
        /// <param name="status">nullable</param>
        /// <param name="isActive">nullable</param>
        /// <param name="SharedWithMe">nullable</param>
        /// <param name="CreatedByMe">nullable</param>
        /// <param name="MISGroupId">nullable</param>
        /// <param name="OtherGroupId">nullable</param>
        /// <param name="CourseId">nullable</param>
        /// <param name="GradeId">nullable</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getplanschemedetailbyschoolid")]
        [ProducesResponseType(typeof(IEnumerable<PlanSchemeDetail>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPlanSchemeDetailBySchoolId(int schoolId, int? userId, int? status, bool? isActive,
            bool? SharedWithMe, bool? CreatedByMe,
            string MISGroupId, string OtherGroupId, string CourseId, string GradeId,
            int? pageIndex = null, int? PageSize = null, string searchString = "", string sortBy = "")
        {
            var result = await _planSchemeService.GetPlanSchemeBySchoolId(schoolId, userId, status, isActive,
                SharedWithMe, CreatedByMe, MISGroupId, OtherGroupId, CourseId, GradeId,
                pageIndex, PageSize, searchString, sortBy);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        if (a.SharedLessonPlanTeacherList != null)
                        {
                            a.SharedLessonPlanTeacherList = a.SharedLessonPlanTeacherList.Select(s =>
                            {
                                s.UserImageFilePath = Helpers.CommonHelper.GemsStudentImagePath(s.UserImageFilePath);
                                return s;
                            }).ToList();
                        }
                        return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/Feb/2020
        /// Description: To get Plan Scheme by planSchemeDetailId, templateId,schoolId, templateType, userId, status and isActive
        /// </summary>
        /// <param name="planSchemeDetailId">nullable</param>
        /// <param name="templateId">nullable</param>
        /// <param name="schoolId">nullable</param>
        /// <param name="templateType">nullable</param>
        /// <param name="status">nullable</param>
        /// <param name="userId">nullable</param>
        /// <param name="isActive">nullable</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getplanschemedetail")]
        [ProducesResponseType(typeof(PlanSchemeDetail), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPlanSchemeDetail(int? planSchemeDetailId, int? templateId, int? schoolId, string templateType, int? status, int? userId, bool? isActive)
        {
            var result = await _planSchemeService.GetPlanSchemeDetail(planSchemeDetailId, templateId, schoolId, templateType, status, userId, isActive);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        if (a.SharedLessonPlanTeacherList != null)
                        {
                            a.SharedLessonPlanTeacherList = a.SharedLessonPlanTeacherList.Select(s =>
                            {
                                s.UserImageFilePath = Helpers.CommonHelper.GemsStudentImagePath(s.UserImageFilePath);
                                return s;
                            }).ToList();
                        }
                        return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/Feb/2020
        /// Description: To create new plan scheme detail
        /// </summary>
        /// <param name="planSchemeDetail"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addplanschemedetail")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult AddPlanSchemeDetail([FromBody] PlanSchemeDetail planSchemeDetail)
        {
            var result = _planSchemeService.Insert(planSchemeDetail);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Shankar Kadam
        /// Created On: 25/July/2020
        /// Description: To Save PlanScheme Fields
        /// </summary>
        /// <param name="planSchemeField"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SavePlanSchemeFields")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult SavePlanSchemeFields([FromBody] List<PlanSchemeField> planSchemeField)
        {
            var result = _planSchemeService.SavePlanSchemeFields(planSchemeField);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Shankar Kadam
        /// Created On: 25/July/2020
        /// Description: To Get Saved PlanScheme Fields by Id
        /// </summary>
        /// <param name="planSchemeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPlanSchemeFields")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> GetPlanSchemeFields(long planSchemeId)
        {
            var result = await _planSchemeService.GetPlanSchemeFields(planSchemeId);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/Feb/2020
        /// Description: To update plan scheme details
        /// </summary>
        /// <param name="planSchemeDetail"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateplanschemedetail")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult UpdatePlanSchemeDetail([FromBody] PlanSchemeDetail planSchemeDetail)
        {
            var result = _planSchemeService.Update(planSchemeDetail);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 29/Feb/2020
        /// Description: To update plan scheme details status
        /// </summary>
        /// <param name="planSchemeDetail"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateplanschemestatus")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult UpdatePlanSchemeDetailStatus([FromBody] PlanSchemeDetail planSchemeDetail)
        {
            var result = _planSchemeService.UpdatePlanSchemeStatus(planSchemeDetail);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/Feb/2020
        /// Description: To delete plan scheme details 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deletedBy"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteplanschemedetail/{id:int}/{deletedBy:int}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteTemplate(int id, int deletedBy)
        {
            var result = _planSchemeService.Delete(id, deletedBy);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Shankar Kadam
        /// Created On: 08/Jul/2020
        /// Description: Update Shared Lesson PlanDetial
        /// </summary>
        /// <param name="PlanSchemeId"></param>
        /// <param name="SelectedTeacherList"></param>
        /// <param name="Operation"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UpdateSharedLessonPlanDetial")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult UpdateSharedLessonPlanDetial(long PlanSchemeId, string SelectedTeacherList, long UserId, string Operation)
        {
            var result = _planSchemeService.UpdateSharedLessonPlanDetial(PlanSchemeId, SelectedTeacherList, UserId, Operation);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Shankar Kadam
        /// Created On: 09/July/2020
        /// Description: Get Shared Lesson Plan Teacher List
        /// </summary>
        /// <param name="PlanSchemeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSharedLessonPlanTeacherList")]
        [ProducesResponseType(typeof(IEnumerable<User>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSharedLessonPlanTeacherList(string PlanSchemeId)
        {
            var result = await _planSchemeService.GetSharedLessonPlanTeacherList(PlanSchemeId);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.UserImageFilePath = Helpers.CommonHelper.GemsStudentImagePath(a.UserImageFilePath);
                        return a;
                    });
                }
            }
            return Ok(result);
        }
        /// <summary>
        /// Created By - Shankar Kadam
        /// Created Date - 10 July 2020
        /// Description - To get topic subtopic structure
        /// </summary>
        /// <param name="groupIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUnitStructure")]
        [ProducesResponseType(typeof(Subject), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnitStructure(string groupIds)
        {
            var structure = await _planSchemeService.GetUnitStructure(groupIds);
            return Ok(structure);
        }

        /// <summary>
        /// Created By - Shankar Kadam
        /// Created Date - 10 July 2020
        /// Description - To lesson plan by filter 
        /// </summary>
        /// <param name="SchoolId"></param>
        /// <param name="TeacherId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetLessonPlanFilterModule")]
        [ProducesResponseType(typeof(LessonPlanFilterModule), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetLessonPlanFilterModule(long SchoolId, long TeacherId)
        {
            var lessonPlanFilterModule = await _planSchemeService.GetLessonPlanFilterModule(SchoolId, TeacherId);
            return Ok(lessonPlanFilterModule);
        }

        /// <summary>
        /// Created By - Shankar Kadam
        /// Created Date - 10 July 2020
        /// Description - To get pending lesson plan for approvla by schoolid
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPendingForApprovalLessonPlan")]
        [ProducesResponseType(typeof(IEnumerable<PlanSchemeDetail>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPendingForApprovalLessonPlan(long schoolId)
        {
            var lessonPlanFilterModule = await _planSchemeService.GetPendingForApprovalLessonPlan(schoolId);
            return Ok(lessonPlanFilterModule);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 27/July/2020
        /// Description: To get lesson plans by unit id
        /// </summary>
        /// <param name="unitId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getplanschemesbyunitid")]
        [ProducesResponseType(typeof(IEnumerable<PlanSchemeDetail>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPlanSchemesByUnitId(long unitId, int? status)
        {
            var result = await _planSchemeService.GetPlanSchemesByUnitId(unitId, status);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 27/July/2020
        /// Description: Get user assigned groups
        /// </summary>
        /// <param name="Userid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUserGroups")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult> GetUserGroups(long Userid)
        {
            var result = await _planSchemeService.GetUserGroups(Userid);
            return Ok(result);
        }


    }
}