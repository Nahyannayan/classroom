﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.PlanTemplate
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class TemplateController : ControllerBase
    {
        private readonly ITemplateService _templateService;
        public TemplateController(ITemplateService templateService)
        {
            _templateService = templateService;
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 06/Feb/2020
        /// Description: To get all Templates
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("gettemplates")]
        [ProducesResponseType(typeof(IEnumerable<Template>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Gettemplates()
        {
            var result = await _templateService.GetAll();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 06/Feb/2020
        /// Description: To get Template by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("gettemplatebyid/{id:int}")]
        [ProducesResponseType(typeof(Template), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTemplateById(int id)
        {
            var result = await _templateService.GetById(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 09/Feb/2020
        /// Description: To get Template by schoolid, userid & active status
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <param name="isActive"></param>
        /// <param name="templateType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("gettemplatesbyschoolid")]
        [ProducesResponseType(typeof(IEnumerable<Template>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GettemplatesBySchoolId(int schoolId, int? userId, bool isActive, string templateType, int? status)
        {
            var result = await _templateService.GetTemplatesBySchoolId(schoolId, userId, isActive, templateType, status);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 06/Feb/2020
        /// Description: To get Template by id, schoolid, templatetype, period, userid, isactive, includetemplatefield. All values are nullable 
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="schoolId"></param>
        /// <param name="templateType"></param>
        /// <param name="period"></param>
        /// <param name="userId"></param>
        /// <param name="isActive"></param>
        /// <param name="includeTemplateField"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("gettemplatedetail")]
        [ProducesResponseType(typeof(Template), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTemplateById(int? templateId, int? schoolId, string templateType, string period, int? userId, bool? isActive, bool? includeTemplateField)
        {
            var result = await _templateService.GetTemplateDetail(templateId, schoolId, templateType, period, userId, isActive, includeTemplateField);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 06/Feb/2020
        /// Description: To get TemplateField by Id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("gettemplatefieldbyid/{id:int}")]
        [ProducesResponseType(typeof(TemplateField), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTemplateFieldById(int id)
        {
            var result = await _templateService.GetTemplateFieldById(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 06/Feb/2020
        /// Description: To get All TemplateField by Template Id 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isActive"></param>
        ///         /// <param name="UserId"></param>
        /// <param name="groupid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("gettemplatefieldbytemplateid/{id:int}/{isActive:bool}/{UserId:long}/{groupid:int}")]
        [ProducesResponseType(typeof(IEnumerable<TemplateField>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTemplateFieldByTemplateId(int id, bool isActive, long UserId,int groupid)
        {
            var result = await _templateService.GetTemplateFieldByTemplateId(id, isActive, UserId, groupid);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 06/Feb/2020
        /// Description: To create new template
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addtemplate")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult AddTemplate([FromBody]Template template)
        {
            var result = _templateService.Insert(template);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 06/Feb/2020
        /// Description: To update existing template
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatetemplate")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult UpdateTemplate([FromBody]Template template)
        {
            var result = _templateService.Update(template);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 06/Feb/2020
        /// Description: To delete template
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletetemplate/{id:int}/{deletedBy:int}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteTemplate(int id, int deletedBy)
        {
            var result = _templateService.Delete(id, deletedBy);
            return Ok(result);
        }

        /// <summary>
        ///    Created By - Rohit patil
        ///    Created Date - 19th feb 2020
        ///    Description - Save Image template mapping data with certificates
        /// </summary>
        /// <param name="templateModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveTemplateImageData")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SaveTemplateImageData([FromBody]Template templateModel)
        {
            bool result = await _templateService.SaveTemplateImageData(templateModel);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19 Feb 2020
        /// Description - get studengt list by teacher group 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="templateId"></param>
        /// <param name="searchString"></param>
        /// <param name="schoolGroupIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCertificateMappingStudents")]
        [ProducesResponseType(typeof(IEnumerable<Student>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCertificateMappingStudents([FromQuery] long userId, [FromQuery]int pageIndex, [FromQuery]int pageSize, [FromQuery] int templateId,
                [FromQuery]string searchString, [FromQuery]string schoolGroupIds)
        {
            var studentList = await _templateService.GetCertificateMappingStudents(userId, pageIndex, pageSize, templateId, schoolGroupIds, searchString);
            return Ok(studentList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 20 Feb 2020
        /// Description - assign deassign students from certificate
        /// </summary>
        /// <param name="certificateMapping"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveTemplateStudentMapping")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SaveTemplateStudentMapping(StudentCertificateMapping certificateMapping)
        {
            bool result = await _templateService.SaveTemplateStudentMapping(certificateMapping);
            return Ok(result);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 20 Feb 2020
        /// Description - assign database columnto template fields
        /// </summary>
        /// <param name="fieldList"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveCertificateAssignedColumns")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SaveCertificateAssignedColumns([FromBody]IEnumerable<TemplateField> fieldList)
        {
            bool result = await _templateService.SaveCertificateAssignedColumns(fieldList);
            return Ok(result);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 20 Feb 2020
        /// Description - get assigned student certificates
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentAssignedCertificates")]
        [ProducesResponseType(typeof(IEnumerable<Template>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetStudentAssignedCertificates([FromQuery]long userId, [FromQuery]PlanTemplateTypes templateType)
        {
            IEnumerable<Template> templateList = await _templateService.GetStudentAssignedCertificates(userId, templateType);
            return Ok(templateList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 21 Feb 2020
        /// Description - get column mapping data of certificate
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTemplateFieldData")]
        [ProducesResponseType(typeof(IEnumerable<TemplateFieldMapping>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetTemplateFieldData([FromQuery]int templateId, [FromQuery]long userId)
        {
            IEnumerable<TemplateFieldMapping> lst = await _templateService.GetTemplateFieldData(templateId, userId);
            return Ok(lst);
        }

        /// <summary>
        /// Created Date - 24th Frb 2020
        /// Created By - Rohit Patil
        /// Description - approve template certificate added by teacher.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveTemplateCertificateStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SaveTemplateCertificateStatus([FromBody]Template model)
        {
            bool result = await _templateService.SaveTemplateCertificateStatus(model);
            return Ok(result);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 24 Feb 2020
        /// Description - Get Teacher list based on grades and school id
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="departmentId"></param>
        /// <param name="templateType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolTemplateApproverDelegates")]
        [ProducesResponseType(typeof(IEnumerable<CertificateTeacherMapping>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetSchoolTemplateApproverDelegates(long schoolId, PlanTemplateTypes templateType, long? departmentId=null)
        {
            IEnumerable<CertificateTeacherMapping> teacherList = new List<CertificateTeacherMapping>();
            teacherList = await _templateService.GetSchoolTemplateApproverDelegates(schoolId, templateType, departmentId);
            return Ok(teacherList);
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 25th Deb 2020
        /// Desciption - Assign/deassign teachers from tempate approval permision
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("SaveCertificateApprovalTeacher")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SaveCertificateApprovalTeacher([FromBody]CertificateTeacherMapping model)
        {
            bool result = await _templateService.SaveCertificateApprovalTeacher(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 16th April 2020
        /// Descriptrion - Get default data for previewing certificate template
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTemplatePreviewData")]
        [ProducesResponseType(typeof(IEnumerable<TemplateFieldMapping>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetTemplatePreviewData([FromQuery]int templateId, [FromQuery] long schoolId)
        {
            var result = await _templateService.GetTemplatePreviewData(templateId, schoolId);
            return Ok(result);
        }

        /// <summary>
        /// Author: Shankar Kadam
        /// Date: 07/05/2020
        /// To get all active certificate column list.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCertificateColumns")]
        [ProducesResponseType(typeof(IEnumerable<CertificateColumns>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCertificateColumns(int certificateColumnType)
        {
            var result = await _templateService.GetCertificateColumns(certificateColumnType);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Shankar Kadam
        /// Created On: 25/July/2020
        /// Description: To  Get TimeTable List 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="SelectDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTimeTableList")]
        [ProducesResponseType(typeof(IEnumerable<TemplateCollectionList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTimeTableList(long UserId, string SelectDate)
        {
            var result = await _templateService.GetTimeTableList(UserId, SelectDate);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Shankar Kadam
        /// Created On: 25/July/2020
        /// Description: To Get Grade List 
        /// </summary>
        /// <param name="Groupid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGradeList")]
        [ProducesResponseType(typeof(IEnumerable<TemplateCollectionList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradeList(long Groupid,long SchoolId)
        {
            var result = await _templateService.GetGradeList(Groupid, SchoolId);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Ashwin Dubey
        /// Created On: 18-Aug-2020
        /// Description: To get the list of template field data mapping for certificate 
        /// </summary>
        /// <param name="template">Template id and Users ids object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetTemplateFieldDataForMultipleUsers")]
        [ProducesResponseType(typeof(IEnumerable<TemplateCollectionList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTemplateFieldDataForMultipleUsers(TemplateMappingData template)
        {
            var result = await _templateService.GetTemplateFieldData(template.TemplateId, template.UserIds, template.DynamicParameters);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 10 Jan 2021
        /// Description - get certificate report data
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet("GetCertificateReportData")]
        [ProducesResponseType(typeof(IEnumerable<CertificateReportView>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCertificateReportData(long schoolId)
        {
            var result = await _templateService.GetCertificateReportData(schoolId);
            return Ok(result);
        }
    }
}