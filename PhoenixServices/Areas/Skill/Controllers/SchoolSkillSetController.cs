﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.Skill.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SchoolSkillSetController : Controller
    {
        private readonly ISchoolSkillSetService _schoolSkillSetService;
        public SchoolSkillSetController(ISchoolSkillSetService schoolSkillSetService)
        {
            _schoolSkillSetService = schoolSkillSetService;
        }

        /// <summary>
        /// Created By - Rohit Karayat
        /// Created Date - 15 Dec 2019
        /// Description - To Get all School Skill Set.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getschoolskillset")]
        [ProducesResponseType(typeof(IEnumerable<SchoolSkillSet>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolSkillSet( int SchoolGradeId, int languageId = 1)
        {
            var SkillList = await _schoolSkillSetService.GetSchoolSkillSet(languageId, SchoolGradeId);
            return Ok(SkillList);
        }

        /// <summary>
        /// Created By - Rohit Karayat
        /// Created Date - 15 Dec 2019
        /// Description - To Get  School Skill Set by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="langaugeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getschoolskillsetById")]
        [ProducesResponseType(typeof(SchoolSkillSet), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolSkillSetById(int id, short langaugeId=1)
        {
            var Suggestion = await _schoolSkillSetService.GetSchoolSkillSetById(id,langaugeId);
            return Ok(Suggestion);
        }

        /// <summary>
        /// Created By - Rohit Karayat
        /// Created Date - 15 Dec 2019
        /// Description - To Insert School Skill Set.
        /// </summary>
        /// <param name="schoolSkillSet"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertSchoolSkillSet")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertSchoolSkillSet([FromBody]SchoolSkillSet schoolSkillSet)
        {
            var result = _schoolSkillSetService.InsertSchoolSkillSet(schoolSkillSet);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Karayat
        /// Created Date - 15 Dec 2019
        /// Description - To Update School Skill Set.
        /// </summary>
        /// <param name="schoolSkillSet"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateSchoolSkillSet")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateSchoolSkillSet([FromBody]SchoolSkillSet schoolSkillSet)
        {
            var result = _schoolSkillSetService.UpdateSchoolSkillSet(schoolSkillSet);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Karayat
        /// Created Date - 15 Dec 2019
        /// Description - To Update School Skill Set.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteSchoolSkillSet/{id:int}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteSchoolSkillSet(int id)
        {
            var result = _schoolSkillSetService.DeleteSchoolSkillSet(id);
            return Ok(result);
        }
    }
}