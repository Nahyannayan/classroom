﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Models;
using Phoenix.API.Services;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;

namespace Phoenix.API.Areas.Skill.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class StudentSkillSetController : ControllerBase
    {
        private readonly IStudentSkillSetService _studentSkillSetService;
        public StudentSkillSetController(IStudentSkillSetService studentSkillSetService)
        {
            _studentSkillSetService = studentSkillSetService;
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 26th January 2020
        /// Description - Get Student skills data by userid of student
        /// </summary>
        /// <param name="id"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getstudentskillsdata")]
        [ProducesResponseType(typeof(IEnumerable<StudentSkills>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentSkillsData(long id,short languageId=1)
        {
            var lstSkillList = await _studentSkillSetService.GetStudentSkillsData(id, languageId);
            return Ok(lstSkillList);
        }

        /// <summary>
        /// Created by  - Rohit Patil
        /// Created Date : 27th january 2020
        ///  Description - Remove and update student skill from predefined list.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateStudentSkillsData")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateStudentSkillsData([FromBody] StudentSkillDTO model)
        {
            var op = await _studentSkillSetService.UpdateStudentSkillsData(model.StudentId, model.CreatedUserId, model.SkillId, Convert.ToChar(StringEnum.GetStringValue(model.Mode)));
            return Ok(op);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 27 Jan 2020
        /// Description - Insert new skill into database. Teacher added will auto approve, Student added need approval
        /// </summary>
        /// <returns>OperationDetails</returns>
        [HttpPost]
        [Route("InsertStudentSkill")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> InsertStudentSkill([FromBody] StudentSkillDTO model)
        {
            var op = await _studentSkillSetService.InsertStudentSkill(model);
            return Ok(op);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 4th Feb 2020
        /// Description - Endorse new skills for student.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateStudentSkillEndorsementDetails")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateStudentSkillEndorsementDetails(SkillEndorsement model)
        {
            bool result = await _studentSkillSetService.UpdateStudentSkillEndorsementDetails(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 4th Feb 2020
        /// Description - Get list of all skills endorsed by student
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>

        [HttpGet]
        [Route("GetAllStudentEndorsedSkills")]
        [ProducesResponseType(typeof(IEnumerable<SkillEndorsement>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllStudentEndorsedSkills([FromQuery] long userId, [FromQuery] int pageIndex, [FromQuery] int pageSize, [FromQuery] long currentUserId, [FromQuery] string searchString = "")
        {
            var studentSkills = await _studentSkillSetService.GetAllStudentEndorsedSkills(userId, pageIndex, pageSize, searchString, currentUserId);
            return Ok(studentSkills);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date -  4th Feb 2020
        /// Description - updated rating and show on dashboard status of endorsed skill
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateEndorseSkillStatusRating")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateEndorseSkillStatusRating(SkillEndorsement model)
        {
            bool result = await _studentSkillSetService.UpdateEndorseSkillStatusRating(model);
            return Ok(result);
        }

        /// <summary>
        /// Created by - Rahul Verma
        /// Created Date -  25th July 2020
        /// Description - updated show on dashboard status of endorsed skill that are checked
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAllEndorsedSkillStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAllEndorsedSkillStatus(List<SkillEndorsement> lstModel)
        {
            bool result = await _studentSkillSetService.UpdateAllEndorsedSkillStatus(lstModel);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 2nd Aug 2020
        /// Description - delete student endorsement skill
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteEndorsedSkill")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteEndorsedSkill(SkillEndorsement model)
        {
            var result = await _studentSkillSetService.DeleteEndorsedSkill(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 02 Aug 2020
        /// Description - retreives student skill by id
        /// </summary>
        /// <param name="skillEndorsementId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentEndorsedSkillById")]
        [ProducesResponseType(typeof(SkillEndorsement), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentEndorsedSkillById(int skillEndorsementId)
        {
            var result = await _studentSkillSetService.GetStudentEndorsedSkillById(skillEndorsementId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Cretaed Date - 5th Aug 2020
        /// Description - approved/rejects student endorsement
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateStudentEndorsementStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateStudentEndorsementStatus(SkillEndorsement model)
        {
            var result = await _studentSkillSetService.UpdateStudentEndorsementStatus(model);
            return Ok(result);
        }
    }
}