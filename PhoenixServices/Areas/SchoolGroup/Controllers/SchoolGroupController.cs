﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Services;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Phoenix.API.Areas.Group.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SchoolGroupController : ControllerBase
    {
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly IConfiguration _config;

        public SchoolGroupController(ISchoolGroupService schoolGroupService, IConfiguration config)
        {
            _schoolGroupService = schoolGroupService;
            _config = config;
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 27 May 2019
        /// Description - To get all groups.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="IsBeSpokeGroup"></param>
        /// <param name="searchString"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolGroups")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGroups(int userId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "")
        {
            var schoolGroupList = await _schoolGroupService.GetSchoolGroups(userId, pageNumber, pageSize, IsBeSpokeGroup, searchString);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Add by ashish for GetOtherschoolgroups
        /// </summary>
        [HttpGet]
        [Route("GetOtherschoolgroups")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetOtherschoolgroups(int userId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "")
        {
            var schoolGroupList = await _schoolGroupService.GetOtherschoolgroups(userId, pageNumber, pageSize, IsBeSpokeGroup, searchString);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 18 OCT 2020
        /// Description - To get school groups for admin space with pagination
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getSchoolGroupsForAdminSpace")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGroupsForAdminSpace(SchoolGroupsReportRequest model)
        {
            var eventList = await _schoolGroupService.GetSchoolGroupsForAdminSpace(model);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 20 OCT 2020
        /// Description - To get all courses by schoolId
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getCoursesBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<Phoenix.Models.Course>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCoursesBySchoolId(long schoolId)
        {
            var schoolCourseList = await _schoolGroupService.GetCoursesBySchoolId(schoolId);
            return Ok(schoolCourseList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 27 May 2019
        /// Description - To get group by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolGroupById/{id:int}")]
        [ProducesResponseType(typeof(SchoolGroup), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGroupById(int id)
        {
            var schoolGroup = await _schoolGroupService.GetSchoolGroupById(id);
            return Ok(schoolGroup);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 27 May 2019
        /// Description - To get group by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolGroupsHavingBlogs/{id:long}")]
        [ProducesResponseType(typeof(SchoolGroup), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGroupsHavingBlogs(long id)
        {
            var schoolGroup = await _schoolGroupService.GetSchoolGroupsHavingBlogs(id);
            return Ok(schoolGroup);
        }

        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 27 May 2019
        /// Description - To insert school Group
        /// </summary>
        /// <param name="schoolGroup"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertSchoolGroup")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertSchoolGroup([FromBody] SchoolGroup schoolGroup)
        {
            var result = _schoolGroupService.InsertSchoolGroup(schoolGroup);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 27 May 2019
        /// Description - To update school group.
        /// </summary>
        /// <param name="schoolGroup"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateSchoolGroup")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateSchoolGroup([FromBody] SchoolGroup schoolGroup)
        {
            var result = _schoolGroupService.UpdateSchoolGroupData(schoolGroup);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 27 may 2019
        /// Description - To delete group.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteSchoolGroup")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteSchoolGroup(int id, int UserId = 0)
        {
            var result = _schoolGroupService.DeleteSchoolGroup(id, UserId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 9 july 2019
        /// Description - To get all groups belongs to student.
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSchoolGroupsByStudentId")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGroupsByStudentId(long studentId)
        {
            var schoolGroupList = await _schoolGroupService.GetSchoolGroupsByStudentId(studentId);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 10 july 2019
        /// Description - To get all groups belongs to student page wise.
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="IsBespokeGroup"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentGroups")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string SearchString = "")
        {
            var schoolGroupList = await _schoolGroupService.GetStudentGroups(studentId, PageNumber, PageSize, IsBespokeGroup, SearchString);
            return Ok(schoolGroupList);
        }

        /// Add by ashish from GetOtherschoolStudentGroups
        [HttpGet]
        [Route("GetOtherschoolStudentGroups")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetOtherschoolStudentGroups(long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string SearchString = "")
        {
            var schoolGroupList = await _schoolGroupService.GetOtherschoolStudentGroups(studentId, PageNumber, PageSize, IsBespokeGroup, SearchString);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By - Mahesh C.
        /// Created Date - 10 july 2019
        /// Description - To get all groups belongs to student page wise.
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsubjectschoolgroup")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubjectSchoolGroup(int subjectId, int schoolId)
        {
            var schoolGroupList = await _schoolGroupService.GetSubjectSchoolGroup(subjectId, schoolId);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 22 July 2019
        /// Description - To update teacher given name in school group table.
        /// </summary>
        /// <param name="schoolGroup"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateTeacherGivenName")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateTeacherGivenName([FromBody] SchoolGroup schoolGroup)
        {
            var result = _schoolGroupService.UpdateTeacherGivenName(schoolGroup);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 28/July/2019
        /// Description: To get all school groups by school id
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSchoolGroupsBySchoolId/{schoolid:int}")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGroupsBySchoolId(long schoolId)
        {
            var schoolGroupList = await _schoolGroupService.GetSchoolGroupsBySchoolId(schoolId);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 11/August/2019
        /// Description: To get all school groups by teacher or student id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isTeacher"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSchoolGroupsByUserId/{userid:int}/{isteacher:bool}")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGroupsByUserId(long userId, bool isTeacher)
        {
            var schoolGroupList = await _schoolGroupService.GetSchoolGroupsByUserId(userId, isTeacher);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By: Pranav Deo
        /// Created On: 18/August/2019
        /// Description: To get all unassigned members
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="systemLanguageId"></param>
        /// <param name="schoolId"></param>
        /// <param name="userTypeId"></param>
        /// <param name="schoolGroupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getUnassignedMembers")]
        [ProducesResponseType(typeof(IEnumerable<ListItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnassignedMembers(short systemLanguageId, int schoolId, long userId, int? userTypeId, int schoolGroupId)
        {
            var schoolGroupList = await _schoolGroupService.GetUnassignedMembers(systemLanguageId, schoolId, userId, userTypeId, schoolGroupId);
            return Ok(schoolGroupList);
        }

        [HttpGet]
        [Route("GetUnassignedMembersWithSelectedGroup")]
        [ProducesResponseType(typeof(IEnumerable<ListItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUnassignedMembersWithSelectedGroup(short systemLanguageId, int schoolId, long userId, int? userTypeId, int schoolGroupId, string selectedSchoolGroupIds)
        {
            var schoolGroupList = await _schoolGroupService.GetUnassignedMembers(systemLanguageId, schoolId, userId, userTypeId, schoolGroupId, selectedSchoolGroupIds);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Description - To add or update a member in a group.
        /// </summary>
        /// <param name="groupMemberMapping"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddUpdateGroupMember")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddUpdateGroupMember([FromBody] List<GroupMemberMapping> groupMemberMapping)
        {
            var result = _schoolGroupService.AddUpdateGroupMember(groupMemberMapping);
            return Ok(result);
        }

        /// <summary>
        /// Description - To get list of assigned members of the group.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAssignedMembers")]
        [ProducesResponseType(typeof(IEnumerable<ListItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignedMembers(short systemLanguageId, int schoolGroupId)
        {
            var schoolGroupList = await _schoolGroupService.GetAssignedMembers(systemLanguageId, schoolGroupId);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (schoolGroupList != null && schoolGroupList.Count() > 0)
                {
                    schoolGroupList = schoolGroupList.Select(a =>
                    {
                        a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a;
                    });
                }
            }
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 23 Aug 2019
        /// Description - To Insert Group message.
        /// </summary>
        /// <param name="groupMessage"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertGroupMessage")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertGroupMessage([FromBody] GroupMessage groupMessage)
        {
            var result = _schoolGroupService.InsertGroupMessage(groupMessage);
            return Ok(result);
        }

        /// <summary>
        /// Description - To get list of messages in a group.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getGroupMessageListByGroup")]
        [ProducesResponseType(typeof(IEnumerable<GroupMessage>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGroupMessageListByGroup(int groupId, long id)
        {
            var groupMessageList = await _schoolGroupService.GetGroupMessageListByGroup(groupId, id);
            return Ok(groupMessageList);
        }

        /// <summary>
        /// Created By -Mahesh Chikhale
        /// Created Date - 30 Oct 2019
        /// Description - To update lastseen of User in group
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="schoolGroupId"></param>
        /// <param name="isParent"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UpdateLastSeen")]
        public async Task<ActionResult> UpdateLastSeen(long userId, int schoolGroupId, bool isParent)
        {
            var result = _schoolGroupService.UpdateLastSeen(userId, schoolGroupId, isParent);
            return Ok(result);
        }

        /// <summary>
        /// Created By -Sonali Shinde
        /// Created Date - 6 Dec 2020
        /// Description - To update school group
        /// </summary>
        /// <param name="schoolGroupIds"></param>
        /// <param name="teacherId"></param>

        /// <returns></returns>
        [HttpGet]
        [Route("UpdateArchiveSchoolGroup")]
        public async Task<ActionResult> UpdateArchiveSchoolGroup(string schoolGroupIds, long teacherId)
        {
            var result = _schoolGroupService.UpdateArchiveSchoolGroup(schoolGroupIds, teacherId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - sonali shinde
        /// Created Date - 7 jan 2020
        /// Description - To get Archived school group by teacher Id.
        /// </summary>
        /// <param name="teacherId"></param>
        /// <param name="isTeacher"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetArchivedSchoolGroupsByUserId")]
        [ProducesResponseType(typeof(Task<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetArchivedSchoolGroupsByUserId(long teacherId, bool isTeacher)
        {
            var archivedGroupList = await _schoolGroupService.GetArchivedSchoolGroupsByUserId(teacherId, isTeacher);
            return Ok(archivedGroupList);
        }

        /// <summary>
        /// Created By - sonali shinde
        /// Created Date - 7 jan 2020
        /// Description - To get Active school group by teacher Id.
        /// </summary>
        /// <param name="teacherId"></param>
        /// <param name="isTeacher"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetActiveSchoolGroupsByUserId")]
        [ProducesResponseType(typeof(Task<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetActiveSchoolGroupsByUserId(long teacherId, bool isTeacher)
        {
            var activeGroupList = await _schoolGroupService.GetActiveSchoolGroupsByUserId(teacherId, isTeacher);
            return Ok(activeGroupList);
        }

        /// <summary>
        /// Created By - Sonali Shinde
        /// Created Date - 13th Jan 2020
        /// Description - To get all Archived groups.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="IsBeSpokeGroup"></param>
        /// <param name="searchString"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetArchivedSchoolGroups")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetArchivedSchoolGroups(int userId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string searchString = "")
        {
            var schoolGroupList = await _schoolGroupService.GetArchivedSchoolGroups(userId, pageNumber, pageSize, IsBeSpokeGroup, searchString);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Cretaed By - Rohit Patil
        /// Created Date - 16th March 2020
        /// Description - insert newly created meeting id on database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateGroupMeeting")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateGroupMeeting([FromBody] MeetingResponse model)
        {
            bool result = await _schoolGroupService.UpdateGroupMeeting(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 17th March 2020
        /// Description - Remove school group meeting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("RemoveGroupMeeting")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> RemoveGroupMeeting([FromBody] SchoolGroup model)
        {
            bool result = await _schoolGroupService.RemoveGroupMeeting(model.MeetingRoomId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19th April 2020
        /// Description - Add/update ms teams meeting information of each school group
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateTeamsMeetingInfo")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateTeamsMeetingInfo([FromBody] MSTeamsReponse model)
        {
            var id = await _schoolGroupService.UpdateTeamsMeetingInfo(model);
            return Ok(id);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 23 March 2020
        /// Description - Get All Previous Lessons
        /// </summary>
        /// <param name="schoolGroupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllTeamsMeetings")]
        [ProducesResponseType(typeof(IEnumerable<MSTeamsReponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllTeamsMeetings(int schoolGroupId)
        {
            var result = await _schoolGroupService.GetAllTeamsMeetings(schoolGroupId);
            return Ok(result);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 26 April 2020
        /// Description - retreives all zoom meeting by school group
        /// </summary>
        /// <param name="schoolGroupId"></param>
        /// <param name="meetingId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllZoomMeetings")]
        [ProducesResponseType(typeof(IEnumerable<ZoomMeetingResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllZoomMeetings(int schoolGroupId, string meetingId, long userId)
        {
            var result = await _schoolGroupService.GetAllZoomMeetings(schoolGroupId, meetingId, userId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 23 June 2020
        /// Description - Added/save zoom meeting participants
        /// </summary>
        /// <param name="meetingParticipants"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateZoomMeetingParticipants")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateZoomMeetingParticipants(List<ZoomMeetingResponse> meetingParticipants)
        {
            bool result = await _schoolGroupService.UpdateZoomMeetingParticipants(meetingParticipants);
            return Ok(result);
        }

        /// <summary>
        /// Created BY - Rohit Patil
        /// Created Date - 26 April 2020
        /// Description - UPdated Zoom Meeting information
        /// </summary>
        /// <param name="zoomMeetingResponse"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateZoomMeetingInfo")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateZoomMeetingInfo([FromBody] ZoomMeetingResponse zoomMeetingResponse)
        {
            var result = await _schoolGroupService.UpdateZoomMeetingInfo(zoomMeetingResponse);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 29th April 2020
        /// Description - Save adobe connect meeting of school groups
        /// </summary>
        /// <param name="meetingInfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveGroupAdobeMeeting")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveGroupAdobeMeeting(AdobeConnectMeeting meetingInfo)
        {
            bool result = await _schoolGroupService.SaveGroupAdobeMeeting(meetingInfo);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Cretaed Date - 29th April 2020
        /// Description - Retreive all adobe meetings
        /// </summary>
        /// <param name="schoolGroupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllAdobeMeetings")]
        [ProducesResponseType(typeof(IEnumerable<AdobeConnectMeeting>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllAdobeMeetings(int schoolGroupId)
        {
            var result = await _schoolGroupService.GetAllAdobeMeetings(schoolGroupId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 3rd May 2020
        /// Description - retreives all big blue button meetings by school group
        /// </summary>
        /// <param name="schoolGroupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllGroupBBBMeetings")]
        [ProducesResponseType(typeof(IEnumerable<MeetingResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllGroupBBBMeetings(int? schoolGroupId, string meetingId = null)
        {
            var result = await _schoolGroupService.GetAllGroupBBBMeetings(schoolGroupId, meetingId);
            return Ok(result);
        }

        /// <summary>
        /// Created BY - Rohit Patil
        /// Created Date - 6th May 2020
        /// Description - Get current date user meetings list
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCurrentGroupMeeting")]
        [ProducesResponseType(typeof(IEnumerable<EventView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCurrentGroupMeeting(long userId)
        {
            var result = await _schoolGroupService.GetCurrentGroupMeeting(userId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 11 May 2020
        /// Description - retreives all web ex meetings of school groups
        /// </summary>
        /// <param name="schoolGroupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllGroupWebExMeetings")]
        [ProducesResponseType(typeof(IEnumerable<WebExMeetingData>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllGroupWebExMeetings(int schoolGroupId)
        {
            var result = await _schoolGroupService.GetAllGroupWebExMeetings(schoolGroupId);
            return Ok(result);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created By - 11 May 2020
        /// Description - updates group web ex meeting
        /// </summary>
        /// <param name="meetingData"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateGroupWebExMeeting")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateGroupWebExMeeting([FromBody] WebExMeetingData meetingData)
        {
            bool result = await _schoolGroupService.UpdateGroupWebExMeeting(meetingData);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Cretaed Date - 27th May 2020
        /// Description - get user schoolgroup bespoke and normal groups combines
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolGroupWithPagination")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGroupWithPagination(long userId, short pageSize, int pageNumber)
        {
            var result = await _schoolGroupService.GetSchoolGroupWithPagination(userId, pageSize, pageNumber);
            return Ok(result);
        }

        /// New Exampler API For Filtration school level
        /// <summary>
        /// Created By - Deependra Sharma
        /// Cretaed Date - 08 July 2020
        /// Description - get all School level
        /// </summary>
        /// /// <param name="schoolId"></param>
        /// <returns></returns>

        [HttpGet]
        [Route("GetAllSchoolLevel")]
        [ProducesResponseType(typeof(IEnumerable<SchoolLevel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllSchoolLevel(long schoolId)
        {
            var result = await _schoolGroupService.GetAllSchoolLevel(schoolId);
            return Ok(result);
        }

        /// New Exampler API For Filtration  Department
        /// <summary>
        /// Created By - Deependra Sharma
        /// Cretaed Date - 08 July 2020
        /// Description - get all Department
        /// </summary>
        /// /// <param name="schoolId"></param>
        /// <returns></returns>
        ///

        [HttpGet]
        [Route("GetAllSchoolDepartment")]
        [ProducesResponseType(typeof(IEnumerable<SchoolDepartment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllSchoolDepartment(long schoolId)
        {
            var result = await _schoolGroupService.GetAllSchoolDepartment(schoolId);
            return Ok(result);
        }

        /// New Exampler API For Filtration  Department
        /// <summary>
        /// Created By - Deependra Sharma
        /// Cretaed Date - 08 July 2020
        /// Description - get all Department using SchoolId and SchoolLevelId
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="schoolLevelId"></param>
        /// <returns></returns>
        ///

        [HttpGet]
        [Route("GetAllSchoolDepartmentBySchoolLevel")]
        [ProducesResponseType(typeof(IEnumerable<SchoolDepartment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllSchoolDepartmentBySchoolLevel(long schoolId, long schoolLevelId)
        {
            var result = await _schoolGroupService.GetAllSchoolDepartmentBySchoolLevel(schoolId, schoolLevelId);
            return Ok(result);
        }

        // New Exampler API For Filtration  Department
        /// <summary>
        /// Created By - Deependra Sharma
        /// Cretaed Date - 08 July 2020
        /// Description - get all Department using SchoolId and SchoolLevelId
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="schoolLevelId"></param>
        /// <param name="departmentlId"></param>
        /// <returns></returns>
        ///

        [HttpGet]
        [Route("GetAllSchoolCoursesBySchoolLevelAndSchoolIdAndDepartmentId")]
        [ProducesResponseType(typeof(IEnumerable<SchoolCourse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllSchoolCoursesBySchoolLevelAndSchoolIdAndDepartmentId(long schoolId, long schoolLevelId, long departmentlId)
        {
            var result = await _schoolGroupService.GetAllSchoolCoursesBySchoolLevelAndSchoolIdAndDepartmentId(schoolId, schoolLevelId, departmentlId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 13th July 2020
        /// Description - returns user google/microsoft email addresses
        /// </summary>
        /// <param name="schoolGroupIds"></param>
        /// <param name="studentIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolGroupUserEmailAddress")]
        [ProducesResponseType(typeof(IEnumerable<UserEmailAccountView>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetSchoolGroupUserEmailAddress(string schoolGroupIds, string studentIds)
        {
            var result = await _schoolGroupService.GetSchoolGroupUserEmailAddress(schoolGroupIds, studentIds);
            return Ok(result);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 21st July 2020
        /// Descrition :- create syncronoud lesson and insert into database
        /// </summary>
        /// <param name="meetingInfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateSynchounousLesson")]
        [ProducesResponseType(typeof(ZoomMeetingResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateSynchounousLesson(ZoomMeetingView meetingInfo)
        {
            meetingInfo.IsSSOEnabled = Convert.ToBoolean(_config.GetValue<string>("SynchronousLesson:SSO"));
            meetingInfo.AccessToken = _schoolGroupService.GetZoomMeetingAccessToken();
            var meetInfo = await SynchronousClassHelper.CreateZoomMeeting(meetingInfo);
            if (!string.IsNullOrEmpty(meetInfo.id))
            {
                meetInfo.IsSSOEnabled = meetingInfo.IsSSOEnabled;
                meetInfo.SchoolGroupId = meetingInfo.SchoolGroupId;
                meetInfo.CreatedBy = meetingInfo.CreatedBy;
                long meetingMasterId = await _schoolGroupService.UpdateZoomMeetingInfo(meetInfo);
                if (!meetingInfo.IsSSOEnabled)
                {
                    List<ZoomMeetingResponse> meetingParticipants = new List<ZoomMeetingResponse>();
                    var students = await _schoolGroupService.GetAssignedMembers(1, meetingInfo.SchoolGroupId);
                    //students = students.Where(e => e.UserTypeId == 1);
                    if (Helpers.CommonHelper.StudentImageHasFullPath)
                    {
                        if (students != null && students.Count() > 0)
                        {
                            students = students.Select(a =>
                            {
                                a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a;
                            });
                        }
                    }
                    foreach (var item in students.Where(e => !string.IsNullOrEmpty(e.Email)))
                    {
                        var registrantsData = await SynchronousClassHelper.AddZoomMeetingRegistrants(item, meetInfo.id, meetingInfo.AccessToken);
                        if (!string.IsNullOrEmpty(registrantsData.join_url))
                            meetingParticipants.Add(new ZoomMeetingResponse { join_url = registrantsData.join_url, id = meetInfo.id, UserId = item.MemberId });
                    }
                    if (meetingParticipants.Count > 0)
                    {
                        bool result = await _schoolGroupService.UpdateZoomMeetingParticipants(meetingParticipants);
                    }
                }
            }
            return Ok(meetInfo);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19th Aug 2020
        /// Description - Get all recorded session of a group
        /// </summary>
        ///  <param name="groupId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchText"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGroupRecordedSessions")]
        [ProducesResponseType(typeof(IEnumerable<RecordedSessionView>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetGroupRecordedSessions(int groupId, int pageIndex, int pageSize, string searchText, string fromDate, string toDate)
        {
            var result = await _schoolGroupService.GetGroupRecordedSessions(groupId, pageIndex, pageSize, searchText, fromDate, toDate);
            return Ok(result);
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 20 Aug 2020
        /// Description - Get All sessions for SLT users
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchText"></param>
        /// <param name="type"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllLiveSessions")]
        [ProducesResponseType(typeof(IEnumerable<ZoomMeetingView>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllLiveSessions(long userId, int pageIndex, int pageSize, string searchText, string type, string groupId)
        {
            var result = await _schoolGroupService.GetAllLiveSessions(userId, pageIndex, pageSize, searchText, type, groupId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 20 Aug 2020
        /// Description - Generate join  meeting url for users
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GenerateZoomMeetingJoinURL")]
        [ProducesResponseType(typeof(ZoomMeetingView), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GenerateZoomMeetingJoinURL([FromBody] ZoomMeetingView model)
        {
            model.AccessToken = _schoolGroupService.GetZoomMeetingAccessToken();
            var registrantsData = await SynchronousClassHelper.AddZoomMeetingRegistrants(new GroupMemberMapping { Email = model.ZoomEmail, FirstName = model.FirstName, LastName = model.LastName }, model.ZoomMeetingId, model.AccessToken);
            if (!string.IsNullOrEmpty(registrantsData.join_url))
            {
                await _schoolGroupService.SaveUserMeetingJoinURL(new ZoomMeetingResponse { join_url = registrantsData.join_url, id = model.ZoomMeetingId, UserId = model.CreatedBy });
            }
            return Ok(new ZoomMeetingView { MeetingURL = registrantsData.join_url });
        }

        /// <summary>
        /// CREATED BY - Rohit Patil
        /// CREATED DATE - 04 SEPT 2020
        /// DESCRIPTION - Get unique access token for zoom user meeting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GenerateUserLiveSessionInfo")]
        [ProducesResponseType(typeof(ZoomMeetingView), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GenerateUserLiveSessionInfo([FromBody] ZoomMeetingJoinView model)
        {
            MeetingJoinResponse responseModel = new MeetingJoinResponse();
            var zoomAccessToken = _schoolGroupService.GetZoomMeetingAccessToken();
            var result = await SynchronousClassHelper.GenerateZoomUserAccessToken(model.Email, zoomAccessToken);
            if (!string.IsNullOrEmpty(result.code) && result.code == "1001") // User Not present
            {
                model.UserLicenceType = _config.GetValue<short>("SynchronousLesson:UserLicenceType");
                var userResponse = await SynchronousClassHelper.AddZoomUser(model, zoomAccessToken);
                responseModel.Code = userResponse.code;
                responseModel.Message = userResponse.message;
                if (!string.IsNullOrEmpty(userResponse.code))
                    return Ok(responseModel);
                else
                {
                    result = await SynchronousClassHelper.GenerateZoomUserAccessToken(model.Email, zoomAccessToken);
                }
            }
            if (!string.IsNullOrEmpty(model.MeetingURL) && model.MeetingURL.Contains("?"))
                responseModel.MeetingURL = $"{model.MeetingURL}&zak={result.token}";
            else responseModel.MeetingURL = $"{model.MeetingURL}?zak={result.token}";
            responseModel.UserToken = result.token;
            return Ok(responseModel);
        }

        /// <summary>
        /// CREATED BY - Rohit Patil
        /// CREATED DATE-
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteLiveSession")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteLiveSession(ZoomMeetingView model)
        {
            bool result = await _schoolGroupService.DeleteLiveSession(model);
            if (result)
                await SynchronousClassHelper.DeleteMeeting(model.ZoomMeetingId, _schoolGroupService.GetZoomMeetingAccessToken());
            return Ok(result);
        }

        /// <summary>
        /// CREATED BY - Rohit Patil
        /// CREATED DATE-
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteTeamsSession")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteTeamsSession(MSTeamsReponse model)
        {
            bool result = await _schoolGroupService.DeleteTeamsSession(model);
            return Ok(result);
        }

        /// <summary>
        /// CREATED BY - Rohit Patil
        /// CREATED DATE - 27th Oct 2020
        /// DESCRIPTION - get selected users mailing address
        /// </summary>
        /// <param name="userIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUsersMailingDetails")]
        [ProducesResponseType(typeof(IEnumerable<GroupMemberMapping>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetUsersMailingDetails(string userIds)
        {
            var result = await _schoolGroupService.GetMemberMailingDetails(userIds);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 3-Dec-2020
        /// Description: To insert group primary teacher record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertgroupprimaryteacher")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult InsertGroupPrimaryTeacher([FromBody] GroupPrimaryTeacher model)
        {
            var result = _schoolGroupService.InsertGroupPrimaryTeacher(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 3-Dec-2020
        /// Description: To delete group primary teacher record 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletegroupprimaryteacher/{id:long}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteGroupPrimaryTeacher(int id)
        {
            var result = _schoolGroupService.DeleteGroupPrimaryTeacher(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 9-Dec-2020
        /// Description: To update school groups to hide or show class groups
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateschoolgrouptohide")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult UpdateSchoolGroupToHide([FromBody] SchoolGroup model)
        {
            bool result = _schoolGroupService.UpdateSchoolGroupsToHideClassGroups(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 22 DEC 2020
        /// Description - To check if the student groups available
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="IsBespokeGroup"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("checkStudentGroupsAvailable")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> CheckStudentGroupsAvailable(long studentId, int IsBespokeGroup = 0)
        {
            var result = _schoolGroupService.CheckStudentGroupsAvailable(studentId, IsBespokeGroup);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Shankar Kadam
        /// Created Date - 30 DEC 2020
        /// Description - Get Disabled Group List so user cannot shared same post with same group
        /// </summary>
        /// <param name="BlogId"></param>
        /// <param name="SchoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDisabledGroupList")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetDisabledGroupList(long BlogId, long SchoolId )
        {
            var result = _schoolGroupService.GetDisabledGroupList(BlogId, SchoolId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Shankar Kadam
        /// Created Date - 30 DEC 2020
        /// Description - update blog with new update
        /// </summary>
        /// <param name="UpdateBlogId"></param>
        /// <param name="UpdatFromBlogId"></param>
        /// <param name="IsApproved"></param>
        /// <param name="IsRejected"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UpdateBlogWithNewUpdate")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateBlogWithNewUpdate(long UpdateBlogId, long UpdatFromBlogId, bool IsApproved, bool IsRejected)
        {
            var result =  _schoolGroupService.UpdateBlogWithNewUpdate(UpdateBlogId, UpdatFromBlogId, IsApproved, IsRejected);
            return Ok(result);
        }
    }
}