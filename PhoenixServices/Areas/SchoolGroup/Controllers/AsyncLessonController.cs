﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Phoenix.API.Services;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;


namespace Phoenix.API.Areas
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class AsyncLessonController : ControllerBase
    {

        private readonly IAsyncLessonService _asyncLessonService;
        public AsyncLessonController(IAsyncLessonService asyncLessonService)
        {
            _asyncLessonService = asyncLessonService;
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 20/July/2020
        /// Description: Insert async lesson detail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertAsyncLesson")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult InsertAsyncLesson([FromBody] AsyncLesson model)
        {
            var result = _asyncLessonService.InsertAsyncLesson(model);
            return Ok(result);
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 20/July/2020
        /// Description: To update aync lesson detail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAsyncLesson")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult UpdateAsyncLesson([FromBody] AsyncLesson model)
        {
            var result = _asyncLessonService.UpdateAsyncLesson(model);
            return Ok(result);
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 20/July/2020
        /// Description: To delete async lesson by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteasynclesson/{id:long}/{userId:long}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        public IActionResult DeleteAsyncLesson(int id, long userId)
        {
            var result = _asyncLessonService.DeleteAsyncLesson(id, userId);
            return Ok(result);
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 20/July/2020
        /// Description: To get list asynclesson by parameters
        /// </summary>
        /// <param name="asyncLessonId"></param>
        /// <param name="sectionId"></param>
        /// <param name="moduleId"></param>
        /// <param name="folderId"></param>
        /// <param name="isActive"></param>
        /// <param name="IsIncludeResources"></param>
        /// <param name="IsIncludeStudents"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getasynclesson")]
        [ProducesResponseType(typeof(ViewAsyncLessonDetail), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAsyncLesson(long? asyncLessonId, long? sectionId, long? moduleId, long? folderId, bool? isActive, int? IsIncludeResources, int? IsIncludeStudents)
        {
            var modelList = await _asyncLessonService.GetAsyncLessons(asyncLessonId, sectionId, moduleId, folderId, isActive, IsIncludeResources, IsIncludeStudents);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (modelList != null)
                {
                    modelList.StudentList = modelList.StudentList.Select(a =>
                    {
                        a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a;
                    }).ToList();
                }
            }

            return Ok(modelList);
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 20/July/2020
        /// Description: To get async lesson by id
        /// </summary>
        /// <param name="asyncLessonId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getasynclessonbyid")]
        [ProducesResponseType(typeof(AsyncLesson), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAsyncLessonById(long asyncLessonId, bool? isActive)
        {
            var modelList = await _asyncLessonService.GetAsyncLessonById(asyncLessonId, isActive);
            return Ok(modelList);
        }


        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 28/July/2020
        /// Description: To insert async lesson resources and activity
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertResourceActivity")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult InsertResourceActivity([FromBody] List<AsyncLessonResourcesActivities> model)
        {
            var result = _asyncLessonService.InsertResourceActivity(model);
            return Ok(result);
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 28/July/2020
        /// Description: To update async lesson resources and activity
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateResourceActivity")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult UpdateResourceActivity([FromBody] AsyncLessonResourcesActivities model)
        {
            var result = _asyncLessonService.UpdateResourceActivity(model);
            return Ok(result);
        }


        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 28/July/2020
        /// Description: To delete async lesson resources and activity by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteresourceactivity/{id:long}/{userId:long}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        public IActionResult DeleteResouceActivity(long id, long userId)
        {
            var result = _asyncLessonService.DeleteResourceActivity(id, userId);
            return Ok(result);
        }


        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 28/July/2020
        /// Description: To get async lesson resources and activity by parameters
        /// </summary>
        /// <param name="asyncLessonId"></param>
        /// <param name="userId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getresourcesactivities")]
        [ProducesResponseType(typeof(IEnumerable<AsyncLessonResourcesActivities>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetResourcesActivities(long asyncLessonId, long userId, bool? isActive)
        {
            var modelList = await _asyncLessonService.GetResourceActivity(asyncLessonId, userId, isActive);
            return Ok(modelList);
        }


        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 28/July/2020
        /// Description: To update async lesson student mapping table record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Updatestudentmapping")]
        [ProducesResponseType(typeof(long), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult UpdateStudentMapping([FromBody] AsyncLessonStudentMapping model)
        {
            var result = _asyncLessonService.UpdateStudentMapping(model);
            return Ok(result);
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 28/July/2020
        /// Description: To get async lesson student mapping records 
        /// </summary>
        /// <param name="asyncLessonId"></param>
        /// <param name="userId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getstudentmappingbyasynclessonId")]
        [ProducesResponseType(typeof(IEnumerable<AsyncLessonStudentMapping>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAsyncLessonStudentMappingByAsyncLessonId(long asyncLessonId, long? userId, bool? isActive)
        {
            var modelList = await _asyncLessonService.GetAsyncLessonStudentMappingByAsyncLessonId(asyncLessonId, userId, isActive);
            return Ok(modelList);
        }


        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 28/July/2020
        /// Description: To insert into async lesson comment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertComment")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult InsertComment([FromBody] AsyncLessonComments model)
        {
            var result = _asyncLessonService.InsertComment(model);
            return Ok(result);
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 28/July/2020
        /// Description: To update into async lesson comment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateComment")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult UpdateComment([FromBody] AsyncLessonComments model)
        {
            var result = _asyncLessonService.UpdateComment(model);
            return Ok(result);
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 28/July/2020
        /// Description: To delete from async lesson comment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletecomment/{id:long}/{userId:long}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        public IActionResult DeleteComment(long id, long userId)
        {
            var result = _asyncLessonService.DeleteComment(id, userId);
            return Ok(result);
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 28/July/2020
        /// Description: To get list asynclesson comments by parameters 
        /// </summary>
        /// <param name="asyncLessonCommentId"></param>
        /// <param name="resourceActivityId"></param>
        /// <param name="studentId"></param>
        /// <param name="asyncLessonId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getasynclessoncomments")]
        [ProducesResponseType(typeof(IEnumerable<AsyncLessonComments>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAsyncLessonComments(long? asyncLessonCommentId, long? resourceActivityId, long studentId, long asyncLessonId)
        {
            var modelList = await _asyncLessonService.GetAsyncLessonComments(asyncLessonCommentId, resourceActivityId, studentId, asyncLessonId);
            return Ok(modelList);
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 28/July/2020
        /// Description: To update async lesson resources and activity status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateresourceactivitystatus")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult UpdateResourcesActivityStatus([FromBody] UpdateAsyncLessonResourcesActivity model)
        {
            var result = _asyncLessonService.UpdateResourceActivityStatus(model);
            return Ok(result);
        }
    }
}