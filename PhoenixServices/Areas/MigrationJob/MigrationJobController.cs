﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.API.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.MigrationJob.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MigrationJobController : ControllerBase
    {
        private readonly IMigrationJobService _IMigrationJobService;
        public MigrationJobController(IMigrationJobService iMigrationJobService)
        {
            _IMigrationJobService = iMigrationJobService;
        }

        [HttpGet]
        [Route("GetMigrationDetailList")]
        [ProducesResponseType(typeof(IEnumerable<MigrationDetail>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMigrationDetailList(long SchoolId)
        {
            var MigrationDetailList = await _IMigrationJobService.GetMigrationDetailList(SchoolId);
            return Ok(MigrationDetailList);
        }

        [HttpPost]
        [Route("GetSyncDetailbyId")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSyncDetailbyId(SyncDetailById obj)
        {
            var result = await _IMigrationJobService.GetSyncDetailbyId(obj);
            return Ok(result);
        }
        
        [HttpGet]
        [Route("GetMigrationDetailCount")]
        [ProducesResponseType(typeof(IEnumerable<MigrationDetailCount>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMigrationDetailCount()
        {
            var MigrationDetailCount = await _IMigrationJobService.GetMigrationDetailCount();
            return Ok(MigrationDetailCount);
        }


    }
}