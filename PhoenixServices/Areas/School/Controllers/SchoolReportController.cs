﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Models;
using Phoenix.API.Services;
using Phoenix.Common.Helpers;
using Phoenix.Models;

namespace Phoenix.API.Areas.School.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SchoolReportController : ControllerBase
    {
        private readonly ISchoolReportService schoolReportsService;

        public SchoolReportController(ISchoolReportService schoolReportsService)
        {
            this.schoolReportsService = schoolReportsService;
        }
        /// <summary>
        /// Created By : Ashwin Dubey
        /// Created On : 13-09-2020
        /// Description: To save/update school report setup
        /// </summary>
        /// <param name="schoolReports"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SchoolReportCU")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SchoolReportCU(SchoolReports schoolReports)
        {
            var result = await schoolReportsService.SchoolReportCU(schoolReports);
            return Ok(result);
        }

        /// <summary>
        /// Created By : Ashwin Dubey
        /// Created On : 14-09-2020
        /// Description: To get report list data
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetReportLists")]
        [ProducesResponseType(typeof(IEnumerable<ReportListModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetReportLists(long schoolId)
        {
            var result = await schoolReportsService.GetReportLists(schoolId);
            return Ok(result);
        }
        /// <summary>
        /// Created By : Ashwin Dubey
        /// Created On : 14-09-2020
        /// Description: To get report details
        /// </summary>
        /// <param name="reportDetailId"></param>
        ///  <param name="isCommonReport"></param>
        ///  <param name="schoolId"></param>
        ///  <param name="isPreview"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetReportDetail")]
        [ProducesResponseType(typeof(ReportDetailModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetReportDetail(long reportDetailId, bool isCommonReport, long schoolId, bool isPreview = false)
        {
            var result = await schoolReportsService.GetReportDetail(reportDetailId, isPreview, isCommonReport, schoolId);
            return Ok(result);
        }

        /// <summary>
        /// Created By : Ashwin Dubey
        /// Created On : 16-09-2020
        /// Description: To get parameter list
        /// </summary>
        /// <param name="filterModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetReportFilter")]
        [ProducesResponseType(typeof(IEnumerable<ReportFilterModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetReportFilter(ReportFilterModel filterModel)
        {
            var result = await schoolReportsService.GetReportFilter(filterModel.Name, filterModel.value);
            return Ok(result);
        }
        /// <summary>
        /// Created By : Ashwin Dubey
        /// Created On : 16-09-2020
        /// Description: To get parameter list
        /// </summary>
        /// <param name="filterModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetReportData")]
        [ProducesResponseType(typeof(ReportDataModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetReportData(ReportParameterRequestModel filterModel)
        {
            var result = await schoolReportsService.GetReportData(filterModel);
            return Ok(result);
        }
        [HttpGet]
        [Route("DeleteSchoolReport")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult DeleteSchoolReport(int id, long schoolId)
        {
            schoolReportsService.DeleteAsync(id,schoolId);
            return Ok();
        }
    }
}