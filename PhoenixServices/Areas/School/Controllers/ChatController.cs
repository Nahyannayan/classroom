﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.School.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ChatController : ControllerBase
    {
        private readonly IChatService _ChatService;

        public ChatController(IChatService ChatService)
        {
            _ChatService = ChatService;
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 19/June/2020
        /// Description : To get the all chats
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getchats")]
        [ProducesResponseType(typeof(IEnumerable<Chat>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetChats()
        {
            var result = await _ChatService.GetAllAsync();
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.FromUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.FromUserProfileImage);
                        a.ToUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.ToUserProfileImage);
                        return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 19/June/2020
        /// Description : To get the chats based on chat id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getchatbyid/{id:int}")]
        [ProducesResponseType(typeof(Chat), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetChatById(int id)
        {
            var result = await _ChatService.GetAsync(id);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null)
                {
                    result.FromUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(result.FromUserProfileImage);
                    result.ToUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(result.ToUserProfileImage);
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 19/June/2020
        /// Description : To get the chats based on schools
        /// </summary>
        /// <param name="schoolid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getchatbyschoolid/{schoolid:int}")]
        [ProducesResponseType(typeof(IEnumerable<Chat>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetChatsBySchoolId(int schoolid)
        {
            var result = await _ChatService.GetChatsBySchoolId(schoolid);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 19/June/2020
        /// Description : To get the chats based on type of message for a particular school
        /// </summary>
        /// <param name="schoolid"></param>
        /// <param name="messagetypeid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getChatsbymessagetypeid/{schoolid:int}/{messagetypeid:int}")]
        [ProducesResponseType(typeof(IEnumerable<Chat>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetChatsByMessageTypeId(int schoolid, int messagetypeid)
        {
            var result = await _ChatService.GetChatsByMessageTypeId(schoolid, messagetypeid);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 19/June/2020
        /// Description : To add chat
        /// </summary>
        /// <param name="Chat"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addchat")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult AddChat([FromBody] Chat Chat)
        {
            var ChatId = _ChatService.InsertAsync(Chat);
            return Ok(ChatId);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 19/June/2020
        /// Description : To delete the chat
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletechat/{id:int}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteChat(int id)
        {
            _ChatService.DeleteAsync(id);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 19/June/2020
        /// Description : To get the list of types of message
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getMessageTypes")]
        [ProducesResponseType(typeof(IEnumerable<MessageTypes>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMessageTypes()
        {
            var result = await _ChatService.GetMessageTypes();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 19/June/2020
        /// Description : To get the chats based on groups
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getchatsbygroupid")]
        [ProducesResponseType(typeof(IEnumerable<Chat>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetChatByGroupId([FromQuery] int groupId, [FromQuery] int schoolId, [FromQuery] int? userId)
        {
            var result = await _ChatService.GetChatsByGroupId(groupId, schoolId, userId);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 19/June/2020
        /// Description : To get the chats based on chat type
        /// </summary>
        /// <param name="Chattypeid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getchatsbychattypeid/{Chattypeid:int}")]
        [ProducesResponseType(typeof(IEnumerable<Chat>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetChatByChatTypeId(int Chattypeid)
        {
            var result = await _ChatService.GetChatsByChatTypeId(Chattypeid);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 19/June/2020
        /// Description : To get the types of chats
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getchattypes")]
        [ProducesResponseType(typeof(IEnumerable<ChatTypes>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetChatTypes()
        {
            var result = await _ChatService.GetChatTypes();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 23/June/2020
        /// Description : To get the Recent Chats of a User
        /// </summary>
        /// <param name="schoolid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getrecentchats/{schoolid:int}/{userid:int}")]
        [ProducesResponseType(typeof(IEnumerable<ChatList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetRecentChats(int schoolid, int userid)
        {
            var result = await _ChatService.GetRecentChats(schoolid, userid);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        if (a.UserChatList != null && a.UserChatList.Count() > 0)
                        {
                            a.UserChatList = a.UserChatList.Select(s =>
                            {
                                s.FromUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(s.FromUserProfileImage);
                                s.ToUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(s.ToUserProfileImage);
                                return s;
                            }).ToList();
                        }
                        return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 23/June/2020
        /// Description : To get the Recent Chat User Lists
        /// </summary>
        /// <param name="schoolid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getrecentchatuserlist/{schoolid:int}/{userid:int}")]
        [ProducesResponseType(typeof(IEnumerable<ChatList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetRecentChatUserList(int schoolid, int userid)
        {
            var result = await _ChatService.GetRecentChatUserList(schoolid, userid);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.FromUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.FromUserProfileImage);
                        a.ToUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.ToUserProfileImage); return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 23/June/2020
        /// Description : To get the Chat History by UserId
        /// </summary>
        /// <param name="schoolid"></param>
        /// <param name="userid"></param>
        /// <param name="otheruser"></param>
        /// <param name="groupid"></param>
        /// <param name="pageNum"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getchathistorybyuserid/{schoolid:long}/{userid:long}/{otheruser:long}/{groupid:long}/{pageNum:int}")]
        [ProducesResponseType(typeof(IEnumerable<Chat>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetChatHistorybyUserId(long schoolid, long userid, long otheruser, long groupid, int? pageNum = 1)
        {
            var result = await _ChatService.GetChatHistorybyUserId(schoolid, userid, otheruser, groupid, pageNum);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.FromUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.FromUserProfileImage);
                        a.ToUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.ToUserProfileImage);
                        return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 24/June/2020
        /// Description : To get the search result in chat
        /// </summary>
        /// <param name="schoolid"></param>
        /// <param name="userId"></param>
        /// <param name="userTypeId"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getsearchinchat")]
        [ProducesResponseType(typeof(IEnumerable<Chat>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSearchInChat(int schoolid, int userId, int userTypeId, string text)
        {
            var result = await _ChatService.GetSearchInChat(schoolid, userId, userTypeId, text);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.FromUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath( a.FromUserProfileImage);
                        a.ToUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.ToUserProfileImage);
                        return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 25/June/2020
        /// Description : To get list of my contacts
        /// </summary>
        /// <param name="schoolid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getmycontactslist/{schoolid:int}/{userid:int}")]
        [ProducesResponseType(typeof(IEnumerable<LogInUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMyContactsList(int schoolid, int userid)
        {
            var result = await _ChatService.GetMyContactsList(schoolid, userid);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.ProfilePhoto = Helpers.CommonHelper.GemsStudentImagePath(a.ProfilePhoto);
                        return a;
                    });
                }
            }
            return Ok(result);
        }


        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 07/July/2020
        /// Description : To invite User for Quick Chat
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("inviteuserforchat")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult InviteUserForChat([FromBody] InviteChat entity)
        {
            var InviteChatId = _ChatService.InviteUserForChat(entity);
            return Ok(InviteChatId);
        }


        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 30/July/2020
        /// Description : To update the user access or group related info for Quick Chat
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateinviteusergroupchat")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult UpdateInviteUserGroupChat([FromBody] InviteChat entity)
        {
            var InviteChatId = _ChatService.UpdateInviteUserGroupChat(entity);
            return Ok(InviteChatId);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 30/July/2020
        /// Description : To Delete the user from Group Chat permanently
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deleteinviteusergroupchat")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult DeleteInviteUserGroupChat([FromBody] InviteChat entity)
        {
            var InviteChatId = _ChatService.DeleteInviteUserGroupChat(entity);
            return Ok(InviteChatId);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 30/July/2020
        /// Description : To update the user permission or group related information in quick chat
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateinvitegroupname")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Created)]
        public IActionResult UpdateInviteGroupName([FromBody] InviteChat entity)
        {
            var InviteChatId = _ChatService.UpdateInviteChatGroupName(entity);
            return Ok(InviteChatId);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 30/July/2020
        /// Description : To get the latest group chat id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getlatestinviteGroup")]
        [ProducesResponseType(typeof(IEnumerable<LogInUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetLatestInviteGroup()
        {
            var result = _ChatService.GetLatestInviteGroup();
            return Ok(result);
        }

        /// <summary>
        /// To get the list of users in group chat
        /// </summary>
        /// <param name="groupid"></param>
        /// <param name="schoolid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getgroupchatusers")]
        [ProducesResponseType(typeof(IEnumerable<InviteChat>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGroupChatUsers(long groupid, long schoolid)
        {
            var result = await _ChatService.GetGroupChatUsers(groupid, schoolid);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.GroupUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.GroupUserProfileImage); return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Get Group Details based on various parameters
        /// </summary>
        /// <param name="groupid"></param>
        /// <param name="schoolid"></param>
        /// <param name="userid"></param>
        /// <param name="groupname"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getgroupdetails")]
        [ProducesResponseType(typeof(IEnumerable<InviteChat>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGroupDetails(long? groupid, long? schoolid, long? userid, string groupname)
        {
            var result = await _ChatService.GetGroupDetails(groupid, schoolid, userid, groupname);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.GroupUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.GroupUserProfileImage); return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 02/August/2020
        /// Description : To get the invite group name of group
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getinviteGroupName")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public IActionResult GetInviteGroupName(long groupid)
        {
            var result = _ChatService.GetInviteGroupName(groupid);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 18 Aug 2020
        /// Description : To get all the active users in invite Chat
        /// </summary>

        /// <param name="schoolid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllGroupUsers")]
        [ProducesResponseType(typeof(IEnumerable<InviteChat>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllGroupUsers(long schoolid)
        {
            var result = await _ChatService.GetAllGroupUsers(schoolid);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.GroupUserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.GroupUserProfileImage); return a;
                    });
                }
            }
            return Ok(result);
        }


        /// <summary>
        /// Created By: Vivek Kumar
        /// Created On: 31/July/2020
        /// Description : To get the User Activity by UserId
        /// </summary>
        /// <param name="schoolid"></param>
        /// <param name="userid"></param>
        /// <param name="enableChat"></param>
        /// <param name="groupid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getrecentuseractivity/{schoolid:int}/{userid:int}/{enableChat:bool}")]
        [ProducesResponseType(typeof(IEnumerable<RecentUserActivity>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetRecentUserActivity(int schoolid, int userid, bool enableChat, long? groupid = 0)
        {
            var result = await _ChatService.GetRecentUserActivity(schoolid, userid, enableChat, groupid.Value);
            return Ok(result);
        }


        [HttpGet]
        [Route("deletechatconversation")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult DeleteChatConversation(long schoolid, long groupId, long fromUser, long toUser)
        {
            var result = _ChatService.DeleteChatConversation(schoolid, groupId, fromUser, toUser);
            return Ok(result);
        }

        /// <summary>
        /// To add the chat log related to user such as connection id , login time  etc.
        /// </summary>
        /// <param name="chatlog"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addchatlog")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult AddChatLog([FromBody] UserChatLog chatlog)
        {
            var ChatId = _ChatService.InsertChatLog(chatlog);
            return Ok(ChatId);
        }

        /// <summary>
        /// this is to delete permanently from chat log for connection of a user
        /// </summary>
        /// <param name="chatlog"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletechatlog")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult DeleteChatLog([FromBody] UserChatLog chatlog)
        {
            _ChatService.DeleteChatlog(chatlog);
            return Ok(true);
        }

        /// <summary>
        /// To get the list of connections for a particular users
        /// </summary>
        /// <param name="connectionid"></param>
        /// <param name="schoolid"></param>
        /// <param name="userid"></param>
        /// <param name="sessionid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getconnectionbyuserid")]
        [ProducesResponseType(typeof(IEnumerable<UserChatLog>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetConnectionbyUserId(string connectionid, long? schoolid, long? userid, string sessionid)
        {
            var result = await _ChatService.GetConnectionbyUserId(connectionid, schoolid, userid, sessionid);
            return Ok(result);
        }

        [HttpDelete]
        [Route("deleteallchatlog")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult DeleteAllChatLog()
        {
            _ChatService.DeleteAllChatlog();
            return Ok(true);
        }

    }
}
