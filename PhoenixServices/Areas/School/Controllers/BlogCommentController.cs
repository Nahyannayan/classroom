﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.School.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class BlogCommentController : ControllerBase
    {
        private readonly IBlogCommentService _blogCommentService;

        public BlogCommentController(IBlogCommentService blogCommentService)
        {
            _blogCommentService = blogCommentService;
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 17/June/2019
        /// Description: To get all Blog comments
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogcomments")]
        [ProducesResponseType(typeof(IEnumerable<BlogComment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogComments()
        {
            var result = await _blogCommentService.GetAllAsync();
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 17/June/2019
        /// Description: To get Blog coment by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogcommentbyid/{id:int}")]
        [ProducesResponseType(typeof(BlogComment), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogCommentById(int id)
        {
            var result = await _blogCommentService.GetAsync(id);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null)
                {
                    result.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(result.UserProfileImage);
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 17/June/2019
        /// Description: To get all Blog comment by blog Id and ispublish status(optional)
        /// </summary>
        /// <param name="blogId">required</param>
        /// <param name="isPublish">optional(default null)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogcommentsbyblogid/{blogid:int}/{ispublish:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<BlogComment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogCommentByBlogId(int blogId, bool? isPublish)
        {
            var result = await _blogCommentService.GetBlogCommentByBlogId(blogId, isPublish);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 18/June/2019
        /// Description: To get all Blog comment by publish by id and ispublish status(optional) 
        /// </summary>
        /// <param name="publishbyId"></param>
        /// <param name="isPublish">optional(default null)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogcommentbypublishby/{publishbyid:int}/{ispublish:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<BlogComment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogCommentByPublishBy(int publishbyId, bool? isPublish)
        {
            var result = await _blogCommentService.GetBlogCommentByPublishBy(publishbyId, isPublish);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 18/June/2019
        /// Description: To add new blog comment in blog comment table  
        /// </summary>
        /// <param name="blogComment">blog entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("addblogcomment")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult AddBlogComment([FromBody] BlogComment blogComment)
        {

            var commentId = _blogCommentService.Insert(blogComment);
            return Ok(commentId);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 18/June/2019
        /// Description: To update existing blog comment in blog comment table 
        /// </summary>
        /// <param name="blogComment"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateblogcomment")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult UpdateBlogComment([FromBody] BlogComment blogComment)
        {
            _blogCommentService.UpdateAsync(blogComment);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 18/June/2019
        /// Description: To delete/unpublish existing blog comment in blog comment table 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteblogcomment/{id:int}/{userId:long}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteBlogComment(int id, long userId)
        {
            _blogCommentService.DeleteBlogComment(id, userId);
            return Ok(true);
        }
        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/July/2019
        /// Description: To get all comment likes by comment id/blogcommentLikeId/likedby or isLike
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="likedBy"></param>
        /// <returns></returns>
        [HttpGet]
        //[Route("getblogcommentlikes/{commentid:int?}/{blogcommentlikeid:int?}/{likedby:int?}/{islike:bool?}")]
        [Route("getblogcommentlikes/{commentid:int?}/{likedby:int?}")]
        [ProducesResponseType(typeof(IEnumerable<BlogComment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogCommentLikes(int? commentId, int? likedBy)
        {
            var result = await _blogCommentService.GetBlogCommentLikes(commentId, null, likedBy, null);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/July/2019
        /// Description: To add new like in blog comment like table
        /// </summary>
        /// <param name="blogCommentLike"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addblogcommentlike")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult AddBlogCommentLike([FromBody] BlogCommentLike blogCommentLike)
        {
            var result = _blogCommentService.InsertCommentLike(blogCommentLike);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/July/2019
        /// Description: To update existing blog comment like in blog comment like table 
        /// </summary>
        /// <param name="blogCommentLike"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateblogcommentlike")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult UpdateBlogCommentLike([FromBody] BlogCommentLike blogCommentLike)
        {
            var result = _blogCommentService.UpdatedCommentLike(blogCommentLike);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/July/2019
        /// Description: To delete blog commet like from blog comment like table
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteblogcommentlike/{id:int}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteBlogCommentLike(int id)
        {
            var result = _blogCommentService.DeleteCommentLike(id);
            return Ok(result);
        }
    }
}