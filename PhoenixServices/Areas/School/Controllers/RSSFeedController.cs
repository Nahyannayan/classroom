﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;

namespace Phoenix.API.Areas.School.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class RSSFeedController : ControllerBase
    {
        private readonly IRSSFeedService _rssFeedService;

         public RSSFeedController(IRSSFeedService rssFeedService)
        {
            _rssFeedService = rssFeedService;

        }
        /// <summary>
        /// Description - To add or update RSSFeed.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("AddUpdateRSSFeed")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddUpdateRSSFeed([FromBody]RSSFeed objRSSFeed)
        {
            var result = _rssFeedService.AddUpdateRSSFeed(objRSSFeed);
            return Ok(result);
        }
        /// <summary>
        /// Description - To get a list of all RSSFeed.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRSSFeedList")]
        [ProducesResponseType(typeof(IEnumerable<RSSFeed>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetRSSFeedList()
        {
            var schoolList = await _rssFeedService.GetRSSFeedList();
            return Ok(schoolList);
        }

        /// <summary>
        /// Description - To get a particular RSSFeed by Ids.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRSSFeedById")]
        [ProducesResponseType(typeof(IEnumerable<RSSFeed>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetRSSFeedById(int Id)
        {
            var result = await _rssFeedService.GetRSSFeedById(Id);
            return Ok(result);
        }
    }
}