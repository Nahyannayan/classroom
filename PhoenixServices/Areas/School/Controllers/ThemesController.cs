﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.School.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class ThemesController : ControllerBase
    {

        private readonly IThemesService _themesService;

        public ThemesController(IThemesService themesService)
        {
            _themesService = themesService;
        }

        /// <summary>
        /// Created By: Girish S
        /// Created On: 12/3/2019
        /// Description: To get school themes by school Id.
        /// </summary>
        /// <param name="schoolId">school id</param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getallschoolthemesbyschoolid")]
        [ProducesResponseType(typeof(IEnumerable<Themes>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllSchoolThemesBySchoolId(int? schoolId = null,short languageId=0)
        {
            var result = await _themesService.GetAllSchoolThemesById(schoolId,languageId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 30 March 2020
        /// Description : Get All School Theme By Curriculum and by school
        /// </summary>
        /// <param name="curriculumId"></param>
        /// <param name="schoolId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllSchoolThemesByCurriculum")]
        [ProducesResponseType(typeof(IEnumerable<Themes>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllSchoolThemesByCurriculum(int? curriculumId, int? schoolId = null, short languageId = 0)
        {
            var result = await _themesService.GetAllSchoolThemesByCurriculum(schoolId, curriculumId,languageId);
            return Ok(result);
        }



        /// <summary>
        /// Created By - Girish s
        /// Created Date - 12/4/2019
        /// Description - To insert school theme
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertschoolthemes")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertSchoolThemes([FromBody]Themes model)
        {
            var result = _themesService.InsertSchoolTheme(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Girish s
        /// Created Date - 12/4/2019
        /// Description - To udpate school theme
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateschoolthemes")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateSchoolThemes([FromBody]Themes model)
        {
            var result = _themesService.UpdateSchoolTheme(model);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Girish s
        /// Created Date - 04/12/2019
        /// Description - To get theme by id
        /// </summary>
        /// <param name="themeId">themeId</param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getschoolthemebyid")]
        [ProducesResponseType(typeof(Themes), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolThemeById(int themeId, short languageId = 0)
        {
            var quizList = await _themesService.GetSchoolThemeById(themeId,languageId);
            return Ok(quizList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 30 March 2020
        /// Description - Get student grades assigned theme
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentGradeTheme")]
        [ProducesResponseType(typeof(IEnumerable<Themes>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentGradeTheme(long userId)
        {
            var result = await _themesService.GetStudentGradeTheme(userId);
            return Ok(result);
        }

    }
}