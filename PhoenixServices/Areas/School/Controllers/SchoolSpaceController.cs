﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Phoenix.API.Areas.School.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SchoolSpaceController : ControllerBase
    {
        private readonly ISchoolSpaceService _schoolSpaceService;

        public SchoolSpaceController(ISchoolSpaceService schoolSpaceService)
        {
            _schoolSpaceService = schoolSpaceService;
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 8/May/2019
        /// Description: To get all school spaces 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getschoolspaces")]
        [ProducesResponseType(typeof(IEnumerable<SchoolSpace>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolSpaces()
        {
            var result = await _schoolSpaceService.GetSchoolSpaces();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 8/May/2019
        /// Description: To get school spaces  by school Id
        /// </summary>
        /// <param name="schoolId">school id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getschoolspacesbyschoolid")]
        [ProducesResponseType(typeof(IEnumerable<SchoolSpace>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetContactInfosBySchoolId(int schoolId)
        {
            var result = await _schoolSpaceService.GetSchoolSpacesBySchoolId(schoolId);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 8/May/2019
        /// Description: To get school space by space Id
        /// </summary>
        /// <param name="id">space id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getschoolspacebyid")]
        [ProducesResponseType(typeof(IEnumerable<SchoolSpace>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolSpaceById(int id)
        {
            var result = await _schoolSpaceService.GetSchoolSpaceById(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 8/May/2019
        /// Description: To add school space
        /// </summary>
        /// <param name="schoolSpace">school space entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("addschoolspace")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult AddSchooSpace([FromBody]SchoolSpace schoolSpace)
        {
            var result = _schoolSpaceService.InsertSchoolSpace(schoolSpace);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 8/May/2019
        /// Description: To update school space
        /// </summary>
        /// <param name="schoolSpace">school space entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateschoolspace")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult UpdateSchoolSpace([FromBody]SchoolSpace schoolSpace)
        {
            var result = _schoolSpaceService.UpdateSchoolSpace(schoolSpace);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 8/May/2019
        /// Description: To delete school space by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteschoolspace/{id:int}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteSchoolSpace(int id)
        {
            var result = _schoolSpaceService.DeleteSchoolSpace(id);
            return Ok(result);
        }
    }
}