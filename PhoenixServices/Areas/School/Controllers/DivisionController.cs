﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Models;
using Phoenix.API.Services;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.School.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class DivisionController : ControllerBase
    {
        private readonly IDivisionService _divisionService;
        public DivisionController(IDivisionService divisionService)
        {
            _divisionService = divisionService;
        }
        [HttpPost]
        [Route("SaveDivisionDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveDivisionDetails(DivisionDetails DivisionDetails, string DATAMODE)
        {
            var result = await _divisionService.SaveDivisionDetails(DivisionDetails, DATAMODE);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetDivisionDetails")]
        [ProducesResponseType(typeof(IEnumerable<DivisionDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDivisionDetails(long BSU_ID, int CurriculumId = 1)
        {
            var result = await _divisionService.GetDivisionDetails(BSU_ID, CurriculumId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetReportingTermDetail")]
        [ProducesResponseType(typeof(IEnumerable<ReportingTermModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetReportingTermDetail(long ReportingTermId, long SchoolId)
        {
            var result = await _divisionService.GetReportingTermDetail(ReportingTermId, SchoolId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetReportingSubTermDetail")]
        [ProducesResponseType(typeof(IEnumerable<ReportingSubTermModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetReportingSubTermDetail(long ReportingTermId, long ReportingSubTermId)
        {
            var result = await _divisionService.GetReportingSubTermDetail(ReportingTermId, ReportingSubTermId);
            return Ok(result);
        }
        [HttpPost]
        [Route("SaveReportingTermDetail")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveReportingTermDetail(ReportingTermModel reportingTermModel)
        {
            var result = await _divisionService.SaveReportingTermDetail(reportingTermModel);
            return Ok(result);
        }
        [HttpGet]
        [Route("LockUnlockTerm")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> LockUnlockTerm(long ReportingTermId,bool IsLock)
        {
            var result = await _divisionService.LockUnlockTerm(ReportingTermId, IsLock);
            return Ok(result);
        }
        [HttpPost]
        [Route("SaveReportingSubTermDetail")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveReportingSubTermDetail(ReportingSubTermModel reportingSubTermModel)
        {
            var result = await _divisionService.SaveReportingSubTermDetail(reportingSubTermModel);
            return Ok(result);
        }
        [HttpGet]
        [Route("LockUnlockSubTerm")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> LockUnlockSubTerm(long ReportingSubTermId, bool IsLock)
        {
            var result = await _divisionService.LockUnlockSubTerm(ReportingSubTermId, IsLock);
            return Ok(result);
        }
    }
}