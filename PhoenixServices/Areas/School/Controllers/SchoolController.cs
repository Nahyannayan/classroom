﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.School.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SchoolController : ControllerBase
    {
        private readonly ISchoolService _schoolService;
        private readonly ITeacherDashboardService _teacherDashboard;
        public SchoolController(ISchoolService schoolService, ITeacherDashboardService teacherdashboard)
        {
            _schoolService = schoolService;
            _teacherDashboard = teacherdashboard;
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 05 May 2019
        /// Description - To get all school list.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getschoollist")]
        [ProducesResponseType(typeof(IEnumerable<SchoolInformation>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolList()
        {
            var schoolList = await _schoolService.GetSchoolList();
            return Ok(schoolList);
        }


        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 05 May 2019
        /// Description - To get all admin school list.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getadminschoollist")]
        [ProducesResponseType(typeof(IEnumerable<SchoolInformation>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAdminSchoolList()
        {
            var schoolList = await _schoolService.GetAdminSchoolList();
            return Ok(schoolList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 05 May 2019
        /// Description - To get school details by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getschoolbyid/{id:int}")]
        [ProducesResponseType(typeof(SchoolInformation), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolById(int id)
        {
            var school = await _schoolService.GetSchoolById(id);
            return Ok(school);
        }

        /// <summary>
        ///  Description - To get details related to teacher dashboard.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTeacherDashboard/{id:int}")]
        [ProducesResponseType(typeof(TeacherDashboard), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTeacherDashboard(int id)
        {
            var trhDashboard = await _teacherDashboard.GetTeacherDashboard(id);
            return Ok(trhDashboard);
        }

        /// <summary>
        /// Description - To get list of students associated with a particular teacher.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentForTeacher")]
        [ProducesResponseType(typeof(List<Student>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentForTeacher(long id, string ids = "")
        {
            var students = await _teacherDashboard.GetStudentForTeacher(id, ids);
            return Ok(students);
        }

        /// <summary>
        /// To Get Student list associated with teacher and filtering with school groups.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="schoolGroupIds"></param>
        /// <param name="searchString"></param>
        /// <param name="sortBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentForTeacherBySchoolGroup")]
        [ProducesResponseType(typeof(List<Student>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentForTeacherBySchoolGroup(int id, int pageNumber, int pageSize, string schoolGroupIds, string searchString = "", string sortBy = "")
        {
            var students = await _teacherDashboard.GetStudentForTeacherBySchoolGroup(id, pageNumber, pageSize, schoolGroupIds, searchString, sortBy);
            return Ok(students);
        }



        /// <summary>
        /// Description - To get module file details as per file id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="filetype"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetModuleFileDetails")]
        [ProducesResponseType(typeof(FileDownload), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetModuleFileDetails(int id, string filetype)
        {
            var fileDetails = await _schoolService.GetModuleFileDetails(id, filetype);
            return Ok(fileDetails);
        }

        /// <summary>
        /// Description - To get dashboard data.
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetReportDashboard")]
        [ProducesResponseType(typeof(ReportDashboard), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetReportDashboard(long? schoolId, string startDate, string endDate, long? userId, long? groupId)
        {
            var dashboardDate = await _schoolService.GetReportDashboard(schoolId, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), userId, groupId);
            return Ok(dashboardDate);
        }

        /// <summary>
        /// Created By: Mukund Patil
        /// Created On: 03 SEPT 2020
        /// Description - To get assignment report card data
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentReportData")]
        [ProducesResponseType(typeof(AssignmentReportCard), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentReportData(long? schoolId, string startDate, string endDate, long? userId, long? groupId)
        {
            var dashboardDate = await _schoolService.GetAssignmentReportData(schoolId, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), userId, groupId);
            return Ok(dashboardDate);
        }

        /// <summary>
        /// Created By: Mukund Patil
        /// Created On: 26 MAY 2020
        /// Description - To get assignments by group report data
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSchoolGroupAssignmentReport")]
        [ProducesResponseType(typeof(IEnumerable<TableData>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGroupAssignmentReport(long? schoolId, string startDate, string endDate, long? userId)
        {
            var groupAssignments = await _schoolService.GetSchoolGroupAssignmentReport(schoolId, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), userId);
            return Ok(groupAssignments);
        }

        /// <summary>
        /// Created By: Mukund Patil
        /// Created On: 28 MAY 2020
        /// Description - To get group chatter report data
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getGroupChatterReport")]
        [ProducesResponseType(typeof(IEnumerable<ChatterTableData>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGroupChatterReport(long? schoolId, string startDate, string endDate, long? userId)
        {
            var groupChatters = await _schoolService.GetGroupChatterReport(schoolId, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), userId);
            return Ok(groupChatters);
        }

        /// <summary>
        /// Created By: Mukund Patil
        /// Created On: 28 MAY 2020
        /// Description - To get teacher assignment report data
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getTeacherAssignmentReport")]
        [ProducesResponseType(typeof(IEnumerable<TeacherAssignment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTeacherAssignmentReport(long? schoolId, string startDate, string endDate, long? userId)
        {
            var teacherAssignments = await _schoolService.GetTeacherAssignmentReport(schoolId, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), userId);
            return Ok(teacherAssignments);
        }

        /// <summary>
        /// Created By: Mukund Patil
        /// Created On: 31 MAY 2020
        /// Description - To get student assignment report data
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentAssignmentReport")]
        [ProducesResponseType(typeof(IEnumerable<StudentAssignment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAssignmentReport(long? schoolId, string startDate, string endDate, long? userId)
        {
            var studentAssignments = await _schoolService.GetStudentAssignmentReport(schoolId, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), userId);
            return Ok(studentAssignments);
        }

        /// <summary>
        /// Created By: Mukund Patil
        /// Created On: 31 MAY 2020
        /// Description - To get abuse report data
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSchoolAbuseReport")]
        [ProducesResponseType(typeof(IEnumerable<Data>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolAbuseReport(long? schoolId, string startDate, string endDate, long? userId)
        {
            var abuseReportData = await _schoolService.GetSchoolAbuseReport(schoolId, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), userId);
            return Ok(abuseReportData);
        }

        /// <summary>
        /// Description - To get child quiz report data.
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getChildQuizReportData")]
        [ProducesResponseType(typeof(IEnumerable<ChildQuizReportData>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetChildQuizReportData(long? schoolId, string startDate, string endDate, long? userId)
        {
            var childQuizReportData = await _schoolService.GetChildQuizReportData(schoolId, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), userId);
            return Ok(childQuizReportData);
        }

        /// <summary>
        /// Description - To get teacher list by schoolId
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSchoolTeachersBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<ChildQuizReportData>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolTeachersBySchoolId(long schoolId)
        {
            var teachersList = await _schoolService.GetSchoolTeachersBySchoolId(schoolId);
            return Ok(teachersList);
        }

        /// <summary>
        /// Description - To get quiz report data with date range
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="schoolId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getQuizReportDataWithDateRange")]
        [ProducesResponseType(typeof(IEnumerable<QuizReportData>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizReportDataWithDateRange(long? userId, long? schoolId, string startDate, string endDate)
        {
            var quizReportData = await _schoolService.GetQuizReportDataWithDateRange(userId, schoolId, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate));
            return Ok(quizReportData);
        }



        /// <summary>
        /// Description - To get module file details as per file id.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetReportDetails")]
        [ProducesResponseType(typeof(FileDownload), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetReportDetails(DateTime startDate, DateTime endDate)
        {
            var GetReportDetails = await _schoolService.GetReportDetails(startDate, endDate);
            return Ok(GetReportDetails);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 8th April 2020
        /// Description - Get All schools with pagination data
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchString"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllBusinesUnitSchools")]
        [ProducesResponseType(typeof(IEnumerable<SchoolInformation>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllBusinesUnitSchools([FromQuery] int pageIndex, [FromQuery] int pageSize, [FromQuery] string searchString = "")
        {
            var result = await _schoolService.GetAllBusinesUnitSchools(pageIndex, pageSize, searchString);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 9th April 2020
        /// Description - change classroom active status of schools
        /// </summary>
        /// <param name="schoolInformation"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateSchoolClassroomStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateSchoolClassroomStatus([FromBody] SchoolInformation schoolInformation)
        {
            bool result = await _schoolService.UpdateSchoolClassroomStatus(schoolInformation);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 2 Sept 2020
        /// Description - get school login report data
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="schoolId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolLoginReport")]
        [ProducesResponseType(typeof(LoginDetailData), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolLoginReport([FromQuery] long userId, [FromQuery] long schoolId, [FromQuery] string startDate, [FromQuery] string endDate)
        {
            var result = await _schoolService.GetSchoolLoginReport(userId, schoolId, startDate, endDate);
            return Ok(result);
        }

        /// <summary>
        /// CREATED BY - Rohit Patil
        /// CREATED DATE - 19 Oct 2020
        /// DESCRIPTION - Get live session status
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolSessionStatus")]
        [ProducesResponseType(typeof(SessionConfiguration), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolSessionStatus([FromQuery]long schoolId)
        {
            var result = await _schoolService.GetSchoolSessionStatus(schoolId);
            return Ok(result);
        }

        /// <summary>
        /// CREATED BY - Rohit Patil
        /// CREATED DATE - 19 Oct 2020
        /// DESCRIPTION - Save school live session status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveSchoolSessionStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveSchoolSessionStatus([FromBody]SessionConfiguration model)
        {
            bool result = await _schoolService.SaveSchoolSessionStatus(model);
            return Ok(result);
        }


        #region For department
        [HttpPost]
        [Route("DepartmentCourseCU")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DepartmentCourseCU([FromBody]Department model)
        {
            bool result = await _schoolService.DepartmentCourseCU(model);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetDepartmentCourse")]
        [ProducesResponseType(typeof(IEnumerable<Department>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDepartmentCourse(long schoolId, long curriculumId, long? departmentId)
        {
            var GetDepartmentCourse = await _schoolService.GetDepartmentCourse(schoolId, curriculumId, departmentId);
            return Ok(GetDepartmentCourse);
        }
        [HttpGet]
        [Route("CanDeleteDepartment")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CanDeleteDepartment(long departmentId)
        {
            var GetDepartmentCourse = await _schoolService.CanDeleteDepartment(departmentId);
            return Ok(GetDepartmentCourse);
        }
        #endregion


        /// <summary>
        /// CREATED BY - Rohit Patil
        /// CREATED DATE - 12 Jan 2021
        /// DESCRIPTION - get student vault report for teacher
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="teacherId"></param>
        /// <returns></returns>
        [HttpGet("GetStudentVaultReportData")]
        [ProducesResponseType(typeof(IEnumerable<Student>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetStudentVaultReportData(long schoolId, long teacherId)
        {
            var result = await _schoolService.GetStudentVaultReportData(schoolId, teacherId);
            return Ok(result);
        }
    }
}