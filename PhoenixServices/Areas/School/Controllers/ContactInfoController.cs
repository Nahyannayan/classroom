﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Phoenix.API.Areas.School.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class ContactInfoController : ControllerBase
    {
        private readonly IContactInfoService _contactInfoService;

        public ContactInfoController(IContactInfoService contactInfoService)
        {
            _contactInfoService = contactInfoService;
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 30/Apr/2019
        /// Description: To get all school contact info 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getcontactinfos")]
        [ProducesResponseType(typeof(IEnumerable<ContactInfo>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetContactInfos()
        {
            var result = await _contactInfoService.GetContactInfos();
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 30/Apr/2019
        /// Description: To get school contact info by school Id
        /// </summary>
        /// <param name="schoolId">school id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getcontactinfobyschoolid")]
        [ProducesResponseType(typeof(IEnumerable<ContactInfo>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetContactInfosBySchoolId(int schoolId)
        {
            var result = await _contactInfoService.GetContactInfoBySchoolId(schoolId);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 30/Apr/2019
        /// Description: To get school contact info by contact Id
        /// </summary>
        /// <param name="id">contact id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getcontactinfobyid")]
        [ProducesResponseType(typeof(ContactInfo), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetContactInfoById(int id)
        {
            var result = await _contactInfoService.GetContactInfoById(id);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 30/Apr/2019
        /// Description: Add contact info 
        /// </summary>
        /// <param name="contactInfo">contact info object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("addcontactinfo")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult AddContactInfo([FromBody]ContactInfo contactInfo)
        {
            var result = _contactInfoService.InsertContactInfo(contactInfo);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 30/Apr/2019
        /// Description: Update contact info 
        /// </summary>
        /// <param name="contactInfo">contact info object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatecontactinfo")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult UpdateContactInfo([FromBody]ContactInfo contactInfo)
        {
            var result = _contactInfoService.UpdateContactInfo(contactInfo);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 30/Apr/2019
        /// Description: Delete contact info 
        /// </summary>
        /// <param name="id">contactinfo id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("deletecontactinfo")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteContactInfo(int id)
        {
            var result = _contactInfoService.DeleteContactInfo(id);
            return Ok(result);
        }
    }
}