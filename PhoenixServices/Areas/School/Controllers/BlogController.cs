﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.School.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class BlogController : ControllerBase
    {
        private readonly IBlogService _blogService;
        private readonly IBlogShareService _blogShareService;

        public BlogController(IBlogService blogService, IBlogShareService blogShareService)
        {
            _blogService = blogService;
            _blogShareService = blogShareService;
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 17/June/2019
        /// Description: To get all Blogs
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogs")]
        [ProducesResponseType(typeof(IEnumerable<Blog>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogs()
        {
            var result = await _blogService.GetAllAsync();
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 17/June/2019
        /// Description: To get all Blogs by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogbyid/{id:int}")]
        [ProducesResponseType(typeof(Blog), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogById(int id)
        {
            var result = await _blogService.GetAsync(id);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null)
                {
                    result.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(result.UserProfileImage);
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 17/June/2019
        /// Description: To get all Blogs by schoolId and ispublish status(optional)
        /// </summary>
        /// <param name="schoolId">required</param>
        /// <param name="isPublish">optional(default null)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogsbyschoolid/{schoolid:int}/{ispublish:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<Blog>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogsBySchoolId(int schoolId, bool? isPublish = null)
        {
            var result = await _blogService.GetBlogsBySchoolId(schoolId, isPublish);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a => { a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a; }).ToList();
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 18/June/2019
        /// Description: To get all Blogs by schoolId, categoryId and ispublish status(optional)
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="categoryId"></param>
        /// <param name="isPublish">optional(default null)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogsbycategoryid/{schoolid:int}/{categoryid:int}/{ispublish:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<Blog>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogsByCategoryId(int schoolId, int categoryId, bool? isPublish = null)
        {
            var result = await _blogService.GetBlogsByCategoryId(schoolId, categoryId, isPublish);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a => { a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a; }).ToList();
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 18/June/2019
        /// Description: To add new blogs in blog table
        /// Modified on: 26 Sept 2019 By Deepak Singh. To get inserted blog id and return it
        /// </summary>
        /// <param name="blog">blog entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("addblog")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddBlog([FromBody] Blog blog)
        {
            var blogId = _blogService.Insert(blog);
            return Ok(blogId);
        }


        /// <summary>
        /// Created By: Deependra Sharma
        /// Created On: 09/July/2020
        /// Description: To add new school group and student tagging map
        /// Modified on: 26 Sept 2019 By Deepak Singh. To get inserted blog id and return it
        /// </summary>
        /// <param name="blog">blog entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddSchoolGroupAndStudentMapping")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult AddSchoolGroupAndStudentMapping([FromBody] SchoolGroup_StudentMapping mapModal)
        {
            var blogId = _blogService.InsertMapAsync(mapModal);
            return Ok(blogId);
        }


        /// <summary>
        /// Created By - Deependra Sharma
        /// Cretaed Date - 12 July 2020
        /// Description - save the sared post details
        /// </summary>
        /// <param name="shareModal">shareModal entity</param
        /// <returns></returns>
        [HttpPost]
        [Route("sharepost")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult AddSchoolGroupAndStudentMapping([FromBody] sharepost shareModal)
        {
            var sharedId = _blogService.InsertSharePost(shareModal);
            return Ok(sharedId);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 18/June/2019
        /// Description: To delete/unpublish existing blog comment in blog comment table
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UpdateDiscloseDate/{id:int}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult UpdateDiscloseDate(int id)
        {
            var result = _blogService.UpdateDiscloseDate(id);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 18/June/2019
        /// Description: To update existing blog in blog table
        /// </summary>
        /// <param name="blog"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateblog")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult UpdateBlog([FromBody] Blog blog)
        {
            _blogService.UpdateAsync(blog);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 18/June/2019
        /// Description: To delete/unpublish existing blog in blog table
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteblog/{id:int}/{userId:long}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteBlog(int id, long userId)
        {
            _blogService.DeleteBlog(id, userId);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 23/June/2019
        /// Description: To blog categories from blog category table
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogcategories")]
        [ProducesResponseType(typeof(IEnumerable<BlogCategory>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogCategories(long? schoolId, short languageId)
        {
            var result = await _blogService.GetBlogCategories(schoolId, languageId);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Rohit Patil
        /// Created On: 14/July/2019
        /// Description: To get all Blogs by schoolId with pagination
        /// </summary>
        /// <param name="schoolId">required</param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getTeacherBlogs")]
        [ProducesResponseType(typeof(IEnumerable<Blog>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTeacherBlogs(int schoolId, int pageNumber, int pageSize)
        {
            var result = await _blogService.GetTeacherBlogs(schoolId, pageNumber, pageSize);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 22/July/2019
        /// Description: To publish/unpublish blogs by Id
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="isPublish"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("publishblogbyid/{blogid:int}/{ispublish:bool}/{publishBy:int}")]
        [ProducesResponseType(typeof(IEnumerable<bool>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult PublishBlogById(int blogId, bool isPublish, int publishBy)
        {
            _blogService.PublishBlog(blogId, isPublish, publishBy);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 24/July/2019
        /// Description: To get all blogs by group and blogtypeId
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="blogTypeId"></param>
        /// <param name="isPublish"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogsbygroupnblogtypeid/{groupid:int}/{blogtypeid:int}/{ispublish:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<Blog>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogByGroupNBlogTypeId(int groupId, int blogTypeId, bool? isPublish = null)
        {
            var result = await _blogService.GetBlogsByGroupNBlogtypeId(groupId, blogTypeId, isPublish);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 24/July/2019
        /// Description: To get all blogs by group id
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <param name="isPublish"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogsbygroupid")]
        [ProducesResponseType(typeof(IEnumerable<Blog>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogByGroupId([FromQuery] int groupId, [FromQuery] int schoolId, [FromQuery] int? userId, [FromQuery] bool? isPublish = null)
        {
            var result = await _blogService.GetBlogsByGroupId(groupId, schoolId, userId, isPublish);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a;
                    });
                }
            }
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 24/July/2019
        /// Description: To get all blogs by blogType id
        /// </summary>
        /// <param name="blogTypeId"></param>
        /// <param name="isPublish"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogsbyblogtypeid/{blogtypeid:int}/{ispublish:bool?}")]
        [ProducesResponseType(typeof(IEnumerable<Blog>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogByBlogTypeId(int blogTypeId, bool? isPublish = null)
        {
            var result = await _blogService.GetBlogsByBlogTypeId(blogTypeId, isPublish);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.UserProfileImage = Helpers.CommonHelper.GemsStudentImagePath(a.UserProfileImage); return a;
                    });
                }
            }
            return Ok(result);
        }




        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/July/2019
        /// Description: To get all Blog like by blogid/blogLikeid/likedBy and isLike status(optional)
        /// </summary>
        /// <param name="blogId">optional(default null)</param>
        /// <param name="blogLikeId">optional(default null)</param>
        /// <param name="likedBy">optional(default null)</param>
        /// <param name="isLike">optional(default null)</param>
        /// <returns></returns>
        [HttpGet]
        //[Route("getbloglikes/{blogid:int?}/{likedby:int?}/{islike:bool?}/{bloglikeid:int?}")]
        // public async Task<IActionResult> GetBlogLikes(int? blogId,int? likedBy, bool? isLike,int? blogLikeId)
        [Route("getbloglikes/{blogid:int?}/{likedby:int?}")]
        [ProducesResponseType(typeof(IEnumerable<Blog>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogLikes(int? blogId, int? likedBy)
        {
            var result = await _blogService.GetBlogLikes(blogId, null, likedBy, null);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/July/2019
        /// Description: To add new likes in blog like table
        /// </summary>
        /// <param name="blogLike">blog like entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("addbloglike")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult AddBlogLike([FromBody] BlogLike blogLike)
        {
            var result = _blogService.InsertLike(blogLike);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/July/2019
        /// Description: To update existing liks in blog like table
        /// </summary>
        /// <param name="blogLike"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatebloglike")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult UpdateBlogLike([FromBody] BlogLike blogLike)
        {
            var result = _blogService.UpdatedLike(blogLike);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 25/July/2019
        /// Description: To delete like in blog like from blog like table
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletebloglike/{id:int}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult DeleteBlogLike(int id)
        {
            var result = _blogService.DeleteLike(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 26/JuLy/2019
        /// Description: To blog types from blog type table
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogtypes")]
        [ProducesResponseType(typeof(IEnumerable<BlogTypes>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogTypes(long? schoolId, short languageId = 0)
        {
            var result = await _blogService.GetBlogTypes(schoolId, languageId);
            return Ok(result);
        }
        /// <summary>
        /// Created By: Rohit Patil
        /// Created On: 16/Jan/2020
        /// Description: To Delete exemplar wall
        /// </summary>
        /// <param name="blog">blog entity</param>
        /// <returns></returns>
        [HttpPost]
        [Route("deleteExemplar")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult DeleteExemplar([FromBody] Blog blog)
        {
            var blogId = _blogService.DeleteExemplar(blog);
            return Ok(blogId);
        }
        /// <summary>
        /// Created By: Rohit Patil
        /// Created On: 16/Jan/2020
        /// Description: To Update Sort order of exemplar wall
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateWallSortOrder")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public IActionResult UpdateWallSortOrder([FromBody] ExemplarWallOrder model)
        {
            var blogId = _blogService.UpdateWallSortOrder(model.dataTable, model.UpdatedBy);
            return Ok(blogId);
        }


        /// <summary>
        /// Created By: Deepak Singh
        /// Created On: 03/may/2020
        /// Description: To get chatter with comments
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="schoolId"></param>
        /// <param name="categoryId"></param>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <param name="isTeacher"></param>
        /// <param name="isPublish"></param>
        /// <param name="commentId"></param>
        /// <param name="publishBy"></param>
        /// <param name="isCommentPublish"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getChatterWithComments")]
        [ProducesResponseType(typeof(IEnumerable<Chatter>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetChatterWithComment([FromQuery] int? blogId, [FromQuery] int schoolId, [FromQuery] int? categoryId, [FromQuery] int groupId, [FromQuery] int? userId, [FromQuery] bool isTeacher, [FromQuery] bool? isPublish, [FromQuery] int? commentId, [FromQuery] int? publishBy, [FromQuery] bool? isCommentPublish)
        {
            var result = await _blogService.GetChattersWithComment(blogId, schoolId, categoryId, groupId, userId, isTeacher, isPublish, commentId, publishBy, isCommentPublish);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetUserExemplerWallData")]
        [ProducesResponseType(typeof(IEnumerable<Blog>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserExemplerWallData(long userId, int pageSize, int pageIndex)
        {
            var result = await _blogService.GetUserExemplerWallData(userId, pageSize, pageIndex);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 02/July/2020
        /// Description: To Search Groups, members and topic of blog in a school
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="searchString"></param>
        /// <param name="userId"></param>
        /// <param name="isTeacher"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSearchOnMyWall")]
        [ProducesResponseType(typeof(IEnumerable<SearchMyWall>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSearchOnMyWall(long schoolId, string searchString, long userId, bool isTeacher)
        {
            var result = await _blogService.GetSearchOnMyWall(schoolId, searchString, userId, isTeacher);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.SearchImage = Helpers.CommonHelper.GemsStudentImagePath(a.SearchImage); return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 14/Aug/2020
        /// Description: To Search Groups, members and topic of blog in a school by group
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="searchString"></param>
        /// <param name="userId"></param>
        /// <param name="isTeacher"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSearchBlogsOnMyGroup")]
        [ProducesResponseType(typeof(IEnumerable<SearchMyWall>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSearchBlogsOnMyGroup(long schoolId, string searchString, long userId, bool isTeacher, long groupId)
        {
            var result = await _blogService.GetSearchBlogsOnMyGroup(schoolId, searchString, userId, isTeacher, groupId);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.SearchImage = Helpers.CommonHelper.GemsStudentImagePath(a.SearchImage); return a;
                    });
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 30/June/2020
        /// Description: To share the blog to other Groups
        /// </summary>
        /// <param name="shareBlog"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("shareblogs")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public IActionResult ShareBlogs([FromBody] ShareBlogs shareBlog)
        {
            _blogShareService.ShareBlogs(shareBlog);
            return Ok(true);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 02/July/2020
        /// Description: to Get the list of blogs for the my wall including shared blogs and comments
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="schoolId"></param>
        /// <param name="categoryId"></param>
        /// <param name="blogTypeId"></param>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <param name="isTeacher"></param>
        /// <param name="isPublish"></param>
        /// <param name="commentId"></param>
        /// <param name="publishBy"></param>
        /// <param name="isCommentPublish"></param>
        /// <param name="LogedInUserId"></param>
        /// <param name="pageNum"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getmywalldata")]
        [ProducesResponseType(typeof(IEnumerable<Chatter>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMyWallData([FromQuery] int? blogId, [FromQuery] int schoolId, [FromQuery] int? categoryId, [FromQuery] int? blogTypeId,
            [FromQuery] int? groupId, [FromQuery] int? userId, [FromQuery] bool isTeacher, [FromQuery] bool? isPublish, [FromQuery] int? commentId, [FromQuery] int? publishBy, [FromQuery] bool? isCommentPublish, [FromQuery] int? LogedInUserId, [FromQuery] int? pageNum = 1)
        {
            var result = await _blogService.GetMyWallData(blogId, schoolId, categoryId, blogTypeId, groupId, userId, isTeacher, isPublish, commentId, publishBy, isCommentPublish, LogedInUserId, pageNum);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 04/Aug/2020
        /// Description: to Get the list of blogs on search for the my wall including shared blogs and comments
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="schoolId"></param>
        /// <param name="categoryId"></param>
        /// <param name="blogTypeId"></param>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <param name="isTeacher"></param>
        /// <param name="isPublish"></param>
        /// <param name="commentId"></param>
        /// <param name="publishBy"></param>
        /// <param name="isCommentPublish"></param>
        /// <param name="LogedInUserId"></param>
        /// <param name="pageNum"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getmywalldataonsearch")]
        [ProducesResponseType(typeof(IEnumerable<Chatter>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMyWallDataOnSearch([FromQuery] long? blogId, [FromQuery] long schoolId, [FromQuery] int? categoryId, [FromQuery] int? blogTypeId,
            [FromQuery] long? groupId, [FromQuery] long? userId, [FromQuery] bool isTeacher, [FromQuery] bool? isPublish, [FromQuery] int? commentId, [FromQuery] int? publishBy, [FromQuery] bool? isCommentPublish, int? LogedInUserId, [FromQuery] int? pageNum = 1)
        {
            var result = await _blogService.GetMyWallDataOnSearch(blogId, schoolId, categoryId, blogTypeId, groupId, userId, isTeacher, isPublish, commentId, publishBy, isCommentPublish, LogedInUserId, pageNum);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Sachin Gupta
        /// Created On: 14/August/2020
        /// Description: To get recent 10 Blogs for student by schoolId and userID for Dashboard
        /// </summary>
        /// <param name="userId">required</param>
        /// <param name="schoolId">required</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentChatter")]
        [ProducesResponseType(typeof(IEnumerable<BlogStudentDash>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentDashBlogs(int userId, int schoolId)
        {
            var result = await _blogService.GetStudentDashBlogs(userId, schoolId);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Sachin Gupta
        /// Created On: 14/August/2020
        /// Description: To get recent 10 Blogs for Teacher by schoolId and userID for Dashboard
        /// </summary>
        /// <param name="userId">required</param>
        /// <param name="schoolId">required</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getTeacherChatter")]
        [ProducesResponseType(typeof(IEnumerable<BlogTeacherDash>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTeacherDashBlogs(int userId, int schoolId)
        {
            var result = await _blogService.GetTeacherDashBlogs(userId, schoolId);
            return Ok(result);
        }


        /// <summary>
        /// Created By: Shankar
        /// Created On: 13/December/2020
        /// Description: To Get Post And Comment Like User List
        /// </summary>
        /// <param name="BlogId">required</param>
        /// <param name="CommentSectioId">required</param>
        /// <param name="UserId">required</param>
        /// <param name="IsCommentDetailDisplay">required</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPostAndCommentLikeUserList")]
        [ProducesResponseType(typeof(IEnumerable<PostAndCommentLikeUserList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPostAndCommentLikeUserList(long BlogId, long CommentSectioId, long UserId,bool IsCommentDetailDisplay)
        {
            var result = await _blogService.GetPostAndCommentLikeUserList(BlogId, CommentSectioId, UserId, IsCommentDetailDisplay);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Aman Jain
        /// Created On: 04/Aug/2020
        /// Description: to Get the list of blogs on search for the my wall including shared blogs and comments
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="schoolId"></param>
        /// <param name="categoryId"></param>
        /// <param name="blogTypeId"></param>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <param name="isTeacher"></param>
        /// <param name="isPublish"></param>
        /// <param name="commentId"></param>
        /// <param name="publishBy"></param>
        /// <param name="isCommentPublish"></param>
        /// <param name="LogedInUserId"></param>
        /// <param name="pageNum"></param>
        /// <param name="EnableChat"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyChatterData")]
        [ProducesResponseType(typeof(IEnumerable<BlogCompleteData>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMyChatterData([FromQuery] long? blogId, [FromQuery] long schoolId, [FromQuery] int? categoryId, [FromQuery] int? blogTypeId,
            [FromQuery] long? groupId, [FromQuery] long? userId, [FromQuery] bool isTeacher, [FromQuery] bool? isPublish, [FromQuery] int? commentId, [FromQuery] int? publishBy, [FromQuery] bool? isCommentPublish, int? LogedInUserId, [FromQuery] int? pageNum = 1, [FromQuery] bool EnableChat=false)
        {
            var result = await _blogService.GetMyChatterData(blogId, schoolId, categoryId, blogTypeId, groupId, userId, isTeacher, isPublish, commentId, publishBy, isCommentPublish, LogedInUserId, pageNum, EnableChat);
            return Ok(result);
        }
    }
}