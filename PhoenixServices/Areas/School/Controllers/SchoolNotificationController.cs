﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.School.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class SchoolNotificationController : ControllerBase
    {
        private readonly ISchoolNotificationService _schoolNotificationService;
        public SchoolNotificationController(ISchoolNotificationService schoolNotificationService)
        {
            _schoolNotificationService = schoolNotificationService;
        }

        /// <summary>
        /// Created by - Rohit Patil
        /// Created Date - 10th Feb 2020
        /// Description - Get School deployment notification of an school
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolDeploymentNotificationList")]
        [ProducesResponseType(typeof(IEnumerable<DeploymentNotification>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolDeploymentNotificationList()
        {
            IEnumerable<DeploymentNotification> lst = new List<DeploymentNotification>();
            lst = await _schoolNotificationService.GetSchoolDeploymentNotificationList();
            return Ok(lst);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 16 Feb 2020
        /// Description- Insert/update deployment notification message
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveDeploymentNotificationData")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveDeploymentNotificationData(DeploymentNotification model)
        {
            bool result = await _schoolNotificationService.SaveDeploymentNotificationData(model);
            return Ok(result);
        }
    }
}