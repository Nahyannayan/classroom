﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System;
using Phoenix.API.Models;
using Phoenix.API.Services;
using SIMS.API.Models;
using Phoenix.Models.Entities;
using System.Linq;

namespace Phoenix.API.Areas.ClassListControllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ClassListController : ControllerBase
    {
        private readonly IClassListService _ClassListService;

        public ClassListController(IClassListService ClassList_Service)
        {
            _ClassListService = ClassList_Service;
        }

        [HttpGet]
        [Route("GetClassList")]
        [ProducesResponseType(typeof(IEnumerable<ClassListModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetClassList(long SchoolId, long GroupId, long GradeId, long SectionId)
        {
            var result = await _ClassListService.GetClassList(SchoolId, GroupId, GradeId, SectionId);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.StudentImageUrl = Helpers.CommonHelper.GemsStudentImagePath(a.StudentImageUrl); return a;
                    });
                }
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStudentDetails")]
        [ProducesResponseType(typeof(BasicDetailModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentDetails(string stu_id)
        {
            var result = await _ClassListService.GetStudentDetails(stu_id);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetStudentProfileDetail")]
        [ProducesResponseType(typeof(StudentProfileModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentProfileDetail(long stu_id, long SchoolId, DateTime nowDate, short languageId = 1)
        {
            var result = await _ClassListService.GetStudentProfileDetail(stu_id, SchoolId, nowDate, languageId);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null)
                {
                    result.BasicDetailModel.StudentImageUrl = Helpers.CommonHelper.GemsStudentImagePath(result.BasicDetailModel.StudentImageUrl);
                    result.SiblingDetailModel = result.SiblingDetailModel.Select(a =>
                    {
                        a.Photopath = Helpers.CommonHelper.GemsStudentImagePath(a.Photopath); return a;
                    }).AsEnumerable();
                }
            }
            return Ok(result);
        }
        [HttpGet]
        [Route("GetQuickContactDetail")]
        [ProducesResponseType(typeof(IEnumerable<ParentDetailModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuickContactDetail(long StudentId, long SchoolId)
        {
            var result = await _ClassListService.GetQuickContactDetail(StudentId, SchoolId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetStudentFindMeDetail")]
        [ProducesResponseType(typeof(TimeTableModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentFindMeDetail(long StudentId, long SchoolId, DateTime nowDate)
        {
            var result = await _ClassListService.GetStudentFindMeDetail(StudentId, SchoolId, nowDate);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetWeeklyTimeTableDetail")]
        [ProducesResponseType(typeof(IEnumerable<WeekDayModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetWeeklyTimeTableDetail(long StudentId, long SchoolId, int WeekCount, DateTime nowDate, short languageId = 1)
        {
            var result = await _ClassListService.GetWeeklyTimeTableDetail(StudentId, SchoolId, WeekCount, nowDate, languageId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAttendanceChart")]
        [ProducesResponseType(typeof(AttendanceChart), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAttendanceChart(string stu_id)
        {
            var result = await _ClassListService.GetAttendanceChart(stu_id);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetAttendenceList")]
        [ProducesResponseType(typeof(AttendenceListModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAttendenceList(string stu_id, DateTime EndDate)
        {
            var result = await _ClassListService.GetAttendenceList(stu_id, EndDate);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetStudentOnReportMasters")]
        [ProducesResponseType(typeof(IEnumerable<StudentOnReportMasterModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentOnReportMasters(long studentId, long academicYearId, long schoolId)
        {
            var result = await _ClassListService.GetStudentOnReportMasters(studentId, academicYearId, schoolId);
            return Ok(result);
        }
        [HttpPost]
        [Route("GetStudentPhotoPath")]
        [ProducesResponseType(typeof(IEnumerable<ClassListModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentPhotoPath(string BSU_ID, long RPF_ID, [FromBody] string STU_ID)
        {
            var result = await _ClassListService.GetStudentPhotoPath(BSU_ID, RPF_ID, STU_ID);
            return Ok(result);
        }

        [HttpPost]
        [Route("GetStudentOnReportDetails")]
        [ProducesResponseType(typeof(IEnumerable<StudentOnReportModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentOnReportDetails(StudentOnReportParameterModel detailsParameter)
        {
            var result = await _ClassListService.GetStudentOnReportDetails(detailsParameter);
            return Ok(result);
        }

        [HttpPost]
        [Route("StudentOnReportMasterCU")]
        [ProducesResponseType(typeof(long), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> StudentOnReportMasterCU(StudentOnReportMasterModel studentOnReportMaster)
        {
            var result = await _ClassListService.StudentOnReportMasterCU(studentOnReportMaster);
            return Ok(result);
        }

        [HttpPost]
        [Route("StudentOnReportDetailsCU")]
        [ProducesResponseType(typeof(long), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> StudentOnReportDetailsCU(StudentOnReportModel studentOnReportDetail)
        {
            var result = await _ClassListService.StudentOnReportDetailsCU(studentOnReportDetail);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAttendanceProfileDetail")]
        [ProducesResponseType(typeof(AttendanceProfileModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAttendanceProfileDetail(long StudentId, long SchoolId, string AttendanceDt, string AttendanceType)
        {
            var result = await _ClassListService.GetAttendanceProfileDetail(StudentId, SchoolId, AttendanceDt, AttendanceType);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null)
                {
                    result.StudentDetail.StudentImageUrl = Helpers.CommonHelper.GemsStudentImagePath(result.StudentDetail.StudentImageUrl);
                }
            }
            return Ok(result);
        }
        [HttpGet]
        [Route("GetCourseWiseSchoolGroups")]
        [ProducesResponseType(typeof(ChangeGroupStudentModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseWiseSchoolGroups(long StudentId)
        {
            var result = await _ClassListService.GetCourseWiseSchoolGroups(StudentId);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null)
                {
                    result.StudentImageUrl = Helpers.CommonHelper.GemsStudentImagePath(result.StudentImageUrl);
                }
            }
            return Ok(result);
        }
        [HttpPost]
        [Route("ChangeCourseWiseStudentSchoolGroup")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ChangeCourseWiseStudentSchoolGroup(ChangeGroupStudentModel changeGroupStudentModel)
        {
            var result = await _ClassListService.ChangeCourseWiseStudentSchoolGroup(changeGroupStudentModel);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetStudentListBySearch")]
        [ProducesResponseType(typeof(IEnumerable<ClassListModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentListBySearch(long SchoolId, long UserId, string SearchString)
        {
            var result = await _ClassListService.GetStudentListBySearch(SchoolId, UserId, SearchString);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null)
                {
                    result = result.Select(a =>
                    {
                        a.StudentImageUrl = Helpers.CommonHelper.GemsStudentImagePath(a.StudentImageUrl); return a;
                    });
                }
            }
            return Ok(result);
        }
    }
}