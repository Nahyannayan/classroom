﻿// <summary>
// Created By: Raj M. Gogri
// Created On: 03/Oct/2020
// </summary>
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Helpers;
using Phoenix.API.Services.ChartsDashboard;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Phoenix.API.Models.ChartsDashboard.ChartModel;

namespace Phoenix.API.Areas.ChartsDashboard.Controllers
{
    /// <summary>
    /// Api Controller to handle http(s) request/response pipeline for StudentProgressTracker dashboard.
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ChartsDashboardController : ControllerBase
    {
        /// <value>
        /// Readonly IStudentProgressTrackerService field.
        /// </value>
        private readonly IChartsDashboardService service;

        /// <summary>
        /// Parameterized constructor with IStudentProgressTrackerService parameter.
        /// </summary>
        /// <param name="service">IStudentProgressTrackerService service for StudentProgressTracker dashboard.</param>
        public ChartsDashboardController(IChartsDashboardService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Created By: Raj M. Gogri
        /// Created On: 03/Oct/2020
        /// Description : To get filters for chart dashboard.
        /// </summary>
        /// <param name="req">FilterReq. Http request paramters</param>
        /// <returns>FilterRes. Http response body</returns>
        [HttpGet]
        [Route("GetFilters")]
        [ProducesResponseType(typeof(FilterRes), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(FilterReq), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetFilters([FromQuery] FilterReq req)
        {
            try
            {
                if (req == null || req.BSUID.IsNullOrEmpty())
                    return BadRequest("Missing BSUID (School Id) in query parameter");
                var result = await service.GetFiltersAync(req);
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                //ToDo:Log exception to Application Insights.
                //ToDo:Add server error with localized message in base class.
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occured, contact system administrator.");
            }
        }

        /// <summary>
        /// Created By: Raj M. Gogri
        /// Created On: 03/Oct/2020
        /// Description : To get charts data for StudentProgressTracker dashboard.
        /// </summary>
        /// <param name="req">ChartReq. Http request paramters</param>
        /// <returns>ChartRes. Http response body</returns>
        [HttpGet]
        [Route("GetStudentProgressTrackerCharts")]
        [ProducesResponseType(typeof(ChartRes), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ChartReq), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetStudentProgressTrackerCharts([FromQuery] ChartReq req)
        {
            try
            {
                if (req == null || req.BSUID.IsNullOrEmpty())
                    return BadRequest("Missing BSUID (School Id) in query parameter");
                var result = await service.GetStudentProgressTrackerChartAsync(req);
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                //ToDo:Log exception to Application Insights.
                //ToDo:Add server error with localized message in base class.
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occured, contact system administrator.");
            }
        }

        /// <summary>
        /// Created By: Raj M. Gogri
        /// Created On: 06/Oct/2020
        /// Description : To get student filters for Student Assessment Prediction dashboard.
        /// </summary>
        /// <param name="req">ChartReq. Http request paramters</param>
        /// <returns>List of FilStudent. Http response body</returns>
        [HttpGet]
        [Route("GetStudentFilters")]
        [ProducesResponseType(typeof(IEnumerable<FilStudent>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ChartReq), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetStudentFilters([FromQuery] ChartReq req)
        {
            try
            {
                if (req == null || req.BSUID.IsNullOrEmpty())
                    return BadRequest("Missing BSUID (School Id) in query parameter");
                var result = await service.GetStudentFiltersAync(req);
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                //ToDo:Log exception to Application Insights.
                //ToDo:Add server error with localized message in base class.
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occured, contact system administrator.");
            }
        }

        /// <summary>
        /// Created By: Raj M. Gogri
        /// Created On: 05/Oct/2020
        /// Description : To get charts data for Student Assessment Prediction dashboard.
        /// </summary>
        /// <param name="req">ChartReq. Http request paramters</param>
        /// <returns>ChartRes. Http response body</returns>
        [HttpGet]
        [Route("GetStudentAssessmentPredictionCharts")]
        [ProducesResponseType(typeof(ChartRes), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ChartReq), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetStudentAssessmentPredictionCharts([FromQuery] ChartReq req)
        {
            try
            {
                if (req == null || req.BSUID.IsNullOrEmpty())
                    return BadRequest("Missing BSUID (School Id) in query parameter");
                var result = await service.GetStudentAssessmentPredictionChartAsync(req);
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                //ToDo:Log exception to Application Insights.
                //ToDo:Add server error with localized message in base class.
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occured, contact system administrator.");
            }
        }

        /// <summary>
        /// Created By: Raj M. Gogri
        /// Created On: 06/Oct/2020
        /// Description : To get all assessment data for Student Assessment Prediction dashboard.
        /// </summary>
        /// <param name="req">ChartReq. Http request paramters</param>
        /// <returns>List of AssessmentDataByStudent. Http response body</returns>
        [HttpGet]
        [Route("GetAllAssessmentData")]
        [ProducesResponseType(typeof(IEnumerable<AssessmentDataByStudent>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ChartReq), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllAssessmentData([FromQuery] ChartReq req)
        {
            try
            {
                if (req == null || req.BSUID.IsNullOrEmpty())
                    return BadRequest("Missing BSUID (School Id) in query parameter");
                var result = await service.GetAllAssessmentDataAync(req);
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                //ToDo:Log exception to Application Insights.
                //ToDo:Add server error with localized message in base class.
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occured, contact system administrator.");
            }
        }
    }
}