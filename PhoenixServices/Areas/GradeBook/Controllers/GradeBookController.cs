﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.GradeBook
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class GradeBookController : ControllerBase
    {
        private readonly IGradeBookService gradeBookService;
        public GradeBookController(IGradeBookService gradeBookService)
        {
            this.gradeBookService = gradeBookService;
        }
        [HttpGet]
        [Route("GetTeacherGradeBook")]
        [ProducesResponseType(typeof(TeacherGradeBook), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTeacherGradeBook(long SchoolGroupId, long teacherId)
        {
            var result = await gradeBookService.GetTeacherGradeBook(SchoolGroupId, teacherId);
            return Ok(result);
        }
        [HttpPost]
        [Route("TeacherGradebookCreateNewRow")]
        [ProducesResponseType(typeof(long), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> TeacherGradebookCreateNewRow(TeacherInternalAssessment createNewRow)
        {
            var result = await gradeBookService.TeacherGradebookCreateNewRow(createNewRow);
            return Ok(result);
        }
        [HttpPost]
        [Route("InternalAssessmentCU")]
        [ProducesResponseType(typeof(List<InternalAssessmentScore>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> InternalAssessmentCU(List<InternalAssessmentScore> assessmentScore)
        {
            var result = await gradeBookService.InternalAssessmentCU(assessmentScore);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAssignmentQuizScore")]
        [ProducesResponseType(typeof(IEnumerable<GradeBookAssignmentQuiz>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentQuizScore(long SchoolGroupId, bool isAssignment)
        {
            var result = await gradeBookService.GetAssignmentQuizScore(SchoolGroupId, isAssignment);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetInternalAssessments")]
        [ProducesResponseType(typeof(TeacherGradeBook), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetInternalAssessments(long SchoolGroupId, long teacherId)
        {
            var result = await gradeBookService.GetInternalAssessments(SchoolGroupId, teacherId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStandardExaminations")]
        [ProducesResponseType(typeof(IEnumerable<StandardExamination>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStandardExaminations(long SchoolGroupId, int assessmentTypeId)
        {
            var result = await gradeBookService.GetStandardExaminations(SchoolGroupId, assessmentTypeId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetProgressTracker")]
        [ProducesResponseType(typeof(GradebookProgressTracker), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetProgressTracker(long schoolGroupId, DateTime? startDate = null, DateTime? endDate = null)
        {
            var result = await gradeBookService.GetProgressTracker(schoolGroupId,startDate, endDate);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetSubInternalAssessment")]
        [ProducesResponseType(typeof(TeacherInternalAssessment), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubInternalAssessment(long internalAssessmentId)
        {
            var result = await gradeBookService.GetSubInternalAssessment(internalAssessmentId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetInternalAssessmentScoreByAssessmentId")]
        [ProducesResponseType(typeof(IEnumerable<InternalAssessmentScore>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetInternalAssessmentScoreByAssessmentId(long internalAssessmentId)
        {
            var result = await gradeBookService.GetInternalAssessmentScoreByAssessmentId(internalAssessmentId);
            return Ok(result);
        }
    }
}