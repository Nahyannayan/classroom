﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.GradeBook
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class GradeBookSetupController : ControllerBase
    {
        private readonly IGradeBookSetupService gradeBookSetupService;
        public GradeBookSetupController(IGradeBookSetupService gradeBookSetupService)
        {
            this.gradeBookSetupService = gradeBookSetupService;
        }
        [HttpPost]
        [Route("SaveGradeBookForm")]
        [ProducesResponseType(typeof(long), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SaveGradeBookForm(Phoenix.Models.Entities.GradeBook gradeBook)
        {
            var result = await gradeBookSetupService.SaveGradeBookForm(gradeBook);
            return Ok(result);
        }
        [HttpPost]
        [Route("DeleteGradeBookDetail")]
        [ProducesResponseType(typeof(long), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteGradeBookDetail(Phoenix.Models.Entities.GradeBook gradeBook)
        {
            var result = await gradeBookSetupService.DeleteGradeBookDetail(gradeBook);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetGradeBookDetail")]
        [ProducesResponseType(typeof(IEnumerable<Phoenix.Models.Entities.GradeBook>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradeBookDetail(long gradebookId)
        {
            var result = await gradeBookSetupService.GetGradeBookDetail(gradebookId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetGradeBookDetailPagination")]
        [ProducesResponseType(typeof(IEnumerable<Phoenix.Models.Entities.GradeBook>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradeBookDetailPagination(int curriculumId, long schoolId, int pageNum, int pageSize, string searchString, string GradeIds, string CourseIds, string sortBy)
        {
            var result = await gradeBookSetupService.GetGradeBookDetailPagination(curriculumId, schoolId, pageNum, pageSize, searchString, GradeIds, CourseIds, sortBy);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetGradeAndCourseList")]
        [ProducesResponseType(typeof(IEnumerable<Phoenix.Models.Entities.GradeAndCourse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradeAndCourseList(int curriculumId, long SchoolId)
        {
            var result = await gradeBookSetupService.GetGradeAndCourseList(curriculumId, SchoolId);
            return Ok(result);
        }
        [HttpPost]
        [Route("GradebookFormulaCU")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GradebookFormulaCU(GradebookFormula gradebookFormula)
        {
            var result = await gradeBookSetupService.GradebookFormulaCU(gradebookFormula);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetGradebookFormulas")]
        [ProducesResponseType(typeof(IEnumerable<GradebookFormula>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradebookFormulas(long schoolId)
        {
            var result = await gradeBookSetupService.GetGradebookFormulas(schoolId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetGradebookFormulaDetailById")]
        [ProducesResponseType(typeof(GradebookFormula), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradebookFormulaDetailById(long FormulaId)
        {
            var result = await gradeBookSetupService.GetGradebookFormulaDetailById(FormulaId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetGradeListExceptGradebookGrade")]
        [ProducesResponseType(typeof(IEnumerable<Phoenix.Models.Entities.GradebookGrade>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradeListExceptGradebookGrade(long SchoolId, long GradebookId)
        {
            var result = await gradeBookSetupService.GetGradeListExceptGradebookGrade(SchoolId, GradebookId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetGradebookRuleSetups")]
        [ProducesResponseType(typeof(IEnumerable<GradebookRuleSetup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradebookRuleSetups(string assessmentIds)
        {
            var result = await gradeBookSetupService.GetGradebookRuleSetups(assessmentIds);
            return Ok(result);
        }
        [HttpGet]
        [Route("DeleteRuleProcessingSetup")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteRuleProcessingSetup(long ruleSetupId)
        {
            var result = await gradeBookSetupService.DeleteRuleProcessingSetup(ruleSetupId);
            return Ok(result);
        }

        [HttpPost]
        [Route("ProcessingRuleSetupCU")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ProcessingRuleSetupCU(GradebookRuleSetup ruleSetups)
        {
            var result = await gradeBookSetupService.ProcessingRuleSetupCU(ruleSetups);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetGradingTemplatesByIds")]
        [ProducesResponseType(typeof(IEnumerable<Phoenix.Models.GradingTemplateItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradingTemplatesByIds(string ids)
        {
            var result = await gradeBookSetupService.GetGradingTemplatesByIds(ids);
            return Ok(result);
        }
        [HttpGet]
        [Route("ValidateGradeAndCourse")]
        [ProducesResponseType(typeof(IEnumerable<GradeBookGradeDetail>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ValidateGradeAndCourse(string courseIds, string gradeIds)
        {
            var result = await gradeBookSetupService.ValidateGradeAndCourse(courseIds, gradeIds);
            return Ok(result);
        }
    }
}