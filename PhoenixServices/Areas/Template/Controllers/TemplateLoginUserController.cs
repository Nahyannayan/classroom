﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using System.Web;
using Phoenix.Common.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace Phoenix.API.Areas.Users.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class TemplateLoginUserController : ControllerBase
    {
        private readonly ITemplateLoginService _logInUserService;
       
        public TemplateLoginUserController(ITemplateLoginService logInUserService)
        {
            _logInUserService = logInUserService;
        }

        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 05/May/2019
        /// Description: To get login user details by username
        /// </summary>
        /// <param name="userName">username</param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        [Route("getloginuserbyusername")]
        [ProducesResponseType(typeof(IEnumerable<LogInUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserRoles(string userName)
        {
            var decryptUserName = EncryptDecryptHelper.DecryptUrl(userName);
            var result = await _logInUserService.GetLoginUserByUserName(decryptUserName);
          
            return Ok(result);
        }


        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 12/December/2019
        /// Description: To get login user details by username: Pass username in base64 encrypted string
        /// </summary>
        /// <param name="userName">username</param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        [Route("getloginuserbyencryptedusername")]
        [ProducesResponseType(typeof(IEnumerable<LogInUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetLoginUser(string userName)
        {
            var decodedUserName = EncryptDecryptHelper.DecodeBase64(userName);
            var result = await _logInUserService.GetLoginUserByUserName(decodedUserName);

            return Ok(result);
        }

        /// <summary>
        /// CreatedBy: Rohit Patil
        /// CreatedOn: 05/May/2019
        /// Description: To get all user details filtered by school
        /// </summary>
        /// <param name="schoolId">schoolId</param>
        /// <returns></returns>
        [HttpGet,Authorize]
        [Route("getuserlist/{schoolId:int}")]
        [ProducesResponseType(typeof(IEnumerable<LogInUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserList(int schoolId)
        {           
            var result = await _logInUserService.GetUserList(schoolId);
            return Ok(result);
        }


        [HttpGet, AllowAnonymous]
        [Route("IsValidUser")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public  async Task<IActionResult> IsValidUser(string userName, string Password)
        {
            var decodedUserName = EncryptDecryptHelper.DecryptUrl(userName);
            var decodedPassWord = EncryptDecryptHelper.DecryptUrl(Password);
            var result =  _logInUserService.IsValidUser(decodedUserName, decodedPassWord);
            return Ok(result);
        }

        [HttpPost, AllowAnonymous]
        [Route("studenttemplatebulkimport")]
        [ProducesResponseType(typeof(IEnumerable<StudentUploadTemplate>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> StudentTemplateBulkImport(long UserId, [FromBody] List<StudentUploadTemplate> StudentList)
        {
           var result = await _logInUserService.StudentTemplateBulkImport(StudentList, UserId);
            return Ok(result);
        }


        [HttpPost]
        [Route("stafftemplatebulkimport")]
        [ProducesResponseType(typeof(IEnumerable<StaffUploadTemplate>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult> StaffTemplateBulkImport(long UserId,[FromBody] List<StaffUploadTemplate> StaffList)
        {
            var result =await _logInUserService.StaffTemplateBulkImport(StaffList,UserId);
            return Ok(result);
        }

        [HttpPost]
        [Route("gradewisesubjecttemplatebulkimport")]
        [ProducesResponseType(typeof(IEnumerable<GRADE_WISE_SUBJECT_M>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult> GradeWiseSubjectTemplateBulkImport(long UserId, [FromBody] List<GRADE_WISE_SUBJECT_M> GradeWiseSubjctList)
        {
            var result =await _logInUserService.GradeWiseSubjectTemplateBulkImport(GradeWiseSubjctList, UserId);
            return Ok(result);
        }


        [HttpPost]
        [Route("studentwisesubjecttemplatebulkimport")]
        [ProducesResponseType(typeof(IEnumerable<STUDENT_WISE_SUBJECT_MAPPING>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult> StudentWiseSubjectTemplateBulkImport(long UserId, [FromBody] List<STUDENT_WISE_SUBJECT_MAPPING> StudentWiseSubjctList)
        {
            var result =await _logInUserService.StudentWiseSubjectTemplateBulkImport(StudentWiseSubjctList, UserId);
            return Ok(result);
        }

        [HttpGet, AllowAnonymous]
        [Route("resetpassword")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ResetPassword(string userName, string Password)
        {
            var decodedUserName = EncryptDecryptHelper.DecryptUrl(userName);
            var decodedPassWord = EncryptDecryptHelper.DecryptUrl(Password);
            var result =  _logInUserService.ResetPassword(decodedUserName, decodedPassWord);
            return Ok(result);
        }
    }
}