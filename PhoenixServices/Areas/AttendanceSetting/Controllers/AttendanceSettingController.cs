﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System;
using Phoenix.Models;
using Phoenix.API.Services;
using Phoenix.API.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.AttendanceSetting.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AttendanceSettingController : ControllerBase
    {
        private readonly IAttendanceSettingService _attendanceService;

        public AttendanceSettingController(IAttendanceSettingService attendanceService)
        {
            _attendanceService = attendanceService;

        }
        [HttpGet]
        [Route("GetGradeDetails")]
        [ProducesResponseType(typeof(IEnumerable<GradeDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradeDetails()
        {
            var result = await _attendanceService.GetGradeDetails();
            return Ok(result);
        }

        #region Attendance Calendar
        [HttpGet]
        [Route("GetGradeAndSectionList")]
        [ProducesResponseType(typeof(IEnumerable<GradeAndSection>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGradeAndSectionList(long SchoolId)
        {
            var result = await _attendanceService.GetGradeAndSectionList(SchoolId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetSchoolWeekEnd")]
        [ProducesResponseType(typeof(SchoolWeekEnd), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolWeekEnd(long BSU_ID)
        {
            var result = await _attendanceService.GetSchoolWeekEnd(BSU_ID);
            return Ok(result);
        }
        [HttpPost]
        [Route("SaveCalendarEvent")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> SaveCalendarEvent(AttendanceCalendar attendanceCalendar, string DATAMODE)
        {
            var result = await _attendanceService.SaveCalendarEvent(attendanceCalendar, DATAMODE);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetCalendarDetail")]
        [ProducesResponseType(typeof(AttendanceCalendar), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCalendarDetail(long SchoolId, long SCH_ID, bool IsListView)
        {
            var result = await _attendanceService.GetCalendarDetail(SchoolId, SCH_ID, IsListView);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetAcademicYearDetail")]
        [ProducesResponseType(typeof(AcademicYearDetail), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAcademicYearDetail(long SchoolId)
        {
            var result = await _attendanceService.GetAcademicYearDetail(SchoolId);
            return Ok(result);
        }
        #endregion Attendance Calendar

        #region Parameter Setting
        [HttpPost]
        [Route("SaveParameterSetting")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> SaveParameterSetting(ParameterSetting parameterSetting, string DATAMODE)
        {
            var result = await _attendanceService.SaveParameterSetting(parameterSetting, DATAMODE);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetParameterSettingList")]
        [ProducesResponseType(typeof(IEnumerable<ParameterSetting>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetParameterSettingList(long BSU_ID)
        {
            var result = await _attendanceService.GetParameterSettingList(BSU_ID);
            return Ok(result);
        }
        #endregion
        [HttpGet]
        [Route("GetLeaveApprovalPermission")]
        [ProducesResponseType(typeof(IEnumerable<LeaveApprovalPermissionModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetLeaveApprovalPermission(long ACD_ID, long schoolId, int divisionId)
        {
            var result = await _attendanceService.GetLeaveApprovalPermission(ACD_ID, schoolId, divisionId);
            return Ok(result);
        }
        [HttpPost]
        [Route("LeaveApprovalPermissionCU")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> LeaveApprovalPermissionCU(LeaveApprovalPermissionModel leaveApproval)
        {
            var result = await _attendanceService.LeaveApprovalPermissionCU(leaveApproval);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAttendanceType")]
        [ProducesResponseType(typeof(IEnumerable<AttendanceType>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAttendanceType(long BSU_ID)
        {
            var result = await _attendanceService.GetAttendanceType(BSU_ID);
            return Ok(result);
        }
        [HttpPost]
        [Route("SaveAttendanceType")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> SaveAttendanceType(AttendanceType AttendanceType, string DATAMODE)
        {
            var result = await _attendanceService.SaveAttendanceType(AttendanceType, DATAMODE);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAttendancePeriod")]
        [ProducesResponseType(typeof(IEnumerable<AttendacePeriodModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAttendancePeriodList(long gradeId)
        {
            var result = await _attendanceService.GetAttendancePeriodList(gradeId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAttendanceTypeListBYId")]
        [ProducesResponseType(typeof(IEnumerable<AttendanceType>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAttendanceTypeListBYId(long AttendanceConfigurationTypeID)
        {
            var result = await _attendanceService.GetAttendanceTypeListBYId(AttendanceConfigurationTypeID);
            return Ok(result);
        }
        [HttpPost]
        [Route("AddUpdateAttendancePeriod")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> AddUpdateAttendancePeriod(long schoolId, long academicId, string gradeId, long CreatedBy, List<AttendacePeriodModel> objAttendancePeriod)
        {
            var result = _attendanceService.AddUpdateAttendancePeriod(schoolId, academicId, gradeId, CreatedBy, objAttendancePeriod);
            return Ok(result);
        }
    }
}