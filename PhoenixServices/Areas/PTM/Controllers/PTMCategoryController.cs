﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.PTM.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PTMCategoryController : ControllerBase
    {
        private readonly IPTMCategoryService _ptmCategoryService;
        public PTMCategoryController(IPTMCategoryService ptmCategoryService)
        {
            _ptmCategoryService = ptmCategoryService;
        }
        /// <summary>
        /// Created By - Vipin Kalal
        /// Created Date - 16 Dec 2020
        /// Description - To Get all PTM Category.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getptmcategory")]
        [ProducesResponseType(typeof(IEnumerable<CategoryList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPTMCategory(int SchoolGradeId, int languageId = 1)
        {
            var PTMCategoryList = await _ptmCategoryService.GetPTMCategory(languageId, SchoolGradeId);
            return Ok(PTMCategoryList);
        }

        /// <summary>
        /// Created By - Vipin Kalal
        /// Created Date - 16 Dec 2020
        /// Description - To Get  PTM Category by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="langaugeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getptmcategoryById")]
        [ProducesResponseType(typeof(CategoryList), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPTMCategoryById(int id, short langaugeId = 1)
        {
            var Suggestion = await _ptmCategoryService.GetPTMCategoryById(id, langaugeId);
            return Ok(Suggestion);
        }

        /// <summary>
        /// Created By - Vipin Kalal
        /// Created Date - 16 Dec 2020
        /// Description - To Insert PTM Category.
        /// </summary>
        /// <param name="categoryList"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertptmcategory")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertPTMCategory([FromBody] CategoryList categoryList)
        {
            var result = _ptmCategoryService.InsertPTMCategory(categoryList);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vipin Kalal
        /// Created Date - 16 Dec 2020
        /// Description - To Update PTM Category.
        /// </summary>
        /// <param name="categoryList"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateptmcategory")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdatePTMCategory([FromBody] CategoryList categoryList)
        {
            var result = _ptmCategoryService.UpdatePTMCategory(categoryList);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vipin Kalal
        /// Created Date - 16 Dec 2020
        /// Description - To Update PTM Category.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteptmcategory/{id:int}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeletePTMCategory(int id)
        {
            var result = _ptmCategoryService.DeletePTMCategory(id);
            return Ok(result);
        }
    }
}
