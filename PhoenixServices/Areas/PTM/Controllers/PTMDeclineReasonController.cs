﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.PTM.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PTMDeclineReasonController : ControllerBase
    {
        private readonly IPTMDeclineReasonService _ptmDeclineReasonService;
        public PTMDeclineReasonController(IPTMDeclineReasonService ptmDeclineReasonService)
        {
            _ptmDeclineReasonService = ptmDeclineReasonService;
        }

        /// <summary>
        /// Created By - Vipin Kalal
        /// Created Date - 16 Dec 2020
        /// Description - To Get all PTM Decline Reason.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getptmdeclinereason")]
        [ProducesResponseType(typeof(IEnumerable<DeclineReasonList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPTMDeclineReason(int SchoolGradeId, int languageId = 1)
        {
            var PTMCategoryList = await _ptmDeclineReasonService.GetPTMDeclineReason(languageId, SchoolGradeId);
            return Ok(PTMCategoryList);
        }

        /// <summary>
        /// Created By - Vipin Kalal
        /// Created Date - 16 Dec 2020
        /// Description - To Get  PTM Decline Reason by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="langaugeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getptmdeclinereasonById")]
        [ProducesResponseType(typeof(DeclineReasonList), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPTMDeclineReasonById(int id, short langaugeId = 1)
        {
            var Suggestion = await _ptmDeclineReasonService.GetPTMDeclineReasonById(id, langaugeId);
            return Ok(Suggestion);
        }

        /// <summary>
        /// Created By - Vipin Kalal
        /// Created Date - 16 Dec 2020
        /// Description - To Insert PTM Decline Reason.
        /// </summary>
        /// <param name="declineReasonList"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertptmdeclinereason")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertPTMDeclineReason([FromBody] DeclineReasonList declineReasonList)
        {
            var result = _ptmDeclineReasonService.InsertPTMDeclineReason(declineReasonList);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vipin Kalal
        /// Created Date - 16 Dec 2020
        /// Description - To Update PTM Decline Reason.
        /// </summary>
        /// <param name="declineReasonList"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateptmdeclinereason")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdatePTMDeclineReason([FromBody] DeclineReasonList declineReasonList)
        {
            var result = _ptmDeclineReasonService.UpdatePTMDeclineReason(declineReasonList);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vipin Kalal
        /// Created Date - 16 Dec 2020
        /// Description - To Update PTM Decline Reason.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteptmdeclinereason/{id:int}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeletePTMDeclineReason(int id)
        {
            var result = _ptmDeclineReasonService.DeletePTMDeclineReason(id);
            return Ok(result);
        }
    }
}
