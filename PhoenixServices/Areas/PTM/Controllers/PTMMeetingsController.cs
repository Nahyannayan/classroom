﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models;

namespace Phoenix.API.Areas.PTM.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PTMMeetingsController : ControllerBase
    {
        private readonly IPTMMeetingService _meetingService;
        public PTMMeetingsController(IPTMMeetingService meetingService)
        {
            _meetingService = meetingService;
        }
        /// <summary>
        /// Created By - HATIM
        /// Created Date - 17 Dec 2020
        /// Description - To Insert PTM Meeting.
        /// </summary>
        /// <param name="meeting"></param>
        /// <returns>Integar value</returns>
        [HttpPost]
        [Route("InsertPTMeeting")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> InsertPTMeeting([FromBody] PTMMeeting meeting)
        {
            var result = await _meetingService.InsertPTMMeeting(meeting);
            return Ok(result);
        }
        /// <summary>
        /// Created By - HATIM
        /// Created Date - 17 Dec 2020
        /// Description - To Update PTM Meeting.
        /// </summary>
        /// <param name="meeting"></param>
        /// <returns>Integar value</returns>
        [HttpPost]
        [Route("UpdatePTMeeting")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> UpdatePTMeeting([FromBody] PTMMeeting meeting)
        {
            var result = await _meetingService.UpdatePTMMeeting(meeting);
            return Ok(result);
        }
        /// <summary>
        /// Created By - HATIM
        /// Created Date - 17 Dec 2020
        /// Description - To Delete an existed PTM Meeting in records.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Integar value</returns>
        [HttpDelete]
        [Route("DeletePTMeeting")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeletePTMeeting(int Id)
        {
            var result = await _meetingService.DeletePTMMeeting(Id);
            return Ok(result);
        }
    }
}
