﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services.ExemplarWall.Contracts;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.ExemplarWall.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ExemplarWallController : ControllerBase
    {
        private readonly IExemplarWallService _exemplarWallService;

        public ExemplarWallController(IExemplarWallService exemplarWallService)
        {
            _exemplarWallService = exemplarWallService;
        }
        /// <summary>
        /// Author: Shankar Kadam
        /// Added On: 07/16/2020
        /// Description: Save Exemplar Wall Details
        /// </summary>
        /// <param name="exemplarWallModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveExemplarWall")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> SaveExemplarWall([FromBody] ExemplarWallModel exemplarWallModel)
        {
            var result = _exemplarWallService.AddUpdateExemplarWall(exemplarWallModel, TransactionModes.Insert);
            return Ok(result);
        }


        /// <summary>
        /// Author: Shankar Kadam
        /// Added On: 07/16/2020
        /// Description: Update Exemplar Wall Details
        /// </summary>
        /// <param name="exemplarWallModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateExemplarWall")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateExemplarWall([FromBody] ExemplarWallModel exemplarWallModel)
        {
            var result = _exemplarWallService.AddUpdateExemplarWall(exemplarWallModel, TransactionModes.Update);
            return Ok(result);
        }

        /// <summary>
        /// Author: Deependra Sharma
        /// Added On: 21/07/2020
        /// Description: Save Share Post
        /// </summary>
        /// <param name="sharepostModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ShareExemplarWall")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> ShareExemplarWall([FromBody] sharepost sharepostModel)
        {
            var result = _exemplarWallService.ShareExemplarWall(sharepostModel, TransactionModes.Update);
            return Ok(result);
        }


        /// <summary>
        /// Author: Shankar Kadam
        /// Added On: 07/16/2020
        /// Description: Delete Exemplar Wall Details
        /// </summary>
        /// <param name="exemplarWallModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteExemplarWall")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteExemplarWall([FromBody] ExemplarWallModel exemplarWallModel)
        {
            var result = _exemplarWallService.DeleteExemplarWall(exemplarWallModel);
            return Ok(result);
        }

        /// <summary>
        /// 
        /// Author: Shankar Kadam
        /// Added On: 07/16/2020
        /// Description: Get ExemplarWallDetail By SchoolId
        /// </summary>
        /// <param name="SchoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetExemplarWallDetailBySchoolId")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetExemplarWallDetailBySchoolId(long SchoolId)
        {
            var result = await _exemplarWallService.GetExemplarWallDetailBySchoolId(SchoolId);
            return Ok(result);
        }

        /// <summary>
        /// Author: Shankar Kadam
        /// Added On: 07/16/2020
        /// Description: Get ExemplarWallDetail By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetExemplarDetailById/{id:long}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetExemplarDetailById(long id)
        {
            var result = await _exemplarWallService.GetExemplarWall(id);
            return Ok(result);
        }
        /// <summary>
        /// Created By: Deependra Sharma
        /// Created On: 19/July/2020
        /// Description: To get all blogs by group and blogtypeId
        /// </summary>
        /// <param name="SchoolLevel"></param>
        /// <param name="Department"></param>
        /// <param name="CourseId"></param>
        /// <param name="SchoolId"></param>
        ///<param name="groupId"></param>
        /// <param name="blogTypeId"></param>
        /// <param name="isPublish"></param>
        ///<param name="UserId"></param>
        /// <param name="CreatedBy"></param>
        /// <param name="SearchText"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getblogsbygroupnblogtypeid")]
        [ProducesResponseType(typeof(IEnumerable<ExemplarUserDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogByGroupNBlogTypeId(int SchoolLevel, int Department, int CourseId, int SchoolId, int groupId, int blogTypeId, int? isPublish, Int64 UserId, Int64 CreatedBy, string SearchText, bool MyExemplarWork)
        {
            var result = await _exemplarWallService.GetBlogsByGroupNBlogtypeId(SchoolLevel, Department, CourseId, SchoolId, groupId, blogTypeId, isPublish, UserId, CreatedBy, SearchText, MyExemplarWork);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        if (a.studentList != null && a.studentList.Count > 0)
                        {
                            a.studentList = a.studentList.Select(s =>
                            {
                                s.UserImageFilePath = Helpers.CommonHelper.GemsStudentImagePath(s.UserImageFilePath);
                                return s;
                            }).ToList();
                        }
                        return a;
                    });

                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By: Deependra Sharma
        /// Created On: 19/July/2020
        /// Description: To get all blogs by group and blogtypeId
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="languageId"></param>

        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolLevelBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<SchoolLevel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBlogByGroupNBlogTypeId(long schoolId, short languageId = 1)
        {
            var result = await _exemplarWallService.GetSchoolLevelBySchoolId(schoolId, languageId);
            return Ok(result);
        }
        /// <summary>
        /// Created By: Deependra Sharma
        /// Created On: 23/July/2020
        /// Description: To get all blogs by group and blogtypeId
        /// </summary>
        /// <param name="LevelId"></param>
        /// <param name="UserId"></param>
        /// <param name="CourseId"></param>
        /// <param name="SchoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSchoolGroupBasedOnSchoolLevel")]
        [ProducesResponseType(typeof(IEnumerable<SchoolGroup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolGroupBasedOnSchoolLevel(long LevelId, long UserId, long CourseId, long SchoolId)
        {
            var result = await _exemplarWallService.GetSchoolGroupBasedOnSchoolLevel(LevelId, UserId, CourseId, SchoolId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Shankar Kadam
        /// Created Date - 29 July 2020
        /// Description - To get pending exemplar post for approvla by schoolid
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPendingForApprovalExemplarPost")]
        [ProducesResponseType(typeof(IEnumerable<ExemplarWallModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPendingForApprovalExemplarPost(long schoolId)
        {
            var lessonPlanFilterModule = await _exemplarWallService.GetPendingForApprovalExemplarPost(schoolId);
            return Ok(lessonPlanFilterModule);
        }

        /// <summary>
        /// Created By - Shankar Kadam
        /// Created Date - 29 July 2020
        /// Description - Update ExemplarPost Status From Pending To Approve
        /// </summary>
        /// <param name="ExemplarPostId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UpdateExemplarPostStatusFromPendingToApprove")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateExemplarPostStatusFromPendingToApprove(long ExemplarPostId)
        {
            var result = await _exemplarWallService.UpdateExemplarPostStatusFromPendingToApprove(ExemplarPostId);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Shankar Kadam
        /// Created Date - 29 July 2020
        /// Description - Get Approval Status Details
        /// </summary>
        /// <param name="SchoolId"></param>
        /// <param name="Opearion"></param>
        /// <param name="FlagType"></param>
        /// <param name="Value"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetApprovalStatusDetails")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetApprovalStatusDetails(long SchoolId, string Opearion, string FlagType, bool Value)
        {
            var result = await _exemplarWallService.GetApprovalStatusDetails(SchoolId, Opearion, FlagType, Value);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetSchoolDepartmentList")]
        [ProducesResponseType(typeof(IEnumerable<SchoolDepartment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolDepartmentList(long schoolId, short languageId = 1)
        {
            var result = await _exemplarWallService.GetSchoolDepartmentList(schoolId, languageId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetCourseByDepartment")]
        [ProducesResponseType(typeof(IEnumerable<Phoenix.Models.Course>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseByDepartment(long DepartmentId)
        {
            var result = await _exemplarWallService.GetCourseByDepartment(DepartmentId);
            return Ok(result);
        }
    }
}
