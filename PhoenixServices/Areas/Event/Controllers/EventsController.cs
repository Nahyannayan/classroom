﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.Extensions.Configuration;
using Phoenix.API.Services;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Models;

namespace Phoenix.API.Areas.Event.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class EventsController : ControllerBase
    {
        private readonly IEventsService _eventsService;
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly IConfiguration _config;
        private readonly ISchoolService _schoolService;
        public EventsController(IEventsService eventsService, ISchoolGroupService schoolGroupService, IConfiguration config, ISchoolService schoolService)
        {
            _eventsService = eventsService;
            _schoolGroupService = schoolGroupService;
            _config = config;
            _schoolService = schoolService;
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 27 June 2019
        /// Description - To insert Events
        /// </summary>
        /// <param name="eventView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("EventInsert")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> EventInsert([FromBody] EventView eventView)
        {
            bool success = true;
            if (eventView.OnlineMeetingType == (short)EventCategoryEnum.ZoomMeeting)
            {
                var accessToken = _schoolGroupService.GetZoomMeetingAccessToken();
                var zoomMeetingView = new ZoomMeetingView()
                {
                    MeetingName = eventView.Title,
                    IsSSOEnabled = Convert.ToBoolean(_config.GetValue<string>("SynchronousLesson:SSO")),
                    MeetingPassword = eventView.EventPassword,
                    MeetingDuration = eventView.MeetingDuration.ToString(),
                    FormattedMeetingDateTime = eventView.SelectedDate,
                    ZoomEmail = eventView.EventUserEmail,
                    AccessToken = accessToken,
                    ContactName = eventView.CreatedByUserName,
                    IsRecurringMeeting = eventView.IsRecurringEvent,
                    RecurringDays = eventView.RecurringEventDays,
                    MeetingEndDateTime = eventView.EndDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'")
                };
                var meetingInfo = await SynchronousClassHelper.CreateZoomMeeting(zoomMeetingView);
                if (!string.IsNullOrEmpty(meetingInfo.id))
                {
                    meetingInfo.CreatedBy = eventView.CreatedBy;
                    meetingInfo.IsPerStudent = true;
                    eventView.OnlineMeetingId = meetingInfo.id;
                    meetingInfo.IsSSOEnabled = zoomMeetingView.IsSSOEnabled;
                    long masterId = await _schoolGroupService.UpdateZoomMeetingInfo(meetingInfo);
                    eventView.MeetingMasterId = masterId;
                    if (!meetingInfo.IsSSOEnabled)
                    {
                        List<ZoomMeetingResponse> meetingParticipants = new List<ZoomMeetingResponse>();
                        var usersEmails = await _schoolGroupService.GetMemberMailingDetails(eventView.SelectedPlannerMemberId);
                        foreach (var item in usersEmails.Where(e => !string.IsNullOrEmpty(e.Email)))
                        {
                            var registrantsData = await SynchronousClassHelper.AddZoomMeetingRegistrants(item, meetingInfo.id, accessToken);
                            if (!string.IsNullOrEmpty(registrantsData.join_url))
                                meetingParticipants.Add(new ZoomMeetingResponse { join_url = registrantsData.join_url, id = meetingInfo.id, UserId = item.MemberId });
                        }
                        if (meetingParticipants.Count > 0)
                        {
                            success = await _schoolGroupService.UpdateZoomMeetingParticipants(meetingParticipants);
                        }
                    }
                }
            }
            else if (eventView.OnlineMeetingType == (short)EventCategoryEnum.TeamsMeeting)
            {
                eventView.TeamMeeting.CreatedBy = eventView.UserId;
                eventView.TeamMeeting.IsPerUser = true;
                eventView.OnlineMeetingId = eventView.TeamMeeting.id;
                long teamsMeetingId = await _schoolGroupService.UpdateTeamsMeetingInfo(eventView.TeamMeeting);
                eventView.MeetingMasterId = teamsMeetingId;
            }
            var result = _eventsService.EventInsert(eventView);
            if (eventView.OnlineMeetingType == (short)EventCategoryEnum.WebRTC)
            {
                var webRTCTokenView = await _schoolService.GetWebRTCAuthToken();
                if (webRTCTokenView.ResponseCode == (short)StatusCodes.Status200OK)
                {
                    var eventUsers = await _schoolGroupService.GetMemberMailingDetails(eventView.SelectedPlannerMemberId);
                    var list = eventUsers.ToList();
                    list.Add(new GroupMemberMapping { Email = eventView.EventUserEmail, IsHost = true });
                    var res = await _schoolService.AddWebRTCEventUsers(result, eventView.Title, list, webRTCTokenView.AuthToken);
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 27 June 2019
        /// Description - To Update Events
        /// </summary>
        /// <param name="eventView"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("EventUpdate")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> EventUpdate([FromBody] EventView eventView)
        {
            bool success = false;
            //if (!string.IsNullOrEmpty(eventView.OnlineMeetingId) && eventView.OldMeetingType == (short)EventCategoryEnum.ZoomMeeting)
            //{
            //    await SynchronousClassHelper.DeleteMeeting(eventView.OnlineMeetingId, _schoolGroupService.GetZoomMeetingAccessToken());
            //    success = await _schoolGroupService.DeleteOnlineMeetingEvent(eventView.OnlineMeetingId);
            //    eventView.OnlineMeetingId = !success ? eventView.OnlineMeetingId : string.Empty;
            //}

            if (!string.IsNullOrEmpty(eventView.OnlineMeetingId) && eventView.OldMeetingType == (short)EventCategoryEnum.TeamsMeeting)
            {
                //success = await ();
                // eventView.OnlineMeetingId = !success ? eventView.OnlineMeetingId : string.Empty;
            }

            if (eventView.OnlineMeetingType == (short)EventCategoryEnum.ZoomMeeting)
            {
                var isSSOEnabled = Convert.ToBoolean(_config.GetValue<string>("SynchronousLesson:SSO"));
                var accessToken = _schoolGroupService.GetZoomMeetingAccessToken();
                var zoomMeetingView = new ZoomMeetingView()
                {
                    MeetingName = eventView.Title,
                    IsSSOEnabled = Convert.ToBoolean(_config.GetValue<string>("SynchronousLesson:SSO")),
                    MeetingPassword = eventView.EventPassword,
                    MeetingDuration = eventView.MeetingDuration.ToString(),
                    FormattedMeetingDateTime = eventView.SelectedDate,
                    ZoomEmail = eventView.EventUserEmail,
                    AccessToken = accessToken,
                    ContactName = eventView.CreatedByUserName,
                    ZoomMeetingId = eventView.OnlineMeetingId,
                    IsRecurringMeeting = eventView.IsRecurringEvent,
                    RecurringDays = eventView.RecurringEventDays,
                    MeetingEndDateTime = eventView.EndDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'")
                };
                if (!string.IsNullOrEmpty(eventView.OnlineMeetingId) && eventView.OldMeetingType == (short)EventCategoryEnum.ZoomMeeting)
                {
                    await SynchronousClassHelper.UpdateZoomMeeting(zoomMeetingView);
                    if (!isSSOEnabled)
                    {
                        var allParticipantsInfo = await _schoolGroupService.GetOnlineEventParticipants(eventView.OnlineMeetingId, eventView.SelectedPlannerMemberId);
                        List<ZoomMeetingResponse> meetingParticipants = new List<ZoomMeetingResponse>();
                        foreach (var item in allParticipantsInfo.Where(e => !string.IsNullOrEmpty(e.Email) && e.IsNew))
                        {
                            var registrantsData = await SynchronousClassHelper.AddZoomMeetingRegistrants(item, eventView.OnlineMeetingId, accessToken);
                            if (!string.IsNullOrEmpty(registrantsData.join_url))
                                meetingParticipants.Add(new ZoomMeetingResponse { join_url = registrantsData.join_url, id = eventView.OnlineMeetingId, UserId = item.MemberId });
                        }
                        if (meetingParticipants.Count > 0)
                        {
                            success = await _schoolGroupService.UpdateZoomMeetingParticipants(meetingParticipants, string.Join(",", allParticipantsInfo.Where(e => e.IsDeleted).Select(e => e.MemberId)));
                        }
                    }
                }
                else
                {
                    var meetingInfo = await SynchronousClassHelper.CreateZoomMeeting(zoomMeetingView);
                    if (!string.IsNullOrEmpty(meetingInfo.id))
                    {
                        eventView.OnlineMeetingId = meetingInfo.id;
                        meetingInfo.CreatedBy = eventView.CreatedBy;
                        meetingInfo.IsPerStudent = true;
                        meetingInfo.IsSSOEnabled = isSSOEnabled;
                        long masterMeetingId = await _schoolGroupService.UpdateZoomMeetingInfo(meetingInfo);
                        eventView.MeetingMasterId = masterMeetingId;
                        if (!meetingInfo.IsSSOEnabled)
                        {
                            List<ZoomMeetingResponse> meetingParticipants = new List<ZoomMeetingResponse>();
                            var usersEmails = await _schoolGroupService.GetMemberMailingDetails(eventView.SelectedPlannerMemberId);
                            foreach (var item in usersEmails.Where(e => !string.IsNullOrEmpty(e.Email)))
                            {
                                var registrantsData = await SynchronousClassHelper.AddZoomMeetingRegistrants(item, meetingInfo.id, accessToken);
                                if (!string.IsNullOrEmpty(registrantsData.join_url))
                                    meetingParticipants.Add(new ZoomMeetingResponse { join_url = registrantsData.join_url, id = meetingInfo.id, UserId = item.MemberId });
                            }
                            if (meetingParticipants.Count > 0)
                            {
                                success = await _schoolGroupService.UpdateZoomMeetingParticipants(meetingParticipants);
                            }
                        }
                    }
                }
            }
            else if (eventView.OnlineMeetingType == (short)EventCategoryEnum.TeamsMeeting)
            {
                eventView.TeamMeeting.CreatedBy = eventView.UserId;
                eventView.TeamMeeting.IsPerUser = true;
                eventView.OnlineMeetingId = eventView.TeamMeeting.id;
                long teamsMeetingId = await _schoolGroupService.UpdateTeamsMeetingInfo(eventView.TeamMeeting);
                eventView.MeetingMasterId = teamsMeetingId;
            }
            var result = _eventsService.EventUpdate(eventView);
            return Ok(result);
        }

        /// <summary>
        /// Created By - vinayak y
        /// Created Date - 16 JAN 2020
        /// Description - To Accept Event Request
        /// </summary>
        /// <param name="eventId">eventId</param>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("acceptEventRequest")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AcceptEventRequest(int eventId, long userId)
        {
            var result = _eventsService.AcceptEventRequest(eventId, userId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 20 JAN 2020
        /// Description - To Accept External User Event Request
        /// </summary>
        /// <param name="eventId">eventId</param>
        /// <param name="emailId">emailId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("acceptEventRequestExternalUser")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AcceptEventRequestExternalUser(int eventId, string emailId)
        {
            var result = _eventsService.AcceptEventRequestExternalUser(eventId, emailId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 30 June 2019
        /// Description - To get all Event.
        /// </summary>
        /// <param name="id">Event Id</param>
        /// <param name="userId">User Id</param>
        /// <param name="languageId">languageId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getEvent")]
        [ProducesResponseType(typeof(IEnumerable<EventView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetEvent(int id, long? userId, short languageId = 1)
        {
            var eventList = await _eventsService.GetEvent(id, userId, languageId);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - girish s
        /// Created Date - 30 June 2019
        /// Description - To get all EventCategory.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getEventCategory")]
        [ProducesResponseType(typeof(IEnumerable<EventCategoryView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetEventCategory(long schoolId)
        {
            var eventList = await _eventsService.GetEventCategory(schoolId);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - girish s
        /// Created Date - 30 June 2019
        /// Description - To get all EventType.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getEventType")]
        [ProducesResponseType(typeof(IEnumerable<EventTypeView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetEventType()
        {
            var eventList = await _eventsService.GetEventType();
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - girish s
        /// Created Date - 30 June 2019
        /// Description - To get all EventDuration.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getEventDuration")]
        [ProducesResponseType(typeof(IEnumerable<EventDurationView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetEventDuration()
        {
            var eventList = await _eventsService.GetEventDuration();
            return Ok(eventList);
        }


        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 30 June 2019
        /// Description - To get all Event.
        /// </summary>
        /// <param name="Id">User Id</param>
        /// <param name="fromDate">fromDate</param>
        /// <param name="toDate">toDate</param>
        /// <param name="categoryId">categoryId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getEventByUserId")]
        [ProducesResponseType(typeof(IEnumerable<EventView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetEventByUserId(long Id, DateTime fromDate, DateTime toDate, int? categoryId)
        {
            var eventList = await _eventsService.GetMothlyEvents(Id, fromDate, toDate, categoryId);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 21 June 2020
        /// Description - To get all Events by categoryIds.
        /// </summary>
        /// <param name="Id">User Id</param>
        /// <param name="fromDate">fromDate</param>
        /// <param name="toDate">toDate</param>
        /// <param name="eventCategoryIds">eventCategoryIds</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAllEventByUserId")]
        [ProducesResponseType(typeof(IEnumerable<EventView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllEventByUserId(long Id, DateTime fromDate, DateTime toDate, string eventCategoryIds)
        {
            var eventList = await _eventsService.GetAllMothlyEvents(Id, fromDate, toDate, eventCategoryIds);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 09 SEPT 2020
        /// Description - To get online meetings, other events
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId"></param>
        /// <param name="schoolId"></param>
        /// <param name="fromDate">fromDate</param>
        /// <param name="toDate">toDate</param>
        /// <param name="isOnlineMeetingEvents"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getOnlineMeetingEvents")]
        [ProducesResponseType(typeof(IEnumerable<EventView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetOnlineMeetingEvents(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isOnlineMeetingEvents, short languageId = 1)
        {
            var eventList = await _eventsService.GetOnlineMeetingEvents(pageNumber, pageSize, userId, schoolId, fromDate, toDate, isOnlineMeetingEvents, languageId);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 09 SEPT 2020
        /// Description - To get school timetable events with pagination
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId"></param>
        /// <param name="schoolId"></param>
        /// <param name="fromDate">fromDate</param>
        /// <param name="toDate">toDate</param>
        /// <param name="isTeacher"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSchoolTimetableEvents")]
        [ProducesResponseType(typeof(IEnumerable<WeeklyEventView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolTimetableEvents(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            var eventList = await _eventsService.GetSchoolTimetableEvents(pageNumber, pageSize, userId, schoolId, fromDate, toDate, isTeacher);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 11 OCT 2020
        /// Description - To get school timetable events report data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getTimetableEventsReport")]
        [ProducesResponseType(typeof(IEnumerable<WeeklyEventView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTimetableEventsReport(TimetableReportsRequest model)
        {
            var eventList = await _eventsService.GetTimetableEventsReport(model);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 14 OCT 2020
        /// Description - To get online meeting events report data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getLiveSessionsReport")]
        [ProducesResponseType(typeof(IEnumerable<EventView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetLiveSessionsReport(TimetableReportsRequest model)
        {
            var eventList = await _eventsService.GetLiveSessionsReport(model);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 29 June 2020
        /// Description - To get weekly timetable events
        /// </summary>
        /// <param name="userId">userId</param>
        /// <param name="schoolId">schoolId</param>
        /// <param name="fromDate">fromDate</param>
        /// <param name="toDate">toDate</param>
        /// <param name="isTeacher">isTeacher</param>
        /// <param name="isDashboardEvents">isDashboardEvents</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getWeeklyTimeTableEvents")]
        [ProducesResponseType(typeof(IEnumerable<WeeklyEventView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetWeeklyTimeTableEvents(long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher, bool isDashboardEvents)
        {
            var eventList = await _eventsService.GetWeeklyTimeTableEvents(userId, schoolId, fromDate, toDate, isTeacher, isDashboardEvents);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 01 JULY 2020
        /// Description - To get today or weekly timetable events
        /// </summary>
        /// <param name="userId">userId</param>
        /// <param name="schoolId">schoolId</param>
        /// <param name="fromWeekDay">fromWeekDay</param>
        /// <param name="toWeekDay">toWeekDay</param>
        /// <param name="fromDate">fromDate</param>
        /// <param name="toDate">toDate</param>
        /// <param name="isTeacher">isTeacher</param>
        /// <param name="isWeeklyEvent">isWeeklyEvent</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getTodayOrWeeklyTimeTableEvents")]
        [ProducesResponseType(typeof(IEnumerable<WeeklyEventView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTodayOrWeeklyTimeTableEvents(long userId, long schoolId, int fromWeekDay, int toWeekDay, DateTime fromDate, DateTime toDate, bool isTeacher, bool isWeeklyEvent)
        {
            var eventList = await _eventsService.GetTodayOrWeeklyTimeTableEvents(userId, schoolId, fromWeekDay, toWeekDay, fromDate, toDate, isTeacher, isWeeklyEvent);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 02 SEPT 2020
        /// Description - To get all planner and timetable events data
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="schoolId">schoolId</param>
        /// <param name="fromDate">fromDate</param>
        /// <param name="toDate">toDate</param>
        /// <param name="isTeacher">isTeacher</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getPlannerTimetableData")]
        [ProducesResponseType(typeof(PlannerTimetableView), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPlannerTimetableData(long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            var plannerData = await _eventsService.GetPlannerTimetableData(userId, schoolId, fromDate, toDate, isTeacher);
            return Ok(plannerData);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 17 SEPT 2020
        /// Description - To get all planner and timetable events data with paging
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId">User Id</param>
        /// <param name="schoolId">schoolId</param>
        /// <param name="fromDate">fromDate</param>
        /// <param name="toDate">toDate</param>
        /// <param name="isTeacher">isTeacher</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getPlannerTimetableDataWithPaging")]
        [ProducesResponseType(typeof(PlannerTimetableView), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPlannerTimetableDataWithPaging(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            var plannerData = await _eventsService.GetPlannerTimetableDataWithPaging(pageNumber, pageSize, userId, schoolId, fromDate, toDate, isTeacher);
            return Ok(plannerData);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 18 SEPT 2020
        /// Description - To get timetable events data with paging
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId">User Id</param>
        /// <param name="schoolId">schoolId</param>
        /// <param name="fromDate">fromDate</param>
        /// <param name="toDate">toDate</param>
        /// <param name="isTeacher">isTeacher</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getTimetableDataWithPaging")]
        [ProducesResponseType(typeof(IEnumerable<WeeklyEventView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTimetableDataWithPaging(int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher)
        {
            var plannerData = await _eventsService.GetTimetableDataWithPaging(pageNumber, pageSize, userId, schoolId, fromDate, toDate, isTeacher);
            return Ok(plannerData);
        }


        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 11 AUG 2020
        /// Description - To get event user details
        /// </summary>
        /// <param name="eventId">eventId</param>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getEventUserDetails")]
        [ProducesResponseType(typeof(EventUser), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetEventUserDetails(int eventId, long userId)
        {
            var eventList = await _eventsService.GetEventUserDetails(eventId, userId);
            return Ok(eventList);
        }


        /// <summary>
        /// Created By - vinayak y
        /// Created Date - 13 JAN 2020
        /// Description - To get all Eventuser by event ID.
        /// </summary>
        /// <param name="eventId">eventId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getInternalEventUserByEventId")]
        [ProducesResponseType(typeof(IEnumerable<EventUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetInternalEventUserByEventId(int eventId)
        {
            var eventList = await _eventsService.GetInternalEventUser(eventId);
            return Ok(eventList);
        }
        /// <summary>
        /// Created By - vinayak y
        /// Created Date - 13 JAN 2020
        /// Description - To get all External Eventuser by event ID.
        /// </summary>
        /// <param name="eventId">eventId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getExternalEventUserByEventId")]
        [ProducesResponseType(typeof(IEnumerable<EventUser>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetExternalEventUserByEventId(int eventId)
        {
            var eventList = await _eventsService.GetExternalEventUser(eventId);
            return Ok(eventList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 21 JAN 2020
        /// Description - To cancel Event by id
        /// </summary>
        /// <param name="eventId">id</param>
        /// <param name="onlineMeetingId">id</param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CancelEvent")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> CancelEvent(int eventId, string onlineMeetingId, long userId)
        {
            var result = _eventsService.CancelEvent(eventId, userId);
            if (result == 1 && !string.IsNullOrEmpty(onlineMeetingId))
            {
                await SynchronousClassHelper.DeleteMeeting(onlineMeetingId, _schoolGroupService.GetZoomMeetingAccessToken());
            }
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 09 FEB 2020
        /// Description - Delete event file
        /// </summary>
        /// <param name="eventId">id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteEventFile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteEventFile(int eventId)
        {
            var result = _eventsService.DeleteEventFile(eventId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 1 OCT 2020
        /// Description - To check if the timetable event exists
        /// </summary>
        /// <param name="title"></param>
        /// <param name="meetingPassword"></param>
        /// <param name="startDate"></param>
        /// <param name="startTime"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("checkTimetableEventExists")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> CheckTimetableEventExists(string title, string meetingPassword, DateTime startDate, string startTime, long userId)
        {
            var result = _eventsService.CheckTimetableEventExists(title, meetingPassword, startDate, startTime, userId);
            return Ok(result);
        }
    }
}