﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Models;
using Phoenix.Models;

namespace Phoenix.API.Areas.Event.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class EventCategoryController : ControllerBase
    {
        private readonly IEventCategoryService _eventCategoryService;

        public EventCategoryController(IEventCategoryService eventCategoryService)
        {
            _eventCategoryService = eventCategoryService;
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 27 June 2019
        /// Description - To get all GetEventCategory.
        /// </summary>
        /// <param name="id">EventCategory Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getEventCategory")]
        [ProducesResponseType(typeof(IEnumerable<EventCategoryView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetEventCategory(int id)
        {
            var eventCategoryList = await _eventCategoryService.GetEventCategory(id);
            return Ok(eventCategoryList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 19 JAN 2020
        /// Description - To Get All Event Category By School
        /// </summary>
        /// <param name="schoolId">schoolId</param>
        /// <param name="languageId">languageId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAllEventCategoryBySchool")]
        [ProducesResponseType(typeof(IEnumerable<EventCategoryView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllEventCategoryBySchool(int schoolId,short languageId=1)
        {
            var eventCategoryList = await _eventCategoryService.GetAllEventCategoryBySchool(schoolId, languageId);
            return Ok(eventCategoryList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 05 FEB 2020
        /// Description - To Get All Event Categories by SchoolId
        /// </summary>
        /// <param name="schoolId">schoolId</param>
        /// <param name="languageId">languageId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getEventCategoriesBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<EventCategoryView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetEventCategoriesBySchoolId(int schoolId, short languageId = 1)
        {
            var eventCategoryList = await _eventCategoryService.GetEventCategoriesBySchoolId(schoolId, languageId);
            return Ok(eventCategoryList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 20 JAN 2020
        /// Description - To Get  Event Category by Id.
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("geteventcategorybyid")]
        [ProducesResponseType(typeof(EventCategory), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetEventCategoryById(int Id,short languageId=1)
        {
            var category = await _eventCategoryService.GetEventCategoryById(Id, languageId);
            return Ok(category);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 20 JAN 2020
        /// Description - To insert Event Category.
        /// </summary>
        /// <param name="eventCategory"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("inserteventcategory")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertEventCategory([FromBody]EventCategory eventCategory)
        {
            var result = _eventCategoryService.InsertEventCategory(eventCategory);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 20 JAN 2020
        /// Description - To Update Event Category.
        /// </summary>
        /// <param name="eventCategory"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateEventCategory")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateEventCategory([FromBody]EventCategory eventCategory)
        {
            var result = _eventCategoryService.UpdateEventCategory(eventCategory);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 20 JAN 2020
        /// Description - To Delete Event Category by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteEventCategory/{id:int}")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteEventCategory(int id)
        {
            var result = _eventCategoryService.DeleteEventCategory(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 04 FEB 2020
        /// Description - to check event category available or not.
        /// </summary>
        /// <param name="categoryName">categoryName</param>
        /// <param name="id">id</param>
        /// <param name="schoolId">schoolId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("CheckEventCategoryAvailable")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult> CheckEventCategoryAvailable(string categoryName, int id, long schoolId)
        {
            var result =  _eventCategoryService.CheckEventCategoryAvailable(categoryName, id, schoolId);
            return Ok(result);
        }

    }
}