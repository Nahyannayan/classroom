﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Models;
using Phoenix.Models;

namespace Phoenix.API.Areas.Event.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class DurationController : ControllerBase
    {
        private readonly IDurationService _durationService;

        public DurationController(IDurationService durationService)
        {
            _durationService = durationService;
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 26 June 2019
        /// Description - To get all Duration.
        /// </summary>
        /// <param name="id">Duration Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getDuration")]
        [ProducesResponseType(typeof(IEnumerable<DurationView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDuration(int id)
        {
            var durationList = await _durationService.GetDuration(id);
            return Ok(durationList);
        }
       
    }
}