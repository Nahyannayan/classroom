﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Models.Entities;

namespace Phoenix.API.Areas.Behaviour.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BehaviourSetupController : ControllerBase
    {
        private readonly IBehaviourSetupService behaviourSetupService;

        public BehaviourSetupController(IBehaviourSetupService behaviourSetupService)
        {
            this.behaviourSetupService = behaviourSetupService;
        }
        [HttpGet]
        [Route("GetSubCategoryList")]
        [ProducesResponseType(typeof(IEnumerable<BehaviourSetup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubCategoryList(long MainCategoryId, long SchoolId,short languageId=1)
        {
            var result = await behaviourSetupService.GetSubCategoryList(MainCategoryId, SchoolId, languageId);
            return Ok(result);
        }
        [HttpPost]
        [Route("SaveSubCategory")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> SaveSubCategory(BehaviourSetup behaviourSetup, string DATAMODE)
        {
            var result = await behaviourSetupService.SaveSubCategory(behaviourSetup, DATAMODE);
            return Ok(result);
        }
        #region Action Hierarchy
        [HttpGet]
        [Route("GetDesignations")]
        [ProducesResponseType(typeof(IEnumerable<Designations>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDesignations(long schoolId)
        {
            var result = await behaviourSetupService.GetDesignations(schoolId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetDesignationsRoutings")]
        [ProducesResponseType(typeof(IEnumerable<DesignationsRouting>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDesignationsRoutings(long schoolId, long? designationFrom = null)
        {
            var result = await behaviourSetupService.GetDesignationsRoutings(schoolId, designationFrom);
            return Ok(result);
        }

        [HttpPost]
        [Route("DesignationBySchoolCUD")]
        [ProducesResponseType(typeof(IEnumerable<DesignationsRouting>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DesignationBySchoolCUD(DesignationsRoutingCUD designationsRouting)
        {
            var result = await behaviourSetupService.DesignationBySchoolCUD(designationsRouting);
            return Ok(result);
        }
        #endregion
        #region Certificate Schedule
        [HttpGet]
        [Route("GetCertificateSchedulings")]
        [ProducesResponseType(typeof(IEnumerable<CertificateScheduling>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCertificateSchedulings(long? CertificateSchedulingId = null, long? academicYear = null, long? schoolId = null, int? scheduleType = null,short languageId=1)
        {
            var result = await behaviourSetupService.GetCertificateSchedulings(CertificateSchedulingId, academicYear, schoolId, scheduleType, languageId);
            return Ok(result);
        }

        [HttpPost]
        [Route("CertificateSchedulingCUD")]
        [ProducesResponseType(typeof(long), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CertificateSchedulingCUD(CertificateScheduling certificateScheduling)
        {
            var result = await behaviourSetupService.CertificateSchedulingCUD(certificateScheduling);
            return Ok(result);
        }
        #endregion
    }
}