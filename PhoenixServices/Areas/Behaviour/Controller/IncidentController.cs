﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System;
using Phoenix.API.Services;
using Phoenix.Models.Entities;
using System.Linq;
using Phoenix.Models;

namespace Phoenix.API.Areas.Assignments.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class IncidentController : ControllerBase
    {
        private readonly IIncidentService _IncidentService;
        private readonly IStudentListService studentListService;

        public IncidentController(IIncidentService IIncidentService, IStudentListService studentListService)
        {
            _IncidentService = IIncidentService;
            this.studentListService = studentListService;
        }

        #region Incident
        [HttpGet]
        [Route("GetIncidentList")]
        [ProducesResponseType(typeof(IncidentDashBoardModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetIncidentList(long schoolId = 0, long curriculumId = 0, int month = 0, bool isFA = true)
        {
            var incidentLists = await _IncidentService.GetIncidentList(schoolId, curriculumId, month, isFA);
            return Ok(incidentLists);
        }

        [HttpGet]
        [Route("GetIncident")]
        [ProducesResponseType(typeof(IncidentModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetIncident(long IncidentId)
        {
            var result = await _IncidentService.GetIncident(IncidentId);
            if (Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null)
                {
                    if (result.StudentLists != null && result.StudentLists.Count() > 0)
                    {
                        result.StudentLists = result.StudentLists.Select(a =>
                        {
                            a.Student_Image_url = Helpers.CommonHelper.GemsStudentImagePath(a.Student_Image_url); return a;
                        });
                    }
                }
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetIncidentChart")]
        [ProducesResponseType(typeof(IEnumerable<ChartModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetIncidentChart(long schoolId, long academicYearId, int month, bool isCategory)
        {
            var result = await _IncidentService.GetIncidentChartByCategory(schoolId, academicYearId, month, isCategory);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStudentByIncidentId")]
        [ProducesResponseType(typeof(IEnumerable<IncidentStudentList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentByIncidentId(long incidentId)
        {
            var result = await _IncidentService.GetStudentByIncidentId(incidentId);
            return Ok(result);
        }

        [HttpPost]
        [Route("GetIncidentEntryCUD")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> GetIncidentEntryCUD(IncidentEntry incidentEntry)
        {
            var result = await _IncidentService.IncidentEntryCUD(incidentEntry);
            return Ok(result);
        }

        //#region IncidentAction
        [HttpGet]
        [Route("GetBehaviourAction")]
        [ProducesResponseType(typeof(IEnumerable<ActionDetails>), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> GetBehaviourAction(long incidentId, long studentId)
        {
            var student = await _IncidentService.GetStudentByIncidentId(incidentId);
            var result = await _IncidentService.GetBehaviourAction(incidentId, studentId);
            return Ok(new ActionModel { Student = student.FirstOrDefault(x => x.STUDENT_ID == studentId), ActionDetails = result });
        }

        [HttpGet]
        [Route("GetBehaviourActionFollowups")]
        [ProducesResponseType(typeof(IEnumerable<BehaviourActionFollowup>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBehaviourActionFollowups(long incidentId, long actionId)
        {
            var result = await _IncidentService.GetBehaviourActionFollowups(incidentId, actionId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetFollowUpDesignations")]
        [ProducesResponseType(typeof(IEnumerable<FollowUpDesignation>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFollowUpDesignations(long schoolId, long incidentId, long UserId)
        {
            var result = await _IncidentService.GetFollowUpDesignations(schoolId, incidentId, UserId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetFollowUpStaffs")]
        [ProducesResponseType(typeof(IEnumerable<FollowUpStaff>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFollowUpStaffs(long schoolId, long designationId)
        {
            var result = await _IncidentService.GetFollowUpStaffs(schoolId, designationId);
            return Ok(result);
        }

        [HttpPost]
        [Route("ActionCUD")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> ActionCUD(ActionModel actionModel)
        {
            var result = await _IncidentService.ActionCUD(actionModel);
            return Ok(result);
        }

        [HttpPost]
        [Route("ActionFollowUpCUD")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> ActionFollowUpCUD(BehaviourActionFollowup behaviourActionFollowup)
        {
            var result = await _IncidentService.ActionFollowUpCUD(behaviourActionFollowup);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetSchoolTeachersBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<GroupTeacher>), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolTeachersBySchoolId(string SchoolId)
        {
            var result = await _IncidentService.GetSchoolTeachersBySchoolId(SchoolId);
            return Ok(result);
        }
        //#endregion
        #endregion

    }
}