﻿using Microsoft.AspNetCore.Mvc;
using SIMS.API.Services;
using SIMS.API.Models;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Http;
using Phoenix.Models.Entities;
using Phoenix.Common.Helpers.Extensions;
using Microsoft.Extensions.Configuration;
using Serilog;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.Security.Principal;
using Phoenix.Common.Helpers;
using Phoenix.API.Services;
using System.IO;
using System.Linq;

namespace SIMS.API.Areas.Behaviour.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BehaviourController : ControllerBase
    {
        private readonly IBehaviourService _BehaviourService;
        private readonly IConfiguration _config;
        private readonly ILogger _logger;
        private readonly IFileService _fileService;
        private readonly IAttachmentService attachmentService;
        SafeAccessTokenHandle safeAccessTokenHandle;
        public BehaviourController(IBehaviourService Behaviour_Service, IConfiguration _config, ILogger _logger, IFileService _fileService, IAttachmentService attachmentService)
        {
            _BehaviourService = Behaviour_Service;
            this._config = _config;
            this._logger = _logger;
            this._fileService = _fileService;
            this.attachmentService = attachmentService;
        }

        /// <summary>
        /// Created By: Fraz Ahmed
        /// Created On: 20/June/2019
        /// Description: To get the class list from timetable id or grade and section.
        /// <param name="tt_id">TimeTable id </param>
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentList")]
        [ProducesResponseType(typeof(IEnumerable<ClassList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentList(string username, int tt_id = 0, string grade = null, string section = null)
        {
            var result = await _BehaviourService.GetStudentList(username, tt_id, grade, section);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetCourseGroupList")]
        [ProducesResponseType(typeof(IEnumerable<CourseGroupModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCourseGroupList(long TeacherId, long SchoolId)
        {
            var result = await _BehaviourService.GetCourseGroupList(TeacherId, SchoolId);
            return Ok(result);
        }
        /// <summary>
        /// Created By: Fraz Ahmed
        /// Created On: 14/July/2019
        /// Description: To load the behaviour details by student id.
        /// <param name="stu_id">Student id </param>
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("LoadBehaviourByStudentId")]
        [ProducesResponseType(typeof(IEnumerable<ClassList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> LoadBehaviourByStudentId(string stu_id)
        {
            var result = await _BehaviourService.LoadBehaviourByStudentId(stu_id);
            return Ok(result);
        }
        /// <summary>
        /// Created By: Fraz Ahmed
        /// Created On: 14/July/2019
        /// Description: To load the behaviour details by behaviour id.
        /// <param name="id">Behaviour id </param>
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetBehaviourById")]
        [ProducesResponseType(typeof(IEnumerable<BehaviorDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBehaviourById(int id)
        {
            var result = await _BehaviourService.GetBehaviourById(id);
            return Ok(result);
        }

        /// <summary>
        /// Created By: Fraz Ahmed
        /// Created On: 17/July/2019
        /// Description: To insert/update the behaviour details by behaviour id.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertBehaviourDetails")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> InsertBehaviourDetails(string bsu_id, string mode, [FromBody] BehaviourDetails entity)
        {
            var result = await _BehaviourService.InsertBehaviourDetails(entity, bsu_id, mode);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetListOfStudentBehaviour")]
        [ProducesResponseType(typeof(IEnumerable<StudentBehaviour>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetListOfStudentBehaviour()
        {
            var result = await _BehaviourService.GetListOfStudentBehaviour();
            return Ok(result);
        }

        [HttpPost]
        [Route("InsertUpdateStudentBehavior")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> InsertUpdateStudentBehavior(List<StudentBehaviourFiles> lstbehaviourfiles, long studentId = 0, int behaviourId = 0, string behaviourComment = "")
        {
            var result = _BehaviourService.InsertUpdateStudentBehavior(lstbehaviourfiles, studentId, behaviourId, behaviourComment);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStudentBehaviorByStudentId")]
        [ProducesResponseType(typeof(IEnumerable<StudentBehaviour>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentBehaviorByStudentId(long studentId = 0)
        {
            var result = await _BehaviourService.GetStudentBehaviorByStudentId(studentId);
            return Ok(result);
        }

        [HttpPost]
        [Route("InsertBulkStudentBehaviour")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> InsertBulkStudentBehaviour(List<StudentBehaviourFiles> studentBehaviourFiles, string bulkStudentIds, int behaviourId = 0, string behaviourComment = "")
        {
            var result = _BehaviourService.InsertBulkStudentBehaviour(studentBehaviourFiles, bulkStudentIds, behaviourId, behaviourComment);
            return Ok(result);
        }

        [HttpGet]
        [Route("UpdateBehaviourTypes")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> UpdateBehaviourTypes(int behaviourId = 0, string behaviourType = "", int behaviourPoint = 0, int categoryId = 0)
        {
            var result = _BehaviourService.UpdateBehaviourTypes(behaviourId, behaviourType, behaviourPoint, categoryId);
            return Ok(result);
        }


        [HttpGet]
        [Route("GetFileDetailsByStudentId")]
        [ProducesResponseType(typeof(IEnumerable<StudentBehaviourMerit>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFileDetailsByStudentId(long studentId = 0)
        {
            var result = await _BehaviourService.GetFileDetailsByStudentId(studentId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetBehaviourClassList")]
        [ProducesResponseType(typeof(IEnumerable<ClassList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBehaviourClassList(string username, int tt_id = 0, string grade = null, string section = null, long GroupId = 0, bool IsFilterByGroup = false)
        {
            var result = await _BehaviourService.GetBehaviourClassList(username, tt_id, grade, section, GroupId, IsFilterByGroup);
            if (Phoenix.API.Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.Student_Image_url = Phoenix.API.Helpers.CommonHelper.GemsStudentImagePath(a.Student_Image_url); return a;
                    });
                }
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetBehaviourStudentListByGroupId")]
        [ProducesResponseType(typeof(IEnumerable<ClassList>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetBehaviourStudentListByGroupId(long groupId)
        {
            var result = await _BehaviourService.GetBehaviourStudentListByGroupId(groupId);
            if (Phoenix.API.Helpers.CommonHelper.StudentImageHasFullPath)
            {
                if (result != null && result.Count() > 0)
                {
                    result = result.Select(a =>
                    {
                        a.Student_Image_url = Phoenix.API.Helpers.CommonHelper.GemsStudentImagePath(a.Student_Image_url); return a;
                    });
                }
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("DeleteStudentBehaviourMapping")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> DeleteStudentBehaviourMapping(long studentId = 0, int behaviourId = 0)
        {
            var result = _BehaviourService.DeleteStudentBehaviourMapping(studentId, behaviourId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetSubCategoriesByCategoryId")]
        [ProducesResponseType(typeof(IEnumerable<SubCategories>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubCategoriesByCategoryId(long categoryId, string BSU_ID, string GRD_ID, long GroupId,short languageId=1)
        {
            var result = await _BehaviourService.GetSubCategoriesByCategoryId(categoryId, BSU_ID, GRD_ID, GroupId, languageId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetMeritDetails")]
        [ProducesResponseType(typeof(IEnumerable<StudentBehaviourMerit>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMeritDetails(long meritId)
        {
            var result = await _BehaviourService.GetMeritDetails(meritId);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetMeritCategoryByStudent")]
        [ProducesResponseType(typeof(IEnumerable<SubCategories>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMeritCategoryByStudent(long schoolId, long studentId,short languageId=1)
        {
            var result = await _BehaviourService.GetMeritCategoryByStudent(schoolId, studentId, languageId);
            return Ok(result);
        }

        [HttpPost]
        [Route("InsertMeritDemerit")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> InsertMeritDemerit(string schoolId, int academicId, DateTime? incidentDate, MeritDemerit objMeritDemerit)
        {
            var result = await _BehaviourService.InsertMeritDemerit(schoolId, academicId, incidentDate, objMeritDemerit);
            return Ok(result);
        }

        [HttpPost]
        [Route("UploadBehaviourFile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> UploadBehaviourFile(IFormFile file, long SchoolId, long AttachedToId)
        {
            var storeDirectory = "/Resources/StudentBehaviour/School_" + SchoolId.ToString() + "/";
            List<Attachment> AttachmentList = new List<Attachment>();
            Attachment attachment = new Attachment();
            var result = false;

            if (file.Length > 0)
            {
                try
                {
                    string resourceDir = _config["FileServer:WritePath"] + storeDirectory;

                    ImpersonateLogin(out safeAccessTokenHandle);

                    WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                    {
                        //_logger.Information("\n Impersonate success: ");
                        CommonHelper.CreateDestinationFolder(resourceDir);
                        var _fileTypeList = _fileService.GetFileTypes("").Result;
                        var fileName = file.FileName;

                        var extension = Path.GetExtension(fileName).Replace(".", "");
                        fileName = Guid.NewGuid() + "_" + fileName;
                        string resourceDirWithFileName = Path.Combine(resourceDir, fileName);
                        using (var stream = System.IO.File.Create(resourceDirWithFileName))
                        {
                            file.CopyTo(stream);
                        }
                        var modelFile = (attachmentService.GetAttachment(AttachedToId, Phoenix.Models.Entities.AttachmentKey.StudentBehaviour.EnumToString())).Result;

                        attachment.AttachmentKey = Phoenix.Models.Entities.AttachmentKey.StudentBehaviour.EnumToString();
                        attachment.AttachmentType = AttachmentType.File;
                        attachment.AttachmentPath = storeDirectory + fileName;
                        attachment.AttachedToId = AttachedToId;
                        attachment.FileExtension = extension;
                        attachment.FileName = fileName;
                        attachment.Mode = modelFile != null ? TranModes.Update : TranModes.Insert;
                        attachment.AttachmentId = modelFile != null ? modelFile.AttachmentId : 0;
                        AttachmentList.Add(attachment);
                    });

                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Error while uploading files");
                }
            }
            if (AttachedToId > 0 && AttachmentList.Count > 0)
            {
                result = await attachmentService.AttachmentCU(AttachmentList);
            }
            return Ok(result);
        }

        private void ImpersonateLogin(out SafeAccessTokenHandle safeAccessTokenHandle)
        {
            string userName = _config["Impersonate:userName"];
            string password = _config["Impersonate:password"];
            string domainName = _config["Impersonate:domainName"];

            const int LOGON32_PROVIDER_DEFAULT = 0;
            //This parameter causes LogonUser to create a primary token.     
            const int LOGON32_LOGON_INTERACTIVE = 2;
            // Call LogonUser to obtain a handle to an access token. 
            bool returnValue = LogonUser(userName, domainName, password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, out safeAccessTokenHandle);
            if (!returnValue)
            {
                int ret = Marshal.GetLastWin32Error();
                _logger.Error("LogonUser failed with error code", ret);
                throw new System.ComponentModel.Win32Exception(ret);
            }
        }

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
        int dwLogonType, int dwLogonProvider, out SafeAccessTokenHandle phToken);

        [HttpGet]
        [Route("GetStudentPointCategory")]
        [ProducesResponseType(typeof(IEnumerable<StudentPointCategory>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentPointCategory(int schoolId, long academicYearId, int scheduleType)
        {
            var result = await _BehaviourService.GetStudentPointCategory(schoolId, academicYearId, scheduleType);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetStudentPointCategorySchedule")]
        [ProducesResponseType(typeof(IEnumerable<StudentPointCategory>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentPointCategory(string sectionId, int? scheduleType = null, long? CertificateScheduleId = null, DateTime? date = null, long? studentId = null)
        {
            var result = await _BehaviourService.GetStudentPointCategory(sectionId, scheduleType, CertificateScheduleId, date, studentId);
            return Ok(result);
        }

        [HttpPost]
        [Route("CertificateProcessLogCU")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CertificateProcessLogCU(List<CertificateProcessLog> processLogs)
        {
            var result = await _BehaviourService.CertificateProcessLogCU(processLogs);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetStudentOnReportMDetail")]
        [ProducesResponseType(typeof(IEnumerable<StudentOnReportMaster>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentOnReportMDetail(long studentId)
        {
            var result = await _BehaviourService.GetStudentOnReportMDetail(studentId);
            return Ok(result);
        }
        [HttpPost]
        [Route("StudentOnReportMCU")]
        [ProducesResponseType(typeof(long), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> StudentOnReportMCU(StudentOnReportMaster studentOnReportMaster)
        {
            var result = await _BehaviourService.StudentOnReportMCU(studentOnReportMaster);
            return Ok(result);
        }
        [HttpPost]
        [Route("GetStudentOnReportDetails")]
        [ProducesResponseType(typeof(IEnumerable<StudentOnReportDetail>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentOnReportDetails(StudentOnReportDetailsParameter detailsParameter)
        {
            var result = await _BehaviourService.GetStudentOnReportDetails(detailsParameter);
            return Ok(result);
        }
        [HttpPost]
        [Route("StudentOnReportDetailsCU")]
        [ProducesResponseType(typeof(long), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> StudentOnReportDetailsCU(StudentOnReportDetail studentOnReportDetail)
        {
            var result = await _BehaviourService.StudentOnReportDetailsCU(studentOnReportDetail);
            return Ok(result);
        }

    }
}