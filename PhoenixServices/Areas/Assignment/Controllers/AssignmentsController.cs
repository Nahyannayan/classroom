﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Helpers;
using Phoenix.Models;

namespace Phoenix.API.Areas.Assignments.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class AssignmentsController : ControllerBase
    {
        private readonly IAssignmentService _assignmentService;

        public AssignmentsController(IAssignmentService AssignmentService)
        {
            _assignmentService = AssignmentService;
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 12 May 2019
        /// Description - To get  Assignments by teacher Id.
        /// </summary>
        /// <param name="teacherId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignments")]
        [ProducesResponseType(typeof(IEnumerable<Assignment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignments(int teacherId)
        {
            var assignmentList = await _assignmentService.GetAssignments(teacherId);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 12 May 2019
        /// Description - To get assignemnt by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentById/{id:int}")]
        [ProducesResponseType(typeof(Assignment), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentById(int id)
        {
            var assignment = await _assignmentService.GetAssignmentById(id);
            return Ok(assignment);
        }


        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 27 Dec 2020
        /// Description - To get assignmentSubmitted marks by assignmentStudentId.
        /// </summary>
        /// <param name="assignmentStudentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentSubmitMarksByAssignmentStudentId/{assignmentStudentId:int}")]
        [ProducesResponseType(typeof(AssignmentStudent), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentSubmitMarksByAssignmentStudentId(int assignmentStudentId)
        {
            var assignment = await _assignmentService.GetAssignmentSubmitMarksByAssignmentStudentId(assignmentStudentId);
            return Ok(assignment);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 27 Dec 2020
        /// Description - To get taskSubmitted marks by TaskId.
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTaskSubmitMarksByTaskId/{taskId:int}")]
        [ProducesResponseType(typeof(StudentTask), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTaskSubmitMarksByTaskId(int taskId)
        {
            var assignment = await _assignmentService.GetTaskSubmitMarksByTaskId(taskId);
            return Ok(assignment);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 07 dec 2020
        /// Description - To get assignemnt Category master by its ID and SchoolId.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentCategoryMasterById/{id:int}")]
        [ProducesResponseType(typeof(AssignmentCategoryDetails), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentCategoryMasterById(int id)
        {
            var assignment = await _assignmentService.GetAssignmentCategoryMasterById(id);
            return Ok(assignment);
        }


        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 07 dec 2020
        /// Description - To get assignemnt Category master by its ID.
        /// </summary>
        /// <param name="SchoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentCategoryBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentCategoryDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentCategoryBySchoolId(long SchoolId)
        {
            var assignment = await _assignmentService.GetAssignmentCategoryBySchoolId(SchoolId);
            return Ok(assignment);
        }


        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 02 Sep 2020
        /// Description - To get assignemnt details for export Data to Excel by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetExcelReportDataAssignmentById/{id:int}")]
        [ProducesResponseType(typeof(Assignment), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetExcelReportDataAssignmentById(int id)
        {
            var assignment = await _assignmentService.GetExcelReportDataAssignmentById(id);
            return Ok(assignment);
        }


        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 12 May 2019
        /// Description - To insert assignment
        /// </summary>
        /// <param name="assignment"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertAssignment")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertAssignment([FromBody] Assignment assignment)
        {
            OperationDetails op = new OperationDetails();
            int result = _assignmentService.InsertAssignment(assignment);
            if (result > 0)
            {
                op.Success = true;
                op.InsertedRowId = result;
            }
            else
            {
                op.Success = false;
                op.InsertedRowId = 0;
            }
            return Ok(op);
        }

        /// <summary>
        /// Created By - Mahesh Chikhle
        /// Created Date - 08 July 2020
        /// Description - To save assignment detsil
        /// </summary>
        /// <param name="assignment"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveAssignmentDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> SaveAssignmentDetails([FromBody] Assignment assignment)
        {
            OperationDetails op = new OperationDetails();
            int result = _assignmentService.SaveAssignmentDetails(assignment);
            if (result > 0)
            {
                op.Success = true;
                op.InsertedRowId = result;
            }
            else
            {
                op.Success = false;
                op.InsertedRowId = 0;
            }
            return Ok(op);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 12 May 2019
        /// Description - To update assignment.
        /// </summary>
        /// <param name="assignment"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateAssignment")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateAssignment([FromBody] Assignment assignment)
        {
            OperationDetails op = new OperationDetails();
            int result = _assignmentService.UpdateAssignmentData(assignment);
            if (result > 0)
            {
                op.Success = true;
                op.InsertedRowId = result;
            }
            else
            {
                op.Success = false;
                op.InsertedRowId = 0;
            }
            return Ok(op);
        }

        /// <summary>
        /// Updated By - Girish Sonawane
        /// Created Date - 15/09/2019
        /// Description - To delete assignment.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteAssignment")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteAssignment([FromBody] Assignment assignment)
        {
            OperationDetails op = new OperationDetails();
            int result = _assignmentService.DeleteAssignment(assignment);
            if (result > 0)
            {
                op.Success = true;
                op.InsertedRowId = result;
            }
            else
            {
                op.Success = false;
                op.InsertedRowId = 0;
            }
            return Ok(op);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 27/10/2020
        /// Description - To delete assignment Comment.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteAssignmentComment")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteAssignmentComment([FromBody] AssignmentComment assignmentComment)
        {
            OperationDetails op = new OperationDetails();
            bool result = _assignmentService.DeleteAssignmentComment(assignmentComment);
            if (result == true)
            {
                op.Success = true;
            }
            else
            {
                op.Success = false;
            }
            return Ok(op);
        }



        /// <summary>
        /// Description - To get all student assignment ids.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentStudent")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentStudent()
        {
            var assignmentList = await _assignmentService.GetAssignmentStudent();
            return Ok(assignmentList);
        }
        //GetAssignmentStudentDetails
        /// <summary>
        /// Description - To get all assignment details
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAssignmentStudentDetails")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentStudentDetails(int TeacherId, AssignmentFilter loadAssignment)
        {
            //string teacherIds = string.Join(",", teacherVal);
            var assignmentList = await _assignmentService.GetAssignmentStudentDetails(TeacherId, loadAssignment);
            return Ok(assignmentList);
        }

        //GetAdminAssignmentStudentDetails
        /// <summary>
        /// Description - To get all admin assignment details
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAdminAssignmentStudentDetails")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAdminAssignmentStudentDetails(AssignmentFilter loadAssignment)
        {
            var assignmentList = await _assignmentService.GetAdminAssignmentStudentDetails(loadAssignment);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Description - To get all assignment details
        /// </summary>
        /// <returns></returns>
        /// <param name="filterVal"></param>
        [HttpPost]
        [Route("GetFilterSchoolGroupsById")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetFilterSchoolGroupsById(int TeacherId, AssignmentFilter loadAssignment)
        {
            //string groupIds = string.Join(",", filterVal);
            var assignmentList = await _assignmentService.GetFilterSchoolGroupsById(TeacherId, loadAssignment);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Description - To get all assignment details
        /// </summary>
        /// <returns></returns>
        /// <param name="filterVal"></param>
        [HttpPost]
        [Route("GetArchivedFilterSchoolGroupsById")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetArchivedFilterSchoolGroupsById(int TeacherId, AssignmentFilter loadAssignment)
        {
            //string groupIds = string.Join(",", filterVal);
            var assignmentList = await _assignmentService.GetArchivedFilterSchoolGroupsById(TeacherId, loadAssignment);
            return Ok(assignmentList);
        }

        //GetAssignmentStudentDetailsById
        /// <summary>
        /// Description - To get all assignment details by assignment id.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentStudentDetailsById/{Id:int}")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentStudentDetailsById(int Id)
        {
            var assignmentList = await _assignmentService.GetAssignmentStudentDetailsById(Id);
            return Ok(assignmentList);
        }
        /// <summary>
        /// Description - To get all assignment tasks by assignment id.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTasksByAssignmentId/{assignmentId:int}")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTasksByAssignmentId(int assignmentId)
        {
            var assignmentList = await _assignmentService.GetAssignmentTasksByAssignmentId(assignmentId);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Description - To get all assignment tasks by assignment id.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTaskQuizDetailByTaskId")]
        [ProducesResponseType(typeof(AssignmentStudentDetails), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTaskQuizDetailById(int taskId)
        {
            var assignmentList = await _assignmentService.GetTaskQuizDetailById(taskId);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 09 july 2019
        /// Description - To get all Assignments belongs to student.
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="assignmentType"></param>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="SearchString"></param>
        /// <param name="sortBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAssignmentByStudentId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string SearchString, string assignmentType, string sortBy)
        {
            var assignmentList = await _assignmentService.GetAssignmentByStudentId(PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy);
            return Ok(assignmentList);
        }


        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 29 Sep 2020
        /// Description - To get all Archive Assignments belongs to student.
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="assignmentType"></param>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="SearchString"></param>
        /// <param name="sortBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetArchivedAssignmentByStudentId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetArchivedAssignmentByStudentId(int PageNumber, int PageSize, long studentId, string SearchString, string assignmentType, string sortBy)
        {
            var assignmentList = await _assignmentService.GetArchivedAssignmentByStudentId(PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy);
            return Ok(assignmentList);
        }

        [HttpPost]
        [Route("GeStudentFilterSchoolGroupsById")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GeStudentFilterSchoolGroupsById(int[] filterVal, int PageNumber, int PageSize, long studentId, string SearchString, string assignmentType, string sortBy)
        {
            string groupIds = string.Join(",", filterVal);
            var assignmentList = await _assignmentService.GeStudentFilterSchoolGroupsById(PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy, groupIds);
            return Ok(assignmentList);
        }


        [HttpPost]
        [Route("GetArchivedStudentFilterSchoolGroupsById")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetArchivedStudentFilterSchoolGroupsById(int[] filterVal, int PageNumber, int PageSize, long studentId, string SearchString, string assignmentType, string sortBy)
        {
            string groupIds = string.Join(",", filterVal);
            var assignmentList = await _assignmentService.GetArchivedStudentFilterSchoolGroupsById(PageNumber, PageSize, studentId, SearchString, assignmentType, sortBy, groupIds);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - Rahul Verma
        /// Created Date - 27th July 2020
        /// Description - To get all Assignments belongs to student without pagination
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="assignmentType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentByStudentIdWithoutPagination")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentByStudentIdWithoutPagination(long studentId, string assignmentType)
        {
            var assignmentList = await _assignmentService.GetAssignmentByStudentIdWithoutPagination(studentId, assignmentType);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 12 Feb 2020
        /// Description - To get all Assignments belongs to student and order by assignment title with pagination.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentAssignmentByUserId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAssignmentByUserId(int pageNumber, int pageSize, long userId, string searchString)
        {
            var assignmentList = await _assignmentService.GetAssignmentByUserId(pageNumber, pageSize, userId, searchString);
            return Ok(assignmentList);
        }


        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 29 JAN 2020
        /// Description - To get all Assignments belongs to student with studentId with DateRange.
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAssignmentByStudentIdWithDateRange")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentByStudentIdWithDateRange(int PageNumber, int PageSize, long studentId, DateTime startDate, DateTime endDate, string SearchString, string assignmentType)
        {
            var assignmentList = await _assignmentService.GetAssignmentByStudentIdWithDateRange(PageNumber, PageSize, studentId, startDate, endDate, SearchString, assignmentType);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 18 FEB 2020
        /// Description - To get all Assignments belongs to student with studentId with DateRange.
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="teacherId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAssignmentByStudentIdAndTeacherIdWithDateRange")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentByStudentIdAndTeacherIdWithDateRange(int PageNumber, int PageSize, long studentId, long teacherId, DateTime startDate, DateTime endDate, string SearchString, string assignmentType)
        {
            var assignmentList = await _assignmentService.GetAssignmentByStudentIdAndTeacherIdWithDateRange(PageNumber, PageSize, studentId, teacherId, startDate, endDate, SearchString, assignmentType);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - Vinayak Yenpure
        /// Created Date - 17 DEC 2020
        /// Description - To Get Assignment TaskQuiz By AssignmentId
        /// </summary>
        /// <param name="assignmentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentTaskQuizByAssignmentId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentReport>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentTaskQuizByAssignmentId(int assignmentId)
        {
            var assignmentList = await _assignmentService.GetAssignmentTaskQuizByAssignmentId(assignmentId);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 22 JULY 2020
        /// Description - To get all Assignments belongs to student with studentId with DateRange for MyPlanner.
        /// </summary>
        /// <param name="studentId">studentId</param>
        /// <param name="startDate">startDate</param>
        /// <param name="endDate">endDate</param>
        /// <param name="assignmentType">assignmentType</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAssignmentsForMyPlanner")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentsForMyPlanner(long studentId, DateTime startDate, DateTime endDate, string assignmentType = "")
        {
            var assignmentList = await _assignmentService.GetAssignmentsForMyPlanner(studentId, startDate, endDate, assignmentType);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 09 July 2019
        /// Description - To assign grade to student's assignment.
        /// </summary>
        /// <param name="studentAssignmentId"></param>
        ///  <param name="gradingTemplateItemId"></param>
        ///  <param name="GradedBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("AssignGradeToStudentAssignment")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AssignGradeToStudentAssignment(int studentAssignmentId, int gradingTemplateItemId, int GradedBy)
        {
            var result = _assignmentService.AssignGradeToStudentAssignment(studentAssignmentId, gradingTemplateItemId, GradedBy);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 19 Oct 2020
        /// Description - To get Permission Access for same Group to display Assignments.
        /// </summary>
        /// <param name="assignmentId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentPermissions")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetAssignmentPermissions(int assignmentId, int userId)
        {
            OperationDetails op = new OperationDetails();
            var result = _assignmentService.GetAssignmentPermissions(assignmentId, userId);
            if (result == true)
            {
                op.Success = true;
            }
            else
            {
                op.Success = false;
            }
            return Ok(op);
        }


        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 21 Oct 2020
        /// Description - To UnArchive Assignments by assignmentId.
        /// </summary>
        /// <param name="assignmentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("AssignmentUnArchive")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AssignmentUnArchive(int assignmentId)
        {
            OperationDetails op = new OperationDetails();
            var result = _assignmentService.AssignmentUnArchive(assignmentId);
            if (result == true)
            {
                op.Success = true;
            }
            else
            {
                op.Success = false;
            }
            return Ok(op);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 29 Nov 2020
        /// Description - To Add/update Assignment Category by Admin
        /// </summary>
        /// <param name="AssignmentCategoryId"></param>
        /// <param name="IsActive"></param>
        /// <param name="SchoolId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("AddEditAssignmentCategory")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddEditAssignmentCategory(int AssignmentCategoryId, bool IsActive, long SchoolId)
        {
            OperationDetails op = new OperationDetails();
            var result = _assignmentService.AddEditAssignmentCategory(AssignmentCategoryId, IsActive, SchoolId);
            if (result == true)
            {
                op.Success = true;
            }
            else
            {
                op.Success = false;
            }
            return Ok(op);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 29 Nov 2020
        /// Description - To Add Assignment Category Master
        /// </summary>
        /// <param name="AssignmentCategoryTitle"></param>
        /// <param name="AssignmentCategoryDesc"></param>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("AddAssignmentCategoryMaster")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddAssignmentCategoryMaster(string AssignmentCategoryTitle, string AssignmentCategoryDesc, int CategoryId, bool IsActive, long SchoolId)
        {
            OperationDetails op = new OperationDetails();
            var result = _assignmentService.AddAssignmentCategoryMaster(AssignmentCategoryTitle, AssignmentCategoryDesc, CategoryId, IsActive, SchoolId);
            if (result == true)
            {
                op.Success = true;
            }
            else
            {
                op.Success = false;
            }
            return Ok(op);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 02 Nov 2020
        /// Description - To update seen Assignment Comments by studentAssignmentId.
        /// </summary>
        /// <param name="objAssignmentComment"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SeenStudentAssignmentComment")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> SeenStudentAssignmentComment(AssignmentComment objAssignmentComment)
        {
            OperationDetails op = new OperationDetails();
            bool result = _assignmentService.SeenStudentAssignmentComment(objAssignmentComment);
            if (result == true)
            {
                op.Success = true;
            }
            else
            {
                op.Success = false;
            }
            return Ok(op);
        }


        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 11 July 2019
        /// Description - To get files uploaded with assignment
        /// </summary>
        /// <param name="assignmentId"></param>        
        /// <returns>assignmentFileList</returns>
        [HttpGet]
        [Route("GetAssignmentFilesByAssignmentId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentFilesByAssignmentId(int assignmentId)
        {
            var assignmentFileList = await _assignmentService.GetAssignmentFilesByAssignmentId(assignmentId);
            return Ok(assignmentFileList);
        }

        /// <summary>
        /// Created By - Rahul Verma
        /// Created Date - 29th July 2020
        /// Description - To get Saved Assignments by id
        /// </summary>
        /// <param name="assignmentId"></param> 
        /// <param name="userId"></param> 
        /// <returns>assignmentFileList</returns>
        [HttpGet]
        [Route("GetSavedAssignmentFilesByAssignmentId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSavedAssignmentFilesByAssignmentId(int assignmentId, long userId)
        {
            var assignmentFileList = await _assignmentService.GetSavedAssignmentFilesByAssignmentId(assignmentId, userId);
            return Ok(assignmentFileList);
        }

        /// <summary>
        /// Created By - Rahul Verma
        /// Created Date - 29th July 2020
        /// Description - To get Saved Assignments by id
        /// </summary>
        /// <param name="lstModel"></param>        
        /// <returns>assignmentFileList</returns>
        [HttpPost]
        [Route("SaveAssignmentFiles")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> SaveAssignmentFiles(List<AssignmentFile> lstModel)
        {
            var result = _assignmentService.SaveAssignmentFiles(lstModel);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 11 July 2019
        /// Description - To get files uploaded with task
        /// </summary>
        /// <param name="taskId"></param>        
        /// <returns>taskFilesList</returns>
        [HttpGet]
        [Route("GetTaskFilesByTaskId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTaskFilesByTaskId(int taskId)
        {
            var taskFilesList = await _assignmentService.GetFilesbyTaskId(taskId);
            return Ok(taskFilesList);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 15 July 2019
        /// Description - To get  homework details done by student for perticular assignment
        /// </summary>
        /// <param name="assignmentStudentId"></param>  
        /// <param name="IsSeenByStudent"></param>
        /// <returns>assignmentFileList</returns>
        [HttpGet]
        [Route("GetStudentAsgHomeworkDetailsById")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAsgHomeworkDetailsById(int assignmentStudentId, int IsSeenByStudent = 0)
        {
            var studentHomeworkdetails = await _assignmentService.GetStudentAsgHomeworkDetailsById(assignmentStudentId, IsSeenByStudent);
            return Ok(studentHomeworkdetails);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 03 MAY 2020
        /// Description - To get  homework details of student for assignment
        /// </summary>
        /// <param name="assignmentStudentId"></param>  
        /// <returns>assignmentFileList</returns>
        [HttpGet]
        [Route("GetStudentAssignmentDetailsById")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAssignmentDetailsById(int assignmentStudentId)
        {
            var studentHomeworkdetails = await _assignmentService.GetStudentAssignmentDetailsById(assignmentStudentId);
            return Ok(studentHomeworkdetails);
        }


        /// <summary>
        /// Description - To get particular task in an assignment
        /// </summary>
        /// <param name="taskId"></param>        
        /// <returns>schoolGroupList</returns>
        [HttpGet]
        [Route("GetAssignmentTaskbyTaskId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentTask>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentTaskbyTaskId(int taskId)
        {
            var assignmentTask = await _assignmentService.GetAssignmentTaskbyTaskId(taskId);
            return Ok(assignmentTask);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 11 July 2019
        /// Description - To get school groups associated with Assignment
        /// </summary>
        /// <param name="assignmentId"></param>        
        /// <returns>schoolGroupList</returns>
        [HttpGet]
        [Route("GetSelectegGroupsByAssignmentId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSelectegGroupsByAssignmentId(int assignmentId)
        {
            var schoolGroupList = await _assignmentService.GetSelectegGroupsByAssignmentId(assignmentId);
            return Ok(schoolGroupList);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 12 July 2020
        /// Description - To get school courses associated with Assignment
        /// </summary>
        /// <param name="assignmentId"></param>        
        /// <returns>schoolGroupList</returns>
        [HttpGet]
        [Route("GetSelectegCoursesByAssignmentId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSelectedCoursesByAssignmentId(int assignmentId)
        {
            var schoolGroupList = await _assignmentService.GetSelectedCoursesByAssignmentId(assignmentId);
            return Ok(schoolGroupList);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 18 July 2019
        /// Description - To get school groups associated with Assignment
        /// </summary>
        /// <param name="assignmentId"></param>        
        /// <returns>schoolGroupList</returns>
        [HttpGet]
        [Route("GetStudentsByAssignmentId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentsByAssignmentId(int assignmentId, int GroupId)
        {
            var studentList = await _assignmentService.GetStudentsByAssignmentId(assignmentId, GroupId);
            return Ok(studentList);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 26 Oct 2020
        /// Description - To get Students who have completed their Assignment
        /// </summary>
        /// <param name="assignmentId"></param>        
        /// <returns>schoolGroupList</returns>
        [HttpGet]
        [Route("GetAssignmentCompletedStudentsByAssignmentId")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentCompletedStudentsByAssignmentId(int assignmentId)
        {
            var studentList = await _assignmentService.GetAssignmentCompletedStudentsByAssignmentId(assignmentId);
            return Ok(studentList);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 02 Sep 2020
        /// Description - To get school details associated with Assignment for export data to excel
        /// </summary>
        /// <param name="assignmentId"></param>        
        /// <returns>schoolGroupList</returns>
        [HttpGet]
        [Route("GetExcelReportDataStudentsByAssignmentId")]
        [ProducesResponseType(typeof(AssignmentReport), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetExcelReportDataStudentsByAssignmentId(int assignmentId)
        {
            var studentList = await _assignmentService.GetExcelReportDataStudentsByAssignmentId(assignmentId);
            return Ok(studentList);
        }

        /// <summary>
        /// Created By - Vishal Dhabade
        /// Created Date - 20 July 2019
        /// Description - mark as complete for teacher and student
        /// </summary>
        /// <param name="assignmentStudentId"></param>
        /// <param name="isTeacher"></param>
        /// <param name="CompletedBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("MarkAsComplete")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> MarkAsComplete(int assignmentStudentId, bool isTeacher, int CompletedBy)
        {
            var result = _assignmentService.MarkAsComplete(assignmentStudentId, isTeacher, CompletedBy);
            return Ok(result);
        }


        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 20 Dec 2020
        /// Description - Submit marks of assignment by teacher.
        /// </summary>
        /// <param name="assignmentStudentId"></param>
        /// <param name="submitMarks"></param>
        /// <param name="CompletedBy"></param>
        /// <param name="gradingTemplateId"></param>
        /// <param name="SystemLanguageId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("SubmitAssignmentMarks")]
        [ProducesResponseType(typeof(GradingTemplateItem), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult> SubmitAssignmentMarks(int assignmentStudentId, decimal submitMarks, int CompletedBy, int gradingTemplateId, int SystemLanguageId)
        {
            var result = await _assignmentService.SubmitAssignmentMarks(assignmentStudentId, submitMarks, CompletedBy, gradingTemplateId, SystemLanguageId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 21 May 2019
        /// Description - To insert student files uploaded for Assignment Completion 
        /// </summary>
        /// <param name="lstStudentAssignmentFile"></param>
        ///  <param name="studentId"></param>
        ///  <param name="assignmentId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadStudentAssignmentFiles")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UploadStudentAssignmentFiles(List<StudentAssignmentFile> lstStudentAssignmentFile, int studentId, int assignmentId, string IpDetails)
        {
            var result = _assignmentService.UploadStudentAssignmeentFiles(lstStudentAssignmentFile, studentId, assignmentId, IpDetails);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 21 July 2019
        /// Description - To get files uploaded by Student for assignment work.
        /// </summary>
        /// <param name="assignmentId"></param>    
        /// <param name="studentId"></param>    
        /// <returns>assignmentFileList</returns>
        [HttpGet]
        [Route("GetStudentAssignmentFiles")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudent>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAssignmentFiles(int studentId, int assignmentId)
        {
            var assignmentFileList = await _assignmentService.GetStudentAssignmentFiles(studentId, assignmentId);
            return Ok(assignmentFileList);
        }

        /// <summary>
        /// Created By - Mukund Patil
        /// Created Date - 14 MAY 2020
        /// Description - To get assignment feedbackfiles
        /// </summary>
        /// <param name="assignmentStudentId"></param>   
        /// <param name="assignmentCommentId"></param>   
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentFeedbackFiles")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentFeedbackFiles>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentFeedbackFiles(int assignmentStudentId, int assignmentCommentId)
        {
            var assignmentFeedbackFiles = await _assignmentService.GetAssignmentFeedbackFiles(assignmentStudentId, assignmentCommentId);
            return Ok(assignmentFeedbackFiles);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 24 July 2019
        /// Description - mark as complete for teacher and student
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="assignmentId"></param>
        /// <param name="isReSubmitAssignnment"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("MarkAsIncomplete")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> MarkAsIncomplete(int studentId, int assignmentId, bool isReSubmitAssignnment)
        {
            var result = _assignmentService.MarkAsIncomplete(studentId, assignmentId, isReSubmitAssignnment);
            return Ok(result);
        }

        //added for Student Upload File Task
        /// <summary>
        /// Description - To upload student task files.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadStudentTaskFiles")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UploadStudentTaskFiles(List<StudentTaskFile> lstofTaskFiles, int studentId, int taskId, string IpDetails)
        {
            var result = _assignmentService.UploadStudentTaskFiles(lstofTaskFiles, studentId, taskId, IpDetails);
            return Ok(result);
        }
        /// <summary>
        /// Description - To get student task files.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentTaskFiles")]
        [ProducesResponseType(typeof(IEnumerable<StudentTaskFile>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentTaskFiles(int studentId, int taskId)
        {
            var assignmentFileList = await _assignmentService.GetStudentTaskFiles(studentId, taskId);
            return Ok(assignmentFileList);
        }

        /// <summary>
        /// Created By - Vinayak Y
        /// Created Date - 13 August 2019
        /// Description - To get student task details by task id and student id.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentTaskdetails")]
        [ProducesResponseType(typeof(IEnumerable<StudentTask>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentTAskdetails(int taskId, int studentId)
        {
            var studentTaskdetails = await _assignmentService.GetStudentTaskDetails(taskId, studentId);
            return Ok(studentTaskdetails);
        }

        /// <summary>
        /// Created By - Vinayak Y
        /// Created Date - 13 August 2019
        /// Description - To get student task details by task id and student id and StudentTaskId.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentTaskDetailById")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentTask>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentTaskDetailById(int taskId, int studentId, int assignmentStudentId, long studentTaskId)
        {
            var studentTaskdetails = await _assignmentService.GetStudentTaskDetailById(taskId, studentId, assignmentStudentId, studentTaskId);
            return Ok(studentTaskdetails);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 13 August 2019
        /// Description - To assign grade to student's objectives.
        /// </summary>
        /// <param name="studentObjectiveId"></param>
        ///  <param name="gradingTemplateItemId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("AssignGradeToStudentObjective")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AssignGradeToStudentObjective(int studentObjectiveId, int gradingTemplateItemId)
        {
            var result = _assignmentService.AssignGradeToStudentObjective(studentObjectiveId, gradingTemplateItemId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 13 August 2019
        /// Description - To assign grade to student's assignment.
        /// </summary>
        /// <param name="studentTaskId"></param>
        ///  <param name="gradingTemplateItemId"></param>
        ///  <param name="GradedBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("AssignGradeToStudentTask")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AssignGradeToStudentTask(int studentTaskId, int gradingTemplateItemId, int GradedBy)
        {
            var result = _assignmentService.AssignGradeToStudentTask(studentTaskId, gradingTemplateItemId, GradedBy);
            return Ok(result);
        }


        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 12 Aug 2019
        /// Description - To insert assignment comments
        /// </summary>
        /// <param name="assignmentComment"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertAssignmentComment")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertAssignmentComment([FromBody] AssignmentComment assignmentComment)
        {
            var result = _assignmentService.InsertAssignmentComment(assignmentComment);
            return Ok(result);
        }
        /// <summary>
        /// Description - To get comments related to the assignments.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentComments")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentComment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentComments(int assignmentStudentId)
        {
            var assignmentComments = await _assignmentService.GetAssignmentComments(assignmentStudentId);
            return Ok(assignmentComments);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 12 Aug 2019
        /// Description - mark as complete for task
        /// </summary>
        /// <param name="studentTaskId"></param>   
        /// <param name="isByTeacher"></param> 
        /// <param name="CompletedBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("MarkAsCompleteTask")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> MarkAsCompleteTask(int studentTaskId, bool isByTeacher, int CompletedBy)
        {
            var result = _assignmentService.MarkAsCompleteTask(studentTaskId, isByTeacher, CompletedBy);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 21 Dec 2020
        /// Description - Submit Task marks by teacher
        /// </summary>
        /// <param name="studentTaskId"></param>   
        /// <param name="StudentAssignmentId"></param>   
        /// <param name="submitMarks"></param> 
        /// <param name="CompletedBy"></param>
        /// <param name="SystemLanguageId"></param>
        /// <param name="gradingTemplateId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("SubmitTaskMarks")]
        [ProducesResponseType(typeof(GradingTemplateItem), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult> SubmitTaskMarks(int studentTaskId, int StudentAssignmentId, decimal submitMarks, int CompletedBy, int gradingTemplateId, int languageId)
        {
            var result = await _assignmentService.SubmitTaskMarks(studentTaskId, StudentAssignmentId, submitMarks, CompletedBy, gradingTemplateId, languageId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 21 Aug 2019
        /// Description - to get file tree structure from My Files
        /// </summary>
        /// <param name="userId"></param>          
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyFiles")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentComment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMyFilesStructure(long userId)
        {
            var myFiles = await _assignmentService.GetMyFilesStructure(userId);
            return Ok(myFiles);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 21 Aug 2019
        /// Description - to get file as Assignment File from My File
        /// </summary>
        /// <param name="fileId"></param>          
        /// <param name="isFolder"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentMyFile")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentFile>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentMyFile(long fileId, bool isFolder)
        {
            var myFiles = await _assignmentService.GetAssignmentMyFile(fileId, isFolder);
            return Ok(myFiles);
        }
        /// <summary>
        /// Description - To delete uploaded files.
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Route("DeleteUploadedFiles")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteUploadedFiles(int FileId, int AssignmentFileId, long UserId)
        {
            var result = _assignmentService.DeleteUploadedFiles(FileId, AssignmentFileId, UserId);
            return Ok(result);
        }

        /// <summary>
        /// Description - To delete student uploaded files.
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Route("DeleteStudentAssignmentFile")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteStudentAssignmentFile(int FileId, long UserId, bool IsTaskFile)
        {
            var result = _assignmentService.DeleteStudentAssignmentFile(FileId, UserId, IsTaskFile);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Girish S
        /// Created Date - 24 feb 2019
        /// Description - To save feedback as per task
        /// </summary>
        /// <param name="assignmentTask">assignmentTask</param>
        [HttpPost]
        [Route("InsertTaskFeedback")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertTaskFeedback([FromBody] AssignmentTask assignmentTask)
        {
            var result = _assignmentService.InsertTaskFeedback(assignmentTask);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Girish S
        /// Created Date - 24 feb 2019
        /// Description - To save feedback as per task
        /// </summary>
        /// <param name="studentObjective">assignmentTask</param>
        [HttpPost]
        [Route("InsertStudentObjectiveFeedback")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertStudentObjectiveFeedback([FromBody] AssignmentStudentObjective studentObjective)
        {
            var result = _assignmentService.InsertStudentObjectiveFeedback(studentObjective);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 04 Nov 2019
        /// Description - to get file record by assignmentfileid
        /// </summary>
        /// <param name="id"></param>          
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentFilebyAssignmentFileId")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentFilebyAssignmentFileId(long id)
        {
            var myFiles = await _assignmentService.GetAssignmentFilebyAssignmentFileId(id);
            return Ok(myFiles);
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 22 Dec 2019
        /// Description - to get file record by TaskFileId
        /// </summary>
        /// <param name="id"></param>          
        /// <returns></returns>
        [HttpGet]
        [Route("GetTaskFile")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTaskFile(long id)
        {
            var myFiles = await _assignmentService.GetTaskFile(id);
            return Ok(myFiles);
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 22 Dec 2019
        /// Description - to get file record by StudentTaskFileId
        /// </summary>
        /// <param name="id"></param>          
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentTaskFile")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentTaskFile(long id)
        {
            var myFiles = await _assignmentService.GetStudentTaskFile(id);
            return Ok(myFiles);
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 05 Nov 2019
        /// Description - to get file record by assignmentfileid
        /// </summary>
        /// <param name="id"></param>          
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentAssignmentFilebyStudentAsgFileId")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAssignmentFilebyStudentAsgFileId(long id)
        {
            var myFiles = await _assignmentService.GetStudentAssignmentFilebyStudentAsgFileId(id);
            return Ok(myFiles);
        }

        /// <summary>
        /// Created By - sonali shinde
        /// Created Date - 31 Dec 2019
        /// Description - To update active assignment.
        /// </summary>
        /// <param name="assignmentId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateActiveAssignment")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateActiveAssignment(long assignmentId)
        {
            var result = _assignmentService.UpdateActiveAssignment(assignmentId);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 07 Jan 2020
        /// Description - To get all Assignments belongs to group.
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchString"></param>
        /// <param name="userId"></param>
        /// <param name="schoolId"></param>
        /// <param name="schoolGroupId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentListForReport")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentListForReport(int pageNumber, int pageSize, string searchString, long userId, long schoolId, int schoolGroupId, DateTime startDate, DateTime endDate, string userType)
        {
            var assignmentList = await _assignmentService.GetAssignmentListForReport(pageNumber, pageSize, searchString, userId, schoolId, schoolGroupId, startDate, endDate, userType);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - sonali shinde
        /// Created Date - 31 Dec 2019
        /// Description - To get all archived assignment details
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetArchivedAssignmentStudentDetails")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetArchivedAssignmentStudentDetails(int TeacherId, AssignmentFilter loadAssignment)
        {
            var assignmentList = await _assignmentService.GetArchivedAssignmentStudentDetails(TeacherId, loadAssignment);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - Mohd Ameeq
        /// Created Date - 22 oct 2020
        /// Description - To get all admin archived assignment details
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAdminArchivedAssignmentStudentDetails")]
        [ProducesResponseType(typeof(IEnumerable<AssignmentStudentDetails>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAdminArchivedAssignmentStudentDetails(AssignmentFilter loadAssignment)
        {
            var assignmentList = await _assignmentService.GetAdminArchivedAssignmentStudentDetails(loadAssignment);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 7th Jan 2020
        /// Description - To add update peer mapping.
        /// </summary>
        /// <param name="lstPeerReviewMapping"></param>       
        /// <returns></returns>
        [HttpPost]
        [Route("AddUpdatePeermarking")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddUpdatePeermarking([FromBody] List<AssignmentPeerReview> lstPeerReviewMapping)
        {
            var result = _assignmentService.AddUpdatePeerReviewMapping(lstPeerReviewMapping);
            return Ok(result);

        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 9th Jan 2020
        /// Description - To get peer rev
        /// </summary>
        /// <param name="assignmentId"></param>       
        /// <param name="userId"></param>       
        /// <returns></returns>
        [HttpGet]
        [Route("GetPeerAssignmentDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetPeerAssignmentDetails(long assignmentId, long userId)
        {
            var assignmentPeerList = await _assignmentService.GetPeerAssignmentDetails(assignmentId, userId);
            return Ok(assignmentPeerList);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 24th Jan 2020
        /// Description - To get peer review
        /// </summary>
        /// <param name="assignmentId"></param>       

        /// <returns></returns>
        [HttpGet]
        [Route("GetPeerMappingDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetPeerMappingDetails(long assignmentId)
        {
            var assignmentPeerList = await _assignmentService.GetPeerMappingDetails(assignmentId);
            return Ok(assignmentPeerList);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 7th Jan 2020
        /// Description - To add update peer mapping.
        /// </summary>
        /// <param name="lstDocumentReview"></param>   
        /// <param name="userId"></param>   
        /// <returns>result</returns>
        [HttpPost]
        [Route("AddUpdateDocumentReviewDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddUpdateDocumentReviewDetails([FromBody] List<DocumentReviewdetails> lstDocumentReview, long userId)
        {
            OperationDetails op = new OperationDetails();
            int result = _assignmentService.AddUpdateDocumentReviewDetails(lstDocumentReview, userId);
            if (result > 0)
            {
                op.Success = true;
                op.InsertedRowId = result;
            }
            else
            {
                op.Success = false;
                op.InsertedRowId = 0;
            }
            return Ok(op);

            //var result = _assignmentService.AddUpdatePeerReviewMapping(lstPeerReviewMapping);
            //return Ok(result);

        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 13th Jan 2020
        /// Description - To get peer reviewed files
        /// </summary>       
        /// <param name="peerReviewId"></param>       
        /// <returns></returns>
        [HttpGet]
        [Route("GetReviewedDocumentFiles")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetReviewedDocumentFiles(long peerReviewId)
        {
            var reviewedFileList = await _assignmentService.GetReviewedDocumentFiles(peerReviewId);
            return Ok(reviewedFileList);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 14th FEB 2020
        /// Description - To single file to review with Student assignment file ID
        /// </summary>       
        /// <param name="stdAsgFileId"></param>       
        /// <returns>reviewedFile</returns>
        [HttpGet]
        [Route("GetDocumentFileByStudAsgFileId")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetDocumentFileByStudAsgFileId(long stdAsgFileId)
        {
            var reviewedFile = await _assignmentService.GetDocumentFileByStudAsgFileId(stdAsgFileId);
            return Ok(reviewedFile);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 17 th FEb 2020
        /// Description - To get different assignment status count
        /// </summary>       
        /// <param name="id"></param>   
        /// <param name="isTeacher"></param>
        /// <param name="searchText"></param>
        /// <returns>assignmentcountDetails</returns>
        [HttpGet]
        [Route("GetAssignmentStatusCount")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetAssignmentStatusCounts(long id, bool isTeacher, string searchText)
        {
            var assignmentcountDetails = await _assignmentService.GetAssignmentStatusCounts(id, isTeacher, searchText);
            return Ok(assignmentcountDetails);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 19 FEB 2020
        /// Description - mark as complete for teacher and student
        /// </summary>
        /// <param name="peerReviewId"></param>
        /// <param name="reviewedBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("MarkAsCompleteReview")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> MarkAsCompleteReview(long peerReviewId, long reviewedBy)
        {
            var result = _assignmentService.MarkAsCompleteReview(peerReviewId, reviewedBy);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 9th Jan 2020
        /// Description - To get peer rev
        /// </summary>
        /// <param name="peerReviewId"></param>       

        /// <returns></returns>
        [HttpGet]
        [Route("GetPeerAssignmentDetail")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetPeerAssignmentDetail(long peerReviewId)
        {
            var assignmentPeerList = await _assignmentService.GetPeerAssignmentDetail(peerReviewId);
            return Ok(assignmentPeerList);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 9th Jan 2020
        /// Description - To get peer Details by Assignment student id
        /// </summary>
        /// <param name="assinmentStudentId"></param>  
        /// <returns></returns>
        [HttpGet]
        [Route("GetPeerAssignmentDetailByAssignmentStudentId")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetPeerAssignmentDetailByAssignmentStudentId(long assinmentStudentId)
        {
            var assignmentPeerList = await _assignmentService.GetPeerAssignmentDetailByAssignmentStudentId(assinmentStudentId);
            return Ok(assignmentPeerList);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 24th Feb 2020
        /// Description - To add update peer mapping.
        /// </summary>
        /// <param name="lstTaskDocumentReview"></param>       
        /// <param name="userId"></param>    
        /// <returns>result</returns>
        [HttpPost]
        [Route("AddUpdateTaskDocumentReviewDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AddUpdateTaskDocumentReviewDetails([FromBody] List<TaskDocumentReviewdetails> lstTaskDocumentReview, long userId)
        {
            OperationDetails op = new OperationDetails();
            int result = _assignmentService.AddUpdateTaskDocumentReviewDetails(lstTaskDocumentReview, userId);
            if (result > 0)
            {
                op.Success = true;
                op.InsertedRowId = result;
            }
            else
            {
                op.Success = false;
                op.InsertedRowId = 0;
            }
            return Ok(op);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 9th Jan 2020
        /// Description - To get peer reviwed task details
        /// </summary>
        /// <param name="studentId"></param>       
        /// <param name="taskId"></param>    
        /// <returns>peerTaskdetail</returns>
        [HttpGet]
        [Route("GetPeerTaskDetail")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetPeerTaskDetail(long studentId, long taskId)
        {
            var peerTaskdetail = await _assignmentService.GetPeerTaskDetail(studentId, taskId);
            return Ok(peerTaskdetail);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 9th Jan 2020
        /// Description - To get peer reviwed tasks detail
        /// </summary>
        /// <param name="assignmentId"></param>  
        ///  <param name="studentId"></param> 
        /// <returns>peerTaskdetail</returns>
        [HttpGet]
        [Route("GetPeerTaskDetails")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetPeerTaskDetails(long assignmentId, long studentId)
        {
            var peerTaskdetail = await _assignmentService.GetPeerTaskDetails(assignmentId, studentId);
            return Ok(peerTaskdetail);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 14th FEB 2020
        /// Description - To single file to review with Student task file ID
        /// </summary>       
        /// <param name="stdTaskFileId"></param>       
        /// <returns>reviewedFile</returns>
        [HttpGet]
        [Route("GetTaskDocumentFileByStudTaskFileId")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetTaskDocumentFileByStudTaskFileId(long stdTaskFileId)
        {
            var reviewedFile = await _assignmentService.GetTaskDocumentFileByStudTaskFileId(stdTaskFileId);
            return Ok(reviewedFile);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 14th FEB 2020
        /// Description - To single file to review with Student task file ID
        /// </summary>       
        /// <param name="taskPeerReviewId"></param>       
        /// <returns>reviewedTaskFiles</returns>
        [HttpGet]
        [Route("GetTaskReviewedDocumentFiles")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> GetTaskReviewedDocumentFiles(long taskPeerReviewId)
        {
            var reviewedTaskFiles = await _assignmentService.GetTaskReviewedDocumentFiles(taskPeerReviewId);
            return Ok(reviewedTaskFiles);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 19 FEB 2020
        /// Description - mark as complete task review for teacher and student
        /// </summary>
        /// <param name="peerReviewId"></param>
        /// <param name="reviewedBy"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("MarkAsCompleteTaskReview")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> MarkAsCompleteTaskReview(long peerReviewId, long reviewedBy)
        {
            var result = _assignmentService.MarkAsCompleteTaskReview(peerReviewId, reviewedBy);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 12 May 2019
        /// Description - To get  Assignments by teacher Id.
        /// </summary>
        /// <param name="SchoolGroupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGroupAssignments")]
        [ProducesResponseType(typeof(IEnumerable<Assignment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGroupAssignments(long SchoolGroupId)
        {
            var assignmentList = await _assignmentService.GetGroupAssignments(SchoolGroupId);
            return Ok(assignmentList);
        }
        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 12 May 2019
        /// Description - To get  Assignments by teacher Id.
        /// </summary>
        /// <param name="SchoolGroupId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentGroupAssignments")]
        [ProducesResponseType(typeof(IEnumerable<Assignment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentGroupAssignments(long SchoolGroupId, long UserId)
        {
            var assignmentList = await _assignmentService.GetStudentGroupAssignments(SchoolGroupId, UserId);
            return Ok(assignmentList);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Cretaed Date - 27 May 2020
        /// Description - get students assignments overview
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDashboardAssignmentOverview")]
        [ProducesResponseType(typeof(IEnumerable<Assignment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetDashboardAssignmentOverview(long userId)
        {
            var result = await _assignmentService.GetDashboardAssignmentOverview(userId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Cretaed Date - 27 May 2020
        /// Description - get students assignments overview
        /// </summary>
        /// <param name="assignmentStudentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentAssignmentObjectives")]
        [ProducesResponseType(typeof(IEnumerable<Assignment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAssignmentObjectives(long assignmentStudentId)
        {
            var result = await _assignmentService.GetStudentAssignmentObjectives(assignmentStudentId);

            return Ok(result);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Cretaed Date - 27 May 2020
        /// Description - get students feedback
        /// </summary>
        /// <param name="StudentObjectiveMarkId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentAssignmentObjectiveAudiofeedback")]
        [ProducesResponseType(typeof(IEnumerable<Assignment>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentAssignmentObjectiveAudiofeedback(long StudentObjectiveMarkId)
        {
            var result = await _assignmentService.GetStudentAssignmentObjectiveAudiofeedback(StudentObjectiveMarkId);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 16 July 2020
        /// Description - save cloud files into database
        /// </summary>
        /// <param name="copiedFiles"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadStudentSharedCopyFiles")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UploadStudentSharedCopyFiles(List<AssignmentStudentSharedFiles> copiedFiles)
        {
            var result = await _assignmentService.UploadStudentSharedCopyFiles(copiedFiles);
            return Ok(result);
        }


        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 28 July 2019
        /// Description - To get Quiz Question list by Quiz Id.
        /// </summary>
        /// <param name="taskId">QuizId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuizQuestionsByTaskId")]
        [ProducesResponseType(typeof(IEnumerable<QuizQuestionsView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetQuizQuestionsByTaskId(int taskId)
        {
            var quizQuestion = await _assignmentService.GetQuizQuestionsByTaskId(taskId);
            return Ok(quizQuestion);
        }

        /// <summary>
        /// Created By - vinayak Yenpure
        /// Created Date - 15-12-2020
        /// Description - Get Assignment QuizDetails By TaskId.
        /// </summary>
        /// <param name="taskId">taskId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssignmentQuizDetailsByTaskId")]
        [ProducesResponseType(typeof(GroupQuiz), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAssignmentQuizDetailsByTaskId(int taskId)
        {
            var report = await _assignmentService.GetAssignmentQuizDetailsByTaskId(taskId);
            return Ok(report);
        }

        /// <summary>
        /// Created By - Mahesh Chikhale
        /// Created Date - 19 july 2020
        /// Description - To update assignment.
        /// </summary>
        /// <param name="assignmentId"></param>
        /// <param name="teacherIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("ShareAssignment")]
        [ProducesResponseType(typeof(OperationDetails), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> ShareAssignment(int assignmentId, string teacherIds)
        {
            OperationDetails op = new OperationDetails();
            int result = _assignmentService.ShareAssignment(assignmentId, teacherIds);
            if (result > 0)
            {
                op.Success = true;
                op.InsertedRowId = result;
            }
            else
            {
                op.Success = false;
                op.InsertedRowId = 0;
            }
            return Ok(op);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 11 Jan 2021
        /// Description - retreive archived assignments - option for admins only.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("UndoAssignmentsArchive")]
        [ProducesResponseType(typeof(Boolean), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UndoAssignmentsArchive([FromBody]ArchiveAssignment model)
        {
            bool result = await _assignmentService.UndoAssignmentsArchive(model);
            return Ok(result);
        }
    }
}