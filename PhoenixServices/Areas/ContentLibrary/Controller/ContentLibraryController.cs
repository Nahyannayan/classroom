﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.API.Services;
using Phoenix.Common.Models;
using Phoenix.Models;

namespace Phoenix.API.Areas.ContentLibrary.Controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class ContentLibraryController : ControllerBase
    {
        private readonly IContentLibraryService _contentLibraryService;

        public ContentLibraryController(IContentLibraryService contentLibraryService)
        {
            _contentLibraryService = contentLibraryService;
        }

        /// <summary>
        /// Created By - Vishal Dhabade
        /// Created Date - 12 Dec 2019
        /// Description - Get list of cotent library application for given school
        /// </summary>
        /// <param name="schoolId">School Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getContentLibrary")]
        [ProducesResponseType(typeof(IEnumerable<ContentView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetContentLibrary(long schoolId)
        {
            var contentList = await _contentLibraryService.GetContentLibrary(schoolId);
            return Ok(contentList);
        }

        /// <summary>
        /// Created By - MUKUND PATIL
        /// Created Date - 16 JULY 2020
        /// Description - Get list of School Levels
        /// </summary>
        /// <param name="schoolId">School Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSchoolLevelsBySchoolId")]
        [ProducesResponseType(typeof(IEnumerable<SchoolLevels>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSchoolLevelsBySchoolId(long schoolId)
        {
            var schoolLevels = await _contentLibraryService.GetSchoolLevelsBySchoolId(schoolId);
            return Ok(schoolLevels);
        }

        /// <summary>
        /// Created By - Vinayak Yenpure
        /// Created Date - 09 July 2020
        /// Description - To get paginate content library
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchString"></param>
        /// <param name="SortBy"></param>
        /// <param name="CategoryIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getPaginateContentLibrary")]
        [ProducesResponseType(typeof(IEnumerable<ContentView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPaginateContentLibrary(int schoolId, int pageNumber, int pageSize, string searchString = "", string SortBy = "",string CategoryIds="")
        {
            var schoolGroupList = await _contentLibraryService.GetPaginateContentLibrary(schoolId, pageNumber, pageSize, searchString, SortBy, CategoryIds);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By - Vinayak Yenpure
        /// Created Date - 09 July 2020
        /// Description - To get paginate content library for student
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchString"></param>
        /// <param name="SortBy"></param>
        /// <param name="CategoryIds"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentContentLibrary")]
        [ProducesResponseType(typeof(IEnumerable<ContentView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudentContentLibrary(int schoolId, int pageNumber, int pageSize, string searchString = "", string SortBy="", string CategoryIds = "")
        {
            var schoolGroupList = await _contentLibraryService.GetStudentContentLibrary(schoolId, pageNumber, pageSize, searchString, SortBy, CategoryIds);
            return Ok(schoolGroupList);
        }

        /// <summary>
        /// Created By - Vishal Dhabade
        /// Created Date - 12 Dec 2019
        /// Description - Get list of Content Provider application for given school
        /// </summary>
        /// <param name="schoolId">School Id</param>
        /// <param name="divisionIds">divisionIds</param>
        /// <param name="sortBy">sortBy</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getContentProvider")]
        [ProducesResponseType(typeof(IEnumerable<ContentView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetContentProvider(long schoolId, string divisionIds,string sortBy="")
        {
            var contentList = await _contentLibraryService.GetContentLProvider(schoolId, divisionIds, sortBy);
            return Ok(contentList);
        }
        /// <summary>
        /// Created By - Vishal Dhabade
        /// Created Date - 12 Dec 2019
        /// Description - To Insert Content Resource
        /// </summary>
        /// <param name="contentResource">contentResource</param>
        /// <returns></returns>
        [HttpPost]
        [Route("insertContentResource")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> InsertContentResource([FromBody]Content contentResource)
        {
            var result = _contentLibraryService.InsertContentResource(contentResource);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vishal Dhabade
        /// Created Date - 12 Dec 2019
        /// Description - To Update Content Resource
        /// </summary>
        /// <param name="contentResource">contentResource</param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateContentResource")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateContentResource([FromBody]Content contentResource)
        {
            var result = _contentLibraryService.UpdateContentResource(contentResource);
            return Ok(result);
        }

        /// <summary>
        /// Created By - Vishal Dhabade
        /// Created Date - 12 Dec 2019
        /// Description - To Delete Content Resource
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="userId"
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteContentResource")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> DeleteContentResource(int id, long userId)
        {
            var result = _contentLibraryService.DeleteContentResource(id, userId);
            return Ok(result);
        }


        /// <summary>
        /// Created By : Vinayak Y 
        /// Created On : 28 JAN 2020
        /// Description : Get Content Resource ById
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getContentResourceById")]
        [ProducesResponseType(typeof(Content), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetContentResourceById(int id)
        {
            var contentResource = await _contentLibraryService.GetContentResourceById(id);
            return Ok(contentResource);
        }

        /// <summary>
        /// Created By : Vinayak Y 
        /// Created On : 28 JAN 2020
        /// Description : Get Subjects By ContentId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSubjectsByContentId")]
        [ProducesResponseType(typeof(IEnumerable<Content>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSubjectsByContentId(int id)
        {
            var Suggestion = await _contentLibraryService.GetSubjectsByContentId(id);
            return Ok(Suggestion);
        }

        /// <summary>
        /// Created By - Girish S
        /// Created Date - 24 DEC 2019
        /// Description - approve content library
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("changeContentLibraryStatus")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> ChangeContentLibraryStatus(int resourceId, bool status, long schoolId)
        {
            var result = _contentLibraryService.ChangeContentLibraryStatus(resourceId, status, schoolId);
            return Ok(result);
        }
        /// <summary>
        /// Created By - Girish S
        /// Created Date - 24 DEC 2019
        /// Description - assign content library
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("assignContentResourceToSchool")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.Created)]
        public async Task<ActionResult> AssignContentResourceToSchool(int resourceId, bool status, long schoolId)
        {
            var result = _contentLibraryService.AssignContentResourceToSchool(resourceId, status, schoolId);
            return Ok(result);
        }

    }
}