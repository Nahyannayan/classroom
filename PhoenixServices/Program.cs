﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System;
using System.IO;

namespace Phoenix.API
{
#pragma warning disable CS1591

    public class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;
        public static readonly string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        public static void Main(string[] args)
        {
            var configuration = GetConfiguration();

            try
            {
                Log.Information("Starting web host");
                CreateWebHostBuilder(configuration, args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(IConfiguration configuration, string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .CaptureStartupErrors(true)
            //.UseKestrel(options =>
            //    {
            //        options.Listen(System.Net.IPAddress.Loopback, 443, listenOptions => listenOptions.UseHttps(new HttpsConnectionAdapterOptions { SslProtocols = SslProtocols.Tls12 }));
            //    })
            .UseContentRoot(Directory.GetCurrentDirectory())
            .UseConfiguration(configuration)
            //.UseIISIntegration()
            .UseIIS()
            .UseStartup<Startup>()
            .UseSerilog();

        private static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

#if ELITE
            builder.AddJsonFile($"appsettings.elite.json", optional: true);
#elif GEMS
            builder.AddJsonFile($"appsettings.gems.json", optional: true,reloadOnChange: true);
#else
            builder.AddJsonFile($"appsettings.gems.json", optional: true,reloadOnChange: true);
#endif
            builder.AddEnvironmentVariables();
            var config = builder.Build();

            Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Information)
            .Enrich.FromLogContext()
            .ReadFrom.Configuration(config)
            .CreateLogger();

            //if (config.GetValue<bool>("UseVault", false))
            //{
            //    builder.AddAzureKeyVault(
            //        $"https://{config["Vault:Name"]}.vault.azure.net/",
            //        config["Vault:ClientId"],
            //        config["Vault:ClientSecret"]);
            //}

            return builder.Build();
        }
    }

#pragma warning restore CS1591
}