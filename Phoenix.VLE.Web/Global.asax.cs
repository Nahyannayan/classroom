﻿using jQuery.DataTables.Mvc;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.Logger;
using Phoenix.Models;
using Phoenix.VLE.Web.Controllers;
using Phoenix.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.SessionState;
using Phoenix.VLE.Web.Helpers;
using Newtonsoft.Json;
using Phoenix.Common.ViewModels;
using DevExpress.XtraReports.Security;
using DevExpress.XtraReports.UI;
using Elmah;
using Phoenix.Common.Helpers.Extensions;
using System.Web.Helpers;
using System.Security.Cryptography;
using Phoenix.VLE.Web.Utils;
using System.Web.Configuration;
using System.Configuration;
using System.Reflection;
using DevExpress.XtraReports.Native;

namespace Phoenix.VLE.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private ILoggerClient _loggerClient;
        protected void Application_Start()
        {
            var mksType = typeof(MachineKeySection);
            var mksSection = ConfigurationManager.GetSection("system.web/machineKey") as MachineKeySection;
            var resetMethod = mksType.GetMethod("Reset", BindingFlags.NonPublic | BindingFlags.Instance);

            var newConfig = new MachineKeySection();
            newConfig.ApplicationName = mksSection.ApplicationName;
            newConfig.CompatibilityMode = mksSection.CompatibilityMode;
            newConfig.DataProtectorType = mksSection.DataProtectorType;
            newConfig.Validation = mksSection.Validation;

            newConfig.ValidationKey = ConfigurationManager.AppSettings["MK_ValidationKey"];
            newConfig.DecryptionKey = ConfigurationManager.AppSettings["MK_DecryptionKey"];
            newConfig.Decryption = ConfigurationManager.AppSettings["MK_Decryption"]; // default: AES
            newConfig.ValidationAlgorithm = ConfigurationManager.AppSettings["MK_ValidationAlgorithm"]; // default: SHA1

            resetMethod.Invoke(mksSection, new object[] { newConfig });

            DevExpress.Security.Resources.AccessSettings.ReportingSpecificResources.SetRules(SerializationFormatRule.Allow(SerializationFormat.Code, SerializationFormat.Xml));

            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Tls;
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Tls11;
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Ssl3;

            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;


            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            UnityConfig.RegisterComponents();

            ModelValidatorProviders.Providers.Clear();
            ModelValidatorProviders.Providers.Add(new LocalizedDynamicModelValidatorProvider());

            // Lets MVC know that anytime there is a JQueryDataTablesModel as a parameter in an action to use the
            // JQueryDataTablesModelBinder when binding the model.
            ModelBinders.Binders.Add(typeof(JQueryDataTablesModel), new JQueryDataTablesModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime), new CustomDateModelBinder());
            MvcHandler.DisableMvcResponseHeader = true;
            AntiForgeryConfig.SuppressIdentityHeuristicChecks = true;

            #region To Store Report and configuration
            //Added by    - Ashwin Dubey 
            //Added on    - 20-September-2020
            //Description - Added for reporting module 
            SerializationService.RegisterSerializer(XPCollectionSerializer.Name, new XPCollectionSerializer());
            DevExpress.XtraReports.Web.Extensions.ReportStorageWebExtension.RegisterExtensionGlobal(new SchoolReportStorageWebExtension());
            #endregion

        }

        protected void Application_BeginRequest()
        {
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Tls;
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Tls11;
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Ssl3;

            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;


            /* Commented by Hatim - 20 SEP 2020
             The below lines of code commented because the bundled files were not being cached. 
             In order to allow bundled files to be cached there is no need to set the below code
             */
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            //Response.Cache.SetNoStore();

            if (HttpContext.Current.Request.Path.ToLower() == "/")
            {
                Context.RewritePath("/Home/");
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            _loggerClient = LoggerClient.Instance;
            var httpContext = ((MvcApplication)sender).Context;
            var currentController = " ";
            var currentAction = " ";
            var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

            var ex = Server.GetLastError();
            var cryptoEx = ex as CryptographicException;
            if (cryptoEx != null)
            {
                if (SessionHelper.CurrentSession.IsSSOLogin)
                {
                    Response.Redirect("/Account/MSSignout");
                }
                else
                {
                    Response.Redirect("/Account/Signout");
                }
            }
            _loggerClient.LogWarning("USERERROR:"+SessionHelper.CurrentSession.UserName+":"+ SessionHelper.CurrentSession.Id);
            _loggerClient.LogException(ex);
            //raise elmah
            //ErrorSignal.FromCurrentContext().Raise(ex);              //Logs the Error Message   
            //Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex, HttpContext.Current));


            #region Ajax Exception
            bool isAjaxCall = string.Equals("XMLHttpRequest", Context.Request.Headers["x-requested-with"], StringComparison.OrdinalIgnoreCase);
            Context.ClearError();
            if (isAjaxCall)
            {
                Context.Response.ContentType = "application/json";
                Context.Response.StatusCode = 500;
                Context.Response.Write(
                    new JavaScriptSerializer().Serialize(
                        new { error = ex.Message }
                    )
                );
                return;
            }
            #endregion

            if (currentRouteData != null)
            {
                if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                {
                    currentController = currentRouteData.Values["controller"].ToString();
                }

                if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                {
                    currentAction = currentRouteData.Values["action"].ToString();
                }
            }


            var controller = new ErrorController();
            var routeData = new RouteData();
            var action = "Index";

            if (ex is HttpException)
            {
                var httpEx = ex as HttpException;

                switch (httpEx.GetHttpCode())
                {
                    case 404:
                        // page not found
                        action = "HttpError404";
                        break;
                    case 500:
                        // server error
                        action = "HttpError500";
                        break;
                    // others if any
                    default:
                        action = "Index";
                        break;
                }
            }
            Server.ClearError();
            httpContext.ClearError();
            httpContext.Response.Clear();
            httpContext.Response.StatusCode = ex is HttpException ? ((HttpException)ex).GetHttpCode() : 500;
            httpContext.Response.TrySkipIisCustomErrors = true;
            // You may also need to reset the response type
            httpContext.Response.ContentType = "text/html";


            // routeData.DataTokens["area"] = "AreaName"; // In case controller is in another area
            routeData.Values["controller"] = "Error";
            routeData.Values["action"] = action;

            controller.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));

        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            _loggerClient = LoggerClient.Instance;
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            _loggerClient.LogWarning("Start Application_PostAuthenticateRequest");
            if (authCookie != null && !string.IsNullOrWhiteSpace(authCookie.Value))
            {
                try
                {
                    _loggerClient.LogWarning("Application_PostAuthenticateRequest Cookie Init");
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    UserPrincipalSerializeModel serializeModel = serializer.Deserialize<UserPrincipalSerializeModel>(authTicket.UserData);

                    UserPrincipal newUser = new UserPrincipal(authTicket.Name);
                    newUser.Id = serializeModel.Id;
                    newUser.FirstName = serializeModel.FirstName;
                    newUser.LastName = serializeModel.LastName;
                    newUser.FullName = serializeModel.FullName;
                    newUser.OldUserId = serializeModel.OldUserId;
                    newUser.Email = serializeModel.Email;
                    newUser.UserName = serializeModel.UserName;
                    newUser.UserTypeId = serializeModel.UserTypeId;
                    newUser.UserType = (UserTypes)serializeModel.UserTypeId;
                    newUser.UserTypeName = serializeModel.UserTypeName;
                    newUser.RoleId = serializeModel.RoleId;
                    newUser.RoleName = serializeModel.RoleName;
                    newUser.ProfilePhoto = serializeModel.ProfilePhoto;
                    newUser.UserAvatar = serializeModel.UserAvatar;
                    newUser.ParentId = serializeModel.ParentId;
                    newUser.ParentUsername = serializeModel.ParentUsername;

                    newUser.SchoolId = serializeModel.SchoolId;
                    newUser.SchoolName = serializeModel.SchoolName;
                    newUser.SchoolCode = serializeModel.SchoolCode;
                    newUser.SchoolImage = serializeModel.SchoolImage;
                    newUser.IsAdmin = serializeModel.IsAdmin;

                    newUser.BusinessUnitType = serializeModel.BusinessUnitType;
                    newUser.BusinessUnitTypeId = serializeModel.BusinessUnitTypeId;
                    newUser.LanguageId = serializeModel.LanguageId;
                    newUser.StudentTheme = serializeModel.StudentTheme;
                    newUser.CurrentModuleURL = ((MvcApplication)sender).Context.Request.Url.ToString();
                    HttpContext.Current.User = newUser;
                    _loggerClient.LogWarning("Application_PostAuthenticateRequest Cookie Assigned");
                }
                catch (ArgumentException ex)
                {
                    _loggerClient.LogWarning("Exception in global.asax Application_PostAuthenticateRequest ArgumentException");
                    _loggerClient.LogException(ex);
                }
                catch (CryptographicException ex)
                {
                    _loggerClient.LogWarning("Exception in global.asax Application_PostAuthenticateRequest CryptographicException");
                    _loggerClient.LogException(ex);
                    SessionHelper.LogOffUser();
                    SessionHelper.RedirectToLoginPage(null);
                }
            }
            _loggerClient.LogWarning("End Application_PostAuthenticateRequest");
        }


        protected void Application_AcquireRequestState()
        {
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["SelectedSchoolForSuperAdmin"] != null)
                {
                    var schoolInfo = (SchoolInformation)HttpContext.Current.Session["SelectedSchoolForSuperAdmin"];

                    var userData = HttpContext.Current.User as UserPrincipal;
                    if (userData != null)
                    {
                        userData.SchoolId = schoolInfo.SchoolId;
                        userData.SchoolName = schoolInfo.SchoolName;
                        userData.SchoolCode = schoolInfo.SchoolShortName;
                        userData.SchoolEmail = schoolInfo.SchoolEmail;
                        userData.SchoolImage = string.Empty;
                        userData.IsAdmin = true;

                        HttpContext.Current.User = userData;
                    }
                }
                //set language Id in current session
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["SystemLangaugeId"] != null)
                {
                    var preferedLang = HttpContext.Current.Session["SystemLangaugeId"];

                    var userData = HttpContext.Current.User as UserPrincipal;
                    if (userData != null)
                    {
                        userData.LanguageId = preferedLang.ToInteger();
                        //set user name by prefered lang
                        if (HttpContext.Current.Session["FullName"] != null)
                        {
                            var FullName = HttpContext.Current.Session["FullName"];
                            userData.FullName = FullName.ToString();
                        }
                        HttpContext.Current.User = userData;
                    }
                }
            }

            Helpers.AccessPermissionHelper.HandleAcquireRequestState(Context);
        }
        protected void Application_PreSendRequestHeaders(object sender, EventArgs e)
        {
            //HttpContext.Current.Response.Headers.Remove("X-Powered-By");
            //HttpContext.Current.Response.Headers.Remove("X-AspNet-Version");
            //HttpContext.Current.Response.Headers.Remove("X-AspNetMvc-Version");
            //HttpContext.Current.Response.Headers.Remove("Server");
            //HttpContext.Current.Response.Headers.Add("X-Frame-Options", "SAMEORIGIN");

            foreach (var accessurl in AccessPermissionHelper.GetForgotPasswordAllowedUrlsWithoutLogin())
            {
                if (Request.Url.AbsolutePath.ToLower().Contains(accessurl))
                    HttpContext.Current.Response.Headers.Remove("Location");
            }
        }
        //public override string GetVaryByCustomString(HttpContext context, string arg)
        //{
        //    if (arg == "User")
        //    {
        //        var userData = HttpContext.Current.User as UserPrincipal;
        //        return "User=" + userData.SchoolId+userData.RoleId;
        //    }

        //    return base.GetVaryByCustomString(context, arg);
        //}

        //protected void Application_EndRequest(Object sender, EventArgs e)
        //{
        //    // Iterate through any cookies found in the Response object.
        //    foreach (string cookieName in Response.Cookies.AllKeys)
        //    {
        //        Response.Cookies[cookieName].Secure = true;
        //    }
        //}
    }

}
