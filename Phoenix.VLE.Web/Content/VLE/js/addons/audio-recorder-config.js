var listRecordings = [];
function __log(e, data) {
    //log.innerHTML += "\n" + e + " " + (data || '');
    //$("#log").addClass("sdbfsdbfsdf");
}

var audio_context;
var recorder;
//debugger;


function startUserMedia(stream) {
    var input = audio_context.createMediaStreamSource(stream);
    //__log('Media stream created.');

    // Uncomment if you want the audio to feedback directly
    //input.connect(audio_context.destination);
    //__log('Input connected to audio context destination.');

    recorder = new Recorder(input);
    //__log('Recorder initialised.');
}

function startRecording(button) {
    recorder && recorder.record();
    button.disabled = true;
    button.nextElementSibling.disabled = false;
    __log('Recording...');
}

function stopRecording(button) {
    recorder && recorder.stop();
    button.disabled = true;
    button.previousElementSibling.disabled = false;
    //__log('Stopped recording.');
    // create WAV download link using audio data blob
    createDownloadLink();

    recorder.clear();
}

function createDownloadLink() {
    recorder && recorder.exportWAV(function (blob) {
        var url = URL.createObjectURL(blob);
        var li = document.createElement('li');
        var au = document.createElement('audio');
        var hf = document.createElement('a');
        var i = document.createElement('i');
        //var bt = document.createElement('button');

        au.controls = true;
        au.src = url;
        hf.onclick = function () {
            //debugger;
            var deletedFileName = this.parentElement.closest('li').className;
            var parentElement = this.parentElement.parentElement;
            var childElement = this.parentElement;
            globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
            $(document).bind('okClicked', this, function (e) {
                $.each(listRecordings, function (idx, file) {
                    //debugger;
                    if (typeof file != "undefined") {
                        if (file.name + " align-items-center d-flex mb-2" == deletedFileName) {
                            listRecordings.splice(listRecordings.indexOf(file), 1);
                        }
                    }
                });
                parentElement.removeChild(childElement);
            });
        };
        hf.className = 'deleteRecording mx-2';
        i.className = 'far fa-trash-alt';
        hf.append(i);
        //hf.href = url;
        var FileName = new Date().toISOString() + '.wav';
        //hf.innerHTML = hf.download;
        li.className = FileName + " align-items-center d-flex mb-2";
        li.appendChild(au);
        li.appendChild(hf);
        //recordingslist.appendChild(li);
        if (recordingslist.length == undefined)
            recordingslist.prepend(li);
        else recordingslist[recordingslist.length - 1].prepend(li);


        //bt.textContent = "Send post";

        //bt.onclick = function () { saveRecording(blob); }
        //li.appendChild(bt);

        //changes by vinayak
        var myFile = blobToFile(blob, FileName);
        function blobToFile(theBlob, fileName) {
            //A Blob() is almost a File() - it's just missing the two properties below which we will add
            theBlob.lastModifiedDate = new Date();
            theBlob.name = fileName;
            return new File([theBlob], fileName);
        }
        //var recordings = new FormData()
        //recordings.append("recordings", myFile, "recording.wav");
        listRecordings.push(myFile)

        //$.ajax({
        //    type: 'POST',
        //    url: '/Files/Files/SaveRecordingFile',
        //    data: recordings,
        //    processData: false,
        //    contentType: false,
        //    success: function (result) {

        //    },
        //    error: function (msg) {

        //    }
        //});
        //$("#recordingslist li audio").each(function () {
        //    var showCurrentLi = $(this);
        //    $.ajax({
        //        type: 'POST',
        //        url: '/Files/Files/SaveRecordingFile',
        //        data: { recording: hf.download },
        //        success: function (result) {

        //        },
        //        error: function (msg) {

        //        }
        //    });
        //});

        //end
    });

}


window.onload = function init() {
    //debugger;
    try {
        //// webkit shim
        //window.AudioContext = window.AudioContext || window.webkitAudioContext;
        //navigator.getUserMedia = (
        //    navigator.getUserMedia ||
        //    navigator.webkitGetUserMedia ||
        //    navigator.mozGetUserMedia ||
        //    navigator.msGetUserMedia
        //);

        //if (typeof navigator.mediaDevices.getUserMedia === 'undefined') {
        //    navigator.getUserMedia({
        //        audio: true
        //    }, startUserMedia);
        //} else {
        //    navigator.mediaDevices.getUserMedia({
        //        audio: true
        //    }).then(startUserMedia).catch(errorHandler);
        //}

        //function errorHandler() {
        //    console.log("error");
        //}

        //window.URL = window.URL || window.webkitURL;

        ////if (localStorage.getItem("audiocontextstate") != 'running') {
        ////    audio_context = new AudioContext();
        ////    localStorage.setItem("audio_context", audio_context);
        ////}
        ////else {
        ////    var ext_context = localStorage.getItem("audio_context");
        ////    audio_context = ext_context.resume();
        ////}
        ////localStorage.setItem("audiocontextstate", audio_context.state);


        //audio_context = new AudioContext();
        //audio_context = (typeof window !== 'undefined' && typeof window.AudioContext !== 'undefined') ? new AudioContext() : null;
        //__log('Audio context set up.');
        //__log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
    } catch (e) {
        //alert('No web audio support in this browser!');
    }
};

//window.onbeforeunload = function (e) {
//    //debugger;
//    audio_context.close();
//};