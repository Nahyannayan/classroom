//popoverEnabler.attachPopover();
// webkit shim
window.AudioContext = window.AudioContext || window.webkitAudioContext;
navigator.getUserMedia = (
    navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia
);

if (typeof navigator.mediaDevices.getUserMedia === 'undefined') {
    navigator.getUserMedia({
        audio: true
    }, startUserMedia);
} else {
    navigator.mediaDevices.getUserMedia({
        audio: true
    }).then(startUserMedia).catch(errorHandler);
}

function errorHandler() {
    console.log("error");
}

window.URL = window.URL || window.webkitURL;

audio_context = new AudioContext();