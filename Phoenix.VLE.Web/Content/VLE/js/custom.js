﻿
function getStudentDetails (studentId) {

    $.ajax({
        type: 'POST',
        url: '/Parent/SetCurrentSelectedStudent',
        data: { studentId: studentId },
        success: function (result) {
            if (result) {
                window.location.reload();//.href = "/Parent/Dashboard";                        
            }
        },
        error: function (msg) {
        }
    });
}
   

$(document).ready(function () {

    globalFunctions.convertImgToSvg();
    //page Direction
    let pageDirection = $("html").attr("dir");
    if (pageDirection == "rtl") {
        direction = true
    } else {
        direction = false
    }
    //banner

    var winWidth = $(window).outerWidth();
    var bannerSliderHeight = $("#dashboardBanners .item").outerHeight();
    var studentInfoHeight = bannerSliderHeight - 95;

    /* The below code has been commented due to the values not fetched as required */
    //if (($("#dashboardBanners .item").length >= 1) && winWidth >= 768) {
    //    $(".studentDetails").height(bannerSliderHeight);
    //    //$("studentDetails .studentInfo").height(studentInfoHeight);
    //    $(".studentDetails .studentInfo").mCustomScrollbar({
    //        setHeight: studentInfoHeight,
    //        autoHideScrollbar: true,
    //        autoExpandScrollbar: true,
    //        scrollbarPosition: "outside",
    //    })
    //}
    // quick action button
    if ($('.cd-stretchy-nav').length > 0) {
        var stretchyNavs = $('.cd-stretchy-nav');

        stretchyNavs.each(function () {
            var stretchyNav = $(this),
                stretchyNavTrigger = stretchyNav.find('.cd-nav-trigger');

            stretchyNavTrigger.on('click', function (event) {
                event.preventDefault();
                stretchyNav.toggleClass('nav-is-visible');
            });
        });

        $(document).on('click', function (event) {
            (!$(event.target).is('.cd-nav-trigger') && !$(event.target).is('.cd-nav-trigger span')) && stretchyNavs.removeClass('nav-is-visible');
        });
    }

    //toggle checkbox appearance in grid view

    $(document).on('change', ".grid-container input[type='checkbox']", function () {
        $(this).closest(".check-item").toggleClass("show");
    });

    //create assignment checkbox selection
    $(".selectAll").change(function () {
        console.log("select all clicked");
        $(this).closest(".search-with-bg").next(".accordion").find("input[type='checkbox']").prop('checked', this.checked);
    })
    $(".level-1").change(function () {
        console.log("level 1 checkbox clicked");
        $(this).closest(".card").find(".level-2, .level-3").prop('checked', this.checked);
    })
    $(".level-2").change(function () {
        console.log("level 2 checkbox clicked");
        $(this).closest(".card").find(".level-3").prop('checked', this.checked);
    })
    $(".level-3").change(function () {
        $(this).closest(".card").find(".level-2").prop('checked', this.checked);
        //$(this).closest(".card").closest(".card").find(".level-1").prop('checked', this.checked);
        //$(this).parentsUntil($( ".card-header" ), ".primary-topic").addClass("sdfnahflhasldfhlasjdfjkasdfs");
    })

    $("#BespokesearchContent").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $(".search-item-Bespoke").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    if ($('.selectpicker').length) {
        $('select').selectpicker({
            noneSelectedText: translatedResources.selectedText,
            size: 8
        });
    };
    $('body').tooltip({ selector: '[data-toggle="tooltip"]', html: true });

    //pagination arabic class
    if ($('.pagination').length) {
        $('.pagination li a').addClass('arabictxt');
    };

    //hamburger menu -----
    $('.first-button').on('click', function () {
        $('.animated-icon1').toggleClass('open');
    });
    $('.second-button').on('click', function () {
        $('.animated-icon2').toggleClass('open');
    });
    $('.third-button').on('click', function () {
        $('.animated-icon3').toggleClass('open');
    });


    //sidenav toggle -----
    $(".navbar-toggler").on("click", function () {
        $(".sidebar-fixed").toggleClass("slideout");
        //$(".sidenav-overlay").toggleClass("show");
    });

    $('.has-dropdown .dropdown-toggle').on("click", function (e) {
        e.stopPropagation();
        e.preventDefault();
        $(this).next('.dropdown-menu').toggle();
    });
    $('.has-dropdown .dropdown-menu a.dropdown-item').on('click', function () {
        $(this).parent().parent().find('.dropdown-menu').hide();
    });

    if ($(".select-picker-withImg").length) {
        //const BASE_URL = $('base[ href ]').attr('href');
        //const BASE_URL = "img/"
        const $_SELECT_PICKER = $('.select-picker-withImg');

        $_SELECT_PICKER.find('option').each((idx, elem) => {
            const $OPTION = $(elem);
            const IMAGE_URL = $OPTION.attr('data-thumbnail');

            if (IMAGE_URL) {
                $OPTION.attr('data-content', "<img src='%i'/> %s".replace(/%i/, IMAGE_URL).replace(/%s/, $OPTION.text()))
            }

            console.warn('option:', idx, $OPTION)
        });

        $_SELECT_PICKER.selectpicker();
    };


    //For loader
    $(document)
        .ajaxStart(function () {
            //$('.loader-wrapper').fadeIn(500, function () {
            //});
            $('#AjaxLoader').fadeIn(500, function () { });
        })
        .ajaxStop(function () {
            //$('.loader-wrapper').fadeOut(500, function () {
            //});
            $('#AjaxLoader').fadeOut(500, function () { });
            //setInterval(function () { $('#AjaxLoader').fadeOut(500, function(){ }); }, 500);
        })
        .ajaxError(function (x, xhr, error) {
            globalFunctions.onFailure(x, xhr, error);
        });
    //--------------------------------------------------------------------------------------
    if ($("#particles-js").length) {
        particlesJS("particles-js", {
            "particles": {
                "number": {
                    "value": 60,
                    "density": {
                        "enable": true,
                        "value_area": 500
                    }
                },
                "color": {
                    "value": "#ffffff"
                },
                "shape": {
                    "type": "circle",
                    "stroke": {
                        "width": 0,
                        "color": "#000000"
                    },
                    "polygon": {
                        "nb_sides": 4
                    },
                    "image": {
                        "src": "img/github.svg",
                        "width": 100,
                        "height": 100
                    }
                },
                "opacity": {
                    "value": 0.2,
                    "random": false,
                    "anim": {
                        "enable": false,
                        "speed": 1,
                        "opacity_min": 0.1,
                        "sync": false
                    }
                },
                "size": {
                    "value": 3,
                    "random": true,
                    "anim": {
                        "enable": true,
                        "speed": 4.8,
                        "size_min": 1.6,
                        "sync": false
                    }
                },
                "line_linked": {
                    "enable": true,
                    "distance": 200,
                    "color": "#ffffff",
                    "opacity": 0.4,
                    "width": 1
                },
                "move": {
                    "enable": true,
                    "speed": 2,
                    "direction": "top-right",
                    "random": false,
                    "straight": false,
                    "out_mode": "out",
                    "bounce": false,
                    "attract": {
                        "enable": false,
                        "rotateX": 600,
                        "rotateY": 1200
                    }
                }
            },
            "interactivity": {
                "detect_on": "canvas",
                "events": {
                    "onhover": {
                        "enable": false,
                        "mode": "repulse"
                    },
                    "onclick": {
                        "enable": false,
                        "mode": "push"
                    },
                    "resize": true
                },
                "modes": {
                    "grab": {
                        "distance": 400,
                        "line_linked": {
                            "opacity": 1
                        }
                    },
                    "bubble": {
                        "distance": 400,
                        "size": 40,
                        "duration": 2,
                        "opacity": 8,
                        "speed": 3
                    },
                    "repulse": {
                        "distance": 200,
                        "duration": 0.4
                    },
                    "push": {
                        "particles_nb": 4
                    },
                    "remove": {
                        "particles_nb": 2
                    }
                }
            },
            "retina_detect": true
        });
    }
    //--------------------------------------------------------------------------------------
    $('textarea').each(function (element, i) {
        //console.log($(this).val());
        if (($(this).val() !== undefined && $(this).val().length > 0) || $(this).attr("placeholder") !== null) {
            $(this).siblings('label').addClass('active');
        }
        else {
            $(this).siblings('label').removeClass('active');
        }
    });

    //$("#aShowLogger").on("click", function () {
    //    globalFunctions.showErrorLogger($(this));
    //});

    //set timezone Offset
    if (globalFunctions.getCookie("TimeZone=IsSet") === "false") {
        var offset = new Date().getTimezoneOffset();
        document.cookie = "TimeZone=IsSet=true;expires=session;path=/;secure";
        document.cookie = "TimeZone=OffSet=" + offset + ";expires=session;path=/;secure";
    }
});
$(window).resize(function() {
    var winWidth = $(window).outerWidth();
    if(winWidth<=414)
    {   
        $("#NotificationDV").css({"width":"100%"});
    }
    else
    {
        $("#NotificationDV").css({"width":"400px"});
    }
});
$(window).on("load", function () {

    $(".sidebar-fixed .list-group").mCustomScrollbar({
        setHeight: "86%",
        autoExpandScrollbar: true,
        scrollbarPosition: "outside"
    });

    $(".sidebar-fixed .list-group").mCustomScrollbar('scrollTo', $("#sideNavigations a.active"));

    StartTimers();
    $("body").on("mousemove", ResetTimers());

    

});

function OnSelectStudentVault(Id) {
    if (globalFunctions.isValueValid(Id)) {
        $.post('/Shared/Shared/SetSession', { key: "VaultSelectedStudentId", value: Id }, function (data) {
            if (data == true)
                location.href = '/schoolstructure/locker';
            else {
                globalFunctions.onFailure();
            }
        });
    }
}


//-------- session time out and logout  -- Done By Abhijeet
// Set timeout variables.
var timoutWarning = 2520000; // Display warning in 14 Mins. -- 780000,28mins 1680000 , 58mins = 3480000, 118mins -- 7080000‬, 42mins=2520000
var timoutNow = 2640000; // Timeout in 15 mins. - 900000,  30mins - 1800000, 60mins = 3600000,  120 mins - 7200000‬,44mins=2640000
var logoutUrl = '/account/mssignout'; // URL to logout page.
var timerCountdown = "02:00";
var interval;
var warningTimer;
var timeoutTimer;

// Start timers.
function StartTimers() {
    warningTimer = setTimeout("IdleWarning()", timoutWarning);
    timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
}

// Reset timers.
function ResetTimers() {
    clearTimeout(warningTimer);
    clearTimeout(timeoutTimer);
    StartTimers();

    $('#sessionTimeout').modal({ show: false });

}

// Show idle timeout warning dialog.
function IdleWarning() {
    $('#sessionTimeout').modal({ show: true });
    timeCounter();
}

// Logout the user.
function IdleTimeout() {
    window.location = logoutUrl;
}

function clearCountdown() {
    clearInterval(interval);
    timerCountdown = "02:00";
    $('#sessionSecondsRemaining').text('');

    $.ajax({
        type: 'POST',
        url: "/Account/ExtendSession",
        //data: { 'id': id },
        async: false,
        success: function (result) {
            if (result.Success === true)
                console.log(result);
        },
        error: function (msg) {
            globalFunctions.showMessage("error", translatedResources.technicalError);
        }
    });
}

function timeCounter() {
    //var timerCountdown = "1:10";
    interval = setInterval(function () {
        var timer = timerCountdown.split(':');
        //by parsing integer, I avoid all extra string processing
        var minutes = parseInt(timer[0], 10);
        var seconds = parseInt(timer[1], 10);
        --seconds;
        //debugger
        minutes = (seconds < 0) ? --minutes : minutes;
        if (minutes < 0) clearInterval(interval);
        seconds = (seconds < 0) ? 59 : seconds;
        seconds = (seconds < 10) ? '0' + seconds : seconds;
        //minutes = (minutes < 10) ?  minutes : minutes;
        $('#sessionSecondsRemaining').html(minutes + ':' + seconds);
        timerCountdown = minutes + ':' + seconds;
        if (minutes == 0 && seconds == 0) {
            clearInterval(interval);
            IdleTimeout();
        }
    }, 1000);

}
// right drawer animation 
$('body').on('click', '.trigger-rightDrawer', function () {
    let target = $(this).data('target');
    let targetWidth = $(this).data('width');    

    if (typeof targetWidth !== typeof undefined && targetWidth !== false) {
        console.log(targetWidth);
        $('#' + target).width(targetWidth);
    }
    $('#' + target).toggleClass('open');
    $('#' + target).animate({
        right: '0'
    });
});

//$('body').on('click', '#notificationRightDrawer', function () {
//    $('.right-drawer:not(#leaderBoard)').toggleClass('open');
//    $('.right-drawer:not(#leaderBoard)').animate({
//        right: '0'
//    });
//});

$('body').on('click', '.closeRightDrawer', function () {
    if ($('.right-drawer').hasClass('open')) {
        $(this).closest('.right-drawer').removeClass('open');
        $(this).closest('.right-drawer').animate({
            right: '-100%'
        })
    }
});
//language switchers

function OnLangaugeSelection(langId) {
    if (globalFunctions.isValueValid(langId)) {
        $.ajax({
            type: 'POST',
            url: '/Shared/Shared/SetUserCurrentLanguage',
            data: { 'langId': langId },
            async: false,
            success: function (result) {
                if (result)
                    location.reload();
            },
            error: function (msg) {
                globalFunctions.showMessage("error", translatedResources.technicalError);
            }
        });
    }
}

// apply color to svg files
$('img.svg').each(function () {
    var $img = $(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    $.get(imgURL, function (data) {
        // Get the SVG tag, ignore the rest
        var $svg = $(data).find('svg');

        // Add replaced image's ID to the new SVG
        if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Check if the viewport is set, else we gonna set it if we can.
        if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        }

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});



