$(function() {
    var pageDirection = $("html").attr("dir");
    if(pageDirection == "rtl") {
        direction = true
    } else {
        direction = false
    }
    function getStudentDetailsHt() {
        var winWidth = $(window).outerWidth();
        var bannerSliderHeight = $("#dashboardBanners .item").outerHeight();
        //var studentInfoHeight = bannerSliderHeight - 95;

        if (($("#dashboardBanners .item").length >= 1) && winWidth >= 768) {
            $(".studentDetails").height(bannerSliderHeight);
            //$("studentDetails .studentInfo").height(studentInfoHeight);
            /* $(".studentDetails .studentInfo").mCustomScrollbar({
                setHeight: studentInfoHeight,
                autoHideScrollbar: true,
                autoExpandScrollbar: true,
                scrollbarPosition: "outside",
            }) */
        }
    }

    $("#dashboardBanners").owlCarousel({
        margin: 0,
        autoplay: true,                
        nav: false,
        dots: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        onResized: getStudentDetailsHt,
        onInitialized: getStudentDetailsHt
    });
    
    $("#schoolFeed").owlCarousel({
        margin: 0,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });

    $("#circulars").owlCarousel({
        margin: 5,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });

    $("#todo-list").owlCarousel({
        margin: 5,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false
    });

    $(".timeline").owlCarousel({
        margin: 20,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        responsive:{
            0:{
                items: 1,
                slideBy: 1
            },
            768: {
                items: 3,
                slideBy: 3
            },
            1366: {
                items: 5,
                slideBy: 5 
            },
            1920: {
                items: 5,
                slideBy: 5
            }
        }
    });

    $("#todays-updates").owlCarousel({
        margin: 0,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });

    $("#class-group").owlCarousel({
        margin: 15,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 3,
        slideBy: 3
    });

    $(".class-group-2, .other-groups").owlCarousel({
        margin: 15,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
    });
    

    $("#assignmentCarousel").owlCarousel({
        margin: 0,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });
    
    $(".teacherChatter").owlCarousel({
        margin: 20,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 4,
        slideBy: 4,
        mouseDrag: false,
        responsive: {
            0: {
                items: 1,
                slideBy: 1
            },
            768: {
                items: 3,
                slideBy: 3
            },
            1025: {
                items: 4,
                slideBy: 4
            },
            1366: {
                items: 4,
                slideBy: 4
            },
            1920: {
                items: 4,
                slideBy: 4
            }
        }
    });
    
    $(".whatsNewCarousel").owlCarousel({
        margin: 20,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        responsive: {
            0: {
                items: 1,
                slideBy: 1
            },
            768: {
                items: 3,
                slideBy: 3
            },
            1366: {
                items: 4,
                slideBy: 4
            },
            1920: {
                items: 5,
                slideBy: 5
            }
        }
    });

    $(".todaysScheduleCarousel").owlCarousel({
        margin: 20,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        responsive: {
            0: {
                items: 1,
                slideBy: 1
            },
            768: {
                items: 3,
                slideBy: 3
            },
            1366: {
                items: 4,
                slideBy: 4
            },
            1920: {
                items: 5,
                slideBy: 5
            }
        }
    });

    $("#new-assignments, #inProgress-assignments, #overdue-assignments, #completed-assignments").owlCarousel({
        margin: 0,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        onInitialized  : counter,
        onTranslated : counter
    });
    function counter(event) {
        var element   = event.target;
        var items     = event.item.count;
        var item      = event.item.index + 1;

        if(item > items) {
            item = item - items
        }
        $('#counter').html("Showing " + item + " of " + items)
    }

    $("#assignment-list").owlCarousel({
        margin: 0,
        autoplay: false,                
        nav: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        responsive:{
            0:{
                items: 1,
                slideBy: 1
            },
            1366: {
                items: 2,
                slideBy: 2 
            }
        }
    }); 
    
    $("#academicsAssignmentCarousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive:{
            410:{
                items: 2,
                slideBy: 1
            },
            767:{
                items: 3,
                slideBy: 1
            },
            1024: {
                items: 4,
                slideBy: 1
            },
            1200: {
                items: 5,
                slideBy: 1
            }
        }
    });

    $("#academicsVideoCarousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive:{
            410:{
                items: 2,
                slideBy: 1
            },
            767:{
                items: 3,
                slideBy: 1
            },
            1024: {
                items: 4,
                slideBy: 1
            },
            1200: {
                items: 5,
                slideBy: 1
            }
        }
    });

    $("#academicsOtherFileCarousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive:{
            410:{
                items: 2,
                slideBy: 1
            },
            767:{
                items: 3,
                slideBy: 1
            },
            1024: {
                items: 4,
                slideBy: 1
            },
            1200: {
                items: 5,
                slideBy: 1
            }
        }
    });

    $("#academicsReportsCarousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive:{
            410:{
                items: 2,
                slideBy: 1
            },
            767:{
                items: 3,
                slideBy: 1
            },
            1024: {
                items: 4,
                slideBy: 1
            },
            1200: {
                items: 5,
                slideBy: 1
            }
        }
    });

    $("#certificateCarousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive:{
            410:{
                items: 2,
                slideBy: 1
            },
            767:{
                items: 3,
                slideBy: 1
            },
            1024: {
                items: 4,
                slideBy: 1
            }
        }
    });

    $("#achievementCarousel1").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive:{
            410:{
                items: 2,
                slideBy: 1
            },
            767:{
                items: 4,
                slideBy: 1
            },
            1024: {
                items: 7,
                slideBy: 1
            }
        }
    });

    $("#achievementCarousel2").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive:{
            410:{
                items: 2,
                slideBy: 1
            },
            767:{
                items: 4,
                slideBy: 1
            },
            1024: {
                items: 7,
                slideBy: 1
            }
        }
    });
    $("#achievementCarousel3").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive:{
            410:{
                items: 2,
                slideBy: 1
            },
            767:{
                items: 4,
                slideBy: 1
            },
            1024: {
                items: 7,
                slideBy: 1
            }
        }
    });

    $("#meritsCarousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive:{
            410:{
                items: 2,
                slideBy: 1
            },
            767:{
                items: 4,
                slideBy: 1
            },
            1024: {
                items: 7,
                slideBy: 1
            }
        }
    });

    $("#goalsCarousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive:{
            550:{
                items: 2,
                slideBy: 1
            },
            1024: {
                items: 3,
                slideBy: 1
            }
        }
    });
});