document.addEventListener('DOMContentLoaded', function() {
	let pageDirection = $("html").attr("dir");
	
    var calendarEl = document.getElementById('calendar2');

    var calendar = new FullCalendar.Calendar(calendarEl, {
		plugins: [ 'interaction', 'dayGrid', 'dayGridMonth', 'bootstrap' ],
		header: {
			left: '',
			center: 'prev,title,next',
			right: 'dayGridDay,dayGridWeek,dayGridMonth'
		},
		views: {
			dayGridMonth: { // name of view
				titleFormat: { year: 'numeric', month: '2-digit', day: '2-digit' }
				// other view-specific options here
			}
		},
		defaultDate: '2019-06-12',
		//height: 'parent',
		/* contentHeight: 'auto', */
		navLinks: true, // can click day/week names to navigate views
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		themeSystem: 'bootstrap',
		dir: pageDirection,
		events: [
			{
				title: 'All Day Event',
				start: '2019-06-01'
			},
			{
				title: 'Long Event',
				start: '2019-06-07',
				end: '2019-06-10'
			},
			{
				groupId: 999,
				title: 'Repeating Event',
				start: '2019-06-09T16:00:00'
			},
			{
				groupId: 999,
				title: 'Repeating Event',
				start: '2019-06-16T16:00:00'
			},
			{
				title: 'Conference',
				start: '2019-06-11',
				end: '2019-06-13'
			},
			{
				title: 'Meeting',
				start: '2019-06-12T10:30:00',
				end: '2019-06-12T12:30:00'
			},
			{
				title: 'Lunch',
				start: '2019-06-12T12:00:00'
			},
			{
				title: 'Meeting',
				start: '2019-06-12T14:30:00'
			},
			{
				title: 'Happy Hour',
				start: '2019-06-12T17:30:00'
			},
			{
				title: 'Dinner',
				start: '2019-06-12T20:00:00'
			},
			{
				title: 'Birthday Party',
				start: '2019-06-13T07:00:00'
			},
			{
				title: 'Click for Google',
				url: 'http://google.com/',
				start: '2019-06-28'
			}
		]
    });
    calendar.render();
});

$(document).ready(function(){
	$('.fc-header-toolbar .btn-group button').removeClass('btn-primary').addClass('btn-outline-primary');
	$(' .fc-prev-button, .fc-next-button').removeClass('btn-primary');
	
})
