﻿$(document).ready(function () {
    
    var winWidth = $(window).outerWidth();
    var bannerSliderHeight = $("#dashboardBanners .item").outerHeight();
    var studentInfoHeight = bannerSliderHeight - 95;

    if(($("#dashboardBanners .item").length >= 1) && winWidth >= 768) {
        $(".studentDetails").height(bannerSliderHeight);
        $(".studentDetails .studentInfo").mCustomScrollbar({
            setHeight: studentInfoHeight,
            autoHideScrollbar: true,
            autoExpandScrollbar: true,
            scrollbarPosition: "outside",
        })
    }
    // quick action button
    if ($('.cd-stretchy-nav').length > 0) {
        var stretchyNavs = $('.cd-stretchy-nav');

        stretchyNavs.each(function () {
            var stretchyNav = $(this),
                stretchyNavTrigger = stretchyNav.find('.cd-nav-trigger');

            stretchyNavTrigger.on('click', function (event) {
                event.preventDefault();
                stretchyNav.toggleClass('nav-is-visible');
            });
        });

        $(document).on('click', function (event) {
            (!$(event.target).is('.cd-nav-trigger') && !$(event.target).is('.cd-nav-trigger span')) && stretchyNavs.removeClass('nav-is-visible');
        });
    }

    //toggle checkbox appearance in grid view

    $(document).on('change', ".grid-container input[type='checkbox']", function () {
        $(this).closest(".check-item").toggleClass("show");
    })
    
    if ($('.selectpicker').length) {
        $('select').selectpicker({
            noneSelectedText: "Select",
            size: 8
        });
    }

    if($('.pagination').length) {
        $('.pagination li a').addClass('arabictxt');
    }
    
    $('body').tooltip({ selector: '[data-toggle="tooltip"]', html: true });

    //hamburger menu -----
    $('.first-button').on('click', function () {
        $('.animated-icon1').toggleClass('open');
    });
    $('.second-button').on('click', function () {
        $('.animated-icon2').toggleClass('open');
    });
    $('.third-button').on('click', function () {
        $('.animated-icon3').toggleClass('open');
    });

    
    //sidenav toggle -----
    $(".navbar-toggler").on("click",function(){
        $(".sidebar-fixed").toggleClass("slideout");
        //$(".sidenav-overlay").toggleClass("show");
    });

    $('.has-dropdown .dropdown-toggle').on("click", function(e) {
        e.stopPropagation();
        e.preventDefault();
        $(this).next('.dropdown-menu').toggle();
    });
    $('.has-dropdown .dropdown-menu a.dropdown-item').on('click', function(){
        $(this).parent().parent().find('.dropdown-menu').hide();
    });

    // apply color to svg files
    $('img.svg').each(function(){
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
            
            // Check if the viewport is set, else we gonna set it if we can.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });

    //For loader
    $(document).ajaxStart(function () {
            //debugger;
            //$('.loader-wrapper').fadeIn(500, function () {
            //});
            $('#AjaxLoader').fadeIn(500, function () { });
        })
        .ajaxStop(function () {
            //$('.loader-wrapper').fadeOut(500, function () {
            //});
          
                $('#AjaxLoader').fadeOut(500, function () { }); 
           // setInterval(function () { $('#AjaxLoader').fadeOut(500, function () { }); }, 500);
        })
        .ajaxError(function (x, xhr, error) {
            globalFunctions.onFailure(x, xhr, error);
        });

    $(window).click(function(e) {
        if (e.target.class === 'show') {
            //Nothing
        } else {
            $(".incident-details").removeClass("animate slideIn show"); 
        }  
    });
    $('body').on('click', '.trigger-rightDrawer', function () {
        let target = $(this).data('target');
        let targetWidth = $(this).data('width');    

        if (typeof targetWidth !== typeof undefined && targetWidth !== false) {
            console.log(targetWidth);
            $('#' + target).width(targetWidth);
        }
        $('#' + target).toggleClass('open');
        $('#' + target).animate({
            right: '0'
        });
    });
    $('body').on('click', '.closeRightDrawer', function () {
        if ($('.right-drawer').hasClass('open')) {
            $(this).closest('.right-drawer').removeClass('open');
            $(this).closest('.right-drawer').animate({
                right: '-100%'
            })
        }
    });

    //remove notification after clicking on read btn
    $(".mark-noti-read li i").click(function () {
        $(this).closest("li").fadeOut(500, function () { $(this).remove(); });
    });
    

    //set timezone Offset
    if (globalFunctions.getCookie("TimeZone=IsSet") === "false") {
        var offset = new Date().getTimezoneOffset();
        document.cookie = "TimeZone=IsSet=true;expires=session;path=/;secure";
        document.cookie = "TimeZone=OffSet=" + offset + ";expires=session;path=/;secure";
    }

    
});
$(window).resize(function() {
    var winWidth = $(window).outerWidth();
    if(winWidth<=414)
    {   
        $("#NotificationDV").css({"width":"100%"});
    }
    else
    {
        $("#NotificationDV").css({"width":"400px"});
    }
});
$(window).on("load", function () {

    $(".sidebar-fixed .list-group").mCustomScrollbar({
        setHeight: "86%",
        autoExpandScrollbar: true,
        scrollbarPosition: "outside"
    });
    
    var delayInMilliseconds = 1000; //1 second

    setTimeout(function () {
        $(".sidebar-fixed .list-group").mCustomScrollbar('scrollTo', $("#sideNavigations a.active"));
    }, delayInMilliseconds);

});

//language switchers

function OnLangaugeSelection(langId) {
    if (globalFunctions.isValueValid(langId)) {
        $.ajax({
            type: 'POST',
            url: '/Shared/Shared/SetUserCurrentLanguage',
            data: { 'langId': langId },
            async: false,
            success: function (result) {
                if (result)
                    location.reload();
            },
            error: function (msg) {
                globalFunctions.showMessage("error", translatedResources.technicalError);
            }
        });
    }
}

//-------- session time out and logout  -- Done By Abhijeet
// Set timeout variables.
var timoutWarning = 2520000; // Display warning in 14 Mins. -- 780000,28mins 1680000 , 58mins = 3480000, 118mins -- 7080000‬, 42mins=2520000
var timoutNow = 2640000; // Timeout in 15 mins. - 900000,  30mins - 1800000, 60mins = 3600000,  120 mins - 7200000‬,44mins=2640000
var logoutUrl = '/account/mssignout'; // URL to logout page.
var timerCountdown = "02:00";
var interval;
var warningTimer;
var timeoutTimer;

// Start timers.
function StartTimers() {
    warningTimer = setTimeout("IdleWarning()", timoutWarning);
    timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
}

// Reset timers.
function ResetTimers() {
    clearTimeout(warningTimer);
    clearTimeout(timeoutTimer);
    StartTimers();

    $('#sessionTimeout').modal({ show: false });

}

// Show idle timeout warning dialog.
function IdleWarning() {
    $('#sessionTimeout').modal({ show: true });
    timeCounter();
}

// Logout the user.
function IdleTimeout() {
    window.location = logoutUrl;
}

function clearCountdown() {
    clearInterval(interval);
    timerCountdown = "02:00";
    $('#sessionSecondsRemaining').text('');

    $.ajax({
        type: 'POST',
        url: "/Account/ExtendSession",
        //data: { 'id': id },
        async: false,
        success: function (result) {
            if (result.Success === true)
                console.log(result);
        },
        error: function (msg) {
            globalFunctions.showMessage("error", translatedResources.technicalError);
        }
    });
}

function timeCounter() {
    //var timerCountdown = "1:10";
    interval = setInterval(function () {
        var timer = timerCountdown.split(':');
        //by parsing integer, I avoid all extra string processing
        var minutes = parseInt(timer[0], 10);
        var seconds = parseInt(timer[1], 10);
        --seconds;
        //debugger
        minutes = (seconds < 0) ? --minutes : minutes;
        if (minutes < 0) clearInterval(interval);
        seconds = (seconds < 0) ? 59 : seconds;
        seconds = (seconds < 10) ? '0' + seconds : seconds;
        //minutes = (minutes < 10) ?  minutes : minutes;
        $('#sessionSecondsRemaining').html(minutes + ':' + seconds);
        timerCountdown = minutes + ':' + seconds;
        if (minutes == 0 && seconds == 0) {
            clearInterval(interval);
            IdleTimeout();
        }
    }, 1000);

}