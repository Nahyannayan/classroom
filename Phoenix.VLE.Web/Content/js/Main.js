﻿$(document).ready(function () {
    ConvertCase();

    $(document).on('click', 'a#changeMood, a.change-mood', function (e) {
        $("#basicExampleModal .modal-body").load("/Home/ListUserFeeling", function () {
            $("#basicExampleModal").modal();
        });
    });

    $(document).on('click', 'p.btnOpenDownload', function (e) {
        if (!$(e.target).is(".chk-downloadable")) {
            window.open($(this).data("filepath"), "_blank")
        }
    });

    $(document).on("click", ".aShowLogger",function () {
        globalFunctions.showErrorLogger($(this));
    });

    $(document).on("blur", "#formSynchronousLessons #MeetingName", function () {
        $(this).val($(this).val().trim());
    })

    $(document).on("click", ".livesesion.joinmeeting", function () {
        var url = $(this).data("meetingurl");
        $.ajax({
            url: "/SchoolStructure/SchoolGroups/GetUserLiveSessionInfo",
            type: "GET",
            data: { meetingUrl: url },
            success: function (data) {
                var finalURL = data.MeetingURL;
                if (!globalFunctions.isValueValid(data.UserToken))
                    finalURL = url;

                var element = $("<a/>", {
                    onclick: "window.open('" + finalURL + "')",
                    id: 'meeting-open'
                });
                element.appendTo($("body"));
                element.click();
                element.remove();
            },
            error: function () {
                window.open(url);
            }

        });
    });

    $(document).on("click", "button.timetable-session.create-session", function () {
        var data = $(this).data("timetable");
        $.ajax({
            url: "/SchoolStructure/SchoolGroups/InitTimeTableSessionForm",
            data: data,
            type: "POST",
            success: function (result) {
                $("#modal_Loader").html(result);
                $('.modal-dialog').addClass("modal-lg");
                $("#modal_heading").text(translatedResources.scheduleLiveSession);
                var currentDate = new Date();
                $("#TimeZoneOffset").val(currentDate.getTimezoneOffset());
                $(".selectpicker").selectpicker({
                    size: 5
                });
                $("#MeetingDate,#MeetingTime").attr("readonly", "readonly");
                //var dateField = $("input[name=MeetingDate]");
                //dateField.datetimepicker({
                //    format: 'DD-MMM-YYYY',
                //    ignoreReadonly: true
                //});
                //dateField.data("DateTimePicker").minDate(moment())
                $('.date-picker1').datetimepicker({
                    format: 'hh:mm A'
                });
               
                $("#myModal").modal("show");
                $("#formSynchronousLessons").attr("data-ajax-success", "onSessionSaveSuccess");
            },
            error: function () {
                globalFunctions.onFailure();
            }
        });
    });

});

function ConvertCase() {
    $(".to-sentence-case").each(function (index) {
        let str = $(this).text();
        strVal = str.toLowerCase().replace(/\b[a-z]/g, function (txtVal) {
            return txtVal.toUpperCase();
        });
        $(this).text(strVal);
    });
}
