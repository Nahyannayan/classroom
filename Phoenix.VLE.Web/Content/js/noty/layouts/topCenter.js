﻿(function ($) {

    $.noty.layouts.topCenter = {
        name: 'topCenter',
        options: { // overrides options

        },
        container: {
            object: '<ul id="noty_topCenter_layout_container" style="transform:translateY(-50%)"/>',
            selector: 'ul#noty_topCenter_layout_container',
            style: function () {
                $(this).css({
                    top: '50%',
                    left: 0,
                    position: 'fixed',
                    width: '410px',
                    height: 'auto',
                    margin: 0,
                    padding: 0,
                    listStyleType: 'none',
                    zIndex: 10000000
                });

                $(this).css({
                    left: ($(window).width() - $(this).outerWidth(false)) / 2 + 'px'
                });
            }
        },
        parent: {
            object: '<li />',
            selector: 'li',
            css: {}
        },
        css: {
            display: 'none',
            width: '410px',
            padding: '15px'
        },
        addClass: ''
    };

})(jQuery);
