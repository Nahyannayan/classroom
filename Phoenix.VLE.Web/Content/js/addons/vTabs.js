// tabbed content
// http://www.entheosweb.com/tutorials/css/tabs.asp
$(".tab-content").hide();
$(".tab-content:first").show();

/* if in tab mode */
$("ul.tabs li").click(function () {

    $(".tab-content").hide();
    var activeTab = $(this).attr("rel");
    $("#" + activeTab).fadeIn();

    $("ul.tabs li").removeClass("active");
    $(this).addClass("active");

    $(".tab-drawer-heading").removeClass("d_active");
    $(".tab-drawer-heading[rel^='" + activeTab + "']").addClass("d_active");

    /*$(".tabs").css("margin-top", function(){ 
       return ($(".tab-container").outerHeight() - $(".tabs").outerHeight() ) / 2;
    });*/
});
$(".tab-container").css("min-height", function () {
    return $(".tabs").outerHeight() + 50;
});
/* if in drawer mode */
$(".tab-drawer-heading").click(function () {

    $(".tab-content").hide();
    var d_activeTab = $(this).attr("rel");
    $("#" + d_activeTab).fadeIn();

    $(".tab-drawer-heading").removeClass("d_active");
    $(this).addClass("d_active");

    $("ul.tabs li").removeClass("active");
    $("ul.tabs li[rel^='" + d_activeTab + "']").addClass("active");
});


/* Extra class "tab_last"
   to add border to bottom side
   of last tab
$('ul.tabs li').last().addClass("tab_last");*/
