$(document).ready(function () {
    //-------- Audit stats ---------
    var ctxD = document.getElementById("assesmentChart").getContext('2d');
    var myLineChart = new Chart(ctxD, {
        type: 'doughnut',
        data: {
            labels: ["No. of activities", "Complete and up to date", "Incomplete activities", "Pending"],
            datasets: [{
                data: [25, 35, 10, 35],
                backgroundColor: ["#4981FD", "#49DB93", "#FFD063", "#FF9850"],
                hoverBackgroundColor: ["#4981FD", "#49DB93", "#FFD063", "#FF9850"],
                borderWidth: [0, 0, 0, 0]
            }]
        },
        options: {
            responsive: true,
            legend: {
                display: false,
                position: "bottom",
                labels: {
                    boxWidth: 10,
                    paddding: 40
                }
            }
        }
    });
});
