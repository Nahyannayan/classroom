﻿/// <reference path="../common/global.js" />
var multipleFileUploader = function () {
    var validatedAttachmentFiles = [], _saveUrl = '', deletedAttachmentFiles = [], _callbackFunction, _parameter;

    var init = function (url, callbackFunction, parameters) {
        validatedAttachmentFiles = [];
        _saveUrl = url;
        deletedAttachmentFiles = [];
        deletedAttachmentFilesID = [];
        _callbackFunction = callbackFunction || function () { };
        _parameter = parameters || [];
    },

        saveFileToCollectionAndAddRowToGrid = function (uploader) {
            var isValidAttachments = true;
            var fileList = [];

            for (var i = 0; i < uploader.files.length; ++i) {
                var file = uploader.files[i];
                if (file == undefined) return false;
                var invalidMessage = validateFileType(file.name);
                var result = checkFileSize(file);
                if (result) {
                    globalFunctions.showWarningMessage("Invalid File size. Please select file size less than 1 MB!");
                    $(uploader).val("");
                    isValidAttachments = false;
                    break;
                }
                else if (globalFunctions.isValueValid(invalidMessage)) {
                    globalFunctions.showWarningMessage(invalidMessage);
                    $(uploader).val("");
                    isValidAttachments = false;
                    break;
                }
                else {
                    fileList.push(file);
                }
            }
            if (isValidAttachments) {
                addNewRowsToAttachmentGrid(uploader, fileList);
            }
            return isValidAttachments;
        },

        prepareAttachmentGrid = function (targetGrid) {
            var tblAttachment = $(targetGrid);

            var hasRows = tblAttachment.find("tbody > tr:not('.no-data-row')").length > 0;
            if (hasRows)
                $(targetGrid).parent().show();
            else {
                if (!tblAttachment.find("tbody > tr.no-data-row").length) {
                    $(targetGrid).parent().hide();
                }
            }
        },

        addNewRowsToAttachmentGrid = function (uploader, fileList) {
            var existingFiles = [];
            var data = $(uploader).data();

            var tblAttachment = $('#tbl-Attachment-fileUploader' + data.fieldname);
            var deleteBtn = "<a class='fa fa-trash action-btn-small btnDelete' onclick='multipleFileUploader.deleteAttachment(this);'></a>";
            var rowHtml = '<td class="col-name" data-filename="{0}"><a {3}>{0}</a></td><td class="text-center" data-linkedfieldname={1}>{2}</td>';
            $.each(fileList, function (idx, item) {
                if (!checkAttachmentExistsInGrid(tblAttachment, item)) {
                    var blob = new Blob([item]);
                    var url = URL.createObjectURL(blob);
                    //var fileName = getFileNameWithTimeStamp($(uploader), item.name);
                    var fileName = item.name;
                    var anchorTagHtml = 'href="' + url + '" download="' + fileName + '"';
                    var newRow = $('<tr />');
                    tblAttachment.find('tbody').append(newRow);
                    newRow.append(rowHtml.format(fileName, data.fieldname, deleteBtn, anchorTagHtml));
                    newRow.attr('data-newfile', true);
                    newRow.addClass('success');
                    validatedAttachmentFiles.push({
                        fieldName: $(uploader).data("fieldname"),
                        externalCategoryId: $(uploader).data("externalCategoryid"),
                        sourceFile: item,
                        fileName: fileName
                    }); // add new files in File Collection
                }
                else
                    existingFiles.push(item.name);
            });
            prepareAttachmentGrid(tblAttachment);
            if (existingFiles.length)
                globalFunctions.showWarningMessage(translatedResources.attachmentExistsMessage.format(existingFiles.join(',')));
        },

        saveAttachmentData = function (registrationId) {
            var formData = new FormData();
            var attachmentDetails = [];
            var existingAttachmentDetails = [];

            if (!globalFunctions.isValueValid(registrationId)) {
                return;
            }

            //Existing Attachments
            $('.attachment-table tr').each(function () {
                var sourceData = $(this).data();
                if (globalFunctions.isValueValid(sourceData.id))
                    existingAttachmentDetails.push({
                        AttachmentId: sourceData.id,
                        CategoryId: sourceData.externalCategoryId,
                        IsProcessed: false,
                        AttachmentName: sourceData.fileName
                    });
            });

            //Current Attachments
            $.each(validatedAttachmentFiles, function (idx, file) {
                attachmentDetails.push({
                    CategoryId: file.externalCategoryId,
                    IsProcessed: false,
                    AttachmentName: file.fileName
                });

                formData.append("files", file.sourceFile, file.fileName);
            });

            formData.append("registrationId", registrationId);
            formData.append("attachmentDetailStr", JSON.stringify(attachmentDetails));
            formData.append("existingAttachmentDetailStr", JSON.stringify(existingAttachmentDetails));
            formData.append('deletedAttachmentFiles', JSON.stringify(deletedAttachmentFiles));


            $.ajax({
                type: 'POST',
                url: _saveUrl,
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    globalFunctions.showMessage(result.NotificationType, result.Message);
                    if (result.Success == true) {
                        formData.delete("files");
//                        formData.reset();

                        existingAttachmentDetails = [];
                        attachmentDetails = [];
                        deletedAttachmentFiles = [];
                    }
                    $('#myModal').modal('hide');
                    _callbackFunction.apply(this, _parameter);
                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });
        },

        deleteAttachment = function (source) {
            globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
            $(source).unbind('okClicked');
            $(source).bind('okClicked', source, function (e) {
                var targetRow = $(source).closest('tr');
                var isNewFile = targetRow.data('newfile') != undefined ? targetRow.data('newfile') : false;

                if (isNewFile)
                    deleteNewlyAddedAttachment(targetRow.find('.col-name').text());
                else
                    deletedAttachmentFiles.push({
                        AttachmentId: targetRow.data('id'),
                        IsProcessed: false,
                        AttachmentName: targetRow.data('fileName')
                    });

                var tblAttachment = $('#tbl-Attachment-fileUploader' + $(source).closest('td').data('linkedfieldname'));
                targetRow.remove();
                prepareAttachmentGrid(tblAttachment);
            });
        },

        deleteNewlyAddedAttachment = function (value) {
            var index = checkIfAttachmentExistsAndReturnIndex(validatedAttachmentFiles, 'fileName', value);
            if (index != -1)
                validatedAttachmentFiles.splice(index, 1);
        },

        checkAttachmentExistsInGrid = function (targetGrid, value) {
            var isExists = false;
            var tblAttachment = $(targetGrid);
            tblAttachment.find('tr > td.col-name').each(function () {
                if ($(this).data('filename') == value) isExists = true;
            });
            return isExists;
        },

        getFileNameWithTimeStamp = function (source, fileName) {
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
            return $(source).data('fieldname') + '_' + $.now() + '.' + fileNameExt;
        },

        validateFileType = function (fileName) {
            var validExtensions = ['jpg', 'jpeg', 'png', 'gif', 'doc', 'docx', 'pdf', 'pptx'];
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
            if ($.inArray(fileNameExt, validExtensions) == -1) {
                return translatedResources.fileTypeValidationMessage.replace("{0}", validExtensions.join(', '));
            }
        },

        checkFileSize = function (file) {
            var filesize = file.size;
            return (filesize > 1000000);
        },

        checkIfAttachmentExistsAndReturnIndex = function (data, property, value) {
            for (var i = 0; i < data.length; i++) {
                if (data[i][property] === value) {
                    return i;
                }
            }
            return -1;
        };

    return {
        init: init,
        saveFileToCollectionAndAddRowToGrid: saveFileToCollectionAndAddRowToGrid,
        checkIfAttachmentExistsAndReturnIndex: checkIfAttachmentExistsAndReturnIndex,
        saveAttachmentData: saveAttachmentData,
        deleteAttachment: deleteAttachment
    }
}();