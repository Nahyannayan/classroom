﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var common = function () {
    var init = function () {
        $('.date-picker').attr("autocomplete", "off").datetimepicker({
            format: 'DD-MMM-YYYY',
            minDate: '2000-01-01',
            extraFormats: ['MM/DD/YYYY LT', 'DD/MM/YYYY LT', 'DD/MMM/YYYY LT', 'DD-MM-YYYY', 'MM-DD-YYYY']
        });//.on('keydown', function () { return false }).attr('autocomplete', 'off');
        $('.selectpicker').selectpicker('refresh');
    },
        IntegerRegex = /^[-]?\d+$/,
        UnsignedintegerRegex = /^\d*[0-9]\d*$/,
        DecimalRegex = /^[0-9]+([.][0-9]{1,3})?$/,
        AlfanumericRegex = /^[a-zA-Z0-9 ]+$/i,
        DatetimeRegex = /^(([0-9])|([0-2][0-9])|([3][0-1]))\-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\-\d{4}$/,
        attributes = ['required', 'min', 'max', 'minlength', 'maxlength', 'email', 'alphabet', 'numeric', 'alphanumeric', 'integer', 'unsignedint', 'decimal', 'minrangeid', 'maxrangeid', 'dateformat', 'datemax'],
        bindSingleDropDownByParameter = function (bindId, url, jsonParameterList, firstOptionValue, firstOptionText, IsSelectFirstOption = false, valueObj = "", textObj = "", isPost = false) {
            var functionResult;
            $.ajax({
                type: isPost ? "POST" : "GET",
                url: url,
                data: jsonParameterList,
                dataType: 'json',
                async: false,
                success: function (response) {
                    functionResult = response;
                    var dropDown = $(bindId);
                    dropDown.empty();
                    if (globalFunctions.isValueValid(firstOptionValue) || globalFunctions.isValueValid(firstOptionText))
                        dropDown.append($('<option></option>').val(firstOptionValue).html(firstOptionText));
                    $.each(response, function (i, item) {
                        if (globalFunctions.isValueValid(valueObj) && globalFunctions.isValueValid(textObj))
                            dropDown.append($('<option></option>').val(item[valueObj]).html(item[textObj]));
                        else
                            dropDown.append($('<option></option>').val(item.Value).html(item.Text));
                    });
                    $(bindId).selectpicker('refresh');
                    if (IsSelectFirstOption) {
                        if ($(bindId + " option").length > 1) {
                            $(bindId).prop("selectedIndex", 1);
                            $(bindId).selectpicker('refresh');
                        }
                    }
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
            return functionResult;
        },
        bindMultipleDropDownByParameter = function (bindIdList, url, jsonParameterList, firstOptionValueList, firstOptionTextList, IsSelectFirstOption = false) {
            var functionResult;
            $.ajax({
                type: "GET",
                url: url,
                data: jsonParameterList,
                dataType: 'json',
                async: false,
                success: function (response) {
                    functionResult = response;
                    $.each(bindIdList, function (index, value) {
                        var firstText = firstOptionValueList[index];
                        var firstValue = firstOptionTextList[index];
                        var dropDown = $(value);
                        dropDown.empty();
                        dropDown.append($('<option></option>').val(firstText).html(firstValue));
                        $.each(response[index], function (i, item) {
                            dropDown.append($('<option></option>').val(item.Value).html(item.Text));
                        });
                        $(value).selectpicker('refresh');
                        if (IsSelectFirstOption) {
                            if ($(value + " option").length > 1) {
                                $(value).prop("selectedIndex", 1);
                                $(value).selectpicker('refresh');
                            }
                        }
                    });
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
            return functionResult;
        },
        bindSingleDropDownByParameterAsync = function (bindId, url, jsonParameterList, firstOptionValue, firstOptionText, valueObj = "", textObj = "", isAsync) {
            return new Promise((resolve, reject) => {
                var functionResult;
                $.ajax({
                    type: "GET",
                    url: url,
                    data: jsonParameterList,
                    dataType: 'json',
                    async: globalFunctions.isValueValid(isAsync),
                    success: function (response) {
                        functionResult = response;
                        var dropDown = $(bindId);
                        dropDown.empty();
                        if (globalFunctions.isValueValid(firstOptionValue) || globalFunctions.isValueValid(firstOptionText))
                            dropDown.append($('<option></option>').val(firstOptionValue).html(firstOptionText));
                        $.each(response, function (i, item) {
                            if (globalFunctions.isValueValid(valueObj) && globalFunctions.isValueValid(textObj))
                                dropDown.append($('<option></option>').val(item[valueObj]).html(item[textObj]));
                            else
                                dropDown.append($('<option></option>').val(item.Value).html(item.Text));
                        });
                        $(bindId).selectpicker('refresh');
                        return resolve({ response: functionResult });
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                        return reject();
                    }
                });
            })
        },
        postRequest = function (url, object) {
            return new Promise((resolve, reject) => {
                $.post(url, object)
                    .then(response => resolve(response))
                    .fail(error => {
                        globalFunctions.showMessage(error.NotificationType, error.Message);
                        reject(error);
                    })
                    .catch(error => {
                        globalFunctions.showMessage(error.NotificationType, error.Message);
                        reject(error)
                    });
            });
        },
        notyDeleteConfirm = function (confirmMesage, onConfirmMethod, onCancelMethod) {
            noty({
                text: confirmMesage,
                layout: 'topCenter',
                modal: true,
                //theme:'metroui',
                buttons: [
                    {
                        addClass: 'btn btn-success btn-md shadow-none btn-clean',
                        text: 'Ok',
                        onClick: function ($noty) {
                            $noty.close();
                            if (onConfirmMethod)
                                onConfirmMethod();
                        }
                    },
                    {
                        addClass: 'btn btn-danger btn-md shadow-none btn-clean',
                        text: 'Cancel',
                        onClick: function ($noty) {
                            $noty.close();
                            if (onCancelMethod)
                                onCancelMethod();
                            //pageLoadingFrame('hide');
                        }
                    }
                ]

            });
        },
        notyConfirm = function (confirmMesage, onConfirmMethod, onCancelMethod) {
            noty({
                text: confirmMesage,
                layout: 'topCenter',
                modal: true,
                //theme:'metroui',
                buttons: [
                    {
                        addClass: 'btn btn-success btn-md shadow-none btn-clean',
                        text: '<i class="fas fa-check"></i>',
                        onClick: function ($noty) {
                            $noty.close();
                            if (onConfirmMethod)
                                onConfirmMethod();
                        }
                    },
                    {
                        addClass: 'btn btn-danger btn-md shadow-none btn-clean',
                        text: '<i class="fas fa-times"></i>',
                        onClick: function ($noty) {
                            $noty.close();
                            if (onCancelMethod)
                                onCancelMethod();
                            //pageLoadingFrame('hide');
                        }
                    }
                ]

            });
        },
        bindDropDown = function (dropdownId, data, textField, valueField, firstOptionValue, firstOptionText) {
            var dropdown = $(dropdownId);
            dropdown.empty();
            if (globalFunctions.isValueValid(firstOptionValue) || globalFunctions.isValueValid(firstOptionText))
                dropdown.append($('<option></option>').val(firstOptionValue).html(firstOptionText));
            $.each(data, function (i, item) {
                dropdown.append($('<option></option>').val(item[valueField]).html(item[textField]));
            });
            dropdown.selectpicker('refresh');
        },
        //-----------------------------Common Validation Functionality-----------------------------
        IsInvalidByFormOrDivId = function (formid, validateHiddenFields) {
            var elementList = [];
            var errorElement = [];
            common.attributes.forEach(x => {
                var inputElement = $("#" + formid).find(`input[data-${x}],textarea[data-${x}],select[data-${x}]`);
                inputElement.each((i, a) => {
                    if (elementList.some(e => e == a))
                        return;
                    elementList.push(a);
                });
            });
            var IsValid = true;
            $.each(elementList, function (index, element, t) {
                if (!$(element).is('div')) {
                    var keys = Object.keys($(element).data())
                    sortedKeys = []
                    common.attributes.forEach(function (key) {
                        var found = false;
                        items = keys.filter(function (item) {
                            if (!found && item == key) {
                                sortedKeys.push(item);
                                found = true;
                                return false;
                            } else
                                return true;
                        })
                    });

                    common.removeValidationMessage(element);

                    //remove space
                    if (!($(element).is('select')) && $(element).attr('type') != 'file' && $(element).attr('type') != 'radio')
                        trimInputValue($(element));
                    var IsSequence = true;

                    // To avoid validation for hidden controls
                    if ($(element).is(':hidden') && !(globalFunctions.isValueValid(validateHiddenFields)) && !(globalFunctions.isValueValid($(element).attr('comboname'))))
                        return;

                    sortedKeys.forEach(x => {
                        if (IsSequence) {
                            switch (x) {
                                case 'required':
                                    IsValid = common.IsRequiredValid(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'minlength':
                                case 'maxlength':
                                    IsValid = common.IsValidLength(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'min':
                                case 'max':
                                    IsValid = common.IsValidMinMaxValue(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'email':
                                    IsValid = common.IsValidEmail(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'alphabet':
                                    IsValid = common.IsValidAlphabet(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'numeric':
                                    IsValid = common.IsValidNumeric(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'integer':
                                    IsValid = common.IsValidInteger(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'unsignedint':
                                    IsValid = common.IsValidUnsignedInt(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'decimal':
                                    IsValid = common.IsValidDecimal(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'alphanumeric':
                                    IsValid = common.IsValidAlphaNumeric(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'dateformat':
                                    IsValid = common.IsValidDateFormat(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'minrangeid':
                                case 'maxrangeid':
                                    IsValid = common.IsValidMinMaxRange(element);
                                    IsSequence = IsValid;
                                    break;
                                case 'datemax':
                                    IsValid = common.IsValidDateRange(element);
                                    IsSequence = IsValid;
                                    break;
                                default:
                            }
                            if (!(IsSequence))
                                errorElement.push(element);
                        }
                    });
                }
            });
            return errorElement.length == 0;
        },
        IsRequiredValid = function (element) {
            var IsValid = true;
            //if (!($(element).is('select')) && $(element).attr('type') != 'file' && $(element).attr('type') != 'radio')
            //    trimInputValue($(element));
            if ($(element).attr('type') == 'radio') {
                var name = $(element).attr('name');
                if (!(globalFunctions.isValueValid($(`input[name="${name}"]:checked`).val()))) {
                    IsValid = false;
                    var msg = $(element).data('required');
                    common.showValidationMessage(element, 'Mandatory field', msg);
                }
            } else if ($(element).attr('comboname') == $(element).attr('id')) {
                common.removeValidationMessage($(element).next('span').find('input')[0]);
                if (!(globalFunctions.isValueValid($(element).next('span').find('input').val()))) {
                    IsValid = false;
                    var msg = $(element).data('required');
                    common.showValidationMessage($(element).next('span').find('input')[0], 'Mandatory field', msg);
                }
            } else if (!(globalFunctions.isValueValid($(element).val()))) {
                IsValid = false;
                var msg = $(element).data('required');
                common.showValidationMessage(element, 'Mandatory field', msg);
            }
            return IsValid;
        },
        IsValidLength = function (element) {
            var IsValid = true;
            var minLength = $(element).data('minlength');
            var maxLength = $(element).data('maxlength');
            var elementValue = $(element).val();

            if (globalFunctions.isValueValid(minLength) && globalFunctions.isValueValid(maxLength) && globalFunctions.isValueValid(elementValue)) {
                if (!(elementValue.length >= minLength) || !(elementValue.length <= maxLength)) {
                    IsValid = false;
                    var msg = $(element).data('msgminmaxlength');
                    common.showValidationMessage(element, `Length of value must be between ${minLength} and ${maxLength}`, msg);
                }
            }
            else if (globalFunctions.isValueValid(minLength) && globalFunctions.isValueValid(elementValue)) {
                if (!(elementValue.length >= minLength)) {
                    IsValid = false;
                    var msg = $(element).data('msgminmaxlength');
                    common.showValidationMessage(element, `Length of value must be greater than or equal to ${minLength}`, msg);
                }
            }
            else if (globalFunctions.isValueValid(maxLength) && globalFunctions.isValueValid(elementValue)) {
                if (!(elementValue.length <= maxLength)) {
                    IsValid = false;
                    var msg = $(element).data('msgminmaxlength');
                    common.showValidationMessage(element, `Length of value must be less than  or equal to ${maxLength}`, msg);
                }
            }
            return IsValid;
        },
        IsValidMinMaxValue = function (element) {
            var IsValid = true;
            var minValue = isNaN(parseFloat($(element).data('min'))) ? "" : parseFloat($(element).data('min'));
            var maxValue = isNaN(parseFloat($(element).data('max'))) ? "" : parseFloat($(element).data('max'));
            var elementValue = isNaN(parseFloat($(element).val())) ? 0 : parseFloat($(element).val());

            if (globalFunctions.isValueValid(minValue) && globalFunctions.isValueValid(maxValue)) {
                if (!(elementValue >= minValue) || !(elementValue <= maxValue)) {
                    IsValid = false;
                    var msg = $(element).data('msgminmaxvalue');
                    common.showValidationMessage(element, `Value must between ${minValue} and ${maxValue}`, msg);
                }
            }
            else if (globalFunctions.isValueValid(minValue)) {
                if (!(elementValue >= minValue)) {
                    IsValid = false;
                    var msg = $(element).data('msgminmaxvalue');
                    common.showValidationMessage(element, `Value must greater than or equal to ${minValue}`, msg);
                }
            }
            else if (globalFunctions.isValueValid(maxValue)) {
                if (!(elementValue <= maxValue)) {
                    IsValid = false;
                    var msg = $(element).data('msgminmaxvalue');
                    common.showValidationMessage(element, `Value must less than  or equal to ${maxValue}`, msg);
                }
            }

            return IsValid;
        },
        IsValidEmail = function (element) {
            var IsValid = true;
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var elementValue = $(element).val();

            if (!emailReg.test(elementValue)) {
                IsValid = false;
                var msg = $(element).data('msgminmaxvalue');
                common.showValidationMessage(element, `Invalid email address.`, msg);
            }

            return IsValid;
        },
        IsValidAlphabet = function (element) {
            var IsValid = true;
            var regex = /^[a-zA-Z ]*$/;

            if (!(globalFunctions.isValueValid($(element).val())))
                return IsValid;

            if (!(regex.test($(element).val()))) {
                var IsValid = false;
                common.showValidationMessage(element, 'Only characters are allowed.', $(element).data('alphabet'));
            }
            return IsValid;
        },
        IsValidNumeric = function (element) {
            var IsValid = true;
            var regex = /^\d+$/;

            if (!(globalFunctions.isValueValid($(element).val())))
                return IsValid;

            if (!(regex.test($(element).val()))) {
                var IsValid = false;
                common.showValidationMessage(element, 'Only numeric are allowed.', $(element).data('numeric'));
            }
            return IsValid;
        },
        IsValidInteger = function (element) {
            var IsValid = true;
            var regex = common.IntegerRegex;

            if (!(globalFunctions.isValueValid($(element).val())))
                return IsValid;

            if (!(regex.test($(element).val()))) {
                var IsValid = false;
                common.showValidationMessage(element, translatedResources.IntegerMsg, $(element).data('numeric'));
            }
            return IsValid;
        },
        IsValidUnsignedInt = function (element) {
            var IsValid = true;
            var regex = common.UnsignedintegerRegex;

            if (!(globalFunctions.isValueValid($(element).val())))
                return IsValid;

            if (!(regex.test($(element).val()))) {
                var IsValid = false;
                common.showValidationMessage(element, translatedResources.UnsignedIntMsg, $(element).data('numeric'));
            }
            return IsValid;
        },
        IsValidDecimal = function (element) {
            var IsValid = true;
            var regex = common.DecimalRegex;

            if (!(globalFunctions.isValueValid($(element).val())))
                return IsValid;

            if (!(regex.test($(element).val()))) {
                var IsValid = false;
                common.showValidationMessage(element, translatedResources.DecimalMsg, $(element).data('numeric'));
            }
            return IsValid;
        },
        IsValidAlphaNumeric = function (element) {
            var IsValid = true;
            var regex = common.AlfanumericRegex

            if (!(globalFunctions.isValueValid($(element).val())))
                return IsValid;

            if (!(regex.test($(element).val()))) {
                var IsValid = false;
                common.showValidationMessage(element, 'Only character or numeric are allowed.', $(element).data('alphanumeric'));
            }
            return IsValid;
        },
        IsValidDateFormat = function (element) {
            var IsValid = true;
            var regex = common.DatetimeRegex;

            if (!(globalFunctions.isValueValid($(element).val())))
                return IsValid;

            if (!(regex.test($(element).val()))) {
                var IsValid = false;
                common.showValidationMessage(element, 'Invalid date format', $(element).data('dateformat'));
            }
            return IsValid;
        },
        IsValidMinMaxRange = function (element) {
            var IsValid = true;
            var maxRangeId = $(`#${$(element).data('maxrangeid')}`).val();
            var minRangeId = $(`#${$(element).data('minrangeid')}`).val();
            var elementValue = parseFloat($(element).val());

            if (globalFunctions.isValueValid(maxRangeId)) {
                if (!(elementValue <= parseFloat(maxRangeId))) {
                    IsValid = false;
                    var msg = $(element).data('maxrangemsg');
                    common.showValidationMessage(element, `Value must be less than or equal to ${maxRangeId}`, msg);
                }
            } else if (globalFunctions.isValueValid(minRangeId)) {
                if (!(elementValue >= parseFloat(minRangeId))) {
                    IsValid = false;
                    var msg = $(element).data('maxrangemsg');
                    common.showValidationMessage(element, `Value must be greater than or equal to ${minRangeId}`, msg);
                }
            }
            return IsValid;
        },
        IsValidDateRange = function (element) {
            var IsValid = true;
            var firstElementId = $(element).data('datemax');
            var firstElement = `#${$(element).data('datemax')}`;

            if (globalFunctions.isValueValid(firstElement) && globalFunctions.isValueValid(element)) {
                if (globalFunctions.isValueValid($(firstElement).val()) && globalFunctions.isValueValid($(element).val())) {
                    IsValid = new Date($(firstElement).val().replaceAll('-', ' ')) <= new Date($(element).val().replaceAll('-', ' '));
                    if (!(IsValid)) {

                        var firstElementControlLabel =
                            globalFunctions.isValueValid($(`label[for="${firstElementId}"]`).text()) ? $(`label[for="${firstElementId}"]`).text() :
                                globalFunctions.isValueValid($(firstElementId).next('label').text()) ? $(firstElementId).next('label').text() :
                                    $(firstElementId).prev('label').text();

                        var msg = $(element).data('datemax-msg');
                        common.showValidationMessage(element, `Value must be greater than or equal to ${firstElementControlLabel}`, msg);
                    }
                }
            }
            return IsValid;
        },
        IsValidMultipleCheckBox = function () {
            var IsValid = true;
            var chkGroup = $('input[data-reqmulticheckbox]');
            var chkTotalLen = chkGroup.length;
            var checkedChkTotalLen = 0;
            common.removeValidationMessage(chkGroup.last());
            chkGroup.each(function (i, ele) {
                if ($(ele).is(':checked')) {
                    checkedChkTotalLen++;
                }
            });
            if (chkTotalLen > 0 && checkedChkTotalLen == 0) {
                IsValid = false;
                common.showValidationMessage(chkGroup.last(), 'Please select atleast one option', '');
            }
            return IsValid;
        },
        showValidationMessage = function (element, defaultMsg, msg) {
            if (globalFunctions.isValueValid(msg)) {
                $(element).parent().parent().append(`<div class=\"field-validation-error\"><span class=\"text-danger\">${msg}</span></div>`);
            }
            else {
                $(element).parent().parent().append(`<div class=\"field-validation-error\"><span class=\"text-danger\">${defaultMsg}</span></div>`);
            }
            //if ($(element).is('select')) {
            //    $(element).next('button').addClass('border-danger');
            //} else {
            //    $(element).addClass('border-danger');
            //}
        },
        removeValidationMessage = function (element) {
            if ($(element).parent().parent().find('.field-validation-error').length > 0) {
                $(element).parent().parent().find('.field-validation-error').remove();
                //if ($(element).is('select')) {
                //    $(element).next('button').removeClass('border-danger');
                //} else {
                //    $(element).removeClass('border-danger');
                //}
            }
        },
        CheckElementToSkipTrim = function (element) {
            !(($(element).is('select')) || ['file', 'radio', 'checkbox'].some(x => x == $(element).attr('type')))
        },
        ResetFormValidation = function (formid) {
            var elementList = [];
            common.attributes.forEach(x => {
                var inputElement = $("#" + formid).find(`input[data-${x}],textarea[data-${x}],select[data-${x}]`);
                inputElement.each((i, a) => {
                    if (elementList.some(e => e == a))
                        return;
                    elementList.push(a);
                });
            });
            $.each(elementList, function (index, element, t) {
                common.removeValidationMessage(element);
            });
        },
        ResetForm = function (formid) {
            var elementList = [];
            var inputElement = $("#" + formid).find(`input,textarea,select`);
            inputElement.each((i, a) => {
                if (elementList.some(e => e == a))
                    return;
                elementList.push(a);
            });
            $.each(elementList, function (index, element, t) {
                $(element).val('');
                if ($(element).is('select'))
                    $(element).selectpicker('refresh');
            });
        },
        validateAjaxForm = function (id) {
            return common.IsInvalidByFormOrDivId(id);
        },
        getDates = function (startDate, stopDate) {
            var dateArray = [];
            var currentDate = moment(startDate);
            var stopDate = moment(stopDate);
            while (currentDate <= stopDate) {
                dateArray.push(moment(currentDate).format('DD-MMM-YYYY'))
                currentDate = moment(currentDate).add(1, 'days');
            }
            return dateArray;
        },
        ConvertJsonEncodeDateStringToJsDate = function (val) {
            return new Date(parseInt(val.slice(6, -2)))
        },
        GetQueryStringValue = function (queryString) {
            if (window.location.href.slice(window.location.href.indexOf('?') + 1).split('&').some(a => a.split('=')[0] == queryString))
                return window.location.href.slice(window.location.href.indexOf('?') + 1).split('&').find(a => a.split('=')[0] == queryString).split('=')[1];
            return "";
        },
        onFailure = function (xhr) {
            globalFunctions.onFailure(undefined, xhr)
        },
        onSuccess = function (response) {
            globalFunctions.showMessage(response.NotificationType, response.Message);
        },
        saveCloudFileDate = function (cloudData) {
            var jsonDataArray = [];
            var fileControlId = $("input[name='FileUploadPopId']").val();
            var controlFilesID = $('#CloudFileList_' + fileControlId);
            if (globalFunctions.isValueValid(controlFilesID.val())) {
                var jsonData = JSON.parse(controlFilesID.val());
                jsonDataArray = Array.from(jsonData);
            }

            jsonDataArray = jsonDataArray.concat(cloudData)
            controlFilesID.val(JSON.stringify(jsonDataArray))
            common.previewFileIcons(jsonDataArray, fileControlId);
        },
        previewFileIcons = function previewFileIcons(data, fileControlId) {
            var previewIcon = fileInputControlSettings.previewFileIconSettings;
            var filePreviewDivid = $('#preview' + fileControlId);
            filePreviewDivid.find('li.newFile.cloud').remove();

            for (var i = 0; i < data.length; i++) {
                var fileIcon = previewIcon[data[i].FileExtension] ? previewIcon[data[i].FileExtension] : '<i class="fas fa-file"></i>';
                filePreviewDivid.append(`
                        <li class="position-relative rounded newFile cloud">
                            <a href="javascript:void(0);" href="${data[i].AttachmentPath}" target="_blank" class="p-3 d-block filetype " data-toggle="tooltip" data-placement="bottom" data-title="${data[i].FileName}">${fileIcon}</a>
                            <a href="javascript:void(0)"  onclick="common.removeFile('${data[i].GoogleDriveFileId}', '${fileControlId}')" class="remove position-absolute d-block bg-white"><i class="fas fa-times-circle"></i></a>
                        </li>
                        `);
            }
            filePreviewDivid.find('.filetype > i').addClass('fa-2x');


            common.setFilePreviewScrollBar(filePreviewDivid);

            var jsonDataFromAllInputs = [];
            $('input[id^="CloudFileList_"]').each(function (i, e) {
                var control = $(e);
                var jsonDataArray = [];
                if (globalFunctions.isValueValid(control.val())) {
                    var jsonData = JSON.parse(control.val());
                    jsonDataArray = Array.from(jsonData);
                }
                jsonDataFromAllInputs = jsonDataFromAllInputs.concat(jsonDataArray);
            });
            $("input[name='CloudFileList']").val(JSON.stringify(jsonDataFromAllInputs));
        },
        removeFile = function (fileId, fileControlId) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                var controlFilesID = $('#CloudFileList_' + fileControlId);

                var fileData = JSON.parse(controlFilesID.val());

                var jsonData = Array.from(fileData);
                jsonData = jsonData.filter(x => x.GoogleDriveFileId != fileId);

                controlFilesID.val(JSON.stringify(jsonData));

                common.previewFileIcons(jsonData, fileControlId);
            });
        },
        removeFileFromFileInput = function (fileInputFileId) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                $(`div[data-fileid="${fileInputFileId}"] .kv-file-remove`).click();
            });
        },
        setFilePreviewScrollBar = function (previewObject) {
            var customeScroll = previewObject.closest('.customScroll');
            if (previewObject.find('li').length > 8) {
                customeScroll.mCustomScrollbar({
                    axis: 'x',
                    setWidth: "80%",
                    autoExpandScrollbar: true,
                    autoHideScrollbar: true,
                })
            } else {
                customeScroll.mCustomScrollbar('destroy')
            }
        }
    return {
        init: init,
        IntegerRegex: IntegerRegex,
        UnsignedintegerRegex: UnsignedintegerRegex,
        DecimalRegex: DecimalRegex,
        AlfanumericRegex: AlfanumericRegex,
        DatetimeRegex: DatetimeRegex,
        attributes: attributes,
        bindSingleDropDownByParameter: bindSingleDropDownByParameter,
        bindMultipleDropDownByParameter: bindMultipleDropDownByParameter,
        bindSingleDropDownByParameterAsync: bindSingleDropDownByParameterAsync,
        postRequest: postRequest,
        notyDeleteConfirm: notyDeleteConfirm,
        notyConfirm: notyConfirm,
        bindDropDown: bindDropDown,
        IsInvalidByFormOrDivId: IsInvalidByFormOrDivId,
        IsRequiredValid: IsRequiredValid,
        showValidationMessage: showValidationMessage,
        IsValidLength: IsValidLength,
        IsValidMinMaxValue: IsValidMinMaxValue,
        IsValidEmail: IsValidEmail,
        IsValidNumeric: IsValidNumeric,
        IsValidAlphaNumeric: IsValidAlphaNumeric,
        IsValidAlphabet: IsValidAlphabet,
        IsValidInteger: IsValidInteger,
        IsValidUnsignedInt: IsValidUnsignedInt,
        IsValidDecimal: IsValidDecimal,
        IsValidMinMaxRange: IsValidMinMaxRange,
        IsValidDateRange: IsValidDateRange,
        IsValidMultipleCheckBox: IsValidMultipleCheckBox,
        IsValidDateFormat: IsValidDateFormat,
        removeValidationMessage: removeValidationMessage,
        CheckElementToSkipTrim: CheckElementToSkipTrim,
        ResetFormValidation: ResetFormValidation,
        ResetForm: ResetForm,
        validateAjaxForm: validateAjaxForm,
        getDates: getDates,
        ConvertJsonEncodeDateStringToJsDate: ConvertJsonEncodeDateStringToJsDate,
        GetQueryStringValue: GetQueryStringValue,
        onFailure: onFailure,
        onSuccess: onSuccess,
        previewFileIcons: previewFileIcons,
        removeFile: removeFile,
        saveCloudFileDate: saveCloudFileDate,
        removeFileFromFileInput: removeFileFromFileInput,
        setFilePreviewScrollBar: setFilePreviewScrollBar
    };
}();



$(document).on("keyup", "input[data-minlength],input[data-maxlength],textarea[data-minlength],textarea[data-maxlength]", function (event) {
    common.removeValidationMessage(this);
    common.IsValidLength(this);
    event.preventDefault();
});

$(document).on("keyup", "input[data-alphabet],textarea[data-alphabet]", function (event) {
    common.removeValidationMessage(this);
    common.IsValidAlphabet(this);
    event.preventDefault();
});

$(document).on("keyup", "input[data-numeric],textarea[data-numeric]", function (event) {
    common.removeValidationMessage(this);
    common.IsValidNumeric(this);
    event.preventDefault();
});

$(document).on("keyup", "input[data-alphanumeric],textarea[data-alphanumeric]", function (event) {
    common.removeValidationMessage(this);
    common.IsValidAlphaNumeric(this);
    event.preventDefault();
});

$(document).on("keyup", "input[data-required],textarea[data-required]", function (event) {
    common.removeValidationMessage(this);
    common.IsRequiredValid(this);
    event.preventDefault();
}).on("change", "select[data-required]", function (event) {
    common.removeValidationMessage(this);
    common.IsRequiredValid(this);
    event.preventDefault();
});
