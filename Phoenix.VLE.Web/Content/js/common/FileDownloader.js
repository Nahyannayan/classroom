﻿var resourceFileType = {
    GoogleDrive: 1,
    OneDrive: 2,
    SharePoint: 3
}, childAuthPopup;

var isAuthSuccess = false;

$.oauthpopup = function (options) {
    options.windowName = options.windowName || 'ConnectWithOAuth'; // should not include space for IE
    options.windowOptions = options.windowOptions || 'location=0,status=0,width=600,height=600';
    var that = this;
    that._oauthWindow = window.open(options.path, options.windowName, options.windowOptions);;
    that._oauthInterval = window.setInterval(function () {
        var isAuthenticationComplete = localStorage.getItem("external_auth_complete");
        if (globalFunctions.isValueValid(isAuthenticationComplete) && isAuthenticationComplete.toBoolean()) {
            $(options.source).trigger('click');
            that._oauthWindow.close();
            clearInterval(that._oauthInterval);
            localStorage.setItem("external_auth_complete", "false");
            $("#action-externalauth").remove();
        }
    }, 1000);

};

var FileDownloader = (function ($) {
    var instance;
    var _files = [];
    var downloadFileInterval;
    var imageFileExtensions = ["jpg", "png", "jpeg", "bmp", "svg"];
    function init() {
        function addFile(filePath, fileName) {
            _files.push({ filePath: filePath, fileName: fileName });
        }

        function clearFiles() {
            _files = [];
        }

        function downloadFile() {
            if (_files.length == 0) {
                clearInterval(downloadFileInterval);
                return;
            }
            const ext = _files[0].filePath.split(".").pop().toLowerCase();
            if (imageFileExtensions.indexOf(ext) !== -1) {
                var filePath = _files[0].filePath;
                _files.shift();
                window.open(filePath, "_blank");
            } else {
                var theAnchor = $('<a />')
                    .attr('href', _files[0].filePath)
                    .attr('download', _files[0].filePath)
                    .appendTo('body');
                theAnchor[0].click();
                _files.shift();
                theAnchor.remove();
            }

        }

        function startDownload() {
            downloadFileInterval = setInterval(downloadFile, 500);
        }

        function getDriveFolderFiles(folderId) {
            var source = $(".drive-child-folder_" + folderId);
            if ($(source).is(":empty")) {
                $.get("/shared/shared/GetFolderFilesList?folderId=" + folderId + "&resourceFileTypeId=" + $("#ResourceFileTypeId").val(), function (data) {
                    $(".drive-child-folder_" + folderId).html(data);
                }).fail(function () { globalFunctions.onFailure(); })
            }
            else source.toggle();
        }

        function createZipFile(source, url) {
            $.ajax({
                url: url,
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success)
                        $(document).trigger('ZIPCREATED', [data]);
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            });
        }

        getCloudFilesList = function (source) {
            var url = $(source).is(".google-drive-file") ? "/Shared/Shared/GetGoogleDriveFileList" : "/Shared/Shared/GetOneDriveFiles";
            var sourceClass = $(source).is(".google-drive-file") ? "google-drive-file" : "one-drive-file";
            $.ajax({
                url: url + "?resourceTypeId=" + $(source).data("resourcetypeid") + "&fileUploadModuleType=" + $(source).data("uploadmoduletype"),
                success: function (data) {
                    if (globalFunctions.isValueValid(data.RedirectUrl)) {
                        $("#action-externalauth").remove();
                        localStorage.setItem("external_auth_complete", "false");
                        var element = $("<a/>", {
                            id: 'action-externalauth',
                        });
                        element.data("redirectUri", data.RedirectUrl);
                        element.data("sourceclass", sourceClass);
                        element.appendTo($("body"));
                        element.trigger("click");
                    }
                    else {
                        $("#myModal .modal-title, #modal_heading").text($(source).data("title"));
                        $("#modal_Loader").html(data.PartialView);
                        $("#myModal").modal("show");
                        //added removeClass('modal-open') by Deepak Singh 11/August/2020 => Create issue after uploading files, screen get freez due to modal-open class in body tag
                        $('#myModal').on('hidden.bs.modal', function () {
                            $('body').removeClass('modal-open');
                        });
                        $("#drive-files-container").mCustomScrollbar({
                            setHeight: "260",
                            autoHideScrollbar: true,
                            autoExpandScrollbar: true
                        });
                    }
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            });
        }

        openOAuthPopup = function (redirectUri) {
            var authWindow = window.open(redirectUri, "", 'width=800, height=600');
            var pollTimer = window.setInterval(function () {
                if (authWindow.document.URL.indexOf(location.pathname) != -1) {
                    window.clearInterval(pollTimer);
                    authWindow.close();
                }
            }, 500);
        };

        return {
            addFile: addFile,
            startDownload: startDownload,
            createZipFile: createZipFile,
            getCloudFilesList: getCloudFilesList,
            getDriveFolderFiles: getDriveFolderFiles,
            openOAuthPopup: openOAuthPopup
        };
    }
    return {
        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    }
})(jQuery);

$(function () {
    $(document).on("click", "a.cloud-file-view.file-explorer,button.cloud-file-view.file-explorer", function () {
        var fileDownloader = FileDownloader.getInstance();
        fileDownloader.getCloudFilesList($(this));
    });

    var fileLoadType = $("#LoadResourceFileType").val();
    if (fileLoadType == resourceFileType.GoogleDrive) {
        $("a.google-drive-file.file-explorer").trigger("click");
    }
    else if (fileLoadType == resourceFileType.OneDrive) {
        $("a.one-drive-file.file-explorer").trigger("click");
    }


    $(document).off("click", "#saveCloudDriveFiles").on("click", "#saveCloudDriveFiles", function () {
        var moduleType = $(this).data("requesttype");
        // Added by Mohd Ameeq for Student Drive File Upload
        var isStudentCheck = $("#CheckStudentUri").val();
        var isStudentTaskCheck = $("#hdnCheckStudentTaskUri").val();
        if (isStudentCheck == "StudentDriveFile") {
            var saveUri = $("#CloudFilesaveStudentUri").length > 0 ? $("#CloudFilesaveStudentUri").val() : "";
        }
        else if (isStudentTaskCheck == "StudentTaskDriveFile") {
            var saveUri = $("#CloudFileSaveStudentTaskUri").length > 0 ? $("#CloudFileSaveStudentTaskUri").val() : "";
        }
        else {
            var saveUri = $("#CloudFilesaveUri").length > 0 ? $("#CloudFilesaveUri").val() : "/Files/Files/SaveCloudFiles";
            var IsAssignmentModule = $("#IsAssignmentFile").length > 0 ? $("#IsAssignmentFile").val() : "0";
            var taskfileSaveUri = $("#CloudFileToTaskSaveUri").length > 0 ? $("#CloudFileToTaskSaveUri").val() : "";
        }

        // Added by Ashwin Dubey for course file upload
        var IscourseFileUpload = $('input[name="CourseFileUpload"]').length > 0;

        var source = $("input:checkbox.chk-drive-file:checked");
        if (!source.length) {
            globalFunctions.showWarningMessage(translatedResources.selectFileMsg);
            return;
        }
        var fileIds = [];
        $.each(source, function (idx, file) {
            var file = $(file).data("file");
            if ($("#DriveRootId").length)
                file.ParentFolderId = $("#DriveRootId").val(); // Need to creating file in root location
            fileIds.push(file);
        });
        //Added by Deepak Singh for my group detail page
        var pageName = $("#PageName").val();
        if (pageName != undefined && pageName == 'GroupDetails') {
            $.post(saveUri, {
                fileIds: JSON.stringify(fileIds),
                parentFolderId: $("#CurrentFolderId").val(),
                sectionId: $("#CurrentSectionId").val(),
                moduleId: $("#CurrentModuleId").val(),
                resourceFileTypeId: $("#ResourceFileTypeId").val()
            }, function (data) {
                globalFunctions.showMessage(data.NotificationType, data.Message);
                if (data.Success) {
                    $("#myModal").modal("hide");
                    $(document).trigger("CLOUDFILESUPLOADED", [data]);
                    groupDetail.loadTopicActivitiesList();
                }
            }).fail(function () { globalFunctions.onFailure(); });
        }
        else if (pageName != undefined && pageName == 'AsyncLesson') {
            $.post(saveUri, {
                fileIds: JSON.stringify(fileIds),
                asyncLessonId: $("#AsyncLessonId").val(),
                parentFolderId: $("#FolderId").val(),
                resourceFileTypeId: $("#ResourceFileTypeId").val()
            }, function (data) {
                globalFunctions.showMessage(data.NotificationType, data.Message);
                if (data.Success) {
                    $("#myModal").modal('hide');
                    $(document).trigger("CLOUDFILESUPLOADED", [data]);
                    asyncLesson.loadResourcesActivityContent();
                }
            }).fail(function () { globalFunctions.onFailure(); });
        }
        else if (IscourseFileUpload) {
            var popupId = $("input[name='FileUploadPopId']").val();
            $.post('/Files/Files/SaveCourseCloudFiles', {
                fileIds: JSON.stringify(fileIds),
                parentFolderId: $("#ParentFolderId").val(),
                moduleId: $("#ModuleId" + popupId).val(),
                resourceFileTypeId: $("#ResourceFileTypeId").val()
            }, function (data) {
                globalFunctions.showMessage(data.NotificationType, data.Message);
                if (data.Success) {
                    $("#myModal").modal("hide");
                    $(document).trigger("CLOUDFILESUPLOADED", [data]);
                    common.saveCloudFileDate(JSON.parse(data.RelatedHtml));
                }
            }).fail(function () { globalFunctions.onFailure(); });
        }
        else if (moduleType == "1" && isStudentCheck != undefined && isStudentCheck == 'StudentDriveFile') {
            $.post(saveUri, {
                fileIds: JSON.stringify(fileIds),
                studentId: parseInt($("#hdnStudentId").val()),
                assignmentId: parseInt($("#hdnAssignmentId").val()),
                createdByOneDriveId: $("#CreatedByOneDriveId").val(),
                createdByGmailId: $("#createdByGmailId").val()
            }, function (data) {
                if (data.Success == true) {
                    $("#myModal").modal("hide");
                    $.ajax({
                        type: "GET",
                        url: "/Assignments/Assignment/GetUploadedStudentFile",
                        data: {
                            assignmentId: parseInt($("#hdnAssignmentId").val()),
                            studentId: parseInt($("#hdnStudentId").val())
                        },
                        beforeSend: function () {
                            $("#StudentAssignmentFiles").mCustomScrollbar("destroy"); //Destroy
                        },
                        success: function (response) {
                            $("#StudentAssignmentFiles").html("");
                            $("#StudentAssignmentFiles").append(response);
                            $('#divMarktoComplete').show();
                            $("#StudentAssignmentFiles").mCustomScrollbar({
                                autoExpandScrollbar: true,
                                autoHideScrollbar: true,
                                scrollbarPosition: "outside"
                            });
                        },
                        error: function (error) {
                            globalFunctions.onFailure();
                        }
                    });
                }
            }).fail(function () { globalFunctions.onFailure(); });
        }
        else if (moduleType == "2" && isStudentTaskCheck != undefined && isStudentTaskCheck == 'StudentTaskDriveFile') {
            $.post(saveUri, {
                fileIds: JSON.stringify(fileIds),
                studentId: parseInt($("#hdnTaskStudentId").val()),
                taskId: $("#TaskId").val(),
                createdByOneDriveId: $("#CreatedByOneDriveId").val(),
                createdByGmailId: $("#createdByGmailId").val()
            }, function (data) {
                if (data.Success == true) {
                    $("#myModal").modal("hide");
                    $.ajax({
                        type: "GET",
                        url: "/Assignments/Assignment/GetuploadedTaskFileDetails",
                        data: {
                            taskId: $("#TaskId").val(),
                            studentId: $("#hdnStudentId").val()
                        },
                        success: function (response) {
                            $("#StudentAssignmentFiles").html("");
                            $("#StudentAssignmentFiles").append(response);

                        },
                        error: function (error) {
                            globalFunctions.onFailure();
                        }
                    });
                }
            }).fail(function () { globalFunctions.onFailure(); });
        }
        else if (moduleType == "2") {
            $.post("/Assignments/Assignment/SaveTaskCloudFiles", {
                fileIds: JSON.stringify(fileIds),
                taskFilesDetails: globalFunctions.isValueValid($("#taskFilesDetails").val()) ? $("#taskFilesDetails").val() : ""
            }, function (data) {
                if (data != null) {
                    globalFunctions.showSuccessMessage(translatedResources.showSuccessMessage);
                    $("#taskFilesDetails").val(JSON.stringify(data.filenames));
                    $("#divTaskFileList").html(data.content);
                    $("#myModal").modal("hide");

                } else globalFunctions.showErrorMessage(translatedResources.technicalError);
            }).fail(function () { globalFunctions.onFailure(); });
        }
        else if (moduleType == "1") {
            $.post(saveUri, {
                fileIds: JSON.stringify(fileIds),
                lstAssignmentFiles: $("#lstAssignmentFiles").val()
            }, function (data) {
                if (globalFunctions.isValueValid(data)) {
                    $("#lstAssignmentFiles").val(JSON.stringify(data.filenames));
                    $("#attachmentslist,#attachmentslistOnDetailPage").html(data.content);
                    $("#myModal").modal("hide");

                }
            }).fail(function () { globalFunctions.onFailure(); });
        }
        else if (moduleType == "4") {

            $.post("/SchoolInfo/Blog/SaveBlogCloudFiles", {
                fileIds: JSON.stringify(fileIds),
                resourceFileTypeId: $("#ResourceFileTypeId").val(),
                blogCloudFilesDetails: globalFunctions.isValueValid($("#blogCloudFilesDetails").val()) ? $("#blogCloudFilesDetails").val() : ""
            }, function (data) {
                if (data != null) {
                    globalFunctions.showSuccessMessage("File Uploaded Successfuly");
                    $("#blogCloudFilesDetails").val(JSON.stringify(data.filenames));
                    $("#myModal").modal("hide");
                    $("#divCloudFileList").html(data.content).show();

                } else globalFunctions.showErrorMessage(data.technicalError);
            }).fail(function () { globalFunctions.onFailure(); });
        }
        else if (moduleType == "5") {
            $.post("/Observations/Observation/SaveSelectedCloudFiles", {
                fileIds: JSON.stringify(fileIds)
            }, function (data) {
                $("#fileList").html(data);
                $('#attachmentCount').html(translatedResources.AttachmentsCount.replace('{0}', $("#fileList .attachedFile").length));
                $("#myModal").modal("hide");
            }).fail(function () { globalFunctions.onFailure(); });
        }
        else {
            $.post(saveUri, {
                fileIds: JSON.stringify(fileIds),
                parentFolderId: $("#ParentFolderId").val(),
                sectionId: $("#SectionId").val(),
                moduleId: $("#ModuleId").val(),
                resourceFileTypeId: $("#ResourceFileTypeId").val()
            }, function (data) {
                globalFunctions.showMessage(data.NotificationType, data.Message);
                if (data.Success) {
                    $("#myModal").modal("hide");
                    $(document).trigger("CLOUDFILESUPLOADED", [data]);
                    folder.navigateToFolder();
                }
            }).fail(function () { globalFunctions.onFailure(); });
        }


    });

    $(document).on("keyup", "input.cloud-file-search", function () {
        var searchText = $(this).val().toLowerCase();
        $('#drive-files-container [data-search-text]').each(function () {
            if ($(this).find('.drive-child-folder_' + $(this).find('a.folder-meta').data('folder-id')).length == 0) {
                var currentItem = $(this).data("search-text").toLowerCase().indexOf(searchText.trim()) !== -1;
                $(this).toggle(currentItem);
                if (currentItem) {
                    $(this).closest('.driveFolder').attr('style', '');
                }
            }
        });
        //cloud folder files
        if ($('#drive-files-container .search-item:visible').length === 0) {
            $("#drive-files-container .nofolderdiv").show();
        }
        else {
            $("#drive-files-container .nofolderdiv").hide();
        }
    });

    $(document).on("click", "a.drive-folder-explorer", function () {
        var fileDownloader = FileDownloader.getInstance();
        fileDownloader.getDriveFolderFiles($(this).data("folderId"));
    });

    $(document).on("click", "#action-externalauth", function () {
        var elementClass = $(this).data("sourceclass");
        $.oauthpopup({
            path: $(this).data("redirectUri"),
            windowName: 'oauth',
            source: $("." + elementClass)
        });
    });
});