﻿//var notifcationModulesUrl = {
//    A: '/Assignments/Assignment/StudentHomeworkAddEdit?Id=',
//    G: '/files/files/groups?grId=',
//    O: '/Observations/Observation/GetObservationDetails?id='
//}
var sharepointToken;
var globalFunctions = function () {
    var showMessage = function (type, text) {
        if (type == undefined) text = $('#sessionExpiredMessage').val();
        if (type == undefined || type == '') type = 'error';
        if (text == undefined || text == '') text = translatedResources.technicalError;
        noty({ text: text, layout: "bottomRight", type: type });

        setTimeout(function () {
            $.noty.closeAll();
        }, 4000);
    },

        showSuccessNotification = function (text) {
            noty({
                text: text,
                layout: 'bottomRight',
                buttons: [
                    {
                        addClass: 'btn btn-success btn-md btn-clean shadow-none',
                        text: translatedResources.ok,
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]
            });
        },
        customScrollbar = function () {
            $(".resource-info .content").mCustomScrollbar({
                autoExpandScrollbar: true,
                autoHideScrollbar: true,
                scrollbarPosition: "outside"
            });
        },
        onUpdateSuccess = function () {
            showSuccessMessage(translatedResources.updateSuccessMsg);
        },
        showSuccessMessage = function (text) {
            noty({
                text: text,
                layout: "bottomRight",
                type: 'success'
            });

            setTimeout(function () {
                $.noty.closeAll();
            }, 4000);
        },

        showWarningMessage = function (text) {
            noty({
                text: text, layout: "bottomRight", type: 'warning'
            });

            setTimeout(function () {
                $.noty.closeAll();
            }, 4000);
        },



        showErrorMessage = function (text) {
            noty({
                text: text, layout: "bottomRight", type: 'error'
            });

            setTimeout(function () {
                $.noty.closeAll();
            }, 4000);
        },

        onFailure = function (x, xhr, status) {
            //debugger;
            if (xhr !== undefined && xhr.statusText === "canceled")
                return;
            if (xhr !== undefined && xhr.readyState == 0) {
                //globalFunctions.showErrorMessage("Session Expired.Please login again.");

                setTimeout(function () {
                    window.location.href = "/Account/Signout";
                }, 2000);
            }
            else if (xhr !== undefined && xhr.status != 200) {
                globalFunctions.showMessage("error", translatedResources.technicalError);
            }
            else {
                globalFunctions.showMessage("error", translatedResources.ajaxCallFailure);
            }
        },

        loadPopup = function (source, url, header, panelCssClass) {
            //pageLoadingFrame("show");            
            //debugger;
            $("#modal_Loader").html("");
            $("#modal_Loader").load(url, function () {
                //debugger;
                $("#modal_Loader").closest('.modal-dialog').removeClass('modal-lg modal-xl modal-md');

                $("#modal_Loader").closest('.modal-dialog').addClass(panelCssClass);
                $("#modal_heading").text(header);
                $('#myModal').modal({ backdrop: 'static' });

                $('#myModal').one('shown.bs.modal', function () {
                    source.trigger('modalLoaded');

                    //make input labels active if there is initial value available
                    $('textarea').each(function (element, i) {
                        //console.log($(this).val());
                        if (($(this).val() !== undefined && $(this).val().length > 0) || $(this).attr("placeholder") !== null) {
                            $(this).siblings('label').addClass('active');
                        }
                        else {
                            $(this).siblings('label').removeClass('active');
                        }
                    });

                    /* Trim spaces of inputs  */
                    trimInputValue = function (input) {
                        var trimmedValue = $(input).val().trim();
                        $(input).val(trimmedValue);
                    };

                    $(":input[type='text']").on('change', function () {
                        trimInputValue($(this));
                    });

                    $("textarea").on('change', function () {
                        trimInputValue($(this));
                    });
                });
                $('#myModal').one('hidden.bs.modal', function () {
                    source.trigger('modalClosed');
                });
                //setTimeout(function () {
                //    pageLoadingFrame("hide");
                //}, 100);
            });
        },


        substringMatcher = function (list, searchString) {

            var matches, substringRegex;
            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `searchString`
            substrRegex = new RegExp(searchString, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(list, function (i, item) {
                if (searchString == undefined || searchString == null || substrRegex.test(item)) {
                    matches.push(item);
                }
            });
            return matches;
        },
        substringBannedWords = function (list, searchString) {
            debugger;
            var matches;
            //an array that will be populated with substring matches
            matches = [];
            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            if (isValueValid(searchString)) {
                $.each(list, function (i, item) {
                    if (new RegExp("(^|[\\?\\.,\\s])" + escaperegex(item.trim().toLowerCase()) + "([\\?\\.,\\s]|$)").test(searchString.trim().toLowerCase())) {
                        matches.push(item);
                    }
                });
            }
            return matches;
        },
        escaperegex = function escapeRegExp(text) {
            return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&').trim().toLowerCase();

        },
        // finds any duplicate array elements using the fewest possible comparison
        arrayHasDuplicateElements = function (A) {
            var i, j, n;
            n = A.length;
            // to ensure the fewest possible comparisons
            for (i = 0; i < n; i++) {                        // outer loop uses each item i at 0 through n
                for (j = i + 1; j < n; j++) {              // inner loop only compares items j at i+1 to n
                    if (A[i].ItemId == A[j].ItemId) return true;
                }
            }
            return false;
        },

        notyDeleteConfirm = function (source, confirmMesage) {
            $(document).unbind('okToDelete');  // Make sure 'okToDelete' is not bound to any element in the DOM
            $(document).unbind('okToDelete', source);
            noty({
                text: confirmMesage,
                layout: 'topCenter',
                modal: true,
                //theme:'metroui',
                buttons: [
                    {
                        type: "button",
                        addClass: 'btn btn-outline-primary mr-5',
                        text: translatedResources.noCancel,
                        onClick: function ($noty) {
                            $noty.close();
                            //pageLoadingFrame('hide');
                        }
                    },

                    {
                        type: "button",
                        addClass: 'btn btn-primary',
                        text: translatedResources.yesDelete,
                        onClick: function ($noty) {
                            source.trigger('okToDelete');
                            $noty.close();
                        }
                    }
                ]

            });
        },

        notyConfirm = function (source, confirmMesage) {
            $(document).unbind('okClicked');  // Make sure 'okToDelete' is not bound to any element in the DOM
            $(document).unbind('okClicked', source);
            noty({
                text: confirmMesage,
                layout: 'topCenter',
                buttons: [
                    {
                        type: "button",
                        addClass: 'btn btn-outline-primary mr-5',
                        text: translatedResources.noCancel,
                        onClick: function ($noty) {
                            $noty.close();
                            //pageLoadingFrame('hide');
                            source.trigger('cancelClicked');
                        }
                    },
                    {
                        type: "button",
                        addClass: 'btn btn-primary',
                        text: translatedResources.yesConfirm,
                        onClick: function ($noty) {
                            source.trigger('okClicked');
                            $noty.close();
                        }
                    }
                ]

            });
        },

        notyRevert = function (source, confirmMesage) {
            $(document).unbind('okClicked');  // Make sure 'okToDelete' is not bound to any element in the DOM
            $(document).unbind('okClicked', source);
            noty({
                text: confirmMesage,
                layout: 'topCenter',
                buttons: [
                    {
                        type: "button",
                        addClass: 'btn btn-outline-primary mr-5',
                        text: translatedResources.noRevert,
                        onClick: function ($noty) {
                            $noty.close();
                            //pageLoadingFrame('hide');
                            source.trigger('cancelClicked');
                        }
                    },
                    {
                        type: "button",
                        addClass: 'btn btn-primary',
                        text: translatedResources.yesRevert,
                        onClick: function ($noty) {
                            source.trigger('okClicked');
                            $noty.close();
                        }
                    }
                ]

            });
        },

        deleteItem = function (sourceIdentifier, url, tableId, sourceObject, id, isDataTable) {
            sourceObject = $(sourceObject);
            isDataTable = $.fn.DataTable.isDataTable('#' + tableId);
            notyDeleteConfirm(sourceObject, translatedResources.deleteConfirm);
            $(document).bind('okToDelete', sourceObject, function (e) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: { 'id': id },
                    async: false,
                    success: function (result) {
                        if (result.Success == false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            sourceObject.closest('table').trigger('grid:itemdeleted');
                            if (isDataTable) {
                                var sourceTable = $('#' + tableId);
                                if (!sourceTable.length) sourceTable = sourceObject.closest('table');
                                var table = sourceTable.DataTable();
                                table.row(sourceObject.closest('tr')).remove().draw(false);
                            }
                            else
                                sourceObject.closest('tr').remove();
                        }

                        //setTimeout(function () {
                        //    debugger;
                        //    globalFunctions.convertImgToSvg();
                        //}, 1000);
                    },
                    error: function (msg) {
                        //pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            });
        },


        isDataValid = function (elementId) {
            return isValueValid($('#' + elementId).val());
        },

        isValueValid = function (value) {
            return value != undefined && value != '';
        },

        isNumber = function (value) {
            var regex = /^[-]*\d+(?:\.\d{1,2})?$/;
            return regex.test(value);
        },

        isNumberInRange = function (min, max, value) {
            if (isNumber(min) && isNumber(max) && isNumber(value)) {
                if ((min == 0 && max == 0) || value >= min && value <= max)
                    return true;
            }
            return false;
        },

        isArabicText = function (text) {
            var regEx = /[\u0600-\u06FF]/;
            return regEx.test(text);
        },

        repeatChars = function (c, repeatNum) {
            var result = '';
            for (var i = 1; i <= repeatNum; i++) {
                result += c;
            }
            return result;
        },

        mergeRows = function mergeRows(tableId, colIndices, mergeRowsById) {
            for (i = 0; i < colIndices.length; i++) {
                var nthChild = colIndices[i];
                $("#" + tableId + " tbody tr").each(function (index, val) {
                    var currentRow = $(this);
                    if ($(val).find('td:nth-child(' + nthChild + '):hidden').length == 0) {
                        var cnt = 0;
                        $("#" + tableId + " tbody tr").each(function (index, val) {
                            var result;
                            if (mergeRowsById)
                                result = $(currentRow).find('td:nth-child(' + nthChild + ')').attr('data-id') == $(this).find('td:nth-child(' + nthChild + ')').attr('data-id');
                            else
                                result = $.trim($(currentRow).find('td:nth-child(' + nthChild + ')').text()).toUpperCase() == $.trim($(this).find('td:nth-child(' + nthChild + ')').text()).toUpperCase();

                            if (result == true) {
                                cnt = cnt + 1;
                                if (cnt > 1) {
                                    $(val).find('td:nth-child(' + nthChild + ')').attr('data-merged', true);
                                }
                            }
                        });
                        if (cnt > 1) {
                            $(currentRow).find('td:nth-child(' + nthChild + ')').attr("rowspan", cnt);
                        }
                    }
                });
            }
            $("#" + tableId + " tbody tr").find('td[data-merged=true]').remove();//  hide();
        },


        isImageExists = function (url) {
            var img = new Image();
            img.src = url;
            return img.height != 0;
        },

        getSelectedText = function (element) {
            var options = element.find('option:selected');
            var text = '';
            $.each(options, function () {
                text += (text.length > 0 ? ', ' : '') + $(this).text();
            });
            return text;
        },

        isTraceExcluded = function (element) {
            return element.hasClass('exclude-trace');
        },

        enableCascadedDropdownList = function (sourceDropdownListId, destinationDropdownListId, actionUrl, triggerChangeEvent, enableBootStrapSelect, disableDefaultText) {
            $(document).on("change", "#" + sourceDropdownListId, function () {
                if ($('#' + destinationDropdownListId).data('attachFilterby') === true) {
                    $('#' + destinationDropdownListId).data('filterBy', $(this).val());
                }
                //console.log($("#" + sourceDropdownListId).has('option').length);
                console.log($(this).val());
                $.ajax({
                    url: actionUrl,
                    type: 'POST',
                    data: {
                        id: $("#" + sourceDropdownListId).has('option').length > 0 ? $(this).val() : -99
                    },
                    success: function (data) {
                        console.log("Entered");
                        console.log(data);
                        var SelectedOption = $("#" + destinationDropdownListId).val();
                        dropDownListLoadComplete(data, destinationDropdownListId, triggerChangeEvent, enableBootStrapSelect, disableDefaultText);
                        $("#" + destinationDropdownListId).val(SelectedOption);
                        $("#" + destinationDropdownListId).selectpicker({
                            dropdownAlignRight: true
                        });
                        var maxLength = 18;
                        $('.ddlStudentList > option').text(function (i, text) {
                            if (text.length > maxLength) {
                                return text.substr(0, maxLength) + '...';
                            }
                        });
                        $("#" + destinationDropdownListId).selectpicker('refresh');

                        if ($('span[data-valmsg-for="' + sourceDropdownListId + '"]').length > 0) {
                            $("#" + sourceDropdownListId).valid(); //Added by Deepak Singh on 2/Nov/2020: for validation of source dropdownlist
                        }

                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure(data, xhr, status);
                    }
                });
            });
        },

        dropDownListLoadComplete = function (data, destinationDropdownListId, triggerChangeEvent, enableBootStrapSelect, disableDefaultText) {
            var defaultText = null;
            if (disableDefaultText == undefined || disableDefaultText == null || disableDefaultText == false)
                defaultText = $('#' + destinationDropdownListId).attr('placeholder') == undefined ? translatedResources.selectedText : $('#' + destinationDropdownListId).attr('placeholder');

            formElements.feEnableSelect(destinationDropdownListId, enableBootStrapSelect);
            formElements.feReloadSelect(destinationDropdownListId, data, defaultText, enableBootStrapSelect, null);

            if (triggerChangeEvent)
                $("#" + destinationDropdownListId).trigger('change');
        },
        getParameterValueFromQueryString = function (param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
            return '';
        },
        loadNotificationPopup = function () {
            $("#modal_Loader").load('/Home/LoadNotificationPopup', function () {
                $("#modal_heading").html(translatedResources.Notifications);
                $('#modal_close').addClass('d-block');
                $('#myModal').modal({ backdrop: 'static' });
                $('#myModal').one('shown.bs.modal', function () {
                });
            });
            //$.ajax({
            //    type: 'GET',
            //    url: '/Home/IsNotificationExist',
            //    success: function (result) {
            //        if (result) {
            //            $("#modal_Loader").load('/Home/LoadNotificationPopup', function () {
            //                $("#modal_heading").html(translatedResources.Notifications);


            //                $('#myModal').modal({ backdrop: 'static' });
            //                $('#myModal').one('shown.bs.modal', function () {
            //                });
            //            });
            //        }
            //    },
            //    error: function (msg) {
            //        globalFunctions.onFailure();
            //    }
            //});
        },
        loadAllNotifications = function () {
            $.ajax({
                type: 'GET',
                url: '/Home/AllNotifications',
                success: function (result) {
                    $("#allNotifications").html(result);
                    $('.tooltip').tooltip('hide');
                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });
        },
        ClosePopUp = function () {
            $("#myModal").modal("hide");
        },
        loadStuddentTermsAndConditionPopUp = function () {
            $("#modal_Loader").load('/Home/LoadStuddentTermsAndConditionPopUp', function () {
                $("#modal_heading").html(translatedResources.StudentPermissionInstruction);
                $('#myModal').modal({ backdrop: 'static' });
                $('#modal_close').addClass('d-none');
                $('#myModal').one('shown.bs.modal', function () {
                });
            });
        },
        loadTermsAndConditionPopUp = function () {
            $("#modal_Loader").load('/Home/LoadTermsAndConditionPopUp', function () {
                $("#modal_heading").html(translatedResources.PermissionInstruction);
                $('#myModal').modal({ backdrop: 'static' });
                $('#modal_close').addClass('d-none');
                $('#myModal').one('shown.bs.modal', function () {
                });
            });
        },
        assignPermission = function () {
            if ($('input.custom-control-input:checked').length == 2) {
                $.ajax({
                    type: 'GET',
                    url: '/Home/AssignPermission',
                    success: function (result) {
                        $("#myModal").modal("hide");
                    },
                    error: function (msg) {
                        globalFunctions.onFailure();
                    }
                });
            } else {
                $('#ErrorPopup').modal('show');
            }
        },
        openNotification = function (notifiactionType, sourceId, IsMarked, notificationId, resourceId, studentId) {
            if (!IsMarked) {
                $.ajax({
                    type: 'POST',
                    url: '/Shared/Shared/UpdateNotificationMarkAsRead',
                    data: { notificationId: notificationId, IsMarked: true },
                    success: function (result) {
                        if (result.RelatedHtml != "") {
                            $('#dvnotificationdrawer').html('');
                            $('#dvnotificationdrawer').html(result.RelatedHtml);
                            //$('.right-drawer:not(#leaderBoard)').toggleClass('open');
                            //$('.right-drawer:not(#leaderBoard)').animate({
                            //    right: '0'
                            //});
                        }

                        // Check first if the resource is deleted or not
                        var isTeacherNotification = result.Identifier == "staff";
                        $.ajax({
                            type: 'POST',
                            url: '/Home/CheckIfTheResourceDeleted',
                            data: {
                                'sourceId': sourceId,
                                'notificationType': notifiactionType,
                                'isTeacherNotification': isTeacherNotification
                            },
                            success: function (result) {
                                if (result.Success === true) {
                                    if (notifiactionType === "MGRP" || notifiactionType === "CHAT") {
                                        globalFunctions.showWarningMessage(translatedResources.GroupResourceDeleteMsg);
                                    }
                                    else {
                                        globalFunctions.showWarningMessage(translatedResources.ResourceDeleteMsg);
                                    }
                                }
                                else {
                                    if (notifiactionType === "ASN") {
                                        window.location.href = isTeacherNotification ? "/Assignments/Assignment/TeacherStudentAssignment?id=" + sourceId : "/Assignments/Assignment/StudentHomeworkAddEdit?Id=" + sourceId;
                                    }
                                    if (notifiactionType === "MGRP") {
                                        window.location.href = "/schoolstructure/schoolgroups/groupdetails?grpId=" + sourceId;
                                    }
                                    if (notifiactionType === "TSK") {
                                        window.location.href = isTeacherNotification ? "/Assignments/Assignment/GetAssignmentTaskDetails?taskId=" + sourceId + "&studentId=" + studentId : "/Assignments/Assignment/GetStudentTaskDetails?taskId=" + sourceId + "&studentId=" + studentId;
                                    }
                                    if (notifiactionType === "CHAT") {
                                        window.location.href = "/schoolstructure/schoolgroups/groupdetails?tb=1&grpId=" + sourceId;
                                    }
                                    if (notifiactionType === "OBS") {
                                        window.location.href = "/observations/observation";
                                    }
                                    if (notifiactionType === 'LOC') {
                                        window.location.href = "/schoolstructure/locker";
                                    }
                                    //if (notifiactionType == 'QUZ') {
                                    //    window.location.href = "Quiz/Quiz/ViewQuiz?id=" + sourceId
                                    //}
                                    //if (notifiactionType == 'MYP') {
                                    //    window.location.href = "Planner/MyPlanner/InitAddEditPlannerForm?id=" + sourceId + "&sd=" + "GBL";
                                    //}
                                    if (notifiactionType == 'MYP') {
                                        window.location.href = "/Planner/MyPlanner";
                                    }
                                    if (notifiactionType == 'CEB') {
                                        window.location.href = isTeacherNotification ? "/document/certificate" : "/studentinformation/studentinformation/PortfolioHome?userId=" + sourceId
                                    }
                                    if (notifiactionType == 'CAN') {
                                        localStorage.removeItem("cID");
                                        window.location.href = "/SchoolStructure/schoolspaces?tb=1&cID=" + sourceId;
                                    }
                                    if (notifiactionType == 'RES') {
                                        window.location.href = "/SchoolStructure/SchoolSpaces?tb=2";
                                    }
                                    if (notifiactionType == 'GAL') {
                                        window.location.href = "/SchoolStructure/SchoolSpaces?tb=4";
                                    }
                                    if (notifiactionType == 'EVT') {
                                        window.location.href = "/SchoolStructure/SchoolSpaces?tb=3";
                                    }
                                    if (notifiactionType == 'THB') {
                                        window.location.href = "/suggestionbox/suggestions";
                                    }
                                    if (notifiactionType == 'KNH') {
                                        window.location.href = "/contentlibrary/resources";
                                    }
                                    if (notifiactionType == 'BCR') {
                                        window.location.href = "/behaviour/behaviour/BehaviorCertificate?sid=" + sourceId
                                    }
                                }
                            },
                            error: function (data, xhr, status) {
                                globalFunctions.onFailure();
                            }

                        });

                    },
                    error: function (msg) {
                        globalFunctions.onFailure();
                    }
                });
            }
            if (IsMarked) {
                $("#" + resourceId).css('display', 'none !important');
                var notificationCount = $('.arabictxt:first').text();
                $('.notification-count .arabictxt').html(notificationCount - 1);
                $.ajax({
                    type: 'POST',
                    url: '/Shared/Shared/UpdateNotificationMarkAsRead',
                    data: { notificationId: notificationId, IsMarked: IsMarked },
                    success: function (result) {
                        //$("#notificationCount").html(result);
                        if (result.RelatedHtml != "") {
                            $('#dvnotificationdrawer').html('');
                            $('#dvnotificationdrawer').html(result.RelatedHtml);
                            //$('.right-drawer:not(#leaderBoard)').toggleClass('open');
                            //$('.right-drawer:not(#leaderBoard)').animate({
                            //    right: '0'
                            //});
                        }
                        $('.tooltip').tooltip('hide');
                        globalFunctions.showSuccessMessage(translatedResources.updateSuccessMsg);
                        loadAllNotifications();
                    },
                    error: function (msg) {
                        globalFunctions.onFailure();
                    }
                });
            }
        },
        readAllNotification = function () {
            $.ajax({
                type: 'POST',
                url: '/Shared/Shared/UpdateAllNotification',
                success: function (result) {
                    globalFunctions.showSuccessMessage(translatedResources.updateSuccessMsg);
                    $('#dvnotificationdrawer').html('');
                    $('#dvnotificationdrawer').html(result.DrawerHTML);
                    //$('.right-drawer:not(#leaderBoard)').toggleClass('open');
                    //$('.right-drawer:not(#leaderBoard)').animate({
                    //    right: '0'
                    //});

                    $('#notificationRightDrawer span.notification-count').addClass("d-none");

                    loadAllNotifications();

                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });
        },
        notificationMarkAsRead = function (notificationId, resourceId, IsMarked) {
            debugger
            if (IsMarked) {
                $("#" + resourceId).remove();
                var notificationCount = $('.arabictxt:first').text();
                var count = parseInt(notificationCount) - 1;
                $('.notification-count .arabictxt').text(count);
                if (count > 0) {
                    $('#notificationRightDrawer span.notification-count').removeClass("d-none");
                }
                else {
                    $('#notificationRightDrawer span.notification-count').addClass("d-none");
                }

            } else {
                $("#" + resourceId).css('display', 'none !important');
                var notificationCount = $('.arabictxt:first').text();
                var count = parseInt(notificationCount) + 1;
                $('.notification-count .arabictxt').text(count);
                if (count > 0) {
                    $('#notificationRightDrawer span.notification-count').removeClass("d-none");
                }
                else {
                    $('#notificationRightDrawer span.notification-count').addClass("d-none");
                }
            }
            $.ajax({
                type: 'POST',
                url: '/Shared/Shared/UpdateNotificationMarkAsRead',
                data: { notificationId: notificationId, IsMarked: IsMarked },
                success: function (result) {
                    if (result.RelatedHtml != "") {
                        $('#dvnotificationdrawer').html('');
                        $('#dvnotificationdrawer').html(result.RelatedHtml);
                    }
                    globalFunctions.showSuccessMessage(translatedResources.updateSuccessMsg);
                    loadAllNotifications();
                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });

        },
        allNotification = function () {
            $("#IsSeenNotitifcationPoup").val(false);
            window.location.href = "/Home/Notifications";
        },
        setCookie = function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        },
        getCookie = function (cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        },
        validateBannedWords = function (formId) {
            var wordsToBeBanned = [];
            var jsonString = JSON.stringify(_bannedWords);
            $("#" + formId + " :input[type=text],textarea").each(function () {
                var input = $(this);
                var bannedWord = substringBannedWords(JSON.parse(jsonString), input.val());

                if (bannedWord.length !== 0)
                    wordsToBeBanned.push(bannedWord);
            });

            if (wordsToBeBanned.length !== 0) {
                var distinctBannedWords = [];
                $.each([].concat.apply([], wordsToBeBanned), function (i, el) {
                    if ($.inArray(el, distinctBannedWords) === -1) distinctBannedWords.push(el);
                });
                globalFunctions.showWarningMessage(distinctBannedWords + ' ' + translatedResources.validateBannedWordMessage);
                return false;
            }
            else {
                return true;
            }
        },
        validateBannedWordsAndSubmitForm = function (formId) {
            var isValidated = validateBannedWords(formId);
            if (isValidated) {
                $('form#' + formId).submit();
            }
        },
        isCurrentDate = function (sourceDate) {
            return moment(sourceDate).format('YYYYMMDD') == moment().format('YYYYMMDD');
        },
        validateDocumentType = function (file, fileFormat) {
            var formats = fileFormat.split(',');
            var allowedFileTypes;
            formats.forEach(function (item, index) {
                if (item === "I")
                    $.merge(allowedFileTypes, fileUploadSettings.allowedImageExtensions);
                if (item === "V")
                    $.merge(allowedFileTypes, fileUploadSettings.allowedVideoExtensions);
                if (item === "F")
                    $.merge(allowedFileTypes, fileUploadSettings.allowedFileExtensions);
            });
            var fileExt = file.name.split('.').pop().toLowerCase();
            if ($.inArray(fileExt, allowedFileTypes) === -1)
                return false;
        },
        showErrorLogger = function (source) {
            var title = $(source).data("formtype") == "I" ? translatedResources.issues : translatedResources.feedback;
            globalFunctions.loadPopup(source, '/Home/SendErrorLog?type=' + $(source).data("formtype"), title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $("#myModal .modal-content").attr("id", "reportErrorModal");
            })
            $('#formModified').val(true);
        },
        validateBannedWordsFormData = function (formId) {
            var wordsToBeBanned = [];
            var jsonString = JSON.stringify(_bannedWords);
            var currentInstance;
            if (typeof (CKEDITOR) !== 'undefined') {
                for (var i in CKEDITOR.instances) {
                    currentInstance = i;
                    var oEditor = CKEDITOR.instances[currentInstance].getData();
                    if (isValueValid(oEditor)) {
                        var bannedWordCkEditor = substringBannedWords(JSON.parse(jsonString), oEditor.replace(/<[^>]*>/gi, ''));
                        if (bannedWordCkEditor.length !== 0) {
                            wordsToBeBanned.push(bannedWordCkEditor);
                        }
                    }
                }
            }

            $("#" + formId + " :input[type=text],textarea").each(function () {
                var input = $(this);
                var bannedWord = substringBannedWords(JSON.parse(jsonString), input.val());
                if (bannedWord.length !== 0)
                    wordsToBeBanned.push(bannedWord);
            });

            if (wordsToBeBanned.length !== 0) {
                var distinctBannedWords = [];
                $.each([].concat.apply([], wordsToBeBanned), function (i, el) {
                    if ($.inArray(el, distinctBannedWords) === -1) distinctBannedWords.push(el);
                });
                globalFunctions.showWarningMessage(distinctBannedWords + ' ' + translatedResources.validateBannedWordMessage);
                return false;
            }
            else {
                return true;
            }
        },
        onSubmitErrorLog = function (data) {
            console.log(data);
            if (data.Success) {
                globalFunctions.showMessage("success", "Error log submitted successfully");
                $("#myModal").modal('hide');
            }
            else if (data.NotificationType == 'warning') {
                globalFunctions.showMessage(data.NotificationType, data.Message);
            }
            else {
                globalFunctions.showMessage("error", "Error occurred while submiting log");
            }


        },
        downloadFile = function (filePath) {
            window.open("/shared/shared/DownloadFile?filePath=" + filePath, "_blank");
        },
        downloadSharepointFile = function (filename, filepath, SchoolCode, isParent, ResourceFileTypeId) {
            var str2 = "gemsedu.sharepoint";
            if (filepath.indexOf(str2) === -1) {
                var url = filepath;
                var link = document.createElement("a");
                link.setAttribute("href", url);
                link.setAttribute("target", "_blank");
                link.style = "visibility:hidden";
                document.body.appendChild(link);
                link.click();
                return;
            }

            if (ResourceFileTypeId != 3) {
                var url = filepath;
                var link = document.createElement("a");
                link.setAttribute("href", url);
                link.setAttribute("target", "_blank");
                link.style = "visibility:hidden";
                document.body.appendChild(link);
                link.click();
            }
            else {
                var token = $("#SharepointToken").val();
                var serverRelativeUrl = filepath.split("https://gemsedu.sharepoint.com")[1];
                if (isParent == 'true') {
                    const request = new XMLHttpRequest();
                    request.responseType = 'arraybuffer';
                    request.open('GET', "https://gemsedu.sharepoint.com/sites/App-" + SchoolCode + "/ClassroomSync/_api/Web/GetFileByServerRelativePath(decodedurl='" + serverRelativeUrl + "')/$value");
                    request.setRequestHeader("binarystringresponsebody", "true");
                    request.setRequestHeader("Authorization", "Bearer " + token);
                    request.onload = () => {
                        if (request.status == 200) {
                            var blob = null;
                            if (typeof Blob !== 'undefined') {
                                // Web browser: Return blob from ArrayBuffer
                                blob = new Blob([request.response], { type: "application/octet-stream" });
                            }
                            else {
                                blob = Buffer.from(blob, 'binary');
                            }
                            var url = (window.URL || window.webkitURL).createObjectURL(blob);
                            var link = document.createElement("a");
                            link.setAttribute("href", url);

                            link.setAttribute("download", filename);
                            link.style = "visibility:hidden";
                            document.body.appendChild(link);
                            link.click();
                            setTimeout(function () { document.body.removeChild(link); }, 500);
                        } else {
                            console.error('Error!');
                        }
                    };
                    request.onerror = function () {
                        console.error('Error!');
                    };

                    request.send();
                }
                else {
                    //debugger;
                    var url = filepath;
                    var doc = ['xls', 'xlsx', 'doc', 'docx', 'ppt', 'pptx', 'csv'];
                    var fileType = filename != undefined && filename.length > 0 ? filename.substring(filename.lastIndexOf('.') + 1) : undefined;
                    if (fileType !== undefined && doc.includes(fileType.toLowerCase())) {
                        url = url + "?web=1";
                    }

                    var link = document.createElement("a");
                    link.setAttribute("href", url);

                    link.setAttribute("target", "_blank");
                    link.style = "visibility:hidden";
                    document.body.appendChild(link);
                    link.click();
                }
            }

        };
    getmonthname = function (monthid) {
        var month = new Array();
        month[0] = "Jan";
        month[1] = "Feb";
        month[2] = "Mar";
        month[3] = "Apr";
        month[4] = "May";
        month[5] = "Jun";
        month[6] = "Jul";
        month[7] = "Aug";
        month[8] = "Sep";
        month[9] = "Oct";
        month[10] = "Nov";
        month[11] = "Dec";

        return n = month[monthid];
    },
        setActualDateValue = function (dt, type) {
            //based on the assumption that datepicker has display format 'DD-MMM-YYYY' e.g. 01-SEP-2019

            if (dt != null && dt != undefined) {
                var picker = $(dt).datetimepicker();
                var month = getmonthname(picker.val().substring(3, 6));
                var year = picker.val().substring(7);
                var day = trimStartingZero(picker.val().substring(0, 2));

                var time = "00:00:00 AM"; //default starting time
                if (type != null && type != undefined && type == 1) { //ending time (for ToDate)
                    time = "23:59:59 PM";
                }

                var cDateValue = month + '/' + day + '/' + year + " " + time;
                picker.attr('value', cDateValue);
            }
        },
        trimStartingZero = function (val) {
            var retVal = val;
            if (val.indexOf('0') == 0) {
                retVal = val.substring(1, 2); //trim starting zero
            }
            return retVal;
        },
        convertImgToSvg = function () {
            // apply color to svg files
            $('img.svg').each(function () {
                var $img = $(this);
                var imgID = $img.attr('id');
                var imgClass = $img.attr('class');
                var imgURL = $img.attr('src');

                $.get(imgURL, function (data) {
                    // Get the SVG tag, ignore the rest
                    var $svg = $(data).find('svg');

                    // Add replaced image's ID to the new SVG
                    if (typeof imgID !== 'undefined') {
                        $svg = $svg.attr('id', imgID);
                    }
                    // Add replaced image's classes to the new SVG
                    if (typeof imgClass !== 'undefined') {
                        $svg = $svg.attr('class', imgClass + ' replaced-svg');
                    }

                    // Remove any invalid XML tags as per http://validator.w3.org
                    $svg = $svg.removeAttr('xmlns:a');

                    // Check if the viewport is set, else we gonna set it if we can.
                    if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                        $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
                    }

                    // Replace image with new SVG
                    $img.replaceWith($svg);

                }, 'xml');

            });
        },
        getFormattedDate = function (date) {
            let year = date.getFullYear();
            let month = date.getMonth().toString();
            let day = date.getDate().toString().padStart(2, '0');
            return day + '-' + globalFunctions.getmonthname(month) + '-' + year;
        },
        back = function (event) {
            event.preventDefault();
            history.back();
        },
        enableCheckboxCascade = function (allSelector, checkboxSelectors) {
            var selectAllCheckbox = $(allSelector), targetCheckboxes = $(checkboxSelectors);
            selectAllCheckbox.off("change");
            targetCheckboxes.off("change");
            selectAllCheckbox.on("change", function () {
                targetCheckboxes.prop("checked", selectAllCheckbox.is(":checked"));
            });
            targetCheckboxes.on("change", function () {
                if ($(checkboxSelectors + ":checked").length == 0)
                    selectAllCheckbox.prop("checked", false);
                selectAllCheckbox.prop("checked", $(checkboxSelectors + ":checked").length == targetCheckboxes.length);
            });
        },
        notyDltConfirm = function (confirmMesage, onConfirmMethod, onCancelMethod) {
            noty({
                text: confirmMesage,
                layout: 'topCenter',
                buttons: [
                    {
                        type: "button",
                        addClass: 'btn btn-outline-primary mr-5',
                        text: translatedResources.noCancel,
                        onClick: function ($noty) {
                            $noty.close();
                            if (onCancelMethod)
                                onCancelMethod();
                            //pageLoadingFrame('hide');
                        }
                    },
                    {
                        type: "button",
                        addClass: 'btn btn-primary',
                        text: translatedResources.yesConfirm,
                        onClick: function ($noty) {
                            $noty.close();
                            if (onConfirmMethod)
                                onConfirmMethod();
                        }
                    }
                ]

            });
        },
        UpdateStatusToDelete = function (id, filepath) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                $.get('/Course/Course/DeleteAttachment', { id: id, filepath: filepath }).then(function (response) {
                    if (response) {
                        var parent = $('#listIt' + response).parent();
                        $('#listIt' + response).remove();
                        globalFunctions.showSuccessMessage(translatedResources.deleteRecord);
                        $(parent).lightGallery({ selector: '.galleryImg', share: false });
                        common.setFilePreviewScrollBar(parent);
                    } else {
                        globalFunctions.showErrorMessage(translatedResources.technicalError);
                    }

                });
            });
        },
        isJson = function (str) {
            try {
                JSON.parse(str);
                return true;
            } catch (e) {
                return false;
            }
        },
        getUniqueString = function () {
            var d = new Date();
            return d.getUTCFullYear() + "" + d.getUTCMonth() + "" + d.getUTCDate() + "" + d.getUTCMinutes() + "" + d.getUTCSeconds() + "" + d.getUTCMilliseconds();
        },
        // Get the local file as an array buffer.
        getFileBuffer = function (file) {
            var deferred = jQuery.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }
            reader.readAsArrayBuffer(file);
            return deferred.promise();
        },
        getFile = function (fileName) {
            var fileInput = document.getElementById("postedFile");
            var files = fileInput.files;
            var file = null;
            for (var i = 0; i < files.length; i++) {
                var a = files[i];
                if (a.name = fileName) {
                    file = a;
                }
            }
            if (file != null) {
                var blob = new Blob([file]);
                var buffer = getFileBuffer(blob);
                buffer.then(function (resp) {
                    debugger;
                    return resp;
                })
            }
        },
        convertBase64ToImageFile = function (dataurlBase64, filename) {
            // To convert base64 into input file type (file content) to post in HttpPostedFileBase in C#
            if (dataurlBase64 != '' && dataurlBase64 != undefined) {
                var arr = dataurlBase64.split(','),
                    mime = arr[0].match(/:(.*?);/)[1],
                    bstr = atob(arr[1]),
                    n = bstr.length,
                    u8arr = new Uint8Array(n);

                while (n--) {
                    u8arr[n] = bstr.charCodeAt(n);
                }

                return new File([u8arr], filename, { type: mime });
            }
            else {
                return null;
            }
        },
        templateCreator = function (tmpl, vals) {
            var rgxp, repr;

            // default to doing no harm
            tmpl = tmpl || '';
            vals = vals || {};

            // regular expression for matching our placeholders; e.g., #{my-cLaSs_name77}
            rgxp = /#\{([^{}]*)}/g;

            // function to making replacements
            repr = function (str, match) {
                return typeof vals[match] === 'string' || typeof vals[match] === 'number' ? vals[match] : str;
            };

            return tmpl.replace(rgxp, repr);
        },
        toSystemReadableTime = function toSystemReadableTime(text) {
            return text.replaceAll('٠', '0')
                .replaceAll('١', '1')
                .replaceAll('٢', '2')
                .replaceAll('٣', '3')
                .replaceAll('٤', '4')
                .replaceAll('٥', '5')
                .replaceAll('٦', '6')
                .replaceAll('٧', '7')
                .replaceAll('٨', '8')
                .replaceAll('٩', '9')
                .replaceAll("م", "PM")
                .replaceAll("ص", "AM");

        },
        toSystemReadableDate = function toSystemReadableDate(text) {
            return text.replaceAll('٠', '0')
                .replaceAll('١', '1')
                .replaceAll('٢', '2')
                .replaceAll('٣', '3')
                .replaceAll('٤', '4')
                .replaceAll('٥', '5')
                .replaceAll('٦', '6')
                .replaceAll('٧', '7')
                .replaceAll('٨', '8')
                .replaceAll('٩', '9')
                .replaceAll("يناير", "Jan")
                .replaceAll("فبراير", "Feb")
                .replaceAll("مارس", "Mar")
                .replaceAll("أبريل", "Apr")
                .replaceAll("مايو", "May")
                .replaceAll("يونيو", "Jun")
                .replaceAll("يوليو", "Jul")
                .replaceAll("أغسطس", "Aug")
                .replaceAll("سبتمبر", "Sep")
                .replaceAll("أكتوبر", "Oct")
                .replaceAll("نوفمبر", "Nov")
                .replaceAll("ديسمبر", "Dec");

        };

    return {
        showMessage: showMessage,
        showSuccessNotification: showSuccessNotification,
        onUpdateSuccess: onUpdateSuccess,
        showSuccessMessage: showSuccessMessage,
        showWarningMessage: showWarningMessage,
        showErrorMessage: showErrorMessage,
        onFailure: onFailure,
        loadPopup: loadPopup,
        substringMatcher: substringMatcher,
        arrayHasDuplicateElements: arrayHasDuplicateElements,
        notyConfirm: notyConfirm,
        notyRevert: notyRevert,
        notyDeleteConfirm: notyDeleteConfirm,
        deleteItem: deleteItem,
        isDataValid: isDataValid,
        isValueValid: isValueValid,
        isNumberInRange: isNumberInRange,
        isNumber: isNumber,
        mergeRows: mergeRows,
        getSelectedText: getSelectedText,
        isTraceExcluded: isTraceExcluded,
        getParameterValueFromQueryString: getParameterValueFromQueryString,
        isCurrentDate: isCurrentDate,
        enableCascadedDropdownList: enableCascadedDropdownList,
        setCookie: setCookie,
        allNotification: allNotification,
        getCookie: getCookie,
        openNotification: openNotification,
        notificationMarkAsRead: notificationMarkAsRead,
        loadAllNotifications: loadAllNotifications,
        loadTermsAndConditionPopUp: loadTermsAndConditionPopUp,
        assignPermission: assignPermission,
        validateDocumentType: validateDocumentType,
        loadNotificationPopup: loadNotificationPopup,
        ClosePopUp: ClosePopUp,
        readAllNotification: readAllNotification,
        loadStuddentTermsAndConditionPopUp: loadStuddentTermsAndConditionPopUp,
        validateBannedWords: validateBannedWords,
        validateBannedWordsAndSubmitForm: validateBannedWordsAndSubmitForm,
        showErrorLogger: showErrorLogger,
        validateBannedWordsFormData: validateBannedWordsFormData,
        onSubmitErrorLog: onSubmitErrorLog,
        downloadFile: downloadFile,
        getmonthname: getmonthname,
        setActualDateValue: setActualDateValue,
        trimStartingZero: trimStartingZero,
        getFormattedDate: getFormattedDate,
        substringBannedWords: substringBannedWords,
        back: back,
        enableCheckboxCascade: enableCheckboxCascade,
        convertImgToSvg: convertImgToSvg,
        notyDltConfirm: notyDltConfirm,
        customScrollbar: customScrollbar,
        UpdateStatusToDelete: UpdateStatusToDelete,
        isJson: isJson,
        convertBase64ToImageFile: convertBase64ToImageFile,
        getUniqueString: getUniqueString,
        getFileBuffer: getFileBuffer,
        getFile: getFile,
        downloadSharepointFile: downloadSharepointFile,
        templateCreator: templateCreator,
        toSystemReadableTime: toSystemReadableTime,
        toSystemReadableDate: toSystemReadableDate
    };


}();

globalFunctions.customScrollbar();
(function ($) {
    jQuery.fn.extend({
        isEnabledInDOM: function () {
            return this.is(':visible') && this.prop('disabled') == false;
        },
        valIfEnabledInDOM: function () {
            return this.isEnabledInDOM() ? (this.val() == '' ? null : this.val()) : undefined;
        },
        textIfEnabledInDOM: function () {
            return this.isEnabledInDOM() ? $('option:selected', this).text() : undefined;
        },
        checkedIfEnabledInDOM: function () {
            return this.isEnabledInDOM() ? this.prop('checked') : false;
        },
        removeHighlight: function () {
            return this.removeClass('warning').removeClass('success').removeClass('modified').removeClass('active').removeClass('error').removeClass('success');
        },
        setModified: function () {
            return this.removeHighlight().addClass('warning').addClass('modified');
        },
        setBeginSave: function () {
            var processingIcon = $('<div class="processing"></div>');
            this.after(processingIcon);
            processingIcon.css(
                {
                    'left': this.position().left + this.outerWidth() - $('.processing').outerWidth() - 2,
                    'top': this.position().top + (this.outerHeight() - $('.processing').outerHeight()) / 2 + 1
                });
            return this.removeHighlight().addClass('saving');
        },
        setEndSave: function (addModifiedCss) {
            var thisObj = this;
            setTimeout(function () {
                thisObj.removeClass('saving');
                if (addModifiedCss)
                    thisObj.addClass('modified');
                $('.processing').remove();
            }, 100);
            return thisObj;
        },
        setError: function () {
            $('.processing').remove();
            return this.removeHighlight().addClass('error');
        },
        readValueFromCookie: function () {
            return this.attr('icck-name') != undefined ? globalFunctions.getCookie(this.attr('icck-name')) : undefined;
        },
        hasVerticalScrollBar: function () {
            return this.get(0).scrollHeight > this.height();
        },
        hasRows: function () {
            return this.find('tbody > tr > td.dataTables_empty').length == 0;
        }
    });

    String.prototype.toBoolean = function () { return this != undefined && this.toLowerCase() == 'true'; };

    String.prototype.format = function () {
        var str = this;
        for (var i = 0; i < arguments.length; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            str = str.replace(reg, arguments[i]);
        }
        return str;
    }

    String.prototype.replaceAll = function (target, replacement) {
        return this.split(target).join(replacement);
    };

    /* Trim spaces of inputs Start */
    trimInputValue = function (input) {
        var trimmedValue = $(input).val().trim();
        $(input).val(trimmedValue);
    };

    $("input [type='text']").on('change', function () {
        trimInputValue($(this));
    });

    $("textarea").on('change', function () {
        trimInputValue($(this));
    });

    /* Trim spaces of inputs End */

}(jQuery));

var fileInputControlSettings = {
    fileActionSettings: {
        showZoom: false,
        showUpload: false,
        //indicatorNew: "",
        showDrag: false
    },
    previewFileIconSettings: { // configure your icon file extensions
        'doc': '<i class="fas fa-file-word ic-file-word color-doc"></i>',
        'docx': '<i class="fas fa-file-word ic-file-word color-doc"></i>',
        'xls': '<i class="fas fa-file-excel ic-file-excel color-excel"></i>',
        'xlsx': '<i class="fas fa-file-excel ic-file-excel color-excel"></i>',
        'ppt': '<i class="fas fa-file-powerpoint ic-file-ppt color-ppt"></i>',
        'pptx': '<i class="fas fa-file-powerpoint ic-file-ppt color-ppt"></i>',
        'pdf': '<i class="fas fa-file-pdf ic-file-pdf color-pdf "></i>',
        'zip': '<i class="fas fa-file-archive ic-file text-primary"></i>',
        'rar': '<i class="fas fa-file-archive ic-file text-primary"></i>',
        'htm': '<i class="fas fa-file-code ic-file text-primary"></i>',
        'html': '<i class="fas fa-file-code ic-file text-primary"></i>',
        'txt': '<i class="fas fa-file-alt ic-file color-txt"></i>',
        'rtf': '<i class="fas fa-file-alt ic-file color-txt"></i>',
        'mov': '<i class="fas fa-file-video ic-file color-video"></i>',
        'mp3': '<i class="fas fa-file-audio ic-file color-audio"></i>',
        'mp4': '<i class="fas fa-file-video ic-file color-video"></i>',
        // note for these file types below no extension determination logic
        // has been configured (the keys itself will be used as extensions)
        'jpg': '<i class="fas fa-file-image ic-file-img color-image"></i>',
        'jpeg': '<i class="fas fa-file-image ic-file-img color-image"></i>',
        'JPEG': '<i class="fas fa-file-image ic-file-img color-image"></i>',
        'gif': '<i class="fas fa-file-image ic-file-img color-image"></i>',
        'png': '<i class="fas fa-file-image ic-file-img color-image"></i>'
    },
    previewSettings: {
        image: { width: "50px", height: "auto" },
        html: { width: "50px", height: "auto" },
        other: { width: "50px", height: "auto" }
    },
    previewFileExtSettings: { // configure the logic for determining icon file extensions
        'doc': function (ext) {
            return ext.match(/(doc|docx)$/i);
        },
        'xls': function (ext) {
            return ext.match(/(xls|xlsx)$/i);
        },
        'ppt': function (ext) {
            return ext.match(/(ppt|pptx)$/i);
        },
        'zip': function (ext) {
            return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
        },
        'htm': function (ext) {
            return ext.match(/(htm|html)$/i);
        },
        'txt': function (ext) {
            return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
        },
        'mov': function (ext) {
            return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
        },
        'mp3': function (ext) {
            return ext.match(/(mp3|wav)$/i);
        }

    }
};
$('#myModal').on('hidden.bs.modal', function () {
    $('#myModal').find('form').trigger('reset');
})
$('.sort-dropdown .selectpicker').selectpicker({
    dropdownAlignRight: true
});
//remove notification after clicking on read btn
$(".mark-noti-read li i").click(function () {
    $(this).closest("li").fadeOut(500, function () { $(this).remove(); });
})

if (!String.prototype.padStart) {
    String.prototype.padStart = function padStart(targetLength, padString) {
        targetLength = targetLength >> 0; //truncate if number or convert non-number to 0;
        padString = String((typeof padString !== 'undefined' ? padString : ' '));
        if (this.length > targetLength) {
            return String(this);
        }
        else {
            targetLength = targetLength - this.length;
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0, targetLength) + String(this);
        }
    };
}
var GetBlobUri = function (fileName) {
    if (fileName == null)
        return fileName;
    if (fileName.toLowerCase().includes(".sharepoint.com"))
        return fileName;
    if (fileName.toLowerCase().includes("/resources"))
        return fileName;
    else
        return fileName != null ? (fileName.toLowerCase().includes("downloadblobfile") ? fileName : "/FileDownlod/DownloadBlobFile?FileName=" + fileName) : "";
}
$(document).ready(function () {
    if ($("#ParentCorner").find("a").hasClass("active")) {
        if (!$('a[href^="#ParentCorner"]').hasClass("active")) {
            $('a[href^="#ParentCorner"]').addClass("active");
        }
    }
});


