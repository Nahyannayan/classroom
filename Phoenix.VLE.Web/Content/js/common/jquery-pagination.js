﻿var DynamicPagination = function (newDivId) {
    this.panelId = newDivId;
};

DynamicPagination.prototype = function () {
    var _settings = {};

    var init = function (settings) {
        _settings = settings;
        _settings["panelId"] = this.panelId;
        loadPaginationData(settings.url, this.panelId);
        initEventListner();
    },
        initEventListner = function () {
            $(document).off("click", ".custom.page-link");
            $(document).on("click", ".custom.page-link", function () {
                var pageUrl = $(this).data("url");
                var panelId = $(this).closest('.pagination-panel').attr('id');
                loadPaginationData(pageUrl, panelId);
            });

            $(document).off('keyup', ".custom.search-field");
            $(document).on('keyup', ".custom.search-field", function (e) {
                e.stopPropagation();
                if (e.keyCode === 13) {
                    var panelId = $(this).closest('.pagination-panel').attr('id');
                    loadPaginationData($(this).data("url"), panelId);
                }
            });
        },

        loadPaginationData = function (url, panelId) {
            $.ajax({
                url: getPaginationUrl(url, panelId),
                success: function (data) {
                    $("#" + panelId).html(data);
                    $('[data-toggle="tooltip"]').tooltip({
                        //trigger: 'focus',
                        html: true
                    });
                    $("#grid").addClass('active');
                    $('#list').click(function (event) {
                        event.preventDefault();
                        $('#library .item').addClass('list-group-item');
                        $(this).toggleClass('active');
                        $("#grid").removeClass('active');
                    });
                    $('#grid').click(function (event) {
                        event.preventDefault();
                        $('#library .item').removeClass('list-group-item');
                        $('#library .item').addClass('grid-group-item');
                        $(this).toggleClass('active');
                        $("#list").removeClass('active');
                    });
                    $(document).trigger("DynamicPagination:Complete");
                }
            });
        },
        getPaginationUrl = function (url, panelId) {
            var paginateUrl;
            if ($("#" + panelId).find("#searchContent").val() === null || $("#" + panelId).find("#searchContent").val() === undefined)
                paginateUrl = url;
            else
                paginateUrl = url + "&searchString=" + $("#" + panelId).find("#searchContent").val();

            if ($("#" + panelId).find("#sortByContent").val() === null || $("#" + panelId).find("#sortByContent").val() === undefined)
                return paginateUrl;
            else
                paginateUrl = paginateUrl + "&sortBy=" + $("#" + panelId).find("#sortByContent").val();

            return paginateUrl;
        };


    return {
        init: init

    };
}();
