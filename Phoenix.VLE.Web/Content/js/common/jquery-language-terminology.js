﻿$(document).ready(function () {
    var pageURL = $(location).attr("href");
    $.ajax({
        type: "GET",
        url: "/Shared/Shared/GetLanguageTerminologyList?url=" + pageURL,
        dataType: "json",
        error: function () {

        },
        success: function (data) {
            $.each(data, function (k, v) {
                $("label").each(function (i) {
                    var text = $(this).text().trim(); //Get the text
                    //if (text.indexOf(v.OldTerm) !== -1) {
                    //    $(this).text(text.replace(v.OldTerm, v.NewTerm));
                    //}
                    $(this).text(replaceAll(text, v.OldTerm, v.NewTerm));

                });
                $("p").each(function (i) {
                    var text = $(this).text(); //Get the text
                     //if (text.indexOf(v.OldTerm) !== -1) {
                    //    $(this).text(text.replace(v.OldTerm, v.NewTerm));
                    //}
                    $(this).text(replaceAll(text, v.OldTerm, v.NewTerm));
                });
            });
        }

    });
});


function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}