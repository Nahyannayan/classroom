﻿var DynamicGrid = function (newTableId) {
    this.tableId = newTableId;
};

DynamicGrid.prototype = function () {
    var _otable;
    var _settings;
    var _langSettings={
	"sEmptyTable":     translatedResources.noData,
	"sInfo":           translatedResources.sInfo,
	"sInfoEmpty":      translatedResources.sInfoEmpty,
	"sInfoFiltered":   translatedResources.sInfoFiltered,
	"sInfoPostFix":    "",
	"sInfoThousands":  ",",
	"sLengthMenu":     translatedResources.sLengthMenu,
	"sLoadingRecords": "Loading...",
	"sProcessing":     "Processing...",
	"sSearch":         translatedResources.search,
	"sZeroRecords":    translatedResources.sZeroRecords,
	"oPaginate": {
		"sFirst":    translatedResources.first,
		"sLast":     translatedResources.last,
		"sNext":     translatedResources.next,
		"sPrevious": translatedResources.previous
	},
	"oAria": {
		"sSortAscending":  translatedResources.sSortAscending,
		"sSortDescending": translatedResources.sSortDescending
	}
}

    var init = function (settings) {
        var thisObj = this;
        _settings = settings;
        
        var gridSettings = {
            "LengthChange": false,
            "searching": settings.searching == undefined ? true : settings.searching,
            "paging": settings.paging == undefined ? true : settings.paging,
            "processing": settings.processing == undefined ? false : settings.processing,
            "serverSide": settings.serverSide,
            "bLengthChange": false,
            "aLengthMenu": settings.pageSizeMenu == undefined ? [[10, 30, 50, 100, 200], [10, 30, 50, 100, 200]] : settings.pageSizeMenu,
            "iDisplayLength": settings.pageSize == undefined ? (settings.simpleGrid ? 500 : 10) : settings.pageSize,
            "bDestroy": true,
            "bAutoWidth": settings.autoWidth == undefined || settings.autoWidth == false ? false : true,
            "bDeferRender": true,
            "bSortCellsTop": true,
            "ordering": (settings.sortable == undefined || settings.sortable == true ? true : false),
            "aaSorting": settings.sorting != undefined ? settings.sorting : (settings.defaultSorting ? [[0, "asc"]] : []),
            "aaSortingFixed": settings.sortingFixed != undefined ? settings.sortingFixed : [],
            "columnDefs": [{ orderable: false, targets: ['nosort'] }],
            "scrollCollapse": settings.scrollCollapse,
            "oLanguage": _langSettings,
            "info": settings.info == undefined ? true : settings.info,
            //"oClasses": { sLengthSelect: 'bs-select' },
            stateSave: !settings.simpleGrid,
            //dom: settings.simpleGrid == true ? 'srt' : (settings.dom == undefined ? (settings.enableColumnSearch == true ? 'lBrtip' : 'lBfrtip') : settings.dom),
            fixedColumns: settings.fixedColumns,
            buttons: settings.columnSelector == undefined ? [{ extend: 'colvis', columns: ':gt(0)', text: '', className: 'btn-column-chooser' }] :
                settings.columnSelector != false ?
                    [{
                        extend: 'colvis', columns: settings.columnSelector.columns == undefined ? ':gt(0)' : settings.columnSelector.columns,
                        text: '', className: 'btn-column-chooser'
                    }] : [],
            fnDrawCallback: function () {
                $(this).css('width', '100%');
                var element = $('#' + thisObj.tableId);
                // trigger event back to the called
                element.trigger("DynamicGrid:DrawCallBack", []);

                //Added by shankar on 19/08/2020
                var table = $('#' + thisObj.tableId).dataTable();
                if (table.fnGetData().length < 10) {
                    $('.dataTables_paginate').hide();
                } else {
                    $('.dataTables_paginate').show();
                }
            },
            "rowCallback": function (row, data, index) {
                var element = $('#' + thisObj.tableId);
                element.trigger("DynamicGrid:RowCallback", [row, data, index]);
            },
            "fnInitComplete": function () {
                $('#' + thisObj.tableId).trigger("DynamicGrid:InitComplete", []);
                globalFunctions.convertImgToSvg();
            },
            "createdRow": function (row, data, dataIndex) {
                var table = $('#' + thisObj.tableId).DataTable();
                $.each($('td', row), function (colIndex) {
                    var headerVal = table.column(colIndex).header().innerHTML;
                    $(this).attr('data-title', headerVal);
                });
            }
        };
        if (settings.scrollX != undefined)
            gridSettings["scrollX"] = settings.scrollX;
        if (settings.scrollY != undefined)
            gridSettings["scrollY"] = settings.scrollY;

        if (settings.multiSelect) {
            gridSettings["columnDefs"] = [{
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '<input type="checkbox" name="id[]" class="row-check" value="' + $('<div/>').text(data).html() + '">';
                }
            }];
        }
        else {
            if (settings.columnDefs != undefined)
                gridSettings["columnDefs"] = settings.columnDefs;
            else
                gridSettings["columnDefs"] = [{ orderable: false, targets: ['nosort'] }];
        }

        if (settings.columnWidths != undefined) {
            $.merge(gridSettings["columnDefs"], settings.columnWidths);
        }
        if (settings.url != null || settings.url != undefined) {
            gridSettings["sAjaxSource"] = settings.url;
            gridSettings["aoColumns"] = settings.columnData;
        }
        else if (settings.data != undefined) {
            gridSettings["aaData"] = settings.data;
            gridSettings["aoColumns"] = settings.columnData;
        }

        // Format datetime to enable proper sorting
        if (settings.enableDateTimeSorting) {
            $.fn.dataTable.moment('dd-mm-yyyy hh:mm A');
            $.fn.dataTable.moment('dd-mm-yyyy');
            $.fn.dataTable.moment('hh:mm A');
        }
        if (settings.enableDateSorting) {
            $.fn.dataTable.moment('dd-mm-yyyy');
        }
        if (settings.enableTimeSorting) {
            $.fn.dataTable.moment('hh:mm A');
        }

        _otable = $('#' + thisObj.tableId).DataTable(gridSettings);
        $('.dataTables_length').addClass('bs-select');
        if (!settings.serverSide) {
            _otable.columns.adjust().draw();
        }

        if (settings.columnSelector != undefined && settings.columnSelector != false)
            attachColumnSelector(thisObj.tableId, settings.columnSelector.addToPanel);

        enableEnterKeySearchForFilterTextBox();

        if (settings.enableColumnSearch) {
            enableColumnSearch();
        }
        return _otable;
    },

        clear = function (tableId) {
            $('#' + tableId).DataTable().clear().draw();
        },

        enableColumnSearch = function () {
            var firstHeaderRow = $('tr', _otable.table().header());
            var searchHeaderRow = $('<tr>');
            var colLength = _otable.columns().shift().length;

            for (var col = 0; col < colLength; col++) {
                searchHeaderRow.append($('<th><input type="text" style="width:100%" value="' + _otable.column(col).search() + '" /></th>'));
            }
            searchHeaderRow.insertAfter(firstHeaderRow);

            _otable.columns().every(function () {
                var that = this;
                $('input', searchHeaderRow).off('keypress');
                $('input', searchHeaderRow).on('keypress', function (e) {
                    if (e.keyCode == 13) {
                        that.column($(this).closest('th').index()).search(this.value);
                        that.draw();
                    }
                });
            });
        },

        attachColumnSelector = function (tableId, addToPanel) {
            var colVisBtn = $('#' + tableId + '_wrapper .buttons-colvis');
            if (colVisBtn.length) {
                $('#' + tableId + '_wrapper .buttons-colvis').addClass('fa fa-table');
                $('#' + tableId + '_wrapper .buttons-colvis').attr('title', translatedResources.chooseColumns);
                $(addToPanel).find('.buttons-colvis').remove();
                $(addToPanel + ":last").append($('#' + tableId + '_wrapper .buttons-colvis'));
            }
        },

        enableEnterKeySearchForFilterTextBox = function () {
            $('#' + this.tableId + '_filter input').off('keyup.DT input.DT');
            $('#' + this.tableId + '_filter input').on('keyup', function (e) {
                e.stopPropagation();
                if (e.keyCode == 13) {
                    _otable.search($(this).val()).draw();
                }
            });
        },

        applyFilter = function (searchValue) {
            $('#' + this.tableId).DataTable().search(searchValue).draw();
        },

        table = function () {
            return $('#' + this.tableId).DataTable();
        },

        clearGrid = function () {
            if (_otable != undefined) {
                _otable.destroy();
                $('#' + this.tableId).empty();
                _otable = undefined;
            }
        };

    return {
        init: init,
        clear: clear,
        table: table,
        applyFilter: applyFilter,
        clearGrid: clearGrid
    }
}();


