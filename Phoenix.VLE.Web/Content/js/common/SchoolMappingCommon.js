﻿/// <reference path="../common/global.js" />
var schoolMappingCommon = function () {
    var _saveUrl = '', _cityUrl = '', _buUrl = ''
    var init = function (saveUrl, cityUrl, buUrl) {
        _saveUrl = saveUrl;
        _cityUrl = cityUrl;
        _buUrl = buUrl;
    },

        saveMappedData = function (registrationId) {
            var formData = new FormData();

            if (!globalFunctions.isValueValid(registrationId)) {
                return;
            }
            var countryId = $('#CountryId').val();
            var cityId = $('#CityId').val();

            $.each($("input[name='BusinessUnits']"), function (i) {

                formData.append('SelectedBUId[' + i + '].Value', this.value);
                formData.append('SelectedBUId[' + i + '].Selected', this.checked);
            });

            formData.append("RegistrationId", registrationId);
            formData.append("CountryId", countryId);
            formData.append("CityId", cityId);


            $.ajax({
                type: 'POST',
                url: _saveUrl,
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    $("#myModal").modal('hide');
                    globalFunctions.showMessage(result.NotificationType, result.Message)
                },
                error: function (msg) {
                    console.log(msg);
                    globalFunctions.onFailure();
                }
            });
        },

        loadMappingDetails = function (id, target) {
            $.ajax({
                type: "GET",
                url: _loadUrl,
                data: { id: id },//
                success: function (response) {
                    $(target).empty();
                    $(target).append(response);
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        },

        loadCityDetails = function (id) {
            $.ajax({
                type: "GET",
                url: _cityUrl,
                data: { id: id },
                dataType: 'json',
                success: function (response) {

                    var businessUnits = $('#BusinessUnitId');
                    businessUnits.empty();

                    var cities = $('#CityId');
                    cities.empty();
                    cities.append("<option value=''>Select Region</option>");

                    $.each(response, function (i, item) {
                        cities.append($('<option></option>').val(item.ItemId).html(item.ItemName)
                        );
                    });

                    $('.selectpicker').selectpicker('refresh');
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        },
        loadBUDetails = function (id, RefId) {
            $.ajax({
                type: "GET",
                url: _buUrl,
                data: { id: id, RefId: RefId },
                dataType: 'json',
                success: function (response) {
                    $('#selectAll').prop('disabled', false);
                    $('#selectAll').prop('checked', false);

                    var businessUnits = $('#BusinessUnitId');
                    businessUnits.empty();

                    $.each(response, function (i, item) {
                        var html = $('<div class="col-lg-6 col-md-6 col-12 buclass"></div>')
                            .append($('<div class="custom-control custom-checkbox mb-2"></div>')
                                .append(item.Selected ? '<input type="checkbox" class="custom-control-input" name="BusinessUnits" id="SelectedBUId[' + i + ']" value="' + item.Value + '"checked="' + item.Selected + '" >' : '<input type="checkbox" class="custom-control-input" name="BusinessUnits" id="SelectedBUId[' + i + ']" value="' + item.Value + '" >')
                                .append('<label class="custom-control-label" for="SelectedBUId[' + i + ']">' + item.Text)
                                .append('</label>'));
                        businessUnits.append(html);
                    });

                    if ($("input[name='BusinessUnits']").length == $("input[name='BusinessUnits']:checked").length) {
                        $('#selectAll').prop('checked', true);
                    };

                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        }

    return {
        init: init,
        saveMappedData: saveMappedData,
        loadMappingDetails: loadMappingDetails,
        loadCityDetails: loadCityDetails,
        loadBUDetails: loadBUDetails
    };
}();



