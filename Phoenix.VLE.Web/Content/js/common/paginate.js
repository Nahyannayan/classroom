/*global console, document, $, jQuery */
(function ($) {
    'use strict';

    function log(d) {
        console.log(d);
    }

    //    $(document).ready(function () {

    $.fn.pagination = function (options) {
        //        alert('prout');
        var paginationContainer = this,
            itemsPerPage,
            itemsToPaginate,
            defaults,
            settings,
            i,
            numberOfPaginationLinks;

        log(paginationContainer);
        //debugger;
        defaults = {
            itemsPerPage: 5
        };

        settings = {};

        $.extend(settings, defaults, options);

        itemsPerPage = settings.itemsPerPage;

        itemsToPaginate = $(settings.itemsToPaginate);
        numberOfPaginationLinks = Math.ceil((itemsToPaginate.length / itemsPerPage));
        log(numberOfPaginationLinks);
        // Création de list dans la div ciblée
        $('<ul class="pagination quiz-pagination float-right"></ul>').prependTo(paginationContainer);
        if (numberOfPaginationLinks == 1) {
            if (itemsToPaginate.length != 1) {
                paginationContainer.find('ul').append('<li class="prev page-item disabled"><a class="page-link custom next" href="javascript:void(0);">' + translatedResources.previous + '</a></li>');
            }
        } else {
            paginationContainer.find('ul').append('<li class="prev page-item disabled"><a class="page-link custom previous" href="javascript:void(0);">' + translatedResources.previous + '</a></li>');
        }
        // Boucle qui créer des li
        //debugger;
        for (i = 0; i < numberOfPaginationLinks; i += 1) {
            //debugger;
            if (i == 0) {
                paginationContainer.find('ul').append('<li onclick="paginateperform()" class="number page-item active" id=' + (i + 1) + '><a class="page-link custom arabictxt">' + (i + 1) + '</a></li>');
            } else {
                paginationContainer.find('ul').append('<li onclick="paginateperform()" class="number page-item" id=' + (i + 1) + '><a class="page-link custom arabictxt">' + (i + 1) + '</a></li>');
            }
            $('#TotalPages').val(i + 1);
        }
        if (numberOfPaginationLinks == 1) {
            if (itemsToPaginate.length != 1) {
                paginationContainer.find('ul').append('<li onclick="paginateperform()" class="page-item disabled"><a class="page-link custom next" href="javascript:void(0);">' + translatedResources.next + '</a></li>');
            }
        } else {
            paginationContainer.find('ul').append('<li onclick="paginateperform()" class="page-item"><a class="page-link custom next" href="javascript:void(0);">' + translatedResources.next +'</a></li>');
        }
        
        itemsToPaginate.filter(':gt(' + (itemsPerPage - 1) + ')').hide();

        paginationContainer.find('.number').click(function () {
            var itemsToHide,
                linkNumber,
                itemsToShow,
                $this = $(this);

            $this.addClass(settings.activeClass);
            $this.siblings().removeClass(settings.activeClass);

            linkNumber = $this.text();
            var currentPage = parseInt(linkNumber);
            var totalPages = $('.quiz-pagination').children("li").length;
            if (currentPage == totalPages - 2) {
                $('.next').addClass('disabled');
            }
            else {
                $('.next').removeClass('disabled')
            }
            if (currentPage==1)
            {
                $('.prev').addClass('disabled');
            }else {
                $('.prev').removeClass('disabled')
            }
            itemsToHide = itemsToPaginate.filter(':lt(' + ((linkNumber - 1) * itemsPerPage) + ')');

            $.merge(itemsToHide, itemsToPaginate.filter(':gt(' + ((linkNumber * itemsPerPage) - 1) + ')'));

            itemsToHide.hide();

            itemsToShow = itemsToPaginate.not(itemsToHide);
            itemsToShow.show();

        });
        paginationContainer.find('.prev').click(function () {
            //debugger;
            if ($('.number.active').text() != "1") {
                var itemsToHide,
                    linkNumber,
                    itemsToShow,
                    $this = $('.number.active').prev();

                $this.addClass(settings.activeClass);
                $this.siblings().removeClass(settings.activeClass);

                linkNumber = $this.text();
                var currentPage = parseInt(linkNumber);
                var totalPages = $('.quiz-pagination').children("li").length;
                if (currentPage == totalPages - 3) {
                    $('.next').removeClass('disabled')
                }
                if (currentPage == 1) {
                    $('.prev').addClass('disabled')
                }
               
                itemsToHide = itemsToPaginate.filter(':lt(' + ((linkNumber - 1) * itemsPerPage) + ')');

                $.merge(itemsToHide, itemsToPaginate.filter(':gt(' + ((linkNumber * itemsPerPage) - 1) + ')'));

                itemsToHide.hide();

                itemsToShow = itemsToPaginate.not(itemsToHide);
                itemsToShow.show();

            }

        });
        paginationContainer.find('.next').click(function () {
            //debugger;
            if ($('.number.active').text() != $('#TotalPages').val())
            {
                var itemsToHide,
                    linkNumber,
                    itemsToShow,
                    $this = $('.number.active').next();

                $this.addClass(settings.activeClass);
                $this.siblings().removeClass(settings.activeClass);

                linkNumber = $this.text();
                var currentPage = parseInt(linkNumber);
                var totalPages = $('.quiz-pagination').children("li").length;
                if (currentPage > 1) {
                    $('.prev').removeClass('disabled')
                }
                if (currentPage == totalPages - 2)
                {
                    $('.next').addClass('disabled')
                }
                //else {
                //    $('.next').removeClass('disabled')
                //}
                itemsToHide = itemsToPaginate.filter(':lt(' + ((linkNumber - 1) * itemsPerPage) + ')');

                $.merge(itemsToHide, itemsToPaginate.filter(':gt(' + ((linkNumber * itemsPerPage) - 1) + ')'));

                itemsToHide.hide();

                itemsToShow = itemsToPaginate.not(itemsToHide);
                itemsToShow.show();

            }
           
        });
        //paginationContainer.find('ul li').click(function () {

        //    var itemsToHide,
        //        linkNumber,
        //        itemsToShow,
        //        $this = $(this);

        //    $this.addClass(settings.activeClass);
        //    $this.siblings().removeClass(settings.activeClass);

        //    linkNumber = $this.text();

        //    itemsToHide = itemsToPaginate.filter(':lt(' + ((linkNumber - 1) * itemsPerPage) + ')');

        //    $.merge(itemsToHide, itemsToPaginate.filter(':gt(' + ((linkNumber * itemsPerPage) - 1) + ')'));

        //    itemsToHide.hide();

        //    itemsToShow = itemsToPaginate.not(itemsToHide);
        //    itemsToShow.show();

        //});

        ////OLD IMPLEMENTATION 

        ////        alert('prout');
        //var paginationContainer = this,
        //    itemsPerPage,
        //    itemsToPaginate,
        //    defaults,
        //    settings,
        //    i,
        //    numberOfPaginationLinks;

        //log(paginationContainer);

        //defaults = {
        //    itemsPerPage: 5
        //};

        //settings = {};

        //$.extend(settings, defaults, options);

        //itemsPerPage = settings.itemsPerPage;

        //itemsToPaginate = $(settings.itemsToPaginate);
        //numberOfPaginationLinks = Math.ceil((itemsToPaginate.length / itemsPerPage));
        //log(numberOfPaginationLinks);

        //// Création de list dans la div ciblée
        //$('<ul></ul>').prependTo(paginationContainer);

        //// Boucle qui créer des li
        //for (i = 0; i < numberOfPaginationLinks; i += 1) {
        //    paginationContainer.find('ul').append('<li>' + (i + 1) + '</li>');
        //}

        //itemsToPaginate.filter(':gt(' + (itemsPerPage - 1) + ')').hide();

        //paginationContainer.find('ul li').click(function () {

        //    var itemsToHide,
        //        linkNumber,
        //        itemsToShow,
        //        $this = $(this);

        //    $this.addClass(settings.activeClass);
        //    $this.siblings().removeClass(settings.activeClass);
            
        //    linkNumber = $this.text();

        //    itemsToHide = itemsToPaginate.filter(':lt(' + ((linkNumber - 1) * itemsPerPage) + ')');

        //    $.merge(itemsToHide, itemsToPaginate.filter(':gt(' + ((linkNumber * itemsPerPage) - 1) + ')'));

        //    itemsToHide.hide();

        //    itemsToShow = itemsToPaginate.not(itemsToHide);
        //    itemsToShow.show();

        //});
    };
    //    });
    //$('.quiz-pagination li:nth-child(2)').addClass('active');

}(jQuery));