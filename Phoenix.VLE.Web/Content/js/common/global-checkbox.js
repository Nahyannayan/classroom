﻿var CheckAllEnabler = function () { };

CheckAllEnabler.prototype = function () {
    // Checks/unchecks all child check boxes recursively
    var checkAllChildCheckBoxesOnTree = function (childGroupId, checkedVal) {
        if (childGroupId) {
            var childCheckboxes = $(':checkbox[data-groupid=' + childGroupId + ']:visible');
                childCheckboxes.prop('checked', checkedVal);

            // The attribute "data-child-groupid" of a checkbox indicates that the checkbox 
            // has child checkboxes further down in the tree.  
            // Get the group id of its children from "data-child-groupid"
            // and make recursive call to check/uncheck them.
            childCheckboxes.filter('[data-child-groupid]').each(function () {
                checkAllChildCheckBoxesOnTree($(this).attr('data-child-groupid'), checkedVal);
            });
        }
    },

    // Checks/unchecks all parent check boxes recursively
    checkParentCheckBoxesOnTree = function (activeCheckBoxGroupId) {
        var activeCheckboxGroupCount = $(':checkbox[data-groupid=' + activeCheckBoxGroupId + ']').length;
        var activeCheckboxCheckedGroupCount = $(':checked[data-groupid=' + activeCheckBoxGroupId + ']').length;
        var parentCheckboxCheckedVal = (activeCheckboxGroupCount == activeCheckboxCheckedGroupCount ? true : false);
        var parentCheckBox = $(':checkbox[data-child-groupid=' + activeCheckBoxGroupId + ']');

        parentCheckBox.prop('checked', parentCheckboxCheckedVal);

        // Recursive call to check/uncheck all parent checkboxes in tree        
        if (parentCheckBox.attr('data-groupid') != undefined) {
            checkParentCheckBoxesOnTree(parentCheckBox.attr('data-groupid'));
        }
    },

    // Checks/unchecks all parent check boxes recursively on load
    checkParentCheckBoxesOnTreeLoad = function (firstChildGroupId) {
        var firstChildCheckboxes = $(':checkbox[data-groupid=' + firstChildGroupId + ']');

        $(firstChildCheckboxes).each(function () {
            var childGroupId = $(this).attr('data-child-groupid');
            if (childGroupId) {
                var checkboxGroupCount = $(':checkbox[data-groupid=' + childGroupId + ']').length;
                var checkboxCheckedGroupCount = $(':checked[data-groupid=' + childGroupId + ']').length;
                var parentCheckboxCheckedVal = (checkboxGroupCount == checkboxCheckedGroupCount ? true : false);
                var parentCheckBox = $(this);

                parentCheckBox.prop('checked', parentCheckboxCheckedVal);

                // Recursive call to check/uncheck all parent checkboxes in tree        
                if (parentCheckBox.attr('data-groupid') != undefined) {
                    checkParentCheckBoxesOnTree(parentCheckBox.attr('data-groupid'));
                }
            }
        });
    };
    return {
        checkAllChildCheckBoxesOnTree: checkAllChildCheckBoxesOnTree,
        checkParentCheckBoxesOnTree: checkParentCheckBoxesOnTree,
        checkParentCheckBoxesOnTreeLoad: checkParentCheckBoxesOnTreeLoad
    };
}();