﻿
var LanguageTextEditPopover = function () {
    var self = this;
};

LanguageTextEditPopover.prototype = function () {

    var jsonTemplate = '{"root": { "item": [{0}] } }';
    var itemTemplate = '{ "@lang": "{0}", "@name": "{1}", "#text": "{2}" }';

    var attachPopover = function () {
        $('[rel=multilang-textedit-popover]').each(function () {
            var target = $(this);
            attachIndividualPopover(target);

            $(document).on('click', '.popover-submit', function () {
                $(this).closest('.popover').find('input').each(function () {
                    $('[data-mapped-to="' + $(this).attr('id') + '"]').val($(this).val());
                    $('[id="' + $(this).attr('id') + '"]').val($(this).val());
                });
                $(this).closest('.popover').popover('hide');
            });
            $(document).on('click', '.popover-cancel', function () {                 //On cancel clears the value from the hidden fields        
                $(this).closest('.popover').popover('hide');
            });
            $(document).click(function (e) {
                $(this).find('.popover').each(function () {
                    if ($(this).has(e.target).length == 0 && $(e.target).attr('aria-describedby') != $(this).attr('id')) {
                        $(this).popover('hide');
                    }
                });
            });
        });
    },
        attachIndividualPopover = function (targetObj) {
            var contentHtml = "";
            var labelName = targetObj.attr('data-labelname');
            var popoverTemplate = ['<div class="popover">',
                '<div class="arrow"></div>',
                '<h3 class="popover-header">' + labelName + '</h3>',
                '<div class="popover-body"></div>',
                '<div class="popover-footer">',
                '<button id="test" type="button" class="btn btn-default popover-cancel">',
                '<i class="far fa-trash-alt"></i></button>',
                '<button type="button" class="btn btn-primary popover-submit">',
                '<i class="fas fa-check"></i></button>',
                '</div>',
                '</div>'].join('');

            var inputClass = targetObj.attr('data-input-class');
            var languageInputids = targetObj.attr('data-inputids').split(',');
            var maxLength = targetObj.attr('maxlength');


            var langCode = targetObj.attr('data-otherlang-codes').split(',');
            var count = langCode.length;
            for (var i = 0; i <= parseInt(count) - 1; i++) {
                var input = $('[name="' + languageInputids[i].trim().replace('po_', '') + '"]');
                contentHtml += [
                    '<div class="row">',
                    '<div class= "col-lg-12 col-md-12 col">',
                    '<div class="md-form md-outline">',
                    '<label class="control-label placeholder-label active">' + langCode[i] + '</label>',
                    '<input type="text" id="' + languageInputids[i].trim() + '" value="' + input.val() + '" class="' + inputClass + '" ' + (maxLength != undefined ? ('maxlength="' + maxLength + '" ') : "") + '/>',
                    '</div></div></div> '
                ].join('');
            }
            //contentHtml = $("#popoverform").html();
            var options = {
                placement: 'bottom',
                //trigger: 'manual',
                html: true,
                template: popoverTemplate,
                sanitize: false,
                content: function () {
                    return contentHtml;
                }
            };
            targetObj.popover(options)
                .focus(function (e) {
                    e.preventDefault();
                    $('.popover').popover('hide');

                    if ((targetObj.next('.popover:visible').length == 0)) {
                        targetObj.popover('show');
                        $('.popover:visible').find('input').each(function () {
                            var previousInputValue = $('[data-mapped-to="' + $(this).attr('id') + '"]').val();
                            if (previousInputValue === undefined)
                                previousInputValue = "";
                            $(this).val(previousInputValue);
                        });
                    }
                });
        }
    return {
        attachPopover: attachPopover,
        attachIndividualPopover: attachIndividualPopover
    };
}();

$(document).ready(function () {

    $(document).on('click', '.multilang-submit', function () {
        var langData = [];

        $('[rel=multilang-textedit-popover]').each(function (idx, item) {
            var target = $(this);
            //base input
            var jsonData = target.data();
            langData.push({
                "@lang": jsonData.langId,
                "@name": jsonData.langName.slice(0, jsonData.langName.lastIndexOf('_')),
                "#text": target.val()
            });
            //multi lingual
            var inputids = target.data('inputids').split(',');
            $.each(inputids, function (idx, item) {
                var input = $('[name="' + item.replace('po_', '') + '"]');
                var inputData = input.data();
                langData.push({
                    "@lang": inputData.langId,
                    "@name": inputData.langName.slice(0, jsonData.langName.lastIndexOf('_')),
                    "#text": input.val()
                });
            });

            $('#' + jsonData.xmlField).val(JSON.stringify(langData));
        });
    });
});
