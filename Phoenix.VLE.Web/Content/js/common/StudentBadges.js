﻿var dashboardBadgesFunctions = function () {

    getLeadBoardData = function (schoolId, studentId, range, gradeId, date) {
        
        $.get('/Home/GetLeadBoardData', { schoolId: schoolId, studentId: studentId, range: range, gradeId: gradeId, date: date })
            .then(response => {
                $('#overallPoints').html(response.overallPoints + "<span class='ml-2'>points</span>");
                $('#videoPoints').html(response.videoPoints + "<span>Activity Videos</span>");
                $('#physicalPoints').html(response.physicalPoints + "<span>Physical Activity</span>");
                $('#artsPoints').html(response.artsPoints + "<span>All Projects</span>");
                $('#leadBoardContent').html(response.RelatedHtml);
                setTimeout(function () { winnerListHeight(); }, 800);
            });
    },

    winnerListHeight = function () {
        let drawerHeight = $('.right-drawer').outerHeight();
        let pointsBreakdownHeight = $('.pointsBreakdown').outerHeight();
        let winnerListSwitcherHeight = $('.winnerList-switcher').outerHeight();
        let winnerListHeight = drawerHeight - (pointsBreakdownHeight + winnerListSwitcherHeight + 32);

        $(".winnerlist").mCustomScrollbar({
            setHeight: winnerListHeight,
            autoExpandScrollbar: true,
            scrollbarPosition: "outside",
            autoHideScrollbar: true,
        });
    }

    return {
        getLeadBoardData: getLeadBoardData
    }

}();

$(function () {
    
    $('.rightDrawerLeadBoard').click(function () {
        $('#leaderBoard').toggleClass('open');
        $('#leaderBoard').animate({
            right: '0'
        })
    });

    $('.closeRightLeadBoardDrawer').click(function () {
        $('#leaderBoard').toggleClass('open');
        $('#leaderBoard').animate({
            right: '-500'
        })
    });

    $(".badget-scroll").mCustomScrollbar({
        axis: "x",
        autoExpandScrollbar: true,
        scrollbarPosition: "inside",
        autoHideScrollbar: true,
    });

    $('[data-toggle="tooltip"]').tooltip({
        //trigger: 'focus',
        html: true
    });

});