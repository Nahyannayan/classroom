﻿var CustomValidator = function (rules, id, messages) {
    this.validationRules = rules;
    this.formId = id;
    this.messages = messages;
};

CustomValidator.prototype = function () {
    //Jquery Validation
    var init = function () { //passed id
        //Jquery Validation
        $(this.formId).validate({
            ignore: [],
            rules: this.validationRules,
            messages: this.messages,
            invalidHandler: function (event, validator) { //display error alert on form submit
                $('.alert-error', $(this.formId)).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('error');
            },

            success: function (label, element) {
                $(element).closest('.form-group').removeClass('error');
                $(element).closest('.btn-group').addClass("valid");
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.addClass('help-small no-left-padding').insertAfter(element.closest('.form-control'));
            }
        });
    };
    return {
        init: init
    };
}();
