﻿var Config = function () {
    var counter = 1;
    var SlabMasterCount = 0;
    var DeleteMasterids = [];
    var ConfigArray = [];
    var init = function () {
        $('.colorpicker').minicolors({
            theme: 'bootstrap',
            control: 'hue',
            forrmat: 'hex'


        });
    },

        AddRow = function () {
            debugger

            var newTr = '<tr id="tablerowMaster' + SlabMasterCount + '"><td><div class="md-form md-outline mb-0">' +
                //'<input type="text" class="text-box single-line" placeholder="TemPlateName" name="GTM_Description[' + counter + ']" value="" required="required" />' +
                //'</td>' +
                //'<td>' +

                '<input class="form-control" placeholder="TemPlateName" id="GradeTemplateDetailId" value="0" name="GradeTemplateDetailId" type="hidden" value="" />' +
                //'<input class="form-control" placeholder="TemPlateName" id="GradeTemplateMasterId" value="0" name="GradeTemplateMasterId" type="hidden" value="" />' +

                '<input type="text" class="form-control" placeholder="Code" name="GTD_ShortCode[' + SlabMasterCount + ']" value=""  />' +
                '<label class="active" for="Code">Code</label>' +
                '</div></td>' +
                //'<input type="text" class="form-control" placeholder="ShortCode" name="GTD_ShortCode[' + SlabMasterCount + ']" value="" required="required" />' +
                //'</td>' +
                '<td><div class="md-form md-outline mb-0">' +
                '<input type="text" class="form-control"  placeholder="Description" name="GTD_Description[' + SlabMasterCount + ']" value="" />' +
                '<label class="active" for="Description">Description</label>' +
                '</div></td>' +
                //'<td>' +
                //'<input type="text" class="form-control"  placeholder="Description" name="GTD_Description[' + SlabMasterCount + ']" value="" required="required" />' +
                //'</td>' +

                '<td><div class="md-form md-outline mb-0">' +
                '<input type="hidden" class="form-control colorpicker" data-control="hue"  placeholder="Color" name="GTD_ColorCode[' + SlabMasterCount + ']" value=""  /></div>' +
                '<label class="active" for="Colour">Colour</label>' +
                '</div></td>' +
                //'<td>' +
                //'<div class="color-picker" ><input type="hidden" class="form-control colorpicker" data-control="hue"  placeholder="ColorCode" name="GTD_ColorCode[' + SlabMasterCount + ']" value="#ccc" required="required" /></div>' +
                //'</td>' +

                '<td><div class="md-form md-outline mb-0">' +
                '<input type="text" class="form-control" placeholder="Value" name="GTD_Value[' + SlabMasterCount + ']" value=""  />' +
                '<label class="active" for="Value">Value</label>' +
                '</div></td>' +
                //'<td>' +
                //'<input type="text" class="form-control" placeholder="Value" name="GTD_Value[' + SlabMasterCount + ']" value="" required="required" />' +
                //'</td>' +

                '<td><div class="md-form md-outline mb-0">' +
                '<input type="text" class="form-control" placeholder="Order" name="GTD_Order[' + SlabMasterCount + ']" value=""  />' +
                '<label class="active" for="Order">Order</label>' +
                '</div></td>' +
                //'<td>' +
                //'<input type="text" class="form-control" placeholder="Order" name="GTD_Order[' + SlabMasterCount + ']" value="" required="required" />' +
                //'</td>' +

                '<td>' +
                '<button type="button" class="btn btn-primary" onclick="Config.removeTrMaster(' + SlabMasterCount + ',0);">Delete</button>' +
                '</td>' +
                '</tr>';
            $('#tbl-ConfigList tbody').append(newTr);

            SlabMasterCount++;
            $('.colorpicker').minicolors('destroy').minicolors({
                theme: 'bootstrap',
                control: 'hue',
                forrmat: 'hex'


            });

        },
        EditConfig = function (id) {
            debugger
            var object = Config.ConfigArray.find(x => x.GradeTemplateDetailId == id);
            $("#Description").val(object.TemPlateName);

            $("#GradeTemplateMasterId").val(object.GradeTemplateMasterId);
            $("#GradeTemplateDetailId").val(object.GradeTemplateDetailId);
            $("#GTD_ShortCode").val(object.ShortCode);
            $("#GTD_Description").val(object.Description);
            $("#GTD_ColorCode").val(object.ColorCode);
            $("#GTD_Value").val(object.Value);
            $("#GTD_Order").val(object.GradeOrder);
            $('#Showtable').addClass('d-none');
            $('.selectpicker').selectpicker('refresh');
            $('#divAddAssessmentConfig').removeClass('d-none');
            $('#divAddAssessmentConfig').show();
            $('.colorpicker').minicolors('destroy').minicolors({
                theme: 'bootstrap',
                control: 'hue',
                forrmat: 'hex'


            });
        },
        removeTr = function (index) {

            //function removeTr(index) {
            if (counter > 1) {
                $('#tablerow' + index).remove();
                counter--;
            }
            return false;
        },
        onAssessmentConfigSuccess = function (response) {
            debugger
            $("#MasterIds").val('');
            Config.DeleteMasterids = [];
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success) {
                debugger
               
            }
        },
        saveGradeTemplate = function () {
            var table = $('#tbl-ConfigList>tbody>tr');
            var gradingList = [];
            $.each(table, function (index, val) {
                var Description = $(val).find('td').eq(0).find('input[type="text"]').val();
                var GradeTemplateDetailId = $(val).find('td').eq(0).find('input[type="hidden"]').val();
                var ShortCode = $(val).find('td').eq(1).find('input[type="text"]').val();
                var ColorCode = $(val).find('td').eq(2).find('input[type="hidden"]').val();
                var Value = $(val).find('td').eq(3).find('input[type="text"]').val();
                var GradeOrder = $(val).find('td').eq(4).find('input[type="text"]').val();
                gradingList.push({
                    Description: Description,
                    GradeTemplateDetailId: GradeTemplateDetailId,
                    ShortCode: ShortCode,
                    ColorCode: ColorCode,
                    Value: Value,
                    GradeOrder: GradeOrder
                });
            });

            console.log(gradingList);
            $.post('/Assessment/AssessmentConfiguration/SaveGradeTemplate', {
                gradingList: gradingList,
                Description: $("#Description").val(),
                GradeTemplateMasterId: $("#GradeTemplateMasterId").val(),
                MasterIds: $("#MasterIds").val()
            }).then((response) => {
                globalFunctions.showMessage(response.NotificationType, response.Message);
                if (response.Success) {
                    debugger

                    Config.LoadDetails();

                }

            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
        },
        EditMaster = function (MasterId) {
            $.post('/Assessment/AssessmentConfiguration/GetAssessmentConfigList?MasterId=' + MasterId, function (response) {
                debugger


                for (i = 0; i < response.length; i++) {

                    $("#Description").val(response[0].TemPlateName);
                    $("#GradeTemplateMasterId").val(response[0].GradeTemplateMasterId);
                    var newTr = '<tr id="tablerowMaster' + i + '"><td><div class="md-form md-outline mb-0">' +

                        '<input class="form-control" placeholder="TemPlateName" id="GradeTemplateDetailId" value="' + response[i].GradeTemplateDetailId + '" name="GradeTemplateDetailId[' + i + ']" type="hidden" value="" />' +

                        '<input type="text" class="form-control" placeholder="Code" name="GTD_ShortCode[' + i + ']" value="' + response[i].ShortCode + '"  />' +
                        '<label class="active" for="Code">Code</label>' +
                        '</div></td>' +

                        '<td><div class="md-form md-outline mb-0">' +
                        '<input type="text" class="form-control"  placeholder="Description" name="GTD_Description[' + i + ']" value="' + response[i].Description + '" />' +
                        '<label class="active" for="Description">Description</label>' +
                        '</div></td>' +

                        '<td><div class="md-form md-outline mb-0">' +
                        '<input type="hidden" class="form-control colorpicker" data-control="hue"  placeholder="Color" name="GTD_ColorCode[' + i + ']" value="' + response[i].ColorCode + '"  /></div>' +
                        '<label class="active" for="Colour">ColorCode</label>' +
                        '</div></td>' +

                        '<td><div class="md-form md-outline mb-0">' +
                        '<input type="text" class="form-control" placeholder="Value" name="GTD_Value[' + i + ']" value="' + response[i].Value + '"  />' +
                        '<label class="active" for="FromMark">FromMark</label>' +
                        '</div></td>' +

                        '<td><div class="md-form md-outline mb-0">' +
                        '<input type="text" class="form-control" placeholder="Order" name="GTD_Order[' + i + ']" value="' + response[i].GradeOrder + '"  />' +
                        '<label class="active" for="ToMark">ToMark</label>' +
                        '</div></td>' +

                        '<td><div class="md-form md-outline mb-0">' +
                        '<button type="button" class="btn btn-primary" onclick="Config.removeTrMaster(' + i + ',' + response[i].GradeTemplateDetailId + ');">Delete</button>' +
                        '</div></td>' +
                        '</tr>';
                    $('#tbl-ConfigList tbody').append(newTr);
                    SlabMasterCount++;
                }
                $('.colorpicker').minicolors('destroy').minicolors({
                    theme: 'bootstrap',
                    control: 'hue',
                    forrmat: 'hex'
                });
                //$('#tbl-ConfigList').hide();
                $('#Showtable').addClass('d-none');
                $('#divAddAssessmentConfig').removeClass('d-none');
            });
        },
        removeTrMaster = function (index, id) {
            debugger
            
            if (id > 0) {
                Config.DeleteMasterids.push(id)
            }
            $('#tablerowMaster' + index).remove();
            SlabMasterCount--;
            $("#MasterIds").val(Config.DeleteMasterids.join(','));
            return false;
        },
        LoadDetails = function () {
            $.post('/Assessment/AssessmentConfiguration/GetAssessmentLoad', function (response) {
                $("#Showtable").html(response);
                $('#Showtable').removeClass('d-none');
                $("#tbl-ConfigListMaster tr").remove();
                Config.DeleteMasterids.length = 0
                $('#divAddAssessmentConfig').addClass('d-none');
                $('#tbl-AssessmentConfig').DataTable({
                    "pageLength": 5,
                    "ordering": false,
                    "bPaginate": false,
                    "bLengthChange": false
                });
            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });;

        }
    return {
        init,
        AddRow,
        removeTr,
        onAssessmentConfigSuccess,
        EditConfig,
        EditMaster,
        removeTrMaster,
        saveGradeTemplate,
        LoadDetails
    }


}();
$(document).ready(function () {
    Config.init();
   
});
$(document).on('click', '#btnAssessmentConfig', function () {
    Config.DeleteMasterids.length = 0
    $('#Showtable').addClass('d-none');
    $('#tbl-ConfigList').show();
    $("#MasterIds").val('');
    $('#divAddAssessmentConfig').removeClass('d-none');
    $("#Description").val('');
    $("#GradeTemplateMasterId").val('');
    $("#GradeTemplateDetailId").val('');
    $("#tbl-ConfigListMaster tr").remove();
    Config.AddRow();
});
$(document).on('click', '#spnBtnBacktoGrid', function () {
    $("#MasterIds").val('');
    Config.DeleteMasterids.length = 0
    Config.SlabMasterCount = 0;
    $("#GradeTemplateMasterId").val('');
    $("#GradeTemplateDetailId").val('');
    $("#GTD_ShortCode").val();
    $("#GTD_Description").val();
    $("#GTD_ColorCode").val();
    $("#GTD_Value").val();
    $("#GTD_Order").val();
    $('#Showtable').removeClass('d-none');
    $("#tbl-ConfigListMaster tr").remove();
    $('#divAddAssessmentConfig').addClass('d-none');
});
