﻿var DeleteMasterids = [];
var GradeSlab = function () {
    var counter = 1;
    var SlabMasterCount = 0;
    var ConfigArray = [];
    var ConfigArrayMaster = [];
    var init = function () {
        $('.colorpicker').minicolors({
            theme: 'bootstrap',
            control: 'hue',
            forrmat: 'hex'
        });
    },
        AddRow = function (count) {
            var newTr = '<tr id="tablerow' + SlabMasterCount + '"><td><div class="md-form md-outline mb-3">' +
                '<input class="form-control" placeholder="TemPlateName" id="GradeTemplateDetailId" value="0" name="GradeTemplateDetailId" type="hidden" value="" />' +
                '<input type="text" class="form-control" placeholder="Code" name="Code[' + SlabMasterCount + ']" value=""  />' +
                '<label class="active" for="Description">Code</label>' +
                '</td>' +

                '<td><div class="md-form md-outline mb-3">' +
                '<input type="text" class="form-control"  placeholder="Description" name="Description[' + SlabMasterCount + ']" value=""  />' +
                '<label class="active" for="Description">Description</label>' +
                '</td></div>' +
                '<td><div class="md-form md-outline mb-3">' +
                //'<label class="active" for="Colour">Colour</label>' +
                '<div class="color-picker" ><input type="hidden" class="form-control colorpicker" data-control="hue"  placeholder="Colour" name="Color[' + SlabMasterCount + ']" value="#ccc"  /></div>' +
                '</td></div>' +
                '<td><div class="md-form md-outline mb-3">' +
                '<input type="text" class="form-control" placeholder="Value" name="FromMark[' + SlabMasterCount + ']" value=""  />' +
                '</td></div>' +
                '<td><div class="md-form md-outline mb-3">' +
                '<input type="text" class="form-control" placeholder="Order" name="ToMark[' + SlabMasterCount + ']" value=""  />' +
                '</td></div>' +
                '<td><div class="md-form md-outline mb-3">' +
                '<button type="button" class="btn btn-primary" onclick="GradeSlab.removeTr(' + SlabMasterCount + ',0);">Delete</button>' +
                '</div></td></div>' +
                '</tr>';
            $('#tbl-ConfigListSlab tbody').append(newTr);

            counter++;
            $('.colorpicker').minicolors('destroy').minicolors({
                theme: 'bootstrap',
                control: 'hue',
                forrmat: 'hex'
            });

        },
        removeTr = function (index) {
            debugger
            //function removeTr(index) {
            if (counter > 1) {
                $('#tablerow' + index).remove();
                counter--;
            }
            return false;
        },
        onGradingSlabSuccess = function (response) {
            debugger
            $("#MasterIds").val('');
            DeleteMasterids = [];
            globalFunctions.showMessage(response.NotificationType, response.Message);
            //if (response.Success) {
            $.post('/Assessment/AssessmentConfiguration/GradeSlabListDetails', function (response) {
                $("#Showtable").html(response);
                $('#Showtable').removeClass('d-none');
                $('#divAddAssessmentConfig').addClass('d-none');
                $('#tbl-AssessmentConfig').DataTable({
                    "pageLength": 5,
                    "ordering": false
                });
            });
            // }
        },
        EditSlab = function (Id) {
            debugger
            var object = GradeSlab.ConfigArray.find(x => x.GradeSlabId == Id);
            $("#Code").val(object.Code);
            $("#Description").val(object.Description);
            $("#FromMark").val(object.FromMark);
            $("#ToMark").val(object.ToMark);
            $("#Color").val(object.Colour);
            $("#GradeSlabMasterDesc").val(object.GradeSlabMasterDesc);
            $("#GradeSlabId").val(object.GradeSlabId);
            $("#GradeSlabMasterId").val(object.GradeSlabMasterId);

            $('#Showtable').addClass('d-none');
            $('.selectpicker').selectpicker('refresh');
            $('#divAddAssessmentConfig').removeClass('d-none');
            $('#divAddAssessmentConfig').show();
            $('.colorpicker').minicolors('destroy').minicolors({
                theme: 'bootstrap',
                control: 'hue',
                forrmat: 'hex'
            });
        },
        EditSlabMaster = function (MasterId) {
            $.post('/Assessment/AssessmentConfiguration/GradeSlabListList?MasterId=' + MasterId, function (response) {
                debugger


                for (i = 0; i < response.length; i++) {

                    $("#GradeSlabMasterDesc").val(response[0].GradeSlabMasterDesc);
                    $("#GradeSlabMasterId").val(response[0].GradeSlabMasterId);


                    var newTr = '<tr id="ConfigListTemp' + i + '"><td><div class="md-form md-outline mb-3">' +

                        '<input class="form-control" placeholder="TemPlateName" id="GradeSlabId" value="' + response[i].GradeSlabId + '" name="GradeSlabId[' + SlabMasterCount + ']" type="hidden" value="" />' +

                        '<input type="text" class="form-control" placeholder="Code" name="Code[' + SlabMasterCount + ']" value="' + response[i].Code + '"  />' +
                        '<label class="active" for="Code">Code</label>' +
                        '</div></td>' +

                        '<td><div class="md-form md-outline mb-3">' +
                        '<input type="text" class="form-control"  placeholder="Description" name="Description[' + SlabMasterCount + ']" value="' + response[i].Description + '" />' +
                        '<label class="active" for="Description">Description</label>' +
                        '</div></td>' +

                        '<td><div class="md-form md-outline mb-3">' +
                        '<input type="hidden" class="form-control colorpicker" data-control="hue"  placeholder="Color" name="Color[' + SlabMasterCount + ']" value="' + response[i].Colour + '"  /></div>' +
                        //'<label class="active" for="Colour">Colour</label>' +
                        '</div></td>' +

                        '<td><div class="md-form md-outline mb-3">' +
                        '<input type="text" class="form-control" placeholder="Value" name="FromMark[' + SlabMasterCount + ']" value="' + response[i].FromMark + '"  />' +
                        '<label class="active" for="FromMark">FromMark</label>' +
                        '</div></td>' +

                        '<td><div class="md-form md-outline mb-3">' +
                        '<input type="text" class="form-control" placeholder="Order" name="ToMark[' + SlabMasterCount + ']" value="' + response[i].ToMark + '"  />' +
                        '<label class="active" for="ToMark">ToMark</label>' +
                        '</div></td>' +

                        '<td><div class="md-form md-outline mb-3">' +
                        '<button type="button" class="btn btn-primary" onclick="GradeSlab.removeTrMaster(' + i + ',' + response[i].GradeSlabId + ');">Delete</button>' +
                        '</div></td>' +
                        '</tr>';
                    $('#tbl-ConfigListSlab tbody').append(newTr);
                    SlabMasterCount++;
                }
                $('.colorpicker').minicolors('destroy').minicolors({
                    theme: 'bootstrap',
                    control: 'hue',
                    forrmat: 'hex'
                });
               // $('#tbl-ConfigListSlab').hide();
                $('#Showtable').addClass('d-none');
                $('#divAddAssessmentConfig').removeClass('d-none');
          
            });
        },
        removeTrMaster = function (index, id) {
            debugger
            if (id > 0) {

                DeleteMasterids.push(id)
            }
            $('#ConfigListTemp' + index).remove();
            SlabMasterCount--;
            $("#MasterIds").val(DeleteMasterids.join(','));

            return false;
        },
        saveGradeSlab = function () {
            debugger
            var table = $('#tbl-ConfigListSlab>tbody>tr');
            var obj = [];
            $.each(table, function (index, val) {
                var Description = $(val).find('td').eq(0).find('input[type="text"]').val();
                var GradeSlabId = $(val).find('td').eq(0).find('input[type="hidden"]').val();
                var Code = $(val).find('td').eq(1).find('input[type="text"]').val();
                var Colour = $(val).find('td').eq(2).find('input[type="hidden"]').val();
                var FromMark = $(val).find('td').eq(3).find('input[type="text"]').val();
                var ToMark = $(val).find('td').eq(4).find('input[type="text"]').val();
                obj.push({
                    Description: Description,
                    GradeSlabId: GradeSlabId,
                    Code: Code,
                    Colour: Colour,
                    FromMark: FromMark,
                    ToMark: ToMark
                });
            });
            $.post('/Assessment/AssessmentConfiguration/SaveGradingSlab', {
                obj: obj,
                GradeSlabMasterDesc: $("#GradeSlabMasterDesc").val(),
                GradeSlabMasterId: $("#GradeSlabMasterId").val(),
                MasterIds: $("#MasterIds").val()
            }).then((response) => {
                globalFunctions.showMessage(response.NotificationType, response.Message);
                if (response.Success) {
                    debugger
                    GradeSlab.LoadSlabDetails();
                    

                }

            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
            console.log(obj);

        },
        LoadSlabDetails = function () {
            $.post('/Assessment/AssessmentConfiguration/GradeSlabListDetails', function (response) {
                debugger
                $("#Showtable").html(response);
                $('#Showtable').removeClass('d-none');
                DeleteMasterids.length = 0
                $("#tbl-ConfigListSlab tr").remove();
                $('#divAddAssessmentConfig').addClass('d-none');
                $('#tbl-AssessmentConfig').DataTable({
                    "pageLength": 5,
                    "ordering": false,
                    "bPaginate": false,
                    "bLengthChange": false
                });
            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });;
        }
    return {
        init: init,
        AddRow: AddRow,
        removeTr: removeTr,
        onGradingSlabSuccess: onGradingSlabSuccess,
        EditSlab: EditSlab,
        EditSlabMaster: EditSlabMaster,
        removeTrMaster: removeTrMaster,
        saveGradeSlab: saveGradeSlab,
        LoadSlabDetails: LoadSlabDetails
    }
}();
$(document).ready(function () {
    GradeSlab.init();
});
$(document).on('click', '#btnAssessmentConfig', function () {
    $("#MasterIds").val('');
    DeleteMasterids.length = 0;
    $('#Showtable').addClass('d-none');
    $("#GradeSlabMasterDesc").val('')
    $('#divAddAssessmentConfig').removeClass('d-none');
    $("#tbl-ConfigListMasterSlab tr").remove();
    GradeSlab.AddRow();
});
$(document).on('click', '#spnBtnBacktoGrid', function () {
    $("#MasterIds").val('');
    DeleteMasterids.length = 0;
    GradeSlab.SlabMasterCount = 0;
    $('#Showtable').removeClass('d-none');
    $('#divAddAssessmentConfig').addClass('d-none');
    $("#GradeSlabId").val('');
    $("#GradeSlabMasterId").val('');
    $("#tbl-ConfigListMasterSlab tr").remove();

});
