﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
//let SessionType = [{
//    SessionId: 1,
//    SessionType: "SESSION1"
//},
//{
//    SessionId: 2,
//    SessionType: "SESSION2"
//}];
let SessionType = [];
let idPresent;
let idAbsent;
let configtype = 0;
let currenPeriodNo;
let studentlist;
let isDailyWeekly;
let hideshowCardCircle;
let pagePermission;
let blockasPerpagePermission;
let IssingleStatusSelectionForDaily = false;
let IssingleStatusSelectionForRoom = false;
let hasPlanner = false;
let isWeeklyHoliday = false;
let isWeeklyHolidayForRoom = false;
let currCount = 0;
let AttendanceParameterShortCut = [];
var view = localStorage.getItem('view');
var save = localStorage.getItem('save');
var cancel = localStorage.getItem('Cancel');
var students = localStorage.getItem('students');
var select = localStorage.getItem('select');
var present = localStorage.getItem('present');
var absent = localStorage.getItem('Absent');
var Others = localStorage.getItem('Others');
var SelectOtherOption = localStorage.getItem('SelectOtherOption');
var WeeklyHoliday = localStorage.getItem('WeeklyHoliday');
var SelectPeriodToAdd = localStorage.getItem('SelectPeriodToAdd');
var NoData = localStorage.getItem('NoData');
var TransportStatus = localStorage.getItem('TransportStatus');
var NoScan = localStorage.getItem('NoScan');
var GroupNotPresentSection = localStorage.getItem('GroupNotPresentSection');
var MarkAttendance = localStorage.getItem('MarkAttendance');
var prevClassAttendance = localStorage.getItem('prevClassAttendance');
let tblheaderforDailyAttendance = localStorage.getItem('dailyAttendance');
let remarkForUnmarkedAttendance = localStorage.getItem('UnmarkedAttendance');
let remarkOnPopoverHeader = localStorage.getItem('RemarkOnPopOverHeader');
let OtherGroups = localStorage.getItem('OtherGroups');
let MyGroups = localStorage.getItem('MyGroups');
let holidayRemark = "";
let holidayRemarkForRoom = "";
let attendanceMarkValue = "";
let roomattendanceMarkValue = "";
let dailyAttendance = function () {
    let studentDailyAttendanceModel;
    let parameterList;
    let gradeSectionDetails;
    let thisForSingleDailyAttendance;
    let curriculumnList = [];
    let AttendanceStudentModel = [];
    let sessionTypeForGrade = []
    init = function () {



        $('#breadcrumbActions').append(`<ul  class="list-pattern">
                         <li class="m-0 mr-3">
                        <select id="ddlCurriculumnList" class="selectpicker ${blockasPerpagePermission}"></select>
                       </li> 
                         <li>
                         <button id="btnSaveDailyAttendance" type="button" class="btn btn-sm btn-primary m-0 mr-3 ${blockasPerpagePermission}">${save}</button>
                          </li>
                        <li class="type-list clslifordailyatt">${view}</li>
                        <li id="liGridView"class="grid-view clslifordailyatt"></li>
                        <li id="liListView" class="list-view active clslifordailyatt"></li>
                          <li class="d-none">
<a href="javascript:void(0);" data-toggle="tooltip" title="${prevClassAttendance}" data-placement="bottom" id="btnWatchPrevRoomAttendance" class="btn btn-sm btn-transparent m-0 mr-3 px-2 d-none mb-xl-n2" data-target="#mpPreviewClassAttendance">
                           <i class="fa-2x fa-history fas text-primary"></i>
                         </a>
                          </li>
                        </ul>`);

        $('.date-picker').datetimepicker({
            format: 'DD-MMM-YYYY',
            maxDate: Date.parse(new Date()),
            minDate: '2000-01-01',
            defaultDate: Date.parse(new Date()),
            ignoreReadonly: true,
            locale: translatedResources.locale
        });






        /*================ DOCUMENT CLICK CHANGE EVENTS ==================*/



        $(document).on("change", "#SectionId", function () {

            $("#SessionType").val($("#SessionType option:eq(1)").val()).selectpicker('refresh').trigger('change');
        });

        $(document).on('change', '#SessionType', function () {
            var day = new Date(globalFunctions.toSystemReadableDate(globalFunctions.toSystemReadableDate($('#txtAttDate').val())));
            let coursegroupId = $('#CourseGroupId').val() == "" || $('#CourseGroupId').val() == undefined ? 0 : $('#CourseGroupId').val();
            dailyAttendance.GetDailyAttendanceDetails(coursegroupId);
            /*$.get('/Attendance/Attendance/CheckforWeeklyHoliday', { selectedDate: $('#txtAttDate').val(), academicYearId: $("#ddlCurriculumnList").val(), schoolGradeId: $('#GradeId').val() }).then((response) => {

              if (response.IsWeeklyHoliday) {
                   globalFunctions.showWarningMessage(response.Remark == "" ? WeeklyHoliday : response.Remark)

               }
               else {
                   let coursegroupId = $('#CourseGroupId').val() == "" || $('#CourseGroupId').val() == undefined ? 0 : $('#CourseGroupId').val();
                   dailyAttendance.GetDailyAttendanceDetails(coursegroupId);

               }
           });*/


        });

        $(document).on('change', '#GradeId', function () {
            $("#SectionId").empty();
            $('#SessionType').empty();

            $('#SectionId').append(`<option value="">${select}</option>`);
            $('#SessionType').append(`<option value="">${select}</option>`);

            dailyAttendance.gradeSectionDetails.filter(x => x.GradeId == $(this).val()).forEach((section) => {
                $('#SectionId').append(`<option value="${section.SectionId}">${section.SectionName}</option>`).selectpicker('refresh');
            });

            SessionType = dailyAttendance.sessionTypeForGrade.filter(x => x.GradeId == $(this).val()).length > 0 ? dailyAttendance.sessionTypeForGrade.filter(x => x.GradeId == $(this).val()) : dailyAttendance.sessionTypeForGrade.filter(x => x.SessionId == 1);
            SessionType.forEach((x) => {
                $('#SessionType').append(`<option value="${x.SessionId}">${x.SessionType}</option>`).selectpicker('refresh');
            });

            $("#SectionId").val($("#SectionId option:eq(1)").val()).selectpicker('refresh').trigger('change');
            /*$.get("/Attendance/Attendance/GetGradeSessionByGradeId", { schoolGradeId: $(this).val() }).then((response) => {
                SessionType = response;
                SessionType.forEach((x) => {
                    $('#SessionType').append(`<option value="${x.SessionId}">${x.SessionType}</option>`).selectpicker('refresh');
                });

                $("#SectionId").val($("#SectionId option:eq(1)").val()).selectpicker('refresh').trigger('change');

            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });*/




        });

        $(document).on('blur', '#txtAttDate', function () {
            var day = new Date($(this).val());
            let corId = configtype == 1 ? $('#CourseGroupId').val() : 0;
            let isCallFromRoom = configtype == 1 ? true : false;

            isWeeklyHoliday = false;
            switch (configtype) {
                case 0:
                    dailyAttendance.GetDailyAttendanceDetails();
                    $("#btnSaveDailyAttendance").removeClass("d-none")
                    break;
                case 1:
                    //coursegroupId = $('#CourseGroupId').val();
                    //dailyAttendance.GetDailyAttendanceDetails(coursegroupId);
                    classAttendance.getCourseList()
                    $("#btnSaveDailyAttendance").removeClass("d-none")
                    break;
                case 2:

                    let coursegroupId = $('#CourseGroupId').val() == "" || $('#CourseGroupId').val() == undefined ? 0 : $('#CourseGroupId').val();
                    if (!$('#GradeId').parent().parent().hasClass('d-none')) {
                        dailyAttendance.GetDailyAttendanceDetails(coursegroupId);

                    }
                    else {

                        /*if (coursegroupId == "" || coursegroupId == undefined) {
                            if ($('#btnroomAttendance').hasClass('active')) {
                                globalFunctions.showWarningMessage("Group is not present in the selection")

                            }

                        }
                        else {
                            classAttendance.getClassListforMergeConfig(coursegroupId)
                        }*/


                    }

                    $("#btnSaveDailyAttendance").removeClass("d-none")
                    break;
            }
            /* $.get('/Attendance/Attendance/CheckforWeeklyHoliday', { selectedDate: $(this).val(), academicYearId: $("#ddlCurriculumnList").val(), schoolGradeId: $('#GradeId').val(), courseId: corId, IsCallFrmRoom: isCallFromRoom }).then((response) => {
                 debugger;
                 if (response.IsWeeklyHoliday) {
 
                     globalFunctions.showWarningMessage(response.Remark == "" ? WeeklyHoliday : response.Remark)
                     isWeeklyHoliday = response.IsWeeklyHoliday;
                     $("#btnSaveDailyAttendance").addClass("d-none")
                     $("#ulStudentAttendanceDetails").empty()
                     $("#selectall").parent().hide()
                     $("#tblStudentDailyAttendance").empty()
                 }
                 else {
                     isWeeklyHoliday = false;
                     switch (configtype) {
                         case 0:
                             dailyAttendance.GetDailyAttendanceDetails();
                             $("#btnSaveDailyAttendance").removeClass("d-none")
                             break;
                         case 1:
                             //coursegroupId = $('#CourseGroupId').val();
                             //dailyAttendance.GetDailyAttendanceDetails(coursegroupId);
                             classAttendance.getCourseList()
                             $("#btnSaveDailyAttendance").removeClass("d-none")
                             break;
                         case 2:
 
                             let coursegroupId = $('#CourseGroupId').val() == "" || $('#CourseGroupId').val() == undefined ? 0 : $('#CourseGroupId').val();
                             if (!$('#GradeId').parent().parent().hasClass('d-none')) {
                                 dailyAttendance.GetDailyAttendanceDetails(coursegroupId);
 
                             }
                             else {
 
                               
 
 
                             }
 
                             $("#btnSaveDailyAttendance").removeClass("d-none")
                             break;
                     }
                 }
 
             })*/

        });

        $(document).on("blur", "#txtRoomAttDate", function () {
            isWeeklyHolidayForRoom = false;
            classAttendance.getCourseList()
            $("#btnSaveDailyAttendance").removeClass("d-none")
            /*$.get('/Attendance/Attendance/CheckforWeeklyHoliday', { selectedDate: $(this).val(), courseId: $('#CourseGroupId').val(), IsCallFrmRoom: true }).then((response) => {

                if (response.IsWeeklyHoliday) {
                    globalFunctions.showWarningMessage(response.Remark == "" ? WeeklyHoliday : response.Remark)
                    isWeeklyHoliday = response;
                    $("#btnSaveDailyAttendance").addClass("d-none")

                }
                else {
                    isWeeklyHoliday = false;
                    classAttendance.getCourseList()
                    $("#btnSaveDailyAttendance").removeClass("d-none")
                }

            })*/



        })

        $(document).on('change', '.clsChkforstudent', function () {
            //data - sessionId="${m.SessionId}" data - stuId
            if (configtype == 0 || configtype == 2) {
                $('.clsChkforstudent:checked').length > 1 ? $('#divFloatNav').removeClass('d-none') : $('#divFloatNav').addClass('d-none');
            }
            else if (configtype == 1) {

                $('.clsChkforstudent:checked').length > 1 ? $('#divFloatNavforRoom').removeClass('d-none') : $('#divFloatNavforRoom').addClass('d-none');
            }
            let studentId = $(this).attr('data-stuId');
            let sessionId = $(this).attr('data-sessionId');
            if ($(this).is(":checked")) {
                $('.clsChk_' + studentId + '_' + sessionId).prop('checked', true);
                $('.clsChk_' + studentId).prop('checked', true);
            }
            else {
                $('.clsChk_' + studentId + '_' + sessionId).prop('checked', false);
                $('.clsChk_' + studentId).prop('checked', false);
            }

        });

        $(document).on('change', '.dt-checkboxes', function () {
            //data - sessionId="${m.SessionId}" data - stuId
            if (configtype == 0 || configtype == 2) {
                $('.dt-checkboxes:checked').length > 1 ? $('#divFloatNav').removeClass('d-none') : $('#divFloatNav').addClass('d-none');

            }
            else if (configtype == 1) {

                $('.dt-checkboxes:checked').length > 1 ? $('#divFloatNavforRoom').removeClass('d-none') : $('#divFloatNavforRoom').addClass('d-none');
                //$('.dt-checkboxes:checked').length > 1 ? $('#liMarkListAttendance').removeClass('d-none') : $('#liMarkListAttendance').addClass('d-none');
            }
            let studentId = $(this).attr('data-stuId');
            let sessionId = $(this).attr('data-sessionId');
            if ($(this).is(":checked")) {
                $('.clsChk_' + studentId + '_' + sessionId).prop('checked', true);
                $('.clsChk_' + studentId).prop('checked', true);
                $('#selectStud_' + studentId).prop('checked', true);
            }
            else {
                $('.clsChk_' + studentId + '_' + sessionId).prop('checked', false);
                $('.clsChk_' + studentId).prop('checked', false);
                $('#selectStud_' + studentId).prop('checked', false);
            }

        });
        $(document).on('click', '#btnSaveDailyAttendance', function () {

            if (configtype == 0) {


                $.post('/Attendance/Attendance/SaveDailyAttendance', { __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(), objlstAttendanceStudent: dailyAttendance.AttendanceStudentModel, academicYearId: $("#ddlCurriculumnList").val() })
                    .then((response) => {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        if (response.Success) {
                            dailyAttendance.GetDailyAttendanceDetails();
                            $('#divFloatNav').addClass('d-none')


                        }

                    })
                    .catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
            }
            else if (configtype == 1) {
                let coursegroupId = hasPlanner ? classAttendance.frmMyplannerCourseId : $('#CourseGroupId').val();
                let entryDate = hasPlanner ? classAttendance.frmMyplannerasonDate : (configtype == 2 || $("#txtAttDate").parent().parent().hasClass("divDailyparameterddl") ? globalFunctions.toSystemReadableDate($("#txtRoomAttDate").val()) : globalFunctions.toSystemReadableDate($('#txtAttDate').val()));
                let objlstofStudentAttendanceDetails = classAttendance.ClassAttendanceModuleStruct.map((x) => {

                    return { logId: 0, DetailId: x.RegisterId, StudentId: x.StudentId, RoomDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()), SGR_ID: coursegroupId, PeriodNo: x.SessionId, Status: x.ParameterId, Remark: x.Remark }

                })

                if (objlstofStudentAttendanceDetails.length > 0) {


                    $.post('/Attendance/Attendance/SaveClassAttendance', { __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(), objClassListDetails: objlstofStudentAttendanceDetails, schoolGroupId: coursegroupId, isDailyWeeklyTimeTable: isDailyWeekly, entryDate: entryDate })
                        .then((response) => {
                            globalFunctions.showMessage(response.NotificationType, response.Message);
                            if (response.Success) {
                                dailyAttendance.GetDailyAttendanceDetails(coursegroupId, entryDate, hasPlanner);
                                $('#divFloatNavforRoom').addClass('d-none')


                            }

                        })
                        .catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
                }

            }
            else if (configtype == 2) {
                let coursegroupId = $('#CourseGroupId').val();
                if ($('#btndailyAttendanceListing').hasClass('active')) {
                    $.post('/Attendance/Attendance/SaveDailyAttendance', { __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(), objlstAttendanceStudent: dailyAttendance.AttendanceStudentModel, academicYearId: $("#ddlCurriculumnList").val() })
                        .then((response) => {
                            globalFunctions.showMessage(response.NotificationType, response.Message);
                            if (response.Success) {
                                dailyAttendance.GetDailyAttendanceDetails(coursegroupId);
                                $('#divFloatNav').addClass('d-none')

                            }

                        })
                        .catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
                }
                else if ($('#btnroomAttendance').hasClass('active')) {

                    let objlstofStudentAttendanceDetails = classAttendance.ClassAttendanceModuleStruct.map((x) => {

                        return { logId: 0, DetailId: x.RegisterId, StudentId: x.StudentId, RoomDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()), SGR_ID: coursegroupId, PeriodNo: x.SessionId, Status: x.ParameterId, Remark: x.Remark }

                    })
                    if (objlstofStudentAttendanceDetails.length > 0) {
                        $.post('/Attendance/Attendance/SaveClassAttendance', { __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(), objClassListDetails: objlstofStudentAttendanceDetails, schoolGroupId: coursegroupId, isDailyWeeklyTimeTable: isDailyWeekly, entryDate: globalFunctions.toSystemReadableDate($('#txtRoomAttDate').val()) })
                            .then((response) => {
                                globalFunctions.showMessage(response.NotificationType, response.Message);
                                if (response.Success) {
                                    classAttendance.getClassListforMergeConfig(coursegroupId);
                                    $('#divFloatNavforRoom').addClass('d-none')


                                }

                            })
                            .catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
                    }
                }

            }


        });

        $(document).on('click', '.grid-wrapper li div.attend-stud-footer', function () {
            var _this = $(this);
            setTimeout(function () {
                _this.closest('li').toggleClass('expanded')
            }, 90);

            _this.closest('li').find('.expander').stop().slideToggle().parent('li').siblings('li').removeClass('expanded').find('.expander').stop().slideUp();
        });
        $(document).on('click', '.mark-all-stud', function () {
            $(this).closest(".grid-wrapper").find(".all-stud-expander").removeClass("d-none").addClass("animate slideIn");
        });
        $(document).on('click', '#btnlistAttMarking', function () {
            $('#divMarkclassAttlst').removeClass('d-none');



        })
        $(document).on('click', '#anchrCloseAtt', function () {
            $('#divMarkclassAttlst').addClass('d-none');
        });

        $(document).on('click', '.closeAttendance', function () {
            $(this).closest(".all-stud-expander").removeClass("animate slideIn").addClass("d-none");
        });
        $(document).on('change', '#selectAllList', function () {

            if ($(this).is(":checked")) {
                $('#selectall').prop('checked', true);

                $('.clsChkforstudent').prop('checked', true);
                $('.dt-checkboxes').prop('checked', true);
                if (configtype == 0 || configtype == 2) {
                    $('#divFloatNav').removeClass('d-none');
                }
                else {


                    $('#divFloatNavforRoom').removeClass('d-none');


                }
            }
            else {
                $('#selectall').prop('checked', false);
                $('.closeAttendance').trigger('click');
                $('.clsChkforstudent').prop('checked', false);
                $('.dt-checkboxes').prop('checked', false);

                if (configtype == 0 || configtype == 2) {
                    $('#divFloatNav').addClass('d-none');
                }
                else {

                    $('#divFloatNavforRoom').addClass('d-none');
                }
            }

        });

        $(document).on('change', '#selectall', function () {

            if ($(this).is(":checked")) {
                $('#selectAllList').prop('checked', true);

                $('.clsChkforstudent').prop('checked', true);
                $('.dt-checkboxes').prop('checked', true);
                if (configtype == 0 || configtype == 2) {
                    $('#divFloatNav').removeClass('d-none');
                }
                else {


                    $('#divFloatNavforRoom').removeClass('d-none');
                }

            }
            else {
                $('#selectAllList').prop('checked', false);
                $('.closeAttendance').trigger('click');
                $('.clsChkforstudent').prop('checked', false);
                $('.dt-checkboxes').prop('checked', false);
                if (configtype == 0 || configtype == 2) {
                    $('#divFloatNav').addClass('d-none');
                }
                else {
                    $('#divFloatNavforRoom').addClass('d-none');
                }

            }



        });
        $(document).on('click', '.list-pattern .list-view', function () {
            let gridView = $('.grid-list-card .grid-wrapper');
            let listView = $('.grid-list-card .list-wrapper');
            $('.list-pattern .grid-view').removeClass('active');
            $('.list-pattern .list-view').addClass('active');
            gridView.addClass('d-none');
            listView.removeClass('d-none');
            if (configtype == 2) {
                if ($('#btnroomAttendance').hasClass('active')) {
                    $('.divDailyparameterddl').addClass('d-none');
                    $('.divRoomParameterddl').removeClass('d-none');
                }
                else if ($('#btndailyAttendanceListing').hasClass('active')) {
                    $('.divDailyparameterddl').removeClass('d-none');
                    $('.divRoomParameterddl').addClass('d-none');
                }
            }
        });

        $(document).on('click', '.list-pattern .grid-view', function () {

            let gridView = $('.grid-list-card .grid-wrapper');
            let listView = $('.grid-list-card .list-wrapper');
            $('.list-pattern .list-view').removeClass('active');
            $('.list-pattern .grid-view').addClass('active');
            gridView.removeClass('d-none');
            listView.addClass('d-none');
            if (configtype == 2) {
                $('.divDailyparameterddl').removeClass('d-none');
                $('.divRoomParameterddl').addClass('d-none');
            }
        });
        $(document).on('click', '.clsDailySingleOtherOption', function () {
            IssingleStatusSelectionForDaily = true;
            $(this).attr("data-ispresent") != undefined ? $("#ddlOtherAttendanceList").parent().parent().addClass("d-none") : $("#ddlOtherAttendanceList").parent().parent().removeClass("d-none")
            let dataRemark = $(this).prev().find(".clsdatastatus").attr("data-original-title");
            $("#txtoptionalRemark").val(dataRemark);
            dailyAttendance.thisForSingleDailyAttendance = $(this);

        })

        $(document).on('click', '.clsRoomSingleOtherOption', function () {
            IssingleStatusSelectionForRoom = true;
            let datastatus = $(this).prev().find(".clsdatastatus").attr("data-status")
            $(this).attr("data-ispresent", datastatus)
            $(this).attr("data-ispresent") != undefined ? $("#ddlOtherAttendanceListforRoom").parent().parent().addClass("d-none") : $("#ddlOtherAttendanceListforRoom").parent().parent().removeClass("d-none")
            let dataRemark = $(this).prev().find(".clsdatastatus").attr("data-original-title");
            $("#txtoptionalRemarkForRoom").val(dataRemark);
            classAttendance.thisforSingleRoomAttendance = $(this);

        })

        $(document).on('click', '.btnModelpopUpSave', function () {


            let markDetails = $('#ddlOtherAttendanceList').find(':selected');
            if (configtype == 0) {

                if ($(dailyAttendance.thisForSingleDailyAttendance).attr("data-ispresent") == undefined && ($('#ddlOtherAttendanceList').val() == "Select" || $('#ddlOtherAttendanceList').val() == "" || $('#ddlOtherAttendanceList').val() == null)) {

                    globalFunctions.showWarningMessage(SelectOtherOption)
                    return false;


                }
                if (IssingleStatusSelectionForDaily) {
                    if ($(dailyAttendance.thisForSingleDailyAttendance).attr("data-ispresent") == undefined) {
                        $(dailyAttendance.thisForSingleDailyAttendance).attr('data-other', $('#ddlOtherAttendanceList').val())
                    }
                    dailyAttendance.dropdownMenu($(dailyAttendance.thisForSingleDailyAttendance))
                    IssingleStatusSelectionForDaily = false;
                }
                else {
                    dailyAttendance.AppendStatusForSelected(markDetails)

                }
                $('#mpOtherOptions').modal('toggle')
            }
            else if (configtype == 1) {

                if ($(classAttendance.thisforSingleRoomAttendance).attr("data-ispresent") == undefined && ($('#ddlOtherAttendanceListforRoom').val() == "Select" || $('#ddlOtherAttendanceList').val() == "" || $('#ddlOtherAttendanceListforRoom').val() == null)) {

                    globalFunctions.showWarningMessage(SelectOtherOption)
                    return false;

                }
                if (IssingleStatusSelectionForRoom) {
                    if ($(classAttendance.thisforSingleRoomAttendance).attr("data-ispresent") == undefined) {
                        $(classAttendance.thisforSingleRoomAttendance).attr('data-other', $('#ddlOtherAttendanceListforRoom').val())
                    }
                    classAttendance.dropdownMenuRoom($(classAttendance.thisforSingleRoomAttendance))

                    IssingleStatusSelectionForRoom = false;
                } else {


                    markDetails = $('#ddlOtherAttendanceListforRoom').find(':selected')
                    classAttendance.AppendStatusForSelectedRoom(markDetails)

                }
                $('#mpOtherforRoomOptions').modal('toggle')
            }
            else if (configtype == 2) {

                if ($('#btnroomAttendance').hasClass('active')) {

                    if ($(classAttendance.thisforSingleRoomAttendance).attr("data-ispresent") == undefined && ($('#ddlOtherAttendanceListforRoom').val() == "Select" || $('#ddlOtherAttendanceListforRoom').val() == "" || $('#ddlOtherAttendanceListforRoom').val() == null)) {

                        globalFunctions.showWarningMessage(SelectOtherOption)
                        return false;

                    }

                    if (IssingleStatusSelectionForRoom) {
                        if ($(classAttendance.thisforSingleRoomAttendance).attr("data-ispresent") == undefined) {
                            $(classAttendance.thisforSingleRoomAttendance).attr('data-other', $('#ddlOtherAttendanceListforRoom').val())
                        }
                        classAttendance.dropdownMenuRoom($(classAttendance.thisforSingleRoomAttendance))

                        IssingleStatusSelectionForRoom = false;
                    }
                    else {
                        if (!$('.clschkPeriodHeader').is(":checked")) {
                            globalFunctions.showWarningMessage(SelectPeriodToAdd)
                            return false
                        }
                        markDetails = $('#ddlOtherAttendanceListforRoom').find(':selected')
                        classAttendance.AppendStatusForSelectedRoom(markDetails)
                    }
                    $('#mpOtherforRoomOptions').modal('toggle')
                }
                else if ($('#btndailyAttendanceListing').hasClass('active')) {
                    if ($(dailyAttendance.thisForSingleDailyAttendance).attr("data-ispresent") == undefined && ($('#ddlOtherAttendanceList').val() == "Select" || $('#ddlOtherAttendanceList').val() == "" || $('#ddlOtherAttendanceList').val() == null)) {

                        globalFunctions.showWarningMessage(SelectOtherOption)
                        return false;

                    }
                    if (IssingleStatusSelectionForDaily) {
                        if ($(dailyAttendance.thisForSingleDailyAttendance).attr("data-ispresent") == undefined) {
                            $(dailyAttendance.thisForSingleDailyAttendance).attr('data-other', $('#ddlOtherAttendanceList').val())
                        }
                        dailyAttendance.dropdownMenu($(dailyAttendance.thisForSingleDailyAttendance))
                        IssingleStatusSelectionForDaily = false;
                    }
                    else {
                        dailyAttendance.AppendStatusForSelected(markDetails)
                    }


                    $('#mpOtherOptions').modal('toggle')
                }
            }

            $('#ddlOtherAttendanceList').val("").selectpicker('refresh')
            $('#ddlOtherAttendanceListforRoom').val("").selectpicker('refresh')
            $('#txtoptionalRemarkForRoom').val("");
            $('#txtoptionalRemark').val("");
            $("#ddlOtherAttendanceList").parent().parent().removeClass("d-none");
            $("#ddlOtherAttendanceListforRoom").parent().parent().removeClass("d-none");


        });

        $(document).on('click', '.btnModelCancel', function () {

            $('#ddlOtherAttendanceList').val("").selectpicker('refresh')
            $('#ddlOtherAttendanceListforRoom').val("").selectpicker('refresh')
            $("#ddlOtherAttendanceList").parent().parent().removeClass("d-none")
            $("#ddlOtherAttendanceListforRoom").parent().parent().removeClass("d-none")

            $('#txtoptionalRemarkForRoom').val("");
            $('#txtoptionalRemark').val("");

            IssingleStatusSelectionForDaily = false;
            IssingleStatusSelectionForRoom = false;

        });

        //$(document).on('click', '#btnroomAttendance', function () {
        //    $('.divRoomParameterddl').removeClass('d-none');
        //    $('.divDailyparameterddl').addClass('d-none');

        //});

        /*====================== DOCUMENT CLICK CHANGE EVENTS ======================*/

        /*============================== SEARCHING  ================================*/



        $(document).on('keyup', '.searchfield', function (e) {

            if (e.which == 8) {
                var filter = $(this).val();
                $("#ulStudentAttendanceDetails li").each(function () {
                    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                        $(this).hide();
                    } else {
                        $(this).show()
                    }
                });

                var value = $(this).val().toLowerCase();
                $("#tblStudentDailyAttendance tr").not('thead tr').filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });

                $('#tblRoomAttendance tr').not('thead tr').filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)

                });

                /*to display no data*/
                $('#ulStudentAttendanceDetails span.no-data-found').remove()
                $('#trDailyNotFound').remove()
                $('#trRoomNotFound').remove()


                //if ($('#ulStudentAttendanceDetails li').filter(':visible').length == 0) {

                //    $('#ulStudentAttendanceDetails').append(`<span class='no-data-found text-bold d-block mx-auto my-auto'>No data available</span>`)
                //    let trDailyNF = `<tr id="trDailyNotFound"><td></td><td class="sub-heading">No data available 
                //   </td><td></td></tr>`;

                //    let trRoomNF = `<tr id="trRoomNotFound"><td></td><td class="sub-heading"> No data available   
                //   </td><td></td></tr>`;

                //    $('#tblStudentDailyAttendance tbody').append(trDailyNF);

                //    $('#tblRoomAttendance tbody').append(trRoomNF);


                //}

            }


        });

        $(document).on('keypress', '.searchfield', function (e) {

            if (e.which == 13) {
                var filter = $(this).val();

                var value = $(this).val().toLowerCase();

                $("#ulStudentAttendanceDetails li").each(function () {
                    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                        //if (!$(this).text().includes(value)) {
                        $(this).addClass('d-none');
                    } else {
                        $(this).removeClass('d-none')
                    }
                });


                $("#tblStudentDailyAttendance tr").not('thead tr').filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });

                $('#tblRoomAttendance tr').not('thead tr').filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)

                });

                /*to display no data*/

                $('.view-heading').removeClass('d-none')
                $('#ulStudentAttendanceDetails span.no-data-found').remove()
                $('#trDailyNotFound').remove()
                $('#trRoomNotFound').remove()

                if ($("#liGridView").hasClass("active") && $("#btndailyAttendanceListing").hasClass("active")) {


                    if ($('#ulStudentAttendanceDetails li:visible').length == 0) {

                        $('#ulStudentAttendanceDetails').append(`<span class='no-data-found text-bold d-block mx-auto my-auto'>${NoData}</span>`)
                        let trDailyNF = `<tr id="trDailyNotFound"><td></td><td class="sub-heading">${NoData}
                   </td><td></td></tr>`;

                        let trRoomNF = `<tr id="trRoomNotFound"><td></td><td class="sub-heading"> ${NoData}  
                   </td><td></td></tr>`;

                        $('#tblStudentDailyAttendance tbody').append(trDailyNF);

                        $('.view-heading').addClass('d-none')
                    }
                    else {
                        $('#ulStudentAttendanceDetails span.no-data-found').remove()
                        $('#trDailyNotFound').remove()

                    }
                }
                else if ($("#liListView").hasClass("active") && $("#btndailyAttendanceListing").hasClass("active")) {
                    if ($('#tblStudentDailyAttendance tbody tr:visible').length == 0) {

                        $('#ulStudentAttendanceDetails').append(`<span class='no-data-found text-bold d-block mx-auto my-auto'>${NoData}</span>`)
                        let trDailyNF = `<tr id="trDailyNotFound"><td></td><td class="sub-heading">${NoData} 
                   </td><td></td></tr>`;

                        let trRoomNF = `<tr id="trRoomNotFound"><td></td><td class="sub-heading"> ${NoData}  
                   </td><td></td></tr>`;

                        $('#tblStudentDailyAttendance tbody').append(trDailyNF);

                        $('.view-heading').addClass('d-none')
                    }
                    else {
                        $('#ulStudentAttendanceDetails span.no-data-found').remove()
                        $('#trDailyNotFound').remove()

                    }
                }

                if ($("#btnroomAttendance").hasClass("active")) {
                    if ($('#tblRoomAttendance tbody tr:visible').length == 0) {

                        let trRoomNF = `<tr id="trRoomNotFound"><td></td><td class="sub-heading"> ${NoData}   
                   </td><td></td></tr>`;

                        $('#tblRoomAttendance tbody').append(trRoomNF);

                        $('.view-heading').addClass('d-none')
                    }
                    else {

                        $('#trRoomNotFound').remove()
                    }
                }



            }


        });
        /*============================== SEARCHING  ================================*/

        $(document).on("change", "#ddlCurriculumnList", function () {

            let acdId = $(this).val();
            if (acdId == "" && configtype == 2) {
                configtype = 1;
                $(".divDailyparameterddl").addClass("d-none")
                $(".divRoomParameterddl").removeClass("d-none")
                classAttendance.getCourseList();
            }
            else {
                dailyAttendance.getAttGradeSection(acdId)
            }

        });
        switch (configtype) {
            case 0:
                //dailyAttendance.getAttGradeSection();
                dailyAttendance.getCurriculumn();
                break;
            case 1:
                classAttendance.getCourseList();
                $("#ddlCurriculumnList").parent().addClass("d-none")
                break;
            case 2:

                // dailyAttendance.getAttGradeSection();
                dailyAttendance.getCurriculumn();
                break;
        }
    },
        onWindowLoad = function () {
            $(".sidebar-fixed .list-group").mCustomScrollbar({
                setHeight: "86%",
                autoExpandScrollbar: true,
                autoHideScrollbar: true,
                scrollbarPosition: "outside"
            });

            $('.loader-wrapper').fadeOut(500, function () {
                //$('.loader-wrapper').remove();
            });

            $('.list-pattern .list-view').click(function () {
                let gridView = $('.grid-list-card .grid-wrapper');
                let listView = $('.grid-list-card .list-wrapper');
                $('.list-pattern .grid-view').removeClass('active');
                $('.list-pattern .list-view').addClass('active');
                gridView.addClass('hide-item ');
                listView.removeClass('hide-item');
            });
            $('.list-pattern .grid-view').click(function () {
                let gridView = $('.grid-list-card .grid-wrapper');
                let listView = $('.grid-list-card .list-wrapper');
                $('.list-pattern .list-view').removeClass('active');
                $('.list-pattern .grid-view').addClass('active');
                gridView.removeClass('hide-item');
                listView.addClass('hide-item');
            });

        },
        getCurriculumn = function () {
            $("#ddlCurriculumnList").empty();
            $('#ddlCurriculumnList').append(`<option value="">${localStorage.getItem('select')}</option>`);
            currCount = dailyAttendance.curriculumnList.length;
            dailyAttendance.curriculumnList.forEach((x) => {
                $('#ddlCurriculumnList').append(`<option value="${x.AcademicYearId}">${x.CurriculumnName}</option>`).selectpicker('refresh');

            });

            if (currCount == 1 || currCount == 0) {
                $("#ddlCurriculumnList").parent().addClass("d-none")
            }
            $("#ddlCurriculumnList").val($("#ddlCurriculumnList option:eq(1)").val()).selectpicker('refresh').trigger("change");
            /* $.get('/Attendance/Attendance/GetCurriculumByStaffId').then((response) => {
                 currCount = response.length;
                 response.forEach((x) => {
                     $('#ddlCurriculumnList').append(`<option value="${x.AcademicYearId}">${x.CurriculumnName}</option>`).selectpicker('refresh');
 
                 });
 
                 if (currCount == 1 || currCount == 0) {
                     $("#ddlCurriculumnList").parent().addClass("d-none")
                 }
 
                 $("#ddlCurriculumnList").val($("#ddlCurriculumnList option:eq(1)").val()).selectpicker('refresh').trigger('change')
             }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });*/
        },
        getAttGradeSection = function (academicYearId) {
            $.get('/Attendance/Attendance/GetAttendanceGradeSection', { academicYearId: academicYearId }).then((response) => {
                AttendanceParameterShortCut = response.AttendanceParameterShortCut;
                dailyAttendance.parameterList = response.parameterList;
                idPresent = dailyAttendance.parameterList.filter(x => x.Text.toUpperCase() == "PRESENT").length > 0 ? dailyAttendance.parameterList.filter(x => x.Text.toUpperCase() == "PRESENT")[0].Value : 0;
                idAbsent = dailyAttendance.parameterList.filter(x => x.Text.toUpperCase() == "ABSENT").length > 0 ? dailyAttendance.parameterList.filter(x => x.Text.toUpperCase() == "ABSENT")[0].Value : 0;
                configtype = response.configtype;
                dailyAttendance.gradeSectionDetails = response.gradeSectionDetails;
                dailyAttendance.sessionTypeForGrade = response.gradeSessionType;
                // dailyAttendance.curriculumnList = response.curriculumnList;

                $('#GradeId').empty();
                $('#GradeId').append(`<option value="">${localStorage.getItem('select')}</option>`);

                dailyAttendance.gradeSectionDetails.filter((i, idx) => {

                    var Gidx = dailyAttendance.gradeSectionDetails.findIndex(x => x.GradeId == i.GradeId);

                    return Gidx == idx
                }).forEach((x) => {
                    $('#GradeId').append(`<option value="${x.GradeId}">${x.GradeName}</option>`).selectpicker('refresh');

                });

                //dailyAttendance.gradeSectionDetails.forEach((x) => {
                //    $('#GradeId').append(`<option value="${x.GradeId}">${x.GradeName}</option>`).selectpicker('refresh');

                //});
                $("#GradeId").val($("#GradeId option:eq(1)").val()).selectpicker('refresh').trigger('change')
                dailyAttendance.parameterList.filter(x => x.Value != idAbsent && x.Value != idAbsent).filter((pl) => {

                    return !AttendanceParameterShortCut.some((sc) => { return pl.Value === sc.Value })

                }).map((x) => { return { Value: x.Value, Text: x.Text } }).forEach((p) => {

                    $('#ddlOtherAttendanceList').append(`<option data-other="${p.Value}" value="${p.Value}">${p.Text}</option>`).selectpicker('refresh');
                    $('#ddlOtherAttendanceListforRoom').append(`<option data-other="${p.Value}" value="${p.Value}">${p.Text}</option>`).selectpicker('refresh');
                })

                /*if (configtype == 2) {
                    classAttendance.getCourseList();
                }*/

            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });

        },

        GetDailyAttendanceDetails = function (courseGroupId, asOndate, hasPlanner) {
            let postasOnDate = asOndate != "" && asOndate != undefined ? asOndate : globalFunctions.toSystemReadableDate($('#txtAttDate').val())
            configtype = hasPlanner ? 1 : configtype;

            $.post('/Attendance/Attendance/GetDailyAttendance', {
                gradeId: $('#GradeId').val(),
                sectionId: $('#SectionId').val(),
                sessionType: $('#SessionType').val(),
                asOnDate: postasOnDate,
                configType: configtype,
                courseGroupId: courseGroupId,
                academicYearId: $("#ddlCurriculumnList").val()
            }).then((response) => {
                if (response.IsWeeklyHoliday) {
                    globalFunctions.showWarningMessage(response.Remark == "" ? WeeklyHoliday : response.Remark)
                    isWeeklyHoliday = response.IsWeeklyHoliday;
                    holidayRemark = response.Remark == "" ? WeeklyHoliday : response.Remark;
                    $("#btnSaveDailyAttendance").addClass("d-none")
                    $("#ulStudentAttendanceDetails").empty()
                    $("#selectall").parent().hide()
                    $("#tblStudentDailyAttendance").empty()
                    return false;
                }
                configtype = hasPlanner ? 1 : configtype;
                dailyAttendance.AttendanceStudentModel.length = 0
                $('.searchfield').val("")
                let displayDailyAsPerConfig = configtype == 0 || configtype == 2 ? "" : "d-none";
                let displayRoomAsPerConfig = configtype == 1 ? "" : "d-none";

                $('#divDailyAttendance').html(response);

                $('#thCurrentDate').attr('data-title', globalFunctions.toSystemReadableDate($('#txtAttDate').val()));
                $('#thCurrentDate').html(globalFunctions.toSystemReadableDate($('#txtAttDate').val()));

                $('#aHref-selectAllPresent').attr('data-other', idPresent)
                $('#aHref-selectAllAbsent').attr('data-other', idAbsent)

                $('#divmrkAllDailyAttendance').addClass(displayDailyAsPerConfig)
                $('#divmrkAllRoomAttendance').addClass(displayRoomAsPerConfig)



                let otherCategoryForAll = ``;
                if (configtype == 0 || configtype == 2) {


                    dailyAttendance.parameterList.forEach((p) => {
                        otherCategoryForAll += `<a class="dropdown-item" data-other="${p.Value}" href="javascript:void(0)" onclick="dailyAttendance.AppendStatusForSelected(this)" >${p.Text}</a>`
                    })
                    $('#divOtherCategoryList').html(otherCategoryForAll);
                }
                else if (configtype == 1) {
                    dailyAttendance.studentDailyAttendanceModel = classAttendance.classAttendanceDetails;
                    hideshowCardCircle = classAttendance.classAttendanceHeader.length > 1 ? "d-none" : "d-none";
                    let mrkAllclassHeader = ``;
                    let mrkAllclassBody = ``;


                    classAttendance.classAttendanceHeader.forEach((th) => {
                        if (th.IsActive) {

                            dailyAttendance.studentDailyAttendanceModel.filter(x => x.SessionId == th.PeriodNo).forEach((x) => {

                                dailyAttendance.AttendanceStudentModel.push({
                                    AcademicYearId: x.AcademicYearId
                                    , AsOnDate: x.AsOnDate
                                    , DisplayDate: x.DisplayDate
                                    , ParameterId: x.ParameterId
                                    , RegisterId: x.RegisterId
                                    , Remark: x.Remark
                                    , SessionId: x.SessionId
                                    , StudentId: x.StudentId
                                    , StudentImageUrl: x.StudentImageUrl
                                    , StudentName: x.StudentName
                                    , StudentNumber: x.StudentNumber
                                    , MLDisplayDate: x.MLDisplayDate
                                })

                            })
                        }

                        let activeheaderCss = th.IsActive == true ? "" : "bg-light";
                        let togglecss = th.IsActive == true ? "dropdown" : "";
                        let spnAppendStatuson = th.IsActive == true ? `spnClassSelectAll_${th.PeriodNo}` : "";
                        mrkAllclassHeader += ` <th  data-title="PERIOD-${th.PeriodNo}" class="text-center clsthCurrentClassPeriod ${activeheaderCss}">PERIOD-${th.PeriodNo}</th>`
                        mrkAllclassBody += ` <td data-title="" class="text-center ${activeheaderCss}">
                                             <!--Attendance dropdown event starts-->
                                             <a href="javascript:void(0)" class="d-inline-block nav-link p-0 text-primary" data-toggle="${togglecss}" aria-haspopup="true" aria-expanded="false">
                                                 <span id="${spnAppendStatuson}" class="${spnAppendStatuson}">
                                                     <i class="fas fa-2x fa-plus-circle text-default"></i>
                                                 </span>

                                             </a>
                                             <div class="dropdown-menu dropdown-menu-right z-depth-2 pt-2 pb-2 w-25  ${blockasPerpagePermission}" style="z-index: 1034;">
                                                 <a class="dropdown-item"  data-SessionId="${th.PeriodNo}" data-isPresent="1" data-other="${idPresent}" onclick="classAttendance.AppendStatusForSelected(this)" href="javascript:void(0)">${localStorage.getItem('present')}</a>
                                                 <a class="dropdown-item"  data-SessionId="${th.PeriodNo}" data-isPresent="0" data-other="${idAbsent}" onclick="classAttendance.AppendStatusForSelected(this)" href="javascript:void(0)"> ${localStorage.getItem('absent')}</a>
                                                 <div class="dropdown-divider mb-0"></div>
                                                 <h6 class="dropdown-header bg-light p-1 pl-2 mb-1 text-dark"><small class="text-uppercase"><strong>Other</strong></small></h6>
                                                 <div  class="">
                                                   ${classAttendance.generateParameterHeaderList(th.PeriodNo)}
                                                 </div>



                                             </div>
                                             <!--Attendance dropdown event starts-->
                                              </td>`
                    });

                    //currenPeriodNo = classAttendance.classAttendanceHeader.filter(x => x.IsActive == true).length > 0 ? classAttendance.classAttendanceHeader.filter(x => x.IsActive == true)[0].PeriodNo : 0
                    $('#tblMarkAllClassAttendance thead tr').html(mrkAllclassHeader);
                    $('#tblMarkAllClassAttendance tbody tr').html(mrkAllclassBody);
                    $('#liRoomAttendance a').trigger('click')

                }



                let liStudentDetails = ``;
                //var studentlist = dailyAttendance.studentDailyAttendanceModel.map((x) => { return { StudentId : x.StudentId, StudentName :x.StudentName, StudentImageUrl:x.StudentImageUrl, GradeId:x.GradeId, SectionId:x.SectionId } });

                studentlist = dailyAttendance.studentDailyAttendanceModel.filter((i, idx) => {

                    var Midx = dailyAttendance.studentDailyAttendanceModel.findIndex(x => x.StudentId == i.StudentId);

                    return Midx == idx
                }).map((x) => {
                    if (configtype == 0 || configtype == 2) {

                        dailyAttendance.AttendanceStudentModel.push({ StudentId: x.StudentId, SessionId: x.SessionId, RegisterId: 0, PhoenixALGID: 0, ParameterId: idPresent, Remark: "Present", AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                    }

                    return { StudentId: x.StudentId, StudentName: x.StudentName, StudentImageUrl: !x.StudentImageUrl ? "~/Content/img/avatar-2.jpg" : x.StudentImageUrl, GradeId: x.GradeId, SectionId: x.SectionId, StudentNumber: x.StudentNumber, SessionId: x.SessionId }
                });

                if (configtype == 0 || configtype == 2) {
                    if (dailyAttendance.studentDailyAttendanceModel.filter(x => x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) && x.SessionId == $('#SessionType').val()).length > 0) {

                        dailyAttendance.studentDailyAttendanceModel.filter(x => x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) && x.SessionId == $('#SessionType').val()).forEach((sa) => {
                            let saIdx = dailyAttendance.AttendanceStudentModel.findIndex(x => x.StudentId == sa.StudentId);
                            if (sa.ParameterId > 0) {
                                dailyAttendance.AttendanceStudentModel[saIdx].RegisterId = sa.RegisterId;
                                dailyAttendance.AttendanceStudentModel[saIdx].PhoenixALGID = sa.PhoenixALGID;
                                dailyAttendance.AttendanceStudentModel[saIdx].ParameterId = sa.ParameterId;
                                dailyAttendance.AttendanceStudentModel[saIdx].Remark = sa.Remark;

                            }

                        });

                    }
                }


                studentlist.forEach((s) => {

                    let studentName = s.StudentName.length > 15 ? s.StudentName.substring(0, s.StudentName.length = 15) : s.StudentName;
                    let prmId = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == s.StudentId && x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val())).length > 0 ? dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == s.StudentId && x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()))[0].ParameterId : undefined;

                    let prmidx;
                    if (prmId != undefined) {
                        prmidx = dailyAttendance.parameterList.findIndex(x => x.Value == prmId);
                    }


                    let displayParam;
                    let displaycss = "";
                    if (prmidx > -1) {
                        let prmDetails = dailyAttendance.parameterList[prmidx];
                        displayParam = prmDetails.Text.toUpperCase() == "PRESENT" ? `<i class="fas fa-check-circle  text-success"></i>`
                            : (prmDetails.Text.toUpperCase() == "ABSENT" ? `<i class="fas fa-times-circle  text-danger"></i>` : `<i class="fas fa-circle  text-warning position-relative"><span class="font-small position-absolute text-bold text-white"> ${prmDetails.Text.substring(0, 1)}</span></i>`)
                        displaycss = prmDetails.Text.toUpperCase() == "PRESENT" || prmDetails.Text.toUpperCase() == "ABSENT" ? "" : "other-status";
                    }
                    else {
                        displayParam = `<i class="fas fa-circle text-default"></i>`;
                    }

                    liStudentDetails += ` <li class="attend-card mb-3 col mr-0 px-2"> 
                          <div class="card w-100">
                            <div class="card-body">
                                <div class="attend-stud-header">
                                    <div class="attend-stud-info d-flex justify-content-between">
                                        <span class="ml-3"></span>
                                        <div class="custom-control custom-checkbox mt-0 mr-2">
                                            <input type="checkbox" data-stuId="${s.StudentId}" data-sessionId="${s.SessionId}" class="custom-control-input clsChkforstudent clsChk_${s.StudentId}_${s.SessionId}" id="selectStud_${s.StudentId}">
                                            <label class="custom-control-label" for="selectStud_${s.StudentId}"></label>
                                        </div> 
                                    </div>
                                </div>
                                <div class="attend-stud-body mt-4">
                                    <div class="d-flex justify-content-around">
                                        <a href="javascript:void(0);" class="rounded-circle stud-avatar">
                                            <img src="${s.StudentImageUrl}" class="img-fluid" alt="${s.StudentName}">
                                        </a>
                                    </div>
                                    <div id="spnstatus_${s.StudentId}" class="attend-stud-status ${displaycss} ${hideshowCardCircle}">
                                        
                                       ${displayParam}
                                        
                                    </div>
                                    <h3 class="attend-stud-name" data-toggle="tooltip" title="${s.StudentName.replace("  ", " ")}">${studentName.replace("  ", " ")}</h3>
                                    <span class="d-none">${s.StudentName.replace("  ", " ")}</span>
                                    <div class="attend-stud-id">${s.StudentNumber}</div>
                                </div>
                                <div class="attend-stud-footer">
                                    <div class="attend-stud-link position-relative border-top d-flex justify-content-around py-3">
                                        <h5 class="m-0">${MarkAttendance}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                              <!-- expander content -->
                        <div class="expander">
                                            <div class="attend-expand">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <div class="card mb-0 px-4">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-12  pr-4 pt-2 pb-3 ${displayDailyAsPerConfig}">
                                                                    <h3 class="mt-3 text-center">Daily Attendance</h3>
                                                                    <div class="responsive-table">
                                                                        <table class="table table-bordered">
                                                                            <thead>
                                                                                  <tr>
                                                                              ${dailyAttendance.expanderContentHearder(s.StudentId)}
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                             <tr>
                                                                                ${dailyAttendance.expanderContent(s.StudentId)}
                                                                             </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                              
                                                            </div>                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        <!-- expander content -->               </li>`;

                });

                $('#ulStudentAttendanceDetails').append(liStudentDetails);

                $('#divFloatNav').html(`<nav class="cd-stretchy-nav position-fixed">
                                <a class="cd-nav-trigger" href="javascript:void(0);">
                                    Menu
                                    <span aria-hidden="true"></span>
                                </a>
                                <ul>
                                    <li data-toggle="tooltip" data-placement="left" title="${present}">
                                        <a id="aHref-selectAllListPresent" data-isPresent="1" data-other="${idPresent}" onclick="dailyAttendance.AppendStatusForSelected(this)" href="javascript:void(0)">
                                            <span><i class="fas fa-check-circle text-white"></i></span>
                                         </a>
                                    </li>
                                    <li data-toggle="tooltip" data-placement="left" title="${absent}">
                                        <a id="aHref-selectAllListAbsent" data-isPresent="0" data-other="${idAbsent}" onclick="dailyAttendance.AppendStatusForSelected(this)" href="javascript:void(0)">
                                            <span><i class="fas fa-times-circle text-white"></i></span>
                                        </a>
                                    </li>
                                    ${dailyAttendance.generateParameterShortCutForAll()}
                                    <li data-toggle="tooltip" data-placement="left" title="${Others}">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#mpOtherOptions">
                                            <span><i class="fas fa-filter text-white"></i></span>
                                        </a>
                                    </li>
                                  
                                </ul>
                        
                                <span aria-hidden="true" class="stretchy-nav-bg"></span>
                            </nav>`)


                if ($('.list-view').hasClass('active')) {
                    $('.list-view').trigger('click')
                }
                else if ($('.grid-view').hasClass('active')) {
                    $('.grid-view').trigger('click')
                }
                /*configuration*/

                switch (parseInt(configtype)) {
                    case 0:
                        $('#liDailyAttendance').removeClass('d-none')
                        $('#liRoomAttendance').addClass('d-none')
                        $('#dailyAttendance').removeClass('d-none')
                        $('#roomAttendance').addClass('d-none')
                        dailyAttendance.getDailyAttendanceList();


                        break;
                    case 1:
                        $('#liDailyAttendance').addClass('d-none')
                        $('#liRoomAttendance').removeClass('d-none')
                        $('#dailyAttendance').addClass('d-none')
                        $('#roomAttendance').removeClass('d-none')
                        $('.clslifordailyatt').addClass('d-none')
                        $("#btnWatchPrevRoomAttendance").removeClass("d-none")
                        dailyAttendance.parameterList.filter(x => x.Value != idAbsent && x.Value != idAbsent).filter((pl) => {

                            return !AttendanceParameterShortCut.some((sc) => { return pl.Value === sc.Value })

                        }).map((x) => { return { Value: x.Value, Text: x.Text } }).forEach((p) => {


                            $('#ddlOtherAttendanceListforRoom').append(`<option data-other="${p.Value}" value="${p.Value}">${p.Text}</option>`).selectpicker('refresh');

                        })
                        //classAttendance.getClassListforMergeConfig(courseGroupId);
                        classAttendance.getClassRoomAttendanceListing();

                        $('#divFloatNavforRoom').html(`<nav class="cd-stretchy-nav position-fixed">
                                <a class="cd-nav-trigger" href="javascript:void(0);">
                                    
                                    <span aria-hidden="true"></span>
                                </a>
                                <ul>
                                    <li data-toggle="tooltip" data-placement="left" title="${present}">
                                        <a id="aHref-selectAllListPresent"  data-isPresent="1" data-other="${idPresent}" onclick="classAttendance.AppendStatusForSelectedRoom(this)" href="javascript:void(0)" >
                                            <span><i class="fas fa-check-circle text-white"></i></span>
                                         </a>
                                    </li>
                                    <li data-toggle="tooltip" data-placement="left" title="${absent}">
                                        <a id="aHref-selectAllListAbsent" data-isPresent="0" data-other="${idAbsent}" onclick="classAttendance.AppendStatusForSelectedRoom(this)" href="javascript:void(0)">
                                            <span><i class="fas fa-times-circle text-white"></i></span>
                                        </a>
                                    </li>
                                  ${classAttendance.generateRoomParameterShortCutForAll()}
                                    <li data-toggle="tooltip" data-placement="left" title="${Others}">
                                        <a href="javascript:void(0)"  data-toggle="modal" data-target="#mpOtherforRoomOptions">
                                            <span><i class="fas fa-filter text-white"></i></span>
                                        </a>
                                    </li>
                                  
                                </ul>
                        
                                <span aria-hidden="true" class="stretchy-nav-bg"></span>
                            </nav>`)
                        break;
                    case 2:
                        $('#liDailyAttendance').removeClass('d-none')
                        $('#liRoomAttendance').removeClass('d-none')
                        $('#dailyAttendance').removeClass('d-none')
                        $('#roomAttendance').removeClass('d-none')
                        dailyAttendance.getDailyAttendanceList();
                        /*if (courseGroupId != undefined) {
                            classAttendance.getClassListforMergeConfig(courseGroupId)
                        }
                        else {
                            classAttendance.getCourseList()
                        }*/
                        if (classAttendance.classAttendanceHeader != undefined) {
                            if (classAttendance.classAttendanceHeader.length > 0) {
                                classAttendance.getClassRoomAttendanceListing();
                            }
                        }


                        break;
                }

                /* configuration */
                $('[data-toggle="tooltip"]').tooltip();

                $(".clsothercategorylst").mCustomScrollbar({
                    setHeight: "200",
                });


                if ($('.cd-stretchy-nav').length > 0) {
                    var stretchyNavs = $('.cd-stretchy-nav');

                    stretchyNavs.each(function () {
                        var stretchyNav = $(this),
                            stretchyNavTrigger = stretchyNav.find('.cd-nav-trigger');

                        stretchyNavTrigger.on('click', function (event) {
                            event.preventDefault();
                            stretchyNav.toggleClass('nav-is-visible');
                        });
                    });

                    $(document).on('click', function (event) {
                        (!$(event.target).is('.cd-nav-trigger') && !$(event.target).is('.cd-nav-trigger span')) && stretchyNavs.removeClass('nav-is-visible');
                    });
                }


            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });


        },
        generateParameterList = function (studentId, sessionId) {
            let trothercat = ``;
            dailyAttendance.parameterList.forEach((p) => {
                trothercat += `<a class="dropdown-item" data-other="${p.Value}" data-stuId="${studentId}" data-SessionId="${sessionId}" data-periodNo="" data-subject="" href="javascript:void(0)" onclick="dailyAttendance.dropdownMenu(this)">${p.Text}</a>`
            });
            return trothercat;
        },

        dropdownMenu = function (drp, isQuicklink) {
            let dataPresent = $(drp).attr('data-isPresent');
            let selParamvalue = $(drp).attr('data-other');
            let studentId = $(drp).attr('data-stuId');
            let sessionId = $(drp).attr('data-SessionId');
            let registerId = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val())).length == 0 ? dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()))[0].RegisterId : 0;
            let PhoenixALGID = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val())).length == 0 ? dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()))[0].PhoenixALGID : 0;
            let remark = undefined;
            if (isQuicklink) {
                $(drp).parent().prev().removeClass("d-none")
                $(drp).parent().prev().attr("data-isPresent", dataPresent)
                $(drp).parent().prev().attr("data-other", selParamvalue)
            }
            if (dataPresent == 0) {
                remark = $('#txtoptionalRemark').val() == "" ? `${absent}` : $('#txtoptionalRemark').val();
                $('#spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>`)
                $('#spnstatus_' + studentId).html(`<i class="fas fa fa-times-circle  text-danger"></i>`)
                $('.spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>`)

            }
            else if (dataPresent == 1) {
                remark = $('#txtoptionalRemark').val() == "" ? `${present}` : $('#txtoptionalRemark').val();
                $('#spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`)
                $('#spnstatus_' + studentId).html(`<i class="fas fa fa-check-circle  text-success"></i>`)
                $('.spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`)

            }
            //else if (dataPresent == -1) {
            //    remark = $('#txtoptionalRemark').val() ==  $('#txtoptionalRemark').val();
            //    $('#spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`)
            //    $('#spnstatus_' + studentId).html(`<i class="fas fa fa-check-circle  text-success"></i>`)
            //    $('.spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`)

            //}
            else {
                let indexparamid = dailyAttendance.parameterList.findIndex(x => x.Value == selParamvalue)//(selParamvalue == undefined ? dataPresent : selParamvalue))
                let paramValue = dailyAttendance.parameterList[indexparamid].Text;
                remark = $('#txtoptionalRemark').val() == "" ? paramValue : $('#txtoptionalRemark').val();
                if (selParamvalue == idPresent) {
                    remark = $('#txtoptionalRemark').val() == "" ? `${present}` : $('#txtoptionalRemark').val();
                    $('#spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`)
                    $('#spnstatus_' + studentId).html(`<i class="fas fa fa-check-circle  text-success"></i>`)
                    $('.spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`)

                }
                else if (selParamvalue == idAbsent) {
                    remark = $('#txtoptionalRemark').val() == "" ? `${absent}` : $('#txtoptionalRemark').val();
                    $('#spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>`)
                    $('#spnstatus_' + studentId).html(`<i class="fas fa fa-times-circle  text-danger"></i>`)
                    $('.spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>`)
                }
                else {


                    $('#spn_' + studentId + '_' + sessionId).html(`<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}">${paramValue.substring(0, 1)}</span></i>`).addClass('other-status')
                    $('#spnstatus_' + studentId).html(`<i class="fas fa-circle  text-warning position-relative"><span class="font-small position-absolute text-bold text-white">${paramValue.substring(0, 1)}</span></i>`).addClass('other-status')
                    $('.spn_' + studentId + '_' + sessionId).html(`<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}">${paramValue.substring(0, 1)}</span></i>`).addClass('other-status')
                }
            }

            let index = dailyAttendance.AttendanceStudentModel.findIndex(x => x.StudentId == studentId);
            if (index > -1) {
                dailyAttendance.AttendanceStudentModel[index].ParameterId = selParamvalue;
                dailyAttendance.AttendanceStudentModel[index].Remark = remark;

            }
            else {
                dailyAttendance.AttendanceStudentModel.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, PhoenixALGID: PhoenixALGID, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
            }
            $('[data-toggle="tooltip"]').tooltip();
        },
        expanderContentHearder = function (studentId) {

            let expanderheader = ``;
            if (configtype == 0 || configtype == 2) {

                dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId).forEach((ex) => {
                    let AsOnDate = ex.DisplayDate;
                    let dateCss = AsOnDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? "" : "bg-light text-black-50";
                    expanderheader += `
                                 <th data-title="${AsOnDate}" class="text-center ${dateCss}">${AsOnDate}</th>
                                 
                              `;

                });

            }

            return expanderheader;

        },

        expanderContent = function (studentId) {

            let expander = ``;
            if (configtype == 0 || configtype == 2) {
                dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId).forEach((ex) => {
                    let AsOnDate = ex.DisplayDate;
                    let remark = ex.Remark;
                    let dateCss = AsOnDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? "" : "bg-light";
                    let IsToggable = AsOnDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? "dropdown" : "";
                    let idxstatus;
                    let statusdetails;
                    let statusDisplay;
                    let statusdisplyCss = "";
                    let frmSession1 = ex.FromSession1 ? "text-default" : "";
                    let studentspnId = AsOnDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? `spn_${ex.StudentId}_${ex.SessionId}` : '';
                    idxstatus = dailyAttendance.parameterList.findIndex(x => x.Value == ex.ParameterId);
                    statusdetails = dailyAttendance.parameterList[idxstatus];
                    let studOtherDetails = ex.ParameterId == 0 ? "d-none" : "";
                    if (idxstatus > -1) {

                        statusDisplay = statusdetails.Text.toUpperCase() == "PRESENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success ${frmSession1}"></i></span>`
                            : (statusdetails.Text.toUpperCase() == "ABSENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-times-circle text-danger ${frmSession1}"></i></span>` : `<i class="fas fa-circle fa-2x text-warning position-relative ${frmSession1}"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}">${statusdetails.Text.substring(0, 1)}</span></i>`)

                        statusdisplyCss = statusdetails.Text.toUpperCase() == "PRESENT" || statusdetails.Text.toUpperCase() == "ABSENT" ? "" : "other-status";

                    }
                    else {
                        statusDisplay = `<i class="fas fa-2x fa-plus-circle text-default"></i>`;
                    }

                    expander += `
                           <td data-title="${AsOnDate}" class="text-center ${dateCss} position-relative">   
                               <!--Attendance dropdown event starts-->                                                                                 
                               <a href="javascript:void(0)" class="d-inline-block nav-link p-0 text-primary" data-toggle="${IsToggable}" aria-haspopup="true" aria-expanded="false">
                                    <div id="${studentspnId}" class="${statusdisplyCss}">
                                      ${statusDisplay}    
                                     </div>
                               </a>
                                <a href="javascript:void(0)" class="p-0 text-primary clsDailySingleOtherOption clsadditionalComment ${studOtherDetails} position-absolute ml-2" data-stuId="${ex.StudentId}" data-SessionId="${ex.SessionId}" data-toggle="modal" data-target="#mpOtherOptions" data-isPresent="${ex.ParameterId}" data-other="${ex.ParameterId}" style="
                                top: 22px;
                                ">
                               <img src="/Content/VLE/img/speech-bubble.png" alt="speech-bubble-icon" class="speech-bubble" style="
                                  width: 20px;
                                  line-height: 1;
                                 ">
                                </a>
                               <div class="dropdown-menu dropdown-menu-right z-depth-1 pt-2 pb-2 w-25 ${blockasPerpagePermission}" style="z-index: 1034;"> 
                                    <a class="dropdown-item ml-2 d-block" data-stuId="${ex.StudentId}" data-isPresent="1" data-other="${idPresent}" data-SessionId="${ex.SessionId}" href="javascript:void(0)" onclick="dailyAttendance.dropdownMenu(this,true)"> ${present}</a>
                                    <a class="dropdown-item ml-2 clsDailySingleOtherOption d-block" data-stuId="${ex.StudentId}" data-isPresent="0" data-other="${idAbsent}" data-SessionId="${ex.SessionId}" href="javascript:void(0)" onclick="dailyAttendance.dropdownMenu(this,true)">${absent}</a>
                                     ${dailyAttendance.generateParameterShortCut(ex.StudentId, ex.SessionId)}
                                    <div class="dropdown-divider mb-0"></div>
                                    <h6 class="dropdown-header bg-light p-1 pl-2 mb-1 text-dark">
                                    <small class="text-uppercase">
                                    <strong>
                                     <a href="javascript:void(0)" data-stuId="${ex.StudentId}" data-SessionId="${ex.SessionId}" data-toggle="modal" data-target="#mpOtherOptions" class="clsDailySingleOtherOption d-block">
                                       <span>${Others}</span>
                                     </a>
                                    </strong>
                                    </small>
                                    </h6>
                                    <div class="">
                                     
                                      <!--${dailyAttendance.generateParameterList(ex.StudentId, ex.SessionId)}-->
                                    </div>
                                   
                               </div>
                               <!--Attendance dropdown event starts-->
                           </td>
                       
                           `;

                });

            }
            return expander;
        },
        AppendStatusForSelected = function (drp) {
            let sessionId = $('#SessionType').val();
            let selParamvalue = $(drp).attr('data-other');
            let dataPresent = $(drp).attr('data-isPresent');
            attendanceMarkValue = dataPresent;
            let remark;
            let appendStatus;
            let appendStatusOnStudent;
            let appendDisplayClass = "";
            if (dataPresent == 0) {
                appendStatus = `<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${absent}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>`;
                appendStatusOnStudent = `<i class="fas fa fa-times-circle  text-danger"></i>`;
                $('#spnSelectAll').html(`<i class="fas fa-2x fa-times-circle text-danger"></i>`);
                remark = `${absent}`;
            }
            else if (dataPresent == 1) {
                appendStatus = `<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${present}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`;
                appendStatusOnStudent = `<i class="fas fa fa-check-circle  text-success"></i>`;
                $('#spnSelectAll').html(`<i class="fas fa-2x fa-check-circle text-success"></i>`);
                remark = `${present}`;
            }
            else {
                let indexparamid = dailyAttendance.parameterList.findIndex(x => x.Value == selParamvalue)
                let paramValue = dailyAttendance.parameterList[indexparamid].Text;
                remark = $('#txtoptionalRemark').val() == "" ? paramValue : $('#txtoptionalRemark').val();
                appendStatus = `<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}">${paramValue.substring(0, 1)}</span></i>`;
                appendStatusOnStudent = `<i class="fas fa-circle  text-warning position-relative"><span class="font-small position-absolute text-bold text-white">${paramValue.substring(0, 1)}</span ></i>`;
                $('#spnSelectAll').html(appendStatusOnStudent).addClass('other-status');

                appendDisplayClass = "other-status";
            }

            /* if ($('#selectall').is(":checked")) {
 
                 dailyAttendance.studentDailyAttendanceModel.filter((i, idx) => {
 
                     var Midx = dailyAttendance.studentDailyAttendanceModel.findIndex(x => x.StudentId == i.StudentId);
                     return Midx == idx
                 }).forEach((st) => {
                     let studentId = st.StudentId;
                     let registerId = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.DisplayDate == globalFunctions.toSystemReadableDate(globalFunctions.toSystemReadableDate($('#txtAttDate').val())).length > 0 ? dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.DisplayDate == globalFunctions.toSystemReadableDate(globalFunctions.toSystemReadableDate($('#txtAttDate').val()))[0].RegisterId : 0;
 
                     $('#spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                     $('#spnstatus_' + studentId).html(appendStatusOnStudent).addClass(appendDisplayClass);
                     $('.spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
 
                     let idx = dailyAttendance.AttendanceStudentModel.findIndex(x => x.StudentId == studentId);
 
                     if (idx > -1) {
                         dailyAttendance.AttendanceStudentModel[idx].ParameterId = selParamvalue;
                         dailyAttendance.AttendanceStudentModel[idx].Remark = remark;
                     }
                     else {
                         dailyAttendance.AttendanceStudentModel.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate(globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                     }
 
                 });
 
 
             }
             else {
                 $('.clsChkforstudent:checked').each(function () {
                     let studentId = $(this).attr('data-stuid')
 
                     let registerId = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.DisplayDate == globalFunctions.toSystemReadableDate(globalFunctions.toSystemReadableDate($('#txtAttDate').val()))[0].RegisterId
 
 
                     $('#spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                     $('.spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                     $('#spnstatus_' + studentId).html(appendStatusOnStudent).addClass(appendDisplayClass);
 
                     let idx = dailyAttendance.AttendanceStudentModel.findIndex(x => x.StudentId == studentId);
                     if (idx > -1) {
                         dailyAttendance.AttendanceStudentModel[idx].ParameterId = selParamvalue;
                         dailyAttendance.AttendanceStudentModel[idx].Remark = remark;
                     }
                     else {
                         dailyAttendance.AttendanceStudentModel.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                     }
 
                 });
             } */

            $('.clsChkforstudent:checked').each(function () {
                let studentId = $(this).attr('data-stuid')

                let registerId = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()))[0].RegisterId;

                let PhoenixALGID = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val())).length == 0 ? dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()))[0].PhoenixALGID : 0;
                $('#spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                $('.spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                $('#spnstatus_' + studentId).html(appendStatusOnStudent).addClass(appendDisplayClass);

                $('.spn_' + studentId + '_' + sessionId).parent().parent().find(".clsDailySingleOtherOption").removeClass('d-none');
                if (attendanceMarkValue == "1")//Present
                {
                    //$('.spn_' + studentId + '_' + sessionId).parent().parent().find(".clsDailySingleOtherOption").attr('data-ispresent', "1")[1];                    
                }
                else if (attendanceMarkValue == "0")//Absent
                {
                    //$('.spn_' + studentId + '_' + sessionId).parent().parent().find(".clsDailySingleOtherOption").attr('data-ispresent', "0")[1];
                }
                else {
                    $('.spn_' + studentId + '_' + sessionId).parent().parent().find(".clsDailySingleOtherOption").attr('data-ispresent', "-1")[1];
                }
                //$('.spn_' + studentId + '_' + sessionId).parent().parent().find(".clsDailySingleOtherOption").attr('data-other', selParamvalue)[1];

                let idx = dailyAttendance.AttendanceStudentModel.findIndex(x => x.StudentId == studentId);
                if (idx > -1) {
                    dailyAttendance.AttendanceStudentModel[idx].ParameterId = selParamvalue;
                    dailyAttendance.AttendanceStudentModel[idx].Remark = remark;
                }
                else {
                    dailyAttendance.AttendanceStudentModel.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, PhoenixALGID: PhoenixALGID, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                }

            });

            $('[data-toggle="tooltip"]').tooltip();
            $('.clsChkforstudent:checked').prop("checked", false).trigger('change');
            $('#selectall').prop("checked", false).trigger('change');
        }

    /*============= DAILY ATTENDANCE LIST SECTION ==========*/

    getDailyAttendanceList = function () {

        let trDetails = ``;

        let thDetails = ``;
        let sessionOnHeader = ``;


        dailyAttendance.studentDailyAttendanceModel.filter((i, idx) => {

            var Midx = dailyAttendance.studentDailyAttendanceModel.findIndex(x => x.DisplayDate == i.DisplayDate);

            return Midx == idx
        }).forEach((th) => {
            //let activeDay = th.DisplayDate == globalFunctions.toSystemReadableDate(globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? "activeDay" : "";
            let dateCss = th.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? "" : "bg-light text-black-50";
            sessionOnHeader += ` <th class="text-center arabictxt  ${dateCss}" id="thSessionDateType">${th.MLDisplayDate}</th>`;


        });
        let otherCategoryForAll = ``;
        dailyAttendance.parameterList.forEach((p) => {
            otherCategoryForAll += `<a class="dropdown-item" data-other="${p.Value}" href="javascript:void(0)" onclick="dailyAttendance.AppendStatusForSelected(this)" >${p.Text}</a>`


        })

        $('#divSelectStatusForList').html(otherCategoryForAll);
        $('#aHref-selectAllListPresent').attr('data-other', idPresent)
        $('#aHref-selectAllListAbsent').attr('data-other', idAbsent)
        thDetails =
            `<th class="text-center">
             <div class="checkbox">
              <input type="checkbox" id="selectAllList" class=""><label></label>
             </div>
              </th>
              <th>${students}</th>
              <th class="text-center d-none">${TransportStatus}</th>
               ${sessionOnHeader}
               `;



        dailyAttendance.studentDailyAttendanceModel.filter((i, idx) => {

            var Midx = dailyAttendance.studentDailyAttendanceModel.findIndex(x => x.StudentId == i.StudentId);

            return Midx == idx
        }).map((x) => {
            return { StudentId: x.StudentId, StudentName: x.StudentName, StudentNumber: x.StudentNumber, StudentImageUrl: x.StudentImageUrl, GradeId: x.GradeId, SectionId: x.SectionId, SessionId: x.SessionId, Nationality: x.Nationality, IsSEN: x.IsSEN }
        }).forEach((s) => {
            let studentName = s.StudentName.length > 15 ? s.StudentName.substring(0, s.StudentName.length = 15) : s.StudentName;
            let uaeNationality = s.Nationality == "UAE" ? `<i class="ae flag"></i>` : "";
            let IsSEN = s.IsSEN ? `<a href="javascript:void(0);" class="text-primary"><i class="fas fa-book-reader"></i></a>` : ""
            trDetails += `<tr>
                    <td class="text-center">
                        <div class="checkbox">
                            <input type="checkbox" data-stuId="${s.StudentId}" data-sessionId="${s.SessionId}" class="dt-checkboxes clsChk_${s.StudentId}_${s.SessionId}"><label></label>
                        </div>
                    </td>
                    <td>
                        <div class="student-info d-flex align-items-center">
                            <span class="rounded-circle overflow-hidden defaultAvatar mr-2">
                                <img src="${s.StudentImageUrl}" alt="" class="img-fluid">
                            </span>
                            <h3 class="m-0 name text-medium">
                                ${s.StudentName.replace("  ", " ")} ${uaeNationality}${IsSEN}
                                <span class="small sub-heading text-medium d-block userid"> ${s.StudentNumber} </span>
                            </h3>
                        </div>
                    </td>
                    <td class="text-center d-none">${NoScan}</td>
                    ${dailyAttendance.generateSessionRow(s.StudentId)}`
        });

        $('#tblStudentDailyAttendance thead tr').html(thDetails);
        $('#tblStudentDailyAttendance tbody').html(trDetails);
        //if ($("#btndailyAttendanceListing").hasClass('active')) { $("#btndailyAttendanceListing").trigger("click") }




    },
        generateSessionRow = function (studentId) {

            let sessionRows = ``;
            dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId).forEach((x) => {
                //let cssIsActive = x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? 'activeDay' : '';
                let dnone = x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? '' : 'd-none';
                let remark = x.Remark;
                let idxstatus;
                let statusdetails;
                let statusDisplay;
                let statusCss = "";
                let frmSession1 = x.FromSession1 ? "text-default" : "";
                let dateCss = x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? "" : "bg-light";
                idxstatus = dailyAttendance.parameterList.findIndex(p => p.Value == x.ParameterId);
                statusdetails = dailyAttendance.parameterList[idxstatus];
                if (idxstatus > -1) {

                    statusDisplay = statusdetails.Text.toUpperCase() == "PRESENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success ${frmSession1}"></i></span>`
                        : (statusdetails.Text.toUpperCase() == "ABSENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-times-circle text-danger ${frmSession1}"></i></span>` : `<i class="fas fa-circle fa-2x text-warning position-relative ${frmSession1}"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}">${statusdetails.Text.substring(0, 1)}</span></i>`)

                    statusCss = statusdetails.Text.toUpperCase() == "PRESENT" || statusdetails.Text.toUpperCase() == "ABSENT" ? "" : "other-status";
                }
                else {
                    statusDisplay = remark == null ? `<i class="fas fa-2x fa-plus-circle text-default"></i>` : `<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-toggle="tooltip" data-placement="left" title="${remark}">${remark.substring(0, 1).toUpperCase()}</span></i>`;
                    statusCss = remark == null ? "" : "other-status";
                }
                let studentspnId = x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? `spn_${x.StudentId}_${x.SessionId}` : '';
                let studentOtherDetails = x.ParameterId == 0 ? "d-none" : "";
                sessionRows += ` <td class="text-center  ${dateCss} position-relative">
                            <a href="javascript:void(0)" class="d-inline-block nav-link p-0 text-primary" data-toggle="dropdown" data-placement="left" aria-haspopup="true" aria-expanded="false">
                                <span class="${studentspnId} ${statusCss}">
                                    ${statusDisplay}
                                </span>
                            </a>  
                                <a href="javascript:void(0)" class="p-0 text-primary clsDailySingleOtherOption clsadditionalComment ${studentOtherDetails} position-absolute ml-2" data-stuId="${x.StudentId}" data-SessionId="${x.SessionId}" data-toggle="modal" data-target="#mpOtherOptions" data-isPresent="${x.ParameterId}" data-other="${x.ParameterId}" style="
                                top: 22px;
                                ">
                               <img src="/Content/VLE/img/speech-bubble.png" alt="speech-bubble-icon" class="speech-bubble" style="
                                  width: 20px;
                                  line-height: 1;
                                 ">
                                </a>
                            <div class="dropdown-menu dropdown-menu-right z-depth-1 pt-2 pb-2 w-25 ${dnone} ${blockasPerpagePermission}" style="z-index: 1034;">
                                <a class="dropdown-item ml-2 d-block" data-stuId="${x.StudentId}" data-isPresent="1" data-other="${idPresent}" data-SessionId="${x.SessionId}" href="javascript:void(0)" onclick="dailyAttendance.dropdownMenu(this,true)"> ${present}</a>
                                <a class="dropdown-item ml-2 d-block" data-stuId="${x.StudentId}" data-isPresent="0" data-other="${idAbsent}" data-SessionId="${x.SessionId}" href="javascript:void(0)" onclick="dailyAttendance.dropdownMenu(this,true)"> ${absent}</a>
                                 ${dailyAttendance.generateParameterShortCut(x.StudentId, x.SessionId)}  
                                <div class="dropdown-divider mb-0"></div>
                                <h6 class="dropdown-header bg-light p-1 pl-2 mb-1 text-dark">
                                  <small class="text-uppercase">
                                       <strong>
                                      <a href="javascript:void(0)" data-stuId="${x.StudentId}" data-SessionId="${x.SessionId}" data-toggle="modal" data-target="#mpOtherOptions" class="clsDailySingleOtherOption" d-block">
                                       <span>${Others}</span>
                                     </a>
                               </strong></small></h6>
                                <div class="">
                                   <!-- ${dailyAttendance.generateParameterList(x.StudentId, x.SessionId)}-->
                                </div>



                            </div>
                        </td>`

            });
            return sessionRows;

        },

        /*============= DAILY ATTENDANCE LIST SECTION ==========*/
        DailyParameterHideShow = function () {
            if (configtype == 2) {
                if (isWeeklyHoliday) {
                    globalFunctions.showWarningMessage(holidayRemark)
                    $("#btnSaveDailyAttendance").addClass("d-none")

                }
                else {
                    $("#btnSaveDailyAttendance").removeClass("d-none")
                }
                $("#btnWatchPrevRoomAttendance").addClass("d-none")
                $('#divFloatNav').remove()
                $('#divFloatNavforRoom').remove()
                $('.grid-list-card').append(`<div id="divFloatNav" class="">
                  </div>`)
                currCount == 1 || currCount == 0 ? $("#ddlCurriculumnList").parent().addClass("d-none") : $("#ddlCurriculumnList").parent().removeClass("d-none")

                $('.divDailyparameterddl').removeClass('d-none');
                $('.divRoomParameterddl').addClass('d-none');

                $('.dt-checkboxes:checked').length > 1 ? $('#divFloatNav').removeClass('d-none') : $('#divFloatNav').addClass('d-none')
                $('.clslifordailyatt').removeClass('d-none');

                $('#divFloatNav').html(`<nav class="cd-stretchy-nav position-fixed">
                                <a class="cd-nav-trigger" href="#0">
                                    Menu
                                    <span aria-hidden="true"></span>
                                </a>
                                <ul>
                                    <li data-toggle="tooltip" data-placement="left" title="${present}">
                                        <a id="aHref-selectAllListPresent" data-isPresent="1" data-other="${idPresent}" onclick="dailyAttendance.AppendStatusForSelected(this)" href="javascript:void(0)">
                                            <span><i class="fas fa-check-circle text-white"></i></span>
                                         </a>
                                    </li>
                                    <li data-toggle="tooltip" data-placement="left" title="${absent}">
                                        <a id="aHref-selectAllListAbsent" data-isPresent="0" data-other="${idAbsent}" onclick="dailyAttendance.AppendStatusForSelected(this)" href="javascript:void(0)">
                                            <span><i class="fas fa-times-circle text-white"></i></span>
                                        </a>
                                    </li>
                                     ${dailyAttendance.generateParameterShortCutForAll()}
                                    <li data-toggle="tooltip" data-placement="left" title="${Others}">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#mpOtherOptions">
                                            <span><i class="fas fa-filter text-white"></i></span>
                                        </a>
                                    </li>
                                  
                                </ul>
                        
                                <span aria-hidden="true" class="stretchy-nav-bg"></span>
                            </nav>`)

                if ($('.cd-stretchy-nav').length > 0) {
                    var stretchyNavs = $('.cd-stretchy-nav');

                    stretchyNavs.each(function () {
                        var stretchyNav = $(this),
                            stretchyNavTrigger = stretchyNav.find('.cd-nav-trigger');

                        stretchyNavTrigger.on('click', function (event) {
                            event.preventDefault();
                            stretchyNav.toggleClass('nav-is-visible');
                        });
                    });

                    $(document).on('click', function (event) {
                        (!$(event.target).is('.cd-nav-trigger') && !$(event.target).is('.cd-nav-trigger span')) && stretchyNavs.removeClass('nav-is-visible');
                    });
                }

            }

        },
        RoomParameterHideShow = function () {

            if (configtype == 2) {

                if ($("#CourseGroupId").val() == "") {
                    $("#txtRoomAttDate").trigger("blur")
                }
                $("#btnWatchPrevRoomAttendance").removeClass("d-none")
                if (isWeeklyHolidayForRoom) {
                    globalFunctions.showWarningMessage(holidayRemarkForRoom)
                    $("#btnSaveDailyAttendance").addClass("d-none")

                }
                else {
                    $("#btnSaveDailyAttendance").removeClass("d-none")
                }
                //if (classAttendance.classAttendanceHeader.length > 0) {
                //    $("#btnSaveDailyAttendance").removeClass("d-none")
                //}
                //else {
                //    globalFunctions.showWarningMessage("No lessons scheduled for today in the Timetable");
                //    $("#btnSaveDailyAttendance").addClass("d-none");
                //    // $('#tblRoomAttendance').empty();
                //}



                $('#divFloatNav').remove()
                $('#divFloatNavforRoom').remove()
                $('.grid-list-card').append(`<div id="divFloatNavforRoom" class="">
                  </div>`)

                $("#ddlCurriculumnList").parent().addClass("d-none")

                $('.divRoomParameterddl').removeClass('d-none');
                $('.divDailyparameterddl').addClass('d-none');
                $('.dt-checkboxesRoom:checked').length > 1 ? $('#divFloatNavforRoom').removeClass('d-none') : $('#divFloatNavforRoom').addClass('d-none')
                $('.clslifordailyatt').addClass('d-none');

                $('#divFloatNavforRoom').html(`<nav class="cd-stretchy-nav position-fixed">
                                <a class="cd-nav-trigger" href="javascript:void(0)">
                                    Menu
                                    <span aria-hidden="true"></span>
                                </a>
                                <ul>
                                    <li data-toggle="tooltip" data-placement="left" title="Present">
                                        <a id="aHref-selectAllListPresent"  data-isPresent="1" data-other="${idPresent}" onclick="classAttendance.AppendStatusForSelectedRoom(this)" href="javascript:void(0)">
                                            <span><i class="fas fa-check-circle text-white"></i></span>
                                         </a>
                                    </li>
                                    <li data-toggle="tooltip" data-placement="left" title="Absent">
                                        <a id="aHref-selectAllListAbsent"  data-isPresent="0" data-other="${idAbsent}" onclick="classAttendance.AppendStatusForSelectedRoom(this)" href="javascript:void(0)">
                                            <span><i class="fas fa-times-circle text-white"></i></span>
                                        </a>
                                    </li>
                                    ${classAttendance.generateRoomParameterShortCutForAll()}
                                    <li data-toggle="tooltip" data-placement="left" title="Other Options">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#mpOtherforRoomOptions">
                                            <span><i class="fas fa-filter text-white"></i></span>
                                        </a>
                                    </li>
                                  
                                </ul>
                        
                                <span aria-hidden="true" class="stretchy-nav-bg"></span>
                            </nav>`)

                if ($('.cd-stretchy-nav').length > 0) {
                    var stretchyNavs = $('.cd-stretchy-nav');

                    stretchyNavs.each(function () {
                        var stretchyNav = $(this),
                            stretchyNavTrigger = stretchyNav.find('.cd-nav-trigger');

                        stretchyNavTrigger.on('click', function (event) {
                            event.preventDefault();
                            stretchyNav.toggleClass('nav-is-visible');
                        });
                    });

                    $(document).on('click', function (event) {
                        (!$(event.target).is('.cd-nav-trigger') && !$(event.target).is('.cd-nav-trigger span')) && stretchyNavs.removeClass('nav-is-visible');
                    });
                }
            }

        },
        generateParameterShortCut = function (studentId, sessionId) {

            let paramSC = ``;
            AttendanceParameterShortCut.forEach((sc) => {
                paramSC += `<a class="dropdown-item ml-2  d-block" data-ispresent ="-1" data-stuId="${studentId}"  data-other="${sc.Value}" data-SessionId="${sessionId}" href="javascript:void(0)" onclick="dailyAttendance.dropdownMenu(this,true)">${sc.Text}</a>`;

            })



            return paramSC;

        },
        generateParameterShortCutForAll = function () {

            let paramSCAll = ``;
            AttendanceParameterShortCut.forEach((sc) => {
                paramSCAll += ` <li data-toggle="tooltip" data-placement="left" title="${sc.Text}">
                                        <a  data-other="${sc.Value}" onclick="dailyAttendance.AppendStatusForSelected(this)" href="javascript:void(0)">
                                            <span class="font-weight-bolder text-white">${sc.Text.substring(0, 1)}</span>
                                        </a>
                                    </li>`;

            })
            return paramSCAll;
        }
    return {
        init: init,
        onWindowLoad: onWindowLoad,
        getAttGradeSection: getAttGradeSection,
        studentDailyAttendanceModel: studentDailyAttendanceModel,
        parameterList: parameterList,
        gradeSectionDetails: gradeSectionDetails,
        GetDailyAttendanceDetails: GetDailyAttendanceDetails,
        generateParameterList: generateParameterList,
        dropdownMenu: dropdownMenu,
        AttendanceStudentModel: AttendanceStudentModel,
        expanderContentHearder: expanderContentHearder,
        expanderContent: expanderContent,
        AppendStatusForSelected: AppendStatusForSelected,
        getDailyAttendanceList: getDailyAttendanceList,
        generateSessionRow: generateSessionRow,
        DailyParameterHideShow: DailyParameterHideShow,
        RoomParameterHideShow: RoomParameterHideShow,
        thisForSingleDailyAttendance: thisForSingleDailyAttendance,
        curriculumnList: curriculumnList,
        getCurriculumn: getCurriculumn,
        generateParameterShortCut: generateParameterShortCut,
        generateParameterShortCutForAll: generateParameterShortCutForAll,
        sessionTypeForGrade: sessionTypeForGrade
    }
}();

/*======================================== CLASSLIST ATTENDANCE =========================================*/

let classAttendance = function () {
    let classAttendanceHeader;
    let classAttendanceDetails;
    let frmMyplannerCourseId;
    let frmMyplannerasonDate;
    let thisforSingleRoomAttendance;
    let ClassAttendanceModuleStruct = [];
    init = function () {
        $(document).on('change', '#CourseId', function () {
            let courseId = $(this).val();

            $.get('/Attendance/Attendance/GetCourseGroupByCourse', { courseId: courseId }).then((response) => {
                $('#CourseGroupId').empty();
                if (configtype != 2) {
                    $('#divDailyAttendance').empty();
                }

                $('#CourseGroupId').append(`<option value="">${select}</option>`)
                response.forEach((x) => {
                    $('#CourseGroupId').append(`<option value="${x.Value}">${x.Text}</option>`).selectpicker('refresh');

                })

                $("#CourseGroupId").val($("#CourseGroupId option:eq(1)").val()).selectpicker('refresh').trigger('change');
            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });


        });

        $(document).on('change', '#CourseGroupId', function () {
            let courseGroupId = $(this).val();

            let selectedDate = configtype == 2 || $("#txtAttDate").parent().parent().hasClass("divDailyparameterddl") ? globalFunctions.toSystemReadableDate($("#txtRoomAttDate").val()) : globalFunctions.toSystemReadableDate($('#txtAttDate').val());
            if (configtype == 1) {
                dailyAttendance.GetDailyAttendanceDetails(courseGroupId)
            }
            else if (configtype == 2) {
                if (courseGroupId != undefined && courseGroupId != "") {
                    classAttendance.getClassListforMergeConfig(courseGroupId)
                }

            }
            /*$.get('/Attendance/Attendance/CheckforWeeklyHoliday', { selectedDate: selectedDate, courseId: courseGroupId, IsCallFrmRoom: true }).then((response) => {

                if (response.IsWeeklyHoliday) {
                    if ($('#btnroomAttendance').hasClass('active')) {
                        globalFunctions.showWarningMessage(response.Remark == "" ? WeeklyHoliday : response.Remark)
                        $('#tblRoomAttendance thead tr').empty();
                        $('#tblRoomAttendance tbody').empty();
                    }
                }
                else {


                    if (courseGroupId == "" || courseGroupId == undefined) {
                        if (configtype == 2) {
                            if ($('#btnroomAttendance').hasClass('active')) {
                                globalFunctions.showWarningMessage(GroupNotPresentSection)

                            }
                        }
                        else {
                            globalFunctions.showWarningMessage(GroupNotPresentSection)
                        }
                        $('#tblRoomAttendance thead tr').empty();
                        $('#tblRoomAttendance tbody').empty();
                    }
                    else {


                        if (configtype == 1) {
                            dailyAttendance.GetDailyAttendanceDetails(courseGroupId)
                        }
                        else if (configtype == 2) {
                            if (courseGroupId != undefined && courseGroupId != "") {
                                classAttendance.getClassListforMergeConfig(courseGroupId)
                            }

                        }

                    }
                }
            });*/



        });

        if (classAttendance.frmMyplannerCourseId != 0 && classAttendance.frmMyplannerasonDate != "") {
            hasPlanner = true;
            dailyAttendance.GetDailyAttendanceDetails(classAttendance.frmMyplannerCourseId, classAttendance.frmMyplannerasonDate, hasPlanner)
        }

        $(document).on('change', '.dt-checkboxesRoom', function () {
            //data - sessionId="${m.SessionId}" data - stuId
            if (configtype == 0 || configtype == 2 || configtype == 1) {
                $('.dt-checkboxesRoom:checked').length > 1 && $('.clschkPeriodHeader').is(":checked") ? $('#divFloatNavforRoom').removeClass('d-none') : $('#divFloatNavforRoom').addClass('d-none');

            }

            let studentId = $(this).attr('data-stuId');
            let sessionId = $(this).attr('data-sessionId');
            if ($(this).is(":checked")) {
                $('.clsChkRoom_' + studentId + '_' + sessionId).prop('checked', true);

            }
            else {
                $('.clsChkRoom_' + studentId + '_' + sessionId).prop('checked', false);

            }

        });
        $(document).on('change', '#selectAllRoomList', function () {

            if ($(this).is(":checked")) {



                $('.dt-checkboxesRoom').prop('checked', true);
                if (configtype == 2 || configtype == 1) {
                    if ($('.clschkPeriodHeader ').is(":checked")) {
                        $('#divFloatNavforRoom').removeClass('d-none');

                    }
                }

            }
            else {


                $('.dt-checkboxesRoom').prop('checked', false);

                if (configtype == 1 || configtype == 2) {
                    $('#divFloatNavforRoom').addClass('d-none');
                }

            }

        });
        $(document).on('change', '.clschkPeriodHeader', function () {

            if ($(this).is(":checked")) {
                if ($('.dt-checkboxesRoom:checked').length > 1) {
                    $('#divFloatNavforRoom').removeClass("d-none")
                }

            }
            else {
                if ($(".clschkPeriodHeader:checked").length == 0) {
                    $('#divFloatNavforRoom').addClass("d-none")
                }
            }

        })

        $(document).on("click", "#btnWatchPrevRoomAttendance", function () {
            let selectedDate = configtype == 2 || $("#txtAttDate").parent().parent().hasClass("divDailyparameterddl") ? globalFunctions.toSystemReadableDate($("#txtRoomAttDate").val()) : globalFunctions.toSystemReadableDate($('#txtAttDate').val())
            $.get("/Attendance/Attendance/GetPrevAttendanceByGroupIdAndDate", { groupId: $("#CourseGroupId").val(), courseDate: selectedDate }).then((response) => {

                console.log(response)
                let prvperiodHeaders = ``;
                let headerDetails = ``;
                let prvtrDetails = ``;
                response.PreviousAttendanceHeader.forEach((h) => {
                    let actCss = h.IsActive == true ? "activeDay" : ""
                    prvperiodHeaders += `<th class="text-center arabictxt">${h.DisplayStartTime} - ${h.DisplayEndTime}</th>`
                })

                headerDetails = ` <th>${students}</th>${prvperiodHeaders}`
                $('#tblPreviousClassAttendance thead tr').html(headerDetails);
                response.PreviousAttendanceDetails.filter((i, idx) => {

                    let Pidx = response.PreviousAttendanceDetails.findIndex(x => x.StudentId == i.StudentId);
                    return Pidx == idx

                }).map((x) => {

                    return { StudentId: x.StudentId, StudentName: x.StudentName, StudentImageUrl: !x.StudentImageUrl ? "~/Content/img/avatar-2.jpg" : x.StudentImageUrl, GradeId: x.GradeId, SectionId: x.SectionId, StudentNumber: x.StudentNumber, SessionId: x.SessionId, IsInReport: x.IsInReport, IsInReportID: x.IsInReportID, PeriodNo: x.PeriodNo, SchoolGroupId: x.SchoolGroupId, Nationality: x.Nationality, IsSEN: x.IsSEN }
                }).forEach((s) => {
                    let uaeNationality = s.Nationality == "UAE" ? `<i class="ae flag"></i>` : "";
                    let IsSEN = s.IsSEN ? `<a href="javascript:void(0);" class="text-primary"><i class="fas fa-book-reader"></i></a>` : ""
                    prvtrDetails += `<tr><td>
                                          <div class="student-info d-flex align-items-center">
                                              <span class="rounded-circle overflow-hidden defaultAvatar mr-2">
                                                  <img src="${s.StudentImageUrl}" alt="" class="img-fluid">
                                              </span>
                                              <h3 class="m-0 name text-medium">
                                                   ${s.StudentName.replace("  ", " ")} ${uaeNationality}${IsSEN}
                                                  <span class="small sub-heading text-medium d-block userid"> ${s.StudentNumber} </span>
                                              </h3>
                                          </div>
                                      </td> ${classAttendance.generatePrvClassRoomStudentRows(s, response)}</tr>`


                })


                if ($.fn.DataTable.isDataTable($('#tblPreviousClassAttendance'))) {
                    $('#tblPreviousClassAttendance').DataTable().destroy();
                }

                $('#tblPreviousClassAttendance tbody').html(prvtrDetails);
                $('#tblPreviousClassAttendance').DataTable({
                    "bLengthChange": false,
                    "bFilter": true,
                    "bInfo": false,
                    //columnDefs: [{
                    //    "targets": 'no-sort',
                    //    "bSort": false,
                    //    "order": []
                    //}],

                });
                $('#mpPreviewClassAttendance').modal('toggle')
                $('[data-toggle="popover"]').popover({ trigger: "hover" });

            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });





        })




    },
        getCourseList = function () {
            let courseDate = configtype == 2 || $("#txtAttDate").parent().parent().hasClass("divDailyparameterddl") ? globalFunctions.toSystemReadableDate($("#txtRoomAttDate").val()) : globalFunctions.toSystemReadableDate($('#txtAttDate').val())
            $.get('/Attendance/Attendance/GetGroupsByTimeTable', { courseDate: courseDate }).then((response) => {
                /* response.forEach((x) => {
                     $('#CourseId').append(`<option value="${x.Value}">${x.Text}</option>`).selectpicker('refresh');
 
                 })
                 $("#CourseId").val($("#CourseId option:eq(1)").val()).selectpicker('refresh').trigger('change');*/

                $('#CourseGroupId').empty();
                $('#CourseGroupId').append(`<option value="">Select</option>`)
                if (response.IsWeeklyHoliday) {
                    globalFunctions.showWarningMessage(response.Remark == "" ? WeeklyHoliday : response.Remark)
                    isWeeklyHoliday = response.IsWeeklyHoliday;
                    $("#btnSaveDailyAttendance").addClass("d-none")
                    $('#tblRoomAttendance thead tr').empty();
                    $('#tblRoomAttendance tbody').empty();
                    return false;
                }

                if (response.userTypeId == 4) {

                    if (response.groupList.filter(x => x.IsMemberBelongsToGroup == 1).length > 0) {
                        $('#CourseGroupId').append(`<option value="" class="text-primary" disabled>${MyGroups}</option>`).selectpicker('refresh');
                        response.groupList.filter(x => x.IsMemberBelongsToGroup == 1).forEach((x) => {
                            $('#CourseGroupId').append(`<option value="${x.SchoolGroupId}">${x.SchoolGroupName}</option>`).selectpicker('refresh');
                        })
                    }

                    if (response.groupList.filter(x => x.IsMemberBelongsToGroup == 0).length > 0) {
                        $('#CourseGroupId').append(`<option value="" class="text-primary" disabled> ${OtherGroups} </option>`).selectpicker('refresh');
                        response.groupList.filter(x => x.IsMemberBelongsToGroup == 0).forEach((x) => {
                            $('#CourseGroupId').append(`<option value="${x.SchoolGroupId}">${x.SchoolGroupName}</option>`).selectpicker('refresh');
                        })
                    }

                }
                else {
                    response.groupList.forEach((x) => {
                        $('#CourseGroupId').append(`<option value="${x.SchoolGroupId}">${x.SchoolGroupName}</option>`).selectpicker('refresh');

                    })
                }
                if (response.groupList.length > 0) {

                    response.userTypeId == 4 ? $("#CourseGroupId").val($("#CourseGroupId option:eq(2)").val()).selectpicker('refresh').trigger('change') : $("#CourseGroupId").val($("#CourseGroupId option:eq(1)").val()).selectpicker('refresh').trigger('change');
                }
                else {
                    globalFunctions.showWarningMessage(GroupNotPresentSection)
                    $('#CourseGroupId').empty().selectpicker('refresh');
                    $('#tblRoomAttendance thead tr').empty();
                    $('#tblRoomAttendance tbody').empty();
                }
            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });

        },

        getClassAttendanceDetails = function (courseGroupId) {

            $.get('/Attendance/Attendance/GetClassAttendanceDetails', {
                courseGroupId: $('#CourseGroupId').val(),
                asOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val())
            }).then((response) => {




            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
        },

        expanderContentHeader = function (studentId) {
            let expanderHeader = ``;
            if (configtype == 1) {


                classAttendance.classAttendanceHeader.forEach((ex) => {
                    let activeCss = ex.IsActive ? "" : "bg-light";
                    expanderHeader += `<th data-title="PERIOD-${ex.PeriodNo}" class="text-center ${activeCss}">PERIOD-${ex.PeriodNo}</th>`
                });
            }

            return expanderHeader;
        },
        expanderContent = function (studentId) {
            let expander = ``;
            if (configtype == 1) {


                let AsonDate = globalFunctions.toSystemReadableDate($('#txtAttDate').val());
                classAttendance.classAttendanceHeader.forEach((ch) => {
                    let stu_idx = classAttendance.classAttendanceDetails.findIndex(x => x.StudentId == studentId && x.SessionId == ch.PeriodNo);
                    let ParameterId
                    if (stu_idx > -1) {
                        ParameterId = classAttendance.classAttendanceDetails[stu_idx].ParameterId

                    }

                    let activeCss = ch.IsActive ? "" : "bg-light";
                    let IstoggableDropDown = ch.IsActive ? "dropdown" : ""

                    let idxstatus;
                    let statusdetails;
                    let statusDisplay;
                    let statusdisplyCss = "";
                    let studentspnId = ch.IsActive ? `spn_${studentId}_${ch.PeriodNo}` : '';
                    idxstatus = dailyAttendance.parameterList.findIndex(x => x.Value == ParameterId);
                    statusdetails = dailyAttendance.parameterList[idxstatus];

                    if (idxstatus > -1) {

                        statusDisplay = statusdetails.Text.toUpperCase() == "PRESENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="Present"><i class="fas fa-2x fa-check-circle text-success"></i></span>`
                            : (statusdetails.Text.toUpperCase() == "ABSENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="Absent"><i class="fas fa-2x fa-times-circle text-danger"></i></span>` : `<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${statusdetails.Text}">${statusdetails.Text.substring(0, 1)}</span></i>`)

                        statusdisplyCss = statusdetails.Text.toUpperCase() == "PRESENT" || statusdetails.Text.toUpperCase() == "ABSENT" ? "" : "other-status";

                    }
                    else {
                        statusDisplay = `<i class="fas fa-2x fa-plus-circle text-default"></i>`;
                    }



                    expander += `
                                   <td data-title="PERIOD-${ch.PeriodNo}" class="text-center ${activeCss}">
                                       <!--Attendance dropdown event starts-->                                                                                 
                                             <a href="javascript:void(0)" class="nav-link p-0 text-primary" data-toggle="${IstoggableDropDown}" aria-haspopup="true" aria-expanded="false">
                                                 <div id="${studentspnId}" class="${statusdisplyCss}">
                                                     ${statusDisplay}    
                                                  </div>                                                                                    
                                             </a>
                                             <div class="dropdown-menu dropdown-menu-right z-depth-1 pt-2 pb-2  ${blockasPerpagePermission}" style="z-index: 1034;">
                                                 <a class="dropdown-item" data-stuId="${studentId}" data-isPresent="1" data-other="${idPresent}" data-SessionId="${ch.PeriodNo}" href="javascript:void(0)" onclick="classAttendance.dropdownMenu(this)"> ${present}</a>
                                                 <a class="dropdown-item" data-stuId="${studentId}" data-isPresent="0" data-other="${idAbsent}" data-SessionId="${ch.PeriodNo}" href="javascript:void(0)" onclick="classAttendance.dropdownMenu(this)"> ${absent}</a>
                                                 <div class="dropdown-divider mb-0"></div>
                                                 <h6 class="dropdown-header bg-light p-1 pl-2 mb-1 text-dark"><small class="text-uppercase"><strong>${others}</strong></small></h6>
                                                 <div class="">
                                                   ${classAttendance.generateParameterList(studentId, ch.PeriodNo)}
                                                 </div>
                                             </div>
                                             <!--Attendance dropdown event starts-->
                                              </td>`





                })

            }
            return expander;

        },

        AppendStatusForSelected = function (drp) {
            let sessionId = $(drp).attr('data-SessionId');
            let selParamvalue = $(drp).attr('data-other');
            let dataPresent = $(drp).attr('data-isPresent');
            attendanceMarkValue = dataPresent;
            let remark;
            let appendStatus;
            let appendStatusOnStudent;
            let appendDisplayClass = "";
            if (dataPresent == 0) {
                appendStatus = `<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${absent}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>`;
                appendStatusOnStudent = `<i class="fas fa fa-times-circle text-danger"></i>`;
                $('#spnClassSelectAll_' + sessionId).html(`<i class="fas fa-2x fa-times-circle text-danger"></i>`);
                $('.spnClassSelectAll').html(`<i class="fas fa-2x fa-times-circle text-danger"></i>`);
                remark = `${absent}`;
            }
            else if (dataPresent == 1) {
                appendStatus = `<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${present}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`;
                appendStatusOnStudent = `<i class="fas fa fa-check-circle text-success"></i>`;
                $('#spnClassSelectAll_' + sessionId).html(`<i class="fas fa-2x fa-check-circle text-success"></i>`);
                $('.spnClassSelectAll').html(`<i class="fas fa-2x fa-check-circle text-success"></i>`);
                remark = `${present}`;
            }
            else {
                let indexparamid = dailyAttendance.parameterList.findIndex(x => x.Value == selParamvalue)
                let paramValue = dailyAttendance.parameterList[indexparamid].Text;
                appendStatus = `<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${paramValue}">${paramValue.substring(0, 1)}</span></i>`;
                appendStatusOnStudent = `<i class="fas fa-circle text-warning position-relative"><span class="font-small position-absolute text-bold text-white">${paramValue.substring(0, 1)}</span ></i>`;
                $('#spnClassSelectAll_' + sessionId).html(appendStatusOnStudent).addClass('other-status');
                $('.spnClassSelectAll_' + sessionId).html(appendStatusOnStudent).addClass('other-status');
                remark = paramValue;
                appendDisplayClass = "other-status";
            }

            /*if ($('#selectall').is(":checked")) {

                dailyAttendance.studentDailyAttendanceModel.filter((i, idx) => {

                    var Midx = dailyAttendance.studentDailyAttendanceModel.findIndex(x => x.StudentId == i.StudentId);
                    return Midx == idx
                }).forEach((st) => {
                    let studentId = st.StudentId;
                    let registerId = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == currenPeriodNo).length > 0 ? dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == currenPeriodNo)[0].RegisterId : 0;

                    $('#spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                    $('#spnstatus_' + studentId).html(appendStatusOnStudent).addClass(appendDisplayClass);
                    $('.spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);

                    let idx = dailyAttendance.AttendanceStudentModel.findIndex(x => x.StudentId == studentId && x.SessionId == sessionId);

                    if (idx > -1) {
                        dailyAttendance.AttendanceStudentModel[idx].ParameterId = selParamvalue;
                        dailyAttendance.AttendanceStudentModel[idx].Remark = remark;
                        dailyAttendance.AttendanceStudentModel[idx].SessionId = sessionId;
                    }
                    else {
                        dailyAttendance.AttendanceStudentModel.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                    }

                });


            }
            else {
                $('.clsChkforstudent:checked').each(function () {
                    let studentId = $(this).attr('data-stuid')

                    let registerId = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == sessionId).length > 0 ? dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == sessionId)[0].RegisterId : 0;


                    $('#spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                    $('.spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                    $('#spnstatus_' + studentId).html(appendStatusOnStudent).addClass(appendDisplayClass);

                    let idx = dailyAttendance.AttendanceStudentModel.findIndex(x => x.StudentId == studentId && x.SessionId == sessionId);
                    if (idx > -1) {
                        dailyAttendance.AttendanceStudentModel[idx].ParameterId = selParamvalue;
                        dailyAttendance.AttendanceStudentModel[idx].Remark = remark;
                        dailyAttendance.AttendanceStudentModel[idx].SessionId = sessionId;
                    }
                    else {
                        dailyAttendance.AttendanceStudentModel.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                    }

                });
            }*/


            $('.clsChkforstudent:checked').each(function () {
                let studentId = $(this).attr('data-stuid')

                let registerId = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == sessionId).length > 0 ? dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == sessionId)[0].RegisterId : 0;


                $('#spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                $('.spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                $('#spnstatus_' + studentId).html(appendStatusOnStudent).addClass(appendDisplayClass);

                $('.spn_' + studentId + '_' + sessionId).parent().parent().find(".clsDailySingleOtherOption").removeClass('d-none');
                if (attendanceMarkValue == "1")//Present
                {
                    $('.spn_' + studentId + '_' + sessionId).parent().parent().find(".clsDailySingleOtherOption").attr('data-ispresent', "1");
                }
                else if (attendanceMarkValue == "0")//Absent
                {
                    $('.spn_' + studentId + '_' + sessionId).parent().parent().find(".clsDailySingleOtherOption").attr('data-ispresent', "0");
                }
                else {
                    $('.spn_' + studentId + '_' + sessionId).parent().parent().find(".clsDailySingleOtherOption").attr('data-ispresent', "-1");
                }

                let idx = dailyAttendance.AttendanceStudentModel.findIndex(x => x.StudentId == studentId && x.SessionId == sessionId);
                if (idx > -1) {
                    dailyAttendance.AttendanceStudentModel[idx].ParameterId = selParamvalue;
                    dailyAttendance.AttendanceStudentModel[idx].Remark = remark;
                    dailyAttendance.AttendanceStudentModel[idx].SessionId = sessionId;
                }
                else {
                    dailyAttendance.AttendanceStudentModel.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                }

            });

            $('[data-toggle="tooltip"]').tooltip();
        },
        dropdownMenu = function (drp) {
            let dataPresent = $(drp).attr('data-isPresent');
            let selParamvalue = $(drp).attr('data-other');
            let studentId = $(drp).attr('data-stuId');
            let sessionId = $(drp).attr('data-SessionId');
            let registerId = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == sessionId).length > 0 ? dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == sessionId)[0].RegisterId : 0;
            let remark = undefined;
            if (dataPresent == 0) {
                $('#spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="Absent"><i class="fas fa-2x fa-times-circle text-danger"></i></span>`)
                $('#spnstatus_' + studentId).html(`<i class="fas fa fa-times-circle  text-danger"></i>`)
                $('.spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="Absent"><i class="fas fa-2x fa-times-circle text-danger"></i></span>`)
                remark = "Absent";
            }
            else if (dataPresent == 1) {
                $('#spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="Present"><i class="fas fa-2x fa-check-circle text-success"></i></span>`)
                $('#spnstatus_' + studentId).html(`<i class="fas fa fa-check-circle  text-success"></i>`)
                $('.spn_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="Present"><i class="fas fa-2x fa-check-circle text-success"></i></span>`)
                remark = "Present";
            }
            else {
                let indexparamid = dailyAttendance.parameterList.findIndex(x => x.Value == selParamvalue)
                let paramValue = dailyAttendance.parameterList[indexparamid].Text;
                $('#spn_' + studentId + '_' + sessionId).html(`<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${paramValue}">${paramValue.substring(0, 1)}</span></i>`).addClass('other-status')
                $('#spnstatus_' + studentId).html(`<i class="fas fa-circle  text-warning position-relative"><span class="font-small position-absolute text-bold text-white">${paramValue.substring(0, 1)}</span></i>`).addClass('other-status')
                $('.spn_' + studentId + '_' + sessionId).html(`<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${paramValue}">${paramValue.substring(0, 1)}</span></i>`).addClass('other-status')
                remark = paramValue;
            }

            let index = dailyAttendance.AttendanceStudentModel.findIndex(x => x.StudentId == studentId && x.SessionId == sessionId);
            if (index > -1) {
                dailyAttendance.AttendanceStudentModel[index].ParameterId = selParamvalue;
                dailyAttendance.AttendanceStudentModel[index].Remark = remark;
                dailyAttendance.AttendanceStudentModel[index].SessionId = sessionId;
            }
            else {
                dailyAttendance.AttendanceStudentModel.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
            }
            $('[data-toggle="tooltip"]').tooltip();
        },
        generateParameterList = function (studentId, sessionId) {
            let trothercat = ``;
            dailyAttendance.parameterList.forEach((p) => {
                trothercat += `<a class="dropdown-item" data-other="${p.Value}" data-stuId="${studentId}" data-SessionId="${sessionId}" data-periodNo="" data-subject="" href="javascript:void(0)" onclick="classAttendance.dropdownMenu(this)">${p.Text}</a>`
            });
            return trothercat;
        },
        generateParameterHeaderList = function (periodNo) {
            let otherCategoryForAll = ``;
            dailyAttendance.parameterList.forEach((p) => {
                otherCategoryForAll += `<a class="dropdown-item" data-SessionId="${periodNo}" data-other="${p.Value}" href="javascript:void(0)" onclick="classAttendance.AppendStatusForSelected(this)" >${p.Text}</a>`
            })
            return otherCategoryForAll;
        },
        /*========= Class Attendance listing==========*/

        getClasAttendanceListing = function () {
            let trDetails = ``;
            let periodHeaders = ``;
            let thDetails = ``;
            let classHeader = ``;

            let thMrkClsAtt = ``;
            let tdMrkclsAtt = ``;
            classAttendance.classAttendanceHeader.forEach((hr) => {
                let activeheaderCss = hr.IsActive == true ? "" : "bg-light";
                let togglecss = hr.IsActive == true ? "dropdown" : "";
                let spnAppendStatuson = hr.IsActive == true ? `spnClassSelectAll_${hr.PeriodNo}` : "";
                thMrkClsAtt += `<th data-title="PERIOD-${hr.PeriodNo}" class="text-center ${activeheaderCss}">PERIOD-${hr.PeriodNo}</th>`
                tdMrkclsAtt += `<td data-title="PERIOD-${hr.PeriodNo}" class="text-center">
                                     <!--Attendance dropdown event starts-->
                                     <a href="javascript:void(0)" class="nav-link p-0 text-primary" data-toggle="${togglecss}" aria-haspopup="true" aria-expanded="false">
                                          <span class="${spnAppendStatuson}">
                                         <i class="fas fa-2x fa-plus-circle text-default"></i>
                                           </span>
                                     </a>
                                     <div class="dropdown-menu dropdown-menu-right z-depth-1 pt-2 pb-2  ${blockasPerpagePermission}" style="z-index: 1034;">
                                          <a class="dropdown-item"  data-SessionId="${hr.PeriodNo}" data-isPresent="1" data-other="${idPresent}" onclick="classAttendance.AppendStatusForSelected(this)" href="javascript:void(0)"> Present</a>
                                          <a class="dropdown-item"  data-SessionId="${hr.PeriodNo}" data-isPresent="0" data-other="${idAbsent}" onclick="classAttendance.AppendStatusForSelected(this)" href="javascript:void(0)"> Absent</a>
                                          <div class="dropdown-divider mb-0"></div>
                                          <h6 class="dropdown-header bg-light p-1 pl-2 mb-1 text-dark"><small class="text-uppercase"><strong>Other</strong></small></h6>
                                          <div  class="">
                                                   ${classAttendance.generateParameterHeaderList(hr.PeriodNo)}
                                           </div>
                                     </div>
                                     <!--Attendance dropdown event starts-->
                                     </td>`

                let activeCss = hr.IsActive == true ? "activeDay" : "";
                periodHeaders += `<th class="text-center ${activeCss}"><input type="checkbox" data-periodNo="${hr.PeriodNo}" id="chkPeriod_${hr.PeriodNo}" class="mr-2 clschkPeriodHeader"/> Period ${hr.PeriodNo}</th>`
            })

            thDetails = `<th class="text-center">
                           <div class="checkbox">
                             <input type="checkbox" id="selectAllList" class="mr-2"><label></label>
                          </div>
                         </th>
                          <th>Students</th>
                          ${periodHeaders}`



            studentlist.forEach((st) => {
                let uaeNationality = st.Nationality == "UAE" ? `<i class="ae flag"></i>` : ""
                trDetails += `<tr>
                               <td class="text-center">
                                 <input type="checkbox" data-stuId="${st.StudentId}"  class="dt-checkboxes clsChk_${st.StudentId}"><label></label>      
                                 </td>
                                      <td>
                                          <div class="student-info d-flex align-items-center">
                                              <span class="rounded-circle overflow-hidden defaultAvatar mr-2">
                                                  <img src="${st.StudentImageUrl}" alt="" class="img-fluid">
                                              </span>
                                              <h3 class="m-0 name text-medium">
                                                   ${st.StudentName.replace("  ", " ")} ${uaeNationality}
                                                  <span class="small sub-heading text-medium d-block userid"> ${st.StudentNumber} </span>
                                              </h3>
                                          </div>
                                      </td>
                            ${classAttendance.generateClassStudentRows(st)}</tr>`



            })


            $('#tblMarkClassAttList thead tr').html(thMrkClsAtt);
            $('#tblMarkClassAttList tbody tr').html(tdMrkclsAtt);

            $('#tblRoomAttendance thead tr').html(thDetails);
            $('#tblRoomAttendance tbody').html(trDetails);

        },
        generateClassStudentRows = function (st) {
            let classAttendanceListRows = ``;

            classAttendance.classAttendanceHeader.forEach((td) => {

                classAttendanceListRows += `
                                         ${classAttendance.generatePeriodRows(st.StudentId, td.PeriodNo, td.IsActive)}`

            })
            return classAttendanceListRows;
        },
        generatePeriodRows = function (studentId, periodNo, IsActive) {
            let sessionRows = ``;
            let stu_idx = classAttendance.classAttendanceDetails.findIndex(x => x.StudentId == studentId && x.SessionId == periodNo);


            let ParameterId;
            if (stu_idx > -1) {
                ParameterId = classAttendance.classAttendanceDetails[stu_idx].ParameterId

            }
            //let cssIsActive = x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? 'activeDay' : '';
            let dnone = IsActive == true ? '' : 'd-none';
            let idxstatus;
            let statusdetails;
            let statusDisplay;
            let statusCss = "";
            let dateCss = IsActive == true ? "" : "bg-light";
            idxstatus = dailyAttendance.parameterList.findIndex(p => p.Value == ParameterId);
            statusdetails = dailyAttendance.parameterList[idxstatus];
            if (idxstatus > -1) {

                statusDisplay = statusdetails.Text.toUpperCase() == "PRESENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="Present"><i class="fas fa-2x fa-check-circle text-success"></i></span>`
                    : (statusdetails.Text.toUpperCase() == "ABSENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${statusdetails.Text}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>` : `<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${statusdetails.Text}">${statusdetails.Text.substring(0, 1)}</span></i>`)

                statusCss = statusdetails.Text.toUpperCase() == "PRESENT" || statusdetails.Text.toUpperCase() == "ABSENT" ? "" : "other-status";
            }
            else {
                statusDisplay = `<i class="fas fa-2x fa-plus-circle text-default"></i>`;
            }
            let studentspnId = IsActive == true ? `spn_${studentId}_${periodNo}` : '';
            sessionRows += ` <td class="text-center  ${dateCss}">
                            <a href="javascript:void(0)" class="d-inline-block nav-link p-0 text-primary" data-toggle="dropdown" data-placement="left" aria-haspopup="true" aria-expanded="false">
                                <span class="${studentspnId} ${statusCss}">
                                    ${statusDisplay}
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right z-depth-1 pt-2 pb-2 w-25 ${dnone}  ${blockasPerpagePermission}" style="z-index: 1034;">
                                <a class="dropdown-item" data-stuId="${studentId}" data-isPresent="1" data-other="${idPresent}" data-SessionId="${periodNo}" href="javascript:void(0)" onclick="classAttendance.dropdownMenu(this)"> Present</a>
                                <a class="dropdown-item" data-stuId="${studentId}" data-isPresent="0" data-other="${idAbsent}" data-SessionId="${periodNo}" href="javascript:void(0)" onclick="classAttendance.dropdownMenu(this)"> Absent</a>
                                <div class="dropdown-divider mb-0"></div>
                                <h6 class="dropdown-header bg-light p-1 pl-2 mb-1 text-dark"><small class="text-uppercase"><strong>Other</strong></small></h6>
                                <div class="">
                                    ${classAttendance.generateParameterList(studentId, periodNo)}
                                </div>

                            </div>
                        </td>`


            return sessionRows;

        },

        getClassListforMergeConfig = function (coursegroupId) {
            let asOnClassDate = classAttendance.frmMyplannerasonDate != "" && classAttendance.frmMyplannerasonDate != undefined ? classAttendance.frmMyplannerasonDate : (configtype == 2 || $("#txtAttDate").parent().parent().hasClass("divDailyparameterddl") ? globalFunctions.toSystemReadableDate($("#txtRoomAttDate").val()) : globalFunctions.toSystemReadableDate($('#txtAttDate').val()))
            $.get('/Attendance/Attendance/GetClassListForMergeConfig', { courseGroupId: coursegroupId, asOnDate: asOnClassDate }).then((response) => {
                if (response.IsWeeklyHoliday) {
                    globalFunctions.showWarningMessage(response.Remark == "" ? WeeklyHoliday : response.Remark)
                    isWeeklyHolidayForRoom = response.IsWeeklyHoliday;
                    holidayRemarkForRoom = response.Remark == "" ? WeeklyHoliday : response.Remark;
                    $('#tblRoomAttendance thead tr').empty();
                    $('#tblRoomAttendance tbody').empty();
                    classAttendance.ClassAttendanceModuleStruct.length = 0;
                    classAttendance.classAttendanceHeader.length = 0;
                    classAttendance.classAttendanceDetails.length = 0;
                    $("#btnSaveDailyAttendance").addClass("d-none")
                    return false;
                }
                classAttendance.classAttendanceHeader = response.ClassAttendanceHeaderList;
                classAttendance.classAttendanceDetails = response.ClassAttendanceDetailsList;
                classAttendance.ClassAttendanceModuleStruct.length = 0;

                if (classAttendance.classAttendanceHeader.length > 0) {


                    $("#btnSaveDailyAttendance").removeClass("d-none")
                    classAttendance.getClassRoomAttendanceListing();
                }
                else {

                    if (configtype == 1) {
                        $("#btnSaveDailyAttendance").addClass("d-none")
                    }
                    if ($('#btnroomAttendance').hasClass('active')) {
                        $("#btnSaveDailyAttendance").addClass("d-none")
                        globalFunctions.showWarningMessage("No lessons scheduled for today in the Timetable")
                        $('#tblRoomAttendance thead tr').empty();
                        $('#tblRoomAttendance tbody').empty();
                    }

                }

            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });

        },
        getClassRoomAttendanceListing = function () {
            let trDetails = ``;
            let periodHeaders = ``;
            let thDetails = ``;
            let classHeader = ``;
            let IsStuOnReport = ``;
            let thMrkClsAtt = ``;
            let tdMrkclsAtt = ``;
            let displayCheckBox = classAttendance.classAttendanceHeader.filter(x => x.IsActive == true).length > 1 ? "" : "d-none"; //classAttendance.classAttendanceHeader.length > 1 ? "" : "d-none";

            classAttendance.classAttendanceHeader.forEach((hr) => {
                let activeheaderCss = hr.IsActive == true ? "" : "bg-light";
                let togglecss = hr.IsActive == true ? "dropdown" : "";
                let spnAppendStatuson = hr.IsActive == true ? `spnClassRoomSelectAll_${hr.PeriodNo}` : "";
                thMrkClsAtt += `<th data-title="PERIOD-${hr.PeriodNo}" class="text-center ${activeheaderCss}">PERIOD-${hr.PeriodNo}</th>`
                tdMrkclsAtt += `<td data-title="PERIOD-${hr.PeriodNo}" class="text-center">
                                     <!--Attendance dropdown event starts-->
                                     <a href="javascript:void(0)" class="nav-link p-0 text-primary" data-toggle="${togglecss}" aria-haspopup="true" aria-expanded="false">
                                          <span class="${spnAppendStatuson}">
                                         <i class="fas fa-2x fa-plus-circle text-default"></i>
                                           </span>
                                     </a>
                                     <div class="dropdown-menu dropdown-menu-right z-depth-1 pt-2 pb-2  ${blockasPerpagePermission}" style="z-index: 1034;">
                                          <a class="dropdown-item"  data-SessionId="${hr.PeriodNo}" data-isPresent="1" data-other="${idPresent}" onclick="classAttendance.AppendStatusForSelectedRoom(this)" href="javascript:void(0)"> Present</a>
                                          <a class="dropdown-item"  data-SessionId="${hr.PeriodNo}" data-isPresent="0" data-other="${idAbsent}" onclick="classAttendance.AppendStatusForSelectedRoom(this)" href="javascript:void(0)"> Absent</a>
                                          <div class="dropdown-divider mb-0"></div>
                                          <h6 class="dropdown-header bg-light p-1 pl-2 mb-1 text-dark"><small class="text-uppercase"><strong>Other</strong></small></h6>
                                          <div  class="">
                                                   ${classAttendance.generateParameterHeaderRoomList(hr.PeriodNo)}
                                           </div>
                                     </div>
                                     <!--Attendance dropdown event starts-->
                                     </td>`

                let activeCss = hr.IsActive == true ? "activeDay" : "";
                let periodcheckboxVisibility = hr.IsActive == true && displayCheckBox == "" ? "" : "d-none";

                periodHeaders += `<th class="text-center arabictxt ${activeCss}"><input type="checkbox" data-periodNo="${hr.PeriodNo}" id="chkPeriod_${hr.PeriodNo}" class="mr-2 clschkPeriodHeader ${periodcheckboxVisibility}"/>${hr.DisplayStartTime} - ${hr.DisplayEndTime}</th>`


            })

            thDetails = `<th class="text-center">
                           <div class="checkbox">
                             <input type="checkbox" id="selectAllRoomList" class=""><label></label>
                          </div>
                         </th>
                          <th>${students}</th>
                          <th class="text-center">${tblheaderforDailyAttendance}</th>
                          ${periodHeaders}`



            classAttendance.classAttendanceDetails.filter((i, idx) => {

                var Midx = classAttendance.classAttendanceDetails.findIndex(x => x.StudentId == i.StudentId);

                return Midx == idx
            }).map((x) => {

                return { StudentId: x.StudentId, StudentName: x.StudentName, StudentImageUrl: !x.StudentImageUrl ? "~/Content/img/avatar-2.jpg" : x.StudentImageUrl, GradeId: x.GradeId, SectionId: x.SectionId, StudentNumber: x.StudentNumber, SessionId: x.SessionId, IsInReport: x.IsInReport, IsInReportID: x.IsInReportID, PeriodNo: x.PeriodNo, DailyAttendanceParameterId: x.DailyAttendanceParameterId, DailyAttendanceParameterRemark: x.DailyAttendanceParameterRemark, Nationality: x.Nationality, IsSEN: x.IsSEN }
            }).forEach((st) => {
                IsStuOnReport = "";
                if (st.IsInReport) {

                    IsStuOnReport += `<span class="d-inline badge badge-danger mr-1" data-periodno="${st.PeriodNo}" onclick="ClassListObject.ViewStudentOnReportDetails(${st.IsInReportID},2,${st.StudentId}, this)" data-target="studentOnReportDetailsFormDiv" data-width="45%" data-point="0" data-toggle="tooltip" title="StudentOnReport"><span><i class="fas fa-binoculars"></i></span></span>`
                }
                else {
                    IsStuOnReport += ""
                }
                let uaeNationality = st.Nationality == "UAE" ? `<i class="ae flag"></i>` : "";
                let IsSEN = st.IsSEN ? `<a href="javascript:void(0);" class="text-primary"><i class="fas fa-book-reader"></i></a>` : ""
                trDetails += `<tr>
                               <td class="text-center">

                                ${IsStuOnReport}

                                  <input type="checkbox" data-stuId="${st.StudentId}"  class="dt-checkboxesRoom clsChkRoom_${st.StudentId}"><label></label> 
                                 </td>
                                      <td>
                                          <div class="student-info d-flex align-items-center">
                                              <span class="rounded-circle overflow-hidden defaultAvatar mr-2">
                                                  <img src="${st.StudentImageUrl}" alt="" class="img-fluid">
                                              </span>
                                              <h3 class="m-0 name text-medium">
                                                   ${st.StudentName.replace("  ", " ")} ${uaeNationality}${IsSEN}
                                                  <span class="small sub-heading text-medium d-block userid"> ${st.StudentNumber} </span>
                                              </h3>
                                          </div>
                                      </td>
                                       <td class="text-center">
                              ${classAttendance.displayDailyAttendanceOnRoom(st)}
                            
                           </td>
                            ${classAttendance.generateClassRoomStudentRows(st)}</tr>`



            })


            $('#tblMarkClassAttList thead tr').html(thMrkClsAtt);
            $('#tblMarkClassAttList tbody tr').html(tdMrkclsAtt);

            $('#tblRoomAttendance thead tr').html(thDetails);
            $('#tblRoomAttendance tbody').html(trDetails);

            if ($('.clschkPeriodHeader').hasClass('d-none') && classAttendance.classAttendanceHeader.filter(x => x.IsActive == true).length == 1) {
                $('.clschkPeriodHeader').prop('checked', true);
            }

        },
        generateClassRoomStudentRows = function (st) {
            let classAttendanceListRows = ``;

            classAttendance.classAttendanceHeader.forEach((td) => {

                classAttendanceListRows += `
                                         ${classAttendance.generateRoomPeriodRows(st.StudentId, td.PeriodNo, td.IsActive, td.SchoolGroupId)}`

            })
            return classAttendanceListRows;
        },
        generateRoomPeriodRows = function (studentId, periodNo, IsActive, SchoolGroupId) {
            let sessionRows = ``;
            let stu_idx = classAttendance.classAttendanceDetails.findIndex(x => x.StudentId == studentId && x.SessionId == periodNo && x.SchoolGroupId == SchoolGroupId);


            let ParameterId;
            let remark;
            if (stu_idx > -1) {
                ParameterId = classAttendance.classAttendanceDetails[stu_idx].ParameterId;
                remark = classAttendance.classAttendanceDetails[stu_idx].Remark;
            }

            //let cssIsActive = x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? 'activeDay' : '';
            let dnone = IsActive == true ? '' : 'd-none';
            let idxstatus;
            let statusdetails;
            let statusDisplay;
            let statusCss = "";
            let dateCss = IsActive == true ? "" : "bg-light";
            idxstatus = dailyAttendance.parameterList.findIndex(p => p.Value == ParameterId);
            statusdetails = dailyAttendance.parameterList[idxstatus];
            let datatogglremark;
            if (idxstatus > -1) {

                statusDisplay = statusdetails.Text.toUpperCase() == "PRESENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`
                    : (statusdetails.Text.toUpperCase() == "ABSENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>` : `<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}">${statusdetails.Text.substring(0, 1)}</span></i>`)

                statusCss = statusdetails.Text.toUpperCase() == "PRESENT" || statusdetails.Text.toUpperCase() == "ABSENT" ? "" : "other-status";
            }
            else {
                if (!IsActive) {


                    statusCss = "other-status";
                    datatogglremark = `data-toggle="tooltip" data-placement="left" title="${remarkForUnmarkedAttendance}"`
                    statusDisplay = `<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white ">U</span></i>`;
                }
                else {
                    statusDisplay = `<i class="fas fa-2x fa-plus-circle text-default"></i>`;
                }
            }
            let studentspnId = IsActive == true ? `spnRoom_${studentId}_${periodNo}` : '';
            let studentOtherDetails = ParameterId == undefined ? "d-none" : "";
            sessionRows += ` <td class="text-center  ${dateCss} position-relative">
                            <a href="javascript:void(0)" class="d-inline-block nav-link p-0 text-primary" data-toggle="dropdown" data-placement="left" aria-haspopup="true" aria-expanded="false">
                                <span class="${studentspnId} ${statusCss}" ${datatogglremark}>
                                    ${statusDisplay}
                                </span>
                            </a>
                             <a href="javascript:void(0)" class="p-0 text-primary clsRoomSingleOtherOption clsadditionalComment ${studentOtherDetails} position-absolute ml-2" data-stuId="${studentId}" data-SessionId="${periodNo}"  data-toggle="modal" data-target="#mpOtherforRoomOptions" data-isPresent="${ParameterId}" style="
                                top: 22px;
                                ">
                            <img src="/Content/VLE/img/speech-bubble.png" alt="speech-bubble-icon" class="speech-bubble" style="
                                  width: 20px;
                                  line-height: 1;
                                 ">
                              </a>
                            <div class="dropdown-menu dropdown-menu-right z-depth-1 pt-2 pb-2 w-25 ${dnone}  ${blockasPerpagePermission}" style="z-index: 1034;">
                                <a class="dropdown-item ml-2 d-block" data-stuId="${studentId}" data-isPresent="1" data-other="${idPresent}" data-SessionId="${periodNo}" href="javascript:void(0)" onclick="classAttendance.dropdownMenuRoom(this,true)"> ${present}</a>
                                <a class="dropdown-item ml-2 d-block clsRoomSingleOtherOption" data-stuId="${studentId}" data-isPresent="0" data-other="${idAbsent}" data-SessionId="${periodNo}" href="javascript:void(0)"  onclick="classAttendance.dropdownMenuRoom(this,true)"> ${absent}</a>
                                ${classAttendance.generateRoomParameterShortCut(studentId, periodNo)}
                                <div class="dropdown-divider mb-0"></div>
                                <h6 class="dropdown-header bg-light p-1 pl-2 mb-1 text-dark">
                               <small class="text-uppercase">
                               <strong>
                              <a href="javascript:void(0)" data-stuId="${studentId}" data-SessionId="${periodNo}" data-toggle="modal" data-target="#mpOtherforRoomOptions" class="clsRoomSingleOtherOption">
                                       <span>${Others}</span>
                                     </a>
                              </strong>
                               </small>
                                </h6>
                                <div class="">
                                   <!-- ${classAttendance.generateParameterListForRoom(studentId, periodNo)}-->
                                </div>

                            </div>
                        </td>`


            return sessionRows;

        },
        AppendStatusForSelectedRoom = function (drp) {
            let sessionId;
            $('.clschkPeriodHeader:checked').each(function () {

                sessionId = $(this).attr('data-periodno');
                let selParamvalue = $(drp).attr('data-other');
                let dataPresent = $(drp).attr('data-isPresent');
                roomattendanceMarkValue = dataPresent;
                let remark;
                let appendStatus;
                let appendStatusOnStudent;
                let appendDisplayClass = "";
                if (dataPresent == 0) {
                    appendStatus = `<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${absent}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>`;
                    appendStatusOnStudent = `<i class="fas fa fa-times-circle text-danger"></i>`;
                    $('#spnClassRoomSelectAll_' + sessionId).html(`<i class="fas fa-2x fa-times-circle text-danger"></i>`);
                    $('.spnClassRoomSelectAll_' + sessionId).html(`<i class="fas fa-2x fa-times-circle text-danger"></i>`);
                    remark = `${absent}`;
                }
                else if (dataPresent == 1) {
                    appendStatus = `<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${present}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`;
                    appendStatusOnStudent = `<i class="fas fa fa-check-circle text-success"></i>`;
                    $('#spnClassRoomSelectAll_' + sessionId).html(`<i class="fas fa-2x fa-check-circle text-success"></i>`);
                    $('.spnClassRoomSelectAll_' + sessionId).html(`<i class="fas fa-2x fa-check-circle text-success"></i>`);
                    remark = `${present}`;
                }
                else {
                    let indexparamid = dailyAttendance.parameterList.findIndex(x => x.Value == selParamvalue)
                    let paramValue = dailyAttendance.parameterList[indexparamid].Text;
                    remark = $('#txtoptionalRemarkForRoom').val() == "" ? paramValue : $('#txtoptionalRemarkForRoom').val();
                    appendStatus = `<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}">${paramValue.substring(0, 1)}</span></i>`;
                    appendStatusOnStudent = `<i class="fas fa-circle text-warning position-relative"><span class="font-small position-absolute text-bold text-white">${paramValue.substring(0, 1)}</span ></i>`;
                    $('#spnClassRoomSelectAll_' + sessionId).html(appendStatusOnStudent).addClass('other-status');
                    $('.spnClassRoomSelectAll_' + sessionId).html(appendStatusOnStudent).addClass('other-status');

                    appendDisplayClass = "other-status";
                }


                classAttendance.classAttendanceHeader.filter(x => x.PeriodNo == sessionId).forEach((th) => {

                    if (classAttendance.ClassAttendanceModuleStruct.filter(x => x.SessionId == th.PeriodNo).length == 0) {
                        classAttendance.classAttendanceDetails.filter((i, idx) => {

                            var Midx = classAttendance.classAttendanceDetails.findIndex(x => x.StudentId == i.StudentId);

                            return Midx == idx
                        }).map((x) => {
                            let parameterId = classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val()).length > 0 ? classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val())[0].ParameterId : idPresent;
                            let regId = classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val()).length > 0 ? classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val())[0].RegisterId : 0;
                            let remark = classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val()).length > 0 ? classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val())[0].Remark : "Present";


                            classAttendance.ClassAttendanceModuleStruct.push({ StudentId: x.StudentId, SessionId: th.PeriodNo, RegisterId: regId, ParameterId: parameterId, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                        })
                    }


                })


                $('.dt-checkboxesRoom:checked').each(function () {
                    let studentId = $(this).attr('data-stuid')

                    let registerId = classAttendance.classAttendanceDetails.filter(x => x.StudentId == studentId && x.SessionId == sessionId).length > 0 ? classAttendance.classAttendanceDetails.filter(x => x.StudentId == studentId && x.SessionId == sessionId)[0].RegisterId : 0;

                    $('.spnRoom_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                    $('.spnRoom_' + studentId + '_' + sessionId).parent().parent().find(".clsRoomSingleOtherOption").removeClass('d-none');

                    if (roomattendanceMarkValue == 0) {
                        //$('.spnRoom_' + studentId + '_' + sessionId).parent().parent().find(".clsRoomSingleOtherOption").attr('data-ispresent', "0")[1];                       
                    }
                    else if (roomattendanceMarkValue == 1) {
                        //$('.spnRoom_' + studentId + '_' + sessionId).parent().parent().find(".clsRoomSingleOtherOption").attr('data-ispresent', "1")[1];
                    }
                    else {
                        $('.spnRoom_' + studentId + '_' + sessionId).parent().parent().find(".clsRoomSingleOtherOption").attr('data-ispresent', "-1")[1];
                    }
                    //$('.spnRoom_' + studentId + '_' + sessionId).parent().parent().find(".clsRoomSingleOtherOption").attr('data-other', selParamvalue)[1];
                    let idx = classAttendance.ClassAttendanceModuleStruct.findIndex(x => x.StudentId == studentId && x.SessionId == sessionId);
                    if (idx > -1) {
                        classAttendance.ClassAttendanceModuleStruct[idx].ParameterId = selParamvalue;
                        classAttendance.ClassAttendanceModuleStruct[idx].Remark = remark;
                        classAttendance.ClassAttendanceModuleStruct[idx].SessionId = sessionId;
                        classAttendance.ClassAttendanceModuleStruct[idx].RegisterId = registerId;
                    }
                    else {
                        classAttendance.ClassAttendanceModuleStruct.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                    }

                });
            });
            /*if ($('#selectall').is(":checked")) {

                dailyAttendance.studentDailyAttendanceModel.filter((i, idx) => {

                    var Midx = dailyAttendance.studentDailyAttendanceModel.findIndex(x => x.StudentId == i.StudentId);
                    return Midx == idx
                }).forEach((st) => {
                    let studentId = st.StudentId;
                    let registerId = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == currenPeriodNo).length > 0 ? dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == currenPeriodNo)[0].RegisterId : 0;

                    $('#spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                    $('#spnstatus_' + studentId).html(appendStatusOnStudent).addClass(appendDisplayClass);
                    $('.spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);

                    let idx = dailyAttendance.AttendanceStudentModel.findIndex(x => x.StudentId == studentId && x.SessionId == sessionId);

                    if (idx > -1) {
                        dailyAttendance.AttendanceStudentModel[idx].ParameterId = selParamvalue;
                        dailyAttendance.AttendanceStudentModel[idx].Remark = remark;
                        dailyAttendance.AttendanceStudentModel[idx].SessionId = sessionId;
                    }
                    else {
                        dailyAttendance.AttendanceStudentModel.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                    }

                });


            }
            else {
                $('.clsChkforstudent:checked').each(function () {
                    let studentId = $(this).attr('data-stuid')

                    let registerId = dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == sessionId).length > 0 ? dailyAttendance.studentDailyAttendanceModel.filter(x => x.StudentId == studentId && x.SessionId == sessionId)[0].RegisterId : 0;


                    $('#spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                    $('.spn_' + studentId + '_' + sessionId).html(appendStatus).addClass(appendDisplayClass);
                    $('#spnstatus_' + studentId).html(appendStatusOnStudent).addClass(appendDisplayClass);

                    let idx = dailyAttendance.AttendanceStudentModel.findIndex(x => x.StudentId == studentId && x.SessionId == sessionId);
                    if (idx > -1) {
                        dailyAttendance.AttendanceStudentModel[idx].ParameterId = selParamvalue;
                        dailyAttendance.AttendanceStudentModel[idx].Remark = remark;
                        dailyAttendance.AttendanceStudentModel[idx].SessionId = sessionId;
                    }
                    else {
                        dailyAttendance.AttendanceStudentModel.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                    }

                });
            }*/




            $('[data-toggle="tooltip"]').tooltip();

            $('.dt-checkboxesRoom').prop("checked", false).trigger("change");
            $('#selectAllRoomList').prop("checked", false).trigger("change");
        },
        dropdownMenuRoom = function (drp, isQuicklink) {
            let dataPresent = $(drp).attr('data-isPresent');
            let selParamvalue = $(drp).attr('data-other') == undefined ? dataPresent : $(drp).attr('data-other');
            let studentId = $(drp).attr('data-stuId');
            let sessionId = $(drp).attr('data-SessionId');
            let registerId = classAttendance.classAttendanceDetails.filter(x => x.StudentId == studentId && x.SessionId == sessionId).length > 0 ? classAttendance.classAttendanceDetails.filter(x => x.StudentId == studentId && x.SessionId == sessionId)[0].RegisterId : 0;
            let remark = undefined;
            if (isQuicklink) {
                $(drp).parent().prev().removeClass("d-none")
                $(drp).parent().prev().attr("data-isPresent", dataPresent)
                $(drp).parent().prev().attr("data-other", selParamvalue)
            }
            $('.spnRoom_' + studentId + '_' + sessionId).parent().parent().find(".clsRoomSingleOtherOption").removeClass('d-none');

            if (dataPresent == 0) {
                remark = $('#txtoptionalRemarkForRoom').val() == "" ? `${absent}` : $('#txtoptionalRemarkForRoom').val();
                $('.spnRoom_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>`)
                // $('.spnRoom_' + studentId + '_' + sessionId).parent().parent().find(".clsRoomSingleOtherOption").attr('data-ispresent', "0")[1];
            }
            else if (dataPresent == 1) {
                remark = $('#txtoptionalRemarkForRoom').val() == "" ? `${present}` : $('#txtoptionalRemarkForRoom').val();
                $('.spnRoom_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`)
                //$('.spnRoom_' + studentId + '_' + sessionId).parent().parent().find(".clsRoomSingleOtherOption").attr('data-ispresent', "1")[1];
            }
            else {
                let indexparamid = dailyAttendance.parameterList.findIndex(x => x.Value == selParamvalue)
                let paramValue = dailyAttendance.parameterList[indexparamid].Text;

                remark = $('#txtoptionalRemarkForRoom').val() == "" ? paramValue : $('#txtoptionalRemarkForRoom').val();
                if (selParamvalue == idPresent) {
                    remark = $('#txtoptionalRemarkForRoom').val() == "" ? `${present}` : $('#txtoptionalRemarkForRoom').val();
                    $('.spnRoom_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`)
                }
                else if (selParamvalue == idAbsent) {
                    remark = $('#txtoptionalRemarkForRoom').val() == "" ? `${absent}` : $('#txtoptionalRemarkForRoom').val();
                    $('.spnRoom_' + studentId + '_' + sessionId).html(`<span class="attendance-status text-status clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>`)
                }
                else {


                    $('.spnRoom_' + studentId + '_' + sessionId).html(`<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${selParamvalue}" data-toggle="tooltip" data-placement="left" title="${remark}">${paramValue.substring(0, 1)}</span></i>`).addClass('other-status')
                    $('.spnRoom_' + studentId + '_' + sessionId).parent().parent().find(".clsRoomSingleOtherOption").attr('data-ispresent', "-1")[1];
                }
            }

            classAttendance.classAttendanceHeader.filter(x => x.PeriodNo == sessionId).forEach((th) => {

                if (classAttendance.ClassAttendanceModuleStruct.filter(x => x.SessionId == th.PeriodNo).length == 0) {
                    classAttendance.classAttendanceDetails.filter((i, idx) => {

                        var Midx = classAttendance.classAttendanceDetails.findIndex(x => x.StudentId == i.StudentId);

                        return Midx == idx
                    }).map((x) => {


                        let parameterId = classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val()).length > 0 ? classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val())[0].ParameterId : idPresent;
                        let regId = classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val()).length > 0 ? classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val())[0].RegisterId : 0;
                        let remark = classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val()).length > 0 ? classAttendance.classAttendanceDetails.filter(ca => ca.StudentId == x.StudentId && ca.PeriodNo == th.PeriodNo && ca.SchoolGroupId == $("#CourseGroupId").val())[0].Remark : `${present}`;

                        classAttendance.ClassAttendanceModuleStruct.push({ StudentId: x.StudentId, SessionId: th.PeriodNo, RegisterId: regId, ParameterId: parameterId, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
                    })
                }


            })

            let index = classAttendance.ClassAttendanceModuleStruct.findIndex(x => x.StudentId == studentId && x.SessionId == sessionId);
            if (index > -1) {
                classAttendance.ClassAttendanceModuleStruct[index].ParameterId = selParamvalue;
                classAttendance.ClassAttendanceModuleStruct[index].Remark = remark;
                classAttendance.ClassAttendanceModuleStruct[index].SessionId = sessionId;
                classAttendance.ClassAttendanceModuleStruct[index].RegisterId = registerId;
            }
            else {
                classAttendance.ClassAttendanceModuleStruct.push({ StudentId: studentId, SessionId: sessionId, RegisterId: registerId, ParameterId: selParamvalue, Remark: remark, AcademicYearId: 0, AsOnDate: globalFunctions.toSystemReadableDate($('#txtAttDate').val()) });
            }
            $('[data-toggle="tooltip"]').tooltip();
        },
        generateParameterListForRoom = function (studentId, sessionId) {
            let trothercat = ``;
            dailyAttendance.parameterList.forEach((p) => {
                trothercat += `<a class="dropdown-item" data-other="${p.Value}" data-stuId="${studentId}" data-SessionId="${sessionId}" data-periodNo="" data-subject="" href="javascript:void(0)" onclick="classAttendance.dropdownMenuRoom(this)">${p.Text}</a>`
            });
            return trothercat;
        },
        generateParameterHeaderRoomList = function (periodNo) {
            let otherCategoryForAll = ``;
            dailyAttendance.parameterList.forEach((p) => {
                otherCategoryForAll += `<a class="dropdown-item" data-SessionId="${periodNo}" data-other="${p.Value}" href="javascript:void(0)" onclick="classAttendance.AppendStatusForSelectedRoom(this)" >${p.Text}</a>`
            })
            return otherCategoryForAll;
        },
        displayDailyAttendanceOnRoom = function (st) {

            let statusCss = "";
            let idxstatus = dailyAttendance.parameterList.findIndex(p => p.Value == st.DailyAttendanceParameterId);
            let statusdetails = dailyAttendance.parameterList[idxstatus];
            let remark = st.DailyAttendanceParameterRemark;
            if (idxstatus > -1) {

                statusDisplay = statusdetails.Text.toUpperCase() == "PRESENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`
                    : (statusdetails.Text.toUpperCase() == "ABSENT" ? `<span class="attendance-status text-status clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>` : `<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white clsdatastatus" data-status="${statusdetails.Value}" data-toggle="tooltip" data-placement="left" title="${remark}">${statusdetails.Text.substring(0, 1)}</span></i>`)

                statusCss = statusdetails.Text.toUpperCase() == "PRESENT" || statusdetails.Text.toUpperCase() == "ABSENT" ? "" : "other-status";
            }
            else {
                statusCss = "other-status"
                statusDisplay = `<i class="fas fa-circle fa-2x text-warning position-relative">
              <span class="font-small position-absolute text-bold text-white" data-status="" data-toggle="tooltip" data-placement="left" title="${remarkForUnmarkedAttendance}">U</span></i>`
            }
            return `<a href="javascript:void(0)" class="d-inline-block nav-link p-0 text-primary" data-toggle="dropdown" data-placement="left" aria-haspopup="true" aria-expanded="false">
                   <span class="${statusCss}">
                      ${statusDisplay}
                  </span>
                   </a>`;
        },
        generatePrvClassRoomStudentRows = function (st, response) {
            let classAttendanceListRows = ``;

            response.PreviousAttendanceHeader.forEach((td) => {

                classAttendanceListRows += `
                                         ${classAttendance.generatePrvRoomPeriodRows(st.StudentId, td.PeriodNo, td.IsActive, td.SchoolGroupId, response)}`

            })
            return classAttendanceListRows;
        },
        generatePrvRoomPeriodRows = function (studentId, periodNo, IsActive, SchoolGroupId, response) {
            let sessionRows = ``;
            let stu_idx = response.PreviousAttendanceDetails.findIndex(x => x.StudentId == studentId && x.SessionId == periodNo && x.SchoolGroupId == SchoolGroupId);


            let ParameterId;
            let remark = remarkForUnmarkedAttendance;
            if (stu_idx > -1) {
                ParameterId = response.PreviousAttendanceDetails[stu_idx].ParameterId;
                remark = response.PreviousAttendanceDetails[stu_idx].Remark;
            }

            //let cssIsActive = x.DisplayDate == globalFunctions.toSystemReadableDate($('#txtAttDate').val()) ? 'activeDay' : '';
            let dnone = IsActive == true ? '' : 'd-none';
            let idxstatus;
            let statusdetails;
            let statusDisplay;
            let statusCss = "other-status";
            let dateCss = IsActive == true ? "" : "bg-light";
            idxstatus = dailyAttendance.parameterList.findIndex(p => p.Value == ParameterId);
            statusdetails = dailyAttendance.parameterList[idxstatus];
            if (idxstatus > -1) {

                statusDisplay = statusdetails.Text.toUpperCase() == "PRESENT" ? `<span class="attendance-status text-status" data-status="${statusdetails.Value}"><i class="fas fa-2x fa-check-circle text-success"></i></span>`
                    : (statusdetails.Text.toUpperCase() == "ABSENT" ? `<span class="attendance-status text-status " data-status="${statusdetails.Value}"><i class="fas fa-2x fa-times-circle text-danger"></i></span>` : `<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white " data-status="${statusdetails.Value}">${statusdetails.Text.substring(0, 1)}</span></i>`)

                statusCss = statusdetails.Text.toUpperCase() == "PRESENT" || statusdetails.Text.toUpperCase() == "ABSENT" ? "" : "other-status";
            }
            else {
                statusDisplay = `<i class="fas fa-circle fa-2x text-warning position-relative"><span class="font-small position-absolute text-bold text-white ">U</span></i>`;
            }

            sessionRows += ` <td class="text-center">
                            <a href="javascript:void(0)" class="d-inline-block nav-link p-0 text-primary" data-toggle="popover" title="${remarkOnPopoverHeader}" data-content="${remark}">
                                <span class="${statusCss}">
                                    ${statusDisplay}
                                </span>
                            </a>
                           
                        </td>`


            return sessionRows;

        },
        generateRoomParameterShortCut = function (studentId, sessionId) {

            let paramSC = ``;
            AttendanceParameterShortCut.forEach((sc) => {
                paramSC += `<a class="dropdown-item ml-2 d-block" data-ispresent="-1" data-stuId="${studentId}"  data-other="${sc.Value}" data-SessionId="${sessionId}" href="javascript:void(0)" onclick="classAttendance.dropdownMenuRoom(this,true)">${sc.Text}</a>`;

            })



            return paramSC;

        },
        generateRoomParameterShortCutForAll = function () {

            let paramSCAll = ``;
            AttendanceParameterShortCut.forEach((sc) => {
                paramSCAll += ` <li data-toggle="tooltip" data-placement="left" title="${sc.Text}">
                                        <a  data-other="${sc.Value}" onclick="classAttendance.AppendStatusForSelectedRoom(this)" href="javascript:void(0)">
                                            <span class="font-weight-bolder text-white">${sc.Text.substring(0, 1)}</span>
                                        </a>
                                    </li>`;

            })
            return paramSCAll;
        }

    /*========= Class Attendance listing==========*/
    return {
        classAttendanceHeader: classAttendanceHeader,
        classAttendanceDetails: classAttendanceDetails,
        init: init,
        getCourseList: getCourseList,
        getClassAttendanceDetails: getClassAttendanceDetails,
        expanderContentHeader: expanderContentHeader,
        expanderContent: expanderContent,
        AppendStatusForSelected: AppendStatusForSelected,
        dropdownMenu: dropdownMenu,
        generateParameterList: generateParameterList,
        generateParameterHeaderList: generateParameterHeaderList,
        getClasAttendanceListing: getClasAttendanceListing,
        generateClassStudentRows: generateClassStudentRows,
        generatePeriodRows: generatePeriodRows,
        frmMyplannerCourseId: frmMyplannerCourseId,
        frmMyplannerasonDate: frmMyplannerasonDate,
        getClassListforMergeConfig: getClassListforMergeConfig,
        getClassRoomAttendanceListing: getClassRoomAttendanceListing,
        generateClassRoomStudentRows: generateClassRoomStudentRows,
        generateRoomPeriodRows: generateRoomPeriodRows,
        AppendStatusForSelectedRoom: AppendStatusForSelectedRoom,
        dropdownMenuRoom: dropdownMenuRoom,
        generateParameterListForRoom: generateParameterListForRoom,
        generateParameterHeaderRoomList: generateParameterHeaderRoomList,
        ClassAttendanceModuleStruct: ClassAttendanceModuleStruct,
        thisforSingleRoomAttendance: thisforSingleRoomAttendance,
        displayDailyAttendanceOnRoom: displayDailyAttendanceOnRoom,
        generatePrvClassRoomStudentRows: generatePrvClassRoomStudentRows,
        generatePrvRoomPeriodRows: generatePrvRoomPeriodRows,
        generateRoomParameterShortCut: generateRoomParameterShortCut,
        generateRoomParameterShortCutForAll: generateRoomParameterShortCutForAll

    }


}();



/*======================================== CLASSLIST ATTENDANCE =========================================*/
$(document).ready(function () {

    dailyAttendance.init();
    classAttendance.init();
    // quick action button

});

$(window).on("load", function () {

    dailyAttendance.onWindowLoad();
});
