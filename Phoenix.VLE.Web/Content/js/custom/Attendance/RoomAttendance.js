﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var studentRoomList = [];
var checkBoxid = undefined;
var DropDownEnum = {
    MarkasPresent: ["0", "MarkasPresent"],
    MarkasAbsent: ["1", "MarkasAbsent"]


};
var RoomAttendance = function () {
    var StudentAttendanceModel = [];
    var otherCategoriesList = [];
    var roomRemarksList = [];
    var OptionalHeader = [];
    var MenuStatusForStu = [];
    var init = function () {

        $.extend($.expr[":"], {
            "containsIN": function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });



        $('#attendancedate').datetimepicker({
            format: 'DD-MMM-YYYY',
            defaultDate: new Date(),
            maxDate: moment()
        });
        debugger
        var dt = new Date();
        //var cDate = ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear();
        var cDate = dt.getFullYear() + '-' + ("0" + (dt.getMonth() + 1)).slice(-2) + '-' + ("0" + dt.getDate()).slice(-2);

        // RoomAttendance.LoadStudentAttendanceDetails(cDate);




        $('#search').keyup(function () {
            $('.item').removeClass('d-none');
            var filter = $(this).val(); // get the value of the input, which we filter on
            $('#accStudentAttendance').find('.clsH4Name:not(:containsIN("' + filter + '"))').parent().parent().parent().parent().parent().addClass('d-none');
            //$('#products').find('.card .card-body:containsIN("' + filter + '")').addClass('highlighter');

        })

        //$('#btnRoomAttendance').on('click', function () {
        //    RoomAttendance.SaveRoomAttendance();



        //});


        $('#attendancedate').on('blur', function () {
            var date = $(this).val();
            var Coursegroupid = $("#CourseGroup").val();
           
            //var Courseid = $("#Course").val();
            RoomAttendance.LoadStudentAttendanceDetails(date, Coursegroupid)


        });


      

        $('#Course').on('change', function () {
            $("#CourseGroup").empty();

            //var grade = $("#RoomAttandanceGrade").val(); 
            try {
                var url = "/Attendance/Attendance/GetCourseGroupByCourse";

                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    data: { Course_id: $(this).val() },
                    async: false,
                    success: function (Sections) {
                        $("#CourseGroup").append('<option value="0">Select</option>');
                        $.each(Sections, function (i, Section) {
                            $("#CourseGroup").append('<option value="'
                                + Section.Value + '">'
                                + Section.Text + '</option>');
                        });

                        //  $('#SubjectGroup').val($("#SubjectGroup")[0].options[1].value).trigger('change');
                        $('#CourseGroup').selectpicker('refresh');
                        //$('#SubjectGroup').trigger("change");
                        //setDropDownValueAndTriggerChange('SubjectGroup');
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });
            } catch (e) {
                alert(e);
            }

        });

        $('#CourseGroup').on('change', function () {
            var date = $('#attendancedate').val();
            var Coursegroupid = $(this).val();
            var Courseid = $("#Course").val();
            RoomAttendance.LoadStudentAttendanceDetails(date, Coursegroupid)

        });
        RoomAttendance.LoadGrades();

    },
        lstofstudentIds = function addStudentIdTolist(chkStudentId) {
            if ($('#chk_' + chkStudentId).is(":checked")) {
                var index = studentRoomList.indexOf(chkStudentId);
                if (index > -1) {
                    studentRoomList[index] = chkStudentId
                }
                else {
                    studentRoomList.push(chkStudentId);
                }




            }
            else {
                var index = studentRoomList.indexOf(chkStudentId);
                if (index > -1) {
                    studentRoomList.splice(index, 1);
                }
            }

            if (studentRoomList.length > 1) {

                $('#divmultiselectmenu').show();

            }
            else {
                $('#divmultiselectmenu').hide();
            }




        },
        dropdownMenu = function (drp, IsMultiple, IsOptionalSubject = false) {
            var value = $(drp).attr('data-other');
            var idx = RoomAttendance.StudentAttendanceModel.lstOtherCategories.findIndex(x => x.ItemId == value);
            if (!IsMultiple) {


                var stuId = $(drp).attr('data-stuId');
                var Subject = $(drp).attr('data-subject');
                var PeriodN = $(drp).attr('data-periodNo');

                if (RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName.toUpperCase() == "PRESENT") {
                    $('#spn_' + stuId + '_' + PeriodN).html(`<h3 class="m-0 d-inline-block text-success clsdatastatus" data-status="${value}" data-toggle="tooltip" title="${RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName}" data-placement="left"><i class="fas fa-check"></i></h3>`)
                    var index = RoomAttendance.MenuStatusForStu.indexOf(stuId);
                    if (index > -1) {
                        RoomAttendance.MenuStatusForStu[idx] = parseInt(stuId)

                    }
                    else {
                        RoomAttendance.MenuStatusForStu.push(parseInt(stuId));
                    }

                }
                else if (RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName.toUpperCase() == "ABSENT") {
                    $('#spn_' + stuId + '_' + PeriodN).html(`<h3 class="m-0 d-inline-block text-danger clsdatastatus" data-status="${value}" data-toggle="tooltip" title="${RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName}" data-placement="left"><i class="fas fa-times"></i></h3>`)
                    var index = RoomAttendance.MenuStatusForStu.indexOf(stuId);
                    if (index > -1) {
                        RoomAttendance.MenuStatusForStu[idx] = parseInt(stuId)

                    }
                    else {
                        RoomAttendance.MenuStatusForStu.push(parseInt(stuId));
                    }
                }
                else {
                    $('#spn_' + stuId + '_' + PeriodN).html(`<span class="attendance-status text-status clsdatastatus" data-status="${value}" data-toggle="tooltip" data-placement="left" title="${RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName}">${RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName.substring(0, 1)}</span>`)
                    var index = RoomAttendance.MenuStatusForStu.indexOf(stuId);
                    if (index > -1) {
                        RoomAttendance.MenuStatusForStu[idx] = parseInt(stuId)

                    }
                    else {
                        RoomAttendance.MenuStatusForStu.push(parseInt(stuId));
                    }
                }

                $('#chk_' + stuId).prop('checked', false);



            }
            else {

                var subjectList = [];
                if (checkBoxid != undefined) {
                    subjectList = RoomAttendance.StudentAttendanceModel.objTimeTable.filter(x => x.IsActive == true && x.PeriodNo == checkBoxid);
                }
                else {


                    subjectList = RoomAttendance.StudentAttendanceModel.objTimeTable.filter(x => x.IsActive == true);
                }
                //var subjectOptional = RoomAttendance.StudentAttendanceModel.objTimeTable.filter(x => x.IsActive == true);

                if (subjectList.length > 0) {
                    var ddlstudents = [];

                    $.each($('.clsStuCheckBox:checked'), function () {
                        var chkid = $(this).attr("id");
                        if ($('#' + chkid).is(":checked")) {

                            var studentId = parseInt($('#' + chkid).attr('data-stuId'));
                            var index = ddlstudents.indexOf(studentId);
                            if (index > -1) {
                                ddlstudents[index] = studentId;
                            }
                            else {
                                ddlstudents.push(studentId)
                            }

                        }

                    });
                    if (RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName.toUpperCase() == "PRESENT") {
                        for (var i = 0; i < ddlstudents.length; i++) {

                            for (var j = 0; j < subjectList.length; j++) {

                                $('#spn_' + ddlstudents[i] + '_' + subjectList[j].PeriodNo).html(`<h3 class="m-0 d-inline-block text-success clsdatastatus" data-status="${value}" data-toggle="tooltip" title="${RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName}" data-placement="left"><i class="fas fa-check"></i></h3>`)

                            }

                        }

                    }
                    else if (RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName.toUpperCase() == "ABSENT") {

                        for (var i = 0; i < ddlstudents.length; i++) {

                            for (var j = 0; j < subjectList.length; j++) {

                                $('#spn_' + ddlstudents[i] + '_' + subjectList[j].PeriodNo).html(`<h3 class="m-0 d-inline-block text-danger clsdatastatus" data-status="${value}" data-toggle="tooltip" title="${RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName}" data-placement="left"><i class="fas fa-times"></i></h3>`)


                            }

                        }
                    }
                    else {
                        for (var i = 0; i < ddlstudents.length; i++) {

                            for (var j = 0; j < subjectList.length; j++) {

                                $('#spn_' + ddlstudents[i] + '_' + subjectList[j].PeriodNo).html(`<span class="attendance-status text-status clsdatastatus" data-status="${value}" data-toggle="tooltip" data-placement="left" title="${RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName}">${RoomAttendance.StudentAttendanceModel.lstOtherCategories[idx].ItemName.substring(0, 1)}</span>`)



                            }

                        }
                    }

                }

                $('.clsStuCheckBox').prop('checked', false);
                if (checkBoxid != undefined) {
                    $('#chk_' + checkBoxid).prop('checked', false);
                    var checkboxlst = RoomAttendance.StudentAttendanceModel.objTimeTable.filter(x => x.IsActive == true && x.PeriodNo != checkBoxid);
                    for (var i = 0; i < checkboxlst.length; i++) {

                        $('#chk_' + checkboxlst[i].PeriodNo).removeAttr('disabled');
                    }

                    checkBoxid = undefined;
                }
                if ($('#chkallSelection').is(":checked")) {
                    $('#chkallSelection').prop('checked', false);
                }

            }
            $('[data-toggle="tooltip"]').tooltip();
        },
        SaveRoomAttendance = function () {
            debugger
            var totalstudentlist = [];


            if (RoomAttendance.MenuStatusForStu.length > 0) {

                totalstudentlist = $.extend(studentRoomList, RoomAttendance.MenuStatusForStu);  //$.merge(studentRoomList, MenuStatusForStu)
            }
            else {
                totalstudentlist = studentRoomList;
            }

            var objlstofStudentAttendanceDetails = [];
            var objListOfAttendanceLog = [];
            var subjectList = RoomAttendance.StudentAttendanceModel.objTimeTable.filter(x => x.IsActive == true);


            for (var s = 0; s < subjectList.length; s++) {
                var attenddancelogdetails = RoomAttendance.roomRemarksList.length > 0 ? RoomAttendance.roomRemarksList.filter(e => e.PeriodNo == subjectList[s].PeriodNo) : null;

                var objlog = {

                    logId: attenddancelogdetails != null && attenddancelogdetails.length > 0 ? attenddancelogdetails[0].logId : 0,
                    RoomDate: $('#attendancedate').val(),
                    PeriodNo: subjectList[s].PeriodNo,
                    SGR_ID: subjectList[s].SGR_ID,
                }
                objListOfAttendanceLog.push(objlog);
            }


            for (var i = 0; i < totalstudentlist.length; i++) {


                for (var j = 0; j < subjectList.length; j++) {
                    var stuDetaillogid = RoomAttendance.roomRemarksList.length > 0 ? RoomAttendance.roomRemarksList.filter(e => e.StudentId == studentRoomList[i] && e.PeriodNo == subjectList[j].PeriodNo) : null;
                    var status = $('#spn_' + totalstudentlist[i] + '_' + subjectList[j].PeriodNo).children('.clsdatastatus').attr('data-status');
                    if (status != undefined) {


                        var objDetails =
                        {
                            logId: stuDetaillogid != null && stuDetaillogid.length > 0 ? stuDetaillogid[0].logId : 0,
                            DetailId: stuDetaillogid != null && stuDetaillogid.length > 0 ? stuDetaillogid[0].DetailId : 0,
                            RoomDate: $('#attendancedate').val(),
                            StudentId: studentRoomList[i],
                            SGR_ID: subjectList[j].SGR_ID,
                            Subject: subjectList[j].Subject,
                            PeriodNo: subjectList[j].PeriodNo,
                            IsOptional: subjectList[j].IsOptional,
                            Status: status

                        }


                        objlstofStudentAttendanceDetails.push(objDetails);
                    }

                }





            }
            //optional headder details
            //for (var s = 0; s < optionalsubject.length; s++) {
            //    var attenddancelogdetails = RoomAttendance.roomRemarksList.length > 0 ? RoomAttendance.roomRemarksList.filter(e => e.PeriodNo == optionalsubject[s].PeriodNo) : null;
            //    var objlog = {

            //        logId: attenddancelogdetails != null && attenddancelogdetails.length > 0 ? attenddancelogdetails[0].logId : 0,
            //        RoomDate: $('#attendancedate').val(),
            //        PeriodNo: optionalsubject[s].PeriodNo,
            //        SGR_ID: optionalsubject[s].SGR_ID,
            //    }
            //    objListOfAttendanceLog.push(objlog);
            //}
            //for (var i = 0; i < studentRoomList.length; i++) {

            //    for (var j = 0; j < optionalsubject.length; j++) {

            //        var stuDetaillogid = RoomAttendance.roomRemarksList.length > 0 ? RoomAttendance.roomRemarksList.filter(e => e.StudentId == studentRoomList[i] && e.PeriodNo == optionalsubject[j].PeriodNo) : null;
            //        var status = $('#spn_' + studentRoomList[i] + '_' + optionalsubject[j].OptionalHeader.replace(/ /g, "_")).children('.clsdatastatus').attr('data-status');
            //        if (status != undefined) {


            //            var objDetails =
            //            {
            //                logId: stuDetaillogid != null && stuDetaillogid.length > 0 ? stuDetaillogid[0].logId : 0,
            //                DetailId: stuDetaillogid != null && stuDetaillogid.length > 0 ? stuDetaillogid[0].DetailId : 0,
            //                RoomDate: $('#attendancedate').val(),
            //                StudentId: studentRoomList[i],
            //                SGR_ID: optionalsubject[j].SGR_ID,
            //                Subject: optionalsubject[j].Subject,
            //                PeriodNo: optionalsubject[j].PeriodNo,
            //                IsOptional: optionalsubject[j].IsOptional,
            //                Status: status

            //            }


            //            objlstofStudentAttendanceDetails.push(objDetails);
            //        }

            //    }

            //}


            if (objlstofStudentAttendanceDetails.length > 0) {

                $.post('/Attendance/Attendance/SaveRoomAttendance', { objlstofStudentAttendanceDetails: objlstofStudentAttendanceDetails, objListOfAttendanceLog: objListOfAttendanceLog }, function (response) {

                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    studentRoomList.length = 0;
                });
            }


            studentRoomList.length = 0;
            RoomAttendance.MenuStatusForStu.length = 0;
            if ($("#chkallSelection").is(":checked")) {
                $("#chkallSelection").trigger("change");

            }

        },
        LoadStudentAttendanceDetails = function (entryDate, Coursegroupid=0) {
            debugger
            $.post('/Attendance/Attendance/RoomAttendanceDetails', { entryDate: entryDate, Coursegroupid: Coursegroupid}, function (response) {

                $('#divstudentAttendancedetails').html(response);

                if ($(window).width() > 768) {
                    var fixmeTop = $('.sticky-group').offset().top;
                    $(window).scroll(function () {
                        var contentWidth = $('.collapse .bg-white').outerWidth();
                        var currentScroll = $(window).scrollTop();

                        if (currentScroll >= fixmeTop) {
                            $('.sticky-group').css({
                                position: 'fixed',
                                top: '70px',
                                width: contentWidth,
                                'z-index': 1
                                //left: '0'
                            });
                            $('.attendanceHeader').removeClass('w-100');
                        } else {
                            $('.sticky-group').css({
                                position: 'static'
                            });
                            $('.attendanceHeader').addClass('w-100');
                        }
                    });
                }
                if ($(window).width() <= 414) {
                    $(".collapse.show").removeClass("show");
                    $(".collapse").removeClass("h-100");
                }
            });

        },
        SelectAll = function (chk) {

            var status = $(".selectAllStudents:checked").length;
            if (status === 1) {
                $(chk).parent().next().show();
                $('.clsStuCheckBox').prop('checked', true);

                for (var i = 0; i < RoomAttendance.StudentAttendanceModel.objRoomAttendance.length; i++) {
                    studentRoomList.push(RoomAttendance.StudentAttendanceModel.objRoomAttendance[i].Student_ID);
                }

            } else {
                $(chk).parent().next().hide();
                $('.clsStuCheckBox').prop('checked', false);
                // $('.clsspnResult').html(`<i class="far fa-plus-square"></i>`)
                studentRoomList.length = 0;
            }


        },
        LoadGrades = function () {

            $("#Grades").empty();
            try {
                var url = "/Attendance/Attendance/LoadGrade";

                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    // data: { grd_id: grade, sbg_id: $(this).val() },
                    async: false,
                    success: function (Sections) {
                        $("#Grades").append('<option value="0">Select</option>');
                        $.each(Sections, function (i, Section) {
                            $("#Grades").append('<option value="'
                                + Section.GRD_ID + '">'
                                + Section.GRM_DISPLAY + '</option>');
                        });
                        // $('#Grades').selectpicker('refresh');
                        // $('#Grades').val($("#Grades")[0].options[1].value).trigger('change');
                        $('#Grades').selectpicker('refresh');

                        //setDropDownValueAndTriggerChange('Grades');
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });
            } catch (e) {
                alert(e);
            }
        },

        RAForCheckActive = function (chkActivePeriodNo) {

            if ($('#chk_' + chkActivePeriodNo).is(":checked")) {

                checkBoxid = chkActivePeriodNo;

                var checkboxlst = RoomAttendance.StudentAttendanceModel.objTimeTable.filter(x => x.IsActive == true && x.PeriodNo != chkActivePeriodNo);
                for (var i = 0; i < checkboxlst.length; i++) {

                    $('#chk_' + checkboxlst[i].PeriodNo).attr('disabled', true);
                }

            }
            else {
                checkBoxid = undefined;

            }

        }





    return {
        init: init,
        lstofstudentIds: lstofstudentIds,
        StudentAttendanceModel: StudentAttendanceModel,
        dropdownMenu: dropdownMenu,
        otherCategoriesList: otherCategoriesList,
        SaveRoomAttendance: SaveRoomAttendance,
        roomRemarksList: roomRemarksList,
        LoadStudentAttendanceDetails: LoadStudentAttendanceDetails,
        SelectAll: SelectAll,
        OptionalHeader: OptionalHeader,
        MenuStatusForStu: MenuStatusForStu,
        LoadGrades: LoadGrades,
        RAForCheckActive: RAForCheckActive
    };



}();
$(document).ready(function () {

    RoomAttendance.init();



});
//$(document).on('change','#RoomAttandanceGrade', function () {
//    alert(1)
//    $("#Subject").empty();

//    var grade = $(this).val();
//    try {
//        var url = "/Attendance/Attendance/GetSubjectsByGrade";

//        $.ajax({
//            type: 'GET',
//            dataType: "json",
//            url: url,
//            contentType: 'application/json;charset=utf-8',
//            data: { acd_id: $("#AcademicYearDDL").val(), grd_id: grade },
//            async: false,
//            success: function (Sections) {
//                $("#Subject").append('<option value="0">Select</option>');

//                $.each(Sections, function (i, Section) {
//                    $("#Subject").append('<option value="'
//                        + Section.Value + '">'
//                        + Section.Text + '</option>');
//                });

//                // $('#Subject').val($("#Subject")[0].options[1].value).trigger('change');
//                $('#Subject').selectpicker('refresh');
//                //setDropDownValueAndTriggerChange('Grades');
//            },
//            error: function (ex) {
//                alert('Failed.' + ex);
//            }
//        });
//    } catch (e) {
//        alert(e);
//    }

//});