﻿var attendanceType = undefined;
$(document).ready(function () {
    var hdnEntryDate = new Date($('#hdnEntryDate').val());
    var entryDate = new Date();
    entryDate = (hdnEntryDate === "" || hdnEntryDate === undefined) ? entryDate : hdnEntryDate;
    // $('#btn_Save').hide();
    $('[data-toggle="tooltip"]').tooltip();
    $('.date-picker').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: new Date(),
        maxDate: moment()
    });
    $('#txtEntryDate').datetimepicker({
        format: 'DD-MMM-YYYY',
        defaultDate: new Date(),
        maxDate: moment()
    })
    var dt = new Date();
    //var cDate = ("0" + dt.getDate()).slice(-2) + '-' + ("0" + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
    //$('.date-picker').val(cDate);
    //var cDate = ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear();
    //$('.date-picker').val(Date());
    setDropDownValueAndTriggerChange('Grades');

    var _ttm_id = $('#hf_ttm_id').val();
    var _entrydate = $('#hf_entry_date').val();
    var _grade = $('#hf_grade').val();
    var _section = $('#hf_section').val();
    var _isFromTT = $('#hf_isFromTT').val();
    if (_isFromTT === '1') {
        $('#dv_entry_date').hide();
        FetchAttendance(_ttm_id, _entrydate, _grade, _section, $('#AttendanceSessionType').val());
    }

    $('#AttendanceSessionType').on('change', function () {
        var atttype = $(this).val();
        $('#hdnAttendanceType').val(atttype);
        var _ttm_id = $('#hf_ttm_id').val();
        var _entrydate = $('#hf_entry_date').val();
        var _grade = $('#hf_grade').val();
        var _section = $('#hf_section').val();
        FetchAttendance(_ttm_id, _entrydate, _grade, _section, atttype);



    });
    $('#zoomBtn').click(function () {
        $('.zoom-btn-sm').toggleClass('scale-out');
        if (!$('.zoom-card').hasClass('scale-out')) {
            $('.zoom-card').toggleClass('scale-out');
        }
    });

    $('.zoom-btn-sm').click(function () {
        var btn = $(this);
        var card = $('.zoom-card');

        if ($('.zoom-card').hasClass('scale-out')) {
            $('.zoom-card').toggleClass('scale-out');
        }
        if (btn.hasClass('zoom-btn-person')) {
            card.css('background-color', '#d32f2f');
        } else if (btn.hasClass('zoom-btn-doc')) {
            card.css('background-color', '#fbc02d');
        } else if (btn.hasClass('zoom-btn-tangram')) {
            card.css('background-color', '#388e3c');
        } else if (btn.hasClass('zoom-btn-report')) {
            card.css('background-color', '#1976d2');
        } else {
            card.css('background-color', '#7b1fa2');
        }
    });

    $(".inp").on('blur', function () {
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        var selecteddate = $(this).val();
        var dt = new Date();
        var monthN = dt.getMonth();
        var cDate = ("0" + dt.getDate()).slice(-2) + '-' + (monthNames[monthN]) + '-' + dt.getFullYear();
        // var cDate = ("0" + dt.getDate()).slice(-2) + '-' + ("0" + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();


        //$('#hf_ttm_id').val();
        $('#hf_entry_date').val($('#txtEntryDate').val());
        var onlygrade = "";
        if ($("#Grades").val() != undefined) {
            onlygrade = $("#Grades option:selected").val().split("|");
            $('#hf_grade').val(onlygrade[0]);
        }

        $('#hf_section').val($('#Section').val());

        //using those hidden fields
        var _ttm_id = $('#hf_ttm_id').val();
        var _entrydate = $(this).val();
        var _grade = $('#hf_grade').val();
        var _section = $('#hf_section').val();
        //debugger;
        //if ($('#AttendanceSessionType').val() != null || $('#AttendanceSessionType').val() != "") {
        //    FetchAttendance(_ttm_id, _entrydate, _grade, _section, $('#AttendanceSessionType').val());
        //}
        //else {
            GetAttendanceType(_ttm_id, _entrydate, _grade, _section)
        //}
      
        //
        if (new Date(selecteddate) <= new Date(cDate)) {
            $('#btn_Save').removeClass('d-none');

        }
        else {
            $('#btn_Save').addClass('d-none');
        }
    })
});

function selecctchange(txt) {
    //debugger;
    //$('#hf_ttm_id').val(0);

    $('#hf_entry_date').val($('#txtEntryDate').val());
    var onlygrade = "";
    if ($("#Grades").val() != undefined) {
        onlygrade = $("#Grades option:selected").val().split("|");
        $('#hf_grade').val(onlygrade[0]);
    }
    $('#hf_section').val($('#Section').val());

    //using those hidden fields
    var _ttm_id = $('#hf_ttm_id').val();
    var _entrydate = $(txt).val();
    var _grade = $('#hf_grade').val();
    var _section = $('#hf_section').val();
    FetchAttendance(_ttm_id, _entrydate, _grade, _section);

}
$("#Section").change(function () {
    //Assigning the value to the page hidden fields
    //$('#hf_ttm_id').val(0);
    //debugger;
    var _isFromTT = $('#hf_isFromTT').val();
    if (_isFromTT === '0') {
        $('#hf_entry_date').val($('#txtEntryDate').val());
    }
    
    var onlygrade = "";
    if ($("#Grades").val() != undefined) {
        onlygrade = $("#Grades option:selected").val().split("|");
        $('#hf_grade').val(onlygrade[0]);
    }
    $('#hf_section').val($('#Section').val());

    //using those hidden fields
    var _ttm_id = $('#hf_ttm_id').val();
    var _entrydate = $('#hf_entry_date').val();
    var _grade = $('#hf_grade').val();
    var _section = $('#hf_section').val();
    //For attendance type


    if ($(this).val() != "") {
        $('#divbtnsave').removeClass('d-none');
        GetAttendanceType(_ttm_id, _entrydate, _grade, _section);
        // FetchAttendance(_ttm_id, _entrydate, _grade, _section);
    }
    else {
        $('#divbtnsave').addClass('d-none');
        $('#FetchAttendance_B_tbody').html("");
        $('#FetchAttendance_B_tbody').html(`<tr><td></td><td></td><td></td><td class="sub-heading">No Data</td><td></td><td></td><td></td></tr>`);

    }

})



function FetchAttendance(_ttm_id, _entrydate, _grade, _section, attendanceType) {
    //debugger;
    //FOR LOADING THE Compare Leave Table
    // alert(attendanceType);
    $.ajax({
        type: "GET",
        url: "/Attendance/Attendance/GetStudentList",
        //data: '{ ttm_id: "' + _ttm_id + '", entrydate : "' + _entrydate + '", grade :"' + _grade + '", section :"' + _section + '"}',
        data: { tt_id: _ttm_id, entrydate: _entrydate, grade: _grade, section: _section, attendanceType: attendanceType == null || attendanceType == undefined || attendanceType == "" ? "Session1" : attendanceType },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            $('#hf_Ajax_response').val(encodeURIComponent(JSON.stringify(results)));
            onAjaxSuccess(results);
            GetAttendanceFreeze(_grade, _entrydate)
        },
        error: function (error) {
            //   globalFunctions.onFailure();
        }
    });
    return true;
}
function StatusChange(status) {

    if ($('#selectall').is(":checked")) {

    }
    else if ($('input[name="image[]"]:checked').length == 0) {
        globalFunctions.showMessage("warning", "Please select students to add");
        return false;
    }

    var data = JSON.parse(decodeURIComponent($('#hf_Ajax_response').val()));
    var ids = "";
    $(".image-checkbox-checked").each(function () {
        ids += $(this).find('input[type="hidden"]').eq(0).val() + ',';
    });
    var arr_id = ids.split(",");
    if (arr_id.length > 0) {
        for (var i = 0; i < data.length; i++) {
            for (var j = 0; j < arr_id.length; j++) {
                if (data[i].STU_ID === arr_id[j] && data[i].AllowEdit === "1") {
                    data[i].Status = status;
                    if (status === "P") {
                        data[i].APD_ID = '0';
                        data[i].STATUS_DESCR = "Present - Not Saved";
                    }
                    if (status === "A") {
                        data[i].APD_ID = '-1';
                        data[i].STATUS_DESCR = "Absent - Not Saved";
                    }
                    if (status === "O") {
                        if ($('#AttendanceType').val() == "") {
                            globalFunctions.showWarningMessage("Please select other options to add");
                            return false;
                        }
                        data[i].APD_ID = $('#AttendanceType').val();
                        data[i].Status = $("#AttendanceType option:selected").text().substring(0, 1);
                        data[i].STATUS_DESCR = $("#AttendanceType option:selected").text();
                        data[i].Remarks = $("#txt_remarks").val();
                    }
                }
            }
        }
        $('#hf_Ajax_response').val(encodeURIComponent(JSON.stringify(data)));
        onAjaxSuccess(data, "Edit");
        $("#txt_remarks").val('');  //reset textbox
        $('#AttendanceType').prop('selectedIndex', 0); //reset dropdown
        $('#AttendanceType').selectpicker('refresh');
        $('.contactDetails').modal('hide'); //close the modal 
    }

}
$(document).on('click', '#btn_Save', function () {

    SaveAttendance();
});

function SaveAttendance() {
    var stu_id = "";
    var status = "";
    var remarks = "";
    var data_xml = "<DATA>";
    $('.tr_data').each(function () {
        stu_id = $(this).find(".hf_STU_ID").val();
        sectionId = $(this).find(".hf_STU_ID").attr("data-sectionId");
        ALG_ID = $(this).find(".hf_STU_ID").attr("data-algid");
        status = $(this).find(".hf_current_status").val();
        remarks = $(this).find(".hf_remarks").val();
        data_xml += "<STUDENT>";
        data_xml += "<STU_ID>" + stu_id + "</STU_ID>";
        data_xml += "<APD_ID>" + status + "</APD_ID>";
        data_xml += "<REMARKS>" + remarks + "</REMARKS>";
        data_xml += "<SCT_ID>" + sectionId + "</SCT_ID>";
        data_xml += "<ALG_ID>" + ALG_ID + "</ALG_ID>";
        data_xml += "</STUDENT>";
    });
    data_xml += "</DATA>";
    $("#hf_Student_XML").val(data_xml);
    //alert(data_xml);
    //OTHER VALIDATIONS WILL COME HERE
};

onAttendanceSaveSuccess = function (data) {
    globalFunctions.showMessage(data.NotificationType, data.Message);

    if (data.Success) {
        var _ttm_id = $('#hf_ttm_id').val();
        var _entrydate = $('#hf_entry_date').val();
        var _grade = $('#hf_grade').val();
        var _section = $('#hf_section').val();
        FetchAttendance(_ttm_id, _entrydate, _grade, _section, $('#AttendanceSessionType').val());
    }
}

//save success for the attendance
function onAjaxSuccess(results, loadtype = 'Load') {
    //debugger;
    if (results.length > 0) {


        $('#FetchAttendance_H_tr').html("");
        var items = results;// JSON.parse(results);
        var lookup = {};
        var result = [];

        for (var item, i = 0; item = items[i++];) {
            var dates = item.TDATE;

            if (!(dates in lookup)) {
                lookup[dates] = 1;
                result.push(dates);
            }
        }
        var tr;
        var headerlayout = "";
        for (var i = 0; i < result.length; i++) {
            headerlayout += "<th class='text-center no-sort font-weight-light'>";
            headerlayout += result[i];
            headerlayout += "</th>";
            ////  tr = $('<th/>');
            //  //tr.append(result[i]);
        }
        $('#FetchAttendance_H_tr').append(headerlayout);
        $('#FetchAttendance_H_tr').prepend("<th class='font-weight-light'>Transport Status</th>");
        $('#FetchAttendance_H_tr').prepend("<th class='font-weight-light'>Name</th>");
        $('#FetchAttendance_H_tr').prepend("<th class='text-center no-sort'><div class='custom-control custom-checkbox ml-2'><input value='SelectAll' class='custom-control-input' id='selectall' name='SelectAll'  type='checkbox'><input name='SelectAll' type='hidden' value='false'><label class='custom-control-label sms-checkbox-border' for='selectall'></label></div></th>");

        ////FOR THE BODY

        var lookupstudent = {};
        var resultemp = [];
        for (var item, i = 0; item = items[i++];) {
            var student = item.Student_Name;

            if (!(student in lookupstudent)) {
                lookupstudent[student] = 1;
                resultemp.push(student);
            }
        }
        //ASssigning the alg_id to hidden field
        //debugger;
        var alg_id = items.filter(function (element) { return element.AllowEdit === "1"; });
        if (alg_id.length > 0) {
            if (alg_id[0].ALG_ID != undefined) {

                $("#hf_ALG_ID").val(alg_id[0].ALG_ID);
            }
            else {
                $("#hf_ALG_ID").val(0);
            }
        }


        var STREAM_ID = items[0].STREAM_ID;
        $('#hf_SHF_ID').val(STREAM_ID);


        var SHIFT_ID = items[0].SHIFT_ID;
        $('#hf_STM_ID').val(SHIFT_ID);
        var onlygrade = "";
        if ($("#Grades").val() != undefined) {
            onlygrade = $("#Grades option:selected").val().split("|");
            $('#hf_grade').val(onlygrade[0]);
        }
        //$('#hf_grade').val(onlygrade[0]);
        var gradeId = onlygrade = "" ? 0 : onlygrade[0];//$("#Grades option:selected").text();
        $('#hf_GRD_ID').val(gradeId)
        var layout = "";
        for (var j = 0; j < resultemp.length; j++) {   //loop for tr

            layout += "<tr class='tr_data' role='row'>";

            var data_filter = items.filter(function (element) { return element.Student_Name === resultemp[j]; });

            layout += "<td class='p-0'><div class='user-action nopad'><label class='image-checkbox'><img class='stud-grid-profile' src='" + data_filter[0].Student_Image_url + "' /> ";

            layout += "<input type='checkbox' name='image[]' value='' /><i class='fa fa-check hidden'></i><input class='hf_STU_ID' data-sectionId=" + data_filter[0].SectionId + " data-algid=" + data_filter[0].ALG_ID+" type='hidden' value='" + data_filter[0].STU_ID + "'/></label></div></td>";

            layout += "<td class='sorting_1 text-bold'><a target='_blank' href='/Attendance/Attendance/StudentHistory/" + data_filter[0].STU_ID + "'>" + data_filter[0].Student_Name + "</a></td>";

            //TRANSPORT STATUS START HERE 
            var transport_class = '';
            if (data_filter[0].transport_code === 'IN') {
                transport_class = 'success';
            }
            else if (data_filter[0].transport_code === 'OUT') {
                transport_class = 'default';
            }
            else if (data_filter[0].transport_code === 'NO SCAN') {
                transport_class = 'primary';
            }
            else if (data_filter[0].transport_code === 'OT') {
                transport_class = 'warning';
            }
            else if (data_filter[0].transport_code === 'LATE') {
                transport_class = 'primary';
            }
            var styleString = transport_class == 'primary' ? 'style="background-color: #a2a2a2!important;"' : ''
            layout += "<td><span class='badge badge-" + transport_class + " p-2 shadow-none' " + styleString + "  data-toggle='tooltip' data-placement='left' title='" + data_filter[0].transport_descr + "'>" + data_filter[0].transport_code + "</span>";
            //layout += "<span class='arrow-right text-" + transport_class + "' style='color: #a2a2a2!important;'></span></td>";
            layout += "</td>";
            for (var i = 0; i < data_filter.length; i++) {    //loop for td
                if (i == 0 && alg_id.length > 0) {
                    layout += "<td class='text-center current-entry'><input class='hf_current_status' type='hidden' value='" + data_filter[0].APD_ID + "'/>";
                    layout += "<input class='hf_remarks' type='hidden' value='" + data_filter[0].Remarks + "'/>";
                }
                else {
                    layout += "<td class='text-center'>";
                }
                if ((loadtype == "Edit" || data_filter[i].ALG_ID == 0) && data_filter[i].AllowEdit == 1) {   // For showing gray icons 
                    //if (data_filter[i].Status == "P") //Present
                    //{
                    //    layout += "<span class='attendance-status text-white bg-grey'  data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "'>P</span>";
                    //}
                    //else if (data_filter[i].Status == "A") // Absent
                    //{
                    //    layout += "<span class='attendance-status text-white bg-grey' data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "'>A</span>";
                    //}
                    if (data_filter[i].Status == "P") //Present
                    {
                        layout += "<i class='fa fa-2x fa-check fa-color-grey'  data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "'></i>";
                    }
                    else if (data_filter[i].Status == "A") // Absent
                    {
                        layout += "<i class='fa fa-2x fa-times fa-color-grey' data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "'></i>";
                    }
                    else if (data_filter[i].Status == "U" && data_filter[i].APD_ID == 0) // Unmarked
                    {
                        layout += "<i class='fa fa-2x fa-minus fa-color-grey' data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "'></i>";
                    }
                    else //No entry
                    {
                        layout += "<span class='attendance-status text-status-gray' data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "(" + data_filter[i].Remarks + ")'>" + data_filter[i].Status + "</span>";
                    }
                }
                else {
                    //if (data_filter[i].Status == "P") //Present
                    //{
                    //    layout += "<span class='attendance-status text-white bg-success' data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "'>P</span>";
                    //}
                    //else if (data_filter[i].Status == "A") // Absent
                    //{
                    //    layout += "<span class='attendance-status text-white bg-danger' data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "'>A</span>";
                    //}
                    if (data_filter[i].Status == "P") //Present
                    {
                        layout += "<i class='fa fa-2x fa-check fa-color-success' data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "'></i>";
                    }
                    else if (data_filter[i].Status == "A") // Absent
                    {
                        layout += "<i class='fa fa-2x fa-times fa-color-danger' data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "'></i>";
                    }
                    else if (data_filter[i].Status == "U" && data_filter[i].APD_ID == 0) // Unmarked
                    {
                        layout += "<i class='fa fa-2x fa-minus text-warning' data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "'></i>";
                    }
                    else //No entry
                    {
                        layout += "<span class='attendance-status text-status' data-toggle='tooltip' data-placement='left' title='" + data_filter[i].STATUS_DESCR + "(" + data_filter[i].Remarks + ")'>" + data_filter[i].Status + "</span>";
                    }
                }

                layout += "</td>";
            }
            layout += "</tr>";

        }
        $('#FetchAttendance_B_tbody').html(layout);
        $('#btn_Save').removeClass('d-none');
        $('[data-toggle="tooltip"]').tooltip();
        $("#selectall").click(function () {
            chkAll();
        });


        // image gallery
        // init the state from the input
        $(".image-checkbox").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-checked');

            }
            else {
                $(this).removeClass('image-checkbox-checked');

            }
        });

        function chkAll() {
            $(".image-checkbox").each(function () {
                if ($('#selectall').is(':checked')) {

                    $(this).addClass('image-checkbox-checked');
                    $('input[name="image[]"]').prop('checked', true);
                }
                else {
                    $(this).removeClass('image-checkbox-checked');
                    $('input[name="image[]"]').prop('checked', false);
                }
            });
        }


        // sync the state to the input
        $(".image-checkbox").on("click", function (e) {
            //debugger;
            $(this).toggleClass('image-checkbox-checked');
            var $checkbox = $(this).find('input[type="checkbox"]');
            $checkbox.prop("checked", !$checkbox.prop("checked"))
            if (!$checkbox.is(":checked")) {
                if ($('#selectall').is(":checked")) {
                    $('#selectall').prop('checked', false);
                }
            }

            e.preventDefault();
        });
     

        //if (!$.fn.dataTable.isDataTable('#example')) {
        //    $('#example').DataTable({
        //        "paging": false,
        //        "searching":false,
        //        //"ordering": false,
        //        "columnDefs": [
        //            {
        //                "width": "8%",
        //                "targets": 0,
        //                "orderable": false
        //            },
        //            {
        //                "width": "15%",
        //                "targets": 1,
        //                "orderable": true
        //            },
        //            {
        //                "targets": "no-sort",
        //                "orderable": false
        //            }
        //        ]
        //    });
        //}

    }
    else {
        globalFunctions.showMessage("warning", "No Data");
    }
}


//Extending the :contains selector property to be case in-sensitive for the card search option. 
$.extend($.expr[":"], {
    "containsIN": function (elem, i, match, array) {
        return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
});
//Search option for students cards
$('#search').keyup(function () {
    $('.tr_data').removeClass('d-none');
    var filter = $(this).val(); // get the value of the input, which we filter on
    $('#example').find('.tr_data .sorting_1 :not(:containsIN("' + filter + '"))').parent().parent().addClass('d-none');
    //$('#products').find('.card .card-body:containsIN("' + filter + '")').addClass('highlighter');

})

$(document).on('change', '#Grades', function () {
    smsCommon.bindSingleDropDownByParameter('#Section', '/Attendance/Attendance/GetSectionByGradeId', { gradeId: $(this).val() }, '', '');
    setDropDownValueAndTriggerChange('Section');

});

function GetAttendanceType(_ttm_id, _entrydate, _grade, _section) {
    //debugger;
    $.get('/Attendance/Attendance/GetAttendanceType', { grade: _grade, entryDate: _entrydate, TTM_Id: _ttm_id }, function (response) {
        if (response.AttendanceType.length > 0) {
            //debugger;
            $("#AttendanceSessionType").empty();
            var arrSessiondetails = response.AttendanceType.filter(e => e.AttendanceType == "Ses1&Ses2")
            if (arrSessiondetails.length > 0) {

                $.each(arrSessiondetails, function (index, item) {
                    var ddlDetails = item.AttendanceDescription.split('&');
                    if (ddlDetails.length > 0) {
                        for (var i = 0; i < ddlDetails.length; i++) {
                            $("#AttendanceSessionType").append('<option value="'
                                + ddlDetails[i] + '">'
                                + ddlDetails[i] + '</option>');
                        }

                    }



                });
            }
            else {
                $.each(response.AttendanceType, function (index, item) {

                    $("#AttendanceSessionType").append('<option value="'
                        + item.AttendanceType + '">'
                        + item.AttendanceDescription + '</option>');


                });
            }



        }
        else {
            $("#AttendanceSessionType").empty();
            $("#AttendanceSessionType").append(`<option value="Session1">Session1 </option>`);
        }
        $("#AttendanceSessionType").selectpicker('refresh');

        attendanceType = $("#AttendanceSessionType").val();
        $('#hdnAttendanceType').val($("#AttendanceSessionType").val());
        FetchAttendance(_ttm_id, _entrydate, _grade, _section, $("#AttendanceSessionType").val());
        //if (response.Freezed) {
        //    $('#divbtnZoom').addClass('d-none');
        //    $('#spnFreezed').removeClass('d-none');

        //    $('#divbtnsave').addClass('d-none');
        //    $('[data-toggle="tooltip"]').tooltip();
        //}
        //else {
        //    $('#divbtnZoom').removeClass('d-none');
        //    $('#spnFreezed').addClass('d-none');
        //    $('#divbtnsave').removeClass('d-none');
        //}


    });

}

function GetAttendanceFreeze(grade, entryDate) {
    //debugger;
    $.get('/Attendance/Attendance/GetAttendanceFreeze', { grade: grade, entryDate: entryDate }, function (response) {

         if (response.Freezed) {
            $('#divbtnZoom').addClass('d-none');
            $('#spnFreezed').removeClass('d-none');

            $('#divbtnsave').addClass('d-none');
            $('[data-toggle="tooltip"]').tooltip();
        }
        else {
            $('#divbtnZoom').removeClass('d-none');
            $('#spnFreezed').addClass('d-none');
            $('#divbtnsave').removeClass('d-none');
        }
    });


}