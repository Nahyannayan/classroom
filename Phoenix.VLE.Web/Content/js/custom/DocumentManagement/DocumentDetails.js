﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var documentDetails = function () {
    var init = function (folderName, id) {
        loadDocumentsdetails(folderName, id);
    },

        loadUploadFilepopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.file;
            globalFunctions.loadPopup(source, '/DocumentManagement/Document/AddAttachment?id=' + id, title, 'library-dailog');

        },

        addUploadFilePopup = function (source, id) {
            loadUploadFilepopup(source, translatedResources.add, id);
        },
        loadUploadFolderpopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.folder;
            globalFunctions.loadPopup(source, '/DocumentManagement/Document/AddFolder?id=' + id, title, 'library-dailog');

        },

        addUploadFolderPopup = function (source, id) {
            loadUploadFolderpopup(source, translatedResources.add, id);
        },
        loadMappingpopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.map;
            globalFunctions.loadPopup(source, '/DocumentManagement/Document/AddMapping?id=' + id, title, 'library-dailog');

        },

        addMappingPopup = function (source, id) {
            loadMappingpopup(source, translatedResources.add, id);
        },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                var id = $('#CurrCatId').val();
                var folderName = '';
                loadDocumentsdetails(folderName, id);
            }
        },


        loadDocumentsdetails = function (folderName, id) {
            $.ajax({
                type: "GET",
                url: "/DocumentManagement/Document/LoadContent",
                data: { id: id },//parent id
                success: function (response) {
                    var library = $('#library');
                    library.empty();
                    library.append(response);
                    $('#CurrCatId').val(id);
                    $('#CurrCatName').val(folderName);
                    if (folderName != '')
                        $('.library-breadcrumbs ul').append("<li><a href='javascript:;' onclick=documentDetails.loadDocumentsdetails(''," + id + ") >" + folderName + "</a></li>");

                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        },

        deleteDocumentData = function (source, id) {
            globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
            $(source).unbind('okClicked');
            $(source).bind('okClicked', source, function (e) {
                $.ajax({
                    type: "POST",
                    url: "/DocumentManagement/Document/DeleteDocumentData",
                    data: { id: id },//id
                    success: function (data) {
                        if (data.Success) {
                            var id = $('#CurrCatId').val();
                            var folderName = '';
                            loadDocumentsdetails(folderName, id);
                        }
                        globalFunctions.showMessage(data.NotificationType, data.Message);
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            });
        },

        deleteCatagoryData = function (source, id) {
            globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
            $(source).unbind('okClicked');
            $(source).bind('okClicked', source, function (e) {
                $.ajax({
                    type: "POST",
                    url: "/DocumentManagement/Document/DeleteCatagoryData",
                    data: { id: id },//id
                    success: function (data) {
                        if (data.Success) {
                            var id = $('#CurrCatId').val();
                            var folderName = '';
                            loadDocumentsdetails(folderName, id);
                        }
                        globalFunctions.showMessage(data.NotificationType, data.Message);
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            });
        }

    return {
        init: init,
        loadDocumentsdetails: loadDocumentsdetails,
        addUploadFilePopup: addUploadFilePopup,
        addUploadFolderPopup: addUploadFolderPopup,
        addMappingPopup: addMappingPopup,
        onSaveSuccess: onSaveSuccess,
        deleteDocumentData: deleteDocumentData,
        deleteCatagoryData: deleteCatagoryData
    };
}();

$(document).ready(function () {
    documentDetails.init('root', 1);
    //---- grid and list view toggle
    $("#grid").addClass('active');
    $('#list').click(function (event) {
        event.preventDefault();
        $('#library .item').addClass('list-group-item');
        $(this).toggleClass('active');
        $("#grid").removeClass('active');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#library .item').removeClass('list-group-item');
        $('#library .item').addClass('grid-group-item');
        $(this).toggleClass('active');
        $("#list").removeClass('active');
    });


    $("#searchLib").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#library .item").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $(document).on('click', '#addFolder', function (event) {
        event.preventDefault();
        documentDetails.addUploadFolderPopup($(this), $('#CurrCatId').val());
    });
    $(document).on('click', '#addFile', function (event) {
        event.preventDefault();
        //initialise fileuploader
        var parameters = ['', $('#CurrCatId').val()];
        multipleFileUploader.init('/DocumentManagement/Document/SaveAttachmentData', documentDetails.loadDocumentsdetails, parameters);

        documentDetails.addUploadFilePopup($(this), $('#CurrCatId').val());
    });
    schoolMappingCommon.init('/DocumentManagement/Document/SaveCatBUMapData', '/DocumentManagement/Document/GetCities/','/DocumentManagement/Document/GetBusinessUnits/');
});



