﻿/// <reference path="../../common/global.js" />
//var popoverEnabler = new LanguageTextEditPopover();
var contentLibrary = function () {

    var init = function (type) {
        if (type === 'student') {
            loadStudentContentLibraryGrid();
        }
        else {
            loadContentLibraryGrid();
        }

    },
        loadContentLibraryGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "ContentName", "sTitle": translatedResources.contentName, "sWidth": "20%", "sClass": "wordbreak" },
                { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "20%", "sClass": "open-details-control wordbreak" },
                { "mData": "SubjectName", "sTitle": translatedResources.curriculumSubject, "sWidth": "20%" },
                //{ "mData": "Status", "sTitle": translatedResources.status, "sWidth": "18%" },
                { "mData": "CreatedBy", "sTitle": translatedResources.createdBy, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "20%" }

            );
            initContentLibraryGrid('tbl-ContentLibrary', _columnData, '/ContentLibrary/Resources/LoadContentLibraryGrid');
            $("#tbl-ContentLibrary_filter").hide();
            $('#tbl-ContentLibrary').DataTable().search('').draw();
            $("#ContentLibrarySearch").on("input", function (e) {
                e.preventDefault();
                $('#tbl-ContentLibrary').DataTable().search($(this).val()).draw();
            });
        },
        downloadAttachment = function (file) {
            debugger;
            window.open(file, "_blank");
        },
        loadStudentContentLibraryGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "ContentName", "sTitle": translatedResources.contentName, "sWidth": "25%", "sClass": "wordbreak" },
                { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "25%", "sClass": "open-details-control wordbreak" },
                { "mData": "SubjectName", "sTitle": translatedResources.subject, "sWidth": "40%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "10%" }

            );
            initContentLibraryGrid('tbl-StudentContentLibrary', _columnData, '/ContentLibrary/Resources/LoadStudentContentLibraryGrid');
            $("#tbl-StudentContentLibrary_filter").hide();
            $('#tbl-StudentContentLibrary').DataTable().search('').draw();
            $("#StudentContentLibrarySearch").on("input", function (e) {
                e.preventDefault();
                $('#tbl-StudentContentLibrary').DataTable().search($(this).val()).draw();
            });
        },
        addContentLibraryPopup = function (source) {
            loadContentLibraryPopup(source, translatedResources.add);
        },
        editContentLibraryPopup = function (source, id) {
            loadContentLibraryPopup(source, translatedResources.edit, id);
        },
        downloadConentImage = function (file) {
            window.open(file, "_blank");
        },
        loadContentLibrary = function () {
            var grid = new DynamicPagination("PaginateResourceLibrary");
            var settings = {
                url: '/ContentLibrary/Resources/PaginateContentLibraryGrid?pageIndex=1'
            };
            grid.init(settings);
        },
        loadContentLibraryPopup = function (source, mode, id) {
            //debugger;
            var title = mode + ' ' + translatedResources.contentresource;
            globalFunctions.loadPopup(source, '/ContentLibrary/Resources/InitAddContentResourceForm?id=' + id, title, 'suggestion-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                //formElements.feSelect();
                $('select').selectpicker({
                    noneSelectedText: translatedResources.PleaseSelect
                });
                $(document).on('click', '#downloadConentImage', function () {
                    var fileName = $('#ContentImage').val();
                    contentLibrary.downloadConentImage(fileName);
                });
            });
        },
        deleteContentResourceData = function (source, id) {
            ////debugger;
            globalFunctions.notyConfirm(source, translatedResources.deleteConfirm);
            $(document).bind('okClicked', source, function (e) {
                $.ajax({
                    type: 'POST',
                    data: { id: id },
                    url: '/ContentLibrary/Resources/DeleteContentResourceData',
                    success: function (result) {
                        globalFunctions.showSuccessMessage(translatedResources.deletedSuccess)
                        contentLibrary.loadContentLibrary();
                    },
                    error: function (msg) {
                    }
                });
            });
            
        },
        saveResource = function (e) {
            if ($('form#frmAddContentResource').valid()) {
                var formData = new FormData();
                var resourceFile = document.getElementById("dropContentImage").files[0];
                formData.append("resourceFile", resourceFile);
                formData.append("IsAddMode", $('#IsAddMode').val());
                formData.append("ContentName", $('#ContentName').val());
                formData.append("Description", $('#Description').val());
                formData.append("SubjectIds", $('#SubjectIds').val());
                formData.append("ContentId", $('#ContentId').val());
                formData.append("RedirectUrl", $('#RedirectUrl').val());
                formData.append("ContentImage", $('#ContentImage').val());
                formData.append("PhysicalPath", $('#PhysicalPath').val());
                formData.append("FileExtenstion", $('#FileExtenstion').val());
                formData.append("IsSharepointFile", $('#IsSharepointFile').val());
                $.ajax({
                    type: 'POST',
                    url: '/ContentLibrary/Resources/InsertUpdateContentResource',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result.Success == true)
                            $('#myModal').modal('hide');
                        globalFunctions.showMessage(result.NotificationType, result.Message);
                        contentLibrary.loadContentLibrary();
                    },
                    error: function (msg) {
                        globalFunctions.onFailure();
                    }
                });
                $("#myModal").modal('hide');
            }
        },
        initContentLibraryGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false
            };
            grid.init(settings);
        };

    return {
        init: init,
        addContentLibraryPopup: addContentLibraryPopup,
        editContentLibraryPopup: editContentLibraryPopup,
        saveResource: saveResource,
        loadContentLibrary: loadContentLibrary,
        downloadConentImage: downloadConentImage,
        deleteContentResourceData: deleteContentResourceData,
        loadStudentContentLibraryGrid: loadStudentContentLibraryGrid,
        downloadAttachment: downloadAttachment,
        loadContentLibraryGrid: loadContentLibraryGrid
    };
}();


$(document).ready(function () {
    $('#content-provider-no-data').toggle(false);
    
    //contentLibrary.init('teacher');
    $("#searchContentProvider").on("keypress", function (e) {
        
        if (e.which == 13) {
            var value = $(this).val().toLowerCase();
            $(".search-item").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                if ($('#content-provider .search-item').length === $('#content-provider .search-item[style="display: none;"]').length) {
                    $('#content-provider-no-data').toggle(true);
                }
                else {
                    $('#content-provider-no-data').toggle(false);
                }

            });
        }
    });
    $("#searchContentLibrary").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $(".librarySearch-item").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $('.paymentDue-toast').addClass('animated fadeInRight');
    $('body').on('click', '.close-toast', function () {
        $(this).parent().parent().fadeOut();
    })
    $('[data-toggle="popover-hover"]').popover({
        html: true,
        trigger: 'hover',
        placement: 'left',
        container: '#groups-popover',
    });
    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'focus',
        placement: 'right',
        container: '#assignment-popover'
    });
    $("#GemsResourceLibrary").on("click", function () {
        contentLibrary.loadContentLibrary();
        //$.ajax({
        //    url: '/ContentLibrary/Resources/GetContentLibrary',
        //    type: 'GET',
        //    success: function (data) {
        //        if (data !== '') {
        //            $("#ResourceLibraryList").html("");
        //            $("#ResourceLibraryList").html(data);
        //            $(".descriptionPopup").on("click", function () {
        //                var desc = $(this).data("desc");
        //                $("#fullDescription").html(desc);
        //                $("#readMoreTitle").html("Description");
        //            });
        //            $(".subjectPopup").on("click", function () {
        //                var desc = $(this).data("subjects");
        //                $("#fullDescription").html(desc);
        //                $("#readMoreTitle").html("Subjects");
        //            });
        //        }
        //        else {
        //            $("#ResourceLibraryList").html("");
        //        }
        //    },
        //    error: function (data, xhr, status) {
        //        globalFunctions.onFailure();
        //    }

        //});
    });
    $(document).on('click', '#btnAddContentLibrary', function () {
        //debugger;
        contentLibrary.addContentLibraryPopup($(this));
    });
    $(document).on('click', '#deleteAttachment', function () {
        $('.attachments-preview').hide();
        $('#ContentImage').val('');
    });
    $(document).on('click', '#btnSaveContentData', function () {
        //debugger;
        contentLibrary.saveResource($(this));
    });
    

 
});
$(window).on("load", function () {

    $(".resources").mCustomScrollbar({
        autoHideScrollbar: true,
        autoExpandScrollbar: true,
        scrollbarPosition: "outside",
    });
    $(".sidebar-fixed .list-group").mCustomScrollbar({
        setHeight: "86%",
        autoExpandScrollbar: true,
        autoHideScrollbar: true,
        scrollbarPosition: "outside"
    });

    $('.loader-wrapper').fadeOut(500, function () {
        //$('.loader-wrapper').remove();
    });
    $(".notification-list").mCustomScrollbar({
        //setHeight: "76%",
        autoExpandScrollbar: true,
        scrollbarPosition: "inside",
        autoHideScrollbar: true,
    });
    $('.search-container').click(function () {
        $('.search-field').addClass('search-input');
        $('.close-icon').addClass('show-icon');
        $('.filter-title').toggleClass('hide-title');
    });
    $('.close-icon').click(function () {
        $('.close-icon').removeClass('show-icon');
    })
    $('.tab-container .content-provider').click(function () {
        let providerView = $('.tab-wrapper .content-provider-wrapper');
        let resourceLibView = $('.tab-wrapper .resource-grid-wrapper');
        $('.tab-container .resource-lib').removeClass('active');
        $('.tab-container .content-provider').addClass('active');
        resourceLibView.addClass('d-none');
        providerView.removeClass('d-none');
    });
    $('.tab-container .resource-lib').click(function () {
        let providerView = $('.tab-wrapper .content-provider-wrapper');
        let resourceLibView = $('.tab-wrapper .resource-grid-wrapper');
        $('.tab-container .content-provider').removeClass('active');
        $('.tab-container .resource-lib').addClass('active');
        resourceLibView.removeClass('d-none');
        providerView.addClass('d-none');
    });
    $("#sortByProvider").change(function () {
        var value = $(this).val();
        var _divisionIds = "";
        $.ajax({
            type: 'POST',
            data: { sortBy: value, divisionIds: _divisionIds},
            url: '/ContentLibrary/Resources/GetContentProvider',
            success: function (result) {
                $('#sectionContentProvider').html(result);
                $('#content-provider-no-data').toggle(false);
            },
            error: function (msg) {
            }
        });
    });

    $(document).on("change", "#ddlSchoolLevelId", function () {
        var value = "";
        var _divisionIds = $("#ddlSchoolLevelId").val();
        $.ajax({
            type: 'POST',
            data: { sortBy: value, divisionIds: _divisionIds.toString() },
            url: '/ContentLibrary/Resources/GetContentProvider',
            success: function (result) {
                $('#sectionContentProvider').html(result);
                $('#content-provider-no-data').toggle(false);
            },
            error: function (msg) {
            }
        });
    });
   
});



