﻿
var ClassListObject = function () {
    var Init = function () {
        PeriodNoArray = [];
    },
        StudentOnReportMasterArray = [],
        StudentOnReportDetailArray = [],
        PeriodNoArray = [];
    LoadStudentOnReport = (studentId) => {
        $.get('/Behaviour/Behaviour/LoadStudentOnReportForm', { studentId: studentId })
            .then(x => {
                $('#studentOnReportDiv').html(x);
                loadSideDrawer('studentOnReportDiv', '45%');
                $('#StudentId').val(studentId);
                $('.date-picker').datetimepicker({
                    format: 'DD-MMM-YYYY'
                })
                $("#SectionId").val($("#ddlBehaviourSectionId").val())
                $('#IsActive')[0].checked = true;
                //var ttm_id = smsCommon.GetQueryStringValue('strtt_id');
                //if (globalFunctions.isValueValid(ttm_id)) {
                //    $('#GroupId').val(ttm_id);
                //} else {
                //    $('#GradeId').val($('#Grades').val());
                //    $('#SectionId').val($('#Section').val());
                //}
            })
    },
        loadSideDrawer = function (target, targetWidth) {
            //if (typeof targetWidth !== typeof undefined && targetWidth !== false) {
            //    $('#' + target).width(targetWidth);
            //}
            //$("#studentBehaviourDrawer").attr("style", "right:0"); //add by ashish
            $('#' + target).toggleClass('open');
            $('#' + target).animate({ //remove by ashish
                right: '0'
            })
        },
        OnStudentOnReportSaveSuccess = (e) => {
            debugger
            if (e.NotificationType) {
                globalFunctions.showMessage(e.NotificationType, e.Message);
            } else {
                $('#studentOnReportFormGrid').html(e);


                $('#studentOnReportForm')[0].reset();
                $('#Id').val('');
                $('#IsActive')[0].checked = true;
                $('#radioButtons').show();
                globalFunctions.showSuccessMessage(translatedResources.updateSuccessMsg);
                $('#PeriodNo').val('');
                PeriodNoArray.length = 0
                var _ttid = $('#hdnTTId').val();
                var _grade = $('#ddlBehaviourGradeId').val();
                var _section = $('#ddlBehaviourSectionId').val();
                if (_ttid != 0) {
                    StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                }
                else {
                    StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                }
            }
        },
        EditStudentOnReport = (id) => {
            debugger
            if (ClassListObject.StudentOnReportMasterArray.some(x => x.Id == id)) {
                var object = ClassListObject.StudentOnReportMasterArray.find(x => x.Id == id)
                //var date = new Date(parseInt(object.FromDate.slice(6, -2)));
                //var FromDate = ('' + (1 + date.getMonth()) + '/' + date.getDate() + '/' + date.getFullYear().toString());

                //var Todate = new Date(parseInt(object.ToDate.slice(6, -2)));
                //var ToDate = ('' + (1 + Todate.getMonth()) + '/' + Todate.getDate() + '/' + Todate.getFullYear().toString());
                var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                var fromDate = moment(object.FromDate).toDate();
                var fromDate = ('' + fromDate.getDate() + '/' + month[fromDate.getMonth()] + '/' + fromDate.getFullYear().toString());
                var Todate = moment(object.ToDate).toDate();
                var Todate = ('' + Todate.getDate() + '/' + month[Todate.getMonth()] + '/' + Todate.getFullYear().toString());
                $('#Id').val(id);
                $('#onReportFromDate').val(fromDate);
                $('#onReportToDate').val(Todate);
                $('#onReportToDesc').val(object.Description);
                $('#IsActive')[0].checked = object.IsActive;
                $('#radioButtons').hide();
                $('.date-picker').datetimepicker({
                    format: 'DD-MMM-YYYY'
                })
                $('.selectpicker').selectpicker('refresh');

            }
        },
        OnStudentOnReportDetailsSaveSuccess = (e) => {
            if (e.NotificationType) {
                globalFunctions.showMessage(e.NotificationType, e.Message);
            } else {
                $('#studentOnReportDetailsFormGrid').html(e);
                $('#studentOnReportDetailsForm')[0].reset();
                $('#onReportDetailId').val('');
                globalFunctions.showSuccessMessage(translatedResources.updateSuccessMsg);
                ClassListObject.PeriodNoArray = [];
            }
        },
        ViewStudentOnReportDetails = (id, EditStatus, studentId, element) => {
            let PeriodNo;
            ClassListObject.PeriodNoArray = [];
            PeriodNoArray.length = 0
            debugger
            if (EditStatus != 1) {
                if ($('.clschkPeriodHeader').length == 0) {
                    globalFunctions.showWarningMessage("Please select period No")
                    return false
                }
                if ($('.clschkPeriodHeader').length > 1) {
                    if (!$('.clschkPeriodHeader').is(":checked")) {
                        globalFunctions.showWarningMessage("Please select period No")
                        return false
                    }
                    $('.clschkPeriodHeader:checked').each(function () {

                        PeriodNo = $(this).attr('data-periodno');
                        PeriodNoArray.push(PeriodNo);
                    });
                }
                else {
                    PeriodNo = $(".activeDay input[type='checkbox']").attr('data-periodno');
                    PeriodNoArray.push(PeriodNo);
                }
            }
            studentId = studentId || $("#StudentId").val();
            $.get('/Behaviour/Behaviour/LoadStudentOnReportDetailsForm', { studentId: studentId, studentOnReportMasterId: id, groupId: $("#CourseGroupId").val() })
                .then(x => {
                    $('#studentOnReportDetailsFormDiv').html(x).show();

                    if (globalFunctions.isValueValid($("#StudentId").val())) {
                        $('#reportBackBtn').show();
                        $('#StudentOnReportForm').hide();
                        $('.formElement').hide()
                    }

                    $('#onReportDetailStudentId').val(studentId);
                    $('#StudentOnReportMasterId').val(id);

                    if (globalFunctions.isValueValid(element)) {
                        $('#PeriodNo').val(PeriodNoArray.join(','));
                        $('#GroupId').val($("#CourseGroupId").val());
                        loadSideDrawer('studentOnReportDetailsFormDiv', '50%');
                        if ($('#attendancedate').is(":visible")) {
                            $('#CreatedOn').val($('#attendancedate').val());
                        } else {
                            $('#CreatedOn').val($(element).data('entrydate'));
                        }
                    }
                    $('.clschkPeriodHeader').prop('checked', false);
                    if (EditStatus == 1) {
                        $("#StudentOnReportDetails th")[5].remove();
                        $('#StudentOnReportDetails tr>td#EditAction').remove();
                    }
                    //else {
                    //    $("#Action").show();
                    //    $("#EditAction").show();
                    //}


                });
        },
        showStudentOnReportMainForm = () => {
            if (globalFunctions.isValueValid($("#StudentId").val())) {
                $('#studentOnReportDetailsFormDiv').hide();
                $('#StudentOnReportForm').show();
            }
        },
        EditStudentOnReportDetails = (id) => {
            $('#PeriodNo').val('');
            debugger
            if (ClassListObject.StudentOnReportDetailArray.some(x => x.Id == id)) {
                var object = ClassListObject.StudentOnReportDetailArray.find(x => x.Id == id)
                $('#onReportDetailId').val(id);
                $('#PeriodNo').val(object.PeriodNo);
                $('#onReportDetailToDesc').val(object.Description);
                ClassListObject.PeriodNoArray = [];
            }
        }
    return {
        Init,
        StudentOnReportMasterArray,
        StudentOnReportDetailArray,
        LoadStudentOnReport,
        OnStudentOnReportSaveSuccess,
        EditStudentOnReport,
        OnStudentOnReportDetailsSaveSuccess,
        ViewStudentOnReportDetails,
        showStudentOnReportMainForm,
        EditStudentOnReportDetails,
        loadSideDrawer
    }
}();

$(document).on('click', 'input[name="WeekMonthRange"]', function () {
    $('#onReportFromDate,#onReportToDate').val('');
    if (['1', '2'].some(x => x == $('input[name="WeekMonthRange"]:checked').val())) {
        $('#onReportFromDate').datetimepicker('date', new Date())
        if ($('input[name="WeekMonthRange"]:checked').val() == '1') {
            $('#onReportToDate').datetimepicker('date', new Date(new Date().setDate(new Date().getDate() + 7)))
        } else if ($('input[name="WeekMonthRange"]:checked').val() == '2') {
            $('#onReportToDate').datetimepicker('date', new Date(new Date().setMonth(new Date().getMonth() + 1)))
        }
    }
    $('.date-picker').datetimepicker({
        format: 'DD-MMM-YYYY'
    })
        .on('dp.change', function (e) {
            $('#onReportFromDate').data("DateTimePicker").maxDate(e.date);
            $('#onReportToDate').data("DateTimePicker").minDate(e.date);
            $('#onReportToDate').next('label').addClass("active");
        });
    $(".selectpicker").selectpicker('refresh');
});
