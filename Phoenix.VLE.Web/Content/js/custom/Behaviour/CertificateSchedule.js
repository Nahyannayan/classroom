﻿
var popoverEnabler = new LanguageTextEditPopover();
var certificateScheduling = function () {
    var init = function () {
        $('#hdnCurriculumId').val($('#CurriculumId').val());
        loadCertificateGrid();
    },
        loadCertificateGrid = function (id) {
            var _columnData = [];
            _columnData.push(
                { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "30%" },
                { "mData": "CertificateType", "sTitle": translatedResources.certificateType, "sWidth": "20%" },
                { "mData": "Points", "sTitle": translatedResources.points, "sWidth": "10%" },                
                { "mData": "Print", "sTitle": translatedResources.print, "sWidth": "10%" },
                //{ "mData": "Email", "sTitle": translatedResources.email, "sWidth": "10%" },
                { "mData": "EmailOther", "sTitle": translatedResources.emailOthers, "sWidth": "10%" },
                { "mData": "ScheduleType", "sTitle": translatedResources.schedule, "sWidth": "20%" }
            );
            if (isCustomPermissionEnabled.toBoolean()) {
                _columnData.push({ "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "10%" });
            }
            initCertificateGrid('tblCertificateScheduling', _columnData, '/Behaviour/BehaviourSetup/LoadCertificateSchedule?curriculumId=' + $('#CurriculumId').val());
        },

        initCertificateGrid = function (_tableId, _columnData, _url) {

            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [1, 2]
                }],
                searching: true
            };
            grid.init(settings);

            $("#tblCertificateScheduling_filter").hide();
            $('#tblCertificateScheduling').DataTable().search('').draw();
            $("#searchTeacherTemplate").on("input", function (e) {
                e.preventDefault();
                $('#tblCertificateScheduling').DataTable().search($(this).val()).draw();
            });
        },
        onSuccess = function (response) {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success) {
                $('#myModal').modal('hide');
                loadCertificateGrid();
            }
            
        },
        AddEditCertificateSchedule = function (source, id) {
            var header = globalFunctions.isValueValid(id) ? translatedResources.editSchedule : translatedResources.addSchedule;
            globalFunctions.loadPopup($(source), '/Behaviour/BehaviourSetup/CertificateSchedulingsCUD?id=' + id, header, '')
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $('#hdnCurriculumId').val($('#CurriculumId').val())

                var isAdd = parseInt($('#CertificateSchedulingId').val()) <= 0;

                var checkedValue = parseInt($('input[name="ScheduleType"]:checked').val());
                var data = Array.from($('#tblCertificateScheduling').DataTable().data());
                var hideWeekly = data.some(x => x.ScheduleTypeId == weekly),
                    hideMonthly = data.some(x => x.ScheduleTypeId == monthly);
                

                if (hideWeekly && (checkedValue != weekly || isAdd))
                    $(`input[name="ScheduleType"][value="${weekly}"]`).parent().remove();
                if (hideMonthly && (checkedValue != monthly || isAdd))
                    $(`input[name="ScheduleType"][value="${monthly}"]`).parent().remove();

                $('.selectpicker').selectpicker('refresh');
                popoverEnabler.attachPopover();
            })
        },
        deleteCertificateSchedule = function (id) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                var token = $('input[name="__RequestVerificationToken"]').val();
                common.postRequest('/Behaviour/BehaviourSetup/CertificateSchedulingsCUD', {
                    __RequestVerificationToken: token,
                    CertificateSchedulingId: id,
                    IsActive: false
                })
                    .then(x => {
                        globalFunctions.showMessage(x.NotificationType, x.Message);
                        if (x.Success) {
                            loadCertificateGrid();
                        }
                    });
            });
        }
    return {
        init,
        loadCertificateGrid,
        initCertificateGrid,
        onSuccess,
        AddEditCertificateSchedule,
        deleteCertificateSchedule
    }
}();

$(document).ready(function () {
    certificateScheduling.init();
})

$(document).on('click', '#btnAddSchedule', function () {
    certificateScheduling.AddEditCertificateSchedule(this, '');
})
    .on('change', '#CurriculumId', function () {
        certificateScheduling.loadCertificateGrid();
    });