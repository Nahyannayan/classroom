﻿var GlobalSubCategoryList = [];
var popoverEnabler = new LanguageTextEditPopover();
//----------------Functionality----------------------
var objBehaviourSetup = function () {

    GetSubCategoryGrid = () => {
        loadBehaviourSetupGrid();
        //var MainCategoryId = $("#ddlMainCategory").val() == '' ? '0' : $("#ddlMainCategory").val();
        //$.post("/Behaviour/BehaviourSetup/SubCategoryGrid", { MainCategoryId }, function (response) {
        //    $("#divBehaviourSetupGrid").html('');
        //    $("#divBehaviourSetupGrid").html(response);
        //});
    },

        loadBehaviourSetupGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "MainCategoryName", "sTitle": translatedResources.MainCategoryName, "sWidth": "20%", "sClass": "wordbreak" },
                { "mData": "SubCategoryName", "sTitle": translatedResources.SubCategoryName, "sWidth": "20%", "sClass": "wordbreak" },
                { "mData": "GradeDisplay", "sTitle": translatedResources.GradeDisplay, "sWidth": "20%", "sClass": "open-details-control wordbreak" },
                { "mData": "CategoryScore", "sTitle": translatedResources.CategoryScore, "sWidth": "20%", "sClass": "open-details-control wordbreak" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.Action, "sWidth": "20%" },
                { "mData": "BehaviourCategoryXml", "sClass": "text-center no-sort d-none" }
            );
            var MainCategoryId = $("#ddlMainCategory").val() == '' ? '0' : $("#ddlMainCategory").val();
            initBehaviourSetupGrid('tbl-BehaviourSetup', _columnData, '/Behaviour/BehaviourSetup/SubCategoryGrid?MainCategoryId=' + MainCategoryId);


        },
        AddEditSubCategory = (SubCategoryID = 0) => {
            var behaviourSetup = {};
            GlobalSubCategoryList = Array.from($('#tbl-BehaviourSetup').DataTable().rows().data());
            if (SubCategoryID > 0) {
                behaviourSetup = GlobalSubCategoryList.find(function (x) { if (x.SubCategoryID == SubCategoryID) return x; })
            }

            $.post("/Behaviour/BehaviourSetup/AddEditSubCategory", { behaviourSetup }, function (response) {
                $("#divBehaviourSetupDrawer").html('');
                $("#divBehaviourSetupDrawer").append(response);
                $('.selectpicker').selectpicker('refresh');
            }).done(function () {
                popoverEnabler.attachPopover();
            });
        },
        SaveSubCategory = () => {
            var IsValid = common.IsInvalidByFormOrDivId('frmBehaviourSetup');
            if (IsValid) {
                var GRD_ID = $("#ddlEditBSGrade").val() == undefined ? ($("#ddlM_EditBSGrade").val()).join('|') : $("#ddlEditBSGrade").val();
                var form = $('#frmBehaviourSetup')[0];
                var formData = new FormData(form);
                formData.append("GradeIds", GRD_ID);

                $.ajax({
                    url: "/Behaviour/BehaviourSetup/SaveSubCategory",
                    data: formData,
                    type: 'POST',
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false, // NEEDED, DON'T OMIT THIS
                    success: function (response) {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        if (response.Success) {
                            $('.closeRightDrawer').trigger('click');
                            objBehaviourSetup.GetSubCategoryGrid();
                        }
                    }
                });
            }
        },
        DeleteSubCategory = (SubCategoryID, element) => {
            common.notyDeleteConfirm(translatedResources.deleteConfirm, () => {
                $.post("/Behaviour/BehaviourSetup/DeleteSubCategory", { SubCategoryID }, function (response) {
                    if (response.Success) {
                        var table = $('#tbl-BehaviourSetup').DataTable();
                        table.row($(element).closest("tr")).remove().draw();
                    }
                    else
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                });
            });
        },
        DeleteBehaviourSetupImage = () => {
            $("#divBehaviourSetupImage").remove();
            $("#IsDeletedImage").val(true);
        },
        initBehaviourSetupGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false
            };
            grid.init(settings);

        };
    return {
        GetSubCategoryGrid,
        AddEditSubCategory,
        SaveSubCategory,
        DeleteSubCategory,
        DeleteBehaviourSetupImage
    }
}();
//----------End Functionality-------------------------

//----------Event Handling Functionality--------------
$(document).ready(function () {
    objBehaviourSetup.GetSubCategoryGrid();
    $(document).on('click', '#AddEditSubCat', function () {
        objBehaviourSetup.SaveSubCategory();
    });
    $('#tbl-BehaviourSetup').DataTable().search('').draw();
    $("#SchoolBannerSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-BehaviourSetup').DataTable().search($(this).val()).draw();
    });

    $("#ddlMainCategory").on("change", function () {
        objBehaviourSetup.GetSubCategoryGrid();
    });
});
//----------End Event Handling Functionality----------
