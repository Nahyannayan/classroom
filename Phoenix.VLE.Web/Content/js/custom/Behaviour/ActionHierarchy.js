﻿
var ActionHierarchy = function () {
    var Init = function () {
        common.init();
        ActionHierarchy.DesignationRoutes = DesignationRoutesObject;
    },
        DesignationRoutes = [],
        AddDesignation = () => {
            
            var FromDesignation = $('.DesignationList.active').data('id');
            var toDesignations = $("#SelectedDesignationList input[type='checkbox']:checked:visible");

            if (!(globalFunctions.isValueValid(FromDesignation) && toDesignations.length > 0)) {
                globalFunctions.showWarningMessage(globalFunctions.isValueValid(FromDesignation) ? "Minimum on designation need to be selected to assign" : "Please select designation")
                return;
            }

            //to set all the to designation set is active false and set active true for checked
            var CurrentObjectList = ActionHierarchy.DesignationRoutes.filter(x => x.DesignationFromId == FromDesignation.id);
            CurrentObjectList.forEach(x => x.IsActive = false);

            toDesignations.each((i, e) => {
                if (ActionHierarchy.DesignationRoutes.some(x => x.DesignationToId == $(e).data('id').id)) {
                    var object = ActionHierarchy.DesignationRoutes.find(x => x.DesignationToId == $(e).data('id').id);
                    object.DesignationFromId = FromDesignation.id;
                    object.DesignationFrom = FromDesignation.name;
                    object.DesignationToId = $(e).data('id').id;
                    object.DesignationTo = $(e).data('id').name;
                    object.DesignationOrder = i + 1;
                    object.IsActive = true;
                } else {
                    var object = {
                        DesignationFromId: FromDesignation.id,
                        DesignationFrom: FromDesignation.name,
                        DesignationToId: $(e).data('id').id,
                        DesignationTo: $(e).data('id').name,
                        DesignationOrder: i + 1,
                        IsActive: true
                    }
                    ActionHierarchy.DesignationRoutes.push(object);
                }
            });
            ActionHierarchy.PopulateBucket(ActionHierarchy.DesignationRoutes);
            ActionHierarchy.SaveActionHierarchy();
        },
        searchInDesignationList = (element, divId) => {
            var searchText = $(element).val().toLowerCase();
            $('#' + divId).find('div[data-search-text],li[data-search-text]').each(function () {
                var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
                $(this).toggle(showCurrentLi);
            });
            if (divId == 'SelectedDesignationList') {
                var selected = $('.DesignationList.active');
                if (globalFunctions.isValueValid(selected)) {
                    $(`#SelectedDesignationList div[data-search-text="${$(selected).data('id').name}"]`).toggle(false)
                }
            }
        },
        searchInSelectedDesignationList = (element) => {

        },
        loadDesignationBucketUsingDesignation = (designationId) => {
            $('#AllocatedDesignationSet').html('');
            $.get('/Behaviour/BehaviourSetup/GetActionHierarchyByDesignationId', { designationId: designationId })
                .then(x => {
                    $('#AllocatedDesignationSet').html(x);
                });
        },
        PopulateBucket = DesignationRoutesObject => {
            var object = DesignationRoutesObject.filter(x => x.DesignationFromId == $('.DesignationList.active').data('id').id)
            $('#AllocatedDesignationSet').html('');
            common.postRequest('/Behaviour/BehaviourSetup/GetActionHierarchyBuckets', { routings: object })
                .then(x => {
                    $('#AllocatedDesignationSet').html(x);
                    $('#SelectedDesignationList').find('input[type = checkbox]:visible').prop('checked', false)
                })
        },
        EditBucket = fromDesignationId => {
            $('#SelectedDesignationList').find('input[type = checkbox]:visible').prop('checked', false)
            var object = ActionHierarchy.DesignationRoutes.filter(x => x.DesignationFromId == fromDesignationId && x.IsActive);

            if (globalFunctions.isValueValid(object) && object.length > 0) {
                $('.DesignationList').removeClass('active');
                $(`.DesignationList#li_${object[0].DesignationFromId}`).addClass('active');

                $(`#SelectedDesignationList div[data-search-text="${object[0].DesignationFrom.toUpperCase()}"]`).toggle(false);

                object.forEach(x => {
                    $('#SelectedDesignationList').find(`input[id="${x.DesignationToId}"]:visible`).prop('checked', true);
                });
            }
        },
        DeleteBucket = fromDesignationId => {
            common.notyDeleteConfirm(translatedResources.deleteConfirm, () => {
                var object = ActionHierarchy.DesignationRoutes.filter(x => x.DesignationFromId == fromDesignationId);
                object.forEach(x => x.IsActive = false);
                ActionHierarchy.SaveActionHierarchy();
                if (!($(`.DesignationList#li_${fromDesignationId}`).hasClass('active'))) {
                    $('.DesignationList').removeClass('active');
                    $(`.DesignationList#li_${fromDesignationId}`).addClass('active');
                    ActionHierarchy.loadDesignationBucketUsingDesignation();
                } else {
                    ActionHierarchy.PopulateBucket(ActionHierarchy.DesignationRoutes);
                }   
            });
        },
        SaveActionHierarchy = () => {
            common.postRequest('/Behaviour/BehaviourSetup/SaveActionHierarchy', { routings: ActionHierarchy.DesignationRoutes })
                .then(response => {
                    if (response.NotificationType) {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                    } else {
                        response.forEach(x => {
                            var from = $('#SelectedDesignationList').find(`input[id="${x.DesignationFromId}"]`).data('id');
                            var to = $('#SelectedDesignationList').find(`input[id="${x.DesignationToId}"]`).data('id');
                            x.DesignationFrom = from.name;
                            x.DesignationTo = to.name;
                        });
                        globalFunctions.showSuccessMessage(translatedResources.updateSuccessMsg);
                        ActionHierarchy.DesignationRoutes = response;
                    }
                })
        }
    return {
        Init,
        DesignationRoutes,
        AddDesignation,
        searchInDesignationList,
        searchInSelectedDesignationList,
        loadDesignationBucketUsingDesignation,
        PopulateBucket,
        EditBucket,
        DeleteBucket,
        SaveActionHierarchy
    };
}();

$(document).on("change", "#chkSelectAllDesignationToRemove", function (e) {
    var $inputs = $('#SelectedDesignationList').find('input[type = checkbox]:visible');
    if (e.originalEvent === undefined) {
        var allChecked = true;
        $inputs.each(function () {
            allChecked = allChecked && this.checked;
        });
        this.checked = allChecked;
    } else {
        $inputs.prop('checked', this.checked);
    }
});

$(document).on('click', '.DesignationList', function () {
    $('.DesignationList').removeClass('active');
    $(this).addClass('active');
    $('#SelectedDesignationList div[data-search-text]').toggle(true)
    $(`#SelectedDesignationList div[data-search-text="${$(this).data('id').name}"]`).toggle(false)
    ActionHierarchy.loadDesignationBucketUsingDesignation($(this).data('id').id);
})

$(document).ready(function () {
    ActionHierarchy.Init();
})