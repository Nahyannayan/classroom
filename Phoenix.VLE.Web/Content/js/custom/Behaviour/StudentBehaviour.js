﻿var formElements = new FormElements();
var studentIdList = [];
var taskOrder = 0;
var multiSelectModule = 0;
var hndStuId = 0;
var modelcategoryId = 0;

var StudentBehaviour = function () {
    var lstSubCategories = [];
    var BehavioursubCategorylist = [];
    var init = function () {
        $('[data-toggle="tooltip"]').tooltip();

    },
        SectionLoad = function LoadonSectionChange(_ttid, _grade, _section, IsFilterByGroup = false) {
            var GroupId = $("#ddlCourseGroupId").val() != '' ? $("#ddlCourseGroupId").val() : '0';
            //if ((_section == "" || _grade == "") && !IsFilterByGroup) {
            //    $('#divStudentListDetails').empty();
            //    globalFunctions.showWarningMessage("No Data");
            //    return false;
            //}
            var url = "/Behaviour/Behaviour/GetStudentList";
            $.ajax({
                type: 'GET',
                url: url,
                data: { tt_id: _ttid, grade: _grade, section: _section, IstudentBehaviour: 1, GroupId: GroupId, IsFilterByGroup: IsFilterByGroup },
                success: function (response) {
                    $('#divStudentListDetails').empty();
                    $('#divStudentListDetails').html(response);
                    StudentBehaviour.init();
                },
                error: function (ex) {
                    globalFunctions.onFailure();
                }
            });
            return false;
        },
        openModal = function openModal(StudentId, AddMultiple) {
            //$('.behave.modal').modal('show');
            //$('#StudentId').val(StudentId);
            //hndStuId = StudentId;
            //multiSelectModule = AddMultiple;
            //$.get('/Behaviour/Behaviour/StudentBehaviourList', function (response) {

            //    $('#divstudentbehaviourlist').html(response);

            //});
            $('.trigger-rightDrawer').trigger('click');


        },
        openSelectedModal = function openSelectedModal(StudentId) {
            $('.selected.modal').modal('show');
            var grd = $('#ddlBehaviourGradeId').val().split('|');
            $.get("/Behaviour/Behaviour/GetMeritDetails", { studentId: StudentId, type: "", gradeId: grd[0] }, function (response) {
                // ;
                $('#divSelectedBehaviour').html(response);
                $('.clsbehaviourEdit').hide();
                $('.clsbehaviourDelete').hide();
            });
        },
        openAddEditModal = function openAddEditModal(behaviourId) {
            $('.AddEdit.modal').modal('show');

            $.post("/Behaviour/Behaviour/EditStudentBehaviourType", { behaviourId: behaviourId }, function (response) {
                // ;
                $('#divAddEditBehaviour').html(response);
                $('#divBehaveIcon').hide();

            });

        },
        HoverPopup = function HoverPopup(a, state) {
            if (state == '1') {

                $(a).find(".behav-profile").attr("data-header-animation", true);
                // $(".behav-profile").attr("data-header-animation", true);
            }
            else {
                $(a).find(".behav-profile").attr("data-header-animation", false);
                //  $(".behav-profile").attr("data-header-animation", false);
            }
        },
        lstofstudentIds = function addStudentIdTolist(chkStudentId) {
            if ($('#chkstudent_' + chkStudentId).is(":checked")) {
                studentIdList.push(chkStudentId);
            }
            else {
                var index = studentIdList.indexOf(chkStudentId);
                if (index > -1) {
                    studentIdList.splice(index, 1);
                }
            }

        },
        SubmitBulkIds = function SubmitBulkIds() {
            //  ;
            $.post("/Behaviour/Behaviour/BulkInsert", { lstofstudentId: studentIdList }, function (response) {
                globalFunctions.showMessage(response.NotificationType, response.Message);

            });
        },
        EditBehaviourType = function EditBehaviourType() {
            var behaviourId = $('#hndBehaviourId').val() == undefined || null ? $('#ddlBehaviourList').val() : $('#hndBehaviourId').val();
            var behaviourType = $('#txtBehaviourType').val();
            var behaviourPoint = $('#txtBehaviourpoint').val();
            var categoryId = $('#ddlCategoryType').val() == 0 || undefined || null ? 0 : $('#ddlCategoryType').val();
            $.post("/Behaviour/Behaviour/EditStudentTypeById", { behaviourId: behaviourId, behaviourType: behaviourType, behaviourPoint: behaviourPoint, categoryId: categoryId }, function (response) {
                globalFunctions.showMessage(response.NotificationType, response.Message);
                $.get('/Behaviour/Behaviour/StudentBehaviourList', { categoryId: modelcategoryId }, function (response) {
                    $('#divstudentbehaviourlist').html(response);
                });
                //content to load student and its details
                var _ttid = $('#hdnTTId').val();
                var _grade = $('#ddlBehaviourGradeId').val();
                var _section = $('#ddlBehaviourSectionId').val();

                if (_ttid != 0) {
                    StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                }
                else {
                    StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                }
                $('.AddEdit.modal').modal('hide');
                // $('.behave.modal').modal('hide');

            });
        },
        BehaviourOnChange = function BehaviourOnChange(behaviourId) {
            var behaviourId = $(behaviourId).val();
            if (behaviourId == 0) {
                return false;
            }
            else {
                $.post('/Behaviour/Behaviour/OnselectLoadBehaviourLogo', { behaviourId: behaviourId }, function (response) {
                    $('#imgBehaviour').attr('src', "/Content/img/SIMS/Behaviour/" + response);
                    $('#divBehaveIcon').show();
                });

            }

        },
        BehaviourEdit = function BehaviourEdit() {
            $("#BehaveEdit").hide();
            $(".behav-container-edit").show();
            $("#BehaveAdd").show();

        },
        BehaviourEditModelClose = function BehaviourEditModelClose() {
            $("#BehaveEdit").show();
            $(".behav-container-edit").hide();
            $("#BehaveAdd").hide();
        },
        BehaviourContentaddition = function BehaviourContentaddition(BehaviourId) {
            var behaviourId = BehaviourId;
            var studentId = hndStuId;
            if (multiSelectModule == 1) {
                if (studentIdList.length == 0) {
                    globalFunctions.showMessage(2, "Please select Students to Add");
                    $('.behave.modal').modal('hide');
                    return false;
                }
                else {
                    $.post("/Behaviour/Behaviour/BulkInsert", { lstofstudentId: studentIdList, behaviourId: behaviourId }, function (response) {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        ;
                        var _ttid = $('#hdnTTId').val();
                        var _grade = $('#ddlBehaviourGradeId').val();
                        var _section = $('#ddlBehaviourSectionId').val();

                        if (_ttid != 0) {
                            StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                        }
                        else {
                            StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                        }

                    });
                }

            }
            else {
                $.post("/Behaviour/Behaviour/AddEditStudentBehaviour", { studentId: studentId, behaviourId: behaviourId }, function (response) {

                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    ;
                    var _ttid = $('#hdnTTId').val();
                    var _grade = $('#ddlBehaviourGradeId').val();
                    var _section = $('#ddlBehaviourSectionId').val();
                    if (_ttid != 0) {
                        StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                    }
                    else {
                        StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                    }
                    //StudentBehaviour.SectionLoad(_ttid, _grade, _section);

                });
            }
            studentIdList.length = 0;
            $('.behave.modal').modal('hide');

        },
        CategoryOnchange = function CategoryOnchange(categoryId) {
            var id = $(categoryId).val();
            modelcategoryId = $(categoryId).val();
            var grdId = $('#ddlBehaviourGradeId').val();//.split('|')
            var GroupId = $('#ddlCourseGroupId').val();
            $.get('/Behaviour/Behaviour/GetSubCategoriesByCategoryId', { categoryId: id, gradeId: grdId, GroupId: GroupId }, function (response) {
                var titleHeading = $(categoryId).find("option:selected").text();
                $('#MeritType').val(titleHeading);
                $('#divsubcategories').html(response);
                StudentBehaviour.init();
            });
        },
        BehaviourCatalogWithUpload = function BehaviourCatalogFileUpload(id) {
            $('#BehaviourId').val(id);
            $('#StudentId').val(hndStuId);
            // $('#data').submit();
            var form = $('#data')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);
            // var url = $('#data').attr('action');
            if (multiSelectModule == 1) {
                if (studentIdList.length == 0) {
                    globalFunctions.showMessage(2, translatedResources.PleaseSelectStudent);
                    $('.behave.modal').modal('hide');
                    return false;
                }
                else {

                    formData.append('lstofstudentId', studentIdList);
                    $.ajax({
                        url: "/Behaviour/Behaviour/BulkInsert",
                        data: formData,
                        type: 'POST',
                        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                        processData: false, // NEEDED, DON'T OMIT THIS
                        // ... Other options like success and etc
                        success: function (response) {
                            globalFunctions.showMessage(response.NotificationType, response.Message);
                            ;
                            var _ttid = $('#hdnTTId').val();
                            var _grade = $('#ddlBehaviourGradeId').val();
                            var _section = $('#ddlBehaviourSectionId').val();
                            if (_ttid != 0) {
                                StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                            }
                            else {
                                StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                            }
                        }
                    });
                }

            }
            else {
                $.ajax({
                    url: "/Behaviour/Behaviour/AddEditStudentBehaviour",
                    data: formData,
                    type: 'POST',
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false, // NEEDED, DON'T OMIT THIS
                    // ... Other options like success and etc
                    success: function (response) {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        ;
                        var _ttid = $('#hdnTTId').val();
                        var _grade = $('#ddlBehaviourGradeId').val();
                        var _section = $('#ddlBehaviourSectionId').val();
                        if (_ttid != 0) {
                            StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                        }
                        else {
                            StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                        }
                    }
                });
            }



            studentIdList.length = 0;
            $('.behave.modal').modal('hide');
        },
        DownloadBehaviourAttachment = function DownloadBehaviourAttachment(studentId, UploadedId) {

            ;
            var stuId = studentId == undefined || null ? 0 : studentId
            if (stuId != 0) {
                // window.location.href = "/Behaviour/Behaviour/DownloadAttachment?studentId=" + stuId;
                window.location.href = `/Behaviour/Behaviour/DownloadAttachment?studentId =${studentId}&UploadedId=${UploadedId}`;

            }


        },
        OncardClick = function OncardClick(card, event) {
            $(card).toggleClass("selected-stud");
            var studentId = $(card).attr('data-studentId');

            // $("#chkstudent_" + studentId).trigger("click");
            //$(".custom-control-input").val($(this).is(':checked'));   
            //$('input[name=LocationFilter]').attr('checked', true);

            if ($(card).hasClass('selected-stud')) {

                // $(card).find(".custom-control-input").attr('checked', true);
                studentIdList.push(studentId);

            }
            else {
                // $(card).find(".custom-control-input").attr('checked', false);
                $(card).find(".card.card-chart").removeClass('selected-stud');
                var index = studentIdList.indexOf(studentId);
                if (index > -1) {
                    studentIdList.splice(index, 1);
                }
            }


        },
        DeleteBehaviour = function DeleteBehaviour(Bhvdelete) {
            var studentId = $(Bhvdelete).attr('data-studentId');
            var BehaviourId = $(Bhvdelete).attr('data-behaviourId');

            globalFunctions.notyConfirm($(Bhvdelete), translatedResources.deleteConfirm);
            $(Bhvdelete).unbind('okClicked');
            $(Bhvdelete).bind('okClicked', Bhvdelete, function (e) {
                $.post('/Behaviour/Behaviour/DeleteStudentBehaviour', { studentId: studentId, BehaviourId: BehaviourId }, function (response) {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    // $('#divSelectedBehaviour').html(response.Result);
                    var _ttid = $('#hdnTTId').val();
                    var _grade = $('#ddlBehaviourGradeId').val();
                    var _section = $('#ddlBehaviourSectionId').val();
                    var GroupId = $("#ddlCourseGroupId").val();
                    var IsFilterByGroup = GroupId != '0' && GroupId != '' ? true : false;
                    if (_ttid != 0) {
                        StudentBehaviour.SectionLoad(_ttid, _grade, _section, IsFilterByGroup);
                    }
                    else {
                        StudentBehaviour.SectionLoad(_ttid, _grade, _section, IsFilterByGroup);
                    }
                });
            });
        },
        ListOfSubCategoriesId = function (subCategoryId, levelMappingId, element) {
            var hdnMeritId = $("#MeritId").val();
            if (hdnMeritId != "0") {
                StudentBehaviour.lstSubCategories = [];
                if ($(element).is(":checked")) {
                    var object = {
                        CategoryId: $('#MeritCategoryId').val(),
                        SubcategoryId: subCategoryId,
                        LevelMappingId: levelMappingId
                    }
                    StudentBehaviour.lstSubCategories.push(object);
                    $(".chkLst").prop('checked', false);
                    var chkId = $(element)[0].id;
                    $(chkId).prop('checked', true);
                }
            }
            else {
                if ($('#chkLst_' + subCategoryId + levelMappingId).is(":checked")) {
                    var object = {
                        CategoryId: $('#MeritCategoryId').val(),
                        SubcategoryId: subCategoryId,
                        LevelMappingId: levelMappingId
                    }
                    StudentBehaviour.lstSubCategories.push(object);
                    $('#chkLst_' + subCategoryId).prop('checked', true);
                }
                else {
                    var indexof = StudentBehaviour.lstSubCategories.findIndex(x => x.SubcategoryId === subCategoryId);

                    if (indexof > -1) {
                        StudentBehaviour.lstSubCategories.splice(indexof, 1);
                    }
                    $('#chkLst_' + subCategoryId).prop('checked', false);
                }
            }
        },
        ListOfSubCategorieslstId = function (subCategoryId, levelMappingId, element) {
            var hdnMeritId = $("#MeritId").val();
            if (hdnMeritId != "0") {
                StudentBehaviour.lstSubCategories = [];
                if ($(element).is(":checked")) {
                    var object = {
                        CategoryId: $('#MeritCategoryId').val(),
                        SubcategoryId: subCategoryId,
                        LevelMappingId: levelMappingId
                    }
                    StudentBehaviour.lstSubCategories.push(object);
                    $(".chkLst").prop('checked', false);
                    var chkId = $(element)[0].id;
                    $('.' + chkId).prop('checked', true);
                }
            }
            else {
                if ($('#chkLst_' + subCategoryId + levelMappingId).is(":checked")) {
                    var object = {
                        CategoryId: $('#MeritCategoryId').val(),
                        SubcategoryId: subCategoryId,
                        LevelMappingId: levelMappingId
                    }
                    StudentBehaviour.lstSubCategories.push(object);
                    $('.chkLst_' + subCategoryId + levelMappingId).prop('checked', true);
                }
                else {
                    var indexof = StudentBehaviour.lstSubCategories.findIndex(x => x.SubcategoryId === subCategoryId);

                    if (indexof > -1) {
                        StudentBehaviour.lstSubCategories.splice(indexof, 1);
                    }
                    $('.chkLst_' + subCategoryId + levelMappingId).prop('checked', false);
                }
            }
        },
        SaveStudentMeritDeMerit = function () {

            if (common.IsInvalidByFormOrDivId('frmMeritDemerit')) {
                var form = $('#frmMeritDemerit')[0]; // You need to use standard javascript object here
                var formData = new FormData(form);
                var studentId = $('#StudentId').val()
                if (studentId > 0) {
                    var index = studentIdList.indexOf(studentId);
                    if (index > -1) {
                        studentIdList[index] = studentId;
                    }
                    else {
                        studentIdList.push(studentId);
                    }
                    formData.append("StudentIds", studentIdList);
                }
                if (studentId == 0) {
                    if (studentIdList.length > 0) {
                        formData.append("StudentIds", studentIdList);
                    }
                    else {
                        globalFunctions.showWarningMessage(translatedResources.PleaseSelectStudent);
                        $('.closeRightDrawer').trigger('click');
                        return false;
                    }
                }
                if ($('input[id^="chkLst_"]:checked').length == 0 && $('input[id^="chk_"]:checked').length == 0) {
                    globalFunctions.showWarningMessage(translatedResources.pleaseSelectBehaviour);
                    return false;
                }

                formData.append("MeritDemertIds", JSON.stringify(StudentBehaviour.lstSubCategories));
                var IsExistFile = false;
                if ($('[id*=listIt]').length > 0) {
                    IsExistFile = true;
                }
                formData.append("IsExistFile", IsExistFile);

                $.ajax({
                    url: "/Behaviour/Behaviour/SaveMeritDeMerit",
                    data: formData,
                    type: 'POST',
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false, // NEEDED, DON'T OMIT THIS
                    // ... Other options like success and etc
                    success: function (response) {
                        globalFunctions.showMessage(response.NotificationType, response.Message);

                        $('.closeRightDrawer').trigger('click');
                        //;
                        var _ttid = $('#hdnTTId').val();
                        var _grade = $('#ddlBehaviourGradeId').val();
                        var _section = $('#ddlBehaviourSectionId').val();
                        var GroupId = $("#ddlCourseGroupId").val();
                        var IsFilterByGroup = GroupId != '0' && GroupId != '' ? true : false;
                        if (_ttid != 0) {
                            StudentBehaviour.SectionLoad(_ttid, _grade, _section, IsFilterByGroup);
                        }
                        else {
                            StudentBehaviour.SectionLoad(_ttid, _grade, _section, IsFilterByGroup);
                        }

                        StudentBehaviour.lstSubCategories.length = 0;
                        studentIdList.length = 0;
                        StudentBehaviour.BehavioursubCategorylist.length = 0;
                        $('.clsChkStudents').prop('checked', false);
                    }
                });
            }
        },
        TriggerDrawer = function (aHref) {
            var meritId = $(aHref).attr('data-meritId');
            var studentId = $(aHref).attr('data-StudentId');
            var IsEdit = $(aHref).attr('data-edit');
            var gradeId = $('#ddlBehaviourGradeId').val();//.split('|');
            var GroupId = $('#ddlCourseGroupId').val();
            if (IsEdit == 1) {
                $('#H2drawerTitle').text(`${translatedResources.editBehaviour}`);
            }
            else {
                $('#H2drawerTitle').text(`${translatedResources.addBehaviour}`);
            }
            $('#divStudentBehaviourList').html("");
            $.get('/Behaviour/Behaviour/AddEditStudentBehaviour', { MeritId: meritId, StudentId: studentId, gradeId: gradeId, GroupId: GroupId }, function (response) {
                $('#divStudentBehaviourList').html(response);
                $("#divAddEditStudentMerit").mCustomScrollbar({
                    setHeight: "80vh",
                    autoExpandScrollbar: true,
                    scrollbarPosition: "outside",
                    autoHideScrollbar: true,
                });
                $('.selectpicker').selectpicker('refresh');
                StudentBehaviour.init();
                $('#divfooter').removeClass('d-none');
            });
        },
        OnScoreClick = function (aHref) {
            var point = $(aHref).attr('data-point');
            if (parseInt(point) == 0) {
                //$('.closeRightDrawer').trigger('click');
                return false;
            }
            else {
                let target = $(aHref).data('target');
                let targetWidth = $(aHref).data('width');
                $('#divStudentBehaviourList').html("");
                var meritId = $(aHref).attr('data-meritId');
                var studentId = $(aHref).attr('data-StudentId');
                var gradeId = $('#ddlBehaviourGradeId').val();
                var type = $(aHref).attr('data-type');
                $('#H2drawerTitle').text(`${translatedResources.behaviourList}`);
                $.get('/Behaviour/Behaviour/GetMeritDetails', { studentId: studentId, type: type, gradeId: gradeId }, function (response) {
                    // $("#divsubcatdetailslist").mCustomScrollbar('destroy');
                    $('#divStudentBehaviourList').html(response);
                    $("#divListGridSubCategory").mCustomScrollbar({
                        setHeight: $('.custom-scroll-container').outerHeight(),
                        autoExpandScrollbar: true,
                        scrollbarPosition: "inside",
                        autoHideScrollbar: true,
                    });
                    $('#divfooter').addClass('d-none');
                    $('.selectpicker').selectpicker('refresh');
                    StudentBehaviour.init();
                    StudentBehaviour.loadSideDrawer(target, targetWidth);
                });
            }
        },
        AddMultipleStudent = function (ahref) {
            if (studentIdList.length == 0) {
                globalFunctions.showWarningMessage(translatedResources.PleaseSelectStudent);
                //$('.closeRightDrawer').trigger('click');
                return false;
            }
            else {
                let target = $(ahref).data('target');
                let targetWidth = $(ahref).data('width');
                StudentBehaviour.loadSideDrawer(target, targetWidth);
                StudentBehaviour.TriggerDrawer(ahref);
            }
        },
        DrawerOnClick = function () {
            StudentBehaviour.lstSubCategories.length = 0;
            studentIdList.length = 0;
            StudentBehaviour.BehavioursubCategorylist.length = 0;
            $('.clsChkStudents').prop('checked', false);
            //$("#studentBehaviourDrawer").removeAttr("style", "");//add by ashish
        },
        loadSideDrawer = function (target, targetWidth) {
            if (typeof targetWidth !== typeof undefined && targetWidth !== false) {
                $('#' + target).width(targetWidth);
            }
            //$("#studentBehaviourDrawer").attr("style", "right:0"); //add by ashish
            $('#' + target).toggleClass('open');
            $('#' + target).animate({ //remove by ashish
                right: '0'
            })
        },
        setDropDownValueAndTriggerChange = dropdownId => {
            if ($("#" + dropdownId)[0] !== undefined) {
                if ($("#" + dropdownId)[0].options.length > 1) {
                    $('select[name=' + dropdownId + ']').val($("#" + dropdownId)[0].options[1].value);
                    $('.selectpicker').selectpicker('refresh');
                    $("#" + dropdownId).val($("#" + dropdownId)[0].options[1].value).trigger('change');
                }
            }
        },
        ShowListView = function () {
            $("#list").removeClass('grid-list-unclick');
            $("#list").addClass('grid-list-click');
            $("#grid").addClass('grid-list-unclick');
        },
        ShowGridView = function () {
            $("#grid").removeClass('grid-list-unclick');
            $("#grid").addClass('grid-list-click');
            $("#list").addClass('grid-list-unclick');
        }
    return {
        init: init,
        SectionLoad: SectionLoad,
        openModal: openModal,
        openSelectedModal: openSelectedModal,
        openAddEditModal: openAddEditModal,
        HoverPopup: HoverPopup,
        lstofstudentIds: lstofstudentIds,
        SubmitBulkIds: SubmitBulkIds,
        EditBehaviourType: EditBehaviourType,
        BehaviourOnChange: BehaviourOnChange,
        BehaviourEdit: BehaviourEdit,
        BehaviourEditModelClose: BehaviourEditModelClose,
        BehaviourContentaddition: BehaviourContentaddition,
        CategoryOnchange: CategoryOnchange,
        BehaviourCatalogWithUpload: BehaviourCatalogWithUpload,
        DownloadBehaviourAttachment: DownloadBehaviourAttachment,
        OncardClick: OncardClick,
        DeleteBehaviour: DeleteBehaviour,
        lstSubCategories: lstSubCategories,
        ListOfSubCategoriesId: ListOfSubCategoriesId,
        SaveStudentMeritDeMerit: SaveStudentMeritDeMerit,
        TriggerDrawer: TriggerDrawer,
        OnScoreClick: OnScoreClick,
        AddMultipleStudent: AddMultipleStudent,
        BehavioursubCategorylist: BehavioursubCategorylist,
        DrawerOnClick: DrawerOnClick,
        ListOfSubCategorieslstId: ListOfSubCategorieslstId,
        loadSideDrawer: loadSideDrawer,
        setDropDownValueAndTriggerChange: setDropDownValueAndTriggerChange,
        ShowListView: ShowListView,
        ShowGridView: ShowGridView
    };
}();

$(document).ready(function () {
    StudentBehaviour.init();
    $('#breadcrumbActions').html(`<div class="justify-content-end row">
                                    <div>${html}</div>
                                    <div class="ml-3">
                                     <button onclick="StudentBehaviour.AddMultipleStudent(this)" data-target="studentBehaviourDrawer" type="button" class="btn btn-primary m-0"><i class="fas fa-plus mr-2"></i>${translatedResources.addBehaviour}</button>
                                    </div>
                                    </div>`);
    $('#CurriculumId').selectpicker().trigger('change');
    //if ($("#ddlCourseGroupId option").length > 1) {
    //    $("#ddlCourseGroupId option")[1].selected = true;
    //    $("#ddlCourseGroupId").selectpicker('refresh');
    //}
    $("#ddlBehaviourSectionId").change(function () {
        $("#ddlCourseGroupId").val('');
        $("#ddlCourseGroupId").selectpicker('refresh');
        var _ttid = 0;
        var _grade = $('#ddlBehaviourGradeId').val();
        var _section = $('#ddlBehaviourSectionId').val();

        if ($(this).val() == "") {
            $('#divStudentListDetails').empty();
            globalFunctions.showWarningMessage("No Data");
        }
        else {
            StudentBehaviour.SectionLoad(_ttid, _grade, _section);
        }
    })

    $('#ddlBehaviourGradeId').on('change', function () {
        if ($(this).val() == "") {
            $('#divStudentListDetails').empty();
            globalFunctions.showWarningMessage("No Data");
            return false;
        }
        common.bindSingleDropDownByParameter(
            "#ddlBehaviourSectionId",
            '/Behaviour/Behaviour/FetchSectionListByGrade',
            { GradeId: $(this).val() },
            "",
            translatedResources.SelectText
        );
        StudentBehaviour.setDropDownValueAndTriggerChange('ddlBehaviourSectionId');
    });

    //StudentBehaviour.setDropDownValueAndTriggerChange('ddlBehaviourGradeId');

    $('#ddlCourseGroupId').on('change', function () {
        $("#ddlBehaviourGradeId").val('');
        $("#ddlBehaviourSectionId").val('');
        $("#ddlBehaviourGradeId").selectpicker('refresh');
        $("#ddlBehaviourSectionId").selectpicker('refresh');
        var _ttid = 0;
        var _grade = $('#ddlBehaviourGradeId').val();
        var _section = $('#ddlBehaviourSectionId').val();
        if ($(this).val() == "") {
            $('#divStudentListDetails').empty();
            globalFunctions.showWarningMessage("No Data");
            return false;
        }
        StudentBehaviour.SectionLoad(_ttid, _grade, _section, true);
    });

    $.extend($.expr[":"], {
        "containsIN": function (elem, i, match, array) {
            return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        }
    });
    $('#search').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);

        if (keycode == '13') {
            $('.clsstudetails').removeClass('d-none');
            var filter = $(this).val(); // get the value of the input, which we filter on
            if (filter != "") {
                $('#products').find('.student-meta:not(:containsIN("' + filter.split(' ').pop() + '"))').parent().parent().addClass('d-none');
                $('#products').find('.student-meta:not(:containsIN("' + filter.split(' ')[0] + '"))').parent().parent().addClass('d-none');
                if ($('#products').find('.student-meta:not(:containsIN("' + filter.split(' ').pop() + '"))').parent().length == $('#hdnClsCount').val()

                ) {
                    $('#divshowMessage').removeClass('d-none');
                }
                else {
                    $('#divshowMessage').addClass('d-none');
                }
            }
            else {
                $('#products').find('.student-meta').parent().parent().removeClass('d-none');
                $('#divshowMessage').addClass('d-none');
            }
        }
    });

    // Ellipsis for for student name 
    $(document).on('mouseenter', ".tooltip-name", function () {
        var $this = $(this);
        if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
            $this.tooltip({
                title: $this.text(),
                placement: "top"
            });
            $this.tooltip('show');
        }
    });

    $('.hide-name').css('width', $('.hide-name').parent().width());

    $(".modal-content .close").click(function () {
        $("#BehaveEdit").show();
        $(".behav-container-edit").hide();
        $("#BehaveAdd").hide();
    });
})
    .on('change', '#CourseId', function () {
        common.bindSingleDropDownByParameter('#CourseGroupId', '/Behaviour/Behaviour/GetCourseGroupByCourseId', { courseid: $(this).val() }, '', translatedResources.selectedText, false, 'ItemId', 'ItemName');
    })
    .on('change', '#CourseGroupId', function () {
        $('#divStudentListDetails').empty();
        $.get('/Behaviour/Behaviour/GetStudentListBygGroupId', { groupId: $(this).val() })
            .then(response => {
                $('#divStudentListDetails').html(response);
                StudentBehaviour.init();
            });
    })
    .on('change', "#CurriculumId", function () {
        common.bindSingleDropDownByParameterAsync('#ddlBehaviourGradeId', '/Behaviour/Behaviour/GetGradeWhenCurriculumChange', { curriculumId: $(this).val() }, '', translatedResources.selectedText, 'ItemId', 'ItemName')
            .then(response => StudentBehaviour.setDropDownValueAndTriggerChange('ddlBehaviourGradeId'));
    });
