﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
//------------------------Incident Functionality------------------------
var GlobalSubCategoryList = [];

var Incident = function () {
    var studentList = [],
        witness = [],
        studentLoadData = [],
        witnessLoadData = [],
        parentTable = [],
        init = function () {
            //Populate Student Involved list in Edit Mode Datatable---------------------------------- 
            var studentInvolvedList = Incident.selectedStudents;
            Incident.studentLoadData.forEach(e => {
                studentInvolvedList.push({
                    InvolvedId: e.InvolvedId,
                    GRADE:
                    {
                        grade: e.GRADE
                    },
                    SECTION:
                    {
                        section: e.SECTION
                    },
                    STUDENT:
                    {
                        id: e.STUDENT_ID, text: e.STUDENT_NAME
                    },
                    IsConfidential: e.IsConfidential == "Yes" ? true : false,
                    DataMode: 'Edit',
                    ActionTakenId: e.ActionTakenId
                })
            })
            Incident.populateOtherStudentInvoled(studentInvolvedList);
            //---------------------------------------------------------------------------------------
            //Populate Witness list in Edit mode-----------------------------------------------------
            var witness = Incident.witness;
            Incident.witnessLoadData.forEach(e => {
                witness.push({ WitnessId: e.WitnessId, id: e.WitnessInvolvedId, Name: e.WitnessInvolvedName, type: e.WitnessType == 0 ? translatedResources.staff : translatedResources.student, comment: e.WitnessRemark, WitnessDatamode: 'Edit' });
            })
            Incident.populatewitnessList(witness);
            //----------------------------------------------------------------------------------------
            if (globalFunctions.isValueValid($('#ddlAcademicYear').val()) && globalFunctions.isValueValid($('#month').val())) {
                Incident.loadIncidentLandingPageData($('#ddlAcademicYear').val(), $('#month').val() == "" ? 0 : $('#month').val() == "All" ? 0 : $('#month').val());
            }
            if (parseInt($('#IncidentId').val()) <= 0)
                $('#Category').val('3').trigger('change');


            $('.selectpicker').selectpicker('refresh');
        },
        toggler = toggle => {

            event.stopPropagation();

            if ($(toggle).children("i").hasClass('fa-plus-square')) {
                $(toggle).children("i").removeClass('fa-plus-square').addClass('fa-minus-square');
            } else {
                $(toggle).children("i").removeClass('fa-minus-square').addClass('fa-plus-square');
            }

            var $target = $(event.target);

            if ($target.closest("td").attr("colspan") > 1) {
                $target.slideUp();
            } else {
                $target.closest("tr").next().find(".subjectList").slideToggle();
            }
        },
        getStudentList = (tt_id, gradeId, sectionId) => {
            $.get("/Attendance/GetStudentList", { grade: gradeId, section: sectionId }).fail(() => globalFunctions.showErrorMessage("Unable to load student list"))
        },
        GetCategoryLevel = (SubCategoryID) => {

            $.post("/Behaviour/Behaviour/GetSubCategoryLevel", { SubCategoryID }, function (response) {
                $("#SubCategoryLevelId").empty();
                $("#SubCategoryLevelId").append('<option value>Select</option>');
                $.each(response, function (i, option) {
                    if (Number(option.Mapping_ID) > 0) {
                        $("#SubCategoryLevelId").append('<option value="' + option.Mapping_ID + '" data-point="' + option.Level_Score + '">' + option.Level_Description + '</option>');
                    }
                });
                $('.selectpicker').selectpicker('refresh');

                //for (var i = 0; i < response.length; i++) {
                //    $("#SubCategoryLevelId").append("<option value='"
                //        + response[i].Level_ID + "'>"
                //        + response[i].Level_Description + "</option>");
                //}
                //$('.selectpicker').selectpicker('refresh');
            });
        },
        updateStudentInvolvedArray = (selectedStudents) => {
            var selectedStudentIds = $('#ddlOtherStudentList').val();
            selectedStudentIds.forEach((studentId) => {
                if (!selectedStudents.some(e => e.STUDENT.id == studentId)) {
                    selectedStudents.push({
                        InvolvedId: 0,
                        GRADE:
                        {
                            gradeId: $('#ddlGrades').val(), grade: $('#ddlGrades option:selected').text()
                        },
                        SECTION:
                        {
                            sectionId: $('#ddlSection').val(), section: $('#ddlSection  option:selected').text()
                        },
                        STUDENT:
                        {
                            id: studentId, text: $('#ddlOtherStudentList option[value=' + studentId + ']').text()
                        },
                        DataMode: 'Add',
                        IsConfidential: true
                    })
                } else if (selectedStudents.some(e => e.STUDENT.id == studentId && e.DataMode == 'Delete')) {
                    var filter = selectedStudents.filter(e => e.STUDENT.id == studentId);
                    if (filter.length) {
                        filter[0].DataMode = 'Edit';
                    }
                } else {

                }
            });
        },
        populateOtherStudentInvoled = (studentList) => {
            var incidentId = parseInt($('#IncidentId').val()), actionAnchor = '';
            var trText = "";
            for (var i = 0; i < studentList.length; i++) {
                if (studentList[i].DataMode != 'Delete') {
                    if (incidentId && $('#chkActiontoggler').is(':checked'))
                        actionAnchor = `<a onclick="BehaviourAction.GetBehaviourAction(${incidentId},${studentList[i].STUDENT.id})" class="action" data-toggle="tooltip" data-target="ActionSideDrawer" data-title="Action"><span><i class="fas fa-share"></i></span></a>`;
                    // if action is taken then avoid delete
                    var actionDelete = studentList[i].ActionTakenId == 0 || !globalFunctions.isValueValid(studentList[i].ActionTakenId) ? `<a href="JavaScript:void(0);" data-studentid="${studentList[i].STUDENT.id}" onclick="Incident.deleteOtherStudentInvoledList(${studentList[i].STUDENT.id})" class="table-action-icon-btn delete" data-toggle="tooltip" data-title="${translatedResources.Delete}" data-placement="left"><span><i class="fas fa-trash"></i></span></a>` : ``;
                    trText += ` <tr>
                                <td>${studentList[i].GRADE.grade}</td>
                                <td class="text-center">${studentList[i].SECTION.section}</td>
                                <td>${studentList[i].STUDENT.text}</td>
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox custom-control-inline mr-5">
                                        <input type="checkbox"  onchange="Incident.updateConfidentialFlag(${i}, this.checked)" ${studentList[i].IsConfidential ? 'checked' : ''}>
                                        
                                    </div>                                    
                                </td>
                                <td class="text-center">
                                    ${actionAnchor} &nbsp
                                    ${actionDelete}
                                </td>
                            </tr>`;
                }
            }
            // to destroy and recreate table
            //if ($.fn.DataTable.isDataTable($('#involvedStudentList')))
            //    $('#involvedStudentList').DataTable().destroy();
            $('#involvedStudentList > tbody').html('');
            $('#involvedStudentList > tbody').html(trText)

            //$('#involvedStudentList').DataTable({ "autoWidth": false, });


            //if (studentList.length) {
            //    $('#involvedStudentList').show();
            //} else {
            //    $('#involvedStudentList').hide();
            //}
            Incident.hideShowTable('#involvedStudentList', studentList)
        },
        populatewitnessList = (witnessList) => {
            var trText = "";
            for (var i = 0; i < witnessList.length; i++) {
                if (witnessList[i].WitnessDatamode != 'Delete')
                    var witnessComment = witnessList[i].comment == null ? '' : witnessList[i].comment;
                trText += ` <tr>
                                <td>${witnessList[i].type}</td>
                                <td>${witnessList[i].Name}</td>
                                <td class="comment" id="witnessCommentTD_${i}"><span> ${witnessComment}</span><div class="m-auto md-form md-outline"><input type="text" class="form-control" data-alphanumeric data-maxlength="500" value="${witnessComment}"/></div></td>                                
                                <td class="text-center">
                                    <a href="JavaScript:void(0);" onclick="Incident.UpdateWitnessList(${witnessList[i].id},this,'witnessCommentTD_${i}')" class="table-action-icon-btn save" data-toggle="tooltip" data-title="${translatedResources.Save}" data-placement="left"><span><i class="fas fa-save"></i></span></a>
                                    <a href="JavaScript:void(0);" onclick="Incident.editWitnessList(this)" class="table-action-icon-btn edit" data-toggle="tooltip" data-title="${translatedResources.Edit}" data-placement="left"><span><i class="fas fa-pen"></i></span></a>
                                    &nbsp<a href="JavaScript:void(0);" onclick="Incident.deleteWitnessList(${witnessList[i].id})" class="table-action-icon-btn delete" data-toggle="tooltip" data-title=${translatedResources.Delete}"" data-placement="left"><span><i class="fas fa-trash"></i></span></a></td>
                            </tr>`;
            }
            // to destroy and recreate table
            //Incident.destroyDataTable('#witnessStudentList')
            $('#witnessStudentList > tbody').html('');
            $('#witnessStudentList > tbody').html(trText)

            $('#witnessStudentList > tbody > tr > td > div > input').hide()
            $('#witnessStudentList > tbody > tr > td > a.save').hide()

            //$('#witnessStudentList').DataTable({ "autoWidth": false, });
            //if (witnessList.length) {
            //    $('#witnessStudentList').show();
            //} else {
            //    $('#witnessStudentList').hide();
            //}
            Incident.hideShowTable('#witnessStudentList', witnessList)
        },
        updateConfidentialFlag = (index, value) => {
            Incident.selectedStudents[index].IsConfidential = value;
        }
    deleteOtherStudentInvoledList = (studentId) => {
        globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
            //update dropdown list ---------------------------------------------
            var list = $('#ddlOtherStudentList').val(),
                index = $('#ddlOtherStudentList').val().indexOf(`${studentId}`);
            if (index >= 0) {
                list.splice(index, 1);
                $('#ddlOtherStudentList').selectpicker('val', list);
            }
            //update dropdown list-----------------------------------------------

            //update table list
            var studentList = Incident.selectedStudents;
            Incident.selectedStudents = studentList.filter(e => e.STUDENT.id != studentId);
            //if (involedStudentIndex != undefined && involedStudentIndex >= 0)
            //    studentList[involedStudentIndex].DataMode = 'Delete';
            Incident.populateOtherStudentInvoled(Incident.selectedStudents);
        });
    },
        deleteWitnessList = (witnessId) => {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                //update table list
                var witnessList = Incident.witness;
                Incident.witness = witnessList.filter(e => e.id != witnessId);
                //if (involedwitnessIndex != undefined && involedwitnessIndex >= 0)
                //    witnessList[involedwitnessIndex].WitnessDatamode = 'Delete';
                Incident.populatewitnessList(Incident.witness);
            });
        },
        hideShowTable = (tableId, array) => {
            if (array.length && $(tableId + ' > tbody > tr ').length) {
                $(tableId).show();
            } else {
                $(tableId).hide();
            }
        },
        destroyDataTable = (tableId) => {
            // to destroy and recreate table
            if ($.fn.DataTable.isDataTable($(tableId)))
                $(tableId).DataTable().destroy();
        },
        loadIncidentLandingPageData = (academicYearId, month) => {
            $.get('/Behaviour/Behaviour/GetIncidentChartByCategory', { academicYearId: academicYearId, month: month }, (response) => { $('#BarGraph').html(response); })
            $.get('/Behaviour/Behaviour/GetIncidentChartByGrade', { academicYearId: academicYearId, month: month }, (response) => { $('#PieChart').html(response); })
            $.get('/Behaviour/Behaviour/GetIncidentList', { academicYearId: academicYearId, month: month, isFA: $('#chkActiontoggler').is(':checked') }, (response) => { $('#ParentTable').html(response);/* Incident.filterIncidentRecords($('#chkActiontoggler')[0].checked, 'tblIncidentList')*/ })
        },
        submitIncidentData = () => {

            var studentInvoledObject = Incident.selectedStudents.map(function (d, i) {
                return {
                    InvolvedId: d.InvolvedId || 0,
                    InvolvedStudentId: d.STUDENT.id,
                    IsConfidential: d.IsConfidential,
                    DataMode: d.DataMode
                };
            });
            if (studentInvoledObject.filter(x => x.DataMode.toLowerCase() == 'add' || x.DataMode.toLowerCase() == 'edit').length <= 0) {
                globalFunctions.showWarningMessage(translatedResources.selectStudent);
                return;
            }


            var witnessList = Incident.witness.map((d, i) => {
                return {
                    WitnessId: d.WitnessId,
                    WitnessInvolvedId: d.id,
                    WitnessType: d.type === translatedResources.staff ? 0 : 1,
                    WitnessRemark: d.comment,
                    WitnessDatamode: d.WitnessDatamode
                };
            });

            if (witnessList.filter(x => x.WitnessDatamode.toLowerCase() == 'add' || x.WitnessDatamode.toLowerCase() == 'edit').length <= 0) {
                globalFunctions.showWarningMessage(translatedResources.selectWitness);
                return;
            }

            var behaviourData = {
                BehaviourId: $('#IncidentId').val(),
                IncidentDate: globalFunctions.toSystemReadableDate($('#dueDatePicker').val()),
                IncidentTime: globalFunctions.toSystemReadableTime($('#dueTimePicker').val()),
                IncidentType: $('#chkActiontoggler')[0].checked ? 0 : 1,
                IncidentDesc: $('#Comments').val(),
                CategoryId: $('#SubCategory').val(),
                CurriculumId: $('#CurriculumId').val(),
                LevelMapping_ID: $('#SubCategoryLevelId').val(),
                StudentInvolved: studentInvoledObject,
                Witness: witnessList
            };

            $.post('/Behaviour/Incident/SaveIncidentDetails', behaviourData)
                .done((response) => {
                    var isNew = $('#IncidentId').val() == "0";
                    if (parseInt(response.InsertedRowId) > 1) {
                        $('#IncidentId').val(response.InsertedRowId)
                        Incident.populateOtherStudentInvoled(Incident.selectedStudents);
                        if ($('#chkActiontoggler').prop('checked') && isNew) {
                            //noty({
                            //    text: response.Message + " please add action",
                            //    layout: 'topCenter',
                            //    modal: true,
                            //    closeWith: ['button'],
                            //    buttons: [
                            //        {
                            //            addClass: 'btn btn-success btn-clean',
                            //            text: 'Ok',
                            //            onClick: function ($noty) {
                            //                $noty.close();
                            //            }
                            //        }
                            //    ]

                            //});
                            globalFunctions.showSuccessMessage(translatedResources.incidentAdded);
                        } else {
                            globalFunctions.showSuccessMessage(isNew ? translatedResources.addSuccessMsg : translatedResources.updateSuccessMsg);
                            window.location.href = '/behaviour/incident/incidentdashboard';
                        }
                    } else {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                    }
                    //setTimeout(function () {
                    //    $.noty.closeAll();

                    //}, 2000);                    
                })
            //.fail((error) => globalFunctions.showErrorMessage(translatedResources.technicalError));
        },
        formatSubCategoryList = () => {
            $('#input-points').parent().parent().hide();
            var val = $('#SubCategory').val();
            if (val && val != '') {
                $('#input-points').parent().parent().show();
                var point = $('#SubCategory option[value=' + val + ']').text().split('|')[1];

                $('#input-points').removeClass(parseFloat(point) <= 0 ? "bg-success" : "bg-danger");
                $('#input-points').parent('span').removeClass(parseFloat(point) <= 0 ? "bg-success" : "bg-danger");

                $('#input-points').addClass(parseFloat(point) > 0 ? "bg-success" : "bg-danger");
                $('#input-points').parent('span').addClass(parseFloat(point) > 0 ? "bg-success" : "bg-danger");

                $('#input-points').text(point)
            }
            var list = $('#SubCategory option');
            for (var i = 0; i < list.length; i++) {
                $(list[i]).attr('data-point', $(list[i]).text().split('|')[1]);
                $(list[i]).text($(list[i]).text().split('|')[0])
            }
            $('.selectpicker').selectpicker('refresh');
        },
        editWitnessList = (cell) => {
            $(cell).parent().prev().find('span').hide();
            $(cell).parent().prev().find('input').show();
            $(cell).hide();
            $(cell).prev().show();
        },
        UpdateWitnessList = (witnessId, cell, tdId) => {
            if (common.IsInvalidByFormOrDivId(tdId)) {
                $(cell).hide();
                $(cell).next().show();
                $(cell).parent().prev().find('span').html($(cell).parent().prev().find('input').val());
                $(cell).parent().prev().find('span').show();
                $(cell).parent().prev().find('input').hide();
                var witnessList = Incident.witness;
                var involedwitnessIndex = witnessList.findIndex(e => e.id == witnessId);
                if (involedwitnessIndex != undefined) {
                    witnessList[involedwitnessIndex].WitnessDatamode = 'Edit';
                    witnessList[involedwitnessIndex].comment = $(cell).parent().prev().find('input').val();
                }
            }
        },
        GetFollowUp = (incidentId, actionId) => {
            if (incidentId && actionId) {
                $.get('/Behaviour/Behaviour/GetBehaviourActionFollowups', { incidentId, actionId }, (response) => {
                    $('#divSelectedBehaviour').html(response);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selected.modal').modal('show');
                })
            }
        },
        resetWitnessForm = function () {
            $('#txtWitnessComment').val('');
            if ($('#chkwitnesstoggler').prop('checked')) {
                $('#divstaffwitness').show();
                $('#ddlwitnessstaff').val('').attr('data-required', "").data('required', "");

                $('#divwitnessstudent').hide();
                $('#ddlwitnessGrades,#ddlwitnessSection,#ddlwitnessStudentList').val('').removeAttr('data-required').removeData('required');

            } else {
                $('#divstaffwitness').hide();
                $('#ddlwitnessstaff').val('').removeAttr('data-required').removeData('required');

                $('#divwitnessstudent').show();
                $('#ddlwitnessGrades,#ddlwitnessSection,#ddlwitnessStudentList').val('').attr('data-required', "").data('required', "");
            }
            $('.selectpicker').selectpicker('refresh');
        },
        showFollowUpForm = () => {
            var incidentId = $('#IncidentId').val();
            var StudentId = $('#StudentId').val();

            if (incidentId && StudentId) {
                $.get('/Behaviour/Incident/GetBehaviourActionFollowupForm', { incidentId, StudentId }, (response) => {
                    $('#followUpForm').html(response);
                    $('.selectpicker').selectpicker('refresh');
                    BehaviourAction.showSuccessAndGetFollowUp();
                })
            }
        }
    return {
        init: init,
        toggler: toggler,
        getStudentList: getStudentList,
        selectedStudents: studentList,
        witness: witness,
        parentTable: parentTable,
        studentLoadData: studentLoadData,
        witnessLoadData: witnessLoadData,
        populateOtherStudentInvoled: populateOtherStudentInvoled,
        deleteOtherStudentInvoledList: deleteOtherStudentInvoledList,
        updateStudentInvolvedArray: updateStudentInvolvedArray,
        populatewitnessList: populatewitnessList,
        deleteWitnessList: deleteWitnessList,
        destroyDataTable: destroyDataTable,
        loadIncidentLandingPageData: loadIncidentLandingPageData,
        submitIncidentData: submitIncidentData,
        updateConfidentialFlag: updateConfidentialFlag,
        formatSubCategoryList: formatSubCategoryList,
        editWitnessList: editWitnessList,
        UpdateWitnessList: UpdateWitnessList,
        hideShowTable: hideShowTable,
        GetFollowUp: GetFollowUp,
        GetCategoryLevel: GetCategoryLevel,
        resetWitnessForm: resetWitnessForm,
        showFollowUpForm: showFollowUpForm
    }
}();


$(document).ready(function () {


    //start other student involve section------------------------------------------------------------------
    $("#ddlGrades").change(function () {
        if (globalFunctions.isValueValid($(this).val())) {
            common.bindSingleDropDownByParameter('#ddlSection', '/Behaviour/Incident/GetSections', { gradeId: $(this).val() }, '', translatedResources.selectedText);
        }
    });

    $("#ddlSection").change(function () {
        var grade = $('#ddlGrades').val();
        var section = $(this).val();
        if (globalFunctions.isValueValid(section)) {
            common.bindSingleDropDownByParameter('#ddlOtherStudentList', '/Behaviour/Incident/GetStudentListUsingGradeSection', { gradeId: grade, sectionId: section }, '', '');
        }
    });

    $('#ddlOtherStudentList').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
        var studentInvolvedList = Incident.selectedStudents;
        if (!newValue) {
            var array = $('#ddlOtherStudentList>option').map(function () { return $(this).val(); });
            var involedStudentIndex = studentInvolvedList.findIndex(e => e.STUDENT.id == array[clickedIndex]);
            studentInvolvedList.splice(involedStudentIndex, 1);
        }
        Incident.updateStudentInvolvedArray(studentInvolvedList)

    });

    $('#btnsavestudentlist').click(() => {
        if (common.IsInvalidByFormOrDivId('studentForm')) {
            var selectedStudents = Incident.selectedStudents;
            if (selectedStudents.length) {
                Incident.populateOtherStudentInvoled(selectedStudents);
            }
        }
    })
    //end other student involve section--------------------------------------------------------------------

    //start witness section--------------------------------------------------------------------------------
    $("#ddlwitnessGrades").change(function () {
        if (globalFunctions.isValueValid($(this).val())) {
            common.bindSingleDropDownByParameter('#ddlwitnessSection', '/Behaviour/Incident/GetSections', { gradeId: $(this).val() }, '', translatedResources.selectedText);
        }
    });

    $("#ddlwitnessSection").change(function () {
        $("#ddlwitnessStudentList").empty();
        var grade = $('#ddlwitnessGrades').val();
        var section = $(this).val();
        if (globalFunctions.isValueValid(section)) {
            common.bindSingleDropDownByParameter('#ddlwitnessStudentList', '/Behaviour/Incident/GetStudentListUsingGradeSection', { gradeId: grade, sectionId: section }, '', translatedResources.selectedText);
        }
    });

    $('#btnsavewitnesslist').click(() => {
        if (common.IsInvalidByFormOrDivId('witnessForm')) {
            var witness = Incident.witness;
            var witnessId = $('#chkwitnesstoggler')[0].checked ? $('#ddlwitnessstaff').val() : $('#ddlwitnessStudentList').val();
            if (!witness.some(e => e.id == witnessId) && witnessId != '' && witnessId != 'select') {
                if ($('#chkwitnesstoggler')[0].checked) {
                    witness.push({ id: witnessId, Name: $('#ddlwitnessstaff option:selected').text(), type: translatedResources.staff, comment: $('#txtWitnessComment').val(), WitnessDatamode: 'Add' });
                } else {
                    witness.push({ id: witnessId, Name: $('#ddlwitnessStudentList option:selected').text(), type: translatedResources.student, comment: $('#txtWitnessComment').val(), WitnessDatamode: 'Add' });
                }
            }

            if (witness.length) {
                Incident.populatewitnessList(witness);
            }
            Incident.resetWitnessForm();
        }
    })
    //end witness section----------------------------------------------------------------------------------   

    $("#Category").change(function () {
        $("#SubCategory").empty().selectpicker('refresh');
        var categeory = $("#Category").val();
        if (globalFunctions.isValueValid(categeory)) {
            try {
                var url = "/Behaviour/Incident/FetchSubCategory";
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    data: { id: $("#Category").val() },
                    success: function (options) {
                        $("#SubCategory").append('<option value="">Select</option>');
                        $.each(options, function (i, option) {
                            $("#SubCategory").append('<option value="' + option.ItemId + '" data-point=' + option.ItemName.split('|')[1] + ' data-haslevel=' + option.ItemName.split('|')[2] + '>' + option.ItemName.split('|')[0] + '</option>');
                        });
                        $('.selectpicker').selectpicker('refresh');
                    }
                });
                return false;
            } catch (e) {
                alert(e);
            }
        }
    });

    $("#SubCategory").change(() => {
        $('#input-points').text('');
        var selectedPoint = $("#SubCategory option:selected").attr("data-point");
        var haslevel = $("#SubCategory option:selected").attr("data-haslevel");

        if (haslevel == "1") {

            $('#divSubCategoryLevel').show();
            Incident.GetCategoryLevel($('#SubCategory option:selected').val());

        }
        else {
            $('#divSubCategoryLevel').hide();
            $('#input-points').parent().parent().show();

            $('#input-points').removeClass(parseFloat(selectedPoint) <= 0 ? "bg-success" : "bg-danger");
            $('#input-points').parent('span').removeClass(parseFloat(selectedPoint) <= 0 ? "bg-success" : "bg-danger");

            $('#input-points').addClass(parseFloat(selectedPoint) > 0 ? "bg-success" : "bg-danger");
            $('#input-points').parent('span').addClass(parseFloat(selectedPoint) > 0 ? "bg-success" : "bg-danger");

            $('#input-points').text(selectedPoint);
        }

    });

    $("#SubCategoryLevelId").change(() => {
        var selectedPoint = $("#SubCategoryLevelId option:selected").attr("data-point");

        $('#input-points').parent().parent().show();
        $('#input-points').removeClass(parseInt(selectedPoint) <= 0 ? "bg-success" : "bg-danger");
        $('#input-points').parent('span').removeClass(parseInt(selectedPoint) <= 0 ? "border-success" : "border-danger");
        $('#input-points').addClass(parseInt(selectedPoint) > 0 ? "bg-success" : "bg-danger");
        $('#input-points').parent('span').addClass(parseInt(selectedPoint) > 0 ? "border-success" : "border-danger");
        $('#input-points').text(selectedPoint);

    });


    Incident.init();
    $('#divSubCategoryLevel').hide();
    Incident.showFollowUpForm();
});