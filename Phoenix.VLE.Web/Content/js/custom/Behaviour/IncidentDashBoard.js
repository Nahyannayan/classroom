﻿
var incidentDashBoard = function () {
    var init = function () {

    },
        loadIncidentLandingPageData = function (curriculumId, month) {
            $.get('/Behaviour/Incident/GetIncidentList', { academicYearId: curriculumId, month: month, isFA: $('#chkActiontoggler').is(':checked') })
                .then((response) => {
                    $('#BarGraph').html(response.barChart);
                    $('#PieChart').html(response.pieChart);
                    $('#ParentTable').html(response.table);
                });
        },
        filterIncidentRecords = function (checked, tableId) {
            incidentDashBoard.showHideAction(tableId);
            var object = incidentDashBoard.parentTable;
            if (rows.length && tableId == 'tblIncidentList') {
                $.post('/Behaviour/Incident/GetIncidentListFilter?isFA=' + checked, { incidentLists: object }, (response) => {
                    $('#ParentTable').html(response);
                    incidentDashBoard.showHideAction(tableId);
                });
            }
        },
        loadChildStudentListToggler = function (toggle, incidentId) {
            event.stopPropagation();
            var $target = $(event.target);
            if ($target.closest("tr").next().find(".subjectList").length > 0 && $(toggle).children("i").hasClass('fa-plus-square')) {
                $target.closest("tr").next().find(".subjectList").slideToggle();
            } else if ($target.closest("tr").next('tr').children().attr('colspan') > 1) {
                $target.closest("tr").next().find(".subjectList").slideUp()
            } else {
                $.get('/Behaviour/Incident/GetIncidentStudentList', { incidentId }, (response) => {
                    $(response).insertAfter($target.closest("tr"))
                    //$target.closest("tr").next().find(".subjectList").slideToggle();
                    incidentDashBoard.showHideAction('tblIncidentList');
                });
            }

            if ($(toggle).children("i").hasClass('fa-plus-square')) {
                $(toggle).children("i").removeClass('fa-plus-square').addClass('fa-minus-square');
            } else {
                $(toggle).children("i").removeClass('fa-minus-square').addClass('fa-plus-square');
            }

        },
        showHideAction = function(tableId) {
            rows = $('#' + tableId + ' tr');
            if ($('#chkActiontoggler').is(':checked')) {
                rows.children().children().filter('.action').show();
                rows.children().filter('.action').show();
            } else {
                rows.children().children().filter('.action').hide()
                rows.children().filter('.action').hide()
            }
        },
        showFollowUpForm = () => {
            var incidentId = $('#IncidentId').val();
            var StudentId = $('#StudentId').val();

            if (incidentId && StudentId) {
                $.get('/Behaviour/Incident/GetBehaviourActionFollowupForm', { incidentId, StudentId }, (response) => {
                    $('#followUpForm').html(response);
                    $('.selectpicker').selectpicker('refresh');
                    BehaviourAction.showSuccessAndGetFollowUp();
                })
            }
        }
        
    return {
        init,
        loadIncidentLandingPageData,
        filterIncidentRecords,
        loadChildStudentListToggler,
        showHideAction,
        showFollowUpForm

    }
}();

$(document)
    .on('change', '#Category', function () {
        common.bindSingleDropDownByParameter('#SubCategory', '/Behaviour/Incident/FetchSubCategory', { id: $(this).val() }, '', '', false, 'ItemId', 'ItemName');
    })
    .on('change', '#chkActiontoggler',function() {
        incidentDashBoard.filterIncidentRecords($('#chkActiontoggler')[0].checked, 'tblIncidentList');
    })
    .ready(function () {
        var month = $('#month');
        var curriculum = $('#CurriculumId');
        curriculum.on('change', function () {
            var value = globalFunctions.toSystemReadableDate( $('#month').val());
            var curriculumEnc = curriculum.find('option:selected').data("curriculum");
            var addIncident = $('#addIncident');
            var href = addIncident.attr("href");
            href = href.split('=')[0] + "=" + curriculumEnc;
            addIncident.attr("href", href);
            var month = globalFunctions.isValueValid(value) ? value.split('-')[0] : "";
            if (month != "" && month != "All") {
                incidentDashBoard.loadIncidentLandingPageData(curriculum.val(), month);
            } else {
                incidentDashBoard.loadIncidentLandingPageData(curriculum.val(), 0);
            }
        })
        month.on('dp.change', function (date) {
            var month = globalFunctions.toSystemReadableDate($('#month').val().split('-')[0]);
            if (month != "") {
                incidentDashBoard.loadIncidentLandingPageData(curriculum.val(), month);
            } else {
                $('#month').val(translatedResources.allText)
                incidentDashBoard.loadIncidentLandingPageData(curriculum.val(), 0);
            }
        });
        month.datetimepicker({
            viewMode: 'months',
            format: 'MMM',
            showClear: true,
            useCurrent: false,
            icons: {
                clear: 'fas fa-eraser'
            },
            locale: translatedResources.locale
        }).on('dp.show', function () {
            $('th.picker-switch').parent().hide();
            if (!globalFunctions.isValueValid(globalFunctions.toSystemReadableDate(month.val())))
                month.val(translatedResources.allText);
        });
        month.val(translatedResources.allText);
        incidentDashBoard.loadIncidentLandingPageData(curriculum.val(), 0);
    })