﻿var pixie, templateId, studentId, filePath;
var doc;
var studentCertificateProcess = function () {
    var init = function () {
        $('[data-toggle="tooltip"]').tooltip({ sanitizeFn: function (content) { return content; } });
    },
        currentdate = new Date(),
        addCertificateFields = function (source, fieldName) {
            pixie.getTool('text').add(fieldName);
        },
        Index,
        Inputs,
        isEmail,
        isEmailOther,
        IsAll,
        AllObject = [],
        AllMapingObjects = [],
        studentObject,
        studentForCertificateLog = [],
        initImageEditor = function () {
            pixie = new Pixie({
                crossOrigin: true,
                ui: {
                    visible: true,
                    theme: 'light',
                    allowEditorClose: false,
                    allowZoom: true,
                    toolbar: {
                        hideSaveButton: false,
                        hide: false,
                        hideOpenButton: false,
                        hideCloseButton: true,
                    },
                },
                baseUrl: baseUri,
                onLoad: function () {
                    var activeObject = pixie.get('activeObject');
                    studentCertificateProcess.loadStudentObject(activeObject);
                    pixie.get('canvas').on('after:render', function (e) {
                        if (IsAll) {
                            if (globalFunctions.isValueValid(this._iTextInstances) && this._iTextInstances.some(x => x.text.charAt(0) == '_')) {
                                studentCertificateProcess.loadStudentObject(activeObject);
                            }
                        }
                        //var activeObject = pixie.get('activeObject');
                        //if (globalFunctions.isValueValid(this._iTextInstances) && this._iTextInstances.some(x => x.text.charAt(0) == '_')) {
                        //    studentCertificateProcess.loadStudentObject(activeObject);
                        //}
                    });
                },
                onSave: function (data, name) {
                    openFileForPrintingAndSendEmail(data, name);
                },
                image: getImagePath(),
                tools: {
                    export: {
                        defaultFormat: 'jpeg', //png, jpeg or json
                        defaultName: currentdate.getDate() +
                            + (currentdate.getMonth() + 1) +
                            + currentdate.getFullYear() +
                            + currentdate.getHours() +
                            + currentdate.getMinutes() +
                            + currentdate.getSeconds(), //default name for downloaded photo file
                        defaultQuality: 0.8, //works with jpeg only, 0 to 1
                    }
                },
                text: {
                    replaceDefault: true,
                    defaultCategory: 'handwriting',
                    items: [
                        {
                            family: 'Roboto',
                            category: 'serif',
                            filePath: '/Roboto/Roboto-Medium.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Bad Script',
                            category: 'handwriting',
                            filePath: '/Handwriting/Bad_script/BadScript-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Amatic SC',
                            category: 'handwriting',
                            filePath: '/Handwriting/Amatic_SC/AmaticSC-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Bonbon',
                            category: 'handwriting',
                            filePath: '/Handwriting/Bonbon/Bonbon-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Caveat',
                            category: 'handwriting',
                            filePath: '/Handwriting/Caveat/Caveat-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Cookie',
                            category: 'handwriting',
                            filePath: '/Handwriting/Cookie/Cookie-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Courgette',
                            category: 'handwriting',
                            filePath: '/Handwriting/Courgette/Courgette-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Great Vibes',
                            category: 'handwriting',
                            filePath: '/Handwriting/Great_Vibes/GreatVibes-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Handlee Regular',
                            category: 'handwriting',
                            filePath: '/Handwriting/Handlee/Handlee-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Indie Flower',
                            category: 'handwriting',
                            filePath: '/Handwriting/Indie_Flower/IndieFlower-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Kaushan Script',
                            category: 'handwriting',
                            filePath: '/Handwriting/Kaushan_Script/KaushanScript-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Mr Bedfort',
                            category: 'handwriting',
                            filePath: '/Handwriting/MrBedfort/Mr_Bedfort-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Norican',
                            category: 'handwriting',
                            filePath: '/Handwriting/Norican/Norican-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Pacifico',
                            category: 'handwriting',
                            filePath: '/Handwriting/Pacifico/Pacifico-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Parisienne',
                            category: 'handwriting',
                            filePath: '/Handwriting/Parisienne/Parisienne-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Permanent Marker',
                            category: 'handwriting',
                            filePath: '/Handwriting/Permanent_Marker/PermanentMarker-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Rochester',
                            category: 'handwriting',
                            filePath: '/Handwriting/Rochester/Rochester-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Rouge Script',
                            category: 'handwriting',
                            filePath: '/Handwriting/Rouge_Script/RougeScript-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Sacramento',
                            category: 'handwriting',
                            filePath: '/Handwriting/Sacramento/Sacramento-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Satisfy',
                            category: 'handwriting',
                            filePath: '/Handwriting/Satisfy/Satisfy-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Tangerine',
                            category: 'handwriting',
                            filePath: '/Handwriting/Tangerine/Tangerine-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Cormorant Garamond',
                            category: 'serif',
                            filePath: '/Serif/Cormorant_Garamond/CormorantGaramond-Medium.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Alegreya SC',
                            category: 'serif',
                            filePath: '/Serif/Alegreya_SC/AlegreyaSC-Medium.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Cambo',
                            category: 'serif',
                            filePath: '/Serif/Cambo/Cambo-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Caudex',
                            category: 'serif',
                            filePath: '/Serif/Caudex/Caudex-Italic.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Cinzel',
                            category: 'serif',
                            filePath: '/Serif/Cinzel/Cinzel-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Cinzel',
                            category: 'serif',
                            filePath: '/Serif/Cinzel/Cinzel-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Arima Madurai',
                            category: 'Display',
                            filePath: '/Display/Arima_Madurai/ArimaMadurai-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Baloo 2',
                            category: 'Display',
                            filePath: '/Display/Baloo_2/Baloo2-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Bangers',
                            category: 'Display',
                            filePath: '/Display/Bangers/Bangers-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Bebas Neue',
                            category: 'Display',
                            filePath: '/Display/Bebas_Neue/BebasNeue-Regular.ttf',
                            type: 'custom'
                        },
                        {
                            family: 'Bellota',
                            category: 'Display',
                            filePath: '/Display/Bellota/Bellota-Regular.ttf',
                            type: 'custom'
                        },
                    ]
                }
            });
        },
        printCertificate = function (element) {
            isEmail = false;
            isEmailOther = false;
            processImage(element);
        },
        emailCertificate = function (element) {
            isEmail = true;
            isEmailOther = false;
            processImage(element);
        },
        emailCertificateOther = function (element) {
            isEmail = true;
            isEmailOther = true;
            processImage(element);
        },
        processImage = function (element) {
            if (isEmail || isEmailOther) {
                var student = $(element).closest('div[data-student]').data('student');

                students = [];
                students.push(student);

                //to send email notification to the staff
                sendEmailToStudentAndTeachers(students);
            } else {
                studentObject = $(element).closest('div[data-student]').data('student');
                templateId = studentObject.CertificateId;
                studentId = studentObject.StudentID;
                filePath = `${imagePath}${studentObject.FilePath}`;
                if (pixie) {
                    pixie.resetEditor('image', filePath);
                    if (!IsAll) {
                        setTimeout(() => {
                            var activeObject = pixie.get('activeObject');
                            loadStudentObject(activeObject);
                        }, 800);
                    }
                } else {
                    initImageEditor();
                }
            }
        },
        processAllImages = function () {
            var $inputs = getVisibleCheckBoxes(true);
            templateId = $inputs.data("certificateid");
            studentId = [];
            $inputs.each(function (i, e) {
                studentId.push($(e).data("sdid"));
            });
            if (isEmail || isEmailOther) {
                students = [];
                $inputs.each(function (i, e) {
                    students.push($(this).closest("div.student-card").data('student'));
                });

                //to send email notification to the staff
                sendEmailToStudentAndTeachers(students);
            } else {
                if ($inputs.length > 0) {
                    common.postRequest('/Behaviour/Behaviour/GetCertificateFieldData', { templateId: templateId, studentId: studentId })
                        .then(response => {
                            studentCertificateProcess.AllMapingObjects = response;
                            Inputs = $inputs;
                            Index = 0
                            $('.loader-wrapper').fadeIn(500, function () { });
                            processImage(Inputs[Index]);
                        })
                        .catch(error => console.log(error));
                } else {
                    globalFunctions.showWarningMessage(translatedResources.pleaseSelectStudent)
                }
            }
        },
        loadStudentObject = function (activeObject) {
            if (IsAll) {
                var labelList = studentCertificateProcess.AllMapingObjects.filter(x => x.UserId == studentObject.StudentID);
                labelList.forEach(res => {
                    if (globalFunctions.isValueValid(activeObject.canvasState.fabric._iTextInstances)) {
                        if (activeObject.canvasState.fabric._iTextInstances.some(x => x.text == res.PlaceHolder)) {
                            var object = activeObject.canvasState.fabric._iTextInstances.find(x => x.text == res.PlaceHolder)
                            object.text = res.ColumnData;
                        }
                    }
                });
                $('.export-button.mat-button.mat-button-base').click();
            } else {
                $.post('/Behaviour/Behaviour/GetCertificateFieldData', { templateId: templateId, studentId: [studentId] }).then(response => {
                    response.forEach(res => {
                        if (globalFunctions.isValueValid(activeObject.canvasState.fabric._iTextInstances)) {
                            if (activeObject.canvasState.fabric._iTextInstances.some(x => x.text == res.PlaceHolder)) {
                                var object = activeObject.canvasState.fabric._iTextInstances.find(x => x.text == res.PlaceHolder)
                                object.text = res.ColumnData;
                            }
                        }
                    });
                    $('.export-button.mat-button.mat-button-base').click();
                });
            }
        },
        loadStudentObjectList = function (studentId) {
            return new Promise(function (resolve, reject) {
                common.postRequest('/Behaviour/Behaviour/GetCertificateFieldData', { templateId: templateId, studentId: studentId })
                    .then(response => {
                        studentCertificateProcess.AllMapingObjects = response;
                        resolve(response);
                    })
                    .catch(error => reject(error));
            });
        },
        openFileForPrintingAndSendEmail = function (data, name) {
            var imgData = data;
            name = studentObject.StudentName + name;
            var activeObject = pixie.get('activeObject');
            if (!globalFunctions.isValueValid(doc)) {
                if (activeObject.canvasState.fabric.width > activeObject.canvasState.fabric.height) {
                    doc = new jsPDF("p", "mm", "a4");
                } else {
                    doc = new jsPDF("l", "mm", "a4");
                }
            }
            var width = doc.internal.pageSize.getWidth();
            var height = doc.internal.pageSize.getHeight();
            doc.setProperties({
                title: name,
                subject: 'Certificate'
            });
            doc.setFontSize(40);
            doc.addImage(imgData, 'JPEG', 0, 0, width, height);

            if (isEmail) {
                var blob = doc.output();
                studentObject.IsEmailOther = isEmailOther;
                studentObject.FileArray = btoa(blob);
                //studentObject.StudentEmail = 'dubeyashwin95@gmail.com';
                //studentObject.OtherEmail = 'dubeyashwin95@gmail.com,dubeyashwin95@gmail.com';
                if (IsAll) {
                    AllObject.push(studentObject);
                } else {
                    sendEmail([studentObject]);
                }
            } else {
                if (IsAll) {
                    AllObject.push({ imgData, name });
                    studentForCertificateLog.push(studentObject.StudentID)
                } else {
                    doc.autoPrint();
                    doc.output('dataurlnewwindow');
                    certificateProcess = {
                        StudentId: studentObject.StudentID,
                        TemplateId: studentObject.CertificateScheduleId
                    };
                    sendCertificateProcessLog([certificateProcess]);
                }
            }
            if (IsAll) {
                Index++;
                if (Inputs.length > Index)
                    processImage(Inputs[Index]);
                else
                    printAllTheDocument(AllObject, isEmail);
            }
        },
        getImagePath = function () {
            return filePath;
        },
        checkAll = function () {
            var $inputs = getVisibleCheckBoxes();
            $inputs.prop('checked', !($inputs.prop('checked')));
            $inputs.closest("div.student-card")[$inputs.prop('checked') ? "addClass" : "removeClass"]("selected");
        },
        printAll = function () {
            IsAll = true;
            isEmail = false;
            isEmailOther = false;
            processAllImages();
        },
        emailAll = function () {
            IsAll = true;
            isEmail = true;
            isEmailOther = false;
            processAllImages();
        },
        emailOtherAll = function () {
            IsAll = true;
            isEmail = true;
            isEmailOther = true;
            processAllImages();
        },
        sendEmail = function (student) {
            var token = $('input[name="__RequestVerificationToken"]').val();
            var scheduleDate = $('#ScheduleDate').val();
            var isNotification = $('#IsNotification').val();
            var sectionIds = $('#ddlSection').val() ? $('#ddlSection').val().join(',') : '';
            if (isEmailOther) {
                $.post('/Behaviour/Behaviour/SendEmail', {
                    __RequestVerificationToken: token,
                    student: student,
                    sectionIds: sectionIds,
                    date: scheduleDate || null,
                    isNotification: isNotification
                }).then(function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        $('#ddlSection').trigger('change')
                    }
                    if (isNotification) {
                        $('[data-toggle="tooltip"]').tooltip('dispose').tooltip({ sanitizeFn: function (content) { return content; } });
                        studentCertificateProcess.getVisibleCheckBoxes().closest('div.tab-pane').html(data.RelatedHtml);
                    }
                })
            } else if (student.length > 1) {
                var count = Math.floor(student.length / 10);
                for (var i = 0; i < count; i++) {
                    $.post('/Behaviour/Behaviour/SendEmail',
                        {
                            __RequestVerificationToken: token,
                            student: student.splice((i * 10), (i * 10) + 10),
                            sectionIds: sectionIds,
                            date: scheduleDate || null
                        }).then(function (data) {
                            //globalFunctions.showMessages(data);
                        })
                }
                $.post('/Behaviour/Behaviour/SendEmail', {
                    __RequestVerificationToken: token,
                    student: student.splice((i * 10), (i * 10) + 10),
                    sectionIds: sectionIds,
                    date: scheduleDate || null
                }).then(function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        $('#ddlSection').trigger('change')
                    }
                    //$('[data-toggle="tooltip"]').tooltip('dispose').tooltip({ sanitizeFn: function (content) { return content; } });
                    //studentCertificateProcess.getVisibleCheckBoxes().closest('div.tab-pane').html(data.RelatedHtml);
                })
            } else {
                $.post('/Behaviour/Behaviour/SendEmail', {
                    __RequestVerificationToken: token,
                    student: student.splice((i * 10), (i * 10) + 10),
                    sectionIds: sectionIds,
                    date: scheduleDate || null
                }).then(function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        $('#ddlSection').trigger('change')
                    }
                    //$('[data-toggle="tooltip"]').tooltip('dispose').tooltip({ sanitizeFn: function (content) { return content; } });
                    //studentCertificateProcess.getVisibleCheckBoxes().closest('div.tab-pane').html(data.RelatedHtml);
                })
            }
        },
        printAllTheDocument = function (imageObject, isEmail) {
            if (isEmail) {
                sendEmail(imageObject);
                resetEveryField();
            } else {
                Index = 0;
                imageObject.forEach(x => {
                    studentCertificateProcess.getImageFromUrl(x.imgData)
                });
            }
        },
        getImageFromUrl = function (url) {
            var img = new Image();
            img.onError = function () {
                alert('Cannot load image: "' + url + '"');
            };
            img.onload = function () {
                var imgData = img;
                var activeObject = pixie.get('activeObject');
                if (!globalFunctions.isValueValid(doc)) {
                    if (activeObject.canvasState.fabric.width > activeObject.canvasState.fabric.height) {
                        doc = new jsPDF("p", "mm", "a4");
                    } else {
                        doc = new jsPDF("l", "mm", "a4");
                    }
                }
                var width = doc.internal.pageSize.getWidth();
                var height = doc.internal.pageSize.getHeight();
                if (Index != 0)
                    doc.addPage();
                doc.addImage(imgData, 'JPEG', 0, 0, width, height);
                if (AllObject.length - 1 == Index) {
                    //doc.deletePage(1)
                    //doc.save(studentObject.CetificateDescription);
                    doc.autoPrint();
                    doc.output('dataurlnewwindow');
                    var certificateProcess = [];
                    studentForCertificateLog.forEach(x => {
                        certificateProcess.push({ StudentId: x, TemplateId: studentObject.CertificateScheduleId })
                    })
                    sendCertificateProcessLog(certificateProcess);
                    resetEveryField();
                }
                Index++;
            };
            img.src = url;
        },
        resetEveryField = function () {
            AllObject = [];
            Index = 0;
            IsAll = false;
            isEmail = false;
            isEmailOther = false;
            Inputs = [];
            doc = undefined;
            studentForCertificateLog = [];
            $('.loader-wrapper').fadeOut(500, function () { });
        },
        sendCertificateProcessLog = function (certificateProcessLog) {
            var token = $('input[name="__RequestVerificationToken"]').val();
            common.postRequest('/Behaviour/Behaviour/CertificateProcessLogCU', {
                __RequestVerificationToken: token,
                processLogs: certificateProcessLog
            })
                .then(response => {
                    //globalFunctions.showMessage(response.NotificationType, response.Message);
                    //if (response.Success) {
                    //    $('.certificateTab').trigger('click');
                    //}
                    //$.get('/Behaviour/Behaviour/GetCertificateStudentCards', {
                    //    scheduleType: $('.certificateTab.active').data('schedule'),
                    //    certificateScheduleType: $('.certificateTab.active').attr('id')
                    //}).then(response => {
                    //    $(`#card_${$('.certificateTab.active').attr('id')}`).html(response);
                    //});
                    resetEveryField();
                })
                .catch(error => globalFunctions.onFailure());
        },
        printDocument = function (data) {
            $.post('/Behaviour/Behaviour/PrintCertificates', { imageString: [data] }).then(x => {
                console.log(x);
            })
        },
        getVisibleCheckBoxes = function (isChecked) {
            var $inputs;
            if (isChecked) {
                $inputs = $(`.clsstudetailsblock input[id*='CheckBox-']:visible:checked`);
            } else {
                $inputs = $(`.clsstudetailsblock input[id*='CheckBox-']:visible`);
            }
            return $inputs;
        },
        sendEmailToStudentAndTeachers = function (student) {
            var token = $('input[name="__RequestVerificationToken"]').val();
            var scheduleDate = $('#ScheduleDate').val();
            var isNotification = $('#IsNotification').val();
            var sectionIds = $('#ddlSection').val() ? $('#ddlSection').val().join(',') : '';
            $.post('/Behaviour/Behaviour/SendEmail', {
                __RequestVerificationToken: token,
                student: student,
                sectionIds: sectionIds,
                date: scheduleDate || null,
                isNotification: isNotification
            }).then(function (data) {
                globalFunctions.showMessage(data.NotificationType, data.Message);
                if (data.Success) {
                    $('#ddlSection').trigger('change')
                }
                if (isNotification) {
                    $('[data-toggle="tooltip"]').tooltip('dispose').tooltip({ sanitizeFn: function (content) { return content; } });
                    studentCertificateProcess.getVisibleCheckBoxes().closest('div.tab-pane').html(data.RelatedHtml);
                }
            })
        }
    return {
        init: init,
        Index: Index,
        Inputs: Inputs,
        isEmail: isEmail,
        isEmailOther: isEmailOther,
        isAll: IsAll,
        AllObject: AllObject,
        AllMapingObjects: AllMapingObjects,
        studentObject: studentObject,
        studentForCertificateLog: studentForCertificateLog,
        addCertificateFields: addCertificateFields,
        initImageEditor: initImageEditor,
        printCertificate: printCertificate,
        loadStudentObject: loadStudentObject,
        getImagePath: getImagePath,
        openFileForPrintingAndSendEmail: openFileForPrintingAndSendEmail,
        emailCertificate: emailCertificate,
        emailCertificateOther: emailCertificateOther,
        processImage: processImage,
        processAllImages: processAllImages,
        checkAll: checkAll,
        printAll: printAll,
        emailAll: emailAll,
        emailOtherAll: emailOtherAll,
        sendEmail: sendEmail,
        loadStudentObjectList: loadStudentObjectList,
        printAllTheDocument: printAllTheDocument,
        getImageFromUrl: getImageFromUrl,
        resetEveryField: resetEveryField,
        sendCertificateProcessLog: sendCertificateProcessLog,
        printDocument: printDocument,
        getVisibleCheckBoxes: getVisibleCheckBoxes,
        sendEmailToStudentAndTeachers: sendEmailToStudentAndTeachers
    };
}();

$(document).on('change', '#CurriculumId', function () {
    common.bindSingleDropDownByParameterAsync('#ddlGrade', '/Behaviour/Behaviour/GetGradeWhenCurriculumChange', { curriculumId: $(this).val() }, undefined, undefined, 'ItemId', 'ItemName', true);
}).on('change', '#ddlGrade', function () {
    var curriculum = $('#CurriculumId').val();
    var gradeId = $(this).val().join('#');
    var object = {
        filterCode: 'SCT_M',
        filterParameter: `CurriculumId:${curriculum}|GRD_M:${gradeId}`
    };
    common.bindSingleDropDownByParameter('#ddlSection', '/SchoolInfo/SchoolReport/GetReportFilter', object, undefined, undefined, undefined, "value", "Name", true)
    var dropdown = $('#ddlSection');
    dropdown.val(dropdown.find('option:eq(0)').val()).trigger('change')
}).on('change', '#ddlSection', function () {
    var schedule = $('ul.certificateCategory li a.active').attr("data-certificatetype");
    //var schedule = $('ul.certificateCategory li.active').attr("data-certificatetype");
    var currId = $("#CurriculumId").val();
    var object = {
        section : $(this).val().join(','),
        scheduleType : schedule || 0,
        currId : currId
    }
    $.get('/Behaviour/Behaviour/GetStudentListBySchedule', object)
        .then(response => {
            $('#maintab_' + schedule).html(response).find('a.active[data-toggle="pill"]').trigger('shown.bs.tab');
            //$('#pdfElement').html(response);
            $('[data-toggle="tooltip"]').tooltip('dispose').tooltip({ sanitizeFn: function (content) { return content; } });
        })
}).on('click', '.certificateTab', function () {
    $('#ddlSection').trigger('change');
})
   .on('click', '.certificateStudentCheck', function () {
       var check = $(this);
       if (check.is('a.certificateStudentCheck')) {
           check = check.closest("div.student-card").find('input[id^="CheckBox-"]');
           check.prop('checked', !check.prop('checked'));
       }
       check.closest("div.student-card")[check.prop('checked') ? "addClass" : "removeClass"]("selected");
   }).on('click', '.ScheduleCertificate', function () {
       var targetDiv = $(this).attr('href');

       $('.ScheduleCertificate').removeClass('active');       
       $(targetDiv).closest('div.tab-pane.fade').removeClass('show active');
       
       $(this).addClass('active');
       $(targetDiv).addClass('show active');

   }).ready(function () {
    var dropdown = $('#ddlGrade');
    dropdown.val(dropdown.find('option:eq(1)').val()).trigger('change')
})

$(document).ajaxStart(function () {
    //debugger;
    //$('.loader-wrapper').fadeIn(500, function () {
    //});
    $('#AjaxLoader').fadeIn(500, function () { });
})
    .ajaxStop(function () {
        //$('.loader-wrapper').fadeOut(500, function () {
        //});

        $('#AjaxLoader').fadeOut(500, function () { });
        // setInterval(function () { $('#AjaxLoader').fadeOut(500, function () { }); }, 500);
    })
    .ajaxError(function (x, xhr, error) {
        globalFunctions.onFailure(x, xhr, error);
    });