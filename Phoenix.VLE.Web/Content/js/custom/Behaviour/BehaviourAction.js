﻿var BehaviourAction = function () {
    var Init = () => {
        $('.date-picker').datetimepicker({
            format: 'DD-MMM-YYYY',
             extraFormats: ['DD-MM-YYYY', 'MM-DD-YYYY']
       });
        
        var dt = new Date();
        var cDate = ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear();


        $('body').on('click', '.add-option-name', function () {
            $('.otherOptName').slideToggle();
        });
        $(".student-allocation-wrapper").mCustomScrollbar({
            setHeight: "250",
            autoExpandScrollbar: true,
        });
        $(".student-group-settings .content").mCustomScrollbar({
            //setHeight: "280",
            autoExpandScrollbar: true,
            scrollbarPosition: "outside",
            autoHideScrollbar: false
        });
        $(".custom-scroll-container").mCustomScrollbar({
            //setHeight: "76%",
            autoExpandScrollbar: true,
            scrollbarPosition: "inside",
            autoHideScrollbar: true,
        });

        for (var i = 0; i < $('input[type=radio]').length; i++) {
            var target = $('input[type=radio]')[i];
            var ids = $(target).attr('data-txtId');
            if (ids != '') {
                if ($("input[name='" + $(target).attr('name') + "']:checked").val() == "True") {
                    $("input[id^='" + ids + "'],textarea[id^='" + ids + "']").removeAttr('disabled')
                } else {
                    $("input[id^='" + ids + "'],textarea[id^='" + ids + "']").attr('disabled', 'disabled')
                }
            }
        }
    },
        GetBehaviourAction = (Incident, StudentId) => {
            $.get('/Behaviour/Incident/GetBehaviourAction', { IncidentId: Incident, StudentId: StudentId })
                .done((response) => {
                    $('#partialAddEditAction').html(response);
                    BehaviourAction.showFollowUpForm();
                    $('.actionDrawer').toggleClass('open');
                    $('.actionDrawer').animate({
                        right: '0'
                    });
                    $('.selectpicker').selectpicker('refresh');
                    BehaviourAction.Init();
                })
        },
        onFailure = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },
        onSaveSuccess = function (data) {
            globalFunctions.showSuccessMessage(translatedResources.updateSuccessMsg);;
        },
        showSuccessAndGetFollowUp = () => {
            var incidentId = $('#IncidentId').val();
            var actionId = $('#FollowUpActionId').val();
            if (incidentId && actionId) {
                $.get('/Behaviour/Incident/GetBehaviourActionFollowups', { incidentId, actionId }, (response) => {
                    $('#followUpDiv').html(response);
                    $('.selectpicker').selectpicker('refresh');
                })
            }
        },
        showFollowUpForm = () => {
            var incidentId = $('#IncidentId').val();
            var StudentId = $('#StudentId').val();

            if (incidentId && StudentId) {
                $.get('/Behaviour/Incident/GetBehaviourActionFollowupForm', { incidentId, StudentId }, (response) => {
                    $('#followUpForm').html(response);
                    $('.selectpicker').selectpicker('refresh');
                    BehaviourAction.showSuccessAndGetFollowUp();
                })
            }
        },
        onActionSaveSuccess = (response) => {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success) {
                $(`a[data-studentid="${response.InsertedRowId}"]`).remove();
            }
            //BehaviourAction.showFollowUpForm();
        },
    getFollowUpStaffList = designationId => {
        $("#ddlStaffList").empty();
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: '/Behaviour/Incident/GetFollowUpStaffList',
            contentType: 'application/json;charset=utf-8',
            data: { designationId: designationId },
            success: function (response) {
                console.log(response);
                var div_data = "<option value=''>select</option>";
                $(div_data).appendTo('#ddlStaffList');
                for (var i = 0; i < response.length; i++) {
                    div_data = "<option value=" + response[i].EMPLOYEE_ID + ">" + response[i].EMPLOYEE_NAME + "</option>";
                    $(div_data).appendTo('#ddlStaffList');
                }
                $('.selectpicker').selectpicker('refresh');
            },
            error: function (ex) {
                alert('Failed.' + ex);
            }
        });
    },
        GetFollowUp = (incidentId, actionId) => {
            if (incidentId && actionId) {
                $.get('/Behaviour/Incident/GetBehaviourActionFollowups', { incidentId, actionId }, (response) => {
                    $('#divSelectedBehaviour').html(response);
                    $('.selectpicker').selectpicker('refresh');
                    $('.selected.modal').modal('show');
                })
            }
        }
    return {
        Init: Init,
        GetBehaviourAction: GetBehaviourAction,
        onFailure: onFailure,
        onSaveSuccess: onSaveSuccess,
        getFollowUpStaffList: getFollowUpStaffList,
        showSuccessAndGetFollowUp: showSuccessAndGetFollowUp,
        showFollowUpForm: showFollowUpForm,
        onActionSaveSuccess: onActionSaveSuccess,
        GetFollowUp: GetFollowUp
    }
}();

$(document)
    .ready(function () {
    })
    .on('change', 'input[type=radio]', function () {
        var ids = $(this).attr('data-txtId');
        if (globalFunctions.isValueValid(ids)) {
            if ($("input[name='" + $(this).attr('name') + "']:checked").val() == "True") {
                $("input[id^='" + ids + "'],textarea[id^='" + ids + "']").removeAttr('disabled')
            } else {
                $("input[id^='" + ids + "'],textarea[id^='" + ids + "']").attr('disabled', 'disabled').val('');
            }
        }
    });