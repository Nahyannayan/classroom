﻿var aldid = "";
var Globalactivityid = "";
function RequestTOEnroll(alD_ID, EventCost, allowMultiple, EnrolledSiblingStudentIDs,
    onlinePaymentAllowed, paymentTypeID, eventCost, feeTypeId, eventName, paymentProcessingCharge, apdId, feeCollType, isTermsExist) {    
    if (isTermsExist == "true") {
        $(".collapseCommentDiv").show();
        $(".collapseCommentMsg").html($(".AcctivityComment_" + alD_ID).text());
        $(".SendRequestbtn").attr("disabled", true);
        $('#termsAndConditionChkb').attr("checked", false);
    }
    else {
        if (!$('#termsAndConditionChkb').is("checked")) {
            $('#termsAndConditionChkb').attr("checked", true);
        }
        $('#termsAndConditionChkb').attr("checked", true);
        $(".collapseCommentDiv").hide();
        $(".collapseCommentMsg").html('');
    }

    $("#SelectedAlD_ID").val(alD_ID);
    $("#SelectedAllowMultiple").val(allowMultiple);
    $("#SelectedEnrolledSiblingStudentIDs").val(EnrolledSiblingStudentIDs);
    $("#SelectedOnlinePaymentAllowed").val(onlinePaymentAllowed);
    $("#SelectedPaymentTypeID").val(paymentTypeID);
    $("#SelectedEventCost").val(eventCost);
    $("#SelectedFeeTypeId").val(feeTypeId);
    $("#SelectedEventName").val(eventName);
    $("#SelectedPaymentProcessingCharge").val(paymentProcessingCharge);
    $("#SelectedApdId").val(apdId);
    $("#SelectedFeeCollType").val(feeCollType);
    var stu_no = $('#StudentNumber').val();
    $.ajax({
        url: '/ParentCorner/EnrollActivities/RequestToEnroll',
        type: "GET",
        data: { aldId: alD_ID, Studentnumber: stu_no },
        success: function (response) {
            if (response != null) {
                $('#AcademicYear').html(response.academicYear);
                $('#EventName').text(response.eventName);
                $('#EventDates').text(response.eventDate);
                $('#Grade').text(response.grades);
                $('#RegisterBy').text(response.requestBy);
                $('#activityidhide').val(response.aldId);
                var Isscheduleactive = response.isScheduleAvailable;
                if (Isscheduleactive == false) {
                    $('.Eventschedule').hide();
                }
                else {
                    $('.Eventschedule').show();
                }
                $("#StudentListBind").empty();
                if (response.siblings.length > 1) {
                    $.each(response.siblings, function (i, data) {
                        if (data.studentNo == stu_no) {
                            $("#StudentListBind").append("<option value='" + data.studentNo + "' selected>" + data.studentName + "</option>")
                        }
                        else {
                            $("#StudentListBind").append("<option  value='" + data.studentNo + "'>" + data.studentName + "</option>")
                        }
                    });
                }
                else {
                    $("#StudentListBind").append("<option value='" + response.siblings[0].studentNo + "' selected>" + response.siblings[0].studentName + "</option>")
                }
                $("#BindRadioButton").empty();                
                $.each(response.schedules, function (key, value) {
                    if (key == 0) {
                        $("#BindRadioButton").append(`<li class='active' ><div class="custom-control custom-radio"><input type='radio' class='custom-control-input' onChange='eventScheduleChkBox()' id= '${value.scheduleID}' name='radiogroup' value='${value.description}' checked/>  <label class='custom-control-label' for='${value.scheduleID}'>${value.description}</label></div></li> `);
                    }
                    else {
                        $("#BindRadioButton").append(`<li class=''><div class="custom-control custom-radio"><input type='radio' class='custom-control-input' onChange='eventScheduleChkBox()' id= '${value.scheduleID}' name='radiogroup' value='${value.description}'/>  <label class='custom-control-label' for='${value.scheduleID}'>${value.description}</label></div></li> `);
                    }
                    
                });

                $("#CouponValue").empty();
                if (allowMultiple == 'false' || allowMultiple == 'False') {
                    $("#CouponValue").append("<option class='text-semibold text-dark' value='" + EventCost + "'selected>1 x " + EventCost + translatedResources.AED+" </option>")
                    $('.Coupenddl').hide()
                }
                else {
                    $('.Coupenddl').show();
                    $(".SendRequestandPayOnlinebtn").attr("disabled", true);
                    $(".SendRequestbtn").attr("disabled", true);
                    var coupenValue = "<option class='text-semibold text-dark'  value='" + 0 + "'selected>" + translatedResources.selectOption+"</option >";
                    for (var no = 1; no <= response.maximumEligibleEventCount; no++) {
                        coupenValue = coupenValue + "<option class='text-semibold text-dark'  value = '" + EventCost + "'>" + no + " x " + EventCost + translatedResources.AED+"</option >"
                    }
                    $("#CouponValue").append(coupenValue);
                }
                if (allowMultiple == 'false' || allowMultiple == 'False') {
                    $("#CouponValue").selectpicker('refresh');
                    $('#TotalAmount').text('');
                }
                if (response.allowComments) {
                    $(".allowCommentsActivity").show();
                }
                else {
                    $(".allowCommentsActivity").hide();
                }
                $("#enrollmentForm").modal("show");
                $("#StudentListBind").selectpicker('refresh');
                $("#CouponValue").selectpicker('refresh');
                ValidateSendRequestButton();
            }
        }
    });

}


function ValidateSendRequestButton() {
    var isTermsAndConditionChk = $('#termsAndConditionChkb').is(":checked");
    $('#TotalAmount').text('');
    var IndexCount = $("#CouponValue option:selected").index() == 0 ? 1 : $("#CouponValue option:selected").index();
    var TotalStudent = $("#StudentListBind").val();
    var LengthOfStudent = TotalStudent.length;
    var SelectedCouponval = $('#CouponValue :Selected').val();
    var AmountOfCouopen = LengthOfStudent * IndexCount * SelectedCouponval;
    $('#TotalAmount').text(AmountOfCouopen);
    if (isTermsAndConditionChk) {
        if (parseFloat(AmountOfCouopen) > 0) {
            if ($('#CouponValue :selected').text() != "Select" && $("#StudentListBind").val().length > 0) {
                $(".SendRequestandPayOnlinebtn").attr("disabled", false);
            }
            if ($('#CouponValue :selected').text() == "Select" && $("#StudentListBind").val().length == 0) {
                $(".SendRequestandPayOnlinebtn").attr("disabled", true);
            }
            $(".SendRequestbtn").attr("disabled", false);
        }
        else {            
            if ($("#StudentListBind").val().length > 0 && $('#CouponValue :selected').text() != "Select") {
                $(".SendRequestbtn").attr("disabled", false);
            }
            if ($("#StudentListBind").val().length == 0 || $('#CouponValue :selected').text() == "Select") {
                $(".SendRequestbtn").attr("disabled", true);
            }
            $(".SendRequestandPayOnlinebtn").attr("disabled", true);
        }

    }
    else {
        $(".SendRequestbtn").attr("disabled", true);
        $(".SendRequestandPayOnlinebtn").attr("disabled", true);
    }
}


$('#StudentListBind').change(function () {
    ValidateSendRequestButton();
})


function UnsubscribeEvent(EventName) {
    $('#AddEventname').val(EventName);
}
$('#SelectMonthFilter').change(function () {
    var SelctedMonthId = $('#SelectMonthFilter :Selected').val();
    var stu_no = $('#StudentNumber').val();
    $.ajax({
        url: '/ParentCorner/EnrollActivities/SelectMonthFilter',
        type: "GET",
        data: { NumberOfMonths: SelctedMonthId, StudentNumber: stu_no },
        success: function (response) {
            if (response != null) {
                $('#divEnrolledActivity').html('');
                $('#divEnrolledActivity').html(response);
                var str = $('#EnrollActivityCount').val();
                if (str === undefined) {
                    $('#EnrollActivitId').html('<span> ' + translatedResources.ShowingReports +'</span> ');
                }
                else {
                    $('#PdfNoOfList').html(' <p class="mb-0 mr-3"><span class="text-bold pl-2">' + str + '  ' + translatedResources.Activities + '</span> ' + translatedResources.EnrolledIn+'</p>');
                }

            }
        }
    });
})
function Sendfeedbackpost(i, alid) {
    $('#Ratingcount').val(i);
    var a = i;
    var b = 5 - a;
    for (var y = 1; y <= a; y++) {
        $('#Star' + y + '_' + alid).html('<i class="fas fa-star "></i>');
    }
    for (var x = 1; x <= b; x++) {
        var m = a + x;
        $('#Star' + m + '_' + alid).html('<i class="far fa-star"></i>');
    }
    aldid = '';
    aldid = '#btn_send_' + alid;
}
function SubmitFeedback() {
    var StarRating = $('#Ratingcount').val();
    var ActivityId = $('#activityidforstar').val();
    $.ajax({
        url: '/ParentCorner/EnrollActivities/ActivityFeedback',
        type: "GET",
        data: { StarRating: StarRating, ActivityId: ActivityId },
        success: function (response) {
            if (response != null) {
                if (response == true) {

                    $(aldid).attr("disabled", true);
                    for (var x = 1; x < 6; x++) {

                        $('#Star' + x + '_' + ActivityId).addClass("disabled");
                    }
                    globalFunctions.showSuccessMessage(translatedResources.SendFeedback)
                }
                else {
                    alert(translatedResources.TechnicalError );
                }

            }
        }
    });
}
function UnsubscribeEventName(EventName, activityId) {
    Globalactivityid = '';
    Globalactivityid = activityId;
    $('#AddEventname').html('<h4 class="text-semibold my-4 text-center dark-purple">'+translatedResources.UnsubscribeFrom+' ‘' + EventName + '’?</h4>')
    $('#unsubscribePopup').modal('show');

}

function UnsubscribeBtnDisplay() {
    Globalactivityid = '';
    Globalactivityid = activityId;
    $('#AddEventname').html('<h4 class="text-semibold my-4 text-center dark-purple">' + translatedResources.UnsubscribeFrom + ' ‘' + EventName + '’?</h4>')
    $('#unsubscribePopup').modal('show');

}
function UnsubscribeRequestSend() {
    var ACDID = Globalactivityid;
    $.ajax({
        url: '/ParentCorner/EnrollActivities/UnsubscribeActivity',
        type: "GET",
        data: { APD_ID: ACDID },
        success: function (result) {
            if (result == true) {
                $('#unsubscribePopup').modal('hide');
                globalFunctions.showMessage("success", translatedResources.ActivityUnsubscribed);
                window.location.reload();
            }
            else {
                globalFunctions.showMessage("error", translatedResources.TechnicalError );
            }
        }
    });
}
$("#btnConfirmProceed").on("click", function () {
    var url = $("#GetPayUrl").val();
    if (url) {
        location.href = url;
    } else {
        return;
    }
});


function PayOnline(EnrolledSiblingStudentIDs, onlinePaymentAllowed, paymentTypeID, eventCost, feeTypeId, eventName, paymentProcessingCharge, apdId, feeCollType, aldId) {
    ConfirmPopupDontClose();
    var model = {
        Id: $('input[name="radiogroup"]:checked').attr('id'),
        StudentNumber: EnrolledSiblingStudentIDs.replace(',', '|'),
        OnlinePaymentAllowed: onlinePaymentAllowed,
        PaymentTypeID: paymentTypeID,
        EventCost: eventCost,
        FeeTypeId: feeTypeId,
        EventName: eventName,
        PaymentProcessingCharge: paymentProcessingCharge,
        ApdId: apdId,
        FeeCollType: feeCollType,
        Radioselect: $('input[name="radiogroup"]:checked').val(),
        Enrolledstudents: $("#StudentListBind").val(),
        AlD_ID: aldId,
        Couponcount: $("#CouponValue option:selected").index(),
        EnrolledSiblingStudentIDs: EnrolledSiblingStudentIDs,
        Comment: "",
        IsTermsExist: $('#termsAndConditionChkb').is(":checked"),
        EnrollmentRequestType: 2
    }

    ProcessEnrollRequest(model);
}
function SendRequestandPayOnline() {
    var ScheduleSelect = $('input[name="radiogroup"]:checked').val();
    if (ScheduleSelect == undefined) {
        ScheduleSelect = "";
    }
    var ScheduleId = $('input[name="radiogroup"]:checked').attr('id');
    if (ScheduleId == undefined) {
        ScheduleId = 0;
    }
    var model = {
        Id: ScheduleId,
        StudentNumber: $("#StudentListBind").val().join("|"),
        OnlinePaymentAllowed: $("#SelectedOnlinePaymentAllowed").val(),
        PaymentTypeID: $("#SelectedPaymentTypeID").val(),
        EventCost: parseFloat($('#TotalAmount').text()),
        FeeTypeId: $("#SelectedFeeTypeId").val(),
        EventName: $("#SelectedEventName").val(),
        PaymentProcessingCharge: $("#SelectedPaymentProcessingCharge").val(),
        ApdId: $("#SelectedApdId").val(),
        FeeCollType: $("#SelectedFeeCollType").val(),
        Radioselect: ScheduleSelect,
        Enrolledstudents: $("#StudentListBind").val(),
        AlD_ID: $('#activityidhide').val(),
        Couponcount: $("#CouponValue option:selected").index(),
        EnrolledSiblingStudentIDs: $("#SelectedEnrolledSiblingStudentIDs").val(),
        Comment: $("#txtarea").val(),
        IsTermsExist: $('#termsAndConditionChkb').is(":checked"),
        EnrollmentRequestType: 3
    }

    ProcessEnrollRequest(model);
}
// Send activity request and pay online payment.

function ProcessEnrollRequest(ProcessEnrollRequestModel) {
    $("#GetPayUrl").val('');
    var NetPayable = parseFloat(ProcessEnrollRequestModel.EventCost);
    $.ajax({
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(ProcessEnrollRequestModel),
        url: '/ParentCorner/EnrollActivities/GetRequestAndPayOnlineDetails',
        success: function (result) {
            debugger
            var data = JSON.parse(result.result), msg = "", succ = "";
            if (data.Response != null) {
                succ = data.Response.success.toLowerCase(); msg = data.Response.message;
                if (succ == "true") {
                    //If request is not for payment
                    if (ProcessEnrollRequestModel.EnrollmentRequestType != 1) {
                        $("#GetPayUrl").val('');
                        $("#GetPayUrl").val(data.PaymentRedirectURL);
                        $("#confirmPopup").find(".totAmnt").html(translatedResources.AED + NetPayable);
                        $("#confirmPopup").find(".refNo").html(translatedResources.no + data.PaymentRefNo);
                        $("#enrollmentForm").modal('hide');
                        $("#confirmPopup").modal({
                            show: true,
                            keyboard: false,
                            backdrop: 'static'
                        });
                        //$("#btnConfirmProceed").attr("disabled", true);
                    }

                } else {
                    globalFunctions.showErrorMessage(msg);
                }
            }
            else {
                globalFunctions.showErrorMessage(translatedResources.TechnicalError );
            }
        },
        error: function (msg) {
            globalFunctions.onFailure();
        }
    });

}

function SendRequest() {

    var ScheduleSelect = $('input[name="radiogroup"]:checked').val();
    if (ScheduleSelect == undefined) {
        ScheduleSelect = "";
    }
    var ScheduleId = $('input[name="radiogroup"]:checked').attr('id');
    if (ScheduleId == undefined) {
        ScheduleId = 0;
    }
    var model = {
        Id: ScheduleId,
        StudentNumber: $("#StudentListBind").val().join("|"),
        OnlinePaymentAllowed: false,
        PaymentTypeID: 0,
        EventCost: parseFloat($('#TotalAmount').text()),
        FeeTypeId: 0,
        EventName: "",
        PaymentProcessingCharge: 0,
        ApdId: 0,
        FeeCollType: "",
        Radioselect: ScheduleSelect,
        Enrolledstudents: 0,
        AlD_ID: $('#activityidhide').val(),
        Couponcount: $("#CouponValue option:selected").index(),
        EnrolledSiblingStudentIDs: "",
        Comment: $("#txtarea").val(),
        IsTermsExist: $('#termsAndConditionChkb').is(":checked"),
        EnrollmentRequestType: 1
    }


    $.ajax({
        contentType: "application/json",
        type: "POST",
        url: '/ParentCorner/EnrollActivities/SendRequest',
        data: JSON.stringify(model),
        success: function (response) {
            if (response == true) {
                $("#enrollmentForm").modal('hide');
                location.reload();
                globalFunctions.showSuccessMessage(translatedResources.RequestSent)
            }
            else {
                alert(translatedResources.TechnicalError);
            }
        }
    });
}

//$('#group-value1').change(function () {
//    if ($(this).is(":checked")) {
//        $("#btnConfirmProceed").attr("disabled", false);
//    }
//    else {
//        $("#btnConfirmProceed").attr("disabled", true);
//    }
//});

function ConfirmPopupDontClose() {
    $('#confirmPopup').modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });
}

$("#collapseComment").mCustomScrollbar({
    autoExpandScrollbar: true,
    autoHideScrollbar: true,
    scrollbarPosition: "outside"
});

$('#termsAndConditionChkb').change(function () {
    ValidateSendRequestButton();
});

function eventScheduleChkBox() {
    $("#BindRadioButton >li").removeClass("active");
    $('input[name="radiogroup"]:checked').parents("li").addClass('active');
}