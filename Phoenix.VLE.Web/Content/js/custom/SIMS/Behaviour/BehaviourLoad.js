﻿// Author   : Fraz Ahmed
// Date     : 16/07/2019
// Version  :  1.0

$(document).ready(function () {
    intialize();

    
   

});
var studentIdList = [];
function intialize() {
    $('.selectpicker').selectpicker('refresh');
    $('#chkwitnesstoggler').on('change', function () {
        //debugger;
        if ($('#chkwitnesstoggler').is(':checked')) {
            $('#divstaffwitness').removeClass('d-none');
            $('#divwitnessstudent').addClass('d-none');
        }
        else {
            $('#divwitnessstudent').removeClass('d-none');
            $('#divstaffwitness').addClass('d-none');
        }

    });

    $('.time-picker').datetimepicker({
        format: 'LT',
        //debug: true
    });

    $('.date-picker').datetimepicker({
        format: 'MM/DD/YYYY',
        //debug: true
    });
    var dt = new Date();
    var cDate = ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear();
    var cTime = dt.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    $('.date-picker').val(cDate);
    $('.time-picker').val(cTime);

    //toggle add log panel
    var leftContentHeight = $('.content-wrapper-left').height();
    var addTaskWrapperHeight = $('.addTask-wrapper').height();
    var taskDetailsWrapperHeight = $('.taskDetails-wrapper').height();
    $('.addTask-wrapper, .taskDetails-wrapper').hide();

    $(".btn-addTask").on('click', function () {
        editBehaviour(this, 0);
    });


    $('.close-content-right').on('click', function () {
        $('.content-wrapper-left').animate({
            width: "100%"
        }, 600);
        $('.content-wrapper-right').animate({
            right: "-75%"
        }, 600);
        $('.addTask-wrapper').hide();
        $('.taskDetails-wrapper').hide();
        $('.search-here').css('overflow', '');
        $('.content-wrapper-right, .content-wrapper-left').css('height', 'auto');
        $('.task-list ul li').removeClass('active');
    });


    $('.task-list ul li').each(function () {
        $(this).click(function () {
            $('.addTask-wrapper').hide();
            if (leftContentHeight > taskDetailsWrapperHeight) {
                $('.content-wrapper-right').height(leftContentHeight);
            } else {
                $('.content-wrapper-left').height(taskDetailsWrapperHeight);
            }
            //alert($(this).attr("data-id"))
            loadBehaviourHistoryGrid($(this).attr("data-id"))
            $('.taskDetails-wrapper').show();
            // adding a extra style for hide the long student name
            $('.search-here').css('overflow', 'hidden');
            $(this).parent().find('li.active').removeClass('active');
            $(this).addClass('active');
            $('.content-wrapper-left').animate({
                width: "25%"
            }, 600);
            $('.content-wrapper-right').animate({
                right: "0"
            }, 600);
        })
    })
    $('input[type="file"]').fileinput({
        title: 'Drag and drop or <strong>BROWSE</strong>',
        multipleText: '{0} files',
        showMultipleNames: true,
        multiple: true,
        buttonClass: 'custom-upload',
        uploadUrl: "/SMS/Behaviour/Upload",
    });
    
    //added for grade and section for addbehaviour page

    $("#ddlGrades").change(function () {
        $("#ddlSection").empty();
        try {
            var url = "/SchoolManagement/FetchSection"; //"@Url.Action("FetchSection", "SIMS1", new {area = string.Empty})";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { id: $("#ddlGrades").val() },
                success: function (Sections) {
                    $("#ddlSection").append('<option value="">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#ddlSection").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    });
    $("#ddlwitnessGrades").change(function () {
        $("#ddlwitnessSection").empty();
        try {
            var url = "/SchoolManagement/FetchSection"; //"@Url.Action("FetchSection", "SIMS1", new {area = string.Empty})";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { id: $("#ddlwitnessGrades").val() },
                success: function (Sections) {
                    $("#ddlwitnessSection").append('<option value="">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#ddlwitnessSection").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    });
    $("#ddlSection").change(function () {
        $("#ddlOtherStudentList").empty();
        var grade = $('#ddlGrades').val();
        var section = $('#ddlSection').val();
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: '/SMS/Behaviour/GetStudentByGrade',
            contentType: 'application/json;charset=utf-8',
            data: { grade: grade, section: section },
            success: function (response) {
                // $("#ddlStudentList").append('<option value="">Select</option>');


                for (var i = 0; i < response.length; i++) {
                    //debugger;
                    console.log(response[i].Value + ":" + response[i].Text);
                    var div_data = "<option value=" + response[i].Value + ">" + response[i].Text + "</option>";
                    console.log(div_data);
                    $(div_data).appendTo('#ddlOtherStudentList');
                    //$("#ddlOtherStudentList").append('<option value="'
                    //    + response[i].Value + '">'
                    //    + response[i].Text + '</option>');
                }

                $('.selectpicker').selectpicker('refresh');
            },
            error: function (ex) {
                alert('Failed.' + ex);
            }
        });
    });
    $("#ddlwitnessSection").change(function () {
        $("#ddlwitnessStudentList").empty();
        var grade = $('#ddlwitnessGrades').val();
        var section = $('#ddlwitnessSection').val();
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: '/SMS/Behaviour/GetStudentByGrade',
            contentType: 'application/json;charset=utf-8',
            data: { grade: grade, section: section },
            success: function (response) {
                // $("#ddlStudentList").append('<option value="">Select</option>');


                for (var i = 0; i < response.length; i++) {
                    //debugger;
                    console.log(response[i].Value + ":" + response[i].Text);
                    var div_data = "<option value=" + response[i].Value + ">" + response[i].Text + "</option>";
                    console.log(div_data);
                    $(div_data).appendTo('#ddlwitnessStudentList');
                    //$("#ddlOtherStudentList").append('<option value="'
                    //    + response[i].Value + '">'
                    //    + response[i].Text + '</option>');
                }

                $('.selectpicker').selectpicker('refresh');
            },
            error: function (ex) {
                alert('Failed.' + ex);
            }
        });
    });


    $("#Category").change(function () {
        $("#SubCategory").empty();
        try {
            var url = "/Behaviour/FetchSubCategory";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { id: $("#Category").val() },
                success: function (options) {
                    $("#SubCategory").append('<option value="">Select</option>');
                    $.each(options, function (i, option) {
                        $("#SubCategory").append('<option value="'
                            + option.Value + '">'
                            + option.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    });
    //Extending the :contains selector property to be case in-sensitive for the card search option.
    $.extend($.expr[":"], {
        "containsIN": function (elem, i, match, array) {
            return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        }
    });
    //Search option for students cards
    $(function () { // this will be called when the DOM is ready
        $('#search').keyup(function () {
            $('.search-card').removeClass('d-none');
            var filter = $(this).val(); // get the value of the input, which we filter on
            $('.search-card').find('.task-title .text-muted .search-here:not(:containsIN("' + filter + '"))').parent().parent().parent().parent().addClass('d-none');

        });
    });

    $('#btnsaveincident').on('click', function () {
        var form = $('#frmIncidentBehaviour')[0]; // You need to use standard javascript object here
        var formData = new FormData(form);

        $.ajax({
            url: "/SMS/Behaviour/SaveBehaviourDetail",
            data: formData,
            type: 'POST',
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            // ... Other options like success and etc
            success: function (response) {
                globalFunctions.showMessage(response.NotificationType, response.Message);
               
            }
        });



    });

}
$("#Section").change(function () {
    var _ttid = 0;
    var _grade = $('#Grades').val();
    var _section = $('#Section').val();
    ajax_call(_ttid, _grade, _section);
})
function ajax_call(_ttid, _grade, _section) {
    var url = "/Behaviour/GetStudentList";
    $.ajax({
        type: 'GET',
        url: url,
        data: { tt_id: _ttid, grade: _grade, section: _section },
        success: function (result) {
            $('#BehaviourBody').html('');
            $('#BehaviourBody').html(result);
            intialize();
        },
        error: function (ex) {
            globalFunctions.onFailure();
        }
    });

    return false;

}


loadBehaviourHistoryGrid = function (stu_id) {
    if (stu_id.length > 0) {
        var _columnData = [];
        _columnData.push(
            { "mData": "Student_Name", "sTitle": "Student Name", "sClass": "border-right  bg-secondary text-center font-weight-light", "sWidth": "25%" },
            { "mData": "Type", "sTitle": "Type", "sClass": "border-right  bg-secondary text-center font-weight-light", "sWidth": "8%" },
            { "mData": "Comments", "sTitle": "Comments", "sClass": "border-right  bg-secondary font-weight-light", "sWidth": "28%" },
            { "mData": "Recorded_by", "sTitle": "Recorded by", "sClass": "border-right  bg-secondary text-center font-weight-light", "sWidth": "16%" },
            { "mData": "Recorded_Date", "sTitle": "Recorded Date", "sClass": "border-right  bg-secondary text-center font-weight-light", "sWidth": "18%" },
            { "mData": "editbutton", "sTitle": "", "sClass": "border-right  bg-secondary text-center", "sWidth": "5%", "orderable": false }
        );
        initBehaviourGrid('tbl_BehaviourHistoryGridList', _columnData, '/Behaviour/LoadBehaviourHistoryGrid?stu_id=' + stu_id);
    }
}

initBehaviourGrid = function (_tableId, _columnData, _url) {
    var grid = new DynamicGrid(_tableId);
    var settings = {
        columnData: _columnData,
        url: _url,
        columnSelector: false,
        searching: true,
        sortable: true,
        paging: false
    };
    grid.init(settings);
}

editBehaviour = function (source, id) {
    //alert(id);
    //toggle add log panel
    var leftContentHeight = $('.content-wrapper-left').height();
    var addTaskWrapperHeight = $('.addTask-wrapper').height();
    var taskDetailsWrapperHeight = $('.taskDetails-wrapper').height();
    $('.addTask-wrapper, .taskDetails-wrapper').hide();
    if (leftContentHeight > addTaskWrapperHeight) {
        $('.content-wrapper-right').height(leftContentHeight);
    } else {
        $('.content-wrapper-left').height(addTaskWrapperHeight);
    }
    $('.content-wrapper-left').animate({
        width: "25%"
    }, 600);
    $('.content-wrapper-right').animate({
        right: "0"
    }, 600);
    $('.taskDetails-wrapper').hide();

    $.ajax({
        type: "POST",
        url: "/Behaviour/EditBehaviourForm?id=" + id + "",
        data: { id },
        success: function (response) {
            var editbehaviour = $("#AddEditBehaviour");
            editbehaviour.empty();
            editbehaviour.append(response);
            intialize();
            $('#listOfStudentIds').val(studentIdList);
            $(".addTask-wrapper").show();
        },
        error: function (error) {
            globalFunctions.onFailure();
        }
    });
}
GetBehaviourById = function (id) {
    var url = "/Behaviour/GetBehaviourById";
    $.ajax({
        type: 'GET',
        url: url,
        data: { id: id },
        success: function (response) {
            $('#ReporterAddress2').val(response.ReporterAddress2);
        },
        error: function (ex) {
            globalFunctions.onFailure();
        }
    });

    return false;

}

onBehaviourSaveSuccess = function (data) {
    // debugger
    globalFunctions.showMessage(data.NotificationType, data.Message);

    if (data.Success) {
        //   debugger
        $('.taskDetails-wrapper').hide();

        $('.addTask-wrapper').show();

        loadBehaviourHistoryGrid(this, 123);//NEED TO LOAD VALUE FROM HIDDEN FIELD
    }
},

    OnchkStudentAdd = function addStudentIdTolist(chkStudentId) {
        //debugger;
        if ($('#chkStudentId_' + chkStudentId).is(":checked")) {
            studentIdList.push(chkStudentId);

        }
        else {
            var index = studentIdList.indexOf(chkStudentId);
            if (index > -1) {
                studentIdList.splice(index, 1);
            }
        }


    }


