﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var studentIdList = [];
var taskOrder = 0;
var multiSelectModule = 0;
var hndStuId = 0;
var modelcategoryId = 0;
var StudentBehaviour = function () {
    var init = function () {
        $('[data-toggle="tooltip"]').tooltip();
    },
        SectionLoad = function LoadonSectionChange(_ttid, _grade, _section) {

            var url = "/Behaviour/GetStudentList";
            $.ajax({
                type: 'GET',
                url: url,
                data: { tt_id: _ttid, grade: _grade, section: _section, IstudentBehaviour: 1 },
                success: function (response) {
                    // //debugger;
                    // $('#BehaviourBody').html('');
                    $('#divStudentListDetails').empty();
                    $('#divStudentListDetails').html(response.Result);
                },
                error: function (ex) {
                    globalFunctions.onFailure();
                }
            });

            return false;

        },
        openModal = function openModal(StudentId, AddMultiple) {
            //debugger;
            $('.behave.modal').modal('show');
            $('#StudentId').val(StudentId);
            hndStuId = StudentId;
            multiSelectModule = AddMultiple;
            $.get('/SMS/Behaviour/StudentBehaviourList', function (response) {

                $('#divstudentbehaviourlist').html(response);

            });

        },
        openSelectedModal = function openSelectedModal(StudentId) {
            $('.selected.modal').modal('show');
            $.post("/SMS/Behaviour/SelectedBehaviour", { studentId: StudentId }, function (response) {
                // //debugger;
                $('#divSelectedBehaviour').html(response.Result);

            });

        },
        openAddEditModal = function openAddEditModal(behaviourId) {
            //debugger;
            $('.AddEdit.modal').modal('show');

            $.post("/SMS/Behaviour/EditStudentBehaviourType", { behaviourId: behaviourId }, function (response) {
                // //debugger;
                $('#divAddEditBehaviour').html(response);
                $('#divBehaveIcon').hide();

            });

        },
        HoverPopup = function HoverPopup(a, state) {
            if (state == '1') {

                $(a).find(".behav-profile").attr("data-header-animation", true);
                // $(".behav-profile").attr("data-header-animation", true);
            }
            else {
                $(a).find(".behav-profile").attr("data-header-animation", false);
                //  $(".behav-profile").attr("data-header-animation", false);
            }
        },
        lstofstudentIds = function addStudentIdTolist(chkStudentId) {
            //debugger;
            if ($('#chkstudent_' + chkStudentId).is(":checked")) {
                studentIdList.push(chkStudentId);

            }
            else {
                var index = studentIdList.indexOf(chkStudentId);
                if (index > -1) {
                    studentIdList.splice(index, 1);
                }
            }

        },
        SubmitBulkIds = function SubmitBulkIds() {
            //  //debugger;
            $.post("/SMS/Behaviour/BulkInsert", { lstofstudentId: studentIdList }, function (response) {
                globalFunctions.showMessage(response.NotificationType, response.Message);

            });
        },
        EditBehaviourType = function EditBehaviourType() {
            //debugger;

            var behaviourId = $('#hndBehaviourId').val() == undefined || null ? $('#ddlBehaviourList').val() : $('#hndBehaviourId').val();
            var behaviourType = $('#txtBehaviourType').val();
            var behaviourPoint = $('#txtBehaviourpoint').val();
            var categoryId = $('#ddlCategoryType').val() == 0 || undefined || null ? 0 : $('#ddlCategoryType').val();
            $.post("/SMS/Behaviour/EditStudentTypeById", { behaviourId: behaviourId, behaviourType: behaviourType, behaviourPoint: behaviourPoint, categoryId: categoryId }, function (response) {
                globalFunctions.showMessage(response.NotificationType, response.Message);

                $.get('/SMS/Behaviour/StudentBehaviourList', { categoryId: modelcategoryId }, function (response) {
                    //debugger;
                    $('#divstudentbehaviourlist').html(response);

                });
                //content to load student and its details
                var _ttid = $('#hdnTTId').val();
                var _grade = $('#Grades').val();
                var _section = $('#Section').val();

                if (_ttid != 0) {
                    StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                }
                else {
                    StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                }
                $('.AddEdit.modal').modal('hide');
                // $('.behave.modal').modal('hide');

            });
        },
        BehaviourOnChange = function BehaviourOnChange(behaviourId) {
            //debugger;
            var behaviourId = $(behaviourId).val();
            if (behaviourId == 0) {
                return false;
            }
            else {
                $.post('/SMS/Behaviour/OnselectLoadBehaviourLogo', { behaviourId: behaviourId }, function (response) {
                    $('#imgBehaviour').attr('src', "/Content/img/SIMS/behaviour/" + response);
                    $('#divBehaveIcon').show();
                });

            }

        },
        BehaviourEdit = function BehaviourEdit() {
            //debugger;
            $("#BehaveEdit").hide();
            $(".behav-container-edit").show();
            $("#BehaveAdd").show();

        },
        BehaviourEditModelClose = function BehaviourEditModelClose() {
            $("#BehaveEdit").show();
            $(".behav-container-edit").hide();
            $("#BehaveAdd").hide();
        },
        BehaviourContentaddition = function BehaviourContentaddition(BehaviourId) {
            //debugger;
            var behaviourId = BehaviourId;
            var studentId = hndStuId;
            if (multiSelectModule == 1) {
                if (studentIdList.length == 0) {
                    globalFunctions.showMessage(2, "Please select Students to Add");
                    $('.behave.modal').modal('hide');
                    return false;
                }
                else {
                    $.post("/SMS/Behaviour/BulkInsert", { lstofstudentId: studentIdList, behaviourId: behaviourId }, function (response) {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        //debugger;
                        var _ttid = $('#hdnTTId').val();
                        var _grade = $('#Grades').val();
                        var _section = $('#Section').val();

                        if (_ttid != 0) {
                            StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                        }
                        else {
                            StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                        }

                    });
                }

            }
            else {
                $.post("/SMS/Behaviour/AddEditStudentBehaviour", { studentId: studentId, behaviourId: behaviourId }, function (response) {

                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    //debugger;
                    var _ttid = $('#hdnTTId').val();
                    var _grade = $('#Grades').val();
                    var _section = $('#Section').val();
                    if (_ttid != 0) {
                        StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                    }
                    else {
                        StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                    }
                    //StudentBehaviour.SectionLoad(_ttid, _grade, _section);

                });
            }
            studentIdList.length = 0;
            $('.behave.modal').modal('hide');

        },
        CategoryOnchange = function CategoryOnchange(categoryId) {
            //debugger;
            var id = $(categoryId).val();
            modelcategoryId = $(categoryId).val();
            $.get('/SMS/Behaviour/StudentBehaviourList', { categoryId: id }, function (response) {
                var titleHeading = $(categoryId).find("option:selected").text();
                $('#divstudentbehaviourlist').html(response);
                $('#lblCategorytitle').html(titleHeading);


            });
        },
        BehaviourCatalogWithUpload = function BehaviourCatalogFileUpload(id) {
            //debugger;
            $('#BehaviourId').val(id);
            $('#StudentId').val(hndStuId);
            // $('#data').submit();
            var form = $('#data')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);
            // var url = $('#data').attr('action');
            if (multiSelectModule == 1) {
                if (studentIdList.length == 0) {
                    globalFunctions.showMessage(2, "Please select student's to add");
                    $('.behave.modal').modal('hide');
                    return false;
                }
                else {

                    formData.append('lstofstudentId', studentIdList);
                    $.ajax({
                        url: "/SMS/Behaviour/BulkInsert",
                        data: formData,
                        type: 'POST',
                        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                        processData: false, // NEEDED, DON'T OMIT THIS
                        // ... Other options like success and etc
                        success: function (response) {
                            globalFunctions.showMessage(response.NotificationType, response.Message);
                            //debugger;
                            var _ttid = $('#hdnTTId').val();
                            var _grade = $('#Grades').val();
                            var _section = $('#Section').val();
                            if (_ttid != 0) {
                                StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                            }
                            else {
                                StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                            }
                        }
                    });
                }

            }
            else {
                $.ajax({
                    url: "/SMS/Behaviour/AddEditStudentBehaviour",
                    data: formData,
                    type: 'POST',
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false, // NEEDED, DON'T OMIT THIS
                    // ... Other options like success and etc
                    success: function (response) {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        //debugger;
                        var _ttid = $('#hdnTTId').val();
                        var _grade = $('#Grades').val();
                        var _section = $('#Section').val();
                        if (_ttid != 0) {
                            StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                        }
                        else {
                            StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                        }
                    }
                });
            }



            studentIdList.length = 0;
            $('.behave.modal').modal('hide');
        },
        DownloadBehaviourAttachment = function DownloadBehaviourAttachment(studentId) {

            //debugger;
            var stuId = studentId == undefined || null ? 0 : studentId
            if (stuId != 0) {
                // window.location.href = "/SMS/Behaviour/DownloadAttachment?studentId=" + stuId;
                window.location.href = "/SMS/Behaviour/Download?studentId =" + stuId

            }


        },
        OncardClick = function OncardClick(card, event) {
            //debugger;
            $(card).toggleClass("selected-stud");
            var studentId = $(card).attr('data-studentId');

            // $("#chkstudent_" + studentId).trigger("click");
            //$(".custom-control-input").val($(this).is(':checked'));   
            //$('input[name=LocationFilter]').attr('checked', true);

            if ($(card).hasClass('selected-stud')) {

                // $(card).find(".custom-control-input").attr('checked', true);
                studentIdList.push(studentId);

            }
            else {
                // $(card).find(".custom-control-input").attr('checked', false);
                $(card).find(".card.card-chart").removeClass('selected-stud');
                var index = studentIdList.indexOf(studentId);
                if (index > -1) {
                    studentIdList.splice(index, 1);
                }
            }


        },
        DeleteBehaviour = function DeleteBehaviour(Bhvdelete) {
            var studentId = $(Bhvdelete).attr('data-studentId');
            var BehaviourId = $(Bhvdelete).attr('data-behaviourId');

            $.post('/SMS/Behaviour/DeleteStudentBehaviour', { studentId: studentId, BehaviourId: BehaviourId }, function (response) {
                globalFunctions.showMessage(response.OperationMessage.NotificationType, response.OperationMessage.Message);
                $('#divSelectedBehaviour').html(response.Result);
                var _ttid = $('#hdnTTId').val();
                var _grade = $('#Grades').val();
                var _section = $('#Section').val();
                if (_ttid != 0) {
                    StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                }
                else {
                    StudentBehaviour.SectionLoad(_ttid, _grade, _section);
                }


            });


        }
    return {
        init: init,
        SectionLoad: SectionLoad,
        openModal: openModal,
        openSelectedModal: openSelectedModal,
        openAddEditModal: openAddEditModal,
        HoverPopup: HoverPopup,
        lstofstudentIds: lstofstudentIds,
        SubmitBulkIds: SubmitBulkIds,
        EditBehaviourType: EditBehaviourType,
        BehaviourOnChange: BehaviourOnChange,
        BehaviourEdit: BehaviourEdit,
        BehaviourEditModelClose: BehaviourEditModelClose,
        BehaviourContentaddition: BehaviourContentaddition,
        CategoryOnchange: CategoryOnchange,
        BehaviourCatalogWithUpload: BehaviourCatalogWithUpload,
        DownloadBehaviourAttachment: DownloadBehaviourAttachment,
        OncardClick: OncardClick,
        DeleteBehaviour: DeleteBehaviour
    };
}();

$(document).ready(function () {

    StudentBehaviour.init();

    $("#Section").change(function () {
        // //debugger;
        var _ttid = 0;
        var _grade = $('#Grades').val();
        var _section = $('#Section').val();
        StudentBehaviour.SectionLoad(_ttid, _grade, _section);
    })


    //$('.clsbehaviourcontent').on('click', function () {
    //    //debugger;
    //    var behaviourId = $(this).attr('data-bId');
    //    var studentId = $('#StudentId').val();
    //    if (multiSelectModule == 1) {
    //        if (studentIdList.length == 0) {
    //            globalFunctions.showMessage(2, "Please select Students to Add");
    //            $('.behave.modal').modal('hide');
    //            return false;
    //        }
    //        else {
    //            $.post("/SMS/Behaviour/BulkInsert", { lstofstudentId: studentIdList, behaviourId: behaviourId }, function (response) {
    //                globalFunctions.showMessage(response.NotificationType, response.Message);
    //                var _ttid = 0;
    //                var _grade = $('#Grades').val();
    //                var _section = $('#Section').val();
    //                StudentBehaviour.SectionLoad(_ttid, _grade, _section);
    //            });
    //        }

    //    }
    //    else {
    //        $.post("/SMS/Behaviour/AddEditStudentBehaviour", { studentId: studentId, behaviourId: behaviourId }, function (response) {
    //            ////debugger;
    //            //$('#divSelectedBehaviour').html(response.Result);
    //            globalFunctions.showMessage(response.NotificationType, response.Message);
    //            //$('#divBehaviourPoints_' + studentId).html(response.points);
    //            //$('#spnLastUpdatedOn_' + studentId).html(response.lastUpdatedOn);
    //            var _ttid = 0;
    //            var _grade = $('#Grades').val();
    //            var _section = $('#Section').val();
    //            StudentBehaviour.SectionLoad(_ttid, _grade, _section);

    //        });
    //    }
    //    studentIdList.length = 0;
    //    $('.behave.modal').modal('hide');


    //});
    $.extend($.expr[":"], {
        "containsIN": function (elem, i, match, array) {
            return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        }
    });

    $('#search').keyup(function () {
        $('.item').removeClass('d-none');
        var filter = $(this).val(); // get the value of the input, which we filter on
        $('#divStudentList').find('.card .card-content:not(:containsIN("' + filter + '"))').parent().parent().addClass('d-none');
        //$('#products').find('.card .card-body:containsIN("' + filter + '")').addClass('highlighter');

    })

    // Ellipsis for for student name 
    $(document).on('mouseenter', ".tooltip-name", function () {
        var $this = $(this);
        if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
            $this.tooltip({
                title: $this.text(),
                placement: "top"
            });
            $this.tooltip('show');
        }
    });
    $('.hide-name').css('width', $('.hide-name').parent().width());

    //$("#BehaveEdit").click(function () {
    //    $("#BehaveEdit").hide();
    //    $(".behav-container-edit").show();
    //    $("#BehaveAdd").show();
    //});

    $(".modal-content .close").click(function () {
        $("#BehaveEdit").show();
        $(".behav-container-edit").hide();
        $("#BehaveAdd").hide();
    });


    //$('#Category').on('change', function () {
    //    var id = $(this).val();
    //    $.get('/SMS/Behaviour/StudentBehaviourList', { categoryId: id }, function (response) {

    //        $('#divstudentbehaviourlist').html(response);

    //    });

    //});



});
