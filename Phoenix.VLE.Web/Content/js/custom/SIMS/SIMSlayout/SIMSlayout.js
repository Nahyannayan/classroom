﻿
/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var studentIdList = [];
var taskOrder = 0;
var multiSelectModule = 0;
var hndStuId = 0;
var modelcategoryId = 0;
var SIMSLayoutJs = function () {
    var init = function () {
        $("#btnBack").show(); //to show the back button block
    },
    btnhide = function btnhide() {

        $("#btnBack").hide();
    },
   btnbackblock = function btnbackblock() {
       $(document).on('click', '#btnBack', function () {
           history.back();
       });

   }

    return {
        init: init,
        btnbackblock: btnbackblock,
        btnhide: btnhide
    };
}();

$(document).ready(function () {
    $(document).on('click', '#btnBack', function () {
        history.back();
    });
    

});
