﻿
$(document).ready(function () {
    $(function () {
        setDropDownValueAndTriggerChange('Grades')
    });
    $("#Grades").change(function () {
        $("#Section").empty();
        try {
            var url = "/SchoolManagement/FetchSection"; //"@Url.Action("FetchSection", "SIMS1", new {area = string.Empty})";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { id: $("#Grades").val() },
                success: function (Sections) {
                    $("#Section").append('<option value="">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Section").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Section');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }
    })    
});

