﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var formElements = new FormElements();
var objPopulateDropdownlist = [];
var SIMSReport = function () {

    onchangeLoadReport = function onchangeLoadReport() {
        $("#ReportType").change(function () {
            //debugger;
            objPopulateDropdownlist.length = 0;
            $("#ReportDetails").empty();
            try {
                var url = "/Reports/LoadReportsByReportType";
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    data: { ReportType: $("#ReportType").val() },
                    success: function (Sections) {
                        $("#ReportDetails").append('<option value="">Select</option>');
                        $.each(Sections, function (i, Section) {
                            $("#ReportDetails").append('<option value="'
                                + Section.Value + '">'
                                + Section.Text + '</option>');
                        });
                        $('.selectpicker').selectpicker('refresh');
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });

                return false;
            } catch (e) {
                alert(e);
            }

        })


    },
        populateDetails = function (dropdown) {
            //debugger;
            //var objPopulateDropdownlist = [];
            var value = $(dropdown).val();
            var rdf_code = $(dropdown).attr('data-RDFCode');
            var RDSR_ID = $('#hdfRDSRId').val();
            var nxtddl = $(dropdown).attr('data-NextDDL');
            var nxtdllval = $("#ddl_" + nxtddl).attr('data-RDFCode');
            var objPopulateDropdown = {

                FILTER_CODE: rdf_code,
                Filter_Value: value

            }
            if (objPopulateDropdownlist.length > 0) {
                var indexxof = objPopulateDropdownlist.findIndex(x => x.FILTER_CODE === objPopulateDropdown.FILTER_CODE);
                if (indexxof > -1) {
                    objPopulateDropdownlist.splice(indexxof, 1);
                }
                if (nxtdllval != "") {
                    var nxtddlIndex = objPopulateDropdownlist.findIndex(x => x.FILTER_CODE === nxtdllval);
                    if (nxtddlIndex > -1) {
                        objPopulateDropdownlist.splice(nxtddlIndex, 1);
                    }
                }

            }
            objPopulateDropdownlist.push(objPopulateDropdown);

            $.post('/Reports/PopulateReportFormControls',
                { RDSR_ID: RDSR_ID, RDF_FILTER_CODE: nxtddl, objPopulateDropdownlist: objPopulateDropdownlist },
                function (response) {
                    $("#ddl_" + nxtddl).empty();
                    $("#ddl_" + nxtddl).append('<option value="">Select</option>');
                    for (var i = 0; i < response.length; i++) {

                        $("#ddl_" + nxtddl).append('<option value="'
                            + response[i].Code + '">'
                            + response[i].Name + '</option>');

                    }
                    $('.selectpicker').selectpicker('refresh');
                   
                });
            return false;

        },

        populateMultiselect = function populateMultiselect(dropdown) {
            //debugger;
            var values = $(dropdown).val();
            var value = $(dropdown).val();
            var rdf_code = $(dropdown).attr('data-RDFCode');
            var RDSR_ID = $('#hdfRDSRId').val();
            var nxtddl = $(dropdown).attr('data-NextDDL');
            var objPopulateDropdown = {

                FILTER_CODE: rdf_code,
                Filter_Value: values.toString(),
                DataType: "Checkbox"
            }
            if (objPopulateDropdownlist.length > 0) {
                var indexxof = objPopulateDropdownlist.findIndex(x => x.FILTER_CODE === objPopulateDropdown.FILTER_CODE);
                if (indexxof > -1) {
                    objPopulateDropdownlist.splice(indexxof, 1);
                }

            }
            objPopulateDropdownlist.push(objPopulateDropdown);
            if (nxtddl != "") {
                $("#ddl_" + nxtddl).empty();
                $("#ddl_" + nxtddl).append('<option value="">Select</option>');
                $.post('/Reports/PopulateMultiselectReportFormControls',
                    { RDSR_ID: RDSR_ID, RDF_FILTER_CODE: nxtddl, currentFilterCode: rdf_code, objPopulateDropdownlist: objPopulateDropdownlist },
                    function (response) {

                        for (var i = 0; i < response.length; i++) {

                            $("#ddl_" + nxtddl).append('<option value="'
                                + response[i].Code + '">'
                                + response[i].Name + '</option>');

                        }
                        $('.selectpicker').selectpicker('refresh');
                       
                    });
                return false;

            }


        },

        LoadReportHeader = function LoadReportHeader() {
            $("#ReportHeaders").empty();
            try {
                var url = "/Reports/FetchReportHeaders";
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    data: { id: $("#Term").val() },
                    success: function (Sections) {
                        $("#ReportHeaders").append('<option value="0">Select</option>');
                        $.each(Sections, function (i, Section) {
                            $("#ReportHeaders").append('<option value="'
                                + Section.Value + '">'
                                + Section.Text + '</option>');
                        });
                        $('.selectpicker').selectpicker('refresh');
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });

                return false;
            } catch (e) {
                alert(e);
            }
        },
        LoadTerms = function LoadTerms() {
            $("#Term").empty();
            try {
                var url = "/Assessment/FetchTerms";
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    data: { acd_id: $("#AcademicYear").val() },
                    success: function (Sections) {
                        $("#Term").append('<option value="0">Select</option>');
                        $.each(Sections, function (i, Section) {
                            $("#Term").append('<option value="'
                                + Section.Value + '">'
                                + Section.Text + '</option>');
                        });
                        $('.selectpicker').selectpicker('refresh');
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });

                return false;
            } catch (e) {
                alert(e);
            }
        }



    return {
        onchangeLoadReport: onchangeLoadReport,
        populateDetails: populateDetails,
        populateMultiselect: populateMultiselect,
        LoadReportHeader: LoadReportHeader,
        LoadTerms: LoadTerms

    };


}();

$(document).ready(function () {

    SIMSReport.onchangeLoadReport();

    $('#ReportDetails').on('change', function () {
        $.get('/Reports/LoadReportFormControls', {
            ReportType: $('#ReportType').val(), Dev_Id: $(this).val()
        }, function (response) {

            $('#divrptcontrol').html(response);
            $('.selectpicker').selectpicker('refresh');
            // $('.clsdatepicker').datepicker();
        });



    });

    $("#AcademicYear").change(function () {
        //$("#Grades").empty();
        //$("#Subjects").empty();
        //$("#SubjectGroups").empty();
        //$("#SubjectCategory").empty();
        //$("#ReportSchedule").empty();
        //SIMSReport.LoadReportHeader();
        SIMSReport.LoadTerms();

    });
    $("#Term").change(function () {
        $("#Subjects").empty();
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();
        //LoadGrades();
        SIMSReport.LoadReportHeader();
    });

    $('#ReportHeaders').on('change', function () {
        //debugger;
        objPopulateDropdownlist.length = 0;
        var RSM_Id = $(this).val();
        $.get('/Reports/GetDevIdFromRSM_ID', {
            RSM_ID: RSM_Id, ACD_ID: $("#AcademicYear").val()
        }, function (response) {

            $('#divrptcontrol').html(response);
            $('.selectpicker').selectpicker('refresh');
            // $('.clsdatepicker').datepicker();

        });

        var objACD_ID = {

            FILTER_CODE: "ACD_ID",
            Filter_Value: $("#AcademicYear").val()

        }
        var objRSM_ID = {

            FILTER_CODE: "RSM_ID",
            Filter_Value: RSM_Id

        }
        objPopulateDropdownlist.push(objACD_ID);
        objPopulateDropdownlist.push(objRSM_ID);



    });


    $('#toggler').on('change', function () {

        if ($(this).is(':checked')) {
            $('#divrptcontrol').empty();
            $('#divrpttype').addClass('d-none');
            $('#divrptdetails').addClass('d-none');

            $('#divrptacd').removeClass('d-none');
            $('#divrpthdr').removeClass('d-none');
            $('#divterm').removeClass('d-none');
            
        }
        else {

            $('#divrptcontrol').empty();
            $('#divrpttype').removeClass('d-none');
            $('#divrptdetails').removeClass('d-none');

            $('#divrptacd').addClass('d-none');
            $('#divrpthdr').addClass('d-none');
            $('#divterm').addClass('d-none');

        }


    });

});