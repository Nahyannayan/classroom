﻿
$(document).ready(function () {
    $('.GradeSection').hide();
    var _date = new Date().toISOString();
    var _type = $("#toggler").is(":checked");
    var _grade = $('#Grades').val();
    var _section = $('#Section').val();
  
        ajax_call( _date, _type, _grade, _section);
 

    $("#Section").change(function () {
        var _date = new Date().toISOString();
        var _type = $("#toggler").is(":checked");
        var _grade = $('#Grades').val();
        var _section = $('#Section').val();
        ajax_call(_date, _type, _grade, _section);
    })
  

});
function ajax_call(_date_val, _type, _grade, _section) {
    var url = "/SchoolManagement/LoadTimeTableLayout"; // "@Url.Action("LoadTimeTableLayout", "SIMS1", new {area = string.Empty})";
    $.ajax({
        type: 'GET',
        url: url,
        data: { dateTime: _date_val, type: _type, grade: _grade, section: _section },
        success: function (result) {
            $("#dvTimeTable").html("");
            $("#dvTimeTable").html(result);
          
            $('.multiple-items').mCustomScrollbar({
                autoHideScrollbar: true,
                autoExpandScrollbar: true

            });

        
          

        },
        error: function (ex) {
            globalFunctions.onFailure();
        }
    });

    return false;

}

function getWeeklyTimetable(pushdate) {
    var pshdate = $(pushdate).attr('data-frwddate');
    var _date = pshdate;
    var _type = $("#toggler").is(":checked");
    var _grade = $('#Grades').val();
    var _section = $('#Section').val();
    ajax_call(_date, _type, _grade, _section);
}

$(function () {
    $('#toggler').change(function () {
        if ($(this).prop('checked')) {
           

            var _date = new Date().toISOString();
            var _type = $("#toggler").is(":checked");
            var _grade = $('#Grades').val();
            var _section = $('#Section').val();
            ajax_call(_date, _type, _grade, _section);

            $('.GradeSection').fadeOut('slow');
           
        }
        else {

            $('.GradeSection').fadeIn('slow');
           
        }
    })
});
