﻿
var CommentsToAdd = [];
$(document).ready(function () {
    $('#divrowbtnsave').hide();
    $('[data-toggle="tooltip"]').tooltip();
    setDropDownValueAndTriggerChange('AcademicYear')
    LoadTerms();
    // LOADING ReportHeaders
    function LoadReportHeader() {
        $("#ReportHeaders").empty();
        try {
            var url = "/Assessment/FetchReportHeaders";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { id: $("#Term").val() },
                success: function (Sections) {
                    $("#ReportHeaders").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#ReportHeaders").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('ReportHeaders');
                    loadDataAuto();
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }
    }
    //$("#AcademicYear").change(function () {
    //    $("#Grades").empty();
    //    $("#Subjects").empty();
    //    $("#SubjectGroups").empty();
    //    $("#SubjectCategory").empty();
    //    $("#ReportSchedule").empty();
    //    //append for select as default
    //    $("#Subjects").append('<option value="">Select</option>');
    //    $("#SubjectGroups").append('<option value="">Select</option>');
    //    $("#Grades").append('<option value="">Select</option>');
    //    $("#ReportSchedule").append('<option value="">Select</option>');
    //    //  LoadReportHeader();

    //});
    //-----------------------------------------
    // LOADING Grades
    function LoadGrades() {
        $("#Grades").empty();
        try {
            var url = "/Assessment/GetGradesAccess";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYear").val(), rsm_id: $("#ReportHeaders").val() },
                success: function (Sections) {
                    $("#Grades").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Grades").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Grades');
                    loadDataAuto();
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    }
    $("#ReportHeaders").change(function () {
        $("#Subjects").empty();
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();
        LoadGrades();
        LoadReportSchedule();
    });

    function LoadTerms() {
        $("#Term").empty();
        try {
            var url = "/Assessment/FetchTerms";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYear").val() },
                success: function (Sections) {
                    $("#Term").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Term").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Term');
                    loadDataAuto();
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }
    }
    $("#AcademicYear").change(function () {
        
        $("#Grades").empty();
        $("#Subjects").empty();
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();

        $("#Subjects").append('<option value="">Select</option>');
        $("#SubjectGroups").append('<option value="">Select</option>');
        $("#Grades").append('<option value="">Select</option>');
        $("#ReportSchedule").append('<option value="">Select</option>');
        LoadTerms();
        // LoadAssessmentActivity();

    });
    $("#Term").change(function () {
        $("#Subjects").empty();
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();
        //LoadGrades();
        LoadReportHeader();
    });
    //-----------------------------------------
    // LOADING Subjects
    function LoadSubjects() {
        $("#Subjects").empty();
        try {
            var url = "/Assessment/GetSubjectsByGrade";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYear").val(), grd_id: $("#Grades").val() },
                success: function (Sections) {
                    $("#Subjects").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Subjects").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Subjects');
                    loadDataAuto();
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }
    }
    $("#Grades").change(function () {
        
        if ($('#toggler').prop('checked')) {
            $("#PreviousReportSchedule").empty();
            $("#OptionalHeaders").empty();
            $("#SubjectGroups").empty();
            $("#SubjectCategory").empty();
            LoadSubjects();
            $("#PreviousReportSchedule").append('<option value="">Select</option>');
            $("#OptionalHeaders").append('<option value="">Select</option>');
            $.post('/Assessment/GetAssessmentOptionalLists',
                { ACD_ID: $('#AcademicYear').val(), GRD_ID: $(this).val() },
                function (response) {

                    for (var i = 0; i < response.PreviousResult.length; i++) {

                        $("#PreviousReportSchedule").append('<option value="'
                            + response.PreviousResult[i].RPF_ID + '">'
                            + response.PreviousResult[i].DISPLAY_TEXT + '</option>');

                    }

                    for (var i = 0; i < response.OptionResult.length; i++) {

                        $("#OptionalHeaders").append('<option value="'
                            + response.OptionResult[i].AOM_ID + '">'
                            + response.OptionResult[i].AOM_DESCR + '</option>');

                    }
                    $('.selectpicker').selectpicker('refresh');
                    //$('.filter-option-inner-inner').html('Select');
                    setDropDownValueAndTriggerChange('PreviousReportSchedule');
                    setDropDownValueAndTriggerChange('OptionalHeaders');
                    loadDataAuto();
                });
            return false;
        }
        else {
            LoadSection();
        }

    });
    //-----------------------------------------
    // LOADING Sections
    function LoadSection() {
        $("#Section").empty();
        try {
            var url = "/Assessment/GetSectionAccess";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { ACD_ID: $("#AcademicYear").val(), GRD_ID: $("#Grades").val() },
                success: function (Sections) {
                    $("#Section").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Section").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Section');
                    loadDataAuto();
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    }
    //-----------------------------------------
    // LOADING Subject Groups
    function LoadSubjectGroups() {
        $("#SubjectGroups").empty();
        try {
            var url = "/Assessment/GetSubjectGroupBySubject";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { sbg_id: $("#Subjects").val(), grd_id: $("#Grades").val() },
                success: function (Sections) {
                    $("#SubjectGroups").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#SubjectGroups").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('SubjectGroups');
                    loadDataAuto();
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    }
    $("#Subjects").change(function () {
        LoadSubjectGroups();
        LoadSubjectCategory();
    });
    //-----------------------------------------
    // LOADING Subject Category
    function LoadSubjectCategory() {
        $("#SubjectCategory").empty();
        try {
            var url = "/Assessment/FetchSubjectCategory";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { sbg_id: $("#Subjects").val() },
                success: function (Sections) {
                    $("#SubjectCategory").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#SubjectCategory").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('SubjectCategory');
                    loadDataAuto();
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    }
    //-----------------------------------------
    // LOADING Report Schedule
    function LoadReportSchedule() {
        $("#ReportSchedule").empty();
        try {
            var url = "/Assessment/FetchReportSchedule";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { rsm_id: $("#ReportHeaders").val() },
                success: function (Sections) {
                    $("#ReportSchedule").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#ReportSchedule").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('ReportSchedule');     
                    loadDataAuto();
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    }


    $('#ddlassessmentCategory').on('change', function () {
        
        var value = $(this).val();
        $.get('/SMS/Assessment/GetCommentByCategoryId', { CAT_ID: value, STU_ID: studentId }, function (response) {
            console.log(response);
            $('#divJtableComments').html(response);
        });


    });

    var loadDataAuto = () => {
        if ($('#toggler').prop('checked')) {
            if (
                $('#AcademicYear')[0].options.length > 1 &&
                $('#ReportHeaders')[0].options.length > 1 &&
                $('#Grades')[0].options.length > 1 &&
                $('#Subjects')[0].options.length > 1 &&
                $('#SubjectGroups')[0].options.length > 1 &&
                $('#ReportSchedule')[0].options.length > 1 ) {
                ViewStudents();
            }
        } else {
            if (
                $('#AcademicYear')[0].options.length > 1 &&
                $('#ReportHeaders')[0].options.length > 1 &&
                $('#Grades')[0].options.length > 1 &&
                $('#Section')[0].options.length > 1 &&
                $('#ReportSchedule')[0].options.length > 1) {
                ViewStudents();
            }
        }
    }

});
function datatableInit() {

    if (!$.fn.dataTable.isDataTable('#example')) {
        $('#example').DataTable({
            "paging": false,

            "fixedHeader": true,
            //"ordering": false,
            "columnDefs": [
                {
                    "width": "8%",
                    "targets": 0,
                    "orderable": false
                },
                {
                    "width": "25%",
                    "targets": 1,
                    "orderable": true
                },
                {
                    "targets": "no-sort",
                    "orderable": false
                }
            ]
        });
    }

}
onAssessmentSaveSuccess = function (data) {
    globalFunctions.showMessage(data.NotificationType, data.Message);

    if (data.Success) {
        $('#btn_View').click();
        //var _ttm_id = $('#hf_ttm_id').val();
        //var _entrydate = $('#hf_entry_date').val();
        //var _grade = $('#hf_grade').val();
        //var _section = $('#hf_section').val();
        //FetchAttendance(_ttm_id, _entrydate, _grade, _section);
    }
}
function ViewStudents() {
    if ($('#toggler').prop('checked')) {
        if (validate_Dropdowns($('#AcademicYear')) && validate_Dropdowns($('#ReportHeaders')) && validate_Dropdowns($('#Grades')) && validate_Dropdowns($('#Subjects')) && validate_Dropdowns($('#SubjectGroups')) && validate_Dropdowns($('#ReportSchedule'))) {

            var _GRD_ID = $('#Grades').val();
            var _ACD_ID = $('#AcademicYear').val();
            var _SBG_ID = $('#Subjects').val() == "" || $('#Subjects').val() == null ? 0 : $('#Subjects').val();
            var _RPF_ID = $('#ReportSchedule').val() == "" || $('#ReportSchedule').val() == null ? 0 : $('#ReportSchedule').val();
            var _SGR_ID = $('#SubjectGroups').val() == "" || $('#SubjectGroups').val() == null ? 0 : $('#SubjectGroups').val();
            var _RSM_ID = $('#ReportHeaders').val();
            var _SCT_ID = $('#Section').val() == "" || $('#Section').val() == null ? 0 : $('#Section').val();
            var _AOD_IDS = $('#OptionalHeaders').val().toString().replace(",", "|"); //NEED TO SET THIS
            LoadOptionalHeaders(_GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS)
            $('#divrowbtnsave').show();
        }
    }
    else {
        if (validate_Dropdowns($('#AcademicYear')) && validate_Dropdowns($('#ReportHeaders')) && validate_Dropdowns($('#Grades')) && validate_Dropdowns($('#Section')) && validate_Dropdowns($('#ReportSchedule'))) {

            var _GRD_ID = $('#Grades').val();
            var _ACD_ID = $('#AcademicYear').val();
            var _SBG_ID = $('#Subjects').val() == "" || $('#Subjects').val() == null ? 0 : $('#Subjects').val();
            var _RPF_ID = $('#ReportSchedule').val() == "" || $('#ReportSchedule').val() == null ? 0 : $('#ReportSchedule').val();
            var _SGR_ID = $('#SubjectGroups').val() == "" || $('#SubjectGroups').val() == null ? 0 : $('#SubjectGroups').val();
            var _RSM_ID = $('#ReportHeaders').val();
            var _SCT_ID = $('#Section').val() == "" || $('#Section').val() == null ? 0 : $('#Section').val();
            var _AOD_IDS = $('#OptionalHeaders').val().toString().replace(",", "|"); //NEED TO SET THIS
            LoadOptionalHeaders(_GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS)
            $('#divrowbtnsave').show();
        }
    }


}
//CHAINING OF MULTIPLE AJAX CALLS TO GET THE DESIRED RESULT SET
function LoadOptionalHeaders(_GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS) {
    $.ajax({
        type: "GET",
        url: "/Assessment/GetReportHeaderOptional",
        data: { AOD_IDs: _AOD_IDS },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (OptionalHeaderResults) {
            LoadOptionalAssessmentData(OptionalHeaderResults, _GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS)
        },
        error: function (error) {

        }
    });
    return true;
}
function LoadOptionalAssessmentData(OptionalHeaderResults, _GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS) {
    $.ajax({
        type: "GET",
        url: "/Assessment/GetAssessmentDataOptional",
        data: { ACD_ID: _ACD_ID, RPF_ID: _RPF_ID, RSM_ID: _RSM_ID, SBG_ID: _SBG_ID, SGR_ID: _SGR_ID, GRD_ID: _GRD_ID, SCT_ID: _SCT_ID, AOD_IDs: _AOD_IDS },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (OptionalDataResults) {
            LoadHeaders(OptionalHeaderResults, OptionalDataResults, _GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS)
        },
        error: function (error) {

        }
    });
    return true;
}


function LoadHeaders(OptionalHeaderResults, OptionalDataResults, _GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS) {
    $.ajax({
        type: "GET",
        url: "/Assessment/GetReportHeaders",
        data: { GRD_ID: _GRD_ID, ACD_ID: _ACD_ID, SBG_ID: _SBG_ID, RPF_ID: _RPF_ID, RSM_ID: _RSM_ID, prv: $('#PreviousReportSchedule').val().toString() },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            LoadStudentList(OptionalHeaderResults, OptionalDataResults, results, _GRD_ID, _ACD_ID, _SGR_ID, _SBG_ID, _RSM_ID, _RPF_ID, _SCT_ID, _AOD_IDS)
        },
        error: function (error) {

        }
    });
    return true;
}
function LoadStudentList(OptionalHeaderResults, OptionalDataResults, Headerresult, _GRD_ID, _ACD_ID, _SGR_ID, _SBG_ID, _RSM_ID, _RPF_ID, _SCT_ID, _AOD_IDS) {
    $.ajax({
        type: "GET",
        url: "/Assessment/GetStudentList",
        data: { GRD_ID: _GRD_ID, ACD_ID: _ACD_ID, SGR_ID: _SGR_ID, SCT_ID: _SCT_ID },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            LoadAssessmentData(OptionalHeaderResults, OptionalDataResults, Headerresult, results, _ACD_ID, _SBG_ID, _RPF_ID, _RSM_ID, _AOD_IDS);

        },
        error: function (error) {
            //   globalFunctions.onFailure();
        }
    });
    return true;
}
function LoadAssessmentData(OptionalHeaderResults, OptionalDataResults, Headerresult, results, _ACD_ID, _SBG_ID, _RPF_ID, _RSM_ID, _AOD_IDS) {
    var headerth = "";
    $.ajax({
        type: "GET",
        url: "/Assessment/GetAssessmentData",
        data: { ACD_ID: _ACD_ID, SBG_ID: _SBG_ID, RPF_ID: _RPF_ID, RSM_ID: _RSM_ID, prv: $('#PreviousReportSchedule').val().toString() },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (assess_data) {
            //===================HEADER START=====================================
            $('#LoadAssessment_H_tr').html("");
            var items = Headerresult;
            //PLACE OPTIONAL HEADERS HERE
            var headerlayout = "";
            for (var i = 0; i < OptionalHeaderResults.length; i++) {
                headerth = OptionalHeaderResults[i].AOM_DESCR;
                headerlayout += "<th class='text-center border-right  no-sort font-weight-light bg-secondary border-right' width='200px'>";
                headerlayout += headerth;
                headerlayout += "</th>";
            }
            for (var i = 0; i < items.length; i++) {
                if (items[i].RSD_SUB_DESC == '') {
                    headerth = items[i].RSD_HEADER;
                }
                else {
                    if (items[i].IS_PREV) {
                        headerth = items[i].RSD_SUB_DESC + " (" + items[i].RSD_HEADER + ")";

                    }
                    else {
                        headerth = items[i].RSD_SUB_DESC;

                    }
                }
                headerlayout += "<th class='text-center border-right  no-sort font-weight-light bg-secondary border-right' width='200px'>";
                headerlayout += headerth;
                headerlayout += "</th>";
            }

            $('#LoadAssessment_H_tr').append(headerlayout);
            //$('#LoadAssessment_H_tr').prepend("<th class='font-weight-light bg-secondary border-right'>Name</th>");
            //$('#LoadAssessment_H_tr').prepend("<th class='font-weight-light bg-secondary border-right' width='10%'>ID</th>");
            $('#LoadAssessment_H_tr').prepend("<th class='font-weight-light bg-secondary border-right table-first' width='10%'></th>");

            //===================HEADER END=====================================

            //===================BODY START=====================================
            var layout = "";
            var header = "";
            //
            if (assess_data.length > 0) {


                for (var j = 0; j < results.length; j++) {   //loop for tr
                    //<div class='user-action nopad'><img class='stud-grid-profile' src='" + results[j].STU_PHOTO + "' /> " + results[j].STU_NO + "<br>" + results[j].STU_NAME;
                    layout += "<tr class='tr_data'>";
                    layout += "<td class='p-0 freezecol assessment-first-col'>  <div class='row'> <div class='col-lg-4 col-md-4 col-sm-4 col-4 pl-3'><div class='user-action nopad'><img class='stud-grid-profile' src='" + results[j].STU_PHOTO + "'/></div></div>";
                    layout += "<div class='col-lg-8 col-md-8 col-sm-8 col-8 pl-2'><div class='row'> <div class='col-lg-12 col-12 pt-2'><h6>" + results[j].STU_NAME + "</h6></div><div class='col-lg-12 col-12 pt-2'><p class='text-bold'>" + results[j].STU_NO + "</p></div></div></div></div>";
                    //layout += "<td class='freezecol bg-white'>" + results[j].STU_NO + "</td>";
                    //layout += "<td class='freezecol bg-white'>" + results[j].STU_NAME + "</td>";
                    //PLACE OPTIONAL COLUMN CODES HERE
                    for (var i = 0; i < OptionalHeaderResults.length; i++) {
                        layout += "<td class='text-center p-2 td_optional'> "
                        for (var k = 0; k < OptionalDataResults.length; k++) {
                            if (results[j].STU_ID == OptionalDataResults[k].STU_ID) {
                                if (OptionalHeaderResults[i].AOM_ID == OptionalDataResults[k].AOM_ID) {

                                    layout += OptionalDataResults[k].OPTIONAL_VALUE
                                }
                            }
                        }
                        layout += "</td>"
                    }
                    for (var i = 0; i < Headerresult.length; i++) {    //loop for td
                        if (Headerresult[i].RSD_SUB_DESC == '') {
                            header = Headerresult[i].RSD_HEADER;
                        }
                        else {
                            header = Headerresult[i].RSD_SUB_DESC;
                        }
                        if (Headerresult[i].IS_PREV) {
                            layout += "<td class='text-center p-2 td_Prev'><div class='form-group focused m-0'>";
                        }
                        else {
                            layout += "<td class='text-center p-2 td_data'><div class='form-group focused m-0'>";
                        }

                        layout += "<input type='hidden' class='hf_stu_id' value='" + results[j].STU_ID + "'>";
                        layout += "<input type='hidden' class='hf_rsd_id' value='" + Headerresult[i].RSD_ID + "'>";
                        if (Headerresult[i].RSD_RESULT === "C") {

                            if (Headerresult[i].CSSCLASS.toLowerCase() === "textboxmulti") {
                                var headerId = Headerresult[i].IS_PREV ? "Prv_" + Headerresult[i].RSD_HEADER + "_" + Headerresult[i].RSM_ID : Headerresult[i].RSD_HEADER + "_" + Headerresult[i].RSM_ID;
                                if (assess_data.length == 0) {
                                    layout += "<textarea cols='0' rows='0' data-header=" + headerId + " data-stuid=" + results[j].STU_ID + " id=txt_" + results[j].STU_NO + "_" + headerId + " class='form-control form-control-alternative custom-textarea hf_comments' spellcheck='true'></textarea>";
                                }
                                else {
                                    var already_created = 0;
                                    for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                        if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //   --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                            if (already_created != 1) {
                                                if (assess_data[k].COMMENTS != "" || assess_data[k].COMMENTS != null) {

                                                    layout += "<textarea  rows='0' data-header=" + headerId + " data-stuid=" + results[j].STU_ID + " id=txt_" + results[j].STU_NO + "_" + headerId + " class='form-control form-control-alternative custom-textarea hf_comments' spellcheck='true'>" + assess_data[k].COMMENTS + "</textarea>";


                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                    for (var k = 0; k < assess_data.length; k++) { //for filling empty text boxes
                                        //if (!$('#toggler').prop('checked')) {
                                        if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {
                                                if (already_created != 1) {


                                                    layout += "<textarea cols='0' rows='0' data-header=" + headerId + " data-stuid=" + results[j].STU_ID + " id=txt_" + results[j].STU_NO + "_" + headerId + " class='form-control form-control-alternative custom-textarea hf_comments' spellcheck='true'></textarea>";


                                                    already_created = 1;
                                                }
                                            }
                                        }
                                        //}
                                    }
                                }
                                layout += "  <a href='javascript:void(0)' data-header=" + headerId + " onclick='OpenComments(" + results[j].STU_NO + ",this)'><i class='fa fa-search text-muted' style='position: absolute;'></i></a>";

                                //layout += "  <a data-toggle='modal' data-toggle='tooltip' data-placement='left' title='Select Category' href='javascript:void(0)' data-target='#selectCategory'><img src='/Content/img/SIMS/document-edit.png' class='img-document-entry' /></a>";
                                if (!Headerresult[i].IS_PREV) {
                                    layout += "  <a  href='javascript:void(0)' data-header=" + headerId + " onclick='OpenCommentModal(" + results[j].STU_NO + ",this)'><img src='/Content/img/SIMS/document-edit.png' class='img-document-entry' /></a>";

                                }
                            }
                            else if (Headerresult[i].CSSCLASS.toLowerCase() === "textboxsmall") {
                                if (assess_data.length == 0) {
                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_comments'>";
                                }
                                else {
                                    for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                        if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                            layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_comments' value='" + assess_data[k].COMMENTS + "'>";
                                        }
                                    }

                                }
                            }
                        } //COMMENTS

                        else if (Headerresult[i].RSD_RESULT === "D") {  //DROPDOWN
                            //
                            if (Headerresult[i].RSP_DESCR === null) {
                                layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_comments' spellcheck='true' >";
                            }
                            else {

                                if (assess_data.length == 0) {
                                    layout += "<select class='form-control form-control-alternative table-select hf_comments' data_selected='' type='dropdown'>";
                                    layout += Headerresult[i].RSP_DESCR;
                                    layout += "</select>";
                                }
                                else {
                                    var already_created = 0;
                                    for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                        if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                            if (already_created != 1) {
                                                if (assess_data[k].COMMENTS != "" || assess_data[k].COMMENTS != null) {
                                                    layout += "<select class='form-control form-control-alternative table-select hf_comments' data_selected='" + assess_data[k].COMMENTS + "' type='dropdown'>";
                                                    layout += Headerresult[i].RSP_DESCR;
                                                    layout += "</select>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                    for (var k = 0; k < assess_data.length; k++) { //for filling empty dropdown
                                        //if (!$('#toggler').prop('checked')) {
                                        if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {
                                                if (already_created != 1) {
                                                    layout += "<select class='form-control form-control-alternative table-select hf_comments' data_selected='' type='dropdown'>";
                                                    layout += Headerresult[i].RSP_DESCR;
                                                    layout += "</select>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                        //}
                                    }

                                }
                            }
                        }
                        else if (Headerresult[i].RSD_RESULT === "L") {
                            //if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                            //    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark' disabled value='" + assess_data[k].MARK + "'>";
                            //}
                            //else {
                            //    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark' disabled>";
                            //}
                            if (assess_data.length == 0) {
                                layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark' disabled>";
                            }
                            else {
                                var already_created = 0;
                                for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                    if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                        if (already_created != 1) {
                                            if (assess_data[k].MARK != "" || assess_data[k].MARK != null) {
                                                layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark' disabled value='" + assess_data[k].MARK + "'>";
                                                already_created = 1;
                                            }
                                        }
                                    }
                                }
                                for (var k = 0; k < assess_data.length; k++) { //for filling empty text boxes
                                    //if (!$('#toggler').prop('checked')) {
                                    if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                        if (Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {
                                            if (already_created != 1) {
                                                layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark' disabled>";
                                                already_created = 1;
                                            }
                                        }
                                    }
                                    //}
                                }

                            }
                        }
                        else if (Headerresult[i].RSD_RESULT === "G") {
                            for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                //if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                //    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_grading' value='" + assess_data[k].GRADING + "'>";
                                //}
                                //else {
                                //    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_grading'>";
                                //}
                                if (assess_data.length == 0) {
                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_grading'>";
                                }
                                else {
                                    var already_created = 0;
                                    for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                        if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                            if (already_created != 1) {
                                                if (assess_data[k].GRADING != "" || assess_data[k].GRADING != null) {
                                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_grading' value='" + assess_data[k].GRADING + "'>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                    for (var k = 0; k < assess_data.length; k++) { //for filling empty text boxes
                                        //if (!$('#toggler').prop('checked')) {
                                        if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {
                                                if (already_created != 1) {
                                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_grading'>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                        //}
                                    }

                                }
                            }
                        }
                        else {
                            for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                //if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                //    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark' value='" + assess_data[k].MARK + "'>";
                                //}
                                //else {
                                //    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark'>";
                                //}
                                if (assess_data.length == 0) {
                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark'>";
                                }
                                else {
                                    var already_created = 0;
                                    for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                        if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                            if (already_created != 1) {
                                                if (assess_data[k].MARK != "" || assess_data[k].MARK != null) {
                                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark' value='" + assess_data[k].MARK + "'>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                    for (var k = 0; k < assess_data.length; k++) { //for filling empty text boxes
                                        //if (!$('#toggler').prop('checked')) {
                                        if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {
                                                if (already_created != 1) {
                                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark'>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                        //}
                                    }

                                }
                            }
                        }
                        layout += "</div></td>";
                    }
                    layout += "</tr>";
                }
                $('#LoadAssessment_B_tbody').html(layout);
            }
            else {
                $('#LoadAssessment_H_tr').html("");

               // $('#LoadAssessment_B_tbody').html("<tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr><td></td><td></td><td></td><td></td><td class='sub-heading'> No Data</td></tr>");
                $('#divrowbtnsave').hide();
                globalFunctions.showMessage('warning', "No Data Available");

            }
          

            //===================BODY END=====================================
            SetDropdownValues();
            //  datatableInit();
            $('.td_Prev').find(":input").prop("disabled", true);

            //$('.divtable').mCustomScrollbar({
            //    axis: "yx",
            //    autoHideScrollbar: true,
            //    autoExpandScrollbar: true,
            //    //autoExpandHorizontalScroll: true
            //});

        },
        error: function (error) {
            //   globalFunctions.onFailure();
        }
    });
    return true;
}
function SetDropdownValues() {
    $(".table-select").each(function () {
        if ($(this).attr('data_selected') != "") {
            $(this).val($(this).attr('data_selected'));
        }
    });
}
function SaveAssessmentData() {
    var RST_ID = "0";
    var STU_ID = "";
    var RPF_ID = $('#ReportSchedule').val();
    var RSD_ID = "";
    var ACD_ID = $('#AcademicYear').val();
    var GRD_ID = $('#Grades').val();
    var RSS_ID = $('#ReportSchedule').val();
    var SGR_ID = $('#SubjectGroups').val() == "" || $('#SubjectGroups').val() == null ? 0 : $('#SubjectGroups').val();
    var SBG_ID = $('#Subjects').val() == "" || $('#Subjects').val() == null ? 0 : $('#Subjects').val();
    var SCT_ID = $('#Section').val() == "" || $('#Section').val() == null ? 0 : $('#Section').val();
    var TYPE_LEVEL = "";
    if ($('#toggler').prop('checked')) {
        TYPE_LEVEL = "Core";
    }
    var MARK = "";
    var COMMENTS = "";
    var GRADING = "";
    var data_xml = "<DATA>";
    $('.tr_data .td_data').each(function () {
        STU_ID = $(this).find(".hf_stu_id").val();
        RSD_ID = $(this).find(".hf_rsd_id").val();
        MARK = $(this).find(".hf_mark").val() == 'undefined' ? 'undefined' : $(this).find(".hf_mark").val();
        COMMENTS = $(this).find(".hf_comments").val() == 'undefined' ? 'undefined' : $(this).find(".hf_comments").val();
        GRADING = $(this).find(".hf_grading").val() == 'undefined' ? 'undefined' : $(this).find(".hf_grading").val();
        if (MARK != 'undefined' && COMMENTS != "") {
            data_xml += "<STUDENT>";
            data_xml += "<RST_ID>" + RST_ID + "</RST_ID>";
            data_xml += "<RPF_ID>" + RPF_ID + "</RPF_ID>";
            data_xml += "<RSD_ID>" + RSD_ID + "</RSD_ID>";
            data_xml += "<ACD_ID>" + ACD_ID + "</ACD_ID>";
            data_xml += "<GRD_ID>" + GRD_ID + "</GRD_ID>";
            data_xml += "<STU_ID>" + STU_ID + "</STU_ID>";
            data_xml += "<RSS_ID>" + RSS_ID + "</RSS_ID>";
            data_xml += "<SGR_ID>" + SGR_ID + "</SGR_ID>";
            data_xml += "<SBG_ID>" + SBG_ID + "</SBG_ID>";
            data_xml += "<SCT_ID>" + SCT_ID + "</SCT_ID>";
            data_xml += "<TYPE_LEVEL>" + TYPE_LEVEL + "</TYPE_LEVEL>";
            data_xml += "<MARK>" + MARK + "</MARK>";
            data_xml += "<COMMENTS>" + COMMENTS + "</COMMENTS>";
            data_xml += "<GRADING>" + GRADING + "</GRADING>";
            data_xml += "</STUDENT>";
        }

    });
    data_xml += "</DATA>";
    $("#hf_Student_XML").val(data_xml);
}

/*-------------------------------------------ASSESSMENT COMMENT SECTION--------------------------------------------------*/
var Stu_No = 0;
var studentId = 0;
var dataheader = "";
function OpenCommentModal(studentNo, header) {
    if (Stu_No == studentNo) {
        $('#tblassessmentComment tbody tr td input[type="checkbox"]').each(function () {
            $(this).prop('checked', false);
        });
    }
    else {
        $('#ddlassessmentCategory').val("");
        $('#divJtableComments').empty();
    }


    Stu_No = studentNo;
    dataheader = $(header).attr('data-header');

    var txtvalue = $('#txt_' + Stu_No + '_' + dataheader);
    studentId = $('#txt_' + Stu_No + '_' + dataheader).attr('data-stuid');

    $('.selectCategory').modal('show');


}

function SelectComments(cmdid) {
    //var Comments = $('#txt_' + Stu_No + '_' + dataheader).val();
    var Comments = $('#td_' + cmdid).html().trim();

    if ($('#chk_' + cmdid).is(":checked")) {
        CommentsToAdd.push(Comments);

    }
    else {
        var index = CommentsToAdd.indexOf(Comments);
        if (index > -1) {
            CommentsToAdd.splice(index, 1);
        }
    }
    //if (Comments != '') {

    //    $('#txt_' + Stu_No + '_' + dataheader).val(Comments);
    //    globalFunctions.showMessage("1", "Comment added for student no:" + Stu_No);

    //}


}


function OpenComments(studentNo, header) {
    
    var headerval = $(header).attr('data-header');
    var values = $('#txt_' + studentNo + '_' + headerval).val();
    $('#prgComments').html(values);

    $('.commentbox').modal('show');


}



function AddComments() {
    
    var Comments = $('#txt_' + Stu_No + '_' + dataheader).val()

    //$('.commentbox').modal('close');

    for (var i = 0; i < CommentsToAdd.length; i++) {

        Comments = Comments + CommentsToAdd[i].trim();


    }
    $('#txt_' + Stu_No + '_' + dataheader).val(Comments);
    globalFunctions.showMessage("1", "Comments Added ");
    CommentsToAdd.length = 0;
    $('.selectCategory').modal('hide');
}


$(function () {
    $('#tdSection').hide();
    $('#toggler').change(function () {
        if ($(this).prop('checked')) {
            $('#tdSubject').show();
            $('#tdSubjectGroup').show();
            $('#tdSection').hide();
            $('#Grades').val(0);
            $('#Section').val(0);
            $('.selectpicker').selectpicker('refresh');
            $('#divoptionalheaders').show();
            $('#LoadAssessment_H_tr').html("");
            $('#LoadAssessment_B_tbody').html("");
            $('#divrowbtnsave').hide();
        }
        else {
            $('#tdSubject').hide();
            $('#tdSubjectGroup').hide();
            $('#tdSection').show();
            $('#Subjects').val(0);
            $('#SubjectGroups').val(0);
            $('#Grades').val(0);
            $('.selectpicker').selectpicker('refresh');
            $('#divoptionalheaders').hide();
            $('#PreviousReportSchedule').empty();
            $('#OptionalHeaders').empty();
            $('#LoadAssessment_H_tr').html("");
            $('#LoadAssessment_B_tbody').html("");
            $('#divrowbtnsave').hide();
        }
    })
});


