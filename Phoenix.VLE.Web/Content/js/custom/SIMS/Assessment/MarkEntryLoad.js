﻿$(document).ready(function () {
    setDropDownValueAndTriggerChange('AcademicYear');
    function LoadTerms() {
        $("#Term").empty();
        try {
            var url = "/Assessment/FetchTerms";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYear").val() },
                success: function (Sections) {
                    $("#Term").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Term").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Term');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }
    }
    $("#AcademicYear").change(function () {
        $("#Grades").empty();
        $("#Subjects").empty();
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();
        LoadTerms();
        LoadAssessmentActivity();

    });
    //-----------------------------------------
    // LOADING Grades
    function LoadGrades() {
        $("#Grades").empty();
        try {
            var url = "/Assessment/GetGradesAccess";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYear").val(), rsm_id: '-1' },
                success: function (Sections) {
                    $("#Grades").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Grades").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Grades');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    }
    $("#Term").change(function () {
        $("#Subjects").empty();
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();
        LoadGrades();
    });
    //-----------------------------------------
    // LOADING Subjects
    function LoadSubjects() {
        $("#Subjects").empty();
        var grade = $("#Grades").val().split("|")[0];
        try {
            var url = "/Assessment/GetSubjectsByGrade";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYear").val(), grd_id: grade },
                success: function (Sections) {
                    $("#Subjects").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Subjects").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Subjects');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }
    }
    $("#Grades").change(function () {
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();
        LoadSubjects();
    });
    //-----------------------------------------
    // LOADING Subject Groups
    function LoadSubjectGroups() {
        $("#SubjectGroups").empty();
        var grade = $("#Grades").val().split("|")[0];
        try {
            var url = "/Assessment/GetSubjectGroupBySubject";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { sbg_id: $("#Subjects").val(), grd_id: grade },
                success: function (Sections) {
                    $("#SubjectGroups").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#SubjectGroups").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('SubjectGroups');
                    if ($('#SubjectGroups')[0].options.length > 1) {
                        ViewStudents();
                    }

                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    }
    $("#Subjects").change(function () {
        LoadSubjectGroups();
    });

    //-----------------------------------------
    // LOADING LoadAssessmentActivity
    function LoadAssessmentActivity() {
        $("#AssessmentActivity").empty();
        try {
            var url = "/Assessment/FetchAssessmentActivity";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: {},
                success: function (Sections) {
                    $("#AssessmentActivity").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#AssessmentActivity").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('AssessmentActivity');
                    if ($('#SubjectGroups')[0].options.length > 1) {
                        ViewStudents();
                    }
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    }

    
});
function datatableInit() {

    if (!$.fn.dataTable.isDataTable('#example')) {
        $('#example').DataTable({
            "paging": false,
            //"ordering": false,
            "columnDefs": [
                {
                    "width": "8%",
                    "targets": 0,
                    "orderable": false
                },
                {
                    "width": "25%",
                    "targets": 1,
                    "orderable": true
                },
                {
                    "targets": "no-sort",
                    "orderable": false
                }
            ]
        });
    }

}

function ViewStudents() {
    if (validate_Dropdowns($('#AcademicYear')) && validate_Dropdowns($('#Term')) && validate_Dropdowns($('#Grades')) && validate_Dropdowns($('#Subjects')) && validate_Dropdowns($('#SubjectGroups'))) {
        
        var Data = $('#Grades').val(); //dynamically generated 
        var arrGrdSec = [];
        //alert(Data);
        $.each(Data.split(/\|/), function (i, val) {
            //alert(val);
            arrGrdSec.push(val);
        })
        var _GRD_ID = arrGrdSec[0];
        var _ACD_ID = $('#AcademicYear').val();
        var _SBG_ID = $('#Subjects').val();
        var _TRM_ID = $('#Term').val();
        var _SGR_ID = $('#SubjectGroups').val();
        var _CAM_ID = $('#AssessmentActivity').val();
        var _TRM_ID = $('#Term').val();
        var section = arrGrdSec[1];
        $.get("/SMS/Assessment/ViewListMArkEntry", { ACD_ID: _ACD_ID, CAM_ID: _CAM_ID, GRD_ID: _GRD_ID, STM_ID: section, TRM_ID: _TRM_ID, SGR_ID: _SGR_ID, SBG_ID: _SBG_ID }, function (response) {

            $('#divMarkEntry').hide();
            $('#divViewMarkEntry').html(response);

            $('#divViewMarkEntry').show();
            $('#divMarkEntryAOL').hide();



        });

        // LoadHeaders(_GRD_ID, _ACD_ID, _SBG_ID, _TRM_ID, _SGR_ID, _CAM_ID);
    }
}

function BacktoView() {
    $('#divViewMarkEntry').show();
    $('#divMarkEntryAOL').hide();
}

function MarkAsAOLEXAM(marks) {

    var IsAOLEXAM = $(marks).attr('data-isAOLExam');
    var CAS_ID = $(marks).attr('data-CASId');
    var Isbwithoutskill = $(marks).attr('data-bwithoutskill');
    var slabId = $(marks).attr('data-slabId');
    var EntryType = $(marks).attr('data-entrytype');
    var minMarks=  $(marks).attr("data-minMarks");
    var maxMarks=  $(marks).attr("data-maxMarks");
    if (IsAOLEXAM == true) {


        $.get('/SMS/Assessment/GetMarkEntryAOL', { IsAOLEXAM: IsAOLEXAM, CAS_ID: CAS_ID, Isbwithoutskill: Isbwithoutskill }, function (response) {
            $('#divViewMarkEntry').hide();
            $('#divMarkEntryAOL').html(response);
            $('#divMarkEntryAOL').show();

        });

    }
    else {

        $.get('/SMS/Assessment/GetMarkEntryAOL', { IsAOLEXAM: IsAOLEXAM, CAS_ID: CAS_ID, Isbwithoutskill: Isbwithoutskill, SlabId: slabId, Type: EntryType, minMarks: minMarks, maxMarks: maxMarks }, function (response) {
            $('#divViewMarkEntry').hide();
            $('#divMarkEntryAOL').html(response);
            $('#divMarkEntryAOL').show();

        });

    }



}

function gettextboxChange(skillSetVal) {
    
    var SkillMarks = $(skillSetVal).val();
    var SkillName = $(skillSetVal).attr('data-stu_skillname');
    var Stuno = $(skillSetVal).attr('data-stu_no');

    SKILL_MARK.push(SkillMarks);
    SKILL_NAME.push(SkillName);
    STU_NO.push(Stuno);




}


/*-----------------------------------------------SAVE FOR MARK AOL EXAM------------------------*/
function SaveMarkAsAOLEXAM() {

    var markEntryAOLData = [];//to pass as modl object to actionresult

    var count = $('#hdnSkillCount').val();//to get the skill count for looping
    var Isbwithoutskill = $('#hdnisbwithoutskill').val();
    var CAS_ID = $('#hdnCAS_ID').val();
    $('#tblMarkEntryAOL tbody tr').each(function () {

        var StuNo = parseInt($(this).find(".clstdstuno").html().trim());
        var StuName = $(this).find(".clsstuname").html().trim();
        var stuSkill = [];
        var j = 0;
        $(this).find(".clsstuSkillInput").each(function () {
            j++;
            var stuskill_name = $(this).attr('data-SkillName');
            stuSkill.push(stuskill_name);



        });

        for (var i = 0; i <= count; i++) {
            var SkillMarks = $('#txt_' + StuNo + '_' + stuSkill[i]).val();
            var SkillName = stuSkill[i];
            var stuId = $('#txt_' + StuNo + '_' + stuSkill[i]).attr('data-stu_Id');
            var Sta_Id = $('#txt_' + StuNo + '_' + stuSkill[i]).attr('data-sta_Id');
            var skillOrder = $('#txt_' + StuNo + '_' + stuSkill[i]).attr('data-skillOrder');
            if (SkillMarks != undefined || SkillMarks != null) {
                var markEntry = {
                    STA_ID: Sta_Id,
                    STU_ID: stuId,
                    STU_NO: StuNo,
                    STU_NAME: StuName,
                    SKILL_NAME: SkillName,
                    SKILL_MARK: SkillMarks,
                    SKILL_GRADE: "",
                    SKILL_MAX_MARK: 0.0,
                    MARKS: 0.0,
                    STA_GRADE: "",
                    IS_ENABLED: "",
                    WITHOUTSKILLS_MARKS: "",
                    WITHOUTSKILLS_GRADE: "",
                    WITHOUTSKILLS_MAXMARKS: 0,
                    CHAPTER: "",
                    FEEDBACK: "",
                    WOT: "",
                    TARGET: "",
                    bATTENDED: false,
                    STU_STATUS: "",
                    SKILL_ORDER: skillOrder
                }

                markEntryAOLData.push(markEntry);
            }


        }

    });


    $.post('/SMS/Assessment/SaveMarkEntryList', { markEntryAOLData: markEntryAOLData, Isbwithoutskill: Isbwithoutskill, CAS_ID: CAS_ID }, function (response) {
        globalFunctions.showMessage(response.NotificationType, response.Message);
        $('#divViewMarkEntry').show();
        $('#divMarkEntryAOL').hide();
    });



}

/*-----------------------------------------------SAVE FOR MARK ENTRY DATA------------------------*/

function SaveMarkEntryData() {
    
    var hdnSlabId = $('#hdnSlabId').val();//slab id
    var hdnEntryType = $('#hdnEntryType').val();//entry type
    var hdnCAS_ID = $('#hdnCAS_ID').val();//CAS_ID
    var hdnMinMarks = $('#hdnMinMax').val();
    var hdnMaxMarks = $('#hdnMaxMax').val();
    var objListofMarkEntryData = [];
    $('#tblMarkEntryData tbody tr').each(function () {
        var StuNo = parseInt($(this).find(".clstdstuno").html().trim());
        var StuName = $(this).find(".clsstuname").html().trim();
        var STA_ID = $('#txt_' + StuNo).attr('data-sta_Id');
        var STU_ID = $('#txt_' + StuNo).attr('data-stu_Id');
        var MARKS = $('#txt_' + StuNo).val();
        var MARK_GRADE = $(this).find('.clsstumarkgrade').attr('data-markgrade');
        var MIN_MARK = hdnMinMarks;
        var MAX_MARK = hdnMaxMarks;
        var IS_ENABLED = $('#txt_' + StuNo).attr('data-ISEnabled');
        var STU_STATUS = $('#txt_' + StuNo).attr('data-Stu_Status');
        var objMarkEntryData = {

            STA_ID: STA_ID,
            STU_ID: STU_ID,
            STU_NO: StuNo,
            STU_NAME: StuName,
            MARKS: MARKS,
            MARK_GRADE: MARK_GRADE,
            MIN_MARK: MIN_MARK,
            MAX_MARK: MAX_MARK,
            IS_ENABLED: IS_ENABLED,
            STU_STATUS: STU_STATUS



        }
        objListofMarkEntryData.push(objMarkEntryData);

    });
    //POST LIST OF MARKS
    $.post('/SMS/Assessment/SaveMarkEntryData', { objListofMarkEntryData: objListofMarkEntryData, slabId: hdnSlabId, entryType: hdnEntryType, CAS_ID: hdnCAS_ID }, function (response) {
        globalFunctions.showMessage(response.NotificationType, response.Message);
        $('#divViewMarkEntry').show();
        $('#divMarkEntryAOL').hide();
    });
}