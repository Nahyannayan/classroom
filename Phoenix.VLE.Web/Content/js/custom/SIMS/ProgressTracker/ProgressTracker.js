﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var comboTree1 = undefined;
var objStudentEvidenceList = [];
var ProgressTracker = function () {
    var init = function () {
        $('[data-toggle="tooltip"]').tooltip();

        $('#Grades').on('change', function () {
            $("#Subjects").empty();
            $('#AgeBand').empty();
            $('#divAgeBand').addClass('d-none');
            var grade = $(this).val();
            try {
                var url = "/ProgressTracker/GetSubjectsByGrade";
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    data: { acd_id: $("#AcademicYear").val(), grd_id: grade },
                    success: function (Sections) {
                        $("#Subjects").append('<option value="0">Select</option>');
                        $.each(Sections, function (i, Section) {
                            $("#Subjects").append('<option value="'
                                + Section.Value + '">'
                                + Section.Text + '</option>');
                        });
                        $('.selectpicker').selectpicker('refresh');
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });


                //ageBand
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: "/ProgressTracker/BindAgeBandOnGrade",
                    contentType: 'application/json;charset=utf-8',
                    data: { ACD_ID: $("#AcademicYear").val(), Grd_id: grade },
                    success: function (reponse) {
                        $("#AgeBand").append('<option value="0">Select</option>');

                        if (reponse.length > 0) {
                            $('#divAgeBand').removeClass('d-none');
                            $.each(reponse, function (i, reponse) {

                                $("#AgeBand").append('<option value="'
                                    + reponse.Value + '">'
                                    + reponse.Text + '</option>');


                            });
                            $('#divSubjects').prependTo('#divSubjectRow');
                            $('#divTopics').prependTo('#divTopicRow');
                        }
                        else {
                            $('#divSubjects').appendTo('#divAgeBandrow');
                            $('#divTopics').appendTo('#divSubjectRow');

                        }

                        $('.selectpicker').selectpicker('refresh');
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });

                return false;
            } catch (e) {
                alert(e);
            }



        });

        $('#AcademicYear').on('change', function () {
            $("#Term").empty();
            $('#txtTopics').empty();
            $("#Grades").empty();

            $("#Subjects").empty();
            $("#SubjectGroups").empty();

            try {
                var url = "/ProgressTracker/BindSubTerms";

                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    data: { ACD_ID: $("#AcademicYear").val() },
                    success: function (response) {
                        $("#Term").append('<option value="0">Select</option>');
                        for (var i = 0; i < response.length; i++) {
                            $("#Term").append('<option value="'
                                + response[i].TERM_ID + '" data-tsmId=' + response[i].ID + '>'
                                + response[i].DESCRIPTION + '</option>');
                        }
                        $("#Term").selectpicker('refresh');
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });

                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: "/ProgressTracker/GetGradesAccess",
                    contentType: 'application/json;charset=utf-8',
                    data: { acd_id: $("#AcademicYear").val(), rsm_id: '-1' },
                    success: function (Sections) {
                        $("#Grades").append('<option value="0">Select</option>');
                        $.each(Sections, function (i, Section) {
                            $("#Grades").append('<option value="'
                                + Section.Value + '">'
                                + Section.Text + '</option>');
                        });

                        $('#Grades').selectpicker('refresh');
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });



                return false;
            } catch (e) {
                alert(e);
            }


        });

        $('#Subjects').on('change', function () {
            $('#SubjectGroups').empty();
            var subject = $(this).val();
            var grade = $("#Grades").val().split("|")[0];
            $("#Term").val("0");
            $("#Term").selectpicker('refresh');
            $('#divTopics').addClass('d-none');
            $('#divsteps').addClass('d-none');
            try {
                var url = "/ProgressTracker/GetSubjectGroupBySubject";
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    data: { sbg_id: subject, grd_id: grade },
                    success: function (Sections) {
                        $("#SubjectGroups").append('<option value="0">Select</option>');
                        $.each(Sections, function (i, Section) {
                            $("#SubjectGroups").append('<option value="'
                                + Section.Value + '">'
                                + Section.Text + '</option>');
                        });
                        $('#SubjectGroups').selectpicker('refresh');
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });



                return false;
            } catch (e) {
                alert(e);
            }


        });

        $('#Term').on('change', function () {
            $('#txtTopics').empty();

            var sub_Id = $('#Subjects').val();
            var term = $(this).val();

            $('#divTopics').addClass('d-none');
            $('#divsteps').addClass('d-none');

            ProgressTracker.BindTopicTree(term, sub_Id);

        });


        $('#txtTopics').on('change', function () {
            ////debugger;
            $("#Steps").empty();
            if (comboTree1 != undefined) {
                var grsec = [];
                var acdid = $('#AcademicYear').val();
                var grade = $('#Grades').val();
                var Data = grade
                // alert(Data);
                $.each(Data.split(/\|/), function (i, val) {
                    // alert(val);
                    grsec.push(val);
                });
                var sbgId = $('#Subjects').val();
                var selectedIds = comboTree1.getSelectedItemsId();
                try {
                    var url = "/ProgressTracker/BindStepsOntopic";
                    $.ajax({
                        type: 'GET',
                        dataType: "json",
                        url: url,
                        contentType: 'application/json;charset=utf-8',
                        data: { Topicname: selectedIds, Acd_Id: acdid, GRD_Id: grsec[0], Sbg_Id: sbgId },
                        success: function (options) {
                            ////debugger;
                            if (options.length > 0) {
                                $('#divsteps').removeClass('d-none');
                                $("#Steps").append('<option value="0">Select</option>');
                                for (var i = 0; i < options.length; i++) {
                                    $("#Steps").append('<option value="'
                                        + options[i].SYC_STEP + '">'
                                        + options[i].SYC_STEP + '</option>');
                                }

                            }
                            else {
                                $('#divsteps').addClass('d-none');
                            }

                            $('#Steps').selectpicker('refresh');
                        },
                        error: function (ex) {
                            alert('Failed.' + ex);
                        }
                    });

                    return false;
                } catch (e) {
                    alert(e);
                }
            }



        });

        $('#btnProgressTracker').on('click', function () {

            ProgressTracker.DisplayProgressTracker();


        });

        $("#ProgressPdf").show();
        $('#radio-two').attr("checked", "checked");

        $("#radio-one").click(function () {
            $("#AttainmentPdf").show();
            $("#ProgressPdf").hide();
            $("#DetailedViewPdf").hide();
            $('#radio-one').prop('checked', true);
            $('#radio-two').prop('checked', false);
            $('#radio-three').prop('checked', false);
        });
        $("#radio-two").click(function () {
            $("#AttainmentPdf").hide();
            $("#ProgressPdf").show();
            $("#DetailedViewPdf").hide();
            $('#radio-one').prop('checked', false);
            $('#radio-two').prop('checked', true);
            $('#radio-three').prop('checked', false);
        });
        $("#radio-three").click(function () {
            $("#AttainmentPdf").hide();
            $("#ProgressPdf").hide();
            $("#DetailedViewPdf").show();
            $('#radio-one').prop('checked', false);
            $('#radio-two').prop('checked', false);
            $('#radio-three').prop('checked', true);
        });
        $('#iClear').on('click', function () {

            $('#txtTopics').val("");
            $("#Steps").empty();
            $('#divsteps').addClass('d-none');
        });

    },
        BindTopicTree = function BindTopicTree(TRM_ID, SBG_ID) {
            try {

                var url = "/ProgressTracker/BindTopics";
                if (comboTree1 != undefined) {
                    if (comboTree1.length != 0) {
                        comboTree1.destroy();

                    }

                }


                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    data: { TRM_ID: TRM_ID, SBG_ID: SBG_ID },
                    success: function (options) {
                        //debugger;
                        if (options.length > 0) {


                            comboTree1 = $('#txtTopics').comboTree({
                                source: options,
                                //isMultiple: true,
                                //cascadeSelect: true
                            });

                            //comboTree1 = $('#txtTopics').comboTree('loadData', options);
                            $('#divTopics').removeClass('d-none');
                        }
                        else {
                            $('#divTopics').addClass('d-none');
                        }


                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });



                return false;
            } catch (e) {
                alert(e);
            }


        },
        DisplayProgressTracker = function DisplayProgressTracker() {
            //debugger;
            var grdSection = [];
            var ACD_ID = $('#AcademicYear').val();
            var SGR_ID = $('#SubjectGroups').val();
            var SBG_ID = $('#Subjects').val();
            var TOPIC_ID = "";
            var txtvalue = $('#txtTopics').val();
            if (comboTree1 != undefined && txtvalue != "") {
                TOPIC_ID = comboTree1.getSelectedItemsId() == false ? "" : comboTree1.getSelectedItemsId();
            }

            var AGE_BAND_ID = $('#AgeBand').val();

            var STEPS = $("#Steps").val();
            var TSM_ID = $('#Term').find(':selected').attr('data-tsmid');
            var GRD_ID = $('#Grades').val();
            var Data = GRD_ID
            // alert(Data);
            $.each(Data.split(/\|/), function (i, val) {
                // alert(val);
                grdSection.push(val);
            });
            $.post('/ProgressTracker/Progresstrack', { STEPS: $("#Steps").val(), ACD_ID: ACD_ID, SGR_ID: SGR_ID, SBG_ID: SBG_ID, TOPIC_ID: TOPIC_ID, AGE_BAND_ID: AGE_BAND_ID, TSM_ID: TSM_ID, GRD_ID: grdSection[0] }, function (response) {
                $('#divIndexDetails').hide();
                $('#divDisplayTracker').removeClass('d-none');
                $('#divPartialProgressTracker').html(response);
                $('.selectpicker').selectpicker('refresh');

            });
            //$.ajax({
            //    url: '/ProgressTracker/Progresstrack',
            //    type: "POST",
            //    contentType: "text/html; charset=utf-8",
            //    success: function (data) {
            //        $('#divIndexDetails').hide();
            //        $('#divDisplayTracker').removeClass('d-none');
            //        $('#divPartialProgressTracker').html(data);
            //        $('.selectpicker').selectpicker('refresh');

            //    }
            //});

        },
        ddlChanged = function ddlChanged(ddl) {
            var color = $("option:selected", ddl).attr("style");
            $(ddl).parent().find(".dropdown-toggle").attr("style", color);
        },
        SetColor = function SetColor() {
            $(".ProgressSelect").each(function (index, ddl) {
                var color = $("option:selected", ddl).attr("style");
                $(ddl).parent().find(".dropdown-toggle").attr("style", color);

            });
        },
        SetColorBar = function SetColorBar() {

            $(".btn-group").each(function (index, btngroup) {
                var stuId = $(btngroup).find(".btn-selected").attr('data-stuid');
                var ObjId = $(btngroup).find(".btn-selected").attr('data-objid');
                var hf_Clicked = "hf_Clicked_" + stuId + "_" + ObjId;
                var click_order = $(btngroup).find(".btn-selected").attr("data-order");
                var click_color = $(btngroup).find(".btn-selected").attr("data-color");
                $(btngroup).find("#" + hf_Clicked).val("1");
                //$(btngroup).find('button').each(function (index, el) {
                //    if ($(el).attr("data-order") <= click_order) {
                //        $(el).css("background-color", click_color);
                //        $(el).addClass('btn-selected');
                //        $(el).removeClass('btn-notselected');
                //    }
                //    else {
                //        $(el).removeClass('btn-selected');
                //        $(el).addClass('btn-notselected');
                //    }
                //});

            });

        },
        BarClick = function BarClick(btn) {
            var stuId = $(btn).attr('data-stuid');
            var ObjId = $(btn).attr('data-objid');
            var GroupId = "div_" + stuId + "_" + ObjId;
            var hf_Clicked = "hf_Clicked_" + stuId + "_" + ObjId;
            $("#" + GroupId + " button").removeClass("btn-notselected");
            $("#" + GroupId + " button").removeClass("btn-selected");
            $("#" + GroupId + " button").addClass("btn-notselected");
            $(btn).removeClass('btn-notselected');
            $(btn).addClass('btn-selected');
            $(btn).addClass('btn-value');
            $("#" + hf_Clicked).val("1");
            //------------------------------------------
            var click_order = $(btn).attr("data-order");
            var click_color = $(btn).attr("data-color");
            //$("#" + GroupId + " button").each(function (index, el) {
            //    if ($(el).attr("data-order") <= click_order) {
            //        $(el).css("background-color", click_color);
            //        $(el).addClass('btn-selected');
            //        $(el).removeClass('btn-notselected');
            //    }
            //    else {
            //        $(el).removeClass('btn-selected');
            //        $(el).addClass('btn-notselected');
            //    }
            //});
            if ($("#" + hf_Clicked).val() == "1") {
                $(btn).removeClass('btn-notselected');
            }
            else if ($("#" + hf_Clicked).val() == "0") {
                $(btn).removeClass('btn-notselected');
            }
            $("#" + GroupId + " button").find('.btn-selected').removeClass('btn-notselected');

        },
        BarOver = function BarOver(btn) {
            var stuId = $(btn).attr('data-stuid');
            var ObjId = $(btn).attr('data-objid');
            var GroupId = "div_" + stuId + "_" + ObjId;

            $(btn).removeClass('btn-notselected');
        },
        BarOut = function BarOut(btn) {
            var stuId = $(btn).attr('data-stuid');
            var ObjId = $(btn).attr('data-objid');
            var GroupId = "div_" + stuId + "_" + ObjId;
            var hf_Clicked = "hf_Clicked_" + stuId + "_" + ObjId;

            if ($("#" + hf_Clicked).val() == "1") {
                if (!$(btn).hasClass("btn-selected"))
                    $(btn).addClass('btn-notselected');
            }
            else if ($("#" + hf_Clicked).val() == "0") {
                $(btn).addClass('btn-notselected');
            }
            $("#" + GroupId + " button").find('.btn-selected').removeClass('btn-notselected');
        },
        DropDownHeaderChange = function DropDownHeaderChange(ddl, IsDropDown) {
            //debugger;

            var color = $("option:selected", ddl).attr("style");
            $(ddl).parent().find(".dropdown-toggle").attr("style", color);

            var ObjId = $(ddl).attr('data-objid');
            var selectedVal = $(ddl).val();

            if (!IsDropDown) {
                $(".btn-group[data-objid=" + ObjId + "]").each(function (index, btngroup) {
                    var stuId = $(btngroup).attr('data-stuid');
                    var hf_Clicked = "hf_Clicked_" + stuId + "_" + ObjId;
                    $('#' + hf_Clicked).val("1");
                    var cssClass = '.cls_' + ObjId + '_' + selectedVal;
                    $(btngroup).find('button').each(function (index, el) {
                        $(el).removeClass('btn-selected');
                        $(el).addClass('btn-notselected');
                    });

                    $(cssClass).removeClass('btn-notselected');
                    $(cssClass).addClass('btn-selected');



                });
            }
            else {


                $(".cls_" + ObjId + " option[value=" + selectedVal + "]").attr('selected', 'selected');
                $(".cls_" + ObjId).selectpicker('refresh');
                $(".cls_" + ObjId).parent().find(".dropdown-toggle").attr("style", color);
            }


        },
        ProgressSaver = function ProgressSaver(IsDropDown, e) {
            //debugger;
            var objListofStudentObjectData = [];
            var form = $('#frmprogressdata')[0];
            var formData = new FormData(form);
            var txtvalue = $('#txtTopics').val();
            var TOPIC_ID = "";
            if (comboTree1 != undefined && txtvalue != "") {
                TOPIC_ID = comboTree1.getSelectedItemsId() == false ? "" : comboTree1.getSelectedItemsId();
            }
            var acdId = $('#AcademicYear').val();
            var GrdId = $('#Grades').val();
            var subId = $('#Subjects').val();
            var termId = $('#Term').val();
            var grdSec = [];
            var Data = GrdId
            // alert(Data);
            $.each(Data.split(/\|/), function (i, val) {
                // alert(val);
                grdSec.push(val);
            });
            if (IsDropDown) {

                $('.clstudentObjData').each(function () {
                    var studentId = $(this).attr('data-stuid');
                    var objId = $(this).attr('data-objid');

                    var selectId = "#ddlProgressOption_" + studentId + "_" + objId;
                    var selectValue = $(selectId).val();
                    if (selectValue != "") {

                        var objStudentObjectData = {
                            StudentId: studentId,
                            ObjectiveId: objId,
                            data: selectValue,
                            SYD_ID: TOPIC_ID,
                            TermId: termId,
                            ACD_ID: acdId,
                            GRD_ID: grdSec[0],
                            SBG_ID: subId



                        }

                        objListofStudentObjectData.push(objStudentObjectData);

                    }


                });

            }
            else {
                $('.btn-selected').each(function () {

                    var studentId = $(this).attr('data-stuid');
                    var objId = $(this).attr('data-objid');
                    var selectValue = $(this).text();
                    if (selectValue != "") {

                        var objStudentObjectData = {
                            StudentId: studentId,
                            ObjectiveId: objId,
                            data: selectValue,
                            SYD_ID: TOPIC_ID,
                            TermId: termId,
                             ACD_ID: acdId,
                            GRD_ID: GrdId,
                            SBG_ID: subId
                        }
                        objListofStudentObjectData.push(objStudentObjectData);
                    }

                });


            }

            // formData.append('objListofStudentObjectData', objListofStudentObjectData);
            formData.append('objListofStudentObjectData', JSON.stringify(objListofStudentObjectData));
            formData.append('objStudentEvidenceList', JSON.stringify(objStudentEvidenceList));
            
            $.ajax({
                url: "/ProgressTracker/SaveStudentProgressData",
                data: formData,
                type: 'POST',
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                // ... Other options like success and etc
                success: function (response) {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    //debugger;

                    objStudentEvidenceList.splice(0, objStudentEvidenceList.length)
                    return false;

                }
            });
            e.preventDefault();
            e.stopImmediatePropagation();
        },

        ProgressFileUpload = function ProgressFileUpload(iUploadFile) {
            var studentId = $(iUploadFile).attr('data-stuid');
            var objId = $(iUploadFile).attr('data-objid');
            $('#fileupload_' + studentId + "_" + objId).trigger('click');
        },

        ProgressFileDownLoad = function ProgressFileDownLoad(iFileDownLoad) {



        },
        ProgressFileOnChange = function ProgressFileOnChange(inputfile) {
            //debugger;
            var file = $(inputfile);
            var length = file[0].files.length;
            var items = file[0].files;
            var studentId = $(inputfile).attr('data-stuid');
            var objId = $(inputfile).attr('data-objid');
            var fname = "";
            for (var i = 0; i < length; i++) {
                fname = items[i].name;
                var objEvidence = {

                    StudentId: studentId,
                    ObjectiveId: objId,
                    FileName: items[i].name,
                    FileSize: items[i].size,
                    FileType: items[i].type


                }

                objStudentEvidenceList.push(objEvidence);

            }

            var val = $(inputfile).val();
            $(inputfile).siblings('span').text(fname);

        }


    return {
        init: init,
        BindTopicTree: BindTopicTree,
        DisplayProgressTracker: DisplayProgressTracker,
        ddlChanged: ddlChanged,
        SetColor: SetColor,
        SetColorBar: SetColorBar,
        BarClick: BarClick,
        BarOver: BarOver,
        BarOut: BarOut,
        DropDownHeaderChange: DropDownHeaderChange,
        ProgressSaver: ProgressSaver,
        ProgressFileUpload: ProgressFileUpload,
        ProgressFileDownLoad: ProgressFileDownLoad,
        ProgressFileOnChange: ProgressFileOnChange
    };
}();

$(document).ready(function () {
    var comboTree1, comboTree2;
    ProgressTracker.init();
    //  ProgressTracker.BindTopicTree(2211, 58666);

    $('#evd_check').on('change', function () {
        //debugger;
        if ($(this).is(":checked")) {
            $('.diveveUpload').removeClass('d-none')
        }
        else {
            $('.diveveUpload').addClass('d-none')
        }
    });

    $('#blk_check').on('change', function () {
        //debugger;
        if ($(this).is(":checked")) {
            $('.ddlbulkProgressRate').removeClass('d-none')
        }
        else {
            $('.ddlbulkProgressRate').addClass('d-none')
        }
    });

    $('#iBackToSelect').on('click', function () {
        $('#divDisplayTracker').addClass('d-none');
        $('#divPartialProgressTracker').empty();
        $('#divIndexDetails').show();


    });

    $('.imgStd').on('click', function () {
        window.open("/ProgressTracker/ProgressTrackerReport");

    });

});

