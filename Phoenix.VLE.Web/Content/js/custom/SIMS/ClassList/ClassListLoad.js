﻿// Author   : Fraz Ahmed
// Date     : 20/06/2019
// Version  :  1.0

$(document).ready(function () {
    //$('#list').click(function (event) { event.preventDefault(); $('#products .item').addClass('list-group-item'); $('#list').removeClass('hidden'); $('#grid').removeClass('hidden') });
    //$('#grid').click(function (event) { event.preventDefault(); $('#products .item').removeClass('list-group-item'); $('#products .item').addClass('grid-group-item'); $('#grid').addClass('hidden'); $('#list').removeClass('hidden') });

    $("#Section").change(function () {
        var _ttid = 0;
        var _grade = $('#Grades').val();
        var _section = $('#Section').val();
        ajax_call( _ttid, _grade, _section);
    })
    $("#Grades").change(function () {
        $('#dvClassList').html('');
    })
    // image gallery
    // init the state from the input
    //Multiple Images Selection 
    $(".image-checkbox").each(function () {
        if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
            $(this).addClass('image-checkbox-checked');
        }
        else {
            $(this).removeClass('image-checkbox-checked');
        }
    });

    // sync the state to the input
    $(".image-checkbox").on("click", function (e) {
        $(this).toggleClass('image-checkbox-checked');
        var $checkbox = $(this).find('input[type="checkbox"]');
        $checkbox.prop("checked", !$checkbox.prop("checked"))

        e.preventDefault();
    });

    //Extending the :contains selector property to be case in-sensitive for the card search option. 
    $.extend($.expr[":"], {
        "containsIN": function (elem, i, match, array) {
            return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        }
    });
    //Search option for students cards
    $('#search').keyup(function () {

        $('.item').removeClass('d-none');
        var filter = $(this).val(); // get the value of the input, which we filter on
        $('#products').find('.card .card-body:not(:containsIN("' + filter + '"))').parent().parent().addClass('d-none');
        //$('#products').find('.card .card-body:containsIN("' + filter + '")').addClass('highlighter');

    })

    // Ellipsis for for student name 

    $(document).on('mouseenter', ".tooltip-name", function () {
        var $this = $(this);
        if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
            $this.tooltip({
                title: $this.text(),
                placement: "top"
            });
            $this.tooltip('show');
        }
    });
    $('.hide-name').css('width', $('.hide-name').parent().width());
});

//Ajax Call For Student Cards
function ajax_call( _ttid, _grade, _section) {
    var url = "/ClassList/GetStudentCard";
    $.ajax({
        type: 'GET',
        url: url,
        data: {  tt_id: _ttid, grade: _grade, section: _section },
        success: function (result) {
            $('#dvClassList').html('');
            $('#dvClassList').html(result);
            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (ex) {
            globalFunctions.onFailure();
        }
    });

    return false;

}

