﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />

var subjectmaster = function () {
    var init = function () {

    },
        loadSubjectMasterList = function () {
            var _columnData = [];

            _columnData.push(
                { "mData": "Curriculum_Description", "sTitle": 'Curriculum', "sWidth": "30%" },
                { "mData": "Subject_Description", "sTitle": 'Subject', "sWidth": "15%" },
                { "mData": "Subject_ShortCode", "sTitle": 'Short code', "sWidth": "15%" },
                { "mData": "IsLanguage", "sClass": "text-center", "sTitle": 'Language', "sWidth": "10%" },
                { "mData": "Subject_BoardCode", "sClass": "text-center", "sTitle": 'Board Code', "sWidth": "20%" },
                { "mData": "Actions", "sClass": "tbl-actions text-center", "sTitle": 'Action', "sWidth": "10%" }
            );
            initGrid('tbl-SubjectMaster', _columnData, '/SubjectSetting/GetSubjectMasterList');
        }
    initGrid = function (_tableId, _columnData, _url) {
        var grid = new DynamicGrid(_tableId);

        var settings = {
            columnData: _columnData,
            url: _url,
            columnSelector: false,
            //columnDefs: [{
            //    'orderable': false,
            //    'targets': [8, 9, 10]
            //}]
        };
        grid.init(settings);
    },
        onSaveSuccessSubjectMaster = function (data) {
        console.log("---------- save of subject master save ----------");
            console.log(data);
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                $(".closeRightDrawer").trigger("click");
                subjectmaster.loadSubjectMasterList();
            }
        },
        loadSubjectMasterModel = function (SubjectId) {
            $('#divAddEditSubjectMaster').html();
            $.post("/SubjectSetting/AddEditSubjectMaster", { SubjectId: SubjectId }, function (response) {
                $('#divAddEditSubjectMaster').html(response);
                $('select').selectpicker();
                $("#CurriculumId").selectpicker('refresh');
            });
        }
    return {
        init: init,
        loadSubjectMasterList: loadSubjectMasterList,
        onSaveSuccessSubjectMaster: onSaveSuccessSubjectMaster,
        loadSubjectMasterModel: loadSubjectMasterModel
    }
}();

$(document).ready(function () {
    subjectmaster.loadSubjectMasterList();
});

$("#btnRightDrawerSubjectMaster").on('click', function () {
    subjectmaster.loadSubjectMasterModel(0);
});
