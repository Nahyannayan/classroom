﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var comboTree1 = undefined;
var SubjectByGrade = function () {
    var init = function () {

        $('#tblSubjectByGrade').DataTable({
            "ordering": false,
            columnDefs: [{
                orderable: false
            }]
            //"scrollY": "500px",
            //"scrollCollapse": true,
            //"searching": false
        });
        $('.dataTables_length').addClass('bs-select');

        $(".custom-scroll-container").mCustomScrollbar({
            //setHeight: "76%",
            autoExpandScrollbar: true,
            scrollbarPosition: "inside",
            autoHideScrollbar: true,
        });
        $('.delete').hide();
        $(".toggle-row").click(function (event) {
            event.stopPropagation();
            if ($(this).children("i").hasClass('fa-plus-square')) {
                $(this).children("i").removeClass('fa-plus-square').addClass('fa-minus-square');
            } else {
                $(this).children("i").removeClass('fa-minus-square').addClass('fa-plus-square');
            }
            var $target = $(event.target);
            if ($target.closest("td").attr("colspan") > 1) {
                $target.slideUp();
            } else {
                $target.closest("tr").next().find(".subjectList").slideToggle();
            }
        });


        $('#AcademicYear').on('change', function () {

            SubjectByGrade.SubjectGradeParent();


        });
        $("#divGradeEntry").hide();
        $("#divGradeEntryh3").hide();
        $('#btnBack').on('click', function () {
            SubjectByGrade.BacktoView();
        });
    },
        GetSubjectByGradeEntry = (gradeId, streamid, subjectGradeId) => {
            var acd_Id = $('#AcademicYear').val();
            $.get("/SubjectSetting/SubjectByGradeEntry?grdId=" + gradeId + "&acdId=" + acd_Id + "&streamId=" + streamid + "&subjectGradeId=" + subjectGradeId, (response) => {
                $('#btnBack').show();
                $("#divGradeEntry").hide();
                $("#divGradeEntry").html(response);
                $('#divGradeStreamGrid').hide();
                $('#divGradeStreamGridh3').hide();
                $("#divGradeEntry").show();
                $("#divGradeEntryh3").show();
                $('.selectpicker').selectpicker('refresh');
                SubjectByGrade.BindParentSubject(acd_Id, streamid, gradeId);

                //$("#SubjectId option:contains(" + subject +")").attr('selected', 'selected');
                //$("#DepartmentId option:contains(" + department + ")").attr('selected', 'selected');
                //$('#ShortCode').val(ShortCode);
                //$('#Description').val();

                //$('#SubjectType1').checked(Major);
                //$('#SubjectType2').checked(!Major);

                //$('#parentSubject').val(parentSubject);
                //$('#ShortCode').val('ShortCode');
                //$('#ShortCode').val('ShortCode');
            })
        },
        BindParentSubject = (acdId, streamId, grdId) => {
            try {
                var url = "/SubjectSetting/BindParentOptions";
                if (comboTree1 !== undefined) {
                    if (comboTree1.length > 0) {
                        comboTree1.destroy();
                    }
                }
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    data: { acdId: acdId, streamId: streamId, grdId: grdId },
                    success: function (options) {
                        if (options.length > 0) {
                            comboTree1 = $('#parentSubjectInput').comboTree({
                                source: options
                            });
                            $('.comboTreeArrowBtn').attr('type', 'button');
                        } else {
                            $('#parentSubjectInput').comboTree();
                        }
                        var parentSubject = $('#ParentSubject').val();
                        if (parentSubject !== "") {
                            var object = comboTree1.options.source.filter(e => e.id === parseInt(parentSubject))
                            if (object.length > 0) {
                                $('#parentSubjectInput').val(object[0].title);
                            }
                        }
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });
                return false;
            } catch (e) {
                alert(e);
            }
        },
        SubjectGradeParent = function SubjectGradeParent(element) {
            var acd_Id = $('#AcademicYear').val();
            if (acd_Id != "" || acd_Id != 0) {
                $.get("/SubjectSetting/SubjectByGradeParent", { ACD_ID: acd_Id }, function (response) {
                    $('#divGradeStreamGrid').html(response);
                    if (element !== undefined)//to open the accordian menu
                        element.click();
                })
            }
        },
        GetStreamFromGrade = () => {
            $("#FromStreamId").empty();
            var acd_Id = $('#AcademicYear').val();
            var grd_id = $('#FromGradeId option:selected').val();
            try {
                var url = "/SubjectSetting/GetStreamFromGrade";
                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    url: url,
                    contentType: 'application/json;charset=utf-8',
                    data: { ACD_ID: acd_Id, GRD_ID: grd_id },
                    success: function (Sections) {
                        $("#FromStreamId").append('<option value="0">Select</option>');
                        $.each(Sections, function (i, Section) {
                            $("#FromStreamId").append('<option value="'
                                + Section.STM_ID + '">'
                                + Section.STM_DESCR + '</option>');
                        });
                        $('.selectpicker').selectpicker('refresh');
                    },
                    error: function (ex) {
                        alert('Failed.' + ex);
                    }
                });

                return false;
            } catch (e) {
                alert(e);
            }
        },
        BacktoView = () => {
            $('#divGradeStreamGrid').show();
            $('#divGradeStreamGridh3').show();
            $("#divGradeEntry").hide();
            $("#divGradeEntryh3").hide();
            $('#btnBack').hide();
        },
        onFailure = function (data) {
            console.log("Failed");
            console.log(data);
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                BacktoView();
                //var anchorTag = ($('.toggle-row').find('i').filter((i, e) => $(e).hasClass('fa-minus-square') === true)[0]).closest('a');
                SubjectByGrade.SubjectGradeParent();
            }
        },
        subjectByGradeEntry = () => {
            var optionNameSelectedValue = $('#OptionName').selectpicker().val();
            if (optionNameSelectedValue !== undefined && optionNameSelectedValue.length > 0) {
                var optionNameSelectedInXML = optionNameSelectedValue.reduce((result, el) => {
                    return result + `<ID><SGO_OPT_ID>${parseInt(el)}</ID></SGO_OPT_ID>\n`
                }, '')
                optionNameSelectedValue = "<IDs>" + optionNameSelectedInXML + "</IDs>"
            }
            var subjectByGradeEntryModel = {
                SubjectId: $('#SubjectId').val(),
                ShortCode: $('#ShortCode').val(),
                DepartmentId: $('#DepartmentId').val(),
                Description: $('#Description').val(),
                SubjectType: ($('#SubjectTypeMajor')[0] !== undefined && $('#SubjectTypeMajor')[0].checked) ? true : false,
                ParentSubjectId: comboTree1._selectedItem.id !== undefined ? comboTree1._selectedItem.id : 0,
                OptionName: optionNameSelectedValue,
                MinimumMarks: $('#MinimumMarks').val(),
                DisplaySubjectInTC: $('#DisplaySubjectInTC').val(),
                DisplaySubjectInReportCard: $('#DisplaySubjectInReportCard').val(),
                DisplayExcel: $('#DisplayExcel').val(),
                BtecSubject: $('#BtecSubject').val(),
                MarkAsOptional: $('#MarkAsOptional').val(),

                DisplayMarksInReportCard: $('#DisplayMarksInReportCard').checked,
                DisplayGradeInReportCard: $('#DisplayGradeInReportCard').checked,
                EnterMarksGarde: $('#EnterMarksGarde').val(),
                EnterMarksGarde: $('#EnterMarksGarde').val()
            }
            console.log(subjectByGradeEntryModel);
        },
        CopyGrade = () => {
            var fromGradeId = $('#FromGradeId option:selected').val();
            var toStreamId = $('#FromStreamId option:selected').val();
            var toStm_Id = $('#STM_ID').val();
            var toGrd_id = $('#Grade').val();
            var acd_Id = $('#AcademicYear').val();
            $.get("/SubjectSetting/CopySubjectByGrade",
                {
                    ACD_ID: acd_Id,
                    From_GRD_ID: fromGradeId,
                    From_STM_ID: toStreamId,
                    To_GRD_ID: toGrd_id,
                    To_STM_ID: toStm_Id
                },
                (response) => {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    SubjectByGrade.SubjectGradeChild();
                },
                (error) => {
                    globalFunctions.showErrors(error.NotificationType, error.Message);
                });
        },
        SubjectGradeChild = () => {
            var acdId = $("#AcademicYear").val();
            var stmId = $("#STM_ID").val();
            var grdId = $("#Grade").val();
            if (acdId !== "" || acdId !== 0 && stmId !== "" && grdId !== "") {
                $.get("/SubjectSetting/SubjectByGradeChild",
                    { Acd_Id: acdId, Grade_Id: grdId, STM_Id: stmId },
                    function(response) {
                        $("#SubjectByGradeChildDiv").html(response);
                        if ($("#SubjectByGradeChildDiv").find("tbody").find("tr:eq(0)").find("td").length === 8) {
                            SubjectByGrade.BacktoView();
                            SubjectByGrade.SubjectGradeParent();
                        }
                    }
                );
            }
        },
        DeletSubjectGradeEntry = (subjectGradeId) => {
            if (confirm("Are you sure you want to delete?")) {
                $.get('/SubjectSetting/SubjectByGradeDelete?subjectGradeId=' + subjectGradeId,
                    (response) => {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        SubjectByGrade.SubjectGradeChild();
                    });
            }
        },
    toggler = function toggler(toggle) {
        event.stopPropagation();
        if ($(toggle).children("i").hasClass('fa-plus-square')) {
            $(toggle).children("i").removeClass('fa-plus-square').addClass('fa-minus-square');
        } else {
            $(toggle).children("i").removeClass('fa-minus-square').addClass('fa-plus-square');
        }
        var $target = $(event.target);
        if ($target.closest("td").attr("colspan") > 1) {
            $target.slideUp();
        } else {
            $target.closest("tr").next().find(".subjectList").slideToggle();
        }
    }
    return {
        init: init,
        SubjectGradeParent: SubjectGradeParent,
        toggler: toggler,
        GetSubjectByGradeEntry: GetSubjectByGradeEntry,
        BacktoView: BacktoView,
        BindParentSubject: BindParentSubject,
        onFailure: onFailure,
        onSaveSuccess: onSaveSuccess,
        subjectByGradeEntry, subjectByGradeEntry,
        CopyGrade: CopyGrade,
        DeletSubjectGradeEntry: DeletSubjectGradeEntry,
        SubjectGradeChild: SubjectGradeChild,
        GetStreamFromGrade: GetStreamFromGrade
    };

}();
$(document).ready(function () {
    SubjectByGrade.init();
    SubjectByGrade.SubjectGradeParent();
});