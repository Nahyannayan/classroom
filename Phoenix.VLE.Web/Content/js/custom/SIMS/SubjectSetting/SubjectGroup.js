﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />

var subjectGroup = function () {
    var init = function () {

    },
        onSaveSubjectGroupSuccess = function (response) {
            console.log("onsave success");
            globalFunctions.showMessage(response.NotificationType, response.Message);
            location.reload();
        },
        onSaveSubjectGroupFailure = function (response) {
            console.log("onsave error");
            globalFunctions.showMessage(response.NotificationType, response.Message);
        },
        loadSubjectGroupList = function () {
            var acd_Id = $('#AcademicYear').val();

            var _columnData = [];

            _columnData.push(
                { "mData": "Group_Description", "sTitle": 'Group Name', "sWidth": "18%" },
                { "mData": "SubjectGrade_Description", "sTitle": 'Subject', "sWidth": "25%" },
                { "mData": "SubjectGrade_Parent", "sTitle": 'Parent', "sWidth": "17%" },
                { "mData": "Grade_Display", "sClass": "", "sTitle": 'Grade', "sWidth": "10%" },
                { "mData": "Shift_Description", "sClass": "", "sTitle": 'Shift', "sWidth": "20%" },
                { "mData": "Stream_Description", "sClass": "", "sTitle": 'Stream', "sWidth": "20%" },
                { "mData": "Actions", "sClass": "tbl-actions text-center", "sTitle": 'Action', "sWidth": "10%" }
            );
            initGrid('tbl-SubjectGroup', _columnData, '/SubjectSetting/GetSubjectGroupList?ACD_ID=' + acd_Id);
        }
    initGrid = function (_tableId, _columnData, _url) {
        var grid = new DynamicGrid(_tableId);

        var settings = {
            columnData: _columnData,
            url: _url,
            columnSelector: false,
            //columnDefs: [{
            //    'orderable': false,
            //    'targets': [8, 9, 10]
            //}]
        };
        grid.init(settings);
    },
        loadGrade = function (ACD_ID) {
            $.ajax({
                type: "GET",
                url: "/SubjectSetting/GetGradesByACD_ID",
                data: { ACD_ID: ACD_ID },
                dataType: 'json',
                success: function (response) {
                    console.log(response);

                    var ddlGrade = $('#ddlGrade');
                    ddlGrade.empty();
                    ddlGrade.append("<option value=''>Select Grade</option>");

                    $.each(response, function (i, item) {
                        ddlGrade.append($('<option></option>').val(item.Value).html(item.Text)
                        );
                    });
                    $('#ddlGrade').val(null);
                    $('#ddlShift').val(null);
                    $('#ddlStream').val(null);
                    $('#ddlSubject').val(null);
                    $('.selectpicker').selectpicker('refresh');
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        },
        loadStream = function (ACD_ID, GRD_ID) {
            $.ajax({
                type: "GET",
                url: "/SubjectSetting/BindFilterById",
                data: { ACD_ID: ACD_ID, GRD_ID: GRD_ID },
                dataType: 'json',
                success: function (response) {
                    console.log(response);

                    var ddlGrade = $('#ddlStream');
                    ddlGrade.empty();
                    ddlGrade.append("<option value=''>Select Stream</option>");

                    $.each(response.Stream_Filter, function (i, item) {
                        ddlGrade.append($('<option></option>').val(item.Value).html(item.Text)
                        );
                    });
                    $('#ddlStream').selectpicker('refresh');

                    var ddlSubject = $('#ddlSubject');
                    ddlSubject.empty();
                    ddlSubject.append("<option value=''>Select Subject</option>");
                    $.each(response.Subject_Filter, function (i, item) {
                        ddlSubject.append($('<option></option>').val(item.Value).html(item.Text)
                        );
                    });
                    $('#ddlSubject').selectpicker('refresh');

                    var ddlShift = $('#ddlShift');
                    ddlShift.empty();
                    ddlShift.append("<option value=''>Select Shift</option>");
                    $.each(response.Shift_Filter, function (i, item) {
                        ddlShift.append($('<option></option>').val(item.Value).html(item.Text)
                        );
                    });
                    $('#ddlShift').selectpicker('refresh');
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        },
        loadAssignTeacherModel = function () {
            $('#divAssignTeacher').html();
            $.post("/SubjectSetting/AssignTeacher", function (response) {
                $('#divAssignTeacher').html(response);
                var TeacherId = $("#SubjectGroupTeacher_TeacherId").val();
                if (TeacherId != 0 || TeacherId != '' || TeacherId != "") {
                    $("#ddlTeacher").val(TeacherId);
                    $("#txtDate").val($("#SubjectGroupTeacher_FromDate").val());
                    $("#txtSchedule").val($("#SubjectGroupTeacher_GroupTeacherSchedule").val());
                    $("#txtRoomNo").val($("#SubjectGroupTeacher_GroupTeacherRoom").val());
                    $(".select-label").addClass("active");
                }
                $('.selectpicker').selectpicker('refresh');
                subjectGroup.loadSubjectGroupTeachersList();
            });
        },
        openSubjectGroupModel = function (SGR_ID) {
            var ACD_ID = $("#AcademicYear").val();
            $('#divAddEditSubjectGroup').html();
            $.post("/SubjectSetting/AddEditSubjectGroup", { SGR_ID: SGR_ID, ACD_ID: ACD_ID }, function (response) {
                $("#divSubjectGroupGrid").hide();
                $('#divAddEditSubjectGroup').html(response);
                $('.selectpicker').selectpicker('refresh');
            });
        },
        loadSubjectGroupTeachersList = function () {

            var SGR_ID = $('#ddlSubject').val();

            var _columnData = [];

            _columnData.push(
                { "mData": "TeacherId", "sTitle": 'Teacher Id', "bVisible": false },
                { "mData": "TeacherName", "sTitle": 'Teacher Name' },
                { "mData": "FromDate", "sTitle": 'Date' },
                { "mData": "GroupTeacherSchedule", "sTitle": 'Schedule', "bVisible": false },
                { "mData": "GroupTeacherRoom", "sTitle": 'Room No', "bVisible": false },
                { "mData": "Actions", "sClass": "tbl-actions text-center", "sTitle": 'Action' }
            );
            initGridTeacherList('tbl-TeacherList', _columnData, '/SubjectSetting/LoadSubjectGroupTeachersList?SGR_ID=' + SGR_ID);
        }
    initGridTeacherList = function (_tableId, _columnData, _url) {
        var grid = new DynamicGrid(_tableId);

        var settingsGridTeacherList = {
            columnData: _columnData,
            url: _url,
            columnSelector: false,
            searching: false,
            paging: false,
            info: false,
            //columnDefs: [{
            //    "targets": [2],
            //    "bVisible": false
            //}]
        };
        grid.init(settingsGridTeacherList);
    },
        assignTeacher = function () {
            var IsTeacherSelected = subjectGroup.validateField($("#ddlTeacher"));
            var IsFromDateSelected = subjectGroup.validateField($("#txtDate"));
            if (IsTeacherSelected || IsFromDateSelected) {
                return false;
            }
            else {
                $("#SubjectGroupTeacher_GroupTeacherId").val($("#hdnSGS_ID").val());
                $("#SubjectGroupTeacher_TeacherId").val($("#ddlTeacher").val());
                $("#SubjectGroupTeacher_FromDate").val($("#txtDate").val());
                $("#SubjectGroupTeacher_GroupTeacherSchedule").val($("#txtSchedule").val());
                $("#SubjectGroupTeacher_GroupTeacherRoom").val($("#txtRoomNo").val());
                $("#BindSelectedTeacher").html($("#ddlTeacher option:selected").text());
                $("#assignTeacherModal").modal('hide');
            }
        },
        editAssignedTeacher = function (GroupId, TeacherId, source) {
            var table = $("#tbl-TeacherList").DataTable();
            var tableData = table.rows($(source).parent().parent()).data()[0];
            var FromDate = tableData.FromDate;
            var GroupTeacherSchedule = tableData.GroupTeacherSchedule;
            var GroupTeacherRoom = tableData.GroupTeacherRoom;
            $("#hdnSGS_ID").val(GroupId);
            $("#ddlTeacher").val(TeacherId);
            $("#txtDate").val(FromDate);
            $("#txtSchedule").val(GroupTeacherSchedule);
            $("#txtRoomNo").val(GroupTeacherRoom);
            $('#ddlTeacher').selectpicker('refresh');
        },
        unAssignTeacher = function (GroupId, TeacherId) {
            $.ajax({
                type: "GET",
                url: "/SubjectSetting/UnAssignGroupTeacher",
                data: { GroupId: GroupId, TeacherId: TeacherId },
                dataType: 'json',
                success: function (response) {
                    if (response.Success) {
                        $("#assignTeacherModal").modal('hide');
                    }
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        },
        validateField = function (element) {
            var IsValid = false;
            if ($(element).val() == "0" || $(element).val() == '0' || $(element).val() == "" || $(element).val() == '' || $(element).val() == undefined || $(element).val() == null) {
                IsValid = true;
                if ($(element).parent().find('.field-validation-error').length == 0)
                    $(element).parent().append('<span class=\"field-validation-error\"><span>Mandatory field</span></span>');
            }
            else if ($(element).parent().find('.field-validation-error').length > 0) {
                $(element).parent().find('.field-validation-error')[0].remove();
            }
            return IsValid;
        },
        loadSubjectGroupStudentList = function () {
            $('#divAllocatedStudent').html();
            $.post("/SubjectSetting/LoadAllocateStudentList",
                { ACD_ID: $("#ddlAcademicYear").val(), GRD_ID: $("#ddlGrade").val(), SHF_ID: $("#ddlShift").val(), STM_ID: $("#ddlStream").val(), SBG_ID: $("#ddlSubject").val(), SGR_ID: $("#GroupId").val() },
                function (response) {
                    $('#divAllocatedStudent').html(response);
                    $('.selectpicker').selectpicker('refresh');
                    subjectGroup.setCustomScrollbar();
                });
        },
        setCustomScrollbar = function () {
            $(".student-allocation-wrapper").mCustomScrollbar({
                setHeight: "350",
                autoExpandScrollbar: true
            });
        },
        sortStudentList = function () {
            $("#StudentList").html($("#StudentList .custom-checkbox").sort(function (a, b) {
                return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
            }));
        },
        sortSelectedStudentList = function () {
            $("#SelectedStudentList").html($("#SelectedStudentList .custom-checkbox").sort(function (a, b) {
                return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
            }));
        },
        addStudentInList = function () {
            var $selectedStudentList = $("#StudentList input[type='checkbox']:checked").parent('.custom-checkbox');
            var SelectedStudentListId = $("#SelectedStudentListId").val().split(',');

            if ($selectedStudentList != null && $selectedStudentList.length > 0) {

                $selectedStudentList.each(function (i, elem) {
                    if ($("#AddedStudentListId").val().indexOf("") == -1) {

                        if (SelectedStudentListId.indexOf($(this).children("input[type='checkbox']").val()) == -1) {
                            $("#AddedStudentListId").val($(this).children("input[type='checkbox']").val() + ",");
                            $("#AddedStudentListSSD").val($(this).children("input[type='checkbox']").data("studentgroupid") + ",");
                        }

                        if ($("#SelectedStudentListId").val().indexOf($(this).children("input[type='checkbox']").val() + ",") != -1) {
                            $("#RemovedStudentListId").val($("#RemovedStudentListId").val().replace($(this).children("input[type='checkbox']").val() + ",", ""));
                            $("#RemovedStudentListSSD").val($("#RemovedStudentListSSD").val().replace($(this).children("input[type='checkbox']").data("studentgroupid") + ",", ""));
                        }
                    }
                    else {
                        if (SelectedStudentListId.indexOf($(this).children("input[type='checkbox']").val()) == -1) {
                            $("#AddedStudentListId").val($("#AddedStudentListId").val() + $(this).children("input[type='checkbox']").val() + ",");
                            $("#AddedStudentListSSD").val($("#AddedStudentListSSD").val() + $(this).children("input[type='checkbox']").data("studentgroupid") + ",");
                        }
                        if ($("#SelectedStudentListId").val().indexOf($(this).children("input[type='checkbox']").val() + ",") != -1) {
                            $("#RemovedStudentListId").val($("#RemovedStudentListId").val().replace($(this).children("input[type='checkbox']").val() + ",", ""));
                            $("#RemovedStudentListSSD").val($("#RemovedStudentListSSD").val().replace($(this).children("input[type='checkbox']").data("studentgroupid") + ",", ""));
                        }
                    }
                });


                $("#SelectedStudentList").append($selectedStudentList);
                $("#SelectedStudentList").html($("#SelectedStudentList .custom-checkbox").sort(function (a, b) {
                    return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
                }));
                if ($("#StudentList input[type='checkbox']").parent('.custom-checkbox').length == 0) {
                    $("#chkSelectAllStudentToAdd").prop('checked', false);
                }
                $("#SelectedStudentList input[type='checkbox']").prop('checked', $('#chkSelectAllStudentToRemove').is(':checked'));
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.selectAdd);
            }
        },
        removeStudentFromList = function () {
            var $selectedStudentList = $("#SelectedStudentList input[type='checkbox']:checked").parent('.custom-checkbox');

            if ($selectedStudentList != null && $selectedStudentList.length > 0) {

                $selectedStudentList.each(function (i, elem) {
                    if ($("#AddedStudentListId").val().indexOf($(this).children("input[type='checkbox']").val() + ",") != -1) {
                        $("#AddedStudentListId").val($("#AddedStudentListId").val().replace($(this).children("input[type='checkbox']").val() + ",", ""));
                        $("#AddedStudentListSSD").val($("#AddedStudentListSSD").val().replace($(this).children("input[type='checkbox']").data("studentgroupid") + ",", ""));
                    }
                    else if ($("#SelectedStudentListId").val().indexOf($(this).children("input[type='checkbox']").val() + ",") != -1) {
                        $("#RemovedStudentListId").val($("#RemovedStudentListId").val() + $(this).children("input[type='checkbox']").val() + ",");
                        $("#RemovedStudentListSSD").val($("#RemovedStudentListSSD").val() + $(this).children("input[type='checkbox']").data("studentgroupid") + ",");
                    }
                });

                $("#StudentList").append($selectedStudentList);
                $("#StudentList").html($("#StudentList .custom-checkbox").sort(function (a, b) {
                    return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
                }));
                if ($("#SelectedStudentList input[type='checkbox']").parent('.custom-checkbox').length == 0) {
                    $("#chkSelectAllStudentToRemove").prop('checked', false);
                }
                $("#StudentList input[type='checkbox']").prop('checked', $('#chkSelectAllStudentToAdd').is(':checked'));
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.selectRemove);
            }
        },
        searchInStudentList = function (e) {
            var searchText = $(e).val().toLowerCase();
            $('#StudentList div').each(function () {
                var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
                $(this).toggle(showCurrentLi);
            });
        },
        searchInSelectedStudentList = function (e) {
            var searchText = $(e).val().toLowerCase();
            $('#SelectedStudentList div').each(function () {
                var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
                $(this).toggle(showCurrentLi);
            });
        },
        submitSubjectGroupForm = function () {
            console.log("submit form");
            //---------------------Added student--------------------------

            var AddedStudentListId = $("#AddedStudentListId").val().split(',');
            var AddedStudentListSSD = $("#AddedStudentListSSD").val().split(',');

            AddedStudentListId.forEach(function (item, index) {
                if (AddedStudentListId[index] === "" || AddedStudentListId[index] == undefined) {
                    AddedStudentListId.splice(index, 1);
                }
            });

            AddedStudentListSSD.forEach(function (item, index) {
                if (AddedStudentListSSD[index] === "" || AddedStudentListSSD[index] == undefined) {
                    AddedStudentListSSD.splice(index, 1);
                }
            });

            var optionNameSelectedValue = "";

            if (AddedStudentListId.length > 0) {
                var optionNameSelectedInXML = AddedStudentListId.reduce((result, value, index) => {
                    var SSD = 0;
                    if (AddedStudentListSSD.length) {
                        SSD = parseInt(AddedStudentListSSD[index]);
                    }
                    return result + `<IDs><SSD_ID>${parseInt(SSD)}</SSD_ID><STU_ID>${parseInt(value)}</STU_ID></IDs>`;
                },
                    '');
                optionNameSelectedValue = "<DATA>" + optionNameSelectedInXML + "</DATA>";
            }
            console.log(optionNameSelectedValue);
            $("#SelectedStudentListXML").val(optionNameSelectedValue);

            //---------------------Removed student--------------------------

            var RemovedStudentListId = $("#RemovedStudentListId").val().split(',');
            var RemovedStudentListSSD = $("#RemovedStudentListSSD").val().split(',');

            RemovedStudentListId.forEach(function (item, index) {
                if (RemovedStudentListId[index] === "" || RemovedStudentListId[index] == undefined) {
                    RemovedStudentListId.splice(index, 1);
                }
            });

            RemovedStudentListSSD.forEach(function (item, index) {
                if (RemovedStudentListSSD[index] === "" || RemovedStudentListSSD[index] == undefined) {
                    RemovedStudentListSSD.splice(index, 1);
                }
            });

            var optionNameSelectedValue = "";

            if (RemovedStudentListId.length > 0) {
                var optionNameSelectedInXML = RemovedStudentListId.reduce((result, value, index) => {
                    var SSD = 0;
                    if (RemovedStudentListSSD.length) {
                        SSD = parseInt(RemovedStudentListSSD[index]);
                    }
                    return result + `<IDs><SSD_ID>${parseInt(SSD)}</SSD_ID><STU_ID>${parseInt(value)}</STU_ID></IDs>`;
                },
                    '');
                optionNameSelectedValue = "<DATA>" + optionNameSelectedInXML + "</DATA>";
            }
            console.log(optionNameSelectedValue);
            $("#RemovedStudentListXML").val(optionNameSelectedValue);
            //-----------------------------------------------
            $("#frmSubjectGroup").submit();
        }
    return {
        init: init,
        onSaveSubjectGroupSuccess: onSaveSubjectGroupSuccess,
        onSaveSubjectGroupFailure: onSaveSubjectGroupFailure,
        loadSubjectGroupList: loadSubjectGroupList,
        loadGrade: loadGrade,
        loadStream: loadStream,
        loadAssignTeacherModel: loadAssignTeacherModel,
        openSubjectGroupModel: openSubjectGroupModel,
        loadSubjectGroupTeachersList: loadSubjectGroupTeachersList,
        assignTeacher: assignTeacher,
        editAssignedTeacher: editAssignedTeacher,
        unAssignTeacher: unAssignTeacher,
        validateField: validateField,
        loadSubjectGroupStudentList: loadSubjectGroupStudentList,
        setCustomScrollbar: setCustomScrollbar,
        sortStudentList: sortStudentList,
        sortSelectedStudentList: sortSelectedStudentList,
        addStudentInList: addStudentInList,
        removeStudentFromList: removeStudentFromList,
        searchInStudentList: searchInStudentList,
        searchInSelectedStudentList: searchInSelectedStudentList,
        submitSubjectGroupForm: submitSubjectGroupForm
    }
}();

$("#AcademicYear").on('change', function () {
    subjectGroup.loadSubjectGroupList();
});

$("#ddlAcademicYear").on('change', function () {
    subjectGroup.loadGrade($(this).val());
});

$("#ddlGrade").on('change', function () {
    $("#Grade_Display").val($("#ddlGrade :selected").text());
    subjectGroup.loadStream($("#ddlAcademicYear").val(), $(this).val());
});

$("#ddlShift").on('change', function () {
    $("#Shift_Description").val($("#ddlShift :selected").text());
});

$("#ddlStream").on('change', function () {
    $("#Stream_Description").val($("#ddlStream :selected").text());
});

$("#ddlSubject").on('change', function () {
    $("#SubjectGrade_Description").val($("#ddlSubject :selected").text());
    var IsSubjectSelected = subjectGroup.validateField($("#ddlSubject"));
    subjectGroup.loadSubjectGroupStudentList()
});

$("#btnAssignTeacher").on('click', function () {
    var IsSubjectSelected = subjectGroup.validateField($("#ddlSubject"));
    if (IsSubjectSelected)
        return false;
    else
        subjectGroup.loadAssignTeacherModel();
});

$("#btnOpenSubjectGroupModel").on('click', function () {
    subjectGroup.openSubjectGroupModel(0);
});

$("#btnAssignNow").on('click', function () {
    subjectGroup.assignTeacher();
});

$('#chkSelectAllStudentToAdd').on('change', function (e) {
    var $inputs = $('#StudentList input[type=checkbox]');
    if (e.originalEvent === undefined) {
        var allChecked = true;
        $inputs.each(function () {
            allChecked = allChecked && this.checked;
        });
        this.checked = allChecked;
    } else {
        $inputs.prop('checked', this.checked);
    }
});

$('#chkSelectAllStudentToRemove').on('change', function (e) {
    var $inputs = $('#SelectedStudentList input[type=checkbox]');
    if (e.originalEvent === undefined) {
        var allChecked = true;
        $inputs.each(function () {
            allChecked = allChecked && this.checked;
        });
        this.checked = allChecked;
    } else {
        $inputs.prop('checked', this.checked);
    }
});

