﻿
/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var sortedQuizQuestionIds = [];
var targetResource = true;
var targetResourceId = '';
var _dataTableLangSettings = {
    "sEmptyTable": translatedResources.noData,
    "sInfo": translatedResources.sInfo,
    "sInfoEmpty": translatedResources.sInfoEmpty,
    "sInfoFiltered": translatedResources.sInfoFiltered,
    "sInfoPostFix": "",
    "sInfoThousands": ",",
    "sLengthMenu": translatedResources.sLengthMenu,
    "sLoadingRecords": "Loading...",
    "sProcessing": "Processing...",
    "sSearch": translatedResources.search,
    "sZeroRecords": translatedResources.sZeroRecords,
    "oPaginate": {
        "sFirst": translatedResources.first,
        "sLast": translatedResources.last,
        "sNext": translatedResources.next,
        "sPrevious": translatedResources.previous
    },
    "oAria": {
        "sSortAscending": translatedResources.sSortAscending,
        "sSortDescending": translatedResources.sSortDescending
    }
};
var quizquestions = function () {

    addQuizQuestionsPopup = function (source) {
        var title = 'Add ' + translatedResources.QuizQuestions;
        globalFunctions.loadPopup(source, '/Quiz/QuizQuestions/AddQuizQuestion', title, 'quiz-question-dailog');

        $(source).off("modalLoaded");
        $(source).on("modalLoaded", function () {
            $('.selectpicker').selectpicker('refresh');
        });

    },
        editQuizQuestionsPopup = function (source, id) {
            id = btoa(id);
            window.location.href = '/Quiz/QuizQuestions/EditQuizQuestion?id=' + id;
            //var title = 'Edit ' + translatedResources.QuizQuestions;
            //globalFunctions.loadPopup(source, '/Quiz/QuizQuestions/EditQuizQuestion?id=' + id, title, 'quiz-question-dailog');


            //$(source).off("modalLoaded");
            //$(source).on("modalLoaded", function () {
            //    $('.selectpicker').selectpicker('refresh');
            //});
        },
        deleteObjective = function (objectiveId) {
            $.ajax({
                type: 'GET',
                url: '/Quiz/QuizQuestions/DeleteObjective?objectiveId=' + objectiveId,
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    $("#selectedObjectiveDetails").html(data);
                    globalFunctions.showSuccessMessage(translatedResources.DeleteObjectiveSuccess);
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
        },
        deleteMTPResource = function (resourceId) {
            //debugger;
            $.ajax({
                type: 'POST',
                url: '/Quiz/QuizQuestions/DeleteMTPResource',
                data: { ResourceId: resourceId },
                success: function (data) {
                    globalFunctions.showSuccessMessage(translatedResources.DeletedSuccessfully);
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
        },
        postObjective = function () {
            //debugger;
            var mappingDetails = [];
            if ($("#topicAccordion input:checkbox.level-3:visible").length > 0) {
                $("#topicAccordion input:checkbox.level-3:visible").each(function () {

                    var lessonId = $(this).data('lessonid');
                    var isLesson = $(this).data('islesson');
                    var lessonname = $(this).data('lessonname');

                    if ($(this).is(":checked")) {
                        if (isLesson === "True") {
                            mappingDetails.push({
                                ObjectiveId: lessonId,
                                ObjectiveName: lessonname,
                                isSelected: true
                            });
                        }
                    }
                    else {
                        mappingDetails = mappingDetails.filter(function (item) {
                            return item.ObjectiveId !== lessonId;
                        });
                    }

                });
            }
            if ($("#topicAccordion input:checkbox.level-2:visible").length > 0) {
                $("#topicAccordion input:checkbox.level-2:visible").each(function () {

                    var lessonId = $(this).data('lessonid');
                    var isLesson = $(this).data('islesson');
                    var lessonname = $(this).data('lessonname');

                    if ($(this).is(":checked")) {
                        if (isLesson === "True") {
                            mappingDetails.push({
                                ObjectiveId: lessonId,
                                ObjectiveName: lessonname,
                                isSelected: true
                            });
                        }
                    }
                    else {
                        mappingDetails = mappingDetails.filter(function (item) {
                            return item.LessonId !== lessonId;
                        });
                    }

                });
            }
            var saveparams = {
                mappingDetails: mappingDetails
            };

            if (true) {
                $.ajax({
                    type: 'POST',
                    url: '/Quiz/QuizQuestions/SaveObjectives',
                    async: false,
                    contentType: 'application/json',
                    data: JSON.stringify(saveparams),
                    success: function (data) {
                        $("#selectedObjectiveDetails").html(data);
                        $("#myModal").modal('hide');
                        $("#scrollableTopic").mCustomScrollbar({
                            setHeight: "100",
                        })
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure(data, xhr, status);
                    }
                });
                if ($("#CourseIds").val() != 0 && $("#IsPool").val() === "True") {
                    quizquestions.filterQuestion();
                }
            }
        },
        filterObjective = function () {
            //debugger;
            var mappingDetails = [];
            $("#tbl_permissionDataList >.permission-checkbox input:checkbox:visible").each(function () {
                mappingDetails.push({
                    ObjectiveId: $(this).data('objectiveid'),
                    ObjectiveName: $(this).data('objectivename'),
                    subjectId: $(this).data('subjectid'),
                    isSelected: true
                });
            });
            var subjectIds = $("#SubjectIds").val();
            var saveparams = {
                mappingDetails: mappingDetails,
                subjectIds: subjectIds
            };
            $.ajax({
                type: 'POST',
                url: '/Quiz/QuizQuestions/FilterObjectives',
                async: false,
                contentType: 'application/json',
                data: JSON.stringify(saveparams),
                success: function (data) {
                    $("#selectedObjectiveDetails").html(data);
                    $("#myModal").modal('hide');
                    $("#scrollableTopic").mCustomScrollbar({
                        setHeight: "100",
                    })
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
            //if ($("#SubjectIds").val() != 0 && $("#IsPool").val() === "True") {
            //    quizquestions.filterQuestion();
            //}
        },
        deleteQuizQuestionData = function (source, quizQuestionId, quizId) {
            //if (globalFunctions.isValueValid(quizQuestionId) && globalFunctions.isValueValid(quizId)) {
            //        //globalFunctions.deleteItem('Quiz', '/Quiz/QuizQuestions/deleteQuizQuestionData', 'tbl-QuizQuestion', source, id);
            //}
            if (globalFunctions.isValueValid(quizQuestionId) && globalFunctions.isValueValid(quizId)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        type: 'POST',
                        url: '/Quiz/QuizQuestions/DeleteQuizQuestion',
                        data: { QuizQuestionId: quizQuestionId, QuizId: quizId },
                        success: function (data) {
                            //debugger;
                            if (data.Success === true) {
                                window.location.reload();
                            }
                            else {
                                globalFunctions.onFailure();
                            }
                        },
                        error: function (data, xhr, status) {
                            //////debugger;
                            globalFunctions.onFailure();
                        }

                    });
                })

            }
        },
        imageUploader = function () {
            $(".quizImgUploader").fileinput({
                language: translatedResources.locale,
                theme: "fas",
                uploadUrl: "/Quiz/QuizQuestions/UploadMTPFiles",
                fileActionSettings: fileInputControlSettings.fileActionSettings,
                showUpload: true,
                showCaption: false,
                showMultipleNames: false,
                allowedFileExtensions: ["jpg", "jpeg", "gif", "png"],
                uploadAsync: true,
                minFileCount: 1,
                maxFileCount: 1,
                browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
                removeClass: "btn btn-sm m-0 z-depth-0 d-none",
                uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
                cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
                overwriteInitial: false,
                initialPreviewAsData: false, // defaults markup
                initialPreviewConfig: [{
                    frameAttr: {
                        title: 'My Custom Title',
                    }
                }],
                preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
                previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
                previewSettings: fileInputControlSettings.previewSettings,
                previewFileExtSettings: fileInputControlSettings.previewFileExtSettings
            }).on('fileuploaded', function (event, data, previewId, index) {
                var fileName = data.response;
                var resourceID = '#' + this.dataset.controlname;
                var fileId = '#' + this.dataset.controlid;
                $(fileId).addClass('disabled');
                if (targetResourceId != resourceID) {
                    targetResource = true;
                }
                targetResourceId = resourceID;
                $(resourceID).val(fileName);
                if (targetResource) {
                    globalFunctions.showSuccessMessage(translatedResources.FileUploaded);
                }
                targetResource = false;
                //var previewImage = '#' + this.dataset.previewimage
                //    $(previewImage).html('<div class="quiz-ans-img bg-light rounded-2x" id="imageGrid2">< img src = "/Resources/QuizMTPQuestion/32685882-1810-4f5b-92ad-284e094a1d7a_summer.jpg" id = "editResource2" > <a href="javascript:void(0)" onclick="toggleFileController($(this),2,4171)"><i class="fas fa-trash"></i></a>                  </div >');
            });
            $('.quizImgUploader').on('filedeleted', function (event, data, previewId, index) {
                //debugger;
                //console.log('Key = ' + key);
            });

            $(document).on('click', '.kv-file-remove', function (event, data, previewId, index) {
                //debugger;
                var fileId = '#' + $(this).closest('.quizFileUploader').find('input.quizImgUploader').data('controlid');
                var resourceID = '#' + $(this).closest('.quizFileUploader').find('input.quizImgUploader').data('controlname');
                $(fileId).removeClass('disabled');
                $(resourceID).val('');
            });
        },
        FromQuestionPool = function (source, quizId) {
            quizId = btoa(quizId);
            window.location.href = '/Quiz/QuizQuestions/PoolQuestions?quiz=' + quizId;
            $('#AjaxLoader').fadeIn(500, function () { });
        },
        filterQuestion = function () {
            var listCourse = $("#CourseIds").val();
            if ($("#IsPool").val() === "True") {
                if (listCourse.length != 0) {
                    $.ajax({
                        traditional: true,
                        data: { courseIds: listCourse, quizId: $("#QuizId").val() },
                        url: '/Quiz/QuizQuestions/filterQuestionWithSubjectAndCurriculum',
                        success: function (result) {
                            $('#unassignedQuestionList').html(result);
                            $('#selectAllUnassignedQuestions').change(function (e) {
                                var $inputs = $('#unassignedQuestionList input[type=checkbox]');
                                if (e.originalEvent === undefined) {
                                    var allChecked = true;
                                    $inputs.each(function () {
                                        allChecked = allChecked && this.checked;
                                    });
                                    this.checked = allChecked;
                                } else {
                                    $inputs.prop('checked', this.checked);
                                }
                            });
                            $('#unassignedQuestionList input[type=checkbox]').change(function (e) {
                                //debugger;
                                var $inputs = this;
                                //if (e.originalEvent === undefined) {
                                var allChecked = true;
                                $('#unassignedQuestionList input[type=checkbox]').each(function () {
                                    allChecked = allChecked && this.checked;
                                });
                                if (!allChecked) { $('#selectAllUnassignedQuestions').prop('checked', false); }
                                else { $('#selectAllUnassignedQuestions').prop('checked', true); }
                                //this.checked = allChecked;
                                //} else {
                                //    $inputs.prop('checked', this.checked);
                                //}
                            });
                        },
                        error: function (data) { }
                    });
                    $("#btnClearFilter").show();
                    //postObjective();
                } else {
                    clearUnAssignedQuestions();
                    $("#btnClearFilter").hide();
                    postObjective();
                }
            } else {
                if (listCourse.length == 0) {
                    postObjective();
                }
            }

        },
        clearUnAssignedQuestions = function () {
            //debugger;
            $.ajax({
                traditional: true,
                data: { quizId: $("#QuizId").val() },
                url: '/Quiz/QuizQuestions/ClearUnAssignedQuestions',
                success: function (result) {
                    $('#unassignedQuestionList').html(result);
                },
                error: function (data) { }
            });

        },
        loadTopicSubtopic = function (source, subjectId) {
            if ($("#CourseIds").val() != "") {
                var title = translatedResources.SelectCurriculum;
                globalFunctions.loadPopup(source, '/Quiz/QuizQuestions/GetUnitStructureView?courseIds=' + $("#CourseIds").val(), title, 'addtask-dialog modal-md');
                $(source).off("modalLoaded");
                $('#formModified').val(true);

                $(source).on("modalLoaded", function () {
                    formElements.feSelect();
                    $('select').selectpicker();
                });

                //select all as per accordion
                $(document).on('change', "#topicAccordion input:checkbox.level-1", function (event) {
                    var id = $(this).data("id");
                    $("#collapse_" + id + " input:checkbox").prop("checked", $(this).is(":checked"));
                });
                $(document).on('change', "#topicAccordion input:checkbox.level-2", function (event) {
                    var parentid = $(this).data("parentid");
                    if ($("#topicAccordion input:checkbox.level-2:checked").length == 0)
                        $("#heading_" + parentid + " input:checkbox").prop("checked", false);
                    $("#heading_" + parentid + " input:checkbox").prop("checked", $("#topicAccordion input:checkbox.level-2:checked").length == $("#topicAccordion input:checkbox.level-2").length);
                });
                //select all
                $(document).on("change", ".selectAll", function () {
                    $("#topicAccordion .level-1,.level-2,.level-3").prop("checked", $(this).is(":checked"));
                });
                $(document).on("change", "#topicAccordion .level-1,.level-2,.level-3", function () {
                    if ($("#topicAccordion .level-1:checked,.level-2:checked,.level-3:checked").length == 0)
                        $(".selectAll").prop("checked", false);
                    $(".selectAll").prop("checked", $("#topicAccordion .level-1:checked,.level-2:checked,.level-3:checked").length == $("#topicAccordion .level-1,.level-2,.level-3").length);
                });
                //selected lesson count
                $(document).on('change', "#topicAccordion input:checkbox,#topicAccordion input:checkbox, .selectAll", function (event) {
                    //update selected count
                    //debugger
                    var count = 0;
                    $("#topicAccordion input:checkbox.level-3").each(function () {
                        var isLesson = $(this).data('islesson');
                        if ($(this).is(":checked")) {
                            if (isLesson === "True") {
                                count++;
                            }
                        }
                    });

                    $("#topicAccordion input:checkbox.level-2").each(function () {
                        var isLesson = $(this).data('islesson');
                        if ($(this).is(":checked")) {
                            if (isLesson === "True") {
                                count++;
                            }
                        }
                    });
                    $("#item-count").html(translatedResources.TopicsSelected.replace('{0}', count));
                });
                //lession search
                $(document).on("keyup", ".searchfield", function () {
                    var source = $(".maincard[data-searchtext]");
                    var searchString = $(this).val();
                    $(source).filter(function () {
                        $(this).toggle($(this).data("searchtext").toLowerCase().indexOf(searchString.trim().toLowerCase()) > -1)
                    });
                    if ($(".maincard:visible").length === 0) {
                        $("#nodataDiv").show();
                    }
                    else {
                        $("#nodataDiv").hide();
                    }
                });

            }
            else {
                globalFunctions.showErrorMessage(translatedResources.SelectSubjectError);
            }

        },
        onFailure = function (data) {
            console.log("Failed");
            console.log(data);
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                window.location.reload();
            }
        },
        correctAnswer = function (answerid, answer, questiontype, questionid) {
            $.ajax({
                type: "POST",
                url: '/Quiz/QuizQuestions/AddCorrectAnswer',
                data: { id: answerid, response: answer, questionType: questiontype, questionId: questionid },
                dataType: "json",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        $("#myModal").modal('hide');
                    }
                }
            });
        },
        addCorrectAnswer = function (source, id) {
            var title = 'Add ' + translatedResources.correctAnswer;
            globalFunctions.loadPopup(source, '/Quiz/QuizQuestions/AddCorrectAnswer/' + id, title, 'quiz-correct-answer-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
            });
        },
        loadImportQuizPopup = function (source) {
            var title = translatedResources.ImportQuiz;
            globalFunctions.loadPopup(source, '/Quiz/QuizQuestions/LoadImportQuizPopup', title, 'import-quiz-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $("#ImportQuizQuestions").fileinput({
                    language: translatedResources.locale,
                    theme: "fas",
                    showUpload: true,
                    showCaption: false,
                    showMultipleNames: false,
                    multiple: false,
                    uploadUrl: "/Quiz/QuizQuestions/ImportQuizQuestions",
                    uploadExtraData: function () {
                        return {
                            quizId: $("#QuizId").val(),
                        };
                    },
                    fileActionSettings: {
                        showZoom: false,
                        showUpload: false,
                        //indicatorNew: "",
                        showDrag: false,
                        showRemove: false
                    },
                    allowedFileExtensions: ["xlsx", "xls"],
                    uploadAsync: true,
                    uploadedfilepath: false,
                    minFileCount: 1,
                    maxFileCount: 1,
                    browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
                    removeClass: "btn btn-sm m-0 z-depth-0",
                    uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
                    cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
                    overwriteInitial: true,
                    //previewFileIcon: '<i class="fas fa-file"></i>',
                    initialPreviewAsData: false, // defaults markup
                    initialPreviewConfig: [{
                        uploadedfilepath: false,
                        //progress: "d-none",
                        frameAttr: {
                            title: 'My Custom Title',
                        }
                    }],
                    preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
                    previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
                    previewSettings: fileInputControlSettings.previewSettings,
                    previewFileExtSettings: fileInputControlSettings.previewFileExtSettings

                }).on('fileuploaded', function (event, data, previewId, index) {
                    //debugger;
                    //$('.progress').addClass('d-none');
                    if (!data.response.Success) {
                        window.location.href = "/Quiz/QuizQuestions/DownloadImportFile";
                    }
                    globalFunctions.showMessage(data.response.NotificationType, data.response.Message);
                    if (data.response.Message != translatedResources.InvalidFormatMessage && data.response.Message != translatedResources.InvalidColumnMessage) {
                        $('#myModal').modal('hide');
                        if (data.response.Success) {
                            window.location.reload();
                        }
                    }

                });
            })
        },
        deleteQuizQuestionFile = function (quizQuestionFileid, quizQuestionId, fileName, uploadedfilepath) {
            //debugger;
            $.ajax({
                type: 'POST',
                data: { id: quizQuestionFileid, quizQuestionId: quizQuestionId, fileName: fileName, UploadedFilePath: uploadedfilepath },
                url: '/Quiz/QuizQuestions/DeleteQuizQuestionFile',
                success: function (result) {
                    //debugger;
                    globalFunctions.showMessage('success', translatedResources.DeletedSuccessfully);
                    $("#quizQuestionFileList").html(result);
                    globalFunctions.convertImgToSvg();
                },
                error: function (msg) {
                    //debugger;
                    quizquestions.onFailure;
                }
            });

        },
        validationQuizQuestionImage = function (e) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'];

            if ($.inArray($('input[type=file]').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                globalFunctions.showErrorMessage(translatedResources.fileTypeValidationMessage.replace("{0}", fileExtension.join(', ')));
                $('input[type=file]').val('');
            }
        },
        noSort = function () {
            var table = $('#tbl-QuizQuestion').DataTable();
            table.destroy();
            $('#tbl-QuizQuestion').DataTable({
                "columnDefs": [{
                    "targets": "no-sort",
                    "orderable": false
                }],
                "rowReorder": [{
                    dataSrc: translatedResources.Index
                }],
                "paging": false,
                "oLanguage": _dataTableLangSettings
            });
            $("#tbl-QuizQuestion_length").hide();
            $("#tbl-QuizQuestion_filter").hide();
        };
    return {
        addQuizQuestionsPopup: addQuizQuestionsPopup,
        editQuizQuestionsPopup: editQuizQuestionsPopup,
        deleteQuizQuestionData: deleteQuizQuestionData,
        deleteQuizQuestionFile: deleteQuizQuestionFile,
        validationQuizQuestionImage: validationQuizQuestionImage,
        clearUnAssignedQuestions: clearUnAssignedQuestions,
        onSaveSuccess: onSaveSuccess,
        FromQuestionPool: FromQuestionPool,
        onFailure: onFailure,
        loadTopicSubtopic: loadTopicSubtopic,
        deleteObjective: deleteObjective,
        postObjective: postObjective,
        filterObjective: filterObjective,
        filterQuestion: filterQuestion,
        correctAnswer: correctAnswer,
        deleteMTPResource: deleteMTPResource,
        imageUploader: imageUploader,
        loadImportQuizPopup: loadImportQuizPopup,
        addCorrectAnswer: addCorrectAnswer,
        noSort: noSort

    };
}();

$(document).ready(function () {
    $('#tbl-QuizQuestion thead').attr('class', 'thead-dark');
    $('.selectpicker').selectpicker('refresh');
    //$(".manageQuestion-wrapper").mCustomScrollbar({
    //    setHeight: "250",
    //    autoExpandScrollbar: true,
    //});

    if ($("#IsRandomQuestion").val() === 'False') {
        $("#tbl-QuizQuestion").DataTable({
            "columnDefs": [{
                "targets": "no-sort",
                "orderable": false
            }],
            "rowReorder": [{
                dataSrc: translatedResources.Index
            }],
            "paging": false,
            "oLanguage": _dataTableLangSettings
        });
    } else {
        $("#tbl-QuizQuestion").DataTable({
            "columnDefs": [{
                "targets": "no-sort",
                "orderable": false
            }],
            "paging": false,
            "oLanguage": _dataTableLangSettings
        });
    }
    $("#tbl-QuizQuestion_filter").hide();
    $("#tbl-QuizQuestion_length").hide();
    $('#tbl-QuizQuestion').DataTable().search('').draw();
    $("#QuestionListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-QuizQuestion').DataTable().search($(this).val()).draw();
    });
    //$(document).on('click', '#btnAddQuizQuestion', function () {
    //    quizquestions.addQuizQuestionsPopup($(this));
    //});

    $(document).on('click', '#addResponse', function (e) {
        var response = $("#Response").val();
        if (response === null || response === "") {
            e.preventDefault();
            return false;
        }
        //var htmlString = '<span class="responses col-12">' + response + '<input type="hidden" value="' + response + '" class="responseInput"><span type="button" class="pull-right delete-response">×</span></span>';
        var htmlString = '<span class="responses badge badge-primary p-2 mr-2 shadow-none">' + response + '<input type="hidden" value="' + response + '" class="responseInput"><a href="javascript:void(0)" class="delete-response ml-1 white-text">×</a></span>';
        $("#response-container").append(htmlString);
        $("#Response").val("");
        e.preventDefault();
    });

    $(document).on('click', '.delete-response', function (e) {
        $(this).parent(".responses").remove();
    });

    $('body').on('mouseup', '#tbl-QuizQuestion tr td', function () {
        //debugger;
        if ($(this).data('action') != 'actionButton') {
            if ($("#IsRandomQuestion").val() === 'False') {
                sortedQuizQuestionIds = [];
                $('#tbl-QuizQuestion .sortedQuizQuestionId').each(function () {
                    sortedQuizQuestionIds.push($(this).html());
                });
                sortedQuizQuestionIds.pop();
                var formData = new FormData();
                formData.append("QuizId", $("#Quizid").val());
                formData.append("SortedQuestionIds", sortedQuizQuestionIds);

                $.ajax({
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    url: '/Quiz/QuizQuestions/SortQuestionByOrder',
                    success: function (result) {
                        globalFunctions.showMessage('success', translatedResources.OrderSet);
                        //$("#fileList").html(result);
                    },
                    error: function (msg) {
                        quizquestions.onFailure;
                    }
                });
            }
        }

    });

    $(document).on('click', '#btn-submit', function (e) {
        //debugger;
        var validateQuestion = true;
        if ($("#QuestionTypeId").val() == 6) {
            var optionCount = $("#fibCount").val();
            if (optionCount.trim() == '') {
                $('#fibError').html('');
                $('#fibError').html(translatedResources.Mandatory);
            }
            else {
                //debugger;
                if (CKEDITOR.instances.QuestionText.getData().replace(/<[^>]*>/gi, '').length) {
                    var question = CKEDITOR.instances.QuestionText.getData();
                    var count = (question.match(/blank_space_/g) || []).length;
                    if (count != parseInt($("#fibCount").val())) {
                        globalFunctions.showWarningMessage("please check blank space count and question format.")
                        validateQuestion = false;
                    }
                    else {
                        validateQuestion = true;
                    }
                }
            }
        }
        if (!CKEDITOR.instances.QuestionText.getData().replace(/<[^>]*>/gi, '').length) {
            //debugger
            $('#QuestionTextError').html(translatedResources.Mandatory);
            $('#QuestionTextError').removeClass("d-none");

            validateQuestion = false;
        }
        if ($('#frmAddQuizQuestion').valid() && validateQuestion) {
            e.preventDefault();
            var QuestionTypeId = $("#QuestionTypeId").val();
            var QuizQuestionId = $("#QuizQuestionId").val();
            if (QuestionTypeId === null || QuestionTypeId === "") {
                return false;
            }
            if (QuizQuestionId === null || QuizQuestionId === "") {
                return false;
            }
            var responseInputs = $(".responseInput");
            //console.log(responseInputs);
            //if (responseInputs.length > 0) {
            var list = [];
            var isValid = true;
            var isAnyChecked = false;
            //validation

            for (var i = 0; i < responseInputs.length; i++) {
                //var isChecked = $("#chkbox_" + (i + 1)).attr("checked") ? true : false;
                let isChecked = $("#chkbox_" + (i + 1)).is(":checked");
                if (isChecked) {
                    isAnyChecked = true;
                }
                if (jQuery.trim(responseInputs[i].value).length == 0 && QuestionTypeId != 4) {
                    $(responseInputs[i]).focus();
                    $(responseInputs[i]).closest('.element').effect("pulsate", {}, 3000);
                    globalFunctions.showWarningMessage(translatedResources.EnterAnserValidation);
                    Isvalid = false;
                    return false;
                }
                if (QuestionTypeId == 8) {
                    var imagePath = $("#MTPAnswer_" + (i + 1)).val();
                    if (imagePath == '') {
                        globalFunctions.showWarningMessage(translatedResources.CheckImage);
                        Isvalid = false;
                        return false;
                    }
                }
            }
            if (QuestionTypeId != 4 && QuestionTypeId != 7 && QuestionTypeId != 6 && QuestionTypeId != 8) {
                if (!isAnyChecked) {
                    globalFunctions.showWarningMessage(translatedResources.MarkAnswerValidation);
                    Isvalid = false;
                    return false;
                }
            }
            if (QuestionTypeId == 6) {
                var optionCount = $("#fibCount").val();
                var value = parseInt(optionCount);
                if (optionCount.trim() == '') {
                    $('#fibError').html('');
                    $('#fibError').html(translatedResources.Mandatory);
                    Isvalid = false;
                    return false;
                } else if (value - 0 == 0) {
                    Isvalid = false;
                    return false;
                }
            }
            //push to array
            if (QuestionTypeId != 4 && QuestionTypeId != 7 && QuestionTypeId != 6 && QuestionTypeId != 8) {
                for (var i = 0; i < responseInputs.length; i++) {
                    //var isChecked = $("#chkbox_" + (i + 1)).attr("checked") ? true : false;
                    let isChecked = $("#chkbox_" + (i + 1)).is(":checked");
                    if (jQuery.trim(responseInputs[i].value).length)
                        list.push({ QuizAnswerId: null, AnswerText: responseInputs[i].value, SortOrder: (i + 1), IsCorrectAnswer: isChecked, ResourcePath: null });
                }
            }
            //for free text
            if (QuestionTypeId == 4) {
                list.push({ QuizAnswerId: null, AnswerText: 'freetext', SortOrder: 0, IsCorrectAnswer: false, ResourcePath: null });
            }
            //for match the order
            if (QuestionTypeId == 7) {
                for (var i = 0; i < responseInputs.length; i++) {
                    //let isChecked = $("#chkbox_" + (i + 1)).is(":checked");
                    //if (jQuery.trim(responseInputs[i].value).length)
                    list.push({ QuizAnswerId: null, AnswerText: responseInputs[i].value, SortOrder: $("#txtAnswer_" + (i + 1)).val(), IsCorrectAnswer: true, ResourcePath: null });
                }
            }
            //for fill in the blank
            if (QuestionTypeId == 6) {
                for (var i = 0; i < responseInputs.length; i++) {
                    //let isChecked = $("#chkbox_" + (i + 1)).is(":checked");
                    //if (jQuery.trim(responseInputs[i].value).length)
                    list.push({ QuizAnswerId: null, AnswerText: responseInputs[i].value.trim(), SortOrder: i + 1, IsCorrectAnswer: true, ResourcePath: null });
                }
            }
            //for match the pair
            if (QuestionTypeId == 8) {
                for (var i = 0; i < responseInputs.length; i++) {
                    //let isChecked = $("#chkbox_" + (i + 1)).is(":checked");
                    //if (jQuery.trim(responseInputs[i].value).length)
                    list.push({ QuizAnswerId: null, AnswerText: responseInputs[i].value, SortOrder: i + 1, IsCorrectAnswer: true, ResourcePath: $("#MTPAnswer_" + (i + 1)).val() });
                }
            }
            if (isValid) {
                var formData = new FormData();
                //if (globalFunctions.isValueValid(document.getElementById("QuizImage"))) {
                //    var file = document.getElementById("QuizImage").files[0]
                //        formData.append("QuizImage", file);
                //}

                if (!$('#quizQuestionImage').attr('src') == '') {
                    formData.append("QuestionImagePath", $('#quizQuestionImage').attr('src'));
                }
                else if ($('#QuestionImagePath').val() !== undefined && $('#QuestionImagePath').val() !== '') {
                    formData.append("QuestionImagePath", $('#QuestionImagePath').val());
                }

                formData.append("QuestionTypeId", $("#QuestionTypeId").val());
                formData.append("QuizQuestionId", $("#QuizQuestionId").val());
                formData.append("QuestionText", CKEDITOR.instances.QuestionText.getData());
                formData.append("QuizId", $("#QuizId").val());
                formData.append("IsAddMode", $('#IsAddMode').val());
                formData.append("Marks", $('#Marks').val());
                formData.append("IsRequired", $("#IsRequired").attr("checked") ? true : false);
                formData.append("CourseIds", $("#CourseIds").val());

                //formData.append("Answers", JSON.stringify(list));

                for (var i = 0; i < list.length; i++) {
                    formData.append("Answers[" + i + "].AnswerText", list[i].AnswerText);
                    formData.append("Answers[" + i + "].SortOrder", list[i].SortOrder)
                    formData.append("Answers[" + i + "].QuizAnswerId", list[i].QuizAnswerId)
                    formData.append("Answers[" + i + "].IsCorrectAnswer", list[i].IsCorrectAnswer)
                    formData.append("Answers[" + i + "].ResourcePath", list[i].ResourcePath)
                }
                $.each(listRecordings, function (idx, file) {
                    formData.append("recordings", file, file.name);
                });
                //var formData = {

                //        QuestionTypeId: $("#QuestionTypeId").val(),
                //        QuizQuestionId: $("#QuizQuestionId").val(),
                //        QuestionText: $("#QuestionText").val(),
                //        QuizId: $("#QuizId").val(),
                //        IsAddMode: $("#IsAddMode").val(),
                //        Marks: $("#Marks").val(),
                //        IsRequired: $("#IsRequired").attr("checked") ? true : false,
                //        Answers: list,
                //        QuizImage: file


                //    };

                $.ajax({
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    url: '/Quiz/QuizQuestions/SaveQuizQuestion',
                    //data: { model: formData, answersList: list },
                    data: formData,
                    success: function (result) {
                        if (result.Success) {
                            //$("#myModal").modal('hide');
                            window.location.href = '/Quiz/QuizQuestions/';
                        }
                        else {
                            globalFunctions.showMessage(result.NotificationType, result.Message);
                        }
                    },
                    error: function (msg) {
                        quizquestions.onFailure;
                    }
                });
            }
            //} else {
            //    $("#ResponseValidation").text(translatedResources.ResponseValidation);
            //    return false;
            //}
        }

    });
    //

    // Add new element
    $('body').on('click', '.add', function () {
        var QTypeId = $('#QuestionTypeId').val();
        // Finding total number of elements added
        var total_element = $(".element").length;

        // last <div> with element class id
        var lastid = $(".element:last").attr("id");
        var split_id = lastid.split("_");
        var nextindex = Number(split_id[1]) + 1;

        //var max = 5;
        // Check total number elements
        //if (total_element < max) {
        // Adding new div container after last occurance of element class
        $(".element:last").after("<div class='element clearfix mb-3' id='div_" + nextindex + "'></div>");

        // Adding element to <div>
        if (QTypeId == 1 || QTypeId == 2) {
            $("#div_" + nextindex).append("<div class='quiz-answer-field row mb-4'><div class='col-md-7 col-12'><div class='md-form md-outline mb-0'><input type='text'  class='responseInput form-control' id='txt_" + nextindex + "'> <label for='txt_" + nextindex + "'>" + translatedResources.EnterAnswer + "<span class='text-danger'>*</span></label></div></div> <div class='col-md-5 col-12 d-flex justify-content-between align-items-center'> <div class='custom-control custom-radio custom-control-inline mr-0'><input type='radio' class='custom-control-input' name='single1' id='chkbox_" + nextindex + "' ><label class='custom-control-label' for='chkbox_" + nextindex + "'>" + translatedResources.MarkAsAnswer + "</label> </div><button type='button' id='remove_" + nextindex + "' class='remove btn btn-outline-danger m-0 mr-1'><i class='fas fa-plus'></i></button></div></div>");
        }
        else if (QTypeId == 7) {
            $("#div_" + nextindex).append("<div class='quiz-answer-field'><input type='text'  class='responseInput form-control' id='txt_" + nextindex + "'> <label for='txt_" + nextindex + "'>" + translatedResources.EnterAnswer + "<span class='text-danger'>*</span></label> <input type='text'  placeholder='" + translatedResources.CorrectAnswer + "' id='txtAnswer_" + nextindex + "'></div>  <button class='remove btn btn-outline-danger m-0' type='button' id='remove_" + nextindex + "'><i class='fas fa-plus'></i></button>");
        }
        else if (QTypeId == 8) {
            $("#div_" + nextindex).append("<hr><div class='quiz-answer-field'><div class='row'><div class='col-md-7 col-12'><div class='md-form md-outline'><input type='text'  class='responseInput mb-3 form-control' id='txt_" + nextindex + "'><label for='txt_" + nextindex + "'>" + translatedResources.EnterAnswer + "<span class='text-danger'>*</span></label> <input type='text' class='d-none' id='MTPAnswer_" + nextindex + "'></div></div><div class='col-md-5 col-12'> <button class='float-right remove btn btn-outline-danger m-0' type='button' data-toggle='tooltip' data-placement='top' title='Remove' id='remove_" + nextindex + "'><i class='fas fa-plus'></i></button></div></div><div class='uploaderWidget quizFileUploader'><input class='quizImgUploader' type='file' id='file_" + nextindex + "' data-controlid='file_" + nextindex + "' data-controlname='MTPAnswer_" + nextindex + "'></div></div>  ");
            quizquestions.imageUploader()
        }
        else {
            $("#div_" + nextindex).append("<div class='quiz-answer-field row mb-4'><div class='col-md-7 col-12'><div class='md-form md-outline mb-0'><input type='text'  class='responseInput form-control' id='txt_" + nextindex + "'> <label for='txt_" + nextindex + "'>" + translatedResources.EnterAnswer + "<span class='text-danger'>*</span></label></div></div> <div class='col-md-5 col-12 d-flex justify-content-between align-items-center'> <div class='custom-control custom-checkbox custom-control-inline mr-0'><input type='checkbox' class='custom-control-input' id='chkbox_" + nextindex + "' ><label class='custom-control-label' for='chkbox_" + nextindex + "'>" + translatedResources.MarkAsAnswer + "</label> </div><button type='button' id='remove_" + nextindex + "' class='remove btn btn-outline-danger m-0 mr-1'><i class='fas fa-plus'></i></button></div></div>");
        }
        //}
    });




    $('#QuestionTypeId').on('change', function () {
        ////debugger;
        if ($(this).val() != "") {
            $("#QuestionTypeId-error").addClass('d-none');
        }

        if ($(this).val() == 4) {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").hide();
            $(".element").remove();
            $("#answerSection").append("<div class='element clearfix' id='div_1'></div>");
            // Adding element to <div>
            $("#div_1").append("<div class='quiz-answer-field'><input type='text'  class='responseInput d-none form-control' placeholder='" + translatedResources.EnterAnswer + "' id='txt_1'>  <div class='custom-control custom-checkbox custom-control-inline mr-0'> </div></div> ");
        }
        else if ($(this).val() == 1 || $(this).val() == 2) {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").show();
            $(".element").remove();
            $("#answerSection").append("<div class='element clearfix' id='div_1'></div>");
            $("#div_1").append("<div class='quiz-answer-field row mb-4'><div class='col-md-7 col-12'><div class='md-form md-outline mb-0'><input type='text'  class='responseInput form-control mb-0' id='txt_1'> <label for='txt_1'>" + translatedResources.EnterAnswer + "<span class='text-danger'>*</span></label></div></div><div class='col-md-5 col-12 d-flex justify-content-between align-items-center'> <div class='custom-control custom-radio custom-control-inline mr-0'><input type='radio' class='custom-control-input' id='chkbox_1' name='single1'><label class='custom-control-label' for='chkbox_1'>" + translatedResources.MarkAsAnswer + "</label> </div>  <button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button></div></div>");
            if ($(this).val() == 1) {
                setTimeout(function () {
                    $('.add').click();
                }, 100); // for 3 second delay
            }
        }
        else if ($(this).val() == 7) {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").show();
            $(".element").remove();
            $("#answerSection").append("<div class='element clearfix' id='div_1'></div>");
            $("#div_1").append("<div class='quiz-answer-field'><input type='text'  class='responseInput form-control' placeholder='" + translatedResources.EnterAnswer + "' id='txt_1'>  <input type='text'  placeholder='" + translatedResources.CorrectAnswer + "' id='txtAnswer_1'></div>  <button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button>");
        }
        else if ($(this).val() == 8) {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").show();
            $(".element").remove();
            $("#answerSection").append("<div class='element clearfix mt-3' id='div_1'></div>");
            $("#div_1").append("<div class='quiz-answer-field'><div class='row'><div class='col-md-7 col-12'><div class='md-form md-outline'> <input type='text'  class='responseInput mb-3 form-control' id='txt_1'> <label for='txt_1'>" + translatedResources.EnterAnswer + "<span class='text-danger'>*</span></label></div> <input type='text' class='d-none' id='MTPAnswer_1'>  </div><div class='col-md-5 col-12'><button class='float-right add btn btn-outline-primary m-0' type='button' data-toggle='tooltip' data-placement='top' title='Add'><i class='fas fa-plus'></i></button></div></div><div class='pb-3' id='fileUploader1'><div class='uploaderWidget quizFileUploader'><input class='quizImgUploader' type='file' id='file_1' data-controlid='file_1' data-controlname='MTPAnswer_1' data-previewimage='previewImage1' ></div></div></div>  ");
            quizquestions.imageUploader();
        }
        else if ($(this).val() == 6) {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").hide();
            $(".element").remove();
            $("#fibMessage").html(translatedResources.FIBMessage);
            $("#fibSelector").append("<div class='element clearfix' id='div_1'></div>");
            $("#div_1").append("<div class='quiz-answer-field w-100'><input class='w-100 form-control mb-0' type='text' id='fibCount'><label for='fibCount'>" + translatedResources.FIBCount + "<span class='text-danger'>*</span></label><span class='field-validation-error d-inline-block'><span id='fibError' class=''></span></span> </div>  ");

        }
        else {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").show();
            $(".element").remove();
            $("#answerSection").append("<div class='element clearfix' id='div_1'></div>");
            $("#div_1").append("<div class='quiz-answer-field row mb-4'><div class='col-md-7 col-12'><div class='md-form md-outline mb-0'><input type='text'  class='responseInput form-control mb-0' id='txt_1'> <label for='txt_1'>" + translatedResources.EnterAnswer + "<span class='text-danger'>*</span></label></div></div><div class='col-md-5 col-12 d-flex justify-content-between align-items-center'> <div class='custom-control custom-checkbox custom-control-inline mr-0'><input type='checkbox' class='custom-control-input' id='chkbox_1'><label class='custom-control-label' for='chkbox_1'>" + translatedResources.MarkAsAnswer + "</label> </div>  <button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button></div></div>");
            setTimeout(function () {
                $('.add').click();
                $('.add').click();
            }, 100); // for 3 second delay 

        }
    });
    $('body').on('keypress keyup blur', '#fibCount', function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('body').on('change', '#fibCount', function (event) {
        //debugger;
        $("#answerHeader").show();
        var optionCount = $(this).val();
        var value = parseInt(optionCount);
        if (optionCount.trim() == '') {
            $('#fibError').html(translatedResources.Mandatory);
            return false;
        }
        else {
            $('#fibError').html('');
        }
        if (value - 0 == 0) {
            $('#fibError').html(translatedResources.MoreThanZero);
            return false;
        }
        else {
            $('#fibError').html('');
        }
        if ($('#IsAddMode').val() == true) {
            $("#answerHeader").show();
            $(".fibelement").remove();
            var i = 1;
            while (i <= optionCount) {
                $("#answerSection").append("<div class='fibelement element mb-3' id='fibDiv_" + i + "'></div>");
                $("#fibDiv_" + i + "").append("<div class='quiz-answer-field'><span class='font-small text-medium'>blank_space_" + i + "<span class='text-danger'>*</span></span>   <input type='text'  class='responseInput form-control' placeholder='Answer for blank_space_" + i + "' id='fibTxt_" + i + "'></div>");
                i++;
            }
        }
        else {
            var responseInputs = $(".responseInput");
            var i = responseInputs.length;
            if (i <= optionCount) {
                i++;
                while (i <= optionCount) {
                    $("#answerSection").append("<div class='fibelement element mb-3' id='fibDiv_" + i + "'></div>");
                    $("#fibDiv_" + i + "").append("<div class='quiz-answer-field'><span class='font-small text-medium'>blank_space_" + i + "<span class='text-danger'>*</span></span>   <input type='text'  class='responseInput form-control' placeholder='Answer for blank_space_" + i + "' id='fibTxt_" + i + "'></div>");
                    i++;
                }
            }
            else {
                while (i > optionCount) {
                    var selector = "#fibDiv_" + i + "";
                    $(selector).remove();
                    i--;
                }
            }
        }

    });

    // Remove element
    $('body').on('click', '.remove', function () {
        ////debugger;
        if (parseInt($('#QuestionTypeId').val()) === 3) {
            if ($('.remove').length >= 2) {
                var id = this.id;
                var split_id = id.split("_");
                var deleteindex = split_id[1];
                $("#div_" + deleteindex).remove();
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.multipleQuestion);
            }
        }
        else if (parseInt($('#QuestionTypeId').val()) === 1) {
            if ($('.remove').length > 1) {
                var id = this.id;
                var split_id = id.split("_");
                var deleteindex = split_id[1];
                $("#div_" + deleteindex).remove();
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.singleQuestion);
            }
        }
        else {
            var id = this.id;
            var split_id = id.split("_");
            var deleteindex = split_id[1];
            $("#div_" + deleteindex).remove();
        }


    });

    $('body').on('click', '.custom-control-label', function () {
        let chkBox = $(this).parent('.custom-checkbox').find('input');
        //console.log(chkBox.attr('id'));
        $(this).parent('.custom-checkbox').find('input').change(function () {
            if ($(this).parent('.custom-checkbox').find('input').is(":checked")) {

                chkBox.attr("checked", true);
            } else {
                chkBox.removeAttr("checked");

            }
        })

        //for radio
        let rbBox = $(this).parent('.custom-radio').find('input');
        //console.log(chkBox.attr('id'));
        $(this).parent('.custom-radio').find('input').change(function () {
            if ($(this).parent('.custom-radio').find('input').is(":checked")) {
                $('#answerSection .custom-control-input').removeAttr("checked");
                rbBox.attr("checked", true);
            } else {
                rbBox.removeAttr("checked");

            }
        })
    });
    $('#Quizlightgallery').lightGallery({
        selector: '.galleryImg',
        share: false
    });

    $('body').on('click', '#DeleteQuizQuestionFiles', function () {
        ////debugger;
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        var fileName = $(this).attr('data-fileName');
        var uploadedfilepath = $(this).attr('data-uploadedPath');
        var quizQuestionFileId = $(this).attr('data-FileId');
        var quizQuestionId = $(this).attr('data-QuizQuestionId');
        $(document).bind('okClicked', $(this), function (e) {
            quizquestions.deleteQuizQuestionFile(quizQuestionFileId, quizQuestionId, fileName, uploadedfilepath);
        });
    });
    $('body').on('click', '#DeleteQuizQuestionRecording', function () {
        ////debugger;
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        var fileName = $(this).attr('data-fileName');
        var uploadedfilepath = $(this).attr('data-uploadedPath');
        var quizQuestionFileId = $(this).attr('data-fileid');
        var quizQuestionId = $(this).attr('data-QuizQuestionId');
        $(document).bind('okClicked', $(this), function (e) {
            quizquestions.deleteQuizQuestionFile(quizQuestionFileId, quizQuestionId, fileName, uploadedfilepath);
            $("#audio_" + quizQuestionFileId).remove();
        });

    });
    $(document).on('click', '#ViewQuizQuestionFile', function (e) {
        e.preventDefault();
        if ($("#IsAddMode").val() == "False") {
            var id = $(this).data("id");
            if (globalFunctions.isValueValid(id)) {
                window.open("/Document/Viewer/Index?id=" + id + "&module=editQuizQuestion", "_blank");
            }
        }
        else {
            var id = $(this).data("filename");
            if (globalFunctions.isValueValid(id)) {
                window.open("/Document/Viewer/Index?id=" + id + "&module=addeditQuizQuestion", "_blank");
            }
        }
    });

    $("#questionsSearch").on("keypress", function (e) {
        if (e.which == 13) {
            var searchText = $(this).val().toLowerCase();
            $('#AssignedQuestionList div').each(function () {
                var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
                //$(this).toggle(showCurrentLi);
                if (showCurrentLi == true)
                    $(this).show();
                else {
                    $(this).removeClass("d-block");
                    $(this).hide();
                }
            });
        }
    });
    //$(document).on('keyup', '#availableQuestionSearch', function (e) {

    //    var searchText = $(this).val().toLowerCase();
    //    $('#unassignedQuestionList div').each(function () {
    //        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
    //        //$(this).toggle(showCurrentLi);

    //        if (showCurrentLi == true)
    //            $(this).show();
    //        else {
    //            $(this).removeClass("d-block");
    //            $(this).hide();
    //        }
    //    });
    //});
    $("#availableQuestionSearch").on("keypress", function (e) {
        if (e.which == 13) {
            var searchText = $(this).val().toLowerCase();
            $('#unassignedQuestionList div').each(function () {
                var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
                //$(this).toggle(showCurrentLi);

                if (showCurrentLi == true)
                    $(this).show();
                else {
                    $(this).removeClass("d-block");
                    $(this).hide();
                }
            });
        }
    });
    $("#btnAddQuizQuestion").click(function () {
        var $selectedQuestions = $("#unassignedQuestionList input[type='checkbox']:checked").parent('.custom-checkbox');
        var output = jQuery.map($('#AssignedQuestionList :checkbox'), function (n, i) {
            return n.value;
        }).join(',');
        if ($selectedQuestions != null && $selectedQuestions.length > 0) {
            $selectedQuestions.each(function (i, elem) {
                if ($("#SelectedQuestionId").val().indexOf("") == -1) {
                    $("#SelectedQuestionId").val($(this).children("input[type='checkbox']").val() + ",");
                }
                else {
                    if (output != "" && i == 0) {
                        $("#SelectedQuestionId").val(output + ",");
                    }
                    $("#SelectedQuestionId").val($("#SelectedQuestionId").val() + $(this).children("input[type='checkbox']").val() + ",");
                }
            });
            $("#AssignedQuestionList").append($selectedQuestions);
            $("#AssignedQuestionList").html($("#AssignedQuestionList .custom-checkbox").sort(function (a, b) {
                return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
            }));
            $("#AssignedQuestionList input[type='checkbox']").prop('checked', $('#selectAllAssignedQuestions').is(':checked'));
            $("#AssignedQuestionList .custom-checkbox").addClass('d-block');
            $('#selectAllUnassignedQuestions').prop('checked', false);
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.selectAdd);
        }
    });

    $("#btnRemoveQuizQuestion").click(function () {
        var $selectedQuestions = $("#AssignedQuestionList input[type='checkbox']:checked").parent('.custom-checkbox');

        if ($selectedQuestions != null && $selectedQuestions.length > 0) {
            $selectedQuestions.each(function (i, elem) {
                if ($("#SelectedQuestionId").val().indexOf($(this).children("input[type='checkbox']").val() + ",") != -1) {
                    $("#SelectedQuestionId").val($("#SelectedQuestionId").val().replace($(this).children("input[type='checkbox']").val() + ",", ""));
                }
                else {
                    $("#DeselectedQuestionId").val($("#DeselectedQuestionId").val() + $(this).children("input[type='checkbox']").val() + ",");
                }
            });

            $("#unassignedQuestionList").append($selectedQuestions);
            $("#unassignedQuestionList").html($("#unassignedQuestionList .custom-checkbox").sort(function (a, b) {
                //console.log($(a).children('.custom-control-label').text());
                return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
            }));
            $("#unassignedQuestionList input[type='checkbox']").prop('checked', $('#selectAllUnassignedQuestions').is(':checked'));
            $('#selectAllAssignedQuestions').prop('checked', false);
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.selectRemove);
        }


    });

    $('#selectAllUnassignedQuestions').change(function (e) {
        //debugger;
        var $inputs = $('#unassignedQuestionList input[type=checkbox]');
        if (e.originalEvent === undefined) {
            var allChecked = true;
            $inputs.each(function () {
                allChecked = allChecked && this.checked;
            });
            this.checked = allChecked;
        } else {
            $inputs.prop('checked', this.checked);
        }
    });
    $('input[type=checkbox]', "#unassignedQuestionList").on('change', function () {
        $('#selectAllUnassignedQuestions').trigger('change');
    });
    $('input[type=checkbox]', "#AssignedQuestionList").on('change', function () {
        $('#selectAllAssignedQuestions').trigger('change');
    });

    $('#selectAllAssignedQuestions').change(function (e) {
        //debugger;
        var $inputs = $('#AssignedQuestionList input[type=checkbox]');
        if (e.originalEvent === undefined) {
            var allChecked = true;
            $inputs.each(function () {
                allChecked = allChecked && this.checked;
            });
            this.checked = allChecked;
        } else {
            $inputs.prop('checked', this.checked);
        }
    });

    $("#btnAssignQuestions").click(function () {
        //debugger;
        var $selectedQuestions = $("#AssignedQuestionList input[type='checkbox']").parent('.custom-checkbox');
        //if ($selectedQuestions.length === 0) {
        //    globalFunctions.showWarningMessage(translatedResources.selectAdd);
        //}
        if ($selectedQuestions.length === 0) {
            globalFunctions.showWarningMessage(translatedResources.selectAdd);
        } else {
            var formData = new FormData();
            formData.append("QuizId", $('#QuizId').val());
            formData.append("SelectedQuestionId", $('#SelectedQuestionId').val().slice(0, -1));
            formData.append("DeselectedQuestionId", $('#DeselectedQuestionId').val().slice(0, -1));
            if ($('#SelectedQuestionId').val() != "" || $('#DeselectedQuestionId').val() != "") {
                $.ajax({
                    type: 'POST',
                    url: '/Quiz/QuizQuestions/InsertUpdatePoolQuestions',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result.Success == true)
                            globalFunctions.showMessage(result.NotificationType, result.Message);
                        window.location.href = '/quiz/quizquestions';
                    },
                    error: function (msg) {
                        globalFunctions.onFailure();
                    }
                });
            } else {
                globalFunctions.showWarningMessage(translatedResources.selectAdd);
            }
        }
    });

    $('#btnFilterQuestions').on('click', function () {
        quizquestions.filterQuestion();
        //quizquestions.postObjective();
    });
    $('#SubjectIds').on('change', function () {
        //debugger;
        quizquestions.filterObjective();
    });
    $('#btnClearFilter').on('click', function () {
        quizquestions.postObjective();
        $("#CourseIds").selectpicker('deselectAll');
        quizquestions.filterQuestion();
    });
    $('#btnCancelAssignQuestions').on('click', function () {
        location.href = "/Quiz/QuizQuestions";
    });
    CKEDITOR.plugins.addExternal('ckeditor_wiris', '', 'plugin.js');
    CKEDITOR.replace('QuestionText', {
        htmlencodeoutput: false,
        height: '180px',
        extraPlugins: 'ckeditor_wiris',
        allowedContent: true,
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P,
        contentsLangDirection: $("html").attr("dir")
    });

    //for multi language virtual keyboard
    for (var instanceName in CKEDITOR.instances) {

        CKEDITOR.instances[instanceName].addCommand("mySimpleCommand", {
            exec: function (edt) {
                openLanguageModal();
            }
        });
        CKEDITOR.instances[instanceName].ui.addButton('SuperButton', {
            label: "Languages",
            command: 'mySimpleCommand',
            toolbar: 'insert',
            icon: '/Content/js/ckeditor/plugins/ckeditor_wiris/icons/languages.png'
        });

    }

    function openLanguageModal() {
        $('#languageModal').modal('show')
    }

    CKEDITOR.instances.QuestionText.on('change', function () {
        if (CKEDITOR.instances.QuestionText.getData().replace(/<[^>]*>/gi, '').length > 0) {
            $("#QuestionTextError").addClass("d-none");
        }
    });

    // webkit shim
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    navigator.getUserMedia = (
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia
    );

    if (typeof navigator.mediaDevices.getUserMedia === 'undefined') {
        navigator.getUserMedia({
            audio: true
        }, startUserMedia);
    } else {
        navigator.mediaDevices.getUserMedia({
            audio: true
        }).then(startUserMedia).catch(errorHandler);
    }

    function errorHandler() {
        console.log("error");
    }

    window.URL = window.URL || window.webkitURL;

    audio_context = new AudioContext();

    //function ShowPreview(input,resourceId) {
    //    //debugger;
    //    if (input.files && input.files[0]) {
    //        var ImageDir = new FileReader();
    //        ImageDir.onload = function (e) {
    //            $(resourceId).attr('src', e.target.result);
    //        }
    //        ImageDir.readAsDataURL(input.files[0]);
    //    }
    //}

    //$(".quizImgUploader").fileinput({
    //    title: 'Drag and drop files here or BROWSE',
    //    theme: "fas",
    //    showUpload: true,
    //    showCaption: false,
    //    showMultipleNames: false,
    //    uploadUrl: "/Quiz/QuizQuestions/UploadQuizQuestionFiles",
    //    //uploadExtraData: function () {
    //    //    return {
    //    //        assignmentId: $("#AssignmentId").val(),
    //    //    };
    //    //},
    //    fileActionSettings: fileInputControlSettings.fileActionSettings,
    //    allowedFileExtensions: "png",
    //    uploadAsync: true,
    //    maxFileSize: translatedResources.fileSizeAllow,
    //    browseClass: "btn btn-sm m-0 z-depth-0 btn-browse",
    //    removeClass: "btn btn-sm m-0 z-depth-0",
    //    uploadClass: "btn btn-sm m-0 z-depth-0",
    //    overwriteInitial: false,
    //    //previewFileIcon: '<i class="fas fa-file"></i>',
    //    initialPreviewAsData: false, // defaults markup
    //    initialPreviewConfig: [{
    //        frameAttr: {
    //            title: 'My Custom Title',
    //        }
    //    }],
    //    preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
    //    previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
    //    previewSettings: fileInputControlSettings.previewSettings,
    //    previewFileExtSettings: fileInputControlSettings.previewFileExtSettings

    //}).on('fileuploaded', function (event, data, previewId, index) {
    //    validateUploadFiles(data);
    //});
    //function validateUploadFiles(data) {
    //    var filesSize = 0;
    //    for (var i = 0; i < data.files.length; i++) {
    //        filesSize = filesSize + data.files[i].size;
    //    }
    //    if ((filesSize / 1024 / 1024) < 200) {
    //        globalFunctions.showSuccessMessage(translatedResources.UploadSuccess);
    //        $.ajax({
    //            type: "GET",
    //            url: "/Quiz/QuizQuestions/GetuploadedFileDetails",
    //            data: {
    //                id: $("#QuizQuestionId").val(),
    //                isTask: false
    //            },
    //            success: function (response) {
    //                $('.file-upload-widget').animate({
    //                    right: '-300'
    //                });
    //                $("#fileList").html('');
    //                $("#fileList").append(response);
    //                $('#lightgallery').lightGallery({
    //                    selector: '.galleryImg',
    //                    share: false
    //                });
    //                $(".fileinput-remove-button").click();
    //            },
    //            error: function (error) {
    //                globalFunctions.onFailure();
    //            }
    //        });
    //        $('.file-upload-widget').animate({
    //            right: '-300'
    //        });
    //    }
    //    else {
    //        globalFunctions.showErrorMessage(translatedResources.UploadLimit);
    //        $('li[data-file-id="' + data.id + '"]').html("Not valid 100MB");
    //        return false;
    //    }

    //}


});