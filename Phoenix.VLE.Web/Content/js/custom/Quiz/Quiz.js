﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var quiz = function () {

    var init = function () {
        loadQuizGrid();
        //quiz.noSort();
    },
        loadQuizGrid = function () {

            //debugger;
            if ($("#IsStudent").val() == "False") {
                var _columnData = [];
                _columnData.push(
                    { "mData": "QuizName", "sTitle": translatedResources.QuizName, "sWidth": "25%" },
                    { "mData": "StartDate", "sTitle": translatedResources.StartDateTime, "sWidth": "20%" },
                    { "mData": "IsStarted", "sTitle": translatedResources.Status, "sWidth": "20%" },
                    { "mData": "IsActive", "sClass": "no-sort text-left text-sm-center pl-3", "sTitle": translatedResources.IsActive, "sWidth": "15%" },
                    { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "30%" }
                );
                initQuizGrid('tbl-Quiz', _columnData, '/Quiz/Quiz/LoadQuizGrid');
            }
            else {
                var _columnData = [];
                _columnData.push(
                    { "mData": "QuizName", "sTitle": translatedResources.QuizName, "sWidth": "40%" },
                    { "mData": "GroupName", "sTitle": translatedResources.ResourceName, "sWidth": "25%" },
                    { "mData": "CreatedByName", "sTitle": translatedResources.TeacherName, "sWidth": "25%" },
                    { "mData": "TotalMarks", "sTitle": translatedResources.Score, "sWidth": "10%" }
                );
                initQuizGrid('tbl-StudentQuizSearch', _columnData, '/Quiz/Quiz/LoadStudentQuizGrid');
                $("#tbl-StudentQuizSearch_filter").hide();
                $('#tbl-StudentQuizSearch').DataTable().search('').draw();
                $("#StudentQuizSearch").on("input", function (e) {
                    e.preventDefault();
                    $('#tbl-StudentQuizSearch').DataTable().search($(this).val()).draw();
                });
            }
            globalFunctions.convertImgToSvg();
        },
        deleteQuizFile = function (quizFileid, quizId, fileName, uploadedfilepath) {
            //debugger;
            $.ajax({
                type: 'POST',
                data: { id: quizFileid, quizId: quizId, fileName: fileName, UploadedFilePath: uploadedfilepath },
                url: '/Quiz/Quiz/DeleteQuizFile',
                success: function (result) {
                    //debugger;
                    globalFunctions.showMessage('success', translatedResources.DeletedSuccessfully);
                    $("#quizFileList").html(result);
                },
                error: function (msg) {
                    quiz.onFailure;
                }
            });
            globalFunctions.convertImgToSvg();
        },
        deleteQuizRecording = function (quizFileid) {
            $.ajax({
                type: 'POST',
                data: { id: quizFileid },
                url: '/Quiz/Quiz/DeleteQuizRecordingFile',
                success: function (result) {
                    globalFunctions.showMessage('success', translatedResources.DeletedSuccessfully);
                    $("#audio_" + quizFileid).remove();
                },
                error: function (msg) {
                    quiz.onFailure;
                }
            });
        },
        solveGroupQuiz = function (source, id, resourceId, studentId) {
            window.location.href = '/Files/Files/Quiz/?id=' + id + '&resourceId=' + resourceId + '&studentId=' + studentId;
        },
        initQuizGrid = function (_tableId, _columnData, _url) {
            //debugger;
            if ($("#IsTeacher").val() == "True") {
                var grid = new DynamicGrid(_tableId);
                var settings = {
                    columnData: _columnData,
                    url: _url,
                    columnSelector: false,
                    columnDefs: [{
                        "targets": [0, 1, 2, 3, 4],
                        "orderable": false
                    }],
                    searching: true,
                    serverSide: true
                };
                grid.init(settings);
            }
            else {
                var grid = new DynamicGrid(_tableId);
                var settings = {
                    columnData: _columnData,
                    url: _url,
                    columnSelector: false
                };
                grid.init(settings);
            }
            $("#tbl-Quiz_filter").hide();
            $('#tbl-Quiz').DataTable().search('').draw();
            $("#QuizListSearch").on("input", function (e) {
                e.preventDefault();
                $('#tbl-Quiz').DataTable().search($(this).val()).draw();
            });
        },
        loadQuizPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.Quiz;
            window.location.href = '/Quiz/Quiz/InitAddEditQuizForm?id=' + id;
            //globalFunctions.loadPopup(source, '/Quiz/Quiz/InitAddEditQuizForm?id=' + id, title, 'quiz-dailog');

            //$(source).off("modalLoaded");
            //$(source).on("modalLoaded", function () {
            //    window.AudioContext = window.AudioContext || window.webkitAudioContext;
            //    navigator.getUserMedia = (
            //        navigator.getUserMedia ||
            //        navigator.webkitGetUserMedia ||
            //        navigator.mozGetUserMedia ||
            //        navigator.msGetUserMedia
            //    );

            //    if (typeof navigator.mediaDevices.getUserMedia === 'undefined') {
            //        navigator.getUserMedia({
            //            audio: true
            //        }, startUserMedia);
            //    } else {
            //        navigator.mediaDevices.getUserMedia({
            //            audio: true
            //        }).then(startUserMedia).catch(errorHandler);
            //    }

            //    function errorHandler() {
            //        console.log("error");
            //    }

            //    window.URL = window.URL || window.webkitURL;

            //    audio_context = new AudioContext();
            //    var minDateData = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), nowTemp.getDate(), 0, 0, 0, 0);
            //    var minTime = moment();
            //    $('.date-picker1').datetimepicker({
            //        format: 'DD-MMM-YYYY',
            //        minDate: minDateData
            //    }).on('dp.change', function (e) {
            //    });
            //    $('.time-picker').datetimepicker({
            //        format: 'LT'
            //    }).on('dp.change', function (ev) {
            //    }).data("DateTimePicker");
            //    var pagingValue = $('#QuestionPaginationRange').val();
            //    if (pagingValue == 0) {
            //        $('#QuestionPaginationRange').val('');
            //    }
            //    $('.selectpicker').selectpicker('refresh');
            //});
        },
        addQuizPopup = function (source) {
            window.location.href = '/Quiz/Quiz/InitAddEditQuizForm';
        },

        editQuizPopup = function (source, id, taskCount) {
            if (taskCount > 0) {
                globalFunctions.showErrorMessage(translatedResources.QuizTaskEditValidationMessage);
                return false;
            } else {
                loadQuizPopup(source, translatedResources.edit, id);
            }
        },

        deleteQuizData = function (source, id, taskCount) {
            if (globalFunctions.isValueValid(id)) {
                if (taskCount > 0) {
                    globalFunctions.showErrorMessage(translatedResources.QuizTaskDeleteValidationMessage);
                    return false;
                } else {
                    globalFunctions.deleteItem('Quiz', '/Quiz/Quiz/DeleteQuizData', 'tbl-Quiz', source, id);
                }
            }
        },
        deleteObjective = function (objectiveId) {
            $.ajax({
                type: 'GET',
                url: '/Quiz/Quiz/DeleteObjective?objectiveId=' + objectiveId,
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    $("#selectedObjectiveDetails").html(data);
                    globalFunctions.showSuccessMessage(translatedResources.DeleteObjectiveSuccess);
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
        },
        loadTopicSubtopic = function (source, subjectId) {
            if ($("#CourseIds").val() != "") {
                var title = translatedResources.SelectCurriculum;
                globalFunctions.loadPopup(source, '/Quiz/Quiz/GetUnitStructureView?courseIds=' + $("#CourseIds").val(), title, 'addtask-dialog modal-md');
                $(source).off("modalLoaded");
                $('#formModified').val(true);

                $(source).on("modalLoaded", function () {
                    formElements.feSelect();
                    $('select').selectpicker();
                });

                //select all as per accordion
                $(document).on('change', "#topicAccordion input:checkbox.level-1", function (event) {
                    var id = $(this).data("id");
                    $("#collapse_" + id + " input:checkbox").prop("checked", $(this).is(":checked"));
                });
                $(document).on('change', "#topicAccordion input:checkbox.level-2", function (event) {
                    var parentid = $(this).data("parentid");
                    if ($("#topicAccordion input:checkbox.level-2:checked").length == 0)
                        $("#heading_" + parentid + " input:checkbox").prop("checked", false);
                    $("#heading_" + parentid + " input:checkbox").prop("checked", $("#topicAccordion input:checkbox.level-2:checked").length == $("#topicAccordion input:checkbox.level-2").length);
                });
                //select all
                $(document).on("change", ".selectAll", function () {
                    $("#topicAccordion .level-1,.level-2,.level-3").prop("checked", $(this).is(":checked"));
                });
                $(document).on("change", "#topicAccordion .level-1,.level-2,.level-3", function () {
                    if ($("#topicAccordion .level-1:checked,.level-2:checked,.level-3:checked").length == 0)
                        $(".selectAll").prop("checked", false);
                    $(".selectAll").prop("checked", $("#topicAccordion .level-1:checked,.level-2:checked,.level-3:checked").length == $("#topicAccordion .level-1,.level-2,.level-3").length);
                });
                //selected lesson count
                $(document).on('change', "#topicAccordion input:checkbox,#topicAccordion input:checkbox, .selectAll", function (event) {
                    //update selected count
                    //debugger
                    var count = 0;
                    $("#topicAccordion input:checkbox.level-3").each(function () {
                        var isLesson = $(this).data('islesson');
                        if ($(this).is(":checked")) {
                            if (isLesson === "True") {
                                count++;
                            }
                        }
                    });

                    $("#topicAccordion input:checkbox.level-2").each(function () {
                        var isLesson = $(this).data('islesson');
                        if ($(this).is(":checked")) {
                            if (isLesson === "True") {
                                count++;
                            }
                        }
                    });
                    $("#item-count").html(translatedResources.TopicsSelected.replace('{0}', count));
                });
                //lession search
                $(document).on("keyup", ".searchfield", function () {
                    var source = $(".maincard[data-searchtext]");
                    var searchString = $(this).val();
                    $(source).filter(function () {
                        $(this).toggle($(this).data("searchtext").toLowerCase().indexOf(searchString.trim().toLowerCase()) > -1)
                    });
                    if ($(".maincard:visible").length === 0) {
                        $("#nodataDiv").show();
                    }
                    else {
                        $("#nodataDiv").hide();
                    }
                });

            }
            else {
                globalFunctions.showErrorMessage(translatedResources.SelectSubjectError);
            }

        },
        postObjective = function () {            
            var mappingDetails = [];
            $("#topicAccordion input:checkbox.level-3").each(function () {

                var lessonId = $(this).data('lessonid');
                var isLesson = $(this).data('islesson');
                var lessonname = $(this).data('lessonname');

                if ($(this).is(":checked")) {
                    if (isLesson === "True") {
                        mappingDetails.push({
                            ObjectiveId: lessonId,
                            ObjectiveName: lessonname,
                            isSelected: true
                        });
                    }
                }
                else {
                    mappingDetails = mappingDetails.filter(function (item) {
                        return item.ObjectiveId !== lessonId;
                    });
                }

            });

            $("#topicAccordion input:checkbox.level-2").each(function () {

                var lessonId = $(this).data('lessonid');
                var isLesson = $(this).data('islesson');
                var lessonname = $(this).data('lessonname');

                if ($(this).is(":checked")) {
                    if (isLesson === "True") {
                        mappingDetails.push({
                            ObjectiveId: lessonId,
                            ObjectiveName: lessonname,
                            isSelected: true
                        });
                    }
                }
                else {
                    mappingDetails = mappingDetails.filter(function (item) {
                        return item.LessonId !== lessonId;
                    });
                }

            });
            var saveparams = {
                mappingDetails: mappingDetails
            };

            if (true) {
                $.ajax({
                    type: 'POST',
                    url: '/Quiz/Quiz/SaveObjectives',
                    async: false,
                    contentType: 'application/json',
                    data: JSON.stringify(saveparams),
                    success: function (data) {
                        $("#selectedObjectiveDetails").html(data);
                        $("#myModal").modal('hide');
                        $("#scrollableTopic").mCustomScrollbar({
                            setHeight: "100",
                        })
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure(data, xhr, status);
                    }
                });
                if ($("#CourseIds").val() != 0 && $("#IsPool").val() === "True") {
                    quizquestions.filterQuestion();
                }
            }
        },
        downloadQuizData = function (source, id) {

            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: '/Quiz/Quiz/GetQuizQuestionsCount',
                    data: {
                        'quizId': id
                    },
                    success: function (result) {

                        if (result.Message === "True") {
                            window.location.href = '/Quiz/Quiz/DownloadQuizDetails/?quizId=' + id;
                        }
                        else {
                            globalFunctions.showWarningMessage(translatedResources.quizQuestionsCountWarning);
                        }
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure();
                    }

                });

            }
        },

        onFailure = function (data) {
            console.log("Failed");
            console.log(data);
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                window.location.href = '/quiz/quiz';
                //$("#myModal").modal('hide');
                //init();
            }
        },
        openQuizQuestion = function (id) {
            ////debugger
            if (globalFunctions.isValueValid(id)) {
                $.post('/Shared/Shared/SetSession', { key: "SelectedQuizId", value: id }, function (data) {
                    if (typeof id !== 'undefined' && data === true)
                        window.open('/Quiz/QuizQuestions', '_self');
                    else {
                        globalFunctions.onFailure();
                    }
                });
            }
        },
        reviewQuiz = function (id) {
            //debugger;
            window.location.href = '/Quiz/Quiz/ViewQuiz/?id=' + id;
        },
        validationQuizImage = function (e) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'];

            if ($.inArray($('input[type=file]').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                $('input[type=file]').val('');
                globalFunctions.showErrorMessage(translatedResources.fileTypeValidationMessage.replace("{0}", fileExtension.join(', ')));
            }
        },
        changeStatus = function (quizid, quizname, description, passmarks, isactive, maxSubmit, quizImagePath) {
            $.ajax({
                type: 'POST',
                url: '/Quiz/Quiz/ActiveDeactiveQuiz',
                data: { QuizId: quizid },
                success: function (result) {
                    if (result) {
                        quiz.init();
                        quiz.onSaveSuccess;
                    }
                },
                error: function (msg) {
                    quiz.onFailure;
                }
            });
        };


    return {
        init: init,
        loadQuizGrid: loadQuizGrid,
        loadQuizPopup: loadQuizPopup,
        addQuizPopup: addQuizPopup,
        editQuizPopup: editQuizPopup,
        deleteQuizData: deleteQuizData,
        onSaveSuccess: onSaveSuccess,
        deleteQuizFile: deleteQuizFile,
        deleteQuizRecording: deleteQuizRecording,
        onFailure: onFailure,
        loadTopicSubtopic: loadTopicSubtopic,
        solveGroupQuiz: solveGroupQuiz,
        reviewQuiz: reviewQuiz,
        postObjective: postObjective,
        deleteObjective: deleteObjective,
        openQuizQuestion: openQuizQuestion,
        validationQuizImage: validationQuizImage,
        changeStatus: changeStatus,
        downloadQuizData: downloadQuizData
    };
}();

$(document).ready(function () {
    quiz.init();
    $('#tbl-Quiz thead').attr('class', 'thead-dark');
    $('#tbl-StudentQuizSearch thead').attr('class', 'thead-dark');
    $(document).on('click', '#btnAddQuiz', function () {
        quiz.addQuizPopup($(this));
    });
    $(document).on('click', '#btnSubmitQuiz', function () {
        var validationCheck = true;
        if ($("#IsSetTime").prop("checked")) {
            if ($('#StartDate').val().trim() != '') {
                if ($('#StartTime').val() === null || $('#StartTime').val().trim() === '') {
                    $(".validateStartTime").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
                    validationCheck = false;
                }
            }
        }
        if ($("#QuizName").val().trim() === null || $("#QuizName").val().trim() === '') {
            $(".validateQuizName").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
            validationCheck = false;
        }
        if ($("#MaxSubmit").val() === null || $("#MaxSubmit").val() === '') {
            $(".validateMaxSubmit").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
            validationCheck = false;
        }
        if ($("#QuizTime").val() === null || $("#QuizTime").val() === '') {
            $(".validateQuizTime").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
            validationCheck = false;
        }

        if (validationCheck) {
            //var isValid = globalFunctions.validateBannedWordsFormData('frmAddCycle')
            var isValid = true;
            if (isValid) {

                var formData = new FormData();
                if (!$('#QuizImagePath').val() !== '') {
                    formData.append("QuizImagePath", $('#QuizImagePath').val())
                }
                //if ($("#QuizImage").val()!=='') {
                //    var file = document.getElementById("QuizImage").files[0]
                //    formData.append("QuizImage", file);
                //}
                //file = document.getElementById("QuizImage").files[0];
                //var token = $('input[name=__RequestVerificationToken]').val();
                //formData.append("__RequestVerificationToken", token);

                formData.append("QuizName", $('#QuizName').val());
                formData.append("QuizId", $('#QuizId').val());
                formData.append("SchoolId", $('#SchoolId').val());
                formData.append("IsAddMode", $('#IsAddMode').val());
                formData.append("IsForm", $('#IsForm').val());
                formData.append("Description", CKEDITOR.instances.Description.getData());
                formData.append("MaxSubmit", $('#MaxSubmit').val());
                formData.append("QuestionPaginationRange", $('#QuestionPaginationRange').val());
                formData.append("IsRandomQuestion", $("#IsRandomQuestion").prop("checked"));
                formData.append("IsSetTime", $("#IsSetTime").prop("checked"));
                formData.append("QuizTime", $('#QuizTime').val());
                formData.append("StartDate", $('#StartDate').val());
                formData.append("StartTime", $('#StartTime').val());
                //formData.append("EndDate", $('#EndDate').val());
                formData.append("EndTime", $('#EndTime').val());
                formData.append("IsActive", $("#IsActive").prop("checked"));
                formData.append("CourseIds", $("#CourseIds").val());
                //console.log($("#IsActive").prop("checked"));
                $.each(listRecordings, function (idx, file) {
                    formData.append("recordings", file, file.name);
                });
                $.ajax({
                    type: 'POST',
                    url: '/Quiz/Quiz/SaveQuizData',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result.Success == true) {
                            quiz.onSaveSuccess(result);
                            listRecordings = [];
                        }
                        else {
                            quiz.onFailure(result);
                        }

                    },
                    error: function (msg) {
                        globalFunctions.onFailure();
                    }
                });
            }
        }
    });

    $(document).on('click', '#ViewQuizFile', function (e) {
        e.preventDefault();
        var id = $(this).data("filename");
        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=editQuizFile", "_blank");
        }
    });
    $('body').on('keyup', '#QuestionPaginationRange', function () {
        //debugger;
        if ($(this).val() === null || $(this).val() === '') {
            $("#paginationCount").html('');
        }
        else {
            if ($(this).val() <= 0) {
                $("#paginationCount").html('Please enter a value greater than or equal to 1.');
            }
            else {
                $("#paginationCount").html('');
            }
        }

    });
    $('body').on('click', '#DeleteQuizFiles', function () {
        ////debugger;
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        var fileName = $(this).attr('data-fileName');
        var uploadedfilepath = $(this).attr('data-uploadedPath');
        var quizFileId = $(this).attr('data-FileId');
        var quizId = $(this).attr('data-QuizId');
        $(document).bind('okClicked', $(this), function (e) {
            quiz.deleteQuizFile(quizFileId, quizId, fileName, uploadedfilepath);
        });
    });

    $('body').on('click', '#DeleteQuizQuestionRecording', function () {
        ////debugger;
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        var quizFileId = $(this).attr('data-fileid');
        $(document).bind('okClicked', $(this), function (e) {
            quiz.deleteQuizRecording(quizFileId);
        });
    });
    $(document).on('click', '#btnStartGroupQuiz', function () {
        $("#StartQuizId").val($(this).data("id"));
        $("#StartQuizresourceid").val($(this).data("quizresourceid"));
        $("#StartStudid").val($(this).data("studid"));
        $("#selectedQuizName").html($(this).data("quizname"));
        $("#selectedQuizTitle").html($(this).data("starttime"));
        $('#startQuiz').modal({ show: true });
        //groupQuiz.logQuizTime($(this), $(this).data("id"), $(this).data("quizresourceid"), 'Q');
    });
    $(document).on('click', '#btnSolveGroupQuiz', function () {
        //debugger; //quiz//quiz.js
        localStorage.setItem("quizStarted", 0);
        var id = $("#StartQuizId").val();
        var resourceId = $("#StartQuizresourceid").val();
        var studentId = $("#StartStudid").val();
        //var id = $(this).data("id");
        //var resourceId = $(this).data("quizresourceid");
        //var studentId = $(this).data("studid");
        quiz.solveGroupQuiz($(this), id, resourceId, studentId);

    });
    $(document).on('click', '#btnSubmitWithouTimeQuiz', function () {
        var id = $(this).data("id");
        var resourceId = $(this).data("quizresourceid");
        var studentId = $(this).data("studid");
        quiz.solveGroupQuiz($(this), id, resourceId, studentId);
    });
    //popoverEnabler.attachPopover();
    // webkit shim
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    navigator.getUserMedia = (
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia
    );

    if (typeof navigator.mediaDevices.getUserMedia === 'undefined') {
        navigator.getUserMedia({
            audio: true
        }, startUserMedia);
    } else {
        navigator.mediaDevices.getUserMedia({
            audio: true
        }).then(startUserMedia).catch(errorHandler);
    }

    function errorHandler() {
        console.log("error");
    }

    window.URL = window.URL || window.webkitURL;

    audio_context = new AudioContext();
    var minDateData = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), nowTemp.getDate(), 0, 0, 0, 0);
    var minTime = moment();
    $('.date-picker1').datetimepicker({
        format: 'DD-MMM-YYYY',
        minDate: minDateData
    }).on('dp.change', function (e) {
        //debugger;
        //if (e.date._d.getDate() == nowTemp.getDate()) {
        //    $('.time-picker').data("DateTimePicker").minDate(minTime);
        //}
        //else {
        //    $('.time-picker').data("DateTimePicker").minDate(null);
        //}

    });
    $('.time-picker').datetimepicker({
        format: 'LT'
        //,
        //minDate: minTime
    }).on('dp.change', function (ev) {
    }).data("DateTimePicker");
    var pagingValue = $('#QuestionPaginationRange').val();
    if (pagingValue == 0) {
        $('#QuestionPaginationRange').val('');
    }
    $('.selectpicker').selectpicker('refresh');

});