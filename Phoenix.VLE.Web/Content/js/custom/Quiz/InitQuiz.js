﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var quiz = function () {

    $.fn.dataTable.ext.errMode = 'none';

    var init = function () {
        loadQuizGrid();
        //quiz.noSort();
    },
        loadQuizGrid = function () {
            if ($("#IsStudent").val() == "False") {
                var _columnData = [];
                _columnData.push(
                    //{ "mData": "QuizName", "sTitle": translatedResources.QuizName, "sWidth": "25%" },
                    ////{ "mData": "StartDate", "sTitle": translatedResources.StartDateTime, "sWidth": "20%" },
                    //{ "mData": "IsStarted", "sTitle": translatedResources.Status, "sWidth": "25%" },
                    //{ "mData": "IsActive", "sClass": "no-sort text-left text-sm-center pl-3", "sTitle": translatedResources.IsActive, "sWidth": "25%" },
                    //{ "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "25%" }

                    { "mData": "QuizName", "sTitle": translatedResources.QuizName },
                    //{ "mData": "StartDate", "sTitle": translatedResources.StartDateTime, "sWidth": "20%" },
                    { "mData": "IsStarted", "sTitle": translatedResources.Status },
                    { "mData": "IsActive", "sClass": "no-sort text-left text-sm-center pl-3", "sTitle": translatedResources.IsActive },
                    { "mData": "Actions", "sClass": "text-left no-sort", "sTitle": translatedResources.actions }
                );
                initQuizGrid('tbl-Quiz', _columnData, '/Quiz/Quiz/LoadQuizGrid');
                ConvertImageToSVGFile();
            }
            else {
                var _columnData = [];
                _columnData.push(
                    { "mData": "QuizName", "sTitle": translatedResources.QuizName, "sWidth": "40%" },
                    { "mData": "GroupName", "sTitle": translatedResources.ResourceName, "sWidth": "25%" },
                    { "mData": "CreatedByName", "sTitle": translatedResources.TeacherName, "sWidth": "25%" },
                    { "mData": "TotalMarks", "sTitle": translatedResources.Score, "sWidth": "10%" }
                );
                initQuizGrid('tbl-StudentQuizSearch', _columnData, '/Quiz/Quiz/LoadStudentQuizGrid');
                $("#tbl-StudentQuizSearch_filter").hide();
                $('#tbl-StudentQuizSearch').DataTable().search('').draw();
                $("#StudentQuizSearch").on("input", function (e) {
                    e.preventDefault();
                    $('#tbl-StudentQuizSearch').DataTable().search($(this).val()).draw();
                });
            }
        },
        deleteQuizFile = function (quizFileid, quizId, fileName, uploadedfilepath) {
            ////debugger;
            $.ajax({
                type: 'POST',
                data: { id: quizFileid, quizId: quizId, fileName: fileName, UploadedFilePath: uploadedfilepath },
                url: '/Quiz/Quiz/DeleteQuizFile',
                success: function (result) {
                    ////debugger;
                    globalFunctions.showMessage('success', translatedResources.DeletedSuccessfully);
                    $("#quizFileList").html(result);
                },
                error: function (msg) {
                    quiz.onFailure;
                }
            });
        },
        deleteQuizRecording = function (quizFileid) {
            $.ajax({
                type: 'POST',
                data: { id: quizFileid },
                url: '/Quiz/Quiz/DeleteQuizRecordingFile',
                success: function (result) {
                    globalFunctions.showMessage('success', translatedResources.DeletedSuccessfully);
                    $("#audio_" + quizFileid).remove();
                },
                error: function (msg) {
                    quiz.onFailure;
                }
            });
        },
        solveGroupQuiz = function (source, id, resourceId, studentId) {
            window.location.href = '/Files/Files/Quiz/?id=' + id + '&resourceId=' + resourceId + '&studentId=' + studentId;
        },
        initQuizGrid = function (_tableId, _columnData, _url) {
            ////debugger;
            if ($("#IsStudent").val() == "False") {
                var grid = new DynamicGrid(_tableId);
                var settings = {
                    columnData: _columnData,
                    url: _url,
                    columnSelector: false,
                    columnDefs: [{
                        "targets": [0, 1, 2, 3],
                        "orderable": false
                    }],
                    searching: true,
                    serverSide: true
                };
                grid.init(settings);
            }
            else {
                var grid = new DynamicGrid(_tableId);
                var settings = {
                    columnData: _columnData,
                    url: _url,
                    columnSelector: false
                };
                grid.init(settings);
            }
            $("#tbl-Quiz_filter").hide();
            $('#tbl-Quiz').DataTable().search('').draw();
            $("#QuizListSearch").on("input", function (e) {
                e.preventDefault();
                var _searchText = $(this).val();
                if (_searchText.indexOf("</") === -1) {
                    
                    var table = $('#tbl-Quiz').DataTable().search($(this).val()).draw();
                    
                    table.on('draw', function () {
                        ConvertImageToSVGFile();
                    });
                }
                else {                    
                    //alert("Invalid characters entered");
                }
            });
        },
        loadQuizPopup = function (source, mode, id) {
            //debugger;
            var title = mode + ' ' + translatedResources.Quiz;
            window.location.href = '/Quiz/Quiz/InitAddEditQuizForm?id=' + id;
            //globalFunctions.loadPopup(source, '/Quiz/Quiz/InitAddEditQuizForm?id=' + id, title, 'quiz-dailog');

            //$(source).off("modalLoaded");
            //$(source).on("modalLoaded", function () {
            //    window.AudioContext = window.AudioContext || window.webkitAudioContext;
            //    navigator.getUserMedia = (
            //        navigator.getUserMedia ||
            //        navigator.webkitGetUserMedia ||
            //        navigator.mozGetUserMedia ||
            //        navigator.msGetUserMedia
            //    );

            //    if (typeof navigator.mediaDevices.getUserMedia === 'undefined') {
            //        navigator.getUserMedia({
            //            audio: true
            //        }, startUserMedia);
            //    } else {
            //        navigator.mediaDevices.getUserMedia({
            //            audio: true
            //        }).then(startUserMedia).catch(errorHandler);
            //    }

            //    function errorHandler() {
            //        console.log("error");
            //    }

            //    window.URL = window.URL || window.webkitURL;

            //    audio_context = new AudioContext();
            //    var minDateData = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), nowTemp.getDate(), 0, 0, 0, 0);
            //    var minTime = moment();
            //    $('.date-picker1').datetimepicker({
            //        format: 'DD-MMM-YYYY',
            //        minDate: minDateData
            //    }).on('dp.change', function (e) {
            //    });
            //    $('.time-picker').datetimepicker({
            //        format: 'LT'
            //    }).on('dp.change', function (ev) {
            //    }).data("DateTimePicker");
            //    var pagingValue = $('#QuestionPaginationRange').val();
            //    if (pagingValue == 0) {
            //        $('#QuestionPaginationRange').val('');
            //    }
            //    $('.selectpicker').selectpicker('refresh');
            //});
        },
        addQuizPopup = function (source) {
            window.location.href = '/Quiz/Quiz/InitAddEditQuizForm';
        },

        editQuizPopup = function (source, id, taskCount) {
            if (taskCount > 0) {
                globalFunctions.showErrorMessage(translatedResources.QuizTaskEditValidationMessage);
                return false;
            } else {
                loadQuizPopup(source, translatedResources.edit, id);
            }
        },
        cloneQuizData = function (source, id) {

            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: '/Quiz/Quiz/CloneQuizData',
                    data: {
                        'quizId': id
                    },
                    success: function (result) {

                        if (result.Message === "True") {
                            globalFunctions.showSuccessMessage(translatedResources.cloneQuizSuccess);
                            loadQuizGrid();
                        }
                        else {
                            globalFunctions.onFailure();
                        }
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure();
                    }

                });

            }
        },
        shareQuizWithOthers = function (source, id) {
            var title = translatedResources.SelectTeacherList;
            globalFunctions.loadPopup(source, '/Quiz/Quiz/GetTeacherListToShareQuiz?quizId=' + id, title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: translatedResources.PleaseSelect
                });
            });
        },
        postShareQuizWithTeachers = function () {
            var selectedTeachers = [];
            selectedTeachers = $("#selectteachers").val();
            if (selectedTeachers.length > 0) {
                var data = [];
                $.post('/Quiz/Quiz/PostShareQuizWithTeachers?QuizId=' + $("#quizId").val(), {
                    selectedTeachers: JSON.stringify(selectedTeachers)
                }, function (result) {
                    if (result.Message === "True") {
                        $("#myModal").modal("hide");
                        globalFunctions.showSuccessMessage(translatedResources.sharedQuizSuccess);
                    }
                }).fail(function () { globalFunctions.onFailure(); });
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.selectTeacherMsg);
            }
           
        },
        deleteQuizData = function (source, id, taskCount) {
            if (globalFunctions.isValueValid(id)) {
                if (taskCount > 0) {
                    globalFunctions.showErrorMessage(translatedResources.QuizTaskDeleteValidationMessage);
                    return false;
                } else {
                    globalFunctions.deleteItem('Quiz', '/Quiz/Quiz/DeleteQuizData', 'tbl-Quiz', source, id);
                }
            }
        },
        downloadQuizData = function (source, id) {

            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: '/Quiz/Quiz/GetQuizQuestionsCount',
                    data: {
                        'quizId': id
                    },
                    success: function (result) {

                        if (result.Message === "True") {
                            window.location.href = '/Quiz/Quiz/DownloadQuizDetails/?quizId=' + id;
                        }
                        else {
                            globalFunctions.showWarningMessage(translatedResources.quizQuestionsCountWarning);
                        }
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure();
                    }

                });

            }
        },
        downloadQuizReport = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                window.location.href = '/Quiz/Quiz/DownloadReportDataAsExcelFile/?quizId=' + id;
            }
        },
        onFailure = function (data) {
            console.log("Failed");
            console.log(data);
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },
        openQuizQuestion = function (id) {
            ////debugger
            if (globalFunctions.isValueValid(id)) {
                $.post('/Shared/Shared/SetSession', { key: "SelectedQuizId", value: id }, function (data) {
                    if (typeof id !== 'undefined' && data === true)
                        window.open('/Quiz/QuizQuestions', '_self');
                    else {
                        globalFunctions.onFailure();
                    }
                });
            }
        },
        reviewQuiz = function (id) {
            window.location.href = '/Quiz/Quiz/ViewQuiz/?id=' + id;
        },
        downloadQuizAsPDF = function (id) {
            window.location.href = '/Quiz/Quiz/DownloadQuizAsPDF/?id=' + id;
        },
        validationQuizImage = function (e) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'];

            if ($.inArray($('input[type=file]').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                $('input[type=file]').val('');
                globalFunctions.showErrorMessage(translatedResources.fileTypeValidationMessage.replace("{0}", fileExtension.join(', ')));
            }
        },
        changeStatus = function (quizid, quizname, description, passmarks, isactive, maxSubmit, quizImagePath) {
            $.ajax({
                type: 'POST',
                url: '/Quiz/Quiz/ActiveDeactiveQuiz',
                data: { QuizId: quizid },
                success: function (result) {
                    if (result) {
                        quiz.init();
                        quiz.onSaveSuccess;
                    }
                },
                error: function (msg) {
                    quiz.onFailure;
                }
            });
        };


    return {
        init: init,
        loadQuizGrid: loadQuizGrid,
        loadQuizPopup: loadQuizPopup,
        addQuizPopup: addQuizPopup,
        editQuizPopup: editQuizPopup,
        deleteQuizData: deleteQuizData,
        onSaveSuccess: onSaveSuccess,
        deleteQuizFile: deleteQuizFile,
        deleteQuizRecording: deleteQuizRecording,
        onFailure: onFailure,
        solveGroupQuiz: solveGroupQuiz,
        reviewQuiz: reviewQuiz,
        downloadQuizAsPDF: downloadQuizAsPDF,
        openQuizQuestion: openQuizQuestion,
        validationQuizImage: validationQuizImage,
        changeStatus: changeStatus,
        downloadQuizReport: downloadQuizReport,
        downloadQuizData: downloadQuizData,
        cloneQuizData: cloneQuizData,
        shareQuizWithOthers: shareQuizWithOthers,
        postShareQuizWithTeachers: postShareQuizWithTeachers
    };
}();

$(document).ready(function () {
    quiz.init();
    $('#tbl-Quiz thead').attr('class', 'thead-dark');
    $(document).on('click', '#btnAddQuiz', function () {
        quiz.addQuizPopup($(this));
    });
    $(document).on('click', '#btnSubmitQuiz', function () {
        ////debugger;
        var validationCheck = true;
        if ($("#IsSetTime").prop("checked")) {
            if ($('#StartDate').val().trim() != '') {
                if ($('#StartTime').val() === null || $('#StartTime').val().trim() === '') {
                    $(".validateStartTime").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
                    validationCheck = false;
                }
            }
        }
        if ($("#QuizName").val().trim() === null || $("#QuizName").val().trim() === '') {
            $(".validateQuizName").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
            validationCheck = false;
        }
        if ($("#MaxSubmit").val() === null || $("#MaxSubmit").val() === '') {
            $(".validateMaxSubmit").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
            validationCheck = false;
        }
        if ($("#QuizTime").val() === null || $("#QuizTime").val() === '') {
            $(".validateQuizTime").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
            validationCheck = false;
        }

        if (validationCheck) {
            var isValid = globalFunctions.validateBannedWordsFormData('frmAddCycle')
            if (isValid) {

                var formData = new FormData();
                if (!$('#QuizImagePath').val() !== '') {
                    formData.append("QuizImagePath", $('#QuizImagePath').val())
                }
                //if ($("#QuizImage").val()!=='') {
                //    var file = document.getElementById("QuizImage").files[0]
                //    formData.append("QuizImage", file);
                //}
                //file = document.getElementById("QuizImage").files[0];
                //var token = $('input[name=__RequestVerificationToken]').val();
                //formData.append("__RequestVerificationToken", token);

                formData.append("QuizName", $('#QuizName').val());
                formData.append("QuizId", $('#QuizId').val());
                formData.append("SchoolId", $('#SchoolId').val());
                formData.append("IsAddMode", $('#IsAddMode').val());
                formData.append("IsForm", $('#IsForm').val());
                formData.append("Description", $('#Description').val());
                formData.append("MaxSubmit", $('#MaxSubmit').val());
                formData.append("QuestionPaginationRange", $('#QuestionPaginationRange').val());
                formData.append("IsRandomQuestion", $("#IsRandomQuestion").prop("checked"));
                formData.append("IsSetTime", $("#IsSetTime").prop("checked"));
                formData.append("QuizTime", $('#QuizTime').val());
                formData.append("StartDate", $('#StartDate').val());
                formData.append("StartTime", $('#StartTime').val());
                //formData.append("EndDate", $('#EndDate').val());
                formData.append("EndTime", $('#EndTime').val());
                formData.append("IsActive", $("#IsActive").prop("checked"));
                formData.append("SubjectIds", $("#SubjectIds").val());
                //console.log($("#IsActive").prop("checked"));
                $.each(listRecordings, function (idx, file) {
                    formData.append("recordings", file, file.name);
                });
                $.ajax({
                    type: 'POST',
                    url: '/Quiz/Quiz/SaveQuizData',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result.Success == true) {
                            quiz.onSaveSuccess(result);
                            listRecordings = [];
                        }
                        else {
                            quiz.onFailure(result);
                        }

                    }
                });
            }
        }
    });
    
    $(document).on('click', '#downloadPDF', function (e) {
        var isQuestionExist = $(this).data("isquestionexist");
        if (isQuestionExist == 'True') {
            var id = $(this).data("quizid");
            quiz.downloadQuizAsPDF(id);
        } else {
            globalFunctions.showErrorMessage(translatedResources.NoDownloadQuestion);
        }
    });
    $(document).on('click', '#viewQuiz', function (e) {
        var isQuestionExist = $(this).data("isquestionexist");
        if (isQuestionExist == 'True') {
            var id = $(this).data("quizid");
            quiz.reviewQuiz(id);
        } else {
            globalFunctions.showErrorMessage(translatedResources.NoViewQuestion);
        }
    });
    $(document).on('click', '#ViewQuizFile', function (e) {
        e.preventDefault();
        var id = $(this).data("filename");
        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=editQuizFile", "_blank");
        }
    });
    $('body').on('keyup', '#QuestionPaginationRange', function () {
        ////debugger;
        if ($(this).val() === null || $(this).val() === '') {
            $("#paginationCount").html('');
        }
        else {
            if ($(this).val() <= 0) {
                $("#paginationCount").html('Please enter a value greater than or equal to 1.');
            }
            else {
                $("#paginationCount").html('');
            }
        }

    });
    $('body').on('click', '#DeleteQuizFiles', function () {
        //////debugger;
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        var fileName = $(this).attr('data-fileName');
        var uploadedfilepath = $(this).attr('data-uploadedPath');
        var quizFileId = $(this).attr('data-FileId');
        var quizId = $(this).attr('data-QuizId');
        $(document).bind('okClicked', $(this), function (e) {
            quiz.deleteQuizFile(quizFileId, quizId, fileName, uploadedfilepath);
        });
    });

    $('body').on('click', '#DeleteQuizQuestionRecording', function () {
        //////debugger;
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        var quizFileId = $(this).attr('data-fileid');
        $(document).bind('okClicked', $(this), function (e) {
            quiz.deleteQuizRecording(quizFileId);
        });
    });
    $(document).on('click', '#btnStartGroupQuiz', function () {
        ////debugger;
        $("#StartQuizId").val($(this).data("id"));
        $("#StartQuizresourceid").val($(this).data("quizresourceid"));
        $("#StartStudid").val($(this).data("studid"));
        $("#selectedQuizName").html($(this).data("quizname"));
        $("#selectedQuizTitle").html($(this).data("starttime"));
        $('#startQuiz').modal({ show: true });
        //groupQuiz.logQuizTime($(this), $(this).data("id"), $(this).data("quizresourceid"), 'Q');
    });
    $(document).on('click', '#btnSolveGroupQuiz', function () {
        ////debugger; //quiz//quiz.js
        localStorage.setItem("quizStarted", 0);
        var id = $("#StartQuizId").val();
        var resourceId = $("#StartQuizresourceid").val();
        var studentId = $("#StartStudid").val();
        //var id = $(this).data("id");
        //var resourceId = $(this).data("quizresourceid");
        //var studentId = $(this).data("studid");
        quiz.solveGroupQuiz($(this), id, resourceId, studentId);

    });
    $(document).on('click', '#btnSubmitWithouTimeQuiz', function () {
        var id = $(this).data("id");
        var resourceId = $(this).data("quizresourceid");
        var studentId = $(this).data("studid");
        quiz.solveGroupQuiz($(this), id, resourceId, studentId);
    });

    var nowTemp = new Date();
    var minDateData = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), nowTemp.getDate(), 0, 0, 0, 0);
    var minTime = moment();
    $('.date-picker1').datetimepicker({
        format: 'DD-MMM-YYYY',
        minDate: minDateData
    }).on('dp.change', function (e) {
        ////debugger;
        //if (e.date._d.getDate() == nowTemp.getDate()) {
        //    $('.time-picker').data("DateTimePicker").minDate(minTime);
        //}
        //else {
        //    $('.time-picker').data("DateTimePicker").minDate(null);
        //}

    });
    $('.time-picker').datetimepicker({
        format: 'LT'
        //,
        //minDate: minTime
    }).on('dp.change', function (ev) {
    }).data("DateTimePicker");
    var pagingValue = $('#QuestionPaginationRange').val();
    if (pagingValue == 0) {
        $('#QuestionPaginationRange').val('');
    }
    $('.selectpicker').selectpicker('refresh');

});

function ConvertImageToSVGFile() {

    $('img.svg').each(function () {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
}