﻿
//for multi language keyboard extension
$(function () {
    var t,
        o = '',
        layouts = [];

    // Change display language, if the definitions are available
    showKb = function (layout) {
        var kb = $('#txtTextAreaID').getkeyboard();
        kb.options.layout = layout;
        // redraw keyboard with new layout
        kb.redraw();
    };

    $.each(jQuery.keyboard.layouts, function (i, l) {
        if (l.name) {
            layouts.push([i, l.name]);
        }
    });
    // sort select options by language name, not
    layouts.sort(function (a, b) {
        return a[1] > b[1] ? 1 : a[1] < b[1] ? -1 : 0;
    });
    $.each(layouts, function (i, l) {
        o += '<option value="' + l[0] + '">' + l[1] + '</option>';
    });


    $('#txtTextAreaID').keyboard({
        layout: 'qwerty',
        stayOpen: false,
        usePreview: false
    }).addTyping({
        showTyping: true,
        delay: 200
    }).previewKeyset();

    $('#ddlLanguage')
        .html(o)
        .change(function () {
            var kb = $('#txtTextAreaID'),
                $this = $(this),
                $opt = $this.find('option:selected'),
                layout = $this.val();
            //$('h2').text($opt.text());
            showKb(layout);
        }).trigger('change');

    $("#txtTextAreaID_keyboard").addClass('d-none');

});