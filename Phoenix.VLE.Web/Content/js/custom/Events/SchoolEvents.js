﻿$(document).ready(function () {
    //timepicker
    $('.time-picker').datetimepicker({
        format: 'LT'
        //debug: true
    });

    //datepicker
    $('.date-picker').datetimepicker({
        format: 'MM/DD/YYYY'
        //debug: true
    });
    $("#ResourceFileUploade").change(function (e) {
        e.preventDefault();
        var formData = new FormData();
        var file = document.getElementById("ResourceFileUploade").files[0];

        //formData.append("Title", $("#Title").val());
        //formData.append("EventId", $("#EventId").val());
        //formData.append("StartDate", $("#StartDate").val());
        //formData.append("StartTime", $("#StartTime").val());
        //formData.append("DurationId", $("#DurationId").val());
        //formData.append("EventCategoryId", $("#EventCategoryId").val());
        //formData.append("EventPriority", document.getElementsByName("EventPriority :checked"));
        //formData.append("IsTeacherVisible", $("#IsTeacherVisible").val());
        //formData.append("EventRepeatTypeId", $("#EventRepeatTypeId").val());
        //formData.append("EventRepeatTimes", $("#EventRepeatTimes").val());
        //formData.append("IsCopyMessage", $("#IsCopyMessage").val());
        //formData.append("Discription", $("#Discription").val());
        formData.append("ResourceFileUploade", file);

        $.ajax({
            url: '/Events/SchoolEvents/SaveResourceFile',
            type: 'POST',
            data: formData ,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function ($data) {
                
            }
        });
    });


});
