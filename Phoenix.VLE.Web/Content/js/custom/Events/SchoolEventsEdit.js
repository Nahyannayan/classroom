﻿$(function () {
});

function loadRadioButton(model) {
    var eventPriority = model.EventPriority;
    $("#" + eventPriority).prop("checked", true);
    var IsTeacherVisible = model.IsTeacherVisible;
    if (IsTeacherVisible)
        $("#IsTeacherVisibleYes").prop("checked", true);
    else
        $("#IsTeacherVisibleNo").prop("checked", true);
    var EventRepeatTypeId = model.EventRepeatTypeId;
    var EventRepeatTypes = $("EventRepeatTypeId");
    $('input:radio[name=EventRepeatTypeId]:nth(' + (EventRepeatTypeId - 1) + ')').attr('checked', true);
    $("#EventRepeatTimes").val(model.EventRepeatTimes);
    $("#RepeatTimes .filter-option-inner-inner").html(model.EventRepeatTimes);
    //console.log(model.EventRepeatTimes);
}