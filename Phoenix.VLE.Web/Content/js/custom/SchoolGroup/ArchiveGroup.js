﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
//var popoverEnabler = new LanguageTextEditPopover();

function archiveSchoolGroup() {
    //debugger;
    if ($('#selectAllUnassigned').is(':checked')) {
        $('#selectAllUnassigned').prop('checked', false);
    }
    var selectedMembers = $("#activeGroupList input[type='checkbox']:checked");
    var assignmentsToArchive = [];
    if (selectedMembers != null && selectedMembers.length > 0) {
        selectedMembers.each(function (i, elem) {
            assignmentsToArchive = assignmentsToArchive + elem.value + ',';
        });
        $.ajax({
            type: 'POST',
            data: { assignmentsToArchive: assignmentsToArchive },
            url: '/SchoolStructure/SchoolGroups/UpdateArchiveGroup',
            success: function (result) {
                loadGroupList();
                // location.reload();
            },
            error: function (data) { }
        });
    }
    else {
        globalFunctions.showWarningMessage(translatedResources.PleaseGroupToArchive);
    }
}
function loadGroupList() {
    loadActiveGroup();
    loadArchiveGroup();
}

function loadActiveGroup() {
    $.ajax({
        url: '/SchoolStructure/SchoolGroups/GetActiveSchoolGroup',
        success: function (result) {
            $('#activeGroupList').html(result);
        },
        error: function (data) { }
    });
}
function loadArchiveGroup() {
    $.ajax({
        url: '/SchoolStructure/SchoolGroups/GetArchivedSchoolGroup',
        success: function (result) {
            $('#archiveGroupList').html(result);
        },
        error: function (data) { }
    });
}
$(document).on('keyup', '#availableMemberSearch', function (e) {
    var searchText = $(this).val().toLowerCase();
    $('#activeGroupList ul li').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
});
$(document).on('keyup', '#groupMemberSearch', function (e) {
    //if (e.which === 13) {
    var searchText = $(this).val().toLowerCase();
    $('#archiveGroupList ul li').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
    //}
});



$('#selectAllUnassigned').change(function () {
    //debugger;
    var chk = $(this).is(':checked');
    $('input[type=checkbox]', "#activeGroup").each(function () {
        $(this).prop('checked', chk);
    });
});

$(document).ready(function () {

    $(".delegates-wrapper").mCustomScrollbar({
        setHeight: "250",
        autoExpandScrollbar: true
    });
});
