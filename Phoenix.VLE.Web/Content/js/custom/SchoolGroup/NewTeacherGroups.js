﻿var pageSize = 8;
var _userLognType = "", usr_Parent = "parent", usr_Student = "student";
var teacherGroup = function () {
    var init = function () {

        _userLognType = $("#hdnLognUsrType").val();

        if (_userLognType == usr_Parent || _userLognType == usr_Student) {
            CheckMISGroupsCountForSTandPT();
        }
        else {
            loadSchoolGroupData($("#mis-groups"), "", false);
        }
       
        addSendEmailButton();
    },
        
        CheckMISGroupsCountForSTandPT = function () {
            $.ajax({
                type: 'POST',
                url: '/SchoolGroups/CheckMISGroupsCountForStudentandParent',
                data: {
                    'pageIndex': 1,
                    'searchString': ""
                },
                success: function (result) {
                    if (result.Success === true) {
                        teacherGroup.loadOtherGroupsData($("#bespoke-groups"), '', false);
                        $('#btnLoadMISGroups').removeClass('active')
                        $('#btnLoadMISGroups').addClass('d-none')
                        $('#btnLoadOtherGroups').addClass('active')
                        $('#misGroups').removeClass('active');
                        $('#misGroups').removeClass('show');
                        $('#otherGroups').addClass('active');
                        $('#otherGroups').addClass('show');
                    }
                    else {
                        teacherGroup.loadSchoolGroupData($("#mis-groups"), "", false);
                    }
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure();
                }

            });
        },
        loadSchoolGroupData = function (source, searchString, isSearchCall) {
            var indexValue = 0;
            var searchTxt = searchString;
            if ($(source).children().length == 0 || isSearchCall) {
                loadPartials(source, '/SchoolGroups/GetSchoolGroupsWithPagination?pageIndex=1&searchString=' + encodeURIComponent(searchString));
                $(source).off("partialLoaded");
                $(source).on("partialLoaded", function () {

                    $("#mis-groups").removeClass('owl-hidden');
                    source.trigger('destroy.owl.carousel');
                    applyCarousel(source, { navigation: true, margin: 8, items: 1, slideBy: 1 });
                    enableCarouselNav(source);
                    hideNextPrevIcon(source);
                    $(source).off("click", ".owl-next").on("click", ".owl-next", function () {
                        //debugger;

                        var carouselIndex = 0;
                        if (indexValue > 0) {
                            carouselIndex = indexValue + 1;
                            indexValue = carouselIndex;
                        }
                        else {
                            carouselIndex = carouselIndex + 1;
                            indexValue = carouselIndex;
                        }

                        var pageIndex = $(source).find(".item").length;
                        var totalRecordsCount = $(source).find(':first .row.item').data('totalrow');
                        $(source).find(':first .row.item').data('pageindex', (pageIndex + 1));
                        var totalPageIndex = Math.ceil(totalRecordsCount / pageSize);


                        if (pageIndex == carouselIndex) {

                            $.ajax({
                                url: "/SchoolGroups/GetSchoolGroupsWithPagination?pageIndex=" + (pageIndex + 1) + "&searchString=" + encodeURIComponent(searchTxt),
                                success: function (result) {
                                    if (result.trim() !== "") {
                                        source.trigger("add.owl.carousel", [result]).trigger("refresh.owl.carousel");
                                        source.trigger('to.owl.carousel', pageIndex);
                                        enableCarouselNav(source);
                                    } else {
                                        $(source).find(".owl-next").addClass("disabled");
                                    }
                                    //To disable next button once reached to last page
                                    if (totalPageIndex == (pageIndex + 1)) {
                                        $(source).find(".owl-next").addClass("disabled");
                                        $(source).off("click", ".owl-next");
                                    }

                                    //To resolve Ipad group menu issue, Deepak Singh on 11/Nov/2020
                                    changeGroupAction();

                                    //To hide and show groups changes, Deepak Singh on 9/Dec/2020
                                    if ($("#btnHideShowGroup").length > 0) {
                                        var isShow = $("#btnHideShowGroup").data('isshow');
                                        if (isShow === 1) {
                                             $('.showHideGroup').removeClass('d-none');
                                        }
                                        else {
                                            $('.showHideGroup').addClass('d-none');
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            //To disable next button once reached to last page
                            if (totalPageIndex == (pageIndex + 1)) {
                                $(source).find(".owl-next").addClass("disabled");
                                $(source).off("click", ".owl-next");
                            }
                            else {
                                $(source).find(".owl-next").removeClass("disabled");
                            }
                            return false;
                        }
                    });
                    $(source).off("click", ".owl-prev").on("click", ".owl-prev", function () {
                        indexValue = indexValue - 1;
                    });
                });
            }


        },
        loadOtherGroupsData = function (source, searchString, isSearchCall) {
            var indexValue = 0;
            var searchTxt = searchString;
            if ($(source).children().length == 0 || isSearchCall) {
                loadPartials(source, "/SchoolGroups/GetOtherGroupsWithPagination?pageIndex=1&searchString=" + encodeURIComponent(searchString));
                $(source).off("partialLoaded").on("partialLoaded", function () {
                    $("#bespoke-groups").removeClass('owl-hidden');
                    source.trigger('destroy.owl.carousel');
                    applyCarousel(source, { navigation: true, margin: 8, items: 1, slideBy: 1 });
                    enableCarouselNav(source);
                    hideNextPrevIcon(source);
                    $(source).off("click", ".owl-next").on("click", ".owl-next", function () {
                        //debugger;

                        var carouselIndex = 0;
                        if (indexValue > 0) {
                            carouselIndex = indexValue + 1;
                            indexValue = carouselIndex;
                        }
                        else {
                            carouselIndex = carouselIndex + 1;
                            indexValue = carouselIndex;
                        }
                        var pageIndex = $(source).find(".item").length;
                        var totalRecordsCount = $(source).find(':first .row.item').data('totalrow');
                        $(source).find(':first .row.item').data('pageindex', (pageIndex + 1));
                        var totalPageIndex = Math.ceil(totalRecordsCount / pageSize);


                        if (pageIndex == carouselIndex) {
                            $.ajax({
                                url: "/SchoolGroups/GetOtherGroupsWithPagination?pageIndex=" + (pageIndex + 1) + "&searchString=" + encodeURIComponent(searchTxt),
                                success: function (result) {
                                    if (result.trim() !== "") {
                                        source.trigger("add.owl.carousel", [result]).trigger("refresh.owl.carousel");
                                        source.trigger('to.owl.carousel', pageIndex);
                                        enableCarouselNav(source);
                                    } else {
                                        $(source).find(".owl-next").addClass("disabled");
                                    }
                                    //To disable next button once reached to last page
                                    if (totalPageIndex == (pageIndex + 1)) {
                                        $(source).find(".owl-next").addClass("disabled");
                                        $(source).off("click", ".owl-next");
                                    }

                                    //To resolve Ipad group menu issue, Deepak Singh on 11/Nov/2020
                                    changeGroupAction();
                                }
                            });
                        }
                        else {
                            //To disable next button once reached to last page
                            if (totalPageIndex == (pageIndex + 1)) {
                                $(source).find(".owl-next").addClass("disabled");
                                $(source).off("click", ".owl-next");
                            }
                            else {
                                $(source).find(".owl-next").removeClass("disabled");
                            }
                            return false;
                        }
                    });
                    $(source).off("click", ".owl-prev").on("click", ".owl-prev", function () {
                        indexValue = indexValue - 1;
                    });

                });
            }

        },
        loadArchivedSchoolGroupsData = function (source, searchString, isSearchCall) {
            var indexValue = 0;
            var searchTxt = searchString;
            if ($(source).children().length == 0 || isSearchCall) {
                loadPartials(source, "/SchoolGroups/GetArchivedGroupsWithPagination?pageIndex=1&searchString=" + encodeURIComponent(searchString));
                $(source).off("partialLoaded").on("partialLoaded", function () {

                    $("#archived-school-groups").removeClass('owl-hidden');
                    source.trigger('destroy.owl.carousel');
                    applyCarousel(source, { navigation: true, margin: 8, items: 1, slideBy: 1 });
                    enableCarouselNav(source);
                    hideNextPrevIcon(source);
                    $(source).off("click", ".owl-next").on("click", ".owl-next", function () {
                        //debugger;

                        var carouselIndex = 0;
                        if (indexValue > 0) {
                            carouselIndex = indexValue + 1;
                            indexValue = carouselIndex;
                        }
                        else {
                            carouselIndex = carouselIndex + 1;
                            indexValue = carouselIndex;
                        }

                        var pageIndex = $(source).find(".item").length;
                        var totalRecordsCount = $(source).find(':first .row.item').data('totalrow');
                        $(source).find(':first .row.item').data('pageindex', (pageIndex + 1));
                        var totalPageIndex = Math.ceil(totalRecordsCount / pageSize);


                        if (pageIndex == carouselIndex) {

                            $.ajax({
                                url: "/SchoolGroups/GetArchivedGroupsWithPagination?pageIndex=" + (pageIndex + 1) + "&searchString=" + encodeURIComponent(searchTxt),
                                success: function (result) {
                                    if (result.trim() !== "") {
                                        source.trigger("add.owl.carousel", [result]).trigger("refresh.owl.carousel");
                                        source.trigger('to.owl.carousel', pageIndex);
                                        enableCarouselNav(source);
                                    } else {
                                        $(source).find(".owl-next").addClass("disabled");
                                    }
                                    //To disable next button once reached to last page
                                    if (totalPageIndex == (pageIndex + 1)) {
                                        $(source).find(".owl-next").addClass("disabled");
                                        $(source).off("click", ".owl-next");
                                    }
                                }
                            });
                        }
                        else {
                            //To disable next button once reached to last page
                            if (totalPageIndex == (pageIndex + 1)) {
                                $(source).find(".owl-next").addClass("disabled");
                                $(source).off("click", ".owl-next");
                            }
                            else {
                                $(source).find(".owl-next").removeClass("disabled");
                            }
                            return false;
                        }
                    });
                    $(source).off("click", ".owl-prev").on("click", ".owl-prev", function () {
                        indexValue = indexValue - 1;
                    });

                });
            }

        },
        loadArchivedBeSpokeGroupsData = function (source, searchString, isSearchCall) {
            var indexValue = 0;
            var searchTxt = searchString;
            if ($(source).children().length == 0 || isSearchCall) {
                loadPartials(source, "/SchoolGroups/GetArchivedBespokeGroupsWithPagination?pageIndex=-1&searchString=" + encodeURIComponent(searchString));
                $(source).off("partialLoaded").on("partialLoaded", function () {
                    $("#archived-school-groups").removeClass('owl-hidden');
                    source.trigger('destroy.owl.carousel');
                    applyCarousel(source, { navigation: true, margin: 8, items: 1, slideBy: 1 });
                    enableCarouselNav(source);
                    hideNextPrevIcon(source);
                    $(source).off("click", ".owl-next").on("click", ".owl-next", function () {
                        //debugger;

                        var carouselIndex = 0;
                        if (indexValue > 0) {
                            carouselIndex = indexValue + 1;
                            indexValue = carouselIndex;
                        }
                        else {
                            carouselIndex = carouselIndex + 1;
                            indexValue = carouselIndex;
                        }

                        var pageIndex = $(source).find(".item").length;
                        var totalRecordsCount = $(source).find(':first .row.item').data('totalrow');
                        $(source).find(':first .row.item').data('pageindex', (pageIndex + 1));
                        var totalPageIndex = Math.ceil(totalRecordsCount / pageSize);


                        if (pageIndex == carouselIndex) {

                            $.ajax({
                                url: "/SchoolGroups/GetArchivedBespokeGroupsWithPagination?pageIndex=" + (pageIndex + 1) + "&searchString=" + encodeURIComponent(searchTxt),
                                success: function (result) {
                                    if (result.trim() !== "") {
                                        source.trigger("add.owl.carousel", [result]).trigger("refresh.owl.carousel");
                                        source.trigger('to.owl.carousel', pageIndex);
                                        enableCarouselNav(source);
                                    } else {
                                        $(source).find(".owl-next").addClass("disabled");
                                    }
                                    //To disable next button once reached to last page
                                    if (totalPageIndex == (pageIndex + 1)) {
                                        $(source).find(".owl-next").addClass("disabled");
                                        $(source).off("click", ".owl-next");
                                    }
                                }
                            });
                        }
                        else {
                            //To disable next button once reached to last page
                            if (totalPageIndex == (pageIndex + 1)) {
                                $(source).find(".owl-next").addClass("disabled");
                                $(source).off("click", ".owl-next");
                            }
                            else {
                                $(source).find(".owl-next").removeClass("disabled");
                            }
                            return false;
                        }
                    });
                    $(source).off("click", ".owl-prev").on("click", ".owl-prev", function () {
                        indexValue = indexValue - 1;
                    });

                });
            }
        },
        saveStudentDetails = function () {

        },
        loadGroupQuizPopup = function (source, id, groupId, moduleId, folderId) {
            var title = translatedResources.addgroupquiz;
            var grId = groupId; //$("#SectionId").val();
            globalFunctions.loadPopup(source, '/Files/Files/InitAddEditGroupQuiz?id=' + id + '&grId=' + grId + '&pfId=' + folderId + '&moduleId=' + moduleId, title, 'file-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {

            });

            $('.selectpicker').selectpicker('refresh');
        },

        createZipPackage = function (source) {
            var file = FileDownloader.getInstance();
            file.createZipFile(source, "/Schoolstructure/SchoolGroups/CreateZipFile?id=" + $(source).data("id"));
        },
        addnewSchoolGroup = function (id) {
            window.location.assign("/SchoolStructure/SchoolGroups/AddEditSchoolGroupForm?id=" + id);
        },
        enableCarouselNav = function (source) {
            $(source).find(".owl-nav, .owl-prev, .owl-next").removeClass("disabled");
        },
        hideNextPrevIcon = function (source) {
            //var totalRecordsCount = parseInt($(source).find("#hdnTotalCount").val());
            var totalRecordsCount = $(source).find(':first .row.item').data('totalrow');
            if (totalRecordsCount <= pageSize || isNaN(totalRecordsCount)) {
                $(source).find(".owl-nav, .owl-prev, .owl-next").addClass("disabled");
            }
            else {
                $(source).find(".owl-nav, .owl-prev, .owl-next").removeClass("disabled");
            }
        },
        applyCarousel = function (source, options) {
            var settings = {
                navigation: false,
                autoplay: false,
                nav: true,
                dots: false,
                loop: false,
                autoplayHoverPause: true,
                rtl: direction,
                mouseDrag: false
            };
            $(source).owlCarousel(Object.freeze($.extend(settings, options)));
        },

        loadPartials = function (source, url) {
            $(source).load(url, function () {
                $(source).trigger("partialLoaded");
                //To resolve Ipad group menu issue, Deepak Singh on 11/Nov/2020
                changeGroupAction();
            });
        },
        addSendEmailButton = function () {
            $("#breadcrumbActions").html('');
            $("#breadcrumbActions").html(`<ul class="list-pattern"><li><a href="javascript:void(0)" id="btnSendMessageToAll" class="btn btn-outline-primary m-0 text-semibold"> <i class="fas fa-paper-plane mr-2"></i> `+ translatedResources.EmailEveryone+`</a></li></ul>`);
            $("#breadcrumbActions").removeClass("d-none");
        },
        DeleteOtherGroups = function DeleteOtherGroups(deletebespoke) {
            globalFunctions.notyDeleteConfirm(deletebespoke, translatedResources.deleteConfirm);
            $(document).bind('okToDelete', deletebespoke, function (e) {
                var groupId = $(deletebespoke).attr('data-grpid');
                $.post("/SchoolStructure/SchoolGroups/DeleteBespokeGroup", { groupId: groupId }, function (response) {
                    if (response.Success === true) {
                        teacherGroup.loadOtherGroupsData($("#bespoke-groups"), "", true);
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                    } else {
                        globalFunctions.onFailure();
                    }

                });
            });
        };


    return {
        init: init,
        saveStudentDetails: saveStudentDetails,
        loadSchoolGroupData: loadSchoolGroupData,
        loadOtherGroupsData: loadOtherGroupsData,
        loadGroupQuizPopup: loadGroupQuizPopup,
        loadArchivedSchoolGroupsData: loadArchivedSchoolGroupsData,
        loadArchivedBeSpokeGroupsData: loadArchivedBeSpokeGroupsData,
        enableCarouselNav: enableCarouselNav,
        createZipPackage: createZipPackage,
        addnewSchoolGroup: addnewSchoolGroup,
        hideNextPrevIcon: hideNextPrevIcon,
        DeleteOtherGroups: DeleteOtherGroups,
        addSendEmailButton: addSendEmailButton,
        CheckMISGroupsCountForSTandPT: CheckMISGroupsCountForSTandPT
    };
}();

function archiveSchoolGroup() {

    var _selectedMembers = $("#activeGroupList input[type='checkbox']:checked");
    if (_selectedMembers.length == 0) {
        globalFunctions.showWarningMessage(translatedResources.SelectGroupMessage);
    }
    else {
        //Only to assign source, please follow standard way as in other js files for confirmation popup
        var source = $("#activeGroupList");
        globalFunctions.notyDeleteConfirm(source, translatedResources.archiveConfirm);
        $(document).bind('okToDelete', source, function (e) {
            if ($('#selectAllUnassigned').is(':checked')) {
                $('#selectAllUnassigned').prop('checked', false);
            }
            var selectedMembers = $("#activeGroupList input[type='checkbox']:checked");
            var assignmentsToArchive = [];
            if (selectedMembers != null && selectedMembers.length > 0) {
                selectedMembers.each(function (i, elem) {
                    assignmentsToArchive = assignmentsToArchive + elem.value + ',';
                });
                $.ajax({
                    type: 'POST',
                    data: { assignmentsToArchive: assignmentsToArchive },
                    url: '/SchoolStructure/SchoolGroups/UpdateArchiveGroup',
                    success: function (result) {
                        loadGroupList();
                        // location.reload();
                    },
                    error: function (data) { }
                });
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.PleaseGroupToArchive);
            }
        });
    }

}

function loadGroupList() {
    loadActiveGroup();
    loadArchiveGroup();
}

function loadActiveGroup() {
    $.ajax({
        url: '/SchoolStructure/SchoolGroups/GetActiveSchoolGroup',
        success: function (result) {
            $('#activeGroupList').html(result);
        },
        error: function (data) { }
    });
}

function loadArchiveGroup() {
    $.ajax({
        url: '/SchoolStructure/SchoolGroups/GetArchivedSchoolGroup',
        success: function (result) {
            $('#archiveGroupList').html(result);
        },
        error: function (data) { }
    });
}

$(document).ready(function () {
    teacherGroup.init();
   
    //$('body').on('click', '#btnLoadMISGroups', function () {

    //    var searchValue = "";
    //    teacherGroup.loadSchoolGroupData($("#mis-groups"), searchValue, false);
    //});

    $(document).on('click', '#btnAddGroupQuiz', function () {

        var groupId = $(this).data('groupid');
        var folderId = $(this).data('folderid');
        var moduleId = $(this).data('moduleid');
        teacherGroup.loadGroupQuizPopup($(this), 0, groupId, moduleId, folderId);
    });
    

    $(document).on('keyup', '#availableMemberSearch', function (e) {
        var searchText = $(this).val().toLowerCase();
        $('#activeGroupList ul li').each(function () {
            var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
            $(this).toggle(showCurrentLi);
        });
    });

    $(document).on('keyup', '#groupMemberSearch', function (e) {
        var searchText = $(this).val().toLowerCase();
        $('#archiveGroupList ul li').each(function () {
            var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
            $(this).toggle(showCurrentLi);
        });
    });


    $(document).on("keypress", "#searchMISGroupContent", function (e) {
        if (e.which == 13) {
            var searchText = $(this).val().toLowerCase();
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchText, true);

        }
    });

    $(document).on("keypress", "#searchBespokeGroupContent", function (e) {
        if (e.which == 13) {
            var searchText = $(this).val().toLowerCase();
            teacherGroup.loadOtherGroupsData($("#bespoke-groups"), searchText, true);

        }
    });

    $(document).on("keypress", "#searchArchivedGroupContent", function (e) {
        if (e.which == 13) {
            var searchText = $(this).val().toLowerCase();
            teacherGroup.loadArchivedSchoolGroupsData($("#archived-school-groups"), searchText, true);

        }
    });

    $(document).on("keypress", "#searchArchivedBespokeGroupContent", function (e) {
        if (e.which == 13) {
            var searchText = $(this).val().toLowerCase();
            teacherGroup.loadArchivedBeSpokeGroupsData($("#archived-bespoke-groups"), searchText, true);

        }
    });

    $(document).on("click", "#myTab > li", function (e) {
        
        var id = $(this).children('a').attr('id');
        var searchText = '';
        if (id == 'btnLoadMISGroups') {
            searchText = $('#searchMISGroupContent').val();
            if (searchText != '') {
                $('#searchMISGroupContent').val('');
                teacherGroup.loadSchoolGroupData($("#mis-groups"), '', true);
            }
            else {
                teacherGroup.loadSchoolGroupData($("#mis-groups"), '', false);
            }
        }
        else if (id == 'btnLoadOtherGroups') {
            searchText = $('#searchBespokeGroupContent').val();
            if (searchText != '') {
                $('#searchBespokeGroupContent').val('');
                teacherGroup.loadOtherGroupsData($("#bespoke-groups"), '', true);
            }
            else {
                teacherGroup.loadOtherGroupsData($("#bespoke-groups"), '', false);
            }
        }
        else if (id == 'btnLoadArchivedGroups') {
            searchText = $('#searchArchivedBespokeGroupContent').val();
            if (searchText != '') {
                $('#searchArchivedBespokeGroupContent').val('');
                teacherGroup.loadArchivedBeSpokeGroupsData($("#archived-bespoke-groups"), '', true);
            }
            else {
                teacherGroup.loadArchivedBeSpokeGroupsData($("#archived-bespoke-groups"), '', false);
            }
        }
        else if (id == 'btnLoadGroupsTobeArchived') {
            $.get("/SchoolStructure/SchoolGroups/ArchiveGroupList", function (response) {
                $('#groups-tobe-archived').html(response);
                $('.selectpicker').selectpicker('refresh');
                $(".delegates-wrapper").mCustomScrollbar({
                    setHeight: "250",
                    autoExpandScrollbar: true
                });
            });
        }

    });


    $(document).on('click', '#btnAddSchoolGroup', function () {
        teacherGroup.addnewSchoolGroup();
    });

    $(document).on('click', '#btnAddNewGroup', function () {
        groupDetail.loadNewGroupCreatePopup($(this), translatedResources.add);

    });

    //On click of group header section redirect to group detail page
    $('body').off('click', '.group-header').on('click', '.group-header', function (e) {
        if (e.target !== this && e.target.classList.contains("associatedUsers") == false)
            return;

        var id = $(this).data('id');
        location.href = "/SchoolStructure/SchoolGroups/GroupDetails?grpId=" + id;
    });

    
    $(document).off('click', '#btnHideShowGroup').on('click', '#btnHideShowGroup', function (e) {
        var isShow = $(this).data('isshow');
        if (isShow === 0) {
            $(this).data('isshow', 1);
            $('.showHideGroup').removeClass('d-none');
            $(this).removeClass('btn-primary').addClass('btn-outline-primary');
        }
        else {
            $(this).data('isshow', 0);
            $('.showHideGroup').addClass('d-none');
            $(this).removeClass('btn-outline-primary').addClass('btn-primary');
        }
    });

    $("#btnCancelShowHideGroup").off("click").on("click", function (e) {
        $('#btnHideShowGroup').data('isshow', 0);
        $('.showHideGroup').addClass('d-none');
        $('#btnHideShowGroup').removeClass('btn-outline-primary').addClass('btn-primary');
    });

    $("#btnSaveShowHideGroupData").off("click").on("click", function (e) {
        var selectedGroupIds = [];
        $('div.showHideGroup input[type=checkbox]').each(function () {
            if ($(this).is(":checked")) {
                selectedGroupIds.push($(this).attr('data-id'));
            }
        });
        //console.log(selected);

        var formData = new FormData();
        formData.append("ClassGroupIdsToHide", selectedGroupIds);

        $.ajax({
            url: '/SchoolStructure/SchoolGroups/UpdateSchoolGroupsToHide',
            type: 'POST',
            //headers: headers,
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.Success === true) {
                    globalFunctions.showSuccessMessage(translatedResources.showSuccessMessage);
                    //teacherGroup.loadSchoolGroupData($("#mis-groups"), '', true);
                    $('#btnHideShowGroup').data('isshow', 0);
                    $('.showHideGroup').addClass('d-none');
                    $('#btnHideShowGroup').removeClass('btn-outline-primary').addClass('btn-primary');
                }
                else {
                    globalFunctions.onFailure();
                }
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure();
            },
        });

        
    });

    
    $('body').on('click', '.showHideGroup .custom-control-label', function () {
       
        $(this).parent('.custom-checkbox').find('input').on('change', function () {
            if ($(this).parent('.custom-checkbox').find('input').is(":checked")) {
                var _schoolGroupId = $(this).attr('data-id');
                $('#eye_' + _schoolGroupId + ' >i').removeClass('fa-eye')
                $('#eye_' + _schoolGroupId + ' >i').addClass('fa-eye-slash')
            }
            else {
                var _schoolGroupId = $(this).attr('data-id');
                $('#eye_' + _schoolGroupId + ' >i').addClass('fa-eye')
                $('#eye_' + _schoolGroupId + ' >i').removeClass('fa-eye-slash')
            }
        });
       
    });
});

//To resolve Ipad group menu issue, Deepak Singh on 11/Nov/2020
function changeGroupAction() {
    var winWidth = $(window).outerWidth();
    if (winWidth == 1024) {
        $(".dropleft.group-actions").removeClass("dropleft").addClass("dropright");
    }
}

$(window).resize(function () {
    var winWidth = $(window).outerWidth();
    if (winWidth == 1024) {
        changeGroupAction();
    }
});

