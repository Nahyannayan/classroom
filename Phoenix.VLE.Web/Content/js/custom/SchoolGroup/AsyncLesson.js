﻿var formElements = new FormElements();
var asyncLesson = (function ($, w) {
    var init = function () {
        if ($("#PageName") != undefined && $("#PageName").val() == 'AsyncLesson') {
            var tab = globalFunctions.getParameterValueFromQueryString('tb');
            tab = tab == '' ? "1" : tab;
            tabInit(tab);
        }
    },

        loadResourcesActivityContent = function (id) {
            id = id == '' || id == undefined ? $("#AsyncLessonId").val() : id;
            var groupId = globalFunctions.getParameterValueFromQueryString('gpid');
            $.ajax({
                url: "/SchoolStructure/SchoolGroups/GetResourcesActivitiesAsyncLesson?asyncLessonId=" + id + "&grpId=" + groupId,
                type: 'GET',
                success: function (data) {
                    $("#divAsyncResources [data-toggle=tooltip]").tooltip("dispose");
                    if (data.trim() !== '') {

                        $("#asyncResoucesActivities").html('');
                        $("#asyncResoucesActivities").html(data);

                        $("#asyncResoucesActivities").sortable({
                            update: function (event, ui) {
                                updateStepsResourcesActivity();
                            }
                        }).disableSelection();

                        InitSvg();
                    }
                    else {
                        $("#asyncResoucesActivities").html('');
                        $("#asyncResoucesActivities").html(`<span class="no-data-found text-bold d-block mx-auto my-auto">
                                            there is no data to display</span>`);
                    }

                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        loadReviewAndLauncContent = function (id) {
            id = id == '' || id == undefined ? $("#AsyncLessonId").val() : id;
            var groupId = globalFunctions.getParameterValueFromQueryString('gpid');
            $.ajax({
                url: "/SchoolStructure/SchoolGroups/GetAsyncLessonDetail?id=" + id + "&grpId=" + groupId,
                type: 'GET',
                success: function (data) {
                    //debugger;
                    if (data.trim() !== '') {

                        $("#reviewContent").html('');
                        $("#reviewContent").html(data);
                        initCarouselElement();
                        InitSvg();
                        mCustomScrollBar();
                    }
                    else {
                        $("#reviewContent").html('');
                        $("#reviewContent").html(`<span class="no-data-found text-bold d-block mx-auto my-auto">
                                            there is no data to display</span>`);
                    }

                    var previewOnly = globalFunctions.getParameterValueFromQueryString("prv");
                    if (previewOnly != undefined && previewOnly == 'true') {
                        $("#btnSaveNLaunch,#btnEditDescription,#tabMenu,.card-actions,#btnBackStep2").addClass("disabled");

                    }
                    else {
                        $("#btnSaveNLaunch,#btnEditDescription,#tabMenu,.card-actions,#btnBackStep2").removeClass("disabled");
                    }

                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        deleteResourceActivityData = function (source, id) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        url: '/SchoolStructure/SchoolGroups/DeleteAsyncResourcesActivity?id=' + id,
                        type: 'GET',
                        success: function (data) {
                            //debugger;
                            if (data.Success === true) {
                                globalFunctions.showMessage(data.NotificationType, data.Message);
                                loadResourcesActivityContent();
                            }
                            else {
                                globalFunctions.onFailure();
                            }
                        },
                        error: function (data, xhr, status) {
                            //debugger;
                            globalFunctions.onFailure();
                        }

                    });
                });
            }
        },
        updateResourcesActivityStatus = function (id, status, type) {

            $.ajax({
                url: "/SchoolStructure/SchoolGroups/UpdateStatusAsyncResourcesActivity?id=" + id + "&status=" + status + "&type=" + type,
                type: 'GET',
                success: function (data) {
                    //debugger;
                    if (data !== '') {
                        var activeTab = $("#tabMenu li.active a").attr("href");
                        if (activeTab == "#step-2") {
                            loadResourcesActivityContent();
                        }
                        else {
                            loadReviewAndLauncContent();
                        }
                    }

                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        updateStepsResourcesActivity = function () {
            var idsInOrder = $("#asyncResoucesActivities").sortable("toArray");
            idsInOrder = idsInOrder.filter(function (number, index) {
                return globalFunctions.isValueValid(number);
            });

            $.ajax({
                url: "/SchoolStructure/SchoolGroups/UpdateStepOrderAsyncResourcesActivity?ids=" + JSON.stringify(idsInOrder),
                type: 'GET',
                success: function (data) {
                    //debugger;
                    if (data !== '') {
                        globalFunctions.showMessage('success', translatedResources.stepUpdateMsg);
                    }
                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        loadGroupQuizPopup = function (source, id, groupId, moduleId, folderId) {
            var title = translatedResources.addgroupquiz;
            var grId = groupId; //$("#SectionId").val();
            var courseId = globalFunctions.getParameterValueFromQueryString('cid');
            globalFunctions.loadPopup(source, '/Files/Files/InitAddEditGroupQuiz?id=' + id + '&grId=' + grId + '&pfId=' + folderId + '&moduleId=' + moduleId + '&cid=' + courseId, title, 'file-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $("#frmAddGroupQuiz").attr("data-ajax-success", "asyncLesson.onGroupQuizSuccess");
                $(document).off('click', '#submitGroupQuiz').on('click', '#submitGroupQuiz', function (e) {
                    //debugger;
                    var validationScoreDate = true;
                    if ($("#IsSetTime").prop("checked")) {
                        if ($('#StartDate').val().trim() != '') {
                            if ($('#StartTime').val() === null || $('#StartTime').val().trim() === '') {
                                $(".validateStartTime").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
                                formValidate = false;
                            } else {
                                if (!validationQuizTime) {
                                    globalFunctions.showWarningMessage(translatedResources.quizTimeValidation);
                                }
                            }
                        }
                    }
                    if ($("#IsHideScore").is(":checked") === true) {
                        if ($('#ShowScoreDate').val() === '') {
                            globalFunctions.showWarningMessage(translatedResources.scoreDateValidation);
                            validationScoreDate = false;
                        }
                    }
                    if (formValidate && validateQuiz && validationScoreDate) {
                        $('form#frmAddGroupQuiz').submit();
                    }
                    //e.stopPropagation();
                });

            });
            globalFunctions.enableCascadedDropdownList("CourseIds", "QuizId", '/Files/Files/GetQuizByCourse');
            $('.selectpicker').selectpicker('refresh');
        },
        onGroupQuizSuccess = function (data) {
            //debugger;
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                asyncLesson.loadResourcesActivityContent();
            }
        },
        saveStudentResourceComment = function () {
            var resourceId = $(".resourceCarousel .active .item").data("id");
            var comment = $("#txtComment").val();
            var commentId = $("#asyncLessonCommentId").val();
            commentId = commentId == '' ? '0' : commentId;
            if (comment.trim() == '') {
                globalFunctions.showWarningMessage(translatedResources.doubtValidationMsg);
                return;
            }
            else if (resourceId == undefined || resourceId == 0) {
                globalFunctions.showWarningMessage(translatedResources.noResourceValidationMsg);
                return;
            }
            else {
                if (validateBannedWord(comment)) {
                    globalFunctions.showWarningMessage(comment + ' ' + translatedResources.validateBannedWordMessage);
                    return;
                }
            }
            var token = $('input[name=__RequestVerificationToken]').val();
            var formData = new FormData();
            formData.append("__RequestVerificationToken", token);
            formData.append("AsyncLessonCommentId", commentId);
            formData.append("ResourceActiivtyId", resourceId);
            formData.append("Comment", comment.trim());

            $.ajax({
                url: '/SchoolStructure/SchoolGroups/SaveCommentData',
                type: 'POST',
                //headers: headers,
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {

                    if (data.Success == true) {
                        $("#txtComment").val('');
                        $("#asyncLessonCommentId").val('');
                        loadStudentComment();
                        if (data.Identifier == "0") {
                            globalFunctions.showMessage(data.NotificationType, translatedResources.addSuccessMsg);
                        }
                        else {
                            globalFunctions.showMessage(data.NotificationType, data.Message);
                        }
                    }
                    else if (data.notificationObj.Success == false) {
                        globalFunctions.onFailure();
                    }
                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                },
            });
        },
        loadStudentComment = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Name", "sTitle": translatedResources.title, "sWidth": "20%", "sClass": "wordbreak" },
                { "mData": "Comment", "sTitle": translatedResources.comment, "sWidth": "35%", "sClass": "wordbreak" },
                { "mData": "Reply", "sTitle": translatedResources.reply, "sWidth": "35%", "sClass": "wordbreak" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "10%" }
            );
            var asyncLessonId = $("#AsyncLessonId").val();
            initGrid('tbl-Comment', _columnData, '/SchoolStructure/SchoolGroups/LoadStudentComment?asLsId=' + asyncLessonId);
            $(".dataTables_filter").addClass('d-none');
        },
        editDoubtPopup = function (source, id, resourceId) {
            var asyncLessonId = $("#AsyncLessonId").val();
            $.ajax({
                url: "/SchoolStructure/SchoolGroups/EditDoubt?id=" + id + "&rsId=" + resourceId + "&asLsId=" + asyncLessonId,
                type: 'GET',
                success: function (data) {
                    //debugger;
                    if (data !== '') {
                        $("#txtComment").val(data.data);
                        $("label[for='txtComment']").addClass("active");
                        $(".resourceCarousel .item").removeClass('active');
                        $('.resourceCarousel .item[data-id = ' + resourceId + ']').addClass('active');
                        $("#asyncLessonCommentId").val(id);
                    }
                    else {
                        $("#txtComment").val('');
                    }

                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        deleteDoubtData = function (source, id, resourceId) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        url: '/SchoolStructure/SchoolGroups/DeleteDoubt/?id=' + id,
                        type: 'GET',
                        success: function (data) {
                            //debugger;
                            if (data.Success === true) {

                                globalFunctions.showMessage(data.NotificationType, translatedResources.deletedSuccess);
                                loadStudentComment();
                            }
                            else {
                                globalFunctions.onFailure();
                            }
                        },
                        error: function (data, xhr, status) {
                            //debugger;
                            globalFunctions.onFailure();
                        }

                    });
                });
            }
        },
        initGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                processing: true
                //columnDefs: [{
                //    'targets': 6,
                //    'orderable': false
                //}]
            };
            grid.init(settings);
        },
        loadCommentListByStudent = function (source,id) {
            var studId = id == undefined ? $(source).data("id") : id;
            var lessonId = $("#AsyncLessonId").val();
            $("#StudentList").val(studId);
            $('.selectpicker').selectpicker('refresh');

            $.ajax({
                url: "/SchoolStructure/SchoolGroups/GetStudentDoubtList?studentId=" + studId + "&asLsId=" + lessonId,
                type: 'GET',
                success: function (data) {
                    //debugger;
                    if (data.trim() !== '') {

                        $("#doubtList").html('');
                        $("#doubtList").html(data);

                        if (source != undefined) {
                            $('#studentDoubt').one('shown.bs.modal', function () {
                                source.trigger('modalLoaded');
                                
                                //make input labels active if there is initial value available

                            });
                            $('#studentDoubt').one('hidden.bs.modal', function () {
                                source.trigger('modalClosed');
                            });

                            $('#studentDoubt').modal("show");
                        }
                    }
                   
                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        saveTeacherReplyComment = function (source) {
            var resourceId = $(source).data("rs-id");
            var parentCommentId = $(source).data("id");;
            var comment = $("#txt_" + parentCommentId).val();
           
            if (comment.trim() == '') {
                globalFunctions.showWarningMessage(translatedResources.replyValidationMsg);
                return;
            }
            else if (resourceId == undefined || resourceId == 0) {
                globalFunctions.showWarningMessage(translatedResources.noResourceValidationMsg);
                return;
            }
            else {
                if (validateBannedWord(comment)) {
                    globalFunctions.showWarningMessage(comment + ' ' + translatedResources.validateBannedWordMessage);
                    return;
                }
            }
            var token = $('input[name=__RequestVerificationToken]').val();
            var formData = new FormData();
            formData.append("__RequestVerificationToken", token);
            formData.append("ParentCommentId", parentCommentId);
            formData.append("ResourceActiivtyId", resourceId);
            formData.append("Comment", comment.trim());
            formData.append("IsResolved", "true");

            $.ajax({
                url: '/SchoolStructure/SchoolGroups/SaveCommentData',
                type: 'POST',
                //headers: headers,
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {

                    if (data.Success == true) {
                       
                        loadCommentListByStudent(undefined, $("#StudentList").val());
                        if (data.Identifier == "0") {
                            globalFunctions.showMessage(data.NotificationType, translatedResources.addSuccessMsg);
                        }
                        else {
                            globalFunctions.showMessage(data.NotificationType, data.Message);
                        }
                    }
                    else if (data.notificationObj.Success == false) {
                        globalFunctions.onFailure();
                    }
                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                },
            });
        };
    return {
        init: init,
        loadResourcesActivityContent: loadResourcesActivityContent,
        deleteResourceActivityData: deleteResourceActivityData,
        updateResourcesActivityStatus: updateResourcesActivityStatus,
        updateStepsResourcesActivity: updateStepsResourcesActivity,
        loadGroupQuizPopup: loadGroupQuizPopup,
        onGroupQuizSuccess: onGroupQuizSuccess,
        loadReviewAndLauncContent: loadReviewAndLauncContent,
        saveStudentResourceComment: saveStudentResourceComment,
        loadStudentComment: loadStudentComment,
        editDoubtPopup: editDoubtPopup,
        deleteDoubtData: deleteDoubtData,
        loadCommentListByStudent: loadCommentListByStudent,
        saveTeacherReplyComment: saveTeacherReplyComment
    };
})(jQuery, window);

$(function () {
 

    $("#UnitId").on("change", function () {
        var selectVal = $("#UnitId").val();
        if (selectVal != '' && selectVal != '0') {
            $("#dateSection").removeClass('d-none');
            $.ajax({
                url: '/SchoolStructure/SchoolGroups/GetTopicDates/?topicId=' + selectVal,
                type: 'GET',
                success: function (data) {
                    //console.log(data);
                    if (data !== '') {
                        $("#StartDate").val(data.st);
                        $("#EndDate").val(data.en);
                        $("label[for='StartDate']").addClass("active");
                        $("label[for='EndDate']").addClass("active");

                        $('.start-date-picker').data("DateTimePicker").maxDate(data.en);

                        var $select = $('<select/>', {
                            'class': "selectpicker",
                            'data-live-Search': "true",
                            'id': "PlanSchemeId",
                            'name': "PlanSchemeId"
                        });
                        $select.append('<option value="0">' + translatedResources.selectedText + '</option>');
                        for (var idx in data.lp) {
                            $select.append('<option value=' + data.lp[idx].Value + '>' + data.lp[idx].Text + '</option>');
                        }
                        $("#lessonPlanDiv").html('');
                        $select.appendTo('#lessonPlanDiv').selectpicker('refresh');
                    }

                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        }
        else {
            $("#dateSection").addClass('d-none');
            $("#StartDate").val('');
            $("#EndDate").val('');
        }
        //$(this).selectpicker('refresh');
    });

    $('.async-lesson-builder a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

        var href = $(e.target).attr('href');
        var $curr = $(".process-model  a[href='" + href + "']").parent();
        $('.process-model li').removeClass();
        $curr.addClass("active");
        $curr.prevAll().addClass("visited");

        if (href == '#step-2') {
            asyncLesson.loadResourcesActivityContent();
        }
        else if (href == '#step-1') {
            tabInit("1");
        }
        else {
            asyncLesson.loadReviewAndLauncContent();
        }

        //var tabs_offset = $(this).offset();
        scrollTo(0, 0);
        
    });

    $("#btnBackStep1, #btnBackStep2").on("click", function (e) {
        var href = $(this).data('step');
        $("#tabMenu li a[href='" + href + "']").trigger('click');
    });

    $("#btnSaveResourcesNActivity").on("click", function (e) {
        asyncLesson.updateStepsResourcesActivity();
        var href = $(this).data('step');
        $("#tabMenu li a[href='" + href + "']").trigger('click');
    });

    $("#AddAssignment").on('click', function (e) {
        var id = globalFunctions.getParameterValueFromQueryString('id');
        id = id == '' ? $("#AsyncLessonId").val() : id;
        var groupId = globalFunctions.getParameterValueFromQueryString('gpid');
        var courseId = globalFunctions.getParameterValueFromQueryString('cid');
        location.href = '/Assignments/Assignment/AddEditAssignment?id=&courseId=' + courseId + '&selectedGroupId=' + groupId + "&asylsId=" + id;
    });

    $("#btnAttachQuiz").on("click", function (e) {
        var asyncLessonId = $("#AsyncLessonId").val();
        var asyncModuleId = $("#AsyncModuleId").val();
        var folderId = $("#FolderId").val();
        asyncLesson.loadGroupQuizPopup($(this), 0, asyncLessonId, asyncModuleId, folderId);
    });

    $('body').on("click", "#btnEditDescription", function (e) {
        $("#tabMenu li a[href='#step-1']").trigger('click');
    });

    $('body').on('click', '#btnStartGroupQuiz', function () {
       // debugger;

        clearInterval(refreshIntervalId);
        $("#StartQuizId").val($(this).data("id"));
        $("#StartQuizresourceid").val($(this).data("quizresourceid"));
        $("#StartStudid").val($(this).data("studid"));
        $("#selectedQuizName").html($(this).data("quizname"));
        $("#selectedQuizTitle").html($(this).data("starttime"));
        $("#QuizTimeInSec").val($(this).data("timeinsec"));
        $("#selectedQuizStartDate").html($(this).data("startdate"));
        $("#selectedQuizStartTime").html($(this).data("starttimebyteacher"));
        $("#TeacherGivenTime").val($(this).data("teachergiventime"));
        $("#IsSetStartDateByTeacher").val($(this).data("issetstartdate"));

        if ($(this).data("issetstartdate") == 'False') {
            $('.SetTeacherTime').hide();
            $('.selectedQuizFooter').show();
        }
        else {
            //debugger;
            $('.SetTeacherTime').show()
            if ($(this).data("startstartedtimer") == 'False') {
                $('.selectedQuizFooter').hide();
                var self = this;
                refreshIntervalId = setInterval(function () {
                    //debugger;
                    var d2 = new Date();
                    var d1 = new Date($(self).data("teachergiventime"));
                    var diffInSec = parseInt($(self).data("timeinsec")) - parseInt(((d2 - d1) / 1000).toString());
                    if (d2 >= d1) {
                        //$('.selectedQuizFooter').show();
                        if (diffInSec > 0) {
                            $('.selectedQuizFooter').show();
                        } else {
                            $('.selectedQuizFooter').hide();
                        }
                    }
                    else {
                        $('.selectedQuizFooter').hide();
                    }
                    
                }, 200);
                

            } else {
                //debugger;
                //$('.selectedQuizFooter').show();
                var self = this;
                refreshIntervalId = setInterval(function () {
                    //debugger;
                    var d2 = new Date();
                    var d1 = new Date($(self).data("teachergiventime"));
                    var diffInSec = parseInt($(self).data("timeinsec")) - parseInt(((d2 - d1) / 1000).toString());
                    if (d2 >= d1) {
                        //$('.selectedQuizFooter').show();
                        if (diffInSec > 0) {
                            $('.selectedQuizFooter').show();
                        } else {
                            $('.selectedQuizFooter').hide();
                        }
                    }
                    else {
                        $('.selectedQuizFooter').hide();
                    }
                    
                }, 200);
                
            }
        }
        $('#startQuiz').modal({ show: true });
        //groupQuiz.logQuizTime($(this), $(this).data("id"), $(this).data("quizresourceid"), 'Q');
    });

    $('body').on('click', '#btnSubmitWithouTimeQuiz', function () {
        //debugger;
        var id = $(this).data("id");
        var resourceId = $(this).data("quizresourceid");
        var studentId = $(this).data("studid");
        location.href = '/Files/Files/Quiz/?id=' + id + '&resourceId=' + resourceId + '&studentId=' + studentId;
    });

    $('body').on('click', '.cloud-file-view', function () {
        var fileDownloader = FileDownloader.getInstance();
        fileDownloader.getCloudFilesList($(this));
    });

    $("#fileUploadModal").on('hide.bs.modal', function () {
        $('#PostedDocuments').fileinput('clear');
    });

    $("body").on("click", ".ViewDoubt", function (e) {
        asyncLesson.loadCommentListByStudent($(this));
    });

    $("body").on("change", "#StudentList", function (e) {
        asyncLesson.loadCommentListByStudent($(this), $("#StudentList").val());
    });

    $("body").on("click", ".send-btn", function (e) {
        asyncLesson.saveTeacherReplyComment($(this));
    });

    //After event bind to document init: Deepak Singh 31 July 2020
    asyncLesson.init();

    //$("[data-toggle='tooltip']").on("click", function () {
    //    $("[data-toggle='tooltip']").not(this).tooltip("hide")
    //});
});

function mCustomScrollBar() {
    //$.fn.mCustomScrollbar("destroy");
    var $scrollBarDiv = $("#lessonDetailDiv");
    var divHeight = $scrollBarDiv.height();
    if (divHeight > 200) {
        $scrollBarDiv.removeClass('p-3').addClass('pt-3 pb-3 pl-3 pr-0');
        $scrollBarDiv.mCustomScrollbar({
            setHeight: "200px",
            autoExpandScrollbar: true,
            scrollbarPosition: "inside",
            autoHideScrollbar: true
        });
    }
    else {
        $scrollBarDiv.closest(".row").removeClass('mb-3').addClass('mb-5');
    }
}

function tabInit(tabNo)
{
    if (tabNo == "1") {
       //nothing initialize
        $("#tabMenu li a[href='#step-1']").trigger('click');
    }
    else if (tabNo == "2") {
        $("#tabMenu li a[href='#step-2']").trigger('click');
    }
    else {
        $("#tabMenu li a[href='#step-3']").trigger('click');
    }

    CKEDITOR.replace('Description', {
        htmlencodeoutput: false,
        tabSpaces: 0,
        allowedContent: {
            $1: {
                elements: CKEDITOR.dtd,
                attributes: true,
                styles: true,
                classes: true
            }
        },
        disallowedContent: 'script; *[on*];',
        contentsLangDirection: $("html").attr("dir")
    });

    $(".selectpicker").selectpicker('refresh');
    $(".start-date-picker").datetimepicker({
        format: "DD/MMM/YYYY"
    });

    $(".end-date-picker").datetimepicker({
        format: "DD/MMM/YYYY", useCurrent: false
    });

    $(".start-date-picker").on("dp.change", function (e) {
        $('.end-date-picker').data("DateTimePicker").minDate(e.date);
    });

    $(".end-date-picker").on("dp.change", function (e) {
        $('.start-date-picker').data("DateTimePicker").maxDate(e.date);
    });
  
}

function initCarouselElement() {
    $('.resourceCarousel').owlCarousel({
        // Enable thumbnails
        items: 1,
        dots: false,
        nav: true,
        thumbs: true,
        thumbImage: false,
        thumbsPrerendered: true,
    });
    $(".resource-thumbs").mCustomScrollbar({
        setHeight: "425px",
        autoHideScrollbar: true,
        autoExpandScrollbar: true,
        scrollbarPosition: "outside"
    });
}

function InitPostedDocuments() {
    $("#PostedDocuments").fileinput({
        language: translatedResources.langCode,
        title: translatedResources.browsefile,
        theme: "fas",
        showRemove: false,
        showUpload: true,
        showCaption: false,
        showMultipleNames: false,
        minFileCount: 1,
        maxFileCount: 10,
        browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
        //removeClass: "btn btn-sm m-0 z-depth-0",
        uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
        cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
        overwriteInitial: false,
        maxFileSize: translatedResources.fileSizeAllow,
        uploadAsync: false,
        uploadUrl: "/SchoolStructure/SchoolGroups/SaveResources",
        uploadExtraData: function () {
            return {
                __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
                AsyncLessonId: $("#AsyncLessonId").val()
            };
        },
        fileActionSettings: {
            showZoom: false,
            showUpload: false,
            //indicatorNew: "",
            showDrag: false
        },
        allowedFileExtensions: JSON.parse(translatedResources.fileExtesion),
        initialPreviewAsData: false, // defaults markup
        initialPreviewConfig: [{
            frameAttr: {
                title: 'My Custom Title'
            }
        }],
        preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
        previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
        previewSettings: fileInputControlSettings.previewSettings,
        previewFileExtSettings: fileInputControlSettings.previewFileExtSettings,
        msgSizeTooLarge: 'Total File size (<b>{size}</b>) exceeds maximum allowed upload size of <b>{maxSize}</b>. Please retry your upload!'
    }).off('filebatchselected').on('filebatchselected', function (event, files) {
        var size = 0;
        $.each(files, function (ind, val) {
            //console.log($(this).attr('size'));
            size = size + $(this).attr('size');
        });

        var maxFileSize = $(this).data().fileinput.maxFileSize,
            msg = $(this).data().fileinput.msgSizeTooLarge,
            formatSize = (s) => {
                i = Math.floor(Math.log(s) / Math.log(1024));
                sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
                return out;
            };

        if ((size / 1024) > maxFileSize) {
            msg = msg.replace('{name}', 'all files');
            msg = msg.replace('{size}', formatSize(size));
            msg = msg.replace('{maxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
            //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
            if (files.length > 1) {
                $(".kv-fileinput-error.file-error-message").html('');
                $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                $(".close.kv-error-close").on('click', function (e) {
                    $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                    //$(".fileinput-upload").removeClass('d-none');
                });
            }
            else {
                $(".kv-fileinput-error.file-error-message").html('');
                $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                $(".close.kv-error-close").on('click', function (e) {
                    $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                    //$(".fileinput-upload").removeClass('d-none');
                });
            }

            $(".fileinput-upload").attr('disabled', true);
        }
    }).off('filebatchuploadcomplete').on('filebatchuploadcomplete', function (event, data, previewId, index) {
        //$('#PostedDocuments').fileinput('clear');
        $('#fileUploadModal').modal('hide');
        asyncLesson.loadResourcesActivityContent();
    });
}

$(document).on('click', '#btnAddResources', function (e) {
    InitPostedDocuments();
    $("#fileUploadModal").modal({ show: true });
});



$('body').on('click', 'li.back, #btnCancel', function (e) {
    var groupId = globalFunctions.getParameterValueFromQueryString("gpid");
    location.href = "/schoolstructure/schoolgroups/groupdetails?grpId=" + groupId + "&tb=2";
});

$('#btnSubmit, #btnSubmitDraft, #btnSaveNLaunch').off('click').on('click', function (e) {
    var asyncLessonid = $("#AsyncLessonId").val();
    var sectionId = $("#SectionId").val();
    var moduleId = $("#ModuleId").val();
    var folderId = $("#FolderId").val();
    var courseId = $("#CourseId").val();
    var isActive = $("#IsActive").val();
    var lessonTitle = $("#LessonTitle").val();
    var isAddMode = $("#IsAddMode").val();
    var description = CKEDITOR.instances.Description.getData();
    var students = $('#Students').val();
    var planSchemeId = $("#PlanSchemeId option:selected").val();
    var unitId = $("#UnitId option:selected").val();
    var startDate = $('#StartDate').val();
    var endDate = $('#EndDate').val();

    var currentSourceId = $(this).attr("id");
    var isPublish = currentSourceId == 'btnSaveNLaunch' ? true : $('#IsPublish').val();

    

    //var docFiles = $("#PostedDocuments").get(0).files;

    var token = $('input[name=__RequestVerificationToken]').val();
    //debugger;
    //console.log($(summary).html());
    //debugger;
    console.log(students);
    if (lessonTitle.trim() == '') {
        globalFunctions.showWarningMessage(translatedResources.titleMsg);
        return;
    }
    else if (!description.replace(/<[^>]*>/gi, '').length) {
        globalFunctions.showWarningMessage(translatedResources.descriptionMsg);
        return;
    }
    else if (students == '') {
        globalFunctions.showWarningMessage(translatedResources.studentSelectionMsg);
        return;
    }

    studentIds = '';
    $.each(students, function (key, val) {
        if (studentIds == '') {
            studentIds = val;
        }
        else {
            studentIds = studentIds + ',' + val;
        }
    });

    var formData = new FormData();
    formData.append("__RequestVerificationToken", token);
    formData.append("AsyncLessonId", asyncLessonid);
    formData.append("SectionId", sectionId);
    formData.append("ModuleId", moduleId);
    formData.append("FolderId", folderId);
    formData.append("CourseId", courseId);
    formData.append("IsAddMode", isAddMode);
    formData.append("IsActive", isActive);
    formData.append("IsPublish", isPublish);
    formData.append("LessonTitle", lessonTitle.trim());
    formData.append("Description", $("<div />").text(description).html());
    formData.append("Students", students);
    formData.append("PlanSchemeId", planSchemeId);
    formData.append("UnitId", unitId);
    formData.append("StartDate", startDate);
    formData.append("EndDate", endDate);

    //for (var i = 0; i < docFiles.length; i++) {
    //    formData.append("PostedDocuments", docFiles[i]);
    //}

    $.ajax({
        url: '/SchoolStructure/SchoolGroups/SaveAsyncLessonData',
        type: 'POST',
        //headers: headers,
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.notificationObj.Success == true) {
                //groupDetail.onAsyncLessonSuccess(data);
                globalFunctions.showSuccessMessage(translatedResources.addSuccessMsg);
                $("#AsyncLessonId").val(data.id);
                if (currentSourceId == 'btnSaveNLaunch') {
                    var groupId = globalFunctions.getParameterValueFromQueryString("gpid");
                    location.href = "/schoolstructure/schoolgroups/groupdetails?grpId=" + groupId + "&tb=2";
                }
                else {
                    $("#tabMenu li a[href='#step-2']").trigger('click');
                }
                
            }
            else if (data.notificationObj.Success == false) {
                globalFunctions.onFailure();
            }
        },
        error: function (data, xhr, status) {
            //debugger;
            globalFunctions.onFailure();
        }
    });
});

function InitSvg() {
    $('img.svg').each(function () {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
}

function validateBannedWord(txt) {
    var jsonString = JSON.stringify(_bannedWords);
    var bannedWord = globalFunctions.substringBannedWords(JSON.parse(jsonString), txt);

    return bannedWord.length > 0;
}