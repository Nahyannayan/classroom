﻿//var formElements = new FormElements();
var groupDetail = (function ($, w) {
    var baseUri = "/SchoolStructure/SchoolGroups/";
    var init = function () {
        if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
            var tab = globalFunctions.getParameterValueFromQueryString('tb');
            tab = tab == '' ? "2" : tab;
            loadTabContent(tab);
            getGroupSettingHtml();
        }
    },
        loadTopicPopup = function (source, mode, id, groupId, topicId) {
            var title = mode + ' ' + translatedResources.topictitle;
            globalFunctions.loadPopup(source, baseUri + '/InitAddEditTopicForm?id=' + id + '&groupId=' + groupId + '&topicId=' + topicId, title, 'folder-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $(".selectpicker").selectpicker({ noneSelectedText: translatedResources.pleaseSelect, refresh: "refresh" });
                $(".start-date-picker").datetimepicker({
                    format: "DD/MMM/YYYY"
                });

                $(".end-date-picker").datetimepicker({
                    format: "DD/MMM/YYYY", useCurrent: false
                });

                $(".start-date-picker").on("dp.change", function (e) {
                    $('.end-date-picker').data("DateTimePicker").minDate(e.date);
                });

                $(".end-date-picker").on("dp.change", function (e) {
                    $('.start-date-picker').data("DateTimePicker").maxDate(e.date);
                });
            });

        },
        addTopicPopup = function (source, groupId, topicId) {
            loadTopicPopup(source, translatedResources.add, "", groupId, topicId);

        },
        editTopicPopup = function (source, id, groupId, topicId) {
            loadTopicPopup(source, translatedResources.edit, id, groupId, topicId);
        },
        deleteTopicData = function (source, id) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        url: baseUri + 'DeleteTopic/?topicId=' + id,
                        type: 'GET',
                        success: function (data) {
                            //debugger;
                            if (data.Success === true) {

                                globalFunctions.showMessage(data.NotificationType, data.Message);
                                $("#unitCard_" + id).remove();
                            }
                            else {
                                globalFunctions.onFailure();
                            }
                        },
                        error: function (data, xhr, status) {
                            //debugger;
                            globalFunctions.onFailure();
                        }

                    });
                });
            }
        },
        onTopicSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal("hide");
                loadGroupContent("2");
            }
        },
        loadNewGroupCreatePopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.addGroup;
            var groupId = id == undefined ? '0' : id;
            var url = '/SchoolStructure/SchoolGroups/AddEditNewGroupForm?id=' + groupId;
            globalFunctions.loadPopup(source, url, title, 'group-dailog modal-lg');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {

                $(".selectpicker").selectpicker('refresh');
                //InitPostedImage();

            });

        },
        loadEditGroupPopup = function (source, id) {
            var title = translatedResources.edit + ' ' + translatedResources.SchoolGroupName;
            globalFunctions.loadPopup(source, '/SchoolStructure/SchoolGroups/AddEditNewGroupForm?id=' + id, title, 'group-dailog modal-lg');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $(".selectpicker").selectpicker('refresh');
            });
        },
        loadGroupContactList = function () {
            var groupId = $("#SectionId").val();
            var url = "/SchoolStructure/SchoolGroups/GetGroupContactList?groupId=" + groupId;
            $.ajax({
                url: url,
                type: 'GET',
                success: function (data) {
                    //debugger;
                    if (data !== '') {
                        $("#ContactList").html('');
                        $("#ContactList").html(data);
                        //mCustomScrollBar();
                        addScrollBarClass();

                    }
                    else {
                        $("#ContactList").html(`<span class="no-data-found text-bold d-block mx-auto my-auto">
                                            there is no data to display</span>`);
                    }

                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        loadGroupContent = function (tab) {
            var groupId = $("#SectionId").val();
            var url = "/SchoolInfo/Blog/GetWallListByGroupId?groupId=" + groupId;
            if (tab == '2') {
                //Activities Tab
                url = "/SchoolStructure/SchoolGroups/GetActivitesNResources?groupId=" + groupId;
            }
            else if (tab == '3') {
                //Member Tab
                url = "/SchoolStructure/SchoolGroups/GetMemberList?groupId=" + groupId;
            } else if (tab == '4') {
                //Gradebook tab
                url = '/Gradebook/Gradebook/TeacherGradebook?schoolGroupId=' + groupId;
            }
            else if (tab == '5') {
                url = "/SchoolStructure/SchoolGroups/GetGroupRecordedSessions?groupId=" + groupId;
            }
            else {
                //Chatter tab
                // url = "/SchoolStructure/SchoolGroups/GetChatters?groupId=" + groupId;
                url = "/SchoolInfo/Blog/GetWallListForGroup?groupId=" + groupId;
            }


            $.ajax({
                url: url,
                type: 'GET',
                success: function (data) {
                    if (data !== '') {

                        $("#groupPageContent").html('');
                        $("#groupPageContent").html(data);
                        if (tab == "1") {
                            //var loader = $('.loader-wrapper').detach();
                            //if ($('.chat-list-card').length > 0) {
                            //    $.get('/SchoolInfo/Blog/RecentHistory?groupId=' + $('#SectionId').val(), function (data) {
                            //        $('.chat-list-card').html(data);
                            //        $('.chat-list-card').find(".chatListWrap").mCustomScrollbar({
                            //            setHeight: "250px",
                            //            autoExpandScrollbar: true,
                            //            scrollbarPosition: "outside",
                            //            autoHideScrollbar: true
                            //        });
                            //        $(".activity-list").mCustomScrollbar({
                            //            autoExpandScrollbar: true,
                            //            scrollbarPosition: "inside",
                            //            autoHideScrollbar: true
                            //        });
                            //    });
                            //}
                            //if ($('.group-list-card').length > 0) {
                            //    $.get('/SchoolInfo/Blog/MyGroup', function (data) {
                            //        $('.group-list-card').replaceWith(data);
                            //        $('.group-list-card').find(".groupListWrap").mCustomScrollbar({
                            //            setHeight: "250px",
                            //            autoExpandScrollbar: true,
                            //            scrollbarPosition: "outside",
                            //            autoHideScrollbar: true
                            //        });
                            //    });
                            //}
                            //loader.insertAfter($("#dvnotificationdrawer"));
                        }
                        // $("#NoFolderDiv").hide();
                        InitSvg();
                        checkForBespokeGroup();
                        addScrollBarClass();

                        if (tab == '2') {
                            sortAllGroupSection();
                        }
                    }
                    else {
                        $("#groupPageContent").html(`<li><span class="no-data-found text-bold d-block mx-auto my-auto">
                                            there is no data to display</span></li>`);
                    }

                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });


        },
        loadFolderDetail = function (folderId, groupCourseTopicId, moduleId) {
            if (folderId != 0) {
                var groupId = $("#SectionId").val();

                var url = "/SchoolStructure/SchoolGroups/GetFolderDetailByFolderId?folderId=" + folderId + "&groupId=" + groupId;
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (data) {
                        //debugger;
                        if (data.trim() !== '') {
                            $("#topic_" + groupCourseTopicId).html('');
                            $("#topic_" + groupCourseTopicId).html(data);
                            InitSvg();
                            //mCustomScrollBar();
                            addScrollBarClass();

                            //For sorting & filtering the content of section
                            sortNFilterUnitSection(groupCourseTopicId);
                        }
                        else {
                            $("#topic_" + groupCourseTopicId).html(`<li><span class="no-data-found text-bold d-block mx-auto my-auto"> there is no data to display</span> </li>`);
                        }

                        //Ajax to call navigation tab
                        $.ajax({
                            url: "/SchoolStructure/SchoolGroups/GetFolderById?folderId=" + folderId,
                            type: 'GET',
                            success: function (data) {

                                $("#topicTitle_" + groupCourseTopicId).html('');
                                $("#topicTitle_" + groupCourseTopicId).html(data);
                            },
                            error: function (data, xhr, status) {
                                //debugger;
                                globalFunctions.onFailure();
                            }

                        });

                    },
                    error: function (data, xhr, status) {
                        //debugger;
                        globalFunctions.onFailure();
                    }

                });
            }
            else {
                $("#topicTitle_" + groupCourseTopicId).html('');
                $("#topicTitle_" + groupCourseTopicId).html($("#topicTitle_" + groupCourseTopicId).attr("data-title"));
                loadTopicActivitiesList(moduleId, groupCourseTopicId);
            }

            $("#topicMenu_" + groupCourseTopicId + " a").attr("data-parentFd", folderId);
            $("#unitCard_" + groupCourseTopicId).attr("data-parentFd", folderId);
            $("#menuOption_" + groupCourseTopicId).removeClass("d-none");
        },
        loadTopicActivitiesList = function (moduleId, sectionId) {
            //GroupId of page
            var groupId = $("#SectionId").val();

            if (moduleId == undefined && sectionId == undefined) {
                sectionId = $("#CurrentSectionId").val();
                moduleId = $("#CurrentModuleId").val();
                var folderId = $("#CurrentFolderId").val();
                if (folderId != 0) {
                    loadFolderDetail(folderId, sectionId, moduleId);
                    return;
                }
            }

            $.ajax({
                url: "/SchoolStructure/SchoolGroups/GetTopicActivitiesList?moduleId=" + moduleId + "&sectionId=" + sectionId + "&groupId=" + groupId,
                type: 'GET',
                success: function (data) {
                    //debugger;
                    if (data.trim() !== '') {
                        $("#topic_" + sectionId).html('');
                        $("#topic_" + sectionId).html(data);
                        InitSvg();
                        //mCustomScrollBar();
                        addScrollBarClass();

                        //For sorting & filtering the content of section
                        sortNFilterUnitSection(sectionId);
                    }
                    else {
                        $("#topic_" + sectionId).html(`<li><span class="no-data-found text-bold d-block mx-auto my-auto">
                                            there is no data to display</span></li>`);
                    }

                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        sendEmailNotify = function (source, id, studentIds, notifyTo, sendToAll) {
            if (id == undefined) {
                id = $("#SectionId").val();
            }
            if (sendToAll == undefined || sendToAll == '') {
                sendToAll = false;
            }

            if ((studentIds == undefined || studentIds == '') && ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails")) {
                studentIds = '';
                //If select all checked, pass empty value '' to send email to all student's: Deepak Singh 13/Oct/2020
                var selectAll = $("#selectAllAssigned").is(":checked");

                var $selectedMembers = $("#assignedMembers input[type='checkbox']:checked");
                if ($selectedMembers != null && $selectedMembers.length > 0) {

                    if (!selectAll) { //Added on 13/Oct/2020
                        $selectedMembers.each(function (i, elem) {
                            if (studentIds == '') {
                                studentIds = elem.value;
                            }
                            else {
                                studentIds = studentIds + ',' + elem.value;
                            }
                        });
                    }
                } else {
                    globalFunctions.showWarningMessage(translatedResources.MemberSelectMessage);
                    return;
                }
            }

            globalFunctions.loadPopup(source, '/SchoolStructure/SchoolGroups/LoadGroupMessageForm?groupdId=' + id + '&studentIds=' + studentIds + '&notifyTo=' + notifyTo + '&sendToAll=' + sendToAll, translatedResources.SendMessage, 'group-dailog modal-lg');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $(".selectpicker").selectpicker({ noneSelectedText: translatedResources.pleaseSelect, refresh: "refresh" });

                //To resolve link popup not working issue, By Deepak Singh on 26/Oct/2020
                $(document)
                    .off('focusin.modal')
                    .on('focusin.modal', function (e) {
                        // Add this line
                        if (e.target.className && e.target.className.indexOf('cke_') == 0) return;

                        // Original
                        if (that.$element[0] !== e.target && !that.$element.has(e.target).length) {
                            that.$element.focus()
                        }
                    });
            });
        },
        loadAddMemberPopup = function (source, memberTypeId) {
            var title = translatedResources.add + ' ' + (memberTypeId == 1 ? translatedResources.addStudent : translatedResources.addTeacher);
            var groupId = $("#SectionId").val();
            var autoSyncGroupMember = $("#AutoSyncGroupMember").val();
            var modalCss = memberTypeId == 1 ? 'modal-lg' : 'modal-md';

            globalFunctions.loadPopup(source, '/SchoolStructure/SchoolGroups/AddMemberInGroup?memberTypeId=' + memberTypeId + '&groupId=' + groupId + "&autoSyncMember=" + autoSyncGroupMember, title, 'folders-dailog ' + modalCss);
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $(".selectpicker").selectpicker({ noneSelectedText: translatedResources.pleaseSelect, refresh: "refresh" });
                if (autoSyncGroupMember == 'True' && memberTypeId == 1) {

                    $("#btnContribute").addClass('d-none');
                    $("#btnManager").addClass('d-none');
                }
                else {
                    $("#btnContribute").removeClass('d-none');
                    $("#btnManager").removeClass('d-none');
                }
            });
        },
        onGroupNameUpdate = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                //window.location.reload();
                $("#groupNameSpan").html('');
                $("#groupNameDiv").html('');

                $("#groupNameSpan").html(data.HeaderText);
                $("#groupNameDiv").html(data.HeaderText);
            }
        },
        onGroupDetailSaveSuccess = function (data) {
            console.log(data)
            globalFunctions.showMessage(data.operationDetails.NotificationType, data.operationDetails.Message);
            if (data.operationDetails.Success) {
                $("#myModal").modal('hide');
                var tb = $(".toggable-group-tabs .active:visible").data("tab");
                tb = tb == undefined || tb == '' || (data.isAddMode != undefined && data.isAddMode) ? '3' : tb;
                window.location = '/schoolstructure/schoolgroups/GroupDetails?grpId=' + data.operationDetails.Identifier + '&tb=' + tb;
            }
        },
        getGroupSettingHtml = function () {
            var groupId = $("#SectionId").val();
            $.ajax({
                url: "/SchoolStructure/SchoolGroups/GetGroupSettingHtml?groupId=" + groupId,
                type: 'GET',
                success: function (data) {
                    //debugger;
                    if (data !== '') {

                        $("#breadcrumbActions").html('');
                        $("#breadcrumbActions").html(data);
                        $(".selectpicker").selectpicker('refresh');
                        $("#breadcrumbActions").removeClass("d-none");
                        checkForBespokeGroup();
                    }

                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        onSendMessageSave = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
            }
        },
        loadAsyncLessonPopup = function (source, mode, id, sectionId, moduleId, folderId) {
            //var title = mode + ' ' + translatedResources.asyncLessonTitle;
            var courseId = $("#CourseId").val();
            courseId = courseId == "" ? 0 : courseId;
            var groupId = $("#SectionIdEncrypt").val();
            if (mode == 'Edit') {
                location.href = '/SchoolStructure/SchoolGroups/AddEditAsyncLessonForm?id=' + id + '&cid=' + courseId + "&gpid=" + groupId;
            }
            else {
                location.href = '/SchoolStructure/SchoolGroups/AddEditAsyncLessonForm?id=' + id + '&sid=' + sectionId + '&mdid=' + moduleId + '&fdid=' + folderId + '&cid=' + courseId + "&gpid=" + groupId;
            }

        },
        loadAddAsyncLessonPopup = function (source, sectionId, moduleId, folderId) {
            loadAsyncLessonPopup(source, translatedResources.add, "", sectionId, moduleId, folderId);
        },
        loadEditAsyncLessonPopup = function (source, id, sectionId, moduleId, folderId) {
            loadAsyncLessonPopup(source, translatedResources.edit, id, sectionId, moduleId, folderId);
        },
        deleteAsyncLesson = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        url: baseUri + 'DeleteAsyncLessonId/?asyncLessonId=' + id,
                        type: 'GET',
                        success: function (data) {
                            //debugger;
                            if (data.Success === true) {

                                globalFunctions.showMessage(data.NotificationType, translatedResources.deletedSuccess);
                                loadTopicActivitiesList();
                            }
                            else {
                                globalFunctions.onFailure();
                            }
                        },
                        error: function (data, xhr, status) {
                            //debugger;
                            globalFunctions.onFailure();
                        }

                    });
                });
            }
        },
        loadAsyncContentDetail = function (id, isStudent) {
            var groupId = $("#SectionIdEncrypt").val();
            var courseId = $("#CourseId").val();
            if (isStudent == 'false') {
                location.href = '/SchoolStructure/SchoolGroups/TeacherLessonDetailView?id=' + id + "&gpid=" + groupId;
            }
            else {
                location.href = '/SchoolStructure/SchoolGroups/ViewAsyncLessonDetail?id=' + id + "&gpid=" + groupId;
            }
        },
        onAsyncLessonSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                loadTopicActivitiesList();

            }
        },
        onOnlineMeetingSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            $("#btnSaveZoomMeeting").removeAttr("disabled");
            if (data.Success) {
                $("#myModal").modal("hide");
                debugger;
                GetWeeklyEvents();

            }
        },
        showClipboardDataForGroup = function (source) {
            //source.preventDefault();
            var title = translatedResources.clipboard;
            globalFunctions.loadPopup(source, '/Files/Files/GetClipboard', title, 'modal-lg p-2');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $('.deleteFile').attr('onclick', 'groupDetail.addFromClipboardForGroup($(this))');
            });
        },
        addFromClipboardForGroup = function (source) {
            //source.preventDefault();
            var id = $(source).data('id');
            if (globalFunctions.isValueValid(id)) {

                var moduleId = $("#CurrentModuleId").val();
                var parentFolderId = $("#CurrentFolderId").val();
                var sectionId = $("#CurrentSectionId").val();
                //console.log(moduleId + '_' + parentFolderId + '_' + sectionId);
                $.ajax({
                    url: '/Files/Files/AddFromClipboard',
                    type: 'POST',
                    data: { 'id': id, 'moduleId': moduleId, 'parentFolderId': parentFolderId, 'sectionId': sectionId },
                    async: true,
                    success: function (data) {
                        //debugger;
                        if (data.Success === true) {
                            //$("#btnClipboard").show();
                            $(source).closest(".clipboard-item").remove();
                            if ($(".clipboard-list .clipboard-item").length <= 0)
                                $("#myModal").modal("hide");
                            //$("#myModal").modal('hide');
                            globalFunctions.showSuccessMessage(translatedResources.FileUploaded);
                            groupDetail.loadTopicActivitiesList();
                        }
                        else {
                            globalFunctions.showWarningMessage(data.Message);
                        }

                    },
                    error: function (data, xhr, status) {
                        //debugger;
                        globalFunctions.onFailure();
                    }

                });
            }
        },
        sortUnits = function (source) {
            var title = translatedResources.OrganizeUnit;
            var groupId = $("#SectionId").val();
            globalFunctions.loadPopup(source, baseUri + '/GetGroupCourseTopicsByGroupId?groupId=' + groupId, title, 'folder-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $("#groupCourseList").sortable({
                    axis: 'y'
                }).disableSelection();
            });
        },
        updateUnitsSortOrder = function () {
            var idsInOrder = $("#groupCourseList").sortable("toArray");
            idsInOrder = idsInOrder.filter(function (number, index) {
                return globalFunctions.isValueValid(number);
            });

            if (idsInOrder.length > 0) {
                $.ajax({
                    url: "/SchoolStructure/SchoolGroups/UpdateUnitsStepOrder?ids=" + JSON.stringify(idsInOrder),
                    type: 'GET',
                    success: function (data) {
                        //debugger;
                        if (data) {
                            globalFunctions.showMessage('success', translatedResources.UnitOrderMsg);
                            $("#myModal").modal("hide");
                            loadTabContent('2');
                        }
                    },
                    error: function (data, xhr, status) {
                        //debugger;
                        globalFunctions.onFailure();
                    }

                });
            }
            else {
                $("#myModal").modal("hide");
            }
        },
        addGroupPrimaryTeacher = function (groupId, teacherId) {

            var formData = new FormData();
            formData.append("GroupId", groupId);
            formData.append("TeacherId", teacherId);

            $.ajax({
                type: 'POST',
                data: formData,
                url: '/SchoolStructure/SchoolGroups/AddGroupPrimaryTeacher',
                contentType: false,
                processData: false,
                success: function (result) {
                    globalFunctions.showSuccessMessage(translatedResources.showSuccessMessage);
                    groupDetail.loadGroupContent("3");
                },
                error: function (data) { }
            });
        },
        removeGroupPrimaryTeacher = function (source, id) {

            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        url: baseUri + 'RemoveGroupPrimaryTeacher/?id=' + id,
                        type: 'GET',
                        success: function (data) {
                            //debugger;
                            if (data.Success === true) {

                                globalFunctions.showMessage(data.NotificationType, translatedResources.deletedSuccess);
                                groupDetail.loadGroupContent("3");
                            }
                            else {
                                globalFunctions.onFailure();
                            }
                        },
                        error: function (data, xhr, status) {
                            //debugger;
                            globalFunctions.onFailure();
                        }

                    });
                });
            }
        };

    return {
        init: init,
        addTopicPopup: addTopicPopup,
        editTopicPopup: editTopicPopup,
        deleteTopicData: deleteTopicData,
        onTopicSaveSuccess: onTopicSaveSuccess,
        loadGroupContent: loadGroupContent,
        loadFolderDetail: loadFolderDetail,
        loadTopicActivitiesList: loadTopicActivitiesList,
        sendEmailNotify: sendEmailNotify,
        loadEditGroupPopup: loadEditGroupPopup,
        onGroupNameUpdate: onGroupNameUpdate,
        loadAddMemberPopup: loadAddMemberPopup,
        loadGroupContactList: loadGroupContactList,
        loadNewGroupCreatePopup: loadNewGroupCreatePopup,
        onGroupDetailSaveSuccess: onGroupDetailSaveSuccess,
        getGroupSettingHtml: getGroupSettingHtml,
        onSendMessageSave: onSendMessageSave,
        loadAsyncLessonPopup: loadAsyncLessonPopup,
        loadAddAsyncLessonPopup: loadAddAsyncLessonPopup,
        loadEditAsyncLessonPopup: loadEditAsyncLessonPopup,
        deleteAsyncLesson: deleteAsyncLesson,
        loadAsyncContentDetail: loadAsyncContentDetail,
        onAsyncLessonSuccess: onAsyncLessonSuccess,
        onOnlineMeetingSaveSuccess: onOnlineMeetingSaveSuccess,
        showClipboardDataForGroup: showClipboardDataForGroup,
        addFromClipboardForGroup: addFromClipboardForGroup,
        sortUnits: sortUnits,
        updateUnitsSortOrder: updateUnitsSortOrder,
        addGroupPrimaryTeacher: addGroupPrimaryTeacher,
        removeGroupPrimaryTeacher: removeGroupPrimaryTeacher
    };
})(jQuery, window);

$(function () {
    groupDetail.init();
    $("#breadcrumbActions").addClass("d-none");

    $("#btnAddTopic").on("click", function () {
        var groupId = $("#SectionId").val();
        groupDetail.addTopicPopup($(this), groupId, '0');
    });



    $(".chat-list, .group-list").mCustomScrollbar({
        setHeight: "250px",
        autoExpandScrollbar: true,
        scrollbarPosition: "outside",
        autoHideScrollbar: true
    });

    $('.grpTab').click(function () {
        //.activities-tab
        //.chatter-tab
        //.group-members-tab
        //$(this).addClass('active');
        var tabNo = $(this).attr("data-tab");
        loadTabContent(tabNo);
    });


    $(document).on("click", "a.create-new", function () {
        //debugger;
        //console.log($(this));
        var type = $(this).attr("data-type");
        var sectionId = $(this).attr("data-sectionid");
        var moduleId = $(this).attr("data-moduleid");
        var parentFdId = $(this).attr("data-parentfd");
        var resourceTypeId = $(this).attr("data-resourcetypeid");

        $("#CurrentFolderId").val(parentFdId);
        $("#CurrentSectionId").val(sectionId);
        $("#CurrentModuleId").val(moduleId);
        $("#ResourceFileTypeId").val(resourceTypeId);

        var fileDownloader = FileDownloader.getInstance();
        var url = '';
        var courseId = $("#CourseId").val();
        ///check type and perform action
        switch (type) {
            case "editUnit":
                //debugger;
                var groupId = $("#SectionId").val();
                groupDetail.editTopicPopup($(this), sectionId, groupId, 0);
                //redirect(url);
                break;
            case "removeUnit":
                //debugger;
                groupDetail.deleteTopicData($(this), sectionId);
                //redirect(url);
                break;
            case "assignment":
                //debugger;
                // globalFunctions.showWarningMessage("Not yet implementted");
                if (courseId === undefined) {
                    url = "/Assignments/Assignment/AddEditAssignment?id=&isCloned=false&selectedGroupId=" + sectionId;
                }
                else if (moduleId == "11") {
                    var currentGroupId = $("#SectionIdEncrypt").val();
                    url = "/Assignments/Assignment/AddEditAssignment?id=&courseId=" + courseId + "&unitId=" + sectionId + "&isCloned=false&selectedGroupId=" + currentGroupId;
                }
                else {
                    url = "/Assignments/Assignment/AddEditAssignment?id=&courseId=" + courseId + "&unitId=&isCloned=false&selectedGroupId=" + sectionId;
                }
                //redirect(url);
                break;
            case "liveMeeting":
                //debugger;
                var meeting = groupMeetingFunction.getInstance();
                meeting.initZoomMeetingForm($(this), sectionId);
                break;
            case "folder":
                //debugger;
                folder.addfolderPopup($(this), moduleId, parentFdId, sectionId);
                break;
            case "file":
                //debugger;
                initFileUpload(sectionId, moduleId, parentFdId);
                break;
            case "form":
                //debugger;
                var module = $(this).attr("data-module");
                quiz.addQuizPopup($(this), sectionId, module, parentFdId, moduleId);
                break;
            case "quiz":
                //debugger;

                if (courseId === undefined) {
                    courseId = "";
                }
                groupQuiz.loadGroupQuizPopup($(this), 0, sectionId, moduleId, parentFdId, courseId);
                break;
            case "Url":
                //debugger;
                groupUrl.addGroupUrlPopup($(this), 0, sectionId, parentFdId, moduleId);
                break;
            case "addLesson":
                //debugger;
                groupDetail.loadAddAsyncLessonPopup($(this), sectionId, moduleId, parentFdId);
                //redirect(url);
                break;
            case "onedrive":
                //debugger;
                fileDownloader.getCloudFilesList($(this));
                break;
            case "googledrive":

                fileDownloader.getCloudFilesList($(this));
                break;
            case "clipboard":

                groupDetail.showClipboardDataForGroup($(this));
                break;
            case "sendEmail":
                groupDetail.sendEmailNotify($(this), sectionId, '', 'BOTH');
                break;
            default:
                break;
        }

        if (url != '') {
            //console.log(url);
            //return location.href = url;
            window.open(url);
        }
    });


    $('body').on('click', 'a.subItem', function () {
        var element = $(this).closest('.unit-Card-Group');
        var sectionId = $(element).attr("data-sectionId");
        var moduleId = $(element).attr("data-moduleId");
        var parentFdId = $(element).attr("data-parentFd");

        //console.log(element);

        $("#CurrentFolderId").val(parentFdId);
        $("#CurrentSectionId").val(sectionId);
        $("#CurrentModuleId").val(moduleId);
    });

    $('body').off('click', '#btnEditGroupForm').on('click', '#btnEditGroupForm', function () {
        debugger;
        var id = $(this).data("id");
        var secId = $("#CurrentSectionId").val() == "" ? 0 : $("#CurrentSectionId").val();
        var secType = 'Group';
        console.log(id);
        var folderId = $("#CurrentFolderId").val();
        var moduleId = $("#CurrentModuleId").val();
        quiz.editQuizPopup($(this), id, secId, secType, folderId, moduleId);
    });

    $('body').on('change', '#selectAllAssigned', function (e) {
        //debugger;
        var chk = $(this).is(':checked');
        $('input[type=checkbox]', "#assignedMembers").prop('checked', chk);
        //$('input[type=checkbox]', "#assignedMembers").each(function () {
        //    $(this).prop('checked', chk);
        //});

    });

    $('body').on('change', '#assignedMembers input[type=checkbox]', function () {

        var $inputs = $('#assignedMembers input[type=checkbox]');
        var allChecked = true;
        $inputs.each(function () {
            allChecked = allChecked && this.checked;
        });
        //this.checked = allChecked;
        $("#selectAllAssigned").prop('checked', allChecked);
    });

    $('body').on('change', '#selectAllAssignedTeacher', function (e) {
        //debugger;
        var chk = $(this).is(':checked');
        $('input[type=checkbox]', "#assignedTeacherMembers").prop('checked', chk);
        //$('input[type=checkbox]', "#assignedMembers").each(function () {
        //    $(this).prop('checked', chk);
        //});

    });

    $('body').on('change', '#assignedTeacherMembers input[type=checkbox]', function () {

        var $inputs = $('#assignedTeacherMembers input[type=checkbox]');
        var allChecked = true;
        $inputs.each(function () {
            allChecked = allChecked && this.checked;
        });
        //this.checked = allChecked;
        $("#selectAllAssignedTeacher").prop('checked', allChecked);
    });

    $('body').on('keyup', '#searchMember', function (e) {
        if (e.which === 13) {
            //console.log($('ul.searchList li').length);
            var searchText = $(this).val().toLowerCase();
            var isContent = false;
            if (searchText.trim().length == 0) {
                isContent == true;
            }
            $('ul.searchList li').each(function () {
                var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
                console.log(showCurrentLi);
                $(this).toggle(showCurrentLi);
                if (showCurrentLi) {
                    isContent = true;
                    $(this).addClass('d-flex');
                }
                else {
                    $(this).removeClass('d-flex');
                }
            });
            if (isContent == false) {
                $('ul.searchList').append(`<li id='noData' data-search-text=''><span class="no-data-found text-bold d-block mx-auto my-auto">
                                            there is no data to display</span ></li>`);
            }
            else {
                $('ul.searchList li[id="noData"]').remove();
            }
        }
    });

    $('body').on('keyup', '#searchResources', function (e) {
        if (e.which === 13) {
            //console.log($('ul.searchList li').length);
            var searchText = $(this).val().toLowerCase();
            var isContent = false;
            if (searchText.trim().length == 0) {
                isContent == true;
            }
            $('div.activities-list ul').each(function () {
                var id = $(this).attr("id");
                var unitId = id.split('_')[1];
                var typeText = $('#SortResourceTypeBy_' + unitId).find('a.active').data('type').toLowerCase();

                $('#topic_' + unitId + ' li').each(function () {
                    var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
                    var showCurrentLiFilter = typeText !== 'all' ? $(this).attr('data-sort-type').toLowerCase().indexOf(typeText) !== -1 : true;
                    //console.log(showCurrentLi);
                    $(this).toggle(showCurrentLi && showCurrentLiFilter);
                    if (showCurrentLi && showCurrentLiFilter) {
                        isContent = true;
                     }
                });

                if (isContent == false && $('#topic_' + unitId + ' li:visible').length == 0) {
                    $('#topic_' + unitId ).append(`<li id='noData' data-search-text='' data-sort-type='' data-sort-date=''><span class="no-data-found text-bold d-block mx-auto my-auto">there is no data to display</span ></li>`);
                }
                else {
                    $('#topic_' + unitId +' li[id = "noData"]').remove();
                }

            });

            
          
        }
    });

    $('body').on('keyup', '#searchContactList', function (e) {
        if (e.which === 13) {
            //console.log($('ul.searchList li').length);

            var searchText = $(this).val().toLowerCase();

            var isContent = false;
            if (searchText.trim().length == 0) {
                isContent == true;
            }

            $('ul#ContactList li').each(function () {
                var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
                //console.log(showCurrentLi);
                $(this).toggle(showCurrentLi);
                if (showCurrentLi) {
                    isContent = true;
                }
            });

            if (isContent == false) {
                $('ul#ContactList').append(`<li id='noData' data-search-text=''><span class="no-data-found text-bold d-block mx-auto my-auto">
                                            there is no data to display</span ></li>`);
            }
            else {
                $('ul#ContactList li[id="noData"]').remove();
            }



        }
    });

    $('body').on('change', '#selectAllContact', function (e) {
        //debugger;
        var chk = $(this).is(':checked');
        $('input[type=checkbox]', "#ContactList").prop('checked', chk);
    });
    $('body').on('change', '#ContactList input[type=checkbox]', function () {

        var $inputs = $('#ContactList input[type=checkbox]');
        var allChecked = true;
        $inputs.each(function () {
            allChecked = allChecked && this.checked;
        });
        //this.checked = allChecked;
        $("#selectAllContact").prop('checked', allChecked);
    });

    $('body').on('click', 'li.unitCard', function () {
        $('.unit-Card-Group').hide();
        var id = $(this).attr("data-id");
        $('#' + id).show();
    });

    $('#AllUnit').on('click', function () {
        $('.unit-Card-Group').show();
    });

    $('body').on('click', '#btnCreateGroup', function (e) {
        groupDetail.loadNewGroupCreatePopup($(this), translatedResources.add, '0');

    });
    $("body").on('click', '#btnArchive', function (e) {
        var groupId = $(this).attr('data-id');
        ArchiveThisGroup($(this), groupId);
    });

    $("#fileUploadModal").on('hide.bs.modal', function () {
        $('#postedFile').fileinput('clear');
    });

    $(document).on("keyup", "#search-sessions", function (e) {
        if (e.which == 13)
            initRecordedSessionPagination();
    });

    $(document).on('shown.bs.collapse', '.collapse', function (e) {
        addScrollBarClass();
    });

    $(document).on("click", "#btnSendMessageToAll", function (e) {
        groupDetail.sendEmailNotify($(this), 0, '', 'BOTH', true);
    });

    $(document).on("click", ".SortResourceType", function (e) {
        var parentDiv = $(this).parent("div");
        var currentUnitId = $(parentDiv).data("id");
        $(parentDiv).children('a').removeClass('active');
        $(this).addClass('active');
        if ($(this).data('type') === 'All') {
            $("#CurrentSortResourcType_" + currentUnitId).text($("#CurrentSortResourcType_" + currentUnitId).data('text'));
        }
        else {
            $("#CurrentSortResourcType_" + currentUnitId).text($(this).text());
        }

        var typeText = $(this).data('type').toLowerCase();
        filterResourceType(currentUnitId, typeText);
    });
    $(document).on("click", ".SortedBy", function (e) {
        var parentDiv = $(this).parent("div");
        var currentUnitId = $(parentDiv).data("id");
        $(parentDiv).children('a').removeClass('active');
        $(this).addClass('active');
        $("#CurrentSortedBy_" + currentUnitId).text($(this).text());

        var sortArg = $(this).data('type').toLowerCase() === 'name' ? 'data-search-text' : 'data-sort-date';
        var sordOrder = $(this).data('order').toLowerCase();
        var sortSelector = $('#topic_' + currentUnitId);
        var sortElem = 'li';

        sortElementBy(sortArg, sortSelector, sortElem, sordOrder);
    });
});

function sortAllGroupSection() {

    $('.activities-list ul').each(function () {
        var sortArg = 'data-sort-date';
        var sordOrder = 'desc';
        var sortSelector = $(this);
        var sortElem = 'li';
        sortElementBy(sortArg, sortSelector, sortElem, sordOrder);
    });
}

function sortNFilterUnitSection(unitId) {
    var sortArg = $('#SortedBy_' + unitId).find('a.active').data('type').toLowerCase() === 'name' ? 'data-search-text' : 'data-sort-date';
    var sordOrder = $('#SortedBy_' + unitId).find('a.active').data('order').toLowerCase();
    var sortSelector = $('#topic_' + unitId);;
    var sortElem = 'li';
    sortElementBy(sortArg, sortSelector, sortElem, sordOrder);

    var typeText = $('#SortResourceTypeBy_' + unitId).find('a.active').data('type').toLowerCase();
    filterResourceType(unitId, typeText);
}

function sortElementBy(arg, sel, elem, order) {
    var $selector = $(sel),
        $element = $selector.children(elem);
    $element.sort(function (a, b) {
        var an = a.getAttribute(arg),
            bn = b.getAttribute(arg);
        if (order == "asc") {
            if (an > bn)
                return 1;
            if (an < bn)
                return -1;
        } else if (order == "desc") {
            if (an < bn)
                return 1;
            if (an > bn)
                return -1;
        }
        return 0;
    });
    $element.detach().appendTo($selector);
}

function filterResourceType(unitId, type)
{
    $('#topic_' + unitId + ' li').each(function () {
        var showCurrentLi = type !== 'all' ? $(this).attr('data-sort-type').toLowerCase().indexOf(type) !== -1 : true;
        //console.log(showCurrentLi);
        $(this).toggle(showCurrentLi);
        //if (showCurrentLi) {
        //    //isContent = true;
        //    $(this).addClass('d-flex');
        //}
        //else {
        //    $(this).removeClass('d-flex');
       // }
    });
}

function mCustomScrollBar() {
    //$.fn.mCustomScrollbar("destroy");
    var $scrollBarDiv = $(".setScrollForDiv").toArray();
    $.each($scrollBarDiv, function (i, val) {
        var divHeight = $(this).height();
        if (divHeight > 250) {
            $(this).mCustomScrollbar({
                setHeight: "250px",
                autoExpandScrollbar: true,
                scrollbarPosition: "outside",
                autoHideScrollbar: true
            });
        }
    });


}

function addScrollBarClass() {
    var $scrollBarDiv = $('.activities-list').toArray();
    $.each($scrollBarDiv, function (i, val) {
        var divHeight = $(this).children('ul').height();
        if (divHeight > 250) {
            $(this).addClass('overflow-auto');
        }
        else {
            $(this).removeClass('overflow-auto');
        }
    });

}

function checkForBespokeGroup() {
    var isBespokeGroup = $("#IsBespokeGroup").val();
    var autoSync = $("#AutoSyncGroupMember").val();

    if (isBespokeGroup == 'True') {
        $('.isBespoke-add ').removeClass('d-none');
        $(".archDivider").removeClass('d-none');
        $('.btnArchiveOption').removeClass('d-none');
        $('#btnArchive').removeClass('d-none');
        $("#teacherOptions").removeClass("d-none");

        if (autoSync == 'True') {
            $(".autoSyncCls").addClass("d-none");
            $("#memberOption a.autoSyncCls").remove();
        }
        else {
            $(".autoSyncCls").removeClass("d-none");
        }
    }
    else {
        $("#memberOption a.isBespoke-add").remove();

        $(".archDivider").addClass('d-none');
        $('.btnArchiveOption').addClass('d-none');
        $('#btnArchive').addClass('d-none');
        $("#teacherOptions").addClass("d-none");
    }
}

function redirect(url) {
    // window.open(url, "_blank");
    window.location.replace(url);
}

function loadTabContent(tabNo) {

    $('.contact-list-card').parent().removeClass('d-none');
    $('#groupPageContent').parent().addClass('col-lg-9').removeClass('col-lg-12');
    $('#PageName').val($('#PageName').attr("data-val"));
    $('.search-create-row').show();
    if (tabNo == '2') {
        $('.toggable-group-wrapper .activities-tab a').addClass('active');
        //$('.header-strip').removeClass('d-none');
        //$('.group-banner').addClass('d-none');
        //$('.activities-icon').removeClass('d-none');
        //$('.chatter-icon').addClass('d-none');       
        $('.all-topics-card').removeClass('d-none');
        $('.forthcoming-events-card, .chat-list-card, .group-list-card, .viewchatter, .recorded-session-menu').addClass('d-none');
        // $('.activities-wrapper').removeClass('d-none');
        // $('.chatter-wrapper').addClass('d-none');
        $(".chatter-menu").addClass("d-none");
        $(".activity-menu").removeClass("d-none");
        $(".member-menu").addClass("d-none");
        $('.contact-list-card').addClass('d-none');
        $('.group-members-tab a').removeClass('active');
        $('.chatter-tab a').removeClass('active');
        $(".quicklink-card").addClass("d-none");
        $('.gradebook-tab, .reorded-session-tab a').removeClass('active');
        $('#sortUnit').removeClass('d-none');
    }
    else if (tabNo == '3') {
        $('.toggable-group-wrapper .group-members-tab a').addClass('active');
        //$('.group-banner').addClass('d-none');
        //$('.header-strip').removeClass('d-none');
        $('.all-topics-card').addClass('d-none');
        $('.forthcoming-events-card, .chat-list-card, .group-list-card, .viewchatter, .recorded-session-menu').addClass('d-none');
        //$('.chatter-wrapper').removeClass('d-none');
        //$('.activities-wrapper').addClass('d-none');
        $(".chatter-menu").addClass("d-none");
        $(".activity-menu").addClass("d-none");
        $(".member-menu").removeClass("d-none");
        $(".quicklink-card").removeClass("d-none");

        $('.chatter-tab a').removeClass('active');
        $('.activities-tab a').removeClass('active');
        $('.contact-list-card').removeClass('d-none');
        $('.gradebook-tab, .reorded-session-tab a').removeClass('active');
        $('#sortUnit').addClass('d-none');

    }
    else if (tabNo == '4') {
        $('.grpTab a').removeClass('active');
        $(".recorded-session-menu").addClass("d-none");
        $('.gradebook-tab a').addClass('active');
        $('.contact-list-card').parent().addClass('d-none');
        $('#groupPageContent').parent().removeClass('col-lg-9').addClass('col-lg-12');
        $('.search-create-row').hide();
        $('#sortUnit').addClass('d-none');
    }
    else if (tabNo == '5') {
        $("#groupPageContent").empty();
        $(".chatter-menu, .activity-menu, .member-menu").addClass("d-none");
        $('.grpTab a').removeClass('active');
        $(".recorded-session-menu").removeClass("d-none");
        $(".reorded-session-tab a").addClass("active");
        $("#sessionFromDate, #sessionToDate").datetimepicker({
            format: 'DD-MMM-YYYY',
        }).on("dp.change", function () {
            $("#SessionPaginationIndex").val('1')
            initRecordedSessionPagination();
        });
        initRecordedSessionPagination();
        $('#sortUnit').addClass('d-none');
        return false;
    }
    else {
        $('#PageName').val("Chatter");
        $('.toggable-group-wrapper .chatter-tab a').addClass('active');
        //$('.group-banner').removeClass('d-none');
        //$('.header-strip').addClass('d-none');
        //$('.chatter-icon').removeClass('d-none');
        //$('.activities-icon').addClass('d-none');
        $('.all-topics-card, .recorded-session-menu').addClass('d-none');
        $('.forthcoming-events-card, .chat-list-card, .group-list-card, .viewchatter').removeClass('d-none');
        //$('.chatter-wrapper').removeClass('d-none');
        //$('.activities-wrapper').addClass('d-none');
        $(".chatter-menu").removeClass("d-none");
        $(".activity-menu").addClass("d-none");
        $(".member-menu").addClass("d-none");
        $('.group-members-tab a').removeClass('active');
        $('.activities-tab a').removeClass('active');
        $('.contact-list-card').addClass('d-none');
        $(".quicklink-card").addClass("d-none");
        $('.gradebook-tab, .reorded-session-tab a').removeClass('active');
        $('#sortUnit').addClass('d-none');
    }
    $('.group-banner').removeClass('d-none');
    groupDetail.loadGroupContent(tabNo);
}

function initFileUpload(sectionId, moduleId, parentFdId) {
    $("#postedFile, #postedFileList").fileinput({
        language: translatedResources.langCode,
        title: translatedResources.browsefile,
        theme: "fas",
        showRemove: true,
        showUpload: true,
        showCaption: false,
        showMultipleNames: false,
        minFileCount: 1,
        maxFileCount: 10,
        maxTotalFileCount: 10,
        uploadUrl: "/Files/Files/SaveFileData",
        uploadExtraData: function () {
            return {
                __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
                moduleId: $("#CurrentModuleId").val(),
                parentFolderId: $("#CurrentFolderId").val(),
                sectId: $("#CurrentSectionId").val(),
                isAddMode: 'True'
            };
        },
        fileActionSettings: {
            showZoom: false,
            showUpload: false,
            //indicatorNew: "",
            showDrag: false
        },
        allowedFileExtensions: JSON.parse(translatedResources.fileExtesion),
        uploadAsync: true,
        maxFileSize: translatedResources.fileSizeAllow,
        browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
        removeClass: "btn btn-sm m-0 z-depth-0",
        uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
        cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
        overwriteInitial: false,
        initialPreviewAsData: true, // defaults markup
        initialPreviewConfig: [{
            frameAttr: {
                title: 'My Custom Title',
            }
        }],
        preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
        previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
        previewSettings: fileInputControlSettings.previewSettings,
        previewFileExtSettings: fileInputControlSettings.previewFileExtSettings,
        msgSizeTooLarge: 'Total File size (<b>{size}</b>) exceeds maximum allowed upload size of <b>{maxSize}</b>. Please retry your upload!'
    }).off('filebatchselected').on('filebatchselected', function (event, files) {
        var size = 0;
        $.each(files, function (ind, val) {
            //console.log($(this).attr('size'));
            size = size + $(this).attr('size');
        });

        var maxFileSize = $(this).data().fileinput.maxFileSize,
            msg = $(this).data().fileinput.msgSizeTooLarge,
            formatSize = (s) => {
                i = Math.floor(Math.log(s) / Math.log(1024));
                sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
                return out;
            };

        if ((size / 1024) > maxFileSize) {
            msg = msg.replace('{name}', 'all files');
            msg = msg.replace('{size}', formatSize(size));
            msg = msg.replace('{maxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
            //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
            if (files.length > 1) {
                $(".kv-fileinput-error.file-error-message").html('');
                $(".kv-fileinput-error.file-error-message").html('');
                $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                $(".close.kv-error-close").on('click', function (e) {
                    $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                    //$(".fileinput-upload").removeClass('d-none');
                });
            }
            else {
                $(".kv-fileinput-error.file-error-message").html('');
                $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                $(".close.kv-error-close").on('click', function (e) {
                    $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                    //$(".fileinput-upload").removeClass('d-none');
                });
            }

            $(".fileinput-upload").attr('disabled', true);
        }
    }).off('filebatchuploadcomplete').on('filebatchuploadcomplete', function (event, data, previewId, index, data) {
        $('#postedFile').fileinput('clear');
        //folder.navigateToFolder();
        $('#fileUploadModal').modal('hide');
        if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
            groupDetail.loadTopicActivitiesList();
        }

    }).off("filebatchuploadsuccess").on("filebatchuploadsuccess", function (event, data, previewId, index) {
        globalFunctions.showMessage(data.response.NotificationType, data.response.Message);
    });
}

function updateAssignedTeacherMembersPermission(permissionType, source) {

    jQuery.ajaxSettings.traditional = true;

    if ($('#selectAllAssignedTeacher').is(':checked')) {
        $('#selectAllAssignedTeacher').prop('checked', false);
    }
    var $selectedMembers = $("#assignedTeacherMembers input[type='checkbox']:checked");
    var membersToAdd = [];
    if ($selectedMembers != null && $selectedMembers.length > 0) {
        $selectedMembers.each(function (i, elem) {
            var groupId = $(elem).attr('data-groupId');
            membersToAdd.push({
                MemberId: elem.value,
                GroupId: groupId,
                IsViewOnly: permissionType === "ViewOnly" ? true : false,
                IsContribute: permissionType === "Contribute" ? true : false,
                IsManager: permissionType === "Manager" ? true : false,
                ToBeDeleted: permissionType === "Delete" ? true : false
            });
        });
        if (permissionType === "Delete") {
            globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
            $(document).bind('okToDelete', source, function (e) {
                $.ajax({
                    type: 'POST',
                    data: { membersToAdd: JSON.stringify(membersToAdd) },
                    url: '/SchoolStructure/SchoolGroups/AddUpdateMembersToGroup',
                    success: function (result) {
                        globalFunctions.showSuccessMessage(translatedResources.deletedSuccess);
                        groupDetail.loadGroupContent("3");
                    },
                    error: function (data) { }
                });
            });
        }
        else {
            $.ajax({
                type: 'POST',
                data: { membersToAdd: JSON.stringify(membersToAdd) },
                url: '/SchoolStructure/SchoolGroups/AddUpdateMembersToGroup',
                success: function (result) {
                    globalFunctions.showSuccessMessage(translatedResources.showSuccessMessage);
                    groupDetail.loadGroupContent("3");
                },
                error: function (data) { }
            });
        }
    }
    else {
        globalFunctions.showWarningMessage(translatedResources.MemberSelectMessage);
    }
}

function updateAssignedMembersPermission(permissionType, source) {

    jQuery.ajaxSettings.traditional = true;

    if ($('#selectAllAssigned').is(':checked')) {
        $('#selectAllAssigned').prop('checked', false);
    }
    var $selectedMembers = $("#assignedMembers input[type='checkbox']:checked");
    var membersToAdd = [];
    if ($selectedMembers != null && $selectedMembers.length > 0) {
        $selectedMembers.each(function (i, elem) {
            var groupId = $(elem).attr('data-groupId');
            membersToAdd.push({
                MemberId: elem.value,
                GroupId: groupId,
                IsViewOnly: permissionType === "ViewOnly" ? true : false,
                IsContribute: permissionType === "Contribute" ? true : false,
                IsManager: permissionType === "Manager" ? true : false,
                ToBeDeleted: permissionType === "Delete" ? true : false
            });
        });
        if (permissionType === "Delete") {
            globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
            $(document).bind('okToDelete', source, function (e) {
                $.ajax({
                    type: 'POST',
                    data: { membersToAdd: JSON.stringify(membersToAdd) },
                    url: '/SchoolStructure/SchoolGroups/AddUpdateMembersToGroup',
                    success: function (result) {
                        globalFunctions.showSuccessMessage(translatedResources.deletedSuccess);
                        groupDetail.loadGroupContent("3");
                    },
                    error: function (data) { }
                });
            });
        }
        else {
            $.ajax({
                type: 'POST',
                data: { membersToAdd: JSON.stringify(membersToAdd) },
                url: '/SchoolStructure/SchoolGroups/AddUpdateMembersToGroup',
                success: function (result) {
                    globalFunctions.showSuccessMessage(translatedResources.showSuccessMessage);
                    groupDetail.loadGroupContent("3");
                },
                error: function (data) { }
            });
        }
    }
    else {
        globalFunctions.showWarningMessage(translatedResources.MemberSelectMessage);
    }
}

function updateMemberPermission(permissionType, memberId, groupId, source) {

    jQuery.ajaxSettings.traditional = true;

    var membersToAdd = [];
    membersToAdd.push({
        MemberId: memberId,
        GroupId: groupId,
        IsViewOnly: permissionType === "ViewOnly" ? true : false,
        IsContribute: permissionType === "Contribute" ? true : false,
        IsManager: permissionType === "Manager" ? true : false,
        ToBeDeleted: permissionType === "Delete" ? true : false
    });
    if (globalFunctions.isValueValid(memberId) && permissionType === "Delete") {
        globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
        $(document).bind('okToDelete', source, function (e) {
            $.ajax({
                type: 'POST',
                data: { membersToAdd: JSON.stringify(membersToAdd) },
                url: '/SchoolStructure/SchoolGroups/AddUpdateMembersToGroup',
                success: function (result) {
                    globalFunctions.showSuccessMessage(translatedResources.deletedSuccess);
                    groupDetail.loadGroupContent("3");
                },
                error: function (data) { }
            });
        });
    }
    else {
        $.ajax({
            type: 'POST',
            data: { membersToAdd: JSON.stringify(membersToAdd) },
            url: '/SchoolStructure/SchoolGroups/AddUpdateMembersToGroup',
            success: function (result) {
                globalFunctions.showSuccessMessage(translatedResources.showSuccessMessage);
                groupDetail.loadGroupContent("3");
            },
            error: function (data) { }
        });
    }

}

function AddMemberInGroup(permissionType) {
    jQuery.ajaxSettings.traditional = true;
    var groupId = $('#SectionId').val();

    var $selectedMembers = $("#UnAssignedMemberList option:selected");

    var groupIds = $('#groupDropdown').val().toString();

    var membersToAdd = [];
    if ($selectedMembers != null && $selectedMembers.length > 0) {
        $selectedMembers.each(function (i, elem) {
            membersToAdd.push(
                {
                    MemberId: elem.value,
                    GroupId: groupId,
                    IsViewOnly: permissionType === "ViewOnly" ? true : false,
                    IsContribute: permissionType === "Contribute" ? true : false,
                    IsManager: permissionType === "Manager" ? true : false,

                }
            );
        });

        $.ajax({
            type: 'POST',
            data: { membersToAdd: JSON.stringify(membersToAdd), classGroups: groupIds },
            url: '/SchoolStructure/SchoolGroups/AddUpdateMembersToGroup',
            success: function (result) {
                globalFunctions.showSuccessMessage(translatedResources.showSuccessMessage);
                groupDetail.loadGroupContent("3");
                $("#myModal").modal('hide');
            },
            error: function (data) { }
        });
    }
    else {
        globalFunctions.showWarningMessage(translatedResources.MemberSelectMessage);
    }


}

function ArchiveThisGroup(source, groupId) {
    //debugger;

    globalFunctions.notyDeleteConfirm(source, translatedResources.archiveConfirm);
    $(document).bind('okToDelete', source, function (e) {

        var assignmentsToArchive = [];
        if (groupId != undefined && globalFunctions.isValueValid(groupId)) {
            assignmentsToArchive = assignmentsToArchive + groupId + ',';
            $.ajax({
                type: 'POST',
                data: { assignmentsToArchive: assignmentsToArchive },
                url: '/SchoolStructure/SchoolGroups/UpdateArchiveGroup',
                success: function (result) {
                    location.href = '/SchoolStructure/SchoolGroups/';
                },
                error: function (data) { }
            });
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.PleaseGroupToArchive);
        }
    });
}

function InitSvg() {
    //$('img.svg').each(function () {
    //    var $img = $(this);
    //    var imgID = $img.attr('id');
    //    var imgClass = $img.attr('class');
    //    var imgURL = $img.attr('src');


    //    $.get(imgURL, function (data) {
    //        debugger;
    //        // Get the SVG tag, ignore the rest
    //        var $svg = $(data).find('svg');

    //        // Add replaced image's ID to the new SVG
    //        if (typeof imgID !== 'undefined') {
    //            $svg = $svg.attr('id', imgID);
    //        }
    //        // Add replaced image's classes to the new SVG
    //        if (typeof imgClass !== 'undefined') {
    //            $svg = $svg.attr('class', imgClass + ' replaced-svg');
    //        }

    //        // Remove any invalid XML tags as per http://validator.w3.org
    //        $svg = $svg.removeAttr('xmlns:a');

    //        // Check if the viewport is set, else we gonna set it if we can.
    //        if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
    //            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
    //        }

    //        // Replace image with new SVG
    //        $img.replaceWith($svg);

    //    }, 'xml');

    //});
}

function initRecordedSessionPagination() {
    var fromDate = $("#sessionFromDate").val() || "";
    var toDate = $("#sessionToDate").val() || "";
    var searchText = $("#search-sessions").val() || "";
    var grid = new DynamicPagination("groupPageContent");
    var settings = {
        url: '/SchoolStructure/SchoolGroups/GetGroupRecordedSessions?pageIndex=' + $("#SessionPaginationIndex").val() + "&fromDate=" +
            fromDate + "&toDate=" + toDate + "&searchText=" + searchText + "&groupId=" + $("#SectionId").val()

    };
    grid.init(settings);
}


