﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
//var popoverEnabler = new LanguageTextEditPopover();
var schoolGroup = function () {
    var init = function () {
        loadSchoolGroupGrid();
    },
        loadSchoolGroupGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "GroupName", "sTitle": translatedResources.SchoolGroupName, "sWidth": "25%" },
                { "mData": "GroupDesc", "sTitle": translatedResources.SchoolGroupDesc, "sWidth": "25%", "sClass": "open-details-control" },
                { "mData": "Subject", "sTitle": translatedResources.Subject, "sWidth": "25%", "sClass": "open-details-control" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "15%" }
            );
            initSchoolGroupGrid('tbl-SchoolGroups', _columnData, '/SchoolGroup/SchoolGroup/LoadSchoolGroups');
        },

        initSchoolGroupGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false
            };
            grid.init(settings);
        };
    openSchoolGroup = function () {

    },


        addnewSchoolGroup = function (id) {
            //window.location.assign("/SchoolGroup/SchoolGroup/InitAddEditSchoolGroupForm?id=" + id);
            window.location.assign("/SchoolStructure/SchoolGroups/AddEditSchoolGroupForm?id=" + id);
            //popoverEnabler.attachPopover();
        },
        Deletebespokegroup = function Deletebespokegroup(deletebespoke) {
            globalFunctions.notyDeleteConfirm(deletebespoke, translatedResources.deleteConfirm);
            $(document).bind('okToDelete', deletebespoke, function (e) {
                var groupId = $(deletebespoke).attr('data-grpid');
                $.post("/SchoolStructure/SchoolGroups/DeleteBespokeGroup", { groupId: groupId }, function (response) {
                    //window.location.href = "/schoolstructure/schoolgroups"
                    console.log(response.Result);
                    globalFunctions.showMessage(response.operation.NotificationType, response.operation.Message);
                    $('#BespokeGroups').html(response.Result);

                });
            });
        },

        initMeetingDurationPopup = function (source) {
            globalFunctions.loadPopup($(source), "/SchoolStructure/SchoolGroups/InitGroupMeetingDurationForm?schoolGroupId=" + $(source).data("id"), translatedResources.createLiveClass);
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $('.date-picker').datetimepicker({
                    format: 'DD-MMM-YYYY',
                    minDate: moment()
                });
                $('.date-picker1').datetimepicker({
                    format: 'hh:mm A'
                })
            });
        },

        onOnlineMeetingSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            $("#btnSaveZoomMeeting").removeAttr("disabled");
            if (data.Success) {
                $("#myModal").modal("hide");

                if ($(".todaysScheduleCarousel").length > 0)
                    GetWeeklyEvents();
            }
        },

        recreateAndUpdateMeeting = function (data) {
            $.post("/schoolstructure/schoolgroups/RenewExistingBBBMeeting", { MeetingId: data.MeetingId }, function (res) {

            }).fail(function () { globalFunctions.onFailure(); });
        },

        getAllTeamsMeetings = function (source, schoolGroupId) {
            globalFunctions.loadPopup($(source), "/schoolstructure/schoolgroups/GetAllTeamsMeetings?schoolGroupId=" + schoolGroupId, translatedResources.allTeamsMeetings, "modal-lg");
        },
        getAllZoomMeetings = function (source, schoolGroupId) {
            var currentTime = new Date();
            globalFunctions.loadPopup($(source), "/schoolstructure/schoolgroups/GetAllGroupZoomMeetings?schoolGroupId=" + schoolGroupId + "&timeZoneOffset=" + currentTime.getTimezoneOffset(), translatedResources.allMeetings);

            $(source).off("modalLoaded");
            $("#myModal").on('show.bs.modal', function () {
                $("#myModal .modal-dialog").addClass("modal-lg");
            });
            $(source).on("modalLoaded", function () {

            });
        },

        showGroupAdobeMeetings = function (source, schoolGroupId) {
            globalFunctions.loadPopup($(source), "/schoolstructure/schoolgroups/GetAllAdobeMeetings?schoolGroupId=" + schoolGroupId, translatedResources.allMeetings);
        },

        getAllBBBGroupMeetings = function (source, schoolGroupId) {
            globalFunctions.loadPopup($(source), "/schoolstructure/schoolgroups/GetAllGroupBBBMeetings?schoolGroupId=" + schoolGroupId, translatedResources.allMeetings);
        },

        initWebExMeetingForm = function (source, schoolGroupId) {
            globalFunctions.loadPopup($(source), "/schoolstructure/schoolgroups/InitWebExMeetingForm?schoolGroupId=" + schoolGroupId, translatedResources.createMeeting);
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $('.date-picker').datetimepicker({
                    format: 'DD-MMM-YYYY',
                    minDate: moment()
                });

                $('.date-picker1').datetimepicker({
                    format: 'hh:mm A'
                })
            })
        },

        deleteLiveSession = function (source, onlineMeetingId) {
            globalFunctions.notyDeleteConfirm($(source));
            $(source).off("okToDelete");
            $(source).on("okToDelete", function () {
                $.ajax({
                    url: "/SchoolStructure/SchoolGroups/DeleteLiveSession",
                    data: { meetingId: onlineMeetingId },
                    type: "POST",
                    success: function (data) {
                        globalFunctions.showMessage(data.NotificationType, data.Message);
                        if (data.Success) {
                            $(source).closest('tr').remove();
                        }
                    },
                    error: function () {
                        globalFunctions.onFailure();
                    }
                })
            });

        },

        deleteTeamsSession = function (source, schoolTeamMeetingId) {
            globalFunctions.notyDeleteConfirm($(source));
            $(source).off("okToDelete");
            $(source).on("okToDelete", function () {
                $.ajax({
                    url: "/SchoolStructure/SchoolGroups/DeleteTeamsSession",
                    data: { schoolTeamMeetingId: schoolTeamMeetingId },
                    type: "POST",
                    success: function (data) {
                        globalFunctions.showMessage(data.NotificationType, data.Message);
                        if (data.Success) {
                            $(source).closest('tr').remove();
                        }
                    },
                    error: function () {
                        globalFunctions.onFailure();
                    }
                })
            });
        }

    joinWebExMeeting = function (meetingKey) {
        $.get("/schoolstructure/schoolgroups/GetWebExMeetingUrl?meetingKey=" + meetingKey, function (data) {
            if (!data.Success)
                globalFunctions.showMessage(data.NotificationType, data.Message);
            else location.href = data.HeaderText;
        }).fail(function () { globalFunctions.onFailure(); })
    },

        getAllWebExMeetings = function (source, schoolGroupId) {
            globalFunctions.loadPopup($(source), "/schoolstructure/schoolgroups/GetAllWebExMeetings?schoolGroupId=" + schoolGroupId, translatedResources.allMeetings);
        };

    return {
        init: init,
        loadSchoolGroupGrid: loadSchoolGroupGrid,
        addnewSchoolGroup: addnewSchoolGroup,
        Deletebespokegroup: Deletebespokegroup,
        initMeetingDurationPopup: initMeetingDurationPopup,
        onOnlineMeetingSaveSuccess: onOnlineMeetingSaveSuccess,
        getAllTeamsMeetings: getAllTeamsMeetings,
        getAllZoomMeetings: getAllZoomMeetings,
        showGroupAdobeMeetings: showGroupAdobeMeetings,
        getAllBBBGroupMeetings: getAllBBBGroupMeetings,
        initWebExMeetingForm: initWebExMeetingForm,
        getAllWebExMeetings: getAllWebExMeetings,
        joinWebExMeeting: joinWebExMeeting,
        deleteLiveSession: deleteLiveSession,
        deleteTeamsSession: deleteTeamsSession
    };
}();




$(document).ready(function () {

    $("#grid").addClass('active');
    $('#list').click(function (event) {
        event.preventDefault();
        $('#library .item').addClass('list-group-item');
        $(this).toggleClass('active');
        $("#grid").removeClass('active');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#library .item').removeClass('list-group-item');
        $('#library .item').addClass('grid-group-item');
        $(this).toggleClass('active');
        $("#list").removeClass('active');
    });

    schoolGroup.init();

    $(document).on("submit", "form#formSynchronousLessons", function () {
        $("#btnSaveZoomMeeting").attr("disabled", "disabled");
    });

    $(document).on('click', '#btnAddSchoolGroup', function () {
        schoolGroup.addnewSchoolGroup();
        //popoverEnabler.attachPopover();
    });
    //$("#divanchrDeleteGroup a").on('click', function () {
    //    var groupId = $(this).attr('data-grpid');
    //    $.post("/SchoolStructure/SchoolGroups/DeleteBespokeGroup", { groupId: groupId }, function (response) {
    //        window.location.href = "/schoolstructure/schoolgroups"
    //        globalFunctions.showMessage(response.NotificationType, response.Message);

    //    });

    //});

    $(document).on('click', 'a.meeting-actions', function (e) {
        var meeting = groupMeetingFunction.getInstance();
        if ($(e.target).is('.create-new-meeting')) {
            schoolGroup.initMeetingDurationPopup(this);
        }
        else {
            meeting.joinMeeting($(this).data("id"), $(this).data("meetingid"))
        }
    });

    $(document).on('click', 'a.ms-teams.create-meeting', function (e) {
        var meeting = groupMeetingFunction.getInstance();
        meeting.createMsTeamsMeeting($(this), $(this).data("id"));
    });

    $(document).on('click', 'a.zoom-meeting-actions.create-zoom-meeting', function () {
        var meeting = groupMeetingFunction.getInstance();
        meeting.initZoomMeetingForm($(this), $(this).data("id"));
    });

    $(document).on("click", "a.connect-meeting-actions.connectnow-meeting", function () {
        var meeting = groupMeetingFunction.getInstance();
        meeting.initConnectMeetingForm($(this), $(this).data("id"));
    });


    $(document).on("keyup", "#MeetingUrl", function () {
        var value = $(this).val();
        $(".lbl-meeting-url").toggle(globalFunctions.isValueValid(value));
        $(".lbl-meeting-url span").text(value.replace(/ /g, ""));
    });

    $('body').on('click', '#btnSaveZoomMeeting', function (e) {
        $('form#formSynchronousLessons').submit();
    });
});
