﻿var groupMeetingFunction = function () {
    var instance;

    function init() {
        createMeeting = function (source, id, meetingName, meetingDuration) {
            $.ajax({
                data: { id: id, meetingName: meetingName, meetingDuration: meetingDuration },
                url: "/SchoolStructure/SchoolGroups/CreateGroupMeeting",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        $("#myModal").modal("hide");
                        $(source).addClass("join-existing-meeting").removeClass("create-new-meeting");
                        globalFunctions.isValueValid($(source).data("icon")) ? $(source).tooltip('hide').attr("data-original-title", translatedResources.joinMeeting) : $(source).text(translatedResources.joinMeeting);

                        $(source).data("meetingid", data.HeaderText);
                    }
                },
                error: function () { globalFunctions.onFailure() }
            })
        },

            joinMeeting = function (id, meetingid) {
                $.ajax({
                    data: { id: id, meetingId: meetingid },
                    url: "/SchoolStructure/SchoolGroups/GetMeetingJoinUrl",
                    success: function (data) {
                        if (data.Result.Identifier == "CREATENEWMEETING") {
                            $(document).trigger('recreatemeeting', [{ SchoolGroupId: id, Identifier: data.Result.Identifier }]);
                        } else {
                            if (globalFunctions.isValueValid(data.MeetingURL))
                                window.open(data.MeetingURL);
                            else
                                globalFunctions.showWarningMessage(data.Result.Message);
                        }

                    },
                    error: function () { globalFunctions.onFailure() }
                })
            },

            initOnlineMeetingForm = function (source, id) {
                globalFunctions.loadPopup($(source), "/Schoolstructure/SchoolGroups/InitMSTeamsForm?schoolGroupId=" + id, translatedResources.createTeamsMeetingTitle, "modal-lg");
                $(source).off("modalLoaded");
                $(source).on("modalLoaded", function () {
                    $('.date-picker').datetimepicker({
                        format: 'DD-MMM-YYYY',
                        minDate: moment(),
                        locale: translatedResources.locale
                    });

                    $('.date-picker1').datetimepicker({
                        format: 'hh:mm A',
                        locale: translatedResources.locale
                    }).on("dp.change", function (e) {
                        if ($(e.target).is("#MeetingStartTime"))
                            $("#MeetingEndTime").data("DateTimePicker").minDate(e.date.add(5, "minutes"));
                    });
                });
            },

            createMsTeamsMeeting = function (source, id) {
                $.get("/SchoolStructure/SchoolGroups/CheckMSTeamsAccessToken", function (data) {
                    if (data.Success)
                        initOnlineMeetingForm(source, id);
                    else {
                        localStorage.setItem("external_auth_complete", "false");
                        var element = $("<a/>", {
                            id: 'action-externalauth',
                        });
                        element.data("redirectUri", data.HeaderText);
                        element.data("sourceclass", "teams-meeting-"+id);
                        element.appendTo($("body"));
                        element.trigger("click");
                    }
                });

            },

            crceateZoomMeeting = function (source, schoolGroupId) {
                $.ajax({
                    url: "/schoolstructure/schoolgroups/CreateZoomMeeting",
                    data: { schoolGroupId: schoolGroupId },
                    success: function (data) {
                        globalFunctions.showMessage(data.NotificationType, data.Message);
                    },
                    error: function () { globalFunctions.onFailure(); }
                })
            },

            initZoomMeetingForm = function (source, id) {
                globalFunctions.loadPopup($(source), "/Schoolstructure/SchoolGroups/InitZoomMeetingForm?schoolGroupId=" + id, translatedResources.createZoomMeeting);
                $(source).off("modalLoaded");
                $("#myModal").on('show.bs.modal', function () {
                    $("#myModal .modal-dialog").addClass("modal-lg");

                    //Added by Deepak Singh for my group detail page
                    var pageName = $("#PageName").val();
                    if (pageName != undefined && pageName == 'GroupDetails') {
                        $("#formSynchronousLessons").attr("data-ajax-success", "groupDetail.onOnlineMeetingSaveSuccess");
                    }

                });

                $(source).on("modalLoaded", function () {
                    var currentDate = new Date();

                    $("#TimeZoneOffset").val(currentDate.getTimezoneOffset());
                    $(".selectpicker").selectpicker({
                        size: 5
                    });
                    $('.date-picker').datetimepicker({
                        format: 'DD-MMM-YYYY',
                        minDate: moment(),
                        locale: translatedResources.locale
                    });

                    $('.date-picker1').datetimepicker({
                        format: 'HH:mm',
                        locale: translatedResources.locale
                    })
                });
            },

            initConnectMeetingForm = function (source, id) {
                globalFunctions.loadPopup($(source), "/Schoolstructure/SchoolGroups/InitConnectNowMeetingForm?schoolGroupId=" + id, translatedResources.createMeeting);
                $(source).off("modalLoaded");
                $(source).on("modalLoaded", function () {
                    $('.date-picker').datetimepicker({
                        minDate: moment(),
                        format: 'DD-MMM-YYYY'
                    }).on("dp.change", function (e) {
                        if ($(e.target).is("#MeetingStartDate"))
                            $("#MeetingEndDate").data("DateTimePicker").minDate(e.date);
                    });
                    $('.date-picker1').datetimepicker({
                        minDate: moment(),
                        format: 'hh:mm A'
                    }).on("dp.change", function (e) {
                        if ($(e.target).is("#MeetingStartTime"))
                            $("#MeetingEndTime").data("DateTimePicker").minDate(e.date);
                    });
                });
            },

            recreateAndJoinBBBMeeting = function (meetingId) {
                $.ajax({
                    type: 'POST',
                    url: '/SchoolStructure/SchoolGroups/RewampExpiredBBBMeeting',
                    data: { meetingId: meetingId },
                    success: function (data) {
                        if (globalFunctions.isValueValid(data.HeaderText))
                            location.href = data.HeaderText;
                    },
                    error: function () {
                        globalFunctions.onFailure();
                    }
                });
            };

        return {
            createMeeting: createMeeting,
            joinMeeting: joinMeeting,
            createMsTeamsMeeting: createMsTeamsMeeting,
            crceateZoomMeeting: crceateZoomMeeting,
            initZoomMeetingForm: initZoomMeetingForm,
            initConnectMeetingForm: initConnectMeetingForm,
            recreateAndJoinBBBMeeting: recreateAndJoinBBBMeeting
        }
    }

    return {
        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    };
}();

$(function () {

    $(document).on('recreatemeeting', function (e, data) {
        var meeting = groupMeetingFunction.getInstance();
        meeting.recreateAndJoinBBBMeeting(data.MeetingId);
    });

    $(document).on("click", "a.bbb-meeting.join-liveclass", function (e) {
        var schoolGroupId = $(this).data("schoolgroupid"), schoolGroupId = $(this).data("schoolgroupid"),
            meetingId = $(this).data("meetingid");
        $.ajax({
            data: { meetingId: meetingId },
            url: "/SchoolStructure/SchoolGroups/GetMeetingJoinUrl",
            success: function (data) {
                if (globalFunctions.isValueValid(data.MeetingURL))
                    location.href = data.MeetingURL;
                else {
                    if (data.Result.Identifier == "CREATENEWMEETING")
                        $(document).trigger('recreatemeeting', [{ MeetingId: meetingId, Identifier: data.Result.Identifier, SchoolGroupId: schoolGroupId }]);
                    else globalFunctions.showWarningMessage(data.Result.Message);
                }
            },
            error: function () { globalFunctions.onFailure() }
        })
    });
});