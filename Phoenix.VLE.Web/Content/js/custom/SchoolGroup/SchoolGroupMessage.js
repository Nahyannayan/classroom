﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
//var popoverEnabler = new LanguageTextEditPopover();
var schoolGroupMessage = function () {
    var init = function () {
        loadSchoolGroupMessageGrid();
    },
        loadSchoolGroupMessageGrid = function () {
            var id = $('#ParentGroupId').val();
            var _columnData = [];
            _columnData.push(
                { "mData": "Title", "sTitle": translatedResources.Title, "sWidth": "20%" },
                { "mData": "Message", "sTitle": translatedResources.Message, "sWidth": "50%", "sClass": "open-details-control" },
                { "mData": "NotifyTo", "sTitle": translatedResources.NotifyTo, "sWidth": "10%", "sClass": "open-details-control" },
                { "mData": "CreatedOn", "sTitle": translatedResources.CreatedOn, "sWidth": "20%", "sClass": "open-details-control","dateFormat":"mm/dd/yy" },
            );
            initSchoolGroupMessageGrid('tbl-GroupMessage', _columnData, '/SchoolStructure/SchoolGroups/LoadSchoolGroupMessages?id='+id);
            
        },

        initSchoolGroupMessageGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false
            };
            grid.init(settings);
        };
    openSchoolGroup = function () {

    };

    return {
        init: init,
        loadSchoolGroupMessageGrid: loadSchoolGroupMessageGrid
    };
}();




$(document).ready(function () {

    //$("#grid").addClass('active');
    //$('#list').click(function (event) {
    //    event.preventDefault();
    //    $('#library .item').addClass('list-group-item');
    //    $(this).toggleClass('active');
    //    $("#grid").removeClass('active');
    //});
    //$('#grid').click(function (event) {
    //    event.preventDefault();
    //    $('#library .item').removeClass('list-group-item');
    //    $('#library .item').addClass('grid-group-item');
    //    $(this).toggleClass('active');
    //    $("#list").removeClass('active');
    //});

    schoolGroupMessage.init();

    $("#tbl-GroupMessage_filter").hide();
    $('#tbl-GroupMessage').DataTable().search('').draw();
    $("#GroupMessageListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-GroupMessage').DataTable().search($(this).val()).draw();
    });

});