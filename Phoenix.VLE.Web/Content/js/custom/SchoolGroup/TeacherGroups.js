﻿var groups = function () {
    var editgroups = function (source, id) {
        loadgroupPopup(source, translatedResources.edit, id);
    },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');

                groups.getUpdatedGroups();
            }
        },
        loadgroupPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.SchoolGroupName;
            globalFunctions.loadPopup(source, '/SchoolStructure/SchoolGroups/InitAddEditSchoolGroupForm?id=' + id, title, 'group-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                formElements.feColorpicker();
                //popoverEnabler.attachPopover();
            });
        },
        getUpdatedGroups = function () {
            groups.loadSchoolgroups();
        },
        loadStudentList = function (source, id) {
            globalFunctions.loadPopup(source, '/SchoolStructure/SchoolGroups/StudentsInGroup?groupid=' + id, translatedResources.StudentsList, 'group-dailog modal-lg');
            $(source).off("modalLoaded");
            //$(source).on("modalLoaded", function () {

            //});
        },
        sendGroupMessage = function (source, id) {
            globalFunctions.loadPopup(source, '/SchoolStructure/SchoolGroups/LoadGroupMessageForm?groupdId=' + id, translatedResources.SendMessage, 'group-dailog');
            $(source).off("modalLoaded");
            //$(source).on("modalLoaded", function () {

            //});
        },
        loadSchoolgroups = function () {
            var grid = new DynamicPagination("SchoolGroups");
            var settings = {
                url: '/SchoolStructure/SchoolGroups/LoadSchoolGroups?pageIndex=1'
            };
            grid.init(settings);
            var nextGrid = new DynamicPagination("BespokeGroups");
            settings = {
                url: '/SchoolStructure/SchoolGroups/LoadBespokeGroup?pageIndex=1'
            };
            nextGrid.init(settings);
        },

        createZipPackage = function (source) {
            var file = FileDownloader.getInstance();
            file.createZipFile(source, "/Schoolstructure/SchoolGroups/CreateZipFile?id=" + $(source).data("id"));
        },
        downloadZipPackage = function (source) {
            window.location.assign("/shared/shared/DownloadFile?filePath=" + $(source).data("filepath"))
        },

        clearAllGroupFiles = function (source) {
            globalFunctions.notyConfirm($(source), translatedResources.clearAllFileMsg);
            $(source).off("okClicked");
            $(source).on("okClicked", function () {
                $.ajax({
                    type: "POST",
                    url: "/Schoolstructure/SchoolGroups/ClearGroupFiles",
                    data: { schoolGroupId: $(source).data("id") },
                    success: function (data) {
                        globalFunctions.showMessage(data.NotificationType, data.Message);
                    },
                    error: function () { globalFunctions.onFailure(); }
                })
            });
        };

    return {
        editgroups: editgroups,
        getUpdatedGroups: getUpdatedGroups,
        onSaveSuccess: onSaveSuccess,
        loadStudentList: loadStudentList,
        sendGroupMessage: sendGroupMessage,
        loadSchoolgroups: loadSchoolgroups,
        createZipPackage: createZipPackage,
        downloadZipPackage: downloadZipPackage,
        clearAllGroupFiles: clearAllGroupFiles
    };
}();

$(document).ready(function () {
    groups.loadSchoolgroups();

    $(document).on('ZIPCREATED', function (e, data) {
        var source = $("a.zip-action_" + data.InsertedRowId).data("filepath", data.HeaderText);
        groups.downloadZipPackage(source);
    });
});