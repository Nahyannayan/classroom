﻿var groups = function () {
    var editgroups = function (source, id) {
        loadgroupPopup(source,translatedResources.edit, id);
    },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');

                groups.getUpdatedGroups();
            }
        },
        loadgroupPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.SchoolGroupName;
            globalFunctions.loadPopup(source, '/SchoolStructure/SchoolGroups/InitAddEditSchoolGroupForm?id=' + id, title, 'group-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                formElements.feColorpicker();
                //popoverEnabler.attachPopover();
            });
        },
        getUpdatedGroups = function () {
            groups.loadSchoolgroups();
        },
        loadStudentList = function (source, id) {
            globalFunctions.loadPopup(source, '/SchoolStructure/SchoolGroups/StudentsInGroup?groupid=' + id, translatedResources.StudentsList, 'group-dailog modal-lg');
            $(source).off("modalLoaded");
           
        },
        sendGroupMessage = function (source, id) {
            globalFunctions.loadPopup(source, '/SchoolStructure/SchoolGroups/LoadGroupMessageForm?groupdId=' + id, translatedResources.SendMessage, 'group-dailog');
            $(source).off("modalLoaded");
            //$(source).on("modalLoaded", function () {

            //});
        },
        loadSchoolgroups = function () {
            var grid = new DynamicPagination("SchoolGroups");
            var settings = {
                url: '/SchoolStructure/SchoolGroups/LoadArchivedSchoolGroups?pageIndex=1'
            };
            grid.init(settings);
            var nextGrid = new DynamicPagination("BespokeGroups");
            settings = {
                url: '/SchoolStructure/SchoolGroups/LoadArchivedBespokeGroup?pageIndex=1'
            };
            nextGrid.init(settings);
        };
    return {
        editgroups: editgroups,
        getUpdatedGroups: getUpdatedGroups,
        onSaveSuccess: onSaveSuccess,
        loadStudentList: loadStudentList,
        sendGroupMessage: sendGroupMessage,
        loadSchoolgroups: loadSchoolgroups
    };
}();
function archiveSchoolGroup() {
    //Only to assign source, please follow standard way as in other js files for confirmation popup
    var source = $("#activeGroupList");
    globalFunctions.notyDeleteConfirm(source, translatedResources.archiveConfirm);
    $(document).bind('okToDelete', source, function (e) {
        if ($('#selectAllUnassigned').is(':checked')) {
            $('#selectAllUnassigned').prop('checked', false);
        }
        var selectedMembers = $("#activeGroupList input[type='checkbox']:checked");
        var assignmentsToArchive = [];
        if (selectedMembers != null && selectedMembers.length > 0) {
            selectedMembers.each(function (i, elem) {
                assignmentsToArchive = assignmentsToArchive + elem.value + ',';
            });
            $.ajax({
                type: 'POST',
                data: { assignmentsToArchive: assignmentsToArchive },
                url: '/SchoolStructure/SchoolGroups/UpdateArchiveGroup',
                success: function (result) {
                    loadGroupList();
                    // location.reload();
                },
                error: function (data) { }
            });
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.PleaseGroupToArchive);
        }
    });
}

function loadGroupList() {
    loadActiveGroup();
    loadArchiveGroup();
}

function loadActiveGroup() {
    $.ajax({
        url: '/SchoolStructure/SchoolGroups/GetActiveSchoolGroup',
        success: function (result) {
            $('#activeGroupList').html(result);
        },
        error: function (data) { }
    });
}

function loadArchiveGroup() {
    $.ajax({
        url: '/SchoolStructure/SchoolGroups/GetArchivedSchoolGroup',
        success: function (result) {
            $('#archiveGroupList').html(result);
        },
        error: function (data) { }
    });
}

$(document).on('keyup', '#availableMemberSearch', function (e) {
    var searchText = $(this).val().toLowerCase();
    $('#activeGroupList ul li').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
});

$(document).on('keyup', '#groupMemberSearch', function (e) {
    //if (e.which === 13) {
    var searchText = $(this).val().toLowerCase();
    $('#archiveGroupList ul li').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
    //}
});

$('#selectAllUnassigned').change(function () {
    var chk = $(this).is(':checked');
    $('input[type=checkbox]', "#activeGroup").each(function () {
        $(this).prop('checked', chk);
    });
});

$('#pills-studentList-tab').on('click', function (event) {
    groups.loadSchoolgroups();
});

$(document).ready(function () {
    groups.loadSchoolgroups();
    $('#pills-SelectedStudent-tab').on('click', function (event) {
        //var groupId = $('#SchoolGroupId').val();
        $.get("/SchoolStructure/SchoolGroups/ArchiveGroupList", function (response) {
            $('#selectedStudent').html(response);
            $('.selectpicker').selectpicker('refresh');
            $(".delegates-wrapper").mCustomScrollbar({
                setHeight: "250",
                autoExpandScrollbar: true,
            });           
        });
    });
});