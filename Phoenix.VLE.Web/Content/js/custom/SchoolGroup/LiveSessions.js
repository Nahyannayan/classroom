﻿
var liveSessionFunctions = function () {
    var init = function () {
        $(document).on("keyup", "#search-sessions", function (e) {
            if (e.which == 13) {
                $("#LiveSessionPaginationIndex").val('1');
                initPagination();
            }
        });

        $(document).on("change", "#ddlSchoolGroups", function () {
            initPagination();
        });
        $(document).on("change", "#ddlSessionType", function () {
            var value = $(this).val();
            $("#divSchoolGroupFilter").toggle(value != 'N');
            initPagination();
        });
        initPagination();
    },

        generateSessionMeetingURL = function (source) {
            $.ajax({
                type: "POST",
                data: { meetingId: $(source).data("sessionid") },
                url: "/SchoolStructure/SchoolGroups/GenerateZoomMeetingURL",
                success: function (data) {
                    if (data.Success) {
                        initPagination();
                        window.open(data.HeaderText);
                    }
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            })
        },

        initMeetingRegistrationForm = function (source, zoomMeetingId) {
            globalFunctions.loadPopup($(source), "/SchoolStructure/SchoolGroups/InitMeetingRegistrantForm?meetingId=" + zoomMeetingId, translatedResources.addSessionMembers, "modal-lg");
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {

            });
        },

        initPagination = function () {
            $("#LiveSessionPaginationIndex").val('1');
            var type = $("#ddlSessionType").val();
            var schoolGroupId = type == 'N' ? "" : $("#ddlSchoolGroups").val();
            var searchText = $("#search-sessions").val() || "";
            var grid = new DynamicPagination("divSessions");
            var settings = {
                url: '/SchoolStructure/SchoolGroups/GetAllLiveSessions?pageIndex=' + $("#LiveSessionPaginationIndex").val() + "&searchText=" + searchText
                    + "&type=" + type + "&groupId=" + schoolGroupId

            };
            grid.init(settings);
        };

    return {
        init: init,
        initPagination: initPagination,
        generateSessionMeetingURL: generateSessionMeetingURL,
        initMeetingRegistrationForm: initMeetingRegistrationForm
    }
}();

$(function () {
    liveSessionFunctions.init();
})