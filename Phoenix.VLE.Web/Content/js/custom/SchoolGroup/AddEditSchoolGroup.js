﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var popoverEnabler = new LanguageTextEditPopover();
var schoolGroupAddEdit = function () {

    var StudentsToAssign = [];
    var StudentToRemove = [];

    var MemberToAssign = [];
    var MemberToRemove = [];
    var oTableChannel;
    var oTableMember;
    onFailure = function (data) {
        globalFunctions.showMessage("error", data.Message);
    }
    loadStudentsNotInGroup = function () {
        $("#lstStudentsNotInGroup").load("/SchoolStructure/SchoolGroups/GetStudentsNotInGroup?groupId=" + $("#SchoolGroupId").val());
    },

        addStudentsInGroup = function (studentId) {
            
            if ($("#chkStudentToAdd_" + studentId).is(":checked")) {
                StudentsToAssign.push(studentId);
            }
            else {
                var index = StudentsToAssign.indexOf(studentId);
                if (index > -1) {
                    StudentsToAssign.splice(index, 1);
                }
            }
           
            $.post("/SchoolStructure/SchoolGroups/GetStudentinSelectedList", { studentIds: StudentsToAssign }, function (response) {
                $('#lstSelectedStudents').html(response.Result);

            });


        },
        //to add students id from added list
        addStudentIdtoList = function (arrstudentList) {
           
            for (var i = 0; i < arrstudentList.length; i++) {
                StudentsToAssign.push(arrstudentList[i].StudentId);
            }

        },
        //remove student from list
        removeStudentFromSelectedList = function (studentId) {
        
            if ($("#chkSelectedStudent_" + studentId).is(":checked")) {
                var index = StudentsToAssign.indexOf(studentId);
                if (index > -1) {
                    StudentsToAssign.splice(index, 1);
                    StudentToRemove.push(studentId);
                }
                $.post("/SchoolStructure/SchoolGroups/GetStudentinSelectedList", { studentIds: StudentsToAssign }, function (response) {
                    $('#lstSelectedStudents').html(response.Result);

                });
            }
        },
        addMembersInGroup = function (memberId) {
        
            if ($("#chkMemberToAdd_" + memberId).is(":checked")) {
                MemberToAssign.push(memberId);
            }
            else {
                var index = StudentsToAssign.indexOf(memberId);
                if (index > -1) {
                    MemberToAssign.splice(index, 1);
                }

            }
        }
        removeStudentsFromGroup = function (studentId) {
        if ($("#chkStudentToRemove_" + studentId).is(":checked")) {
            StudentToRemove.push(studentId)
        }
        else {
            var index = StudentToRemove.indexOf(studentId);
            if (index > -1) {
                StudentToRemove.splice(index, 1);
            }
        }
    },
        submitGroupDetails = function (e) {

            $("#lstStudentsToAdd").val(JSON.stringify(StudentsToAssign));
            $("#lstStudentsToRemove").val(JSON.stringify(StudentToRemove));
            $("#lstMemebersToAdd").val(JSON.stringify(MemberToAssign));
            $("#lstMemebersToRemove").val(JSON.stringify(MemberToRemove));
            if ($('form#frmAddGroup').valid()) {
                $('form#frmAddGroup').submit();

            }
        },
        selectAllToAdd = function (e) {

            oTableChannel = $("#tblStudents").dataTable();
            if ($(e).is(':checked')) {
                $('.chkSelectToAdd', oTableChannel.fnGetNodes()).prop('checked', true);

                StudentsToAssign = [];
                var table = $("#tblStudents").DataTable();
                rowData = table.rows().data();
                $(rowData).each(function (i, item) {
                    var curRow;
                    curRow = item[0];
                    StudentsToAssign.push(
                        parseInt($(curRow).attr("data-StudentId"))
                    );
                });
            }
            else {
                $('.chkSelectToAdd', oTableChannel.fnGetNodes()).prop('checked', false);

                StudentsToAssign = [];
            }
        },
        selectAllToRemove = function (e) {
            oTableChannel = $("#tblStudentsInGroup").dataTable();
            if ($(e).is(':checked')) {
                $('.chkSelectToRemove', oTableChannel.fnGetNodes()).prop('checked', true);
                StudentToRemove = [];
                var table = $("#tblStudentsInGroup").DataTable();
                rowData = table.rows().data();
                $(rowData).each(function (i, item) {
                    var curRow;
                    curRow = item[0];
                    StudentToRemove.push(
                        parseInt($(curRow).attr("data-StudentId"))
                    );
                });
            }
            else {
                $('.chkSelectToRemove', oTableChannel.fnGetNodes()).prop('checked', false);
                StudentToRemove = [];
            }
        },

        selectMemberAllToAdd = function (e) {

            oTableMember = $("#tblMembers").dataTable();
            if ($(e).is(':checked')) {
                $('.chkSelectToAdd', oTableMember.fnGetNodes()).prop('checked', true);

                MemberToAssign = [];
                var table = $("#tblMembers").DataTable();
                rowData = table.rows().data();
                $(rowData).each(function (i, item) {
                    var curRow;
                    curRow = item[0];
                    MemberToAssign.push(
                        parseInt($(curRow).attr("data-memeberId"))
                    );
                });
            }
            else {
                $('.chkSelectToAdd', oTableMember.fnGetNodes()).prop('checked', false);

                MemberToAssign = [];
            }
        },
        selectAllMemberToRemove = function (e) {
            oTableMember = $("#tblMembers").dataTable();
            if ($(e).is(':checked')) {
                $('.chkSelectToRemove', oTableMember.fnGetNodes()).prop('checked', true);
                MemberToRemove = [];
                var table = $("#tblMembers").DataTable();
                rowData = table.rows().data();
                $(rowData).each(function (i, item) {
                    var curRow;
                    curRow = item[0];
                    MemberToRemove.push(
                        parseInt($(curRow).attr("data-memeberId"))
                    );
                });
            }
            else {
                $('.chkSelectToRemove', oTableMember.fnGetNodes()).prop('checked', false);
                MemberToRemove = [];
            }
        },
        onSaveSuccess = function (data) {

        globalFunctions.showMessage(data.NotificationType, data.Message);
        if (data.Success) {
                $('#SchoolGroupId').val(data.InsertedRowId);
                $('#membersToAdd').removeClass('disabled');
                $('#pills-SelectedStudent-tab').trigger('click');
                $.get("/SchoolStructure/SchoolGroups/GetMemberPage", { groupId: data.InsertedRowId }, function (response) {
                    $('#selectedStudent').html(response);
                    $('#groupDropdown').hide();
                    $('.selectpicker').selectpicker('refresh');
                });
            }

        };

    return {
        loadStudentsNotInGroup: loadStudentsNotInGroup,
        addStudentsInGroup: addStudentsInGroup,
        addStudentIdtoList: addStudentIdtoList,
        removeStudentFromSelectedList: removeStudentFromSelectedList,
        addMembersInGroup: addMembersInGroup,
        removeStudentsFromGroup: removeStudentsFromGroup,
        submitGroupDetails: submitGroupDetails,
        selectAllToAdd: selectAllToAdd,
        selectAllToRemove: selectAllToRemove,
        selectMemberAllToAdd: selectMemberAllToAdd,
        selectAllMemberToRemove: selectAllMemberToRemove,
        onSaveSuccess: onSaveSuccess
    };
}();

$(document).ready(function () {
   
    var groupid = $("#SchoolGroupId").val();
    popoverEnabler.attachPopover();
    $("#btnSaveGroup").click(function (e) {
        e.preventDefault();
        schoolGroupAddEdit.submitGroupDetails();
    });
   
    $('#memberToDisplay').on('change', function (event) {
      
        var memberType = $(this).val();
        if (memberType == "Student")
            $.get("/SchoolStructure/SchoolGroups/GetMembersByType", { memberType: memberType }, function (response) {
            $('#DelegateList').html(response);

            });
        //if (memberType == "OtherGroups") {

        //}
        ConvertCase();
    }); 

    $('#pills-SelectedStudent-tab').on('click', function (event) {
        var groupId = $('#SchoolGroupId').val();
        $.get("/SchoolStructure/SchoolGroups/GetMemberPage", { groupId: groupId }, function (response) {
            $('#selectedStudent').html(response);
            $('#groupDropdown').hide();
            $('.selectpicker').selectpicker('refresh');
        });
    });
  //  globalFunctions.enableCascadedDropdownList("SubjectId", "DDLSchoolGroupId", '/SchoolGroup/SchoolGroup/GetSubjectSchoolGroups');
}); 