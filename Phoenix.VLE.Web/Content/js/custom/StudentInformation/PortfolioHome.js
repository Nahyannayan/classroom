﻿var videoIndex = 0, videoCount = 0, dataSources = [];
var portfolioHomeFunctions = function () {
    var getSelectedUserId = function () {
        return $("#StudentPortolioUserId").val();
    }
    updateActiveNavlink = function (el) {
        $("div.portfolio-nav a").removeClass("active");
        if ($(el).is('.portfolio-nav-actionlinks'))
            $("div.portfolio-nav a[data-target=" + $(el).data("target") + "]").addClass("active");
        else
            $(el).addClass("active");
    },

        updateStudentDescription = function (source) {
            globalFunctions.loadPopup($(source), "/StudentInformation/StudentInformation/UpdateStudentDescription?userId=" + getSelectedUserId(), translatedResources.editDescription)
        },

        initBadgesForm = function (source) {
            globalFunctions.loadPopup($(this), "/StudentInformation/StudentInformation/InitStudentBadgesForm?userId=" + getSelectedUserId(), translatedResources.addNewBadges);
        },

        saveStudentBadges = function () {
            var badgeIds = [];
            $(".divStudentBadge[data-schoolbadgeid].active").each(function () {
                badgeIds.push($(this).data("schoolbadgeid"));
            });

            $.ajax({
                url: "/StudentInformation/StudentInformation/SaveStudentBadges",
                data: { badgeIds: badgeIds.join(','), userId: getSelectedUserId() },
                type: "post",
                success: function (data) {
                    if (data.Success) {
                        $("#myModal").modal("hide");
                        refreshStudentBadges();
                    }
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            });
        },

        saveStudentDescription = function () {
            var desc = $("form#frmStudentDescription #Description").val().trim();
            if (!globalFunctions.isValueValid(desc)) {
                $("form#frmStudentDescription #Description").val('');
            }
            $("form#frmStudentDescription").submit();
        },

        onStudentDataSaveSuccess = function (data) {
            if (data.data.Success) {
                $("#student-description").html(data.description);
                $("#myModal").modal("hide");
            }
        },

        refreshStudentBadges = function () {
            $.get("/StudentInformation/StudentInformation/GetStudentBadges?userId=" + getSelectedUserId(), {}, function (result) {
                $("#divStdBadges").html(result);
                initHomePagePlugins();
            });
        },

        initHomePagePlugins = function () {
            var sync1 = $("#sync1");
            $('.smiley').on('click', function () {
                $('.smiley.active').removeClass('active');
                $(this).addClass('active');
            });
            $(".event-data").mCustomScrollbar({
                setHeight: "250",
            });
            $(".about-me").mCustomScrollbar({
                setHeight: "260",
                autoHideScrollbar: true,
                autoExpandScrollbar: true
            });
            $(".skill-list").mCustomScrollbar({
                autoHideScrollbar: true,
                autoExpandScrollbar: true
            })
            $('#cerficateGallery').lightGallery({
                selector: '.galleryImg'
            });
            $("#certificate-list").owlCarousel({
                margin: 15,
                dots: false,
                autoplay: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                }
            });

            $(".about-me").mCustomScrollbar({
                setHeight: "260",
                autoHideScrollbar: true,
                autoExpandScrollbar: true
            });

            $('[data-toggle="popover-hover"]').popover({
                html: true,
                trigger: 'hover',
                sanitizeFn: function (content) { return content; },
                placement: 'right',
                container: '#badge-popover',
                content: function () { return '<img src="' + $(this).data('img') + '" />'; }
            });

            sync1.owlCarousel({
                items: 1,
                slideSpeed: 2000,
                nav: true,
                autoplay: false,
                dots: false,
                loop: false,
                responsiveRefreshRate: 200,
                margin: 0,
                autoHeight: true,
                navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #fff;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #fff;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
            }).on('changed.owl.carousel', function () {
                $("#sync1 .owl-item video").trigger('pause');
                setTimeout(function () {
                    $("#sync1 .owl-item.active video").trigger('play');
                }, 500)
            });
        };

    return {
        getSelectedUserId: getSelectedUserId,
        updateActiveNavlink: updateActiveNavlink,
        initHomePagePlugins: initHomePagePlugins,
        initBadgesForm: initBadgesForm,
        saveStudentBadges: saveStudentBadges,
        updateStudentDescription: updateStudentDescription,
        onStudentDataSaveSuccess: onStudentDataSaveSuccess,
        saveStudentDescription: saveStudentDescription
    }
}();

$(function () {
    portfolioHomeFunctions.initHomePagePlugins();

    $(document).on('click', 'div.portfolio-nav a, a.portfolio-nav-actionlinks', function () {
        if ($(this).data("url") !== $("div.portfolio-nav a.active").data("url")) {
            $("#portfolioTabContent").load($(this).data("url") + "?userId=" + $("#StudentPortolioUserId").val(), function () {
                $("script[src='/pixie/scripts.min.js']").remove();

                if ($(".selectpicker").length > 0)
                    $(".selectpicker").selectpicker();
                if ($(".student-profile-section").length > 0)
                    portfolioHomeFunctions.initHomePagePlugins();
            });
            portfolioHomeFunctions.updateActiveNavlink($(this));
        }
    })

    $(document).on('click', '.divStudentBadge[data-schoolbadgeid]', function () {
        if ($(this).hasClass("active"))
            $(this).removeClass("active");
        else $(this).addClass("active");
    });

});


window.onload = function () {
    $('.loader-wrapper').fadeOut(500, function () { });
}