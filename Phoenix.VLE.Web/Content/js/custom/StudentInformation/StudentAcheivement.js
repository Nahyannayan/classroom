﻿var studentAchievementFunctions = function () {

    var userId = $("#SelectedPortfolioUserId").val();

    var initPagination = function () {
        var grid = new DynamicPagination("divStudentAchievements");
        var searchString = $("#txtSearchAcheivement").val();
        var settings = {
            url: '/StudentInformation/StudentInformation/GetStudentAchievementsWithFiles?&userId=' + userId + "&pageIndex=" + $("#PageIndexNumber").val() + "&searchString=" + (searchString || '')
        }
        grid.init(settings);
    },
        initStudentAchievementPopup = function (source, acheivementId) {
            var headerText = globalFunctions.isValueValid(acheivementId) ? translatedResources.updateAcheivement : translatedResources.addNewAchievement;
            globalFunctions.loadPopup($(source), "/StudentInformation/StudentInformation/InitStudentAchievementFrom?acheivementId=" + acheivementId + "&userId=" + userId, headerText);
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $("#acheivementUserId").val(userId);
                $('.date-picker1').datetimepicker({
                    format: 'DD-MMM-YYYY',
                    maxDate: new Date(),
                });
                $(".selectpicker").selectpicker()
                initFileUpload();
            });

        },

        initFileUpload = function () {
            $("#stdCertificates").fileinput({
                language: translatedResources.locale,
                title: translatedResources.browseFile,
                theme: "fas",
                showRemove: false,
                showUpload: false,
                showCaption: false,
                showMultipleNames: true,
                minFileCount: 1,
                maxFileCount: 10,
                uploadUrl: "/StudentInformation/StudentInformation/SaveStudentAchievement",
                uploadExtraData: function () {
                    return {
                        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
                        UserId: userId,
                        Title: $("#Title").val(),
                        EventDate: $("#EventDate").val()
                    };
                },
                fileActionSettings: {
                    showZoom: false,
                    showUpload: false,
                    showDrag: false
                },
                allowedFileExtensions: JSON.parse(allowedFileExtensions),
                uploadAsync: true,
                maxFileSize: translatedResources.fileSizeAllow,
                browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
                removeClass: "btn btn-sm m-0 z-depth-0",
                uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
                cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
                overwriteInitial: false,
                initialPreviewAsData: true,
                initialPreviewConfig: [{
                    frameAttr: {
                        title: 'My Custom Title',
                    }
                }],
                preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
                previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
                previewSettings: fileInputControlSettings.previewSettings,
                previewFileExtSettings: fileInputControlSettings.previewFileExtSettings,
                msgSizeTooLarge: 'Total File size (<b>{size}</b>) exceeds maximum allowed upload size of <b>{maxSize}</b>. Please retry your upload!'
            }).off('filebatchselected').on('filebatchselected', function (event, files) {
                var size = 0;
                $.each(files, function (ind, val) {
                    //console.log($(this).attr('size'));
                    size = size + $(this).attr('size');
                });

                var maxFileSize = $(this).data().fileinput.maxFileSize,
                    msg = $(this).data().fileinput.msgSizeTooLarge,
                    formatSize = (s) => {
                        i = Math.floor(Math.log(s) / Math.log(1024));
                        sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                        out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
                        return out;
                    };

                if ((size / 1024) > maxFileSize) {
                    msg = msg.replace('{name}', 'all files');
                    msg = msg.replace('{size}', formatSize(size));
                    msg = msg.replace('{maxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
                    //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
                    if (files.length > 1) {
                        $(".kv-fileinput-error.file-error-message").html('');
                        $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                        $(".close.kv-error-close").on('click', function (e) {
                            $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                            //$(".fileinput-upload").removeClass('d-none');
                        });
                    }
                    else {
                        $(".kv-fileinput-error.file-error-message").html('');
                        $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                        $(".close.kv-error-close").on('click', function (e) {
                            $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                            //$(".fileinput-upload").removeClass('d-none');
                        });
                    }

                    $(".fileinput-upload").attr('disabled', true);
                }
            }).off('fileuploaded').on("fileuploaded", function (e, data) {
                globalFunctions.showMessage(data.response.NotificationType, data.response.Message);
                if (data.response.Success) {
                    $("#Title, #EventDate").val('');
                    initPagination();
                }


            });
            $(".file-upload-widget").mCustomScrollbar({
                autoHideScrollbar: true,
                autoExpandScrollbar: true
            })
        },

        initStudentCertificateForm = function (source, certificateId) {
            var headerText = globalFunctions.isValueValid(certificateId) ? translatedResources.updateCertificate : translatedResources.addCertificate;
            globalFunctions.loadPopup($(source), "/StudentInformation/StudentInformation/InitCertificateInitForm?certificateId=" + certificateId + "&userId=" + userId, headerText);
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                initFileUpload();
            });
        }

    refreshStudentAchievements = function () {
        $.ajax({
            url: "/StudentInformation/StudentInformation/RefreshStudentAchievements",
            data: { userId: userId },
            success: function (result) {
                $("#divStudentAchievements").html(result);
            },
            error: function () {
                globalFunctions.onFailure();
            }
        })
    },

        saveAchievements = function () {
            let selectedList = [];
            $.each($("input.chkAcheivement"), function () {
                let achievementId = $(this).val();
                selectedList.push({
                    StudentId: userId,
                    ShowOnPortfolio: $(this).is(":checked"),
                    AchievementId: achievementId,
                    UpdatedBy: $("#CurrentSessionId").val(),
                    IsCertificate: $(this).hasClass("chkCertificate")
                });
            });

            $.ajax({
                url: "/StudentInformation/StudentInformation/UpdateAchievementsPortfolioStatus",
                data: { selectedAchievements: JSON.stringify(selectedList ? selectedList : "") },
                type: "Post",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success && globalFunctions.isValueValid(data.HeaderText))
                        location.href = data.HeaderText;
                },
                error: function () {
                    globalFunctions.onFailure();
                    initPagination();
                }
            })
        },


        onSaveSuccess = function (data) {
            initPagination.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                initPagination();
                $("#myModal").modal("hide");
            }
        },

        saveAcheivementInfo = function () {
            if (!$("#formStudentAcheivement").valid()) {
                return false;
            }
            if ($("input[type='file'][name=Certificates]")[0].files.length == 0) {
                globalFunctions.showWarningMessage(translatedResources.selectFile);
                return false;
            }
            var data = new FormData();
            data.append("__RequestVerificationToken", $('input[name=__RequestVerificationToken]').val());
            data.append("UserId", userId);
            data.append("Title", $("#Title").val());
            data.append("EventDate", $("#EventDate").val());
            data.append("AchievementId", $("#AchievementId").val());
            data.append("TeacherId", $("#TeacherId").val());
            $.each($("input[type='file'][name=Certificates]")[0].files, function (i, file) {
                data.append(file.name, file);
            });
            $.ajax({
                type: 'POST',
                url: "/StudentInformation/StudentInformation/SaveStudentAchievement",
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                success: function (result) {
                    globalFunctions.showMessage(result.NotificationType, result.Message);
                    if (result.Success) {
                        $("#myModal").modal("hide");
                        $("#Title, #EventDate").val('');
                        initPagination();
                    }
                },
                error: function () {
                    globalFunctions.onFailure()
                }
            })
        },

        deleteAcheivement = function (source, acheivementId) {
            globalFunctions.notyConfirm($(source));
            $(source).off('okClicked');
            $(source).on("okClicked", function () {
                $.post("/StudentInformation/StudentInformation/DeleteAcheivement", { AchievementId: acheivementId }, function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success)
                        initPagination();
                });
            });
        },

        deleteAcheivementFile = function (source, fileId) {
            globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
            $(source).off('okClicked');
            $(source).on("okClicked", function () {
                $.post("/StudentInformation/StudentInformation/DeleteAcheivementFile", { FileId: fileId, FilePath: $(source).data("filepath") }, function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success)
                        $(source).closest(".divAcheivementImage").remove();
                });
            });
        },

        saveStudentCertificateDetails = function (source) {
            if (!$("form#frmStudentCertificates").valid()) {
                return false;
            }
            if ($("input[type='file'][name=Certificates]")[0].files.length == 0) {
                globalFunctions.showWarningMessage(translatedResources.selectFile);
                return false;
            }
            var data = new FormData();
            data.append("__RequestVerificationToken", $('input[name=__RequestVerificationToken]').val());
            data.append("CertificateDescription", $("#CertificateDescription").val());
            data.append("CertificateId", $("#CertificateId").val());
            data.append("CertificateUserId", userId);
            data.append("CertificateTypeId", 2);
            $.each($("input[type='file'][name=Certificates]")[0].files, function (i, file) {
                data.append(file.name, file);
            });
            $.ajax({
                type: 'POST',
                url: "/StudentInformation/StudentInformation/SaveCertificateData",
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                success: function (result) {
                    globalFunctions.showMessage(result.NotificationType, result.Message);
                    if (result.Success) {
                        $("#myModal").modal("hide");
                        initPagination();
                    }
                },
                error: function () {
                    globalFunctions.onFailure()
                }
            })
        },

        updateAcheivementStatus = function (source, acheivementId, mode) {
            globalFunctions.notyConfirm($(source));
            $(source).off("okClicked");
            $(source).on("okClicked", function () {
                $.post("/StudentInformation/StudentInformation/UpdateAcheivementApprovalStatus", { Mode: mode, AchievementId: acheivementId }, function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success)
                        initPagination();
                });
            });
        },

        addAcheivementFile = function (source, acheivementId) {
            $("#postedFile").data("acheivementid", acheivementId);
            $("#fileUploadModal").modal();
        };

    return {
        initPagination: initPagination,
        initStudentAchievementPopup: initStudentAchievementPopup,
        initFileUpload: initFileUpload,
        saveAchievements: saveAchievements,
        onSaveSuccess: onSaveSuccess,
        saveAcheivementInfo: saveAcheivementInfo,
        deleteAcheivementFile: deleteAcheivementFile,
        addAcheivementFile: addAcheivementFile,
        deleteAcheivement: deleteAcheivement,
        initStudentCertificateForm: initStudentCertificateForm,
        saveStudentCertificateDetails: saveStudentCertificateDetails,
        updateAcheivementStatus: updateAcheivementStatus
    }

}();

$(function () {

    $("#postedFile").fileinput({
        language: translatedResources.languageCode,
        title: translatedResources.browseFiles,
        theme: "fas",
        showRemove: false,
        showUpload: true,
        showCaption: false,
        showMultipleNames: true,
        minFileCount: 1,
        maxFileCount: 1,
        uploadUrl: "/StudentInformation/StudentInformation/InsertAchievementFile",
        uploadExtraData: function () {
            return {
                AchievementId: $("#postedFile").data("acheivementid"),
                UserId: $("#SelectedPortfolioUserId").val()
            };
        },
        fileActionSettings: {
            showZoom: false,
            showUpload: false,
            showDrag: false
        },
        allowedFileExtensions: JSON.parse(allowedFileExtensions),
        uploadAsync: false,
        maxFileSize: translatedResources.fileSizeAllow,
        browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
        removeClass: "btn btn-sm m-0 z-depth-0",
        uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
        cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
        overwriteInitial: false,
        initialPreviewAsData: true, // defaults markup
        initialPreviewConfig: [{
            frameAttr: {
                title: 'My Custom Title',
            }
        }],

        preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
        previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
        previewSettings: fileInputControlSettings.previewSettings,
        previewFileExtSettings: fileInputControlSettings.previewFileExtSettings,
        msgSizeTooLarge: 'Total File size (<b>{size}</b>) exceeds maximum allowed upload size of <b>{maxSize}</b>. Please retry your upload!'
}).off('filebatchselected').on('filebatchselected', function (event, files) {
            var size = 0;
            $.each(files, function (ind, val) {
                //console.log($(this).attr('size'));
                size = size + $(this).attr('size');
            });

            var maxFileSize = $(this).data().fileinput.maxFileSize,
                msg = $(this).data().fileinput.msgSizeTooLarge,
                formatSize = (s) => {
                    i = Math.floor(Math.log(s) / Math.log(1024));
                    sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                    out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
                    return out;
                };

            if ((size / 1024) > maxFileSize) {
                msg = msg.replace('{name}', 'all files');
                msg = msg.replace('{size}', formatSize(size));
                msg = msg.replace('{maxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
                //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
                if (files.length > 1) {
                    $(".kv-fileinput-error.file-error-message").html('');
                    $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                    $(".close.kv-error-close").on('click', function (e) {
                        $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                        //$(".fileinput-upload").removeClass('d-none');
                    });
                }
                else {
                    $(".kv-fileinput-error.file-error-message").html('');
                    $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                    $(".close.kv-error-close").on('click', function (e) {
                        $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                        //$(".fileinput-upload").removeClass('d-none');
                    });
                }

                $(".fileinput-upload").attr('disabled', true);
            }
        }).off('filebatchuploadcomplete').on("filebatchuploadcomplete", function () {
        $("#fileUploadModal").modal("hide");
        studentAchievementFunctions.initPagination();
    });

    $("#topPortfolioActions").removeClass("d-none").appendTo($("#breadcrumbActions"));
    studentAchievementFunctions.initPagination();

    $(document).on('keyup', "input.search-field", function (e) {
        e.stopPropagation();
        if (e.keyCode === 13) {
            if ($(e.target).is("#achievementSearch"))
                studentAchievementFunctions.initPagination();
        }
    });

    $(document).on("change", "#ShowAllOnPortfolio", function checkAll(ele) {
        var chkShowAllOnPortfolio = document.getElementById('ShowAllOnPortfolio');
        var chkgoalsCarousel = document.getElementsByTagName('input');
        if (chkShowAllOnPortfolio.checked) {
            for (var i = 0; i < chkgoalsCarousel.length; i++) {
                if (chkgoalsCarousel[i].type == 'checkbox') {
                    chkgoalsCarousel[i].checked = true;
                }
            }
        }
        else {
            for (var i = 0; i < chkgoalsCarousel.length; i++) {
                if (chkgoalsCarousel[i].type == 'checkbox') {
                    chkgoalsCarousel[i].checked = false;
                }
            }
        }
    });

    $(document).on("keyup", "#txtSearchAcheivement", function (e) {
        e.preventDefault();
        if (e.which == 13) {
            studentAchievementFunctions.initPagination();
        }
    })

    $(document).on("DynamicPagination:Complete", function () {
        globalFunctions.enableCheckboxCascade("#ShowAllOnPortfolio", ".chkAcheivement");
        var carouselElements = $("[id^=achievementCarousel_]");
        carouselElements.each(function (idx, item) {
            var isCertificateCarousel = $(item).is("#achievementCarousel_0");
            $(this).owlCarousel({
                margin: 20,
                autoplay: false,
                nav: true,
                dots: false,
                loop: false,
                autoplayHoverPause: true,
                rtl: direction,
                items: 1,
                slideBy: 1,
                responsive: {
                    410: {
                        items: 2,
                        slideBy: 1
                    },
                    767: {
                        items: (idx == 0 ? 3 : 4),
                        slideBy: 1
                    },
                    1024: {
                        items: (idx == 0 ? 4 : 5),
                        slideBy: 1
                    }
                }
            });
        });
    });
});