﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();

var reportAbuse = function () {
    var init = function () {
        loadAbuseReportGrid();
    },
        loadAbuseReportGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Message", "sTitle": translatedResources.contactmessage, "sWidth": "40%", "sClass": "wordbreak" },
                { "mData": "CreatedOn", "sTitle": translatedResources.createdon, "sWidth": "15%", "sClass": "open-details-control wordbreak" },
                //{ "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "10%" }
            );
            initAbuseReportGrid('tbl-AbuseReport', _columnData, '/StudentInformation/ReportAbuse/LoadAbuseReportByUserGrid');
        },

        deleteAbuseReportData = function (source, id) {
            //debugger
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('AbuseReport', '/StudentInformation/ReportAbuse/DeleteAbuseReportData', 'tbl-AbuseReport', source, id);
            }
        },

        initAbuseReportGrid = function (_tableId, _columnData, _url) {
            //debugger
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false
            };
            grid.init(settings);
        };

    onSaveSuccess = function (data) {
        globalFunctions.showMessage(data.NotificationType, data.Message);
    };
    return {
        init: init,
        onSaveSuccess: onSaveSuccess,
        loadAbuseReportGrid: loadAbuseReportGrid,
        deleteAbuseReportData: deleteAbuseReportData
    };
}();

$(document).ready(function () {
    reportAbuse.init();

    $('#tbl-AbuseReport').DataTable().search('').draw();
    $("#AbuseReportListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-AbuseReport').DataTable().search($(this).val()).draw();
    });
});
