﻿var SkillEndorsementUpdateModes = {
    Rating: 'R',
    DashboardStatus: 'D'
}
var studentInformationFunctions = function () {
    var userId = $("#StudentPortolioUserId").val();
    initSchoolBadgesForm = function (source) {
        globalFunctions.loadPopup($(this), "/StudentInformation/StudentInformation/InitGroupWiseStudentBadgesForm", translatedResources.addNewBadges);
    },

        updateSkillRatingAndStatus = function (endorsedSkillId, mode, rating) {
            var obj = {
                StudentUserId: userId,
                SkillEndorsedId: endorsedSkillId,
                Rating: rating,
                Mode: mode
            };
            $.ajax({
                type: "post",
                data: obj,
                url: "/StudentInformation/StudentInformation/UpdateSkillRatingAndDashboardStatus",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success && mode == SkillEndorsementUpdateModes.Rating) {
                        $("ul#stars[data-skillendorsed=" + endorsedSkillId + "] li.star").removeClass("selected");
                        var element = $("ul#stars[data-skillendorsed=" + endorsedSkillId + "] li.star[data-value=" + rating + "]").addClass("selected");
                        $(element).prevAll().addClass("selected").end().nextAll().removeClass("selected");
                    }
                },
                error: function () { globalFunctions.onFailure(); }
            })
        },

        updateSkillRating = function (element) {
            updateSkillRatingAndStatus($(element).closest('ul#stars').data("skillendorsed"), SkillEndorsementUpdateModes.Rating, $(element).data("value"));
        },

        saveGroupWiseStudentBadges = function () {

            var badgeIds = [];
            var schoolGroupIds = $("#SchoolGroupId").val();

            $(".divStudentBadge[data-schoolbadgeid].active").each(function () {
                badgeIds.push($(this).data("schoolbadgeid"));
            });

            $.ajax({
                url: "/StudentInformation/StudentInformation/SaveGroupWiseStudentBadges",
                data: { schoolGroupIds: schoolGroupIds, badgeIds: badgeIds.join(',') },
                type: "post",
                success: function (data) {
                    if (data.Success) {
                        $("#myModal").modal("hide");
                        //refreshStudentBadges();
                    }
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            });


        }

    return {
        initSchoolBadgesForm: initSchoolBadgesForm,
        saveGroupWiseStudentBadges: saveGroupWiseStudentBadges,
        updateSkillRating: updateSkillRating
    }
}();


$(function () {
    $(".group-filter .filter-item-list").mCustomScrollbar({
        autoExpandScrollbar: true,
        scrollbarPosition: "outside"
    });
    $("#topPortfolioActions").removeClass("d-none").appendTo($("#breadcrumbActions"));
    $(".breadcrumbs li.back a").removeAttr("onclick");
    $(".selectpicker").selectpicker({ size: 10 });
    $("#topPortfolioActions button:button.bs-select-all").text(translatedResources.ShowAll)
    $("#topPortfolioActions button:button.bs-deselect-all").text(translatedResources.HideAll)

    if ($("#divStudentList").length) {
        $('#AjaxLoader').fadeIn(500, function () { });
        initStudentSearch();

    }

    $(".border-frame").mCustomScrollbar({
        autoExpandScrollbar: true,
        autoHideScrollbar: true,
        scrollbarPosition: "outside"
    })

    $(document).on('click', '#btnStudentSearch', initStudentSearch);

    $(document).on("keypress", function (e) {
        e.stopPropagation();
        var key = e.which;
        if (key == 13) {
            $("#PageIndexNumber").val('1');
            initStudentSearch();
        }
    });

    $(document).on('click', '#btnInitSchoolBadgeForm', function () {
        var schoolGroupIds = $("#SchoolGroupId").val();

        if (!globalFunctions.isValueValid(schoolGroupIds)) {
            globalFunctions.showMessage("warning", translatedResources.schoolGroupSelectWarning);
        }
        else {
            studentInformationFunctions.initSchoolBadgesForm();
        }
    });

    $(document).on("click", ".breadcrumbs li.back a", function () {
        var userType = $("#UserTypeId").val();
        //$("#searchUsers").val('');
        //initStudentSearch();
        var path = window.location.pathname;
        if (path.toLowerCase() == "/studentinformation/studentinformation")
            window.location.href = "/home"
        if (path.toLowerCase().indexOf("portfoliohome") !== -1)
            window.location.href = userType == 3 ? "/studentinformation/studentinformation" : "/home";
        
    });

    $(document).on('click', '#btnAddBadges', function () {

        var badgeIds = [];

        $(".divStudentBadge[data-schoolbadgeid].active").each(function () {
            badgeIds.push($(this).data("schoolbadgeid"));
        });
        if (badgeIds.length == 0) {
            globalFunctions.showWarningMessage(translatedResources.studentBadgeSelectWarning);
        }
        else {
            globalFunctions.notyConfirm($(this), translatedResources.badgeAssignConfirmMsg);

            $(document).bind('okClicked', $(this), function (e) {
                studentInformationFunctions.saveGroupWiseStudentBadges();
            });
        }

    });

    $(document).on("change", "#SelectPortfolioModules", function () {
        var portfolioSections = $(this).val();
        $.post("/StudentInformation/StudentInformation/UpdateStudentPortfolioSectionDetails", { UserId: $("#StudentPortolioUserId").val(), SectionIds: globalFunctions.isValueValid(portfolioSections) ? portfolioSections.join(",") : "" }, function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success)
                location.reload();
        });

    });

    $(document).on('click', '.divStudentBadge[data-schoolbadgeid]', function () {
        if ($(this).hasClass("active"))
            $(this).removeClass("active");
        else $(this).addClass("active");
    });
    $('.selectpicker').selectpicker('refresh');

    $(".search-text-box").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        var index = $(this).attr("data-index");
        $(".grpSearchList" + index).filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
        if ($(".grpSearchList" + index + ":visible").length === 0) {
            $("#grpSearchNoRecord" + index).addClass("no-data-found text-bold d-block mx-auto my-5").show();
        }
        else {
            $("#grpSearchNoRecord" + index).removeClass("no-data-found text-bold d-block mx-auto my-5").hide();
        }
    });

    $(document).on("click", "button.close-icon", function () {
        var element = $(this).prev('.search-text-box');
        element.val('');
        element.trigger('keyup');
    });

    $(document).on("change", "#sortByContent", function () {
        searchStudByGroup($(this).val());
    });

    $("input[name='groupFilter']").on('change', function (event) {
        searchStudByGroup();
    });

    $(document).on("click", "#selectedFilterList .btn-close", function () {
        var chkbxId = $(this).attr('data-id');
        $("#" + chkbxId).prop("checked", false);
        searchStudByGroup();
    });

    $(document).on("click", "#action-print-portfolio", function () {
        $("#portfolioTabContent").print({
            noPrintSelector: '.no-print'
        });
    });

    //search student by groups
    function searchStudByGroup() {
        let selectedGroupList = [];
        $.each($("input[name='groupFilter']:checked"), function () {
            isGroupSelected = true;
            let groupId = $(this).val();
            let groupName = $(this).attr('data-groupname');
            selectedGroupList.push({
                SchoolGroupId: groupId,
                SchoolGroupName: groupName
            });
        });
        initStudentSearch(selectedGroupList);
    }

    var sources = $("[id^=acheivementCarousel_],#academicsAssignmentCarousel, #academicsReportsCarousel");
    sources.each(function () {
        $(this).owlCarousel({
            margin: 20,
            autoplay: false,
            nav: true,
            dots: false,
            loop: false,
            autoplayHoverPause: true,
            rtl: direction,
            items: 1,
            slideBy: 1,
            responsive: {
                410: {
                    items: 2,
                    slideBy: 1
                },
                767: {
                    items: 4,
                    slideBy: 1
                },
                1024: {
                    items: 5,
                    slideBy: 1
                }
            }
        });
    });


    $("#goalsCarousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive: {
            550: {
                items: 2,
                slideBy: 1
            },
            1024: {
                items: 3,
                slideBy: 1
            }
        }
    });
});


function initStudentSearch(selectedGroupList) {
    var isGroupSelected = false;
    selectedGroupList = globalFunctions.isValueValid(selectedGroupList) ? selectedGroupList : "";
    if (selectedGroupList.length > 0)
        if (globalFunctions.isValueValid(selectedGroupList)) {
            isGroupSelected = true;
            selectedGroupList = selectedGroupList;
        }
        else {
            selectedGroupList = "";
        }

    let sortBy = $("#sortByContent").val();
    var grid = new DynamicPagination("divStudentList");
    var settings = {
        url: '/StudentInformation/StudentInformation/GetStudentBySchoolGroup?pageIndex=' + $("#PageIndexNumber").val() + "&isGroupSelected=" + isGroupSelected + "&selectedGroupData=" + JSON.stringify(selectedGroupList ? selectedGroupList : "") + "&searchString=" + $("#searchUsers").val() + "&sortBy=" + (globalFunctions.isValueValid(sortBy) ? sortBy : "")
    };
    grid.init(settings);
}

function RequestToPdfView(ACD_ID, rpF_ID, StudentNumber) {
    $.ajax({
        url: '/ParentCorner/AssessmentReport/RequestToPdfView',
        type: "GET",
        data: { ACD_ID: ACD_ID, rpF_ID: rpF_ID, StudentNumber: StudentNumber },
        contentType: "application/json; charset=UTF-8",
        success: function (response) {
            if (response != null) {
                var url = response.androidURL;
                window.open(url, '_blank');
            } else {
                alert("something is wrong");
            }
        }
    });
}

function viewStudentAssignmentFiles(source) {
    globalFunctions.loadPopup($(source), "/StudentInformation/StudentInformation/ViewAssignmentFiles?assignmentId=" + $(source).data("assignmentid") + "&userId=" + $("#StudentPortolioUserId").val(), "Assignment Files", "modal-lg");
    $(source).off("modalLoaded");
    $(source).on("modalLoaded", function () {
    })
}