﻿var transactionModes = {
    Insert: 1,
    Update: 2,
    Delete: 3
}

var studentSkillsFunction = function () {
    var updateStudentSkill = function (el) {
        var skillId = $(el).data("skillid"), studentId = $(el).data("studentid"), mode = !$(el).hasClass("active") ? transactionModes.Insert : transactionModes.Delete
        $.ajax({
            url: '/StudentInformation/StudentInformation/UpdateStudentSkillsData',
            data: { studentId: studentId, skillId: skillId, mode: mode },
            type: "POST",
            success: function (data) {
                if (data.Success)
                    $(el).toggleClass("active");
                globalFunctions.showMessage(data.NotificationType, data.Message);
            },
            error: function () {
                globalFunctions.onFailure();
            }
        })
    },

        initStudentSkillsPopup = function (source) {
            globalFunctions.loadPopup($(source), '/StudentInformation/StudentInformation/InitStudentSkillForm?userId=' + portfolioHomeFunctions.getSelectedUserId(), translatedResources.addSkill);
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                if (globalFunctions.isValueValid($(".addSkillInput").val().trim()))
                    $("#SkillName").val($(".addSkillInput").val());
            });
        },

        saveNewStudentSkill = function (source) {
            var skillName = $(".addSkillInput").val();
            var data = {
                IsActive: true,
                SkillName: skillName,
                UserId: portfolioHomeFunctions.getSelectedUserId()
            };
            if (globalFunctions.isValueValid(skillName)) {
                $.post("/StudentInformation/StudentInformation/AddEditStudentSkills", data, function (data) {
                    onSkillSaveSuccess(data);
                    $(".addSkillInput").val('');
                }).fail(function () {
                    globalFunctions.onFailure();
                });
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.pleaseEnterSkill);
            }
        },

        onSkillSaveSuccess = function (data) {
            if (data.Success) {
                refreshStudentSkills();
                $("#myModal").modal("hide");
            }
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },

        refreshStudentSkills = function () {
            $.ajax({
                url: "/StudentInformation/StudentInformation/RefreshStudentSkillsData",
                data: { userId: portfolioHomeFunctions.getSelectedUserId() },
                success: function (result) {
                    $("#divStudentSkillList").html(result);
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            })
        };

    return {
        updateStudentSkill: updateStudentSkill,
        initStudentSkillsPopup: initStudentSkillsPopup,
        saveNewStudentSkill: saveNewStudentSkill,
        onSkillSaveSuccess: onSkillSaveSuccess
    }
}();



$(function () {
    $("#searchContent").on("keyup", function () {
        var value = $(this).val().toLowerCase();

        $(".search-item").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $(document).off("click", ".skills-container a.skill");
    $(document).on("click", '.skills-container a.skill', function () {
        studentSkillsFunction.updateStudentSkill($(this));
    });

   
});
