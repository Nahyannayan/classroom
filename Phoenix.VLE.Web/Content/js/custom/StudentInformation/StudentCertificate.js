﻿var pixie;
var studentCertificateFunctions = function () {
    var initFileUpload = function () {
        $("#stdCertificates").fileinput({
            language: translatedResources.locale,
            title: translatedResources.browseFile,
            theme: "fas",
            showUpload: true,
            showCaption: false,
            showMultipleNames: true,
            minFileCount: 1,
            maxFileCount: 10,
            uploadUrl: "/StudentInformation/StudentInformation/SaveCertificateData",
            uploadExtraData: function () {
                return {
                    __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
                    CertificateTypeId: $("#CertificateTypeId").val(),
                    CertificateUserId: portfolioHomeFunctions.getSelectedUserId(),
                    CertificateDescription: $("#CertificateTitle").val()
                };
            },
            fileActionSettings: {
                showZoom: false,
                showUpload: false,
                //indicatorNew: "",
                showDrag: false
            },
            allowedFileExtensions: JSON.parse(allowedFileExtensions),
            uploadAsync: true,
            maxFileSize: translatedResources.fileSizeAllow,
            //maxFileSize: 15000,//for test
            browseClass: "btn btn-sm m-0 z-depth-0 btn-browse",
            removeClass: "btn btn-sm m-0 z-depth-0",
            uploadClass: "btn btn-sm m-0 z-depth-0",
            overwriteInitial: false,
            //previewFileIcon: '<i class="fas fa-file"></i>',
            initialPreviewAsData: true, // defaults markup
            initialPreviewConfig: [{
                frameAttr: {
                    title: "CustomTitle",
                }
            }],
            preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
            previewFileIconSettings: { // configure your icon file extensions
                'doc': '<i class="fas fa-file-word ic-file-word"></i>',
                'xls': '<i class="fas fa-file-excel ic-file-excel"></i>',
                'ppt': '<i class="fas fa-file-powerpoint ic-file-ppt"></i>',
                'pdf': '<i class="fas fa-file-pdf ic-file-pdf"></i>',
                'zip': '<i class="fas fa-file-archive ic-file"></i>',
                'htm': '<i class="fas fa-file-code ic-file"></i>',
                'txt': '<i class="fas fa-file-alt ic-file"></i>',
                'mov': '<i class="fas fa-file-video ic-file"></i>',
                'mp3': '<i class="fas fa-file-audio ic-file"></i>',
                // note for these file types below no extension determination logic
                // has been configured (the keys itself will be used as extensions)
                'jpg': '<i class="fas fa-file-image ic-file-img"></i>',
                'jpeg': '<i class="fas fa-file-image ic-file-img"></i>',
                'gif': '<i class="fas fa-file-image ic-file-img"></i>',
                'png': '<i class="fas fa-file-image ic-file-img"></i>'
            },
            previewSettings: {
                image: { width: "50px", height: "auto" },
                html: { width: "50px", height: "auto" },
                other: { width: "50px", height: "auto" }
            },
            previewFileExtSettings: { // configure the logic for determining icon file extensions
                'doc': function (ext) {
                    return ext.match(/(doc|docx)$/i);
                },
                'xls': function (ext) {
                    return ext.match(/(xls|xlsx)$/i);
                },
                'ppt': function (ext) {
                    return ext.match(/(ppt|pptx)$/i);
                },
                'zip': function (ext) {
                    return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
                },
                'htm': function (ext) {
                    return ext.match(/(htm|html)$/i);
                },
                'txt': function (ext) {
                    return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
                },
                'mov': function (ext) {
                    return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
                },
                'mp3': function (ext) {
                    return ext.match(/(mp3|wav)$/i);
                }
            },
            msgSizeTooLarge: 'Total File size (<b>{size}</b>) exceeds maximum allowed upload size of <b>{maxSize}</b>. Please retry your upload!'
        }).off('filebatchselected').on('filebatchselected', function (event, files) {
            var size = 0;
            $.each(files, function (ind, val) {
                //console.log($(this).attr('size'));
                size = size + $(this).attr('size');
            });

            var maxFileSize = $(this).data().fileinput.maxFileSize,
                msg = $(this).data().fileinput.msgSizeTooLarge,
                formatSize = (s) => {
                    i = Math.floor(Math.log(s) / Math.log(1024));
                    sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                    out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
                    return out;
                };

            if ((size / 1024) > maxFileSize) {
                msg = msg.replace('{name}', 'all files');
                msg = msg.replace('{size}', formatSize(size));
                msg = msg.replace('{maxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
                //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
                if (files.length > 1) {
                    $(".kv-fileinput-error.file-error-message").html('');
                    $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                    $(".close.kv-error-close").on('click', function (e) {
                        $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                        //$(".fileinput-upload").removeClass('d-none');
                    });
                }
                else {
                    $(".kv-fileinput-error.file-error-message").html('');
                    $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                    $(".close.kv-error-close").on('click', function (e) {
                        $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                        //$(".fileinput-upload").removeClass('d-none');
                    });
                }

                $(".fileinput-upload").attr('disabled', true);
            }
        }).off('fileuploaded').on("fileuploaded", function (e, data) {
            globalFunctions.showMessage(data.response.NotificationType, data.response.Message);
            if (data.response.Success) {
                $("#CertificateTitle").val('');
                $("button.fileinput-remove.fileinput-remove-button").click();
                $('.file-upload-widget').animate({
                    right: '-300'
                });
                refreshStudentCertifcates();
            }


        });
        $(".file-upload-widget").mCustomScrollbar({
            autoHideScrollbar: true,
            autoExpandScrollbar: true
        })
    },

        deleteUserCertificate = function (source, certificateId) {
            globalFunctions.notyDeleteConfirm($(source), translatedResources.deleteConfirm);
            $(source).off("okToDelete");
            $(source).on("okToDelete", function () {
                var obj = {
                    UserId: $("#SelectedPortfolioUserId").val(),
                    CertificateId: certificateId
                }
                $.ajax({
                    type: "POST",
                    url: "/StudentInformation/StudentInformation/DeleteUserCertificate",
                    data: obj,
                    success: function (data) {
                        if (data.Success)
                            studentAchievementFunctions.initPagination();
                        globalFunctions.showMessage(data.NotificationType, data.Message);
                    },
                    error: function () {
                        globalFunctions.onFailure();
                    }
                });
            })
        },

        updateUserCertificateStatus = function (source, certificateId) {
            var message = $(source).data("isselected").toBoolean() ? translatedResources.hideCertificateMessage : translatedResources.showCertificateMessage;
            globalFunctions.notyDeleteConfirm($(source), message);
            $(source).off("okToDelete");
            $(source).on("okToDelete", function () {
                var obj = {
                    UserId: portfolioHomeFunctions.getSelectedUserId(),
                    CertificateId: certificateId
                }
                $.ajax({
                    type: "POST",
                    url: "/StudentInformation/StudentInformation/UpdateCertificateStatus",
                    data: obj,
                    success: function (data) {
                        if (data.Success)
                            refreshStudentCertifcates();
                        globalFunctions.showMessage(data.NotificationType, data.Message);
                    },
                    error: function () {
                        globalFunctions.onFailure();
                    }
                });
            })
        },

        approveStudentCertificate = function (source, certificateId) {
            globalFunctions.notyConfirm($(source), translatedResources.approveCertificateMessage);
            $(source).off("okClicked");
            $(source).on("okClicked", function () {
                var obj = {
                    UserId: $("#SelectedPortfolioUserId").val(),
                    CertificateId: certificateId
                }
                $.ajax({
                    type: "POST",
                    url: "/StudentInformation/StudentInformation/ApproveCertificateStatus",
                    data: obj,
                    success: function (data) {
                        globalFunctions.showMessage(data.NotificationType, data.Message);
                        if (data.Success)
                            studentAchievementFunctions.initPagination();
                    },
                    error: function () {
                        globalFunctions.onFailure();
                    }
                });
            });
        },

        refreshStudentCertifcates = function () {
            $.ajax({
                url: "/StudentInformation/StudentInformation/RefreshStudentCertificates",
                data: { userId: portfolioHomeFunctions.getSelectedUserId() },
                success: function (result) {
                    $("#divStudentCertificates").html(result);
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            })
        },

        viewCertificate = function (source, templateId) {
            $(".modal-dialog").addClass("modal-lg");
            globalFunctions.loadPopup($(source), "/StudentInformation/StudentInformation/ViewStudentCertificate?templateId=" + templateId + "&userId=" + $("#SelectedPortfolioUserId").val(), translatedResources.viewCertificate, "modal-lg")
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {

            })
        },

        initCertificateEditor = function () {
            console.log(imagePath);
            pixie = new Pixie({
                ui: {
                    visible: true,
                    theme: 'light',
                    allowEditorClose: false,
                    allowZoom: false,
                    toolbar: {
                        hideSaveButton: false,
                        hide: false,
                        hideOpenButton: true,
                        hideCloseButton: true,
                    },
                    nav: {
                        position: 'none',
                        replaceDefault: true,
                    },
                },
                baseUrl: baseUri,
                onLoad: function () {
                    pixie.loadState(state).then(function () {
                        $(".editor-overlay-container.cdk-overlay-container").remove();
                        $(".mat-focus-indicator.mat-menu-trigger.mat-button.mat-button-base.ng-star-inserted").remove();
                    });
                    $("div.canvas-mask-wrapper").css("pointer-events", "none");
                    $(".right.ng-star-inserted").empty();
                },
                onSave: function (data, name) {
                    var formData = new FormData();
                    formData.append("CertificateTitle", $("#StudentCertificateTitle").val());
                    formData.append("CertificateTypeId", "2");
                    formData.append("TemplateId", $("#hdnStudentTemplateId").val());
                    formData.append("FileName", name);
                    formData.append("UserId", $("#SelectedPortfolioUserId").val());
                    var certificateFile = globalFunctions.convertBase64ToImageFile(data, name);
                    formData.append("postedFile", certificateFile);

                    $.ajax({
                        type: 'POST',
                        url: "/StudentInformation/StudentInformation/SaveTeacherAssignedCertificate",
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: formData,
                        success: function (data) {
                            if (data.Success) {
                                globalFunctions.showMessage(data.NotificationType, data.Message);
                                $("#myModal").modal("hide");
                                location.reload();
                            }
                        },
                        error: function () {
                            globalFunctions.onFailure();
                        }
                    });
                },
                tools: {
                    export: {
                        defaultFormat: 'jpeg', //png, jpeg or json
                        defaultName: fileName, //default name for downloaded photo file
                        defaultQuality: 0.1, //works with jpeg only, 0 to 1
                    }
                }
            });
        };

    return {
        initFileUpload: initFileUpload,
        deleteUserCertificate: deleteUserCertificate,
        updateUserCertificateStatus: updateUserCertificateStatus,
        approveStudentCertificate: approveStudentCertificate,
        viewCertificate: viewCertificate,
        refreshStudentCertifcates: refreshStudentCertifcates,
        initCertificateEditor: initCertificateEditor
    }

}();

$(function () {

})