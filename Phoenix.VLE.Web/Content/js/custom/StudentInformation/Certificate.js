﻿var TemplateStatusTypes = {
    New: 0,
    Approved: 1,
    Pending: 2,
    Rejected: 3
};

var removedStudents = [], pixie;
var certificate = function () {

    var init = function () {
        loadTemplateGrid();
    },
        loadTemplateGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Title", "sTitle": translatedResources.title, "sWidth": "20%" },
                { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "40%" },
                { "mData": "Status", "sClass": "text-center no-sort", "sTitle": translatedResources.status, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-left no-sort", "bSorting": false, "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initTemplateGrid('tblTeacherTemplates', _columnData, '/Document/Certificate/LoadCertificateGrid');
        },

        initTemplateGrid = function (_tableId, _columnData, _url) {

            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [3]
                }],
                searching: true
            };
            grid.init(settings);

            $("#tblTeacherTemplates_filter").hide();
            $('#tblTeacherTemplates').DataTable().search('').draw();
            $("#searchTeacherTemplate").on("input", function (e) {
                e.preventDefault();
                $('#tblTeacherTemplates').DataTable().search($(this).val()).draw();
            });
        },

        assignStudents = function (source, templateId) {
            globalFunctions.loadPopup($(source), "/Document/Certificate/AssignStudents?templateId=" + templateId, translatedResources.assignStudentsMsg);
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $(".modal-dialog").addClass("modal-lg");
                $(".selectpicker").selectpicker({ dropdownAlignRight: true });
                initStudentSearch();
                initTeacherGroupPagination();
                removedStudents = [];
            });
        },

        saveCertificateStudents = function (source) {
            var studentIds = [];
            var schoolGroupIds = [];
            $.each($(".certificate-student.selected"), function () {
                studentIds.push($(this).data("studentid"));
            });
            $.each($("div.school-group.active"), function () {
                schoolGroupIds.push($(this).data("groupid"));
            })
            var obj = {
                TemplateId: $("#MappingTemplateId").val(),
                StudentIds: studentIds.join(','),
                RemovedStudentIds: removedStudents.join(","),
                SchoolGroupIds: schoolGroupIds.join(',')
            }
            $.post("/Document/Certificate/SaveTemplateStudentMapping", obj, function (data) {
                globalFunctions.showMessage(data.NotificationType, data.Message);

            }).fail(function () {
                globalFunctions.onFailure();
            });
        },

        initTemplateStatusForm = function (source, templateId) {
            globalFunctions.loadPopup($(source), "/Document/Certificate/InitTemplateUpdateStatusForm?templateId=" + templateId, translatedResources.updateTemplateStatus);
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $("select#Status").off("change");
                $("select#Status").on("change", function () {
                    $(".divRejectedComent").toggle($(this).val() == TemplateStatusTypes.Rejected);
                });
                $(".selectpicker").selectpicker();
            });
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal("hide");
                loadTemplateGrid();
            }
        },

        previewCertificate = function (source) {
            window.open("/document/certificate/PreviewCertificateTemplate?id=" + $(source).data("id"));
            // globalFunctions.loadPopup($(source), "/document/certificate/PreviewCertificateTemplate?templateId=" + templateId, translatedResources.tenplatePreview);
        };

    return {
        init: init,
        loadTemplateGrid: loadTemplateGrid,
        assignStudents: assignStudents,
        saveCertificateStudents: saveCertificateStudents,
        initTemplateStatusForm: initTemplateStatusForm,
        onSaveSuccess: onSaveSuccess,
        previewCertificate: previewCertificate
    };
}();


function initTeacherGroupPagination() {
    var grid = new DynamicPagination("groupContainer");
    var settings = {
        url: '/Document/Certificate/GetTeacherGroups?pageIndex=' + $("#GroupsPageIndexNumber").val() + "&searchString=" + $("#txtSchoolGroup").val()
    };
    grid.init(settings);
}

function initStudentSearch() {
    var grid = new DynamicPagination("divStudentList");
    var settings = {
        url: '/Document/Certificate/GetSchoolGroupStudents?pageIndex=' + $("#PageIndexNumber").val() + "&schoolGroupIds=" + $("#SchoolGroupId").val() + "&searchString=" + $("#searchUsers").val()
            + "&templateId=" + $("#MappingTemplateId").val()
    };
    grid.init(settings);
}


$(document).ready(function () {
    certificate.init();

    $(document).on('change', '#SchoolGroupId', function (e) {
        initStudentSearch();
    });

    $("#btn-submit").click(function () {
        if (!$("#frmAddTempalte")[0].checkValidity()) {
            $("#frmAddTempalte")[0].reportValidity();
        }
    });

    $(document).on('click', '.certificate-student', function () {
        var source = $(this);
        $(this).toggleClass("selected");
        if (!source.hasClass("selected") && source.data("added")) {
            removedStudents.push(source.data("studentid"));
        }
        else if (source.hasClass("selected") && source.data("added")) {
            removedStudents = removedStudents.filter(function (item) { return item !== source.data("studentid"); });
        }
    });

    $(document).on("DynamicGrid:InitComplete", "#tblTeacherTemplates", function () {
        $("#tblTeacherTemplates thead").addClass("thead-dark");
    });

    $(document).on("keyup", "input.search-field", function (e) {
        if (!$(e.target).is("#searchTeacherTemplate")) {
            if (e.which == 13) {
                if ($(e.target).is(".group-search"))
                    initTeacherGroupPagination();
                else
                    initStudentSearch();
            }
        }
    });
    $(document).on('click', 'div.school-group', function () {
        $(this).toggleClass("active");
    })
});

