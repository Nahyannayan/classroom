﻿var croppedFileResult, uploadedImageName = "cropped.jpg";
var aboutMeFunctions = new function () {
    openImageUplodWindow = function (source) {
        $("#postedFile").data("uploadtype", $(source).data("uploadtype"));
        $("#fileUploadModal").modal();
    },

        initCoverImageForm = function (source) {
            globalFunctions.loadPopup($(source), "/StudentInformation/StudentInformation/InitStudentCoverForm?userId=" + $("#PortfolioUserId").val(), translatedResources.updateCoverPhoto, "modal-lg");
        },

        saveStudentCoverPhoto = function (source) {
        var imgBase64 = globalFunctions.isValueValid(croppedFileResult) ? croppedFileResult.toDataURL() : '';
            var fname = globalFunctions.isValueValid(imgBase64) ? uploadedImageName : "";
            if (!globalFunctions.isValueValid(fname)) {
                globalFunctions.showWarningMessage(translatedResources.selectFile);
                return false;
            }
            var formData = new FormData();
            var imageFile = globalFunctions.convertBase64ToImageFile(imgBase64, fname);
            formData.append("postedFile", imageFile);
            formData.append("isCoverPhoto", true);
            formData.append("__RequestVerificationToken", $("input[name=__RequestVerificationToken]").val());
            formData.append("userId", $("#PortfolioUserId").val())
            $.ajax({
                url: '/StudentInformation/StudentInformation/SaveStudentProfileImages',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        $("#myModal").modal("hide");
                        $("#imgStudentCover").attr("src", data.HeaderText);
                    }
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure();
                },
            });
        },

        initSchoolBadgesForm = function (source) {
            globalFunctions.loadPopup($(this), "/StudentInformation/StudentInformation/InitStudentBadgesForm?userId=" + $("#PortfolioUserId").val(), translatedResources.addNewBadges);
        },

        refreshStudentBadges = function () {
            $.get("/StudentInformation/StudentInformation/GetStudentBadges?userId=" + $("#PortfolioUserId").val(), {}, function (result) {
                $("#divStdBadges").html(result);
                globalFunctions.enableCheckboxCascade("#chkSelectAllAwards", "[id^=chkAward_]");
                $("#chkSelectAllAwards").prop("checked", $("[id^=chkAward_]:checked").length == $("[id^=chkAward_]").length);
            });
        },

        saveStudentBadges = function () {
            var badgeIds = [];
            $(".divStudentBadge[data-schoolbadgeid].active").each(function () {
                badgeIds.push($(this).data("schoolbadgeid"));
            });

            $.ajax({
                url: "/StudentInformation/StudentInformation/SaveStudentBadges",
                data: { badgeIds: badgeIds.join(','), userId: $("#PortfolioUserId").val() },
                type: "post",
                success: function (data) {
                    if (data.Success) {
                        $("#myModal").modal("hide");
                        refreshStudentBadges();
                    }
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            });
        },

        saveStudentDetails = function (source) {
            var badgesIds = [];
            $("[id^=chkAward_]:checked").each(function () {
                badgesIds.push($(this).data("badgeid"));
            });
            var details = {
                Description: CKEDITOR.instances['StudentDescription'].getData(),
                UserId: $("#PortfolioUserId").val(),
                BadgesIds: badgesIds.join(",")
            };
            $.ajax({
                url: "/StudentInformation/StudentInformation/SaveStudentAboutMe",
                data: details,
                type: "POST",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        location.href = data.HeaderText;
                    }
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            });
        };

    return {
        openImageUplodWindow: openImageUplodWindow,
        saveStudentDetails: saveStudentDetails,
        initCoverImageForm: initCoverImageForm,
        saveStudentCoverPhoto: saveStudentCoverPhoto,
        initSchoolBadgesForm: initSchoolBadgesForm,
        saveStudentBadges: saveStudentBadges
    }
}();

$(function () {
    CKEDITOR.replace("StudentDescription", {
        height: "200px",
        contentsLangDirection: $("html").attr("dir")
    });

    globalFunctions.enableCheckboxCascade("#chkSelectAllAwards", "[id^=chkAward_]");

    $("#postedFile").fileinput({
        language: translatedResources.languageCode,
        title: translatedResources.browseFiles,
        theme: "fas",
        showRemove: false,
        showUpload: true,
        showCaption: false,
        showMultipleNames: true,
        minFileCount: 1,
        maxFileCount: 1,
        uploadUrl: "/StudentInformation/StudentInformation/SaveStudentProfileImages",
        uploadExtraData: function () {
            return {
                __RequestVerificationToken: $("[name='__RequestVerificationToken']").val(),
                isCoverPhoto: $("#postedFile").data("uploadtype") == "cover",
                userId: $("#PortfolioUserId").val()
            };
        },
        fileActionSettings: {
            showZoom: false,
            showUpload: false,
            showDrag: false
        },
        allowedFileExtensions: translatedResources.allowedFileExtensions.split(","),
        uploadAsync: false,
        maxFileSize: translatedResources.fileSizeAllow,
        browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
        removeClass: "btn btn-sm m-0 z-depth-0",
        uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
        cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
        overwriteInitial: false,
        initialPreviewAsData: true, // defaults markup
        initialPreviewConfig: [{
            frameAttr: {
                title: 'My Custom Title',
            }
        }],

        preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
        previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
        previewSettings: fileInputControlSettings.previewSettings,
        previewFileExtSettings: fileInputControlSettings.previewFileExtSettings,
        msgSizeTooLarge: 'Total File size (<b>{size}</b>) exceeds maximum allowed upload size of <b>{maxSize}</b>. Please retry your upload!'
    }).off('filebatchselected').on('filebatchselected', function (event, files) {
        var size = 0;
        $.each(files, function (ind, val) {
            //console.log($(this).attr('size'));
            size = size + $(this).attr('size');
        });

        var maxFileSize = $(this).data().fileinput.maxFileSize,
            msg = $(this).data().fileinput.msgSizeTooLarge,
            formatSize = (s) => {
                i = Math.floor(Math.log(s) / Math.log(1024));
                sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
                return out;
            };

        if ((size / 1024) > maxFileSize) {
            msg = msg.replace('{name}', 'all files');
            msg = msg.replace('{size}', formatSize(size));
            msg = msg.replace('{maxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
            //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
            if (files.length > 1) {
                $(".kv-fileinput-error.file-error-message").html('');
                $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                $(".close.kv-error-close").on('click', function (e) {
                    $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                    //$(".fileinput-upload").removeClass('d-none');
                });
            }
            else {
                $(".kv-fileinput-error.file-error-message").html('');
                $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                $(".close.kv-error-close").on('click', function (e) {
                    $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                    //$(".fileinput-upload").removeClass('d-none');
                });
            }

            $(".fileinput-upload").attr('disabled', true);
        }
    }).off('filebatchuploadcomplete').on('filebatchuploadcomplete', function () {
        $('#postedFileList').fileinput('clear');
        $("#fileUploadModal").modal("hide");
        location.reload();
    });

    $(document).on('click', '.divStudentBadge[data-schoolbadgeid]', function () {
        if ($(this).hasClass("active"))
            $(this).removeClass("active");
        else $(this).addClass("active");
    });
})
