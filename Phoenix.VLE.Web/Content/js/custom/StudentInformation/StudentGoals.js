﻿
//var formElements = new FormElements();
var studentGoalFunctions = function () {
    var userId = $("#SelectedPortfolioUserId").val();

    var initGoalsPagination = function () {
        var grid = new DynamicPagination("divStudentGoals");
        var settings = {
            url: '/StudentInformation/StudentInformation/GetAllStudentGoals?pageIndex=' + $("#PageIndexNumber").val() + "&userId=" + userId
                + "&searchString=" + $("#searchgoals").val()

            //+ "&sortColumn=" + $("th.sorting.active").text().trim()
        };
        grid.init(settings);

    },

        initStudentGoalPopup = function (source) {
           
            var title = translatedResources.addGoal;
            if ($(source).data("goalid") != undefined )
            { title = "Edit Goals"; }
            globalFunctions.loadPopup($(source), "/StudentInformation/StudentInformation/InitStudentGoalForm?goalId=" + $(source).data("goalid") + "&userId=" + userId, title);
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $("#frmStudentGoals #UserId").val(userId);
                $('.date-picker1').datetimepicker({
                    format: 'DD-MMM-YYYY',
                    minDate: new Date(),
                });
                $(".selectpicker").selectpicker();
            })
        },

        toggleDashboardGoals = function (goalId) {
            var obj = {
                UserId: userId,
                GoalId: goalId
            }
            $.ajax({
                url: "/StudentInformation/StudentInformation/UpdateDashboardGoalsStatus",
                data: obj,
                type: "Post",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            })
        },

        saveGoals = function () {
            let selectedGoalList = [];
            $.each($("input[name='goalFilter']"), function () {
                let goalId = $(this).val();
                selectedGoalList.push({
                    UserId: userId,
                    GoalId: goalId,
                    CreatedUserId: $("#CurrentSessionId").val(),
                    ShowOnDashboard: $(this).is(":checked")
                });
            });

            $.ajax({
                url: "/StudentInformation/StudentInformation/UpdateAllDashboardGoalsStatus",
                data: { selectedGoals: JSON.stringify(selectedGoalList ? selectedGoalList : "") },
                type: "Post",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success && globalFunctions.isValueValid(data.HeaderText)) {
                        location.href = data.HeaderText;
                    }
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            })
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                initGoalsPagination();
                $("#myModal").modal("hide");
            }
        },

        deleteStudentGoals = function (source, goalId) {
            globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
            $(source).off('okClicked');
            $(source).on('okClicked', function (e) {
                $.post('/StudentInformation/StudentInformation/DeleteStudentGoals', { goalId: goalId }, function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success)
                        initGoalsPagination();
                });
            });
        },

        approveStudentGoal = function (source, goalId) {
            var obj = {
                UserId: $("#SelectedPortfolioUserId").val(),
                GoalId: goalId
            };
            globalFunctions.notyConfirm($(source), translatedResources.approveGoalMsg);
            $(source).off("okClicked");
            $(source).on("okClicked", function () {
                $.post("/StudentInformation/StudentInformation/ApproveStudentGoal", obj, function (data) {
                    if (data.Success) {
                        initGoalsPagination();
                        globalFunctions.showMessage(data.NotificationType, data.Message);
                    }
                }).fail(function () {
                    globalFunctions.onFailure();
                });
            });

        };

    return {
        initGoalsPagination: initGoalsPagination,
        toggleDashboardGoals: toggleDashboardGoals,
        saveGoals: saveGoals,
        initStudentGoalPopup: initStudentGoalPopup,
        onSaveSuccess: onSaveSuccess,
        approveStudentGoal: approveStudentGoal,
        deleteStudentGoals: deleteStudentGoals
    }
}();

$(function () {
    $("#topPortfolioActions").removeClass("d-none").appendTo($("#breadcrumbActions"));
    studentGoalFunctions.initGoalsPagination();
    $(document).on('keyup', "input.search-field", function (e) {
        e.stopPropagation();
        if (e.keyCode === 13) {
            $("#PageIndexNumber").val('1'); // Reset page to 1 on search
            studentGoalFunctions.initGoalsPagination();
        }
    });

    $(document).on('click', '#btnAssignmentSearch', function () {
        $("#PageIndexNumber").val('1');
        studentGoalFunctions.initGoalsPagination();
    });

    $(document).on("click", "table#tblGoals.dataTable th.sorting", function () {
        $("table#tblGoals.dataTable").removeClass("active");
        $(this).addClass("active");
        $("table#tblGoals.dataTable").find('th:not(.active)').removeClass("sorting_asc sorting_desc");
        $("table#tblGoals.dataTable").find('th').removeClass("active");
        if ($(this).hasClass("sorting_asc"))
            $(this).removeClass('sorting_asc').addClass("sorting_desc")
        else
            $(this).removeClass('sorting_desc').addClass("sorting_asc");

    });

    $(document).on("DynamicPagination:Complete", function () {
        globalFunctions.enableCheckboxCascade("#ShowAllOnPortfolio", "[id^=chkGoal_ ]");
    });

    $("#goalsCarousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        responsive: {
            550: {
                items: 2,
                slideBy: 1
            },
            1024: {
                items: 3,
                slideBy: 1
            }
        }
    });

})