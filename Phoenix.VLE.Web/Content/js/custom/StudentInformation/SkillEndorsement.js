﻿var SkillEndorsementUpdateModes = {
    Rating: 'R',
    DashboardStatus: 'D'
}
var studentEndorsementFunctions = function () {
    var userId = $("#SelectedPortfolioUserId").val();

    var onSaveSuccess = function (data) {
        globalFunctions.showMessage(data.NotificationType, data.Message);
        if (data.Success) {
            initEndorsementPagination();
            $("#myModal").modal("hide");
        }

    },
        initEndorsementPagination = function () {
            var grid = new DynamicPagination("divStudentSkillsAndEndorsements");
            var settings = {
                url: '/StudentInformation/StudentInformation/GetAllEndorsedSkills?pageIndex=' + $("#PageIndexNumber").val() + "&userId=" + userId
                    + "&searchString=" + ($("#searchSkillsCommentsRatings").val() || '')
            };
            grid.init(settings);
        },

        saveSkillsAndEndorsements = function () {
            let selectedSkillsList = [];
            $.each($("input[name='skillFilter']"), function () {
                let SkillEndorsedId = $(this).val();
                selectedSkillsList.push({
                    StudentUserId: userId,
                    SkillEndorsedId: SkillEndorsedId,
                    Mode: SkillEndorsementUpdateModes.DashboardStatus,
                    ShowOnDashboard : $(this).is(":checked")
                });
            });

            $.ajax({
                type: "post",
                data: { selectedSkills: JSON.stringify(selectedSkillsList ? selectedSkillsList : "") },
                url: "/StudentInformation/StudentInformation/UpdateAllEndorsedSkillStatus",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success && globalFunctions.isValueValid(data.HeaderText))
                        location.href = data.HeaderText;
                },
                error: function () { globalFunctions.onFailure(); }
            })
        },

        updateSkillRatingAndStatus = function (endorsedSkillId, mode, rating) {
            var obj = {
                StudentUserId: userId,
                SkillEndorsedId: endorsedSkillId,
                Rating: rating,
                Mode: mode
            };
            $.ajax({
                type: "post",
                data: obj,
                url: "/StudentInformation/StudentInformation/UpdateSkillRatingAndDashboardStatus",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success && mode == SkillEndorsementUpdateModes.Rating) {
                        $("ul#stars[data-skillendorsed=" + endorsedSkillId + "] li.star").removeClass("selected");
                        var element = $("ul#stars[data-skillendorsed=" + endorsedSkillId + "] li.star[data-value=" + rating + "]").addClass("selected");
                        $(element).prevAll().addClass("selected").end().nextAll().removeClass("selected");
                    }
                },
                error: function () { globalFunctions.onFailure(); }
            })
        },

        deleteEndorsementSkill = function (source, skillEndorsementId) {
            globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
            $(source).off('okClicked');
            $(source).on("okClicked", function () {
                $.post("/StudentInformation/StudentInformation/DeleteEndorsedSkill", { SkillEndorsedId: skillEndorsementId }, function () {
                    initEndorsementPagination();
                });
            });
        },

        initSkillEndorsementForm = function (source, endorsementId) {
            var headerText = globalFunctions.isValueValid(endorsementId) ? translatedResources.updateSkillEndorsement : translatedResources.addNewSkillOrEndorsements;
            globalFunctions.loadPopup($(source), "/StudentInformation/StudentInformation/InitSkillEndorsementForm?userId=" + userId + "&skillEndorsementId=" + endorsementId, headerText);
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $("select[name=SkillIds]").val($("#DefaultEndorsedSkillId").val());
                $(".selectpicker").selectpicker();
                $("#StudentUserId").val(userId);
            });
        },

        rejectStudentEndorsement = function (source, endorsementId) {
            updateStudentEndorsementStatus(source, endorsementId, 'R');
        },

        approveStudentEndorsementId = function (source, endorsementId) {
            updateStudentEndorsementStatus(source, endorsementId, 'A', $("#description-area_" + endorsementId).val());
        },

        updateStudentEndorsementStatus = function (source, endorsementId, mode, message) {
            globalFunctions.notyConfirm($(source));
            $(source).off("okClicked");
            $(source).on("okClicked", function () {
                var obj = {
                    Message: message,
                    SkillEndorsedId: endorsementId,
                    Rating: $("#stars[data-skillendorsed=" + endorsementId + "] li.selected").length,
                    Mode: mode
                };

                $.post("/StudentInformation/StudentInformation/UpdateStudentEndorsementStatus", obj, function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success)
                        initEndorsementPagination();
                });
            });
        }

    updateSkillRating = function (element) {
        updateSkillRatingAndStatus($(element).closest('ul#stars').data("skillendorsed"), SkillEndorsementUpdateModes.Rating, $(element).data("value"));
    };


    return {
        onSaveSuccess: onSaveSuccess,
        initEndorsementPagination: initEndorsementPagination,
        updateSkillRating: updateSkillRating,
        saveSkillsAndEndorsements: saveSkillsAndEndorsements,
        deleteEndorsementSkill: deleteEndorsementSkill,
        initSkillEndorsementForm: initSkillEndorsementForm,
        rejectStudentEndorsement: rejectStudentEndorsement,
        approveStudentEndorsementId: approveStudentEndorsementId
    }
}();

$(function () {

    $(document).on("DynamicPagination:Complete", function () {
        globalFunctions.enableCheckboxCascade("#ShowAllOnPortfolio", ".chkSkillEndorsement");
    });

    $("#topPortfolioActions").removeClass("d-none").appendTo($("#breadcrumbActions"));

    studentEndorsementFunctions.initEndorsementPagination();
    $(document).off('keydown', '.bootstrap-select [data-toggle="dropdown"], .bootstrap-select [role="listbox"], .bootstrap-select .bs-searchbox input');
    $(document).on('keyup', "input.endorsement-search-box", function (e) {
        e.stopPropagation();
        if (e.keyCode === 13) {
            $("#PageIndexNumber").val('1'); // Reset page to 1 on search
            studentEndorsementFunctions.initEndorsementPagination();
        }
    });
    $(document).on('click', '#btnEndorsedSkillSearch', studentEndorsementFunctions.initEndorsementPagination);

    $(document).on("change", "input:checkbox#RequestEndorsement", function () {
        $("#divTeacherEndorsementList").toggle($(this).is(":checked"));
    });

});