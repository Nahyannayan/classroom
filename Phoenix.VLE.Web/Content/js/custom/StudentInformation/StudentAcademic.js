﻿var FileUploadTypes = {
    Videos: 'V',
    Others: 'O'
}

var studentAcademicFunctions = function () {
    var userId = $("#SelectedPortfolioUserId").val();
    updateAllAssignmentStatus = function () {
        let selectedAssignmentList = [];
        $.each($("input[name='assignmentFilter']"), function () {
            let assignmentId = $(this).val();
            selectedAssignmentList.push({
                AssignmentId: assignmentId,
                UserId: userId,
                ShowOnDashboard: $(this).is(":checked")
            });
        });

        var reports = [];
        $("[id^=chkReport_]").each(function () {
            var reportData = $(this).data("report");
            reportData.ShowOnDashboard = $(this).is(":checked");
            reportData.UserId = userId;
            reports.push(reportData);
        });

        $.ajax({
            url: "/StudentInformation/StudentInformation/UpdateAllAssignmentStatus",
            data: { selectedAssignments: JSON.stringify(selectedAssignmentList ? selectedAssignmentList : ""), selectedReports: JSON.stringify(reports) },
            type: "post",
            success: function (data) {
                globalFunctions.showMessage(data.NotificationType, data.Message);
                if (data.Success && globalFunctions.isValueValid(data.HeaderText))
                    location.href = data.HeaderText;
            },
            error: function () { globalFunctions.onFailure(); }
        })
    },

        saveAssignmentFiles = function () {
            let selectedAssignmentFilesList = [];
            $.each($("input[name='assignmentFilesFilter']:checked"), function () {
                let assignmentFileId = $(this).val();

                selectedAssignmentFilesList.push({
                    AssignmentFileId: assignmentFileId
                });
            });

            if (selectedAssignmentFilesList.length == 0) {
                globalFunctions.showWarningMessage(translatedResources.selectFile);
                return false;
            }

            $.ajax({
                url: "/StudentInformation/StudentInformation/SaveAssignmentFiles",
                data: { selectedAssignmentFiles: JSON.stringify(selectedAssignmentFilesList ? selectedAssignmentFilesList : "") },
                type: "post",

                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        $("#myModal").modal("hide");
                        initStudentAssignmentPagination();
                    }
                },
                error: function () { globalFunctions.onFailure(); }
            })
        },

        loadStudentAssignmentFiles = function () {
            var assignmentId = $("#AssignmentId").val();
            if (globalFunctions.isValueValid(assignmentId)) {
                var obj = { AssignmentId: assignmentId, StudentId: $("#StudentId").val() };
                $.ajax({
                    url: "/StudentInformation/StudentInformation/GetStudentAssignmnetFiles",
                    data: obj,
                    type: "post",
                    success: function (data) {
                        $("#divStudentAssignmentFiles").html(data);
                        $(".divAcheivementFiles").show();
                        if (globalFunctions.isValueValid($("#StudentAssignmentFileIds").val())) {
                            var attachedFiles = $("#StudentAssignmentFileIds").val();
                            attachedFiles.split(",").map(function (fileId) {
                                $("input:checkbox#" + fileId.trim()).prop("checked", "checked");
                            })
                        }
                    },
                    error: function () { globalFunctions.onFailure(); }
                })
            } else $(".divAcheivementFiles").hide();
        },

        initStudentAssignmentPagination = function () {

            var grid = new DynamicPagination("divStudentAssignment");
            var searchString = $("#assignmentSearch").val() == undefined ? '' : $("#assignmentSearch").val();
            var settings = {
                url: '/StudentInformation/StudentInformation/GetStudentAssignments?pageIndex=' + $("#PageIndexNumber").val() + "&id=" + userId + "&searchString=" + searchString
            };
            grid.init(settings);
            $("#certificateCarousel").owlCarousel({
                margin: 20,
                autoplay: false,
                nav: true,
                dots: false,
                loop: false,
                autoplayHoverPause: true,
                rtl: direction,
                items: 1,
                slideBy: 1,
                responsive: {
                    410: {
                        items: 2,
                        slideBy: 1
                    },
                    767: {
                        items: 3,
                        slideBy: 1
                    },
                    1024: {
                        items: 4,
                        slideBy: 1
                    }
                }
            });
            $(document).off('keyup', ".custom.search-field");
        },

        updateShowhideStatus = function (source, fileId) {
            var message = $(source).data("isselected").toBoolean() ? translatedResources.hideCertificateMessage : translatedResources.showCertificateMessage;
            globalFunctions.notyDeleteConfirm($(source), message);
            $(source).off("okToDelete");
            $(source).on("okToDelete", function () {
                var obj = {
                    UserId: portfolioHomeFunctions.getSelectedUserId(),
                    FileId: fileId
                };

                $.post("/StudentInformation/StudentInformation/UpdateAcademicFileDashboardStatus", obj, function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        var fileType = $(source).data("documenttype") == "video" ? 'V' : 'O';
                        refreshAcademicFiles(fileType);
                    }
                }).fail(function () {
                    globalFunctions.onFailure();
                })
            })
        },

        updateAssignmentStatus = function (assignmentId) {
            var obj = { AssignmentId: assignmentId, UserId: userId };
            $.ajax({
                url: "/StudentInformation/StudentInformation/UpdateAssignmentStatus",
                data: obj,
                type: "post",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                },
                error: function () { globalFunctions.onFailure(); }
            })
        },

        initFileUpload = function (fileType) {
            $("#stdAcademicFiles").off("fileuploaded");
            $("#stdAcademicFiles").fileinput('destroy');
            $("#stdAcademicFiles").fileinput({
                language: translatedResources.locale,
                title: translatedResources.browseFile,
                theme: "fas",
                showUpload: true,
                showCaption: false,
                showMultipleNames: true,
                minFileCount: 1,
                maxFileCount: 10,
                uploadUrl: "/StudentInformation/StudentInformation/SaveAcademicFiles",
                uploadExtraData: function () {
                    return {
                        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
                        fileType: fileType,
                        userId: userId
                    };
                },
                fileActionSettings: {
                    showZoom: false,
                    showUpload: false,
                    //indicatorNew: "",
                    showDrag: false
                },
                allowedFileExtensions: fileType == FileUploadTypes.Videos ?
                    JSON.parse(allowedVideoFileExt) : JSON.parse(allowedOtherFileExtensions),
                uploadAsync: true,
                maxFileSize: translatedResources.fileSizeAllow,
                //maxFileSize: 15000,//for test
                browseClass: "btn btn-sm m-0 z-depth-0 btn-browse",
                removeClass: "btn btn-sm m-0 z-depth-0",
                uploadClass: "btn btn-sm m-0 z-depth-0",
                overwriteInitial: false,
                //previewFileIcon: '<i class="fas fa-file"></i>',
                initialPreviewAsData: true, // defaults markup
                initialPreviewConfig: [{
                    frameAttr: {
                        title: "CustomTitle",
                    }
                }],
                preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
                previewFileIconSettings: { // configure your icon file extensions
                    'doc': '<i class="fas fa-file-word ic-file-word"></i>',
                    'xls': '<i class="fas fa-file-excel ic-file-excel"></i>',
                    'ppt': '<i class="fas fa-file-powerpoint ic-file-ppt"></i>',
                    'pdf': '<i class="fas fa-file-pdf ic-file-pdf"></i>',
                    'zip': '<i class="fas fa-file-archive ic-file"></i>',
                    'htm': '<i class="fas fa-file-code ic-file"></i>',
                    'txt': '<i class="fas fa-file-alt ic-file"></i>',
                    'mov': '<i class="fas fa-file-video ic-file"></i>',
                    'mp3': '<i class="fas fa-file-audio ic-file"></i>',
                    // note for these file types below no extension determination logic
                    // has been configured (the keys itself will be used as extensions)
                    'jpg': '<i class="fas fa-file-image ic-file-img"></i>',
                    'jpeg': '<i class="fas fa-file-image ic-file-img"></i>',
                    'gif': '<i class="fas fa-file-image ic-file-img"></i>',
                    'png': '<i class="fas fa-file-image ic-file-img"></i>'
                },
                previewSettings: {
                    image: { width: "50px", height: "auto" },
                    html: { width: "50px", height: "auto" },
                    other: { width: "50px", height: "auto" }
                },
                previewFileExtSettings: { // configure the logic for determining icon file extensions
                    'doc': function (ext) {
                        return ext.match(/(doc|docx)$/i);
                    },
                    'xls': function (ext) {
                        return ext.match(/(xls|xlsx)$/i);
                    },
                    'ppt': function (ext) {
                        return ext.match(/(ppt|pptx)$/i);
                    },
                    'zip': function (ext) {
                        return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
                    },
                    'htm': function (ext) {
                        return ext.match(/(htm|html)$/i);
                    },
                    'txt': function (ext) {
                        return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
                    },
                    'mov': function (ext) {
                        return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
                    },
                    'mp3': function (ext) {
                        return ext.match(/(mp3|wav)$/i);
                    }
                },
                msgSizeTooLarge: 'Total File size (<b>{size}</b>) exceeds maximum allowed upload size of <b>{maxSize}</b>. Please retry your upload!'
            }).off('filebatchselected').on('filebatchselected', function (event, files) {
                var size = 0;
                $.each(files, function (ind, val) {
                    //console.log($(this).attr('size'));
                    size = size + $(this).attr('size');
                });

                var maxFileSize = $(this).data().fileinput.maxFileSize,
                    msg = $(this).data().fileinput.msgSizeTooLarge,
                    formatSize = (s) => {
                        i = Math.floor(Math.log(s) / Math.log(1024));
                        sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                        out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
                        return out;
                    };

                if ((size / 1024) > maxFileSize) {
                    msg = msg.replace('{name}', 'all files');
                    msg = msg.replace('{size}', formatSize(size));
                    msg = msg.replace('{maxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
                    //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
                    if (files.length > 1) {
                        $(".kv-fileinput-error.file-error-message").html('');
                        $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                        $(".close.kv-error-close").on('click', function (e) {
                            $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                            //$(".fileinput-upload").removeClass('d-none');
                        });
                    }
                    else {
                        $(".kv-fileinput-error.file-error-message").html('');
                        $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                        $(".close.kv-error-close").on('click', function (e) {
                            $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                            //$(".fileinput-upload").removeClass('d-none');
                        });
                    }

                    $(".fileinput-upload").attr('disabled', true);
                }
            }).off('fileuploaded').on("fileuploaded", function (e, data) {
                $("#stdAcademicFiles").off("fileuploaded");
                globalFunctions.showMessage(data.response.NotificationType, data.response.Message);
                if (data.response.Success) {
                    $("#stdAcademicFiles").fileinput('destroy');
                    $("button.fileinput-remove.fileinput-remove-button").click();
                    refreshAcademicFiles(fileType);
                    $('.file-upload-widget').animate({
                        right: '-300'
                    });
                }
            });
        },

        deleteAcademicFile = function (source) {
            var obj = {
                UserId: portfolioHomeFunctions.getSelectedUserId(),
                FileId: $(source).data("fileid")
            }
            globalFunctions.notyDeleteConfirm($(source), translatedResources.deleteConfirm);
            $(source).off("okToDelete");
            $(source).on("okToDelete", function () {
                $.post("/StudentInformation/StudentInformation/DeleteAcademicFile", obj, function (data) {
                    if (data.Success) {
                        var fileType = $(source).data("documenttype") == "video" ? 'V' : 'O';
                        refreshAcademicFiles(fileType);
                    }
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                }).fail(function () {
                    globalFunctions.onFailure();
                });
            });
        },

        initAcademicsForm = function (source, academicId) {
            globalFunctions.loadPopup($(source), "/StudentInformation/StudentInformation/InitAcademicsForm?studentAcademicId=" + academicId + "&userId=" + userId,
                "Add new Assignments");
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $(".selectpicker").selectpicker();
                if (globalFunctions.isValueValid($("select#AssignmentId").val()))
                    $("select#AssignmentId").trigger("change");
            });

        },

        ViewAllFiles = function (source) {
            var allfile = $(source).data('allfile');
            var fileData = allfile.split(",");
            var filesJSON = [];
            fileData.map(function (item, i) {
                filesJSON.push({ FilePath: item, AcademicTitle: $(source).data("assignmentname") });
            });
            $.ajax({
                url: "/StudentInformation/StudentInformation/ViewAllFiles",
                data: { assignmentFileList: JSON.stringify(filesJSON) },
                type: "post",
                success: function (result) {
                    $("#modal_Loader").html(result);
                    $("#modal_heading").text('Assignment Files');
                    $("#myModal").modal();
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            });
        },

        deleteAcademicAcheivement = function (source, assignmentId) {
            globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
            $(source).off("okClicked");
            $(source).on("okClicked", function () {
                $.post("/StudentInformation/StudentInformation/DeleteAcademicAssignment", { AssignmentId: assignmentId, UserId: userId }, function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        initStudentAssignmentPagination();
                    }
                });
            });
        },

        refreshAcademicFiles = function (fileType) {
            $("#academicFiles_" + fileType).load("/StudentInformation/StudentInformation/GetStudentAcademicFiles?userId=" + userId + "&fileType=" + fileType);
        };

    return {
        initFileUpload: initFileUpload,
        loadStudentAssignmentFiles: loadStudentAssignmentFiles,
        saveAssignmentFiles: saveAssignmentFiles,
        updateAssignmentStatus: updateAssignmentStatus,
        updateAllAssignmentStatus: updateAllAssignmentStatus,
        initStudentAssignmentPagination: initStudentAssignmentPagination,
        deleteAcademicFile: deleteAcademicFile,
        updateShowhideStatus: updateShowhideStatus,
        initAcademicsForm: initAcademicsForm,
        deleteAcademicAcheivement: deleteAcademicAcheivement,
        ViewAllFiles: ViewAllFiles
    }
}();

$(function () {
    $("#topPortfolioActions").removeClass("d-none").appendTo($("#breadcrumbActions"));

    studentAcademicFunctions.initStudentAssignmentPagination();
    $(document).off('keyup', ".custom.search-field");
    $(document).off("keypress");
    $(document).on("keypress", function (e) {
        e.stopPropagation();
        var key = e.which;
        if (key == 13) {
            $("#PageIndexNumber").val('1')
            studentAcademicFunctions.initStudentAssignmentPagination();
        }
    });

    $(document).on('click', '#btnAssignmentSearch', function () {
        $("#PageIndexNumber").val('1');
        studentAcademicFunctions.initStudentAssignmentPagination();
    });

    $(document).on("change", "#checkAllFiles", function checkAllFiles(ele) {
        var checkAllFiles = document.getElementById('checkAllFiles');
        var chkFiles = document.getElementsByTagName('input');
        if (checkAllFiles.checked) {
            for (var i = 0; i < chkFiles.length; i++) {
                if (chkFiles[i].type == 'checkbox') {
                    chkFiles[i].checked = true;
                }
            }
        }
        else {
            for (var i = 0; i < chkFiles.length; i++) {
                if (chkFiles[i].type == 'checkbox') {
                    chkFiles[i].checked = false;
                }
            }
        }
    });

    $(document).on("DynamicPagination:Complete", function () {
        globalFunctions.enableCheckboxCascade("#ShowAllOnPortfolio", ".chkAcademicAssignment");
    });

    $(document).on("change", "#AcademicYear", function () {
        var academicYearId = $(this).val();
        if (globalFunctions.isValueValid(academicYearId)) {
            var param = "?studentNumber=" + $("#SelectedStudentNumber").val() + "&selectedYearId=" + academicYearId + "&userId=" + $("#SelectedPortfolioUserId").val();
            $("#stdAssessmentReports").load("/StudentInformation/StudentInformation/GetStudentAssessmentReports" + param, function () { });
        }
    })

});