﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />


var formElements = new FormElements();
var addEditSchoolBanner = function () {
    var init = function () {


        //$("#dropAreaBanner").fileinput({
        //    language: translatedResources.locale,
        //    title: translatedResources.BrowseFile,
        //    theme: "fas",
        //    showUpload: false,
        //    showCaption: false,
        //    showMultipleNames: false,
        //    maxFileCount: 1,
        //    //minImageWidth: 300,
        //    //minImageHeight: 200,
        //    //maxImageWidth: 500,
        //    maxImageHeight: 330,
        //    showPreview: true,
        //    fileActionSettings: {
        //        showZoom: false,
        //        showDrag: false
        //    },

        //    //uploadAsync: true,
        //    browseClass: "btn btn-sm m-0 z-depth-0 btn-browse",
        //    removeClass: "btn btn-sm m-0 z-depth-0",
        //    //uploadClass: "btn btn-sm m-0 z-depth-0",
        //    overwriteInitial: false,
        //    //previewFileIcon: '<i class="fas fa-file"></i>',
        //    initialPreviewAsData: false, // defaults markup
        //    initialPreviewConfig: [{
        //        frameAttr: {
        //            title: 'My Custom Title',
        //        }
        //    }],
        //    allowedFileExtensions: translatedResources.FileExtensions,
        //    preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
        //    previewFileIconSettings: { // configure your icon file extensions
        //        'doc': '<i class="fas fa-file-word ic-file-word"></i>',
        //        'xls': '<i class="fas fa-file-excel ic-file-excel"></i>',
        //        'ppt': '<i class="fas fa-file-powerpoint ic-file-ppt"></i>',
        //        'pdf': '<i class="fas fa-file-pdf ic-file-pdf"></i>',
        //        'zip': '<i class="fas fa-file-archive ic-file"></i>',
        //        'htm': '<i class="fas fa-file-code ic-file"></i>',
        //        'txt': '<i class="fas fa-file-alt ic-file"></i>',
        //        'mov': '<i class="fas fa-file-video ic-file"></i>',
        //        'mp3': '<i class="fas fa-file-audio ic-file"></i>',
        //        // note for these file types below no extension determination logic
        //        // has been configured (the keys itself will be used as extensions)
        //        'jpg': '<i class="fas fa-file-image ic-file-img"></i>',
        //        'jpeg': '<i class="fas fa-file-image ic-file-img"></i>',
        //        'gif': '<i class="fas fa-file-image ic-file-img"></i>',
        //        'png': '<i class="fas fa-file-image ic-file-img"></i>'
        //    },
        //    previewSettings: {
        //        image: { width: "50px", height: "auto" },
        //        html: { width: "50px", height: "auto" },
        //        other: { width: "50px", height: "auto" }
        //    },
        //    previewFileExtSettings: { // configure the logic for determining icon file extensions
        //        'doc': function (ext) {
        //            return ext.match(/(doc|docx)$/i);
        //        },
        //        'xls': function (ext) {
        //            return ext.match(/(xls|xlsx)$/i);
        //        },
        //        'ppt': function (ext) {
        //            return ext.match(/(ppt|pptx)$/i);
        //        },
        //        'zip': function (ext) {
        //            return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
        //        },
        //        'htm': function (ext) {
        //            return ext.match(/(htm|html)$/i);
        //        },
        //        'txt': function (ext) {
        //            return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
        //        },
        //        'mov': function (ext) {
        //            return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
        //        },
        //        'mp3': function (ext) {
        //            return ext.match(/(mp3|wav)$/i);
        //        }
        //    }

        //}).on('fileselect', function (event, numFiles, label) {
        //    $("#ActualImageName").val(label);
        //});

        var date = new Date();
        date.setDate(date.getDate() - 2);
        console.log($("#hdnStartDate").val());
        var minDateData = $("#hdnStartDate").val();

        $('.date-picker1').datetimepicker({
            format: 'DD-MMM-YYYY',
            minDate: new Date(minDateData)
        }).on('dp.change', function (e) {
            $('.date-picker2').data("DateTimePicker").minDate(e.date);
            if ($('.date-picker2').data("DateTimePicker").date() < e.date) {
                $('.date-picker2').data("DateTimePicker").clear();
                $('.date-picker2').data("DateTimePicker").minDate(e.date);
            }
        });
        $('.date-picker2').datetimepicker({
            format: 'DD-MMM-YYYY'           
        })
        $('.date-picker1').datetimepicker('show');
        $('.date-picker1').datetimepicker('hide');

        $(".date-picker2").on("dp.change", function (e) {
            $('.date-picker1').data("DateTimePicker").maxDate(e.date);
        });
        if ($("#IsAddMode").val() == "False") {
            $('.date-picker2').data("DateTimePicker").minDate($('.date-picker1').data("DateTimePicker").date());
        }
    },
        saveSchoolBanner = function (e) {
            var _URL = window.URL || window.webkitURL;
            if ($('form#frmAddEditSchoolBanner').valid()) {
                var formData = new FormData();
                var token = $('input[name=__RequestVerificationToken]').val();
                formData.append("__RequestVerificationToken", token);
                if ($("#UploadedImageName").val() == '') {
                    var bannerFile = document.getElementById("dropContentImage").files[0];
                    formData.append("bannerFile", bannerFile);
                }

                formData.append("SchoolBannerId", $('#SchoolBannerId').val());
                formData.append("IsAddMode", $('#IsAddMode').val());
                formData.append("UploadedImageName", $('#UploadedImageName').val());
                formData.append("BannerName", $('#BannerName').val());
                formData.append("ClickURL", $('#ClickURL').val());
                formData.append("SchoolIds", $('#SchoolIds').val());
                formData.append("UserTypeIds", $('#UserTypeIds').val());
                formData.append("BannerPublishDate", $('#BannerPublishDate').val());
                formData.append("BannerEndDate", $('#BannerEndDate').val());
                formData.append("DisplayOrder", $('#DisplayOrder').val());
                formData.append("DisplayOrder", $('#DisplayOrder').val());
                formData.append("ActualImageName", $('#ActualImageName').val());
                formData.append("SchoolBannerXml", $('#SchoolBannerXml').val());

                if (bannerFile == undefined && $("#UploadedImageName").val() == '') {
                    globalFunctions.showErrorMessage(translatedResources.showFileuploadError);
                    return;
                }

                //if (bannerFile !== undefined) {
                //    img = new Image();
                //    var objectUrl = _URL.createObjectURL(bannerFile);
                //    img.onload = function () {
                //        _URL.revokeObjectURL(objectUrl);
                //        if ((this.width >= 300 && this.width <= 500) && (this.height >= 200 && this.height <= 330)) {
                //            submitSchoolBanner(formData);
                //        }
                //        else {
                //            globalFunctions.showErrorMessage(translatedResources.FileSizeValidation);
                //        }
                //    };
                //    img.src = objectUrl;
                //}
                //else {
                    submitSchoolBanner(formData);
                //}
            }
        },
        submitSchoolBanner = function (formdata) {
            $.ajax({
                type: 'POST',
                url: '/SchoolInfo/SchoolBanner/SaveSchoolBannerDetails',
                data: formdata,
                processData: false,
                contentType: false,
                success: function (result) {
                    debugger
                    if (result.Success == true)
                        schoolBanner.onSaveBannerSuccess($('#IsAddMode').val());
                    else {
                        schoolBanner.onSaveBannerError($('#IsAddMode').val());
                    }
                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });

        },
        deleteImage = function () {
            $('#library').hide();
            $('#UploadedImageName').val('');
            $('#imgUpload').toggleClass('d-none');
        }
    return {
        init: init,
        saveSchoolBanner: saveSchoolBanner,
        submitSchoolBanner: submitSchoolBanner,
        deleteImage: deleteImage
    };
}();

$(document).ready(function () {
    formElements.feSelect();
    addEditSchoolBanner.init();
    //$("#btnSaveSchoolbanner").on("click", function () {
    //    addEditSchoolBanner.saveSchoolBanner();
    //});
    $('#ahrefDeleteImage').click(function () {
        addEditSchoolBanner.deleteImage();
    })
    $(document).on('click', '#btnSaveSchoolbannerData', function () {
        addEditSchoolBanner.saveSchoolBanner();
    });
});