﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />


var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var schoolBanner = function () {
    var init = function () {
        loadBannerGrid();
        $("#tbl-SchoolBannerList_filter").hide();
    },
        addSchoolBanner = function (source, bannerId) {
            loadCreatePopup(source, translatedResources.add, bannerId);
        },
        editSchoolBanner = function (source, bannerId) {
            loadCreatePopup(source, translatedResources.edit, bannerId);
        },
        loadCreatePopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.SchoolBanner;
            globalFunctions.loadPopup(source, '/SchoolInfo/SchoolBanner/AddEditSchoolBanner?bannerId=' + id, title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: translatedResources.selectedText
                });
            });
        },
        onSaveBannerSuccess = function (isAddmode) {          
           // schoolBanner.init();
            if (isAddmode == 'True') {
                globalFunctions.showSuccessMessage(translatedResources.AddSuccess);
            } else {
                globalFunctions.showSuccessMessage(translatedResources.EditSuccess);
            }
            $("#myModal").modal('hide');
            schoolBanner.init();
        },
        onSaveBannerError = function (isAddmode) {
            init();
            if (isAddmode == 'True') {
                globalFunctions.showErrorMessage(translatedResources.AddError);
            } else {
                globalFunctions.showErrorMessage(translatedResources.EditError);
            }
            $("#myModal").modal('hide');
        }
        loadBannerGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "BannerName", "sTitle": translatedResources.BannerName, "sWidth": "50%", "sClass": "wordbreak" },
                { "mData": "ClickURL", "sTitle": translatedResources.ClickUrl, "sWidth": "30%", "sClass": "wordbreak" },               
                { "mData": "UploadedImage", "sTitle": translatedResources.QuizImage, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initBannerGrid('tbl-SchoolBannerList', _columnData, '/SchoolInfo/SchoolBanner/LoadSchoolBanners');
        },
        initBannerGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnDefs: [{
                    'orderable': false,
                    'targets': 2
                }],
                columnSelector: false
            };
            grid.init(settings);
        },
        deleteSchoolBanner = function (source,id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('SchoolBanner', '/SchoolInfo/SchoolBanner/DeleteSchoolBanner', 'tbl-SchoolBannerList', source, id);
            }
        }
    

    return {
        init: init,
        addSchoolBanner: addSchoolBanner,
        loadCreatePopup: loadCreatePopup,
        onSaveBannerSuccess: onSaveBannerSuccess,
        onSaveBannerError: onSaveBannerError,
        loadBannerGrid: loadBannerGrid,
        editSchoolBanner: editSchoolBanner,
        deleteSchoolBanner: deleteSchoolBanner

    };
}();
$(document).ready(function () {
    schoolBanner.init();
    $(document).on('click', '#btnAddSchoolbanner', function () {
        schoolBanner.addSchoolBanner($(this));
    });
    $("#SchoolBannerSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-SchoolBannerList').DataTable().search($(this).val()).draw();

    });
});