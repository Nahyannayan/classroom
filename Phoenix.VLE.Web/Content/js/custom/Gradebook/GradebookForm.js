﻿var internalAssessmentList = [];
var SubExternalExaminationIds = "";

var gradeBookForm = function () {
    var init = function () {
        $('.date-picker').datetimepicker({
            format: 'DD-MMM-YYYY'
        })
        $('.selectpicker').selectpicker('refresh');
        //to convert the json date to js datetime
        //this object are setup in AddEditGradebook.cshtml page
        assignmentData.forEach(d => {
            d.StartDate = moment(d.StartDate).toDate().toDateString();
            d.EndDate = moment(d.EndDate).toDate().toDateString();
        });

        //to convert the json date to js datetime
        //this object are setup in AddEditGradebook.cshtml page
        internalAssessmentData.forEach(d => {
            if (globalFunctions.isValueValid(d.StartDate))
                d.StartDate = moment(d.StartDate).toDate().toDateString();
            if (globalFunctions.isValueValid(d.EndDate))
                d.EndDate = moment(d.EndDate).toDate().toDateString();
        });
        ShowAllTheHiddenElement();
        $('input[data-mindateid],input[data-maxdateid]').trigger("dp.change");
    },
        onSaveSuccess = function (response) {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success)
                window.location.href = "/gradebook/gradebooksetup";
        },
        onFailure = function () {
            globalFunctions.onFailure();
        },
        // on row type change show and hide there respective input fields
        onRowTypeDropdownChange = function (element, index) {

            var dropdownValue = $(element).val();
            $(`div[id^="RowType_${index}_"]`).addClass('d-none');

            if (dropdownValue)
                $(`#RowType_${index}_${dropdownValue}`).removeClass('d-none');
        },
        //to load internal assessment form New/Edit
        loadInternalAssessmentNewForm = function (id) {

            $.post('/Gradebook/GradebookSetup/GetInternalAssessmentForm', { number: id, internalAssessmentList: internalAssessmentList })
                .then(response => {
                    $('#divInternalAssessmentForm').html('');
                    $('#divInternalAssessmentForm').html(response);
                    gradeBookForm.ShowHideInternalAssessmentForm(true);

                    $('.date-picker').datetimepicker({
                        format: 'DD-MMM-YYYY'
                    })
                    $('.selectpicker').selectpicker('refresh');
                    if ($("#IsCalculatedField").is(":checked")) {
                        $('#dataentry0,#calculate0').toggleClass('active');
                    }
                })
        },
        removeInternalAssessmentNewForm = function () {
            globalFunctions.notyDltConfirm(translatedResources.removeFormConfiramtionMessage, () => {
                $('.internalAssessmentForm:last').remove();
                var countTextBox = $('#select-columns');
                countTextBox.val(parseInt(countTextBox.val() || 0) - 1);
            });
        },
        ShowAllTheHiddenElement = function () {
            $('.rowType').trigger('change');
            dataEntryCheckBoxChecked();
        },
        //if data entry is check the show data entry form else calculated form
        dataEntryCheckBoxChecked = function () {
            $('.calculateField').each(function (i, e) {
                var element = $(e);
                var checked = element.prop('checked');
                var elementIndex = e.id.split('_')[1];
                $(`#dataentry${elementIndex}`)[checked ? "removeClass" : "addClass"]("active");
                $(`#dataentry_${elementIndex}`)[checked ? "addClass" : "removeClass"]('d-none')

                $(`#calculate${elementIndex}`)[checked ? "addClass" : "removeClass"]("active");
                $(`#calculatedFields_${elementIndex}`)[checked ? "removeClass" : "addClass"]('d-none')
            });
        },
        //to load formula form in popup screen
        //loadFormulaForm = function (source) {
        //    globalFunctions.loadPopup(source, '/Gradebook/Gradebooksetup/LoadFormulaForm', 'Add Formula', 'processing-rule');
        //    $(source).off("modalLoaded");
        //    $(source).on("modalLoaded", function () {
        //    });
        //},
        //to add internal assessment data 
        AddInternalAssessmentData = function () {

            var IsValid = common.IsInvalidByFormOrDivId("InternalAssessmentFormId");

            if (IsValid) {

                var table = $('#tableInternalAssessment').DataTable();

                var InternalIsEdit = $("#InternalIsEdit").val();
                var InternalAssessmentId = $("#InternalAssessmentId").val();
                var Index = $('#Index').val();
                var RowName = $("#RowName").val();
                //var Weightage = $("#Weightage").val();
                var IsCalculatedField = $("#IsCalculatedField").is(":checked");
                var RowType = $("#RowType").val();
                var FromMark = $("#FromMark").val();
                var ToMark = $("#ToMark").val();
                var DataEntryGradingTemplateId = $("#DataEntryGradingTemplateId").val();
                var CommentLength = $("#CommentLength").val();
                var AllowTeacherToAddColumn = $("#AllowTeacherToAddColumn").is(":checked");
                var ResultsToConsider = [];
                $(".chkAssignmentQuiz:checked").each(function (index, obj) {
                    ResultsToConsider.push($(obj).val());
                });
                var StartDate = $("#StartDate").val();
                var EndDate = $("#EndDate").val();
                var CalculatedGradingTemplateId = $("#CalculatedGradingTemplateId").val();
                var CalculatedFormulaId = $("#CalculatedFormulaId").val();
                var calculate = $("#calculate0").text();
                var dataentry = $("#dataentry0").text();
                var Action = `<span>
                                    <a class="icon-btn mr-3" href="javascript:void(0)" onclick="gradeBookForm.loadInternalAssessmentNewForm(${parseInt(Index)})"> 
                                        <img src="/Content/VLE/img/svg/tbl-edit.svg">
                                    </a>
                                    <a class="icon-btn" href="javascript:void(0)" onclick="gradeBookForm.DeleteInternalAssessment(${parseInt(Index)},this)"> 
                                        <img src="/Content/VLE/img/svg/tbl-delete.svg">
                                    </a>
                             </span>`

                if (InternalIsEdit == '0') {
                    if (IsCalculatedField) {
                        internalAssessmentList.push({
                            Index: parseInt(Index),
                            RowName: RowName,
                            //Weightage: Weightage,
                            IsCalculatedField: IsCalculatedField,
                            ResultsToConsider: ResultsToConsider,
                            StartDate: StartDate,
                            EndDate: EndDate,
                            CalculatedGradingTemplateId: CalculatedGradingTemplateId,
                            AllowTeacherToAddColumn: AllowTeacherToAddColumn,
                            CalculatedFormulaId: CalculatedFormulaId,
                            IsEdit: 1,
                            IsActive: true
                        });
                    }
                    else {
                        internalAssessmentList.push({
                            Index: parseInt(Index),
                            RowName: RowName,
                            //Weightage: Weightage,
                            IsCalculatedField: IsCalculatedField,
                            RowType: RowType,
                            FromMark: FromMark,
                            ToMark: ToMark,
                            DataEntryGradingTemplateId: DataEntryGradingTemplateId,
                            CommentLength: CommentLength,
                            AllowTeacherToAddColumn: AllowTeacherToAddColumn,
                            IsEdit: 1,
                            IsActive: true
                        });
                    }

                    table.row.add([
                        RowName,
                        IsCalculatedField ? calculate : dataentry,
                        //Weightage,
                        AllowTeacherToAddColumn ? "Yes" : "No",
                        Action
                    ]).draw(true);
                }
                else {
                    if (IsCalculatedField) {
                        $(internalAssessmentList).each(function (index, obj) {
                            if (internalAssessmentList[index].Index == Index) {
                                internalAssessmentList[index].RowName = RowName;
                                //internalAssessmentList[index].Weightage = Weightage;
                                internalAssessmentList[index].IsCalculatedField = IsCalculatedField;
                                internalAssessmentList[index].ResultsToConsider = ResultsToConsider;
                                internalAssessmentList[index].StartDate = StartDate;
                                internalAssessmentList[index].EndDate = EndDate;
                                internalAssessmentList[index].CalculatedGradingTemplateId = CalculatedGradingTemplateId;
                                internalAssessmentList[index].AllowTeacherToAddColumn = AllowTeacherToAddColumn;
                                internalAssessmentList[index].CalculatedFormulaId = CalculatedFormulaId;
                            }
                        });
                    }
                    else {
                        $(internalAssessmentList).each(function (index, obj) {
                            if (internalAssessmentList[index].Index == Index) {
                                internalAssessmentList[index].RowName = RowName;
                                //internalAssessmentList[index].Weightage = Weightage;
                                internalAssessmentList[index].IsCalculatedField = IsCalculatedField;
                                internalAssessmentList[index].RowType = RowType;
                                internalAssessmentList[index].FromMark = FromMark;
                                internalAssessmentList[index].ToMark = ToMark;
                                internalAssessmentList[index].DataEntryGradingTemplateId = DataEntryGradingTemplateId;
                                internalAssessmentList[index].CommentLength = CommentLength;
                                internalAssessmentList[index].AllowTeacherToAddColumn = AllowTeacherToAddColumn
                            }
                        });
                    }
                    var currentRow = table.row(Index).data();
                    currentRow[0] = RowName;
                    currentRow[1] = IsCalculatedField ? calculate : dataentry;
                    //currentRow[2] = Weightage;
                    currentRow[2] = AllowTeacherToAddColumn ? "Yes" : "No";
                    table.row(Index).data(currentRow).draw(true);
                }
                gradeBookForm.ShowHideInternalAssessmentForm(false);
                gradeBookForm.LoadInternalAssessmentGridData();
            }
        },
        //load internal assessment grid when new data is added, this is just json array posted and show in table 
        LoadInternalAssessmentGridData = function () {
            $.post('/Gradebook/GradebookSetup/LoadInternalAssessmentGridData', { internalAssessmentList: internalAssessmentList })
                .then(response => {
                    $('#divInternalAssessmentGridData').html('');
                    $('#divInternalAssessmentGridData').html(response);
                })
        },
        //Delete internal assessment when deleted from table 
        DeleteInternalAssessment = function (Index, element) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {

                internalAssessmentList.forEach((ele, index) => {
                    if (index == Index) {
                        internalAssessmentList[index].IsActive = false;
                    }
                });

                var table = $('#tableInternalAssessment').DataTable();
                table.row($(element).parents('tr')).remove().draw();

                gradeBookForm.LoadInternalAssessmentGridData();
            });
        },
        ShowHideInternalAssessmentForm = function (IsShow = false) {
            if (IsShow) {
                $("#btnInternalAssessmentId").addClass('d-none');
            }
            else {
                $("#btnInternalAssessmentId").removeClass('d-none');
                $('#divInternalAssessmentForm').html('');
            }
        },
        loadFormulaForm = function (source) {
            var buttons = [], object = {};
            $("#modal_Loader").html("");
            $('.chkAssignmentQuiz:checked').each(function (i, e) {
                buttons.push({ Key: $(e).data("label"), Value: $(e).data("placeholder") })
            })
            object["Buttons"] = buttons;
            $.post('/Gradebook/Gradebooksetup/LoadFormulaFromGradebookForm', { gradebookFormula: object })
                .then(response => {
                    $("#modal_heading").text('Add Formula');
                    $("#modal_Loader").html(response).parent().parent().addClass('modal-lg');
                    $('#myModal').modal({ backdrop: 'static' });
                    $('#myModal').one('shown.bs.modal', function () {
                        //make input labels active if there is initial value available
                        $('textarea').each(function (element, i) {
                            //console.log($(this).val());
                            if (($(this).val() !== undefined && $(this).val().length > 0) || $(this).attr("placeholder") !== null) {
                                $(this).siblings('label').addClass('active');
                            }
                            else {
                                $(this).siblings('label').removeClass('active');
                            }
                        });
                        /* Trim spaces of inputs  */
                        trimInputValue = function (input) {
                            var trimmedValue = $(input).val().trim();
                            $(input).val(trimmedValue);
                        };

                        $(":input[type='text']").on('change', function () {
                            trimInputValue($(this));
                        });

                        $("textarea").on('change', function () {
                            trimInputValue($(this));
                        });
                    });
                });
        },
        validateAssessment = function (element) {
            element = $(element);
            if (!element.prop('checked')) {
                var checkedValue = element.data('placeholder');
                $('#CalculatedFormulaId option').each(function (i, e) {
                    if ($(e).data('assessment').split(',').some(x => x == checkedValue)) {
                        globalFunctions.showWarningMessage('Assessment included in formula');
                        element.prop('checked', true);
                    }
                });
            }
        },
        //to load preview of gradebook, teacher gradebook partial view is called and shown in popup
        loadPreviewGradebook = function () {
            if (internalAssessmentList.length > 0) {
                var object = {
                    SchoolGradeIds: $('#SchoolGradeIds').val(),
                    AssessmentTypeIds: 3,
                    InternalAssessments: internalAssessmentList
                }
                $("#previewGradebook").html("");
                $.post('/Gradebook/Gradebook/GetGradebookPreview', { gradeBook: object })
                    .then(response => {
                        $("#previewGradebook").html(response);
                        $('#previewGradebookPopup').modal({ backdrop: 'static' });
                        $('.selectpicker').selectpicker('refresh');
                        $('.paymentDue-toast').addClass('animated fadeInRight');
                        $('body').on('click', '.close-toast', function () {
                            $(this).parent().parent().fadeOut();
                        })
                        $('.sibling-row').addClass('hide-row');
                        //$(".inner-right-table").mCustomScrollbar({
                        //    autoExpandHorizontalScroll: true,
                        //    autoHideScrollbar: true,
                        //    axis: "x",
                        //    //setWidth: $('.gradebook-wrapper').width() - ($('.outer-table td:first').width() + 45)
                        //    //scrollbarPosition: "outside"
                        //});
                        $('div[id^=headingTwo]').click(function () {
                            var assessmentType = $(this).data("assessment");
                            if (!$(this).siblings().hasClass('show')) {
                                $('.inner-right-table tr.sibling-row').addClass('hide-row');
                                $('.inner-right-table tr.sibling-row.assessment' + assessmentType).removeClass('hide-row');
                            }
                            else {
                                $('.inner-right-table tr.sibling-row').addClass('hide-row');
                            }
                        });
                        $('.hide-btn').click(function () {
                            $('.dropdown-wrapper').toggleClass('hide-item');
                        });
                    });
            }
            else {
                globalFunctions.showErrorMessage("No data to preview");
            }
        },
        //to validate is course and grade combination already exists in any setup
        validateGradesAndCourse = function () {
            var courseIds = $('#CourseIds').val();
            var gradeIds = $('#SchoolGradeIds').val();
            if (globalFunctions.isValueValid($('#GradeBookId').val())) {
                var oldcourses = $('#OldCourse').val().split(',');
                var oldgrades = $('#OldGrade').val().split(',');
                if (globalFunctions.isValueValid(oldcourses)) {
                    oldcourses.forEach(x => courseIds.splice(courseIds.indexOf(x), 1));
                }
                if (globalFunctions.isValueValid(oldgrades)) {
                    oldgrades.forEach(x => gradeIds.splice(courseIds.indexOf(x), 1));
                }
            }
            var isUnique;
            courseIds = courseIds.join(',');
            gradeIds = gradeIds.join(',');

            if (courseIds.length > 0 || gradeIds.length > 0) {
                $.ajax({
                    type: 'GET',
                    url: '/Gradebook/GradebookSetup/ValidateGradeandCourseCombination',
                    data: { courseIds, gradeIds },
                    async: false,
                    success: function (data) {
                        console.log(data)
                        var message = [];
                        data.forEach(i => {
                            var msg = translatedResources.mappingWarning.replace('{1}', i.GradeDisplay).replace('{0}', i.CourseName);
                            message.push(msg);
                        })
                        if (data.length > 0) {
                            noty({
                                text: message.join(),
                                layout: 'topCenter',
                                modal: true,
                                closeWith: ['button'],
                                buttons: [
                                    {
                                        type: "button",
                                        addClass: 'btn btn-outline-primary',
                                        text: translatedResources.ok,
                                        onClick: function ($noty) {
                                            $noty.close();
                                        }
                                    }
                                ]

                            });
                            isUnique = false;
                        } else {
                            isUnique = true;
                        }
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure(data, xhr, status);
                        return false;
                    }
                });
                return isUnique;
            } else {
                return true;
            }
        },
        //to validate the entire gradebook setup form
        validateForm = function (formId) {
            var result = gradeBookForm.validateGradesAndCourse();
            if (result) {
                var IsValidForm = common.IsInvalidByFormOrDivId(formId, true);
                var IsMultiCheck = common.IsValidMultipleCheckBox();
                if (IsValidForm && IsMultiCheck) {
                    return true;
                }
            }
            return false;

        },
        //to reset gradebook setup form
        ResetForm = function () {
            $("#GradeBookName").val('');
            $("#CourseIds").val('');
            $("#SchoolGradeIds").val('');
            $("#AssessmentTypeIds").val('');
            $('.selectpicker').selectpicker('refresh');
            $("#AssessmentTypeIds").trigger('changed.bs.select');
        }
    return {
        init,
        onSaveSuccess,
        onFailure,
        onRowTypeDropdownChange,
        loadInternalAssessmentNewForm,
        removeInternalAssessmentNewForm,
        ShowAllTheHiddenElement,
        dataEntryCheckBoxChecked,
        //loadFormulaForm,
        AddInternalAssessmentData,
        LoadInternalAssessmentGridData,
        DeleteInternalAssessment,
        ShowHideInternalAssessmentForm,
        loadFormulaForm,
        validateAssessment,
        loadPreviewGradebook,
        validateGradesAndCourse,
        validateForm,
        ResetForm
    }
}();


$(document).ready(function () {
    gradeBookForm.init();
}).on('changed.bs.select', '#AssessmentTypeIds', function () {
    var values = ($(this).val()).join(',');
    $.post('/Gradebook/GradebookSetup/GetGradeBookAssessmentTypeForm', { assessmentTypeIds: values, setups: assignmentData, standardizeds: standardData, internalAssessments: internalAssessmentData })
        .then(response => {
            $('#assessmentForm').html('');
            $('#assessmentForm').html(response);
            gradeBookForm.init();
            $('#tableInternalAssessment').DataTable({
                "bLengthChange": false,
                "bFilter": false,
                "bInfo": true,
                "paging": true,
                "bSort": false,
                "pageLength": 5
            });
            $('.dataTables_paginate').addClass('pt-0');
            $('.dataTables_info').addClass('pt-0');
        });
}).on('change', '#select-columns', function () {
    gradeBookForm.loadInternalAssessmentNewForm($('#tableInternalAssessment>tbody>tr').length);
}).on('click', '#plusButton', function () {
    $('#select-columns').val(parseInt($('#select-columns').val() || 0) + 1);
    gradeBookForm.loadInternalAssessmentNewForm($('#tableInternalAssessment>tbody>tr').length);
}).on('click', '#CancelInternalAssessment', function () {
    gradeBookForm.ShowHideInternalAssessmentForm(false);
}).on('click', '#btnInternalAssessmentId', function () {
    gradeBookForm.loadInternalAssessmentNewForm(internalAssessmentList.length);
}).on('click', '#previewBtn', function () {
    gradeBookForm.loadPreviewGradebook();
}).on('hidden.bs.select', '#SchoolGradeIds', function () {
    gradeBookForm.validateGradesAndCourse();
}).on('dp.change', 'input[data-mindateid],input[data-maxdateid]', function (e) {
    if (e.date) {
        try {
            if (globalFunctions.isValueValid($(this).data('mindateid')))
                $(`#${$(this).data('mindateid')}`).data("DateTimePicker").maxDate(e.date);

            else if (globalFunctions.isValueValid($(this).data('maxdateid')))
                $(`#${$(this).data('maxdateid')}`).data("DateTimePicker").minDate(e.date);

        } catch (e) {

        }
    }
}).on('hidden.bs.select', '#ddlExternalExamination', function () {
    var ExternalExaminationId = $("#ddlExternalExamination").val();
    if (ExternalExaminationId != '' && ExternalExaminationId != undefined) {
        common.bindSingleDropDownByParameter(
            "#ddlExternalSubExamination",
            '/GradeBook/GradeBookSetup/GetExternalSubExaminationList',
            { ExternalExaminationId: ExternalExaminationId.join(',') },
            "",
            ""
        );
        $('#ddlExternalSubExamination').val(SubExternalExaminationIds);
        $('#ddlExternalSubExamination').selectpicker('refresh');
    }
}); 