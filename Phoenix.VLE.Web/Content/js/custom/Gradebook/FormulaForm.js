﻿var formulaForm = function () {
    var init = function () {
        loadFormulaGrid();
    },
        assessments = [],
        appendToTextbox = function (button, isAssessment) {
            var textElement = $('#formulaText');
            var formulaPlaceHolder = $('#formulaPlaceHolder');
            var value = textElement.val();
            var formulaDesc = $('#FormulaDescription').val();
            var NewValue = $(button).data('value');
            switch (NewValue.toString().toLowerCase()) {
                case "c":
                    textElement.val('');
                    $('#FormulaDescription').val('');
                    formulaForm.assessments = [];
                    break;
                case "d":
                    var splitValues = value.split(' ');
                    if (formulaForm.assessments.some(x => x == NewValue)) {
                        formulaForm.assessments = formulaForm.assessments.filter(x => x != NewValue);
                    }
                    splitValues.pop();
                    textElement.val(splitValues.join(' '));
                    var splitV = $('#FormulaDescription').val().split(' ');
                    splitV.pop();
                    $('#FormulaDescription').val(splitV.join(' '));
                    break;
                default:
                    var val = parseInt(NewValue);
                    if (isNaN(val)) {
                        value += " " + NewValue;
                        formulaDesc += " " + $(button).data('key');
                    } else {
                        value += NewValue;
                        formulaDesc += $(button).data('key');
                    }
                    textElement.val(value);
                    $('#FormulaDescription').val(formulaDesc);
                    break;
            }

            var formVal = textElement.val().replaceAll(" ", "");
            placeHolders.forEach(x => formVal = formVal.replaceAll(x, '5'));
            formulaPlaceHolder.attr("data-formula", formVal).data("formula", formVal);


            if (isAssessment) {
                if (!formulaForm.assessments.some(x => x == NewValue)) {
                    formulaForm.assessments.push(NewValue);
                }
            }
            //var formulaId = parseInt($('#FormulaId').val())
            //if (formulaId > 0) {
            //    $('#Assessments').val($('#formulaText').val().split(' ').filter(x => x.indexOf('$') !== -1).join(',').replaceAll('$', ''));
            //} else {
                $('#Assessments').val(formulaForm.assessments.join(','));
            //}
        },
        onSuccess = function (response) {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success) {
                if ($('#CalculatedFormulaId').length > 0) {
                    common.bindSingleDropDownByParameter('#CalculatedFormulaId', '/Gradebook/gradebooksetup/GetFormulaList', {}, '', translatedResources.selectedText);
                } else {
                    loadFormulaGrid();                    
                }
                $('#myModal').modal('hide');
            }
        },
        onFail = function () {
            globalFunctions.onFailure();
        },
        loadFormulaGrid = function () {
            var _columnData = [];
            _columnData.push(
                //{ "mData": "Formula", "sTitle": translatedResources.formula, "sWidth": "40%" },
                { "mData": "FormulaDescription", "sTitle": translatedResources.formulaDesc, "sWidth": "40%" }

            );
            //if (isPermissionToEdit.toBoolean()) {
            _columnData.push({ "mData": "Actions", "sClass": "text-center no-sort", "bSorting": false, "sTitle": translatedResources.actions, "sWidth": "20%" });
            //}
            initFormulaGrid('tblFormula', _columnData, '/Gradebook/GradebookSetup/LoadFormulas');
        },

        initFormulaGrid = function (_tableId, _columnData, _url) {

            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [1]
                }],
                searching: true
            };
            grid.init(settings);

            $("#tblFormula_filter").hide();
            $('#tblFormula').DataTable().search('').draw();
            $("#searchFormula").on("input", function (e) {
                e.preventDefault();
                $('#tblFormula').DataTable().search($(this).val()).draw();
            });
        },

        editFormula = function (element, formulaId) {
            //var data = Array.from($('#tblFormula').DataTable().rows().data());
            //var object = data.find(x => x.FormulaId == formulaId);
            $("#modal_Loader").html("");
            $.post('/Gradebook/Gradebooksetup/LoadFormulaForm', { FormulaId: formulaId })
                .then(response => {
                    $("#modal_heading").text(translatedResources.formula);
                    $("#modal_Loader").html(response).parent().parent().addClass('modal-lg');
                    $('#myModal').modal({ backdrop: 'static' });
                    $('#myModal').one('shown.bs.modal', function () {
                        //make input labels active if there is initial value available
                        $('textarea').each(function (element, i) {
                            //console.log($(this).val());
                            if (($(this).val() !== undefined && $(this).val().length > 0) || $(this).attr("placeholder") !== null) {
                                $(this).siblings('label').addClass('active');
                            }
                            else {
                                $(this).siblings('label').removeClass('active');
                            }
                        });
                        /* Trim spaces of inputs  */
                        trimInputValue = function (input) {
                            var trimmedValue = $(input).val().trim();
                            $(input).val(trimmedValue);
                        };

                        $(":input[type='text']").on('change', function () {
                            trimInputValue($(this));
                        });

                        $("textarea").on('change', function () {
                            trimInputValue($(this));
                        });
                    });
                });
        },
        loadFormulaForm = function (source) {
            globalFunctions.loadPopup($(source), '/Gradebook/Gradebooksetup/LoadFormulaForm', translatedResources.formula, 'processing-rule modal-lg');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
            });
        },
        validateFormula = function () {
            //$('#frmAddEditFormula').calx();
            //registered to calculate mode of the value
            $('#frmAddEditFormula').calx();
            $('#frmAddEditFormula').calx('registerFunction', 'MODE', function () {
                var numbers = Array.from(arguments);
                return Object.values(
                    numbers.reduce((count, e) => {
                        if (!(e in count)) {
                            count[e] = [0, e];
                        }



                        count[e][0]++;
                        return count;
                    }, {})
                ).reduce((a, v) => v[0] < a[0] ? a : v, [0, 0])[1];
            });
            $('#frmAddEditFormula').calx('calculate');
            var value = parseFloat($('#formulaPlaceHolder').val());
            if (isNaN(value)) {
                globalFunctions.showWarningMessage(translatedResources.invalidFormula);
                return false;
            }
            return true;
        }
    return {
        init,
        appendToTextbox,
        onSuccess,
        onFail,
        loadFormulaGrid,
        initFormulaGrid,
        editFormula,
        loadFormulaForm,
        formulaForm,
        assessments,
        validateFormula
    }
}();

$(document).on('keypress', '#formulaText', function () { return false })
    .ready(function () {
        formulaForm.init()
    });