﻿
var gradeBook = function () {
    var init = function () {
        $('.date-picker').datetimepicker({
            format: 'DD-MMM-YYYY'
        })
        $('.selectpicker').selectpicker('refresh');
        $('.paymentDue-toast').addClass('animated fadeInRight');
        $('body').on('click', '.close-toast', function () {
            $(this).parent().parent().fadeOut();
        })
        $('[data-toggle="popover-hover"]').popover({
            html: true,
            trigger: 'hover',
            placement: 'left',
            container: '#groups-popover',
        });
        $('[data-toggle="popover"]').popover({
            html: true,
            trigger: 'focus',
            placement: 'right',
            container: '#assignment-popover'
        });
        $('#breadcrumbActions').append(`
        <div class="other-actions">
                <ul class="list-pattern">                       
                    <li class="type-list">${html}</li>
                    <li class="type-list"><a href="/gradebook/gradebooksetup/formula" class="btn btn-primary m-0 rounded-4x"> <i class="fas fa-calculator mr-2"></i> ${translatedResources.viewFormula}</a></li>
                    <li class="type-list">View</li>
                    <li class="grid-view active"></li>
                    <li class="list-view"></li>
                </ul>
            </div>
        `);
        gradeBook.GetGradeBookGridView();
        gradeBook.GetGradeBookListView();
    },
        GetGradeBookGridView = () => {
            $("#gradebook-grid-view").html('');
            var curriculumId = $('#CurriculumId').val();
            var grid = new DynamicPagination("gradebook-grid-view");
            var settings = {
                url: '/GradeBook/GradeBookSetup/GetGradeBookGridView?curriculumId=' + curriculumId
            };
            grid.init(settings);
        },
        BindGradeBookGridByFilter = () => {
            var curriculumId = $('#CurriculumId').val();
            //let url = '/GradeBook/GradeBookSetup/GetGradeBookGridView?curriculumId=' + curriculumId;
            let url = '/GradeBook/GradeBookSetup/GetGradeBookGridView?curriculumId=' + curriculumId;
            let sortBy = $("#ddlGradeBookSortBy").val();
            let searchString = $("#searchContent").val();
            let GradeIdList = [];
            let CourseIdList = [];
            let chkCourseList = $("input[name='ByCourseFilter']:checked");
            let chkGradeList = $("input[name='ByGradeFilter']:checked");
            let pageIndex = $("#PageIndexNumber").val();
            let gradeAndCourseList = [];

            $.each(chkCourseList, function () {
                CourseIdList.push($(this).val());
                gradeAndCourseList.push({
                    Id: $(this).val(),
                    Description: $(this).data('description'),
                    IsCourse: true
                });
            });
            $.each(chkGradeList, function () {
                GradeIdList.push($(this).val());
                gradeAndCourseList.push({
                    Id: $(this).val(),
                    Description: $(this).data('description'),
                    IsCourse: false
                });
            });

            let GradeIds = GradeIdList.join(",");
            let CourseIds = CourseIdList.join(",");

            $.post(url, { pageIndex, searchString, GradeIds, CourseIds, sortBy, gradeAndCourseList }, function (response) {
                $("#gradebook-grid-view").html('');
                $('#gradebook-grid-view').html(response);
                $("#ddlGradeBookSortBy").val(sortBy);
                $('.selectpicker').selectpicker('refresh');
            });
        },
        GetGradeBookListView = (GradeIds = '', CourseIds = '') => {
            $("#gradebook-list-view").html('');
            var curriculumId = $('#CurriculumId').val();
            var sortBy = $("#ddlListViewSortBy").val();
            let searchString = $("#searchListViewId").val();
            $.post("/GradeBook/GradeBookSetup/GetGradeBookListView?curriculumId=" + curriculumId, { searchString, GradeIds, CourseIds, sortBy }, function (response) {
                $("#gradebook-list-view").html('');
                $('#gradebook-list-view').html(response);
                $("#ddlListViewSortBy").val(sortBy);
                $('.selectpicker').selectpicker('refresh');
            });
        },
        DeleteGradeBookDetail = function (GradebookId) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                $.post("/GradeBook/GradeBookSetup/DeleteGradeBookDetail", { GradebookId }, function (response) {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    if (response.Success) {
                        gradeBook.BindGradeBookGridByFilter();
                        gradeBook.GetGradeBookListView();
                    }
                });
            });
        },
        loadGridAndListViewFilters = function (curriculumId) {
            $('#gridviewFilterDiv').html('');
            $('#listviewFilterDiv').html('');
            $.get("/GradeBook/GradeBookSetup/GetGradebookFilters", { curriculumId }, function (response) {
                $('#gridviewFilterDiv').html(response.gridViewFilterHtml);
                $('#listviewFilterDiv').html(response.listViewFilterHtml);
            });
        }
    return {
        init,
        GetGradeBookGridView,
        BindGradeBookGridByFilter,
        GetGradeBookListView,
        DeleteGradeBookDetail,
        loadGridAndListViewFilters
    }
}();

$(document).ready(function () {
    gradeBook.init();
});

$(document).on('click', '.list-pattern .list-view', function () {
    let gridView = $('.gradebook-grid-view');
    let listView = $('.gradebook-list-view');
    $('.list-pattern .grid-view').removeClass('active');
    $('.list-pattern .list-view').addClass('active');
    gridView.addClass('hide-item ');
    listView.removeClass('hide-item');
});
$(document).on('click', '.list-pattern .grid-view', function () {
    let gridView = $('.gradebook-grid-view');
    let listView = $('.gradebook-list-view');
    $('.list-pattern .list-view').removeClass('active');
    $('.list-pattern .grid-view').addClass('active');
    gridView.removeClass('hide-item');
    listView.addClass('hide-item');
});

$(document).on('change', '#ddlGradeBookSortBy', function () {
    gradeBook.BindGradeBookGridByFilter();
});

$(document).on('change', '#ddlListViewSortBy', function () {
    gradeBook.GetGradeBookListView();
});

$(document).on('keypress', '#searchListViewId', function (e) {
    if (e.which == 13) {
        gradeBook.GetGradeBookListView();
    }
});
$(document).on('change', '#CurriculumId', function (e) {
    var curriculumId = $(this).val();
    gradeBook.GetGradeBookGridView();
    gradeBook.GetGradeBookListView();
    gradeBook.loadGridAndListViewFilters(curriculumId);
});

$(document).on('keyup', ".searchfield.search-field.custom", function (e) {
    e.stopPropagation();
    if (e.keyCode === 13) {
        gradeBook.BindGradeBookGridByFilter();
    }
});

$(document).on('change', 'input[name="ByCourseFilter"]', function () {
    gradeBook.BindGradeBookGridByFilter();
})
$(document).on('change', 'input[name="ByGradeFilter"]', function () {
    gradeBook.BindGradeBookGridByFilter();
})
$(document).on('change', "#ddlGradeCourseFilter", function () {
    var listGrade = [];
    var listCourse = [];
    $.each($("#ddlGradeCourseFilter option:selected"), function (index, value) {
        if ($(value).hasClass('filter-grade')) {
            listGrade.push($(value).val());
        }
        else if ($(value).hasClass('filter-course')) {
            listCourse.push($(value).val());
        }
    });
    let GradeIds = listGrade.join(",");
    let CourseIds = listCourse.join(",");
    gradeBook.GetGradeBookListView(GradeIds, CourseIds);
});