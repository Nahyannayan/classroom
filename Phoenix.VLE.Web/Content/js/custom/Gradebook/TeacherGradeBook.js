﻿var list = [];
var teacherGradebook = function () {
    var init = function () {
        //updateNameAttr();
        //when loaded for after any rule is setup the update the cell reference to formula
        updateFormulaForRuleSetup();
    },
        //used to store the reference of preview selected element for copy row feature
        previousElement = undefined,
        //method is used to load add new row form in model popup
        addNewRow = function (source) {

            var object = getParametersForAddEditRow(source);

            $.post(`/Gradebook/Gradebook/LoadAddNewRowForm`, object)
                .then(response => {
                    $('#addNewRowModel').html(response);
                    var isEdit = parseInt($('#InternalAssessmentId').val()) > 0;
                    $('#addRowPopup').find('h6.modal-title').text(isEdit ? translatedResources.editRow : translatedResources.addRow);
                    $('#addRowPopup').modal({ backdrop: 'static' });
                    $('#addNewRowModel').find('.selectpicker').selectpicker('refresh');
                });
        },
        getParametersForAddEditRow = function (source) {
            source = $(source)
            var parentId = source.data('rowid');

            //if parentid is null the this edit form 
            if (!globalFunctions.isValueValid(parentId)) {
                parentId = source.data('parentassessmentid');
            }
            var studentList = [];
            $.each($("input[id^='StudentId_']"), function () {
                studentList.push($(this).val());
            });
            var object = {
                id: parentId,
                studentIds: studentList,
                internalAssessmentId: source.data('internalassessmentid')
            }
            return object;
        },
        onAddNewRowSaveSuccess = function (response) {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            var assessmentId = $('#ParentInternalAssessmentId').val();
            if (response.Success) {

                var isEdit = parseInt($('#InternalAssessmentId').val()) > 0;

                if (isEdit) {
                    onEditNewRowSaveSuccess(response);
                } else {
                    //these values are fetched from the current visible add new row form 
                    var columnName = $("#RowName").val();
                    var assessmentId = $('#ParentInternalAssessmentId').val();
                    var ruleType = $('#RowType').val()
                    //append the entered row and its tr
                    $(`#assessmentListItem${assessmentId} ul.inner-sub-tree`)
                        .append(`
                        <li data-internalAssessmentId="${response.InsertedRowId}" class="${ruleType == 3 ? '' : "isMarkAndDropdown"}">
                            <span class="text-truncate" data-toggle="tooltip" data-original-title="${columnName}" title="${columnName}">${columnName} </span>
                            <div class="delete-row" data-parentassessmentid="${assessmentId}" data-internalassessmentid="${response.InsertedRowId}">
                                <img src="/Content/VLE/img/svg/pencil-big.svg" alt="edit-icon" data-parentassessmentid="${assessmentId}" data-internalAssessmentId="${response.InsertedRowId}" class="cursor-pointer edit-teacher-assessment" />
                                <img src="/Content/VLE/img/svg/delete.svg" alt="delete-icon" data-parentassessmentid="${assessmentId}" data-internalAssessmentId="${response.InsertedRowId}" class="cursor-pointer delete-teacher-assessment" />                                    
                            </div>
                        </li>
                    `);
                    //append response html to respective row 
                    $(`.assessment3[data-id="${assessmentId}"]:last`).after(response.RelatedHtml);
                    $(`.assessment3[data-id="${assessmentId}"]:last`).removeClass('hide-row');
                }

                //to update the cell reference to the formula placeholder
                teacherGradebook.init();

                $('#frmAddNewRow').find('button.resetBtn').click();
                $('#InternalAssessmentId').val('0');
                $('.selectpicker').selectpicker('refresh');
                autoSetupRuleProcessingSetupToAverage(assessmentId);
            }
        },
        onEditNewRowSaveSuccess = function (response) {
            //these values are fetched from the current visible add new row form 
            var rowName = $("#RowName").val();
            var assessmentId = $('#ParentInternalAssessmentId').val();
            var teacherInternalAssessmentId = $('#InternalAssessmentId').val();
            //update the teacher internal assessment column name if change
            var $li = $(`#assessmentListItem${assessmentId} ul.inner-sub-tree li[data-internalAssessmentId="${teacherInternalAssessmentId}"]`);
            $li.find('span[data-toggle="tooltip"]').attr("data-original-title", rowName).attr('title', rowName).text(rowName);
            //update response html to respective row 
            $(`tr.assessment3[data-id="${assessmentId}"][data-internalassessmentid="${teacherInternalAssessmentId}"]`).html($(response.RelatedHtml).html());
        },
        onAddEditNewRowSaveFail = function () {
            globalFunctions.onFailure();
        },
        updateNameAttr = function () {
            var studentCount = $('.teacher-image').length;
            $('tr.sibling-row.assessment3').each(function (i, e) {
                // to find the cell count of current row
                var count = i * studentCount;
                //iterate through cells to update the name attribute using index
                $(e).find('td.internalAssessmentTD').each(function (index, element) {
                    $(element).find('input,select,textarea').each(function (ind, ele) {
                        //var nameAttr = $(ele).attr("name");
                        //var cellField = $(ele).attr("data-cellfield");
                        //var cell = $(ele).attr("data-cell");
                        //var id = ele.id;
                        //var newindex = `_${index}_`;
                        //if (nameAttr) {                            
                        //    nameAttr = nameAttr.replace(index, count);
                        //    id = id.replace(newindex, count);                            
                        //    $(ele).attr("name", nameAttr);
                        //    $(ele).attr("id", id);                            
                        //}
                        //if (cellField) {
                        //    cellField = cellField.replace(newindex, count);
                        //    $(ele).attr("data-cellfield", cellField);
                        //    $(ele).data("cellfield", cellField);
                        //}
                        //if (cell) {
                        //    cell = cell.replace(newindex, count);
                        //    $(ele).attr("data-cell", cell);
                        //    $(ele).data("cell", cell);
                        //}

                        if ($(ele).data('formula')) {
                            showCalculatedLabel($(ele), 0);
                        }
                    })
                    count++;
                })
            });
            $('.selectpicker').selectpicker('refresh');
        },
        //used to show grading template for calculated field
        showCalculatedLabel = function (element, value) {
            if (!isNaN(value)) {
                //get the grading template hidden fields
                var maxValue = 0;
                var elements = element.parent().parent().find('input[id^="gradingTemplate"]');
                var maxValueArray = Array.from(elements.map(function (e) { return parseFloat($(this).attr("data-scoreto")) })).sort((a, b) => b - a);
                if (maxValueArray.length > 0) {
                    maxValue = maxValueArray[0];
                }
                elements.each(function (i, a) {
                    var e = $(a);
                    //check value for range 
                    if ((parseFloat(e.data("scorefrom")) <= value && value <= parseFloat(e.data("scoreto")))) {
                        var span = element.next('span');
                        span = $(span);
                        var image = e.data('image');
                        //if grading template has image then show image else label with color
                        if (image && isNaN(image) && image.includes('.png')) {
                            span.html(`<img style='height:20px;' src='${image}' />`);
                        } else {
                            span.text(e.data('label'));
                            span.css('color', e.data('gradingcolor'));
                        }
                        //value = parseFloat(value.toFixed(2));
                        span.attr("data-original-title", value);
                        span.next('span').text(value);
                    }
                })
            }
        },
        //used to load subsection of assessment type
        loadSubSections = function (assessmentType) {
            var studentList = [];
            //student list is used to identify number of td's
            $.each($("input[id^='StudentId_']"), function () {
                studentList.push($(this).val());
            });
            var schoolGroupId = $('#SchoolGroupId').val();
            if ($(`#collapseTwo${assessmentType} ul.sub-tree li`).length <= 0) {
                $.post('/Gradebook/Gradebook/GetSubSectionAndRow', { schoolGroupId, assessmentType, studentIds: studentList })
                    .then(response => {
                        $(`#collapseTwo${assessmentType} ul.sub-tree`).html(response.subSection);
                        $(`tr.headTr.assessment${assessmentType}`).after(response.subRow);

                        //called to update the formula references and show grading template for calculated field
                        teacherGradebook.init();
                        setTimeout(x => $('.selectpicker').selectpicker('refresh'), 500);
                    });
            }
        },
        loadRuleSetupForm = function (source, internalAssessmentId) {
            globalFunctions.loadPopup($(source), '/Gradebook/Gradebooksetup/LoadRuleSetupForm?internalAssessmentId=' + internalAssessmentId, 'Processing Rule Setup', 'processing-rule');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $('#myModal').find('.selectpicker').selectpicker('refresh');
            });
        },
        //to update the cell reference to the formula with internal assessment place holder 
        updateFormulaForRuleSetup = function () {
            var studentList = [];
            //student ids will be used to iterate over every cell
            $.each($("input[id^='StudentId_']"), function () {
                studentList.push($(this).val());
            });
            var format = '0.00';
            studentList.forEach(student => {
                $('td[data-student="' + student + '"]').each(function () {
                    var currentElement = $(this);
                    //if current element has formula then get internal assessment ids
                    if (currentElement.data('isformula') == "True") {
                        var cellchar = currentElement.data("cellchar");
                        var internalAssessments = currentElement.data("internal");
                        //get , seperated ids in array 
                        var internalAssessmentIds = getSplitValue(internalAssessments);

                        var element = currentElement.find('input.valuefield,select.valuefield');
                        var formula = currentElement.data('cell_formula');
                        var ruleType = currentElement.data('ruletype');

                        //iterate over every id to fetch cell
                        internalAssessmentIds.forEach(ia => {
                            //get cell id and replace it with internal assessment id placeholder i.e _19__
                            //for ruleType == 3 formula works as _19__:_20__

                            var cellId = $(`input[data-isadminassessment="False"][data-assessmentid="${ia}"][data-cellchar="${cellchar}"]`).data("cell");
                            //if (ruleType == 3) {
                            //    formula = formula.replace('_' + ia + '__', cellId);
                            //} else {
                            formula = formula.replace('_' + ia + '__', "$" + cellId);
                            //}
                        })
                        if (globalFunctions.isValueValid(formula)) {
                            if (element.is('select')) {
                                //var inputElement = element.closest('input[type="hidden"]');
                                element.data('formula', formula);
                                element.attr('data-formula', formula);

                                //to identify if formula is present the field is readonly
                                if (globalFunctions.isValueValid(formula)) {
                                    element.attr('disabled', "");
                                }

                                element.data('format', format);
                                element.attr('data-format', format);

                            } else {
                                element.data('formula', formula);
                                element.attr('data-formula', formula);
                                //to identify if formula is present the field is readonly
                                if (globalFunctions.isValueValid(formula)) {
                                    element.attr('disabled', "");
                                    element.addClass("disabled text-bold text-center text-primary");
                                }

                                element.data('format', format);
                                element.attr('data-format', format);

                            }
                        }
                    }
                });
            })
            //call calx to sync all the cells with formula and generate all the dependences 
            callCalx();
            studentList.forEach(student => {
                $('td[data-student="' + student + '"]').each(function () {
                    var currentElement = $(this);
                    if (currentElement.hasClass("calculatedField") && globalFunctions.isValueValid(currentElement.data("resultconsider"))) {
                        var cellchar = currentElement.data("cellchar");
                        var resultToConsider = currentElement.data("resultconsider");
                        var internalAssessmentIds = getSplitValue(resultToConsider);
                        var element = currentElement.find('input.calculatefield');
                        var formula = element.data('formularef');
                        internalAssessmentIds.forEach(ia => {
                            var cellId = $(`input[data-isadminassessment="True"][data-assessmentid="${ia}"][data-cellchar="${cellchar}"]`).data("cell");
                            if (cellId) {
                                var subCellId = $('input[data-cellfield="' + cellId + '"],select[data-cellfield="' + cellId + '"]').data('cell');
                                if (subCellId)
                                    formula = formula.replace(' $' + ia + " ", `CHECKCELLVALUE(${subCellId},'${subCellId}')`);
                                else
                                    formula = formula.replace(' $' + ia + " ", `CHECKCELLVALUE(${cellId},'${cellId}')`);
                                //if (subCellId)
                                //    formula = formula.replace(' $' + ia + " ", subCellId);
                                //else
                                //    formula = formula.replace(' $' + ia + " ",cellId);
                            }
                        })
                        formula = formula.replaceAll(' ', '');
                        if (globalFunctions.isValueValid(formula)) {
                            formula = 'SETVALAUTO(' + formula + ')';

                            element.data('formula', formula);
                            element.attr('data-formula', formula);

                            element.data('format', format);
                            element.attr('data-format', format);
                            if ($(element).data('formula')) {
                                showCalculatedLabel($(element), parseFloat($(element).val()));
                            }
                        }
                    }
                });
            });
            callCalx(true);
            setTimeout(() => $('#gradeBookDiv').calx('calculate'), 800);
        },
        callCalx = function (isRefresh) {

            if (isRefresh) {
                $('#gradeBookDiv').calx('refresh');
                return;
            }

            // if some time calx doesn't work and show call stack full kind of error then there is an circular refrence 
            //to find that out uncomment the code in config to check 
            $('#gradeBookDiv').calx(
                //{ checkCircularReference: true }
            );

            //registed to calculate best of 
            $('#gradeBookDiv').calx('registerFunction', 'BESTOF', function (args1, c) {
                var array = Object.keys(args1).map(function (e) { return { obj: e, value: args1[e] } }).filter(x => x.value !== false);
                var final = array.sort(function (a, b) {
                    return b.value - a.value;
                }).slice(0, c);
                var sum = final
                    .map(x => {
                        var value = parseFloat(x.value);
                        if (isNaN(value))
                            return 0;
                        else
                            return value;
                    })
                    .reduce(function (a, b) {
                        return a + b;
                    });
                return Math.round(sum / final.length);
            });

            //registered to set the value in the text box for in dropdown
            $('#gradeBookDiv').calx('registerFunction', 'SETVAL', function (value) {
                var element = $(this.activeCell.el);
                if (element.is('select')) {
                    value = Math.round(value);
                    var tdElement = $(element).closest('td');
                    var setValue;
                    var isOutOfRange = true;
                    element.find('option').each((i, ele) => {
                        elem = $(ele);
                        var from = elem.data('from');
                        var to = elem.data('to');
                        if (value >= from && value <= to) {
                            setValue = elem.attr("value");
                            isOutOfRange = false;
                        }
                    })
                    if (isOutOfRange) {
                        setValue = '';
                        showErrorMessage(tdElement, translatedResources.rangeValidatorMessage);
                    } else {
                        removeErrorMessage(tdElement);
                    }
                    element.val(setValue).selectpicker('refresh');
                } else if (element.is('input')) {
                    var tdElement = $(element).closest('td');
                    var isOutOfRange = true;
                    var maxValue = parseFloat(element.attr("max"))
                    var minValue = parseFloat(element.attr("min"))
                    value = Math.round(value * (maxValue / 100));
                    if (value >= minValue && value <= maxValue) {
                        isOutOfRange = false;
                    }
                    if (isOutOfRange) {
                        var message = translatedResources.marksValidation.replace('{0}', minValue).replace('{1}', maxValue);
                        showErrorMessage(tdElement, message);
                    } else {
                        removeErrorMessage(tdElement);
                    }
                }
                return value;
            });
            // this is used to show grading template for the calculated field value
            $('#gradeBookDiv').calx('registerFunction', 'SETVALAUTO', function (value) {
                var element = $(this.activeCell.el);
                value = Math.round(value);
                showCalculatedLabel(element, value);
                return value;
            });

            $('#gradeBookDiv').calx('registerFunction', 'CHECKCELLVALUE', function (value, cellId) {
                var element = $(`[data-cell="${cellId}"`);
                //if (element.is('select')) {
                //    value = Math.round(value);
                //} else
                if (element.is('input') && globalFunctions.isValueValid(element.data("max"))) {
                    var maxValue = parseFloat(element.data("max"))
                    value = Math.round(value / maxValue * 100);
                }
                return value;
            });

            //registered to calculate mode of the value
            $('#gradeBookDiv').calx('registerFunction', 'MODE', function () {
                var numbers = Array.from(arguments);
                return Object.values(
                    numbers.reduce((count, e) => {
                        if (!(e in count)) {
                            count[e] = [0, e];
                        }
                        count[e][0]++;
                        return count;
                    }, {})
                ).reduce((a, v) => v[0] < a[0] ? a : v, [0, null])[1];
            });
        },
        //after the response of rule processing setup success apply the formula to all the element associated to 
        //assessment and resync calx cells 
        onRuleSetupSuccess = function (response, isAuto) {
            if (isAuto === undefined) {
                globalFunctions.showMessage(response.NotificationType, response.Message);
            }
            if (response.Success) {
                var formula = response.RelatedHtml;
                var rowId = response.InsertedRowId;
                var internal = response.Identifier;
                var ruleProcessingSetupId = response.CssClass

                var element = $(`td[data-internalassessmentid="${rowId}"][data-isformula="True"]`);
                element.data("cell_formula", formula).attr("data-cell_formula", formula);
                element.data("internal", internal).attr("data-internal", internal);
                element.data("ruletype", rowId).attr("data-ruletype", rowId);

                $(`#assessmentListItem${rowId}`).data('ruleprocessingsetupid', ruleProcessingSetupId);
                $('#myModal').modal('hide');
                updateFormulaForRuleSetup();
            }
        },
        getSplitValue = function (value) {
            return isNaN(value) && value.includes(',') ? value.split(',') : [value];
        },
        showErrorMessage = function (tdElement, Message) {
            tdElement.addClass('error-per');
            tdElement.attr('data-invalid', 'true');
            tdElement.data('invalid', 'true');
            tdElement.append(`<i class="active d-flex fa-info-circle fas mt-2 position-absolute text-danger top-0" 
                data-toggle="tooltip" data-placement="top" title=""
                data-original-title="${Message}"></i>`);
        },
        removeErrorMessage = function (tdElement) {
            tdElement.removeClass('error-per');
            tdElement.removeAttr('data-invalid');
            tdElement.removeData('invalid');
            tdElement.find('i').remove();
        },
        enterFullScreen = function (elem) {
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) { /* Firefox */
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
                elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) { /* IE/Edge */
                elem.msRequestFullscreen();
            }
        },
        exitFullScreen = function () {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) { /* Firefox */
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) { /* IE/Edge */
                document.msExitFullscreen();
            }
        },
        setScroolBarToTable = function () {
            $(".inner-right-table").mCustomScrollbar({
                autoExpandHorizontalScroll: true,
                autoHideScrollbar: true,
                axis: "x",
                setWidth: $('.gradebook-wrapper').width() - ($('.outer-table td:first').width() + 45),
                //scrollbarPosition: "outside"
                advanced: {
                    updateOnContentResize: true,
                    updateOnImageLoad: true
                }
            });
        },
        removeRuleProcessingSetup = function (ruleSetupId, internalAssessmentId, isAuto) {
            var token = $('input[name="__RequestVerificationToken"]').val();
            $.post('/Gradebook/GradebookSetup/DeleteRuleSetup', { ruleSetupId, internalAssessmentId, __RequestVerificationToken: token })
                .then(function (response) {
                    if (response.Success) {

                        $('#myModal').modal('hide');

                        if (isAuto === undefined) {
                            globalFunctions.showMessage(response.NotificationType, translatedResources.deletedSuccess);
                        }

                        var rowId = response.InsertedRowId;
                        var currentElement = $(`td[data-internalassessmentid="${rowId}"][data-isformula="True"]`);
                        currentElement.data("cell_formula", "").attr("data-cell_formula", "");
                        currentElement.data("internal", "").attr("data-internal", "");
                        var element = currentElement.find('input.valuefield,select.valuefield');
                        element.removeAttr('disabled');
                        element.removeClass("disabled text-bold text-primary");
                        element.removeData('formula').removeData('cell');
                        element.removeAttr('data-formula').removeAttr('data-cell');
                        if (element.is('select')) {
                            element.selectpicker('refresh');
                        }
                        updateFormulaForRuleSetup();
                    } else {
                        globalFunctions.showErrorMessage(translatedResources.technicalError)
                    }
                })
        },
        removeAssessmentFromGradebook = function (response) {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success) {

                var $div = $('div[data-internalassessmentid="' + response.InsertedRowId + '"]');
                $div.closest('li').remove();
                $('td[data-current="' + response.InsertedRowId + '"]').parent().remove();

                var parentInternalAssessmentId = $div.data('parentassessmentid');

                autoSetupRuleProcessingSetupToAverage(parentInternalAssessmentId);
            }
        },
        forceDeleteRecords = function (internalAssessmentId) {
            globalFunctions.notyDltConfirm(translatedResources.duplicateRecords, () => {
                var token = $('input[name="__RequestVerificationToken"]').val();
                $.post('/Gradebook/Gradebook/DeleteAssessment', {
                    internalAssessmentId: internalAssessmentId,
                    forceDelete: true,
                    __RequestVerificationToken: token,
                })
                    .then(response => {
                        teacherGradebook.removeAssessmentFromGradebook(response);
                    })
            });
        },
        autoSetupRuleProcessingSetupToAverage = function (parentInternalAssessmentId) {
            var $li = $(`#assessmentListItem${parentInternalAssessmentId}`);
            var allowtoAddRow = $li.data('allowtoaddrow').toBoolean();
            //check if not enabled then setup rule
            if (!$li.data("isruleprocessingsetupenabled").toString().toBoolean() && allowtoAddRow) {
                var token = $('input[name="__RequestVerificationToken"]').val();
                var ruleProcessingSetupId = parseInt($li.data('ruleprocessingsetupid'));
                var teacherInternalAssessmentIds = Array.from($li.find('ul.inner-sub-tree li.isMarkAndDropdown').map(function () { return $(this).data('internalassessmentid') }));
                if (teacherInternalAssessmentIds.length > 1) {
                    var formula = `SETVAL(AVERAGE(_${teacherInternalAssessmentIds.join('__,_')}__))`
                    var teacherInternalAssessment = teacherInternalAssessmentIds.map(function (e) { return { "SubInternalAssessmentId": e } });
                    var ruleSetupJson = {
                        "ProcessingRuleSetupId": ruleProcessingSetupId,
                        "InternalAssessmentId": parentInternalAssessmentId,
                        "CalculationTypeId": 1,
                        "BestOfCount": 0,
                        "Formula": formula,
                        "IsDelete": false,
                        "RuleSetupMappings": teacherInternalAssessment
                    };

                    $.post('/Gradebook/GradebookSetup/RuleSetupForm', { gradebookRules: ruleSetupJson, __RequestVerificationToken: token })
                        .then(function (response) {
                            onRuleSetupSuccess(response, true);
                        })
                } else {
                    removeRuleProcessingSetup(ruleProcessingSetupId, parentInternalAssessmentId, true);
                }                
            }
        }
    return {
        init,
        addNewRow,
        onAddNewRowSaveSuccess,
        onEditNewRowSaveSuccess,
        onAddEditNewRowSaveFail,
        updateNameAttr,
        showCalculatedLabel,
        loadSubSections,
        previousElement,
        loadRuleSetupForm,
        updateFormulaForRuleSetup,
        onRuleSetupSuccess,
        callCalx,
        getSplitValue,
        showErrorMessage,
        removeErrorMessage,
        enterFullScreen,
        exitFullScreen,
        setScroolBarToTable,
        getParametersForAddEditRow,
        removeRuleProcessingSetup,
        removeAssessmentFromGradebook,
        forceDeleteRecords,
        autoSetupRuleProcessingSetupToAverage
    }
}()

$(document)
    .on('click', 'input[id^="switch"]', function () {
        var assessment = $(this).data('assessment');
        if ($(this).prop('checked')) {
            if (!$('.sibling-row.assessment' + assessment).hasClass("hide-row"))
                $('.sibling-row.assessment' + assessment).addClass("hide-row");
            $('.card.assessment' + assessment).addClass("hide-row");
            $('.headTr.assessment' + assessment).addClass("hide-row");
        } else {
            if ($('.sibling-row.assessment' + assessment).hasClass("hide-row"))
                $('.sibling-row.assessment' + assessment).removeClass("hide-row");
            $('.card.assessment' + assessment).removeClass("hide-row");
            $('.headTr.assessment' + assessment).removeClass("hide-row");
        }
    })
    .on('click', '.add-row,.edit-teacher-assessment', function () {
        teacherGradebook.addNewRow(this);
    })
    .on('change', '.rowTypeAddnewRow', function () {
        var dropdownValue = $(this).val();
        if ($(this).is('select')) {
            $('div[id^="RowType"]').hide();
            if (dropdownValue)
                $('#RowType' + dropdownValue).show();


            $('div[id^="RowType"]').find('input,select').val('');
            $('div[id^="RowType"]').find('select').selectpicker('refresh');
        }
    })
    .on('click', '#fullScreen', function () {
        var elem = $('#gradeBookDiv');
        var table = elem.find('table.outer-table');
        if (elem.hasClass('fullscreen')) {
            elem.removeClass('fullscreen m-0');
            table.removeClass("table-responsive");
            elem.height("unset");
            elem.width("unset");
            //table.mCustomScrollbar('destroy');
        } else {
            elem.addClass('fullscreen m-0');
            table.addClass("table-responsive");
            var width = $('body').outerWidth();
            var height = $('body').outerWidth();
            var contentHeight = height - elem.find('section.d-flex').outerHeight();
            elem.height(height);
            elem.width(width);
            //table.mCustomScrollbar({
            //        setHeight: "80vh",
            //        autoExpandScrollbar: true,
            //        autoHideScrollbar: true,
            //        scrollbarPosition: "inside"
            //    });
        }
        teacherGradebook.setScroolBarToTable();
    })
    .on('click', '#saveGradeBook', function () {
        //$('input.valuefield').trigger('keyup');
        var token = $('input[name="__RequestVerificationToken"]').val();
        //to check if any cell is invalid then navigate to it
        if ($('td[data-invalid="true"]').length > 0) {
            $(".inner-right-table").mCustomScrollbar("scrollTo", $('td[data-invalid="true"]:first'));
            globalFunctions.showWarningMessage(translatedResources.EnterValidMarks);
            return;
        }
        var data = [];
        $('tr.sibling-row.assessment3').each(function (i, e) {
            $(e).find('td.internalAssessmentTD').each(function (index, element) {
                var object = {};
                $(element).find('input,select,textarea').each(function (ind, ele) {
                    var nameAttr = $(ele).attr("name");
                    if (globalFunctions.isValueValid(nameAttr)) {
                        var propertyName = nameAttr.split('.')[1];
                        object[propertyName] = $(ele).val();
                    }
                })
                data.push(object);
            })
        });
        // these filters are added to avoid null/0 values to get inserted in table 
        data = data.filter(value => Object.keys(value).length !== 0).filter(x => x.Score != "" /*&& x.Score != "0"*/);
        if (data.length > 0) {
            $.post('/Gradebook/Gradebook/SaveInternalAssessmentData', {
                __RequestVerificationToken: token,
                assessmentScores: data
            }).then(response => {
                globalFunctions.showMessage(response.NotificationType, response.Message);
            });
        }
    })
    .on('keyup change', 'input.valuefield', function (e) {
        var currentElement = $(this);
        if (globalFunctions.isValueValid(currentElement.val())) {
            var maxValue = parseFloat(currentElement.attr("max"));
            var minValue = parseFloat(currentElement.attr("min"));
            var currentValue = parseFloat(currentElement.val());
            var message = translatedResources.marksValidation.replace('{0}', minValue).replace('{1}', maxValue);
            var td = currentElement.parent();
            if (currentValue < minValue || currentValue > maxValue || isNaN(currentValue)) {
                teacherGradebook.showErrorMessage(td, message);
            } else {
                teacherGradebook.removeErrorMessage(td);
            }
        }
    })
    .on('change', '.valuefield', function (e) {
        var currentElement = $(this);
        //if (currentElement.is('select') && currentElement.find('select').length > 0) {
        //    teacherGradebook.previousElement = currentElement.find('select');
        //} else {
        if (!currentElement.is('div')) {
            teacherGradebook.previousElement = currentElement;
        }
        //}
        if (currentElement.is('select')) {
            var score = currentElement.find('option:selected').data('score');
            var field = currentElement.data('cellfield');
            var setElement = $('#' + field);
            var isParent = setElement.attr("data-isadminassessment").toBoolean();

            //to check if cell is under rule processing setup and individual
            var isCell = $('input[data-cellfield="' + setElement[0].id + '"],select[data-cellfield="' + setElement[0].id + '"]').data('cell');
            if (isParent && isCell) {
                setElement.val(score);
            } else {
                setElement.val(score).trigger('calx.setValue').trigger('calx.calculateCellDependant');
            }

            $('#' + field).val(score).trigger('calx.setValue').trigger('calx.calculateCellDependant');
        } else if (currentElement.is('input')) {
            var maxValue = parseFloat(currentElement.attr("max"));
            var minValue = parseFloat(currentElement.attr("min"));
            var currentValue = parseFloat(currentElement.val());

            var field = currentElement.data('cellfield');
            if (!isNaN(maxValue) && !isNaN(currentValue) && !isNaN(minValue)) {
                var score;
                var setElement = $('#' + field);
                //to verify weather the cell is admin internal assessment
                var isParent = setElement.attr("data-isadminassessment").toBoolean();
                if (isParent) {
                    score = currentValue;
                } else {
                    score = (currentValue / maxValue) * 100;
                }
                //to check if cell is under rule processing setup and individual
                var isCell = $('input[data-cellfield="' + setElement[0].id + '"],select[data-cellfield="' + setElement[0].id + '"]').data('cell')

                if (isParent && isCell) {
                    setElement.val(score)//.trigger('calx.setValue').trigger('calx.calculateCellDependant');
                } else {
                    setElement.val(score).trigger('calx.setValue').trigger('calx.calculateCellDependant');
                }

            }
        }
    })
    .on('click', '#copyCurrentCell', function () {
        if (teacherGradebook.previousElement) {
            var currentValue = $(teacherGradebook.previousElement).val();
            if (!globalFunctions.isValueValid(currentValue) || currentValue == "0" || currentValue == "") {
                globalFunctions.showWarningMessage(translatedResources.copyValidation);
                return;
            }
            var count = 0;
            var inputType = teacherGradebook.previousElement.prop('tagName').toLowerCase();
            $(teacherGradebook.previousElement).closest('tr').children().each(function (i, e) {
                var element = $(e).find(inputType + '.valuefield');
                var value = $(element).val();
                if (!globalFunctions.isValueValid(value) || value == 0) {
                    $(element).val(currentValue).trigger('change');
                    count++;
                }
            });
            if (teacherGradebook.previousElement.is('select') && count > 0) {
                $('.selectpicker').selectpicker('refresh');
            }
            if (count > 0) {
                let currentElement = $(this);
                currentElement.addClass('active');
                setTimeout(() => {
                    currentElement.removeClass('active');
                    $('.checkmark').toggle();
                    $('#gradeBookDiv').calx('calculate')
                }, 1000);

                setTimeout(() => {
                    $('.checkmark').toggle();
                }, 600);
            }

            if (count > 1) {
                var message = translatedResources.cellsUpdated.replace('{0}', count);
                globalFunctions.showSuccessMessage(message)
            } else if (count == 1) {
                globalFunctions.showSuccessMessage(translatedResources.cellUpdated)
            } else if (count == 0) {
                globalFunctions.showSuccessMessage(translatedResources.noCellUpdated)
            }

        } else {
            globalFunctions.showWarningMessage(translatedResources.noCellSelected);
        }
    })
    .on('click', '.delete-teacher-assessment', function () {
        var internalAssessmentId = $(this).attr("data-internalassessmentid");
        var parentId = $(this).attr("data-parentassessmentid");
        var internalAssessment = $(`td[data-internalassessmentid="${parentId}"][data-isformula="True"]`).data("internal");
        //to validate if row is in rule processing then avoid delete and show warning message
        var ids = teacherGradebook.getSplitValue(internalAssessment);
        if (ids.some(x => x == internalAssessmentId) && $(`#assessmentListItem${parentId}`).data("isruleprocessingsetupenabled").toString().toBoolean()) {
            globalFunctions.showWarningMessage(translatedResources.ruleSetupConfirmMessage);
        } else {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                var token = $('input[name="__RequestVerificationToken"]').val();
                $.post('/Gradebook/Gradebook/DeleteAssessment', {
                    internalAssessmentId: internalAssessmentId,
                    __RequestVerificationToken: token,
                })
                    .then(response => {
                        if (response.Success) {
                            teacherGradebook.removeAssessmentFromGradebook(response);
                        } else if (response.Message == translatedResources.duplicateRecords) {
                            teacherGradebook.forceDeleteRecords(internalAssessmentId)
                        }
                    })
            });
        }


    })
    .keyup(function (e) {
        if (e.keyCode == 27) {
            if ($('#gradeBookDiv').hasClass('fullscreen')) {
                $('#fullScreen').trigger('click');
            }
        }
    });