﻿$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.date-picker').datetimepicker({
        format: 'YYYY-MM-DD',
        widgetPositioning: {
            vertical: 'bottom'
        }
    });
})
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches
var animateNext = function (reference) {
    if (animating) return false;
    animating = true;
    current_fs = $(reference).parent();
    next_fs = $(reference).parent().next();
    next_fs.show();
    current_fs.animate({ opacity: 0 }, {
        step: function (now, mx) {
            scale = 1;
            left = (now * 10) + "%";
            opacity = 1 - now;
            current_fs.css({
                'transform': 'scale(' + scale + ')',
                'position': 'absolute'
            });
            next_fs.css({ 'left': left, 'opacity': opacity });
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        easing: 'easeInOutBack'
    });
}
var animatePrev = function (reference) {
    if (animating) return false;
    animating = true;
    current_fs = $(reference).parent();
    previous_fs = $(reference).parent().prev();
    previous_fs.show();
    current_fs.animate({ opacity: 0 }, {
        step: function (now, mx) {
            scale = 1;
            left = ((1 - now) * 10) + "%";
            opacity = 1 - now;
            current_fs.css({
                'left': left
            });
            previous_fs.css({
                'transform': 'scale(' + scale + ')',
                'opacity': opacity
            });
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        easing: 'easeInOutBack'
    });
}
$(".next").click(function () {
    switch ($(this).parent()[0].id) {
        case "StudentIdDiv":
            if (ValidateStudentIdDiv()) {
                $.ajax({
                    url: '/Account/PostForgetUserName',
                    type: 'POST',
                    datatype: "json",
                    data: {
                        studentId: $("#StudentID").val(),
                        dateOfBirth: $("#dob").val()
                    }
                }).done(function (data, status, jqXHR) {
                    //todo:add logic here
                    var responseJson = data;
                    if (responseJson) {
                        if (responseJson.message) {
                            $("#lblMessege").text(responseJson.message);
                        }
                        //$("#lblMessege").css("display", "block");
                        if (responseJson.success) {
                            if (responseJson.success == "true") {
                                animateNext($('.next')[0]);
                                $("#lblMessege").css("display", "none");
                            } else {
                                $("#lblMessege").css("display", "block");
                            }
                        }
                    }
                }).fail(function (jqXHR, status, err) {
                    var responseJson = jqXHR.responseJSON;
                    if (responseJson) {
                        if (responseJson.message) {
                            $("#lblMessege").text(responseJson.message);
                        }
                        //$("#lblMessege").css("display", "block");
                        if (responseJson.success) {
                            if (responseJson.success == "true") {
                                animateNext($('.next')[0]);
                                $("#lblMessege").css("display", "none");
                            } else {
                                $("#lblMessege").css("display", "block");
                            }
                        }
                    }
                }).always(function () {
                    $(".next").removeClass("disabled");
                })
            }
            break;
        case "OTPDiv":
            if (ValidateOTPDiv()) {
                $.ajax({
                    url: '/Account/GetValidateUserName',
                    type: 'POST',
                    data: {
                        ParentUserName: $("#parentUsername").val(),
                        otp: $("#otp").val()
                    }
                }).done(function (data, status, jqXHR) {
                    var responseJson = data;
                    if (responseJson) {
                        if (responseJson.message) {
                            $("#lblMessege").text(responseJson.message);
                        }
                        if (responseJson.isValidUser) {
                            animateNext($('.next')[1]);
                            $("#lblMessege").css("display", "none");
                        }
                        else {
                            $("#lblMessege").css("display", "block");
                        }
                    }
                }).fail(function (jqXHR, status, err) {
                    var responseJson = jqXHR.responseJSON;
                    if (responseJson) {
                        if (responseJson.message) {
                            $("#lblMessege").text(responseJson.message);
                        }
                        if (responseJson.isValidUser) {
                            animateNext($('.next')[1]);
                            $("#lblMessege").css("display", "none");
                        }
                        else {
                            $("#lblMessege").css("display", "block");
                        }
                    }
                }).always(function () {
                    $(".next").removeClass("disabled");
                })
            }
            break;
        default:
            break;
    }
});
$(".previous").click(function () {
    $("#lblMessege").css("display", "none");
    $("#" + $(this).parent()[0].id + " .md-form input").val('');
    $("#" + $(this).parent()[0].id + " .md-form small").css("display", "none");
    $("#" + $(this).parent()[0].id + " .md-form label").removeClass("active");
    switch ($(this).parent()[0].id) {
        case "StudentIdDiv":
            animatePrev(this);
            break;
        case "OTPDiv":
            animatePrev(this);
            break;
        case "NewPasswordDiv":
            animatePrev(this);
            break;
        default:
            break;
    }
});
$(".submit").click(function () {
    return false;
});
function ValidateStudentIdDiv() {
    var isValid = true;
    if ($("#StudentID").val().length === 0) {
        ShowElement("StudentID");
        isValid = false;
        $(".next").removeClass("disabled");
    }
    if ($("#dob").val().length === 0) {
        ShowElement("dob");
        isValid = false;
        $(".next").removeClass("disabled");
    }
    return isValid;
}
function ValidateOTPDiv() {
    var isValid = true;
    if ($("#parentUsername").val().length === 0) {
        ShowElement("parentUsername");
        isValid = false;
        $(".next").removeClass("disabled");
    }
    if ($("#otp").val().length === 0) {
        ShowElement("otp");
        isValid = false;
        $(".next").removeClass("disabled");
    }
    return isValid;
}
function ValidatePasswordDiv() {
    var isValid = true;
    if ($("#newPassword").val().length === 0) {
        ShowElement("newPassword");
        isValid = false;
        $("#btnUpdate").removeClass("disabled");
    }
    if ($("#confirmPassword").val().length === 0) {
        ShowElement("confirmPassword");
        isValid = false;
        $("#btnUpdate").removeClass("disabled");
    }
    if ($("#confirmPassword").val() != $("#newPassword").val()) {
        $("#confirmPasswordValidation").text(PasswordNotMatch);
        ShowElement("confirmPassword");
        isValid = false;
        $("#btnUpdate").removeClass("disabled");
    }
    return isValid;
}
function ShowElement(Id) {
    $("#" + Id + "Validation").css("display", "block");
}
function HideElement(Id) {
    $("#" + Id + "Validation").css("display", "none");
}
$("#btnUpdate").on("click", function () {
    if (ValidatePasswordDiv()) {
        $.ajax({
            url: '/Account/PasswordChange',
            type: 'POST',
            data: {
                Password: $("#newPassword").val(),
                UserName: $("#parentUsername").val()
            }
        }).done(function (data, status, jqXHR) {
            var responseJson = data;
            if (responseJson) {
                if (responseJson.bPasswordChanged) {
                    $('#NewPasswordDiv .cancelForgotPass').click();
                    if (responseJson.Message) {
                        $("#lblMainMessege").text(responseJson.Message);
                        $("#lblMainMessegeDiv").css("display", "block");
                        $("#lblMainMessege").css("display", "block");
                    }
                    setTimeout(function () {
                        $('#lblMainMessegeDiv').fadeOut('fast');
                    }, 3000);
                } else {
                    if (responseJson.Message) {
                        $("#lblMessege").text(responseJson.Message);
                        $("#lblMessege").css("display", "block");
                    }
                }
            }
        }).fail(function (jqXHR, status, err) {
            var responseJson = jqXHR.responseJSON;
            if (responseJson) {
                if (responseJson.bPasswordChanged) {
                    $('#NewPasswordDiv .cancelForgotPass').click();
                    if (responseJson.Message) {
                        $("#lblMainMessege").text(responseJson.Message);
                        $("#lblMainMessegeDiv").css("display", "block");
                        $("#lblMainMessege").css("display", "block");
                    }
                    $("#lblMainMessege").css("display", "block");
                    setTimeout(function () {
                        $('#lblMainMessegeDiv').fadeOut('fast');
                    }, 3000);
                } else {
                    if (responseJson.Message) {
                        $("#lblMessege").text(responseJson.Message);
                        $("#lblMessege").css("display", "block");
                    }
                }
            }
        }).always(function () {
        })
    }
})

$("body").on("click", ".forgotPassTrigger", function () {
    $(this).closest(".lgnForm").slideUp();
    $(".forgotPassForm").slideDown();
});
$("body").on("click", ".cancelForgotPass", function () {
    $(this).closest(".forgotPassForm").slideUp();
    $(".lgnForm").slideDown();
    $(".forgotPassForm .md-form input").val('');
    $(".forgotPassForm .md-form label").removeClass("active");
    $(".forgotPassForm .md-form small").css("display", "none");
    $("#lblMessege").css("display", "none");

    switch ($(this).parent()[0].id) {
        case "StudentIdDiv":
            break;
        case "OTPDiv":
            animatePrev(this);
            break;
        case "NewPasswordDiv":
            animatePrev(this);
            setTimeout(function () {
                $('#OTPDiv .cancelForgotPass').click();
            }, 850);
            break;
        default:
            break;
    }
})

if ($('.clsValid').find('ul li').text() === "Invalid User Name or Password.") {
    if ($("#hdnUserTypeId").val() == "2") {
        $(".parent-trigger").parent(".loginBtns").slideUp();
        $(".parent-trigger").parent(".loginBtns").next(".lgnForm").slideDown();
    }
}
$("body").on("click", ".parent-trigger", function () {
    $(this).parent(".loginBtns").slideUp();
    var currentId = $(this).attr("id");
    $("#hdnUserTypeId").val(currentId);
    $(this).parent(".loginBtns").next(".lgnForm").slideDown();
})

$("body").on("click", ".cancelLogin", function () {
    $(this).closest(".lgnForm").slideToggle();
    $(this).closest(".lgnForm").prev(".loginBtns").slideToggle();
});
$("body").on("click", "#btnSubmit", function () {
    $(this).addClass("disabled");
});
$(document).ready(function () {
    $(".GFValidate").css("display", "none");
    $("#btnSubmit").removeClass("disabled");
});