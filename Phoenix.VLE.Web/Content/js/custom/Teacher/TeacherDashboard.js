﻿$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $("#siteBanner").owlCarousel({
        margin: 0,
        dots: false,
        autoplay: true,
        items: 1,
        nav: true,
        loop: true
    });

    //On click of group header section redirect to group detail page
    $('body').off('click', '.group-header').on('click', '.group-header', function (e) {
        if (e.target !== this)
            return;

        var id = $(this).data('id');
        location.href = "/SchoolStructure/SchoolGroups/GroupDetails?grpId=" + id;
    });
});

var _changeInterval = null;
$(document).on('keyup', '#searchClassList', function () {
    clearInterval(_changeInterval);
    _changeInterval = setInterval(function () {
        var SearchString = $("#searchClassList").val() != undefined ? $("#searchClassList").val() : '';
        if ($.trim(SearchString) != '') {
            $.get("/ClassList/ClassList/StudentDetailByGlobalSearch", { SearchString })
                .then(response => {
                    $('#divSearchStudent').html('');
                    $('#divSearchStudent').html(response);
                    $("#divSearchStudent ul>li").highlight(SearchString.trim());
                    $('.searchResult').removeClass('d-none');
                });
        }
        else {
            $('.searchResult').addClass('d-none');
        }
        clearInterval(_changeInterval);
    }, 1000);
});

$(document).on('click', 'body', function (e) {
    if (!$(e.target).is('.searchResult'))
        $('.searchResult').addClass('d-none');
})


jQuery.fn.highlight = function (pat) {
    function innerHighlight(node, pat) {
        var skip = 0;
        if (node.nodeType == 3) {
            var pos = node.data.toUpperCase().indexOf(pat);
            pos -= (node.data.substr(0, pos).toUpperCase().length - node.data.substr(0, pos).length);
            if (pos >= 0) {
                var spannode = document.createElement('span');
                spannode.className = 'highlight';
                var middlebit = node.splitText(pos);
                var endbit = middlebit.splitText(pat.length);
                var middleclone = middlebit.cloneNode(true);
                spannode.appendChild(middleclone);
                middlebit.parentNode.replaceChild(spannode, middlebit);
                skip = 1;
            }
        }
        else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; ++i) {
                i += innerHighlight(node.childNodes[i], pat);
            }
        }
        return skip;
    }
    return this.length && pat && pat.length ? this.each(function () {
        innerHighlight(this, pat.toUpperCase());
    }) : this;
};

jQuery.fn.removeHighlight = function () {
    return this.find("span.highlight").each(function () {
        this.parentNode.firstChild.nodeName;
        with (this.parentNode) {
            replaceChild(this.firstChild, this);
            normalize();
        }
    }).end();
};
