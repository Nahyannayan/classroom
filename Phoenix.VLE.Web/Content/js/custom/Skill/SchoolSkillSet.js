﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();
var SchoolSkillSet = function () {

    var init = function () {
        loadSchoolSkillSetGrid();
        $("#tbl-SchoolSkillSet_filter").hide();
    },

        loadCategoryPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.schoolSkillSet;
            if (mode == 'Add') {
                globalFunctions.loadPopup(source, '/Skill/SchoolSkillSet/InitAddEditSchoolSkillSetForm?SchoolGradeId=' + id, title, 'SchoolSkillSet-dailog');
            }
            else
                globalFunctions.loadPopup(source, '/Skill/SchoolSkillSet/InitAddEditSchoolSkillSetForm?SkillId=' + id, title, 'SchoolSkillSet-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
                //$("#tbl-SuggestionCategories_filter").addClass("d-none");
                $("#tbl-SchoolSkillSet_filter").hide();
                $('#tbl-SchoolSkillSet').DataTable().search('').draw();
                $("#SchoolSkillSetListSearch").on("input", function (e) {
                    e.preventDefault();
                    $('#tbl-SchoolSkillSet').DataTable().search($(this).val()).draw();

                });
            });
        },

        addCategoryPopup = function (source) {
            var id = $("#SchoolGradeId").val();
            loadCategoryPopup(source, translatedResources.add, id);
        },

        editCategoryPopup = function (source, id) {
            loadCategoryPopup(source, translatedResources.edit, id);
        },

        deleteSchoolSkillSetData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('SchoolSkillSet', '/Skill/SchoolSkillSet/DeleteSchoolSkillSetData', 'tbl-SchoolSkillSet', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        loadSchoolSkillSetGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "SkillName", "sTitle": translatedResources.schoolSkillSet, "sWidth": "50%", "sClass": "wordbreak" },
                { "mData": "CreatedByName", "sTitle": translatedResources.CreatedBy, "sWidth": "15%", "sClass": "open-details-control wordbreak" },
                { "mData": "IsApproved", "sTitle": translatedResources.IsApproved, "sWidth": "15%" },
                { "mData": "ApprovedBy", "sTitle": translatedResources.ApprovedBy, "sWidth": "10%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "10%" }
            );
            initSchoolSkillSetGrid('tbl-SchoolSkillSet', _columnData, '/Skill/SchoolSkillSet/LoadSchoolSkillSetGrid?SchoolGradeId=' + $("#SchoolGradeId").val());
        },

        initSchoolSkillSetGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 4,
                    'orderable': false
                }]
            };
            grid.init(settings);
        };

    return {
        init: init,
        addCategoryPopup: addCategoryPopup,
        editCategoryPopup: editCategoryPopup,
        deleteCategoryData: deleteSchoolSkillSetData,
        onSaveSuccess: onSaveSuccess,
        loadSchoolSkillSetGrid: loadSchoolSkillSetGrid
    };
}();

$(document).ready(function () {
    SchoolSkillSet.init();


    $(document).on('click', '#btnAddSchoolSkillSet', function () {
        SchoolSkillSet.addCategoryPopup($(this)); //"#SchoolGradeId"
    });
    //$("#tbl-SuggestionCategories_filter").addClass("d-none");
    $("#tbl-SchoolSkillSet_filter").hide();
    $('#tbl-SchoolSkillSet').DataTable().search('').draw();
    $("#SchoolSkillSetListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-SchoolSkillSet').DataTable().search($(this).val()).draw();

    });

    $(document).on("change", 'input:checkbox[name=IsApproved]', function () {
        if (this.checked) {
            $("input:checkbox[name=IsActive]").prop("checked", true);
        }
    });
    $(document).on("change", 'input:checkbox[name=IsActive]', function () {
        if (!this.checked && $("input:checkbox[name=IsApproved]").is(":checked")) {
            $("input:checkbox[name=IsApproved]").prop("checked", false);
        }
    });
});



