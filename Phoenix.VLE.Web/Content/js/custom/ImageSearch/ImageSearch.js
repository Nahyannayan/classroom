﻿$(document).ready(function () {
    var currentPage = 1, pageSize = 20, txtSearch = "";
    var pg;

    $("#btnBack").show();
    $(document).on('click', '#btnBack', function () {
        window.location.href = "/Files/Files";
    });

    $("#btnSearchImages").click(function () {
        txtSearch = $("#txtImageSearch").val();
        if (txtSearch !== undefined && txtSearch.trim() !== "") {
            if (!validateBannedWord(txtSearch)) {
                LoadFlickrImages(txtSearch, currentPage, pageSize);
            }
            else {
                globalFunctions.showWarningMessage(txtSearch + ' ' +translatedResources.validateBannedWordMessage);
            }
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.providetextmsg);
            $("#searchResult").hide();
            $("#noSearch").show();
        }
    });
    $('#txtImageSearch').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            txtSearch = $(this).val();
            if (txtSearch !== undefined && txtSearch.trim() !== "") {
                if (!validateBannedWord(txtSearch)) {
                    LoadFlickrImages(txtSearch, currentPage, pageSize);
                }
                else {
                    globalFunctions.showWarningMessage(txtSearch + ' ' + translatedResources.validateBannedWordMessage);
                }
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.providetextmsg);
                $("#searchResult").hide();
                $("#noSearch").show();
            }
        }
        event.stopPropagation();
    });
    function LoadFlickrImages(search, currentPage, pageSize) {
        $.ajax({
            url: "/Files/Files/LoadFlickrImages?search=" + search + "&currentPage=" + currentPage + "&pageSize=" + pageSize,
            type: "GET",
            async: false,
            success: function (response) {
                if (response !== "") {
                    $("#lblSearchTerm").text('"' + search + '"');
                    $("#noSearch").hide();
                    $("#searchResult").show();
                    $("#divSearch").html("");
                    $("#divSearch").html(response);

                    if (currentPage == 1) {
                        $(".image-prev").prop("disabled", true);
                    }
                    else {
                        $(".image-prev").prop("disabled", false);
                    }
                }
                else {
                   
                    $("#pagination").hide();
                    $("#searchResult").hide();
                    $("#noSearch").show();
                    $("#noSearch").html("");
                    $("#noSearch").html(translatedResources.noimagesfound);
                }
            },
            error: function (response) {
                //alert(response);
            }
        });
        $("#pagination").show();
        $(".search-result").justifiedGallery({
            rowHeight: 225,
            lastRow: 'nojustify',
            margins: 5
        });
        $(".category-wrapper").mCustomScrollbar({
            setHeight: "80",
        });
    }

    $(".image-prev").click(function () {
        if (currentPage > 1) {
            currentPage = currentPage - 1;
            LoadFlickrImages(txtSearch, currentPage, pageSize);
        }
    });
    $(".image-next").click(function () {
        currentPage = currentPage + 1;
        LoadFlickrImages(txtSearch, currentPage, pageSize);
    });

    $("body").on("click", ".saveImage", function () {
        $("#imgUrl").val($(this).attr("img-url"));
        $(this).addClass("selected");
    });

    $("#btnSaveFlickrImg").on("click", function () {
        //var imgUrl = $(this).attr("img-url");
        var imgUrl = $("#imgUrl").val();
        var imageName = $("#imgName").val();
        if (imageName !== undefined && imageName !== "") {
            
            $.ajax({
                type: 'POST',
                url: "/Files/Files/SaveFile",
                data: { 'url': imgUrl, "imgName": imageName },
                async: true,
                success: function (result) {
                    if (result.Success === true) {
                        globalFunctions.showSuccessMessage(translatedResources.successmsg);
                        $(".image-sizes li a.selected").find("i").removeClass('fa-plus').addClass('fa-check');
                        $("#imageNameSetting").modal('hide');
                        $("#imgName").val('');
                    }
                    else {
                        globalFunctions.showMessage("error", result.Message);
                    }
                },
                error: function (msg) {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        }
        else {
            globalFunctions.showErrorMessage(translatedResources.provideimagename);
        }
    });

    function validateBannedWord(txt) {
        var jsonString = JSON.stringify(_bannedWords);
        var bannedWord = globalFunctions.substringBannedWords(JSON.parse(jsonString), txt);

        return bannedWord.length > 0;
    }

});