﻿
/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var genderChartOptions;
var gradeReportChartOptions;
var quizReportFunctions = function () {

    loadGenderWiseReportChart = function (data) {
        debugger;
        if (data.length > 0 && !data.every(item => item.Value === 0)) {
            $("#GenderChartDiv").empty();
            $("#GenderChartContainer").show();
            //quizReportFunctions.bindGenderChart(data);

            quizReportFunctions.initializeGenderChartOptions(data);
            quizReportFunctions.createGenderWiseChart();
        } else {
            $("#GenderChartContainer").hide();
            $("#GenderLegendsDiv").hide();
            $("#GenderChartDiv").html("<span id='noDataSpan' class='no-data-found d-block mx-auto my-auto'>" + translatedResources.noRecordFound +"</span>");

        }
    },
        initializeGenderChartOptions = function (data) {
            if (data.length > 0) {
                $("#maleCountValue").html(data[0].Value);
                $("#femaleCountValue").html(data[1].Value);

                if (data[0].Value > 0) {
                    $("#spanMale").css('background', '#722fa5');
                    $("#spanFemale").css('background', '#e4498c');
                }
                else {
                    $("#spanMale").css('background', '#e4498c');
                    $("#spanFemale").css('background', '#722fa5');
                }

            }


            genderChartOptions = {
                size: {
                    width: 320,
                    height: 270
                },
                type: "doughnut",
                //palette: "bright",
                palette: ['#722fa5', '#e4498c'],
                //dataSource: data,
                dataSource: $.map(data, function (item) {
                    item.Value = item.Value === 0 ? null : item.Value;
                    return item;
                }),
                onExported: e => {

                    $("#GenderChartContainer").dxPieChart('dispose');
                    quizReportFunctions.createGenderWiseChart();
                },
                legend: {
                    visible: true,
                    verticalAlignment: "bottom",
                    horizontalAlignment: "center"
                },
                series: [
                    {
                        argumentField: "Name",
                        valueField: "Value",
                        label: {
                            visible: true,
                            connector: {
                                visible: true,
                                width: 1
                            },
                            position: "inside"
                        }
                    }
                ],
                tooltip: {
                    enabled: true,
                    shared: true,
                    customizeTooltip: function (info) {
                        return {
                            html: "<div class='tooltip-body'><div class='quiz-tooltip'> <span>Count : </span>" +
                                info.originalValue +
                                "</div>"
                        };
                    }
                },
                onPointClick: function (e) {
                    var point = e.target;

                    toggleVisibility(point);
                },
                onLegendClick: function (e) {
                    var arg = e.target;

                    toggleVisibility(this.getAllSeries()[0].getPointsByArg(arg)[0]);
                },
                export: {
                    enabled: true,
                    printingEnabled: false,
                    formats: ["PDF"],
                    fileName: translatedResources.GenderWiseReport
                }
            }
        },
        createGenderWiseChart = function () {
            $("#GenderChartContainer").dxPieChart(genderChartOptions).dxPieChart("instance")
            $('.dxc-legend').hide();
        },
        initializeGradeWiseReportChart = function (data) {
            gradeReportChartOptions = {
                palette: "Harmony Light",
                dataSource: data,
                onExported: e => {

                    $("#GradeChartContainer").dxChart('dispose');
                    quizReportFunctions.createGradeReportChart();
                },
                commonSeriesSettings: {
                    type: "area",
                    argumentField: "GradeDisplay",
                    point: {
                        visible: true
                    }
                },

                series: {
                    argumentField: "GradeDisplay",
                    valueField: "QuestionCount",
                    name: "Quiz Count",
                    color: '#ffaa66'
                },
                margin: {
                    bottom: 20
                },
                argumentAxis: {
                    label: {
                        overlappingBehavior: "rotate",
                        rotationAngle: -45
                    }
                },

                legend: {
                    visible: false
                },
                valueAxis: {
                    tickInterval: 1
                },
                tooltip: {
                    enabled: true,
                    shared: true,
                    customizeTooltip: function (info) {
                        return {
                            html: "<div class='tooltip-body'><div class='quiz-tooltip'> <span>Count : </span>" +
                                info.originalValue +
                                "</div>"
                        };
                    }
                },
                export: {
                    enabled: true,
                    printingEnabled: false,
                    formats: ["PDF"],
                    fileName: translatedResources.GradeWiseReport
                }
            }
        },
        createGradeReportChart = function () {
            $("#GradeChartContainer").dxChart(gradeReportChartOptions).dxChart("instance")
        },
        loadGrageWiseReportChart = function (data) {
            if (data.length > 0 && !data.every(item => item.Value === 0)) {
                $("#GradeChartDiv").empty();
                $("#GradeChartContainer").show();
                //quizReportFunctions.bindGradeChart(data);

                quizReportFunctions.initializeGradeWiseReportChart(data);
                quizReportFunctions.createGradeReportChart();
            } else {
                $("#GradeChartContainer").hide();
                $("#GradeChartDiv").html("<span id='noDataSpan' class='no-data-found d-block mx-auto my-auto'>" + translatedResources.noRecordFound + "</span>");

            }
        },
        bindGenderChart = function (data) {
        
            if (data.length > 0) {
                $("#maleCountValue").html(data[0].Value);
                $("#femaleCountValue").html(data[1].Value);
            
                if(data[0].Value > 0)
                {
                    $("#spanMale").css('background','#722fa5');
                    $("#spanFemale").css('background','#e4498c');
                }
                else
                {
                    $("#spanMale").css('background','#e4498c');
                    $("#spanFemale").css('background','#722fa5');
                }

             }
            
            $("#GenderChartContainer").dxPieChart({
                size: {
                    width: 320,
                    height: 270
                },
                type: "doughnut",
                //palette: "bright",
                palette: ['#722fa5', '#e4498c'],
                //dataSource: data,
                dataSource: $.map(data, function (item) {
                    item.Value = item.Value === 0 ? null : item.Value;
                    return item;
                }),
                legend: {
                    visible: false
                },
                series: [
                    {
                        argumentField: "Name",
                        valueField: "Value",
                        label: {
                            visible: false,
                            connector: {
                                visible: true,
                                width: 1
                            },
                            position: "inside"
                        }
                    }
                ],
                tooltip: {
                    enabled: true,
                    shared: true,
                    customizeTooltip: function (info) {
                        return {
                            html: "<div class='tooltip-body'><div class='quiz-tooltip'> <span>Count : </span>" +
                                info.originalValue +
                                "</div>"
                        };
                    }
                },
                onPointClick: function (e) {
                    var point = e.target;

                    toggleVisibility(point);
                },
                onLegendClick: function (e) {
                    var arg = e.target;

                    toggleVisibility(this.getAllSeries()[0].getPointsByArg(arg)[0]);
                }
            });

            function toggleVisibility(item) {
                if (item.isVisible()) {
                    item.hide();
                } else {
                    item.show();
                }
            }
        },
        bindGradeChart = function (data) {

        $("#GradeChartContainer").dxChart({
            palette: "Harmony Light",
            dataSource: data,
            commonSeriesSettings: {
                type: "area",
                argumentField: "GradeDisplay",
                point: {
                    visible: true
                }
            },
           
           series: {
                argumentField: "GradeDisplay",
                valueField: "QuestionCount",
                name: "Quiz Count",
                color: '#ffaa66'
            },
            margin: {
                bottom: 20
            },
            argumentAxis: {
                label: {
                    overlappingBehavior: "rotate",
                    rotationAngle: -45
                }
            },

            legend: {
                visible: false
            },
            valueAxis: {
                tickInterval: 1
            },
            tooltip: {
            enabled: true,
            shared: true,
            customizeTooltip: function (info) {
                    return {
                        html: "<div class='tooltip-body'><div class='quiz-tooltip'> <span>Count : </span>" +
                            info.originalValue +
                            "</div>"
                    };
                }
            }
        }).dxChart("instance");
      }

    return {
        loadGenderWiseReportChart: loadGenderWiseReportChart,
        bindGenderChart: bindGenderChart,
        loadGrageWiseReportChart: loadGrageWiseReportChart,
        bindGradeChart: bindGradeChart,
        initializeGenderChartOptions: initializeGenderChartOptions,
        createGenderWiseChart: createGenderWiseChart,
        initializeGradeWiseReportChart: initializeGradeWiseReportChart,
        createGradeReportChart: createGradeReportChart

    }
}();



