﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var bookmark = function () {
    var init = function () {
        initDatatable('myfiles-bookmark-list');
    },
        addEditBookmark = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.addBookmark;
            globalFunctions.loadPopup(source, '/Files/Files/AddEditBookmark?id=' + id, title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                $('select').selectpicker({
                    noneSelectedText: translatedResources.select
                });
            });
        },
        loadBookmarks = function () {
            $.ajax({
                url: '/Files/Files/GetBookmarks',
                type: 'GET',
                success: function (data) {
                    if (data !== '') {
                        $("#bookmark-list").html("");
                        $("#bookmark-list").html(data);
                        folder.showSelectedView()
                        //folder.showSelectedView();
                        initDatatable('myfiles-bookmark-list');
                        globalFunctions.convertImgToSvg();
                    }
                    else {
                        $("#bookmark-list").html("");
                    }
                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        OnSaveBookmark = function (data) {
            globalFunctions.showMessage("success", data.Message);
            $("#myModal").modal('hide');
            bookmark.loadBookmarks();
        },
        OnErrorBookmark = function (data) {
            globalFunctions.showMessage("error", translatedResources.technicalError);
        },
        deleteBookmark = function (source, id) {
            if (globalFunctions.isValueValid(id)) {                
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        url: '/Files/Files/DeleteBookmark/?id=' + id,
                        type: 'GET',
                        success: function (data) {                           
                            if (data.Success === true) {                              
                                bookmark.loadBookmarks();
                            }
                            else {
                                globalFunctions.onFailure();
                            }
                        },
                        error: function (data, xhr, status) {
                            //debugger;
                            globalFunctions.onFailure();
                        }

                    });
                });
            }
        },
    initDatatable = function (_tableId) {
        var grid = new DynamicGrid(_tableId);
        var settings = {
            searching: false,
            columnSelector: false,
            paging: false,
            columnDefs: [{
                "targets": [2],
                'orderable': false,
            }],
            info: false
        };
        grid.init(settings);
    }
    return {
        init: init,
        loadBookmarks: loadBookmarks,
        addEditBookmark: addEditBookmark,
        OnSaveBookmark: OnSaveBookmark,
        OnErrorBookmark: OnErrorBookmark,
        deleteBookmark: deleteBookmark
    };
}();

$(document).ready(function () {
    bookmark.init();
    $("#btnAddBookmark").click(function () {
        bookmark.addEditBookmark($(this), translatedResources.add, "");
    });
});