﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var groupUrl = function () {
    var init = function () {
        loadGroupUrlGrid();
    },
        loadGroupUrlGrid = function () {

            var sectionId = $("#SectionId").val();

            $.ajax({
                url: '/Files/Files/GetGroupUrlList/?grId=' + sectionId,
                type: 'GET',
                success: function (data) {
                    if (data !== '') {
                        $("#group-url").html("");
                        $("#group-url").html(data);
                        $("#NoFolderDiv").hide();
                        showSelectedView();
                        //addCustomScrollbar();
                    }
                    else {
                        $("#group-url").html("");
                    }
                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        loadGroupUrlPopup = function (source, mode, id, groupId,folderId,moduleId) {
            var title = mode +' '+translatedResources.AddGroupUrl;
            var grId = groupId; $("#SectionId").val();
            globalFunctions.loadPopup(source, '/Files/Files/InitAddEditGroupUrl?id=' + id + '&grId=' + grId + '&pfId=' + folderId+'&moduleId='+moduleId, title, 'file-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                //formElements.feSelect();
                //CKEDITOR.replace('Summary', {
                //    htmlEncodeOutput: false
                //});
                //CKEDITOR.replace('Description', {
                //    htmlEncodeOutput: false
                //});
            });
        },
        addGroupUrlPopup = function (source, Id, groupId, folderId,moduleId) {
            loadGroupUrlPopup(source, translatedResources.add,Id, groupId,folderId, moduleId);

        },

        editGroupUrlPopup = function (source, id, groupId,folderId,moduleId) {
            loadGroupUrlPopup(source,translatedResources.edit, id, groupId, folderId, moduleId);
                },
        showSelectedView = function () {
            //debugger;
            var currentView = globalFunctions.getCookie("selectedView");
            if (currentView == "" || currentView == "0") {
                //debugger;
                $("#list").addClass('active');
                $('#library .item').addClass('list-group-item');
            }
            else {
                $("#grid").addClass('active');
                $('#library .item').removeClass('list-group-item');
                $('#library .item').addClass('grid-group-item');
            }
        },
        deleteGroupUrl = function (source, id) {
                    // debugger;
                    if (globalFunctions.isValueValid(id)) {
                        globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                        $(document).bind('okToDelete', source, function (e) {
                            $.ajax({
                                url: '/Files/Files/DeleteGroupUrl/?id=' + id,
                                type: 'GET',
                                success: function (data) {
                                    globalFunctions.showMessage(data.NotificationType, data.Message);
                                    if (data.Success === true) {
                                        ///loadGroupUrlGrid();
                                        if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
                                            groupDetail.loadTopicActivitiesList();
                                        }
                                    }
                                    else {
                                        globalFunctions.onFailure();
                                    }
                                },
                                error: function (data, xhr, status) {
                                    //debugger;
                                    globalFunctions.onFailure();
                                }

                            });
                        })
                        
                    }
                },

                onGroupUrlSuccess = function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        $("#myModal").modal('hide');
                        //loadGroupUrlGrid();
                        if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
                            groupDetail.loadTopicActivitiesList();
                        }
                    }
                };

            return {
                init: init,
                addGroupUrlPopup: addGroupUrlPopup,
                onGroupUrlSuccess: onGroupUrlSuccess,
                editGroupUrlPopup: editGroupUrlPopup,
                deleteGroupUrl: deleteGroupUrl
            };

        }();


    $(document).ready(function () {
        $(document).on('click', '#btnAddGroupUrl', function () {
            var groupId = $("#SectionId").val();
            var folderId = $("#ParentFolderId").val();
            var moduleId = $("#ModuleId").val();

            groupUrl.addGroupUrlPopup($(this), 0, groupId,folderId,moduleId);
        });
        $(document).on('click', '#btnEditGroupUrl', function () {
            var id = $(this).data("id");
            var groupId = $("#SectionId").val();
            var folderId = $("#ParentFolderId").val();
            var moduleId = $("#ModuleId").val();
            groupUrl.editGroupUrlPopup($(this), id, groupId,folderId,moduleId);
        });
        
        $(document).on('click', '#btnDeleteGroupUrl', function () {
            var id = $(this).data("id");
            groupUrl.deleteGroupUrl($(this), id);
        });
    });
