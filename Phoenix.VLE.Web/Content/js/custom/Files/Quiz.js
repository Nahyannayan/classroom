﻿/// <reference path="../../ckeditor/plugin.js" />
/// <reference path="../../ckeditor/plugin.js" />
/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var listFilter = [];
var isTimeOut = false;
//var IsAnswered = true;
var IsRequiredCount = 0;
var IsRequiredAttemptCount = 0;
IsFreeTextBlur = false;
var refreshIntervalId = null;
var formElements = new FormElements();
var groupQuiz = function () {
    var init = function () {

    },
        loadGroupQuizGrid = function () {

            var sectionId = $("#SectionId").val();

            $.ajax({
                url: '/Files/Files/GetGroupQuizList/?grId=' + sectionId,
                type: 'GET',
                success: function (data) {
                    if (data !== '') {
                        $("#group-quiz").html("");
                        $("#group-quiz").html(data);
                        $("#NoFolderDiv").hide();
                        //addCustomScrollbar();
                    }
                    else {
                        $("#group-quiz").html("");
                    }
                },
                error: function (data, xhr, status) {
                    //globalFunctions.onFailure();
                }

            });
        },
        loadGroupQuizPopup = function (source, id, groupId, moduleId, folderId, courseId) {
            var title = translatedResources.addgroupquiz;
            var grId = groupId; //$("#SectionId").val();
            globalFunctions.loadPopup(source, '/Files/Files/InitAddEditGroupQuiz?id=' + id + '&grId=' + grId + '&pfId=' + folderId + '&moduleId=' + moduleId + '&cid=' + courseId, title, 'file-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $(document).off('click', '#submitGroupQuiz').on('click', '#submitGroupQuiz', function (e) {
                    //debugger;
                    var validationScoreDate = true;
                    if ($("#IsSetTime").prop("checked")) {
                        if ($('#StartDate').val().trim() != '') {
                            if ($('#StartTime').val() === null || $('#StartTime').val().trim() === '') {
                                $(".validateStartTime").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
                                formValidate = false;
                            } else {
                                if (!validationQuizTime) {
                                    globalFunctions.showWarningMessage(translatedResources.quizTimeValidation);
                                }
                            }
                        }
                    }
                    if ($("#IsHideScore").is(":checked") === true) {
                        if ($('#ShowScoreDate').val() === '') {
                            globalFunctions.showWarningMessage(translatedResources.scoreDateValidation);
                            validationScoreDate = false;
                        }
                    }
                    if (formValidate && validateQuiz && validationScoreDate) {
                        $('form#frmAddGroupQuiz').submit();
                    }
                    //e.stopPropagation();
                });
            });

            globalFunctions.enableCascadedDropdownList("CourseIds", "QuizId", '/Files/Files/GetQuizByCourse');
            $('.selectpicker').selectpicker('refresh');
        },
        markGroupQuiz = function (source, id, groupId) {
            window.location.href = '/Files/Files/GroupStudentDetails/?id=' + id + '&groupId=' + groupId + '&isForm=' + false + '&selectedGroupId=' + $('#SectionId').val();
            //$.ajax({
            //    type: 'POST',
            //    url: '/Files/Files/GroupStudentDetails',
            //    data: { id: id, groupId: groupId },
            //    success: function (result) {

            //    },
            //    error: function (msg) {

            //    }
            //});
        },
        deleteGroupQuizById = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        url: '/Files/Files/DeleteGroupQuiz/?id=' + id,
                        type: 'GET',
                        success: function (data) {
                            globalFunctions.showMessage(data.NotificationType, data.Message);
                            if (data.Success === true) {

                                if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
                                    groupDetail.loadTopicActivitiesList();
                                }
                            }
                            else {
                                //globalFunctions.onFailure();
                            }
                        },
                        error: function (data, xhr, status) {
                            //globalFunctions.onFailure();
                        }

                    });
                })

            }
        },
        loadGradingTemplateItemPopup = function (source, title, id, studId, quizId, QuizResultId, selectedGradeId, gradingType) {
            globalFunctions.loadPopup(source, '/Files/Files/LoadGradingTemplateItems?gradingtemplateId=' + id + "&studentId=" + studId + "&quizId=" + quizId + "&QuizResultId=" + QuizResultId + "&selectedGradeId=" + selectedGradeId + "&gradingType=" + gradingType, title, 'addtask-dialog');
        },
        loadQuizFeedBackPopup = function (source, title, studId, quizId, QuizResultId, ResourceId) {
            globalFunctions.loadPopup(source, '/Files/Files/LoadQuizFeedBack?studentId=' + studId + "&quizId=" + quizId + "&QuizResultId=" + QuizResultId + "&ResourceId=" + ResourceId, title, 'addtask-dialog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {

                // webkit shim
                window.AudioContext = window.AudioContext || window.webkitAudioContext;
                navigator.getUserMedia = (
                    navigator.getUserMedia ||
                    navigator.webkitGetUserMedia ||
                    navigator.mozGetUserMedia ||
                    navigator.msGetUserMedia
                );

                if (typeof navigator.mediaDevices.getUserMedia === 'undefined') {
                    navigator.getUserMedia({
                        audio: true
                    }, startUserMedia);
                } else {
                    navigator.mediaDevices.getUserMedia({
                        audio: true
                    }).then(startUserMedia).catch(errorHandler);
                }

                function errorHandler() {
                    console.log("error");
                }

                window.URL = window.URL || window.webkitURL;

                audio_context = new AudioContext();
                //audio_context = (typeof window !== 'undefined' && typeof window.AudioContext !== 'undefined') ? new AudioContext() : null;
                //__log('Audio context set up.');
                //__log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
                CKEDITOR.replace('Feedback', {
                    htmlencodeoutput: false,
                    height: '180px'
                });
            });
        },
        downloadReportCard = function (source, id, resourceId, studentId) {
            window.location.href = '/Files/Files/DownloadReportCardAsPDF/?id=' + id + '&resourceId=' + resourceId + '&studentId=' + studentId + '&resourceType=' + 'G';
        },
        solveGroupQuiz = function (source, id, resourceId, studentId) {
            window.location.href = '/Files/Files/Quiz/?id=' + id + '&resourceId=' + resourceId + '&studentId=' + studentId;
        },
        logQuizTime = function (source, id, resourceId, resourceType, IsStart) {
            $.ajax({
                type: 'POST',
                url: '/Files/Files/LogQuizTime',
                data: { id: id, ResourceId: resourceId, ResourceType: resourceType, IsStartQuiz: IsStart },
                success: function (result) {

                },
                error: function (msg) {

                }
            });
        },
        deleteAnswerFile = function (fileId, resourceId, resourceType, questionId, studentId, divid) {
            //debugger;
            $.ajax({
                type: 'POST',
                data: { resourceId: resourceId, resourceType: resourceType, questionId: questionId, studentId: studentId, fileId: fileId, },
                url: '/Files/Files/DeleteQuestionAnswerFile',
                success: function (result) {
                    //debugger;
                    globalFunctions.showMessage('success', translatedResources.deletedSuccess);
                    $(divid).html(result);
                },
                error: function (msg) {
                    //globalFunctions.onFailure();
                }
            });
        },
        assignGradeToQuiz = function (studentId) {
            if ($("#selectedGradingitem").val() != undefined && $("#selectedGradingitem").val() != "0" && $("#selectedGradingitem").val() != "") {
                //Ajax call to update grade 
                $.ajax({
                    type: "GET",
                    url: "/Files/Files/UpdateQuizGrade?gradeId=" + $("#selectedGradingitem").val() + "&quizId=" + $("#quizId").val() + "&studentId=" + $("#quizResponseByUserId").val() + "&QuizResultId=" + $("#QuizResultId").val() + "&gradingTemplateId=" + $("#gradingTemplateId").val(),

                    success: function (response) {
                        $("#myModal").modal('hide');
                        $("#gradingDiv_" + studentId + "").show();
                        $("#gradingDiv_" + studentId + "").html('');
                        $("#gradingDiv_" + studentId + "").attr('data-selectedgradingid', response.GradingTemplateItemId);
                        if (response.GradingTemplateItemSymbol.indexOf('.png') != -1) {
                            // $("#gradingDiv_" + studentId + "").append('<a href="javascript:void(0)" data-toggle="tooltip" title="' + response.ShortLabel + '" data-title="' + response.ShortLabel + '" data-placement="top" class="ml-2"><img class= "img-fluid" style = "height:70%;" src = "' + response.GradingTemplateItemSymbol + '"/></a >')
                            $("#gradingDiv_" + studentId + "").attr("title", response.ShortLabel);
                            $("#gradingDiv_" + studentId + "").attr("data-title", response.ShortLabel);
                            $("#gradingDiv_" + studentId + "").attr("data-title", response.ShortLabel);
                            $("#gradingDiv_" + studentId + "").append('<img class="img-fluid" style="width: 2.1875rem;" src="' + response.GradingTemplateItemSymbol + '" />')
                        }
                        else {
                            $("#gradingDiv_" + studentId + "").css("color", response.GradingColor)
                            $("#gradingDiv_" + studentId + "").append(response.ShortLabel);
                        }
                        globalFunctions.showSuccessMessage(translatedResources.assignGradeStatus);
                        //location.reload();
                    },
                    error: function (error) {
                        //globalFunctions.onFailure();
                    }
                });
            }
            else {
                globalFunctions.showMessage("error", translatedResources.SelectGrade)
            }
        },
        assignFeedBackToQuiz = function (studentId) {
            if (CKEDITOR.instances.Feedback.getData().replace(/<[^>]*>/gi, '').length) {
                var quizId = $("#quizId").val();
                var studentId = $("#quizResponseByUserId").val();
                var userFeedback = CKEDITOR.instances.Feedback.getData();
                var quizFeedback = new FormData()
                quizFeedback.append("feedback", userFeedback);
                quizFeedback.append("QuizResultId", $("#QuizResultId").val());
                $.each(listRecordings, function (idx, file) {
                    quizFeedback.append("recordings", file, file.name);
                });
                //feedback.append("recordings", listRecordings);

                $.ajax({
                    type: "POST",
                    url: "/Files/Files/InsertQuizFeedback",
                    data: quizFeedback,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        $("#myModal").modal('hide');
                        listRecordings = [];
                    },
                    error: function (error) {
                        //globalFunctions.onFailure();
                    }
                });
            }
            else {
                $('#FeedbackValidation').html(translatedResources.Mandatory);
            }

        },
        resetMTPQuiz = function (quizQuestionId) {
            //$.each(list, function (n, a) {
            //    if (a != undefined) {
            //        var listvalue = parseInt(a.QuizQuestionId);
            //        if (listvalue == quizQuestionId) {
            //            list.pop(this);
            //        }
            //    }
            //})
            $.ajax({
                type: 'GET',
                url: '/Files/Files/ResetMTPQuestion?quizQuestionId=' + quizQuestionId,
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    var MTPClass = '.dragdrop_' + quizQuestionId;
                    $(MTPClass).html(data);
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
            list = jQuery.grep(list, function (value) {
                return parseInt(value.QuizQuestionId) != quizQuestionId;
            });
            //$.each(list, function (n, a) {
            //    if (a != undefined) {
            //        var listvalue = parseInt(a.QuizQuestionId);
            //        if (listvalue == quizQuestionId) {
            //            var indexToRemove = 1;
            //            list.splice(indexToRemove, n);
            //            //list.pop(this);
            //        }
            //    }
            //})
        },
        deleteRecording = function (fileId) {
            $.ajax({
                type: 'POST',
                url: '/Files/Files/DeleteRecording',
                data: { id: fileId },
                success: function (result) {
                    if (result.Success) {
                        globalFunctions.showMessage(result.NotificationType, result.Message);
                        $("#audio_" + fileId).remove();
                    }
                },
                error: function (msg) {

                }
            });

        },
        submitQuiz = function () {
            groupQuiz.freetextanswers()
            ////debugger;
            var responseQuestionId = $(".responseQuestionId");
            //changes for freetext
            //var freeText = $(".freetext");
            if (!isTimeOut) {
                var isExist = false;
                $.each(responseQuestionId, function (k, v) {
                    $.each(list, function (n, a) {
                        var responsevalue = parseInt(v.value);
                        var listvalue = parseInt(a.QuizQuestionId);
                        if (responsevalue === listvalue) {
                            isExist = true;
                            return false;
                        } else {
                            isExist = false;
                        }
                    })
                    if (!isExist && v.dataset.questiontype == 'freetext') {
                        var QuizId = $("#QuizId").val();
                        var quesId = v.value;
                        var ansId = v.dataset.answerid;
                        var ansValue = '';
                        var obMarks = 0;
                        list.push({
                            QuizId: QuizId, QuizQuestionId: quesId
                            , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
                        });

                    }
                });
            }
            //
            //console.log(responseInputs);
            if (responseQuestionId.length > 0) {
                ////debugger;
                listFilter = list.reverse().reduce(function (item, e1) {
                    var matches = item.filter(function (e2) { return e1.QuizQuestionId == e2.QuizQuestionId && e1.QuizAnswerId == e2.QuizAnswerId });
                    if (matches.length == 0) {
                        item.push(e1);
                    }
                    return item;
                }, []);
                var isValid = true;
                //if (!IsAnswered && $('#hdnIsTeacher').val() === "0") {
                //    $('[data-questionid="' + questionId + '"]').focus();
                //    $('[data-questionid="' + questionId + '"]').closest('.row').effect("pulsate", {}, 3000);
                //    globalFunctions.showWarningMessage('Please answer required questions');
                //    Isvalid = false;
                //    return false;
                //}
                if (!isTimeOut) {
                    for (var i = 0; i < responseQuestionId.length; i++) {
                        var isReq = ($(responseQuestionId[i]).attr("data-isrequired") === "True" && $('#hdnIsTeacher').val() != 1 ? true : false);
                        var quesTypeId = $(responseQuestionId[i]).data("questype");
                        var questionId = responseQuestionId[i].value;
                        if (isReq) {
                            var isChecked = false;
                            for (var j = 0; j < listFilter.length; j++) {
                                if (listFilter[j].QuizQuestionId == questionId) {
                                    isChecked = true;
                                }
                                if (quesTypeId == 4 && listFilter[j].QuizAnswerText == '' && listFilter[j].QuizQuestionId == questionId) {
                                    isChecked = false;
                                }
                                if (quesTypeId == 7) {
                                    isChecked = true;
                                }
                            }
                            if (quesTypeId == 8) {
                                $.each(list, function (n, a) {
                                    var listvalue = parseInt(a.QuizQuestionId);
                                    if (parseInt(questionId) === listvalue) {
                                        isChecked = true;
                                        return false;
                                    } else {
                                        isChecked = false;
                                    }
                                })
                            }
                            if (!isChecked) {
                                $('[data-questionid="' + questionId + '"]').focus();
                                $('[data-questionid="' + questionId + '"]').closest('.row').effect("pulsate", {}, 3000);
                                globalFunctions.showWarningMessage(translatedResources.AnswerRequiredMessage);
                                Isvalid = false;
                                return false;
                            }
                        }
                        if ($('#hdnIsTeacher').val() == 1 && quesTypeId == 4) {
                            var freetextTeacherresponse = $(".freetextTeacherresponse");
                            var freetxtQuestionId = $(responseQuestionId[i]).data("questype");
                            for (var k = 0; k < freetextTeacherresponse.length; k++) {
                                var isChecked = true;
                                var marks = freetextTeacherresponse[k].value;
                                if (!isFloat(marks)) {
                                    isChecked = false;
                                }
                                if (!isChecked) {
                                    freetextTeacherresponse[k].focus();
                                    $('[data-questionid="' + freetxtQuestionId + '"]').closest('.row').effect("pulsate", {}, 3000);
                                    globalFunctions.showWarningMessage(translatedResources.EnterMarks);
                                    Isvalid = false;
                                    return false;
                                }
                            }
                        }
                    }
                }
                if (isValid) {
                    ////debugger;
                    var formData = {
                        QuizId: $("#QuizId").val(),
                        QuizResultId: $("#QuizResultId").val(),
                        StudentId: $("#StudentId").val(),
                        TskId: $("#ResourceId").val(),
                        TaskId: $("#ResourceId").val()
                        //,
                        //SortedAnswerOrder: JSON.stringify($("#quizMatchList").sortable("toArray"))
                    };
                    $.ajax({
                        type: 'POST',
                        url: '/Files/Files/SubmitQuiz',
                        data: { model: formData, answersList: listFilter },
                        success: function (result) {
                            if (result.Success) {
                                //$("#myModal").modal('hide');
                                if (!isTimeOut) {
                                    globalFunctions.showMessage(result.NotificationType, result.Message);
                                } else {
                                    globalFunctions.showWarningMessage(translatedResources.TimeOut);
                                }
                                if ($('#hdnIsTeacher').val() != 1) {
                                    groupQuiz.logQuizTime($(this), $("#EncrQuizId").val(), $("#EncrResourceId").val(), 'Q', false);
                                }
                                if ($('#hdnIsTeacher').val() == 1) {
                                    window.location.reload();
                                }
                                else {
                                    if (localStorage.getItem("pagerefer") != null) {
                                        var pathrefer = localStorage.getItem("pagerefer");
                                        window.location.href = pathrefer;
                                    }
                                    //history.go(-1);
                                    //var grpId = $('#EncrResourceId').val();
                                    //window.location.href = "/schoolstructure/schoolgroups/groupdetails?grpId=" + grpId+"&tb=2";
                                }
                            }
                        },
                        error: function (msg) {

                        }
                    });
                }
            } else {
                $("#ResponseValidation").text(translatedResources.ResponseValidation);
                return false;
            }
        },
        freetextanswers = function () {
            if ($('#IsStudent').val() != "False") {
                $('.freetext').each(function () {
                    IsFreeTextBlur = true;
                    var QuizId = $("#QuizId").val();
                    var quesId = $("#" + $(this)[0].name).attr("data-questionid");
                    var ansId = $("#" + $(this)[0].name).attr("data-answerid");
                    var selectedAns = "QuizFreeText_" + $("#" + $(this)[0].name).data("answerid")
                    //var ansValue = $(this).val();
                    var ansValue = CKEDITOR.instances[selectedAns].getData();
                    var obMarks = 0;
                    if (ansValue != "") { IsRequiredAttemptCount++; }
                    list.push({
                        QuizId: QuizId, QuizQuestionId: quesId
                        , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
                    });
                });
            }
        },
        downloadQuizAsPDF = function (source, quizid, resourseid) {
            var resourseType = 'G';
            window.location.href = '/Quiz/Quiz/DownloadQuizAsPDF/?id=' + quizid + '&resourceId=' + resourseid + '&resourceType=' + resourseType;
        },
        isFloat = function (val) {
            var floatRegex = /^-?\d+(?:[.,]\d*?)?$/;
            if (!floatRegex.test(val))
                return false;

            val = parseFloat(val);
            if (isNaN(val))
                return false;
            return true;
        },
        onGroupQuizSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                //loadGroupQuizGrid();
                if ($("#PageName") != undefined) {
                    if ($("#PageName").val() == "GroupDetails") {
                        groupDetail.loadTopicActivitiesList();
                        groupDetail.loadGroupContent("2");
                    }
                    else if ($("#PageName").val() == "AsyncLesson") {
                        asyncLesson.loadResourcesActivityContent();
                    }
                }

            }
        };

    return {
        init: init,
        loadGroupQuizPopup: loadGroupQuizPopup,
        loadGroupQuizGrid: loadGroupQuizGrid,
        solveGroupQuiz: solveGroupQuiz,
        assignGradeToQuiz: assignGradeToQuiz,
        submitQuiz: submitQuiz,
        logQuizTime: logQuizTime,
        resetMTPQuiz: resetMTPQuiz,
        freetextanswers: freetextanswers,
        downloadQuizAsPDF: downloadQuizAsPDF,
        assignFeedBackToQuiz: assignFeedBackToQuiz,
        deleteRecording: deleteRecording,
        downloadReportCard: downloadReportCard,
        loadQuizFeedBackPopup: loadQuizFeedBackPopup,
        loadGradingTemplateItemPopup: loadGradingTemplateItemPopup,
        markGroupQuiz: markGroupQuiz,
        deleteGroupQuizById: deleteGroupQuizById,
        deleteAnswerFile: deleteAnswerFile,
        onGroupQuizSuccess: onGroupQuizSuccess,
    };

}();


$(document).ready(function () {

    //$('.quiz-pagination li:nth-child(2)').addClass('active');
    $('body').on('click', '.custom-control-label', function () {
        var selectedCount = 0;
        let chkBox = $(this).parent('.custom-checkbox').find('input');
        //console.log(chkBox.attr('id'));
        $(this).parent('.custom-checkbox').find('input').on('change', function () {
            if ($(this).parent('.custom-checkbox').find('input').is(":checked")) {

                chkBox.attr("checked", true);

                var QuizId = $("#QuizId").val();
                var quesId = $(this).attr("data-questionid");
                var ansId = $(this).attr("data-answerid");
                var ansValue = $(this).attr("checked");
                var obMarks = $(this).attr("data-marks");
                if (QuizId != '' && QuizId != undefined) {
                    list.push({
                        QuizId: QuizId, QuizQuestionId: quesId
                        , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
                    });
                }
                
            } else {
                chkBox.removeAttr("checked");

                var QuizId = $("#QuizId").val();
                var quesId = $(this).attr("data-questionid");
                var ansId = $(this).attr("data-answerid");
                var ansValue = $(this).attr("checked");
                var obMarks = $(this).attr("data-marks");
                if (QuizId != '' && QuizId != undefined) {
                    list = list.reverse().reduce(function (item, e1) {
                        var matches = item.filter(function (e2) { return e1.QuizQuestionId == quesId && e1.QuizAnswerId == ansId });
                        if (matches.length == 0) {
                            item.push(e1);
                        }
                        return item;
                    }, []);
                }
               

            }


        })
        let rdBtn = $(this).parent('.custom-radio').find('input');
        $(this).parent('.custom-radio').find('input').change(function () {
            if ($(this).parent('.custom-radio').find('input').is(":checked")) {

                rdBtn.attr("checked", true);
                var QuizId = $("#QuizId").val();
                var quesId = $(this).attr("data-questionid");
                var ansId = $(this).attr("data-answerid");
                var ansValue = $(this).attr("checked");
                var obMarks = $(this).attr("data-marks");
                list = jQuery.grep(list, function (value) {
                    return parseInt(value.QuizAnswerId) != parseInt(ansId) && parseInt(value.QuizQuestionId) != parseInt(quesId);
                });
                list.push({
                    QuizId: QuizId, QuizQuestionId: quesId
                    , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
                });
            } else {
                rdBtn.removeAttr("checked");
                var QuizId = $("#QuizId").val();
                var quesId = $(this).attr("data-questionid");
                var ansId = $(this).attr("data-answerid");
                var ansValue = $(this).attr("checked");
                var obMarks = $(this).attr("data-marks");
                list = list.reverse().reduce(function (item, e1) {
                    var matches = item.filter(function (e2) { return e1.QuizQuestionId == quesId && e1.QuizAnswerId == ansId });
                    if (matches.length == 0) {
                        item.push(e1);
                    }
                    return item;
                }, []);
            }

            //var QuizId = $("#QuizId").val();
            //var quesId = $(this).attr("data-questionid");
            //var ansId = $(this).attr("data-answerid");
            //var ansValue = $(this).attr("checked");
            //var obMarks = $(this).attr("data-marks");
            //list.push({
            //    QuizId: QuizId, QuizQuestionId: quesId
            //    , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
            //});
        })
    });

    $('body').on('change', '.selectpicker', function () {


        var QuizId = $("#QuizId").val();
        var quesId = $(this).attr("data-questionid");
        var ansId = $("option:selected", this).val();
        var ansValue = $("option:selected", this).text();
        var obMarks = $(this).attr("data-marks");
        if (QuizId != '' && QuizId != undefined) {
            list.push({
                QuizId: QuizId, QuizQuestionId: quesId
                , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
            });
        }
    });

    $("#quizMatchList").disableSelection();
    $('body').on('mouseleave', '#quizMatchList', function () {
        var isSorted = false;
        var listSortedOrder = $("#quizMatchList").sortable("toArray");
        jQuery.each(listSortedOrder, function (i, val) {
            if (val == i + 1) { isSorted = true }
            else { isSorted = false }
        });
        jQuery.each(listSortedOrder, function (i, val) {
            var QuizId = $("#QuizId").val();
            var quesId = $("#" + val).attr("data-questionid");
            var ansId = $("#" + val).attr("data-answerid");
            var ansValue = $("#" + val).attr("data-answerText");
            var obMarks = 0;
            if (isSorted) {
                obMarks = $("#" + val).attr("data-marks");
            }
            list.push({
                QuizId: QuizId, QuizQuestionId: quesId
                , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks, SortOrder: i + 1
            });
        });
    });
    //$('body').on('blur change', '.matchFreeText', function () {

    //    var QuizId = $("#QuizId").val();
    //    var quesId = $(this).attr("data-questionid");
    //    var ansId = $(this).attr("data-answerid");
    //    var ansValue = $(this).val();
    //    var obMarks = 0;
    //    if (ansValue == atob($(this).attr("data-matchSortorder")))
    //        obMarks = $(this).attr("data-marks");
    //    list.push({
    //        QuizId: QuizId, QuizQuestionId: quesId
    //        , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
    //    });
    //});
    $('body').on('blur change', '.fibFreetext', function () {
        var QuizId = $("#QuizId").val();
        var quesId = $(this).attr("data-questionid");
        var ansId = $(this).attr("data-answerid");
        var ans = $(this).attr("data-f").toLowerCase().trim();
        var sortorder = $(this).attr("data-sortorder");
        var ansValue = $(this).val().toLowerCase().trim();
        var obMarks = 0;
        if (ansValue != "") { IsRequiredAttemptCount++; }
        if (ansValue == ans) {
            obMarks = $(this).attr("data-marks");
        }
        list.push({
            QuizId: QuizId, QuizQuestionId: quesId
            , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks, SortOrder: sortorder
        });
    });
    $('body').on('blur change', '.freetext', function () {
        IsFreeTextBlur = true;
        var QuizId = $("#QuizId").val();
        var quesId = $(this).attr("data-questionid");
        var ansId = $(this).attr("data-answerid");
        var selectedAns = "QuizFreeText_" + $(this).data("answerid")
        //var ansValue = $(this).val();
        var ansValue = CKEDITOR.instances.selectedAns.getData()
        var obMarks = 0;
        if (ansValue != "") { IsRequiredAttemptCount++; }
        list.push({
            QuizId: QuizId, QuizQuestionId: quesId
            , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
        });
    });
    $('body').on('blur', '.freetextTeacherresponse', function () {
        var QuizId = $("#QuizId").val();
        var quesId = $(this).closest('.freetext-fieldset').find('.freetext').attr("data-questionid");
        var ansId = $(this).closest('.freetext-fieldset').find('.freetext').attr("data-answerid");
        var ansValue = $(this).closest('.freetext-fieldset').find('.freetext').val();
        var maxValue = $(this).attr("max");
        //var obMarks = $(this).attr("data-marks");
        var teacherMarks = '';
        if (!isNaN($(this).val()) && $(this).val() != '') {
            if ((parseFloat($(this).val()) > parseFloat(maxValue))) {

                globalFunctions.showWarningMessage(translatedResources.EnterValidMarks);
                $(this).val(teacherMarks)
                return;
            }
            teacherMarks = (parseFloat($(this).val()) > parseFloat(maxValue)) ? '' : parseFloat($(this).val());
        }
        else {

            globalFunctions.showWarningMessage(translatedResources.EnterValidMarks);
            $(this).val(teacherMarks)
            return;
        }
        var quizResponseId = $("#QuizResultId").val();
        $(this).val(teacherMarks)
        if (teacherMarks != '') {
            list.push({
                QuizId: QuizId, QuizResponseId: quizResponseId, QuizQuestionId: quesId
                , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: teacherMarks
            });
        }
    });
    //$(document).on('click', '#quizMatchList li', function () {
    //     var answerOrder=$("#quizMatchList").sortable("toArray"); 
    //    $("#QuizAnswerSortOrder").val(answerOrder);
    //});

    $(document).on('click', '#btnAddGroupQuiz', function () {
        var groupId = $("#SectionId").val();
        var folderId = $("#ParentFolderId").val();
        var moduleId = $("#ModuleId").val();
        groupQuiz.loadGroupQuizPopup(this, 0, groupId, moduleId, folderId);
    });
    $(document).on('click', '#btnEditGroupUrl', function () {
        var id = $(this).data("id");
        var groupId = $("#SectionId").val();
        var folderId = $("#ParentFolderId").val();
        var moduleId = $("#ModuleId").val();
        groupUrl.editGroupUrlPopup(this, id, groupId, folderId, moduleId);
    });

    $(document).on('click', '#btnDeleteGroupQuiz', function () {
        var id = $(this).data("id");
        groupQuiz.deleteGroupQuizById($(this), id);
    });

    $(document).on('click', '#btnQuizMark', function () {
        debugger;
        var id = $(this).data("id");
        var groupId = $(this).data("groupid");
        groupQuiz.markGroupQuiz($(this), id, groupId);
    });
    $(document).on('click', '#btnQuizAsPdf', function () {
        debugger;
        var isQuestionExist = $(this).data("isquestionexist");
        if (isQuestionExist == 'True') {
            var quizId = $(this).data("quizid");
            var resourseId = $(this).data("groupid");
            groupQuiz.downloadQuizAsPDF($(this), quizId, resourseId);
        } else {
            globalFunctions.showErrorMessage(translatedResources.NoDownloadQuestion);
        }
    });

    $(document).on('click', '#btnStartGroupQuiz', function () {
        clearInterval(refreshIntervalId);
        $("#StartQuizId").val($(this).data("id"));
        $("#StartQuizresourceid").val($(this).data("quizresourceid"));
        $("#StartStudid").val($(this).data("studid"));
        $("#selectedQuizName").html($(this).data("quizname"));
        $("#selectedQuizTitle").html($(this).data("starttime"));
        $("#QuizTimeInSec").val($(this).data("timeinsec"));
        $("#selectedQuizStartDate").html($(this).data("startdate"));
        $("#selectedQuizStartTime").html($(this).data("starttimebyteacher"));
        $("#TeacherGivenTime").val($(this).data("teachergiventime"));
        $("#IsSetStartDateByTeacher").val($(this).data("issetstartdate"));

        if ($(this).data("issetstartdate") == 'False') {
            $('.SetTeacherTime').hide();
            $('.selectedQuizFooter').show();
        }
        else {
            $('.SetTeacherTime').show()
            if ($(this).data("startstartedtimer") == 'False') {
                $('.selectedQuizFooter').hide();
                var self = this;
                refreshIntervalId = setInterval(function () {
                    var d2_old = new Date();
                    var d2 = new Date();
                    $.ajax({
                        type: 'GET',
                        url: '/Files/Files/GetServerDateTime',
                        async: false,
                        global: false,
                        contentType: 'application/json',
                        success: function (data) {
                            //debugger;
                            d2 = new Date(data);
                        },
                        error: function (data, xhr, status) {
                            globalFunctions.onFailure(data, xhr, status);
                        }
                    });
                    var d1 = new Date($(self).data("teachergiventime"));
                    var diffInSec = parseInt($(self).data("timeinsec")) - parseInt(((d2 - d1) / 1000).toString());
                    if (d2 >= d1) {
                        //$('.selectedQuizFooter').show();
                        if (diffInSec > 0) {
                            $('.selectedQuizFooter').show();
                        } else {
                            $('.selectedQuizFooter').hide();
                        }
                    }
                    else {
                        $('.selectedQuizFooter').hide();
                    }
                    //if (diffInSec < 0) {
                    //    $('.selectedQuizFooter').hide();
                    //} else {
                    //    $('.selectedQuizFooter').show();
                    //}
                }, 200);
                //var d2 = new Date();
                //var d1 = new Date($(this).data("teachergiventime"));
                //var diffInSec = parseInt($(this).data("timeinsec")) - parseInt(((d2 - d1) / 1000).toString());
                //if (diffInSec < 0) {
                //    $('.selectedQuizFooter').hide();
                //}

            } else {
                //$('.selectedQuizFooter').show();
                var self = this;
                refreshIntervalId = setInterval(function () {
                    var d2 = new Date();
                    $.ajax({
                        type: 'GET',
                        url: '/Files/Files/GetServerDateTime',
                        async: false,
                        global: false,
                        contentType: 'application/json',
                        success: function (data) {
                            //debugger;
                            d2 = new Date(data);
                        },
                        error: function (data, xhr, status) {
                            globalFunctions.onFailure(data, xhr, status);
                        }
                    });
                    var d1 = new Date($(self).data("teachergiventime"));
                    var diffInSec = parseInt($(self).data("timeinsec")) - parseInt(((d2 - d1) / 1000).toString());
                    if (d2 >= d1) {
                        //$('.selectedQuizFooter').show();
                        if (diffInSec > 0) {
                            $('.selectedQuizFooter').show();
                        } else {
                            $('.selectedQuizFooter').hide();
                        }
                    }
                    else {
                        $('.selectedQuizFooter').hide();
                    }
                    //if (diffInSec < 0) {
                    //    $('.selectedQuizFooter').hide();
                    //} else {
                    //    $('.selectedQuizFooter').show();
                    //}
                }, 200);
                //var d2 = new Date();
                //var d1 = new Date($(this).data("teachergiventime"));
                //var diffInSec = parseInt($(this).data("timeinsec")) - parseInt(((d2 - d1) / 1000).toString());
                //if (diffInSec < 0) {
                //    $('.selectedQuizFooter').hide();
                //}
            }
        }
        $('#startQuiz').modal({ show: true });
        //groupQuiz.logQuizTime($(this), $(this).data("id"), $(this).data("quizresourceid"), 'Q');
    });
    $(document).on('click', '#btnSolveGroupQuiz', function () {
        //files//quiz.js
        //For not set quiz start datetime get quizStarted as 0
        if ($("#IsSetStartDateByTeacher").val() == "False") {
            localStorage.setItem("quizStarted", 0);
        }
        else {
            var d2 = new Date();
            $.ajax({
                type: 'GET',
                url: '/Files/Files/GetServerDateTime',
                async: false,
                global: false,
                contentType: 'application/json',
                success: function (data) {
                    //debugger;
                    d2 = new Date(data);
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
            var d1 = new Date($("#TeacherGivenTime").val());
            var diffInSec = parseInt($("#QuizTimeInSec").val()) - parseInt(((d2 - d1) / 1000).toString());
            localStorage.setItem("quizStarted", diffInSec);
        }
        var id = $("#StartQuizId").val();
        var resourceId = $("#StartQuizresourceid").val();
        var studentId = $("#StartStudid").val();
        //var id = $(this).data("id");
        //var resourceId = $(this).data("quizresourceid");
        //var studentId = $(this).data("studid");
        groupQuiz.logQuizTime($(this), id, resourceId, 'Q', true);
        groupQuiz.solveGroupQuiz($(this), id, resourceId, studentId);

    });
    $(document).on('click', '#btnSubmitWithouTimeQuiz', function () {
        var id = $(this).data("id");
        var resourceId = $(this).data("quizresourceid");
        var studentId = $(this).data("studid");
        groupQuiz.solveGroupQuiz($(this), id, resourceId, studentId);
    });
    $(document).on('click', '#btnReportCard', function () {
        var id = $(this).data("id");
        var resourceId = $(this).data("quizresourceid");
        var studentId = $(this).data("studid");
        groupQuiz.downloadReportCard($(this), id, resourceId, studentId);
    });
    $(".btnAssignGradeToQuiz").click(function () {
        var gradingId = parseInt($(this).attr("data-gradingId"));
        var studentId = parseInt($(this).attr("data-studentId"));
        var QuizId = $(this).attr("data-QuizId");
        var ResourceId = $(this).attr("data-ResourceId");
        var QuizResultId = $(this).attr("data-QuizResultId");
        var selectedGradeId = parseInt($("#gradingDiv_" + studentId + "").attr("data-selectedGradingId"));

        groupQuiz.loadGradingTemplateItemPopup($(this), translatedResources.assign, gradingId, studentId, QuizId, QuizResultId, selectedGradeId, 1);
        //if (isCompleted == "True") {
        //    assignmentStudentDetails.loadGradingTemplateItemPopup($(this), translatedResources.Assign, gradingTemplateId, studAsgId, 1);
        //}
        //else {
        //    globalFunctions.showMessage("error", "Please mark it as completed then you can assign grade.");
        //}
    });
    $(".btnFeedback").click(function () {
        var studentId = parseInt($(this).attr("data-studentId"));
        var QuizId = $(this).attr("data-QuizId");
        var ResourceId = $(this).attr("data-ResourceId");
        var QuizResultId = $(this).attr("data-QuizResultId");
        groupQuiz.loadQuizFeedBackPopup($(this), translatedResources.feedback, studentId, QuizId, QuizResultId, ResourceId);
    });
    $(document).on('click', '#ViewQuizQuestionFile', function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=editStudentQuizQuestion", "_blank");
        }
    });
    $(document).on('click', '#ViewQuizFile', function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=studentQuiz", "_blank");
        }
    });
    $(document).on('click', '#StopQuiz', function (e) {
        var id = $(this).data("quizid");
        var resourceId = $(this).data("resourceid");
        if (globalFunctions.isValueValid(id)) {
            groupQuiz.logQuizTime($(this), id, resourceId, 'Q', false);
        }
    });

    $(document).on('click', '#btnDeleteRecordedFile', function (e) {

        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);

        var fileId = $(this).attr('data-fileId');
        $(document).bind('okClicked', this, function (e) {
            groupQuiz.deleteRecording(fileId);
        });

    });
    //CKEDITOR.replaceClass('QuizFreeText', {
    //    htmlencodeoutput: false,
    //    height: '180px',
    //    enterMode: CKEDITOR.ENTER_BR,
    //    shiftEnterMode: CKEDITOR.ENTER_P
    //});
    //CKEDITOR.plugins.addExternal('ckeditor_wiris', 'https://www.wiris.net/demo/plugins/ckeditor/', 'plugin.js');
    var currentUrl = window.location.href;
    if (currentUrl.toLowerCase().indexOf("home") == -1) {
        CKEDITOR.plugins.addExternal('ckeditor_wiris', '', 'plugin.js');
    }
    $('.freetext').each(function () {
        var selectedId = "QuizFreeText_" + $(this).data("answerid")
        CKEDITOR.replace(selectedId, {
            htmlencodeoutput: false,
            height: '180px',
            extraPlugins: 'ckeditor_wiris',
            allowedContent: true,
            enterMode: CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P
        }).on('blur', function () {
        });
    });

    $('body').on('click', '.DeleteAnswerFile', function () {
        //debugger;
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        var fileid = $(this).attr('data-fileid');
        var resourceid = $(this).attr('data-resourceid');
        var questionid = $(this).attr('data-questionid');
        var studentid = $(this).attr('data-studentid');
        var divid ="#"+ $(this).closest('div[id^="freeTextFileList_"]').attr('id');
        $(document).bind('okClicked', $(this), function (e) {
            groupQuiz.deleteAnswerFile(fileid, resourceid, 'G', questionid, studentid, divid);
        });
    });

});
