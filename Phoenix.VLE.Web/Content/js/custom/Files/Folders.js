﻿var folder = (function ($, w) {
    var baseUri = "/Files/Files/";
    var init = function () {

        //if ($("input#search-myfiles").val().length > 0) {
        //    $("input#search-myfiles").trigger("keyup");
        //}

        $(document).on("keyup", "input#search-myfiles", function () {
            filterFolders($(this).val());
        });
        $(document).on("change", "#ddlFilesSort", function () {
            navigateToFolder();
        });
        $(document).on('click', '#btnAddFolder', function () {
            var moduleId = $("#ModuleId").val();
            var parentFolderId = $("#ParentFolderId").val();
            var sectionId = $("#SectionId").val();
            folder.addfolderPopup($(this), moduleId, parentFolderId, sectionId);
        });
        $(document).on('click', '#btnEditFolder', function () {
            //debugger
            var id = $(this).data("id");
            var moduleId = $("#ModuleId").val();
            var parentFolderId = $("#ParentFolderId").val();
            var sectionId = $("#SectionId").val();
            //console.log(id);
            folder.editfolderPopup($(this), id, moduleId, parentFolderId, sectionId);
        });
        showSelectedView();
        initDatatable('myfiles-list');
    },

        navigateToFolder = function (folderId) {
            var moduleId = $("#ModuleId").val();
            folderId = folderId === null ? null : folderId !== undefined ? folderId : $("#ParentFolderId").val() > 0 ? $("#ParentFolderId").val() : null;
            if (folderId > 0)
                $("#ParentFolderId").val(folderId);
            var sectionId = $("#SectionId").val() > 0 ? $("#SectionId").val() : "";
            var sortBy = $("#ddlFilesSort").val();
            $("#user-files-view").load(baseUri + "SeachMyFiles?folderId=" + folderId + "&sectionId=" + sectionId + "&moduleId=" + moduleId + "&sortBy=" + sortBy, function () {
                globalFunctions.enableCheckboxCascade(".chkSelectMyFiles", ".chkFileFolder");
                globalFunctions.enableCheckboxCascade(".chkSelectMyFilesList", ".chkFileFolderList");
                files.showHideCopDeletebtn();
                showSelectedView();
                $('.tooltip').tooltip('hide');
                //clear search text
                $("input#search-myfiles").val('')
                if (folderId == null) {
                    $("#bookmark-list").show();
                    $("#btnAddBookmark").closest("li").show();

                }
                else {
                    $("#bookmark-list").hide();
                    $("#btnAddBookmark").closest("li").hide();
                }
                initDatatable('myfiles-list');
                globalFunctions.convertImgToSvg();

            });
        },

        toggleViewStyle = function (source) {
            if (!$(source).hasClass("active")) {
                $("ul.list-pattern li").removeClass("active");
                $(source).addClass("active");
                $(".list-wrapper").toggleClass("hide-item", $(source).data("toggle") == "grid");
                $(".grid-wrapper").toggleClass("hide-item", $(source).data("toggle") == "list");
                if ($(source).data("toggle") == "grid") {
                    globalFunctions.setCookie("selectedView", "grid");
                    $("#gridSelectAllDiv").show();
                }
                else {
                    globalFunctions.setCookie("selectedView", "list");
                    $("#gridSelectAllDiv").hide();
                }
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal("hide");
                navigateToFolder();
                if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
                    groupDetail.loadTopicActivitiesList();
                }
            }
        },

        filterFolders = function (searchString) {
            var source = $(".folder-container[data-searchtext], table tr.row-folder[data-searchtext],.file-container[data-searchtext], table tr.row-file[data-searchtext]");
            $(source).filter(function () {
                $(this).toggle($(this).data("searchtext").toLowerCase().indexOf(searchString.trim().toLowerCase()) > -1)
            });
            //folder files
            if ($('#user-files-view .row-folder:visible,#user-files-view .grid-container:visible,#user-files-view .file-container:visible,#user-files-view .row-file:visible').length === 0) {
                $("#user-files-view .nofolderdiv").show();
            }
            else {
                $("#user-files-view .nofolderdiv").hide();
            }
            //bookmarks
            if ($('#bookmark-list .row-folder:visible,#bookmark-list .grid-container:visible,#bookmark-list .row-file:visible,#bookmark-list .file-container:visible').length === 0) {
                $("#bookmark-list .nofolderdiv").show();
            }
            else {
                $("#bookmark-list .nofolderdiv").hide();
            }

        },

        downloadFolderFiles = function (folderId) {
            $.get(baseUri + "DownloadFolderFiles?folderId=" + folderId, function (data) {
                if (globalFunctions.isValueValid(data))
                    w.location.assign("/shared/shared/DownloadFile?filePath=" + data);
            });
        },

        initRenameFolderForm = function (source, folderId) {
            globalFunctions.loadPopup($(source), baseUri + "/InitAddEditFolderForm?folderId=" + folderId, "Rename Folder");
        },
        loadfolderPopup = function (source, mode, id, moduleId, parentFolderId, sectionId) {
            var title = mode + ' ' + translatedResources.foldertitle;
            globalFunctions.loadPopup(source, baseUri + '/InitAddEditFolderForm?id=' + id + '&moduleid=' + moduleId + '&parentfolderid=' + parentFolderId + '&sectionid=' + sectionId, title, 'folder-dailog');

        },

        addfolderPopup = function (source, moduleId, parentFolderId, sectionId) {
            loadfolderPopup(source, translatedResources.add, "", moduleId, parentFolderId, sectionId);

        },

        editfolderPopup = function (source, id, moduleId, parentFolderId, sectionId) {
            loadfolderPopup(source, translatedResources.edit, id, moduleId, parentFolderId, sectionId);
        },

        deletefolderData = function (source, id) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        url: '/Files/Files/DeleteFolderData/?id=' + id,
                        type: 'GET',
                        success: function (data) {
                            globalFunctions.showMessage(data.NotificationType, data.Message);
                            if (data.Success === true) {

                                navigateToFolder();
                                if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
                                    groupDetail.loadTopicActivitiesList();
                                }
                            }
                            else {
                                globalFunctions.onFailure();
                            }
                        },
                        error: function (data, xhr, status) {
                            //debugger;
                            globalFunctions.onFailure();
                        }

                    });
                });
            }
        },
        initDatatable = function (_tableId) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                searching: false,
                columnSelector: false,
                paging: false,
                columnDefs: [{
                    "targets": "nosort",
                    'orderable': false,
                },
                    {
                    "targets": [5],
                    "sWidth": "14%",
                    'orderable': false,
                }, {
                    "targets": [0],
                    'orderable': false,
                }, {
                    "targets": [1],
                    "sWidth": "40%",
                    'orderable': false
                }, {
                    "targets": [3],
                    "sWidth": "20%",
                    'orderable': false
                }],
                info: false
            };
            grid.init(settings);
        },
        showSelectedView = function () {
            var currentView = globalFunctions.getCookie("selectedView");
            if (currentView == "grid") {
                $(".list-wrapper").addClass("hide-item");
                $(".grid-wrapper").removeClass("hide-item");
                $(".list-pattern li.grid-view").addClass("active");
                $("#gridSelectAllDiv").show();
            }
            else {
                $(".list-wrapper").removeClass("hide-item");
                $(".grid-wrapper").addClass("hide-item");
                $(".list-pattern li.list-view").addClass("active");
                $("#gridSelectAllDiv").hide();
            }
        };

    return {
        navigateToFolder: navigateToFolder,
        toggleViewStyle: toggleViewStyle,
        initRenameFolderForm: initRenameFolderForm,
        onSaveSuccess: onSaveSuccess,
        init: init,
        downloadFolderFiles: downloadFolderFiles,
        addfolderPopup: addfolderPopup,
        editfolderPopup: editfolderPopup,
        deletefolderData: deletefolderData,
        showSelectedView: showSelectedView
    }
})(jQuery, window);

$(function () { folder.init() });
