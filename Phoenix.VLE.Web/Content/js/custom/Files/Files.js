﻿var files = (function () {
    var baseUri = "/Files/Files/";
    var init = function () {
        //added action to breadcrumb
        var actionBtnClipboard = $("<button />", { "class": "btn btn-primary mr-3 btn-sm m-0", onclick: "files.showClipboardData($(this))", text: translatedResources.clipboard, type: "button" });
        var actionBtnBlkCopy = $("<button />", { style: "display:none", id: "BtnBlkCopy", "class": "btn btn-primary mr-3 btn-sm m-0", text: translatedResources.copy, type: "button" });
        var actionBtnBlkDetele = $("<button />", { style: "display:none", id: "BtnBlkDetele", "class": "btn btn-primary mr-3 btn-sm m-0", text: translatedResources.delete, type: "button" });

        var actionToggleView = $("<ul />", { "class": "list-pattern flex-wrap flex-sm-nowrap" })
            .append($("<li />").append(translatedResources.customPermission ? actionBtnBlkCopy : ""))
            .append($("<li />").append(translatedResources.customPermission ? actionBtnBlkDetele : ""))
            .append($("<li />").append(translatedResources.customPermission ? actionBtnClipboard : ""))
            .append($("<li />", { "class": "type-list", text: translatedResources.View }))
            .append($("<li />", { "class": "grid-view  mt-sm-0", onclick: "folder.toggleViewStyle(this)", "data-toggle": "grid" }))
            .append($("<li />", { "class": "list-view  mt-sm-0", onclick: "folder.toggleViewStyle(this)", "data-toggle": "list" }));

        $('#breadcrumbActions').append(actionToggleView);

        $(document).on('click', '#btnDeleteFile', function () {
            var id = $(this).data("id");
            files.deletefileData($(this), id);
        });
        $(document).on('click', '#btnViewFile', function (e) {
            if ($(e.target).is("[data-cloudfilepath]")) {
                location.href = $(this).data("cloudfilepath");
            } else {
                var id = $(this).data("id");
                files.viewFile($(this), id);
            }

        });
        $(document).on('click', '#BtnBlkCopy', function () {
            let ids = [];
            $.each($(".file-container input[type='checkbox']:checked"), function () {
                let id = $(this).data("id");
                ids.push(id);
            });
            $.each($(".row-file input[type='checkbox']:checked"), function () {
                let id = $(this).data("id");
                ids.push(id);
            });
            //remove duplicate
            ids = ids.filter(function (elem, index, self) {
                return index === self.indexOf(elem);
            });
            if (ids.length > 0) {
                $.post(baseUri + "/FileBulkCopy", { ids: ids }, function (data) {
                    if (data.Success) {
                        globalFunctions.showSuccessMessage(translatedResources.Copied);
                    }
                    else {
                        globalFunctions.onFailure();
                    }
                });
            }
        });
        $(document).on('click', '#BtnBlkDetele', function () {
            let filesIds = [];
            let foldersIds = [];

            $.each($(".folder-container input[type='checkbox']:checked"), function () {
                let id = $(this).data("id");
                foldersIds.push(id);
            });
            $.each($(".row-folder input[type='checkbox']:checked"), function () {
                let id = $(this).data("id");
                foldersIds.push(id);
            });
            $.each($(".file-container input[type='checkbox']:checked"), function () {
                let id = $(this).data("id");
                filesIds.push(id);
            });
            $.each($(".row-file input[type='checkbox']:checked"), function () {
                let id = $(this).data("id");
                filesIds.push(id);
            });
            //remove duplicate
            filesIds = filesIds.filter(function (elem, index, self) {
                return index === self.indexOf(elem);
            });
            foldersIds = foldersIds.filter(function (elem, index, self) {
                return index === self.indexOf(elem);
            });
            if (filesIds.length > 0 || foldersIds.length > 0) {
                globalFunctions.notyDeleteConfirm($(this), translatedResources.deleteConfirm);
                $(document).bind('okToDelete', $(this), function (e) {
                    $.post(baseUri + "/FileFolderBulkDelete", { filesIds: filesIds, foldersIds: foldersIds }, function (data) {
                        if (data.Success) {
                            globalFunctions.showMessage(data.NotificationType, data.Message);
                            folder.navigateToFolder();
                        }
                        else {
                            globalFunctions.onFailure();
                        }
                    });
                });
            }
        });

        $(document).on('change', ".grid-view input[type='checkbox']", function (event) {
            showHideCopDeletebtn();
        });
        $(document).on('change', "#gridSelectAllDiv input[type='checkbox']", function (event) {
            if ($(this).is(":checked")) {
                $.each($(".grid-container input[type='checkbox']:checked"), function () {
                    $(this).closest(".check-item").addClass("show");
                })
            }
            else {
                $.each($(".grid-container input[type='checkbox']"), function () {
                    $(this).closest(".check-item").removeClass("show");
                })
            }
            showHideCopDeletebtn();
        });
        //uncheck grid checkbox
        //$(document).on("change", "input:checkbox.chkSelectMyFilesList", function () {
        //    $("input:checkbox.chkSelectMyFiles").prop("checked", !$(this).is(":checked"));
        //});
        ////uncheck list checkbox
        //$(document).on("change", "input:checkbox.chkSelectMyFiles", function () {
        //    $("input:checkbox.chkSelectMyFilesList").prop("checked", !$(this).is(":checked"));
        //});
    },

        initFileRenameForm = function (source, fileId) {
            globalFunctions.loadPopup($(source), baseUri + "/InitAddEditFile?fileId=" + fileId, translatedResources.renameFile);
        },

        onFileSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal("hide");
                folder.navigateToFolder($("#ParentFolderId").val());
                if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
                    groupDetail.loadTopicActivitiesList();
                }
            }
        },
        deletefileData = function (source, id) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                //globalFunctions.deleteItem('file', '/Files/Files/DeletefileData', 'tbl-file', source, id);
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        url: '/Files/Files/DeleteFileData/?id=' + id,
                        type: 'GET',
                        success: function (data) {
                            //debugger;
                            if (data.Success === true) {
                                globalFunctions.showMessage(data.NotificationType, data.Message);
                                folder.navigateToFolder();
                                if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
                                    groupDetail.loadTopicActivitiesList();
                                }
                            }
                            else {
                                globalFunctions.onFailure();
                            }
                        },
                        error: function (data, xhr, status) {
                            //debugger;
                            globalFunctions.onFailure();
                        }

                    });
                });
            }
        },

        downloadFile = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                //window.open("/Document/Viewer/Index?id=" + id +"&module=file", "_blank ");
                window.location.href = "/Files/Files/DownloadFile/?id=" + id;
            }
        },

        loadfileGrid = function (enableFileDownloads) {
            //debugger;
            var moduleId = $("#ModuleId").val();
            var parentFolderId = $("#ParentFolderId").val();
            var sectionId = $("#SectionId").val();

            $.ajax({
                url: '/Files/Files/GetFileListView/?moduleId=' + moduleId + "&pfId=" + parentFolderId + "&sectionId=" + sectionId + "&enableFileDownload=" + enableFileDownloads,
                type: 'GET',
                success: function (data) {
                    //debugger;
                    if (data !== '') {
                        $("#user-files-view").empty();
                        $("#user-files-view").html(data);
                        $("#NoFolderDiv").hide();
                    }
                    else {
                        $("#user-files-view").html("");
                    }
                    if ($("ul.list-pattern li.list-view").hasClass("active")) {
                        $(".list-wrapper").removeClass("hide-item");
                        $(".grid-wrapper").addClass("hide-item");
                    }
                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        viewFile = function (source, id) {
            //source.preventDefault();
            if (globalFunctions.isValueValid(id)) {
                window.open("/Document/Viewer/Index?id=" + id + "&module=file", "_blank");
            }
        },
        showClipboardData = function (source) {
            //source.preventDefault();
            var title = translatedResources.clipboard;
            globalFunctions.loadPopup(source, '/Files/Files/GetClipboard', title, 'modal-lg p-2');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {

            });
        },
        copyToClipboard = function (source, id) {
            //source.preventDefault();
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    url: '/Files/Files/CopyToClipboard/?id=' + id,
                    type: 'GET',
                    success: function (data) {
                        //debugger;
                        if (data.Success === true) {
                            globalFunctions.showSuccessMessage(translatedResources.Copied);
                        }
                        //else {
                        // $("#btnClipboard").hide();
                        //}

                    },
                    error: function (data, xhr, status) {
                        //debugger;
                        globalFunctions.onFailure();
                    }

                });
            }
        },
        addFromClipboard = function (source, id) {
            //source.preventDefault();
            if (globalFunctions.isValueValid(id)) {

                var moduleId = $("#ModuleId").val();
                var parentFolderId = $("#ParentFolderId").val();
                var sectionId = $("#SectionId").val();
                //console.log(moduleId + '_' + parentFolderId + '_' + sectionId);
                $.ajax({
                    url: '/Files/Files/AddFromClipboard',
                    type: 'POST',
                    data: { 'id': id, 'moduleId': moduleId, 'parentFolderId': parentFolderId, 'sectionId': sectionId },
                    async: true,
                    success: function (data) {
                        //debugger;
                        if (data.Success === true) {
                            //$("#btnClipboard").show();

                            folder.navigateToFolder();
                            $(source).closest(".clipboard-item").remove();
                            if ($(".clipboard-list .clipboard-item").length <= 0)
                                $("#myModal").modal("hide");
                            //$("#myModal").modal('hide');
                            globalFunctions.showSuccessMessage(translatedResources.FileUploaded);
                            if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
                                groupDetail.loadTopicActivitiesList();
                            }
                        }
                        else {
                            globalFunctions.showWarningMessage(data.Message);
                        }

                    },
                    error: function (data, xhr, status) {
                        //debugger;
                        globalFunctions.onFailure();
                    }

                });
            }
        },

        removeFromClipboard = function (source, id) {
            //source.preventDefault();
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    url: '/Files/Files/RemoveFromClipboard/?id=' + id,
                    type: 'GET',
                    success: function (data) {
                        //debugger;
                        if (data.Success === true) {
                            //showClipboardData($("#btnClipboard"));
                            globalFunctions.showSuccessMessage(translatedResources.Removed);
                            $(source).closest(".clipboard-item").remove();
                            $('.tooltip').tooltip('hide');
                            if ($(".clipboard-list .clipboard-item").length <= 0)
                                $("#myModal").modal("hide");
                        }
                        else {
                            //$("#btnClipboard").hide();
                        }

                    },
                    error: function (data, xhr, status) {
                        //debugger;
                        globalFunctions.onFailure();
                    }

                });
            }
        },
        showHideCopDeletebtn = function () {
            if ($(".file-container input[type='checkbox']:checked,.row-file input[type='checkbox']:checked,.folder-container input[type='checkbox']:checked,.row-folder input[type='checkbox']:checked").length > 0) {
                $("#BtnBlkCopy").show();
                $("#BtnBlkDetele").show();
            }
            if ($(".folder-container input[type='checkbox']:checked,.row-folder input[type='checkbox']:checked").length > 0) {
                $("#BtnBlkDetele").show();
                $("#BtnBlkCopy").hide();
            }
            if ($(".file-container input[type='checkbox']:checked,.row-file input[type='checkbox']:checked,.folder-container input[type='checkbox']:checked,.row-folder input[type='checkbox']:checked").length == 0) {
                $("#BtnBlkCopy").hide();
                $("#BtnBlkDetele").hide();
            }
        };

    return {
        initFileRenameForm: initFileRenameForm,
        onFileSaveSuccess: onFileSaveSuccess,
        init: init,
        deletefileData: deletefileData,
        downloadFile: downloadFile,
        loadfileGrid: loadfileGrid,
        viewFile: viewFile,
        copyToClipboard: copyToClipboard,
        showClipboardData: showClipboardData,
        addFromClipboard: addFromClipboard,
        removeFromClipboard: removeFromClipboard,
        showHideCopDeletebtn: showHideCopDeletebtn
    }
})(jQuery, window);
$(function () {
    files.init();
})

