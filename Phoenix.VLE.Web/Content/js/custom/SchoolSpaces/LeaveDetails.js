﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var addEditLeaveRequest = function () {
    var init = function () {
        $("#dropAreaLeave").fileinput({
            title: "Drag and drop any supporting doc (Max 1 File)",
            theme: "fas",
            //title: 'Drag and drop any supporting doc (Max 1 File)',
            uploadUrl: "/file-upload-batch/2",
            showUpload: false,
            showCaption: false,
            showRemove: false,
            maxFileCount: 1,
            browseClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
            removeClass: "btn btn-sm m-0 z-depth-0",
            uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
            cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
            overwriteInitial: false,
            allowedFileExtensions: translatedResources.FileExtensions,
            initialPreviewAsData: true, // defaults markup
            initialPreviewConfig: [{
                frameAttr: {
                    title: 'My Custom Title',
                }
            }],
            uploadExtraData: {
                img_key: "1000",
                img_keywords: "happy, nature"
            },
            fileActionSettings: {
                showZoom: false,
                showUpload: false,
                //indicatorNew: "",
                showDrag: false
            },
            preferIconicPreview: false, // this will force thumbnails to display icons for following file extensions
            previewSettings: {
                image: { width: "100%", height: "auto" },
                /* html: { width: "50px", height: "auto" },
                other: { width: "50px", height: "auto" } */
            },
        });
        //$("#dropAreaLeave").fileinput({
        //    language: translatedResources.locale,
        //    title: translatedResources.BrowseFile,
        //    theme: "fas",
        //    showUpload: false,
        //    showCaption: false,
        //    showMultipleNames: false,
        //    maxFileCount: 1,
        //    //maxImageWidth: 520,
        //    //maxImageHeight: 420,
        //    //showPreview: true,
        //    fileActionSettings: {
        //        showZoom: false,
        //        showDrag: false
        //    },

        //    //uploadAsync: true,
        //    browseClass: "btn btn-sm m-0 z-depth-0 btn-browse",
        //    removeClass: "btn btn-sm m-0 z-depth-0",
        //    //uploadClass: "btn btn-sm m-0 z-depth-0",
        //    overwriteInitial: false,
        //    //previewFileIcon: '<i class="fas fa-file"></i>',
        //    initialPreviewAsData: false, // defaults markup
        //    initialPreviewConfig: [{
        //        frameAttr: {
        //            title: 'My Custom Title',
        //        }
        //    }],
        //    allowedFileExtensions: translatedResources.FileExtensions,
        //    preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
        //    previewFileIconSettings: { // configure your icon file extensions
        //        'doc': '<i class="fas fa-file-word ic-file-word"></i>',
        //        'xls': '<i class="fas fa-file-excel ic-file-excel"></i>',
        //        'ppt': '<i class="fas fa-file-powerpoint ic-file-ppt"></i>',
        //        'pdf': '<i class="fas fa-file-pdf ic-file-pdf"></i>',
        //        'zip': '<i class="fas fa-file-archive ic-file"></i>',
        //        'htm': '<i class="fas fa-file-code ic-file"></i>',
        //        'txt': '<i class="fas fa-file-alt ic-file"></i>',
        //        'mov': '<i class="fas fa-file-video ic-file"></i>',
        //        'mp3': '<i class="fas fa-file-audio ic-file"></i>',
        //        // note for these file types below no extension determination logic
        //        // has been configured (the keys itself will be used as extensions)
        //        'jpg': '<i class="fas fa-file-image ic-file-img"></i>',
        //        'jpeg': '<i class="fas fa-file-image ic-file-img"></i>',
        //        'gif': '<i class="fas fa-file-image ic-file-img"></i>',
        //        'png': '<i class="fas fa-file-image ic-file-img"></i>',
        //        'svg': '<i class="fas fa-file-image ic-file-img"></i>'
        //    },
        //    previewSettings: {
        //        image: { width: "50px", height: "auto" },
        //        html: { width: "50px", height: "auto" },
        //        other: { width: "50px", height: "auto" }
        //    },
        //    previewFileExtSettings: { // configure the logic for determining icon file extensions
        //        'doc': function (ext) {
        //            return ext.match(/(doc|docx)$/i);
        //        },
        //        'xls': function (ext) {
        //            return ext.match(/(xls|xlsx)$/i);
        //        },
        //        'ppt': function (ext) {
        //            return ext.match(/(ppt|pptx)$/i);
        //        },
        //        'zip': function (ext) {
        //            return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
        //        },
        //        'htm': function (ext) {
        //            return ext.match(/(htm|html)$/i);
        //        },
        //        'txt': function (ext) {
        //            return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
        //        },
        //        'mov': function (ext) {
        //            return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
        //        },
        //        'mp3': function (ext) {
        //            return ext.match(/(mp3|wav)$/i);
        //        }
        //    }
        //}).on('fileselect', function (event, numFiles, label) {
        //    if ($("#IsAddMode").val().toBoolean())
        //        $("#ActualImageName").val(label);
        //});
    },
        saveLeaveRequest = function (e) {
            var _URL = window.URL || window.webkitURL;
            if ($('form#frmAddEditLeaveRequest').valid()) {
                var formData = new FormData();
                var token = $('input[name=__RequestVerificationToken]').val();
                formData.append("__RequestVerificationToken", token);
                //if ($("#UploadedImageName").val() == '') {
                //    var leaveReqFile = document.getElementById("dropAreaLeave").files[0];
                //    formData.append("leaveReqFile", leaveReqFile);
                //}
                formData.append("UploadedImageName", $('#UploadedImageName').val());

                formData.append("ActualImageName", $('#ActualImageName').val());
                //var form = new FormData();
                var FromDate = $('#From_Date').val();
                var ToDate = $('#To_Date').val();
                var remark = $("#Remarks").val();
                var Leavetype = $("#LeaveReasonId :selected").val();
                var fileUpload = $("#dropAreaLeave").get(0);
                var studNo = $("#StudentNo").val();
                var ref_Id = $("#Ref_Id").val();
                var slType = $("#Leave_Type").val();
                var isEdit = $("#Enable_Button").val() == "1" && ref_Id > 0 ?  true : false;
                var files = fileUpload.files;
                var erFil = $('.file-error-message').html();
                if (erFil != "" && $("#UploadedImageName").val() == '') {
                    globalFunctions.showErrorMessage(translatedResources.showFileuploadError);
                    return;
                }
                if (Leavetype == undefined || Leavetype == '' || FromDate == undefined || FromDate.trim() == '' || ToDate == undefined || ToDate.trim() == '' || remark == undefined || remark.trim() == '') {
                    globalFunctions.showErrorMessage("All mandatory fields are required.");
                    return;
                }
                if (remark.length > 250) {
                    globalFunctions.showErrorMessage("Maximum 250 characters are allowed for remark field.");
                    return;
                }
                if (files.length > 0) {
                    var fName = files[0].name;
                    var fil = files[0];
                    formData.append(fName, fil);
                } else { formData.append('isFile', false);}               
                formData.append('FromDate', FromDate);
                formData.append('ToDate', ToDate);
                formData.append('remark', remark);
                formData.append('Leavetype', Leavetype);
                formData.append('stuId', studNo);
                formData.append('refId', ref_Id);
                formData.append('SLAType', slType);
                formData.append('isEdit', isEdit);

                submitLeaveRequest(formData);         
            }
        },
        submitLeaveRequest = function (formdata) {
           
            $.ajax({
                url: '/ParentCorner/Leave/LeaveRequestSubmit',
                type: "POST",
                contentType: false, // Not to set any content header  
                processData: false, // Not to process data  
                data: formdata,
                dataType:"json",
                success: function (result) {
                    var data = JSON.parse(result.events), msg = "", succ = ""; 
                    succ = data.success.toLowerCase(); msg = data.message; 
                    //if (formdata.get("isFile") != "false") { succ = data.success.toLowerCase(); msg = data.message; }
                    //else { succ = data.Success.toLowerCase(); msg = data.Message; }
                    if (succ == "true") {
                        //globalFunctions.showMessage("success", msg);
                       
                        $("#leaveRequestModal").modal("hide");
                        debugger
                        var strRel = formdata.get("SLAType") == "Leave Request" ? "LR" : "LL";
                        strRel = strRel + "-" + formdata.get("stuId");
                        $("#recordUpdated").modal("show");
                        strRefresh = '/ParentCorner/Leave/LeaveDetails?selectSection=' + strRel;
                        //window.location.href = '/ParentCorner/Leave/LeaveDetails?selectSection=' + strRel;                        
                        //$.get('/ParentCorner/Leave/LeaveDetails', { selectSection: strRel }, function () {}); 
                    }
                    else {
                        globalFunctions.showErrorMessage(msg);
                    }
                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });
            //return;
        },
        onSaveLeaveSuccess = function (isAddmode) {
            //debugger
            if (isAddmode == 'True') {
                globalFunctions.showSuccessMessage(translatedResources.AddSuccess);
            } else {
                globalFunctions.showSuccessMessage(translatedResources.EditSuccess);
            }
            $("#leaveRequestModal").modal('hide');
            addEditLeaveRequest.init();
        },
        onSaveLeaveError = function (isAddmode) {
            //debugger
            init();
            if (isAddmode == 'True') {
                globalFunctions.showErrorMessage(translatedResources.AddError);
            } else {
                globalFunctions.showErrorMessage(translatedResources.EditError);
            }
            $("#leaveRequestModal").modal('hide');
        },
        deleteImage = function (source) {
            globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
            $(source).unbind('okClicked');
            $(source).bind('okClicked', source, function () {
                $('.attachments-preview').addClass("d-none");
                $('#UploadedImageName').val('');
                $('#imgUpload').removeClass('d-none');
            });
        }
    return {
        init: init,
        saveLeaveRequest: saveLeaveRequest,
        submitLeaveRequest: submitLeaveRequest,
        onSaveLeaveSuccess: onSaveLeaveSuccess,
        onSaveLeaveError: onSaveLeaveError,
        deleteImage: deleteImage
    };
}();




$(document).ready(function () {
    debugger
    formElements.feSelect();
    addEditLeaveRequest.init();
    $(".selectpicker").selectpicker(); /// { noneSelectedText: "Please Select", refresh: "refresh" }
    var isAdd = $("#IsAddMode").val();
    if (isAdd.toLowerCase() == "false") {
        $('#frmAddEditLeaveRequest input,textarea').attr('readonly', 'readonly');
        $('#frmAddEditLeaveRequest .selectpicker').prop('disabled', true);
    }
    $('.file-drop-zone-title').text("Drag and drop any supporting doc (Max 1 File)");

    $('#leaveRequestModal').on('hidden.bs.modal', function () {
      $("#noty_topCenter_layout_container").html("");
    });


    var fDT = $("#From_Date").val();
    var tod = new Date();
    if (fDT) {
        tod = new Date(fDT);
    }
    var minDateData = new Date(tod.getFullYear(), (tod.getMonth()), tod.getDate(), 0, 0, 0, 0);
    //datepicker
    $('.date-picker2').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: false,
        minDate: minDateData
    });

    $('.date-picker1').datetimepicker({
        format: 'DD-MMM-YYYY',
        minDate: minDateData,
    }).on('dp.change', function (e) {
        //debugger
        //alert(e.date);
        //$('.date-picker2').data("DateTimePicker").clear();
        $('.date-picker2').data("DateTimePicker").minDate(e.date);
        if ($('.date-picker2').data("DateTimePicker").date() < e.date) {
            $('.date-picker2').data("DateTimePicker").clear();
            $('.date-picker2').data("DateTimePicker").minDate(e.date);
        }
    });

    $(".date-picker2").on("dp.change", function (e) {
        //debugger
        //
        //console.log(e.date);
        //$('.date-picker1').data("DateTimePicker").minDate(e.date);
        //$(".date-picker1").datetimepicker("refresh");
    });

});


$(document).on('click', '#ahrefDeleteFiles', function () {
    $("#noty_topCenter_layout_container").html("");
    addEditLeaveRequest.deleteImage(this);
    //globalFunctions.notyDeleteConfirm($(this), translatedResources.deleteConfirm);
    //$(document).bind('okToDelete', $(this), function (e) {
    //    addEditLeaveRequest.deleteImage();
    //    $("#library").addClass("d-none");
    //    $("#imgUpload").removeClass("d-none");
    //});
});


