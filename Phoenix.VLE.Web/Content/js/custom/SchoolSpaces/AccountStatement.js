﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var _dataTableLangSettings = {
    "sEmptyTable": translatedResources.noData,
    "sInfo": translatedResources.sInfo,
    "sInfoEmpty": translatedResources.sInfoEmpty,
    "sInfoFiltered": translatedResources.sInfoFiltered,
    "sInfoPostFix": "",
    "sInfoThousands": ",",
    "sLengthMenu": translatedResources.sLengthMenu,
    "sLoadingRecords": "Loading...",
    "sProcessing": "Processing...",
    "sSearch": translatedResources.search,
    "sZeroRecords": translatedResources.sZeroRecords,
    "oPaginate": {
        "sFirst": translatedResources.first,
        "sLast": translatedResources.last,
        "sNext": translatedResources.next,
        "sPrevious": translatedResources.previous
    },
    "oAria": {
        "sSortAscending": translatedResources.sSortAscending,
        "sSortDescending": translatedResources.sSortDescending
    }
};

$(document).ready(function () {
    $(".selectpicker").selectpicker();
    var tod = new Date();
    var minDateData = new Date(tod.getFullYear(), tod.getMonth(), 1);
    var last_date = new Date(tod.getFullYear(), tod.getMonth() + 1, 0);

    $(".accountDetTodate").datetimepicker({
        format: 'DD-MMM-YYYY',
        widgetPositioning: {
            //horizontal: 'right',
            vertical: 'bottom'
        },
        minDate: minDateData
    });

    $(".accountDetFromdate").datetimepicker({
        format: 'DD-MMM-YYYY',
        widgetPositioning: {
            //horizontal: 'right',
            vertical: 'bottom'
        }
    }).on("dp.change", function (e) {
        //$('.accountDetTodate').data("DateTimePicker").clear();
        $('.accountDetTodate').data("DateTimePicker").minDate(e.date);
    });

    $(".paymHistTodate").datetimepicker({
        format: 'DD-MMM-YYYY',
        widgetPositioning: {
            //horizontal: 'right',
            vertical: 'bottom'
        },
        minDate: minDateData
    });

    $(".paymHistFromdate").datetimepicker({
        format: 'DD-MMM-YYYY',
        widgetPositioning: {
            //horizontal: 'right',
            vertical: 'bottom'
        }
    }).on("dp.change", function (e) {
        $('.paymHistTodate').data("DateTimePicker").minDate(e.date);
    });

    $(".navAccountDet").on("click", function () {
        if (!$(this).hasClass("active")) {
            $(this).addClass("active");
        }
        $(".navAccountSum").removeClass("active");
        $(".detailsSection").removeClass("hide-item");
        $(".accountsDetails-Filter").removeClass("hide-item");
        if (!$(".summarySection").hasClass("hide-item")) {
            $(".summarySection").addClass("hide-item");
        }
        if (!$(".accountsSummary-Filter").hasClass("hide-item")) {
            $(".accountsSummary-Filter").addClass("hide-item");
        }
    });
    $(".navAccountSum").on("click", function () {
        if (!$(this).hasClass("active")) {
            $(this).addClass("active");
        }
        $(".navAccountDet").removeClass("active");
        $(".summarySection").removeClass("hide-item");
        $(".accountsSummary-Filter").removeClass("hide-item");
        if (!$(".detailsSection").hasClass("hide-item")) {
            $(".detailsSection").addClass("hide-item");
        }
        if (!$(".accountsDetails-Filter").hasClass("hide-item")) {
            $(".accountsDetails-Filter").addClass("hide-item");
        }
    });

});


function OnAccountStatementFilter() {

    var type = "", from = "", to = "", students = ""; var getAllChk = [];
    $.each($("input[name='accountStatement']:checked"), function () {
        getAllChk.push($(this).val());
    });
    if (getAllChk.length > 0) {
        students = getAllChk.toString();
    }
    if (students) {
        if ($(".accountsDetails-Filter").is(":visible")) {
            type = $("#ddlFeeTypeAD").val();
            from = $("#accountDetFromDate").val();
            to = $("#accountDetToDate").val();
            GetAccountsDetails(students, type, from, to);
        }
        else if ($(".accountsSummary-Filter").is(":visible")) {
            type = $("#ddlFeeTypeAS").val();
            var tempYear = $("#AccountSummaryYear").val();
            var splitDate = tempYear.split("|");
            from = splitDate[0];
            to = splitDate[1];
            GetAccountsSummary(students, type, from, to);
        }
    }
}

function GetAccountsDetails(students, type, from, to) {
    $.ajax({
        url: '/ParentCorner/Fee/GetAccountDetails?students=' + students + "&fromDate=" + from + "&toDate=" + to + "&type=" + type,
        //data: JSON.stringify(model),
        dataType: 'html',
        type: "GET",
        success: function (response) {
            $("#accountStatContent .detailsSection").html("");
            $("#accountStatContent .detailsSection").html(response);
            if ($.fn.DataTable.isDataTable('#tblAccountDetails')) {
                $('#tblAccountDetails').DataTable().destroy();
            }
            var tr = $("#tblAccountDetails").find('tr').hasClass("tbl-no-row");
            if (!tr) {
                datatableAccountDetails();
            }

            //InitDataTable();
        },
        error: function () {
            globalFunctions.showErrorMessage("Error occured");
            return;
        }
    });


}

function GetAccountsSummary(students, type, from, to) {

    $.ajax({
        url: '/ParentCorner/Fee/GetAccountSummary?students=' + students + "&fromDate=" + from + "&toDate=" + to + "&type=" + type,
        //data: JSON.stringify(model),
        dataType: 'html',
        type: "GET",
        success: function (response) {
            $("#accountStatContent .summarySection").html("");
            $("#accountStatContent .summarySection").html(response);
            if ($.fn.DataTable.isDataTable('#tblAccountSummary')) {
                $('#tblAccountSummary').DataTable().destroy();
            }
            var tr = $("#tblAccountSummary").find('tr').hasClass("tbl-no-row");
            if (!tr) {
                //$("#tblAccountSummary").DataTable({
                //    "iDisplayLength": 5,
                //    "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
                //    "order": [[0, "desc"]]
                //});
                datatableAccountSummary();
            }
            //$(".dataTables_length").hide();
            $(".dataTables_filter").hide();
            //InitDataTable();
        },
        error: function () {
            globalFunctions.showErrorMessage("Error occured");
            return;
        }
    });
}

function InitDataTable() {
    $("#accountStatContent").find('table').each(function () {
        var tr = $(this).find('tr').hasClass("tbl-no-row");
        if (!tr) {
            if ($(this).attr("id") === "tblAccountDetails") {
                datatableAccountDetails();
            } else {
                datatableAccountSummary();
            }
        }
    });
}

function datatableAccountDetails() {
    var table2 = $("#tblAccountDetails").DataTable({
        "iDisplayLength": 5,
        "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
        "autoWidth": false,
        "columnDefs": [
            { "width": "10%", "targets": 0 },
            { "width": "10%", "targets": 1 },
            { "width": "20%", "targets": 2 },
            { "width": "20%", "targets": 3 },
            { "width": "14%", "targets": 4 },
            { "width": "13%", "targets": 5 },
            { "width": "13%", "targets": 6 },
        ],
        "order": [[1, "desc"]],
        "footerCallback": function (row, data, start, end, display) {

            var api = this.api(), data;
            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            // Total over this page
            var debitTotal = api
                .column(5, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            var creditTotal = api
                .column(6, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            // Update footer
            $(api.column(5).footer()).find('div').html("");
            $(api.column(5).footer()).find('div').html(Number(parseFloat(debitTotal).toFixed(2)).toLocaleString('en', { minimumFractionDigits: 2 }));
            $(api.column(6).footer()).find('div').html("");
            $(api.column(6).footer()).find('div').html(Number(parseFloat(creditTotal).toFixed(2)).toLocaleString('en', { minimumFractionDigits: 2 }));
        },
        "oLanguage": _dataTableLangSettings
    });
    //$(".dataTables_length").hide();
    $(".dataTables_filter").hide();
}

function datatableAccountSummary() {
    var table1 = $("#tblAccountSummary").DataTable({
        "iDisplayLength": 5,
        "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
        "columnDefs": [
            { "width": "30%", "targets": 0 },
            { "width": "40%", "targets": 1 },
            { "width": "15%", "targets": 2 },
            { "width": "15%", "targets": 3 },
        ],
        "order": [[0, "desc"]],
        "footerCallback": function (row, data, start, end, display) {
            debugger
            var api = this.api(), data;
            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            // Total over this page
            var debitTotal = api
                .column(2, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            var creditTotal = api
                .column(3, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            // Update footer
            $(api.column(2).footer()).find('div').html("");
            $(api.column(2).footer()).find('div').html(Number(parseFloat(debitTotal).toFixed(2)).toLocaleString('en', { minimumFractionDigits: 2 }));
            $(api.column(3).footer()).find('div').html("");
            $(api.column(3).footer()).find('div').html(Number(parseFloat(creditTotal).toFixed(2)).toLocaleString('en', { minimumFractionDigits: 2 }));

        },
        "oLanguage": _dataTableLangSettings
    });
    //$(".dataTables_length").hide();
    $(".dataTables_filter").hide();
}

function ChkStudentForAccStatement(th) {

    var student = $(th).val(); var students = ""; var type = "", from = "", to = "", typeSum = "", fromSum = "", toSum = "";
    var isChecked = $(th).is(":checked");
    if (!isChecked) {
        $(th).parents(".child-card").removeClass("active");
        $(th).prop("checked", false);
    } else {
        $.each($("input[name='accountStatement']"), function () {
            $(this).parents(".child-card").removeClass("active");;
        });
        $(th).parents(".child-card").addClass("active");
    }
    //var preSelStud = $("input[name='accountStatement']:checked:visible:first").val();
    var isPaySibling = "";
    var getAllChk = [];
    $.each($("input[name='accountStatement']:checked"), function () {
        getAllChk.push($(this).val());
    });
    var preSelStud = getAllChk.find(function (element) {
        return element != student;
    });
    //var resIsSibling = preSelStud != undefined && preSelStud != null ? checkIsPaySibling(preSelStud, student) : true;
    //if (!resIsSibling) {
    //    $(th).parents(".child-card").removeClass("active");
    //    $(th).prop("checked", false);
    //    globalFunctions.showErrorMessage("Selected student is not from same school.");
    //    return;
    //}

    if (getAllChk.length > 0) {
        students = getAllChk.toString();
        type = $("#ddlFeeTypeAD").val();
        from = $("#accountDetFromDate").val();
        to = $("#accountDetToDate").val();
        typeSum = $("#ddlFeeTypeAS").val();
        var tempYear = $("#AccountSummaryYear").val();
        var splitDate = tempYear.split("|");
        fromSum = splitDate[0];
        toSum = splitDate[1];

        GetAccountsDetails(students, type, from, to);
        GetAccountsSummary(students, typeSum, fromSum, toSum);

    } else {
        globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
    }

}

// Payemnt History section START

function OnPaymentHistoryFilter() {

    var type = "", from = "", to = "", students = "", status = ""; var getAllChk = [];
    $.each($("input[name='paymentHistory']:checked"), function () {
        getAllChk.push($(this).val());
    });
    if (getAllChk.length > 0) {
        students = getAllChk.toString();
    }
    if (students) {
        status = $("#ddlStatusPH").val();
        from = $("#paymHistFromDate").val();
        to = $("#paymHistToDate").val();
        GetPaymentHistory(students, type, from, to, status);
    }
}

function GetPaymentHistory(students, type, from, to, status) {

    $.ajax({
        url: '/ParentCorner/Fee/GetPaymentHistory?students=' + students + "&fromDate=" + from + "&toDate=" + to + "&type=" + type + "&status=" + status,
        dataType: 'html',
        type: "GET",
        success: function (response) {
            $("#paymentHistContent .paymentHistorySection").html("");
            $("#paymentHistContent .paymentHistorySection").html(response);
            if ($.fn.DataTable.isDataTable('#tblPaymentHistory')) {
                $('#tblPaymentHistory').DataTable().destroy();
            }
            var tr = $("#tblPaymentHistory").find('tr').hasClass("tbl-no-row");
            if (!tr) {
                $("#tblPaymentHistory").DataTable({
                    "iDisplayLength": 5,
                    "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
                    "order": [[3, "desc"]],
                    "oLanguage": _dataTableLangSettings
                });
            }
            //$(".dataTables_length").hide();
            $(".dataTables_filter").hide();
        },
        error: function () {
            globalFunctions.showErrorMessage("Error occured");
            return;
        }
    });
}

function ChkStudentPaymentHistory(th) {

    var type = "", from = "", to = "", status = "";
    var student = $(th).val(); var students = "";
    var isChecked = $(th).is(":checked");
    if (!isChecked) {
        $(th).parents(".child-card").removeClass("active");
        $(th).prop("checked", false);
    } else {
        $.each($("input[name='paymentHistory']"), function () {
            $(this).parents(".child-card").removeClass("active");;
        });
        $(th).parents(".child-card").addClass("active");
    }
    //var preSelStud = $("input[name='paymentHistory']:checked:visible:first").val();
    var isPaySibling = "";
    var getAllChk = [];
    $.each($("input[name='paymentHistory']:checked"), function () {
        getAllChk.push($(this).val());
    });
    var preSelStud = getAllChk.find(function (element) {
        return element != student;
    });
    //var resIsSibling = preSelStud != undefined && preSelStud != null ? checkIsPaySibling(preSelStud, student) : true;
    //if (!resIsSibling) {
    //    $(th).parents(".child-card").removeClass("active");
    //    $(th).prop("checked", false);
    //    globalFunctions.showErrorMessage("Selected student is not from same school.");
    //    return;
    //}

    if (getAllChk.length > 0) {
        students = getAllChk.toString();
        status = $("#ddlStatusPH").val();
        from = $("#paymHistFromDate").val();
        to = $("#paymHistToDate").val();
        GetPaymentHistory(students, type, from, to, status);
    } else {
        globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
    }

}

function InitDataTablePaymentHist() {
    var tr = $("#tblPaymentHistory").find('tr').hasClass("tbl-no-row");
    if (!tr) {
        $("#tblPaymentHistory").DataTable({
            "iDisplayLength": 5,
            "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
            "order": [[3, "desc"]],
            "oLanguage": _dataTableLangSettings
        });
    }
    //$(".dataTables_length").hide();
    $(".dataTables_filter").hide();
}

function GetFeeDetailInPdf() {
    var from = "", to = "", students = ""; var getAllChk = [];
    $.each($("input[name='accountStatement']:checked"), function () {
        getAllChk.push($(this).val());
    });
    if (getAllChk.length > 0) {
        students = getAllChk.toString();
    }
    if (students) {
        if ($(".accountsDetails-Filter").is(":visible")) {
            window.location.href = "/parentcorner/fee/GetAccountDetailInPdf?students=" + students + "&fromDate=" + $("#accountDetFromDate").val() + "&toDate=" + $("#accountDetToDate").val() + "&type=" + $("#ddlFeeTypeAD").val()
        }
        else if ($(".accountsSummary-Filter").is(":visible")) {
            var tempYear = $("#AccountSummaryYear").val();
            var splitDate = tempYear.split("|");
            from = splitDate[0];
            to = splitDate[1];
            window.location.href = "/parentcorner/fee/GetAccountSummeryInPdf?students=" + students + "&fromDate=" + from + "&toDate=" + to + "&type=" + $("#ddlFeeTypeAS").val()
        }
    }
}

function SendMailOfStatementsOfAccounts() {

    var type = "", from = "", to = "", students = ""; var getAllChk = [];
    $.each($("input[name='accountStatement']:checked"), function () {
        getAllChk.push($(this).val());
    });
    if (getAllChk.length > 0) {
        students = getAllChk.toString();
    }
    if (students) {

        if ($(".accountsDetails-Filter").is(":visible")) {
            from = $("#accountDetFromDate").val();
            to = $("#accountDetToDate").val();
            type = "DETAILS";
        }
        else if ($(".accountsSummary-Filter").is(":visible")) {
            var tempYear = $("#AccountSummaryYear").val();
            var splitDate = tempYear.split("|");
            from = splitDate[0];
            to = splitDate[1];
            type = "SUMMARY";
        }

        $.ajax({
            url: '/ParentCorner/Fee/SendEmailOfAccountStatements?students=' + students + "&fromDate=" + from + "&toDate=" + to + "&type=" + type,
            dataType: 'json',
            //timeout: 500,     
            type: "GET",
            success: function (data, status, xhr) {

                var respon = JSON.parse(data.events);
                if (respon.success == "true") {
                    globalFunctions.showSuccessMessage(respon.message);
                }
                else {
                    globalFunctions.showErrorMessage(respon.message);
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {
            }
        });

    }
}

// print account statement 
function PrintAccountDetails() {
    debugger
    var from = "", to = "", students = ""; var getAllChk = [];
    $.each($("input[name='accountStatement']:checked"), function () {
        getAllChk.push($(this).val());
    });
    if (getAllChk.length > 0) {
        students = getAllChk.toString();
    }
    if (students) {
        if ($(".accountsDetails-Filter").is(":visible")) {
            $('#printAccStatement').prop('href', "../parentcorner/Fee/PrintAccountDetails?students=" + students + "&fromDate=" + $("#accountDetFromDate").val() + "&toDate=" + $("#accountDetToDate").val() + "&type=" + $("#ddlFeeTypeAD").val());
        }
        else if ($(".accountsSummary-Filter").is(":visible")) {
            var tempYear = $("#AccountSummaryYear").val();
            var splitDate = tempYear.split("|");
            from = splitDate[0];
            to = splitDate[1];
            $('#printAccStatement').prop('href', "../parentcorner/Fee/PrintAccountSummary?students=" + students + "&fromDate=" + from + "&toDate=" + to + "&type=" + $("#ddlFeeTypeAS").val());
        }
    }
}