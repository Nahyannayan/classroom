﻿
$(document).ready(function () {

    //NetPaymentSection("school");
});


function modalScheduleFee(studNo, studName, grade) {
    $.ajax({
        url: '/ParentCorner/PointsRedemption/GetScheduleFee?studNo=' + studNo,
        dataType: 'json',
        //timeout: 500,     
        async: true,
        success: function (data, status, xhr) {
            var lstSchdFee = JSON.parse(data.scheduleFee);
            var modalMorInfo = $("#fee-schedule-table tbody");
            modalMorInfo.html("");
            var strDiv = ``;
            $.each(lstSchdFee, function (i, obj) {
                var Duedate = new Date(obj.DueDate).ddMMMyyyFormat();
                strDiv += `<tr class="">
                                        <td>`+ obj.FeeDescription + `</td>
                                        <td>`+ Duedate + `</td>
                                        <td class="text-right">`+ obj.Amount.toFixed(2) + `</td>
                                    </tr>`;
            });
            if (lstSchdFee.length <= 0) {
                strDiv = `<tr class=""><td colspan="3"><span class="no-data-found text-bold d-block mx-auto my-auto">There is no data to display</span></td></tr>`;
            }
            modalMorInfo.append(strDiv);
            if (strDiv) {
                $("#scheduleOfFees .name-title").text(studName);
                $("#scheduleOfFees .student-grade").text(grade);
                $('#printurlid').prop('href', '../parentcorner/pointsredemption/PrintFeesShedule/' + studNo);
                $("#scheduleOfFees").modal("show");
            }
        },
        error: function (jqXhr, textStatus, errorMessage) {
        }
    });
}

function modalRedemptionHistory() {
    
    //var preSelStud = $("input[name='pointsRedemption']:checked:visible:first").val();
    var studNo = $("input[name='pointsRedemption']:checked").val();
    var getAllChk = [];
    $.each($("input[name='pointsRedemption']:checked"), function () {
        getAllChk.push($(this).val());
    });
    var preSelStud = getAllChk.find(function (element) {
        return element != studNo;
    });
    //var resIsSibling = preSelStud != undefined && preSelStud != null ? checkIsPaySibling(preSelStud, studNo) : true;
    //if (!resIsSibling) {
    //    $(th).parents(".child-card").removeClass("active");
    //    $(th).prop("checked", false);
    //    globalFunctions.showErrorMessage("Selected student is not from same school.");
    //    return;
    //}
   
    if (getAllChk.length > 0) {
        students = getAllChk.toString();
        if (students) {
            GetRedemptionHistory(students);
        } else {
            $("#feeDetlsPR").html("");
            globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
        }
    } else {
        $("#feeDetlsPR").html("");
        globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
    }
   
}

function GetRedemptionHistory(students)
{
  
    $.ajax({
        url: '/ParentCorner/PointsRedemption/GetRedemptionHistory?students=' + students,
        dataType: 'json',
        //timeout: 500,     
        async: true,
        success: function (data, status, xhr) {
            var lstSchdFee = JSON.parse(data.redemptionHistory);
            var modalMorInfo = $("#redemption-history-table tbody");
            modalMorInfo.html("");
            var strDiv = ``;
            $.each(lstSchdFee, function (i, obj) {
                var Ddate = new Date(obj.Date).ddMMMyyyFormat();
                strDiv += `<tr class="">                                       
                                        <td>`+ Ddate + `</td>
                                        <td>`+ obj.StudentNo + `</td>
                                        <td>`+ obj.StudentName + `</td>
                                        <td>`+ obj.RefNo + `</td>
                                        <td>`+ obj.RecNo + `</td>
                                        <td class="text-right">`+ obj.Amount.toFixed(2) + `</td>
                                    </tr>`;
            });
            if (lstSchdFee.length <= 0) {
                strDiv = `<tr class=""><td colspan="6"><span class="no-data-found text-bold d-block mx-auto my-auto">There is no data to display</span></td></tr>`;
            }
            modalMorInfo.append(strDiv);
            if (strDiv) {
                $("#redemptionInfo").modal("show");
            }
        },
        error: function (jqXhr, textStatus, errorMessage) {
        }
    });
}

function RefreshStudentFeeDetails()
{
   
    var students = "", provTypId = "";
    var isPaySibling = "";
    var getAllChk = [];
    $.each($("input[name='pointsRedemption']:checked"), function () {
        getAllChk.push($(this).val());
    });
  
    isPaySibling = getAllChk.length > 1 ? "1" : "0";

    var redAmount = parseFloat($("#tblRedemptionDetls tbody tr").find(".redemAmnt").text());

    if (redAmount != NaN && redAmount > 0) {
        if (getAllChk.length > 0) {
            students = getAllChk.toString();
            if (students) {
                GetFeeDetails(students, isPaySibling, "school");
            } else {
                $("#feeDetlsPR").html("");
                globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
            }
        } else {
            $("#feeDetlsPR").html("");
            globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
        }
    }
}

function ChkStudentPointsRedemption(th) {
    
    var students = "", provTypId = "";
    var studNo = $(th).val();
    var isChecked = $(th).is(":checked");
    if (!isChecked) {
        $(th).parents(".child-card").removeClass("active");
        $(th).prop("checked", false);
    } else {
        $(th).parents(".child-card").addClass("active");
    }

    //var preSelStud = $("input[name='pointsRedemption']:checked:visible:first").val();
    var isPaySibling = "";
    var getAllChk = [];
    $.each($("input[name='pointsRedemption']:checked"), function () {
        getAllChk.push($(this).val());
    });
    var preSelStud = getAllChk.find(function (element) {
        return element != studNo;
    });
    var resIsSibling = preSelStud != undefined && preSelStud != null ? checkIsPaySibling(preSelStud, studNo) : true;
    if (!resIsSibling) {
        $("input[name='pointsRedemption']:checked").prop('checked', false);
        $.each($("input[name='pointsRedemption']"), function () {
            $(this).parents(".child-card").removeClass("active");;
        });
        $(th).parents(".child-card").addClass("active");
        $(th).prop("checked", true);
        $("#feeDetlsPR").html("");
        //globalFunctions.showWarningMessage("Selected student is not from same school.");
        //return;
    }  

    isPaySibling = resIsSibling && getAllChk.length > 1 ? "1" : "0";

    var redAmount = parseFloat($("#tblRedemptionDetls tbody tr").find(".redemAmnt").text());
    if (redAmount != NaN && redAmount > 0) {
    if (getAllChk.length > 0) {
        students = getAllChk.toString();
        if (students) {
            GetFeeDetails(students, isPaySibling, name);            
        } else {
            $("#feeDetlsPR").html("");
            globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
        }
    } else {
        $("#feeDetlsPR").html("");
        globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
        }
    }

}

function GetFeeDetails(stuNo, IsSibling, source) {
    
    $.ajax({
        url: '/ParentCorner/PointsRedemption/GetFeeDetails?students=' + stuNo + '&paymentTo=' + source + '&isPaySibling=' + IsSibling.toString(),
        dataType: 'html', 
        async: true,
        success: function (response) {
            $("#feeDetlsPR").html("");
            $("#feeDetlsPR").html(response);
        },
        error: function (jqXhr, textStatus, errorMessage) {
            globalFunctions.showErrorMessage(errorMessage);
            return;
        }
    });
}

function NetPaymentSection(source) {
    
    var Tot = 0.0000, NetPay = 0.0000;
    var tblFeeDet = $('.NetAmount-' + source);
    var feeAll = $('.' + source + '-wrapper .tblFeeDetail'); 
    feeAll.find("tr:last").filter(function () {
        var tempTot = 0.0000;
        tempTot = parseFloat($(this).find('td:eq(2)').text().trim());
        tempTot = tempTot === NaN ? 0.00 : tempTot
        Tot += tempTot;
        NetPay += tempTot;
    });
    tblFeeDet.find("#total").val(Tot.toFixed(2));
    tblFeeDet.find("#net-payable").val(NetPay.toFixed(2));
    if (NetPay > 0) {
        $(".btnProceedToPay").prop("disabled", false)
    } else {
        $(".btnProceedToPay").prop("disabled", true)
    }
}

function ProceedToPay(th) {
    
    var studFeeDetails = []; var sucess = true;
    var DiscTot = 0.0000, Tot = 0.0000, ProcCharge = 0.0000, NetPay = 0.0000;
    var source = $(th).data("source");
    var reedamAmnt = parseFloat($("#tblRedemptionDetls .redemAmnt").text());
    var main = $('.NetAmount-' + source);
    var NetPayable = parseFloat(main.find("#net-payable").val());
    if (!$.isNumeric(NetPayable) || NetPayable <= 0) {
        globalFunctions.showErrorMessage("There is no calculation found for Net Payable Amount.");
        return false;
    } else if (NetPayable > reedamAmnt) {
        globalFunctions.showErrorMessage("Please check redeem amount with available redeemable amount.");
        return false;
    }
    var feeAll = $('.' + source + '-wrapper .tblFeeDetail tbody'); //  tr: last
    var isPaySibling = "";
    isPaySibling = feeAll && feeAll.length > 1 ? "1" : "0";

    feeAll.each(function () {
        var feeDetails = new Object();
        $(this).find("tr:last").filter(function () {
            
            //var obj = new Object();
            var stud = $(this).data("stud");
            var tempTot = 0.0000;
            tempTot = parseFloat($(this).find('td:eq(2)').text().trim());
            if (tempTot <= 0) {
                globalFunctions.showErrorMessage("There should be some amount for pay now of each selected student.");
                sucess = false;
                return false;
            }

            NetPay += tempTot;

            feeDetails.STU_NO = stud;
            feeDetails.PayingAmount = tempTot;
            feeDetails.Source = source.toUpperCase();
            feeDetails.IsPaySibling = isPaySibling;
        });

        var studWiseFee = [];
        $(this).find("tr").not(':last').filter(function () {
            var tempAr = {
                "FeeID": $(this).attr("data-feeId"),
                "PayAmount": parseFloat($(this).find('td:eq(2)').find('input').val()),                 
                "AdvanceDetails": [],
            }
            studWiseFee.push(tempAr);
        });
        feeDetails.FeeDetail = studWiseFee;

        studFeeDetails.push(feeDetails);

    });

    if (studFeeDetails.length > 0 && sucess == true) {
        $.ajax({
            url: '/ParentCorner/PointsRedemption/SubmitRewardsRedemption',
            type: "POST",
            data: { 'objVM': studFeeDetails },
            dataType: "json",
            success: function (result) {
                
                var data = JSON.parse(result.rewardsResult), msg = "", succ = "";
                if (data.Response != null) {
                    succ = data.Response.success.toLowerCase(); msg = data.Response.message;
                    if (succ == "true") {
                        var preSelStud = $("input[name='pointsRedemption']:checked:visible:first").val();
                        //OnCancelClick("school");
                        GetRedemptionPoints(preSelStud,isPaySibling);
                        RefreshStudentFeeDetails();
                        var str = `<div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <span>
                                        GEMS Rewards points have been successfully redeemed towards the Tuition Fees.
                                    </span>
                                </div>`;

                        $('.successAlert').html("");
                        $('.successAlert').html(str);
                       
                        
                    } else {
                        var str = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <span>`
                                     + msg +`
                                    </span>
                                </div>`;
                        $('.failureAlert').html("");
                        $('.failureAlert').html(str);
                    }
                    document.body.scrollTop = 0;
                    document.documentElement.scrollTop = 0;
                }
                else {
                    globalFunctions.showErrorMessage(translatedResources.technicalError);
                }
            },
            error: function (msg) {
                globalFunctions.onFailure();
            }
        });
    }
}

function checkIsPaySibling(prevStud, CurrStud) {
    var result = true;

    $.ajax({
        url: '/ParentCorner/Fee/CheckIsPaySibling?PreSelectedStudent=' + prevStud + '&SelectedStudent=' + CurrStud,
        dataType: 'json',
        //timeout: 500,     
        async: false,
        success: function (data, status, xhr) {

            var discount = JSON.parse(data.IsSibling);
            if (discount) {
                result = true;
            } else {
                result = false;
            }
            //var sourc = $('.tbl-' + source);
            //var amount = $('.NetAmount-' + source);

        },
        error: function (jqXhr, textStatus, errorMessage) {
            result = false;
        }
    });
    return result;
}


function GetValidationPayingAmount(th, stuNo, payTypId, feeId, source) {
    
    var result = true;
    var reedamAmnt = parseFloat($("#tblRedemptionDetls .redemAmnt").text());
    var amount = parseFloat($(th).val());
    if (amount === NaN || !amount) {
        TotalAmountForStudent(source, stuNo);
        NetPaymentSection("school");
        globalFunctions.showErrorMessage("Amount should be greater than zero.");
        return false;
        
    } else if (reedamAmnt === NaN || reedamAmnt < amount) {
        amount = 0;
        result = false;
    }    
    var NetPay = 0.0000;
    var feeAll = $('.' + source + '-wrapper .tblFeeDetail');
    if (result)
    {
        TotalAmountForStudent(source, stuNo);

        feeAll.find("tr:last").filter(function () {
            var tempTot = 0.0000;
            tempTot = parseFloat($(this).find('td:eq(2)').text().trim());
            tempTot = tempTot === NaN ? 0.00 : tempTot;
            NetPay += tempTot;
            if (NetPay === NaN || reedamAmnt < NetPay) {
                result = false;
            }
        });
    }
   
    if (!result) {
        $(th).val('0.00');
        globalFunctions.showErrorMessage("Please check redeem amount with available redeemable amount.");
        TotalAmountForStudent(source, stuNo);
        NetPaymentSection("school");
        return false;
    }
    
    NetPaymentSection("school");

}

function TotalAmountForStudent(source, student)
{
   
    var paynow = 0.0000, AmntDue = 0.0000;
    var tbl = $('.tbl-' + source + '-' + student + ' tbody tr').not(':last');
    //AmntDue += parseFloat(tbl.find("td:eq(1)").text().trim());
    //paynow += parseFloat(tbl.find("td:eq(2)").find('input').val());
    tbl.find("td:eq(2)").find('input').each(function () {
        var combat = $(this).val();
        if (!isNaN(combat) && combat.length !== 0) {
            paynow += parseFloat(combat);
        }
    });
    var tblLastRow = $('.tbl-' + source + '-' + student + ' tbody tr:last');
    //tblLastRow.find("#tdDueAmt").text(AmntDue);
    tblLastRow.find(".tbl-actions #tdPayAmt").text(paynow.toFixed(2));

}

function OnCancelClick(source)
{
    
    var defAmnt = 0.00;
    var tbl = $('.tblFeeDetail tbody tr');
    tbl.filter(function () {
        $(this).find("td:eq(2)").find('input').val(defAmnt);
        $(this).find("#tdPayAmt").text(defAmnt);
    });
     
    var netPay = $(".NetAmount-" + source);
    netPay.find("#total").val(defAmnt);
    netPay.find("#net-payable").val(defAmnt);
    $(".btnProceedToPay").prop("disabled", true)
}

function isNumberKey(txt, evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46) {
        //Check if the text already contains the . character
        if (txt.value.indexOf('.') === -1) {
            return true;
        } else {
            return false;
        }
    } else {
        if (charCode > 31 &&
            (charCode < 48 || charCode > 57))
            return false;
    }
    return true;
}

function GetRedemptionPoints(student,isPaySibling )
{
   
    $.ajax({
        url: '/ParentCorner/PointsRedemption/GetRedemptionPoints?student=' + student + "&isPaySibling=" + isPaySibling,
        dataType: 'json',
        //timeout: 500,     
        async: false,
        success: function (data, status, xhr) {
            
            var redemption = JSON.parse(data.rewardsResult);    
            var tbl = $("#tblRedemptionDetls tbody tr");
            tbl.find(".availPoints").text(redemption.available_points);
            tbl.find(".redemPoints").text(redemption.redeemable_points);
            tbl.find(".redemAmnt").text(redemption.redeemable_amount.toFixed(2));
        },
        error: function (jqXhr, textStatus, errorMessage) {
        }
    });
}

function printDiv() {
    
    $("#scheduleOfFees .close").click();
    $('#scheduleOfFees').fadeOut();
    $('#scheduleOfFees').modal('hide');
    var divName = "printSchFee";
    var nameSt = $("#nameSt").html();
    var htm = document.getElementById(divName);
    var printContents = htm.innerHTML;
    nameSt += printContents;
    var originalContents = document.body.innerHTML;
    
    document.body.innerHTML = nameSt;
    window.print();
    document.body.innerHTML = originalContents;
    //window.onfocus = function () { closeModal("scheduleOfFees"); }
}

function closeModal(id) {
    //alert('success');
    //$("#" + id).modal("hide");
    $('#' + id).modal('hide');
}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

//function thousands_separators(num)
//{
//    var num_parts = num.toString().split(".");
//    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
//    return num_parts.join(".");
//}