﻿
var resourcesHTML = function () {
    getResourcesHTML = function (resourcesData) {
        var resourcesLayoutStart = `
<div class="row search-row justify-content-end">
    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="search-with-bg">
            <span class="icon-search"></span>
            <input type="search" id="resource-search-input" class="searchfield" placeholder="`+ translatedResources.Search +`">
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-lg-12">
        <div class="card resources-card">
            <div class="card-body p-4">
                    <div class="resource-header border-bottom pb-3 d-block resource-tab-list">
<nav class="respTabs">
                        <ul class="-primary nav">`;
        var resourcesMainHeaderList = `<li>
                                    <a class="nav-link" onclick="toggleResourceTabs('#cat-#{CategoryID}')" data-toggle="tab" href="#cat-#{CategoryID}" role="tab" #{AreaSelected}>
                                        
                                        #{CategoryDescription}
                                    </a>
                                </li>`;
        var resourcesCatListEnd = ` </ul></nav>`;
        var moreCategoryStart = `<div class="btn-group more-dropdown">
                            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                               aria-expanded="false"><span class="text-muted">More</span><span class="blue-circle"></span></a>`;
                            //<div class="dropdown-menu">`;
        var moreCategoryList = `<a class="more-options" onclick="toggleResourceTabs('#cat-#{CategoryID}')"> #{CategoryDescription}</a>`;
        //var moreCategoryend = `</div></div>`;
        var resourcesMainHeaderListEnd = `</div></div><div class="tab-content pt-4">`;
        var resourcesCatTabPanelStart = `<div class="tab-pane fade" id="cat-#{CategoryID}" role="tabpanel">
                                <div class="row">`;
        var resourcesTabPanelbody = `<div class="col-xl-4 col-lg-6 col-md-6 col-12 div-resource-desc" data-category="#{CategoryDescription}" data-title="#{ResourceTitle}">

                                                <div class="card-body resource-type-card p-3">
                                                    <span class="file-icon rounded-circle border border-primary d-flex justify-content-center align-items-center mr-3">
                                                            #{ResourceThumbNailUrl}
                                                    </span>
                                                    <div class="pdf-container">
                                                        <p class="title text-bold d-flex justify-content-between">
                                                            <span class="text-truncate" title="#{ResourceTitle}" data-toggle="tooltip" data-placement="top">#{ResourceTitle}</span>
                                                            <a class="ml-2" href="javascript:void(0)" onclick="window.open('#{ResourceAttachmentUrl}')" data-toggle="tooltip" data-placement="top" data-original-title="`+ translatedResources.ViewAttachment +`">
                                                                <i class="fas fa-paperclip"></i>
                                                            </a>
                                                        </p>

                                                        <p class="text-muted date text-medium">#{ResourceDateTime}</p>
                                                        <div class="text-muted content text-medium #{PopOverClass}">
                                                                #{ResourceDescription}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>`;
        var popOverHTML = `#{ResourceDescriptionTrimmed}&nbsp;<a href="javascript:void(0)" data-toggle="dropdown">` + translatedResources.ReadMore +`</a>
                                                                <div class="resource-info dropdown-menu mt-n5 z-depth-1 border-light rounded-2x p-0">
                                                                    <div class="column-padding py-3">
                                                                        <div class="content">
                                                                            <p class="sub-heading text-semibold text-primary">`+ translatedResources.Description +`</p>
                                                                            <p>#{ResourceDescription}</p>

                                                                        </div>
                                                                        <div class="d-flex justify-content-end mt-4">
                                                                            <button type="button" class="btn btn-outline-primary btn-sm m-0">`+ translatedResources.Close +`</button>
                                                                        </div>
                                                                    </div>
                                                                </div>`;
        var resourcesCatTabPanelEnd = `</div>
                            </div>`;
        var resourceLayoutEnd = `
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12"></div>
        </div>`;
        let tabMaxCount = 5;
        let maxTextLength = 60;
        var data = '';
        if (resourcesData !== null) {
            let totalCatCount = resourcesData[0].categories.length;
            //let isCatMoreThanLimit = totalCatCount >= tabMaxCount;
            let isCatMoreThanLimit = false;
            //let maxCatCount = isCatMoreThanLimit ? tabMaxCount : totalCatCount;
            let maxCatCount =  totalCatCount;
            var resourceCatList = '';
            var resourceCatMoreOptionList = '';
            var resourceCatTabPanelList = '';
            //append category based on max limit
            for (catid = 0; catid < maxCatCount; catid++) {
                let catVar = {
                    CategoryID: resourcesData[0].categories[catid].categoryID,
                    CategoryDescription: resourcesData[0].categories[catid].categoryDescription,
                    CategoryThumbNailUrl: resourcesData[0].categories[catid].categoryThumbNailUrl,
                    AreaSelected: catid === 0 ? 'aria-selected="true"' : ''
                };
                let mappedHtml = globalFunctions.templateCreator(resourcesMainHeaderList, catVar);
                resourceCatList = resourceCatList + mappedHtml;
            }
            //close ul 
            resourceCatList = resourceCatList + resourcesCatListEnd;
            //append rest of the category to more options
            if (isCatMoreThanLimit) {
                for (catid = maxCatCount; catid < totalCatCount; catid++) {
                    let catVar = {
                        CategoryID: resourcesData[0].categories[catid].categoryID,
                        CategoryDescription: resourcesData[0].categories[catid].categoryDescription,
                    };
                    let mappedHtml = globalFunctions.templateCreator(moreCategoryList, catVar);
                    resourceCatMoreOptionList = resourceCatMoreOptionList + mappedHtml;
                }
                if (resourceCatMoreOptionList !== '')
                    resourceCatList = resourceCatList + (moreCategoryStart + resourceCatMoreOptionList + moreCategoryend);
            }
            if (resourceCatList !== '')
                data = resourcesLayoutStart + resourceCatList + resourcesMainHeaderListEnd;
            //append tab panel data
            for (catid = 0; catid < totalCatCount; catid++) {
                let resourcesTabPanelbodyList = '';
                let catVar = {
                    CategoryID: resourcesData[0].categories[catid].categoryID,
                    CategoryDescription: resourcesData[0].categories[catid].categoryDescription,
                    CategoryThumbNailUrl: resourcesData[0].categories[catid].categoryThumbNailUrl
                };
                let mappedTabHtml = globalFunctions.templateCreator(resourcesCatTabPanelStart, catVar);
                //append related resoures.
                let resourceCount = resourcesData[0].categories[catid].resources.length;
                for (resId = 0; resId < resourceCount; resId++) {
                    //check resource desc length and add pop over if length exceed max limit
                    let isDescLengthExceed = resourcesData[0].categories[catid].resources[resId].resourceDescription.length > maxTextLength;
                    let descVar = {
                        ResourceDescriptionTrimmed: isDescLengthExceed ? resourcesData[0].categories[catid].resources[resId].resourceDescription.substring(0, maxTextLength) : resourcesData[0].categories[catid].resources[resId].resourceDescription,
                        ResourceDescription: resourcesData[0].categories[catid].resources[resId].resourceDescription
                    };
                    let resourceDescription = isDescLengthExceed ? globalFunctions.templateCreator(popOverHTML, descVar) : resourcesData[0].categories[catid].resources[resId].resourceDescription;
                    //create resource col
                    let ResVar = {
                        CategoryID: resourcesData[0].categories[catid].categoryID,
                        CategoryDescription: resourcesData[0].categories[catid].categoryDescription,
                        ResourceThumbNailUrl: resourcesData[0].categories[catid].resources[resId].resourceThumbNailUrl !== '' ? '<img class="" width="30px" height="33px" src="' + resourcesData[0].categories[catid].resources[resId].resourceThumbNailUrl + '" />' : '<img class="svg" src="/Content/img/svg/' + resourcesData[0].categories[catid].resources[resId].fileIcon + '.svg" />',
                        ResourceTitle: resourcesData[0].categories[catid].resources[resId].resourceTitle,
                        ResourceAttachmentUrl: resourcesData[0].categories[catid].resources[resId].resourceAttachmentUrl,
                        ResourceDateTime: moment(resourcesData[0].categories[catid].resources[resId].resourceDateTime).format('DD MMM YYYY'),
                        PopOverClass: isDescLengthExceed ? 'dropright popoverInformation' : '',
                        ResourceDescription: resourceDescription
                    };
                    let mappedTabBodyHtml = globalFunctions.templateCreator(resourcesTabPanelbody, ResVar);
                    resourcesTabPanelbodyList = resourcesTabPanelbodyList + mappedTabBodyHtml;
                }
                resourceCatTabPanelList = resourceCatTabPanelList + (mappedTabHtml + resourcesTabPanelbodyList + resourcesCatTabPanelEnd);
            }
            if (resourceCatTabPanelList !== '')
                data = data + resourceCatTabPanelList + resourceLayoutEnd;
        }
        $("#communityContent").html("");
        if (data !== '') {
            $("#communityContent").html(data);
            $('#communityContent a[aria-selected="true"]').click();
            $('#communityContent a[aria-selected="true"]').addClass("active");
            $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', toogleNoRecordsMessage);
            $(document).on("keyup", "#resource-search-input", function () {
                var value = $(this).val().toLowerCase();
                $(".div-resource-desc").each(function () {
                    $(this).toggle(($(this).data("category").toLowerCase().indexOf(value) > -1 || $(this).data("title").toLowerCase().indexOf(value) > -1));
                });

                toogleNoRecordsMessage();
            });

            $(".resource-info .content").mCustomScrollbar({
                setHeight: "150px",
                autoExpandScrollbar: true,
                autoHideScrollbar: true,
                scrollbarPosition: "outside"
            });
            respTabs();

        }
        else {
            $("#communityContent").append("<span class='no-data-found text-bold d-block mx-auto my-5'>" + translatedResources.noRecordsMessage + "</span>");
        }
    };
    return {
        getResourcesHTML: getResourcesHTML
    }
}();

function toggleResourceTabs(tabId) {
    $("a.nav-link, div.tab-pane").removeClass("show active");
    $(tabId).addClass("show active");
}

function toogleNoRecordsMessage() {
    $(".tab-content div.tab-pane.active span.no-data-found").remove();
    $(".tab-content div.tab-pane.active").each(function () {
        if ($(this).find(".div-resource-desc:visible").length == 0)
            $(this).append("<span class='no-data-found text-bold d-block mx-auto my-auto'>" + translatedResources.noResourcesMsg + "</span>");
    });
}

function respTabs() {

    const container = document.querySelector('.respTabs');
    const primary = container.querySelector('.-primary');
    const primaryItems = container.querySelectorAll('.-primary > li:not(.-more)');
    container.classList.add('--jsfied');

    // insert "more" button and duplicate the list

    primary.insertAdjacentHTML('beforeend', `
  <li class="-more">
    <button type="button" aria-haspopup="true" aria-expanded="false">
      <span class="txtmore"></span> <span>&darr;</span>
    </button>
    <ul class="-secondary">
      ${primary.innerHTML}
    </ul>
  </li>
`);
    const secondary = container.querySelector('.-secondary');
    const secondaryItems = secondary.querySelectorAll('li');
    const allItems = container.querySelectorAll('li');
    const moreLi = primary.querySelector('.-more');
    const moreBtn = moreLi.querySelector('button');
    moreBtn.addEventListener('click', e => {
        e.preventDefault();
        container.classList.toggle('--show-secondary');
        moreBtn.setAttribute('aria-expanded', container.classList.contains('--show-secondary'));
    });

    // adapt tabs

    const doAdapt = () => {
        // reveal all items for the calculation
        allItems.forEach(item => {
            item.classList.remove('--hidden');
        });

        // hide items that won't fit in the Primary
        let stopWidth = moreBtn.offsetWidth;
        let hiddenItems = [];
        const primaryWidth = primary.offsetWidth;
        primaryItems.forEach((item, i) => {
            if (primaryWidth >= stopWidth + item.offsetWidth) {
                stopWidth += item.offsetWidth;
            } else {
                item.classList.add('--hidden');
                hiddenItems.push(i);
            }
        });

        // toggle the visibility of More button and items in Secondary
        if (!hiddenItems.length) {
            moreLi.classList.add('--hidden');
            container.classList.remove('--show-secondary');
            moreBtn.setAttribute('aria-expanded', false);
        } else {
            secondaryItems.forEach((item, i) => {
                if (!hiddenItems.includes(i)) {
                    item.classList.add('--hidden');
                }
            });
        }
    };

    doAdapt(); // adapt immediately on load
    window.addEventListener('resize', doAdapt); // adapt on window resize

    // hide Secondary on the outside click

    document.addEventListener('click', e => {
        let el = e.target;
        while (el) {
            if (window.CP.shouldStopExecution(0)) break;
            if (el === secondary || el === moreBtn) {
                return;
            }
            el = el.parentNode;
        } window.CP.exitedLoop(0);
        container.classList.remove('--show-secondary');
        moreBtn.setAttribute('aria-expanded', false);
    });
}