﻿var strRefresh = "";
$(document).ready(function () {
    $('.toggle-wrapper .leave-request').click(function () {
        debugger
        $(this).addClass('active');
        var studid = $(this).attr('data-stud');
        $('#leave-letter-' + studid).removeClass('active');
        $('.panel-header').addClass('hide-item');
        $('#leaveReq-header-' + studid).removeClass('hide-item');
        $('.tab-content #tableLeaveRequest-' + studid).removeClass('hide-item');
        $('.tab-content #tableDefault-' + studid).addClass('hide-item');
        var tr = $('#tblLeaveRequest-' + studid + ' tr').hasClass("placeholder-row");
        if (!tr) { $('#home-' + studid + ' .leave-request-alert').removeClass('hide-item'); }

        $('.disclaimer-leave-request').removeClass('hide-item');
        $('.disclaimer-leave-letter').addClass('hide-item');
        $(".placeholder-row").removeClass('hide-item');
    });
    $('.toggle-wrapper .leave-letter').click(function () {
        $(this).addClass('active');
        var studid = $(this).attr('data-stud');
        $('#leave-request-' + studid).removeClass('active');
        $('.panel-header').addClass('hide-item');
        $('#leaveReq-header-' + studid).removeClass('hide-item');
        $('.tab-content #tableDefault-' + studid).removeClass('hide-item');
        $('.tab-content #tableLeaveRequest-' + studid).addClass('hide-item');
        $('.leave-request-alert').addClass('hide-item');
        $('.disclaimer-leave-letter').removeClass('hide-item');
        $('.disclaimer-leave-request').addClass('hide-item');
        $(".placeholder-row").addClass('hide-item');
    });
    $('.nav-lr-link').click(function () {
        var studid = $(this).attr('data-stud');
        var tr = $('#tblLeaveRequest-' + studid + ' tr').hasClass("placeholder-row");
        var secLT = $('#leave-request-' + studid).hasClass("active");
        //if (secLT) {
        //    $('#home-' + studid + ' .leave-request-alert').removeClass('hide-item');
        //}
        if (secLT) {
            $('.disclaimer-leave-letter').addClass('hide-item');
            $('.disclaimer-leave-request').removeClass('hide-item');
            $(".placeholder-row").removeClass('hide-item');
        } else {
            $('.disclaimer-leave-letter').removeClass('hide-item');
            $('.disclaimer-leave-request').addClass('hide-item');
            $(".placeholder-row").addClass('hide-item');
        }
        $('.leave-request-card').addClass('hide-item');
        $('#leave-request-page-' + studid).removeClass('hide-item');
        $('.panel-header').addClass('hide-item');
        $('#leaveReq-header-' + studid).removeClass('hide-item');
    });
    $(".tblLeaveRequest").each(function () {
        debugger
        var tr = $(this).find('tr').hasClass("tbl-no-row");
        if (!tr) {
            $(this).DataTable({              
                "iDisplayLength": 5,
                "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
                "columnDefs": [{
                    "targets": 0,
                    "visible": false,
                    "searchable": true
                }]
            });
        }
    });
    $(".dataTables_length").hide();
    $(".dataTables_filter").hide();

    //$('.tblLeaveRequest').on('page.dt', function () {
    //    debugger
    //    var main = $(this).parents('.leave-request-card');
    //    //var stud = main.attr("data-studNo");
    //    var allBtn = main.find('.btnFilter');
    //    allBtn.filter(function () {
    //        $(this).removeClass("btn-primary");
    //        $(this).removeClass('btn-outline-primary');
    //        $(this).addClass('btn-outline-primary');     
    //        if ($(this).hasClass("all")) { $(this).removeClass('btn-outline-primary'); $(this).addClass("btn-primary"); }
    //    });     
    //    //var activeBtn = main.find('.button-group .btn-primary');
    //    //activeBtn.click();
    //});
});
function redirectPage() {
    window.location.href = strRefresh
}

function filterLeaveDetails(th, act) {
    //debugger
    var noDataHtml = "<span class='no-data-found text-bold d-block mx-auto my-auto'>There is no data to display</span>";
    var secId = $(th).parents('.leave-request-card').attr('id');
    var allBtn = $('#' + secId + ' .btnFilter');
    allBtn.filter(function () {
        $(this).addClass('btn-outline-primary');
        $(this).removeClass("btn-primary ");
    });
    $(th).removeClass("btn-outline-primary");
    $(th).addClass("btn-primary");


    debugger
    var allTable = $('#' + secId + ' table');
    var source = $('#' + secId + ' table tbody tr');
    //source.hide();
    if (act == "All") {
        source.filter(function () {
            $(this).find('span.no-data-found').parents("tr").remove();
            $(this).show();
        });
    }
    //else {
    //    source.filter(function () {
    //        $(this).find('span.no-data-found').parents("tr").remove();
    //        var category = $(this).data("category");
    //        if (category != undefined && category == act) {
    //            $(this).show();
    //        }
    //    });
    //}


    //allTable.each(function () {
    //    var lg = $(this).find('tbody tr:visible').length;
    //    var length = lg / 5;
    //    var hasAll = $(th).hasClass("all");
    //    if (lg < 5 && act != "all") {
    //        $('.dataTables_paginate .pagination li').addClass("disabled");           
    //    } else {
    //        $('.dataTables_paginate .pagination li').removeClass("disabled");
    //    }
    //});

    allTable.each(function () {
        var tr = $(this).find('tr').hasClass("tbl-no-row"); var table
        console.log($(this).attr("id"));
       
        if (!tr) {
            table = $(this).DataTable();
            var tblCnt = table.columns().count();
            var indx = 0;//tblCnt == 5 ? 3 : 4; //  table.columns().names().indexOf('Approval Status');//
            if (act != "All") {
                //alert(table.val());
                //alert(act);
                //table.search(act).draw();
                //var regExSearch = '^\\s' + act + '\\s*$';
                //table.column(3).search(regExSearch, true, true, true).draw();
                var regex = '\\b' + act + '\\b';
                table.columns([indx]).search(regex, true, false,false).draw();          
                //var regex = '\\b' + act + '\\b';
                //alert(regex);
                //table.columns([1]).search(regex, true, true).draw();

            } else {

                table.columns([indx]).search("", false, false).draw();
            }
        }
        
      
        //var numOfVisibleRows = $(this).find('thead tr th').length;
        //if ($(this).find('tbody tr:visible').length == 0) {
        //    if ($(this).find('tbody tr span.no-data-found').length == 0 && $(this).find('tbody tr .tbl-no-row').length == 0)
        //        $(this).find('tbody').append("<tr><td colspan='" + numOfVisibleRows + "'><span class='no-data-found text-bold d-block mx-auto my-auto'>There is no data to display</td></tr>");
        //} else
        //    $(this).find('tbody tr span.no-data-found').parents("tr").remove();

    });
   

}

function openLeaveApplPop(th, act, leavTyp) {
    var refId = $(th).parents('tr').attr("data-refId");
    var studNo = $(th).parents('tr').attr("data-stud");
    if (!studNo) { studNo = $(th).attr("data-stud"); }
    var frmDt = $(th).parents('tr').attr("data-frmDate");
    if (!frmDt) { frmDt = ""; }
    var model = { LeaveType: leavTyp, RefId: refId, StudNo: studNo, FrmDt: frmDt };
    $.ajax({
        url: '/ParentCorner/Leave/LeaveApplicationModal?LeaveType=' + leavTyp + '&StudNo=' + studNo + '&RefId=' + refId + '&FromDate=' + frmDt,
        //data: JSON.stringify(model),
        dataType: 'html',
        async: false,
        success: function (response) {
            $("#leaveRequestModal .modal-content").html("");
            $("#leaveRequestModal .modal-content").html(response);
            $("#leaveRequestModal").modal("show");
            //InitDateTimePicker();
        },
        error: function () {
            globalFunctions.showErrorMessage("Error occured");
            return;
            //alert("error occured");
        }
    });

}

function InitDateTimePicker() {

    var fDT = $("#From_Date").val();
    var tod = new Date();
    if (fDT) {
        tod = new Date(fDT);
    }
    var minDateData = new Date(tod.getFullYear(), (tod.getMonth()), tod.getDate(), 0, 0, 0, 0);
    //datepicker
    $('.date-picker2').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: true,
        minDate: minDateData
    });

    $('.date-picker1').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: true,
        minDate: minDateData,
    }).on('dp.change', function (e) {
        //debugger
        //alert(e.date);
        //$('.date-picker2').data("DateTimePicker").clear();
        $('.date-picker2').data("DateTimePicker").minDate(e.date);
        if ($('.date-picker2').data("DateTimePicker").date() < e.date) {
            $('.date-picker2').data("DateTimePicker").clear();
            $('.date-picker2').data("DateTimePicker").minDate(e.date);
        }
    });

    $(".date-picker2").on("dp.change", function (e) {
        //debugger
        //
        //console.log(e.date);
        //$('.date-picker1').data("DateTimePicker").minDate(e.date);
        //$(".date-picker1").datetimepicker("refresh");
    });

    $(".date-picker2").data('DateTimePicker').setLocalDate(new Date());
    $(".date-picker1").data('DateTimePicker').setLocalDate(new Date());
}
function clearLeaveAppl() {
    $("#FromDate").val("");
    $("#ToDate").val("");
    $("#txtRemarks").val("");
    $("#StudentNo").val("");
    $("#Ref_Id").val("");
    $("#IsAddMode").val("");
    $("#UploadedImageName").val("");
    $("#ActualImageName").val("");
    $("#ddlLeaveTypes :selected").remove();
}