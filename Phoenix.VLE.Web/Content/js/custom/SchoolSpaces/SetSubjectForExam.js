﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

$(document).ready(function () {

    //$(".tblSubjectsGrid").each(function () {
    //    debugger
    //    var tr = $(this).find('tr').hasClass("tbl-no-row");       
    //    if (!tr) {         
    //        $(this).DataTable({
    //            "iDisplayLength": 5,
    //            "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
    //            "columnDefs": [{
    //                'targets': 5,
    //                'orderable': false                  
    //            }]
    //        });
    //    }
    //});

    //$(".tblExamPaperGrid").each(function () {
    //    debugger
    //    var tr = $(this).find('tr').hasClass("tbl-no-row");
    //    if (!tr) {
    //        $(this).DataTable({
    //            "iDisplayLength": 5,
    //            "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
    //            "columnDefs": [{
    //                'targets': 7,
    //                'orderable': false
    //            }]
    //        });
    //    }
    //});
    //$(".dataTables_length").hide();
    //$(".dataTables_filter").hide();

    $("#btnConfirmProceed").on("click", function () {
        var url = $("#GetPayUrl").val();
        if (url) {
            location.href = url;
        } else {
            return;
        }
    });

});

function onRefreshStudentDetails(studentNo) {

    $.ajax({
        url: '/ParentCorner/SubjectForExam/GetSubjectPaperDetails?studentNo=' + studentNo,
        //data: JSON.stringify(model),
        dataType: 'html',
        async: true,
        success: function (response) {
            $("#home-" + studentNo).html("");
            $("#home-" + studentNo).html(response);
        },
        error: function () {
            globalFunctions.showErrorMessage("Error occured");
            return;
            //alert("error occured");
        }
    });
}

function OpenRemovePopup(examPaperSetupId, studNo, paperName) {

    $("#ExamPaperSetupId").val(examPaperSetupId);
    $("#StudentNumber").val(studNo);
    var str = $(".alertRemove").text();
    str = str.replace("subject details", paperName);
    $(".textRemove").text(str);
    $("#removePopup").modal("show");
}

function OnRemovePaperSelection() {

    var examPaperSetupId = $("#ExamPaperSetupId").val();
    var studNo = $("#StudentNumber").val();
    if (examPaperSetupId) {
        $.ajax({
            url: '/ParentCorner/SubjectForExam/RemovePaperSelection',
            type: "POST",
            data: { 'PaperSetupId': examPaperSetupId },
            dataType: "json",
            success: function (result) {

                var data = JSON.parse(result.events), msg = "", succ = "";
                succ = data.success.toLowerCase(); msg = data.message;
                if (succ == "true") {
                    globalFunctions.showSuccessMessage(msg);
                    onRefreshStudentDetails(studNo);
                } else {
                    globalFunctions.showErrorMessage(msg);
                }
            },
            error: function (msg) {
                globalFunctions.onFailure();
            }
        });
        $("#removePopup").modal("hide");
    }
}

function onConfirmSelection(th, studNo) {
    var amount = parseFloat($("#net-payable-" + studNo).val());
    if (amount != NaN && amount > 0) {
        var studDetails = new Object();
        var paperDetails = [];
        studDetails.StudentNo = studNo;
        studDetails.TotalAmount = amount;

        $("input:checkbox[name=chk-" + studNo + "]:checked").each(function () {
            var tempAr = {
                "PaperSetupID": $(this).val(),
            }
            paperDetails.push(tempAr);
        });

        studDetails.LstPaperDetails = paperDetails;

        if (studDetails) {
            $.ajax({
                url: '/ParentCorner/SubjectForExam/SavePaperSelection',
                type: "POST",
                data: { 'obj': studDetails },
                dataType: "json",
                success: function (result) {

                    var data = JSON.parse(result.events), msg = "", succ = "";
                    succ = data.success.toLowerCase(); msg = data.message;
                    if (succ == "true") {
                        globalFunctions.showSuccessMessage(msg);
                        onRefreshStudentDetails(studNo);
                    } else {
                        globalFunctions.showErrorMessage(msg);
                    }
                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });
        }
    } else {
        globalFunctions.showErrorMessage("Please select subject or paper");
        return;
    }

}

function OnSubjectCheck(th, studNo) {

    var tblSubject = $("#tblSubject-" + studNo);
    var total = 0.0000
    $("input:checkbox[name=chk-" + studNo + "]:checked").each(function () {
        total += parseFloat($(this).closest('td').prev('td').text());
    });
    $("#net-payable-" + studNo).val(total.toFixed(2));
    if (total > 0) {
        $(".btnConfirmSelection-" + studNo).removeClass("d-none")
    } else {
        $(".btnConfirmSelection-" + studNo).addClass("d-none")
    }
}

function resetSelection(th, studNo) {

    //var tblSubject = $("#tblSubject-" + studNo + ' tbody tr').find("td:last");
    $("input:checkbox[name=chk-" + studNo + "]:checked").each(function () {
        this.checked = false;
        //$(this).removeAttr('checked');
    });
    $("#net-payable-" + studNo).val('0.00');
    //$("#home-" + studNo).find(".selectionPayAmount").val('0.00')
}

function payOnline(studNo) {

    var total = parseFloat($(".payingAmount-" + studNo).val());
    if (total != NaN && total > 0) {
        $.ajax({
            url: '/ParentCorner/SubjectForExam/PayOnlineExamFees',
            type: "POST",
            data: { 'studentNo': studNo },
            dataType: "json",
            success: function (result) {

                var data = JSON.parse(result.result), msg = "", succ = "";
                if (data.Response != null) {
                    succ = data.Response.success.toLowerCase(); msg = data.Response.message;
                    if (succ == "true") {
                        $("#GetPayUrl").val('');
                        $("#GetPayUrl").val(data.PaymentRedirectURL);
                        $("#confirmPopup").find(".totAmnt").html(total.toFixed(2));
                        $("#confirmPopup").find(".refNo").html("no. " + data.PaymentRefNo);
                        $("#confirmPopup").modal("show");
                        //var url = data.PaymentRedirectURL;
                        //if (url) {
                        //    location.href = url;
                        //} else {
                        //    return;
                        //}
                    } else {
                        globalFunctions.showErrorMessage(msg);
                    }
                }
                else {
                    globalFunctions.showErrorMessage(translatedResources.technicalErrorS);
                }
            },
            error: function (msg) {
                globalFunctions.onFailure();
            }
        });
    }
    else {
        globalFunctions.showErrorMessage("No amount found to pay.");
        return;
    }
}

function SelectStudentDetails(studentNo) {
    
        $.ajax({
            url: '/ParentCorner/SubjectForExam/GetSubjectPaperDetails?studentNo=' + studentNo,
            //data: JSON.stringify(model),
            dataType: 'html',
            async: true,
            success: function (response) {
                $("#home-" + studentNo).html("");
                $("#home-" + studentNo).html(response);
                $("#home-" + studentNo).data("selstudent", 1);
            },
            error: function () {
                globalFunctions.showErrorMessage("Error occured");
                return;
                //alert("error occured");
            }
        });
    
}