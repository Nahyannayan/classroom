﻿
var _dataTableLangSettings = {
    "sEmptyTable": translatedResources.noData,
    "sInfo": translatedResources.sInfo,
    "sInfoEmpty": translatedResources.sInfoEmpty,
    "sInfoFiltered": translatedResources.sInfoFiltered,
    "sInfoPostFix": "",
    "sInfoThousands": ",",
    "sLengthMenu": translatedResources.sLengthMenu,
    "sLoadingRecords": "Loading...",
    "sProcessing": "Processing...",
    "sSearch": translatedResources.search,
    "sZeroRecords": translatedResources.sZeroRecords,
    "oPaginate": {
        "sFirst": translatedResources.first,
        "sLast": translatedResources.last,
        "sNext": translatedResources.next,
        "sPrevious": translatedResources.previous
    },
    "oAria": {
        "sSortAscending": translatedResources.sSortAscending,
        "sSortDescending": translatedResources.sSortDescending
    }
};

var circularFunctions = function () {
    markAllAsRead = function () {
        var source = $("input:checkbox.Checkallletter:checked");
        var circularData = [];
        $(source).each(function () {
            circularData.push($(this).data("circulars"));
        });

        _url = translatedResources.baseUrl + translatedResources.PostCircularDetail;
        _data = JSON.stringify(circularData);
        $.ajax({
            headers: {
                "Content-Type": "application/json",
                "Authorization": 'Bearer ' + translatedResources.phoenixAccessToken
            },
            url: _url,
            type: "POST",
            data: _data,
            success: function (data) {
                if (data.success) {

                    $(source).each(function () {
                        var parentRow = $(this).closest('tr');
                        if (parentRow.closest('tr').hasClass("read-row")) {
                            parentRow.removeClass("read-row").addClass("unread-row");
                            parentRow.find('span.table-text').removeClass("text-primary");
                            var obj = parentRow.find('[type=checkbox]').data('circulars');
                            obj.ISRead = "true";
                            parentRow.find('[type=checkbox]').data('circulars', obj);
                        }
                        else {
                            parentRow.find('span.table-text').addClass("text-primary");
                            parentRow.addClass("read-row").removeClass("unread-row");
                            var obj = parentRow.find('[type=checkbox]').data('circulars');
                            obj.ISRead = "false";
                            parentRow.find('[type=checkbox]').data('circulars', obj);
                        }
                    });
                    globalFunctions.showSuccessMessage(translatedResources.showSuccessMessage);
                }
            },
            error: function (xhr, status) {
                globalFunctions.onFailure();
            }
        });
    },

        filterCircularGridRows = function () {
            var text = $("#search-cricular").val().toLowerCase();
            var selectedType = $("#ddlCirularNewsletterType").val().toLowerCase();
            $("tr[data-searchtext]").each(function () {
                $(this).toggle(($(this).data("searchtext").toLowerCase().indexOf(text) > -1 && (selectedType == "all" || $(this).data("type").toLowerCase().indexOf(selectedType) > -1)));
            });
            $("#divCircularGrid span.no-data-found").remove();
            if ($("tr[data-searchtext]:visible").length == 0) {
                $("#circularPaginctionRow").hide();
                $("#divCircularGrid").append("<span class='no-data-found text-bold d-block mx-auto  my-5'>" + translatedResources.noRecordsMessage + "</span>");
            } else $("#circularPaginctionRow").show();
        },

        initCircularsPagination = function () {
        var fromDate = globalFunctions.toSystemReadableDate($(".fromdate").val()) || "";
        var toDate = globalFunctions.toSystemReadableDate($(".todate").val()) || "";
            var grid = new DynamicPagination("divCircularGrid");
            var settings = {
                url: '/SchoolStructure/SchoolSpaces/GetFilteredCirculars?pageIndex=' + $("#CircularsPaginationIndex").val() + "&fromDate=" + fromDate + "&toDate=" + toDate
            };
            grid.init(settings);
        },

        filterReocrdsByDate = function () {
            var fromDate = globalFunctions.toSystemReadableDate( $(".fromdate").val()) || "";
        var toDate = globalFunctions.toSystemReadableDate($(".todate").val()) || "";
            _url = translatedResources.baseUrl + translatedResources.circularListAPI;
            _data = JSON.stringify({
                Identifier: _userLognType == usr_Student ? translatedResources.studenNumber : translatedResources.userName,
                Key: (_userLognType == usr_Teacher || _userLognType == usr_Admin) ? "staff" : _userLognType,
                FromDate: fromDate,
                ToDate: toDate,
                Language: "en"
            });
            $.ajax({
                url: _url,
                type: 'Post',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": 'Bearer ' + translatedResources.phoenixAccessToken,
                },
                data: _data,
                success: function (result) {
                    cirularHTML.getcicularTableData(result.data);
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure();
                }

            });
        },
        getCircularDetails = function (id) {
            _url = translatedResources.baseUrl + translatedResources.circularDetailsAPI;
            _data = JSON.stringify({ circularID: id, lang: 'en' });
            $.ajax({
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": 'Bearer ' + translatedResources.phoenixAccessToken,
                    "circularID": id,
                    "lang": 'en'
                },
                url: _url,
                type: 'GET',
                success: function (result) {
                    //called when successful
                    if (result.success === 'true') {
                        $("#divEmailDetails").html("");
                        $("#divEmailDetails").html(result.data.body);
                        $("#circularModalTitle").text(result.data.title);
                        //var NewsletterDate = new Date(convertToJavaScriptDate(data.circularDate));
                        //$('#SetDate').val(NewsletterDate.toDateString());
                        $("#selectCircularModal").data("circularid", result.data.circularId);

                        $("#selectCircularModal").modal('show');
                        var array = result.data.attachments;
                        $("#divEmailAttachments").html("");
                        if (array != null && array.length > 0) {
                            var html = "<b>Attachments : </b><br />";
                            for (var i = 0; i < array.length; i++) {
                                var filename = array[i].fileName;
                                if (filename != "") {
                                    html += "<a style='cursor:pointer;' href='" + array[i].fileURL + "' target='_blank'><i class='fas fa-paperclip'></i> " + filename + "</a><br/>";
                                }
                            }
                            $("#divEmailAttachments").html(html);
                        }
                    }
                },
                error: function (e) {

                }
            });
        };

    return {
        markAllAsRead: markAllAsRead,
        filterReocrdsByDate: filterReocrdsByDate,
        filterCircularGridRows: filterCircularGridRows,
        initCircularsPagination: initCircularsPagination,
        getCircularDetails: getCircularDetails
    }
}();

var cirularHTML = function () {
    getCirularHTML = function (circularData) {
        var cirularLayout = `
<div class="row">
    <div class="col-lg-12 col-md-12 col-12">
        <div class="community-search d-flex justify-content-end row search-row">
            <div class="col-12 col-lg-5 col-md-3 col-sm-6 col-xl-4 col-xs-11">
                <div class="search-with-bg border">
                    <span class="icon-search"></span>
                    <input type="search" class="searchfield" id="search-cricular" placeholder="`+ translatedResources.Search +`">
                </div>
            </div>
            <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6 col-xs-12 col-12">
                <div class="md-form md-outline sort-dropdown mb-1 mb-sm-0">
                    <label class="select-label">`+ translatedResources.Show +`</label>
                    <select class="selectpicker" id="ddlCirularNewsletterType">
                        <option value="ALL" selected>`+ translatedResources.All +`</option>
                        <option value="CIRCULAR">`+ translatedResources.Circular +`</option>
                        <option value="NEWSLETTER">`+ translatedResources.NewsLetter +`</option>
                    </select>
                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="md-form md-outline date-time-picker field-with-icon bg-white mb-1 mb-sm-0 arabictxt">
                    <i class="far fa-calendar-alt active"></i>
                    <input type="text" class="form-control date-picker datepicker fromdate" placeholder="`+ translatedResources.FromDate +`"  id="fromDate">

                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="md-form md-outline date-time-picker field-with-icon bg-white arabictxt">
                    <i class="far fa-calendar-alt active"></i>
                    <input type="text" class="form-control date-picker datepicker todate" placeholder="`+ translatedResources.ToDate +`" id="toDate">

                </div>
            </div>
        </div>
        <!-- search section end -->
        <!-- teacher grid card -->
        <div class="row grid-view">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="card community-wrapper">
                    <div class="responsive-list-table pagination-panel table-responsive" id="divCircularGrid">
                    <table class="table newsletter-circular-table mb-3" id="testTable">
        <thead>
            <tr>
                <th class="checkbox-column">
                    <div class="check-item">
                        <label for="group-value">
                            <input type="checkbox" id="group-value" class="chkSelectAllLettters" data-toggle="toggle" data-on="Show" data-off="Hide" data-onstyle="primary" data-size="small" data-width="95">
                        </label>
                    </div>
                </th>
                <th scope="col" class="align-middle" colspan="3">
                    <div class="align-items-center d-flex font-small justify-content-between">
                        `+ translatedResources.SelectAll+`
                        <button class="btn btn-white btn-sm m-0" type="button" style="display:none" onclick="circularFunctions.markAllAsRead()" id="btnMarkRead">`+translatedResources.MarkAsReadUnread+`</button>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>`;


        var cirularTableRow = `
    
                    <tr data-searchtext="#{Title}" data-type="#{Type}" class="#{ReadClass}" id="CirRow_#{CircularId}">
                        <td data-toggle="modal" id="" data-target="#infoPopup" class="date-column" style="display:none">
                            <span>#{CircularId} </span>
                        </td>

                        <td scope="row" class="checkbox-column">
                            <div class="check-item">
                                <label for="group-value1">
                                    <input type="checkbox" data-circulars='{"CircularID" : "#{CircularId}", "Key" : "#{Key}", "Identifier" : "#{Identifier}", "ISRead": "#{IsRead}", "IsFavorite": "true"}' class="Checkallletter" id="group-value1" data-toggle="toggle" data-on="Show" data-off="Hide" data-onstyle="primary" data-size="small" data-width="95">
                                </label>
                            </div>
                        </td>
                        <td>
                            <a data-toggle="modal" data-target="#infoPopup" class="d-flex align-items-start">
                                <span class="circular-img dx-icon-clock">
                                        <img src="#{TypeIcon}" class="svg">
                                </span>
                                <span class="table-text  mt-1"> #{TitleShort}</span>
                            </a>

                        </td>
                        <td data-toggle="modal" data-target="#infoPopup" class="circular-attachment-column">
                                #{AttachmentIcon}
                        </td>
                        <td data-toggle="modal" data-target="#infoPopup" class="date-column">
                            <span>#{Date}</span>
                        </td>

                    </tr>`;
        var cirularLayoutEnd = `</tbody>
    </table></div>
                </div>
            </div>
        </div>
    </div>
</div>`;
        var circularBodyList = '';
        $.each(circularData, function (key, value) {
            var circularBodyVar = {
                CircularId: value.circularId,
                Title: value.title,
                TitleShort: value.title.length > 90 ? value.title.substring(0, 90) + '...' : value.title,
                Type: value.type,
                IsRead: value.isRead ? "false" : "true",
                Date: moment(value.date).format('DD MMM YYYY'),
                Key: (_userLognType == usr_Teacher || _userLognType == usr_Admin) ? "staff" : _userLognType,
                Identifier: _userLognType == usr_Student ? translatedResources.studenNumber : translatedResources.userName,
                ReadClass: value.isRead ? "read-row" : "unread-row",
                TypeIcon: value.type.toLowerCase() == "newsletter" ? "/Content/VLE/img/svg/newsletter-icon.svg" : "/Content/VLE/img/svg/circular-icon.svg",
                AttachmentIcon: value.attachmentCount > 0 ? '<img src="/Content/VLE/img/svg/circular-attachment.svg">' : ''
            };
            var mappedHtml = globalFunctions.templateCreator(cirularTableRow, circularBodyVar);
            circularBodyList = circularBodyList + mappedHtml;
        });

        //console.log(folderBodyList);

        var data = cirularLayout + circularBodyList + cirularLayoutEnd;
        $("#communityContent").html("");
        if (data != '') {
            $("#communityContent").html(data);
            if (circularBodyList == '')
                $("#divCircularGrid").append("<span class='no-data-found text-bold d-block mx-auto my-5'>" + translatedResources.noRecordsMessage + "</span>");
            $(".selectpicker").selectpicker('refresh');
            //set calender dates
            var date = new Date(), last30Days = new Date(), y = date.getFullYear(), m = date.getMonth();
            last30Days = last30Days.setDate(last30Days.getDate() - 30);
            $("#fromDate").datetimepicker({ format: 'DD-MMM-YYYY', date: moment(last30Days), locale: translatedResources.locale });
            $("#toDate").datetimepicker({ format: 'DD-MMM-YYYY', date: date, locale: translatedResources.locale });

            $(".datepicker").datetimepicker({ format: 'DD-MMM-YYYY', locale: translatedResources.locale}).on("dp.change", function (e) {
                if ($(e.target).is("#fromDate"))
                    $('#toDate').data("DateTimePicker").minDate(e.date);
                circularFunctions.filterReocrdsByDate();
            });
            globalFunctions.enableCheckboxCascade(".chkSelectAllLettters", "input:checkbox.Checkallletter");
        }
    }
    getcicularTableData = function (circularData) {
        var cirularTableRow = `
    
                    <tr data-searchtext="#{Title}" data-type="#{Type}" class="#{ReadClass}" id="CirRow_#{CircularId}">
                        <td data-toggle="modal" id="" data-target="#infoPopup" class="date-column" style="display:none">
                            <span>#{CircularId} </span>
                        </td>

                        <td scope="row" class="checkbox-column">
                            <div class="check-item">
                                <label for="group-value1">
                                    <input type="checkbox" data-circulars='{"CircularID" : "#{CircularId}", "Key" : "#{Key}", "Identifier" : "#{Identifier}", "ISRead": "#{IsRead}", "IsFavorite": "true"}' class="Checkallletter" id="group-value1" data-toggle="toggle" data-on="Show" data-off="Hide" data-onstyle="primary" data-size="small" data-width="95">
                                </label>
                            </div>
                        </td>
                        <td>
                            <a data-toggle="modal" data-target="#infoPopup" class="d-flex align-items-start">
                                <span class="circular-img dx-icon-clock">
                                        <img src="#{TypeIcon}" class="svg">
                                </span>
                                <span class="table-text  mt-1"> #{TitleShort}</span>
                            </a>

                        </td>
                        <td data-toggle="modal" data-target="#infoPopup" class="circular-attachment-column">
                                #{AttachmentIcon}
                        </td>
                        <td data-toggle="modal" data-target="#infoPopup" class="date-column">
                            <span>#{Date}</span>
                        </td>

                    </tr>`;
        var circularBodyList = '';
        $.each(circularData, function (key, value) {
            var circularBodyVar = {
                CircularId: value.circularId,
                Title: value.title,
                TitleShort: value.title.length > 90 ? value.title.substring(0, 90) + '...' : value.title,
                Type: value.type,
                IsRead: value.isRead ? "false" : "true",
                Date: moment(value.date).format('DD MMM YYYY'),
                Key: (_userLognType == usr_Teacher || _userLognType == usr_Admin) ? "staff" : _userLognType,
                Identifier: _userLognType == usr_Student ? translatedResources.studenNumber : translatedResources.userName,
                ReadClass: value.isRead ? "read-row" : "unread-row",
                TypeIcon: value.type.toLowerCase() == "newsletter" ? "/Content/VLE/img/svg/newsletter-icon.svg" : "/Content/VLE/img/svg/circular-icon.svg",
                AttachmentIcon: value.attachmentCount > 0 ? '<img src="/Content/VLE/img/svg/circular-attachment.svg">' : ''
            };
            var mappedHtml = globalFunctions.templateCreator(cirularTableRow, circularBodyVar);
            circularBodyList = circularBodyList + mappedHtml;
        });
        var data = circularBodyList;
        $("#divCircularGrid tbody").html("");
        $("#divCircularGrid span.no-data-found").remove();
        if (circularBodyList != '') {
            $("#divCircularGrid tbody").html(data);
            globalFunctions.enableCheckboxCascade(".chkSelectAllLettters", "input:checkbox.Checkallletter");
        }
        else {
            $("#divCircularGrid").append("<span class='no-data-found text-bold d-block mx-auto  my-5'>" + translatedResources.noRecordsMessage + "</span>");
        }
    };
    return {
        getCirularHTML: getCirularHTML,
        getcicularTableData: getcicularTableData
    }
}();
$(document).ready(function () {

    //$(document).on("DynamicPagination:Complete", function () {
    //    globalFunctions.enableCheckboxCascade(".chkSelectAllLettters", "input:checkbox.Checkallletter");
    //});

    $(document).on("change", ".chkSelectAllLettters, .Checkallletter", function () {
        $("#btnMarkRead").toggle($("input:checkbox.Checkallletter:checked").length > 0);

    });

    $(document).on("change", "input:checkbox[data-unreadcircularid]", function () {
        $("#btnMarkRead").toggle($("input:checkbox[data-unreadcircularid]:checked").length > 0);
    });

    $(document).on("keyup", "#search-cricular", function () {
        circularFunctions.filterCircularGridRows();
    });

    $(document).on("change", "#ddlCirularNewsletterType", circularFunctions.filterCircularGridRows);

    $(document).on('click', '#testTable tbody tr', function (e) {
        if ($(e.target).is("input:checkbox")) return;
        var id = $(this).find("td:first-child").text().trim();
        $(this).toggleClass("select");
        circularFunctions.getCircularDetails(id);
    });

    $(document).on("click", ".close", function () {
        var circularId = $("#selectCircularModal").data("circularid");
        if ($('#CirRow_' + circularId).hasClass("unread-row")) {
            var circularData = [];
            circularData.push($('#CirRow_' + circularId + " input:checkbox.Checkallletter").data("circulars"));
            _url = translatedResources.baseUrl + translatedResources.PostCircularDetail;
            _data = JSON.stringify(circularData);
            $.ajax({
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": 'Bearer ' + translatedResources.phoenixAccessToken
                },
                url: _url,
                method: 'POST',
                data: _data,
                success: function (result) {
                    if (result.success === 'true') {
                        $('#CirRow_' + circularId).removeClass('unread-row');
                        $('#CirRow_' + circularId).addClass('read-row');
                    } else globalFunctions.showMessage(result.NotificationType, result.Message);
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            });
        }
    });
});
function convertToJavaScriptDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return (dt.getMonth() + 1 + "/" + dt.getDate() + "/" + dt.getFullYear());

}
