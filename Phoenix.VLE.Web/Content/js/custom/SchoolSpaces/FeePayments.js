﻿$(document).ready(function () {

    var tod = new Date();
    $("#GetPayUrl").val('');
    $(".title-date").text(translatedResources.date+" " + tod.ddMMMyyyFormat());
    $('.toggable-items-row .transport').click(function () {
        $(this).addClass('active');
        $('.school,.activity').removeClass('active');
        $('.transport-wrapper').removeClass('hide-item');
        $('.school-wrapper').addClass('hide-item');
        $('.activity-wrapper').addClass('hide-item');
    });
    $('.toggable-items-row .school').click(function () {
        $(this).addClass('active');
        $('.transport,.activity').removeClass('active');
        $('.school-wrapper').removeClass('hide-item');
        $('.transport-wrapper').addClass('hide-item');
        $('.activity-wrapper').addClass('hide-item');
    });
    $('.toggable-items-row .activity').click(function () {
        $(this).addClass('active');
        $()
        $('.transport,.school').removeClass('active');
        $('.activity-wrapper').removeClass('hide-item');
        $('.transport-wrapper').addClass('hide-item');
        $('.school-wrapper').addClass('hide-item');
    });

    //$('#card-wrapper .paying-card-container .nbad-card .custom-control-label').click(function () {
    //    //console.log('nbad', $(this).parents())
    //    //
    //    $(this).parents('#card-wrapper').find('.select-card.payment-card.nbad-card').removeClass('active');
    //    $(this).parents('.select-card.payment-card.nbad-card').addClass('active');
    //});

    //.select-card.child-card 
    //$('input[type="checkbox"]').click(function () {

    // });

    // $(".btnProceedToPay").on("click", function () {

    //});

    $("#btnConfirmProceed").on("click", function () {

        var url = $("#GetPayUrl").val();
        if (url) {
            location.href = url;
        } else {
            return false;
        }
    });

    var allSource = $(".card .source");
    allSource.each(function () {
        var source = $(this).attr('data-main-source');
        NetPaymentSection(source);
    });

    //$(".navTabToggle").on("click", function () {

    //    var currentTab = $(this).attr("href");
    //    $.ajax({
    //        url: '/ParentCorner/Fee/Get?Tab=' + currentTab,
    //        //data: JSON.stringify(model),
    //        dataType: 'html',
    //        async: false,
    //        success: function (response) {
    //            $("#home-" + studentNo).html("");
    //            $("#home-" + studentNo).html(response);
    //        },
    //        error: function () {
    //            globalFunctions.showErrorMessage("Error occured");
    //            return;
    //        }
    //    });
    //});

    //Print Fees Shedule
    //$(document).on('click', '#btnprintFeeshedule', function () {
    //    printDiv();
    //}

});

function OnPaymentCardSelection(th, source) {

    $(".paymCardType-" + source).find(".payment-card").removeClass("active");
    $(th).parents(".payment-card").addClass("active");

    CancelResetFeeDetails(source);
    //var isPaySibling = ""; var students = "", provTypId = "";
    //var getAllChk = [];
    //$.each($("input[name='" + source + "']:checked"), function () {
    //    getAllChk.push($(this).val());
    //});
    //isPaySibling = getAllChk.length > 1 ? "1" : "0";
    //provTypId = $("input[name='radio-payment-card-" + source + "']:checked").val();
    //if (!provTypId) {
    //    globalFunctions.showErrorMessage(translatedResources.paymentTypeMsg);
    //    return false;
    //}
    //if (getAllChk.length > 0) {
    //    students = getAllChk.toString();
    //    if (students) {
    //        GetFeeDetails(students, provTypId, isPaySibling, source);
    //    }
    //    else {
    //        $("." + source + "-wrapper #studFeeDetlsTbl").html("");
    //        globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
    //    }
    //}
    //else {
    //    $("." + source + "-wrapper #studFeeDetlsTbl").html("");
    //    globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
    //}
}

function ProceedToPay(th) {
    
    var studFeeDetails = []; var sucess = true;
    var DiscTot = 0.0000, Tot = 0.0000, ProcCharge = 0.0000, NetPay = 0.0000;
    var source = $(th).data("source");
    var main = $('.NetAmount-' + source);
    var NetPayable = parseFloat(replaceComma(main.find("#net-payable").val()));
    if (!$.isNumeric(NetPayable) || NetPayable <= 0) {
        globalFunctions.showErrorMessage(translatedResources.CalculationNetAmt);
        return false;
    }

    var payTypId = $("input[name='radio-payment-card-" + source + "']:checked").attr("data-payTypId");
    var feeAll = $('.' + source + '-wrapper .tblFeeDetail tbody'); //  tr: last
    var isPaySibling = "";
    isPaySibling = feeAll && feeAll.length > 1 ? "1" : "0";

    feeAll.each(function () {
        var feeDetails = new Object();
        $(this).find("tr:last").filter(function () {

            //var obj = new Object();
            var stud = $(this).data("stud");
            var processingCharge = 0.0000, tempDisc = 0.0000, tempTot = 0.0000;
            processingCharge = parseFloat($(this).attr('data-procCharge').trim());
            tempDisc = parseFloat(replaceComma($(this).find('td:eq(2)').text().trim()));
            tempTot = parseFloat(replaceComma($(this).find('td:eq(3)').text().trim()));
            //if (tempTot <= 0) {
            //    globalFunctions.showErrorMessage("There should be some amount for pay now of each selected student.");
            //    sucess = false;
            //    return false;
            //}
            DiscTot = tempDisc;
            ProcCharge = ConvertPercentage(tempTot, processingCharge);
            NetPay += tempTot + ProcCharge;

            feeDetails.STU_NO = stud;
            feeDetails.PaymentTypeID = payTypId;
            feeDetails.PayingAmount = tempTot;
            feeDetails.PaymentProcessingCharge = ProcCharge;
            feeDetails.Source = source.toUpperCase();
            feeDetails.IsPaySibling = isPaySibling;
        });

        var studWiseFee = [];
        $(this).find("tr").not(':last').filter(function () {
            var tempAr = {
                "FeeID": $(this).attr("data-feeId"),
                "ActivityRefID": $(this).attr("data-activityRefId"),
                "FeeDescription": $(this).find('td:eq(0)').find(".feeDesc").text().trim(),
                "DiscAmount": parseFloat($(this).find('td:eq(2)').find('.tdDiscAmnt').text().trim()),
                "PayAmount": parseFloat($(this).find('td:eq(3)').find('input').val()),
                "AdvanceDetails": [],
            }
            studWiseFee.push(tempAr);
        });
        feeDetails.FeeDetail = studWiseFee;

        studFeeDetails.push(feeDetails);

        //console.log(studFeeDetails);
    });

    if (studFeeDetails.length > 0 && sucess == true) {
        $.ajax({
            url: '/ParentCorner/Fee/SubmitToPay',
            type: "POST",
            data: { 'objVM': studFeeDetails },
            dataType: "json",
            success: function (result) {

                var data = JSON.parse(result.result), msg = "", succ = "";
                if (data.Response != null) {
                    succ = data.Response.success.toLowerCase(); msg = data.Response.message;
                    if (succ == "true") {
                        $("#GetPayUrl").val('');
                        $("#GetPayUrl").val(data.PaymentRedirectURL);
                        $("#confirmPopup").find(".totAmnt").html(NetPayable.toFixed(2));
                        $("#confirmPopup").find(".refNo").html("no. " + data.PaymentRefNo);
                        $("#confirmPopup").modal("show");
                    } else {
                        globalFunctions.showErrorMessage(msg);
                    }
                }
                else {
                    globalFunctions.showErrorMessage(translatedResources.technicalError);
                }
            },
            error: function (msg) {
                globalFunctions.onFailure();
            }
        });
    }
}

function OnStudentCheck(th) {

    // $(this).parents('.select-card.child-card').toggleClass('active');
    var students = "", provTypId = "";
    var name = $(th).data("source");
    var studNo = $(th).val();

    //var $radios = $("." + name + "-wrapper .payment-typ-card input");
    var isChecked = $(th).is(":checked");
    if (!isChecked) {
        $(th).parents(".child-card").removeClass("active");
        $(th).prop("checked", false);
    } else {
        $(th).parents(".child-card").addClass("active");
    }
    //var preSelStud = $("input[name='" + name + "']:checked:visible:first").val();
    var isPaySibling = "";
    var getAllChk = [];
    $.each($("input[name='" + name + "']:checked"), function () {
        getAllChk.push($(this).val());
    });
    var preSelStud = getAllChk.find(function (element) {
        return element != studNo;
    });
    var resIsSibling = preSelStud != undefined && preSelStud != null ? checkIsPaySibling(preSelStud, studNo) : true;
    if (!resIsSibling) {
        $("input[name='" + name + "']:checked").prop('checked', false);
        $.each($("input[name='" + name + "']"), function () {
            $(this).parents(".child-card").removeClass("active");;
        });
        $(th).parents(".child-card").addClass("active");
        $(th).prop("checked", true);
        GetPaymentTypeOnStudent(studNo, name);
        $("." + name + "-wrapper #studFeeDetlsTbl").html("");
        getAllChk = [];
        $.each($("input[name='" + name + "']:checked"), function () {
            getAllChk.push($(this).val());
        });
        //globalFunctions.showWarningMessage("Selected student is not from same school.");
        //return false;
    }

    isPaySibling = resIsSibling && getAllChk.length > 1 ? "1" : "0";

    provTypId = $("input[name='radio-payment-card-" + name + "']:checked").val();
    if (!provTypId) {
        globalFunctions.showErrorMessage(translatedResources.paymentTypeMsg);
        return false;
    }

    if (getAllChk.length > 0) {
        students = getAllChk.toString();
        if (students) {
            GetFeeDetails(students, provTypId, isPaySibling, name);

        } else {
            $("." + name + "-wrapper #studFeeDetlsTbl").html("");
            globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
        }
    } else {
        $("." + name + "-wrapper #studFeeDetlsTbl").html("");
        globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
    }

    //$(this).parents('.select-card.child-card').toggleClass('active');
}

function CancelResetFeeDetails(source) {

    var students = "", provTypId = "";
    var name = source;
    var isPaySibling = "";
    var getAllChk = [];
    $.each($("input[name='" + name + "']:checked"), function () {
        getAllChk.push($(this).val());
    });

    isPaySibling = getAllChk.length > 1 ? "1" : "0";
    provTypId = $("input[name='radio-payment-card-" + name + "']:checked").val();
    if (!provTypId) {
        globalFunctions.showErrorMessage(translatedResources.paymentTypeMsg);
        return false;
    }

    if (getAllChk.length > 0) {
        students = getAllChk.toString();
        if (students) {
            GetFeeDetails(students, provTypId, isPaySibling, name);

        } else {
            $("." + name + "-wrapper #studFeeDetlsTbl").html("");
            globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
        }
    } else {
        $("." + name + "-wrapper #studFeeDetlsTbl").html("");
        globalFunctions.showErrorMessage(translatedResources.NoStudentSelected);
    }
}

function modalPaymTypeInfo(sourePaymTyp) {

    var source = $("." + sourePaymTyp + "-wrapper");
    //var studNo = source.find(".payTypActive ,activePT").attr("data-StudNo");
    var getAllChk = [];
    $.each($("input[name='" + sourePaymTyp + "']:checked"), function () {
        getAllChk.push($(this).val());
    });
    var studNo = "";
    if (getAllChk.length > 0) {
        studNo = getAllChk[0];
    }
    if (studNo && source) {
        $.ajax({
            url: '/ParentCorner/Fee/GetPaymentType?studNo=' + studNo + '&source=' + sourePaymTyp,
            dataType: 'json',
            //timeout: 500,     
            type: "GET",
            success: function (data, status, xhr) {

                var lstPTypes = JSON.parse(data.paymentTypes);
                var modalMorInfo = $("#moreInfo .card-list-info");
                modalMorInfo.html("");
                var strDiv = ``;
                $.each(lstPTypes, function (i, obj) {
                    var strInfo = obj.MoreInfo != null ? obj.MoreInfo : '<span class="no-data-found text-bold d-block mx-auto my-auto">"' + translatedResources.noRecords+'"</span>';
                    strDiv += `<li class="d-flex flex-wrap flex-sm-nowrap">
                            <div class="p-0 select-card payment-card nbad-card active">
                                    <img class="rounded-2x payment-type-im" src="`+ obj.PaymentTypeImagePath + `" alt="` + obj.ProviderTypeDesc + `" />                               
                            </div>
                            <div class="select-card-content pt-2 pt-sm-0">
                                <div>`+ obj.ProviderTypeDesc + `</div>
                                <p>`+ strInfo + `</p>
                            </div>
                        </li>  `;
                });
                if (lstPTypes.length <= 0) {
                    strDiv = `<li class="d-flex flex-wrap flex-sm-nowrap"><span class="no-data-found text-bold d-block mx-auto my-auto">
                             ` + translatedResources.noRecords +`</span></li>`;
                }
                modalMorInfo.append(strDiv);
                if (strDiv) { $("#moreInfo").modal("show"); }
            },
            error: function (jqXhr, textStatus, errorMessage) {
            }
        });
    }
}

function modalScheduleFee(studNo, studName, grade) {
    ////;
    $.ajax({
        url: '/ParentCorner/Fee/GetScheduleFee?studNo=' + studNo,
        dataType: 'json',
        //timeout: 500,
        type: "GET",
        success: function (data, status, xhr) {
            var lstSchdFee = JSON.parse(data.scheduleFee);
            var modalMorInfo = $("#fee-schedule-table tbody");
            modalMorInfo.html("");
            var strDiv = ``;
            $.each(lstSchdFee, function (i, obj) {
                var Duedate = new Date(obj.DueDate).ddMMMyyyFormat();
                strDiv += `<tr class="">
                                        <td>`+ obj.FeeDescription + `</td>
                                        <td>`+ Duedate + `</td>
                                        <td class="text-right">`+ (obj.Amount).toFixed(2) + `</td>
                                    </tr>`;
            });
            if (lstSchdFee.length <= 0) {
                strDiv = `<tr class=""><td colspan="3"><span class="no-data-found text-bold d-block mx-auto my-auto">` + translatedResources.noRecords+`</span></td></tr>`;
            }
            modalMorInfo.append(strDiv);
            if (strDiv) {
                $("#scheduleOfFees .name-title").text(studName);
                $("#scheduleOfFees .student-grade").text(grade);
                $('#printSch').prop('href', '../parentcorner/Fee/PrintFeesShedule/' + studNo);
                $("#scheduleOfFees").modal("show");
            }
        },
        error: function (jqXhr, textStatus, errorMessage) {
        }
    });
}

function modalDiscountInfo(th,stuNo, source, feeId) {
    //;
    var teSource = source;
    var payTypId = $("input[name='radio-payment-card-" + source + "']:checked").attr("data-payTypId");
    //var tblFeeDet = $('.tbl-' + source + '-' + stuNo);
    //var amount = tblFeeDet.find("#tdPayNow").val();
    //var payNow = parseFloat($(th).closest('td').next('td').find('input').val());
    var amount = parseFloat($(th).attr("data-amount"));
    if (!payTypId) { globalFunctions.showErrorMessage(translatedResources.paymentTypeMsg); return false; }
    //else if (payNow === NaN || payNow <= 0) { globalFunctions.showErrorMessage("Paying now should be greater than zero. "); return; }
    else if (amount === NaN || amount <= 0) { globalFunctions.showErrorMessage(translatedResources.NoDiscount); return; }
    if (amount !== NaN) {
        $.ajax({
            url: '/ParentCorner/Fee/GetDiscountOnAmount?studNo=' + stuNo + '&paymentTypeID=' + payTypId + '&amount=' + amount + '&feeID=' + feeId,
            dataType: 'json',
            //timeout: 500,     
            type: "GET",
            success: function (data, status, xhr) {


                var lstSchdFee = JSON.parse(data.events);
                if (lstSchdFee.success == "false") {
                    globalFunctions.showMessage("error", lstSchdFee.message);
                    return false;
                }
                var lstDisc;
                if (lstSchdFee) {
                    lstDisc = lstSchdFee.data.discountDetails;
                }

                var modalMorInfo = $("#discount-info-table tbody");
                modalMorInfo.html("");
                var strDiv = ``;
                if (lstDisc) {
                    $.each(lstSchdFee.data.discountDetails, function (i, obj) {

                        strDiv += `<tr class="">
                                        <td>`+ obj.month + `</td>
                                        <td>`+ obj.feeAmount + `</td>
                                        <td>`+ obj.discPrec + `</td>
                                        <td>`+ obj.discAmount + `</td>
                                        <td>`+ obj.netAmount + `</td>
                                    </tr>`;
                    });
                }
                if (strDiv) {
                    modalMorInfo.append(strDiv);
                } else {
                    modalMorInfo.append('<tr class=""> <td colspan="5"> <span class="no-data-found text-bold d-block mx-auto my-auto">"' + translatedResources.noRecords+'"</span ></td ></tr>')
                }
                $("#discountInfo").modal("show");
            },
            error: function (jqXhr, textStatus, errorMessage) {
            }
        });
    }

}

function GetPaymentTypeOnStudent(stuNo, source) {

    $.ajax({
        url: '/ParentCorner/Fee/GetPaymentTypeOnStudent?studNo=' + stuNo + "&source=" + source,
        dataType: 'html',
        //timeout: 500,     
        type: "GET",
        async: false,
        success: function (response) {
            $(".paymCardType-" + source).html("");
            $(".paymCardType-" + source).html(response);
        },
        error: function (jqXhr, textStatus, errorMessage) {
            globalFunctions.showErrorMessage(errorMessage);
            return false;
        }
    });
}

function GetDiscountOnPayingAmount(th, stuNo, payTypId, feeId, source) {

    var amount = parseFloat($(th).val());
    if (!amount) { $(th).val('0.00'); amount = 0 }
    var teSource = source;
    payTypId = $("input[name='radio-payment-card-" + source + "']:checked").attr("data-payTypId");
    if (!payTypId) { globalFunctions.showErrorMessage(translatedResources.paymentTypeMsg); }

    $(th).closest('td').prev('td').find("a").attr("data-amount", amount);
    if (payTypId) {
        $.ajax({
            url: '/ParentCorner/Fee/GetDiscountOnAmount?studNo=' + stuNo + '&paymentTypeID=' + payTypId + '&amount=' + amount + '&feeID=' + feeId,
            dataType: 'json',
            //timeout: 500,     
            type: "GET",
            success: function (data, status, xhr) {

                var discount = JSON.parse(data.events);
                var sourc = $('.tbl-' + teSource.toString() + '-' + stuNo.toString());
                //var amount = $('.NetAmount-' + teSource);
                if (discount.success == "true") {
                    var discDt = discount.data.discountAmount;
                    $(th).closest('td').prev('td').find(".tdDiscAmnt").text(discDt.toFixed(2));
                    $(th).val((parseFloat(amount) - parseFloat(discDt)).toFixed(2));
                    //sourc.find("#tdDiscAmt").text(discDt.toString());
                    //sourc.find("#tdPayAmt").text(parseFloat(amount));// - parseFloat(discDt)

                    //amount.find("#DiscountTotal").text(discount.data.discountAmount.toString());
                    //amount.find("#total").text(amount);
                    //amount.find("#processing-charge").text('0');
                    //amount.find("#net-payable").text(amount);
                    $(th).closest('td').prev('td').find(".discIcon").removeClass("hide-item");
                }
                else {
                    $(th).closest('td').prev('td').find(".tdDiscAmnt").text('0.00');
                    if (!$(th).closest('td').prev('td').find(".discIcon").hasClass("hide-item")) {
                        $(th).closest('td').prev('td').find(".discIcon").addClass("hide-item");
                    }
                    //globalFunctions.showErrorMessage(discount.message);
                }
                $(th).closest('td').prev('td').find("a").attr("data-amount", amount);
                TotalAmountForStudent(teSource, stuNo);
                NetPaymentSection(teSource);
            },
            error: function (jqXhr, textStatus, errorMessage) {
            }
        });
    } else {

    }

}

function GetFeeDetails(stuNo, providerTypId, IsSibling, source) {
    // var amount = $(th).val();
    $.ajax({
        url: '/ParentCorner/Fee/GetFeeDetails?students=' + stuNo + '&paymentTo=' + source + '&providerTypeID=' + providerTypId.toString() + '&isPaySibling=' + IsSibling.toString(),
        dataType: 'html',
        //timeout: 500,     
        type: "GET",
        success: function (response) {
            $("." + source + "-wrapper #studFeeDetlsTbl").html("");
            $("." + source + "-wrapper #studFeeDetlsTbl").html(response);
            NetPaymentSection(source);
        },
        error: function (jqXhr, textStatus, errorMessage) {
            globalFunctions.showErrorMessage(errorMessage);
            return false;
        }
    });
}

function checkIsPaySibling(prevStud, CurrStud) {
    var result = true;

    $.ajax({
        url: '/ParentCorner/Fee/CheckIsPaySibling?PreSelectedStudent=' + prevStud + '&SelectedStudent=' + CurrStud,
        dataType: 'json',
        async: false,
        success: function (data, status, xhr) {

            var discount = JSON.parse(data.IsSibling);
            if (discount) {
                result = true;
            } else {
                result = false;
            }
            //var sourc = $('.tbl-' + source);
            //var amount = $('.NetAmount-' + source);

        },
        error: function (jqXhr, textStatus, errorMessage) {
            result = false;
        }
    });
    return result;
}

function NetPaymentSection(source) {

    //$(".toDiv").find(".on").length > 0
    if ($(".fee-payment-container").find(".NetAmount-" + source).length > 0) {
        var DiscTot = 0.0000, Tot = 0.0000, ProcCharge = 0.0000, NetPay = 0.0000;
        var tblFeeDet = $('.NetAmount-' + source);
        var payTypId = $("input[name='radio-payment-card-" + source + "']:checked").attr("data-payTypId");
        var feeAll = $('.' + source + '-wrapper .tblFeeDetail'); //  tr: last
        var processingCharge = 0.0000;
        processingCharge = parseFloat($("input[name='radio-payment-card-" + source + "']:checked").attr('data-procCharge').trim());
        feeAll.find("tr:last").filter(function () {
            var tempDisc = 0.0000, tempTot = 0.0000;
            tempDisc = parseFloat(replaceComma($(this).find('td:eq(2)').text().trim()));
            tempTot = parseFloat(replaceComma($(this).find('td:eq(3)').text().trim()));
            DiscTot += tempDisc;
            Tot += tempTot;

            //DiscTot += ProcCharge; 
            NetPay += tempTot; // + ProcCharge;
        });
        ProcCharge = ConvertPercentage(Tot, processingCharge);
        NetPay += ProcCharge

        tblFeeDet.find("#DiscountTotal").val(DiscTot.toFixed(2));
        tblFeeDet.find("#total").val(Tot.toFixed(2));
        tblFeeDet.find("#processing-charge").val(ProcCharge.toFixed(2));
        tblFeeDet.find("#net-payable").val(NetPay.toFixed(2));
        if (NetPay > 0) {
            $(".btnProceedToPay-" + source).prop("disabled", false);
        } else {
            $(".btnProceedToPay-" + source).prop("disabled", true);
        }
    }
}

function TotalAmountForStudent(source, student) {

    var paynow = 0.0000, DiscTot = 0.0000, AmntDue = 0.0000;
    var tbl = $('.tbl-' + source + '-' + student + ' tbody tr').not(':last');
    //AmntDue += parseFloat(tbl.find("td:eq(1)").text().trim());
    //DiscTot += parseFloat(tbl.find("td:eq(2)").find('.tdDiscAmnt').text().trim());
    //paynow += parseFloat(tbl.find("td:eq(3)").find('input').val());
    tbl.find("td:eq(2)").find('.tdDiscAmnt').each(function () {
        var combat = replaceComma($(this).text());
        if (!isNaN(combat) && combat.length !== 0) {
            DiscTot += parseFloat(combat);
        }
    });
    tbl.find("td:eq(3)").find('input').each(function () {
        var combat = replaceComma($(this).val());
        if (!isNaN(combat) && combat.length !== 0) {
            paynow += parseFloat(combat);
        }
    });
    var tblLastRow = $('.tbl-' + source + '-' + student + ' tbody tr:last');
    //tblLastRow.find("#tdDueAmt").text(AmntDue);
    tblLastRow.find("#tdDiscAmt").text(DiscTot.toFixed(2));
    tblLastRow.find(".tbl-actions #tdPayAmt").text(paynow.toFixed(2));

}

function ConvertPercentage(num, percentage) {
    //
    var number = num;
    //The percent that we want to get.
    //i.e. We want to get 22% of 90.
    var percentToGet = percentage;

    //Turn our percent into a decimal figure.
    //22 will become 0.22 and 60 will become 0.6
    var percentAsDecimal = (percentToGet / 100);

    //Multiply the percent decimal by the number.
    var percent = percentAsDecimal * number;
    return percent;
}


function rangeSliderInit() {
    //range slider for payment
    if ($("input[type='range']").length) {
        $('input[type="range"]').rangeslider({
            polyfill: false,

            rangeClass: 'rangeslider',
            disabledClass: 'rangeslider--disabled',
            horizontalClass: 'rangeslider--horizontal',
            fillClass: 'rangeslider__fill',
            handleClass: 'rangeslider__handle',

            // Callback function
            onInit: function () {
                $rangeEl = this.$range;
                // add value label to handle
                var $handle = $rangeEl.find('.rangeslider__handle');
                var handleValue = '<div class="rangeslider__handle__value">' + this.value + '</div>';
                $handle.append(handleValue);

                // get range index labels 
                var rangeLabels = this.$element.attr('labels');
                rangeLabels = rangeLabels.split(', ');

                // add labels
                $rangeEl.append('<div class="rangeslider__labels"></div>');
                $(rangeLabels).each(function (index, value) {
                    $rangeEl.find('.rangeslider__labels').append('<span class="rangeslider__labels__label">' + value + '</span>');
                })
            },

            // Callback function
            onSlide: function (position, value) {
                //
                var $handle = this.$range.find('.rangeslider__handle__value');
                $handle.text(this.value);
            },

            // Callback function
            onSlideEnd: function (position, value) {
                //;
                console.log(this);
                var amount = 0.0000;
                var curIndx = this.value;
                var advValues = this.$element.parents(".rangeslider-div").find('input[name=advance-val]').val();
                var nameArr = advValues.split(',');
                var total = 0.0000;
                for (var i = 0; i < curIndx; i++) {
                    total += parseFloat(nameArr[i]);
                }
                if (curIndx > 0) {
                    //var indVal = nameArr[parseInt(curIndx) - 1]
                    this.$element.parents(".rangeslider-div").find('.advanceAmount').val(total.toFixed(2));
                }
                else this.$element.parents(".rangeslider-div").find('.advanceAmount').val('0.00');

            }
        });
    }
    $('.btnSliderCancle').click(function () {
        $('body').removeClass('range-overlay');
        $('.make-payment').fadeOut('fast');
    });

    $('.addSlider').click(function () {
        //
        var slider = $(this).parents('.rangeslider-div');
        var parentTR = $(this).parents('tr');
        var amount = parseFloat(slider.find(".advanceAmount").val());
        if (amount <= 0) {
            globalFunctions.showErrorMessage("Total should not be zero.");
            return false;
        }
        parentTR.find("td:eq(3)").find('input').val(amount);
        parentTR.find("td:eq(3)").find('input').change();
        $('body').removeClass('range-overlay');
        $('.make-payment').fadeOut('fast');
    });
}

function rangeView(source, stud, feeid) {
    //
    $('body').addClass('range-overlay');
    $('.Advcpaym-' + source + '-' + stud + '-' + feeid).fadeIn('fast');
    rangeSliderInit();
}


function printDiv() {

    var divName = "printSchFee";
    var nameSt = $("#nameSt").html();
    var htm = document.getElementById(divName);
    var printContents = htm.innerHTML;
    nameSt += printContents;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = nameSt;
    window.print();
    document.body.innerHTML = originalContents;
    //window.onfocus = function () { closeModal("scheduleOfFees"); }
}

function closeModal(id) {
    //alert('success');
    $("#" + id).modal("hide");
}

//document.getElementById("btnPrint").onclick = function () {
//    printElement(document.getElementById("printSchFee"));
//}

function printElement(elem) {

    var domClone = elem.cloneNode(true);
    var str = `<style> table {
                  font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                }

                td, th {
                  border: 1px solid #dddddd;
                  text-align: left;
                  padding: 8px;
                }

                tr:nth-child(even) {
                  background-color: #dddddd;
                }
                </style>`;
    var $printSection = document.getElementById("printSection");

    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }
    domClone.innerHTML = $.trim(str + domClone.innerHTML);
    $printSection.innerHTML = "";
    //$printSection.innerHTML = domClone.innerHTML;
    $printSection.appendChild(domClone);
    window.print();
}

function replaceComma(num) {
    return num.replace(/,/g, '');
};

function isNumber(evt, element) {

    var charCode = (evt.which) ? evt.which : event.keyCode

    if (

        (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // Check dot and only once.
        (charCode < 48 || charCode > 57))
        return false;

    return true;
}


function GetNavPaymentHistory() {

    var chk = $("#paymentHistory").data("isloadpayhistsect");
    if (chk == 0) {

        $.ajax({
            url: '/ParentCorner/Fee/GetPaymentHistoryView',
            type: "GET",
            dataType: 'html',
            success: function (response) {
                $("#paymentHistory").html("");
                $("#paymentHistory").html(response);
                InitDataTablePaymentHist();
                $("#paymentHistory").data("isloadpayhistsect", 1);
            },
            error: function () {
                globalFunctions.showErrorMessage(translatedResources.technicalError);
                return;
            }
        });
    }
}

function GetNavAccountStatement() {

    var chk = $("#accountStatement").data("isloadaccstatement");
    if (chk == 0) {

        $.ajax({
            url: '/ParentCorner/Fee/GetAccountStatementView',
            type: "GET",
            dataType: 'html',
            success: function (response) {
                $("#accountStatement").html("");
                $("#accountStatement").html(response);
                InitDataTable();
                $("#accountStatement").data("isloadaccstatement", 1);
            },
            error: function () {
                globalFunctions.showErrorMessage(translatedResources.technicalError);
                return;
            }
        });
    }
}



function GetStudentFeeDetails(feetype) {
    var feeTypeId = $("#" + feetype.toLowerCase() + "_div");
    if (feeTypeId.attr("data-isload") === "0") {
     
        $.ajax({
            url: '/ParentCorner/Fee/GetStudentFeeDetails?feeType=' + feetype,
            type: "GET",
            dataType: 'html',
            success: function (response) {
                feeTypeId.attr("data-isload", "1");
                if (feetype == "TRANSPORT") {
                    $("#transport_div").html('');
                    $("#transport_div").html(response);                    
                }
                else {
                    $("#activity_div").html('');
                    $("#activity_div").html(response);
                }
                NetPaymentSection(feetype.toLowerCase());
            },
            error: function (response) {
                console.log(response);
                globalFunctions.showErrorMessage("Error occured");
                return;
            }
        });
    }
}


