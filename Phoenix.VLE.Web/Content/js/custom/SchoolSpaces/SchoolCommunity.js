﻿
var _url = '', _data = {}, _userLognType = "", usr_Teacher = "teacher", usr_Parent = "parent", usr_Student = "student", usr_Admin = "Admin";
var schoolCommunity = (function ($, w) {
    var init = function () {
        _userLognType = $("#hdnLognUsrType").val();
        let qTab = globalFunctions.getParameterValueFromQueryString('tb');
        qTab = qTab === '' ? '1' : qTab;
        $(".communityTab").addClass("mb-2 mb-md-0");
        loadContent(qTab);
        $(document).on('click', '.communityTab', function () {
            $(".communityTab").removeClass("btn-primary");
            $(".communityTab").addClass("btn-outline-primary");
            // $(".communityTab").addClass("mb-2 mb-md-0");
            $(this).addClass("btn-primary");
            $(this).addClass("mb-2 mb-md-0");
            let tabNo = $(this).attr("data-tab");
            loadContent(tabNo);
        });
    },
        loadContent = function (tabNo) {
            $('.communityTab').removeClass('btn-primary');
            $('.communityTab').addClass('btn-outline-primary');
            var requestType = '';
            if (tabNo == '1') {
                $('#circular-Newsletter').addClass('btn-primary');
                $('#circular-Newsletter').removeClass('btn-outline-primary');
                //set calender dates
                var date = new Date(), last30Days = new Date(), y = date.getFullYear(), m = date.getMonth();
                last30Days = last30Days.setDate(last30Days.getDate() - 30);
                var sod = moment(last30Days).format('DD MMM YYYY');
                var eod = moment(date).format('DD MMM YYYY');
                $(".fromdate").val(sod);
                $(".todate").val(eod);
                _url = translatedResources.baseUrl + translatedResources.circularListAPI;
                _data = JSON.stringify({
                    Identifier: _userLognType == usr_Student ? translatedResources.studenNumber : translatedResources.userName,
                    Key: (_userLognType == usr_Teacher || _userLognType == usr_Admin) ? "staff" : _userLognType,
                    FromDate: sod,
                    ToDate: eod,
                    Language: "en"
                });
                requestType = 'Post';
            }
            else if (tabNo == '2') {
                requestType = 'Post';
                _url = translatedResources.baseUrl + translatedResources.resourcesAPI;
                _data = JSON.stringify({
                    ParentID: _userLognType == usr_Student ? translatedResources.parentUsername : _userLognType == usr_Parent ? translatedResources.userName : '',
                    Language: 'en',
                    Source: 'SCHOOL',
                    StaffID: (_userLognType == usr_Teacher || _userLognType == usr_Admin) ? translatedResources.userName : '',
                    IsStaff: (_userLognType == usr_Teacher || _userLognType == usr_Admin) ? '1' : '0'
                })
                $('#Resources').addClass('btn-primary');
                $('#Resources').removeClass('btn-outline-primary');
            }
            else if (tabNo == '3') {
                $('#school-event').addClass('btn-primary');
                $('#school-event').removeClass('btn-outline-primary');
                _url = translatedResources.baseUrl + translatedResources.GetEventCategoryAPI;
                requestType = 'Get';
            }
            else if (tabNo == '4') {

                $('#gallery').removeClass('btn-outline-primary');
                $('#gallery').addClass('btn-primary');
                _url = translatedResources.baseUrl + translatedResources.galleryAPI;
                var IsStaff = '1';
                var TeacherUsername = ''; var ParentUsername = ''; var StudentUsername = '';
                if (translatedResources.IsAdmin == "True" || translatedResources.IsTeacher == "True") {
                    IsStaff = '1';
                    ParentUsername = '';
                    StudentUsername = '';
                    TeacherUsername = translatedResources.userName;
                }
                else if (translatedResources.IsParent == "True") {
                    IsStaff = '0';
                    ParentUsername = translatedResources.userName;
                    StudentUsername = '';
                    TeacherUsername = '';
                }
                else if (translatedResources.IsStudent == "True") {
                    IsStaff = '0';
                    ParentUsername = translatedResources.ParentUsername;
                    StudentUsername = '';
                    TeacherUsername = '';
                }
                _data = JSON.stringify({ ParentID: ParentUsername, Language: 'en', Source: 'SCHOOL', StaffID: TeacherUsername, FileParentID: '', IsStaff: IsStaff, FilterText: '', ToDate: '', FromDate: '', });
                requestType = 'Post';
            }
            else {
            }
            $.ajax({
                url: _url,
                type: requestType,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": 'Bearer ' + translatedResources.phoenixAccessToken,
                },
                data: _data,
                success: function (result) {

                    if (tabNo == '1') {
                        cirularHTML.getCirularHTML(result.data);
                        let circularID = globalFunctions.getParameterValueFromQueryString('cID');
                        let redirected = localStorage.getItem("cID");
                        if (circularID !== '' && redirected === null) {
                            circularFunctions.getCircularDetails(circularID);
                            localStorage.setItem("cID", true);
                        }
                    }
                    else if (tabNo == '2') {
                        resourcesHTML.getResourcesHTML(result.data)
                    }
                    else if (tabNo == '3') {

                        eventHTML.getEventHTML(result.data);
                        eventHTML.loadCalendarControl();
                        eventHTML.setEventheaderText();
                        eventHTML.loadEvents();
                    }
                    else if (tabNo == '4') {
                        galleryHTML.getGalleryHTML(result.data);
                    }
                    else {
                    }
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure();
                }

            });
        };

    return {
        init: init
    }
})(jQuery, window);

$(function () { schoolCommunity.init() });


var eventDates = [];
var currentWeekStartDate = getSundayOfCurrentWeek(new Date());
var weekEndDate = new Date(currentWeekStartDate.getFullYear(), currentWeekStartDate.getMonth(), currentWeekStartDate.getDate() + 6);
var nextWeekStartDate = new Date(currentWeekStartDate.getFullYear(), currentWeekStartDate.getMonth(), currentWeekStartDate.getDate() + 7);
var nextWeekEndDate = new Date(nextWeekStartDate.getFullYear(), nextWeekStartDate.getMonth(), nextWeekStartDate.getDate() + 6);
function getSundayOfCurrentWeek(d) {
    var day = d.getDay();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate() + (day == 0 ? 0 : - day));
}
var eventHTML = function () {
    $("#communityContent").html("");
    getEventHTML = function (eventData) {

        var eventHTML1 = `<div class="row" >
                <div class="col-xl-8 col-lg-12 col-md-12 col-12 order-xl-first order-last">
                    <div class="card school-events-card bg-transparent border-0 py-0 pl-0">
                        <div class="events-header">
                            <div class="events-title topETitle arabictxt">

                            </div>
                            <div class="button-group">
                                <button type="button" class="btn btn-primary  btn-sm rounded-4x btn-today" data-eventcontainer="events-today">`+ translatedResources.Today + `</button>
                                <button type="button" class="btn btn-outline-primary btn-sm rounded-4x btn-week" data-eventcontainer="events-week">`+ translatedResources.Week + `</button>
                                <button type="button" class="btn btn-outline-primary btn-sm rounded-4x btn-month" data-eventcontainer="events-month">`+ translatedResources.Month + `</button>
                            </div>
                        </div>
                        <div class="events-content events-today">

                        </div>
                        <div class="events-content events-week d-none">

                        </div>
                        <div class="events-content events-month d-none">

                        </div>
                        <div class="events-content events-calendar d-none">

                        </div>
                    </div>
                    <div class="card school-events-card bg-transparent border-0 py-0 pl-0 upcoming-events">
                        <div class="events-header">
                            <div class="events-title">
                                `+ translatedResources.UpcomingEvents + `
                </div>
                        </div>
                        <div class="events-content upEvntCont">

                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-12 col-md-12 col-12 order-xl-last order-first">
                    <div class="row mb-3">
                        <div class="col-12 d-flex justify-content-end flex-column flex-md-row">
                            <div class="search-with-bg w-responsive bg-white rounded-6x border mr-0 mr-md-2">
                                <span class="icon-search"></span>
                                <input type="search" id="btnSearchFiles" class="searchfield" placeholder="`+ translatedResources.Search + `">
                </div>
                            <div class="md-form md-outline sort-dropdown category-dropdown mb-0 mt-3 mt-md-0 ml-0 ml-md-3">
                                <div class="multiselect-dropdown">`;
        var categorydropdown = '<select class="event-cat-dropdown selectpicker bg-white rounded-6x" data-dropdown-align-right="auto" data-selected-text-format="count>1" data-width="120px" id="ddlCategories" multiple="" name="Id" tabindex="-98" title="' + translatedResources.SelectCategory + '" style="display: block !important;">';
        var categoryoption = "";
        var categorylist = '';
        for (catid = 0; eventData.length > catid; catid++) {
            if (eventData[catid].categoryDescription != "") {
                if (categoryoption == "") {
                    categoryoption = '<option class="text-truncate" value=' + eventData[catid].categoryID + '>' + eventData[catid].categoryDescription + '</option>';
                    categorylist = '<li class="text-semibold text-truncate"> <span style="--category-color:' + eventData[catid].categoryColorCode + '"></span>' + eventData[catid].categoryDescription + '</li>';
                } else {
                    categoryoption = categoryoption + '<option class="text-truncate" value=' + eventData[catid].categoryID + '>' + eventData[catid].categoryDescription + '</option>';
                    categorylist = categorylist + '<li class="text-semibold text-truncate"> <span style="--category-color:' + eventData[catid].categoryColorCode + '"></span>' + eventData[catid].categoryDescription + '</li>';

                }
            }
        }

        categorydropdown = categorydropdown + categoryoption + "</select>";

        var eventHTML2 = `</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card column-padding sticky-top calendar-card bg-transparent border-0 p-0">
                                <div class="bg-white mb-3 rounded-2x overflow-hidden">
                                    <div class="inline-date-picker pb-3 arabictxt"></div>
                                </div>
                                <ul class="eventCatType d-flex flex-wrap">`;

        var eventHTML3 = `</ul>
                          </div>
                        </div>
                    </div>

                </div>
</div >`;

        var getEventHTML = eventHTML1 + categorydropdown + eventHTML2 + categorylist + eventHTML3;
        $("#communityContent").html(getEventHTML);
        $(".selectpicker").selectpicker('refresh');
    },
        getSundayOfCurrentWeek = function (d) {
            var day = d.getDay();
            return new Date(d.getFullYear(), d.getMonth(), d.getDate() + (day == 0 ? 0 : - day));
        },
        AddEventsCalendarIndication = function () {
            eventDates.map(function (date, i) {
                $("[data-day='" + date + "']:not(.today)").css("background-color", "#e9e1e1");
            });
        },
        loadCalendarControl = function () {
            $('.inline-date-picker').datetimepicker({
                format: 'MM/DD/YYYY',
                inline: true,
                locale: translatedResources.locale
            }).on('dp.change', function (e) {
                eventHTML.getEventsByCalendar("", "", new Date(e.date._d));
                eventHTML.AddEventsCalendarIndication();
                $(".day.active:not(.today)").css("background-color", "#337ab7");
            }).on("dp.update", function (e) {
                eventHTML.AddEventsCalendarIndication();
                $(".day.active:not(.today)").css("background-color", "#337ab7");
            });
        }
    Date.prototype.toShortFormat = function () {

        let monthNames = translatedResources.locale === 'en' ? ["Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec"] : ["يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
        let day = this.getDate();
        let monthIndex = this.getMonth();
        let monthName = monthNames[monthIndex];
        let year = this.getFullYear();
        return `${day}-${monthName}-${year}`;
    },
        GetFormattedDate = function (dt) {
            var month = dt.getMonth() + 1;
            var day = dt.getDate();
            var year = dt.getFullYear();
            return year + "-" + month + "-" + day;
        },
        setEventheaderText = function () {
        },
        getMonthNm = function (d) {
        var month = new Array();
        if (translatedResources.locale === 'en') {
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
        }
        else {
            month[0] = "يناير";
            month[1] = "فبراير";
            month[2] = "مارس";
            month[3] = "أبريل";
            month[4] = "مايو";
            month[5] = "يونيو";
            month[6] = "يوليو";
            month[7] = "أغسطس";
            month[8] = "سبتمبر";
            month[9] = "أكتوبر";
            month[10] = "نوفمبر";
            month[11] = "ديسمبر";
        }
            var n = month[d.getMonth()];
            return n;
        },
        formatAMPM = function (date) {
            let am = translatedResources.locale === 'en' ? 'am' : 'ص';
            let pm = translatedResources.locale === 'en' ? 'pm' : 'م';
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? pm : am;
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        },
        loadEvents = function () {

            var currentDate = new Date(), y = currentDate.getFullYear(), m = currentDate.getMonth();
            var monthStartDate = new Date(y, m, 1);
            var monthEndDate = new Date(y, m + 1, 0);
            var from = eventHTML.GetFormattedDate(monthStartDate);
            var to = eventHTML.GetFormattedDate(monthEndDate);
            var IsStaff = '1';
            var TeacherUsername = ''; var ParentUsername = ''; var StudentUsername = '';
            if (translatedResources.IsAdmin == "True" || translatedResources.IsTeacher == "True") {
                IsStaff = '1';
                ParentUsername = '';
                StudentUsername = '';
                TeacherUsername = translatedResources.userName;
            }
            else if (translatedResources.IsParent == "True") {
                IsStaff = '0';
                ParentUsername = translatedResources.userName;
                StudentUsername = translatedResources.StudnetIdForEvent;
                TeacherUsername = '';
            }
            else if (translatedResources.IsStudent == "True") {
                IsStaff = '0';
                ParentUsername = translatedResources.ParentUsername;
                StudentUsername = translatedResources.StudnetId;
                TeacherUsername = '';
            }
            $.ajax({
                url: translatedResources.baseUrl + translatedResources.eventsAPI,
                type: 'Post',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": 'Bearer ' + translatedResources.phoenixAccessToken,
                },
                data: JSON.stringify({
                    ParentID: ParentUsername,
                    Language: 'en',
                    Source: 'SCHOOL',
                    StudentID: StudentUsername,
                    StaffID: TeacherUsername,
                    IsDateRange: '1',
                    IsStaff: IsStaff,
                    ToDate: to,
                    FromDate: from
                }),
                success: function (data) {

                    var eventsList = data;
                    var eventArray = [];
                    var tod = "", wee = "", mon = "";
                    //var myDate = new Date(data.data.defaultDate.match(/\d+/)[0] * 1);
                    var strToday = "", strWeek = "", strMonth = "", strUpComEvent = "";
                    var today = new Date();
                    var lsDt = new Date(today.getFullYear(), today.getMonth() + 1, today.getDate());
                    tod = today.toShortFormat();
                    var currentWeekStartDate = getSundayOfCurrentWeek(new Date());
                    wee = currentWeekStartDate.toShortFormat();
                    mon = lsDt.toShortFormat();

                    var strDate = today.toShortFormat();
                    $.each(eventsList.data, function (i, obj) {

                        var stDt = new Date(obj.eventStartDT);
                        eventDates.push(moment(stDt).format("MM/DD/YYYY"));
                        $("[data-day='" + moment(stDt).format("MM/DD/YYYY") + "']").css("background-color", "#e9e1e1");
                        var sDay = stDt.getDate();

                        var isCurrentWeek = stDt.getTime() >= currentWeekStartDate.getTime() && stDt.getTime() <= weekEndDate.getTime();
                        var isToday = stDt.getDate() == currentDate.getDate();
                        var isNextWeekEvents = (stDt.getTime() >= nextWeekStartDate.getTime() && stDt.getDate() <= monthEndDate.getTime());
                        var nextWeekClass = isNextWeekEvents ? "nextWeekUpcomingEvents" : "later-weekevents";
                        var isUpComing = ((stDt.getDate() - currentDate.getDate() > 0) && (stDt.getDate() <= monthEndDate.getDate()));
                        var sMon = getMonthNm(stDt);
                        var tm = formatAMPM(stDt);

                        if (isUpComing) {
                            strUpComEvent += `<div class="events-category ${nextWeekClass} primary bg-white px-4" data-category='${obj.categoryID}' style="--category-color:` + obj.categoryColorCode + `">
                                                      <div class="events-time py-2 text-right">
                                                      <span class="date d-block text-bold font-16 text-primary arabictxt">`+ sDay + `</span>
                                                      <span class="month d-block text-semibold text-primary">`+ sMon + `</span>
                                                      <p class="text-semibold mb-0 text-primary arabictxt">`+ tm + `</p>
                                                      </div>
                                                      <div class="events-detail">
                                                      <div class="event-type d-flex align-items-center mt-0">
                                                      <span class="event-icon d-none align-items-center justify-content-center">
                                                      <img src="/Content/img/svg/calendar1.svg" class="svg" alt="event image">
                                                      </span>
                                                      <span class="event-name text-semibold p-0 text-black-50">`+ obj.categoryDescription + `</span>
                                                      </div>
                                                      <p class="event-description" id="event-description-${obj.categoryID}" data-categoryid="${obj.categoryID}">` + obj.eventDescription + `</p>
                                                      </div>
                                                      </div>`;
                        }
                        if (isToday) {
                            strToday += `<div class="events-category primary bg-white px-4" data-category='${obj.categoryID}'  style="--category-color:` + obj.categoryColorCode + `">
                                              <div class="events-time py-2 text-right">
                                              <span class="date d-block text-bold font-16 text-primary arabictxt">`+ sDay + `</span>
                                              <span class="month d-block text-semibold text-primary">`+ sMon + `</span>
                                              <p class="text-semibold mb-0 text-primary arabictxt">`+ tm + `</p>
                                              </div>
                                              <div class="events-detail">
                                              <div class="event-type d-flex align-items-center mt-0">
                                              <span class="event-icon d-none align-items-center justify-content-center">
                                              <img src="/Content/img/svg/calendar1.svg" class="svg" alt="event image">
                                              </span>
                                              <span class="event-name text-semibold p-0 text-black-50">`+ obj.categoryDescription + `</span>
                                              </div>
                                              <p class="event-description" id="event-description-${obj.categoryID}" data-categoryid="${obj.categoryID}">` + obj.eventDescription + `</p>
                                              </div>
                                              </div>`;
                        }
                        if (isCurrentWeek) {
                            strWeek += `<div class="events-category primary bg-white px-4" data-category='${obj.categoryID}' style="--category-color:` + obj.categoryColorCode + `">
                                            <div class="events-time py-2 text-right">
                                            <span class="date d-block text-bold font-16 text-primary arabictxt">`+ sDay + `</span>
                                            <span class="month d-block text-semibold text-primary">`+ sMon + `</span>
                                            <p class="text-semibold mb-0 text-primary arabictxt">`+ tm + `</p>
                                            </div>
                                            <div class="events-detail">
                                            <div class="event-type d-flex align-items-center mt-0">
                                            <span class="event-icon d-none align-items-center justify-content-center">
                                            <img src="/Content/img/svg/calendar1.svg" class="svg" alt="event image">
                                            </span>
                                            <span class="event-name text-semibold p-0 text-black-50">`+ obj.categoryDescription + `</span>
                                            </div>
                                            <p class="event-description" id="event-description-${obj.categoryID}" data-categoryid="${obj.categoryID}">` + obj.eventDescription + `</p>
                                            </div>
                                            </div>`;
                        }

                        strMonth += `<div class="events-category primary bg-white px-4" data-category='${obj.categoryID}' style="--category-color:` + obj.categoryColorCode + `">
                                         <div class="events-time py-2 text-right">
                                         <span class="date d-block text-bold font-16 text-primary arabictxt">`+ sDay + `</span>
                                         <span class="month d-block text-semibold text-primary">`+ sMon + `</span>
                                         <p class="text-semibold mb-0 text-primary arabictxt">`+ tm + `</p>
                                         </div>
                                         <div class="events-detail">
                                         <div class="event-type d-flex align-items-center mt-0">
                                         <span class="event-icon d-none align-items-center justify-content-center">
                                         <img src="/Content/img/svg/calendar1.svg" class="svg" alt="event image">
                                         </span>
                                         <span class="event-name text-semibold p-0 text-black-50">`+ obj.categoryDescription + `</span>
                                         </div>
                                         <p class="event-description" id="event-description-${obj.categoryID}" data-categoryid="${obj.categoryID}">` + obj.eventDescription + `</p>
                                         </div>
                                         </div>`;
                    });
                    //$(".topETitle").html("");
                    $(".day.today").css("background-color", "");
                    $(".upEvntCont").empty();
                    $(".events-today").html("");
                    $(".events-week").html("");
                    $(".events-month").html("");
                    var noDataHtml = "<span class='no-data-found text-bold d-block mx-auto my-auto'>" + translatedResources.noRecordsMessage + "</div>";
                    if (!globalFunctions.isValueValid(strUpComEvent))
                        strUpComEvent = noDataHtml;
                    if (!globalFunctions.isValueValid(strToday))
                        strToday = noDataHtml;
                    if (!globalFunctions.isValueValid(strWeek))
                        strWeek = noDataHtml;
                    if (!globalFunctions.isValueValid(strMonth))
                        strMonth = noDataHtml;
                    $(".topETitle").text(translatedResources.Eventson + " " + strDate.toString());
                    $(".upEvntCont").append(strUpComEvent);
                    $(".events-today").append(strToday);
                    $(".events-week").append(strWeek);
                    $(".events-month").append(strMonth);

                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure();
                }

            });
        },
        getEventsByCalendar = function (from, to, selectDate) {

            var IsStaff = '1';
            var TeacherUsername = ''; var ParentUsername = ''; var StudentUsername = '';
            if (translatedResources.IsAdmin == "True" || translatedResources.IsTeacher == "True") {
                IsStaff = '1';
                ParentUsername = '';
                StudentUsername = '';
                TeacherUsername = translatedResources.userName;
            }
            else if (translatedResources.IsParent == "True") {
                IsStaff = '0';
                ParentUsername = translatedResources.userName;
                StudentUsername = translatedResources.StudnetIdForEvent;
                TeacherUsername = '';
            }
            else if (translatedResources.IsStudent == "True") {
                IsStaff = '0';
                ParentUsername = translatedResources.ParentUsername;
                StudentUsername = translatedResources.StudnetId;
                TeacherUsername = '';
            }
            $.ajax({
                url: translatedResources.baseUrl + translatedResources.eventsAPI,
                type: 'Post',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": 'Bearer ' + translatedResources.phoenixAccessToken,
                },
                data: JSON.stringify({
                    ParentID: ParentUsername,
                    Language: 'en',
                    Source: 'SCHOOL',
                    StudentID: StudentUsername,
                    StaffID: TeacherUsername,
                    IsDateRange: '1',
                    IsStaff: IsStaff,
                    ToDate: eventHTML.GetFormattedDate(selectDate),
                    FromDate: eventHTML.GetFormattedDate(selectDate)
                }),
                success: function (data, status, xhr) {   // success callback function
                    var eventsList = data;
                    var strCalEvent = "";
                    var today = new Date();
                    if (eventsList == null || eventsList.data == null || eventsList.data.length == 0) {
                        strCalEvent = "<span class='no-data-found text-bold d-block mx-auto my-auto'>" + translatedResources.noRecordsMessageForEvent + "</div>";
                    }
                    else {
                        $.each(eventsList.data, function (i, obj) {
                            var stDt = new Date(obj.eventStartDT);
                            var sDay = stDt.getDate();
                            var sMon = getMonthNm(stDt);
                            var tm = formatAMPM(stDt);

                            strCalEvent += `<div class="events-category primary bg-white px-4" data-category='${obj.categoryID}' style="--category-color:` + obj.categoryColorCode + `">
                                                 <div class="events-time py-2 text-right">
                                                 <span class="date d-block text-bold font-16 text-primary arabictxt">`+ sDay + `</span>
                                                 <span class="month d-block text-semibold text-primary">`+ sMon + `</span>
                                                 <p class="text-semibold mb-0 text-primary arabictxt">`+ tm + `</p>
                                                 </div>
                                                 <div class="events-detail">
                                                 <div class="event-type d-flex align-items-center mt-0">
                                                 <span class="event-icon d-none align-items-center justify-content-center">
                                                 <img src="`+ obj.categoryThumbNailUrl + `" alt="calendar" style="width:25px;height:25px;">
                                                 </span>
                                                 <span class="event-name text-semibold p-0 text-black-50">`+ obj.categoryDescription + `</span>
                                                 </div>
                                                 <p class="event-description" id="event-description-${obj.categoryID}" data-categoryid="${obj.categoryID}">` + obj.eventDescription + `</p>
                                                 </div>
                                                 </div>`;
                        });
                    }
                    //$("div.button-group button").removeClass("btn-primary").addClass("btn-light");

                    $("div.button-group button").removeClass("btn-primary").removeClass("btn-outline-primary").addClass("btn-outline-primary");
                    $(".events-content:not(.events-calendar), .upEvntCont").addClass("d-none");

                    $(".events-calendar").html("").removeClass("d-none");
                    $(".topETitle").text(translatedResources.Eventson + " " + selectDate.toShortFormat());
                    $(".events-calendar").append(strCalEvent);
                },
                error: function (jqXhr, textStatus, errorMessage) { // error callback
                }
            });
        }
    return {
        getEventHTML: getEventHTML,
        loadCalendarControl: loadCalendarControl,
        setEventheaderText: setEventheaderText,
        loadEvents: loadEvents,
        getMonthNm: getMonthNm,
        formatAMPM: formatAMPM,
        getEventsByCalendar: getEventsByCalendar,
        AddEventsCalendarIndication: AddEventsCalendarIndication,
        GetFormattedDate: GetFormattedDate
    }
}();
var currentDate = new Date(), y = currentDate.getFullYear(), m = currentDate.getMonth();
var monthStartDate = new Date(y, m, 1);
var monthEndDate = new Date(y, m + 1, 0);
$('.events-header .events-title').text(monthStartDate.toShortFormat() + " to " + monthEndDate.toShortFormat());
$('.btn-week').removeClass('btn-primary');
$('.btn-today').removeClass('btn-primary');
$('.btn-week').addClass('btn-outline-primary');
$('.btn-today').addClass('btn-outline-primary');
$('.btn-month').removeClass('btn-outline-primary');
$('.btn-month').addClass('btn-primary');
$('.events-month').removeClass('d-none');
$('.events-month').removeClass('d-none');
$('.events-week').addClass('d-none');
$('.upcoming-events').addClass('d-none');
$('.events-today').addClass('d-none');
$('.events-calendar').addClass('d-none');
$(document).on('click', '.events-header .btn-week', function () {
    selectedTab = 'events-week';
    filterEvents();
    $(".topETitle").text(currentWeekStartDate.toShortFormat() + " " + translatedResources.To+" " + weekEndDate.toShortFormat());
    $('.upcoming-events .events-header .events-title').text(translatedResources.UpcomingEvents);
    $('.btn-today').removeClass('btn-primary');
    $('.btn-month').removeClass('btn-primary');
    $('.btn-today').addClass('btn-outline-primary');
    $('.btn-month').addClass('btn-outline-primary');
    $('.btn-week').removeClass('btn-outline-primary');
    $('.btn-week').addClass('btn-primary');
    $('.events-week').removeClass('d-none');
    $('.upcoming-events,.upEvntCont').removeClass('d-none');
    $('.events-today, .later-weekevents').addClass('d-none');
    $('.events-month').addClass('d-none');
    $('.events-calendar').addClass('d-none');
})
$(document).on('click', '.events-header .btn-today', function () {
    selectedTab = 'events-today';
    filterEvents();
    $('.events-header .events-title').text(translatedResources.Eventson + " " + currentDate.toShortFormat());
    $('.upcoming-events .events-header .events-title').text(translatedResources.UpcomingEvents);
    $('.btn-week').removeClass('btn-primary');
    $('.btn-month').removeClass('btn-primary');
    $('.btn-week').addClass('btn-outline-primary');
    $('.btn-month').addClass('btn-outline-primary');
    $('.btn-today').removeClass('btn-outline-primary');
    $('.btn-today').addClass('btn-primary');
    $('.upcoming-events, .upEvntCont, .later-weekevents').removeClass('d-none');
    $('.events-today').removeClass('d-none');
    $('.events-week').addClass('d-none');
    $('.events-month').addClass('d-none');
    $('.events-calendar').addClass('d-none');
})
$(document).on('click', '.events-header .btn-month', function () {
    selectedTab = 'events-month';
    filterEvents();
    $('.events-header .events-title').text(monthStartDate.toShortFormat() + " to " + monthEndDate.toShortFormat());
    $('.btn-week').removeClass('btn-primary');
    $('.btn-today').removeClass('btn-primary');
    $('.btn-week').addClass('btn-outline-primary');
    $('.btn-today').addClass('btn-outline-primary');
    $('.btn-month').removeClass('btn-outline-primary');
    $('.btn-month').addClass('btn-primary');
    $('.events-month').removeClass('d-none');
    $('.events-week').addClass('d-none');
    $('.upcoming-events').addClass('d-none');
    $('.events-today').addClass('d-none');
    $('.events-calendar').addClass('d-none');
});
$(document).on("change", "#ddlCategories", function () {
    filterEvents();
});
$(document).on("keyup", "#btnSearchFiles", function () {
    filterEvents();
});
var selectedTab = '';
function filterEvents() {

    var source = $(".events-category"), arList = $("#ddlCategories").val();
    var value = $("#btnSearchFiles").val().toLowerCase();
    source.filter(function () {
        var categoryId = $(this).data("category");
        $(this).toggle(($(this).text().toLowerCase().indexOf(value) > -1 && (arList.length == 0 || arList.includes(categoryId.toString()))));
    });

    //events-week
    //events-month
    //events-today
    var activeCategory = '';
    if (selectedTab == '')
        activeCategory = $(".button-group button.btn-primary").data("eventcontainer");
    else
        activeCategory = selectedTab;

    if ($("." + activeCategory + " .events-category:visible").length == 0) {
        if ($("." + activeCategory + " span.no-data-found").length == 0)
            $("." + activeCategory).append("<span class='no-data-found text-bold d-block mx-auto my-auto'>" + translatedResources.noRecordsMessageForEvent + "</div>");
    } else
        $("." + activeCategory + " span.no-data-found").remove();

    if (activeCategory == "events-today") {
        if ($(".upcoming-events .events-category:visible").length == 0) {
            if ($(".upcoming-events span.no-data-found").length == 0)
                $(".upcoming-events").append("<span class='no-data-found text-bold d-block mx-auto my-auto'>" + translatedResources.noRecordsMessageForEvent + "</div>");
        }
        else {
            $(".upcoming-events span.no-data-found").remove();
        }
    }
    selectedTab = '';
}

var galleryHTML = function () {
    getGalleryHTML = function (GalleryData) {
        var galleryFiles = undefined;
        var galleryFolders = GalleryData;
        var galleryFilesItemCount = [];
        var files = undefined;
        var files1 = '';
        var files2 = '';

        var monthArray = new Array();
        monthArray[1] = "January";
        monthArray[2] = "February";
        monthArray[3] = "March";
        monthArray[4] = "April";
        monthArray[5] = "May";
        monthArray[6] = "June";
        monthArray[7] = "July";
        monthArray[8] = "August";
        monthArray[9] = "September";
        monthArray[10] = "October";
        monthArray[11] = "November";
        monthArray[12] = "December";

        _url = translatedResources.baseUrl + translatedResources.galleryAPI;
        var IsStaff = '1';
        var TeacherUsername = ''; var ParentUsername = ''; var StudentUsername = '';
        if (translatedResources.IsAdmin == "True" || translatedResources.IsTeacher == "True") {
            IsStaff = '1';
            ParentUsername = '';
            StudentUsername = '';
            TeacherUsername = translatedResources.userName;
        }
        else if (translatedResources.IsParent == "True") {
            IsStaff = '0';
            ParentUsername = translatedResources.userName;
            StudentUsername = '';
            TeacherUsername = '';
        }
        else if (translatedResources.IsStudent == "True") {
            IsStaff = '0';
            ParentUsername = translatedResources.ParentUsername;
            StudentUsername = '';
            TeacherUsername = '';
        }
        requestType = 'Post';

        if (galleryFolders != null) {
            if (galleryFolders.length > 0) {
                var FileID = galleryFolders[0].fileID;
                _data = JSON.stringify({ ParentID: ParentUsername, Language: 'en', Source: 'SCHOOL', StaffID: TeacherUsername, FileParentID: FileID, IsStaff: IsStaff, FilterText: '', ToDate: '', FromDate: '', });
                $.ajax({
                    url: _url,
                    type: requestType,
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": 'Bearer ' + translatedResources.phoenixAccessToken,
                    },
                    data: _data,
                    success: function (result) {

                        galleryFiles = result.data;

                        files = $.grep(galleryFiles, function (n, i) {
                            return n.isDirectory === false;
                        });
                        if (files.length > 0) {
                            var file = files[0];
                            files1 = "<img class='img-fluid rounded' src='" + file.filePath + "' alt='" + file.filePath + "'>";
                        }

                        if (files.length > 0) {
                            for (i = 1; files.length > i; i++) {
                                var file = files[i];
                                var url = file.filePath; var resourceUrl = ""; var cssClass = "";
                                var filename = file.fileName.trim() + file.fileExtention;

                                if (filename.toLowerCase().includes("jpg") || filename.toLowerCase().includes("jpeg") || filename.toLowerCase().includes("png") || filename.toLowerCase().includes("bmp") || filename.toLowerCase().includes("gif") ||
                                    filename.toLowerCase().includes("mp4") || filename.toLowerCase().includes("3gp") || filename.toLowerCase().includes("flv") || filename.toLowerCase().includes("avi") || filename.toLowerCase().includes("mov") || filename.toLowerCase().includes("mkv")) {
                                    cssClass = "item-img";
                                }

                                if (filename.toLowerCase().includes("jpg") || filename.toLowerCase().includes("jpeg") || filename.toLowerCase().includes("png") || filename.toLowerCase().includes("bmp") || filename.toLowerCase().includes("gif")) {
                                    files2 = files2 + "<div class='col mb-3 px-2'> <a href='" + url + "' class='d-flex position-relative rounded overflow-hidden hoverable galleryImg item-img' data-sub-html='" + filename + "'> <img src='" + url + "' class='img-fluid'> </a></div>";
                                } else if (filename.toLowerCase().includes("mp4") || filename.toLowerCase().includes("3gp") || filename.toLowerCase().includes("flv") || filename.toLowerCase().includes("avi") || filename.toLowerCase().includes("mov") || filename.toLowerCase().includes("mkv")) {
                                    var imgUrl = translatedResources.ReadFilePath + resourceUrl;
                                    files2 = files2 + "<div class='col mb-3 px-2'>" +
                                        "<a href='' class='d-flex position-relative rounded overflow-hidden hoverable video galleryImg' data-html='#" + file.fileID + "' data-poster='~/Content/img/video-thumb.jpg'>" +
                                        "<img src='../../../Content/img/video-thumb.jpg' class='img-fluid'>" +
                                        "</a>" +
                                        "<div style='display: none;' id='" + file.fileID + "'>" +
                                        "<video class='lg-video-object lg-html5' controls='' preload='none'>" +
                                        "<source src='" + file.filePath + "' type='video/mp4'>" +
                                        'unsupported' +
                                        "</video>" +
                                        "</div>" +
                                        "</div>"
                                } else {
                                    files2 = files2 + "<div class='col mb-3 px-2'>" +
                                        "<a href='" + url + "' class='d-flex position-relative rounded overflow-hidden hoverable'>" +
                                        "<img class='img-fluid' src='" + url + "' alt='" + file.fileName + "'>" +
                                        "</a>" +
                                        "</div>"
                                }
                            }
                        }
                        else {
                            files1 = "<div>" +
                                "<span class='sub-heading d-block'>" + translatedResources.noRecordsMessageForGalley + "</span>" +
                                "</div>";
                        }

                        var getGalleryHTML = galleryHTML1 + files1 + galleryHTML2 + files2 + galleryHTML3 + folderssub1 + folderssub2 + galleryHTML4 + galleryHTML5;
                        $("#communityContent").html(getGalleryHTML);
                        loadlightGallery();
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure();
                    }
                });
            }
            else {
                files1 = "<div>" +
                    "<span class='no-data-found text-bold d-block mx-auto my-auto'>" + translatedResources.noRecordsMessageForGalley + "</span>" +
                    "</div >";
            }
        } else {
            files1 = "<div >" +
                "<span class='no-data-found text-bold d-block mx-auto my-auto'>" + translatedResources.noRecordsMessageForGalley + "</span>" +
                "</div >";
        }

        var folders = null;
        if (galleryFolders != null) {
            folders = $.grep(galleryFolders, function (n, i) {
                return n.isDirectory === true;
            });
        }

        var folderssub2 = "";
        var foldercounter = 0;
        if (folders != null) {
            if (folders.length > 0) {
                for (var i = 0; i < folders.length; i++) {
                    var fparen
                    tId = folders[i].fileID;
                    var item = folders[i];
                    item = folders[foldercounter];
                    var dt = new Date(item.fileCreatedDT);
                    var month = dt.getMonth() + 1;
                    var day = dt.getDate();
                    var year = dt.getFullYear();
                    var fileCreatedDate = day + " " + monthArray[month] + " " + year;

                    folderssub2 = folderssub2 +
                        "<a data-foldername=" + item.fileName + " href='#' data-noofrecord=" + item.filesCount + " data-fileid=" + item.fileID + " data-filename='" + item.fileName + "' class='d-block overflow-hidden position-relative rounded-2x mb-3 folderthumbnail'> <img class='img-fluid rounded' src='" + item.filePath + "' alt='Classroom'> <h2 class='m-0 text-white text-center text-bold position-absolute'>" + item.fileName + "</h2> <span class='items position-absolute text-bold'>" + item.filesCount + " " + translatedResources.Items + "</span> <span class='date position-absolute text-bold'>" + fileCreatedDate + "</span> </a>";
                    $("#communityContent").html('');
                    foldercounter = foldercounter + 1;

                }
            } else {
                folderssub2 = ' <hr class="border-default"> <div id="norecordindicator"><span>' + translatedResources.noRecordsMessageForGalleyFolderSelection + '</span></div>';
            }
        } else {
            folderssub2 = '  <hr class="border-default"> <div id="norecordindicator"><span>' + translatedResources.noRecordsMessageForGalleyFolderSelection + '</span></div>';
        }

        var galleryHTML1 = "<div class='row'>" +
            "<div class='col-lg-12 col-md-12 col-12'>" +
            "<div class='row gallery-sec'>" +
            "<div class='col-xl-9 col-lg-8 col-md-9 col-12 gallery-left'>" +
            "<div class='row mb-4'>" +
            "<div class='col-lg-12 col-md-12 col-12 px-2'>" +
            "<div class='gallery-main-slide bg-white text-center rounded'>";

        var galleryHTML2 = "</div>" +
            "</div>" +
            "</div>" +
            "<div class='row row-cols-2 row-cols-sm-2 row-cols-md-3 row-cols-lg-5 gallery-list-wrap' id='lightgallery'>";

        var galleryHTML3 = "</div>" +
            "</div>" +
            "<div class='col-xl-3 col-lg-4 col-md-3 col-12 gallery-right mt-4 mt-md-0'>" +
            "<div class='gal-dark-bg column-padding pb-3 pt-0 position-relative'>" +
            "<div class='row search-row mb-2'>" +
            "<div class='col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9'>" +
            "<div class='search-with-bg'>" +
            "<span class='icon-search'></span>" +
            "<input type='search' id='searchFolder' class='searchfield text-truncate' placeholder='" + translatedResources.Search + "'>" +
            "</div>" +
            "</div>" +
            "<div class='col-3 col-lg-3 col-md-3 col-sm-3 col-xl-3 mt-2 mt-xl-0'>" +
            //"<div class='md-form md-outline date-time-picker field-with-icon bg-white mb-1 mb-sm-0'>" +
            //"<i class='far fa-calendar-alt active'></i>" +
            //"<input type='text' class='form-control date-picker datepicker fromdate' placeholder='From Date' id='selectDate'>"+
            //"</div>"+
            "<span class='d-flex icon-calendar rounded'><img src='../../../Content/Vle/img/svg/calendar.svg'></span>" +
            "</div>" +
            "</div>" +
            "<div id='GalleryFolderSectionContainer'>";

        var folderssub1 = "<div class='row'>" +
            "<div class='col-lg-12 col-md-12 col-12'>" +
            "<div class='gallery-folder' id='folderthumbnail'>";


        var galleryHTML4 = "</div>" +
            "</div>" +
            "</div>";

        var galleryHTML5 = "</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>";

        var getGalleryHTML = galleryHTML1 + files1 + galleryHTML2 + files2 + galleryHTML3 + folderssub1 + folderssub2 + galleryHTML4 + galleryHTML5;
        $("#communityContent").html(getGalleryHTML);
        loadlightGallery();

    }
    return {
        getGalleryHTML: getGalleryHTML,
    }
}();

$(document).on('keyup', '#searchFolder', function () {
    var value = $(this).val().toLowerCase();
    var anchorlist = document.getElementById('folderthumbnail').getElementsByTagName('a');
    var istagmatched = false;

    if (document.getElementById('norecordindicator') != null) {
        $('#norecordindicator').remove();
    }

    for (anchorcounter = 0; anchorlist.length > anchorcounter; anchorcounter++) {
        var anchor = anchorlist[anchorcounter];
        var foldername = anchor.getAttribute('data-foldername');
        if (value == "") {
            anchorlist[anchorcounter].setAttribute('style', 'display:block !important');
            istagmatched = true;
        }
        else if (foldername.toLowerCase().includes(value)) {
            anchorlist[anchorcounter].setAttribute('style', 'display:block !important');
            istagmatched = true;
        }
        else {
            anchorlist[anchorcounter].setAttribute('style', 'display:none !important');
        }
    }
    if (istagmatched == false) {
        $('#folderthumbnail').append('<div id="norecordindicator"><span>' + translatedResources.noRecordsMessageForGalleyFolderSelection + '</span></div>');
    }
});

$(document).on('click', '.folderthumbnail', function () {
    var fileName = $(this).data('filename');
    var fileID = $(this).data('fileid');
    var noOfRecord = $(this).data('noofrecord');
    var files1 = ''; var files2 = '';

    var IsStaff = '1';
    var TeacherUsername = ''; var ParentUsername = ''; var StudentUsername = '';
    if (translatedResources.IsAdmin == "True" || translatedResources.IsTeacher == "True") {
        IsStaff = '1';
        ParentUsername = '';
        StudentUsername = '';
        TeacherUsername = translatedResources.userName;
    }
    else if (translatedResources.IsParent == "True") {
        IsStaff = '0';
        ParentUsername = translatedResources.userName;
        StudentUsername = '';
        TeacherUsername = '';
    }
    else if (translatedResources.IsStudent == "True") {
        IsStaff = '0';
        ParentUsername = translatedResources.ParentUsername;
        StudentUsername = '';
        TeacherUsername = '';
    }

    $('.gallery-left').html('');
    var galleryHTML1 = "<div class='row mb-4'>" +
        "<div class='col-lg-12 col-md-12 col-12 px-2'>" +
        "<div class='gallery-main-slide bg-white text-center rounded'>";

    if (noOfRecord > 0) {
        _data = JSON.stringify({ ParentID: ParentUsername, Language: 'en', Source: 'SCHOOL', StaffID: TeacherUsername, FileParentID: fileID, IsStaff: IsStaff, FilterText: '', ToDate: '', FromDate: '', });
        $.ajax({
            url: _url,
            type: requestType,
            headers: {
                "Content-Type": "application/json",
                "Authorization": 'Bearer ' + translatedResources.phoenixAccessToken,
            },
            data: _data,
            success: function (result) {
                galleryFiles = result.data;


                files = $.grep(galleryFiles, function (n, i) {
                    return n.isDirectory === false;
                });
                if (files.length > 0) {
                    var file = files[0];
                    files1 = "<img class='img-fluid rounded' src='" + file.filePath + "' alt='" + file.filePath + "'>";
                }

                if (files.length > 0) {
                    for (i = 1; files.length > i; i++) {
                        var file = files[i];
                        var url = file.filePath; var resourceUrl = ""; var cssClass = "";
                        var filename = file.fileName.trim() + file.fileExtention;

                        if (filename.toLowerCase().includes("jpg") || filename.toLowerCase().includes("jpeg") || filename.toLowerCase().includes("png") || filename.toLowerCase().includes("bmp") || filename.toLowerCase().includes("gif") ||
                            filename.toLowerCase().includes("mp4") || filename.toLowerCase().includes("3gp") || filename.toLowerCase().includes("flv") || filename.toLowerCase().includes("avi") || filename.toLowerCase().includes("mov") || filename.toLowerCase().includes("mkv")) {
                            cssClass = "item-img";
                        }

                        if (filename.toLowerCase().includes("jpg") || filename.toLowerCase().includes("jpeg") || filename.toLowerCase().includes("png") || filename.toLowerCase().includes("bmp") || filename.toLowerCase().includes("gif")) {
                            files2 = files2 + "<div class='col mb-3 px-2'> <a href='" + url + "' class='d-flex position-relative rounded overflow-hidden hoverable galleryImg item-img' data-sub-html='" + filename + "'> <img src='" + url + "' class='img-fluid'> </a></div>";
                        } else if (filename.toLowerCase().includes("mp4") || filename.toLowerCase().includes("3gp") || filename.toLowerCase().includes("flv") || filename.toLowerCase().includes("avi") || filename.toLowerCase().includes("mov") || filename.toLowerCase().includes("mkv")) {
                            var imgUrl = translatedResources.ReadFilePath + resourceUrl;
                            files2 = files2 + "<div class='col mb-3 px-2'>" +
                                "<a href='' class='d-flex position-relative rounded overflow-hidden hoverable video galleryImg' data-html='#" + file.fileID + "' data-poster='../../../Content/img/video-thumb.jpg'>" +
                                "<img src='../../../Content/img/video-thumb.jpg' class='img-fluid'>" +
                                "</a>" +
                                "<div style='display: none;' id='" + file.fileID + "'>" +
                                "<video class='lg-video-object lg-html5' controls='' preload='none'>" +
                                "<source src='" + file.filePath + "' type='video/mp4'>" +
                                'unsupported' +
                                "</video>" +
                                "</div>" +
                                "</div>"
                        } else {
                            files2 = files2 + "<div class='col mb-3 px-2'>" +
                                "<a href='" + url + "' class='d-flex position-relative rounded overflow-hidden hoverable'>" +
                                "<img class='img-fluid' src='" + url + "' alt='" + file.fileName + "'>" +
                                "</a>" +
                                "</div>"
                        }
                    }
                }
                else {
                    files2 = files2 + "<div >" +
                        "<span class='no-data-found text-bold d-block mx-auto my-auto'>" + translatedResources.noRecordsMessageForGalley + "</span>" +
                        "</div>";
                }

                var getGalleryHTML = galleryHTML1 + files1 + galleryHTML2 + files2;
                $(".gallery-left").html(getGalleryHTML);
                loadlightGallery();
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure();
            }
        });
    } else {
        files1 = "<div >" +
            "<span class='no-data-found text-bold d-block mx-auto my-auto' >" + translatedResources.noRecordsMessageForGalley + "</span>" +
            "</div >";
    }

    var galleryHTML2 = "</div>" +
        "</div>" +
        "</div>" +
        "<div class='row row-cols-2 row-cols-sm-2 row-cols-md-3 row-cols-lg-5 gallery-list-wrap' id='lightgallery'>";

    var getGalleryHTML = galleryHTML1 + files1 + galleryHTML2 + files2;
    $(".gallery-left").html(getGalleryHTML);
    loadlightGallery();
})


function loadlightGallery() {

    if (document.getElementById('lightgallery') != null) {

        $('#lightgallery').lightGallery({
            selector: '.galleryImg',
            share: false
        });
    }
}