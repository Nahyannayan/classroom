﻿var pixie;

var certificateFunctions = function () {
    var currentdate = new Date();
    var addCertificateFields = function (source, fieldName) {
        pixie.getTool('text').add(fieldName);
    },
        initImageEditor = function () {
            pixie = new Pixie({
                crossOrigin: true,
                ui: {
                    visible: true,
                    theme: 'light',
                    allowEditorClose: false,
                    allowZoom: true,
                    toolbar: {
                        hideSaveButton: false,
                        hide: false,
                        hideOpenButton: false,
                        hideCloseButton: true,
                    },
                },
                baseUrl: baseUri,
                onLoad: function () {
                    /* If image is not loaded properly, then call below logic */
                    //var activeObject = pixie.get('activeObject');
                    //var canW = activeObject.canvasState.original.width;
                    //var canH = activeObject.canvasState.original.height;
                    //console.log("canW: " + canW + " | canH: " + canH);
                    //setTimeout(function () {
                    //    var resizeTool = pixie.getTool('resize');
                    //    resizeTool.apply(canW, canH);
                    //}, 2000);
                },
                onSave: function (data, name) {
                    $.post('/Document/CertificateBuilder/SaveCertificate/', { fileName: name, data: data, templateId: $("#TemplateId").val() }, function (data) {
                        globalFunctions.showMessage(data.NotificationType, data.Message);
                        if (data.Success)
                            location.href = "/Document/CertificateBuilder";
                    }).fail(function () {
                        globalFunctions.OnFailure();
                    });
                },
                image: imagePath,
                tools: {
                    export: {
                        defaultFormat: 'json', //png, jpeg or json
                        defaultName: "image" + currentdate.getDate() +
                            + (currentdate.getMonth() + 1) +
                            + currentdate.getFullYear() +
                            + currentdate.getHours() +
                            + currentdate.getMinutes() +
                            + currentdate.getSeconds(), //default name for downloaded photo file
                        defaultQuality: 0.8, //works with jpeg only, 0 to 1
                    },
                    text: {
                        replaceDefault: true,
                        defaultCategory: 'handwriting',
                        items: [
                            {
                                family: 'Roboto',
                                category: 'serif',
                                filePath: '/Roboto/Roboto-Medium.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Bad Script',
                                category: 'handwriting',
                                filePath: '/Handwriting/Bad_script/BadScript-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Amatic SC',
                                category: 'handwriting',
                                filePath: '/Handwriting/Amatic_SC/AmaticSC-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Bonbon',
                                category: 'handwriting',
                                filePath: '/Handwriting/Bonbon/Bonbon-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Caveat',
                                category: 'handwriting',
                                filePath: '/Handwriting/Caveat/Caveat-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Cookie',
                                category: 'handwriting',
                                filePath: '/Handwriting/Cookie/Cookie-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Courgette',
                                category: 'handwriting',
                                filePath: '/Handwriting/Courgette/Courgette-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Great Vibes',
                                category: 'handwriting',
                                filePath: '/Handwriting/Great_Vibes/GreatVibes-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Handlee Regular',
                                category: 'handwriting',
                                filePath: '/Handwriting/Handlee/Handlee-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Indie Flower',
                                category: 'handwriting',
                                filePath: '/Handwriting/Indie_Flower/IndieFlower-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Kaushan Script',
                                category: 'handwriting',
                                filePath: '/Handwriting/Kaushan_Script/KaushanScript-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Mr Bedfort',
                                category: 'handwriting',
                                filePath: '/Handwriting/MrBedfort/Mr_Bedfort-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Norican',
                                category: 'handwriting',
                                filePath: '/Handwriting/Norican/Norican-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Pacifico',
                                category: 'handwriting',
                                filePath: '/Handwriting/Pacifico/Pacifico-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Parisienne',
                                category: 'handwriting',
                                filePath: '/Handwriting/Parisienne/Parisienne-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Permanent Marker',
                                category: 'handwriting',
                                filePath: '/Handwriting/Permanent_Marker/PermanentMarker-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Rochester',
                                category: 'handwriting',
                                filePath: '/Handwriting/Rochester/Rochester-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Rouge Script',
                                category: 'handwriting',
                                filePath: '/Handwriting/Rouge_Script/RougeScript-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Sacramento',
                                category: 'handwriting',
                                filePath: '/Handwriting/Sacramento/Sacramento-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Satisfy',
                                category: 'handwriting',
                                filePath: '/Handwriting/Satisfy/Satisfy-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Tangerine',
                                category: 'handwriting',
                                filePath: '/Handwriting/Tangerine/Tangerine-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Cormorant Garamond',
                                category: 'serif',
                                filePath: '/Serif/Cormorant_Garamond/CormorantGaramond-Medium.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Alegreya SC',
                                category: 'serif',
                                filePath: '/Serif/Alegreya_SC/AlegreyaSC-Medium.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Cambo',
                                category: 'serif',
                                filePath: '/Serif/Cambo/Cambo-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Caudex', 
                                category: 'serif',
                                filePath: '/Serif/Caudex/Caudex-Italic.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Cinzel',
                                category: 'serif',
                                filePath: '/Serif/Cinzel/Cinzel-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Arima Madurai',
                                category: 'Display',
                                filePath: '/Display/Arima_Madurai/ArimaMadurai-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Baloo 2',
                                category: 'Display',
                                filePath: '/Display/Baloo_2/Baloo2-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Bangers',
                                category: 'Display',
                                filePath: '/Display/Bangers/Bangers-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Bebas Neue',
                                category: 'Display',
                                filePath: '/Display/Bebas_Neue/BebasNeue-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Bellota',
                                category: 'Display',
                                filePath: '/Display/Bellota/Bellota-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Anonymous Pro',
                                category: 'monospace',
                                filePath: '/monospace//Anonymous_Pro/AnonymousPro-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'B612 Mono',
                                category: 'monospace',
                                filePath: '/monospace/B612_Mono/B612Mono-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Courier Prime',
                                category: 'monospace',
                                filePath: '/monospace//Courier_Prime/CourierPrime-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Cousine',
                                category: 'monospace',
                                filePath: '/monospace//Cousine/Cousine-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Fira Code',
                                category: 'monospace',
                                filePath: '/monospace//Fira_Code/FiraCode-VariableFont_wght.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Fira Mono',
                                category: 'monospace',
                                filePath: '/monospace//Fira_Mono/FiraMono-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'IBM Plex Mono',
                                category: 'monospace',
                                filePath: '/monospace//IBM_Plex_Mono/IBMPlexMono-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Inconsolata',
                                category: 'monospace',
                                filePath: '/monospace//Inconsolata/Inconsolata-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Major Mono Display',
                                category: 'monospace',
                                filePath: '/monospace//Major_Mono_Display/MajorMonoDisplay-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'MNanum Gothic Coding',
                                category: 'monospace',
                                filePath: '/monospace//Nanum_Gothic_Coding/NanumGothicCoding-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Nova Mono',
                                category: 'monospace',
                                filePath: '/monospace//Nova_Mono/NovaMono-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Overpass Mono',
                                category: 'monospace',
                                filePath: '/monospace//Overpass_Mono/OverpassMono-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Acme',
                                category: 'sans-serif',
                                filePath: '/sansserif//Acme/Acme-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Archivo Narrow',
                                category: 'sans-serif',
                                filePath: '/sansserif//Archivo_Narrow/ArchivoNarrow-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Barlow Condensed',
                                category: 'sans-serif',
                                filePath: '/sansserif//Barlow_Condensed/BarlowCondensed-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Exo 2',
                                category: 'sans-serif',
                                filePath: '/sansserif//Exo2/Exo2-Regular.ttf',
                                type: 'custom'
                            },
                            {
                                family: 'Fira Sans Condensed',
                                category: 'sans-serif',
                                filePath: '/sansserif//Fira_Sans_Condensed/FiraSansCondensed-Regular.ttf',
                                type: 'custom'
                            }
                        ]
                    }
                }
            });
        };

    return {
        addCertificateFields: addCertificateFields,
        initImageEditor: initImageEditor
    };
}();

$(function () {
    $("#btnBack").show();
    certificateFunctions.initImageEditor();

    $(document).on('click', '#btnBack', function () {
        location.href = "/document/certificateBuilder";
    });

    $(document).on("keypress", 'textarea', function (e) {
        var isNewField = checkForNewFields(e.target);
        if (!isNewField)
            return false;
    })

    $(document).on('keydown', 'textarea', function (e) {
        var isNewField = checkForNewFields(e.target);
        if ((e.which == 8 || e.which == 46) && !isNewField)
            return false;
    });
});

function checkForNewFields(source) {
    var availableFields = $(".btn-certificate-field");
    var result = true;
    $.each(availableFields, function () {
        if ($(source).val() == $(this).text()) {
            result = false;
        }
    });
    return result;
}