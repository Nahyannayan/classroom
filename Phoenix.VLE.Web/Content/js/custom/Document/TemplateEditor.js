﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var certificate = function () {
    var init = function () {
        loadTemplateGrid();
    },
        loadTemplateGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Title", "sTitle": translatedResources.title, "sWidth": "20%" },
                { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "20%" },
                { "mData": "TemplateType", "sTitle": translatedResources.templateType, "sWidth": "10%" },
                { "mData": "Status", "sClass": "text-center", "sTitle": translatedResources.status, "sWidth": "20%" }
            );
            if (isCustomPermissionEnabled.toBoolean()) {
                _columnData.push({ "mData": "Actions", "sClass": "text-center no-sort", "sortable": false,"sTitle": translatedResources.actions, "sWidth": "20%" });
            }
            initTemplateGrid('tbl-certificateTemplate', _columnData, '/Document/CertificateBuilder/LoadCertificateGrid');
        },

        initTemplateGrid = function (_tableId, _columnData, _url) {

            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [1, 2]
                }],
                searching: true
            };
            grid.init(settings);

            $("#tbl-certificateTemplate_filter").hide();
            $('#tbl-certificateTemplate').DataTable().search('').draw();
            $("#QuizListSearch").on("input", function (e) {
                e.preventDefault();
                $('#tbl-certificateTemplate').DataTable().search($(this).val()).draw();
            });
        },

        onFailure = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },

        loadTemplateField = function (id) {
            $.ajax({
                url: '/document/CertificateBuilder/GetTemplateFieldByTemplateId?templateId=' + id,
                type: 'GET',
                success: function (data) {
                    if (globalFunctions.isValueValid(data)) {
                        $("#TemplateFields").html(data);
                    }
                    else {
                        globalFunctions.onFailure();
                    }
                },
                error: function () {
                    globalFunctions.onFailure();
                }

            });
        },

        onSaveSuccess = function (result) {
            globalFunctions.showMessage(result.data.NotificationType, result.data.Message);
            if (result.data.InsertedRowId > 0 && globalFunctions.isValueValid(result.data.InsertedRowId)) {
                $("#IsAddMode").val('false');
                //$("#btnNavCertificateBuilder").removeClass("d-none");
                //$("#btnNavCertificateBuilder").attr('onclick', "location.href='/Document/CertificateBuilder/AssignCertificateFields?id=" + result.EditUrl + "'");
                //$("#TemplateId").val(result.data.InsertedRowId);

                location.href = "/Document/CertificateBuilder/CertificateBuilder?id=" + result.EditUrl;
            }
        },

        addNewField = function (elemNum) {
            if (elemNum == 0) {
                $("#TemplateFields").append("<div class='element row' id='div_0'></div>");
                //$("#div_0").append("<div class='d-flex quiz-answer-field w-100'><input type='hidden' id='TemplateField_0_TemplateId' name='TemplateFields[0].TemplateId' value='0' ><input type='hidden' id='TemplateField_0_IsActive' name='TemplateFields[0].IsActive' value='true' ><input type='text'  class='responseInput' placeholder='Field' id='TemplateField_0_Field' maxlength='150' name='TemplateFields[0].Field' required='required'>  <input type='text'  class='' placeholder='Placeholder' id='TemplateField_0_Placeholder' readonly='readonly' maxlength='160'  name='TemplateFields[0].Placeholder' required='required'>  </div> <button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button>");
                $("#div_0").append(
                    "<div class='col-lg-3 col-md-3 col-12'>" +
	                    "<input type='hidden' id='TemplateField_0_TemplateId' name='TemplateFields[0].TemplateId' value='0'>" + 
                        "<input type='hidden' id='TemplateField_0_IsActive' name='TemplateFields[0].IsActive' value='true'>" +
                        "<input type='text' class='responseInput form-control' placeholder='Field' id='TemplateField_0_Field' maxlength='150' name='TemplateFields[0].Field' required='required'>" +
                    "</div>" +
                    "<div class='col-lg-3 col-md-3 col-12'>" +
                        "<input type='text' class='form-control' placeholder='Placeholder' id='TemplateField_0_Placeholder' readonly='readonly' maxlength='160' name='TemplateFields[0].Placeholder' required='required'>" +
                    "</div>"+
                    "<div class='col-lg-3 col-md-3 col-12'>" +
                        "<select id='TemplateField_0_CertificateColumnId' data-size='5' data-live-search='true' class='form-control form-control-alternative' maxlength='150' name='TemplateFields[0].CertificateColumnId'></select>" +
                    "</div>" +
                    "<div class='col-lg-3 col-md-3 col-12'>" + 
                        "<div class='custom-control custom-checkbox custom-control-inline my-4 my-md-0'>" +
                            "<input type='checkbox' class='DefaultValue custom-control-input' id='TemplateField_0_DefaultFieldValue' name='TemplateFields[0].DefaultFieldValue'>" +
                            "<label class='custom-control-label' for='TemplateField_0_DefaultFieldValue'>Mark To Default Value</label>" + 
                            "<div class='col-md-0'>" +
                                "<button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i>" +
                            "</div>" +
                        "</div>" +
                    "</div>");

                FillDropDown("TemplateField_" + elemNum + "_CertificateColumnId");
            }
            else {
                $(".element:last").after("<div class='element row' id='div_" + elemNum + "'></div>");
                //$("#div_" + elemNum).append("<div class='md-form md-outline col-md-5 pl-2'><input type='hidden' id='TemplateField_" + elemNum + "_TemplateId' name='TemplateFields[" + elemNum + "].TemplateId' value='0' ><input type='hidden' id='TemplateField_" + elemNum + "_IsActive' name='TemplateFields[" + elemNum + "].IsActive' value='true'  > <input type='text'  class='responseInput form-control' placeholder='Field' id='TemplateField_" + elemNum + "_Field' maxlength='150' name='TemplateFields[" + elemNum + "].Field' required='required'></div><div class='md-form md-outline col-md-5 pl-0'>  <input type='text'  class='form-control' placeholder='Placeholder' readonly='readonly' id='TemplateField_" + elemNum + "_Placeholder' maxlength='160' name='TemplateFields[" + elemNum + "].Placeholder' required='required'>  </div> <div class='col-md-2'><button type='button' id='remove_" + elemNum + "' class='remove btn btn-outline-danger m-0'><i class='fas fa-plus'></i></button></div>");
                $("#div_" + elemNum).append(
                    "<div class='col-lg-3 col-md-3 col-12'>" +
                        "<div class='md-form md-outline'>" +
                            "<input type='hidden' id='TemplateField_" + elemNum + "_TemplateId' name='TemplateFields[" + elemNum + "].TemplateId' value='0' >" +
                            "<input type='hidden' id='TemplateField_" + elemNum + "_IsActive' name='TemplateFields[" + elemNum + "].IsActive' value='true'>" +
                            "<input type='text' class='responseInput form-control' placeholder='Field' id='TemplateField_" + elemNum + "_Field' maxlength='150' name='TemplateFields[" + elemNum + "].Field' required='required'>" +
                        "</div>" +
                    "</div>" +
                    "<div class='col-lg-3 col-md-3 col-12'>" +  
                        "<div class='md-form md-outline'>" +
                            "<input type='text'  class='form-control' placeholder='Placeholder' readonly='readonly' id='TemplateField_" + elemNum + "_Placeholder' maxlength='160' name='TemplateFields[" + elemNum + "].Placeholder' required='required'>" +
                        "</div>" +
                    "</div>" +
                    "<div class='col-lg-3 col-md-3 col-12'>" +
                    "<select id='TemplateField_" + elemNum + "_CertificateColumnId' data-size='5' data-live-search='true' class='form-control form-control-alternative' maxlength='150' name='TemplateFields[" + elemNum + "].CertificateColumnId'></select>" +
                    "</div>" +
                    "<div class='col-lg-3 col-md-3 col-12'>" +
                        "<div class='custom-control custom-checkbox custom-control-inline my-4 my-md-0'>" +
                            "<input type='checkbox' class='DefaultValue custom-control-input' id='TemplateField_" + elemNum + "_DefaultFieldValue' name='TemplateFields[" + elemNum + "].DefaultFieldValue'>" +
                            "<label class='custom-control-label' for='TemplateField_" + elemNum + "_DefaultFieldValue'>Mark To Default Value</label>" +
                            "<div class='col-md-0'>" +
                                "<button type='button' id='remove_" + elemNum + "' class='remove btn btn-outline-danger m-0'><i class='fas fa-plus'></i></button>" +
                            "</div>" +
                        "</div>" +
                    "</div>");

                FillDropDown("TemplateField_" + elemNum + "_CertificateColumnId");
            }
        },

        deleteTemplate = function (source, templateId) {
            globalFunctions.notyDeleteConfirm($(source), translatedResources.deleteConfirm);
            $(source).off("okToDelete");
            $(source).on("okToDelete", function () {
                $.post("/Document/CertificateBuilder/DeleteCertificateTemplate", { templateId: templateId }, function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success)
                        loadTemplateGrid();
                }).fail(function () { globalFunctions.onFailure(); });
            });
        };

    return {
        init: init,
        onSaveSuccess: onSaveSuccess,
        onFailure: onFailure,
        initTemplateGrid: initTemplateGrid,
        loadTemplateField: loadTemplateField,
        addNewField: addNewField,
        deleteTemplate: deleteTemplate
    };
}();

$(document).ready(function () {
    certificate.init();
    $('body').on('click', '#add-more', function () {
        var total_element = $(".element").length;
        var lastid = $(".element:last").attr("id");
        var split_id = lastid.split("_");
        var nextindex = Number(split_id[1]) + 1;
        certificate.addNewField(nextindex);
    });

    $("#btn-submit").click(function () {
        if (!$("#frmAddTempalte")[0].checkValidity()) {
            $("#frmAddTempalte")[0].reportValidity();
        }
    });

    $('body').on('click', '.remove', function () {

        var source = $(this);
        var id = this.id;
        var split_id = id.split("_");
        var deleteindex = split_id[1];
        var templateId = $("#TemplateField_" + deleteindex + "_TemplateId").val();
        var text = $("#TemplateField_" + deleteindex + "_Field").val().trim();
        if (templateId === '0' && text === '') {
            $("#div_" + deleteindex).remove();
        }
        else {

            globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
            $(document).bind('okToDelete', source, function (e) {
                if (templateId === '0' && text !== '') {
                    $("#div_" + deleteindex).remove();

                }
                else {
                    $("#div_" + deleteindex).addClass('d-none');
                    $("#TemplateField_" + deleteindex + "_IsActive").val(false);
                }
            });
        }

    });

    $('body').on('focusout', ".responseInput", function () {
        var id = this.id;
        var split_id = id.split("_");
        var currId = split_id[1];
        var text = $(this).val().trim();
        $(this).val(text);
        if (text != '') {
            var isLabel = $("#TemplateField_" + currId + "_IsLabel").prop('checked');
            $("#TemplateField_" + currId + "_IsLabel").val(isLabel);
            if (!isLabel) {
                text = text.replace(/\s/g, '-').trim();
                text = "__" + text + "__";
            }

            $("#TemplateField_" + currId + "_Placeholder").val(text);
        }
        else {
            $("#TemplateField_" + currId + "_Placeholder").val('');
        }
    });

    //$('body').on('change', "input[type='checkbox']", function () {
    //    var id = this.id;
    //    var split_id = id.split("_");
    //    var currId = split_id[1];

    //    var text = $("#TemplateField_" + currId + "_Field").val().trim();
    //    var isLabel = $(this).prop('checked');
    //    $(this).val(isLabel);

    //    if (text != '') {
    //        if (!isLabel) {
    //            text = text.replace(/\s/g, '-').trim();
    //            text = "__" + text + "__";

    //        }
    //        $("#TemplateField_" + currId + "_Placeholder").val(text);
    //    }
    //    else {
    //        $("#TemplateField_" + currId + "_Placeholder").val('');
    //    }
    //});

    $('body').on('change', ".DefaultValue", function () {

        var id = this.id;
        var split_id = id.split("_");
        var currId = split_id[1];
        
        var text = $("#TemplateField_" + currId + "_CertificateColumnId").val().trim();
        var isLabel = $(this).prop('checked');
        $(this).val(isLabel);
        if (isLabel) {
            $("#TemplateField_" + currId + "_CertificateColumnId").prop('disabled', false);
        }
        else {
            $("#TemplateField_" + currId + "_CertificateColumnId").prop('disabled', 'disabled');
        }
        $("#TemplateField_" + currId + "_CertificateColumnId").selectpicker('refresh');
    });
});

window.addEventListener('load', function () {
    
    $('.DefaultValue:checkbox:checked').each(function () {
        var id = this.id;
        var split_id = id.split("_");
        var currId = split_id[1];
        FillDropDown("TemplateField_" + currId + "_CertificateColumnId");
        $("#TemplateField_" + currId + "_CertificateColumnId").removeAttr('disabled');
        $("#TemplateField_" + currId + "_CertificateColumnId").val($("#hdn_TemplateField_" + currId + "_CertificateColumnId").val());
        $("#TemplateField_" + currId + "_CertificateColumnId").selectpicker('refresh');
    });
    $('.DefaultValue:checkbox:unchecked').each(function () {

        var id = this.id;
        var split_id = id.split("_");
        var currId = split_id[1];
        FillDropDown("TemplateField_" + currId + "_CertificateColumnId");
        $("#TemplateField_" + currId + "_CertificateColumnId").prop('disabled', 'disabled');
        $("#TemplateField_" + currId + "_CertificateColumnId").selectpicker('refresh');
    });
});

var JsonResult = null;
function FillDropDown(dropDownId) {
    if (JsonResult != null || JsonResult != undefined) {
        $("#" + dropDownId).empty();
        $("#" + dropDownId).prop('disabled', 'disabled');
        $("#" + dropDownId).prop("title", 'Select Column');
        $.each(JsonResult, function () {
            $("#" + dropDownId).append($("<option />").val(this.Value).text(this.Text));
        });
        $("#" + dropDownId).selectpicker('refresh');
        return;
    }

    $.ajax({
        type: "Get",
        url: "GetCertificateColumns",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#" + dropDownId).empty();
            $("#" + dropDownId).prop('disabled', 'disabled');
            $("#" + dropDownId).prop("title", 'Select Column');
            JsonResult = data;
            $.each(data, function () {
                $("#" + dropDownId).append($("<option />").val(this.Value).text(this.Text));

            });
            $("#" + dropDownId).selectpicker('refresh');
        },
        error: function (xhr, status, error) { }
    });
}

