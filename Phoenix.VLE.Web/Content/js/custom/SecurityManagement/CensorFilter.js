﻿/// <reference path="../../common/global.js" />

var bannedWords = function () {

    var init = function () {  

            enableTagsInput();
        
    },

        initComplete = function () {
            $('#tagBannedWords').next('ul.tag-editor.ui-sortable').addClass('form-control');
            $('#tagBannedWords').next('ul.tag-editor').css('max-height','500px');
            $('#tagBannedWords').next('ul.tag-editor').css('height', '500px');
        },

        enableTagsInput = function () {          

            $('#tagBannedWords').tagEditor({
                initialTags: $('#tagBannedWords').data('initialtags').split(';'),
                delimiter: ';',
                forceLowercase: false,
                maxLength: 2000,
                beforeTagSave(field, editor, tags, tag, val) {
                    //debugger;
                    editor.setBeginSave();
                    if (val != "" && !updateBannedWord(val, tag, '/SecurityManagement/CensorFilter/SaveBannedWordData')) {
                        editor.setError();
                        return false;
                    }
                    else
                        editor.setEndSave(true);
                },
                beforeTagDelete(field, editor, tags, val) {
                    //debugger;
                    editor.setBeginSave();
                    if (!updateBannedWord(val,'', '/SecurityManagement/CensorFilter/DeleteBannedWordData')) {
                        editor.setError();
                        return false;
                    }
                    else
                        editor.setEndSave(true);
                },
                placeholder: $('#tagBannedWords').attr('placeholder')
            });
        },

        updateBannedWord = function (word, previousWord, url) {
            //debugger;
            var result = false;
            var updateParameters = {
                word: word,
                previousWord: previousWord
            };
            $.ajax({
                type: 'POST',
                url: url,
                data: updateParameters,
                async: false,
                success: function (data) {
                    result = data.Success;
                    //globalFunctions.showMessage(data.NotificationType, data.Message);
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
            return result;
        };
    return {
        init: init,
        initComplete: initComplete
    };
}();

var censorFilters = function () {

    var init = function () {
        loadCensorUserGrid();
    },

        loadCensorUserGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "CensorFrom", "sTitle": translatedResources.from, "sWidth": "20%" },
                { "mData": "CensorTo", "sTitle": translatedResources.to, "sWidth": "20%" },
                { "mData": "CensorDate", "sTitle": translatedResources.date, "sWidth": "10%" },
                { "mData": "CensorWord", "sTitle": translatedResources.word, "sWidth": "10%" },
                { "mData": "Severity", "sClass": "text-center", "sTitle": translatedResources.severity, "sWidth": "5%" },
                { "mData": "IP", "sClass": "text-center", "sTitle": translatedResources.ip, "sWidth": "15%" },
                { "mData": "Context", "sClass": "text-center", "sTitle": translatedResources.context, "sWidth": "10%" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "10%" }

            );
            initCensorUserGrid('tbl-CensorUser', _columnData, '/SecurityManagement/CensorFilter/LoadCensorUserGrid');
        },

        initCensorUserGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': 7
                }],
            };
            grid.init(settings);
        };

    return {
        init: init,
        loadCensorUserGrid: loadCensorUserGrid
    };
}();

$(document).ready(function () {

    

    if (globalFunctions.isValueValid($('#bannedwordlist').val())) {

        $('#bannedwordlist').val($('#bannedwordlist').val().replaceAll(';', ','));


        $('#searchBannedWord').off("keyup").on('keyup', function (e) {
            var searchText = $(this).val().toLowerCase();
          
           
            $("#bannedwordlist").val('');
            var existText = '';
            //debugger;
            $("#bannedWords").val().split(";").forEach(function (val, index, arr) {
               
                if (val.toLowerCase().indexOf(searchText) > -1) {
                    if (existText.length === 0)
                    {
                        existText = val;
                    }
                    else
                    {
                        existText = existText + ',' + val;
                    }
                }
            });

            existText.length > 0 ? $("#bannedwordlist").val(existText) : $("#bannedwordlist").val($("#bannedWords").val().replace(';',','));
        });
    }
    else {
        censorFilters.init();
        bannedWords.init();
        bannedWords.initComplete();


        $('#searchBannedWord').off("keyup").on('keyup', function (e) {
            var searchText = $(this).val().toLowerCase();
            $('ul.tag-editor  li ').each(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(searchText) > -1);
            });

        });
    }

    $("#tbl-CensorUser_filter").hide();
    $("#tbl-CensorUser_filter").val('');
    $("#CensorListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-CensorUser').DataTable().search($(this).val()).draw();
    });
});
