﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();

var abusedelegate = function () {
    var init = function () {
        //formElements.feSelect();
    },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success && $("#AbuseContactInfoId").val() == 0) {
                window.location.reload();
            }
        },
        onFailure = function (data) {
            globalFunctions.showMessage("error", data.Message);
        };

    return {
        init: init,
        onSaveSuccess: onSaveSuccess,
        onFailure: onFailure
    };
}();

$(document).ready(function () {
    abusedelegate.init();

    SetCustomScrollbar();

    $(document).on("change", "#EnableCallingPolice", function () {
        var isChecked = $(this).is(":checked");
        $("#divPoliceContactDetails").toggle(isChecked);
        isChecked ? $("#PolicePhoneNumber").removeAttr("disabled") : $("#PolicePhoneNumber").attr("disabled", "disabled");
    })
});

$("#CensorNoticeRecieverId").on("change", function () {
    var censorNoticeDelegateId = $(this).children("option:selected").val();;
    if ($("#SelectedDelegateId").val().indexOf(censorNoticeDelegateId+",") != -1) {
        globalFunctions.showWarningMessage(translatedResources.staffExist.replace("$$", $(this).children("option:selected").text()));
    }
});

$("#btnAddDelegates").click(function () {
    //var $selectedDelegates = $("#DelegateList").children("input[type='checkbox']:selected");
    var $selectedDelegates = $("#DelegateList input[type='checkbox']:checked").parent('.custom-checkbox');
    //console.log($selectedDelegates);
    var censorNoticeDelegateId = $("#CensorNoticeRecieverId option:selected").val();

    if ($selectedDelegates != null && $selectedDelegates.length > 0) {
        $selectedDelegates.each(function (i, elem) {
            if ($(this).children("input[type='checkbox']").val() != censorNoticeDelegateId) {
                if ($("#SelectedDelegateId").val().indexOf("") == -1) {
                    $("#SelectedDelegateId").val($(this).children("input[type='checkbox']").val() + ",");
                }
                else {
                    $("#SelectedDelegateId").val($("#SelectedDelegateId").val() + $(this).children("input[type='checkbox']").val() + ",");
                }
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.staffExist.replace("$$", $(this).text()));
                $selectedDelegates.remove(elem);
            }
        });


        $("#SelectedDelegateList").append($selectedDelegates);
        $("#SelectedDelegateList").html($("#SelectedDelegateList .custom-checkbox").sort(function (a, b) {
            //console.log($(a).children('.custom-control-label'));
            return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
        }));

        $("#SelectedDelegateList input[type='checkbox']").prop('checked', $('#UnselegatedAllDelegate').is(':checked'));
    }
    else {
        globalFunctions.showWarningMessage(translatedResources.selectAdd);
    }
    
});

$("#btnRemoveDelegates").click(function () {
    var $selectedDelegates = $("#SelectedDelegateList input[type='checkbox']:checked").parent('.custom-checkbox');

    if ($selectedDelegates != null && $selectedDelegates.length > 0) {
        $selectedDelegates.each(function (i, elem) {
            if ($("#SelectedDelegateId").val().indexOf($(this).children("input[type='checkbox']").val() + ",") != -1) {
                $("#SelectedDelegateId").val($("#SelectedDelegateId").val().replace($(this).children("input[type='checkbox']").val() + ",", ""));
            }
        });

        $("#DelegateList").append($selectedDelegates);
        $("#DelegateList").html($("#DelegateList .custom-checkbox").sort(function (a, b) {
            console.log($(a).children('.custom-control-label').text());
            return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
        }));
        $("#DelegateList input[type='checkbox']").prop('checked', $('#SelectAllDelegate').is(':checked'));
    }
    else {
        globalFunctions.showWarningMessage(translatedResources.selectRemove);
    }

    
});


$('#SelectAllDelegate').change(function (e) {
    var $inputs = $('#DelegateList input[type=checkbox]');
    if (e.originalEvent === undefined) {
        var allChecked = true;
        $inputs.each(function () {
            allChecked = allChecked && this.checked;
        });
        this.checked = allChecked;
    } else {
        $inputs.prop('checked', this.checked);
    }
});

$('input[type=checkbox]', "#DelegateList").on('change', function () {
    $('#SelectAllDelegate').trigger('change');
});

$('input[type=checkbox]', "#SelectedDelegateList").on('change', function () {
    $('#UnselegatedAllDelegate').trigger('change');
});

$('#UnselegatedAllDelegate').change(function (e) {
    var $inputs = $('#SelectedDelegateList input[type=checkbox]');
    if (e.originalEvent === undefined) {
        var allChecked = true;
        $inputs.each(function () {
            allChecked = allChecked && this.checked;
        });
        this.checked = allChecked;
    } else {
        $inputs.prop('checked', this.checked);
    }
});

$('#searchDelegate').on('keyup',  function (e) {
    var searchText = $(this).val().toLowerCase();
    $('#DelegateList div').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
});

$('#searchSelectedDelegate').on('keyup',function (e) {
    var searchText = $(this).val().toLowerCase();
    $('#SelectedDelegateList div').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
   
});

function SetCustomScrollbar() {
    $(".delegates-wrapper").mCustomScrollbar({
        setHeight: "250",
        autoExpandScrollbar: true
    });
}
