﻿var DelegateSearchType = {
    New: 'N',
    Existing: 'E'
}
var approverDelegateFunctions = function () {
    var filterDelegatesList = function (source) {
        var searchText = $(source).val().toLowerCase();
        var divId = $(source).data("searchtype") == DelegateSearchType.New ? 'DelegateList' : 'SelectedDelegateList';
        $('#' + divId + ' [data-search-text]').each(function () {
            $(this).toggle($(this).data("search-text").toLowerCase().indexOf(searchText.trim()) !== -1);
        });
    },
        toggleDelegateSelection = function (source, type) {
            var divId = type == DelegateSearchType.New ? 'DelegateList' : 'SelectedDelegateList';
            $("#" + divId + ' input:checkbox').prop("checked", $(source).is(":checked"));
        },

        addDeleteDelegates = function (isAddMode) {
            var parentDivId = isAddMode ? "DelegateList" : "SelectedDelegateList";
            var delegates = $("#" + parentDivId + " input:checkbox:checked");
            if (delegates.length == 0) {
                globalFunctions.showMessage("warning", translatedResources.selectApproverMsg);
                return;
            }
            isApproverDataChanges = true;
            delegates.each(function () {
                $(this).prop("checked", false);
                $(this).closest("div.delegate").show().appendTo($("#" + (isAddMode ? "Selected" : "") + "DelegateList"));
            });
        },

        checkAllSelections = function (source) {
            var parent = $(source).parents().closest(".teacher-delegate");
            var sourceCheckboxes = parent.find("input:checkbox");
            parent.parents().find('.chk-select-all[data-approvaltype=' + parent.data("approvaltype") + ']').prop("checked", sourceCheckboxes.length === parent.find("input:checkbox:checked").length);

        },

        saveApproverDelegates = function () {
            if (!isApproverDataChanges) {
                globalFunctions.showMessage("warning", translatedResources.noChangeMsg);
                return;
            }
            var selectDelegates = [];
            $("#SelectedDelegateList input:checkbox").each(function () {
                selectDelegates.push($(this).val());
            });

            $.post("/SecurityManagement/ApproverDelegate/SaveApproverDelegates", { teacherIds: selectDelegates.join(','), templateTypeId: $("input:radio[name=optTemplateType]:checked").val() }, function (data) {
                globalFunctions.showMessage(data.NotificationType, data.Message);
                if (data.Success)
                    isApproverDataChanges = false;
            }).fail(function () {
                globalFunctions.onFailure();
            })
        },

        refreshApproverList = function (templateType) {
            //var departmentId = $("#ddlDepartment").val();
            $("#divApproverList").load("/SecurityManagement/ApproverDelegate/GetApproverDelegatePartial?templateType=" + templateType, initScroll);
        };


    return {
        filterDelegatesList: filterDelegatesList,
        toggleDelegateSelection: toggleDelegateSelection,
        addDeleteDelegates: addDeleteDelegates,
        saveApproverDelegates: saveApproverDelegates,
        checkAllSelections: checkAllSelections,
        refreshApproverList: refreshApproverList
    }
}();

$(function () {
    initScroll();
    $(document).on('change', 'input:radio[name=optTemplateType],#ddlDepartment', function () {
        approverDelegateFunctions.refreshApproverList($('input:radio[name=optTemplateType]:checked').val());
    });

});

function initScroll() {
    $(".delegates-wrapper").mCustomScrollbar({
        setHeight: "250",
        autoExpandScrollbar: true
    });

}