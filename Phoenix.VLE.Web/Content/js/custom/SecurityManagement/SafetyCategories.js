﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();
var safetyCategories = function () {

    var init = function () {
        loadSafetyCategoryGrid();
        $("#tbl-SafetyCategories_filter").hide();

    },

        loadCategoryPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.safetyCategory;
            globalFunctions.loadPopup(source, '/SecurityManagement/SafetyCategories/InitAddEditSafetyCategoryForm?id=' + id, title, 'safetycategory-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();

            });
        },

        addCategoryPopup = function (source) {
            loadCategoryPopup(source, translatedResources.add);
        },

        editCategoryPopup = function (source, id) {
            loadCategoryPopup(source, translatedResources.CanEdit === 'True' ? translatedResources.edit : translatedResources.view, id);
        },

        deleteSafetyCategoryData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('SafetyCategories', '/SecurityManagement/SafetyCategories/DeleteSafetyCategoryData', 'tbl-SafetyCategories', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        loadSafetyCategoryGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "SafetyCategoryName", "sTitle": translatedResources.safetyCategory, "sWidth": "50%", "sClass": "wordbreak" },
                { "mData": "FormattedCreatedOn", "sTitle": translatedResources.createdOn, "sWidth": "15%", "sClass": "open-details-control wordbreak" },
                { "mData": "CreatedByName", "sTitle": translatedResources.createdBy, "sWidth": "15%" },
                { "mData": "IsActive", "sTitle": translatedResources.active, "sWidth": "10%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "10%" }
            );
            initSafetyCategoryGrid('tbl-SafetyCategories', _columnData, '/SecurityManagement/SafetyCategories/LoadSafetyCategoriesGrid');
        },

        initSafetyCategoryGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 4,
                    'orderable': false
                }]
            };
            grid.init(settings);
        };

    return {
        init: init,
        addCategoryPopup: addCategoryPopup,
        editCategoryPopup: editCategoryPopup,
        deleteCategoryData: deleteSafetyCategoryData,
        onSaveSuccess: onSaveSuccess,
        loadSafetyCategoryGrid: loadSafetyCategoryGrid
    };
}();

$(document).ready(function () {
    safetyCategories.init();
   

    $(document).on('click', '#btnAddCategory', function () {
        safetyCategories.addCategoryPopup($(this));       
    });
    //$("#tbl-SafetyCategories_filter").addClass("d-none");
    $("#tbl-SafetyCategories_filter").hide();
    $('#tbl-SafetyCategories').DataTable().search('').draw();
    $("#SafetyCategoriesListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-SafetyCategories').DataTable().search($(this).val()).draw();
        
    });
});



