﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var quiz = function () {

    var init = function () {
        loadQuizGrid();
        //quiz.noSort();


    },
        loadQuizGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "QuizName", "sTitle": translatedResources.QuizName, "sWidth": "70%" },
                { "mData": "IsActive", "sTitle": translatedResources.IsActive, "sWidth": "15%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "15%" }
            );
            initQuizGrid('tbl-Quiz', _columnData, '/Quiz/Quiz/LoadQuizGrid');
        },
        initQuizGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [1, 2]
                }],
                searching: false
            };
            grid.init(settings);
        },
        loadQuizPopup = function (source, mode, id, groupId, module,folderId, moduleId) {
            var title = mode + ' ' + translatedResources.formtitle;
          //  var secId = $("#GroupSectionId").val() == "" ? 0 : $("#GroupSectionId").val();
            //var secType = $("#SectionType").val();
            globalFunctions.loadPopup(source, '/Files/Files/InitAddEditQuizForm?id=' + id+'&pfId='+folderId+'&moduleId='+moduleId, title, 'quiz-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $("#resourceSectionID").val(groupId);
                $("#resourceSectionType").val(module);
                //popoverEnabler.attachPopover();
            });
        },
        addQuizPopup = function (source, groupId, module, folderId, moduleId) {
            loadQuizPopup(source, translatedResources.add, '', groupId, module, folderId, moduleId);
        },

        editQuizPopup = function (source, id, groupId, module, folderId, moduleId) {
            loadQuizPopup(source, translatedResources.edit, id, groupId, module, folderId, moduleId);
        },

        deleteQuizData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
                $(source).unbind('okClicked');
                $(source).bind('okClicked', source, function (e) {
                    $.ajax({
                        url: '/Files/Files/DeleteQuizData/?id=' + id,
                        type: 'GET',
                        success: function (data) {
                            globalFunctions.showMessage(data.NotificationType, data.Message);
                            if (data.Success === true) {
                                //loadFormList();
                                if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
                                    groupDetail.loadTopicActivitiesList();
                                }
                            }
                            else {
                                globalFunctions.onFailure();
                            }
                        },
                        error: function (data, xhr, status) {
                            ////debugger;
                            globalFunctions.onFailure();
                        }

                    });
                });
            }
        },
        onFailure = function (data) {
            console.log("Failed");
            console.log(data);
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                //loadFormList();
                if ($("#PageName") != undefined && $("#PageName").val() == "GroupDetails") {
                    groupDetail.loadTopicActivitiesList();
                }
            }
        },
        openQuizQuestion = function (id) {
            //debugger
            if (globalFunctions.isValueValid(id)) {
                $.post('/Shared/Shared/SetSession', { key: "SelectedQuizId", value: id }, function (data) {
                    if (typeof id !== 'undefined' && data === true)
                        window.open('/Forms/FormsQuestions', '_self');
                    else {
                        globalFunctions.onFailure();
                    }
                });
            }
        },
        noSort = function () {
            var table = $('#tbl-Quiz').DataTable();
            table.destroy();
            $('#tbl-Quiz').DataTable({
                "columnDefs": [{
                    "targets": "no-sort",
                    "orderable": false
                }]
            });
            $("#tbl-Quiz_length").hide();
            $("#tbl-Quiz_filter").hide();
        },
        changeStatus = function (quizid, quizname, description, passmarks, isactive) {
            var formData = {
                QuizId: quizid,
                QuizName: quizname,
                Description: description,
                PassMarks: passmarks,
                IsActive: isactive === 1 ? true : false
            };
            var token = $('#card_body').find('input[name="__RequestVerificationToken"]').val();
            $.ajax({
                type: 'POST',
                url: '/Quiz/Quiz/SaveQuizData',
                data: { __RequestVerificationToken: token, model: formData },
                success: function (result) {
                    if (result) {
                        quiz.init();
                        quiz.onSaveSuccess;
                    }
                },
                error: function (msg) {
                    quiz.onFailure;
                }
            });
        },
        solveForm = function (source, id, resourceId, studentId) {
            window.location.href = '/Files/Files/Form/?id=' + id + '&resourceId=' + resourceId + '&studentId=' + studentId;
        },
        markGroupForm = function (source, id, groupId) {
            window.location.href = '/Files/Files/GroupStudentDetails/?id=' + id + '&groupId=' + groupId +'&isForm='+true;
        },
        loadFormList = function () {
            var currUserId = $("#CurrUserId").val();
            var sectionType = $("#SectionType").val() == undefined ? "" : $("#SectionType").val();
            var sectionId = $("#GroupSectionId").val() == "" ? 0 : $("#GroupSectionId").val();
            $.ajax({
                url: '/Files/Files/GetFormListView/?Id=' + currUserId + '&sectionType=' + sectionType + '&SectionId='+sectionId,
                type: 'GET',
                success: function (data) {
                    ////debugger;
                    if (data !== '') {
                        $("#form-list").html("");
                        $("#form-list").html(data);
                        //folder.showSelectedView();
                    }
                },
                error: function (data, xhr, status) {
                    ////debugger;
                    globalFunctions.onFailure();
                }

            });
        };


    return {
        init: init,
        loadQuizGrid: loadQuizGrid,
        loadQuizPopup: loadQuizPopup,
        addQuizPopup: addQuizPopup,
        solveForm: solveForm,
        markGroupForm: markGroupForm,
        editQuizPopup: editQuizPopup,
        deleteQuizData: deleteQuizData,
        onSaveSuccess: onSaveSuccess,
        onFailure: onFailure,
        noSort: noSort,
        openQuizQuestion: openQuizQuestion,
        changeStatus: changeStatus,
        loadFormList: loadFormList
    };
}();

$(document).ready(function () {
    //quiz.init();
    //quiz.noSort();

    $(document).on('click', '#btnAddForm', function () {
        var secId = $("#GroupSectionId").val() == "" ? 0 : $("#GroupSectionId").val();
        var secType = $("#SectionType").val();
        var folderId = $("#ParentFolderId").val();
        var moduleId = $("#ModuleId").val();
        quiz.addQuizPopup($(this), secId, secType,folderId,moduleId);
    });
    $('#tbl-Quiz').DataTable().search('').draw();
    $("#QuizListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-Quiz').DataTable().search($(this).val()).draw();
    });

    $(document).on('click', '#btnDeleteForm', function () {
        var id = $(this).data("id");
        console.log(id);
        quiz.deleteQuizData($(this), id);
    });
    $(document).on('click', '#btnSolveForm', function () {
        var id = $(this).data("id");
        var resourceId = $(this).data("quizresourceid");
        var studentId = $(this).data("studid");
        quiz.solveForm($(this), id, resourceId, studentId);
    });
    $(document).on('click', '#btnFormMark', function () {
        var id = $(this).data("id");
        var groupId = $(this).data("groupid");
        quiz.markGroupForm($(this), id, groupId);
    });
    $(document).on('click', '#btnEditForm', function () {
        var id = $(this).data("id");
        var secId = $("#GroupSectionId").val() == "" ? 0 : $("#GroupSectionId").val();
        var secType = $("#SectionType").val();
        console.log(id);
        var folderId = $("#ParentFolderId").val();
        var moduleId = $("#ModuleId").val();

        quiz.editQuizPopup($(this), id, secId, secType,folderId,moduleId);
    });
    $(document).on('click', '#btnViewForm', function () {
        var id = $(this).data("id");
        //console.log(id);
        quiz.openQuizQuestion(id);
    });
    $(document).on('click', '#btnSolveGroupForm', function () {
        var id = $(this).data("id");
        var resourceId = $(this).data("quizresourceid");
        var studentId = $(this).data("studid");
        quiz.solveForm($(this), id, resourceId, studentId);
    });
});