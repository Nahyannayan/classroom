﻿
/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var sortedQuizQuestionIds = [];
var targetResource = true;
var targetResourceId = ''; 
var _dataTableLangSettings = {
    "sEmptyTable": translatedResources.noData,
    "sInfo": translatedResources.sInfo,
    "sInfoEmpty": translatedResources.sInfoEmpty,
    "sInfoFiltered": translatedResources.sInfoFiltered,
    "sInfoPostFix": "",
    "sInfoThousands": ",",
    "sLengthMenu": translatedResources.sLengthMenu,
    "sLoadingRecords": "Loading...",
    "sProcessing": "Processing...",
    "sSearch": translatedResources.search,
    "sZeroRecords": translatedResources.sZeroRecords,
    "oPaginate": {
        "sFirst": translatedResources.first,
        "sLast": translatedResources.last,
        "sNext": translatedResources.next,
        "sPrevious": translatedResources.previous
    },
    "oAria": {
        "sSortAscending": translatedResources.sSortAscending,
        "sSortDescending": translatedResources.sSortDescending
    }
};
var quizquestions = function () {

    addQuizQuestionsPopup = function (source) {
        var title = translatedResources.add + translatedResources.QuizQuestions;
        globalFunctions.loadPopup(source, '/Forms/FormsQuestions/AddQuizQuestion', title, 'quiz-question-dailog');

        $(source).off("modalLoaded");
        $(source).on("modalLoaded", function () {
            $('.selectpicker').selectpicker('refresh');
        });

    },
        editQuizQuestionsPopup = function (source, id) {
            var title = translatedResources.edit + translatedResources.QuizQuestions;
            globalFunctions.loadPopup(source, '/Forms/FormsQuestions/EditQuizQuestion?id=' + id, title, 'quiz-question-dailog');


            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $('.selectpicker').selectpicker('refresh');
            });
        },
        deleteQuizQuestionData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('Forms', '/Forms/FormsQuestions/deleteQuizQuestionData', 'tbl-QuizQuestion', source, id);
            }
        },
        onFailure = function (data) {
            console.log("Failed");
            console.log(data);
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                window.location.reload();
            }
        },
        correctAnswer = function (answerid, answer, questiontype, questionid) {
            $.ajax({
                type: "POST",
                url: '/Quiz/QuizQuestions/AddCorrectAnswer',
                data: { id: answerid, response: answer, questionType: questiontype, questionId: questionid },
                dataType: "json",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (data.Success) {
                        $("#myModal").modal('hide');
                    }
                }
            });
        },
        imageUploader = function () {
            $(".quizImgUploader").fileinput({
                language: translatedResources.locale,
                theme: "fas",
                uploadUrl: "/Forms/FormsQuestions/UploadMTPFiles",
                showUpload: true,
                showCaption: false,
                showRemove: true,
                allowedFileExtensions: ["jpg", "jpeg", "gif", "png"],
                uploadAsync: true,
                minFileCount: 1,
                maxFileCount: 1,
                browseClass: "btn btn-sm m-0 z-depth-0 btn-browse",
                removeClass: "btn btn-sm m-0 z-depth-0 removeFiles",
                uploadClass: "btn btn-sm m-0 pl-3 pr-3 z-depth-0",
                overwriteInitial: false,
                initialPreviewAsData: true, // defaults markup
                initialPreviewConfig: [{
                    frameAttr: {
                        title: 'My Custom Title',
                    }
                }],
                uploadExtraData: {
                    img_key: "1000",
                    img_keywords: "happy, nature"
                },
                fileActionSettings: {
                    showZoom: false,
                    showUpload: false,
                    //indicatorNew: "",
                    showDrag: false,
                    showRemove: false
                },
                preferIconicPreview: false, // this will force thumbnails to display icons for following file extensions
                previewSettings: {
                    image: { width: "50px", height: "auto" },
                    html: { width: "50px", height: "auto" },
                    other: { width: "50px", height: "auto" }
                },
            }).on('fileuploaded', function (event, data, previewId, index) {
                //debugger;
                var fileName = data.response;
                var resourceID = '#' + this.dataset.controlname;
                var fileId = '#' + this.dataset.controlid;
                $(fileId).addClass('disabled');
                if (targetResourceId != resourceID) {
                    targetResource = true;
                }
                targetResourceId = resourceID;
                $(resourceID).val(fileName);
                if (targetResource) {
                    globalFunctions.showSuccessMessage(translatedResources.FileUploaded);
                }
                targetResource = false;
            });
            $('.quizImgUploader').on('filedeleted', function (event, data, previewId, index) {
                //debugger;
                //console.log('Key = ' + key);
            });

            $('.removeFiles').on('click', function (event, data, previewId, index) {
                var fileId = '#' + $(this).closest('div').find('input.quizImgUploader').data('controlid');
                var resourceID = '#' + $(this).closest('div').find('input.quizImgUploader').data('controlname');
                $(fileId).removeClass('disabled');
                $(resourceID).val('');
            });
        },
        deleteQuizQuestionFile = function (quizQuestionFileid, quizQuestionId, fileName, uploadedfilepath) {
            //debugger;
            $.ajax({
                type: 'POST',
                data: { id: quizQuestionFileid, quizQuestionId: quizQuestionId, fileName: fileName, UploadedFilePath: uploadedfilepath },
                url: '/Quiz/QuizQuestions/DeleteQuizQuestionFile',
                success: function (result) {
                    //debugger;
                    globalFunctions.showMessage('success', translatedResources.DeletedSuccessfully);
                    $("#fileList").html(result);
                },
                error: function (msg) {
                    //debugger;
                    quizquestions.onFailure;
                }
            });

        },
        validationQuizQuestionImage = function (e) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'];

            if ($.inArray($('input[type=file]').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                globalFunctions.showErrorMessage(translatedResources.fileTypeValidationMessage.replace("{0}", fileExtension.join(', ')));
                $('input[type=file]').val('');
            }
        },
        addCorrectAnswer = function (source, id) {
            var title = translatedResources.AddLabel + translatedResources.correctAnswer;
            globalFunctions.loadPopup(source, '/Quiz/QuizQuestions/AddCorrectAnswer/' + id, title, 'quiz-correct-answer-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
            });
        },
        loadImportQuestionPopup = function (source) {
            var title = translatedResources.ImportQuiz;
        globalFunctions.loadPopup(source, '/Forms/FormsQuestions/LoadImportFormQuestionPopup', title, 'import-quiz-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $("#ImportFormQuestions").fileinput({
                    language: translatedResources.locale,
                    theme: "fas",
                    showUpload: true,
                    showCaption: false,
                    showMultipleNames: false,
                    multiple: false,
                    uploadUrl: "/Quiz/QuizQuestions/ImportQuizQuestions",
                    uploadExtraData: function () {
                        return {
                            quizId: $("#QuizId").val(),
                        };
                    },
                    fileActionSettings: {
                        showZoom: false,
                        showUpload: false,
                        //indicatorNew: "",
                        showDrag: false,
                        showRemove: false
                    },
                    allowedFileExtensions: ["xlsx", "xls"],
                    uploadAsync: true,
                    uploadedfilepath: false,
                    minFileCount: 1,
                    maxFileCount: 1,
                    browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
                    removeClass: "btn btn-sm m-0 z-depth-0",
                    uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
                    cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
                    overwriteInitial: true,
                    //previewFileIcon: '<i class="fas fa-file"></i>',
                    initialPreviewAsData: false, // defaults markup
                    initialPreviewConfig: [{
                        uploadedfilepath: false,
                        //progress: "d-none",
                        frameAttr: {
                            title: 'My Custom Title',
                        }
                    }],
                    preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
                    previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
                    previewSettings: fileInputControlSettings.previewSettings,
                    previewFileExtSettings: fileInputControlSettings.previewFileExtSettings

                }).on('fileuploaded', function (event, data, previewId, index) {
                    //debugger;
                    //$('.progress').addClass('d-none');
                    if (!data.response.Success) {
                        window.location.href = "/Quiz/QuizQuestions/DownloadImportFile";
                    }
                    globalFunctions.showMessage(data.response.NotificationType, data.response.Message);
                    if (data.response.Message != translatedResources.InvalidFormatMessage && data.response.Message != translatedResources.InvalidColumnMessage) {
                        $('#myModal').modal('hide');
                        if (data.response.Success) {
                            window.location.reload();
                        }
                    }

                });
            })
        },
        noSort = function () {
            var table = $('#tbl-QuizQuestion').DataTable();
            table.destroy();
            $('#tbl-QuizQuestion').DataTable({
                "columnDefs": [{
                    "targets": "no-sort",
                    "orderable": false
                }],
                "oLanguage": _dataTableLangSettings
            });
            $("#tbl-QuizQuestion_length").hide();
            $("#tbl-QuizQuestion_filter").hide();
        };
    return {
        addQuizQuestionsPopup: addQuizQuestionsPopup,
        editQuizQuestionsPopup: editQuizQuestionsPopup,
        deleteQuizQuestionData: deleteQuizQuestionData,
        onSaveSuccess: onSaveSuccess,
        onFailure: onFailure,
        correctAnswer: correctAnswer,
        imageUploader: imageUploader,
        addCorrectAnswer: addCorrectAnswer,
        loadImportQuestionPopup: loadImportQuestionPopup,
        deleteQuizQuestionFile: deleteQuizQuestionFile,
        validationQuizQuestionImage: validationQuizQuestionImage,
        noSort: noSort

    };
}();

$(document).ready(function () {
    $("#tbl-QuizQuestion").DataTable({
        "columnDefs": [{
            "targets": "no-sort",
            "orderable": false
        }],
        "oLanguage": _dataTableLangSettings
    });
    $("#tbl-QuizQuestion_filter").hide();
    $("#tbl-QuizQuestion_length").hide();
    $('#tbl-QuizQuestion').DataTable().search('').draw();
    $("#QuestionListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-QuizQuestion').DataTable().search($(this).val()).draw();
    });
    //$(document).on('click', '#btnAddQuizQuestion', function () {
    //    quizquestions.addQuizQuestionsPopup($(this));
    //});

    $(document).on('click', '#addResponse', function (e) {
        var response = $("#Response").val();
        if (response === null || response === "") {
            e.preventDefault();
            return false;
        }
        //var htmlString = '<span class="responses col-12">' + response + '<input type="hidden" value="' + response + '" class="responseInput"><span type="button" class="pull-right delete-response">×</span></span>';
        var htmlString = '<span class="responses badge badge-primary p-2 mr-2 shadow-none">' + response + '<input type="hidden" value="' + response + '" class="responseInput"><a href="javascript:void(0)" class="delete-response ml-1 white-text">×</a></span>';
        $("#response-container").append(htmlString);
        $("#Response").val("");
        e.preventDefault();
    });

    $(document).on('click', '.delete-response', function (e) {
        $(this).parent(".responses").remove();
    });

    $(document).on('click', '#btn-submit', function (e) {
        if ($("#QuestionTypeId").val() == 6) {
            var optionCount = $("#fibCount").val();
            if (optionCount.trim() == '') {
                $('#fibError').html('');
                $('#fibError').html(translatedResources.Mandatory);
            }
        }
        if ($('#frmAddQuizQuestion').valid()) {
            e.preventDefault();
            var QuestionTypeId = $("#QuestionTypeId").val();
            var QuizQuestionId = $("#QuizQuestionId").val();
            if (QuestionTypeId === null || QuestionTypeId === "") {
                return false;
            }
            if (QuizQuestionId === null || QuizQuestionId === "") {
                return false;
            }
            var responseInputs = $(".responseInput");
            //console.log(responseInputs);
            //if (responseInputs.length > 0) {
            var list = [];
            var isValid = true;
            var isAnyChecked = false;
            //validation
            //for (var i = 0; i < responseInputs.length; i++) {
            //    //var isChecked = $("#chkbox_" + (i + 1)).attr("checked") ? true : false;
            //    let isChecked = $("#chkbox_" + (i + 1)).is(":checked");
            //    if (isChecked) {
            //        isAnyChecked = true;
            //    }
            //    if (jQuery.trim(responseInputs[i].value).length == 0 && QuestionTypeId != 4) {
            //        $(responseInputs[i]).focus();
            //        $(responseInputs[i]).closest('.element').effect("pulsate", {}, 3000);
            //        globalFunctions.showWarningMessage(translatedResources.EnterAnserValidation);
            //        Isvalid = false;
            //        return false;
            //    }
            //    if (QuestionTypeId == 8) {
            //        var imagePath = $("#MTPAnswer_" + (i + 1)).val();
            //        if (imagePath == '') {
            //            globalFunctions.showWarningMessage(translatedResources.CheckImage);
            //            Isvalid = false;
            //            return false;
            //        }
            //    }
            //}
            //if (QuestionTypeId != 4 && QuestionTypeId != 7 && QuestionTypeId != 6 && QuestionTypeId != 8) {
            //    if (!isAnyChecked) {
            //        globalFunctions.showWarningMessage(translatedResources.MarkAnswerValidation);
            //        Isvalid = false;
            //        return false;
            //    }
            //}
            if (QuestionTypeId == 6) {
                var optionCount = $("#fibCount").val();
                var value = parseInt(optionCount);
                if (optionCount.trim() == '') {
                    $('#fibError').html('');
                    $('#fibError').html(translatedResources.Mandatory);
                    Isvalid = false;
                    return false;
                } else if (value - 0 == 0) {
                    Isvalid = false;
                    return false;
                }
            }
            //push to array
            if (QuestionTypeId != 4 && QuestionTypeId != 7 && QuestionTypeId != 6 && QuestionTypeId != 8) {
                for (var i = 0; i < responseInputs.length; i++) {
                    //var isChecked = $("#chkbox_" + (i + 1)).attr("checked") ? true : false;
                    let isChecked = $("#chkbox_" + (i + 1)).is(":checked");
                    if (jQuery.trim(responseInputs[i].value).length)
                        list.push({ QuizAnswerId: null, AnswerText: responseInputs[i].value, SortOrder: (i + 1), IsCorrectAnswer: isChecked, ResourcePath: null });
                }
            }
            //for free text
            if (QuestionTypeId == 4) {
                list.push({ QuizAnswerId: null, AnswerText: 'freetext', SortOrder: 0, IsCorrectAnswer: false, ResourcePath: null });
            }
            //for match the order
            if (QuestionTypeId == 7) {
                for (var i = 0; i < responseInputs.length; i++) {
                    //let isChecked = $("#chkbox_" + (i + 1)).is(":checked");
                    //if (jQuery.trim(responseInputs[i].value).length)
                    list.push({ QuizAnswerId: null, AnswerText: responseInputs[i].value, SortOrder: $("#txtAnswer_" + (i + 1)).val(), IsCorrectAnswer: true, ResourcePath: null });
                }
            }
            //for fill in the blank
            if (QuestionTypeId == 6) {
                for (var i = 0; i < responseInputs.length; i++) {
                    //let isChecked = $("#chkbox_" + (i + 1)).is(":checked");
                    //if (jQuery.trim(responseInputs[i].value).length)
                    list.push({ QuizAnswerId: null, AnswerText: responseInputs[i].value, SortOrder: i + 1, IsCorrectAnswer: true, ResourcePath: null });
                }
            }
            //for match the pair
            if (QuestionTypeId == 8) {
                for (var i = 0; i < responseInputs.length; i++) {
                    //let isChecked = $("#chkbox_" + (i + 1)).is(":checked");
                    //if (jQuery.trim(responseInputs[i].value).length)
                    list.push({ QuizAnswerId: null, AnswerText: responseInputs[i].value, SortOrder: i + 1, IsCorrectAnswer: true, ResourcePath: $("#MTPAnswer_" + (i + 1)).val() });
                }
            }
            if (true) {
                var formData = new FormData();
                //if (globalFunctions.isValueValid(document.getElementById("QuizImage"))) {
                //    var file = document.getElementById("QuizImage").files[0]
                //        formData.append("QuizImage", file);
                //}

                var questionText = CKEDITOR.instances.QuestionText.getData();

                if (!$('#quizQuestionImage').attr('src') == '') {
                    formData.append("QuestionImagePath", $('#quizQuestionImage').attr('src'));
                }
                else if ($('#QuestionImagePath').val() !== undefined && $('#QuestionImagePath').val() !== '') {
                    formData.append("QuestionImagePath", $('#QuestionImagePath').val());
                }
                else if (!questionText.replace(/<[^>]*>/gi, '').length) {
                    globalFunctions.showWarningMessage('Please provide required question text');
                    return;
                }

                formData.append("QuestionTypeId", $("#QuestionTypeId").val());
                formData.append("QuizQuestionId", $("#QuizQuestionId").val());
                formData.append("QuestionText", CKEDITOR.instances.QuestionText.getData());
                formData.append("QuizId", $("#QuizId").val());
                formData.append("IsAddMode", $('#IsAddMode').val());
                formData.append("Marks", $('#Marks').val());
                formData.append("IsRequired", $("#IsRequired").attr("checked") ? true : false);
                formData.append("SubjectIds", $("#SubjectIds").val());

                //formData.append("Answers", JSON.stringify(list));

                for (var i = 0; i < list.length; i++) {
                    formData.append("Answers[" + i + "].AnswerText", list[i].AnswerText);
                    formData.append("Answers[" + i + "].SortOrder", list[i].SortOrder)
                    formData.append("Answers[" + i + "].QuizAnswerId", list[i].QuizAnswerId)
                    formData.append("Answers[" + i + "].IsCorrectAnswer", list[i].IsCorrectAnswer)
                    formData.append("Answers[" + i + "].ResourcePath", list[i].ResourcePath)
                }
                //var formData = {

                //        QuestionTypeId: $("#QuestionTypeId").val(),
                //        QuizQuestionId: $("#QuizQuestionId").val(),
                //        QuestionText: $("#QuestionText").val(),
                //        QuizId: $("#QuizId").val(),
                //        IsAddMode: $("#IsAddMode").val(),
                //        Marks: $("#Marks").val(),
                //        IsRequired: $("#IsRequired").attr("checked") ? true : false,
                //        Answers: list,
                //        QuizImage: file


                //    };

                $.ajax({
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    url: '/Forms/FormsQuestions/SaveQuizQuestion',
                    //data: { model: formData, answersList: list },
                    data: formData,
                    success: function (result) {
                        if (result.Success) {
                            //$("#myModal").modal('hide');
                            window.location.href = '/Forms/FormsQuestions/';
                        }
                        else {
                            globalFunctions.showWarningMessage(result.Message);
                        }
                    },
                    error: function (msg) {
                        quizquestions.onFailure;
                    }
                });
            }
            //} else {
            //    $("#ResponseValidation").text(translatedResources.ResponseValidation);
            //    return false;
            //}
        }

    });
    //

    // Add new element
    $('body').on('click', '.add', function () {
        var QTypeId = $('#QuestionTypeId').val();
        // Finding total number of elements added
        var total_element = $(".element").length;

        // last <div> with element class id
        var lastid = $(".element:last").attr("id");
        var split_id = lastid.split("_");
        var nextindex = Number(split_id[1]) + 1;

        //var max = 5;
        // Check total number elements
        //if (total_element < max) {
        // Adding new div container after last occurance of element class
        $(".element:last").after("<div class='element clearfix mb-3' id='div_" + nextindex + "'></div>");

        // Adding element to <div>
        if (QTypeId == 1 || QTypeId == 2) {
            $("#div_" + nextindex).append("<div class='quiz-answer-field row mb-4'><div class='col-md-7 col-12'><div class='md-form md-outline mb-0'><input type='text'  class='responseInput form-control' id='txt_" + nextindex + "'> <label for='txt_" + nextindex + "'>" + translatedResources.EnterAnswer + "</label></div></div> <div class='col-md-5 col-12 d-flex justify-content-between align-items-center'> <button type='button' id='remove_" + nextindex + "' class='remove btn btn-outline-danger m-0 mr-1'><i class='fas fa-plus'></i></button></div></div>");
        }
        else if (QTypeId == 7) {
            $("#div_" + nextindex).append("<div class='quiz-answer-field'><input type='text'  class='responseInput form-control' id='txt_" + nextindex + "'> <label for='txt_" + nextindex + "'>" + translatedResources.EnterAnswer + "</label> <input type='text'  placeholder='" + translatedResources.CorrectAnswer + "' id='txtAnswer_" + nextindex + "'></div>  <button class='remove btn btn-outline-danger m-0' type='button' id='remove_" + nextindex + "'><i class='fas fa-plus'></i></button>");
        }
        else if (QTypeId == 8) {
            $("#div_" + nextindex).append("<hr><div class='quiz-answer-field'><div class='row'><div class='col-md-7 col-12'><div class='md-form md-outline'><input type='text'  class='responseInput mb-3 form-control' id='txt_" + nextindex + "'><label for='txt_" + nextindex + "'>" + translatedResources.EnterAnswer + "</label> <input type='text' class='d-none' id='MTPAnswer_" + nextindex + "'></div></div><div class='col-md-5 col-12'> <button class='float-right remove btn btn-outline-danger m-0' type='button' data-toggle='tooltip' data-placement='top' title='Remove' id='remove_" + nextindex + "'><i class='fas fa-plus'></i></button></div></div><div class='uploaderWidget quizFileUploader'><input class='quizImgUploader' type='file' id='file_" + nextindex + "' data-controlid='file_" + nextindex + "' data-controlname='MTPAnswer_" + nextindex + "'></div></div>  ");
            quizquestions.imageUploader()
        }
        else {
            $("#div_" + nextindex).append("<div class='quiz-answer-field row mb-4'><div class='col-md-7 col-12'><div class='md-form md-outline mb-0'><input type='text'  class='responseInput form-control' id='txt_" + nextindex + "'> <label for='txt_" + nextindex + "'>" + translatedResources.EnterAnswer + "</label></div></div> <div class='col-md-5 col-12 d-flex justify-content-between align-items-center'><button type='button' id='remove_" + nextindex + "' class='remove btn btn-outline-danger m-0 mr-1'><i class='fas fa-plus'></i></button></div></div>");
        }
        //}
    });
    //$('body').on('click', '.add', function () {
    //    var QTypeId = $('#QuestionTypeId').val();
    //    var total_element = $(".element").length;
    //    var lastid = $(".element:last").attr("id");
    //    var split_id = lastid.split("_");
    //    var nextindex = Number(split_id[1]) + 1;
    //    $(".element:last").after("<div class='element' id='div_" + nextindex + "'></div>");
    //    if (QTypeId == 1 || QTypeId == 2) {

    //        $("#div_" + nextindex).append("<div class='quiz-answer-field'><input type='text'  class='responseInput' placeholder='" + translatedResources.EnterAnswer + "' id='txt_" + nextindex + "'>  <div class='custom-control custom-radio custom-control-inline mr-0'><input type='radio' class='custom-control-input' name='single1' id='chkbox_" + nextindex + "' ><label class='custom-control-label' for='chkbox_" + nextindex + "'>" + translatedResources.MarkAsAnswer + "</label> </div></div>  <button type='button' id='remove_" + nextindex + "' class='remove btn btn-outline-primary m-0'><i class='fas fa-plus'></i></button>");
    //    }
    //    else if (QTypeId == 7) {
    //        $("#div_" + nextindex).append("<div class='quiz-answer-field'><input type='text'  class='responseInput' placeholder='" + translatedResources.EnterAnswer + "' id='txt_" + nextindex + "'>  <input type='text'  placeholder='" + translatedResources.CorrectAnswer + "' id='txtAnswer_" + nextindex + "'></div>  <button class='remove btn btn-outline-primary m-0' type='button' id='remove_" + nextindex + "'><i class='fas fa-plus'></i></button>");
    //    }
    //    else if (QTypeId == 8) {
    //        $("#div_" + nextindex).append("<div class='quiz-answer-field'><input type='text'  class='responseInput mb-3' placeholder='" + translatedResources.EnterAnswer + "' id='txt_" + nextindex + "'> <input type='text' class='d-none' id='MTPAnswer_" + nextindex + "'><div class='pt-3 pb-3 mr-3'><div class='standalone-uploader quizFileUploader'><input class='quizImgUploader' type='file' id='file_" + nextindex + "' data-controlid='file_" + nextindex + "' data-controlname='MTPAnswer_" + nextindex + "'></div></div></div>  <button class='remove btn btn-outline-primary m-0' type='button' id='remove_" + nextindex + "'><i class='fas fa-plus'></i></button>");
    //        quizquestions.imageUploader()
    //    }
    //    else {
    //        $("#div_" + nextindex).append("<div class='quiz-answer-field'><input type='text'  class='responseInput' placeholder='" + translatedResources.EnterAnswer + "' id='txt_" + nextindex + "'>  <div class='custom-control custom-checkbox custom-control-inline mr-0'><input type='checkbox' class='custom-control-input' id='chkbox_" + nextindex + "' ><label class='custom-control-label' for='chkbox_" + nextindex + "'>" + translatedResources.MarkAsAnswer + "</label> </div></div>  <button type='button' id='remove_" + nextindex + "' class='remove btn btn-outline-primary m-0'><i class='fas fa-plus'></i></button>");
    //    }
    //});
    //$('body').on('change', '#Marks', function () {
    //    var marks = $("#Marks").val();
    //});
    $('#QuestionTypeId').on('change', function () {
        ////debugger;
        if ($(this).val() != "") {
            $("#QuestionTypeId-error").addClass('d-none');
        }

        if ($(this).val() == 4) {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").hide();
            $(".element").remove();
            $("#answerSection").append("<div class='element clearfix' id='div_1'></div>");
            // Adding element to <div>
            $("#div_1").append("<div class='quiz-answer-field'><input type='text'  class='responseInput d-none form-control' placeholder='" + translatedResources.EnterAnswer + "' id='txt_1'>  <div class='custom-control custom-checkbox custom-control-inline mr-0'> </div></div> ");
        }
        else if ($(this).val() == 1 || $(this).val() == 2) {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").show();
            $(".element").remove();
            $("#answerSection").append("<div class='element clearfix' id='div_1'></div>");
            $("#div_1").append("<div class='quiz-answer-field row mb-4'><div class='col-md-7 col-12'><div class='md-form md-outline mb-0'><input type='text'  class='responseInput form-control mb-0' id='txt_1'> <label for='txt_1'>" + translatedResources.EnterAnswer + "</label></div></div><div class='col-md-5 col-12 d-flex justify-content-between align-items-center'><button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button></div></div>");
            if ($(this).val() == 1) {
                setTimeout(function () {
                    $('.add').click();
                }, 100); // for 3 second delay
            }
        }
        else if ($(this).val() == 7) {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").show();
            $(".element").remove();
            $("#answerSection").append("<div class='element clearfix' id='div_1'></div>");
            $("#div_1").append("<div class='quiz-answer-field'><input type='text'  class='responseInput form-control' placeholder='" + translatedResources.EnterAnswer + "' id='txt_1'>  <input type='text'  placeholder='" + translatedResources.CorrectAnswer + "' id='txtAnswer_1'></div>  <button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button>");
        }
        else if ($(this).val() == 8) {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").show();
            $(".element").remove();
            $("#answerSection").append("<div class='element clearfix mt-3' id='div_1'></div>");
            $("#div_1").append("<div class='quiz-answer-field'><div class='row'><div class='col-md-7 col-12'><div class='md-form md-outline'> <input type='text'  class='responseInput mb-3 form-control' id='txt_1'> <label for='txt_1'>" + translatedResources.EnterAnswer + "</label></div> <input type='text' class='d-none' id='MTPAnswer_1'>  </div><div class='col-md-5 col-12'><button class='float-right add btn btn-outline-primary m-0' type='button' data-toggle='tooltip' data-placement='top' title='Add'><i class='fas fa-plus'></i></button></div></div><div class='pb-3' id='fileUploader1'><div class='uploaderWidget quizFileUploader'><input class='quizImgUploader' type='file' id='file_1' data-controlid='file_1' data-controlname='MTPAnswer_1' data-previewimage='previewImage1' ></div></div></div>  ");
            quizquestions.imageUploader();
        }
        else if ($(this).val() == 6) {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").hide();
            $(".element").remove();
            $("#fibMessage").html(translatedResources.FIBMessage);
            $("#fibSelector").append("<div class='element clearfix' id='div_1'></div>");
            $("#div_1").append("<div class='quiz-answer-field w-100'><input class='w-100 form-control mb-0' type='text' id='fibCount'><label for='fibCount'>" + translatedResources.FIBCount + "</label><span class='field-validation-error d-inline-block'><span id='fibError' class=''></span></span> </div>  ");

        }
        else {
            $("#fibMessage").html("");
            $('#Marks').val(1);
            CKEDITOR.instances.QuestionText.setData('');
            $("#answerHeader").show();
            $(".element").remove();
            $("#answerSection").append("<div class='element clearfix' id='div_1'></div>");
            $("#div_1").append("<div class='quiz-answer-field row mb-4'><div class='col-md-7 col-12'><div class='md-form md-outline mb-0'><input type='text'  class='responseInput form-control mb-0' id='txt_1'> <label for='txt_1'>" + translatedResources.EnterAnswer + "</label></div></div><div class='col-md-5 col-12 d-flex justify-content-between align-items-center'><button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button></div></div>");
            setTimeout(function () {
                $('.add').click();
                $('.add').click();
            }, 100); // for 3 second delay 

        }
    });
    //$('#QuestionTypeId').on('change', function () {
    //    //debugger;
    //    if ($(this).val() == 4) {
    //        $("#fibMessage").html("");
    //        $('#Marks').val(0);
    //        $('#QuestionText').val('');
    //        $("#answerHeader").hide();
    //        $(".element").remove();
    //        $("#answerSection").append("<div class='element' id='div_1'></div>");
    //        $("#div_1").append("<div class='quiz-answer-field'><input type='text'  class='responseInput d-none' placeholder='" + translatedResources.EnterAnswer + "' id='txt_1'>  <div class='custom-control custom-checkbox custom-control-inline mr-0'> </div></div> ");
    //    }
    //    else if ($(this).val() == 1 || $(this).val() == 2) {
    //        $("#fibMessage").html("");
    //        $('#Marks').val(0);
    //        $('#QuestionText').val('');
    //        $("#answerHeader").show();
    //        $(".element").remove();
    //        $("#answerSection").append("<div class='element' id='div_1'></div>");
    //        $("#div_1").append("<div class='quiz-answer-field'><input type='text'  class='responseInput' placeholder='" + translatedResources.EnterAnswer + "' id='txt_1'>  <div class='custom-control custom-radio custom-control-inline mr-0'><input type='radio' class='custom-control-input' id='chkbox_1' name='single1'><label class='custom-control-label' for='chkbox_1'>" + translatedResources.MarkAsAnswer + "</label> </div></div>  <button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button>");
    //    }
    //    else if ($(this).val() == 7) {
    //        $("#fibMessage").html("");
    //        $('#Marks').val(0);
    //        $('#QuestionText').val('');
    //        $("#answerHeader").show();
    //        $(".element").remove();
    //        $("#answerSection").append("<div class='element' id='div_1'></div>");
    //        $("#div_1").append("<div class='quiz-answer-field'><input type='text'  class='responseInput' placeholder='" + translatedResources.EnterAnswer + "' id='txt_1'>  <input type='text'  placeholder='" + translatedResources.CorrectAnswer + "' id='txtAnswer_1'></div>  <button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button>");
    //    }
    //    else if ($(this).val() == 8) {
    //        $("#fibMessage").html("");
    //        $('#Marks').val(0);
    //        $('#QuestionText').val('');
    //        $("#answerHeader").show();
    //        $(".element").remove();
    //        $("#answerSection").append("<div class='element' id='div_1'></div>");
    //        $("#div_1").append("<div class='quiz-answer-field'><input type='text'  class='responseInput mb-3' placeholder='" + translatedResources.EnterAnswer + "' id='txt_1'><input type='text' class='d-none' id='MTPAnswer_1'><div class='pt-3 pb-3 mr-3'><div class='standalone-uploader quizFileUploader'><input class='quizImgUploader' type='file' id='file_1' data-controlid='file_1' data-controlname='MTPAnswer_1'></div></div></div>  <button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button>");
    //        quizquestions.imageUploader();
    //    }
    //    else if ($(this).val() == 6) {
    //        $("#fibMessage").html("");
    //        $('#Marks').val(0);
    //        $('#QuestionText').val('');
    //        $("#answerHeader").hide();
    //        $(".element").remove();
    //        $("#fibMessage").html(translatedResources.FIBMessage);
    //        $("#fibSelector").append("<div class='element' id='div_1'></div>");
    //        $("#div_1").append("<div class='quiz-answer-field w-100'><input class='w-100' type='text' placeholder='" + translatedResources.FIBCount + "' id='fibCount'><span class='field-validation-error d-inline-block'><span id='fibError' class=''></span></span> </div>  ");
    //    }
    //    else {
    //        $("#fibMessage").html("");
    //        $('#Marks').val(0);
    //        $('#QuestionText').val('');
    //        $("#answerHeader").show();
    //        $(".element").remove();
    //        $("#answerSection").append("<div class='element' id='div_1'></div>");
    //        $("#div_1").append("<div class='quiz-answer-field'><input type='text'  class='responseInput' placeholder='" + translatedResources.EnterAnswer + "' id='txt_1'>  <div class='custom-control custom-checkbox custom-control-inline mr-0'><input type='checkbox' class='custom-control-input' id='chkbox_1'><label class='custom-control-label' for='chkbox_1'>" + translatedResources.MarkAsAnswer + "</label> </div></div>  <button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button>");
    //    }
    //});
    $('body').on('keypress keyup blur', '#fibCount', function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('body').on('change', '#fibCount', function (event) {
        //debugger;
        $("#answerHeader").show();
        var optionCount = $(this).val();
        var value = parseInt(optionCount);
        if (optionCount.trim() == '') {
            $('#fibError').html(translatedResources.Mandatory);
            return false;
        }
        else {
            $('#fibError').html('');
        }
        if (value - 0 == 0) {
            $('#fibError').html(translatedResources.MoreThanZero);
            return false;
        }
        else {
            $('#fibError').html('');
        }
        if ($('#IsAddMode').val() == true) {
            $("#answerHeader").show();
            $(".fibelement").remove();
            var i = 1;
            while (i <= optionCount) {
                $("#answerSection").append("<div class='fibelement element' id='fibDiv_" + i + "'></div>");
                $("#fibDiv_" + i + "").append("<div class='quiz-answer-field'><span>blank_space_" + i + "</span>   <input type='text'  class='responseInput' placeholder='" + translatedResources.EnterAnswer + "' id='fibTxt_" + i + "'></div>");
                i++;
            }
        }
        else {
            var responseInputs = $(".responseInput");
            var i = responseInputs.length;
            if (i <= optionCount) {
                i++;
                while (i <= optionCount) {
                    $("#answerSection").append("<div class='fibelement element' id='fibDiv_" + i + "'></div>");
                    $("#fibDiv_" + i + "").append("<div class='quiz-answer-field'><span>blank_space_" + i + "</span>   <input type='text'  class='responseInput' placeholder='" + translatedResources.EnterAnswer + "' id='fibTxt_" + i + "'></div>");
                    i++;
                }
            }
            else {
                while (i > optionCount) {
                    var selector = "#fibDiv_" + i + "";
                    $(selector).remove();
                    i--;
                }
            }
        }

    });
    //$('body').on('change', '#QuestionTypeId', function () {
    //    if ($(this).val() == 4) {
    //        $(".element").remove();
    //        $("#answerSection").append("<div class='element' id='div_1'></div>");
    //        // Adding element to <div>
    //        $("#div_1").append("<div class='quiz-answer-field'><input type='text'  class='responseInput' placeholder='" + translatedResources.EnterAnswer + "' id='txt_1'>  </div> ");
    //    }
    //    else {
    //        $(".element").remove();
    //        $("#answerSection").append("<div class='element' id='div_1'></div>");
    //        $("#div_1").append("<div class='quiz-answer-field'><input type='text'  class='responseInput' placeholder='" + translatedResources.EnterAnswer + "' id='txt_1'>  </div>  <button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i></button>");
    //    }
    //});

    // Remove element
    $('body').on('click', '.remove', function () {
        ////debugger;
        if (parseInt($('#QuestionTypeId').val()) === 3) {
            if ($('.remove').length >= 2) {
                var id = this.id;
                var split_id = id.split("_");
                var deleteindex = split_id[1];
                $("#div_" + deleteindex).remove();
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.multipleQuestion);
            }
        }
        else if (parseInt($('#QuestionTypeId').val()) === 1) {
            if ($('.remove').length > 1) {
                var id = this.id;
                var split_id = id.split("_");
                var deleteindex = split_id[1];
                $("#div_" + deleteindex).remove();
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.singleQuestion);
            }
        }
        else {
            var id = this.id;
            var split_id = id.split("_");
            var deleteindex = split_id[1];
            $("#div_" + deleteindex).remove();
        }


    });
    $('body').on('click', '#DeleteQuizQuestionFiles', function () {
        ////debugger;
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        var fileName = $(this).attr('data-fileName');
        var uploadedfilepath = $(this).attr('data-uploadedPath');
        var quizQuestionFileId = $(this).attr('data-FileId');
        var quizQuestionId = $(this).attr('data-QuizQuestionId');
        $(document).bind('okClicked', $(this), function (e) {
            quizquestions.deleteQuizQuestionFile(quizQuestionFileId, quizQuestionId, fileName, uploadedfilepath);
        });
    });
    $(document).on('click', '#ViewQuizQuestionFile', function (e) {
        e.preventDefault();
        if ($("#IsAddMode").val() == "False") {
            var id = $(this).data("id");
            if (globalFunctions.isValueValid(id)) {
                window.open("/Document/Viewer/Index?id=" + id + "&module=editQuizQuestion", "_blank");
            }
        }
        else {
            var id = $(this).data("filename");
            if (globalFunctions.isValueValid(id)) {
                window.open("/Document/Viewer/Index?id=" + id + "&module=addeditQuizQuestion", "_blank");
            }
        }
    });
    $('body').on('click', '.custom-control-label', function () {
        let chkBox = $(this).parent('.custom-checkbox').find('input');
        //console.log(chkBox.attr('id'));
        $(this).parent('.custom-checkbox').find('input').change(function () {
            if ($(this).parent('.custom-checkbox').find('input').is(":checked")) {

                chkBox.attr("checked", true);
            } else {
                chkBox.removeAttr("checked");

            }
        })

        //for radio
        let rbBox = $(this).parent('.custom-radio').find('input');
        //console.log(chkBox.attr('id'));
        $(this).parent('.custom-radio').find('input').change(function () {
            if ($(this).parent('.custom-radio').find('input').is(":checked")) {
                $('#answerSection .custom-control-input').removeAttr("checked");
                rbBox.attr("checked", true);
            } else {
                rbBox.removeAttr("checked");

            }
        })
    });
    //$('body').on('click', '.custom-control-label', function () {
    //    let chkBox = $(this).parent('.custom-checkbox').find('input');
    //    //console.log(chkBox.attr('id'));
    //    $(this).parent('.custom-checkbox').find('input').change(function () {
    //        if ($(this).parent('.custom-checkbox').find('input').is(":checked")) {

    //            chkBox.attr("checked", true);
    //        } else {
    //            chkBox.removeAttr("checked");

    //        }
    //    })

    //});
    CKEDITOR.plugins.addExternal('ckeditor_wiris', '', 'plugin.js');
    CKEDITOR.replace('QuestionText', {
        htmlencodeoutput: false,
        height: '180px',
        extraPlugins: 'ckeditor_wiris',
        allowedContent: true,
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });

    CKEDITOR.instances.QuestionText.on('change', function () {
        if (CKEDITOR.instances.QuestionText.getData().replace(/<[^>]*>/gi, '').length > 0) {
            $("#QuestionTextError").addClass("d-none");
        }
    });
});