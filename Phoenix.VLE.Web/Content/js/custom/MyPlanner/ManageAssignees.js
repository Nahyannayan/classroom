﻿/// <reference path="../../common/global.js" />
var thinkBox = function () {

    var init = function () {
        loadThinkBoxUsersGrid();
        $("#tbl-ThinkBoxList_filter").hide();
    },
        addThinkBoxUsers = function (source) {
            window.location.href = '/thinkbox/assignusers';
        },
        deleteThinkBoxUser = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('ThinkBoxUser', '/SecurityManagement/ThinkBox/DeleteThinkBoxUser', 'tbl-ThinkBoxList', source, id);
            }
        },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },
        onFailure = function (data) {
            globalFunctions.showMessage("error", data.Message);
        },
        loadThinkBoxUsersGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "UserName", "sTitle": translatedResources.thinkBoxUser, "sWidth": "70%", "sClass": "wordbreak" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "30%" }
            );
            initSuggestionCategoryGrid('tbl-ThinkBoxList', _columnData, '/SecurityManagement/ThinkBox/LoadThinkBoxUsersGrid');
        },

        initSuggestionCategoryGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 1,
                    'orderable': false
                }]
            };
            grid.init(settings);
        };

    return {
        init: init,
        onSaveSuccess: onSaveSuccess,
        onFailure: onFailure,
        addThinkBoxUsers: addThinkBoxUsers,
        deleteThinkBoxUser: deleteThinkBoxUser,
        loadThinkBoxUsersGrid: loadThinkBoxUsersGrid
    };
}();

$(document).ready(function () {
    thinkBox.init();
    $(document).on('click', '#btnAddThinkBoxUsers', function () {
        thinkBox.addThinkBoxUsers($(this));
    });
    $(".manageAssignee-wrapper").mCustomScrollbar({
        setHeight: "250",
        autoExpandScrollbar: true,
    });
    $("#btnCancel").click(function () {
        location.href = "/securitymanagement/thinkbox";
    });
    $("#btnAddThinkBoxUser").click(function () {
        var $selectedUsers = $("#UsersList input[type='checkbox']:checked").parent('.custom-checkbox');
        var usersListId = $("#UsersListId option:selected").val();
        var output = jQuery.map($('#SelectedUsersList :checkbox'), function (n, i) {
            return n.value;
        }).join(',');
        if ($selectedUsers != null && $selectedUsers.length > 0) {
            $selectedUsers.each(function (i, elem) {
                if ($("#SelectedUsersId").val().indexOf("") == -1) {
                    $("#SelectedUsersId").val($(this).children("input[type='checkbox']").val() + ",");
                }
                else {
                    if (output != "" && i == 0) {
                        $("#SelectedUsersId").val(output + ",");
                    }
                    $("#SelectedUsersId").val($("#SelectedUsersId").val() + $(this).children("input[type='checkbox']").val() + ",");
                }
            });


            $("#SelectedUsersList").append($selectedUsers);
            $("#SelectedUsersList").html($("#SelectedUsersList .custom-checkbox").sort(function (a, b) {
                return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
            }));

            $("#SelectedUsersList input[type='checkbox']").prop('checked', $('#UnselegatedAllThinkBoxUsers').is(':checked'));
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.selectAdd);
        }
    });

    $("#btnRemoveThinkBoxUser").click(function () {
        var $selectedUsers = $("#SelectedUsersList input[type='checkbox']:checked").parent('.custom-checkbox');

        if ($selectedUsers != null && $selectedUsers.length > 0) {
            $selectedUsers.each(function (i, elem) {
                if ($("#SelectedUsersId").val().indexOf($(this).children("input[type='checkbox']").val() + ",") != -1) {
                    $("#SelectedUsersId").val($("#SelectedUsersId").val().replace($(this).children("input[type='checkbox']").val() + ",", ""));
                }
                else {
                    $("#DeselectedUsersId").val($("#DeselectedUsersId").val() + $(this).children("input[type='checkbox']").val() + ",");
                }
            });

            $("#UsersList").append($selectedUsers);
            $("#UsersList").html($("#UsersList .custom-checkbox").sort(function (a, b) {
                console.log($(a).children('.custom-control-label').text());
                return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
            }));
            $("#UsersList input[type='checkbox']").prop('checked', $('#SelectAllThinkBoxUsers').is(':checked'));
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.selectRemove);
        }


    });

    $("#tbl-ThinkBoxList_filter").hide();
    $('#tbl-ThinkBoxList').DataTable().search('').draw();
    $("#ThinkBoxListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-ThinkBoxList').DataTable().search($(this).val()).draw();

    });

    $('#SelectAllThinkBoxUsers').change(function (e) {
        var $inputs = $('#UsersList input[type=checkbox]');
        if (e.originalEvent === undefined) {
            var allChecked = true;
            $inputs.each(function () {
                allChecked = allChecked && this.checked;
            });
            this.checked = allChecked;
        } else {
            $inputs.prop('checked', this.checked);
        }
    });

    $('input[type=checkbox]', "#UsersList").on('change', function () {
        $('#SelectAllThinkBoxUsers').trigger('change');
    });

    $('input[type=checkbox]', "#SelectedUsersList").on('change', function () {
        $('#UnselegatedAllThinkBoxUsers').trigger('change');
    });

    $('#UnselegatedAllThinkBoxUsers').change(function (e) {
        var $inputs = $('#SelectedUsersList input[type=checkbox]');
        if (e.originalEvent === undefined) {
            var allChecked = true;
            $inputs.each(function () {
                allChecked = allChecked && this.checked;
            });
            this.checked = allChecked;
        } else {
            $inputs.prop('checked', this.checked);
        }
    });

    $('#searchThinkBoxUser').on('keyup', function (e) {
        var searchText = $(this).val().toLowerCase();
        $('#UsersList div').each(function () {
            var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
            $(this).toggle(showCurrentLi);
        });
    });

    $('#searchSelectedThinkBoxUser').on('keyup', function (e) {
        var searchText = $(this).val().toLowerCase();
        $('#SelectedUsersList div').each(function () {
            var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
            $(this).toggle(showCurrentLi);
        });

    });

    function SetCustomScrollbar() {
        $(".thinkboxUsers-wrapper").mCustomScrollbar({
            setHeight: "250",
            autoExpandScrollbar: true
        });
    }
});



