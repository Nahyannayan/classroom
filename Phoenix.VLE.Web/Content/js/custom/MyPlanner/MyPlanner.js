﻿/// <reference path="../../common/global.js" />
var dir = $("html").attr("dir");
var _userLognType = "", usr_Teacher = "teacher", usr_Parent = "parent", usr_Student = "student", usr_Admin = "Admin";
var header = dir == 'ltr' ? { left: 'title', right: 'prev, next' } : { left: 'prev, next', right: 'title' };
Date.prototype.toShortFormat = function () {

    let monthNames = translatedResources.locale==='en' ?["Jan", "Feb", "Mar", "Apr",
        "May", "Jun", "Jul", "Aug",
        "Sep", "Oct", "Nov", "Dec"] : ["يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];

    let day = this.getDate();

    let monthIndex = this.getMonth();
    let monthName = monthNames[monthIndex];

    let year = this.getFullYear();

    return `${day}-${monthName}-${year}`;
}

var planner = function () {

    var init = function () {

        _userLognType = $("#hdnLognUsrType").val();

        if (_userLognType == usr_Teacher || _userLognType == usr_Admin) {

            var actionBtnCreateEvent = `<button type="button" id="btnCreateEvent" class="btn btn-primary mr-3 btn-sm m-0 waves-effect waves-light"><i class="fas fa-plus mr-2"></i>${translatedResources.createEvent}</button>`;
            var actionToggleView = $("<ul />", { "class": "list-pattern flex-wrap flex-sm-nowrap" })
                .append($("<li />").append(actionBtnCreateEvent));

            $('#breadcrumbActions').append(actionToggleView);
        }
    },

        loadPlannerDetailsPopup = function (source, id) {
            var currentDate = new Date();
            var title = translatedResources.planner;
            globalFunctions.loadPopup(source, '/Planner/MyPlanner/LoadPlannerDetails?id=' + id + "&timeOffset=" + currentDate.getTimezoneOffset(), title, 'myPlanner-dailog modal-lg');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $(document).on('click', '#eventAttachmentViewer', function () {
                    var fileName = $('#ResourceFile').val();
                    planner.downloadAttachment(fileName);
                });
                $(document).on('click', '#editPlanner', function () {
                    var event = $('#EventId').val();
                    var id = btoa(event);
                    planner.editPlanner($(this), id);
                });

                $('#lightgallery').lightGallery({
                    selector: '.galleryImg',
                    share: false
                });


                $(document).off('click', '#ahrefDeleteEventFile');
                $(document).on('click', '#ahrefDeleteEventFile', function () {

                    var source = $(this);
                    globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);

                    var eventId = $(source).attr('data-EventId');
                    $(source).off("okClicked");
                    $(".notiCommon").addClass('border');
                    $(".notiCommon").addClass('border-warning');
                    $(source).on('okClicked', function (e) {
                        $.post('/Planner/MyPlanner/DeleteEventFile', { eventId: eventId }, function (response) {
                            $('.resource-data').addClass('d-none');
                        });
                    });
                });

                //formElements.feSelect();
                //popoverEnabler.attachPopover();
                //$('select').selectpicker({
                //    noneSelectedText: "Please Select"
                //});
                //$(document).on('click', '#downloadAttachment', function () {
                //    var fileName = $('#FileName').val();
                //    suggestion.downloadAttachment(fileName);
                //});
            });
        },
        downloadAttachment = function (file) {
            var fileName = btoa(file)
            window.open("/Planner/MyPlanner/EventAttachmentViewer?fileName=" + fileName, "_blank");
        },
        cancelEvent = function (e) {

            var onlineMeetingType = $("#OnlineMeetingType").val();
            if (onlineMeetingType == 1) {
                $.get("/SchoolStructure/SchoolGroups/CheckMSTeamsAccessToken", function (data) {
                    if (!data.Success) {
                        localStorage.setItem("external_auth_complete", "false");
                        var element = $("<a/>", {
                            id: 'action-externalauth',
                        });
                        element.data("sourceclass", "btnCancelEvent");
                        element.data("redirectUri", data.HeaderText);
                        element.appendTo($("body"));
                        element.trigger("click");
                        return;
                    }
                });

            }

            globalFunctions.notyConfirm($(e), translatedResources.cancelConfirm);
            $(e).off('okClicked');
            $(e).on("okClicked", function () {
                var formData = new FormData();
                formData.append("EventId", $('#EventId').val());
                formData.append("Title", $('#Title').val());
                formData.append("Description", $('#Description').val());
                formData.append("StartDate", $('#StartDate').val());
                formData.append("StartTime", $('#StartTime').val());
                formData.append("OnlineMeetingId", $("#OnlineMeetingId").val());
                formData.append("OnlineMeetingType", $("#OnlineMeetingType").val())
                $.ajax({
                    type: 'POST',
                    url: '/Planner/MyPlanner/CancelEvent',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result.Success === true) {
                            $('#myModal').modal('hide');
                            globalFunctions.showMessage("success", translatedResources.cancelSuccess);
                          
                            var _categoryType = "FilterByDate";
                            if ($('.btn-today').hasClass('btn-primary')) {
                                _categoryType = "Today";
                            }
                            if ($('.btn-week').hasClass('btn-primary')) {
                                _categoryType = "Week";
                            }
                            if (result.InsertedRowId === 1) {
                                GetPlannerEvents(1, "OnlineMeetings", _categoryType, fromDate, toDate);
                            }
                            else {
                                GetPlannerEvents(1, "OtherEvents", _categoryType);
                            }
                        }
                        else {
                            globalFunctions.onFailure();
                        }
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure();
                    }

                });
            });
        },

        editPlanner = function (source, id) {
            window.location.href = "/Planner/MyPlanner/InitAddEditPlannerForm?id=" + id;
        }


    return {
        init: init,
        loadPlannerDetailsPopup: loadPlannerDetailsPopup,
        editPlanner: editPlanner,
        cancelEvent: cancelEvent,
        downloadAttachment: downloadAttachment

    };
}();

var eventDates = [];
var currPage = 1;
var fromDate;
var toDate;
$(document).ready(function () {
    planner.init();

    $(document).on('click', '.btn-LoadMoreOnline', function () {
        var _categoryType = "FilterByDate";
        if ($('.btn-today').hasClass('btn-primary')) {
            _categoryType = "Today";
        }
        if ($('.btn-week').hasClass('btn-primary')) {
            _categoryType = "Week";
        }
        if ($('.btn-month').hasClass('btn-primary')) {
            _categoryType = "Month";
        }

        var pageNumber = ++currPage;
        GetPlannerEvents(pageNumber, "OnlineMeetings", _categoryType, fromDate, toDate);
    });

    $(document).on('click', '.btn-LoadMoreTimetable', function () {

        var _categoryType = "FilterByDate";
        if ($('.btn-today').hasClass('btn-primary')) {
            _categoryType = "Today";
        }
        if ($('.btn-week').hasClass('btn-primary')) {
            _categoryType = "Week";
        }
        if ($('.btn-month').hasClass('btn-primary')) {
            _categoryType = "Month";
        }

        var pageNumber = ++currPage;
        GetPlannerEvents(pageNumber, "Timetable", _categoryType, fromDate, toDate);
    });

    $(document).on('click', '.btn-LoadMoreOtherEvent', function () {
        //debugger
        var _categoryType = "FilterByDate";
        if ($('.btn-today').hasClass('btn-primary')) {
            _categoryType = "Today";
        }
        if ($('.btn-week').hasClass('btn-primary')) {
            _categoryType = "Week";
        }
        if ($('.btn-month').hasClass('btn-primary')) {
            _categoryType = "Month";
        }

        var pageNumber = ++currPage;
        GetPlannerEvents(pageNumber, "OtherEvents", _categoryType, fromDate, toDate);
    });


    $("#btnBack").show();
    var globalEventType = "";
    var globalEventCategoryId = null;
    var isUpdatePermission = $("#IsUpdatePermission").val();

    var userType = $("#UserType").val();
    if (userType == "Teacher")
        $(".eventCategoryTimeTableFilter").hide();

    $(document).on('click', '#btnBack', function () {
        history.back();
    });

    //$(document).on('click', '#btnCancelEvent,.btnCancelEvent', function () {
    //    globalFunctions.notyConfirm($(this), translatedResources.cancelConfirm);
    //    $(document).bind('okClicked', $(this), function (e) {
    //        planner.cancelEvent($(this));
    //    });

    //});

    $(document).on('click', '#btnAcceptEventInvitation', function () {
        var _eventId = $(this).attr('data-eventId');

        $.ajax({
            type: 'POST',
            url: '/Planner/MyPlanner/AcceptEventRequest',
            data: {
                'eventId': btoa(_eventId)
            },
            success: function (result) {
                if (result.Success === true) {
                    globalFunctions.showSuccessMessage(translatedResources.eventAcceptSuccess);
                    $("#eventInvitationPopup").modal("hide");
                }
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure();
            }

        });
    });

    var calendar;

    GetPlannerEvents(1, "OnlineMeetings", "Today")

    $("#searchEventsContent").on("keyup", function () {

        //debugger
        var value = $(this).val().toLowerCase();
        $(".events-category").filter(function () {
            $(this).toggle($(this).find('.events-detail').text().toLowerCase().indexOf(value) > -1)
        });

        var loadedEventCount = $('.events-category').length;


        //debugger
        var todayContainerId;
        var weeklyContainerId;
        var monthlyContainerId;
        if ($('#tab-liveSessions').hasClass('active')) {
            todayContainerId = "today-events-container";
            weeklyContainerId = "weekly-events-container";
            monthlyContainerId = "monthly-events-container";
        }
        if ($('#tab-timetable').hasClass('active')) {
            todayContainerId = "today-timetable-container";
            weeklyContainerId = "weekly-timetable-container";
            monthlyContainerId = "monthly-timetable-container";
        }
        if ($('#tab-schoolEvents').hasClass('active')) {
            todayContainerId = "today-schoolEvents-container";
            weeklyContainerId = "weekly-schoolEvents-container";
            monthlyContainerId = "monthly-schoolEvents-container";
        }
        if ($('#tab-otherEvents').hasClass('active')) {
            todayContainerId = "today-otherEvents-container";
            weeklyContainerId = "weekly-otherEvents-container";
            monthlyContainerId = "monthly-otherEvents-container";
        }

        if ($('.btn-today').hasClass('btn-primary')) {
            if ($('#' + todayContainerId + ' .events-category:visible').length == 0) {
                $("#noDataSpan").removeClass('d-none');
                $("#noDataSpan").addClass('d-block');

                //$('.btn-LoadMoreOtherEvent').removeClass('d-block');
                //$('.btn-LoadMoreOtherEvent').addClass('d-none');
            }
            else {
                $("#noDataSpan").removeClass('d-block');
                $("#noDataSpan").addClass('d-none');

                //if ($('.btn-LoadMoreOtherEvent').hasClass('d-block')) {
                //    $('.btn-LoadMoreOtherEvent').addClass('d-block');
                //    $('.btn-LoadMoreOtherEvent').removeClass('d-none');
                //}
            }
        }
        if ($('.btn-week').hasClass('btn-primary')) {
            if ($('#' + weeklyContainerId + ' .events-category:visible').length == 0) {
                $("#noDataSpan").removeClass('d-none');
                $("#noDataSpan").addClass('d-block');

                //$('.btn-LoadMoreOtherEvent').removeClass('d-block');
                //$('.btn-LoadMoreOtherEvent').addClass('d-none');
            }
            else {
                $("#noDataSpan").removeClass('d-block');
                $("#noDataSpan").addClass('d-none');

                //if ($('.btn-LoadMoreOtherEvent').hasClass('d-block')) {
                //    $('.btn-LoadMoreOtherEvent').addClass('d-block');
                //    $('.btn-LoadMoreOtherEvent').removeClass('d-none');
                //}
            }

        }
        if ($('.btn-month').hasClass('btn-primary')) {
            if ($('#' + monthlyContainerId + ' .events-category:visible').length == 0) {
                $("#noDataSpan").removeClass('d-none');
                $("#noDataSpan").addClass('d-block');

                //$('.btn-LoadMoreOtherEvent').removeClass('d-block');
                //$('.btn-LoadMoreOtherEvent').addClass('d-none');
            }
            else {
                $("#noDataSpan").removeClass('d-block');
                $("#noDataSpan").addClass('d-none');

                //if ($('.btn-LoadMoreOtherEvent').hasClass('d-block')) {
                //    $('.btn-LoadMoreOtherEvent').addClass('d-block');
                //    $('.btn-LoadMoreOtherEvent').removeClass('d-none');
                //}
            }

        }
    });

    $('body').on('click', '.events-category', function (e) {
        //debugger
        if ($(e.target).is(".fa.fa-video") || $(e.target).is(".view-recording") || $(e.target).is(".timetable-session") ||
            $(e.target).is(".fas.fa-video"))
            return;
        var _Id = $(this).find('.hdnEventId').val();
        var _eventType = $(this).find('.hdnEventTypeName').val();

        var _eventTitle = $(this).find('.event-main-title').text();
        var _eventCategory = $(this).find('.event-name').html();
        var _startDate = $(this).find('.hdnStartDate').val();
        var _endDate = $(this).find('.hdnEndDate').val();

        if (_eventType == "MyPlanner") {

            if (globalFunctions.isValueValid(_Id)) {
                $.ajax({
                    type: 'POST',
                    url: '/Planner/MyPlanner/CheckIftheInvitationAccepted',
                    data: {
                        'eventId': _Id
                    },
                    success: function (result) {

                        if (result.Success === true) {
                            planner.loadPlannerDetailsPopup($(this), result.InsertedRowId);
                        }
                        else {
                            $("#eventInvitationPopup").modal("show");
                            $("#btnAcceptEventInvitation").attr('data-eventId', result.InsertedRowId);
                        }
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure();
                    }

                });

            }

        }
        else if (_eventType == "MyCommunity") {
            var _thumbnail = $(this).find('.hdnThumbnail').val();

            $("#eventPopup").modal("show");

            $("#eventThumbnail").html("<img src='" + _thumbnail + "' style='width:50px;height:50px;'/>");
            $("#eventTitle").text(_eventTitle);
            $("#eventDescription").html(_eventTitle);
            $("#eventStart").html(_startDate);
            $("#eventEnd").html(_endDate);
            $("#eventCategory").html(_eventCategory);
        }
        else if (_eventType == "StudentEvent") {
            var _startTime = $(this).find('.hdnStartTime').val();
            var _endTime = $(this).find('.hdnEndTime').val();
            var _roomNumber = $(this).find('.hdnRoomNumber').val();
            var _teacherName = $(this).find('.hdnTeacherName').val();

            $("#studentEventPopup").modal("show");

            $("#eventTitleHeading").text(_eventTitle);
            $("#eventSubDescription").html(_eventTitle);
            $("#eventStartTm").html(_startTime);
            $("#eventEndTm").html(_endTime);
            $("#eventRoomNumber").html(_roomNumber);
            $("#eventTeacherName").html(_teacherName);
        }
        else if (_eventType == "TodayOrWeekly") {
            var _startTime = $(this).find('.hdnFromTime').val();
            var _endTime = $(this).find('.hdnToTime').val();
            var _teacherName = $(this).find('.hdnTeacherName').val();
            var _period = $(this).find('.hdnPeriod').val();
            var _expression = $(this).find('.hdnExpression').val();
            var _roomNo = $(this).find('.hdnRoomNumber').val();

            $("#weeklyEventPopup").modal("show");

            $("#weekly-eventTitle").text(_eventTitle);
            $("#courseName").html(_eventTitle);
            $("#weekly-startTime").html(_startTime);
            $("#weekly-endTime").html(_endTime);
            $("#weekly-teacherName").html(_teacherName);
            $("#weekly-period").html(_period);
            $("#weekly-expression").html(_expression);
            $("#weekly-room-number").html(_roomNo);
        }
        else if (_eventType == "Assignments") {

            var _dueDate = $(this).find('.hdnDueDate').val();
            var _description = $(this).find('.hdnAssignmentDescription').val();

            $("#assignmentEventPopup").modal("show");

            $("#assignment-modal-title").text(translatedResources.assignmentSubmission);
            $("#assignment-title").html(_eventTitle);
            $("#assignmentDueDate").html(_dueDate);
            $("#assignmentDescription").html(_description);
        }

    });

    $('body').on('click', '#btnSaveZoomMeeting', function (e) {
        var _title = $("#MeetingName").val();
        var _meetingPassword = $("#MeetingPassword").val();
        var _startDate = globalFunctions.toSystemReadableDate( $("#strMeetingDate").val());
        var _startTime = globalFunctions.toSystemReadableTime($("#MeetingTime").val());

        $.ajax({
            type: 'POST',
            url: '/Planner/MyPlanner/CheckIftheTimetableEventExists',
            data: {
                'title': _title,
                'meetingPassword': _meetingPassword,
                'startDate': _startDate,
                'startTime': _startTime
            },
            success: function (result) {
                if (result.Success === true) {
                    globalFunctions.showWarningMessage(translatedResources.timetableEventWarning);
                }
                else {
                    $('form#formSynchronousLessons').submit();
                }
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure();
            }

        });
    });

    $(document).on("change", "#EventCategoryId", function () {
        var eventCategoryIds = $("#EventCategoryId").val();

        $('.btn-today').addClass('btn-primary');
        $('.btn-today').removeClass('btn-outline-primary');
        $('.btn-week').removeClass('btn-primary');
        $('.btn-month').removeClass('btn-primary');
        $('.btn-week').addClass('btn-outline-primary');
        $('.btn-month').addClass('btn-outline-primary');
        $('.events-today').removeClass('d-none');
        $('.upcoming-events').removeClass('d-none');
        $('.events-month').addClass('d-none');
        $('.events-week').addClass('d-none');

        GetEvents("", eventCategoryIds, "", "Today");
    });



    //var nowTemp = new Date();
    //var stDate = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), nowTemp.getDate(), 0, 0, 0, 0);
    //var endDate = new Date(nowTemp.getFullYear(), (nowTemp.getMonth() + 2), nowTemp.getDate(), 0, 0, 0, 0);

    //var endDateToShow = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), (nowTemp.getDate() + 1), 0, 0, 0, 0);

    //var StartDate = $('#FromDate').datetimepicker({
    //    format: 'DD-MMM-YYYY',
    //    // minDate: (new Date()-90),
    //    maxDate: endDate
    //}).on('dp.change', function (ev) {
    //    if (ev.date.valueOf() > EndDate.viewDate().valueOf()) {
    //        globalFunctions.showWarningMessage(translatedResources.startTimeWarning);
    //        $('#FromDate').data("DateTimePicker").date(ev.oldDate);
    //    }
    //}).data("DateTimePicker");


    //var EndDate = $('#ToDate').datetimepicker({
    //    format: 'DD-MMM-YYYY',
    //    maxDate: endDate
    //}).on('dp.change', function (ev) {
    //    if (ev.date.valueOf() < StartDate.viewDate().valueOf()) {
    //        globalFunctions.showWarningMessage(translatedResources.endDateWarning);
    //        $('#ToDate').data("DateTimePicker").date(ev.oldDate);
    //    }
    //}).data("DateTimePicker");


    //$("#FromDate").val(globalFunctions.getFormattedDate(stDate));
    //$("#ToDate").val(globalFunctions.getFormattedDate(endDateToShow));

    $('.inline-date-picker').datetimepicker({
        format: 'MM/DD/YYYY',
        inline: true,
        locale: translatedResources.locale
        //sideBySide: true
    }).on('dp.change', function (e) {

        //GetEventsByCalendar("", "", e.date._d);
        GetEventsByCalendar( e.date._d);
        AddEventsCalendarIndication();
        $(".day.today").css("background-color", "");
        $(".day.active").css("background-color", "#337ab7");
    });


    function getSundayOfCurrentWeek(d) {
        var day = d.getDay();
        return new Date(d.getFullYear(), d.getMonth(), d.getDate() + (day == 0 ? 0 : - day));
    }

    function AddEventsCalendarIndication() {
        eventDates.map(function (date, i) {
            $("[data-day='" + date + "']").css("background-color", "#e9e1e1");
        });
    }

    function GetEventsByCalendar(selectedDate) {

        currPage = 1;
        fromDate = GetFormattedDate(selectedDate);
        toDate = GetFormattedDate(selectedDate);

        var _eventType = "OnlineMeetings";
        if ($('#tab-liveSessions').hasClass('active')) {
            _eventType = "OnlineMeetings";
        }
        if ($('#tab-timetable').hasClass('active')) {
            _eventType = "Timetable";
        }
        if ($('#tab-schoolEvents').hasClass('active')) {
            _eventType = "SchoolEvents";
        }
        if ($('#tab-otherEvents').hasClass('active')) {
            _eventType = "OtherEvents";
        }

        $('.btn-today').removeClass('btn-primary');
        $('.btn-week').removeClass('btn-primary');
        $('.btn-month').removeClass('btn-primary');
        $('.btn-today').addClass('btn-outline-primary');
        $('.btn-week').addClass('btn-outline-primary');
        $('.btn-month').addClass('btn-outline-primary');
        //$('.upcoming-events').addClass('d-none');
        $('.events-today').removeClass('d-none');
        $('.events-week').addClass('d-none');
        $('.events-month').addClass('d-none');

        $('#weekly-events-container').empty();
        $('#weekly-timetable-container').empty();
        $('#weekly-otherEvents-container').empty();
        $('#weekly-schoolEvents-container').empty();

        $('#monthly-events-container').empty();
        $('#monthly-timetable-container').empty();
        $('#monthly-schoolEvents-container').empty();
        $('#monthly-otherEvents-container').empty();

        GetPlannerEvents(1, _eventType, "FilterByDate", fromDate, toDate)

    }

    function GetFormattedDate(dt) {

        var month = dt.getMonth() + 1;
        var day = dt.getDate();
        var year = dt.getFullYear();
        return month + "/" + day + "/" + year;
    }

    $('body').on('click', '#btnFilterEvents', function () {

        var fromDate = $("#FromDate").val();
        var toDate = $("#ToDate").val();
        var eventCategoryIds = $("#EventCategoryId").val();

        $('.btn-today').removeClass('btn-primary');
        $('.btn-week').removeClass('btn-primary');
        $('.btn-month').removeClass('btn-primary');
        $('.btn-today').addClass('btn-outline-primary');
        $('.btn-week').addClass('btn-outline-primary');
        $('.btn-month').addClass('btn-outline-primary');
        $('.upcoming-events').addClass('d-none');
        $('.events-today').removeClass('d-none');
        $('.events-week').addClass('d-none');
        $('.events-month').addClass('d-none');

        GetEvents("", eventCategoryIds, "", "FilterByDate", fromDate, toDate);

    });

    $('body').on('click', '#btnCreateEvent', function () {
        var id = "";
        var todayDate = $("#FromDate").val();
        var startDate = btoa(todayDate);
        window.location.href = "/Planner/MyPlanner/InitAddEditPlannerForm?id=" + id + "&sd=" + startDate;
    });

    $('body').on('click', '#tab-liveSessions', function () {


        currPage = 1;
        var isTodayWeeklyMChecked = false;
        var _categoryType = "Today";
        if ($('.btn-today').hasClass('btn-primary')) {
            _categoryType = "Today";
            isTodayWeeklyMChecked = true;
        }
        if ($('.btn-week').hasClass('btn-primary')) {
            _categoryType = "Week";
            isTodayWeeklyMChecked = true;
        }
        if ($('.btn-month').hasClass('btn-primary')) {
            _categoryType = "Month";
            isTodayWeeklyMChecked = true;
        }




        $('#today-timetable-container').empty();
        $('#today-schoolEvents-container').empty();
        $('#today-otherEvents-container').empty();

        $('#weekly-timetable-container').empty();
        $('#weekly-schoolEvents-container').empty();
        $('#weekly-otherEvents-container').empty();

        $('#monthly-timetable-container').empty();
        $('#monthly-schoolEvents-container').empty();
        $('#monthly-otherEvents-container').empty();

        if (fromDate !== undefined && !isTodayWeeklyMChecked) {
            GetPlannerEvents(1, "OnlineMeetings", "FilterByDate", fromDate, toDate);
        }
        else {
            GetPlannerEvents(1, "OnlineMeetings", _categoryType);
        }


    });

    $('body').on('click', '#tab-timetable', function () {
        currPage = 1;
        var _categoryType = "Today";
        var isTodayWeeklyMChecked = false;
        if ($('.btn-today').hasClass('btn-primary')) {
            _categoryType = "Today";
            isTodayWeeklyMChecked = true;
        }
        if ($('.btn-week').hasClass('btn-primary')) {
            _categoryType = "Week";
            isTodayWeeklyMChecked = true;
        }
        if ($('.btn-month').hasClass('btn-primary')) {
            _categoryType = "Month";
            isTodayWeeklyMChecked = true;
        }

        $('#today-events-container').empty();
        $('#today-schoolEvents-container').empty();
        $('#today-otherEvents-container').empty();

        $('#weekly-events-container').empty();
        $('#weekly-schoolEvents-container').empty();
        $('#weekly-otherEvents-container').empty();

        $('#monthly-events-container').empty();
        $('#monthly-schoolEvents-container').empty();
        $('#monthly-otherEvents-container').empty();

        if (fromDate !== undefined && !isTodayWeeklyMChecked) {
            GetPlannerEvents(1, "Timetable", "FilterByDate", fromDate, toDate);
        }
        else {
            GetPlannerEvents(1, "Timetable", _categoryType);
        }

    });

    $('body').on('click', '#tab-schoolEvents', function () {
        currPage = 1;
        var _categoryType = "Today";
        var isTodayWeeklyMChecked = false;
        if ($('.btn-today').hasClass('btn-primary')) {
            _categoryType = "Today";
            isTodayWeeklyMChecked = true;
        }
        if ($('.btn-week').hasClass('btn-primary')) {
            _categoryType = "Week";
            isTodayWeeklyMChecked = true;
        }
        if ($('.btn-month').hasClass('btn-primary')) {
            _categoryType = "Month";
            isTodayWeeklyMChecked = true;
        }

        $('#today-events-container').empty();
        $('#today-timetable-container').empty();
        $('#today-otherEvents-container').empty();

        $('#weekly-events-container').empty();
        $('#weekly-timetable-container').empty();
        $('#weekly-otherEvents-container').empty();

        $('#monthly-events-container').empty();
        $('#monthly-timetable-container').empty();
        $('#monthly-otherEvents-container').empty();

        if (fromDate !== undefined && !isTodayWeeklyMChecked) {
            GetPlannerEvents(1, "SchoolEvents", "FilterByDate", fromDate, toDate);
        }
        else {
            GetPlannerEvents(1, "SchoolEvents", _categoryType);
        }

    });

    $('body').on('click', '#tab-otherEvents', function () {
        currPage = 1;
        var _categoryType = "Today";
        var isTodayWeeklyMChecked = false;
        if ($('.btn-today').hasClass('btn-primary')) {
            _categoryType = "Today";
            isTodayWeeklyMChecked = true;
        }
        if ($('.btn-week').hasClass('btn-primary')) {
            _categoryType = "Week";
            isTodayWeeklyMChecked = true;
        }
        if ($('.btn-month').hasClass('btn-primary')) {
            _categoryType = "Month";
            isTodayWeeklyMChecked = true;
        }

        $('#today-events-container').empty();
        $('#today-timetable-container').empty();
        $('#today-schoolEvents-container').empty();

        $('#weekly-events-container').empty();
        $('#weekly-timetable-container').empty();
        $('#weekly-schoolEvents-container').empty();

        $('#monthly-events-container').empty();
        $('#monthly-timetable-container').empty();
        $('#monthly-schoolEvents-container').empty();

        if (fromDate !== undefined && !isTodayWeeklyMChecked) {
            GetPlannerEvents(1, "OtherEvents", "FilterByDate", fromDate, toDate);
        }
        else {
            GetPlannerEvents(1, "OtherEvents", _categoryType);
        }

    });


    $('body').on('click', 'button.fc-prev-button', function () {
        var month = $(dir == "ltr" ? ".fc-left h2" : ".fc-right h2").text();
        console.log(month);

        var calendarEl = document.getElementById('divCalender');
        calendarEl.innerHTML = "";
        GetEvents(month, globalEventCategoryId, globalEventType);
    });

    $('body').on('click', 'button.fc-next-button', function () {
        //debugger
        console.log(globalEventType);
        console.log(globalEventCategoryId);
        var month = $(dir == "ltr" ? ".fc-left h2" : ".fc-right h2").text();
        console.log(month);
        var calendarEl = document.getElementById('divCalender');
        calendarEl.innerHTML = "";
        GetEvents(month, globalEventCategoryId, globalEventType);
    });

    $('body').on('click', 'button.fc-today-button', function () {
        alert("hi");
        //debugger;
        var calendarEl = document.getElementById('divCalender');
        calendarEl.innerHTML = "";
        GetEvents("");
    });

});

$(window).on("load", function () {

    $('.events-header .btn-week').click(function () {

        // debugger
        currPage = 1;
        var eventCategoryIds = $("#EventCategoryId").val();

        var _eventType = "OnlineMeetings";
        if ($('#tab-liveSessions').hasClass('active')) {
            _eventType = "OnlineMeetings";
        }
        if ($('#tab-timetable').hasClass('active')) {
            _eventType = "Timetable";
        }
        if ($('#tab-schoolEvents').hasClass('active')) {
            _eventType = "SchoolEvents";
        }
        if ($('#tab-otherEvents').hasClass('active')) {
            _eventType = "OtherEvents";
        }

        $('#today-events-container').empty();
        $('#today-timetable-container').empty();
        $('#today-schoolEvents-container').empty();
        $('#today-otherEvents-container').empty();

        $('#monthly-events-container').empty();
        $('#monthly-timetable-container').empty();
        $('#monthly-schoolEvents-container').empty();
        $('#monthly-otherEvents-container').empty();


        $('.btn-today').removeClass('btn-primary');
        $('.btn-today').addClass('btn-outline-primary');
        $('.btn-month').removeClass('btn-primary');
        $('.btn-month').addClass('btn-outline-primary');
        $('.btn-week').addClass('btn-primary');
        $('.btn-week').removeClass('btn-outline-primary');
        $('.events-week').removeClass('d-none');
        $('.upcoming-events').addClass('d-none');
        $('.events-today').addClass('d-none');
        $('.events-month').addClass('d-none');

        GetPlannerEvents(1, _eventType, "Week");
    });
    $('.events-header .btn-today').click(function () {
        //debugger;
        currPage = 1;
        var eventCategoryIds = $("#EventCategoryId").val();

        var _eventType = "OnlineMeetings";
        if ($('#tab-liveSessions').hasClass('active')) {
            _eventType = "OnlineMeetings";
        }
        if ($('#tab-timetable').hasClass('active')) {
            _eventType = "Timetable";
        }
        if ($('#tab-schoolEvents').hasClass('active')) {
            _eventType = "SchoolEvents";
        }
        if ($('#tab-otherEvents').hasClass('active')) {
            _eventType = "OtherEvents";
        }

        $('#weekly-events-container').empty();
        $('#weekly-timetable-container').empty();
        $('#weekly-schoolEvents-container').empty();
        $('#weekly-otherEvents-container').empty();

        $('#monthly-events-container').empty();
        $('#monthly-timetable-container').empty();
        $('#monthly-schoolEvents-container').empty();
        $('#monthly-otherEvents-container').empty();

        $('.upcoming-events .events-header .events-title').text("Upcoming Events");
        $('.btn-today').addClass('btn-primary');
        $('.btn-today').removeClass('btn-outline-primary');
        $('.btn-week').removeClass('btn-primary');
        $('.btn-week').addClass('btn-outline-primary');
        $('.btn-month').removeClass('btn-primary');
        $('.btn-month').addClass('btn-outline-primary');
        $('.upcoming-events').removeClass('d-none');
        $('.events-today').removeClass('d-none');
        $('.events-week').addClass('d-none');
        $('.events-month').addClass('d-none');

        GetPlannerEvents(1, _eventType, "Today");
    });
    $('.events-header .btn-month').click(function () {

        currPage = 1;
        var eventCategoryIds = $("#EventCategoryId").val();

        var _eventType = "OnlineMeetings";
        if ($('#tab-liveSessions').hasClass('active')) {
            _eventType = "OnlineMeetings";
        }
        if ($('#tab-timetable').hasClass('active')) {
            _eventType = "Timetable";
        }
        if ($('#tab-schoolEvents').hasClass('active')) {
            _eventType = "SchoolEvents";
        }
        if ($('#tab-otherEvents').hasClass('active')) {
            _eventType = "OtherEvents";
        }

        $('#today-events-container').empty();
        $('#today-timetable-container').empty();
        $('#today-schoolEvents-container').empty();
        $('#today-otherEvents-container').empty();

        $('#weekly-events-container').empty();
        $('#weekly-timetable-container').empty();
        $('#weekly-schoolEvents-container').empty();
        $('#weekly-otherEvents-container').empty();

        $('.btn-week').removeClass('btn-primary');
        $('.btn-week').addClass('btn-outline-primary');
        $('.btn-today').removeClass('btn-primary');
        $('.btn-today').addClass('btn-outline-primary');
        $('.btn-month').addClass('btn-primary');
        $('.btn-month').removeClass('btn-outline-primary');
        $('.events-month').removeClass('d-none');
        $('.events-week').addClass('d-none');
        $('.upcoming-events').addClass('d-none');
        $('.events-today').addClass('d-none');

        GetPlannerEvents(1, _eventType, "Month");
    });
});



function GetPlannerEvents(pageNumber, eventType, categoryType, fromDate, toDate) {
    var _eventFromDate = fromDate;
    $.ajax(
        {
            url: '/Planner/MyPlanner/GetPlannerEvents?pageNumber=' + pageNumber + '&eventType=' + eventType + '&eventCategoryType=' + categoryType + '&eFromDate=' + fromDate + '&eToDate=' + toDate,
            dataType: 'json', // type of response data
            //timeout: 500,     // timeout milliseconds
            async: true,
            success: function (data, status, xhr) {   // success callback function


                var _eventTempList = JSON.parse(data.events);
                var _eventType = data.type;
                var onlineMeetingEventList;
                var schoolTimetableEventList;
                var myCommunityEventList;
                var otherEventsList;

                if ((_eventType == "OnlineMeetings" || _eventType == "OtherEvents") && _eventTempList != null)
                    onlineMeetingEventList = _eventTempList;
                if (_eventType == "Timetable" && _eventTempList != null)
                    schoolTimetableEventList = _eventTempList;
                if (_eventType == "SchoolEvents" && _eventTempList != null)
                    myCommunityEventList = _eventTempList.data;

                var todayEventHtmlElements = "";
                var upcomingEventHtmlElements = "";
                var weeklyOrMonthlyEventHtmlElements = "";
                var filterByDateEventHtmlElements = "";
                var _calendarFromDate;
                var _splittedFromDate;
                var _calendarFromDay;
                var onlineEventTotalCount;
                var timetableEventTotalCount;
                var schoolEventTotalCount;
                var otherEventTotalCount;

                var fullWeekday = translatedResources.locale === 'en' ? ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"] : ["الأحد", "الأثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت"];

                var todayDate = new Date();
                var todayDay = todayDate.getDate();
                if (categoryType == "FilterByDate") {
                    _calendarFromDate = _eventFromDate;
                    _splittedFromDate = _calendarFromDate.split("/");
                    _calendarFromDay = _splittedFromDate[1];
                }


                $.each(onlineMeetingEventList, function (i, obj) {

                    obj.StartDate = obj.StartDate + "Z";
                    obj.EndDate = obj.EndDate + "Z";
                    var stDt = new Date(obj.StartDate);
                    var sDay = stDt.getDate();
                    var sMon = getMonthNm(stDt);
                    var tm = formatAMPM(stDt);
                    var edDt = new Date(obj.EndDate);
                    var eDay = edDt.getDate();
                    var eMon = getMonthNm(edDt);
                    var edTm = formatAMPM(edDt);
                    var _dateForSort = stDt.toShortFormat() + " " + tm;
                    var _startDate = sDay + " " + sMon + " " + tm;
                    var _endDate = eDay + " " + eMon + " " + edTm;
                    var _eventId = obj.EventId;
                    var _eventTypeName = "MyPlanner";
                    var _colorCode = obj.ColorCode;
                    var _organizer = obj.CreatedByUserName;

                    onlineEventTotalCount = obj.TotalCount;
                    otherEventTotalCount = obj.TotalCount;
                    var recordingLinkHTML = obj.ShowRecordingLink ? `<button class="btn btn-primary btn-sm mt-0 ml-2 view-recording" onclick="window.open('${obj.RecordingLink}')" ><i class="fa fa-video mr-2"></i>` + translatedResources.ViewRecording + `</button>` : "";
                    if (categoryType == "Today" && (sDay == todayDay)) {
                        var _eventCategory = (obj.EventCategory) == null ? "-" : obj.EventCategory;
                        var _description = (obj.Description) == null ? "-" : obj.Description;

                        todayEventHtmlElements += '<div class="events-category primary bg-white px-4" data-sortvalue="' + _dateForSort + '" style="--category-color: ' + _colorCode + ';cursor:pointer">'
                            + '<div class="events-time py-2 text-right">'
                            + '    <span>' + translatedResources.Start + '</span>'
                            + '     <span class="date d-block text-bold font-16 text-primary arabictxt"> ' + sDay + '</span>'
                            + '     <span class="month d-block text-semibold text-primary"> ' + sMon + '</span>'
                            + '     <p class="text-semibold mb-0 text-primary arabictxt"> ' + tm + '</p>'
                            + ' </div>'
                            + ' <div class="events-detail w-100">'
                            + '     <div class="d-flex event-main-title justify-content-between text-semibold">' + obj.Title + recordingLinkHTML + '</div>'
                            + '     <div class="event-type d-flex align-items-center mt-0">'
                            + '         <span class="event-name text-semibold p-0 text-black-50">' + _eventCategory + '</span>'
                            + '     </div>'
                            + '     <p class="event-description">'
                            + _description
                            + '         </p>'
                            + '<p class="event-description pb-0 mb-0"> ' + translatedResources.Organizer + ' ' + _organizer + '</p>'
                            + ' </div>'
                            + '<input type="hidden" class="hdnEventId" value="' + _eventId + '"/>'
                            + '<input type="hidden" class="hdnEventTypeName" value="' + _eventTypeName + '"/>'
                            + ' </div>'

                    }
                    else if (categoryType == "Week" || categoryType == "Month") {
                        var _eventCategory = (obj.EventCategory) == null ? "-" : obj.EventCategory;
                        var _description = (obj.Description) == null ? "-" : obj.Description;

                        weeklyOrMonthlyEventHtmlElements += '<div class="events-category primary bg-white px-4" data-sortvalue="' + _dateForSort + '" style="--category-color: ' + _colorCode + ';cursor:pointer">'
                            + '<div class="events-time py-2 text-right">'
                            + '    <span>' + translatedResources.Start + '</span>'
                            + '     <span class="date d-block text-bold font-16 text-primary arabictxt"> ' + sDay + '</span>'
                            + '     <span class="month d-block text-semibold text-primary"> ' + sMon + '</span>'
                            + '     <p class="text-semibold mb-0 text-primary arabictxt"> ' + tm + '</p>'
                            + ' </div>'
                            + ' <div class="events-detail w-100">'
                            + '     <div class="d-flex event-main-title justify-content-between text-semibold">' + obj.Title + recordingLinkHTML + '</div>'
                            + '     <div class="event-type d-flex align-items-center mt-0">'
                            + '         <span class="event-name text-semibold p-0 text-black-50">' + _eventCategory + '</span>'
                            + '     </div>'
                            + '     <p class="event-description">'
                            + _description
                            + '         </p>'
                            + '<p class="event-description pb-0 mb-0"> ' + translatedResources.Organizer + ' ' + _organizer + '</p>'
                            + ' </div>'
                            + '<input type="hidden" class="hdnEventId" value="' + _eventId + '"/>'
                            + '<input type="hidden" class="hdnEventTypeName" value="' + _eventTypeName + '"/>'
                            + ' </div>'
                    }
                    else if (categoryType == "FilterByDate" && (_calendarFromDay >= sDay && _calendarFromDay <= eDay)) {
                        var _eventCategory = (obj.EventCategory) == null ? "-" : obj.EventCategory;
                        var _description = (obj.Description) == null ? "-" : obj.Description;

                        filterByDateEventHtmlElements += '<div class="events-category primary bg-white px-4" data-sortvalue="' + _dateForSort + '" style="--category-color: ' + _colorCode + ';cursor:pointer">'
                            + '<div class="events-time py-2 text-right">'
                            + '    <span>' + translatedResources.Start + '</span>'
                            + '     <span class="date d-block text-bold font-16 text-primary arabictxt"> ' + _calendarFromDay + '</span>'
                            + '     <span class="month d-block text-semibold text-primary"> ' + sMon + '</span>'
                            + '     <p class="text-semibold mb-0 text-primary arabictxt"> ' + tm + '</p>'
                            + ' </div>'
                            + ' <div class="events-detail w-100">'
                            + '     <div class="d-flex event-main-title justify-content-between text-semibold">' + obj.Title + recordingLinkHTML + '</div>'
                            + '     <div class="event-type d-flex align-items-center mt-0">'
                            + '         <span class="event-name text-semibold p-0 text-black-50">' + _eventCategory + '</span>'
                            + '     </div>'
                            + '     <p class="event-description">'
                            + _description
                            + '         </p>'
                            + '<p class="event-description pb-0 mb-0"> ' + translatedResources.Organizer + ' ' + _organizer + '</p>'
                            + ' </div>'
                            + '<input type="hidden" class="hdnEventId" value="' + _eventId + '"/>'
                            + '<input type="hidden" class="hdnEventTypeName" value="' + _eventTypeName + '"/>'
                            + ' </div>'
                    }

                });


                $.each(myCommunityEventList, function (i, obj) {

                    var stDt = new Date(obj.eventStartDT);
                    var sDay = stDt.getDate();
                    var sMon = getMonthNm(stDt);
                    var tm = formatAMPM(stDt);
                    var edDt = new Date(obj.eventEndDT);
                    var eDay = edDt.getDate();
                    var eMon = getMonthNm(edDt);
                    var edTm = formatAMPM(edDt);
                    var _dateForSort = stDt.toShortFormat() + " " + tm;
                    var _startDate = sDay + " " + sMon + " " + tm;
                    var _endDate = eDay + " " + eMon + " " + edTm;
                    var _thumbnail = (obj.categoryThumbNailUrl) == null ? "-" : obj.categoryThumbNailUrl;
                    var _eventTypeName = "MyCommunity";
                    var _colorCode = obj.categoryColorCode;

                    schoolEventTotalCount = obj.TotalCount;

                    if (categoryType == "Today" && (sDay == todayDay)) {
                        todayEventHtmlElements += '<div class="events-category success bg-white px-4" data-sortvalue="' + _dateForSort + '" style="--category-color: ' + _colorCode + ';cursor:pointer">'
                            + '<div class="events-time py-2 text-right">'
                            + '    <span>' + translatedResources.Start + '</span>'
                            + '     <span class="date d-block text-bold font-16 text-primary arabictxt"> ' + sDay + '</span>'
                            + '     <span class="month d-block text-semibold text-primary"> ' + sMon + '</span>'
                            + '     <p class="text-semibold mb-0 text-primary arabictxt"> ' + tm + '</p>'
                            + ' </div>'
                            + ' <div class="events-detail w-100">'
                            + '     <div class="d-flex event-main-title justify-content-between text-semibold">' + obj.eventDescription + '</div>'
                            + '     <div class="event-type d-flex align-items-center mt-0">'
                            + '         <span class="event-name text-semibold p-0 text-black-50">' + obj.categoryDescription + '</span>'
                            + '     </div>'
                            + '     <p class="event-description">'
                            + obj.schoolName
                            + '         </p>'
                            + ' </div>'
                            + '<input type="hidden" class="hdnEventTypeName" value="' + _eventTypeName + '"/>'
                            + '<input type="hidden" class="hdnStartDate" value="' + _startDate + '"/>'
                            + '<input type="hidden" class="hdnEndDate" value="' + _endDate + '"/>'
                            + '<input type="hidden" class="hdnThumbnail" value="' + _thumbnail + '"/>'
                            + ' </div>'

                    }
                    else if (categoryType == "Week" || categoryType == "Month") {
                        weeklyOrMonthlyEventHtmlElements += '<div class="events-category success bg-white px-4" data-sortvalue="' + _dateForSort + '" style="--category-color: ' + _colorCode + ';cursor:pointer">'
                            + '<div class="events-time py-2 text-right">'
                            + '    <span>' + translatedResources.Start + '</span>'
                            + '     <span class="date d-block text-bold font-16 text-primary arabictxt"> ' + sDay + '</span>'
                            + '     <span class="month d-block text-semibold text-primary"> ' + sMon + '</span>'
                            + '     <p class="text-semibold mb-0 text-primary arabictxt"> ' + tm + '</p>'
                            + ' </div>'
                            + ' <div class="events-detail w-100">'
                            + '     <div class="d-flex event-main-title justify-content-between text-semibold">' + obj.eventDescription + '</div>'
                            + '     <div class="event-type d-flex align-items-center mt-0">'
                            + '         <span class="event-name text-semibold p-0 text-black-50">' + obj.categoryDescription + '</span>'
                            + '     </div>'
                            + '     <p class="event-description">'
                            + obj.schoolName
                            + '         </p>'
                            + ' </div>'
                            + '<input type="hidden" class="hdnEventTypeName" value="' + _eventTypeName + '"/>'
                            + '<input type="hidden" class="hdnStartDate" value="' + _startDate + '"/>'
                            + '<input type="hidden" class="hdnEndDate" value="' + _endDate + '"/>'
                            + '<input type="hidden" class="hdnThumbnail" value="' + _thumbnail + '"/>'
                            + ' </div>'
                    }
                    else if (categoryType == "FilterByDate" && (_calendarFromDay >= sDay && _calendarFromDay <= eDay)) {
                        filterByDateEventHtmlElements += '<div class="events-category success bg-white px-4" data-sortvalue="' + _dateForSort + '" style="--category-color: ' + _colorCode + ';cursor:pointer">'
                            + '<div class="events-time py-2 text-right">'
                            + '    <span>' + translatedResources.Start + '</span>'
                            + '     <span class="date d-block text-bold font-16 text-primary arabictxt"> ' + _calendarFromDay + '</span>'
                            + '     <span class="month d-block text-semibold text-primary"> ' + sMon + '</span>'
                            + '     <p class="text-semibold mb-0 text-primary arabictxt"> ' + tm + '</p>'
                            + ' </div>'
                            + ' <div class="events-detail w-100">'
                            + '     <div class="d-flex event-main-title justify-content-between text-semibold">' + obj.eventDescription + '</div>'
                            + '     <div class="event-type d-flex align-items-center mt-0">'
                            + '         <span class="event-name text-semibold p-0 text-black-50">' + obj.categoryDescription + '</span>'
                            + '     </div>'
                            + '     <p class="event-description">'
                            + obj.schoolName
                            + '         </p>'
                            + ' </div>'
                            + '<input type="hidden" class="hdnEventTypeName" value="' + _eventTypeName + '"/>'
                            + '<input type="hidden" class="hdnStartDate" value="' + _startDate + '"/>'
                            + '<input type="hidden" class="hdnEndDate" value="' + _endDate + '"/>'
                            + '<input type="hidden" class="hdnThumbnail" value="' + _thumbnail + '"/>'
                            + ' </div>'
                    }


                });

                $.each(schoolTimetableEventList, function (i, obj) {

                    var stDt = new Date(obj.EventDate);
                    var sDay = stDt.getDate();
                    var edDt = new Date(obj.EventDate);
                    var eDay = edDt.getDate();
                    var sMon = getMonthNm(stDt);
                    var tm = formatAMPM(stDt);

                    var _weekDayName = "";

                    var _fromTime = obj.StartTime.substring(0, 5);
                    var _toTime = obj.EndTime.substring(0, 5);
                    var _tempStartDT = "2020/01/01 " + _fromTime;
                    var _tempEndDT = "2020/01/01 " + _toTime;
                    _fromTime = formatAMPM(new Date(_tempStartDT));
                    _toTime = formatAMPM(new Date(_tempEndDT));
                    var _dateForSort = stDt.toShortFormat() + " " + _fromTime;
                    var _startDate = sDay + " " + sMon + " " + _fromTime;
                    var _eventTypeName = "TodayOrWeekly";
                    var _period = obj.Period;
                    var _expression = (obj.Expression) == null ? "-" : obj.Expression;
                    var _roomNumber = (obj.RoomNumber) == null ? "-" : obj.RoomNumber;
                    var eventFormattedDate = moment(obj.EventDate).format(moment.HTML5_FMT.DATE);
                    var canAddSessions = ((new Date(eventFormattedDate + " " + obj.StartTime) - new Date()) > 0) && ($("#UserType").val() == 'Teacher' || $("#UserType").val() == 'SchoolAdmin' || $("#UserType").val() == 'Admin');
                    if (obj.WeekDay > 0) {
                        _weekDayName = fullWeekday[obj.WeekDay];
                    }

                    timetableEventTotalCount = obj.TotalCount;
                    var timeTableSessionLink = canAddSessions ? `<button  class='btn btn-primary btn-sm create-session ml-2 mt-0 rounded-4x timetable-session' data-timetable='{"SchoolGroupId" : ${obj.SchoolGroupId}, "EndTime" : "${obj.EndTime}", "FormattedMeetingDateTime":"${obj.EventDate}", "MeetingTime":"${obj.StartTime}", "MeetingName" :"${obj.CourseName}" }' href='javascript:void(0)' ><i class='fas fa-video mr-2'></i>` + translatedResources.ScheduleLiveSession + `</button>` : "";
                    if (categoryType == "Today" && (sDay == todayDay)) {

                        todayEventHtmlElements += '<div class="events-category success bg-white px-4" data-sortvalue="' + _dateForSort + '" style="--category-color: #ff86b3;cursor:pointer">'
                            + '<div class="events-time py-2 text-right">'
                            + '    <span>' + translatedResources.Start + '</span>'
                            + '     <span class="date d-block text-bold font-16 text-primary arabictxt"> ' + sDay + '</span>'
                            + '     <span class="month d-block text-semibold text-primary"> ' + sMon + '</span>'
                            + '     <p class="text-semibold mb-0 text-primary arabictxt"> ' + _fromTime + '</p>'
                            + ' </div>'
                            + ' <div class="events-detail w-100">'
                            + '     <div class="d-flex event-main-title justify-content-between text-semibold">' + obj.CourseName + timeTableSessionLink + '</div>'
                            + '     <div class="event-type d-flex align-items-center mt-0">'
                            + '         <span class="event-name text-semibold p-0 text-black-50">' + obj.CourseName + '</span>'
                            + '     </div>'
                            + '     <p class="event-description">'
                            + translatedResources.SessionWith.replace('{0}', obj.TeacherName)
                            + '         </p>'
                            + ' </div>'
                            + '<input type="hidden" class="hdnEventTypeName" value="' + _eventTypeName + '"/>'
                            + '<input type="hidden" class="hdnStartDate" value="' + _startDate + '"/>'
                            + '<input type="hidden" class="hdnFromTime" value="' + _fromTime + '"/>'
                            + '<input type="hidden" class="hdnToTime" value="' + _toTime + '"/>'
                            + '<input type="hidden" class="hdnTeacherName" value="' + obj.TeacherName + '"/>'
                            + '<input type="hidden" class="hdnPeriod" value="' + _period + '"/>'
                            + '<input type="hidden" class="hdnExpression" value="' + _expression + '"/>'
                            + '<input type="hidden" class="hdnRoomNumber" value="' + _roomNumber + '"/>'
                            + ' </div>'
                    }
                    else if (categoryType == "Week" || categoryType == "Month") {
                        weeklyOrMonthlyEventHtmlElements += '<div class="events-category success bg-white px-4" data-sortvalue="' + _dateForSort + '" style="--category-color: #ff86b3;cursor:pointer">'
                            + '<div class="events-time py-2 text-right">'
                            + '    <span>' + translatedResources.Start + '</span>'
                            + '     <span class="date d-block text-bold font-16 text-primary arabictxt"> ' + sDay + '</span>'
                            + '     <span class="month d-block text-semibold text-primary"> ' + sMon + '</span>'
                            + '     <p class="text-semibold mb-0 text-primary arabictxt"> ' + _fromTime + '</p>'
                            + ' </div>'
                            + ' <div class="events-detail w-100">'
                            + '     <div class="d-flex event-main-title justify-content-between text-semibold">' + obj.CourseName + timeTableSessionLink + '</div>'
                            + '     <div class="event-type d-flex align-items-center mt-0">'
                            + '         <span class="event-name text-semibold p-0 text-black-50">' + obj.CourseName + '</span>'
                            + '     </div>'
                            + '     <p class="event-description">'
                            + translatedResources.SessionWith.replace('{0}', obj.TeacherName)
                            + '         </p>'
                            + ' </div>'
                            + '<input type="hidden" class="hdnEventTypeName" value="' + _eventTypeName + '"/>'
                            + '<input type="hidden" class="hdnWeekDayName" value="' + _weekDayName + '"/>'
                            + '<input type="hidden" class="hdnFromTime" value="' + _fromTime + '"/>'
                            + '<input type="hidden" class="hdnToTime" value="' + _toTime + '"/>'
                            + '<input type="hidden" class="hdnTeacherName" value="' + obj.TeacherName + '"/>'
                            + '<input type="hidden" class="hdnPeriod" value="' + _period + '"/>'
                            + '<input type="hidden" class="hdnExpression" value="' + _expression + '"/>'
                            + '<input type="hidden" class="hdnRoomNumber" value="' + _roomNumber + '"/>'
                            + ' </div>'
                    }
                    else if (categoryType == "FilterByDate" && (_calendarFromDay >= sDay && _calendarFromDay <= eDay)) {
                        filterByDateEventHtmlElements += '<div class="events-category success bg-white px-4" data-sortvalue="' + _dateForSort + '" style="--category-color: #ff86b3;cursor:pointer">'
                            + '<div class="events-time py-2 text-right">'
                            + '    <span>' + translatedResources.Start + '</span>'
                            + '     <span class="date d-block text-bold font-16 text-primary arabictxt"> ' + _calendarFromDay + '</span>'
                            + '     <span class="month d-block text-semibold text-primary"> ' + sMon + '</span>'
                            + '     <p class="text-semibold mb-0 text-primary arabictxt"> ' + _fromTime + '</p>'
                            + ' </div>'
                            + ' <div class="events-detail w-100">'
                            + '     <div class="d-flex event-main-title justify-content-between text-semibold">' + obj.CourseName + timeTableSessionLink + '</div>'
                            + '     <div class="event-type d-flex align-items-center mt-0">'
                            + '         <span class="event-name text-semibold p-0 text-black-50">' + obj.CourseName + '</span>'
                            + '     </div>'
                            + '     <p class="event-description">'
                            + translatedResources.SessionWith.replace('{0}', obj.TeacherName)
                            + '         </p>'
                            + ' </div>'
                            + '<input type="hidden" class="hdnEventTypeName" value="' + _eventTypeName + '"/>'
                            + '<input type="hidden" class="hdnWeekDayName" value="' + _weekDayName + '"/>'
                            + '<input type="hidden" class="hdnFromTime" value="' + _fromTime + '"/>'
                            + '<input type="hidden" class="hdnToTime" value="' + _toTime + '"/>'
                            + '<input type="hidden" class="hdnTeacherName" value="' + obj.TeacherName + '"/>'
                            + '<input type="hidden" class="hdnPeriod" value="' + _period + '"/>'
                            + '<input type="hidden" class="hdnExpression" value="' + _expression + '"/>'
                            + '<input type="hidden" class="hdnRoomNumber" value="' + _roomNumber + '"/>'
                            + ' </div>'
                    }


                });

                if (categoryType == "Today") {
                    var todayEventsContainer;
                    var todayEventsHolderId;
                    if (eventType == "OnlineMeetings") {
                        todayEventsHolderId = $("#today-events-container");
                        todayEventsContainer = document.getElementById('today-events-container');
                    }
                    if (eventType == "Timetable") {
                        todayEventsHolderId = $("#today-timetable-container");
                        todayEventsContainer = document.getElementById('today-timetable-container');
                    }
                    if (eventType == "SchoolEvents") {
                        todayEventsHolderId = $("#today-schoolEvents-container");
                        todayEventsContainer = document.getElementById('today-schoolEvents-container');
                    }
                    if (eventType == "OtherEvents") {
                        todayEventsHolderId = $("#today-otherEvents-container");
                        todayEventsContainer = document.getElementById('today-otherEvents-container');
                    }

                    //var todayEventsContainer = document.getElementById('today-events-container');
                    if (todayEventHtmlElements == "") {
                        todayEventsContainer.innerHTML = "";
                        $("#noDataSpan").removeClass('d-none');
                        $("#noDataSpan").addClass('d-block');
                    }
                    else {
                        if (pageNumber == 1)
                            todayEventsContainer.innerHTML = todayEventHtmlElements;
                        else
                            todayEventsHolderId.append(todayEventHtmlElements);
                        $("#noDataSpan").removeClass('d-block');
                        $("#noDataSpan").addClass('d-none');

                        // sort today events
                        sortTodayEventsByDate(eventType);

                    }
                }


                var weekday = translatedResources.locale === 'en' ? ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"] : ["الأحد", "الأثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت"];
                var months = translatedResources.locale === 'en' ? ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
                    : ["يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
                if (categoryType == "Today") {
                    var todayDate = new Date();
                    var _todayDay = todayDate.getDate();
                    var _currentMonth = getMonthNm(todayDate);
                    var _currentWeekDay = weekday[todayDate.getDay()];

                    $("#all-events-title").text(translatedResources.eventson + " " + _currentWeekDay + ", " + todayDate.toShortFormat());
                }

                if (categoryType == "Week") {

                    var curr = new Date; // get current date
                    var first = curr.getDate() - curr.getDay();
                    var last = first + 6;

                    var firstday = new Date(curr.setDate(first));
                    var lastday = new Date(curr.setDate(last));
                    var _sWeekDay = weekday[firstday.getDay()];
                    var _eWeekDay = weekday[lastday.getDay()];

                    var _eventTitle = _sWeekDay + ", " + firstday.toShortFormat() + " " + translatedResources.To +" " + _eWeekDay + ", " + lastday.toShortFormat();

                    $("#all-events-title").text(_eventTitle);

                    var weeklyEventsContainer;
                    var weeklyEventsHolderId;
                    if (eventType == "OnlineMeetings") {
                        weeklyEventsHolderId = $("#weekly-events-container");
                        weeklyEventsContainer = document.getElementById('weekly-events-container');
                    }
                    if (eventType == "Timetable") {
                        weeklyEventsHolderId = $("#weekly-timetable-container");
                        weeklyEventsContainer = document.getElementById('weekly-timetable-container');
                    }
                    if (eventType == "SchoolEvents") {
                        weeklyEventsHolderId = $("#weekly-schoolEvents-container");
                        weeklyEventsContainer = document.getElementById('weekly-schoolEvents-container');
                    }
                    if (eventType == "OtherEvents") {
                        weeklyEventsHolderId = $("#weekly-otherEvents-container");
                        weeklyEventsContainer = document.getElementById('weekly-otherEvents-container');
                    }

                    //var weeklyEventsContainer = document.getElementById('weekly-events-container');
                    if (weeklyOrMonthlyEventHtmlElements == "") {
                        weeklyEventsContainer.innerHTML = "";
                        $("#noDataSpan").removeClass('d-none');
                        $("#noDataSpan").addClass('d-block');
                    }
                    else {
                        if (pageNumber == 1)
                            weeklyEventsContainer.innerHTML = weeklyOrMonthlyEventHtmlElements;
                        else
                            weeklyEventsHolderId.append(weeklyOrMonthlyEventHtmlElements);
                        $("#noDataSpan").removeClass('d-block');
                        $("#noDataSpan").addClass('d-none');
                    }
                    // sort weekly events
                    sortWeeklyEventsByDate(eventType);
                }
                else if (categoryType == "Month") {
                    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
                    var firstDay = new Date(y, m, 1);
                    var lastDay = new Date(y, m + 1, 0);

                    var _sWeekDay = weekday[firstDay.getDay()];
                    var _eWeekDay = weekday[lastDay.getDay()];

                    var _eventTitle = _sWeekDay + ", " + firstDay.toShortFormat() + " " + translatedResources.To + " " + _eWeekDay + ", " + lastDay.toShortFormat();
                    $("#all-events-title").text(_eventTitle);

                    //debugger
                    var monthlyEventsContainer;
                    var monthlyEventsHolderId;
                    if (eventType == "OnlineMeetings") {
                        monthlyEventsHolderId = $("#monthly-events-container");
                        monthlyEventsContainer = document.getElementById('monthly-events-container');
                    }
                    if (eventType == "Timetable") {
                        monthlyEventsHolderId = $("#monthly-timetable-container");
                        monthlyEventsContainer = document.getElementById('monthly-timetable-container');
                    }
                    if (eventType == "SchoolEvents") {
                        monthlyEventsHolderId = $("#monthly-schoolEvents-container");
                        monthlyEventsContainer = document.getElementById('monthly-schoolEvents-container');
                    }
                    if (eventType == "OtherEvents") {
                        monthlyEventsHolderId = $("#monthly-otherEvents-container");
                        monthlyEventsContainer = document.getElementById('monthly-otherEvents-container');
                    }

                    //var monthlyEventsContainer = document.getElementById('monthly-events-container');
                    if (weeklyOrMonthlyEventHtmlElements == "") {
                        monthlyEventsContainer.innerHTML = "";
                        $("#noDataSpan").removeClass('d-none');
                        $("#noDataSpan").addClass('d-block');
                    }
                    else {
                        if (pageNumber == 1)
                            monthlyEventsContainer.innerHTML = weeklyOrMonthlyEventHtmlElements;
                        else
                            monthlyEventsHolderId.append(weeklyOrMonthlyEventHtmlElements);

                        $("#noDataSpan").removeClass('d-block');
                        $("#noDataSpan").addClass('d-none');

                        //sort monthly events by datetime
                        sortMonthlyEventsByDate(eventType);
                    }
                }
                else if (categoryType == "FilterByDate") {
                    //debugger

                    var fromDate = _eventFromDate;

                    var splittedFromDate = fromDate.split("/");
                    var _selectedMonth = months[splittedFromDate[0] - 1];
                    var _selectedWeekDay = weekday[splittedFromDate[1]];

                    $("#all-events-title").text(translatedResources.eventson + ", " + splittedFromDate[1] + " " + _selectedMonth + " " + splittedFromDate[2]);

                    var filteredEventsContainer;
                    var filteredEventsHolderId;
                    if (eventType == "OnlineMeetings") {
                        filteredEventsHolderId = $("#today-events-container");
                        filteredEventsContainer = document.getElementById('today-events-container');
                    }
                    if (eventType == "Timetable") {
                        filteredEventsHolderId = $("#today-timetable-container");
                        filteredEventsContainer = document.getElementById('today-timetable-container');
                    }
                    if (eventType == "SchoolEvents") {
                        filteredEventsHolderId = $("#today-schoolEvents-container");
                        filteredEventsContainer = document.getElementById('today-schoolEvents-container');
                    }
                    if (eventType == "OtherEvents") {
                        filteredEventsHolderId = $("#today-otherEvents-container");
                        filteredEventsContainer = document.getElementById('today-otherEvents-container');
                    }
                    if (filterByDateEventHtmlElements == "") {
                        filteredEventsContainer.innerHTML = "";
                        $("#noDataSpan").removeClass('d-none');
                        $("#noDataSpan").addClass('d-block');
                    }
                    else {
                        if (pageNumber == 1)
                            filteredEventsContainer.innerHTML = filterByDateEventHtmlElements;
                        else
                            filteredEventsHolderId.append(filterByDateEventHtmlElements);

                        $("#noDataSpan").removeClass('d-block');
                        $("#noDataSpan").addClass('d-none');

                        //sort filter by date events
                        sortTodayEventsByDate(eventType);
                    }
                }

                //var loadedEventCount = $('.events-category').length;
                //if (loadedEventCount < 25) {
                //    $('.btn-LoadMoreOnline').removeClass('d-block');
                //    $('.btn-LoadMoreOnline').addClass('d-none');
                //    //$('.btn-LoadMoreTimetable').removeClass('d-block');
                //    //$('.btn-LoadMoreTimetable').addClass('d-none');
                //    $('.btn-LoadMoreOtherEvent').removeClass('d-block');
                //    $('.btn-LoadMoreOtherEvent').addClass('d-none');
                //}
                //else {
                //    $('.btn-LoadMoreOnline').removeClass('d-none');
                //    $('.btn-LoadMoreOnline').addClass('d-block');
                //    //$('.btn-LoadMoreTimetable').removeClass('d-none');
                //    //$('.btn-LoadMoreTimetable').addClass('d-block');
                //    $('.btn-LoadMoreOtherEvent').removeClass('d-none');
                //    $('.btn-LoadMoreOtherEvent').addClass('d-block');
                //}
                //if (timetableEventTotalCount == loadedEventCount) {
                //    $('.btn-LoadMoreTimetable').removeClass('d-block');
                //    $('.btn-LoadMoreTimetable').addClass('d-none');
                //}
                //if (onlineEventTotalCount == loadedEventCount) {
                //    $('.btn-LoadMoreOnline').removeClass('d-block');
                //    $('.btn-LoadMoreOnline').addClass('d-none');
                //}
                //if (schoolEventTotalCount == loadedEventCount) {
                //    $('.btn-LoadMoreSchoolEvent').removeClass('d-block');
                //    $('.btn-LoadMoreSchoolEvent').addClass('d-none');
                //}
                //if (otherEventTotalCount == loadedEventCount) {
                //    $('.btn-LoadMoreOtherEvent').removeClass('d-block');
                //    $('.btn-LoadMoreOtherEvent').addClass('d-none');
                //}



                globalFunctions.convertImgToSvg();

            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback
            }
        });

    $('button.fc-today-button').addClass("d-none");
}

function getMonthNm(d) {
    var month = new Array();
    if (translatedResources.locale === 'en') {
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
    }
    else {
        month[0] = "يناير";
        month[1] = "فبراير";
        month[2] = "مارس";
        month[3] = "أبريل";
        month[4] = "مايو";
        month[5] = "يونيو";
        month[6] = "يوليو";
        month[7] = "أغسطس";
        month[8] = "سبتمبر";
        month[9] = "أكتوبر";
        month[10] = "نوفمبر";
        month[11] = "ديسمبر";
    }

    var n = month[d.getMonth()];
    return n;
};

function formatAMPM(date) {
    let am = translatedResources.locale === 'en' ? 'am' : 'ص';
    let pm = translatedResources.locale === 'en' ? 'pm' : 'م';
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? pm : am;
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

Array.prototype.get = function (id) {

    callLoadPlannerDetails($(this), id);
    //planner.loadPlannerDetailsPopup($(this), id);
};

function callLoadPlannerDetails(source, id) {
    //debugger
    var sourceArray = $.map(source, function (value, index) {
        return [value];
    });
    var event = sourceArray.find(x => x.id == id)
    if (event.schoolName == undefined) {
        planner.loadPlannerDetailsPopup($(this), id);
    }

};

function sortTodayEventsByDate(eventType) {
    var $wrapper;
    if (eventType == "OnlineMeetings") {
        $wrapper = $('#today-events-container');
    }
    if (eventType == "Timetable") {
        $wrapper = $('#today-timetable-container');
    }
    if (eventType == "SchoolEvents") {
        $wrapper = $('#today-schoolEvents-container');
    }
    if (eventType == "OtherEvents") {
        $wrapper = $('#today-otherEvents-container');
    }

    $wrapper.find('.events-category').sort(function (a, b) {

        var dateA = new Date(a.dataset.sortvalue.replace(/([0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}) /, '$1T'))
        var dateB = new Date(b.dataset.sortvalue.replace(/([0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}) /, '$1T'))

        if (dateA < dateB) {
            return -1;
        }

        if (dateA > dateB) {
            return 1;
        }

    }).appendTo($wrapper);

}



function sortUpcomingEventsByDate() {
    if ($('.btn-today').hasClass('btn-primary')) {
        var $wrapper = $('#upcoming-events-container');

        $wrapper.find('.events-category').sort(function (a, b) {

            var dateA = new Date(a.dataset.sortvalue.replace(/([0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}) /, '$1T'))
            var dateB = new Date(b.dataset.sortvalue.replace(/([0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}) /, '$1T'))

            if (dateA < dateB) {
                return -1;
            }

            if (dateA > dateB) {
                return 1;
            }

        }).appendTo($wrapper);
    }
}

function sortWeeklyEventsByDate(eventType) {
    if ($('.btn-week').hasClass('btn-primary')) {

        var $wrapper;
        if (eventType == "OnlineMeetings") {
            $wrapper = $('#weekly-events-container');
        }
        if (eventType == "Timetable") {
            $wrapper = $('#weekly-timetable-container');
        }
        if (eventType == "SchoolEvents") {
            $wrapper = $('#weekly-schoolEvents-container');
        }
        if (eventType == "OtherEvents") {
            $wrapper = $('#weekly-otherEvents-container');
        }

        $wrapper.find('.events-category').sort(function (a, b) {

            var dateA = new Date(a.dataset.sortvalue.replace(/([0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}) /, '$1T'))
            var dateB = new Date(b.dataset.sortvalue.replace(/([0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}) /, '$1T'))

            if (dateA < dateB) {
                return -1;
            }

            if (dateA > dateB) {
                return 1;
            }

        }).appendTo($wrapper);
    }
}

function sortMonthlyEventsByDate(eventType) {

    if ($('.btn-month').hasClass('btn-primary')) {

        var $wrapper;
        if (eventType == "OnlineMeetings") {
            $wrapper = $('#monthly-events-container');
        }
        if (eventType == "Timetable") {
            $wrapper = $('#monthly-timetable-container');
        }
        if (eventType == "SchoolEvents") {
            $wrapper = $('#monthly-schoolEvents-container');
        }
        if (eventType == "OtherEvents") {
            $wrapper = $('#monthly-otherEvents-container');
        }

        $wrapper.find('.events-category').sort(function (a, b) {

            var dateA = new Date(a.dataset.sortvalue.replace(/([0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}) /, '$1T'))
            var dateB = new Date(b.dataset.sortvalue.replace(/([0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}) /, '$1T'))

            if (dateA < dateB) {
                return -1;
            }
            if (dateA > dateB) {
                return 1;
            }

        }).appendTo($wrapper);
    }
}


function onSessionSaveSuccess(data) {
    globalFunctions.showMessage(data.NotificationType, data.Message);
    if (data.Success)
        $("#myModal").modal("hide");
}