﻿/// <reference path="../../common/global.js" />
var dir = $("html").attr("dir");
var header = dir == 'ltr' ? { left: 'title', right: 'prev, next' } : { left: 'prev, next', right: 'title' };
var planner = function () {

    var init = function () {
    },
        addPlanner = function (source) {
            loadPlannerPopup(source, "");
        },
        loadPlannerDetailsPopup = function (source, id) {
            var title = translatedResources.planner;
            globalFunctions.loadPopup(source, '/Planner/MyPlanner/LoadPlannerDetails?id=' + id, title, 'myPlanner-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $(document).on('click', '#eventAttachmentViewer', function () {
                    var fileName = $('#ResourceFile').val();
                    planner.downloadAttachment(fileName);
                });
                $(document).on('click', '#editPlanner', function () {
                    var event = $('#EventId').val();
                    var id = btoa(event);
                    planner.editPlanner($(this), id);
                });
                
                //formElements.feSelect();
                //popoverEnabler.attachPopover();
                //$('select').selectpicker({
                //    noneSelectedText: "Please Select"
                //});
                //$(document).on('click', '#downloadAttachment', function () {
                //    var fileName = $('#FileName').val();
                //    suggestion.downloadAttachment(fileName);
                //});
            });
        },
        downloadAttachment = function (file) {
            var fileName = btoa(file)
            window.open("/Planner/MyPlanner/EventAttachmentViewer?fileName=" + fileName, "_blank");
        },
        cancelEvent = function (e) {

            var formData = new FormData();
            formData.append("EventId", $('#EventId').val());
            formData.append("Title", $('#Title').val());
            formData.append("Description", $('#Description').val());
            formData.append("StartDate", $('#StartDate').val());
            formData.append("StartTime", $('#StartTime').val());

            $.ajax({
                    type: 'POST',
                    url: '/Planner/MyPlanner/CancelEvent',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result.Success === true) {
                            $('#myModal').modal('hide');
                            globalFunctions.showMessage("success", translatedResources.cancelSuccess);
                            var calendarEl = document.getElementById('divCalender');
                            calendarEl.innerHTML = "";
                            GetEvents("");
                        }
                        else {
                            globalFunctions.onFailure();
                        }
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure();
                    }

                });

        },

        editPlanner = function (source, id) {
            //var title = mode + ' ' + translatedResources.planner;
            //var startDate = btoa(source.data('date'));
            //var startDate = btoa(source.dateStr);
            window.location.href = "/Planner/MyPlanner/InitAddEditPlannerForm?id=" + id;

            //debugger;
            //var datees = source.data('date');
            //$("#SelectedDate").val(source.data('date'));
        }
    loadPlannerPopup = function (source, id) {
        //debugger;
        //var title = mode + ' ' + translatedResources.planner;
        //var startDate = btoa(source.data('date'));
        var startDate = btoa(source.dateStr);
        window.location.href = "/Planner/MyPlanner/InitAddEditPlannerForm?id=" + id + "&sd=" + startDate;

        //debugger;
        //var datees = source.data('date');
        //$("#SelectedDate").val(source.data('date'));
    };

    return {
        init: init,
        loadPlannerDetailsPopup: loadPlannerDetailsPopup,
        editPlanner: editPlanner,
        cancelEvent: cancelEvent,
        downloadAttachment: downloadAttachment,
        addPlanner: addPlanner
    };
}();

$(document).ready(function () {
    planner.init();
    $("#btnBack").show();
    var globalEventType = "";
    var globalEventCategoryId = null;
    var isUpdatePermission = $("#IsUpdatePermission").val();

    var userType = $("#UserType").val();
    if (userType == "Teacher")
        $(".eventCategoryTimeTableFilter").hide();

    $(document).on('click', '#btnBack', function () {
        history.back();
    });
    
    $(document).on('click', '#btnCancelEvent', function () {
        globalFunctions.notyConfirm($(this), translatedResources.cancelConfirm);

        $(document).bind('okClicked', $(this), function (e) {
            planner.cancelEvent($(this));
        });

    });

    var calendar;

    GetEvents("");
    
    $('body').on('click', '.eventCategoryAllFilter', function () {

        $('.eventCategoryAllFilter').removeClass('btn-outline-primary');
        $('.eventCategoryAllFilter').addClass('btn-primary');
        $('.eventCategoryWholeSchoolFilter').addClass('btn-outline-primary');  // whole school
        $('.eventCategoryTimeTableFilter').addClass('btn-outline-primary');  // time table
        $('.eventCategoryFilter').addClass('btn-outline-primary');   //category wise filter

        var month = $(dir == "ltr" ? ".fc-left h2" : ".fc-right h2").text();
        console.log(month);
        var calendarEl = document.getElementById('divCalender');
        calendarEl.innerHTML = "";
        globalEventCategoryId = null;
        globalEventType = "";
        GetEvents(month, globalEventCategoryId, globalEventType);
    });
    $('body').on('click', '.eventCategoryFilter', function () {
      
        var _color = $(this).attr("color");
        $(this).removeClass('btn-outline-primary');
        $(this).css('background-color', _color);
        $(this).css('color', "#fff");

        $('.eventCategoryFilter').not(this).addClass('btn-outline-primary');   // exclude this 
        $('.eventCategoryWholeSchoolFilter').addClass('btn-outline-primary'); // whole school
        $('.eventCategoryTimeTableFilter').addClass('btn-outline-primary'); // time table
        $('.eventCategoryAllFilter').addClass('btn-outline-primary');   // all filter
        $('.eventCategoryAllFilter').removeClass('btn-primary');


        var month = $(dir == "ltr" ? ".fc-left h2" : ".fc-right h2").text();
        console.log(month);
        var calendarEl = document.getElementById('divCalender');
        calendarEl.innerHTML = "";
        var categoryId = $(this).data("id");
        globalEventCategoryId = categoryId;
        globalEventType = "";
        GetEvents(month, categoryId);
    });

    $('body').on('click', '.eventCategoryTimeTableFilter', function () {

        $('.eventCategoryAllFilter').addClass('btn-outline-primary');
        $('.eventCategoryAllFilter').removeClass('btn-primary');
        $('.eventCategoryWholeSchoolFilter').addClass('btn-outline-primary');   // whole school
        $('.eventCategoryTimeTableFilter').removeClass('btn-outline-primary');
        $('.eventCategoryTimeTableFilter').css('background-color', "#008000");  //green color
        $('.eventCategoryTimeTableFilter').css('color', "#fff");
        $('.eventCategoryFilter').addClass('btn-outline-primary');

        var month = $(dir == "ltr" ? ".fc-left h2" : ".fc-right h2").text();
        var calendarEl = document.getElementById('divCalender');
        calendarEl.innerHTML = "";
        var eventType = "StudentTimeTable";
        globalEventCategoryId = null;
        globalEventType = "StudentTimeTable";
        GetEvents(month, null, eventType);
    });

    $('body').on('click', '.eventCategoryWholeSchoolFilter', function () {

        $('.eventCategoryAllFilter').addClass('btn-outline-primary');
        $('.eventCategoryAllFilter').removeClass('btn-primary');
        $('.eventCategoryTimeTableFilter').addClass('btn-outline-primary');
        $('.eventCategoryWholeSchoolFilter').removeClass('btn-outline-primary');
        $('.eventCategoryWholeSchoolFilter').css('background-color', "#0000FF");
        $('.eventCategoryWholeSchoolFilter').css('color', "#fff");
        $('.eventCategoryFilter').addClass('btn-outline-primary');

        var month = $(dir == "ltr" ? ".fc-left h2" : ".fc-right h2").text();
        var calendarEl = document.getElementById('divCalender');
        calendarEl.innerHTML = "";
        var eventType = "WholeSchool";
        globalEventCategoryId = null;
        globalEventType = "WholeSchool";
        GetEvents(month, globalEventCategoryId, eventType);
    });

    $('body').on('click', 'button.fc-prev-button', function () {
        var month = $(dir == "ltr" ? ".fc-left h2" : ".fc-right h2").text();
        console.log(month);
        
        var calendarEl = document.getElementById('divCalender');
        calendarEl.innerHTML = "";
        GetEvents(month, globalEventCategoryId, globalEventType);
    });

    $('body').on('click', 'button.fc-next-button', function () {
        debugger
        console.log(globalEventType);
        console.log(globalEventCategoryId);
        var month = $(dir == "ltr" ? ".fc-left h2" : ".fc-right h2").text();
        console.log(month);
        var calendarEl = document.getElementById('divCalender');
        calendarEl.innerHTML = "";
        GetEvents(month, globalEventCategoryId, globalEventType);
    });
    //$('body').on('click', '.fc-day', function () {
    //    planner.addPlanner($(this));
    //});

    //$('body').on('click', '#divCalender .fc-body tr td table ', function () {
    //    planner.addPlanner($(this));
    //});
    $('body').on('click', 'button.fc-today-button', function () {
        alert("hi");
        //debugger;
        var calendarEl = document.getElementById('divCalender');
        calendarEl.innerHTML = "";
        GetEvents("");
    });

    $(document).on('click', '#ahrefDeleteEventFile', function () {
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
       
        var eventId = $(this).attr('data-EventId');
       
        $(document).bind('okClicked', $(this), function (e) {
            $.post('/Planner/MyPlanner/DeleteEventFile', { eventId: eventId }, function (response) {
                $('.resource-data').addClass('d-none');
            });
        });
    });

    $(document).on('click', '#ViewEventFile', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-EventId');
        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=myPlanner", "_blank");
        }
    });

});

function GetEvents(month, categoryId, eventType) {
    $.ajax(
        {
            url: '/Planner/MyPlanner/GetEvents?month=' + month + '&categoryId=' + categoryId + '&eventType=' + eventType,
            dataType: 'json', // type of response data
            //timeout: 500,     // timeout milliseconds
            async: false,
            success: function (data, status, xhr) {   // success callback function
                
                var _eventTempList = JSON.parse(data.events);
                var myplannerEventList;
                var eventsList;
                var studentEventsList;

                if (_eventTempList[0] != null)
                    myplannerEventList = _eventTempList[0];
                if (_eventTempList[1] != null)
                 eventsList = _eventTempList[1].data;
                if (_eventTempList[2] != null)
                    studentEventsList = _eventTempList[2].data;
                var eventArray = [];
               
                var myDate = new Date(data.defaultDate.match(/\d+/)[0] * 1);
                var defaultView = data.defaultView;

                $.each(myplannerEventList, function (i, obj) {
                    var events = new Object();

                    events.id = obj.eventId;
                    events.title = obj.title;
                    events.start = obj.startDate;
                    events.end = obj.endDate;
                    events.backgroundColor = obj.colorCode;
                    events.schoolName = obj.schoolName;
                    events.categoryThumbNailUrl = obj.categoryThumbNailUrl;
                    events.categoryDescription = obj.categoryDescription;
                    eventArray.push(events);
                });
                $.each(eventsList, function (i, obj) {
                    var events = new Object();

                    events.id = obj.eventID;
                    events.title = obj.eventDescription;
                    events.start = obj.eventStartDT;
                    events.end = obj.eventEndDT;
                    events.backgroundColor = obj.categoryColorCode;
                    events.schoolName = obj.schoolName;
                    events.categoryThumbNailUrl = obj.categoryThumbNailUrl;
                    events.categoryDescription = obj.categoryDescription;
                    eventArray.push(events);
                });
                $.each(studentEventsList, function (i, obj) {
                    var events = new Object();

                    events.title = obj.subjectDescription;
                    events.start = obj.date;
                    events.end = obj.date;
                    events.backgroundColor = "#008000";     // green color
                    events.startTm = obj.fromTime;
                    events.endTm = obj.toTime;
                    events.roomNo = obj.roomNo;
                    events.teacherName = obj.teacherName;
                    eventArray.push(events);
                });
                
                var calendarEl = document.getElementById('divCalender');

                calendar = new FullCalendar.Calendar(calendarEl, {
                    plugins: ['interaction', 'dayGrid', 'bootstrap'],
                    defaultView: defaultView,
                    header: header,
                    defaultDate: myDate,
                    contentHeight: 'auto',
                    navLinks: true,
                    editable: true,
                    themeSystem: 'bootstrap',
                    eventLimit: true, // allow "more" link when too many events
                    eventLimitText: "More", //sets the text for more events
                    events: eventArray,
                    eventStartEditable: false,
                    dir: dir,
                    //locale: translatedResources.locale,
                    dateClick: function (info) {
                       
                        var d = new Date();

                        var month = d.getMonth() + 1;
                        var day = d.getDate();

                        var todayDate = d.getFullYear() + '-' +
                            (('' + month).length < 2 ? '0' : '') + month + '-' +
                            (('' + day).length < 2 ? '0' : '') + day;

                        var clickedDate = info.dateStr;

                        if ((new Date(clickedDate) >= new Date(todayDate)) && ($("#UserType").val() != "Parent") && ($("#IsUpdatePermission").val() == "True")) {
                            planner.addPlanner(info);
                        }
                    },
                    eventClick: function (info) {
                       
                        var id = info.event.id;

                        if (info.event.id !== "") {
                            var event = eventArray.get(id);
                        }

                        if (info.event.id === "") {
                            
                            $("#studentEventPopup").modal("show");

                            $("#eventTitleHeading").text(info.event.title);
                            $("#eventSubDescription").html(info.event.title);
                            $("#eventStartTm").html(info.event.extendedProps.startTm);
                            $("#eventEndTm").html(info.event.extendedProps.endTm);
                            if (info.event.extendedProps.roomNo == null)
                                $("#eventRoomNumber").html("-");
                            else
                                $("#eventRoomNumber").html(info.event.extendedProps.roomNo);
                            if (info.event.extendedProps.teacherName == null)
                                $("#eventTeacherName").html("-");
                            else
                                $("#eventTeacherName").html(info.event.extendedProps.teacherName);
                        }
                        else if (info.event.extendedProps.schoolName != undefined) {

                            var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                            var start = moment(info.event.start).format('MMMM Do YYYY, h:mm:ss a');
                            var end = moment(info.event.end).format('MMMM Do YYYY, h:mm:ss a');

                            $("#eventPopup").modal("show");

                            $("#eventTitle").text(info.event.extendedProps.schoolName);
                            $("#eventDescription").html(info.event.title);
                            $("#eventStart").html(start);
                            $("#eventEnd").html(end);
                            $("#eventCategory").html(info.event.extendedProps.categoryDescription);
                            $("#eventThumbnail").html("<img src='" + info.event.extendedProps.categoryThumbNailUrl + "' style='width:50px;height:50px;'/>");
                        }
                    }
                });
                calendar.render();
            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback
            }
        });

    $('button.fc-today-button').addClass("d-none");
}
Array.prototype.get = function (id) {
   
    callLoadPlannerDetails($(this), id);
    //planner.loadPlannerDetailsPopup($(this), id);
};

function callLoadPlannerDetails(source, id) {
    var sourceArray = $.map(source, function (value, index) {
        return [value];
    });
    var event = sourceArray.find(x => x.id == id)
    if (event.schoolName == undefined) {
        planner.loadPlannerDetailsPopup($(this), id);
    }
    
};


