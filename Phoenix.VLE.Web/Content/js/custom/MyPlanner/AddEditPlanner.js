﻿/// <reference path="../../common/global.js" />
var OnlineMeetingTypes = {
    Teams: 1,
    Zoom: 2,
    RainbowClassroom: 5
}
var addEditplanner = function () {

    var init = function () {
    },
        addPlanner = function (source) {
            loadPlannerPopup(source, translatedResources.AddLabel);
        },
        viewAttachment = function (data) {
            var fileName = btoa($('#ResourceFile').val())
            window.open("/Planner/MyPlanner/EventAttachmentViewer?fileName=" + fileName, "_blank");
        },
        onSaveSuccess = function (data) {
            $('.closepanel').click();
            //globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                //$('#SchoolGroupId').val(data.InsertedRowId);
                $('#membersToAdd').removeClass('disabled');
                //$('#pills-SelectedStudent-tab').trigger('click');
                $('#pills-planner-tab').removeClass("active");
                $('#studentList').removeClass("show");
                $('#studentList').removeClass("active");
                $('#pills-SelectedStudent-tab').addClass("active");
                $('#selectedStudent').addClass("show");
                $('#selectedStudent').addClass("active");
                var id = $("#EventId").val();
                var _selectedValue = $('#EventCategoryId option:selected').text();
                $.get("/Planner/MyPlanner/ManageAssignee?id=" + id + "&categoryValue=" + _selectedValue, function (response) {
                    $('#selectedStudent').html(response);
                    //$('#groupDropdown').hide();
                    $('.selectpicker').selectpicker('refresh');
                    $(".manageAssignee-wrapper").mCustomScrollbar({
                        setHeight: "250",
                        autoExpandScrollbar: true,
                    });

                    LoadManageAssigneeTabDetails();

                    //enter manage assignee js before 
                }); //get manage assignee end here
            }

        },         //end of onSaveSuccess
        loadManageAssigneeTab = function () {
            $('#membersToAdd').removeClass('disabled');
            //$('#pills-SelectedStudent-tab').trigger('click');
            var id = $("#EventId").val();
            var _selectedValue = $('#EventCategoryId option:selected').text();
            $.get("/Planner/MyPlanner/ManageAssignee?id=" + id + "&categoryValue=" + _selectedValue, function (response) {
                $('#selectedStudent').html(response);
                $('.selectpicker').selectpicker('refresh');
                $(".manageAssignee-wrapper").mCustomScrollbar({
                    setHeight: "250",
                    autoExpandScrollbar: true,
                });


                LoadManageAssigneeTabDetails();
            });

        },
        onFailure = function (data) {
            globalFunctions.showMessage("error", data.Message);
        },

        getSelectedEventDetail = function (typeId) {
            var jsonData = JSON.parse(eventOptions).filter(function (item, index) {
                return item.Id == typeId;
            });
            return jsonData;
        },

        loadPlannerPopup = function (source, id) {
            //var title = mode + ' ' + translatedResources.planner;
            //globalFunctions.loadPopup(source, '/Planner/MyPlanner/InitAddEditPlannerForm?id=' + id, title, 'myPlanner-dailog');

            //$(source).off("modalLoaded");
            //$(source).on("modalLoaded", function () {
            //    formElements.feSelect();
            //    $('select').selectpicker({
            //        noneSelectedText: "Please Select"
            //    });
            //});
            window.location.href = "/Planner/MyPlanner/InitAddEditPlannerForm?id=" + id;
        };

    return {
        init: init,
        onSaveSuccess: onSaveSuccess,
        viewAttachment: viewAttachment,
        onFailure: onFailure,
        addPlanner: addPlanner,
        loadManageAssigneeTab: loadManageAssigneeTab,
        getSelectedEventDetail: getSelectedEventDetail
    };
}();

function LoadManageAssigneeTabDetails() {
    $('body').on('click', '#btnCancelManageAssignee', function () {
        $('#pills-planner-tab').click();
    });
    $('body').on('click', '.addAssignee', function () {
        //var QTypeId = $('#QuestionTypeId').val();
        // Finding total number of elements added
        var total_element = $(".element").length;

        // last <div> with element class id
        var lastid = $(".element:last").attr("id");
        var split_id = lastid.split("_");
        var nextindex = Number(split_id[1]) + 1;

        //var max = 5;
        // Check total number elements
        //if (total_element < max) {
        // Adding new div container after last occurance of element class
        $(".emailElement:last").after("<div class='row emailElement mb-2'> <div class='col-lg-6 col-md-6 col-6'> <div class='element' id='div_" + nextindex + "'></div></div>  <div class='col-lg-2 col md-2 col-12'> <button type='button' id='remove_" + nextindex + "' class='remove btn btn-outline-danger m-0'><i class='fas fa-times'></i></button> </div></div>");
        $("#div_" + nextindex).append("<div class='quiz-answer-field'><input type='text'  class='responseInput form-control' placeholder='" + translatedResources.EnterEmailId + "' id='txt_" + nextindex + "'> ");
        // Adding element to <div>
        //if (QTypeId == 1 || QTypeId == 2) {

        //    $("#div_" + nextindex).append("<div class='quiz-answer-field'><input type='text'  class='responseInput' placeholder='Enter Email Id' id='txt_" + nextindex + "'>  <div class='custom-control custom-radio custom-control-inline mr-0'><input type='radio' class='custom-control-input' name='single1' id='chkbox_" + nextindex + "' ><label class='custom-control-label' for='chkbox_" + nextindex + "'>" + translatedResources.MarkAsAnswer + "</label> </div></div>  <button type='button' id='remove_" + nextindex + "' class='remove btn btn-outline-primary m-0'><i class='fas fa-plus'></i></button>");
        //}
        //else {
        //    $("#div_" + nextindex).append("<div class='quiz-answer-field'><input type='text'  class='responseInput' placeholder='Enter Email Id' id='txt_" + nextindex + "'>  <div class='custom-control custom-checkbox custom-control-inline mr-0'><input type='checkbox' class='custom-control-input' id='chkbox_" + nextindex + "' ><label class='custom-control-label' for='chkbox_" + nextindex + "'>" + translatedResources.MarkAsAnswer + "</label> </div></div>  <button type='button' id='remove_" + nextindex + "' class='remove btn btn-outline-primary m-0'><i class='fas fa-plus'></i></button>");
        //}
        //}
    });
    // Remove element
    $('body').on('click', '.remove', function () {
        var id = this.id;
        var split_id = id.split("_");
        var deleteindex = split_id[1];

        // Remove <div> with id
        $("#div_" + deleteindex).remove();
        $("#remove_" + deleteindex).remove();

    });

    //$('#availablePlannerMemberSearch').on('keyup', function (e) {
    //    var searchText = $(this).val().toLowerCase();
    //    $('#unassignedMemberList div').each(function () {
    //        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
    //        $(this).toggle(showCurrentLi);
    //    });
    //});

    //$('#plannerMemberSearch').on('keyup', function (e) {
    //    var searchText = $(this).val().toLowerCase();
    //    $('#AssignedMemberList div').each(function () {
    //        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
    //        $(this).toggle(showCurrentLi);
    //    });

    //});
    $('#searchTeacher').on('keyup', function (e) {
        var searchText = $(this).val().toLowerCase();
        $('#TeachersList div').each(function () {
            var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
            $(this).toggle(showCurrentLi);
        });
    });
    $(document).on('keyup', '#availablePlannerMemberSearch', function (e) {
        //if (e.which === 13) {

        var searchText = $(this).val().toLowerCase();
        $('#unassignedMemberList div').each(function () {
            var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
            //$(this).toggle(showCurrentLi);

            if (showCurrentLi == true)
                $(this).show();
            else {
                $(this).removeClass("d-block");
                $(this).hide();
            }
        });
        //}
    });
    $(document).on('keyup', '#plannerMemberSearch', function (e) {
        //if (e.which === 13) {

        var searchText = $(this).val().toLowerCase();
        $('#AssignedMemberList div').each(function () {
            var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
            //$(this).toggle(showCurrentLi);

            if (showCurrentLi == true)
                $(this).show();
            else {
                $(this).removeClass("d-block");
                $(this).hide();
            }
        });
        //}
    });
    $('#searchSelectedTeacher').on('keyup', function (e) {
        var searchText = $(this).val().toLowerCase();
        $('#SelectedTeacherList div').each(function () {
            var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
            $(this).toggle(showCurrentLi);
        });

    });
    //Student 
    $('#selectAllUnassignedPlannerMember').change(function (e) {
        var $inputs = $('#unassignedMemberList input[type=checkbox]');
        if (e.originalEvent === undefined) {
            var allChecked = true;
            $inputs.each(function () {
                allChecked = allChecked && this.checked;
            });
            this.checked = allChecked;
        } else {
            $inputs.prop('checked', this.checked);
        }
    });

    //$('input[type=checkbox]', "#unassignedMemberList").on('change', function () {
    //    $('#selectAllUnassignedPlannerMember').trigger('change');
    //});

    //$('input[type=checkbox]', "#AssignedMemberList").on('change', function () {
    //    $('#selectAllAssignedPlannerMember').trigger('change');
    //});

    $(document).on('click', '.checkboxUnassigned', function () {
        if ($('.checkboxUnassigned:checked').length == $('.checkboxUnassigned').length) {
            $('#selectAllUnassignedPlannerMember').prop('checked', true);
        } else {
            $('#selectAllUnassignedPlannerMember').prop('checked', false);
        }
    });

    $(document).on('click', '.checkboxAssigned', function () {
        if ($('.checkboxAssigned:checked').length == $('.checkboxAssigned').length) {
            $('#selectAllAssignedPlannerMember').prop('checked', true);
        } else {
            $('#selectAllAssignedPlannerMember').prop('checked', false);
        }
    });


    $('#selectAllAssignedPlannerMember').change(function (e) {

        //var $inputs = $('#AssignedMemberList input[type=checkbox]');
        //if (e.originalEvent === undefined) {
        //    var allChecked = true;
        //    $inputs.each(function () {
        //        allChecked = allChecked && this.checked;
        //    });
        //    this.checked = allChecked;
        //} else {
        //    $inputs.prop('checked', this.checked);
        //}

        var $inputs = $('#AssignedMemberList input[type=checkbox]');
        if (this.checked) {
            $inputs.each(function () {
                this.checked = true;
            });
        } else {
            $inputs.each(function () {
                this.checked = false;
            });
        }

    });

    //end student 
    $('#SelectSelectedAllTeachers').change(function (e) {
        var $inputs = $('#SelectedTeacherList input[type=checkbox]');
        if (e.originalEvent === undefined) {
            var allChecked = true;
            $inputs.each(function () {
                allChecked = allChecked && this.checked;
            });
            this.checked = allChecked;
        } else {
            $inputs.prop('checked', this.checked);
        }
    });

    $('input[type=checkbox]', "#SelectedTeacherList").on('change', function () {
        $('#SelectSelectedAllTeachers').trigger('change');
    });
    $('input[type=checkbox]', "#TeachersList").on('change', function () {
        $('#SelectAllTeachers').trigger('change');
    });

    $('#SelectAllTeachers').change(function (e) {
        var $inputs = $('#TeachersList input[type=checkbox]');
        if (e.originalEvent === undefined) {
            var allChecked = true;
            $inputs.each(function () {
                allChecked = allChecked && this.checked;
            });
            this.checked = allChecked;
        } else {
            $inputs.prop('checked', this.checked);
        }
    });

    $("#btnAddPlannerMember").click(function () {
        var $selectedUsers = $("#unassignedMemberList input[type='checkbox']:checked").parent('.custom-checkbox');
        //var usersListId = $("#UsersListId option:selected").val();
        var output = jQuery.map($('#AssignedMemberList :checkbox'), function (n, i) {
            return n.value;
        }).join(',');
        if ($selectedUsers != null && $selectedUsers.length > 0) {
            $selectedUsers.each(function (i, elem) {
                if ($("#SelectedPlannerMemberId").val().indexOf("") == -1) {
                    $("#SelectedPlannerMemberId").val($(this).children("input[type='checkbox']").val() + ",");
                }
                else {
                    if (output != "" && i == 0) {
                        $("#SelectedPlannerMemberId").val(output + ",");
                    }
                    $("#SelectedPlannerMemberId").val($("#SelectedPlannerMemberId").val() + $(this).children("input[type='checkbox']").val() + ",");
                }
            });


            $("#AssignedMemberList").append($selectedUsers);
            $("#AssignedMemberList").html($("#AssignedMemberList .custom-checkbox").sort(function (a, b) {
                return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
            }));

            $("#AssignedMemberList input[type='checkbox']").prop('checked', $('#selectAllAssignedPlannerMember').is(':checked'));
            $("#AssignedMemberList input[type='checkbox']").addClass('checkboxAssigned');
            $("#AssignedMemberList input[type='checkbox']").removeClass('checkboxUnassigned');
            $("#AssignedMemberList .custom-checkbox").addClass('d-block');
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.selectAdd);
        }
    });

    $("#btnRemovePlannerMember").click(function () {
        var $selectedUsers = $("input[type=checkbox].checkboxAssigned");

        if ($selectedUsers.length > 0) {
            var deselectedMemberList = [], selectedPlannerMemberList = [];

            $selectedUsers.each(function (i, elem) {
                if ($(this).is(":checked")) {
                    $(this).removeClass("checkboxAssigned").addClass("checkboxUnassigned");
                    $(this).parent('.custom-checkbox').appendTo($("#unassignedMemberList"));
                    deselectedMemberList.push($(this).attr("id"));
                }
                else {
                    selectedPlannerMemberList.push($(this).attr("id"));
                }

            });
            $("#SelectedPlannerMemberId").val(selectedPlannerMemberList.join(","));
            if ($("#EventId").val() > 0)
                $("#DeselectedPlannerMemberId").val(deselectedMemberList.join(","));

            $("#unassignedMemberList").html($("#unassignedMemberList .custom-checkbox").sort(function (a, b) {
                return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
            }));
            $("#unassignedMemberList input[type='checkbox']").prop('checked', $('#selectAllUnassignedPlannerMember').is(':checked'));
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.selectRemove);
        }
    });

    $("#btnAddStudent").click(function () {
        var $selectedUsers = $("#StudentList input[type='checkbox']:checked").parent('.custom-checkbox');
        //var usersListId = $("#UsersListId option:selected").val();
        var output = jQuery.map($('#SelectedStudentList :checkbox'), function (n, i) {
            return n.value;
        }).join(',');
        if ($selectedUsers != null && $selectedUsers.length > 0) {
            $selectedUsers.each(function (i, elem) {
                if ($("#SelectedStudentId").val().indexOf("") == -1) {
                    $("#SelectedStudentId").val($(this).children("input[type='checkbox']").val() + ",");
                }
                else {
                    if (output != "" && i == 0) {
                        $("#SelectedStudentId").val(output + ",");
                    }
                    $("#SelectedStudentId").val($("#SelectedStudentId").val() + $(this).children("input[type='checkbox']").val() + ",");
                }
            });


            $("#SelectedStudentList").append($selectedUsers);
            $("#SelectedStudentList").html($("#SelectedStudentList .custom-checkbox").sort(function (a, b) {
                return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
            }));

            $("#SelectedStudentList input[type='checkbox']").prop('checked', $('#SelectAllSelectedStduent').is(':checked'));
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.selectAdd);
        }
    });

    $("#btnRemoveStudent").click(function () {
        var $selectedUsers = $("#SelectedStudentList input[type='checkbox']:checked").parent('.custom-checkbox');

        if ($selectedUsers != null && $selectedUsers.length > 0) {
            $selectedUsers.each(function (i, elem) {
                if ($("#SelectedStudentId").val().indexOf($(this).children("input[type='checkbox']").val() + ",") != -1) {
                    $("#SelectedStudentId").val($("#SelectedStudentId").val().replace($(this).children("input[type='checkbox']").val() + ",", ""));
                }
                else {
                    $("#DeselectedStudentId").val($("#DeselectedStudentId").val() + $(this).children("input[type='checkbox']").val() + ",");
                }
            });

            $("#StudentList").append($selectedUsers);
            $("#StudentList").html($("#StudentList .custom-checkbox").sort(function (a, b) {
                console.log($(a).children('.custom-control-label').text());
                return $(a).children('.custom-control-label').text() == $(b).children('.custom-control-label').text() ? 0 : $(a).children('.custom-control-label').text() < $(b).children('.custom-control-label').text() ? -1 : 1;
            }));
            $("#StudentList input[type='checkbox']").prop('checked', $('#SelectAllStudent').is(':checked'));
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.selectRemove);
        }


    });
    $('#memberToDisplay').on('change', function (event) {
        var memberType = $('#memberToDisplay option:selected').text();
        var memberTypeId = $('#memberToDisplay option:selected').val();
        if (memberType == "Other Groups") {
            $('#groupDropdownDiv').show();
            $('#groupDropdown').show();
            $('#unassignedMemberList').html("");
        }
        else {
            $('#groupDropdownDiv').hide();
            $('#groupDropdown').hide();
            $.ajax({

                data: { memberTypeId: memberTypeId, eventId: $("#EventId").val() },
                url: '/Planner/MyPlanner/GetUnassignedMembers',
                success: function (result) {

                    $('#unassignedMemberList').html(result);
                },
                error: function (data) { }
            });
        }
    });
    $('#groupDropdown').on('change', function (event) {
        var groupId = $('#groupDropdown option:selected').val();
        if (groupId != '' && groupId != undefined) {
            $.ajax({

                //data: { groupId: groupId, formGroupId: formGroupId },
                //url: '/SchoolGroup/SchoolGroup/GetStudentByGroupId',
                data: { groupId: groupId, eventId: $("#EventId").val() },
                url: '/Planner/MyPlanner/GetStudentsInGroup',
                success: function (result) {
                    $('#unassignedMemberList').html(result);
                },
                error: function (data) { }
            });
        }
        else {
            $('#unassignedMemberList').html('');
        }
    });
    $("#btnSubmitPlanner").click(function () {

        var responseInputs = $(".responseInput");
        var _selectedCatValue = $('#EventCategoryId option:selected').text();
        var $selectedUsers = $("#unassignedMemberList input[type='checkbox']").parent('.custom-checkbox');
        var externalEmails = new Array();
        if ($('#SelectedPlannerMemberId').val() === "" && _selectedCatValue.toLowerCase() == "online meeting") {
            globalFunctions.showWarningMessage(translatedResources.selectUserWarning);
            return false;
        }
        if ($selectedUsers != null && $selectedUsers.length > 0) {
            if ($('#SelectedPlannerMemberId').val() === "") {
                globalFunctions.showWarningMessage(translatedResources.addUserWarning);
                return false;
            }
        }
        if (responseInputs.length >= 1) {
            for (var i = 0; i < responseInputs.length; i++) {
                var email = $("#txt_" + (i + 1)).val();

                if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email) && responseInputs.length == 1 && email.trim() != "") {
                    globalFunctions.showWarningMessage(translatedResources.emailWaring);
                    return false;
                }
                else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email) || email.trim() == "") {
                    if (responseInputs.length > 1) {
                        globalFunctions.showWarningMessage(translatedResources.emailWaring);
                        return false;
                    }
                }
                else {
                    externalEmails[i] = email;
                }
            }
        }
        if ($('form#frmAddPlanner').valid()) {
            var categoryId = $('#EventCategoryId').val();
            var selectedEvent = addEditplanner.getSelectedEventDetail(categoryId)[0];
            if (selectedEvent.MeetingTypeId == 1 || selectedEvent.MeetingTypeId == 2) {
                if (!globalFunctions.isValueValid($('#StartTime').val()) || !globalFunctions.isValueValid($('#EndTime').val())) {
                    globalFunctions.showWarningMessage("Start & end time is required for online meeting");
                    return;
                }
                if (!globalFunctions.isValueValid($('#SelectedPlannerMemberId').val())) {
                    globalFunctions.showWarningMessage(translatedResources.selectParticipants);
                    return;
                }
            }

            var formData = new FormData();
            var isRecurringEvent = (selectedEvent.MeetingTypeId == OnlineMeetingTypes.Teams || selectedEvent.MeetingTypeId == OnlineMeetingTypes.Zoom) && $("#IsRecurringEvent").is(":checked");
            formData.append("IsRecurringEvent", isRecurringEvent);
            if (isRecurringEvent) {
                var eventWeekDays = [];
                $("span.event-day.badge-success").each(function () {
                    eventWeekDays.push($(this).data("day"));
                });

                if (eventWeekDays.length == 0) {
                    globalFunctions.showWarningMessage(translatedResources.selectMeetingsDays);
                    return;
                }
                formData.append("RecurringEventDays", eventWeekDays.join(","));
            }
            var resourceFile = document.getElementById("dropPlannerAttachment").files[0];
            formData.append("resourceFile", resourceFile);
            formData.append("Title", $('#Title').val());
            formData.append("ExternalEmails", externalEmails);
            //formData.append("ExternalEmails", postExternalEmail);
            formData.append("EventId", $('#EventId').val());
            formData.append("IsAddMode", $('#IsAddMode').val());
            formData.append("EventCategoryId", categoryId);
            formData.append("StartTime", globalFunctions.toSystemReadableTime($('#StartTime').val()));
            formData.append("EndTime", globalFunctions.toSystemReadableTime($('#EndTime').val()));
            formData.append("StartDate", globalFunctions.toSystemReadableDate($("#FromDate").val().replace('--', '-')));
            formData.append("EndDate", globalFunctions.toSystemReadableDate($("#ToDate").val().replace('--', '-')));
            formData.append("SelectedDate", $("#SelectedDate").val());
            formData.append("DurationId", $('#DurationId').val());
            formData.append("EventPassword", $('#EventPassword').val());
            formData.append("OnlineMeetingId", $("#OnlineMeetingId").val());
            formData.append("Venue", $('#Venue').val());
            formData.append("Description", $('#Description').val());
            formData.append("SelectedPlannerMemberId", $('#SelectedPlannerMemberId').val());
            formData.append("DeselectedPlannerMemberId", $('#DeselectedPlannerMemberId').val());
            formData.append("OnlineMeetingType", selectedEvent.MeetingTypeId);
            formData.append("OldMeetingType", $("#OldMeetingType").val());

            $.ajax({
                type: 'POST',
                url: '/Planner/MyPlanner/InsertUpdatePlanner',
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    globalFunctions.showMessage(result.NotificationType, result.Message);
                    if (result.Success == true) {
                        window.location.href = '/planner/myplanner';
                    }
                    //$('#myModal').modal('hide');

                    //loadContentLibraryGrid();

                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });
            $("#myModal").modal('hide');
        }
    });

}    // end of LoadManageAssigneeTabDetails function

$(document).ready(function () {
    var currentDate = new Date();
    $("#DateTimeOffset").val(currentDate.getTimezoneOffset());
    $("#btnBack").show();

    $("#Title, #StartTime, #EndTime, #EventPassword").each(function () {
        $(this).next().attr("for", $(this).attr("id"))
    });

    var isAddMode = $("#IsAddMode").val();
    var nowTemp = new Date();
    var newTempDate = $("#SelectedDate").val();
    var tempDate = newTempDate.split('-');

    if (isAddMode == "False") {
        var _stDate = $("#StartDate").val();
        var _tmpStartDate = _stDate.split('/');
        var _edDate = $("#EndDate").val();
        var _tmpEndDate = _edDate.split('/');

        var _fromDate = new Date(nowTemp.getFullYear(), parseInt(_tmpStartDate[0]) - 1, parseInt(_tmpStartDate[1]), 0, 0, 0, 0);
        var _toDate = new Date(nowTemp.getFullYear(), parseInt(_tmpEndDate[0]) - 1, parseInt(_tmpEndDate[1]), 0, 0, 0, 0);
    }

    // old code
    //var stDate = new Date(parseInt(tempDate[0]), parseInt(tempDate[1]) - 1, parseInt(tempDate[2]), 0, 0, 0, 0);
    //var endDate = new Date(parseInt(tempDate[0]), parseInt(tempDate[1]) - 1, parseInt(tempDate[2]), 0, 0, 0, 0);

    var stDate = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), nowTemp.getDate(), 0, 0, 0, 0);
    var endDate = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), nowTemp.getDate(), 0, 0, 0, 0);

    var minDateData = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), nowTemp.getDate(), 0, 0, 0, 0);

    $('.date-picker2').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: false,
        minDate: minDateData,
        locale: translatedResources.locale
    });

    $('.date-picker1').datetimepicker({
        format: 'DD-MMM-YYYY',
        minDate: minDateData,
        locale: translatedResources.locale

    }).on('dp.change', function (e) {
        $('.date-picker2').data("DateTimePicker").minDate(e.date);

        //console.log("Selected date:" + e.date._d);
        var selectedDate = new Date(e.date._d);
        if ($('.date-picker2').data("DateTimePicker").date() < e.date) {

            //$('.date-picker2').data("DateTimePicker").clear();
            $('.date-picker2').data("DateTimePicker").minDate(e.date);
            $("#ToDate").val(globalFunctions.getFormattedDate(selectedDate));
        }
    });

    $(".date-picker2").on("dp.change", function (e) {
        $('.date-picker1').data("DateTimePicker").maxDate(e.date);
    });

    if (isAddMode == "True") {
        $("#FromDate").removeAttr('value');
        $("#FromDate").val(globalFunctions.getFormattedDate(stDate));
        $("#ToDate").val(globalFunctions.getFormattedDate(endDate));
    }
    else {
        _fromDate = new Date($("#FromDate").attr('value')), _toDate = new Date($("#ToDate").attr('value'));
        $("#FromDate").val(globalFunctions.getFormattedDate(_fromDate));
        $("#ToDate").val(globalFunctions.getFormattedDate(_toDate));
    }

    $(document).on('change', "#IsRecurringEvent", function () {
        var isRecurringEvent = $(this).is(":checked");
        $("#divEventDays").toggle(isRecurringEvent);
    });

    $("#FromDate").blur(function () {
        var fieldValue = $(this).val();
        if (fieldValue == "") {
            $("#FromDate").val(globalFunctions.getFormattedDate(stDate));
            $("#FromDate").next().addClass("active");
        }

    });


    $(document).on('change', '#EventCategoryId', function () {
        var eventCategoryId = $(this).val();
        if (!globalFunctions.isValueValid(eventCategoryId)) {
            $("#divOnlineMeetingPassword").hide();
            return;
        } else $("#EventCategoryId-error").empty();

        var eventType = addEditplanner.getSelectedEventDetail(eventCategoryId)[0];
        var showPasswordBox = eventType.MeetingTypeId == OnlineMeetingTypes.Zoom;
        $("#divOnlineMeetingPassword").toggle(showPasswordBox);
        showPasswordBox ? $("#EventPassword").removeAttr("disabled") : $("#EventPassword").attr("disabled", "disabled");

        $("#divRecurringEvent").toggle((eventType.MeetingTypeId == OnlineMeetingTypes.Teams || eventType.MeetingTypeId == OnlineMeetingTypes.Zoom));
        if (eventType.MeetingTypeId == OnlineMeetingTypes.Teams) {
            $.get("/SchoolStructure/SchoolGroups/CheckMSTeamsAccessToken", function (data) {
                if (!data.Success) {
                    localStorage.setItem("external_auth_complete", "false");
                    var element = $("<a/>", {
                        id: 'action-externalauth',
                    });
                    element.data("redirectUri", data.HeaderText);
                    element.appendTo($("body"));
                    element.trigger("click");
                }
            });
        }

    });

    $("#ToDate").blur(function () {
        var fieldValue = $(this).val();
        if (fieldValue == "") {
            $("#ToDate").val(globalFunctions.getFormattedDate(stDate));
            $("#ToDate").next().addClass("active");
        }
    });

    if (globalFunctions.isDataValid('EventCategoryId')) {
        $("#EventCategoryId").trigger('change');
    }

    $(document).on('click', '#btnBack', function () {
        window.history.back();
    });

    $('.selected-student-tab').click(function () {
        addEditplanner.loadManageAssigneeTab();
    });

    //timepicker
    var minTime = moment();
    if (isAddMode == "True") {
        $('.time-picker').datetimepicker({
            format: "LT",
            widgetPositioning: {
                vertical: 'bottom'
            },
            locale: translatedResources.locale//minDate: minTime
        });
        $("#StartTime").on('dp.change', function (ev) {
            checkTimeDifference(ev);
        }).data("DateTimePicker");

    }
    else {
        $('.time-picker').datetimepicker({

            format: "LT",
            widgetPositioning: {
                vertical: 'bottom'
            },
            locale: translatedResources.locale
        });
        $("#StartTime").on('dp.change', function (ev) {
            checkTimeDifference(ev);
        }).data("DateTimePicker");

    }

    //$('.date-picker').datetimepicker('show');
    //$('.date-picker').datetimepicker('hide');

    function checkTimeDifference(e) {
        if (($('#StartTime').val() == '') || ($('#EndTime').val() == '')) return;

        var strStartTime = globalFunctions.toSystemReadableTime($('#StartTime').val());
        var strEndTime = globalFunctions.toSystemReadableTime($('#EndTime').val());

        //convert both time into timestamp
        var startTime = new Date("January 01, 2020 " + strStartTime);
        startTime = startTime.getTime();

        var endTime = new Date("January 01, 2020 " + strEndTime);
        endTime = endTime.getTime();
        if (startTime >= endTime) {
            if (e != undefined)
                $("#EndTime").data("DateTimePicker").minDate(e.date.add(5, "minutes"));
            globalFunctions.showWarningMessage(translatedResources.startTimeWarning);
            return false;
        }
        else {
            return true;
        }
    }

    function checkDateFieldValidation() {
        if (($('#FromDate').val() == '') || ($('#ToDate').val() == '')) {
            globalFunctions.showWarningMessage(translatedResources.endDateValWarning);
            return false;
        }
        else {
            return true;
        }
    }

    $("#btnSavePlanner").click(function () {
        var isStartTimeLess = checkTimeDifference();
        var idDateFieldNull = checkDateFieldValidation();
        var fromDate = $("#FromDate").val().replace('--', '-'); toDate = $("#ToDate").val().replace('--', '-');
        var isRecurringEvent = $("#IsRecurringEvent").is(":checked");
        if (isStartTimeLess == false || idDateFieldNull == false) {
            return false;
        }
        else if(fromDate == toDate && isRecurringEvent) {
            globalFunctions.showWarningMessage("Start date and end date cannot be same for recurring event");
            return false;
        }
        else {
            $("#frmAddPlanner").submit();

        }
    });

    $('#dropPlannerAttachment').click(function () {
        $('.fileinput-upload-button').show();
    });

    $("#dropPlannerAttachment").fileinput({
        language: translatedResources.locale,
        title: translatedResources.BrowseFile,
        theme: "fas",
        showUpload: true,
        showCaption: false,
        showRemove: true,
        showMultipleNames: false,
        maxFileCount: 1,
        fileActionSettings: {
            showZoom: false,
            showDrag: false
        },
        browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
        removeClass: "btn btn-sm m-0 z-depth-0",
        uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
        cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
        overwriteInitial: false,
        initialPreviewAsData: false, // defaults markup
        initialPreviewConfig: [{
            frameAttr: {
                title: "CustomTitle",
            }
        }],
        preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
        previewFileIconSettings: { // configure your icon file extensions
            'doc': '<i class="fas fa-file-word ic-file-word"></i>',
            'xls': '<i class="fas fa-file-excel ic-file-excel"></i>',
            'ppt': '<i class="fas fa-file-powerpoint ic-file-ppt"></i>',
            'pdf': '<i class="fas fa-file-pdf ic-file-pdf"></i>',
            'zip': '<i class="fas fa-file-archive ic-file"></i>',
            'htm': '<i class="fas fa-file-code ic-file"></i>',
            'txt': '<i class="fas fa-file-alt ic-file"></i>',
            'mov': '<i class="fas fa-file-video ic-file"></i>',
            'mp3': '<i class="fas fa-file-audio ic-file"></i>',
            'jpg': '<i class="fas fa-file-image ic-file-img"></i>',
            'jpeg': '<i class="fas fa-file-image ic-file-img"></i>',
            'gif': '<i class="fas fa-file-image ic-file-img"></i>',
            'png': '<i class="fas fa-file-image ic-file-img"></i>'
        },
        previewSettings: {
            image: { width: "50px", height: "auto" },
            html: { width: "50px", height: "auto" },
            other: { width: "50px", height: "auto" }
        },
        previewFileExtSettings: { // configure the logic for determining icon file extensions
            'doc': function (ext) {
                return ext.match(/(doc|docx)$/i);
            },
            'xls': function (ext) {
                return ext.match(/(xls|xlsx)$/i);
            },
            'ppt': function (ext) {
                return ext.match(/(ppt|pptx)$/i);
            },
            'zip': function (ext) {
                return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
            },
            'htm': function (ext) {
                return ext.match(/(htm|html)$/i);
            },
            'txt': function (ext) {
                return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
            },
            'mov': function (ext) {
                return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
            },
            'mp3': function (ext) {
                return ext.match(/(mp3|wav)$/i);
            }
        }

    });


    $(".fileinput-upload-button").on("click", function () {
        //$('.file-upload-widget').animate({
        // right: '-300'
        //});
        var resourceFile = document.getElementById("dropPlannerAttachment").files[0];
        if (resourceFile == undefined || resourceFile == '') {
            $('.fileinput-upload-button').hide();
            $("#fileUploadModal").modal('hide');
            return;
        }
        $('.fileinput-upload-button').hide();
        $("#fileUploadModal").modal('hide');
        globalFunctions.showSuccessMessage(translatedResources.UploadSuccessMessage);

    });


    $(document).off('click', "#ahrefDeleteEventFile");
    $(document).on('click', '#ahrefDeleteEventFile', function () {
        var source = $(this);
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);

        var eventId = $(source).attr('data-EventId');
        $(source).off("okClicked");

        $(document).bind('okClicked', $(this), function (e) {
            $.post('/Planner/MyPlanner/DeleteEventFile', { eventId: eventId }, function (response) {
                $('.resource-data').addClass('d-none');
            });
        });
    });

    $(document).on('click', '#ViewEventFile', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-EventId');

        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=myPlanner", "_blank");
        }
    });

    // quick action button
    if ($('.cd-stretchy-nav').length > 0) {
        var stretchyNavs = $('.cd-stretchy-nav');

        stretchyNavs.each(function () {
            var stretchyNav = $(this),
                stretchyNavTrigger = stretchyNav.find('.cd-nav-trigger');

            stretchyNavTrigger.on('click', function (event) {
                event.preventDefault();
                stretchyNav.toggleClass('nav-is-visible');
            });
        });

        $(document).on('click', function (event) {
            (!$(event.target).is('.cd-nav-trigger') && !$(event.target).is('.cd-nav-trigger span')) && stretchyNavs.removeClass('nav-is-visible');
        });
    }

    $("input").on("keypress", function (e) {
        if (e.which === 32 && !this.value.length)
            e.preventDefault();
    });

    $(document).on("click", 'span.event-day', function () {
        var el = $(this);
        if (el.hasClass("badge-light"))
            el.removeClass("badge-light").addClass("badge-success");
        else el.removeClass("badge-success").addClass("badge-light");
    });

});
