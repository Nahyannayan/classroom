﻿var _userLognType = "", usr_Teacher = "teacher", usr_Parent = "parent", usr_Student = "student", usr_Admin = "Admin";

$(document).ready(function () {

    //Load todays schedule data
    GetWeeklyEvents();

    //$(document).on("change", "#ddlFilterEvents", function () {
    //    var categoryIds = $("#ddlFilterEvents").val();

    //    GetWeeklyEvents(true, categoryIds);
    //});

    _userLognType = $("#hdnLognUsrType").val();

    if (_userLognType != usr_Parent) {
        setInterval(function () {
            refreshLiveSessionStatus();
        }, 60000);
    }

    $(".todaysScheduleCarousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        responsive: {
            0: {
                items: 1,
                slideBy: 1
            },
            768: {
                items: 3,
                slideBy: 3
            },
            1366: {
                items: 4,
                slideBy: 4
            },
            1920: {
                items: 5,
                slideBy: 5
            }
        }
    });

    $(document).on("click", ".rainbow-classroom.join-classroom", function () {
        var dataUrl = $(this).data('meetingurl');
        $('<form action="' + dataUrl+'" target="_blank" method="post"></form>').appendTo('body').submit().remove();
    });

});

$(window).on("load", function () {

    $("#weekly-timeline .content").mCustomScrollbar({
        setHeight: "220px",
        autoHideScrollbar: true,
        autoExpandScrollbar: true,
        scrollbarPosition: "outside",
    });
});


function GetWeeklyEvents() {
    var date = new Date();
    var paginationCouter = 0;
    var focusableCounter = 0;
    var isPageFocusCounterFind = false;
    var objSessionList = [];
    $.ajax(
        {
            url: '/Planner/MyPlanner/GetTodaysSchedule',
            dataType: 'json', // type of response data
            ///timeout: 3000,     // timeout milliseconds
            async: true,
            success: function (data, status, xhr) {   // success callback function     

                var _eventTempList = JSON.parse(data.events);
                var weeklyEventsList;
                var myPlannerEventsList;
                var myCommunityEventsList;


                if (_eventTempList[0] != null) {
                    myPlannerEventsList = _eventTempList[0].Result.PlannerEventData;
                    console.log('myPlannerEventsList: ' + myPlannerEventsList);
                    weeklyEventsList = _eventTempList[0].Result.WeeklyEventData;
                    console.log('weeklyEventsList: ' + weeklyEventsList);
                }
                if (_eventTempList[1] != null) {
                    myCommunityEventsList = _eventTempList[1].data;
                    console.log('myCommunityEventsList: ' + myCommunityEventsList);
                }
                var todayDate = new Date();
                var todayDay = todayDate.getDate();
                var _currentTime = formatAMPM(new Date);

                var todaysScheduleHtmlElements = "";
                var weekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

                var _currentWeekDay = weekday[todayDate.getDay()];

                $.each(weeklyEventsList, function (i, obj) {

                    var _eventDate = new Date(obj.EventDate);
                    var _startTime = obj.StartTime.substring(0, 5);
                    var _endTime = obj.EndTime.substring(0, 5);
                    var _tempStartDT = "2020/01/01 " + _startTime;
                    var _tempEndDT = "2020/01/01 " + _endTime;
                    _startTime = formatAMPM(new Date(_tempStartDT));
                    _endTime = formatAMPM(new Date(_tempEndDT));

                    // Daily Events Block
                    if (obj.WeekDay == 0) {
                        var _sWeekDay = weekday[_eventDate.getDay()];

                        if (_sWeekDay == _currentWeekDay) {
                            todaysScheduleHtmlElements += '<article class="m-0 p-3 rounded-2x d-flex align-items-center events-content" data-time="' + _startTime + '">'
                                + '   <div class="content">'
                                + '       <h6 class="m-0 time small text-semibold text-muted">' + _startTime + ' - ' + _endTime + '</h6>'
                                + '       <h3 class="m-0 font-small text-semibold mb-2 mt-1">'
                                + '           <a href="javascript:;" class="cursor-default">' + obj.CourseName + '</a>'
                                + '       </h3>'
                                + '       <p class="m-0 font-small text-semibold text-black-50">' + translatedResources.SessionWith.replace('{0}', obj.TeacherName) + '</p>'
                                + '   </div>'
                                + '  </article>'
                        }

                    }  //End of Daily Events block

                    // Weekly events block start
                    if (obj.WeekDay > 0) {
                        var _sWeekDay = weekday[obj.WeekDay];
                        if ((_sWeekDay == _currentWeekDay)) {
                            todaysScheduleHtmlElements += '<article class="m-0 p-3 rounded-2x d-flex align-items-center events-content" data-time="' + _startTime + '">'
                                + '   <div class="content">'
                                + '       <h6 class="m-0 time small text-semibold text-muted">' + _startTime + ' - ' + _endTime + '</h6>'
                                + '       <h3 class="m-0 font-small text-semibold mb-2 mt-1">'
                                + '           <a href="javascript:;" class="cursor-default">' + obj.CourseName + '</a>'
                                + '       </h3>'
                                + '       <p class="m-0 font-small text-semibold text-black-50">' + translatedResources.SessionWith.replace('{0}', obj.TeacherName) + '</p>'
                                + '   </div>'
                                + '  </article>'
                        }
                    }

                });

                $.each(myPlannerEventsList, function (i, obj) {

                    obj.StartDate = obj.StartDate + "Z";
                    obj.EndDate = obj.EndDate + "Z";
                    var _eventCategory = "";
                    var _liveSessionHtml = "";
                    var _eventStartDate = new Date(obj.StartDate);
                    var _sWeekDay = weekday[_eventStartDate.getDay()];

                    var _startTime = formatAMPM(_eventStartDate);
                    var _eventEndDate = new Date(obj.EndDate);
                    var _endTime = formatAMPM(_eventEndDate);

                    var todaydate = new Date();
                    if (todaydate.getDate() == _eventEndDate.getDate()) {
                        var objSession = { eventName: "", startDate: _eventStartDate, endDate: _eventEndDate, startTime: _startTime, endTime: _endTime };
                        objSessionList.push(objSession);
                    }
                    if (obj.OnlineMeetingType == 5) {
                        obj.MeetingUrl = `https://canvas.rainbow-classroom.com/lti/authenticate?email=${obj.EventUserEmail}&sectionid=${obj.EventId}&start_join=true`;
                    }

                    if (obj.OnlineMeetingType == 2 || obj.OnlineMeetingType == 1 || obj.OnlineMeetingType == 5) {
                        _eventCategory = `<span class="cursor-pointer" onclick="window.open(' ${obj.MeetingUrl}')"><i class="fas fa-video text-primary"></i>` + ' ' + translatedResources.LiveSession + '</span>';

                        if (_sWeekDay == _currentWeekDay) {
                            var isLiveSession = checkSessionTime(_currentTime, _startTime, _endTime);
                            if (isLiveSession && _userLognType != usr_Parent) {
                                var meetingAttr = obj.IsZakTokenRequired ? "" : (obj.OnlineMeetingType ==5 ? "" : `onclick="window.open('${obj.MeetingUrl}')"`);
                                var meetingClass = obj.IsZakTokenRequired ? "livesesion joinmeeting" : (obj.OnlineMeetingType == 5 ? "rainbow-classroom join-classroom" : "");
                                _liveSessionHtml += `<p data-iszaktoken='${obj.IsZakTokenRequired}' data-meetinglink='${obj.MeetingUrl}' data-starttime='${_startTime}' data-endtime='${_endTime}' data-meetingtype='${obj.OnlineMeetingType}' class="m-0 font-small text-semibold text-black-50 pl-4 position-relative dashboard-live-meeting">`
                                    + `     <span class="livesession-indicator">`
                                    + `         <span class="live-pulse-min"></span>`
                                    + `         <span class="live-pulse-max"></span>`
                                    + `         <span class="livesession-indicator__circle"></span>`
                                    + `     </span>`
                                    + translatedResources.Live
                                    + `      <button data-meetingurl='${obj.MeetingUrl}' type="button" ${meetingAttr} class="align-items-center btn btn-sm btn-success d-inline-flex m-0 ml-3 pl-2 pr-3 py-1 ${meetingClass}"><i class="fas fa-video text-primary mr-2 white-ic"></i> ` + translatedResources.JoinNow + `</button>`
                                    + ` </p>`
                            }
                            else {
                                _liveSessionHtml += `<span data-iszaktoken='${obj.IsZakTokenRequired}' data-meetingtype='${obj.OnlineMeetingType}' data-starttime='${_startTime}' data-meetinglink='${obj.MeetingUrl}' data-endtime='${_endTime}' class="dashboard-live-meeting"><p class="m-0 font-small text-semibold text-black-50 d-flex align-items-center">`
                                    + `     <i class="fas fa-video upcoming-session mr-2"></i>`
                                    + translatedResources.LiveSession
                                    + `    </p></span>`
                            }
                        }

                    }
                    else {
                        _eventCategory = (obj.EventCategory) == null ? "-" : obj.EventCategory;
                        _liveSessionHtml = '<p class="m-0 font-small text-semibold text-black-50">' + _eventCategory + '</p>'
                    }

                    if (_sWeekDay == _currentWeekDay) {
                        todaysScheduleHtmlElements += '<article class="m-0 p-3 rounded-2x d-flex align-items-center events-content" data-time="' + _startTime + '">'
                            + '   <div class="content">'
                            + '       <h6 class="m-0 time small text-semibold text-muted">' + _startTime + ' - ' + _endTime + '</h6>'
                            + '       <h3 class="m-0 font-small text-semibold mb-2 mt-1">'
                            + '           <a href="javascript:;" class="cursor-default">' + obj.Title + '</a>'
                            + '       </h3>'
                            + _liveSessionHtml
                            + '   </div>'
                            + '  </article>'

                    }
                });

                $.each(myCommunityEventsList, function (i, obj) {

                    var _eventStartDate = new Date(obj.eventStartDT);
                    var _sWeekDay = weekday[_eventStartDate.getDay()];

                    var _startTime = formatAMPM(_eventStartDate);
                    var _eventEndDate = new Date(obj.eventEndDT);
                    var _endTime = formatAMPM(_eventEndDate);

                    if (_sWeekDay == _currentWeekDay) {
                        todaysScheduleHtmlElements += '<article class="m-0 p-3 rounded-2x d-flex align-items-center events-content" data-time="' + _startTime + '">'
                            + '   <div class="content">'
                            + '       <h6 class="m-0 time small text-semibold text-muted">' + _startTime + ' - ' + _endTime + '</h6>'
                            + '       <h3 class="m-0 font-small text-semibold mb-2 mt-1">'
                            + '           <a href="javascript:;" class="cursor-default">' + obj.eventDescription + '</a>'
                            + '       </h3>'
                            + '       <p class="m-0 font-small text-semibold text-black-50">' + obj.categoryDescription + '</p>'
                            + '   </div>'
                            + '  </article>'
                    }
                });

                var todaysSchedule = document.getElementById('todays-schedule-carousel');
                if (todaysScheduleHtmlElements == "") {
                    todaysSchedule.innerHTML = "";
                    $("#noTodaysEventData").removeClass('d-none');
                    $("#noTodaysEventData").addClass('d-block');
                    $("#noTodaysNote").removeClass('d-block');
                    $("#noTodaysNote").addClass('d-none');
                }
                else {
                    todaysSchedule.innerHTML = todaysScheduleHtmlElements;

                    $("#noTodaysEventData").removeClass('d-block');
                    $("#noTodaysEventData").addClass('d-none');
                    $("#noTodaysNote").removeClass('d-none');
                    $("#noTodaysNote").addClass('d-block');
                }

                sortTodaysSchedule();

                var sortedSessioList = [];
                sortedSessioList = objSessionList.sort((a, b) => new Date(a.startDate) - new Date(b.startDate));
                $.each(sortedSessioList, function (i, obj) {

                    var screenWidth = screen.width;
                    var _eventStartDate = new Date(obj.startDate);
                    var _startTime = obj.startTime;
                    var _eventEndDate = new Date(obj.endDate);
                    var _endTime = obj.endTime;

                    var todaydate = new Date();
                    if (todaydate.getDate() == _eventEndDate.getDate() && isPageFocusCounterFind == false) {
                        if (checkSessionTimeToSetFocusOnScheduleBlock(_currentTime, _startTime, _endTime)) {
                            isPageFocusCounterFind = true;
                        }
                    }

                    if (todaydate.getDate() == _eventEndDate.getDate() && isPageFocusCounterFind == false) { paginationCouter = paginationCouter + 1; }

                    if (todaydate.getDate() == _eventEndDate.getDate()) {
                        if ((screenWidth >= 0 && screenWidth < 768)) {
                            focusableCounter = paginationCouter;
                        } else if ((screenWidth >= 768 && screenWidth < 1366)) {
                            focusableCounter = Math.floor(paginationCouter / 3);
                            focusableCounter = focusableCounter * 3;
                        } else if ((screenWidth >= 1366 && screenWidth < 1920)) {
                            focusableCounter = Math.floor(paginationCouter / 4);
                            focusableCounter = focusableCounter * 4;
                        } else if ((screenWidth >= 1920)) {
                            focusableCounter = Math.floor(paginationCouter / 5);
                            focusableCounter = focusableCounter * 5;
                        }
                    }
                })

                $("#todays-schedule-carousel").trigger('destroy.owl.carousel');
                applyCarousel();

                console.log('final paginationCouter: ' + paginationCouter);

                $('#todays-schedule-carousel').trigger('to.owl.carousel', focusableCounter);


            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback
            }
        });

}

function getMonthNm(d) {
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var n = month[d.getMonth()];
    return n;
};

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}


function applyCarousel() {
    $(".todaysScheduleCarousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        responsive: {
            0: {
                items: 1,
                slideBy: 1
            },
            768: {
                items: 3,
                slideBy: 3
            },
            1366: {
                items: 4,
                slideBy: 4

            },
            1920: {
                items: 5,
                slideBy: 5
            }
        }
    });
}

function applyCarouselToWeeklyTimeline() {
    $(".timeline").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        responsive: {
            0: {
                items: 2,
                slideBy: 2
            },
            768: {
                items: 3,
                slideBy: 3
            },
            1366: {
                items: 5,
                slideBy: 5
            },
            1920: {
                items: 5,
                slideBy: 5
            }
        }
    });
}

function convertImgToSvg() {
    // apply color to svg files
    $('img.svg').each(function () {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
}

function checkSessionTime(currentTime, fromTime, toTime) {
    var _currentTime = new Date("January 01, 2020 " + currentTime);
    _currentTime = _currentTime.getTime();
    var _startTime = new Date("January 01, 2020 " + fromTime);
    _startTime = _startTime.getTime();
    var _endTime = new Date("January 01, 2020 " + toTime);
    _endTime = _endTime.getTime();
    if (_endTime < _startTime) {
        _endTime = new Date("January 02, 2020 " + toTime);
        _endTime = _endTime.getTime();
    }
    if ((_currentTime >= _startTime) && (_currentTime <= _endTime)) {

        return true;
    }
    else {
        return false;
    }
}

function checkSessionTimeToSetFocusOnScheduleBlock(currentTime, fromTime, toTime) {
    var _currentTime = new Date("January 01, 2020 " + currentTime);
    _currentTime = _currentTime.getTime();
    var _startTime = new Date("January 01, 2020 " + fromTime);
    _startTime = _startTime.getTime();
    var _endTime = new Date("January 01, 2020 " + toTime);
    _endTime = _endTime.getTime();

    if (_endTime < _startTime) {
        _endTime = new Date("January 02, 2020 " + toTime);
        _endTime = _endTime.getTime();
    }

    if (_currentTime <= _startTime) {
        return true;
    }
    else if ((_currentTime >= _startTime) && (_currentTime <= _endTime)) {
        return true;
    }
    //else if (_currentTime >= _endTime) {
    //    return true;
    //}
    else {
        return false;
    }
}


function sortSchoolWeekDays() {
    var $weekdayWrapper = $('#weekly-timeline');

    $weekdayWrapper.find('.item').sort(function (a, b) {
        return +a.dataset.sortvalue - +b.dataset.sortvalue;
    }).appendTo($weekdayWrapper);
}

function sortTodaysSchedule() {
    var $scheduleWrapper = $('#todays-schedule-carousel');

    $scheduleWrapper.find('.events-content').sort(function (a, b) {
        return new Date('1970/01/01 ' + a.dataset.time) - new Date('1970/01/01 ' + b.dataset.time);
    }).appendTo($scheduleWrapper);
}

function sortEventsByTime() {

    var $sundayWrapper = $('#sun-events-holder');

    $sundayWrapper.find('.events-content').sort(function (a, b) {
        return new Date('1970/01/01 ' + a.dataset.time) - new Date('1970/01/01 ' + b.dataset.time);
    }).appendTo($sundayWrapper);

    var $mondayWrapper = $('#mon-events-holder');

    $mondayWrapper.find('.events-content').sort(function (a, b) {
        return new Date('1970/01/01 ' + a.dataset.time) - new Date('1970/01/01 ' + b.dataset.time);
    }).appendTo($mondayWrapper);

    var $tuesdayWrapper = $('#tue-events-holder');

    $tuesdayWrapper.find('.events-content').sort(function (a, b) {
        return new Date('1970/01/01 ' + a.dataset.time) - new Date('1970/01/01 ' + b.dataset.time);
    }).appendTo($tuesdayWrapper);

    var $wedWrapper = $('#wed-events-holder');

    $wedWrapper.find('.events-content').sort(function (a, b) {
        return new Date('1970/01/01 ' + a.dataset.time) - new Date('1970/01/01 ' + b.dataset.time);
    }).appendTo($wedWrapper);

    var $thuWrapper = $('#thu-events-holder');

    $thuWrapper.find('.events-content').sort(function (a, b) {
        return new Date('1970/01/01 ' + a.dataset.time) - new Date('1970/01/01 ' + b.dataset.time);
    }).appendTo($thuWrapper);

    var $friWrapper = $('#fri-events-holder');

    $friWrapper.find('.events-content').sort(function (a, b) {
        return new Date('1970/01/01 ' + a.dataset.time) - new Date('1970/01/01 ' + b.dataset.time);
    }).appendTo($friWrapper);

    var $satWrapper = $('#sat-events-holder');

    $satWrapper.find('.events-content').sort(function (a, b) {
        return new Date('1970/01/01 ' + a.dataset.time) - new Date('1970/01/01 ' + b.dataset.time);
    }).appendTo($satWrapper);

}

function refreshLiveSessionStatus() {
    var _currentTime = formatAMPM(new Date);
    var source = $(".dashboard-live-meeting");
    $(source).each(function (idx, item) {
        var sessionData = $(item).data();
        var _liveSessionHtml = "";
        var isLiveSession = checkSessionTime(_currentTime, sessionData.starttime, sessionData.endtime);
        if (isLiveSession) {
            var meetingAttr = sessionData.iszaktoken ? "" : (sessionData.meetingtype == 5 ?  '' : `onclick="window.open('${sessionData.meetinglink}')"`);
            var meetingClass = sessionData.iszaktoken ? "livesesion joinmeeting" : (sessionData.meetingtype == 5  ? "rainbow-classroom join-classroom" :  "");
            _liveSessionHtml += `<p data-iszaktoken='${sessionData.iszaktoken}' data-starttime='${sessionData.starttime}' data-meetinglink='${sessionData.meetinglink}' data-endtime='${sessionData.endtime}' data-meetingtype='${sessionData.meetingtype}' class="m-0 font-small text-semibold text-black-50 pl-4 position-relative dashboard-live-meeting">`
                + `     <span class="livesession-indicator">`
                + `         <span class="live-pulse-min"></span>`
                + `         <span class="live-pulse-max"></span>`
                + `         <span class="livesession-indicator__circle"></span>`
                + `     </span>`
                + translatedResources.Live
                + `     <button data-meetingurl='${sessionData.meetinglink}' type="button" ${meetingAttr} class="align-items-center btn btn-sm btn-success d-inline-flex m-0 ml-3 pl-2 pr-3 py-1 ${meetingClass}"><i class="fas fa-video text-primary mr-2 white-ic"></i> ` + translatedResources.JoinNow + `</button>`
                + ` </p>`
        }
        else {
            _liveSessionHtml += `<span data-iszaktoken='${sessionData.iszaktoken}' data-meetingtype='${sessionData.meetingtype}' data-starttime='${sessionData.starttime}' data-meetinglink='${sessionData.meetinglink}' data-endtime='${sessionData.endtime}' class="cursor-pointer dashboard-live-meeting"><p class="m-0 font-small text-semibold text-black-50 d-flex align-items-center">`
                + `     <i class="fas fa-video upcoming-session mr-2"></i>`
                + translatedResources.LiveSession
                + `    </p></span>`
        }
        $(this)[0].outerHTML = _liveSessionHtml;
    })

};