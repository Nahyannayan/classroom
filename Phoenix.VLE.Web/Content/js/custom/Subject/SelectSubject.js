﻿function GetStudentinfo(Studentnumber, isAfterconfirm) {
    $('#GetStu_No').val(Studentnumber);
    $.ajax({
        url: '/ParentCorner/Subject/GetCourseList',
        type: "GET",
        data: { StudentNumebr: Studentnumber },
        contentType: "html",
        success: function (response) {
            if (response != null) {
                $('#divGetOptionView').html('');
                $('#divGetOptionView').html(response);
                if (isAfterconfirm) {
                    $(".alert-success").removeClass('hide-item');
                    hideAllInputRadio();
                }
                
                $('#ResetRadio').click(function () {
                    //debugger
                    $('input[type=radio]').prop('checked', false);
                });
            }
        }
    });
}



function ConfirmSelection() {    
    var studentId = $('#GetStu_No').val();
    var temp = $('input[type="radio"]:checked')
    var list = [];

    temp.each(function () {
        var SelectCoursesList = new Object();
        var OptionID = $(this).attr("data-name");
        var SubjectGradeID = $(this).attr("data-id");

        SelectCoursesList.OptionID = OptionID;
        SelectCoursesList.SubjectGradeID = SubjectGradeID;
        SelectCoursesList.StudentId = studentId
        list.push(SelectCoursesList);
    });
    var values = JSON.stringify({ "values": list });
    if (list.length > 0) {
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: "POST",
            url: "/ParentCorner/Subject/ConfirmSelection",
            data: values,
            async: false,
            success: function (result) {
                debugger
                var data = JSON.parse(result.events), msg = "", succ = "";
                succ = data.success.toLowerCase(); msg = data.message;
                if (succ == "true") {
                    GetStudentinfo(studentId, true);

                } else {
                    globalFunctions.showErrorMessage(msg);
                }

            },
        });
    } else {
        globalFunctions.showErrorMessage("Please select any subject.");
    }
  
}
$('#ResetRadio').click(function () {
    debugger
    $('input[type=radio]').prop('checked', false);
});
function SubjectGuidelines(APIUrl) {
    window.open(APIUrl, '_blank');

}

function hideAllInputRadio() {
    var temp = $('input[type="radio"]');
    temp.each(function () {
        $(this).attr('disabled', true);
    });
}