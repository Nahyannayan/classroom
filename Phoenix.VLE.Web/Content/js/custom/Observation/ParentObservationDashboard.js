﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();


$(document).ready(function () {
    var grid = new DynamicPagination("ParentObservation");
    var settings = {
        url: '/Observations/Observation/LoadObservation'
    };
    grid.init(settings);
    //assignment details page
    $('[data-toggle="tooltip"]').tooltip();


    //---- grid and list view toggle
    $("#grid").addClass('active');
    $('#list').click(function (event) {
        event.preventDefault();
        $('#library .item').addClass('list-group-item');
        $(this).toggleClass('active');
        $("#grid").removeClass('active');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#library .item').removeClass('list-group-item');
        $('#library .item').addClass('grid-group-item');
        $(this).toggleClass('active');
        $("#list").removeClass('active');
    });

    $("#addAssignment").click(function () {
        window.location.href = "/Assignments/Assignment/InitAddEditAassignmentForm";
    });
    $('[data-toggle="tooltip"]').tooltip();
});
