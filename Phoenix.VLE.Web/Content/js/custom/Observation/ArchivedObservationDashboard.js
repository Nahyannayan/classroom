﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var groups = function () {
    loadObservation = function () {
        var grid = new DynamicPagination("ObservationTeacher");
        var settings = {
            url: '/Observations/Observation/LoadArchivedObservation'
        };
        grid.init(settings);;
    };
    return {
        loadObservation: loadObservation
    };
}();


function archiveActiveObservation() {
    jQuery.ajaxSettings.traditional = true;
    //Only to assign source, please follow standard way as in other js files for confirmation popup
    var source = $("#activeObservationList");
    globalFunctions.notyDeleteConfirm(source, translatedResources.archiveConfirm);
    $(document).bind('okToDelete', source, function (e) {
        if ($('#selectAllUnassigned').is(':checked')) {
            $('#selectAllUnassigned').prop('checked', false);
        }
        var selectedMembers = $("#activeObservationList input[type='checkbox']:checked");
        var observationToArchive = [];
        if (selectedMembers != null && selectedMembers.length > 0) {
            selectedMembers.each(function (i, elem) {
                observationToArchive = observationToArchive + elem.value + ',';
            });
            $.ajax({
                type: 'POST',
                data: { observationToArchive: observationToArchive },
                url: '/Observations/Observation/UpdateActiveObservation',
                success: function (result) {
                    loadObservationList();
                    // location.reload();
                    formElements.feSelect();
                },
                error: function (data) { }
            });
        }
        else {
            globalFunctions.showWarningMessage("Please select observation to be archive");
        }
    });
}
$('#pills-studentList-tab').on('click', function (event) {
    location.reload();
});

function loadObservationList() {
    loadActiveObservation();
    loadArchiveObservation();
}

function loadActiveObservation() {
    $.ajax({
        url: '/Observations/Observation/GetActiveObservations',
        success: function (result) {
            $('#activeObservationList').html(result);
        },
        error: function (data) { }
    });
}
function loadArchiveObservation() {
    $.ajax({
        url: '/Observations/Observation/GetArchivedObservations',
        success: function (result) {
            $('#archiveObservationList').html(result);
        },
        error: function (data) { }
    });
}
$(document).on('keyup', '#availableMemberSearch', function (e) {
    var searchText = $(this).val().toLowerCase();
    $('#activeObservationList ul li').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
});
$(document).on('keyup', '#groupMemberSearch', function (e) {
    //if (e.which === 13) {
    var searchText = $(this).val().toLowerCase();
    $('#archiveObservationList ul li').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
    //}
});

$('#selectAllUnassigned').change(function () {
    var chk = $(this).is(':checked');
    $('input[type=checkbox]', "#activeAssignment").each(function () {
        $(this).prop('checked', chk);
    });
});

$(document).ready(function () {
    groups.loadObservation();
    $('#pills-SelectedStudent-tab').on('click', function (event) {
        //var groupId = $('#SchoolGroupId').val();
        $.get("/Observations/Observation/ArchiveObservation", function (response) {
            $('#selectedStudent').html(response);
            $('.selectpicker').selectpicker('refresh');
            $(".delegates-wrapper").mCustomScrollbar({
                setHeight: "250",
                autoExpandScrollbar: true,
            });
        });
    });



    $('#lightgalleryTeacher').lightGallery({
        selector: '.galleryImg',
        share: false
    });
    //var grid = new DynamicPagination("ObservationTeacher");
    //var settings = {
    //    url: '/Observations/Observation/LoadArchivedObservation'
    //};
    //grid.init(settings);
    //assignment details page
    $('[data-toggle="tooltip"]').tooltip();


    //---- grid and list view toggle
    $("#grid").addClass('active');
    $('#list').click(function (event) {
        event.preventDefault();
        $('#library .item').addClass('list-group-item');
        $(this).toggleClass('active');
        $("#grid").removeClass('active');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#library .item').removeClass('list-group-item');
        $('#library .item').addClass('grid-group-item');
        $(this).toggleClass('active');
        $("#list").removeClass('active');
    });

  
    $('[data-toggle="tooltip"]').tooltip();

});
