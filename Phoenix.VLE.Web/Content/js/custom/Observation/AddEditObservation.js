﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var addEditObservation = function () {
    var init = function () {
        $('#lightgalleryTeacher').lightGallery({
            selector: '.galleryImg',
            share: false
        });
       
    },
        saveObservationData = function (IsPublished) {
            let Isvalid = true;
            CKEDITOR.instances.ObservationDesc.getData()
            if (!CKEDITOR.instances.ObservationDesc.getData().replace(/<[^>]*>/gi, '').length || CKEDITOR.instances.ObservationDesc.getData().replace(/(&nbsp;)+/gm, '').trim() == "") {
                $('[data-valmsg-for="ObservationDesc"]').html(translatedResources.Mandatory);
                Isvalid = false;
                $('span[data-valmsg-for="ObservationDesc"]').show();
            }
            $("#ObservationDesc").val(CKEDITOR.instances.ObservationDesc.getData());
            $("#IsPublished").attr("checked", IsPublished);
            if ($('form#frmAddEditObservation').valid() && Isvalid) {
                globalFunctions.validateBannedWordsAndSubmitForm('frmAddEditObservation');
            }
        },
        onSaveSuccess = function (data) {
            if (data.Success) {
                globalFunctions.showMessage("success", translatedResources.addSuccessMsg);
                window.location.href = "/observations/observation/index";
            }
            else {
                globalFunctions.showErrorMessage(translatedResources.technicalError);
            }

        },
        onCancelObservation = function () {
            window.location.href = "/observations/observation/index";
        },
        loadTopicSubtopic = function (source, subjectId) {
            if ($("#CourseIds").val() != "") {
                var title = translatedResources.SelectCurriculum;
                globalFunctions.loadPopup(source, '/Observations/Observation/GetUnitStructureView?courseIds=' + $("#CourseIds").val(), title, 'addtask-dialog modal-md');
                $(source).off("modalLoaded");
                //$(".modal-dialog").addClass('modal-md modal-dialog-scrollable');
                $('#formModified').val(true);

                $(source).on("modalLoaded", function () {
                    formElements.feSelect();
                    $('select').selectpicker();
                    popoverEnabler.attachPopover();
                });
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.SelectSubjectError);
            }

        },
        loadMyFiles = function (source) {
            var title = translatedResources.FileSelect;
            globalFunctions.loadPopup(source, '/Observations/Observation/LoadSelectedMyFiles', title, 'addtask-dialog modal-md');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
            });
        },
        postMyFilesToObservation = function () {
            var mappingDetails = [];
            $("#tbl_filelist >.permission-checkbox input:checkbox:visible").each(function () {
                mappingDetails.push({
                    ObservationFileId: $(this).attr('data-fileid'),
                    UploadedFileName: $(this).attr('data-filepath'),
                    FileName: $(this).attr('data-filename'),
                    FileTypeId: $(this).attr('data-filetypeid'),
                    ResourceFileTypeId: $(this).attr('data-resourcefiletypeId'),
                    GoogleDriveFileId: $(this).attr('data-googledrivefileid'),
                    FileTypeName: $(this).attr('data-filetypename'),
                    FileIcon: $(this).attr('data-fileicon')
                });
            });
            var saveparams = {
                mappingDetails: mappingDetails
            };
            $.ajax({
                type: 'POST',
                url: '/Observations/Observation/SaveSelectedFiles',
                async: false,
                contentType: 'application/json',
                data: JSON.stringify(saveparams),
                success: function (data) {
                    $("#fileList").html('');
                    $("#fileList").append(data);
                    $('#attachmentCount').html(translatedResources.AttachmentsCount.replace('{0}', $("#fileList .attachedFile").length));
                    $("#myModal").modal('hide');
                    $("#scrollableTopic").mCustomScrollbar({
                        setHeight: "100",
                    });
                    $('#lightgallery').lightGallery({
                        selector: '.galleryImg',
                        share: false
                    });
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
        },
        //loadTopicSubtopic = function (source, subjectId) {

        //    if ($("#SubjectId").val() != "") {
        //        var title = translatedResources.SelectCurriculum;
        //        globalFunctions.loadPopup(source, '/Observations/Observation/loadTopicSubtopic?subjectId=' + $(subjectId).val(), title, 'addtask-dialog modal-md');
        //        $(source).off("modalLoaded");
        //        $('#formModified').val(true);
        //        $(source).on("modalLoaded", function () {
        //            formElements.feSelect();
        //            $('select').selectpicker();
        //            popoverEnabler.attachPopover();
        //        });
        //    }
        //    else {
        //        globalFunctions.showErrorMessage(translatedResources.SelectSubjectError);
        //    }
        //},
        postObjective = function () {
            var mappingDetails = [];
            $("#topicAccordion input:checkbox.level-3").each(function () {

                var lessonId = $(this).data('lessonid');
                var isLesson = $(this).data('islesson');
                var lessonname = $(this).data('lessonname');

                if ($(this).is(":checked")) {
                    if (isLesson === "True") {
                        mappingDetails.push({
                            ObjectiveId: lessonId,
                            ObjectiveName: lessonname,
                            isSelected: true
                        });
                    }
                }
                else {
                    mappingDetails = mappingDetails.filter(function (item) {
                        return item.ObjectiveId !== lessonId;
                    });
                }

            });

            $("#topicAccordion input:checkbox.level-2").each(function () {

                var lessonId = $(this).data('lessonid');
                var isLesson = $(this).data('islesson');
                var lessonname = $(this).data('lessonname');

                if ($(this).is(":checked")) {
                    if (isLesson === "True") {
                        mappingDetails.push({
                            ObjectiveId: lessonId,
                            ObjectiveName: lessonname,
                            isSelected: true
                        });
                    }
                }
                else {
                    mappingDetails = mappingDetails.filter(function (item) {
                        return item.LessonId !== lessonId;
                    });
                }

            });
            var saveparams = {
                mappingDetails: mappingDetails
            };

            if (mappingDetails.length > 0) {
                $.ajax({
                    type: 'POST',
                    url: '/Observations/Observation/SaveObjectives',
                    async: false,
                    contentType: 'application/json',
                    data: JSON.stringify(saveparams),
                    success: function (data) {
                        $("#selectedObjectiveDetails").html(data);
                        $("#myModal").modal('hide');

                        $(".curriculum-list ul").mCustomScrollbar({
                            autoExpandScrollbar: true,
                            autoHideScrollbar: true,
                            scrollbarPosition: "outside"
                        });
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure(data, xhr, status);
                    }
                });
            }
        },
        deleteObjective = function (objectiveId) {
            $.ajax({
                type: 'GET',
                url: '/Observations/Observation/DeleteObjective?objectiveId=' + objectiveId,
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    $("#selectedObjectiveDetails").html(data);
                    globalFunctions.showSuccessMessage(translatedResources.DeleteObjectiveSuccess);
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
        },
        deleteObservation = function (source, observationId) {
            globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
            $(document).bind('okToDelete', source, function (e) {
                $.ajax({
                    type: 'GET',
                    url: '/Observations/Observation/DeleteObservation?observationId=' + observationId,
                    async: false,
                    contentType: 'application/json',
                    success: function (data) {
                        if (data.Success) {
                            globalFunctions.showSuccessMessage(translatedResources.SuccessDelete);
                            window.location.href = "/observations/observation/index";
                        }
                        else {
                            globalFunctions.showErrorMessage(translatedResources.ErrorDelete);
                        }
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure(data, xhr, status);
                    }
                });
            });

        };

    checkAndResetFormData = function () {
        $.ajax({
            url: "/Observations/Observation/RemoveUnsavedFiles",
            success: function (data) {
                // ununsed observation files removed
            },
            error: function () {
                globalFunctions.onFailure();
            }
        });
    }
    loadStudentDetails = function () {
        let observationId = $("#ObservationId").val();
        var templateHtml =`<li>
                <div class="selected-student d-flex align-items-center">
                    <div class="defaultAvatar rounded-circle overflow-hidden">
                        <img src="{0}" />
                    </div>
                    <div class="student-info ml-3">
                        <h3 class="name m-0"><span class="font-small text-semibold">{1}</span></h3>
                    </div>
                </div>
                <div class="faded-text">{2}</div>
            </li>`
        $.ajax({
            type: 'GET',
            url: '/Observations/Observation/loadStudentDetailsByObservation?observationId=' + observationId,
            contentType: 'application/json',
            success: function (data) {
                let count = 0;
                //$('#selectedStudentDetails').html(data);
                var studList = '';
                $.each(data, function (index, stud) {
                    count++;
                    studList += templateHtml.replace('{0}', stud.UserImageFilePath)
                        .replace('{1}', stud.StudentName).replace('{2}', stud.StudentNumber);
                });
                $(".members-list ul").append(studList);
                $("#selectedStudentDetails .list-info").html(translatedResources.StudentsSelected.replace('{0}', count));
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure(data, xhr, status);
            }
        });
    };

    return {
        init: init,
        saveObservationData: saveObservationData,
        onSaveSuccess: onSaveSuccess,
        onCancelObservation: onCancelObservation,
        loadTopicSubtopic: loadTopicSubtopic,
        loadMyFiles: loadMyFiles,
        postMyFilesToObservation: postMyFilesToObservation,
        postObjective: postObjective,
        deleteObjective: deleteObjective,
        deleteObservation: deleteObservation,
        checkAndResetFormData: checkAndResetFormData,
        loadStudentDetails: loadStudentDetails
    };
}();


$(document).ready(function () {

    $(window).on("beforeunload", function (e) {
        var fileSource = $("#lightgalleryTeacher");
        if (fileSource.length && $(fileSource).find("div.item").length) {
            addEditObservation.checkAndResetFormData();
        }
    });

    globalFunctions.enableCascadedDropdownList("SchoolGroupIds", "StudentIds", '/Assignments/Assignment/GetStudentsinGroup');
    //datepicker
    var date = new Date();
    date.setDate(date.getDate() - 2);
    console.log(Date.parse($("#hdnStartDate").val()));
    $('.date-picker1').datetimepicker({
        format: 'DD-MMM-YYYY',
        locale: translatedResources.locale
        // minDate: $("#hdnStartDate").val(),
        //defaultDate: Date.parse($("#hdnStartDate").val()),
    });
    $('.date-picker1').datetimepicker('show');
    $('.date-picker1').datetimepicker('hide');
    $(document).on('click', '#ahrefDeleteFiles', function () {
        var fileName = $(this).attr('data-fileName');
        var uploadedfilepath = $(this).attr('data-uploadedPath');
        var fileId = $(this).attr('data-FileId');
        var observationId = $(this).attr('data-observationId');
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        $(document).bind('okClicked', $(this), function (e) {
            $.post('/Observations/Observation/DeleteFilesFromSession', { fileId: fileId, ObservationId: observationId, fileName: fileName, UploadedFilePath: uploadedfilepath }, function (response) {
                $('#fileList').html(response);
                $('#attachmentCount').html(translatedResources.AttachmentsCount.replace('{0}', $("#fileList .attachedFile").length));
            });
        });

    });

    $(document).on('click', '#ViewFile', function (e) {
        e.preventDefault();
        var id = $(this).data("filename");
        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=editObservation", "_blank");
        }
    });

    $("#StudentIds").on('change', function (event) {
        let StudentIds = $("#StudentIds").val();
        $.ajax({
            type: 'POST',
            url: '/Observations/Observation/loadStudentDetails',
            data: { StudentIds: StudentIds },
            success: function (data) {
                $('#selectedStudentDetails').html(data);
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure(data, xhr, status);
            }
        });
    });

    $(".curriculum-list ul,#fileList").mCustomScrollbar({
        autoExpandScrollbar: true,
        autoHideScrollbar: true,
        scrollbarPosition: "outside"
    });
    //select all as per accordion
    $(document).on('change', "#topicAccordion input:checkbox.level-1", function (event) {
        var id = $(this).data("id");
        $("#collapse_" + id +" input:checkbox").prop("checked", $(this).is(":checked"));
    });
    $(document).on('change', "#topicAccordion input:checkbox.level-2", function (event) {
        var parentid = $(this).data("parentid");
        if ($("#topicAccordion input:checkbox.level-2:checked").length == 0)
            $("#heading_" + parentid+" input:checkbox").prop("checked", false);
        $("#heading_" + parentid + " input:checkbox").prop("checked", $("#topicAccordion input:checkbox.level-2:checked").length == $("#topicAccordion input:checkbox.level-2").length);
    });
    //select all
    $(document).on("change", ".selectAll", function () {
        $("#topicAccordion .level-1,.level-2,.level-3").prop("checked", $(this).is(":checked"));
    });
    $(document).on("change", "#topicAccordion .level-1,.level-2,.level-3",function () {
        if ($("#topicAccordion .level-1:checked,.level-2:checked,.level-3:checked").length == 0)
            $(".selectAll").prop("checked", false);
        $(".selectAll").prop("checked", $("#topicAccordion .level-1:checked,.level-2:checked,.level-3:checked").length == $("#topicAccordion .level-1,.level-2,.level-3").length);
    });
    //selected lesson count
    $(document).on('change', "#topicAccordion input:checkbox,#topicAccordion input:checkbox, .selectAll", function (event) {
        //update selected count
        var count = 0;
        $("#topicAccordion input:checkbox.level-3").each(function () {
            var isLesson = $(this).data('islesson');
            if ($(this).is(":checked")) {
                if (isLesson === "True") {
                    count++;
                }
            }
        });

        $("#topicAccordion input:checkbox.level-2").each(function () {
            var isLesson = $(this).data('islesson');
            if ($(this).is(":checked")) {
                if (isLesson === "True") {
                    count++;
                }
            }
        });
        $("#item-count").html(translatedResources.TopicsSelected.replace('{0}', count));
    });
    //lession search
    $(document).on("keyup", ".searchfield", function () {
        var source = $(".maincard[data-searchtext]");
        var searchString = $(this).val();
        $(source).filter(function () {
            $(this).toggle($(this).data("searchtext").toLowerCase().indexOf(searchString.trim().toLowerCase()) > -1)
        });
        if ($(".maincard:visible").length === 0) {
            $("#nodataDiv").show();
        }
        else {
            $("#nodataDiv").hide();
        }
    });
    CKEDITOR.editorConfig = function (config) {
        config.removePlugins = 'cloudservices';
      
    };
    CKEDITOR.plugins.addExternal('ckeditor_wiris', '', 'plugin.js');
    CKEDITOR.replace('ObservationDesc', {
        htmlencodeoutput: false,
        height: '180px',
        extraPlugins: 'ckeditor_wiris',
        allowedContent: true,
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P,
        contentsLangDirection: $("html").attr("dir")
    });

    CKEDITOR.instances.ObservationDesc.on('change', function () {
        if (CKEDITOR.instances.ObservationDesc.getData().length > 0) {
            console.log("ObservationDesc-ckeditor");
            $('span[data-valmsg-for="ObservationDesc"]').hide();
        }
        else {
            $('span[data-valmsg-for="ObservationDesc"]').hide();
        }
    });

    CKEDITOR.instances.ObservationDesc.on('change', function () {
        if (CKEDITOR.instances.ObservationDesc.getData().length > 0) {
            console.log("ObservationDesc-ckeditor");
            $('span[data-valmsg-for="ObservationDesc"]').hide();
        }
        else {
            $('span[data-valmsg-for="ObservationDesc"]').hide();
        }
    });
    //load student
    loadStudentDetails();
});