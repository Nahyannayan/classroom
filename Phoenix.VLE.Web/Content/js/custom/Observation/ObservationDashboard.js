﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();

$(document).ready(function () {
    $('#lightgalleryTeacher').lightGallery({
        selector: '.galleryImg',
        share: false
    });
    var grid = new DynamicPagination("ObservationTeacher");
    var settings = {
        url: '/Observations/Observation/LoadObservation'
    };
    grid.init(settings);
    //assignment details page
    $('[data-toggle="tooltip"]').tooltip();
    

    //---- grid and list view toggle
    $("#grid").addClass('active');
    $('#list').click(function (event) {
        event.preventDefault();
        $('#library .item').addClass('list-group-item');
        $(this).toggleClass('active');
        $("#grid").removeClass('active');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#library .item').removeClass('list-group-item');
        $('#library .item').addClass('grid-group-item');
        $(this).toggleClass('active');
        $("#list").removeClass('active');
    });

    $("#addAssignment").click(function () {
        window.location.href = "/Assignments/Assignment/InitAddEditAassignmentForm";
    });
    $('[data-toggle="tooltip"]').tooltip();

    $(document).on('change', "input[name='groupFilter']", function (event) {
        searchStudByGroup();
    });
    //remove selected filtered
    $(document).on("click", "#selectedFilterList .btn-close", function () {
        var chkbxId = $(this).attr('data-id');
        $("#" + chkbxId).prop("checked", false);
        searchStudByGroup();
    });
    //search student by groups

    function searchStudByGroup() {
        var url = '/Observations/Observation/LoadObservation/?pageIndex=1';
        let isGroupSelected = false;
        let selectedGroupList = [];
        let searchString = $("#searchContent").val();
        $.each($("input[name='groupFilter']:checked"), function () {
            isGroupSelected = true;
            let groupId = $(this).val();
            let groupName = $(this).attr('data-groupname');
            selectedGroupList.push({
                SchoolGroupId: groupId,
                SchoolGroupName: groupName
            });
        });
        $.post(url, {searchString: searchString, selectedGroupList: selectedGroupList, isGroupSelected: isGroupSelected }, function (response) {
            $('#ObservationTeacher').html(response);
        });
    }
    //group search
    $(".search-text-box").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        var index = $(this).attr("data-index");
        $(".grpSearchList" + index).filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
        if ($(".grpSearchList" + index + ":visible").length === 0) {
            $("#grpSearchNoRecord" + index).show();
        }
        else {
            $("#grpSearchNoRecord" + index).hide();
        }
    });
});
