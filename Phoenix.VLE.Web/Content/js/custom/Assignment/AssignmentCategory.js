﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();
var AssignmentCategoryConfiguration = function () {
    var Init = function () {
        loadAssignmentCategoryGrid();
        $("#tblAssignmentCategory_filter").hide();
    },

        assignmentCategoriesArrayList = [],
        loadAssignmentCategoryGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "CategoryTitle", "sTitle": translatedResources.AssignmentCategoryName, "sWidth": "20%", "sClass": "wordbreak" },
                { "mData": "CategoryDescription", "sTitle": translatedResources.Description, "sWidth": "20%", "sClass": "wordbreak" },
                { "mData": "IsActive", "sTitle": translatedResources.IsActive, "sWidth": "20%", "sClass": "open-details-control wordbreak" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initAssignmentCategoryGrid('tblAssignmentCategory', _columnData, '/Assignments/Assignment/LoadAssignmentCategoryGrid');

        },

        PushAssignmentCategory = function (assignmentCategoryId) {
            if ($("#chk_" + assignmentCategoryId + "").is(":checked")) {
                $.ajax({
                    type: "POST",
                    url: "/Assignments/Assignment/AddEditAssignmentCategory",
                    data: { AssignmentCategoryId: assignmentCategoryId, IsActive: true },
                    success: function (response) {
                        if (response.Success) {
                            globalFunctions.showMessage(true, translatedResources.MarkAssignmentCategory);
                        }
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: "/Assignments/Assignment/AddEditAssignmentCategory",
                    data: { AssignmentCategoryId: assignmentCategoryId, IsActive: false },
                    success: function (response) {
                        if (response.Success) {
                            globalFunctions.showMessage(true, translatedResources.UnMarkAssignmentCategory);
                        }
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            }
        },
        addAssignmentCategoryPopup = function (source, id) {
            loadAssignmentCategoryPopup(source, translatedResources.add, id);
        },
        loadAssignmentCategoryPopup = function (source, mode, id) {
            if (id != undefined)
                mode = 'Edit';
            var title = mode + ' ' + translatedResources.AssignmentCategoryName;
            globalFunctions.loadPopup(source, '/Assignments/Assignment/InitAssignmentCategoryMaster?id=' + id, title, 'suggestion-dailog');
            $(source).off("modalLoaded");
            $("#tblAssignmentCategory_filter").hide();
            //$('#tblAssignmentCategory').DataTable().search('').draw();
            $("#AssignmentCategorySearch").on("input", function (e) {
                e.preventDefault();
                $('#tblAssignmentCategory').DataTable().search($(this).val()).draw();
            });
        },
        onSaveSuccess = function (e) {
            if (e.Success == true) {
                $("#myModal").modal('hide');
                if ($('#AssigntmentCatgoryId').val() === "0") {
                    e.Message = 'Added Successfully';
                } else {
                    e.Message = 'Updated Successfully';
                }
                globalFunctions.showMessage(e.NotificationType, e.Message);
                loadAssignmentCategoryGrid();
                $("#tblAssignmentCategory_filter").hide();
            }
        },
        initAssignmentCategoryGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false
            };
            grid.init(settings);
        };



    return {
        Init,
        assignmentCategoriesArrayList,
        PushAssignmentCategory: PushAssignmentCategory,
        onSaveSuccess: onSaveSuccess,
        addAssignmentCategoryPopup: addAssignmentCategoryPopup,
        loadAssignmentCategoryGrid: loadAssignmentCategoryGrid
    }
}();

$(document).ready(function () {
    AssignmentCategoryConfiguration.Init();

    $(document).on('click', '#btnAddAssignmentCategory', function () {
        AssignmentCategoryConfiguration.addAssignmentCategoryPopup($(this));
    });

    $("#tblAssignmentCategory_filter").hide();
    $('#tblAssignmentCategory').DataTable().search('').draw();
    $("#AssignmentCategorySearch").on("input", function (e) {
        e.preventDefault();
        $('#tblAssignmentCategory').DataTable().search($(this).val()).draw();
    });

});