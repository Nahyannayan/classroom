﻿var uploadedFiles = [];
var studentHomeworkAddEdit = function () {
    //var init = function () {
    //    $("#postedFile").fileinput({
    //        language: translatedResources.locale,
    //        title: translatedResources.BrowseFile,
    //        theme: "fas",
    //        showUpload: true,
    //        showCaption: false,
    //        showMultipleNames: false,
    //        showRemove: false,
    //        //uploadUrl: "https://gemsedu.sharepoint.com/sites/App-" + $("#SchoolCode").val() + "/ClassroomSync/_api/web/getfolderbyserverrelativeurl('StudentAssignmentFile/" + $("#OldUserId").val() + "/')/files/add(overwrite=true, url='#filename#')",
    //        uploadUrl: "/Assignment/Assignment/UploadStudentAssignmentFiles",
    //        uploadExtraData: function () {
    //            return {
    //                //__RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
    //                assignmentId: $("#AssignmentId").val(),
    //                studentId: $("#StudentId").val()

    //            };
    //        },
    //        fileActionSettings: {
    //            showZoom: false,
    //            showUpload: false,
    //            //indicatorNew: "",
    //            showDrag: false
    //        },

    //        uploadAsync: true,
    //        browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
    //        removeClass: "btn btn-sm m-0 z-depth-0",
    //        uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
    //        cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
    //        overwriteInitial: false,
    //        //previewFileIcon: '<i class="fas fa-file"></i>',
    //        initialPreviewAsData: false, // defaults markup
    //        initialPreviewConfig: [{
    //            frameAttr: {
    //                title: 'My Custom Title',
    //            }
    //        }],
    //        allowedFileExtensions: translatedResources.FileTypes,
    //        preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
    //        previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
    //        previewSettings: fileInputControlSettings.previewSettings,
    //        previewFileExtSettings: fileInputControlSettings.previewFileExtSettings,
    //        maxFileSize: translatedResources.fileSizeAllow,
    //        msgSizeTooLarge: 'Total File size (<b>{size}</b>) exceeds maximum allowed upload size of <b>{maxSize}</b>. Please retry your upload!'
    //    }).off('filebatchselected').on('filebatchselected', function (event, files) {
    //        var size = 0;
    //        $.each(files, function (ind, val) {
    //            //console.log($(this).attr('size'));
    //            size = size + $(this).attr('size');
    //        });

    //        var maxFileSize = $(this).data().fileinput.maxFileSize,
    //            msg = $(this).data().fileinput.msgSizeTooLarge,
    //            formatSize = (s) => {
    //                i = Math.floor(Math.log(s) / Math.log(1024));
    //                sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    //                out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
    //                return out;
    //            };

    //        if ((size / 1024) > maxFileSize) {
    //            msg = msg.replace('{name}', 'all files');
    //            msg = msg.replace('{size}', formatSize(size));
    //            msg = msg.replace('{maxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
    //            //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
    //            if (files.length > 1) {
    //                $(".kv-fileinput-error.file-error-message").html('');
    //                $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
    //                <span aria-hidden="true">×</span>
    //              </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
    //                $(".close.kv-error-close").on('click', function (e) {
    //                    $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
    //                    //$(".fileinput-upload").removeClass('d-none');
    //                });
    //            }
    //            else {
    //                $(".kv-fileinput-error.file-error-message").html('');
    //                $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
    //                <span aria-hidden="true">×</span>
    //              </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
    //                $(".close.kv-error-close").on('click', function (e) {
    //                    $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
    //                    //$(".fileinput-upload").removeClass('d-none');
    //                });
    //            }
    //        }

    //    }).on("filepreupload", function (event, data) {
    //        studentHomeworkAddEdit.createFolder($("#OldUserId").val());
    //        data.jqXHR.setRequestHeader('Authorization', 'Bearer ' + $("#SharepointToken").val());
    //        data.jqXHR.setRequestHeader("accept", "application/json;odata=verbose");
    //    }).on('fileuploaded', function (event, previewId, index, fileId, data) {
    //        debugger;
    //        uploadedFiles.push(data.d);
    //        if (uploadedFiles.length == filesList.length) {
    //            $('#postedFile').fileinput('clear');
    //            $("#StudentAssignmentFiles").mCustomScrollbar("destroy"); //Destroy
    //            filesList = [];
    //            studentHomeworkAddEdit.PostUploadedFiles(uploadedFiles);
    //            uploadedFiles = [];
    //            $('#postedFile').fileinput('clear');
    //            $('#fileUploadModal').modal('hide');
    //        }

    //        //addEditAssignment.validateUploadFiles(data);
    //    }).on('filebatchuploadcomplete', function (event, data, previewId, index) {
    //        globalFunctions.showSuccessMessage(translatedResources.FileUploaded);
    //        //$.ajax({
    //        //    type: "GET",
    //        //    url: "/Assignments/Assignment/GetUploadedStudentFile",
    //        //    data: {
    //        //        assignmentId: $("#AssignmentId").val(),
    //        //        studentId: $("#StudentId").val()
    //        //    },
    //        //    beforeSend: function () {
    //        //        $("#StudentAssignmentFiles").mCustomScrollbar("destroy"); //Destroy
    //        //    },
    //        //    success: function (response) {
    //        //        $("#StudentAssignmentFiles").html("");
    //        //        $("#StudentAssignmentFiles").append(response);
    //        //        $('#divMarktoComplete').show();
    //        //        $("#StudentAssignmentFiles").mCustomScrollbar({
    //        //            autoExpandScrollbar: true,
    //        //            autoHideScrollbar: true,
    //        //            scrollbarPosition: "outside"
    //        //        });
    //        //    },
    //        //    error: function (error) {
    //        //        globalFunctions.onFailure();
    //        //    }
    //        //});

    //        $('#postedFile').fileinput('clear');
    //        $('#fileUploadModal').modal('hide');

    //    });

    //    //to close drawer
    //    $('#UploadStudASgFiles').on('fileuploaded', function (event, data, previewId, index) {

    //    });
    //},




    var init = function () {
        $("#postedFile").fileinput({
            language: translatedResources.locale,
            title: translatedResources.BrowseFile,
            theme: "fas",
            showUpload: true,
            showCaption: false,
            showMultipleNames: false,
            showRemove: true,
            uploadUrl: "/Assignments/Assignment/UploadStudentAssignmentFiles",
            uploadExtraData: function () {
                return {
                    //__RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
                    assignmentId: $("#AssignmentId").val(),
                    studentId: $("#StudentId").val()

                };
            },
            fileActionSettings: {
                showZoom: false,
                showUpload: false,
                //indicatorNew: "",
                showDrag: false
            },

            uploadAsync: true,
            browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
            removeClass: "btn btn-sm m-0 z-depth-0",
            uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
            cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
            overwriteInitial: false,
            //previewFileIcon: '<i class="fas fa-file"></i>',
            initialPreviewAsData: false, // defaults markup
            initialPreviewConfig: [{
                frameAttr: {
                    title: 'My Custom Title',
                }
            }],
            allowedFileExtensions: translatedResources.FileTypes,
            preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
            previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
            previewSettings: fileInputControlSettings.previewSettings,
            previewFileExtSettings: fileInputControlSettings.previewFileExtSettings,
            maxFileSize: translatedResources.fileSizeAllow,
            msgSizeTooLarge: 'Total File size (<b>{size}</b>) exceeds maximum allowed upload size of <b>{maxSize}</b>. Please retry your upload!'
        }).off('filebatchselected').on('filebatchselected', function (event, files) {
            var size = 0;
            $.each(files, function (ind, val) {
                //console.log($(this).attr('size'));
                size = size + $(this).attr('size');
            });

            var maxFileSize = $(this).data().fileinput.maxFileSize,
                msg = $(this).data().fileinput.msgSizeTooLarge,
                formatSize = (s) => {
                    i = Math.floor(Math.log(s) / Math.log(1024));
                    sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                    out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
                    return out;
                };

            if ((size / 1024) > maxFileSize) {
                msg = msg.replace('{name}', 'all files');
                msg = msg.replace('{size}', formatSize(size));
                msg = msg.replace('{maxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
                //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
                if (files.length > 1) {
                    $(".kv-fileinput-error.file-error-message").html('');
                    $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                    $(".close.kv-error-close").on('click', function (e) {
                        $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                        //$(".fileinput-upload").removeClass('d-none');
                    });
                }
                else {
                    $(".kv-fileinput-error.file-error-message").html('');
                    $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                    $(".close.kv-error-close").on('click', function (e) {
                        $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                        //$(".fileinput-upload").removeClass('d-none');
                    });
                }

                $(".fileinput-upload").attr('disabled', true);
            }
        }).off('filebatchuploadcomplete').on('filebatchuploadcomplete', function (event, data, previewId, index) {
            globalFunctions.showSuccessMessage(translatedResources.FileUploaded);
            $.ajax({
                type: "GET",
                url: "/Assignments/Assignment/GetUploadedStudentFile",
                data: {
                    assignmentId: $("#AssignmentId").val(),
                    studentId: $("#StudentId").val()
                },
                beforeSend: function () {
                    $("#StudentAssignmentFiles").mCustomScrollbar("destroy"); //Destroy
                },
                success: function (response) {
                    $("#StudentAssignmentFiles").html("");
                    $("#StudentAssignmentFiles").append(response);
                    $('#divMarktoComplete').show();
                    $("#StudentAssignmentFiles").mCustomScrollbar({
                        autoExpandScrollbar: true,
                        autoHideScrollbar: true,
                        scrollbarPosition: "outside"
                    });
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });

            $('#postedFile').fileinput('clear');
            $('#fileUploadModal').modal('hide');

        });

        //to close drawer
        $('#UploadStudASgFiles').on('fileuploaded', function (event, data, previewId, index) {

        });
    },


        createFolder = function (OldUSerId) {

            var settings = {
                "url": "https://gemsedu.sharepoint.com/sites/App-" + $("#SchoolCode").val()+"/ClassroomSync/_api/web/Folders/add('StudentAssignmentFile/" + OldUSerId + "')",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Authorization": "Bearer " + $("#SharepointToken").val(),
                    "Accept": "application/json;odata=verbose"
                }
            };

            $.ajax(settings).done(function (response) {
                console.log(response);
            });
        },
        redirectToTask = function (e) {
            var isEnable = ($(e).attr("data-istaskenable") === 'True');
            var taskId = $(e).attr("data-taskid");
            var studentId = $(e).attr("data-studentid");
            if (isEnable) {
                window.location.href = "/Assignments/Assignment/GetStudentTaskDetails?taskId=" + taskId + "&studentId=" + studentId;
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.CompletePreviousTask);
            }
            localStorage.setItem("quizStarted", 0);
        },
        logQuizTime = function (source, id, resourceId, resourceType, IsStart) {
            $.ajax({
                type: 'POST',
                url: '/Files/Files/LogQuizTime',
                data: { id: id, ResourceId: resourceId, ResourceType: resourceType, IsStartQuiz: IsStart },
                success: function (result) {

                },
                error: function (msg) {

                }
            });
        },
        redirectToPeerPage = function (e) {
            var isEnable = ($(e).attr("data-iscompleted") === 'True');
            var AsgStudId = $(e).attr("data-asgstdid");
            var PeerReviewId = $(e).attr("data-peerreviewid");
            if (isEnable) {
                window.location.href = "/Assignments/Assignment/StudentHomeworkReview?id=" + AsgStudId + "&encPeerReviewId=" + PeerReviewId + "&redirectionId=" + translatedResources.peerAssignmentStudentId
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.StudentNotCompletedHomework);
            }
        },
        PostUploadedFiles = function (uploadedFiles) {

            debugger;
            var filesDetails = [];
            $.each(uploadedFiles, function (i, item) {
                filesDetails.push({
                    FileName: item.Name,
                    UploadedFileName: item.ServerRelativeUrl,
                    PathtoDownload: item.ServerRelativeUrl,
                    SharepointUploadedFileURL: item.LinkingUri
                })
            });
            filesList = [];
            uploadedFiles = [];
            $.ajax({
                type: 'POST',
                url: '/Assignments/Assignment/UploadStudentFiles',
                data: {
                    lstStudentFiles: JSON.stringify(filesDetails),
                    assignmentId: $("#AssignmentId").val(),
                    studentId: $("#StudentId").val()
                },
                success: function (response) {
                    debugger;
                    $("#StudentAssignmentFiles").html("");
                    $("#StudentAssignmentFiles").append(response);
                    $('#divMarktoComplete').show();
                    uploadedFiles = [];
                    $("#StudentAssignmentFiles").mCustomScrollbar({
                        autoExpandScrollbar: true,
                        autoHideScrollbar: true,
                        scrollbarPosition: "outside"
                    });
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });

        }


    return {
        init: init,
        logQuizTime: logQuizTime,
        redirectToTask: redirectToTask,
        redirectToPeerPage: redirectToPeerPage,
        PostUploadedFiles: PostUploadedFiles,
        createFolder: createFolder
    };
}();

$(document).ready(function () {

    studentHomeworkAddEdit.init();

    $('#divMarktoComplete').hide();

    $("#btnMarkInComplete").click(function (source) {
        var studentId = $(this).attr("data-studentId");
        var assignmentId = $(this).attr("data-AsgId");
        globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
        $(source).unbind('okClicked');
        $(source).bind('okClicked', source, function (e) {
            $.ajax({
                type: "GET",
                url: "/Assignments/Assignment/MarkAsIncompleteAssignment",
                data: { studentId: studentId, assignmentId: assignmentId },//parent id
                success: function (response) {

                    globalFunctions.showMessage(true, translatedResources.DeleteTaskSuccess);
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        });

    });
    //$("#postedFile").change(function (e) {
    //    var filesctr = document.getElementById("postedFile");
    //    for (var index = 0; index < filesctr.files.length; index++) { filesList.push(filesctr.files[index]); }
    //});

    $(".lnkMarkAsCompleted").click(function () {

        var assignmentStudentId = $(this).attr("data-student-assignment-id");
        MarkAsCompleted(assignmentStudentId);

    });

    function MarkAsCompleted(id) {
        $.ajax({
            url: '/Assignment/MarkAsComplete?AssignmentStudentId=' + id,
            type: 'GET',  // http method
            success: function (data, status, xhr) {
                if (data) {
                    $('.lnkMarkAsCompleted').attr('style', 'display: none !important');
                    $("#lnkCompleted").show();
                    $(".delete-student-file-btn").hide("");
                    $("[id*='disabledStudentPeer_']").removeClass('disabled');
                    //$("#showPeersSection").show();
                }
                else {
                    $(this).show();
                    $("#lnkCompleted").hide();
                    //$("#showPeersSection").hide();
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {

            }
        });
    }
})