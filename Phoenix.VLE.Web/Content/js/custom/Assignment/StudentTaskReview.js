﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var formElements = new FormElements();
var studentTaskReview = function () {
    var init = function () {

    },
        viewFile = function (source, id) {
            //source.preventDefault();
            if (globalFunctions.isValueValid(id)) {
                window.open("/assignments/Assignment/PeersTaskUploadedDocument?id=" + id, "_blank");
            }
        },
        MarkAsCompleted = function (id) {
            $.ajax({
                url: '/Assignment/MarkAsCompleteTaskReview?peerReviewId=' + id,
                type: 'GET',  // http method
                success: function (data, status, xhr) {
                    if (data) {
                        $("#lnkCompletedReview").show();
                        $("#lnkMarkAsCompletereview").hide();
                        $("#ViewStudentFile").hide();
                    }
                    else {
                        $(this).show();
                        $("#lnkCompletedReview").hide();
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {

                }
            });
        }
    return {
        init: init,
        viewFile: viewFile,
        MarkAsCompleted: MarkAsCompleted
    };
}();
$(document).ready(function () {
    studentTaskReview.init();
    $(".ViewStudentTaskFile").click(function () {
        studentTaskReview.viewFile($(this), $(this).attr("data-id"));
    });
});