﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var studentHomeworkReview = function () {
    var init = function () {
        $('[data-toggle="tooltip"]').tooltip();
        $("#btnBack").show();

        $(document).on('click', '#btnBack', function () {
            if (translatedResources.IsTeacher == "True") {
                location.href = "/assignments/assignment/GetAssignmentStudentDetailsByAssgnmtId?id=" + translatedResources.redirectionId;
            }
            else {
                location.href = "/assignments/assignment/StudentHomeworkAddEdit?id=" + translatedResources.redirectionId;
            }
        });
        $('.lightgallery').lightGallery({
            selector: '.galleryImg',
            share: false
        });

    },
        viewFile = function (source, id) {
            //source.preventDefault();
            if (globalFunctions.isValueValid(id)) {
                window.open("/assignments/Assignment/PeersUploadedDocument?id=" + id, "_blank");
            }
        },
        MarkAsCompleted = function (id) {
            $.ajax({
                url: '/Assignment/MarkAsCompleteReview?peerReviewId=' + id,
                type: 'GET',  // http method
                success: function (data, status, xhr) {
                    if (data) {
                        $("#lnkCompletedReview").show();
                        $("#lnkMarkAsCompletereview").hide();
                        $(".ViewStudentFile").hide();
                        $(".downloadreviewvisible").show();
                    }
                    else {
                        $(this).show();
                        $("#lnkCompleted").hide();
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {

                }
            });
        }
    return {
        init: init,
        viewFile: viewFile,
        MarkAsCompleted: MarkAsCompleted

    };
}();

$(document).ready(function () {
    studentHomeworkReview.init();
    $(".ViewStudentFile").click(function () {
        studentHomeworkReview.viewFile($(this), $(this).attr("data-id"));
    });
    //$(document).on('click', '#ViewFile', function (e) {
    //    e.preventDefault();
    //    var id = $(this).data("id");
    //    if (globalFunctions.isValueValid(id)) {
    //        window.open("/Document/Viewer/Index?id=" + id + "&module=assignment", "_blank");
    //    }
    //});
    $(".ViewFile").click(function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var module = $(this).data("modulename");
       
        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=" + module+"", "_blank");
        }
    });

    $("#lnkMarkAsCompletereview").click(function () {
        var peerreviewId = $(this).attr("data-peerreviewid");
        studentHomeworkReview.MarkAsCompleted(peerreviewId);
    });
});