﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var assignmentComment = function () {
    var loadComments = function () {
        var section = $("#CommentListContainer");
        var assignmentStudentId = section.attr("data-assignmentStudentId");
        $.ajax({
            type: 'GET',
            url: "/Assignments/Assignment/LoadAssignmentComments?assignmentStudentId=" + assignmentStudentId,
            async: false,
            success: function (result) {
                $("#CommentListContainer").html("");
                $("#Comment").val("");
                $("#CommentListContainer").html(result);
            },
            error: function (msg) {
                globalFunctions.showMessage("error", translatedResources.technicalError);
            }
        });
    },
        deleteAssignmentComment = function (source, assignmentCommentId) {
            globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
            $(source).unbind('okClicked');
            $(source).bind('okClicked', source, function (e) {
                $.ajax({
                    type: "POST",
                    url: "/Assignments/Assignment/DeleteAssignmentComment",
                    data: { assignmentCommentId: assignmentCommentId },//parent id
                    success: function (response) {
                        if (response.Success) {
                            $("#commentsList_" + assignmentCommentId + "").html('');
                        }
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            });
        }

    editAssignmentComment = function (assignmentCommentId, assignmentStudentId) {
        $.ajax({
            type: 'GET',
            url: "/Assignments/Assignment/editAssignmentComment?assignmentCommentId=" + assignmentCommentId + "&assignmentStudentId=" + assignmentStudentId,
            async: false,
            success: function (result) {
                CKEDITOR.instances.Comment.setData(result);
                $("#hdnAssignmentCommentId").val(assignmentCommentId);
                $("#commentsList_" + assignmentCommentId + "").html('');
            },
            error: function (msg) {
                globalFunctions.showMessage("error", translatedResources.technicalError);
            }
        })
    };
    return {
        loadComments: loadComments,
        deleteAssignmentComment: deleteAssignmentComment,
        editAssignmentComment: editAssignmentComment
    };
}();

$(document).ready(function () {
    var scrollCounter = 1;
    var assignmentCommentIds = "";
    // this condition call when there is atleast 1 feedback details list
    if ($("#feedbackDiv").html().trim() != "No Feedback") {
        $(window).scroll(function () {
            var scrollPercent = $(window).scrollTop() / $(document).height() * 100
            if (scrollPercent >= 50 && scrollCounter == 1) { // Call Ajax if Scroll more than 50% down
                scrollCounter = 2;
                if ($("#hdnteacherUser").val() == "teacherUser") {
                    $('[id*=studentUnseenCommentId_]').each(function (index, value) {
                        if (assignmentCommentIds == "") {
                            assignmentCommentIds = value.id.split('_')[1];
                        } else {
                            assignmentCommentIds += ',' + value.id.split('_')[1];;
                        }
                    });
                }
                if ($("#hdnstudentUser").val() == "studentUser") {
                    $('[id*=teacherUnseenCommentId_]').each(function (index, value) {
                        if (assignmentCommentIds == "") {
                            assignmentCommentIds = value.id.split('_')[1];;
                        } else {
                            assignmentCommentIds += ',' + value.id.split('_')[1];;
                        }
                    });
                }
                if (assignmentCommentIds != "") {
                    $.ajax({
                        type: "POST",
                        url: "/Assignments/Assignment/MarkSeenStudentAssignmentComments?CommentIds=" + assignmentCommentIds + "&IsCommentSeenByStudent=" + 1,
                        success: function (response) {
                            if (response.Success === true) {
                                globalFunctions.showSuccessMessage(translatedResources.CommentSeenSuccessful);
                            }
                        }
                    });
                }
            }
        });
    }

    $('.lightgallery_quiz').lightGallery({
        selector: '.galleryImg',
        share: false
    });
    CKEDITOR.plugins.addExternal('ckeditor_wiris', '', 'plugin.js');
    CKEDITOR.replace('Comment', {
        htmlencodeoutput: false,
        height: '180px',
        extraPlugins: 'ckeditor_wiris',
        allowedContent: true,
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P,
        contentsLangDirection: $("html").attr("dir")
    });
    $(document).on('click', '#ViewAssignmentTaskFile', function (e) {
        e.preventDefault();
        var id = $(this).data("filename");
        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=addeditAssignmentTask", "_blank");
        }
    });
    $('body').on('click', '#DeleteAssignmentTaskFile', function () {
        //debugger;
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        var fileName = $(this).attr('data-fileName');
        var uploadedfilepath = $(this).attr('data-uploadedPath');
        var quizQuestionFileId = $(this).attr('data-FileId');
        var quizQuestionId = $(this).attr('data-QuizQuestionId');
        $(document).bind('okClicked', $(this), function (e) {
            $.ajax({
                type: 'POST',
                data: { id: quizQuestionFileId, quizQuestionId: quizQuestionId, fileName: fileName, UploadedFilePath: uploadedfilepath },
                url: '/Assignments/Assignment/DeleteAssignmentTaskFile',
                success: function (result) {
                    globalFunctions.showMessage('success', translatedResources.DeletedSuccessfully);
                    $("#fileList").html(result);
                }
            });
        });
    });
    $(document).on('click', '#btn-submit', function (e) {

        var isValidated = globalFunctions.validateBannedWordsFormData('frmAddAssignmentComment');

        if (isValidated) {
            if (!CKEDITOR.instances.Comment.getData().replace(/<[^>]*>/gi, '').length) {
                $('#AssignmentCommentError').html(translatedResources.Mandatory);
                return false;
            }

            if (CKEDITOR.instances.Comment.getData().replace(/<[^>]*>/gi, '').length > 5) {
                var commentValue = CKEDITOR.instances.Comment.getData();
                commentValue = commentValue.slice(0, 6);
                if (commentValue == "&nbsp;") {
                    $('#AssignmentCommentError').html(translatedResources.BlankSpaceMessage);
                    return false;
                }
            }

            var isTeacher = $("#isTeacherLogin").val();
            var formData = new FormData();

            formData.append("AssignmentCommentId", $("#hdnAssignmentCommentId").val());
            formData.append("AssignmentStudentId", $("#AssignmentStudentId").val());
            formData.append("AssignmentTitle", $("#AssignmentTitle").val());
            formData.append("StudentId", $("#StudentId").val());
            formData.append("Comment", CKEDITOR.instances.Comment.getData());

            if (isTeacher == "True") {
                $.each(listRecordings, function (idx, file) {
                    formData.append("recordings", file, file.name);
                });
            }

            $.ajax({
                type: 'POST',
                processData: false,
                contentType: false,
                url: '/Assignments/Assignment/SaveAssignmentComment',
                data: formData,
                success: function (result) {
                    if (result.Success) {
                        globalFunctions.showSuccessMessage(translatedResources.FeedbackSuccessMessage);
                        CKEDITOR.instances.Comment.setData('');
                        $('#recordingslist').empty();
                        $('#AssignmentCommentError').text('');
                        assignmentComment.loadComments();
                        listRecordings = [];
                        $('#fileList').html('');
                        $('.lightgallery_quiz').lightGallery({
                            selector: '.galleryImg',
                            share: false
                        });
                    }
                    else {
                        globalFunctions.showMessage(result.NotificationType, result.Message);
                    }
                },
                error: function (msg) {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        }

    });

});