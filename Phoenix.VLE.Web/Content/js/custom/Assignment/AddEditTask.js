﻿var addEditAssignment = function () {
    saveAssignmentTask = function () {        
        taskOrder = parseInt(taskOrder) + 1;
        taskList.push({
            AssignmentId: $("#AssignmentId").val(),
            TaskId: $("#TaskId").val(),
            TaskTitle: $("#TaskTitle").val(),
            TaskDescription: $("#TaskDescription").val(),
            IsActive: $("#IsActive").val(),
            SortOrder: taskOrder
        });

        if ($('form#frmAddTask').valid()) {
            $('form#frmAddTask').submit();
        }
        $("#myModal").modal('hide');
    }

    return {
        init: init,
       
    };
}();