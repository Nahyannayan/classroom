﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var formElements = new FormElements();
var studentTaskDetails = function () {
    var init = function () {
        $("#postedFile").fileinput({
            language: translatedResources.locale,
            title: translatedResources.BrowseFile,
            theme: "fas",
            showUpload: true,
            showRemove: true,
            showCaption: false,
            showMultipleNames: true,
            uploadUrl: "/Assignments/Assignment/UploadStudentTaskFiles",
            uploadExtraData: function () {
                return {
                    __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
                    taskId: $("#TaskId").val(),
                    studentId: $("#hdnStudentId").val()

                };
            },
            fileActionSettings: {
                showZoom: false,
                showUpload: false,
                //indicatorNew: "",
                showDrag: false
            },
            uploadAsync: false,
            browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
            removeClass: "btn btn-sm m-0 z-depth-0",
            uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
            cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
            overwriteInitial: false,
            //previewFileIcon: '<i class="fas fa-file"></i>',
            initialPreviewAsData: false, // defaults markup
            initialPreviewConfig: [{
                frameAttr: {
                    title: 'My Custom Title',
                }
            }],
            preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
            previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
            allowedFileExtensions: translatedResources.FileTypes,
            previewSettings: fileInputControlSettings.previewSettings,
            previewFileExtSettings: fileInputControlSettings.previewFileExtSettings,
            maxFileSize: translatedResources.fileSizeAllow,
            msgSizeTooLarge: 'Total File size (<b>{size}</b>) exceeds maximum allowed upload size of <b>{maxSize}</b>. Please retry your upload!'
        }).off('filebatchselected').on('filebatchselected', function (event, files) {
            var size = 0;
            $.each(files, function (ind, val) {
                //console.log($(this).attr('size'));
                size = size + $(this).attr('size');
            });

            var maxFileSize = $(this).data().fileinput.maxFileSize,
                msg = $(this).data().fileinput.msgSizeTooLarge,
                formatSize = (s) => {
                    i = Math.floor(Math.log(s) / Math.log(1024));
                    sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                    out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
                    return out;
                };

            if ((size / 1024) > maxFileSize) {
                msg = msg.replace('{name}', 'all files');
                msg = msg.replace('{size}', formatSize(size));
                msg = msg.replace('{maxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
                //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
                if (files.length > 1) {
                    $(".kv-fileinput-error.file-error-message").html('');
                    $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                    $(".close.kv-error-close").on('click', function (e) {
                        $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                        //$(".fileinput-upload").removeClass('d-none');
                    });
                }
                else {
                    $(".kv-fileinput-error.file-error-message").html('');
                    $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                    $(".close.kv-error-close").on('click', function (e) {
                        $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                        //$(".fileinput-upload").removeClass('d-none');
                    });
                }

                $(".fileinput-upload").attr('disabled', true);
            }
        }).off('filebatchuploadcomplete').on('filebatchuploadsuccess', function (event, data) {
            if (data.response.Success) {
                //closing uploadwindow;
                $('.file-upload-widget').animate({
                    right: '-300'
                });
                $('#postedFile').fileinput('clear');
                $('#fileUploadModal').modal('hide');
                globalFunctions.showMessage(data.response.CssClass, data.response.Message);
                getUploadedTaskFileList();

            }
        });
    },
        markTaskAsComplete = function (studentTaskId, assignmentStudentId) {
            var TaskTotalMarks = $("#hdnTaskMarks").val();
            if (TaskTotalMarks === undefined || assignmentStudentId === undefined)
                TaskTotalMarks = 0;
            if (TaskTotalMarks != 0) {
                var submitmarks = $('#txtTaskSubmitMarks').val();
                studentTaskDetails.SaveTaskSubmitMarksPopup($(this), studentTaskId, assignmentStudentId, submitmarks, TaskTotalMarks);
            } else {
                studentTaskDetails.markTaskAsCompleteWithGrade(studentTaskId);
            }
        },
        markTaskAsCompleteWithGrade = function (studentTaskId) {
            var taskMarks = parseInt($("#hdnTaskMarks").val());
            $.ajax({
                type: "GET",
                url: "/Assignments/Assignment/MarkAsComlpeteTask?studentTaskId=" + studentTaskId,
                success: function (response) {
                    if (response.Success) {
                        $("#lnkCompleted").show();
                        $("#lnkMarkCompleted").hide();
                        if ($("#lnkClickToGrade").length > 0 && taskMarks == 0) {
                            $("#lnkClickToGrade").show();
                            $("#lnkClickToGrade").attr("data-IsCompleted", true);
                            globalFunctions.showSuccessMessage(translatedResources.CompletedTaskSuccess);

                        } else if (taskMarks != 0) {
                            $("#lnkClickToEditTaskMarks").show();
                            $("#lnkClickToEditTaskMarks").attr("data-IsCompleted", true);
                        }
                        if ($(".delete-student-file-btn").length > 0) {
                            $(".delete-student-file-btn").hide();
                        }
                    }
                    else {
                        globalFunctions.showErrorMessage(translatedResources.technicalError);
                    }
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        },
        submitTaskMarks = function (studentTaskId, StudentAssignmentId) {
            if ($('#txtTaskSubmitMarks').val() != '') {
                var marks = Math.round(parseFloat($('#txtTaskSubmitMarks').val()).toFixed(2));
                var taskTotalMarks = Math.round(parseFloat($("#hdnTaskMarks").val()).toFixed(2));
                var gradingId = $("#hdnTaskGradingTemplateId").val() != '' ? $("#hdnTaskGradingTemplateId").val() : 0;
                if (marks <= taskTotalMarks && marks >= Math.round(parseFloat(0).toFixed(2))) {
                    var txtMarks = parseFloat($("#txtTaskSubmitMarks").val()).toFixed(2);
                    $.ajax({
                        type: "GET",
                        url: "/Assignments/Assignment/SubmitTaskMarks?studentTaskId=" + studentTaskId + '&StudentAssignmentId=' + StudentAssignmentId + '&submitMarks=' + txtMarks + '&gradingTemplateId=' + gradingId,
                        success: function (response) {
                            if (response.ShortLabel != null) {
                                $("#myModal").modal('hide');
                                $("#gradingDiv").show();
                                $("#gradingDiv").html('');

                                if (response.GradingTemplateItemSymbol.indexOf('.png') != -1) {
                                    $("#gradingDiv").attr("title", response.ShortLabel);
                                    $("#gradingDiv").attr("data-title", response.ShortLabel);
                                    $("#gradingDiv").attr("data-title", response.ShortLabel);
                                    $("#gradingDiv").append('<img class="img-fluid taskGrade mt-2" style="width: 35px;" src="' + response.GradingTemplateItemSymbol + '" />')
                                }
                                else {
                                    $("#gradingDiv").append('<h2 class="m-0" style="color:' + response.GradingColor + '">' + response.ShortLabel + '</h2>');
                                }
                                globalFunctions.showSuccessMessage(translatedResources.TaskGradingMessage);
                            }
                            else {
                                $("#myModal").modal('hide');
                                $("#lnkClickToEditTaskMarks").show();
                                $("#lnkClickToEditTaskMarks").attr("data-IsCompleted", true);
                            }
                        },
                        error: function (error) {
                            globalFunctions.onFailure();
                        }
                    });
                    // Mark Complete task
                    studentTaskDetails.markTaskAsCompleteWithGrade(studentTaskId);
                } else {
                    globalFunctions.showErrorMessage(translatedResources.MarksValidationError);
                }
            } else {
                globalFunctions.showErrorMessage(translatedResources.txtMarksErrorMessage);
            }
        },
        loadGradingTemplateItemPopup = function (source, title, id, taskId, studentId, gradingType) {
            globalFunctions.loadPopup(source, '/Assignments/Assignment/LoadTaskGradingTemplateItems?gradingtemplateId=' + id + "&taskId=" + taskId + "&studentId=" + studentId + "&gradingType=" + gradingType, title, 'addtask-dialog');
        },
        checkGradingTemplateItem = function (e, id) {
            if ($(e).is(":checked")) {
                $('.chkSelectGradingTemplateItem').prop('checked', false);
                $(e).prop('checked', true);
                $("#selectedGradingitem").val(id);
            } else {
                $("#selectedGradingitem").val();
            }
        },
        assignGradeToStudentTask = function () {
            if ($("#selectedGradingitem").val() != undefined && $("#selectedGradingitem").val() != "0") {
                //Ajax call to update grade 
                $.ajax({
                    type: "GET",
                    url: "/Assignments/Assignment/AssignGradeToStudentTask?gradeId=" + $("#selectedGradingitem").val() + "&studentTaskId=" + $("#entityId").val(),

                    success: function (response) {
                        $("#myModal").modal('hide');
                        $("#gradingDiv").show();
                        $("#gradingDiv").html('');
                        if (response.GradingTemplateItemSymbol.indexOf('.png') != -1) {
                            $("#gradingDiv").append('<a href="javascript:void(0)" data-toggle="tooltip" title="' + response.ShortLabel + '" data-title="' + response.ShortLabel + '" data-placement="top" class="ml-2"><img class= "img-fluid" style = "height:70%;" src = "' + response.GradingTemplateItemSymbol + '"/></a >')
                        }
                        else {
                            $("#gradingDiv").append('<h2 class="m-0 text-bold" style="color:' + response.GradingColor + '">' + response.ShortLabel + '</h2>');
                        }
                        globalFunctions.showSuccessMessage(translatedResources.TaskGradingMessage);
                        window.location.reload();
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            }
            else {
                globalFunctions.showMessage("error", translatedResources.Assign);
            }
        },
        freetextanswers = function () {
            if ($('#IsStudent').val() != "False") {
                $('.freetext').each(function () {
                    IsFreeTextBlur = true;
                    var QuizId = $("#QuizId").val();
                    //var quesId = $("#" + $(this)[0].name).attr("data-questionid");
                    //var ansId = $("#" + $(this)[0].name).attr("data-answerid");
                    //var selectedAns = "QuizFreeText_" + $("#" + $(this)[0].name).data("answerid")
                    var quesId = $(this)[0].dataset.questionid;
                    var ansId = $(this)[0].dataset.answerid;
                    var selectedAns = "QuizFreeText_" + $(this)[0].dataset.answerid;
                    var ansValue = CKEDITOR.instances[selectedAns].getData();
                    var obMarks = 0;
                    //if (ansValue != "") { IsRequiredAttemptCount++; }
                    list.push({
                        QuizId: QuizId, QuizQuestionId: quesId
                        , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
                    });
                });
            }
        },
        resetMTPQuiz = function (quizId, quizQuestionId) {
            $.ajax({
                type: 'GET',
                url: '/Assignments/Assignment/ResetMTPQuestion?quizId=' + quizId + '&quizQuestionId=' + quizQuestionId,
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    var MTPClass = '.dragdrop_' + quizQuestionId;
                    $(MTPClass).html(data);
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
            list = jQuery.grep(list, function (value) {
                return parseInt(value.QuizQuestionId) != quizQuestionId;
            });
        },
        getUploadedTaskFileList = function () {

            $.ajax({
                type: "GET",
                url: "/Assignments/Assignment/GetuploadedTaskFileDetails",
                data: {
                    taskId: $("#TaskId").val(),
                    studentId: $("#hdnStudentId").val()
                },
                success: function (response) {
                    $("#StudentAssignmentFiles").html("");
                    $("#StudentAssignmentFiles").append(response);

                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        },
        SaveTaskSubmitMarksPopup = function (source, studentTaskId, assignmentStudentId, submitMarks, TaskTotalMarks) {
            studentTaskDetails.loadTaskSubmitMarksPopup(source, studentTaskId, assignmentStudentId, submitMarks, TaskTotalMarks);
        },
        loadTaskSubmitMarksPopup = function (source, studentTaskId, assignmentStudentId, submitMarks, TaskTotalMarks) {
            var title = 'Submit ' + translatedResources.TaskMarks;
            globalFunctions.loadPopup(source, '/Assignments/Assignment/InitSaveTaskMarks?studentTaskId=' + studentTaskId + '&assignmentStudentId=' + assignmentStudentId + '&=submitMarks=' + submitMarks + '&TaskTotalMarks=' + TaskTotalMarks, title, 'suggestion-dailog');
            $(source).off("modalLoaded");
        };

    return {
        init: init,
        markTaskAsComplete: markTaskAsComplete,
        markTaskAsCompleteWithGrade: markTaskAsCompleteWithGrade,
        submitTaskMarks: submitTaskMarks,
        loadGradingTemplateItemPopup: loadGradingTemplateItemPopup,
        checkGradingTemplateItem: checkGradingTemplateItem,
        resetMTPQuiz: resetMTPQuiz,
        freetextanswers: freetextanswers,
        assignGradeToStudentTask: assignGradeToStudentTask,
        getUploadedTaskFileList: getUploadedTaskFileList,
        SaveTaskSubmitMarksPopup: SaveTaskSubmitMarksPopup,
        loadTaskSubmitMarksPopup: loadTaskSubmitMarksPopup
    };
}();

$(document).ready(function () {
    studentTaskDetails.init();

    $(".btnAssignGrade").click(function () {
        debugger;
        var gradingTemplateId = parseInt($(this).attr("data-gradingId"));
        var studentId = parseInt($(this).attr("data-studentId"));
        var taskId = parseInt($(this).attr("data-taskId"));
        var studentTaskId = parseInt($(this).attr("data-studentTaskId"));
        var isCompleted = $(this).attr("data-IsCompleted");
        var TaskTotalMarks = $("#hdnTaskMarks").val();
        var assignmentStudentId = parseInt($(this).attr("data-studentAsgId"));
        var submitMarks = '';

        if (isCompleted == "true" && TaskTotalMarks == "0.00") {
            studentTaskDetails.loadGradingTemplateItemPopup($(this), translatedResources.Assign, gradingTemplateId, taskId, studentId, 4);
        }
        else if (isCompleted == "true" && TaskTotalMarks != 0) {
            studentTaskDetails.loadTaskSubmitMarksPopup($(this), studentTaskId, assignmentStudentId, submitMarks, TaskTotalMarks);
        }
        else {
            globalFunctions.showMessage("error", translatedResources.AssignmentMarkComplete);
        }
    });
    $(document).on('click', '#ViewQuizFile', function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=studentQuiz", "_blank");
        }
    });
    $(document).on('click', '#ViewQuizQuestionFile', function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=editStudentQuizQuestion", "_blank");
        }
    });

    $(document).on("keypress", "#txtTaskSubmitMarks", function (evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57)))
            return false;
        return true;
    });

});



$('.quiz-cover').on('click', function (event) {
    $('.cover-img-wrapper').removeClass('animate slideIn show');
    $(this).parent().find('.cover-img-wrapper').toggleClass('animate slideIn show');
    event.stopPropagation();
});

$(window).click(function (e) {
    if (e.target.class !== 'show')
        $(".cover-img-wrapper").removeClass("animate slideIn show");
});