﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
//var popoverEnabler = new LanguageTextEditPopover();

function archiveActiveAssignments() {
    jQuery.ajaxSettings.traditional = true;
    if ($('#selectAllUnassigned').is(':checked')) {
        $('#selectAllUnassigned').prop('checked', false);
    }
    var selectedMembers = $("#activeAssignmentList input[type='checkbox']:checked");
    var assignmentsToArchive = [];
    if (selectedMembers != null && selectedMembers.length > 0) {
        selectedMembers.each(function (i, elem) {
            assignmentsToArchive = assignmentsToArchive + elem.value + ',';
        });
        $.ajax({
            type: 'POST',
            data: { assignmentsToArchive: assignmentsToArchive },
            url: '/Assignments/Assignment/UpdateActiveAssignment',
            success: function (result) {
                loadAssignmentList();
                // location.reload();
            },
            error: function (data) { }
        });
    }
    else {
        globalFunctions.showWarningMessage(translatedResources.SelectAssignmentToArchive);
    }
}
function loadAssignmentList() {
    loadActiveAssignments();
    loadArchiveAssignment();
}

function loadActiveAssignments() {
    $.ajax({
        url: '/Assignments/Assignment/GetActiveAssignments',
        success: function (result) {
            $('#activeAssignmentList').html(result);
        },
        error: function (data) { }
    });
}
function loadArchiveAssignment() {
    $.ajax({
        url: '/Assignments/Assignment/GetArchiveAssignments',
        success: function (result) {
            $('#archiveAssignmentList').html(result);
        },
        error: function (data) { }
    });
}
$(document).on('keyup', '#availableMemberSearch', function (e) {
    var searchText = $(this).val().toLowerCase();
    $('#activeAssignmentList ul li').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
});
$(document).on('keyup', '#groupMemberSearch', function (e) {
    //if (e.which === 13) {
    var searchText = $(this).val().toLowerCase();
    $('#archiveAssignmentList ul li').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
    //}
});



$('#selectAllUnassigned').change(function () {
    var chk = $(this).is(':checked');
    $('input[type=checkbox]', "#activeAssignment").each(function () {
        $(this).prop('checked', chk);
    });
});

$(document).ready(function () {

    $(".delegates-wrapper").mCustomScrollbar({
        setHeight: "250",
        autoExpandScrollbar: true,
    });
});
