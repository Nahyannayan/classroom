﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var taskList = [];
var taskOrder = 0;
var pageRecords = [];
var assignmentStudentDetails = function () {
    var init = function () {
        $('[data-toggle="tooltip"]').tooltip()
        $('#tblAssignmentDetails').DataTable({
            ordering: false,
            searching: false,
            bInfo: false,
            bLengthChange: false,
            autoWidth: false,
            paging: false,
            /* columnDefs: [
                { "width": "4%", "targets": 0 },
                { "width": "71%", "targets": 1 },
                { "width": "25%", "targets": 2 }
            ], */
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            }
        });
    },
        loadGradingTemplateItemPopup = function (source, title, id, studAsgId, gradingType) {

            globalFunctions.loadPopup(source, '/Assignments/Assignment/LoadGradingTemplateItems?gradingtemplateId=' + id + "&studAsgId=" + studAsgId + "&gradingType=" + gradingType, title, 'addtask-dialog');

        },
        assignGradeToStudentAssignment = function () {

            if ($("#selectedGradingitem").val() != undefined || $("#selectedGradingitem").val() != "0") {
                //Ajax call to update grade 
                $.ajax({
                    type: "GET",
                    url: "/Assignments/Assignment/UpdateStudentAssignmentGrade?gradeId=" + $("#selectedGradingitem").val() + "&studentAssignmentId=" + $("#entityId").val(),

                    success: function (response) {
                        $("#myModal").modal('hide');
                        $("#gradingDiv").show();
                        $("#gradingDiv").html('');
                        if (response.GradingTemplateItemSymbol.indexOf('.png') != -1) {

                            $("#gradingDiv").append('<a href="javascript:void(0)" data-toggle="tooltip" title="' + response.ShortLabel + '" data-title="' + response.ShortLabel + '" data-placement="top" class="ml-2"><img class= "img-fluid" style = "height:70%;" src = "' + response.GradingTemplateItemSymbol + '"/></a >')
                        }
                        else {
                            $("#gradingDiv").addClass("txt-grade mt-n2")
                            $("#gradingDiv").append('<h2 class="m-0 text-bold" style="color:' + response.GradingColor + '">' + response.ShortLabel + '</h2>');
                        }
                        globalFunctions.showSuccessMessage(translatedResources.AssignmentGradingMessage);
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            }
            else {
                globalFunctions.showMessage("error", translatedResources.Assign)
            }
        },
        assignGradeToStudentAssignmentInList = function () {

            if ($("#selectedGradingitem").val() != undefined && $("#selectedGradingitem").val() != "0") {
                //Ajax call to update grade 
                $.ajax({
                    type: "GET",
                    url: "/Assignments/Assignment/UpdateStudentAssignmentGrade?gradeId=" + $("#selectedGradingitem").val() + "&studentAssignmentId=" + $("#entityId").val(),

                    success: function (response) {
                        $("#myModal").modal('hide');
                        $("#gradingDiv_" + $("#entityId").val() + "").show();
                        $("#gradingDiv_" + $("#entityId").val() + "").html('');
                        if (response.GradingTemplateItemSymbol.indexOf('.png') != -1) {
                            // $("#gradingDiv_" + $("#entityId").val() + "").append('<a href="javascript:void(0)" data-toggle="tooltip" title="' + response.ShortLabel + '" data-title="' + response.ShortLabel + '" data-placement="top" class="ml-2"><img class= "img-fluid" style = "height:70%;" src = "' + response.GradingTemplateItemSymbol + '"/></a >')
                            $("#gradingDiv_" + $("#entityId").val() + "").attr("title", response.ShortLabel);
                            $("#gradingDiv_" + $("#entityId").val() + "").attr("data-title", response.ShortLabel);
                            $("#gradingDiv_" + $("#entityId").val() + "").attr("data-title", response.ShortLabel);
                            $("#gradingDiv_" + $("#entityId").val() + "").append('<img class="img-fluid" src="' + response.GradingTemplateItemSymbol + '" />')
                        }
                        else {
                            $("#gradingDiv_" + $("#entityId").val() + "").css("color", response.GradingColor)
                            $("#gradingDiv_" + $("#entityId").val() + "").append(response.ShortLabel);
                        }
                        globalFunctions.showSuccessMessage(translatedResources.AssignmentGradingMessage);
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            }
            else {
                globalFunctions.showMessage("error", translatedResources.Assign)
            }
        },
        assignFeedBackToTask = function () {

            if ($("#Feedback").val().trim() != '') {
                var studentAssignmentId = $("#studentAssignmentId").val();
                var taskId = $("#taskId").val();
                var studentId = $("#studentId").val();
                var studentTaskId = $("#studentTaskId").val();

                var feedback = new FormData()
                feedback.append("StudentAssignmentId", studentAssignmentId);
                feedback.append("TaskId", taskId);
                feedback.append("StudentId", studentId);
                feedback.append("StudentTaskId", studentTaskId);
                feedback.append("Feedback", $("#Feedback").val());
                $.each(listRecordings, function (idx, file) {
                    feedback.append("recordings", file, file.name);
                });
                $.ajax({
                    type: "POST",
                    url: "/Assignments/Assignment/InsertTaskFeedback",
                    data: feedback,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        listRecordings = pageRecords;
                        $("#myModal").modal('hide');
                        $("ul#recordingslist.multiple-recording").remove();
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            }
            else {
                $('#FeedbackValidation').html('Mandatory field');
            }

        },
        assignFeedBackToObjective = function () {

            if ($("#Feedback").val().trim() != '') {
                var studentAssignmentId = $("#AssignmentStudentId").val();
                var ObjectiveId = $("#ObjectiveId").val();
                var StudentObjectiveMarkId = $("#StudentObjectiveMarkId").val();


                var feedback = new FormData()
                feedback.append("AssignmentStudentId", studentAssignmentId);
                feedback.append("StudentObjectiveMarkId", StudentObjectiveMarkId);
                feedback.append("ObjectiveId", ObjectiveId);
                feedback.append("Feedback", $("#Feedback").val());
                $.each(listRecordings, function (idx, file) {
                    feedback.append("recordings", file, file.name);
                });
                $.ajax({
                    type: "POST",
                    url: "/Assignments/Assignment/InsertStudentObjectiveFeedback",
                    data: feedback,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        listRecordings = pageRecords;
                        $("#myModal").modal('hide');
                        $("ul#recordingslist.multiple-recording").remove();
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            }
            else {
                $('#FeedbackValidation').html('Mandatory field');
            }

        },
        deleteRecording = function (fileId) {
            $.ajax({
                type: 'POST',
                url: '/Assignments/Assignment/DeleteRecording',
                data: { id: fileId },
                success: function (result) {
                    if (result.Success) {
                        globalFunctions.showMessage(result.NotificationType, result.Message);
                        $("#audio_" + fileId).remove();
                    }
                },
                error: function (msg) {

                }
            });
            //source.parentElement.parentElement.removeChild(this.parentElement);
        },
        loadObjectiveGradingTemplateItems = function (source, title, id, selectedGradingTemplateItemId, studentObjectiveId, gradingType) {
            globalFunctions.loadPopup(source, '/Assignments/Assignment/LoadObjectiveGradingTemplateItems?gradingtemplateId=' + id + "&selectedGradingTemplateItemId=" + selectedGradingTemplateItemId + "&studentObjectiveId=" + studentObjectiveId + "&gradingType=" + gradingType, title, 'addtask-dialog');

        },
        loadTaskGradingTemplateItemPopup = function (source, title, id, taskId, studentId, gradingType) {
            globalFunctions.loadPopup(source, '/Assignments/Assignment/LoadTaskGradingTemplateItems?gradingtemplateId=' + id + "&taskId=" + taskId + "&studentId=" + studentId + "&gradingType=" + gradingType, title, 'addtask-dialog');
        },
        loadQuizFeedbackPopup = function (source, title, taskId, studentId, assignmentstudentid, studentTaskId) {
            pageRecords = listRecordings;
            listRecordings = [];
            globalFunctions.loadPopup(source, '/Assignments/Assignment/LoadQuizFeedbackPopup?taskId=' + taskId + "&studentId=" + studentId + "&AssignmentStudentId=" + assignmentstudentid + "&StudentTaskId=" + studentTaskId, title, 'addtask-dialog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                window.AudioContext = window.AudioContext || window.webkitAudioContext;
                navigator.getUserMedia = (
                    navigator.getUserMedia ||
                    navigator.webkitGetUserMedia ||
                    navigator.mozGetUserMedia ||
                    navigator.msGetUserMedia
                );

                if (typeof navigator.mediaDevices.getUserMedia === 'undefined') {
                    navigator.getUserMedia({
                        audio: true
                    }, startUserMedia);
                } else {
                    navigator.mediaDevices.getUserMedia({
                        audio: true
                    }).then(startUserMedia).catch(errorHandler);
                }

                function errorHandler() {
                    console.log("error");
                }

                window.URL = window.URL || window.webkitURL;

                audio_context = new AudioContext();
                //audio_context = (typeof window !== 'undefined' && typeof window.AudioContext !== 'undefined') ? new AudioContext() : null;
                //__log('Audio context set up.');
                //__log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));

            });
        },
        loadObjectiveFeedbackPopup = function (source, title, assignmentstudentid, StudentobjectiveId) {
            pageRecords = listRecordings;
            listRecordings = [];
            globalFunctions.loadPopup(source, '/Assignments/Assignment/LoadObjectiveFeedbackPopup?&AssignmentStudentId=' + assignmentstudentid + '&StudentobjectiveId=' + StudentobjectiveId, title, 'addtask-dialog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                // webkit shim
                window.AudioContext = window.AudioContext || window.webkitAudioContext;
                navigator.getUserMedia = (
                    navigator.getUserMedia ||
                    navigator.webkitGetUserMedia ||
                    navigator.mozGetUserMedia ||
                    navigator.msGetUserMedia
                );

                if (typeof navigator.mediaDevices.getUserMedia === 'undefined') {
                    navigator.getUserMedia({
                        audio: true
                    }, startUserMedia);
                } else {
                    navigator.mediaDevices.getUserMedia({
                        audio: true
                    }).then(startUserMedia).catch(errorHandler);
                }

                function errorHandler() {
                    console.log("error");
                }

                window.URL = window.URL || window.webkitURL;

                audio_context = new AudioContext();
                //audio_context = (typeof window !== 'undefined' && typeof window.AudioContext !== 'undefined') ? new AudioContext() : null;
                //__log('Audio context set up.');
                //__log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));

            });
        },
        assignGradeToStudentTaskInList = function () {
            if ($("#selectedGradingitem").val() != undefined || $("#selectedGradingitem").val() != "0") {
                //Ajax call to update grade 
                $.ajax({
                    type: "GET",
                    url: "/Assignments/Assignment/AssignGradeToStudentTask?gradeId=" + $("#selectedGradingitem").val() + "&studentTaskId=" + $("#entityId").val() + "&gradingType=" + 4,

                    success: function (response) {
                        $("#myModal").modal('hide');
                        $("#gradingDiv_" + $("#entityId").val() + "").show();
                        $("#gradingDiv_" + $("#entityId").val() + "").html('');
                        if (response.GradingTemplateItemSymbol.indexOf('.png') != -1) {
                            // $("#gradingDiv_" + $("#entityId").val() + "").append('<a href="javascript:void(0)" data-toggle="tooltip" title="' + response.ShortLabel + '" data-title="' + response.ShortLabel + '" data-placement="top" class="ml-2"><img class= "img-fluid" style = "height:70%;" src = "' + response.GradingTemplateItemSymbol + '"/></a >')
                            $("#gradingDiv_" + $("#entityId").val() + "").attr("title", response.ShortLabel);
                            $("#gradingDiv_" + $("#entityId").val() + "").attr("data-title", response.ShortLabel);
                            $("#gradingDiv_" + $("#entityId").val() + "").attr("data-title", response.ShortLabel);
                            $("#gradingDiv_" + $("#entityId").val() + "").append('<img class="img-fluid" src="' + response.GradingTemplateItemSymbol + '" />')
                        }
                        else {
                            $("#gradingDiv_" + $("#entityId").val() + "").css("color", response.GradingColor)
                            $("#gradingDiv_" + $("#entityId").val() + "").append(response.ShortLabel);
                        }
                        globalFunctions.showSuccessMessage(translatedResources.ObjectiveGradingMessage);
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            }
            else {
                globalFunctions.showMessage("error", translatedResources.Assign)
            }
        },
        downloadQuizAsPDF = function (source, quizid, resourseid) {
            var resourseType = 'T';
            window.location.href = '/Quiz/Quiz/DownloadQuizAsPDF/?id=' + quizid + '&resourceId=' + resourseid + '&resourceType=' + resourseType;
        },
        loadComments = function () {
            var section = $("#CommentListContainer");
            var assignmentStudentId = section.attr("data-assignmentStudentId");
            $.ajax({
                type: 'GET',
                url: "/Assignments/Assignment/LoadAssignmentComments?assignmentStudentId=" + assignmentStudentId,
                async: false,
                success: function (result) {
                    $("#CommentListContainer").html("");
                    $("#Comment").val("");
                    $("#CommentListContainer").html(result);
                },
                error: function (msg) {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        viewFile = function (source, id) {
            //source.preventDefault();
            if (globalFunctions.isValueValid(id)) {
                window.open("/Document/Viewer/Index?id=" + id + "&module=assignment", "_blank");
            }
        },
        reSubmitAssignment = function (source, studentId, assignmentId) {
            globalFunctions.notyConfirm($(source), translatedResources.reSubmitAssignmentConfirm);
            $(source).bind('okClicked', source, function (e) {
                //Ajax call to ReSubmit Assignment
                $.ajax({
                    type: "POST",
                    url: "/Assignments/Assignment/ReSubmitAssignment?studentId=" + studentId + "&assignmentId=" + assignmentId + "",
                    success: function (response) {
                        globalFunctions.showSuccessMessage(translatedResources.AssignmentReSubmitted);
                        location.reload(true);
                    }
                });
            });
        },
        assignGradeToStudentObjectiveInList = function () {
            if ($("#selectedGradingitem").val() != undefined || $("#selectedGradingitem").val() != "0") {
                //Ajax call to update grade 
                $.ajax({
                    type: "GET",
                    url: "/Assignments/Assignment/AssignGradeToStudentObjective?gradeId=" + $("#selectedGradingitem").val() + "&studentObjectiveId=" + $("#entityId").val() + "&gradingType=" + 4,

                    success: function (response) {
                        $("#myModal").modal('hide');
                        $("#gradingDiv_" + $("#entityId").val() + "").show();
                        $("#gradingDiv_" + $("#entityId").val() + "").html('');
                        if (response.GradingTemplateItemSymbol.indexOf('.png') != -1) {
                            // $("#gradingDiv_" + $("#entityId").val() + "").append('<a href="javascript:void(0)" data-toggle="tooltip" title="' + response.ShortLabel + '" data-title="' + response.ShortLabel + '" data-placement="top" class="ml-2"><img class= "img-fluid" style = "height:70%;" src = "' + response.GradingTemplateItemSymbol + '"/></a >')
                            $("#gradingDiv_" + $("#entityId").val() + "").attr("title", response.ShortLabel);
                            $("#gradingDiv_" + $("#entityId").val() + "").attr("data-title", response.ShortLabel);
                            $("#gradingDiv_" + $("#entityId").val() + "").attr("data-title", response.ShortLabel);
                            $("#gradingDiv_" + $("#entityId").val() + "").append('<img class="img-fluid" src="' + response.GradingTemplateItemSymbol + '" />')

                        }
                        else {
                            $("#gradingDiv_" + $("#entityId").val() + "").css("color", response.GradingColor)
                            $("#gradingDiv_" + $("#entityId").val() + "").append(response.ShortLabel);
                        }
                        $("#btnAssignGradeToObjective_" + $("#entityId").val() + "").attr("data-gradingItem", $("#selectedGradingitem").val());
                        globalFunctions.showSuccessMessage(translatedResources.TaskGradingMessage);
                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            }
            else {
                globalFunctions.showMessage("error", translatedResources.Assign)
            }
        },
        SaveAssignmentSubmitMarksPopup = function (source, assignmentStudentId, submitMarks, AssignmentTotalMarks) {
            assignmentStudentDetails.loadAssignmentSubmitMarksPopup(source, assignmentStudentId, submitMarks, AssignmentTotalMarks);
        },
        loadAssignmentSubmitMarksPopup = function (source, assignmentStudentId, submitMarks, AssignmentTotalMarks) {
            var title = 'Submit ' + translatedResources.AssignmentMarks;
            globalFunctions.loadPopup(source, '/Assignments/Assignment/InitSaveAssignmentMarks?assignmentStudentId=' + assignmentStudentId + '&=submitMarks=' + submitMarks + '&AssignmentTotakMarks=' + AssignmentTotalMarks, title, 'suggestion-dailog');
            $(source).off("modalLoaded");
        };

    return {
        init: init,
        loadGradingTemplateItemPopup: loadGradingTemplateItemPopup,
        assignGradeToStudentAssignment: assignGradeToStudentAssignment,
        loadComments: loadComments,
        deleteRecording: deleteRecording,
        assignFeedBackToTask: assignFeedBackToTask,
        loadQuizFeedbackPopup: loadQuizFeedbackPopup,
        loadTaskGradingTemplateItemPopup: loadTaskGradingTemplateItemPopup,
        assignGradeToStudentTaskInList: assignGradeToStudentTaskInList,
        assignGradeToStudentAssignmentInList: assignGradeToStudentAssignmentInList,
        viewFile: viewFile,
        loadTaskGradingTemplateItemPopup: loadTaskGradingTemplateItemPopup,
        loadObjectiveGradingTemplateItems: loadObjectiveGradingTemplateItems,
        assignGradeToStudentObjectiveInList: assignGradeToStudentObjectiveInList,
        assignFeedBackToObjective: assignFeedBackToObjective,
        loadObjectiveFeedbackPopup: loadObjectiveFeedbackPopup,
        reSubmitAssignment: reSubmitAssignment,
        downloadQuizAsPDF: downloadQuizAsPDF,
        SaveAssignmentSubmitMarksPopup: SaveAssignmentSubmitMarksPopup,
        loadAssignmentSubmitMarksPopup: loadAssignmentSubmitMarksPopup
    };
}();

$(document).ready(function () {
    //assignment details page
    assignmentStudentDetails.init();

    $('[data-toggle="tooltip"]').tooltip()


    let assigneeTable = $('#assignment-assignee-table').DataTable({
        ordering: false,
        searching: false,
        bInfo: false,
        bLengthChange: false,
        autoWidth: false,
        paging: false,
        /* columnDefs: [
            { "width": "4%", "targets": 0 },
            { "width": "71%", "targets": 1 },
            { "width": "25%", "targets": 2 }
        ], */
        columnDefs: [{
            orderable: false,
            targets: 0
        },
        {
            target: 1,
            width: '40%'
        }],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    assigneeTable.on("click", "th.select-checkbox", function () {
        if ($("th.select-checkbox").hasClass("selected")) {
            assigneeTable.rows().deselect();
            $("th.select-checkbox").removeClass("selected");
        } else {
            assigneeTable.rows().select();
            $("th.select-checkbox").addClass("selected");
        }
    }).on("select deselect", function () {
        (translatedResources.SelectOrDeselect)
        if (assigneeTable.rows({
            selected: true
        }).count() !== assigneeTable.rows().count()) {
            $("th.select-checkbox").removeClass("selected");
        } else {
            $("th.select-checkbox").addClass("selected");
        }
    });


    $('input[type="file"]').fileinput({
        language: translatedResources.locale,
        title: translatedResources.BrowseFile,
        multipleText: translatedResources.TotalFiles,
        showMultipleNames: false,
        buttonClass: 'custom-upload',
        previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
    });

    //assignment index page
    $('.time-picker').datetimepicker({
        format: 'LT',
        //debug: true
    });
    $('.date-picker').datetimepicker({
        format: 'MM/DD/YYYY',
        //debug: true
    });
    var dt = new Date();
    var cDate = ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear();
    var cTime = dt.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    $('.date-picker').val(cDate);
    $('.time-picker').val(cTime);

    $("#addAssignment").click(function () {
        window.location.href = "/Assignments/Assignment/InitAddEditAassignmentForm";
    });
    $("#btnMarkInComplete").click(function (source) {
        var studentId = $(this).attr("data-studentId");
        var assignmentId = $(this).attr("data-AsgId");
        globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
        $(source).unbind('okClicked');
        $(source).bind('okClicked', source, function (e) {
            $.ajax({
                type: "GET",
                url: "/Assignments/Assignment/MarkAsIncompleteAssignment",
                data: { studentId: studentId, assignmentId: assignmentId },//parent id
                success: function (response) {

                    globalFunctions.showMessage(true, translatedResources.DeleteTaskSuccess);
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        });

    });
    $("#lnkMarkCompleted").click(function () {
        var assignmentStudentId = $(this).attr("data-student-assignment-id");
        var AssignmentTotalMarks = $("#hdnAssinmentMarks").val();
        var IstaskMarkExits = parseInt($("#hdnIstaskMarkExits").val());
        if (AssignmentTotalMarks != 0 && IstaskMarkExits == 0) {
            var assignmentStudentId = $(this).attr("data-student-assignment-id");
            var submitmarks = $('#txtAssignmentSubmitMarks').val();
            assignmentStudentDetails.SaveAssignmentSubmitMarksPopup($(this), assignmentStudentId, submitmarks, AssignmentTotalMarks);
        } else {
            MarkAsCompleted(assignmentStudentId);
        }

    });

    $("#btnSubmitAssignmentMarks").click(function () {
        if ($('#txtAssignmentSubmitMarks').val() != '') {
            var assignmentStudentId = $(this).attr("data-student-assignment-id");
            var submitmarks = Math.round(parseFloat($('#txtAssignmentSubmitMarks').val()).toFixed(2));
            var assignmentTotalMarks = Math.round(parseFloat($("#hdnAssinmentMarks").val()).toFixed(2));
            var gradingTemplateId = $("#GradingTemplateId").val() != '' ? $("#GradingTemplateId").val() : 0;
            if (submitmarks <= assignmentTotalMarks && submitmarks >= Math.round(parseFloat(0).toFixed(2))) {
                var txtMarks = parseFloat($('#txtAssignmentSubmitMarks').val()).toFixed(2);
                SubmitAssignmentMarks(assignmentStudentId, txtMarks, gradingTemplateId);
                MarkAsCompleted(assignmentStudentId);
            } else {
                globalFunctions.showErrorMessage(translatedResources.MarksValidationError);
            }
        } else {
            globalFunctions.showErrorMessage(translatedResources.txtMarksErrorMessage);
        }
    });

    function MarkAsCompleted(id, title, studentId) {
        var gradingId = $("#GradingTemplateId").val();
        var assignmentMarks = parseInt($("#hdnAssinmentMarks").val());
        var IstaskMarkExits = parseInt($("#hdnIstaskMarkExits").val());
        $.ajax({
            url: '/Assignment/MarkAsComplete?AssignmentStudentId=' + id,
            type: 'GET',  // http method
            success: function (data, status, xhr) {
                if (data) {
                    $("#lnkMarkCompleted").hide("");
                    $("#lnkCompleted").show();
                    if (gradingId > 0 && IstaskMarkExits != 0) {
                        $("#lnkClickToGrade").show();
                        $("#lnkClickToGrade").attr("data-iscompleted", "True");
                        $(".btnAssignGrade").attr("data-iscompleted", "True");
                    }
                    else if (gradingId > 0 && assignmentMarks == 0) {
                        $("#lnkClickToGrade").show();
                        $("#lnkClickToGrade").attr("data-iscompleted", "True");
                        $(".btnAssignGrade").attr("data-iscompleted", "True");
                    } else if (assignmentMarks != 0) {
                        $("#lnkClickToEditMarks").show();
                        $("#lnkClickToEditMarks").attr("data-iscompleted", "True");
                        $(".btnAssignGrade").attr("data-iscompleted", "True");
                    }

                }
                else {
                    $(this).show();
                    $("#lnkCompleted").hide();
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {

            }
        });
    }

    function SubmitAssignmentMarks(id, marks, gradingTemplateId) {
        $.ajax({
            url: '/Assignment/SubmitAssignmentMarks?AssignmentStudentId=' + id + '&submitMarks=' + marks + '&gradingTemplateId=' + gradingTemplateId,
            type: 'POST',  // http method
            success: function (response, status, xhr) {
                if (response.ShortLabel != null) {
                    $("#myModal").modal('hide');
                    $("#gradingDiv").show();
                    $("#gradingDiv").html('');
                    if (response.GradingTemplateItemSymbol.indexOf('.png') != -1) {

                        $("#gradingDiv").append('<a href="javascript:void(0)" data-toggle="tooltip" title="' + response.ShortLabel + '" data-title="' + response.ShortLabel + '" data-placement="top" class="ml-2"><img class= "img-fluid" style = "height:70%;" src = "' + response.GradingTemplateItemSymbol + '"/></a >')
                    }
                    else {
                        $("#gradingDiv").addClass("txt-grade mt-n2")
                        $("#gradingDiv").append('<h2 class="m-0 text-bold" style="color:' + response.GradingColor + '">' + response.ShortLabel + '</h2>');
                    }
                    globalFunctions.showSuccessMessage(translatedResources.AssignmentGradingMessage);
                }
                else {
                    $("#myModal").modal('hide');
                    $("#lnkClickToEditMarks").show();
                    $("#lnkClickToEditMarks").attr("data-iscompleted", "True");
                    $(".btnAssignGrade").attr("data-iscompleted", "True");
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {

            }
        });
    }
    $("#divanchStdDownloadFile a").on('click', function (e) {
        var FId = $(this).attr('data-fid');
        window.open("/Assignments/Assignment/DownloadFile?studentId=" + $("#StudentId").val() + "&assignmentId=" + $("#AssignmentId").val() + "&FId=" + FId, "_blank");

    });

    $(".btnAssignGrade").click(function () {
        var gradingTemplateId = parseInt($(this).attr("data-gradingId"));
        var studAsgId = parseInt($(this).attr("data-studentAsgId"));
        var isCompleted = $(this).attr("data-IsCompleted");
        var AssignmentTotalMarks = $("#hdnAssinmentMarks").val();
        var IstaskMarkExits = parseInt($("#hdnIstaskMarkExits").val());
        var submitMarks = '';

        if (isCompleted == "True" && IstaskMarkExits != 0) {
            assignmentStudentDetails.loadGradingTemplateItemPopup($(this), translatedResources.Assign, gradingTemplateId, studAsgId, 2);
        }
        else if (isCompleted == "True" && AssignmentTotalMarks == 0) {
            assignmentStudentDetails.loadGradingTemplateItemPopup($(this), translatedResources.Assign, gradingTemplateId, studAsgId, 2);
        }
        else if (isCompleted == "True" && AssignmentTotalMarks != 0) {
            assignmentStudentDetails.loadAssignmentSubmitMarksPopup($(this), studAsgId, submitMarks, AssignmentTotalMarks);
        }
        else {
            globalFunctions.showMessage("error", translatedResources.AssignmentMarkComplete);
        }
    });
    $(".btnAssignGradeToTask").click(function () {
        var gradingTemplateId = parseInt($(this).attr("data-gradingId"));
        var studentId = parseInt($(this).attr("data-studentId"));
        var taskId = parseInt($(this).attr("data-taskId"));
        var isCompleted = $(this).attr("data-IsCompleted");

        if (isCompleted == "True") {
            assignmentStudentDetails.loadTaskGradingTemplateItemPopup($(this), translatedResources.Assign, gradingTemplateId, taskId, studentId, 3);
        }
        else {
            globalFunctions.showMessage("error", translatedResources.SelectGradeFirst);
        }

    });
    $(document).on('click', '#btnQuizAsPdf', function () {
        debugger;
        var isQuestionExist = $(this).data("isquestionexist");
        if (isQuestionExist == 'True') {
            var quizId = $(this).data("quizid");
            var resourseId = $(this).data("taskid");
            assignmentStudentDetails.downloadQuizAsPDF($(this), quizId, resourseId);
        } else {
            globalFunctions.showErrorMessage(translatedResources.NoDownloadQuestion);
        }
    });
    $(".btnAssignGradeToObjective").click(function () {
        var gradingTemplateId = parseInt($(this).attr("data-gradingId"));
        var StudentObjectiveMarkId = parseInt($(this).attr("data-studentobjectiveId"));
        var isCompleted = $(this).attr("data-IsCompleted");
        var gradingItem = $(this).attr("data-gradingItem");
        assignmentStudentDetails.loadObjectiveGradingTemplateItems($(this), translatedResources.AssignGradeToObjective, gradingTemplateId, gradingItem, StudentObjectiveMarkId, 5);

    });
    $(".btnAssignFeedbackToTask").click(function () {
        var studentId = parseInt($(this).attr("data-studentId"));
        var taskId = parseInt($(this).attr("data-taskId"));
        var assignmentstudentid = parseInt($(this).attr("data-assignmentststudentid"));
        var studentTaskId = parseInt($(this).attr("data-studenttaskid"));
        assignmentStudentDetails.loadQuizFeedbackPopup($(this), translatedResources.Feedback, taskId, studentId, assignmentstudentid, studentTaskId);
    });

    $(".btnAssignFeedbackToStudentObjective").click(function () {
        var assignmentstudentid = parseInt($(this).attr("data-assignmentststudentid"));
        var StudentobjectiveId = parseInt($(this).attr("data-studentobjectiveId"));
        assignmentStudentDetails.loadObjectiveFeedbackPopup($(this), translatedResources.Feedback, assignmentstudentid, StudentobjectiveId);
    });


    $(".btnAssignGradeToStudentInList").click(function () {
        var gradingTemplateId = parseInt($(this).attr("data-gradingId"));
        var studAsgId = parseInt($(this).attr("data-studentAsgId"));
        var isCompleted = $(this).attr("data-IsCompleted");

        if (isCompleted == "True") {
            assignmentStudentDetails.loadGradingTemplateItemPopup($(this), translatedResources.Assign, gradingTemplateId, studAsgId, 1);
        }
        else {
            globalFunctions.showMessage("error", translatedResources.AssignmentMarkComplete);
        }
    });
    $("#btnStudAttachment").click(function () {
        var studentList = [];
        var assignmentList = [];
        var assignmentId = $('#AssignmentId').val();
        $.each($('input:checkbox.chkStudentAssignments:checked'), function () {
            var row = $(this).closest('tr');
            studentList.push({
                StudentId: row.data('studid'),
                StudentName: row.data('studname') //row.find('span.stdName').text().trim()
            });
            if (row.data('attachmentcount') > 0) {
                assignmentList.push({
                    AttachmentId: row.data('attachmentcount')
                });
            }
        });
        if ($(studentList).length > 0) {
            if ($(assignmentList).length > 0) {
                window.location.href = "/Assignments/Assignment/DownloadStudentAttachment?AssignmentId=" + assignmentId + "&strStudentList=" + JSON.stringify(studentList);
            }
            else {
                globalFunctions.showErrorMessage(translatedResources.SelectAttachmentMessage)
            }
        }
        else {
            globalFunctions.showErrorMessage(translatedResources.SelectMemberMessage)
        }
    });

    $(document).on('click', '#ViewFile', function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        assignmentStudentDetails.viewFile($(this), id);
    });

    $(document).on('click', '#ahrefDeleteFiles', function () {
        var fileName = $(this).attr('data-fileName');
        var uploadedfilepath = $(this).attr('data-uploadedPath');
        var fileId = $(this).attr('data-FileId');
        var assignmentId = $(this).attr('data-assignmentId');
        globalFunctions.notyDeleteConfirm($(this), translatedResources.deleteConfirm);
        $(document).bind('okToDelete', $(this), function (e) {

            $.post('/Assignments/Assignment/DeleteFilesFromSession', { fileId: fileId, AssignmentId: assignmentId, fileName: fileName, UploadedFilePath: uploadedfilepath }, function (response) {
                $('#fileList').html(response);
            });
        });
    });
    $(".btnReviewStudentHomework").click(function () {
        var studentAssignmentId = $(this).attr("data-studasgid");
        var encPeerReviewId = $(this).attr("data-peerreviewid");
        var isAlreadyReviewed = ($(this).attr("data-isreviewcompleted") === 'True');
        var assignmentId = $(this).attr("data-assignmentId");
        //if (!isAlreadyReviewed) {
        location.href = "/Assignments/Assignment/StudentHomeworkReview?id=" + studentAssignmentId + "&encPeerReviewId=" + encPeerReviewId + "&redirectionId=" + assignmentId;
        //}
    });
    $(document).on('click', '#btnDeleteRecordedFile', function (e) {

        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);

        var fileId = $(this).attr('data-fileId');
        $(document).bind('okClicked', this, function (e) {
            assignmentStudentDetails.deleteRecording(fileId);
        });

    });
    $(document).on("keypress", "#txtAssignmentSubmitMarks", function (evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57)))
            return false;
        return true;
    });
});
