﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

//var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var taskList = [];
var taskOrder = 0;


var StudentAssignmentDashboard = function () {
    var init = function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('#tblAssignmentDetails').DataTable({
            ordering: false,
            searching: false,
            bInfo: false,
            bLengthChange: false,
            autoWidth: false,
            paging: false,
            /* columnDefs: [
                { "width": "4%", "targets": 0 },
                { "width": "71%", "targets": 1 },
                { "width": "25%", "targets": 2 }
            ], */
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            }
        });


    },

        PaggingTemplate = function PaggingTemplate(totalPage, currentPage) {
            var template = "";
            var TotalPages = totalPage;
            var CurrentPage = currentPage;
            var PageNumberArray = Array();


            var countIncr = 1;
            for (var i = currentPage; i <= totalPage; i++) {
                PageNumberArray[0] = currentPage;
                if (totalPage != currentPage && PageNumberArray[countIncr - 1] != totalPage) {
                    PageNumberArray[countIncr] = i + 1;
                }
                countIncr++;
            };
            PageNumberArray = PageNumberArray.slice(0, 5);
            var FirstPage = 1;
            var LastPage = totalPage;
            if (totalPage != currentPage) {
                var ForwardOne = currentPage + 1;
            }
            var BackwardOne = 1;
            if (currentPage > 1) {
                BackwardOne = currentPage - 1;
            }

            template = "<p>" + translatedResources.ShowingPages.replace('{0}', CurrentPage).replace('{1}', TotalPages) + "</p>"
            template = template + ' <nav aria-label="Page navigation"><ul class="pagination pg-blue">' +
                '<li class="page-item"><a class="page-link" href="#" onclick="StudentAssignmentDashboard.GetPageData(' + FirstPage + ')">' + translatedResources.Previous + '</a></li>';


            var numberingLoop = "";
            for (var i = 0; i < PageNumberArray.length; i++) {
                numberingLoop = numberingLoop + '<li><a class="page-link active" onclick="StudentAssignmentDashboard.GetPageData(' + PageNumberArray[i] + ')" href="#">' + PageNumberArray[i] + '</a> <span class="sr-only">(current)</span></li>'
            }
            template = template + numberingLoop +
                '<li class="page-item"><a  class="page-link" href="#" onclick="StudentAssignmentDashboard.GetPageData(' + LastPage + ')">' + translatedResourcesNext + '</a></li></ul></nav>';
            $("#paged").append(template);
            $('[data-toggle="tooltip"]').tooltip();

        },

        loadStudentAssignments = function () {
            $(window).off('scroll');
            $('#StudentAssignment').scrollPagination({
                'url': '/Assignments/Assignment/LoadAssignment', // The url you are fetching the results.               
                'data': {
                    // These are the variables you can pass to the request
                    'page': 1, // Which page at the firsttime
                    'size': 15, // Number of pages
                    'searchString': $("#txtAssignmentSearch").val(),
                    'sortBy': $("#ddlAssignmentSort").val(),
                    'assignmentType': $("#hdnAssignmentType").val(),
                    'filterVal': $("#SchoolGroupIds").val().join([separator = ','])
                },
                'scroller': $(window), // Who gonna scroll? default is the full window
                'autoload': true, // Change this to false if you want to load manually, default true.
                'heightOffset': 0, // It gonna request when scroll is 10 pixels before the page ends
                'loading': "#loading", // ID of loading prompt.
                'loadingText': 'click to loading more.', // Text of loading prompt.
                'loadingNomoreText': 'No more.', // No more of loading prompt.
                'manuallyText': 'click to loading more.', // Click of loading prompt.
                'before': function () {
                    // Before load function, you can display a preloader div
                    // example:
                    $(this.loading).fadeIn();
                },
                'after': function (elementsLoaded) {
                    // After loading content, you can use this function to animate your new elements
                    // example:
                    $(this.loading).fadeOut();
                    $(elementsLoaded).fadeInWithDelay();
                }
            });
        },

        loadStudentAssignmentPages = function () {
            var data = {
                page: 1,
                size: 15,
                assignmentType : $("#hdnAssignmentType").val(),
                searchString : $("#txtAssignmentSearch").val(),
                CreatedByMeOnly : false,
                sortBy: $("#ddlAssignmentSort").val(),
                filterVal: $("#SchoolGroupIds").val().join([separator = ','])
            };
            $(window).off('scroll');
            var obj = $("#StudentAssignment").loadMore({
                loadType: 'click',
                conLocation: '#StudentAssignment',
                url: '/Assignments/Assignment/LoadAssignment',
                dataType: 'json',
                data: {
                    'page': data.page, // Which page at the firsttime
                    'size': data.size, // Number of pages
                    'searchString': data.searchString,
                    'sortBy': data.sortBy,
                    'assignmentType': data.assignmentType,
                    'filterVal': data.filterVal
                },

                success: function (res) {
                    data.page += 1;
                    obj.updatePram('data', data);
                    render(res.content);
                    if (res.isContentFinished == true) {
                        //$("._loadMore-click").addClass("d-none");
                        $("#loadmore").addClass("d-none");
                    }
                }
            });

        },

        render = function (data) {
            $.each(data, function (key, value) {
                $("#StudentAssignment").append(value);
            });
        };

    return {
        init: init,
        loadStudentAssignments: loadStudentAssignments,
        PaggingTemplate: PaggingTemplate,
        loadStudentAssignmentPages: loadStudentAssignmentPages

    };
}();

$(document).ready(function () {
  
    StudentAssignmentDashboard.loadStudentAssignmentPages();
    //$("._loadMore-click").trigger("click");
    $("#loadmore").trigger("click");
    $('.time-picker').datetimepicker({
        format: 'LT'
    });
    $('.date-picker').datetimepicker({
        format: 'MM/DD/YYYY'
    });

    localStorage.removeItem("backstudentpagerefer");
    var dt = new Date();
    var cDate = ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear();
    var cTime = dt.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    $('.date-picker').val(cDate);
    $('.time-picker').val(cTime);

    $('input[type="file"]').fileinput({
        language: translatedResources.locale,
        title: translatedResources.BrowseFile,
        multipleText: translatedResources.TotalFiles,
        showMultipleNames: false,
        buttonClass: 'custom-upload',
        previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,

    });

    $("#grid").addClass('active');
    $('#list').click(function (event) {
        event.preventDefault();
        $('#library .item').addClass('list-group-item');
        $(this).toggleClass('active');
        $("#grid").removeClass('active');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#library .item').removeClass('list-group-item');
        $('#library .item').addClass('grid-group-item');
        $(this).toggleClass('active');
        $("#list").removeClass('active');
    });

    $(document).on("change", '#ddlAssignmentSort', function () {
        var activeTab = $("#btnLoadActiveAssignment").attr('class').match('active');
        if (activeTab != null) {
            $("#StudentAssignment").html(''); StudentAssignmentDashboard.loadStudentAssignmentPages();
            //$("._loadMore-click").trigger("click");
            $("#loadmore").trigger("click");
        } else {
            $("#ArchiveAssignments").html('');
            archivedAssignmentDashboard.loadTeacherArchivedAssignmentPages();
            $("#loadmore1").trigger("click");
        }
    });

    $(document).on("keyup", "#txtAssignmentSearch", function (e) {
        if (e.which == 13) {
            var activeTab = $("#btnLoadActiveAssignment").attr('class').match('active');
            if (activeTab != null) {
                $("#StudentAssignment").html(''); StudentAssignmentDashboard.loadStudentAssignmentPages();
                //$("._loadMore-click").trigger("click");
                $("#loadmore").trigger("click");
            } else {
                $("#ArchiveAssignments").html('');
                archivedAssignmentDashboard.loadTeacherArchivedAssignmentPages();
                $("#loadmore1").trigger("click");
            }
        };
    });

    $(document).on('change', '#SchoolGroupIds', function () {
        debugger;
        var activeTab = $("#btnLoadActiveAssignment").attr('class').match('active');
        if (activeTab != null) {
            $("#StudentAssignment").html(''); StudentAssignmentDashboard.loadStudentAssignmentPages();
            //$("._loadMore-click").trigger("click");
            $("#loadmore").trigger("click");
        } else {
            $("#ArchiveAssignments").html('');
            archivedAssignmentDashboard.loadTeacherArchivedAssignmentPages();
            $("#loadmore1").trigger("click");
        }
    });

});
