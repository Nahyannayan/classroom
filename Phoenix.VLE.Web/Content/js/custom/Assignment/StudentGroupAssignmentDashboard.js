﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

//var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var taskList = [];
var taskOrder = 0;
var StudentAssignmentDashboard = function () {
    var init = function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('#tblAssignmentDetails').DataTable({
            ordering: false,
            searching: false,
            bInfo: false,
            bLengthChange: false,
            autoWidth: false,
            paging: false,
            /* columnDefs: [
                { "width": "4%", "targets": 0 },
                { "width": "71%", "targets": 1 },
                { "width": "25%", "targets": 2 }
            ], */
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            }
        });


    },
        //PostAsyncData = function GetPageData(pageNum) {         
        //    //After every trigger remove previous data and paging
        //    $("#tblData").empty();
        //    $("#paged").empty();
        //    $.getJSON("/Assignments/Assignment/Index", { pageIndex: pageNum });
        //},
        //This is paging temlpate ,you should just copy paste
        PaggingTemplate = function PaggingTemplate(totalPage, currentPage) {
            var template = "";
            var TotalPages = totalPage;
            var CurrentPage = currentPage;
            var PageNumberArray = Array();


            var countIncr = 1;
            for (var i = currentPage; i <= totalPage; i++) {
                PageNumberArray[0] = currentPage;
                if (totalPage != currentPage && PageNumberArray[countIncr - 1] != totalPage) {
                    PageNumberArray[countIncr] = i + 1;
                }
                countIncr++;
            };
            PageNumberArray = PageNumberArray.slice(0, 5);
            var FirstPage = 1;
            var LastPage = totalPage;
            if (totalPage != currentPage) {
                var ForwardOne = currentPage + 1;
            }
            var BackwardOne = 1;
            if (currentPage > 1) {
                BackwardOne = currentPage - 1;
            }

            template = "<p>" + translatedResources.ShowingPages.replace('{0}', CurrentPage).replace('{1}', TotalPages) + "</p>"
            template = template + ' <nav aria-label="Page navigation"><ul class="pagination pg-blue">' +
                '<li class="page-item"><a class="page-link" href="#" onclick="StudentAssignmentDashboard.GetPageData(' + FirstPage + ')">' + translatedResources.Previous + '</a></li>';

            //'<li class="page-item"><a class="page-link" href="#" onclick="GetPageData(' + BackwardOne + ')"></a></li>';

            var numberingLoop = "";
            for (var i = 0; i < PageNumberArray.length; i++) {
                numberingLoop = numberingLoop + '<li><a class="page-link active" onclick="StudentAssignmentDashboard.GetPageData(' + PageNumberArray[i] + ')" href="#">' + PageNumberArray[i] + '</a> <span class="sr-only">(current)</span></li>'
            }
            template = template + numberingLoop +
                '<li class="page-item"><a  class="page-link" href="#" onclick="StudentAssignmentDashboard.GetPageData(' + LastPage + ')">' + translatedResourcesNext + '</a></li></ul></nav>';
            $("#paged").append(template);
            //$('#selectedId').change(function () {
            //    GetPageData(1, $(this).val());
            //});
            $('[data-toggle="tooltip"]').tooltip();

        }

    return {
        init: init,
        //PostAsyncData: PostAsyncData,
        //GetPageData: GetPageData,
        PaggingTemplate: PaggingTemplate

    };
}();

$(document).ready(function () {

    initPagination();

    //StudentAssignmentDashboard.GetPageData(1,6);
    $('.time-picker').datetimepicker({
        format: 'LT',
        //debug: true
    });
    $('.date-picker').datetimepicker({
        format: 'MM/DD/YYYY',
        //debug: true
    });

    //StudentAssignmentDashboard.init();
    //StudentAssignmentDashboard.GetPageData(1);
    var dt = new Date();
    var cDate = ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear();
    var cTime = dt.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    $('.date-picker').val(cDate);
    $('.time-picker').val(cTime);

    $('input[type="file"]').fileinput({
        language: translatedResources.locale,
        title: translatedResources.BrowseFile,
        multipleText: translatedResources.TotalFiles,
        showMultipleNames: false,
        buttonClass: 'custom-upload',
        previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,

    });

    //---- grid and list view toggle
    $("#grid").addClass('active');
    $('#list').click(function (event) {
        event.preventDefault();
        $('#library .item').addClass('list-group-item');
        $(this).toggleClass('active');
        $("#grid").removeClass('active');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#library .item').removeClass('list-group-item');
        $('#library .item').addClass('grid-group-item');
        $(this).toggleClass('active');
        $("#list").removeClass('active');
    });

    $(document).on("keyup", "#searchContent", function (e) {
        if (e.which == 13)
            initPagination();
    });

});

function initPagination() {
    var grid = new DynamicPagination("StudentGroupAssignments");
    var searchString = globalFunctions.isValueValid($("#searchContent").val()) ? $("#searchContent").val() : "";
    var settings = {
        url: '/Assignments/Assignment/LoadStudentGroupAssignments/?pageIndex=1&searchString=' + searchString
    };
    grid.init(settings);
}