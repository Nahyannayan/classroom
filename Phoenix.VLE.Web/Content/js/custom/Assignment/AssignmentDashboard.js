﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var taskList = [];
var taskOrder = 0;
var activePageNo = 0;
var assignmentDashboard = function () {
    var init = function () {

    },

        deleteAssignment = function (source, assignmentId) {
            globalFunctions.notyDeleteConfirm($(source), translatedResources.deleteConfirm);
            $(document).bind('okToDelete', $(source), function (e) {
                $.ajax({
                    type: 'GET',
                    url: '/Assignments/Assignment/DeleteAssignment?assignmentId=' + assignmentId,
                    async: false,
                    contentType: 'application/json',
                    success: function (data) {
                        if (data.Success) {
                            globalFunctions.showSuccessMessage(translatedResources.SuccessDelete);
                            location.reload(true);
                        }
                        else {
                            globalFunctions.showErrorMessage(translatedResources.ErrorDelete);
                        }
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure(data, xhr, status);
                    }
                });
            });
        },
        initAddEditAassignmentForm = function (e) {
            var AssignmentId = $(e).attr("data-id");
            window.location.href = "/Assignments/Assignment/AddEditAssignment?id=" + AssignmentId;
        },
        cloneAssignment = function (e) {
            var AssignmentId = $(e).data("id");
            window.location.href = "/Assignments/Assignment/AddEditAssignment?id=" + AssignmentId + "&isCloned=" + true;
        },
        loadAssignmentDtails = function (AssignmentId) {
            window.location.href = "/Assignments/Assignment/InitAddEditAassignmentForm?id=" + AssignmentId;
        },
        shareAssignmentWithOthers = function (source, id) {
            var title = translatedResources.SelectTeacherList;
            globalFunctions.loadPopup(source, '/Assignments/Assignment/GetTeacherListToShareAssignment?AssignmentId=' + id, title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: translatedResources.PleaseSelect
                });
            });
        },
        postTeachersToShare = function () {
            var selectedTeachers = [];
            selectedTeachers = $("#selectteachers").val();
            var data = [];
            if (selectedTeachers.length > 0) {
                //$("#showErrorMsg").hide();
                $.post('/Assignments/Assignment/postTeachersToShare?AssignmentId=' + $("#assignmentId").val(), {
                    selectedTeachers: JSON.stringify(selectedTeachers)
                }, function (data) {
                    if (data.Success) {
                        $("#myModal").modal("hide");
                        globalFunctions.showSuccessMessage(translatedResources.SharedSuccess);
                    }
                }).fail(function () { globalFunctions.onFailure(); });
            } else {
                //$("#showErrorMsg").show();
                globalFunctions.showErrorMessage(translatedResources.SelectTeacherError);
            }
        },
        loadTeacherAssignmentPages = function () {
      
            var groupIds = $("#SchoolGroupIds").val();
            if ($("#ddlteachersDropdownLive").val() === undefined) {
                var teacherIds = [];
            } else {
                var teacherIds = $("#ddlteachersDropdownLive").val();
            }
            
            var data = {
                page: 1,
                size: 15,
                assignmentType: $("#hdnAssignmentType").val(),
                searchString: $("#txtAssignmentSearch").val(),
                CreatedByMeOnly: false,
                sortBy: $("#ddlAssignmentSort").val(),
                filterVal: groupIds.toString(),
                teacherVal: teacherIds.toString()
            };
            $(window).off('scroll');
            var obj = $("#TeacherAssignment").loadMore({
                loadType: 'click',
                conLocation: '#activeAssignmentpages',
                type: "POST",
                url: '/Assignments/Assignment/LoadAssignment',
                dataType: 'json',
                data: data,
                //{
                //    'page': data.page, // Which page at the firsttime
                //    'size': data.size, // Number of pages
                //    'searchString': data.searchString,
                //    'sortBy': data.sortBy,
                //    'assignmentType': data.assignmentType,
                //    'filterVal': data.filterVal,
                //    'teacherVal': data.teacherVal
                //},

                success: function (res) {
                    data.page += 1;
                    obj.updatePram('data', data);
                    render(res.content);
                    if (res.isContentFinished == true) {
                        $("#loadmore").addClass("d-none");
                    }
                }
            });

        },


        render = function (data) {
            $.each(data, function (key, value) {
                $("#activeAssignmentpages").append(value);
            });
        }
    return {
        init: init,
        initAddEditAassignmentForm: initAddEditAassignmentForm,
        cloneAssignment: cloneAssignment,
        deleteAssignment: deleteAssignment,
        loadAssignmentDtails: loadAssignmentDtails,
        shareAssignmentWithOthers: shareAssignmentWithOthers,
        postTeachersToShare: postTeachersToShare,
        loadTeacherAssignmentPages: loadTeacherAssignmentPages
    };
}();

$(document).ready(function () {
    assignmentDashboard.loadTeacherAssignmentPages();
    $("#loadmore").trigger("click");
    $('[data-toggle="tooltip"]').tooltip({
        html: true
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('input[type="file"]').fileinput({
        language: translatedResources.locale,
        title: translatedResources.BrowseFile,
        multipleText: translatedResources.TotalFiles,
        showMultipleNames: false,
        buttonClass: 'custom-upload',
    });

    //assignment index page
    $('.time-picker').datetimepicker({
        format: 'LT',
        //debug: true
    });
    $('.date-picker').datetimepicker({
        format: 'MM/DD/YYYY',
        //debug: true
    });
    var dt = new Date();
    var cDate = ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear();
    var cTime = dt.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    $('.date-picker').val(cDate);
    $('.time-picker').val(cTime);



    //---- grid and list view toggle
    $("#grid").addClass('active');
    $('#list').click(function (event) {
        event.preventDefault();
        $('#library .item').addClass('list-group-item');
        $(this).toggleClass('active');
        $("#grid").removeClass('active');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#library .item').removeClass('list-group-item');
        $('#library .item').addClass('grid-group-item');
        $(this).toggleClass('active');
        $("#list").removeClass('active');
    });

    $("#addAssignment").click(function () {
        window.location.href = "/Assignments/Assignment/InitAddEditAassignmentForm";
    });
    $('[data-toggle="tooltip"]').tooltip();
    $("#CreatedByMeOnly").on("click", function () {
        assignmentDashboard.loadTeacherAssignmentPages(this.is(":checked"));
    })

    $(document).on("keyup", "#txtAssignmentSearch", function (e) {
        if (e.which == 13) {
            var activeTab = $("#btnLoadActiveAssignment").attr('class').match('active');
            if (activeTab != null) {
                $("#activeAssignmentpages").html('');
                assignmentDashboard.loadTeacherAssignmentPages();
                $("#loadmore").trigger("click");
            } else {
                $("#ArchiveAssignments").html('');
                archivedAssignmentDashboard.loadTeacherArchivedAssignmentPages();
                $("#loadmore1").trigger("click");
            }
        };
    });

    $(document).on('change', '#ddlAssignmentSort', function () {
        var activeTab = $("#btnLoadActiveAssignment").attr('class').match('active');
        if (activeTab != null) {
            $("#activeAssignmentpages").html(''); assignmentDashboard.loadTeacherAssignmentPages();
            $("#loadmore").trigger("click");
        } else {
            $("#ArchiveAssignments").html('');
            archivedAssignmentDashboard.loadTeacherArchivedAssignmentPages();
            $("#loadmore1").trigger("click");
        }
    });


    $(document).on('change', '#ddlteachersDropdownLive', function () {
        var activeTab = $("#btnLoadActiveAssignment").attr('class').match('active');
        if (activeTab != null) {
            $("#activeAssignmentpages").html(''); assignmentDashboard.loadTeacherAssignmentPages();
            $("#loadmore").trigger("click");
        } else {
            $("#ArchiveAssignments").html('');
            archivedAssignmentDashboard.loadTeacherArchivedAssignmentPages();
            $("#loadmore1").trigger("click");
        }
    });


    $(document).on('change', '#SchoolGroupIds', function () {
        var activeTab = $("#btnLoadActiveAssignment").attr('class').match('active');
        if (activeTab != null) {
            $("#activeAssignmentpages").html(''); assignmentDashboard.loadTeacherAssignmentPages();
            $("#loadmore").trigger("click");
        } else {
            $("#ArchiveAssignments").html('');
            archivedAssignmentDashboard.loadTeacherArchivedAssignmentPages();
            $("#loadmore1").trigger("click");
        }
    });


});
