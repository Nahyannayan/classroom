﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();

var documentReview = function () {
    var init = function () {

    },
        editMarkingDoc = function (e) {
            DemoRichEdit.commands.changeFontBackColor.execute($(e).attr("data-color"));
            DemoRichEdit.commands.insertText.execute($(e).val());
            DemoRichEdit.commands.changeFontBold.execute(e);
            var markWeight = parseFloat($(e).attr("data-weight"));
            var calcWeight = parseFloat($(e).attr("data-calcweight"));
            var updatedWeight = (markWeight + calcWeight);
            $(this).attr("data-calcweight", updatedWeight);
            var allCalcmarks = parseFloat($("#calculatedmarks").text()) + markWeight;
            $("#calculatedmarks").text(allCalcmarks);

        },
    calculateMarkAndSave=function (filename) {
        DemoRichEdit.commands.fileSave.execute();
        var markingData = [];
        $(".btnMark").each(function () {

            //markingData.push({
            //    'MarkingSchemeId': $(this).attr("data-markingid") ,
            //    'CalculatedWeight':$(this).attr("data-calcweight") 
            //});
            var markDetail = {
                'MarkingSchemeId': $(this).attr("data-markingid"),
                'CalculatedWeight': $(this).attr("data-calcweight")
            }
            markingData.push(markDetail);
        });

        $.ajax({
            type: "POST",
            url: "/Assignments/Assignment/CalculateMarks",
            data: {
                filePath: filename,
                markingData: markingData
            },
            success: function (response) {
                $("#calculatedmarks").text(response);
            },
            error: function (error) {
            }
        });
    }
    return {
        init: init,
        editMarkingDoc: editMarkingDoc,
        calculateMarkAndSave: calculateMarkAndSave
    }
}();
$(document).ready(function () {
    $(".btnMark").click(function () {
        documentReview.editMarkingDoc(this)
    });

});