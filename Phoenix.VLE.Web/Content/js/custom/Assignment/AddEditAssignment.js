﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var uploadedFiles = [];
var taskOrder = 0;
var taskDetails = [];

var addEditAssignment = function () {
    var init = function () {

        $(window).scrollTop(0);
        var selectedGradingId = $("#GradingTemplateId").val();
        $("#chkSelectGradingTemplate_" + selectedGradingId + "").attr("checked", true);
        //  $("#postedFile").fileinput();
        //$("#postedFile").fileinput({
        //    //language: translatedResources.locale,
        //    title: translatedResources.HTMLVideoUnsupported,
        //    theme: "fas",
        //    showUpload: true,
        //    showCaption: false,
        //    showMultipleNames: true,
        //    uploadUrl: "https://gemsedu.sharepoint.com/sites/App-" + $("#SchoolCode").val()+"/ClassroomSync/_api/web/getfolderbyserverrelativeurl('AssignmentFile/" + $("#OldUserId").val() + "/')/files/add(overwrite=true, url='#filename#')",
        //    fileActionSettings: fileInputControlSettings.fileActionSettings,
        //    allowedFileExtensions: translatedResources.FileTypes,
        //    uploadAsync: true,
        //    maxFileSize: 104800,
        //    //ajaxSettings: settings,
        //    browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
        //    removeClass: "btn btn-sm m-0 z-depth-0",
        //    uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
        //    cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
        //    overwriteInitial: false,
        //    //previewFileIcon: '<i class="fas fa-file"></i>',
        //    initialPreviewAsData: false, // defaults markup
        //    initialPreviewConfig: [{
        //        frameAttr: {
        //            title: 'My Custom Title',
        //        }
        //    }],
        //    preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
        //    previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
        //    previewSettings: fileInputControlSettings.previewSettings,
        //    previewFileExtSettings: fileInputControlSettings.previewFileExtSettings

        //}).on("filepreupload", function (event, data) {
        //    addEditAssignment.createFolder($("#OldUserId").val());
        //    data.jqXHR.setRequestHeader('Authorization', 'Bearer ' + $("#SharepointToken").val());
        //    data.jqXHR.setRequestHeader("accept", "application/json;odata=verbose");
        //}).on('fileuploaded', function (event, previewId, index, fileId, data) {
        //    uploadedFiles.push(data.d);
        //    if (uploadedFiles.length == filesList.length) {
        //        $('#postedFile').fileinput('clear');
        //        filesList = [];
        //        addEditAssignment.PostUploadedFiles(uploadedFiles);
        //        uploadedFiles = [];
        //    }

        //    //addEditAssignment.validateUploadFiles(data);
        //}).on('filebatchuploadcomplete', function (event, preview, config, tags, extraData) {
        //    //alert("sdsafcdasf");
        //    //$('#postedFile').fileinput('clear');
        //    //console.log(uploadedFiles);
        //    //addEditAssignment.PostUploadedFiles(uploadedFiles);
        //    //filesList = [];
        //});



        $("#postedFile").fileinput({
            //language: translatedResources.locale,
            title: translatedResources.HTMLVideoUnsupported,
            theme: "fas",
            showUpload: true,
            showCaption: false,
            showMultipleNames: true,
            uploadUrl: "/Assignments/Assignment/UploadAssignmentFiles",
            uploadExtraData: function () {
                return {
                    lstAssignmentFiles: $("#lstAssignmentFiles").val(),
                };
            },
            fileActionSettings: fileInputControlSettings.fileActionSettings,
            allowedFileExtensions: translatedResources.FileTypes,
            uploadAsync: false,
            maxFileSize: translatedResources.fileSizeAllow,
            browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
            removeClass: "btn btn-sm m-0 z-depth-0",
            uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
            cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
            overwriteInitial: false,
            //previewFileIcon: '<i class="fas fa-file"></i>',
            initialPreviewAsData: false, // defaults markup
            initialPreviewConfig: [{
                frameAttr: {
                    title: 'My Custom Title',
                }
            }],
            preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
            previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
            previewSettings: fileInputControlSettings.previewSettings,
            previewFileExtSettings: fileInputControlSettings.previewFileExtSettings,
            msgSizeTooLarge: 'Total File size (<b>{customSize}</b>) exceeds maximum allowed upload size of <b>{customMaxSize}</b>. Please retry your upload!'
        }).on('filebatchselected', function (event, files) {
            var size = 0;
            $.each(files, function (ind, val) {
                //console.log($(this).attr('size'));
                size = size + $(this).attr('size');
            });

            var maxFileSize = $(this).data().fileinput.maxFileSize,
                msg = $(this).data().fileinput.msgSizeTooLarge,
                formatSize = (s) => {
                    i = Math.floor(Math.log(s) / Math.log(1024));
                    sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                    out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
                    return out;
                };

            if ((size / 1024) > maxFileSize) {
                msg = msg.replace('{name}', 'all files');
                msg = msg.replace('{customSize}', formatSize(size));
                msg = msg.replace('{customMaxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
                //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
                //$(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                //    <span aria-hidden="true">×</span>
                //  </button><ul><li data-thumb-id="thumb-postedFile-0" data-file-id="s">`+ msg + `</li></ul>`).attr("style", "");
                //$(".close.kv-error-close").on('click', function (e) { $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html(''); });
                if (files.length > 1) {
                    $(".kv-fileinput-error.file-error-message").html('');
                    $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                    $(".close.kv-error-close").on('click', function (e) {
                        $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                        //$(".fileinput-upload").removeClass('d-none');
                    });
                }
                else {
                    $(".kv-fileinput-error.file-error-message").html('');
                    $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                    $(".close.kv-error-close").on('click', function (e) {
                        $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                        //$(".fileinput-upload").removeClass('d-none');
                    });
                }

                $(".fileinput-upload").attr('disabled', true);
            }
        }).on('filebatchuploadsuccess', function (event, data) {
            $('#postedFile').fileinput('clear');
            addEditAssignment.validateUploadFiles(data);
        });
    },


        createFolder = function (OldUSerId) {
            var settings = {
                "url": "https://gemsedu.sharepoint.com/sites/App-" + $("#SchoolCode").val() + "/ClassroomSync/_api/web/Folders/add('AssignmentFile/" + OldUSerId + "')",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Authorization": "Bearer " + $("#SharepointToken").val(),
                    "Accept": "application/json;odata=verbose"
                }
            };

            $.ajax(settings).done(function (response) {
                console.log(response);
            });
        },
        saveAssignmentData = function (source, IsPublished) {
            CKEDITOR.instances.AssignmentDesc.updateElement();
            $("#lstTeachersToAdd").val($("#ddlTeacherList").val());
            $("#IsPublished").attr("checked", IsPublished);
            var idsInOrder = $("#taskList").sortable("toArray");
            idsInOrder = idsInOrder.filter(function (number, index) {
                return globalFunctions.isValueValid(number);
            });
            //-----------------^^^^
            $("#lstTaskOrdering").val(JSON.stringify(idsInOrder));
            // condition check for assignment marks with multiple tasks manually given by teacher.
            var assignmentMarks = parseFloat($("#AssignmentMarks").val());
            var sumOfTaskMarks = 0;
            if (assignmentMarks > 0 && $('#IsAssignmentMarksEnable').is(":checked") == true) {
                if ($("#lstAssignmentTask").val() != '') {
                    var lstTaskList = JSON.parse($("#lstAssignmentTask").val());
                    $.each(lstTaskList, function (i, item) {
                        sumOfTaskMarks = sumOfTaskMarks + item.TaskMarks;
                    });
                    if (lstTaskList.length > 0) {
                        if (assignmentMarks != sumOfTaskMarks) {
                            globalFunctions.showErrorMessage(translatedResources.TaskMarkError);
                            return false;
                        }
                    }
                }
            }
            //Deepak Singh on 31 July for async lesson
            var asyncLessonId = globalFunctions.getParameterValueFromQueryString('asylsId');
            $("#asyncLessonId").val(asyncLessonId);
            debugger;
            if ($('form#frmAddEditAssignmentDetails').valid()) {
                if ($("#IsAddMode").val() == "False") {
                    globalFunctions.notyRevert($(source), translatedResources.deleteRevertConfirm);//DeleteRevertConfirm
                    $(source).bind('okClicked', source, function (e) {
                        var currentVal = true;
                        $("#hdnIsChangeReverted").val(currentVal);
                        $('form#frmAddEditAssignmentDetails').submit();
                        //globalFunctions.validateBannedWordsAndSubmitForm('frmAddEditAssignment');
                    });
                    $(source).bind('cancelClicked', source, function (e) {
                        $('form#frmAddEditAssignmentDetails').submit();
                    });
                } else {
                    $('form#frmAddEditAssignmentDetails').submit();
                    //globalFunctions.validateBannedWordsAndSubmitForm('frmAddEditAssignment');
                }
            }
            else {
                $("#assignmentbasicdetails").trigger("click");
            }
        },
        saveAssignmentBasicDetails = function (IsPublished) {
            CKEDITOR.instances.AssignmentDesc.updateElement();

            $(":input[type='text']").on('change', function () {
                trimInputValue($(this));
            });
            $("textarea").on('change', function () {
                trimInputValue($(this));
            });
            if ($('form#frmAddEditAssignmentDetails').valid()) {
                $("#objectiveTab").trigger("click");
                $(window).scrollTop(0)
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage("success", translatedResources.addSuccessMsg);
            var asyncLessonId = globalFunctions.getParameterValueFromQueryString('asylsId');
            if (asyncLessonId === undefined || asyncLessonId == '') {
                window.location.href = "/assignments/assignment/index";
            }
            else {
                var groupId = globalFunctions.getParameterValueFromQueryString('selectedGroupId');
                var courseId = globalFunctions.getParameterValueFromQueryString('courseId');
                window.location.href = '/SchoolStructure/SchoolGroups/AddEditAsyncLessonForm?id=' + asyncLessonId + '&cid=' + courseId + "&gpid=" + groupId + "&tb=2";
            }
        },
        onCancelAssignment = function () {
            window.location.href = "/assignments/assignment/index";
        },
        addEditAssignmentTask = function (source, RandomId, isAddMode) {
            $("#collapseTaskAddEdit").addClass("show");
            var assignmentMarks = $("#AssignmentMarks").val();
            if ($('#IsAssignmentMarksEnable').is(":checked") == false) {
                assignmentMarks = Math.round(parseFloat(0));
            }
            $.ajax({
                type: "POST",
                url: "/Assignments/Assignment/InitAddEditTaskDetailsForm",
                data: {
                    tasksDetails: $("#lstAssignmentTask").val(),
                    gradingTemplateId: $("#GradingTemplateId").val(),
                    isAddMode: isAddMode,
                    RandomId: RandomId,
                    AssignmentMarks: assignmentMarks
                },//parent id
                success: function (response) {
                    $("#addEditTaskDetails").html(response);
                    $("." + RandomId).addClass("d-none");

                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });

            //  $("#addEditTaskDetails").load('/Assignments/Assignment/InitAddEditTaskDetailsForm?id=' + id + '&gradingTemplateId=' + $("#GradingTemplateId").val() + "&sortOrder=" + sortOrder);
            $("#addTaskFiles").show();
        },
        loadTopicSubtopic = function (source, subjectId) {

            if ($("#SubjectId").val() != "") {
                var title = translatedResources.SelectCurriculum;
                globalFunctions.loadPopup(source, '/Assignments/Assignment/loadTopicSubtopic?subjectId=' + $(subjectId).val(), title, 'addtask-dialog modal-md');
                $(source).off("modalLoaded");
                //$(".modal-dialog").addClass('modal-md modal-dialog-scrollable');
                $('#formModified').val(true);

                $(source).on("modalLoaded", function () {
                    formElements.feSelect();
                    $('select').selectpicker();
                    popoverEnabler.attachPopover();
                });
            }
            else {
                globalFunctions.showErrorMessage(translatedResources.SelectSubjectError);
            }

        },


        saveAssignmentTask = function (e) {
            var validationCheck = false;
            var validateTime = true;
            if ($("#TaskTitle").val() === null || $("#TaskTitle").val().trim() === '') {
                $(".TaskTitleValidation").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
                validationCheck = true;
            }

            if ($("#TaskDescription").val() === null || $("#TaskDescription").val().trim() === '') {
                $(".TaskDescriptionValidation").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
                validationCheck = true;
            }
            if (validationCheck) {
                return false;
            }
            if (!validationQuizTime) {
                globalFunctions.showWarningMessage(translatedResources.quizTimeValidation);
                return false;
            }

            if ($('form#frmAddTask').valid()) {
                var isValidated = globalFunctions.validateBannedWords('frmAddTask');
                $("#taskDetails").val($("#lstAssignmentTask").val());
                if ($("#IsSetTime").prop("checked")) {
                    if ($('#QuizStartDate').val().trim() != '') {
                        if ($('#StartTime').val() === null || $('#StartTime').val().trim() === '') {
                            $(".validateStartTime").html("<span class='text-danger'>" + translatedResources.Mandatory + "</span>");
                            validateTime = false;
                        } else {
                            validateTime = true;
                        }
                    }
                }
                if (isValidated && validateTime) {
                    $('form#frmAddTask').submit();
                    $("#myModal").modal('hide');
                }
            }
        },
        onSaveTaskSuccess = function (data) {
            $("#NoTask").hide();
            $("#tasksInfo").html(data.content);
            $("#lstAssignmentTask").val(JSON.stringify(data.lstAssignmentTasks));
            $("#taskList").sortable();
            $("#taskList").disableSelection();
            if ($("#taskList > li").length > 1) {
                $("#divTaskOrdering").removeClass('d-none');
            }
            else {
                $("#divTaskOrdering").addClass('d-none');
            }
            $("#addEditTaskDetails").html("");
            globalFunctions.showSuccessMessage(translatedResources.TaskSaveMessage);

        },
        loadGradingTemplatePopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.GradingTemplate;
            globalFunctions.loadPopup(source, '/Assignments/Assignment/LoadGradingTemplate?id=' + id, title, 'addtask-dialog modal-lg');
            //$(".modal-dialog").addClass('modal-lg');
            $('#formModified').val(true);
        },
        checkGradingTemplate = function (e, id) {
            if ($(e).is(":checked")) {
                $('.chkSelectGradingTemplate').prop('checked', false);
                $(e).prop('checked', true);
                $("#GradingTemplateId").val(id);
            } else {
                $("#GradingTemplateId").val();
            }
        },
        saveGradingTemplate = function () {

            $("#myModal").modal('hide');
            if ($("#GradingTemplateId").val() === undefined || $("#GradingTemplateId").val() === "0") {
                $("#IsGradingEnable").prop('checked', false);
            }
            $.ajax({
                type: "GET",
                url: "/Assignments/Assignment/GetGradingTemplateById",
                data: { id: $("#GradingTemplateId").val() },//parent id
                success: function (response) {
                    $("#gradingDiv").show();
                    $("#gradingDiv > a").attr("data-title", response.GradingTemplateTitle);
                    $("#gradingDiv > a > img").attr("src", response.GradingTemplateLogo);
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });
        },
        loadUploadFilepopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.file;
            globalFunctions.loadPopup(source, '/Assignments/Assignment/AddAttachment?id=' + id, title, 'library-dailog');

        },
        addUploadFilePopup = function (source, id) {
            loadUploadFilepopup(source, translatedResources.add, id);
        },
        deleteTask = function (source, randomId) {
            globalFunctions.notyConfirm($(source), translatedResources.deleteConfirm);
            $(source).unbind('okClicked');
            $(source).bind('okClicked', source, function (e) {
                $.ajax({
                    type: "POST",
                    url: "/Assignments/Assignment/DeleteTask",
                    data: { randomId: randomId, taskDetails: $("#lstAssignmentTask").val() },//parent id
                    success: function (response) {
                        $("#tasksInfo").html('');
                        $("#tasksInfo").html(response.content);
                        $("#lstAssignmentTask").val(JSON.stringify(response.lstAssignmentTasks));
                        $("#taskList").sortable();
                        $("#taskList").disableSelection();
                        globalFunctions.showSuccessMessage(translatedResources.DeleteTaskSuccess);
                        if ($("#taskList > li").length <= 1) {
                            $("#divTaskOrdering").addClass('d-none');
                        }

                    },
                    error: function (error) {
                        globalFunctions.onFailure();
                    }
                });
            });
        },
        postObjective = function () {
            var mappingDetails = [];
            var mappingUnitDetails = [];
            $("#topicAccordion input:checkbox.level-3").each(function () {
                var lessonId = $(this).data('lessonid');
                var isLesson = $(this).data('islesson');
                if ($(this).is(":checked")) {
                    if (isLesson === "True") {
                        mappingDetails.push({
                            ObjectiveId: lessonId
                        });
                    }
                }
                else {
                    mappingDetails = mappingDetails.filter(function (item) {
                        return item.ObjectiveId !== lessonId;
                    });
                }
            });
            $("#topicAccordion input:checkbox.level-2").each(function () {
                var lessonId = $(this).data('lessonid');
                var isLesson = $(this).data('islesson');
                if ($(this).is(":checked")) {
                    if (isLesson === "True") {
                        mappingDetails.push({
                            ObjectiveId: lessonId
                        });
                    }
                }
                else {
                    mappingDetails = mappingDetails.filter(function (item) {
                        return item.LessonId !== lessonId;
                    });
                }
            });
            $("#topicAccordion input:checkbox.level-2").each(function () {
                var subUnitId = $(this).data('lessonid');
                var isSubUnit = $(this).data('islesson');
                if ($(this).is(":checked")) {
                    if (isSubUnit === "False") {
                        mappingUnitDetails.push({
                            CourseUnitId: subUnitId
                        });
                    }
                }
                else {
                    mappingUnitDetails = mappingUnitDetails.filter(function (item) {
                        return item.CourseUnitId !== subUnitId;
                    });
                }
            });
            $("input:checkbox.level-1").each(function () {
                var UnitId = $(this).data('unitid');
                var isUnit = $(this).data('isunit');
                if ($(this).is(":checked")) {
                    if (isUnit === "True") {
                        mappingUnitDetails.push({
                            CourseUnitId: UnitId
                        });
                    }
                }
                else {
                    mappingUnitDetails = mappingUnitDetails.filter(function (item) {
                        return item.CourseUnitId !== UnitId;
                    });
                }
            });
            $("#lstUnits").val(JSON.stringify(mappingUnitDetails))
            $("#lstLessons").val(JSON.stringify(mappingDetails));
        },
        deleteObjective = function (objectiveId) {
            $.ajax({
                type: 'GET',
                url: '/Assignments/Assignment/DeleteObjective?objectiveId=' + objectiveId,
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    $("#selectedObjectiveDetails").html(data);
                    globalFunctions.showSuccessMessage(translatedResources.DeleteObjectiveSuccess);
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
        },
        postUnitGroup = function () {
            var mappingGroupTopicIds = [];
            $("#topicAccordion input:checkbox.level-0").each(function () {
                var groupTopicId = $(this).data('grouptopicid');
                var groupId = $(this).data('groupid');
                if ($(this).is(":checked")) {
                    mappingGroupTopicIds.push({
                        GroupCourseTopicId: groupTopicId,
                        GroupId: groupId
                    });
                }
            });
            $("#lstGroupTopics").val(JSON.stringify(mappingGroupTopicIds));
        },
        loadMyFiles = function (source) {
            var title = translatedResources.FileSelect;
            globalFunctions.loadPopup(source, '/Assignments/Assignment/LoadSelectedMyFiles', title, 'addtask-dialog modal-md');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
            });
        },
        postMyFilesToAssignment = function () {
            var mappingDetails = [];
            $("#tbl_filelist >.permission-checkbox input:checkbox:visible").each(function () {
                mappingDetails.push({
                    AssignmentFileId: $(this).attr('data-fileid'),
                    UploadedFileName: $(this).attr('data-filepath'),
                    FileName: $(this).attr('data-filename')
                });
            });
            var saveparams = {
                mappingDetails: mappingDetails
            };
            $.ajax({
                type: 'POST',
                url: '/Assignments/Assignment/SaveSelectedFiles',
                async: false,
                contentType: 'application/json',
                data: JSON.stringify(saveparams),
                success: function (data) {
                    $("#fileList").html('');
                    $("#fileList").append(data);
                    $("#myModal").modal('hide');
                    $("#scrollableTopic").mCustomScrollbar({
                        setHeight: "100",
                    });
                    $('#lightgallery').lightGallery({
                        selector: '.galleryImg',
                        share: false
                    });
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
        },
        validateDueDate = function () {
            if ($("input[name='SubmissionType']:checked").val() === "3") {
                $("#validateDueDate").hide();
                $("#validateDueTimeString").hide();
            }
        },
        validatePeerEnable = function () {
            var submissionType = $("input[name='SubmissionType']:checked").val();
            if (submissionType === "2" || submissionType === "3") {
                $(".peermark").addClass("d-none")
            }
            else {
                $(".peermark").removeClass("d-none")
            }
        },
        resetFormModified = function () {
            $('#formModified').val(false);
        },
        validateUploadFiles = function (data) {
            var filesSize = 0;
            for (var i = 0; i < data.files.length; i++) {
                filesSize = filesSize + data.files[i].size;
            }
            if ((filesSize / 1024 / 1024) < 200) {
                globalFunctions.showSuccessMessage(translatedResources.UploadSuccess);
                $("#lstAssignmentFiles").val(JSON.stringify(data.response.filenames));
                $("#attachmentslist,#attachmentslistOnDetailPage").html(data.response.content);
                $('.file-upload-widget').animate({
                    right: '-300'
                });
            }
            else {
                globalFunctions.showErrorMessage(translatedResources.UploadLimit);
                $('li[data-file-id="' + data.id + '"]').html(translatedResources.UploadLimit);
                return false;
            }
            $('#fileUploadModal').modal('hide');
        },
        PostUploadedFiles = function (uploadedFiles) {
            var filesDetails = [];
            $.each(uploadedFiles, function (i, item) {
                filesDetails.push({
                    FileName: item.Name,
                    UploadedFileName: item.ServerRelativeUrl,
                    ShareableLinkFileURL: item.LinkingUri
                })
            });
            $.ajax({
                type: 'POST',
                url: '/Assignments/Assignment/GetUploadedFiles',
                data: {
                    uploadedFileDetails: filesDetails,
                    lstAssignmentFiles: $("#lstAssignmentFiles").val()
                },
                success: function (data) {
                    $("#lstAssignmentFiles").val(JSON.stringify(data.filenames));
                    $("#attachmentslist,#attachmentslistOnDetailPage").html(data.content);
                    $('.file-upload-widget').animate({
                        right: '-300'
                    });
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });

        },
        addUpdatePeerMArking = function (source, id) {

            var title = translatedResources.PeerManagerTitle;
            globalFunctions.loadPopup(source, '/Assignments/Assignment/AddEditPeerManagerSetting?assignmentId=' + id + '&id=' + $("#StudentIds").val(), title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: translatedResources.PleaseSelect
                });
                //alert("loaded");
                //$('[data-toggle="tooltip"]').tooltip();              
            });
        },
        loadObjectiveStructure = function () {

            //if ($("#topicAccordion").html().trim().length !== 0) return;
            $.ajax({
                type: 'GET',
                url: '/Assignments/Assignment/GetUnitStructureView?courseIds=' + $("#CourseIds").val() + '&groupIds=' + $("#SchoolGroupIds").val() + '&assignmentId=' + $("#AssignmentId").val(),
                success: function (data) {
                    $('#topicAccordion').html(data);

                    // for Teacher Created Unit Ids
                    var teacherCreatedIds = $("#lstGroupTopics").val();
                    if (teacherCreatedIds != "") {
                        teacherCreatedIds = JSON.parse(teacherCreatedIds);
                        $.each(teacherCreatedIds, function (i, item) {
                            $('#chk-' + item.GroupCourseTopicId + '').prop('checked', 'checked');
                        });
                    }

                    // for Unit Ids
                    var unitIds = $("#lstUnits").val();
                    if (unitIds != "") {
                        unitIds = JSON.parse(unitIds);
                        $.each(unitIds, function (i, item) {
                            $('#chk-' + item.CourseUnitId + '').prop('checked', 'checked');
                            $('#sTop-' + item.CourseUnitId + '').prop('checked', 'checked');
                        });
                    }

                    // for Lesson Ids
                    var lessonIds = $("#lstLessons").val();
                    if (lessonIds != "")
                        lessonIds = JSON.parse(lessonIds);
                    $.each(lessonIds, function (i, item) {
                        $('#sTop-1-' + item.ObjectiveId + '').prop('checked', 'checked');
                    });
                    globalFunctions.enableCheckboxCascade("#group-value10", "[id^=chk-],[id^=sTop-]");
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });

        },
        ExpanTree = function () {
            $('#divShowMenuTree').tree('expandAll');
        },
        showObjectives = function (node) {

            $('#objectiveDetails').html('');
            var id = node["id"];
            var courseIds = $("#CourseIds").val();
            var isLesson = false;
            if (node['children'] === undefined) {
                isLesson = true;
            }
            $.ajax({
                type: 'GET',
                url: '/Assignments/Assignment/GetSubtopicObjectives?id=' + id + '&courseIds=' + courseIds + '&isLesson=' + isLesson,
                // data: { id: id, courseIds: courseIds, isLesson: isLesson },
                success: function (data) {
                    //console.log(data);
                    $('#objectiveDetails').html(data);

                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
            //}
            //else {
            //    globalFunctions.ShowMessage("error", translatedResources.parentclickerrorMsg);
            //}

        },
        saveAssignmentDetails = function () {
            $("#frmAddEditAssignmentDetails").submit();
        },
        loadTaskList = function () {
            $("#taskTab").trigger("click");
            $(window).scrollTop(0)
        },
        trimInputValue = function (input) {
            var trimmedValue = $(input).val().trim();
            $(input).val(trimmedValue);
        },

        deleteTaskFile = function (source) {
            var files = JSON.parse($("#taskFilesDetails").val());
            files = files.filter(function (item, i) {
                return item.UploadedFileName !== $(source).data("filepath");
            });
            $("#taskFilesDetails").val(JSON.stringify(files));
            $(source).parents('.divTaskFile').remove();
        },
        isNumberKey = function (evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57)))
                return false;
            return true;
        };

    return {
        init: init,
        saveAssignmentData: saveAssignmentData,
        onSaveSuccess: onSaveSuccess,
        saveAssignmentTask: saveAssignmentTask,
        onSaveTaskSuccess: onSaveTaskSuccess,
        loadGradingTemplatePopup: loadGradingTemplatePopup,
        checkGradingTemplate: checkGradingTemplate,
        saveGradingTemplate: saveGradingTemplate,
        addUploadFilePopup: addUploadFilePopup,
        deleteTask: deleteTask,
        loadTopicSubtopic: loadTopicSubtopic,
        onCancelAssignment: onCancelAssignment,
        postObjective: postObjective,
        postUnitGroup: postUnitGroup,
        deleteObjective: deleteObjective,
        loadMyFiles: loadMyFiles,
        postMyFilesToAssignment: postMyFilesToAssignment,
        validateDueDate: validateDueDate,
        resetFormModified: resetFormModified,
        validateUploadFiles: validateUploadFiles,
        addUpdatePeerMArking: addUpdatePeerMArking,
        validatePeerEnable: validatePeerEnable,
        loadObjectiveStructure: loadObjectiveStructure,
        ExpanTree: ExpanTree,
        showObjectives: showObjectives,
        saveAssignmentDetails: saveAssignmentDetails,
        addEditAssignmentTask: addEditAssignmentTask,
        saveAssignmentBasicDetails: saveAssignmentBasicDetails,
        loadTaskList: loadTaskList,
        trimInputValue: trimInputValue,
        deleteTaskFile: deleteTaskFile,
        createFolder: createFolder,
        PostUploadedFiles: PostUploadedFiles,
        isNumberKey: isNumberKey
    };
}();

$(document).ready(function () {
    CKEDITOR.plugins.addExternal('ckeditor_wiris', '', 'plugin.js');
    CKEDITOR.replace('AssignmentDesc', {
        htmlencodeoutput: false,
        height: '180px',
        tabSpaces: 0,
        extraPlugins: 'ckeditor_wiris',
        allowedContent: true,
        allowedContent: {
            $1: {
                elements: CKEDITOR.dtd,
                attributes: true,
                styles: true,
                classes: true
            }
        },
        disallowedContent: 'script; *[on*];',
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P,
        contentsLangDirection: $("html").attr("dir")
    });

    formElements.feSelect();
    $('.selectpicker').selectpicker({
        dropdownAlignRight: true
    });
    addEditAssignment.init();
    popoverEnabler.attachPopover();
    addEditAssignment.validateDueDate();
    addEditAssignment.validatePeerEnable();
    $(document).on('click', '#btnSaveTask', function () {

        addEditAssignment.saveAssignmentTask($(this));
    });

    // in case of edit Assignment
    if ($('#IsAssignmentMarksEnable').is(":checked")) {
        $("#AssignmentMarks").removeAttr('disabled');
    } else {
        $("#AssignmentMarks").attr('disabled', 'disabled');
        $("#AssignmentMarks").val('');
    }

    $(document).on('click', '#IsAssignmentMarksEnable', function () {
        if ($('#IsAssignmentMarksEnable').is(":checked")) {
            $("#AssignmentMarks").removeAttr('disabled');
        } else {
            $("#AssignmentMarks").attr('disabled', 'disabled');
            $('#AssignmentMarks-error').html('');
            $("#AssignmentMarks").val('');
        }
    });

    $(document).on('change', ":input[type='text'], textarea", function () {
        trimInputValue($(this));
    });
    $(document).on('click', '#IsGradingEnable', function () {
        if ($("#IsGradingEnable").is(":checked")) {
            addEditAssignment.loadGradingTemplatePopup($(this), translatedResources.Select, '#AssignmentId');
        }
    });
    $("#btnCancelAssign").click(function () {
        location.href = "/assignments/assignment";
    });
    globalFunctions.enableCascadedDropdownList("SchoolGroupIds", "StudentIds", '/Assignments/Assignment/GetStudentsinGroup');
    $(document).on('click', '#addFile', function (event) {
        event.preventDefault();
        //initialise fileuploader
        var parameters = [];
        multipleFileUploader.init('/Assignments/Assignment/SaveAttachmentData', addEditAssignment.loadDocumentsdetails, parameters);
        addEditAssignment.addUploadFilePopup($(this), 39);
    });
    /*-----------------------------------delete file------------------------*/
    $(document).on('click', '#ahrefDeleteFiles', function () {
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        var fileName = $(this).attr('data-fileName');
        var uploadedfilepath = $(this).attr('data-uploadedPath');
        var fileId = $(this).attr('data-FileId');
        var assignmentId = $(this).attr('data-assignmentId');
        $(document).bind('okClicked', $(this), function (e) {
            $.post('/Assignments/Assignment/DeleteFilesFromSession', { lstAssignmentFiles: $("#lstAssignmentFiles").val(), fileId: fileId, AssignmentId: assignmentId, fileName: fileName, UploadedFilePath: uploadedfilepath }, function (data) {
                $("#lstAssignmentFiles").val(JSON.stringify(data.filenames));
                //$("#attachmentslist,").html(data.content);
                $("#attachmentslist,#attachmentslistOnDetailPage").html(data.content);
            });
        });
    });
    if ($("#scrollableTopic").height() > 100) {
        $("#scrollableTopic").mCustomScrollbar({
            setHeight: "100"
        });
    }
    //timepicker
    $('.time-picker').datetimepicker({
        locale: translatedResources.locale,
        format: 'LT'
    });
    //datepicker
    var date = new Date();
    date.setDate(date.getDate() - 2);
    console.log($("#hdnStartDate").val());
    var minDateData = $("#hdnStartDate").val();
    var currentDateData = $("#hdnCurrentDate").val();
    var onlyMinDate = minDateData.split(' ')[0];
    var monthMinformat = (onlyMinDate.split('/')[0]).length < 2 ? "0" + (onlyMinDate.split('/')[0]) : (onlyMinDate.split('/')[0]);
    var dateMinformat = (onlyMinDate.split('/')[1]).length < 2 ? "0" + (onlyMinDate.split('/')[1]) : (onlyMinDate.split('/')[1]);
    var yearMinformat = (onlyMinDate.split('/')[2]);
    minDateData = monthMinformat + "/" + dateMinformat + "/" + yearMinformat;

    onlyCurrentDate = currentDateData.split(' ')[0];
    var monthCurrformat = (onlyCurrentDate.split('/')[0]).length < 2 ? "0" + (onlyCurrentDate.split('/')[0]) : (onlyCurrentDate.split('/')[0]);
    var dateCurrformat = (onlyCurrentDate.split('/')[1]).length < 2 ? "0" + (onlyCurrentDate.split('/')[1]) : (onlyCurrentDate.split('/')[1]);
    var yearCurrformat = (onlyCurrentDate.split('/')[2]);
    currentDateData = monthCurrformat + "/" + dateCurrformat + "/" + yearCurrformat;

    var minDataDisplay;
    var counterDate = 0;
    console.log(Date.parse(minDateData));
    if (minDateData < currentDateData) {
        minDataDisplay = minDateData;
    } else {
        minDataDisplay = currentDateData;
    }

    var startDate = Date.parse(minDateData.split(' ')[0]);
    var endDate = Date.parse(currentDateData.split(' ')[0]);
    var timeDiff = endDate - startDate;
    counterDate = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
    //var DisabledDate = ['8/25/2020', '8/26/2020', '8/27/2020'] //Testing
    // declare Array
    var pushDate = [];
    for (var i = 1; i <= counterDate; i++) {
        var currDate = new Date();
        var getCurrDate = currDate.setDate(currDate.getDate() - i)
        var arrCurrentDay = formatCurrDate(getCurrDate);
        pushDate.push(arrCurrentDay);
    }


    $('.date-picker2').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: false,
        minDate: minDateData,
        disabledDates: pushDate,
        locale: translatedResources.locale
    });

    $('.date-picker1').datetimepicker({
        format: 'DD-MMM-YYYY',
        minDate: minDataDisplay,
        disabledDates: pushDate,
        locale: translatedResources.locale
    }).on('dp.change', function (e) {
        $('.date-picker2').data("DateTimePicker").minDate(e.date);

        if ($('.date-picker2').data("DateTimePicker").date() < e.date) {

            $('.date-picker2').data("DateTimePicker").clear();
            $('.date-picker2').data("DateTimePicker").minDate(e.date);
        }
    });

    // format Date and push into Array
    function formatCurrDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }


    $('.date-picker1').datetimepicker('show');
    $('.date-picker1').datetimepicker('hide');

    $(".date-picker2").on("dp.change", function (e) {
        $('.date-picker1').data("DateTimePicker").maxDate(e.date);
        //if ($('.date-picker1').data("DateTimePicker").date() > e.date) {
        //    $('.date-picker1').data("DateTimePicker").clear();
        //}

        $('#ArchiveDate').data("DateTimePicker").enable();
        $('#ArchiveDate').data("DateTimePicker").minDate(e.date);
        $('#ArchiveDate').data("DateTimePicker").clear();
    });


    $('#ArchiveDate').datetimepicker({
        format: 'DD-MMM-YYYY',
        disabledDates: pushDate,
        locale: translatedResources.locale
    });
    $('#ArchiveDate').data("DateTimePicker").disable();
    $(document).on('click', '#ViewFile', function (e) {
        e.preventDefault();
        var id = $(this).data("filename");
        if (globalFunctions.isValueValid(id)) {
            window.open("/Document/Viewer/Index?id=" + id + "&module=editAssignment", "_blank");
        }
    });


    $('.sidebar-fixed a, #btnCancelAssign, .profile-actions a').click(function () {
        var modified = $('#formModified').val();
        if (modified === 'true') {
            var r = confirm(translatedResources.ConfirmRedirection);
            if (r) {
                addEditAssignment.resetFormModified();
            } else {
                event.preventDefault();
                return;
            }
        }
    });

    $(document).on('keyup', '#txtSearchLesson', function () {
        var searchText = $(this).val().toLowerCase();
        var parents = $("li[data-searchtext]");
        var levelParent = $.merge(parents, $("div.card-header[data-searchtext]"));
        var source = $.merge(levelParent, $("div.card.z-depth-0[data-searchtext]"));

        $.each(source, function (i, item) {
            var text = $(item).data("searchtext").trim().toLowerCase();
            var childExists = $(this).find('div.card-header[data-searchtext]:visible,div.card.z-depth-0[data-searchtext]:visible').length !== 0;
            if (!childExists)
                $(this).toggle(text.indexOf(searchText) !== -1);
        });
    });

    $("#btnAddAssignment").click(function () {
        addEditAssignment.resetFormModified();
    });

    //$("#postedFile").change(function (e) {
    //    var filesctr = document.getElementById("postedFile");
    //    for (var index = 0; index < filesctr.files.length; index++) { filesList.push(filesctr.files[index]); }
    //});
    ///If form value changes, warn user of loss of data when redirecting other pages
    $('input, select, textarea, checkbox, radio, .custom-control').on("change", function (e) {

        console.log($(this));
        $('#formModified').val(true);
        var selectSubmissionType = $("input:radio[name='SubmissionType']:checked").val();
        if (selectSubmissionType != undefined && selectSubmissionType == 3) {
            $("#DueDate").addClass("ignore");
            $("#DueTimeString").addClass("ignore");
            $("#validateDueDate").hide();
            $("#validateDueTimeString").hide();
        }
        else {
            $("#DueDate").removeClass("ignore");
            $("#DueTimeString").removeClass("ignore");
            $("#validateDueTimeString").show();
            $("#validateDueDate").show();
        }
        if (selectSubmissionType == 1) {
            $(".peermark").removeClass("d-none")
        }
        else {
            $("#IsPeerMarkingEnable").prop("checked", false);
            $("#btnEditPeerReview").addClass("d-none");
            $("#btnEditPeerReview").removeClass("inline-block");
            $(".peermark").addClass("d-none")
        }
    });
    $("#btnBack").show();
    $(document).on('click', '#btnBack', function () {
        window.location.href = document.referrer;
        //window.history.back();
    });
    $('#IsPeerMarkingEnable').change(function () {
        if (this.checked) {
            $("#btnEditPeerReview").addClass("inline-block");
            $("#btnEditPeerReview").removeClass("d-none");
            $("#PeerMarkingType").val("1");
            if ($("#StudentIds").val().length > 0) {
                addEditAssignment.addUpdatePeerMArking($(this), $("#AssignmentId").val());
            }
            else {
                $("#IsPeerMarkingEnable").prop("checked", false);
                $("#btnEditPeerReview").addClass("d-none");
                globalFunctions.showErrorMessage(translatedResources.StudentSelectionMessage)
            }
        }
        else {
            $("#btnEditPeerReview").addClass("d-none");
            $("#btnEditPeerReview").removeClass("inline-block");
            $("#PeerMarkingType").val("");
        }
    });
    $('#btnEditPeerReview').click(function () {
        if ($("#IsPeerMarkingEnable").is(":checked")) {
            if ($("#StudentIds").val().length > 0) {
                addEditAssignment.addUpdatePeerMArking($(this), $("#AssignmentId").val());
            }
            else {
                $("#IsPeerMarkingEnable").prop("checked", false);
                $("#btnEditPeerReview").addClass("d-none");
                globalFunctions.showErrorMessage(translatedResources.StudentSelectionMessage);

            }

        }
    })

    $(document).on("change", "input:checkbox#IsInstantMessage", function () {
        $(".divDdlInstantMessage").toggleClass("d-none", !$(this).is(":checked"));
    });
    $(".chkSelectGradingTemplate").click(function () {
        addEditAssignment.checkGradingTemplate(this, $(this).attr("data-id"))
    });

    $("#chkSelectFinalGrading").click(function () {
        $("#txtEnterMarks").removeAttr('disabled');
    });

    $(document).on("CLOUDFILESUPLOADED", function (e, data) {
        $.ajax({
            url: "/Assignments/Assignment/GetuploadedFileDetails",
            success: function (result) {
                //$("#attachmentslist").html(result);
                $("#attachmentslist,#attachmentslistOnDetailPage").html(result);
            },
            error: function () {
                globalFunctions.onFailure();
            }
        });
    });
    $("#btnSaveObjective").on("click", function () {
        addEditAssignment.postObjective();
        addEditAssignment.postUnitGroup();
        $("#gradingTab").trigger("click")
        $(window).scrollTop(0);
    });

});