﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var peerData = [];
var peerReview = function () {
    var init = function () {
        if ($("#IsAddMode").val() == "True") {
            $("#PeerType1").prop("checked", true);
            peerReview.autoPeerAllocation();
        }
        else {
            if ($("#PeerMarkingType").val() == 2) {
                $("#PeerType2").prop("checked", true);
            }
            else if ($("#PeerMarkingType").val() == 1) {
                $("#PeerType1").prop("checked", true);
                peerReview.autoPeerAllocation();
            }           
        }
    },
        editReviewer = function (e) {
            var userId = $(e).attr("data-id");
            var reviewerId = $(e).attr("data-reviewerId");
            if ($("#spanSelectReviewer_" + userId).length == 0) {
                var peerSeelctionList = '<span id="spanSelectReviewer_' + userId + '"><select class="form-control selectpicker" data-live-search="true" id="ddlPeersList_' + userId + '" data-width="75%">' + $("#peerSelectionList").html() + '</select>' +
                    '<a class="chkReviewer" onclick="peerReview.updateReviewer(this)" data-id="' + userId + '" data-reviewerid="' + reviewerId + '" href = "javascript:void(0);"> <i class="far fa-check-square fa-2x mx-1"></i></a >' +
                    '<a class="revertReviewer" onclick="peerReview.revertSelection(this)" data-id="' + userId + '" data-reviewerid="' + reviewerId + '" href="javascript:void(0);"><i class="far fa-window-close fa-2x"></i></a></span>';
                $('#reviewerDetails_' + userId).append(peerSeelctionList);
                $('#ddlPeersList_' + userId).val(reviewerId);
                $("#ddlPeersList_" + userId + " option[value='" + userId + "']").remove();
                $('select').selectpicker('refresh');
                $('#spanReviewerName_' + userId).addClass("d-none");
            }
        },
        updateReviewer = function (e) {
            var userId = $(e).attr("data-id");
            var reviewerId = $(e).attr("data-reviewerId");
            var selectedReviwerName = $("#ddlPeersList_" + userId + " option:selected").text();
            var updatedReviewerId = $("#ddlPeersList_" + userId).val();
            if (updatedReviewerId != 0) {
                $("#btnEditReviewer_" + userId).attr("data-reviewerId", "" + updatedReviewerId + "");
                $("#ddlPeersList_" + userId).remove();
                $("#spanReviewerName_" + userId).toggleClass("d-none");
                $("#spanReviewerName_" + userId + "> label").text(selectedReviwerName)
                $("#spanSelectReviewer_" + userId).remove();
            }
            else {
                globalFunctions.showErrorMessage("Please select reviewer");
            }
        },
        changeAllReviewer = function () {
            $.each($("#lstPeer .item"), function (ind, item) {
                //if ($(item).find('p').length == 0)
                //    $(item).remove();
                var details = $(item).find('.editReviewer');
                var userId = $(details).attr("data-id");
                var reviewerId = $(details).attr("data-reviewerId");
                peerReview.editReviewer(details);
            });
        },
        revertSelection = function (e) {
            var userId = $(e).attr("data-id");
            var reviewerId = $(e).attr("data-reviewerId");
            var selectedReviwerName = $("#ddlPeersList_" + userId + " option[value='" + reviewerId + "']").text();
            $("#ddlPeersList_" + userId).remove();
            $("#spanReviewerName_" + userId).toggleClass("d-none");
            $("#spanReviewerName_" + userId + "> label").text(selectedReviwerName)
            $("#spanSelectReviewer_" + userId).remove();
        },
        submitPeerAllocation = function () {
            var assignmentId = $("#AssignmentId").val();
            $.each($("#lstPeer .item"), function (ind, item) {
                var details = $(item).find('.editReviewer');
                var userId = $(details).attr("data-id");
                var reviewerId = $(details).attr("data-reviewerId");
                var peerDetail = {
                    'UserId': userId,
                    'ReviewerId': reviewerId,
                    'AssignmentId': assignmentId
                }
                peerData.push(peerDetail);
            });
            // alert(JSON.stringify(peerData));
            if (peerData.length > 0) {
                $.ajax({
                    url: '/assignments/assignment/updatepeerdetails?studentIds=' + $("#StudentIds").val(),
                    type: 'post',
                    data: {
                        peerData: peerData
                    },
                    datatype: 'json',
                    contenttype: 'application/json; charset=utf-8',

                    success: function (data) {
                        //alert(data.result);
                        if (data.Success) {
                            globalFunctions.showSuccessMessage(translatedResources.SavePeerMappingMessage);
                            $("#myModal").modal('hide');
                        }
                        else {
                            globalFunctions.showErrorMessage(translatedResources.technicalError);
                        }
                    },

                });
            }
       
        },
        autoPeerAllocation = function () {
            var assignmentId = $("#AssignmentId").val();
            var stdIds = $("#StudentIds").val();
            $.ajax({
                url: '/assignments/assignment/AutomaticallyAssignPeers?assignmentId=' + assignmentId + '&id=' + stdIds,
                type: 'get',
                datatype: 'json',
                contenttype: 'application/json; charset=utf-8',
                success: function (data) {
                    $("#lstPeer").html('');
                    $("#lstPeer").append(data);
                         
                },

            });
        }

    return {
        init: init,
        editReviewer: editReviewer,
        updateReviewer: updateReviewer,
        changeAllReviewer: changeAllReviewer,
        revertSelection: revertSelection,
        submitPeerAllocation: submitPeerAllocation,
        autoPeerAllocation: autoPeerAllocation
    }
}();
$(document).ready(function () {
    peerReview.init();
    $(".editReviewer").click(function () {
        var userId = $(this).attr("data-id");
        var reviewerId = $(this).attr("data-reviewerId");
        peerReview.editReviewer(this)
    });
    $("#PeerType2").on("change", function () {
        var allocationType = $("input:radio[name='PeerType']:checked").val();
        if (allocationType != undefined && allocationType == 2) {
            peerReview.changeAllReviewer();
            $("#PeerMarkingType").val("2");
        }
    });
    $("#PeerType1").on("change", function () {
        var allocationType = $("input:radio[name='PeerType']:checked").val();
        if (allocationType != undefined && allocationType == 1) {
            peerReview.autoPeerAllocation();
            $("#PeerMarkingType").val("1");
        }
    });
    $("#btnSavePeerDetails").click(function () {
        peerReview.submitPeerAllocation();
    })
});