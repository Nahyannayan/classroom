﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var taskList = [];
var taskOrder = 0;
var pageNo = 0;
var archivedAssignmentDashboard = function () {
    var init = function () {
    },
        deleteAssignment = function (source, assignmentId) {
            globalFunctions.notyDeleteConfirm($(source), translatedResources.deleteConfirm);
            $(document).bind('okToDelete', $(source), function (e) {
                $.ajax({
                    type: 'GET',
                    url: '/Assignments/Assignment/DeleteAssignment?assignmentId=' + assignmentId,
                    async: false,
                    contentType: 'application/json',
                    success: function (data) {
                        if (data.Success) {
                            globalFunctions.showSuccessMessage(translatedResources.SuccessDelete);
                            location.reload(true);
                        }
                        else {
                            globalFunctions.showErrorMessage(translatedResources.ErrorDelete);
                        }
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.onFailure(data, xhr, status);
                    }
                });
            });
        },
        initAddEditAassignmentForm = function (e) {
            var AssignmentId = $(e).attr("data-id");
            window.location.href = "/Assignments/Assignment/InitAddEditAassignmentForm?id=" + AssignmentId;
        },
        cloneAssignment = function (e) {
            var AssignmentId = $(e).data("id");
            window.location.href = "/Assignments/Assignment/InitAddEditAassignmentForm?id=" + AssignmentId + "&isCloned=" + true;
        },
        loadAssignmentDtails = function (AssignmentId) {
            window.location.href = "/Assignments/Assignment/InitAddEditAassignmentForm?id=" + AssignmentId;
        },
        AssignmentUnArchive = function (source, AssignmentId) {
            globalFunctions.notyConfirm($(source), translatedResources.UnArchiveAssignmentConfirm);
            $(document).bind('okClicked', $(source), function (e) {
                $.ajax({
                    type: "POST",
                    url: "/Assignments/Assignment/AssignmentUnArchive?assignmentId=" + AssignmentId + "",
                    success: function (response) {
                        if (true) {
                            globalFunctions.showSuccessMessage(translatedResources.AssignmentUnArchiveSubmitted);
                            location.reload(true);
                        }
                        else {
                            globalFunctions.showErrorMessage(translatedResources.AssignmentUnArchiveError);
                        }
                    }
                });
            });
        },
        loadTeacherArchivedAssignmentPages = function () {
            var groupIds = $("#SchoolGroupIds").val();
            if ($("#ddlteachersDropdownLive").val() === undefined) {
                var teacherIds = [];
            } else {
                var teacherIds = $("#ddlteachersDropdownLive").val();
            }
            var data = {
                page: 1,
                size: 15,
                assignmentType: $("#hdnAssignmentType").val(),
                searchString: $("#txtAssignmentSearch").val(),
                CreatedByMeOnly: false,
                sortBy: $("#ddlAssignmentSort").val(),
                filterVal: groupIds.toString(),
                teacherVal: teacherIds.toString()
            };
            $(window).off('scroll');
            var obj1 = $("#ArchivedAssignment").loadMore1({
                loadType: 'click',
                conLocation: '#ArchiveAssignments',
                type: "POST",
                url: '/Assignments/Assignment/LoadArchivedAssignment',
                dataType: 'json',
                data: data,
                success: function (res) {
                    data.page += 1;
                    pageNo = data.page;
                    obj1.updatePram('data', data);
                    archivedAssignmentDashboard.render(res.content);
                    if (res.isContentFinished == true) {
                        $("#loadmore1").addClass("d-none");
                    }
                }
            });

        },

        render = function (data) {
            $.each(data, function (key, value) {
                $("#ArchiveAssignments").append(value);
            });
        },
        loadDefaultArchiveAssignments = function () {
            $.ajax({
                type: 'GET',
                url: '/Assignments/Assignment/LoadArchivedAssignment?page=1&size=15',
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    $("#ArchiveAssignmentList").html(data.content);
                    archivedAssignmentDashboard.loadTeacherArchivedAssignmentPages();
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
        }
    return {
        init: init,
        initAddEditAassignmentForm: initAddEditAassignmentForm,
        cloneAssignment: cloneAssignment,
        deleteAssignment: deleteAssignment,
        loadAssignmentDtails: loadAssignmentDtails,
        AssignmentUnArchive: AssignmentUnArchive,
        loadTeacherArchivedAssignmentPages: loadTeacherArchivedAssignmentPages,
        loadDefaultArchiveAssignments: loadDefaultArchiveAssignments,
        render: render
    };
}();
function archiveActiveAssignments() {
    jQuery.ajaxSettings.traditional = true;
    //Only to assign source, please follow standard way as in other js files for confirmation popup
    var source = $("#activeAssignmentList");
    globalFunctions.notyConfirm(source, translatedResources.archiveConfirm);
    $(document).bind('okClicked', source, function (e) {
        if ($('#selectAllUnassigned').is(':checked')) {
            $('#selectAllUnassigned').prop('checked', false);
        }
        var selectedMembers = $("#activeAssignmentList input[type='checkbox']:checked");
        var assignmentsToArchive = [];
        if (selectedMembers != null && selectedMembers.length > 0) {
            selectedMembers.each(function (i, elem) {
                assignmentsToArchive = assignmentsToArchive + elem.value + ',';
            });
            $.ajax({
                type: 'POST',
                data: { assignmentsToArchive: assignmentsToArchive },
                url: '/Assignments/Assignment/UpdateActiveAssignment',
                success: function (result) {
                    loadAssignmentList();
                    // location.reload();
                    formElements.feSelect();
                },
                error: function (data) { }
            });
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.SelectAssignmentToArchive);
        }
    });
}

function retreiveArchivedAssignments(el) {
    globalFunctions.notyConfirm($(el));
    $(el).off("okClicked");
    $(el).on("okClicked", function () {
        var source = $("#archiveAssignmentList input:checkbox.archived:checked");
        var assignmentIds = [];
        source.each(function (i, item) {
            
            assignmentIds.push($(this).attr("value"));
        });
        if (assignmentIds.length == 0) return false;

        $.ajax({
            type: "POST",
            url: "/Assignments/Assignment/UndoAssignmentArchive",
            data: { assignmentIds: assignmentIds.join(",") },
            success: function (data) {
                globalFunctions.showMessage(data.NotificationType, data.Message);
                if (data.Success) {
                    //source.each(function () {
                    //    $(this).closest('li[data-search-text]').remove();
                    //});
                    $('#pills-SelectedStudent-tab').trigger("click");
                }
            },
            error: function () {
                globalFunctions.onFailure();
            }
        });
    });
}

function loadAssignmentList() {
    debugger;
    loadActiveAssignments();
    loadArchiveAssignment();
}

function loadActiveAssignments() {
    $.ajax({
        url: '/Assignments/Assignment/GetActiveAssignments',
        success: function (result) {
            $('#activeAssignmentList').html(result);
        },
        error: function (data) { }
    });
}
function loadArchiveAssignment() {
    $.ajax({
        url: '/Assignments/Assignment/GetArchiveAssignments',
        success: function (result) {
            $('#archiveAssignmentList').html(result);
        },
        error: function (data) { }
    });
}
$(document).on('keyup', '#availableMemberSearch', function (e) {
    var searchText = $(this).val().toLowerCase();
    $('#activeAssignmentList ul li').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
});
$(document).on('keyup', '#groupMemberSearch', function (e) {
    //if (e.which === 13) {
    var searchText = $(this).val().toLowerCase();
    $('#archiveAssignmentList ul li').each(function () {
        var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
        $(this).toggle(showCurrentLi);
    });
    //}
});

$('#selectAllUnassigned').change(function () {
    //debugger;
    var chk = $(this).is(':checked');
    $('input[type=checkbox]', "#activeAssignment").each(function () {
        $(this).prop('checked', chk);
    });
});

$(document).ready(function () {
    var htmlContent;
    $('#pills-SelectedStudent-tab').on('click', function (event) {
        //var groupId = $('#SchoolGroupId').val();
        $.get("/Assignments/Assignment/ArchiveAssignmentsList", function (response) {
            $("#ArchivedAssignment").addClass('d-none');
            $("#TeacherAssignment").addClass('d-none');
            $("#createSearchAssignmentId").hide();
            $('#selectedStudent').html(response);
            $('.selectpicker').selectpicker('refresh');
            //$(".delegates-wrapper").mCustomScrollbar({
            //    setHeight: "250",
            //    autoExpandScrollbar: true,
            //});
        });
    });

    $('#btnLoadActiveAssignment').on('click', function (event) {
        $("#ArchivedAssignment").addClass('d-none');
        $("#TeacherAssignment").removeClass('d-none');
        $("#StudentAssignment").removeClass('d-none');
        $("#btnCreateAssignment").removeClass('d-none');
        $("#createSearchAssignmentId").show();
    });

    $('#btnArchiveAssignment').on('click', function (event) {
        $("#TeacherAssignment").addClass('d-none');
        $("#StudentAssignment").addClass('d-none');
        $("#ArchivedAssignment").removeClass('d-none');
        $("#btnCreateAssignment").addClass('d-none');
        $("#createSearchAssignmentId").show();
        if (pageNo === 0) {
            archivedAssignmentDashboard.loadTeacherArchivedAssignmentPages();
            $("#loadmore1").trigger("click");
            pageNo = 1;
        }
        //htmlContent = $("#TeacherAssignment").html();
        //var grid = new DynamicPagination("ArchivedAssignment");
        //var settings = {
        //    url: '/Assignments/Assignment/LoadArchivedAssignment'
        //};
        ////$("._loadMore-click").trigger("click");
        ////$("._loadMore-click").addClass("d-none");
        //$("#TeacherAssignment").html("");
        //grid.init(settings);
    });
    $('[data-toggle="tooltip"]').tooltip({
        //trigger: 'focus',
        html: true
    });
    //assignment details page
    $('[data-toggle="tooltip"]').tooltip();
    //assignmentDashboard.init();
    //assignmentDashboard.GetPageData(1,6);
    $('input[type="file"]').fileinput({
        language: translatedResources.locale,
        title: translatedResources.BrowseFile,
        multipleText: translatedResources.TotalFiles,
        showMultipleNames: false,
        buttonClass: 'custom-upload',
    });

    //assignment index page
    $('.time-picker').datetimepicker({
        format: 'LT',
        //debug: true
    });
    $('.date-picker').datetimepicker({
        format: 'MM/DD/YYYY',
        //debug: true
    });
    var dt = new Date();
    var cDate = ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear();
    var cTime = dt.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    $('.date-picker').val(cDate);
    $('.time-picker').val(cTime);



    //---- grid and list view toggle
    $("#grid").addClass('active');
    $('#list').click(function (event) {
        event.preventDefault();
        $('#library .item').addClass('list-group-item');
        $(this).toggleClass('active');
        $("#grid").removeClass('active');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#library .item').removeClass('list-group-item');
        $('#library .item').addClass('grid-group-item');
        $(this).toggleClass('active');
        $("#list").removeClass('active');
    });

    $("#addAssignment").click(function () {
        window.location.href = "/Assignments/Assignment/InitAddEditAassignmentForm";
    });
    $('[data-toggle="tooltip"]').tooltip();

    $(".delegates-wrapper").mCustomScrollbar({
        setHeight: "250",
        autoExpandScrollbar: true,
    });
});
