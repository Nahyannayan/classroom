﻿$(document).ready(function () {
    var listFilter = [];
    var isTimeOut = true;
    //$('.quiz-pagination li:nth-child(2)').addClass('active');
    $('body').on('click', '.custom-control-label', function () {

        let chkBox = $(this).parent('.custom-checkbox').find('input');
        //console.log(chkBox.attr('id'));
        $(this).parent('.custom-checkbox').find('input').on('change', function () {
            if ($(this).parent('.custom-checkbox').find('input').is(":checked")) {

                chkBox.attr("checked", true);

                var QuizId = $("#QuizId").val();
                var quesId = $(this).attr("data-questionid");
                var ansId = $(this).attr("data-answerid");
                var ansValue = $(this).attr("checked");
                var obMarks = $(this).attr("data-marks");
                list.push({
                    QuizId: QuizId, QuizQuestionId: quesId
                    , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
                });
            } else {
                chkBox.removeAttr("checked");

                var QuizId = $("#QuizId").val();
                var quesId = $(this).attr("data-questionid");
                var ansId = $(this).attr("data-answerid");
                var ansValue = $(this).attr("checked");
                var obMarks = $(this).attr("data-marks");
                list = list.reverse().reduce(function (item, e1) {
                    var matches = item.filter(function (e2) { return e1.QuizQuestionId == quesId && e1.QuizAnswerId == ansId });
                    if (matches.length == 0) {
                        item.push(e1);
                    }
                    return item;
                }, []);

            }


        })
        let rdBtn = $(this).parent('.custom-radio').find('input');
        $(this).parent('.custom-radio').find('input').change(function () {
            if ($(this).parent('.custom-radio').find('input').is(":checked")) {

                rdBtn.attr("checked", true);
                var QuizId = $("#QuizId").val();
                var quesId = $(this).attr("data-questionid");
                var ansId = $(this).attr("data-answerid");
                var ansValue = $(this).attr("checked");
                var obMarks = $(this).attr("data-marks");
                list = jQuery.grep(list, function (value) {
                    return parseInt(value.QuizAnswerId) != parseInt(ansId) && parseInt(value.QuizQuestionId) != parseInt(quesId);
                });
                list.push({
                    QuizId: QuizId, QuizQuestionId: quesId
                    , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
                });
            } else {
                rdBtn.removeAttr("checked");
                var QuizId = $("#QuizId").val();
                var quesId = $(this).attr("data-questionid");
                var ansId = $(this).attr("data-answerid");
                var ansValue = $(this).attr("checked");
                var obMarks = $(this).attr("data-marks");
                list = list.reverse().reduce(function (item, e1) {
                    var matches = item.filter(function (e2) { return e1.QuizQuestionId == quesId && e1.QuizAnswerId == ansId });
                    if (matches.length == 0) {
                        item.push(e1);
                    }
                    return item;
                }, []);
            }

            //var QuizId = $("#QuizId").val();
            //var quesId = $(this).attr("data-questionid");
            //var ansId = $(this).attr("data-answerid");
            //var ansValue = $(this).attr("checked");
            //var obMarks = $(this).attr("data-marks");
            //list.push({
            //    QuizId: QuizId, QuizQuestionId: quesId
            //    , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
            //});
        })
    });

    $('body').on('change', '.selectpicker', function () {


        var QuizId = $("#QuizId").val();
        var quesId = $(this).attr("data-questionid");
        var ansId = $("option:selected", this).val();
        var ansValue = $("option:selected", this).text();
        var obMarks = $(this).attr("data-marks");
        if (QuizId != '') {
            list.push({
                QuizId: QuizId, QuizQuestionId: quesId
                , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
            });
        }
    });

    $('body').on('blur change', '.freetext', function () {


        var QuizId = $("#QuizId").val();
        var quesId = $(this).attr("data-questionid");
        var ansId = $(this).attr("data-answerid");
        var ansValue = $(this).val();
        var obMarks = 0;
        list.push({
            QuizId: QuizId, QuizQuestionId: quesId
            , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
        });
    });
    $('body').on('blur change', '.fibFreetext', function () {
        //debugger;
        var QuizId = $("#QuizId").val();
        var quesId = $(this).attr("data-questionid");
        var ansId = $(this).attr("data-answerid");
        var ans = $(this).attr("data-f").toLowerCase();
        var sortorder = $(this).attr("data-sortorder");
        var ansValue = $(this).val().toLowerCase();
        var obMarks = 0;
        if (ansValue == ans) {
            obMarks = $(this).attr("data-marks");
        }
        list.push({
            QuizId: QuizId, QuizQuestionId: quesId
            , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks, SortOrder: sortorder
        });
    });
    $('body').on('blur', '.freetextTeacherresponse', function () {


        var QuizId = $("#QuizId").val();
        var quesId = $(this).closest('.freetext-fieldset').find('.freetext').attr("data-questionid");
        var ansId = $(this).closest('.freetext-fieldset').find('.freetext').attr("data-answerid");
        var ansValue = $(this).closest('.freetext-fieldset').find('.freetext').val();
        var maxValue = $(this).attr("max");
        //var obMarks = $(this).attr("data-marks");
        var teacherMarks = '';
        if (!isNaN($(this).val()) && $(this).val() != '') {
            if ((parseFloat($(this).val()) > parseFloat(maxValue))) {

                globalFunctions.showWarningMessage(translatedResources.EnterValidMarks);
                $(this).val(teacherMarks)
                return;
            }
            teacherMarks = (parseFloat($(this).val()) > parseFloat(maxValue)) ? '' : parseFloat($(this).val());
        }
        else {

            globalFunctions.showWarningMessage(translatedResources.EnterValidMarks);
            $(this).val(teacherMarks)
            return;
        }
        var quizResponseId = $("#QuizResponseId").val();
        $(this).val(teacherMarks)
        if (teacherMarks != '') {
            list.push({
                QuizId: QuizId, QuizResponseId: quizResponseId, QuizQuestionId: quesId
                , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: teacherMarks
            });
        }
    });
    var timer1 = $('.quizTimer').startTimer({
        onComplete: function (element) {
            isTimeOut = true;
            $("#btnSubmitQuiz").hide();
            submitAssignmentQuiz();
            localStorage.setItem("quizStarted", 0);
        },
        onStart: $('.timerstart'),
        onPause: $('.timerpause'),
        loop: false
    });
    if ($("#IsSetTime").val() == "True") {
        timer1.trigger('start');
    }
    $("#btnSubmitQuiz").on('click', function (e) {
        e.preventDefault();
        isTimeOut = false;
        submitAssignmentQuiz();

        //var responseQuestionId = $(".responseQuestionId");
        ////console.log(responseInputs);
        //if (responseQuestionId.length > 0) {
        //    listFilter = list.reverse().reduce(function (item, e1) {
        //        var matches = item.filter(function (e2) { return e1.QuizQuestionId == e2.QuizQuestionId && e1.QuizAnswerId == e2.QuizAnswerId });
        //        if (matches.length == 0) {
        //            item.push(e1);
        //        }
        //        return item;
        //    }, []);
        //    var isValid = true;
        //    for (var i = 0; i < responseQuestionId.length; i++) {
        //        var isReq = ($(responseQuestionId[i]).attr("data-isrequired") === "True" ? true : false);
        //        var quesTypeId = $(responseQuestionId[i]).data("questype");
        //        var questionId = responseQuestionId[i].value;
        //        if (isReq) {
        //            var isChecked = false;
        //            for (var j = 0; j < listFilter.length; j++) {
        //                if (listFilter[j].QuizQuestionId == questionId) {
        //                    isChecked = true;
        //                }
        //                if (quesTypeId == 4 && listFilter[j].QuizAnswerText == '' && listFilter[j].QuizQuestionId == questionId) {
        //                    isChecked = false;
        //                }
        //            }

        //            if (!isChecked) {
        //                $('[data-questionid="' + questionId + '"]').focus();
        //                $('[data-questionid="' + questionId + '"]').closest('.row').effect("pulsate", {}, 3000);
        //                globalFunctions.showWarningMessage('Mandatory');
        //                Isvalid = false;
        //                return false;
        //            }
        //        }
        //        if ($('#hdnIsTeacher').val() == 1 && quesTypeId==4) {
        //            var freetextTeacherresponse = $(".freetextTeacherresponse");
        //            var freetxtQuestionId = $(responseQuestionId[i]).data("questype");
        //            for (var k = 0; k < freetextTeacherresponse.length; k++) {
        //                var isChecked = true;
        //                var marks = freetextTeacherresponse[k].value;
        //                if (!isFloat(marks)) {
        //                    isChecked = false;
        //                }
        //                if (!isChecked) {
        //                    freetextTeacherresponse[k].focus();
        //                    $('[data-questionid="' + freetxtQuestionId + '"]').closest('.row').effect("pulsate", {}, 3000);
        //                    globalFunctions.showWarningMessage('Please enter marks');
        //                    Isvalid = false;
        //                    return false;
        //                }
        //            }
        //        }
        //    }


        //    if (isValid) {
        //        var formData = {
        //            QuizId: $("#QuizId").val(),
        //            QuizResponseId: $("#QuizResponseId").val(),
        //            StudentId: $("#StudentId").val(),
        //            TskId: $("#TaskId").val()
        //        };
        //        $.ajax({
        //            type: 'POST',
        //            url: '/Assignments/Assignment/SaveQuizData',
        //            data: { model: formData, answersList: listFilter },
        //            success: function (result) {
        //                if (result.Success) {
        //                    //$("#myModal").modal('hide');
        //                    globalFunctions.showMessage(result.NotificationType, result.Message);
        //                    if ($('#hdnIsTeacher').val() == 1) {
        //                        window.location.reload();
        //                    }
        //                    else {
        //                        history.go(-1);
        //                    }
        //                }
        //                else {
        //                    globalFunctions.showMessage(result.NotificationType, result.Message);
        //                }
        //            },
        //            error: function (msg) {

        //            }
        //        });
        //    }
        //} else {
        //    $("#ResponseValidation").text(translatedResources.ResponseValidation);
        //    return false;
        //}
    });
    function submitAssignmentQuiz() {
        //debugger;
        var responseQuestionId = $(".responseQuestionId");
        studentTaskDetails.freetextanswers();
        //console.log(responseInputs);
        if (responseQuestionId.length > 0) {
            listFilter = list.reverse().reduce(function (item, e1) {
                var matches = item.filter(function (e2) { return e1.QuizQuestionId == e2.QuizQuestionId && e1.QuizAnswerId == e2.QuizAnswerId });
                if (matches.length == 0) {
                    item.push(e1);
                }
                return item;
            }, []);
            var isValid = true;
            if (!isTimeOut) {
                for (var i = 0; i < responseQuestionId.length; i++) {
                    var isReq = ($(responseQuestionId[i]).attr("data-isrequired") === "True" ? true : false);
                    var quesTypeId = $(responseQuestionId[i]).data("questype");
                    var questionId = responseQuestionId[i].value;
                    if (isReq) {
                        var isChecked = false;
                        for (var j = 0; j < listFilter.length; j++) {
                            if (listFilter[j].QuizQuestionId == questionId) {
                                isChecked = true;
                            }
                            if (quesTypeId == 4 && listFilter[j].QuizAnswerText == '' && listFilter[j].QuizQuestionId == questionId) {
                                isChecked = false;
                            }
                        }

                        if (!isChecked && $('#hdnIsTeacher').val() != 1) {
                            $('[data-questionid="' + questionId + '"]').focus();
                            $('[data-questionid="' + questionId + '"]').closest('.row').effect("pulsate", {}, 3000);
                            globalFunctions.showWarningMessage('Mandatory');
                            Isvalid = false;
                            return false;
                        }
                    }
                    if ($('#hdnIsTeacher').val() == 1 && quesTypeId == 4) {
                        var freetextTeacherresponse = $(".freetextTeacherresponse");
                        var freetxtQuestionId = $(responseQuestionId[i]).data("questype");
                        for (var k = 0; k < freetextTeacherresponse.length; k++) {
                            var isChecked = true;
                            var marks = freetextTeacherresponse[k].value;
                            if (!isFloat(marks)) {
                                isChecked = false;
                            }
                            if (!isChecked) {
                                freetextTeacherresponse[k].focus();
                                $('[data-questionid="' + freetxtQuestionId + '"]').closest('.row').effect("pulsate", {}, 3000);
                                globalFunctions.showWarningMessage(translatedResources.EnterValidMarks);
                                Isvalid = false;
                                return false;
                            }
                        }
                    }
                }
            }



            if (isValid) {
                var formData = {
                    QuizId: $("#QuizId").val(),
                    QuizResponseId: $("#QuizResponseId").val(),
                    StudentId: $("#StudentId").val(),
                    TskId: $("#TaskId").val(),
                    StudentTaskId: $("#StudentTaskId").val()
                };
                $.ajax({
                    type: 'POST',
                    url: '/Assignments/Assignment/SaveQuizData',
                    data: { model: formData, answersList: listFilter },
                    success: function (result) {
                        if (result.Success) {
                            //$("#myModal").modal('hide');
                            globalFunctions.showMessage(result.NotificationType, result.Message);
                            if ($('#hdnIsTeacher').val() != 1) {
                                logQuizTime($(this), $("#EncrQuizId").val(), $("#EncrTaskId").val(), 'A', false);
                            }
                            if ($('#hdnIsTeacher').val() == 1) {
                                window.location.reload();
                            }
                            else {
                                if (localStorage.getItem("pagerefer") != null) {
                                    var pathrefer = localStorage.getItem("pagerefer");
                                    window.location.href = pathrefer;
                                }
                                //history.go(-1);
                            }
                        }
                        else {
                            globalFunctions.showMessage(result.NotificationType, result.Message);
                        }
                    },
                    error: function (msg) {

                    }
                });
            }
        } else {
            $("#ResponseValidation").text(translatedResources.ResponseValidation);
            return false;
        }
    }
    function isInt(value) {
        var x;
        return isNaN(value) ? !1 : (x = parseFloat(value), (0 | x) === x);
    }
    function isFloat(val) {
        var floatRegex = /^-?\d+(?:[.,]\d*?)?$/;
        if (!floatRegex.test(val))
            return false;

        val = parseFloat(val);
        if (isNaN(val))
            return false;
        return true;
    }
    CKEDITOR.plugins.addExternal('ckeditor_wiris', '', 'plugin.js');
    $('.freetext').each(function () {
        //debugger;
        var selectedId = "QuizFreeText_" + $(this).data("answerid")
        CKEDITOR.replace(selectedId, {
            htmlencodeoutput: false,
            height: '180px',
            extraPlugins: 'ckeditor_wiris',
            allowedContent: true,
            enterMode: CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P
        }).on('change', function () {
            //debugger;
            //IsFreeTextBlur = true;
            //var QuizId = $("#QuizId").val();
            //var quesId = $("#" + $(this)[0].name).attr("data-questionid");
            //var ansId = $("#" + $(this)[0].name).attr("data-answerid");
            //var selectedAns = "QuizFreeText_" + $("#" + $(this)[0].name).data("answerid")
            //var ansValue = CKEDITOR.instances[selectedAns].getData();
            //var obMarks = 0;
            //list.push({
            //    QuizId: QuizId, QuizQuestionId: quesId
            //    , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
            //});
        });
    });
    function freetextanswerstemp() {
        //debugger;
        $('.freetext').each(function () {
            //debugger;
            IsFreeTextBlur = true;
            var QuizId = $("#QuizId").val();
            //var quesId = $("#" + $(this)[0].name).attr("data-questionid");
            //var ansId = $("#" + $(this)[0].name).attr("data-answerid");
            //var selectedAns = "QuizFreeText_" + $("#" + $(this)[0].name).data("answerid")
            var quesId = $(this)[0].dataset.questionid;
            var ansId = $(this)[0].dataset.answerid;
            var selectedAns = "QuizFreeText_" + $(this)[0].dataset.questionid;
            var ansValue = CKEDITOR.instances[selectedAns].getData();
            var obMarks = 0;
            if (ansValue != "") { IsRequiredAttemptCount++; }
            list.push({
                QuizId: QuizId, QuizQuestionId: quesId
                , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
            });
        });
    }
    function logQuizTime(source, id, resourceId, resourceType, IsStart) {
        $.ajax({
            type: 'POST',
            url: '/Files/Files/LogQuizTime',
            data: { id: id, ResourceId: resourceId, ResourceType: resourceType, IsStartQuiz: IsStart },
            success: function (result) {

            },
            error: function (msg) {

            }
        });
    }
    function freetextanswers() {
        $('.freetext').each(function () {
            IsFreeTextBlur = true;
            var QuizId = $("#QuizId").val();
            var quesId = $("#" + $(this)[0].name).attr("data-questionid");
            var ansId = $("#" + $(this)[0].name).attr("data-answerid");
            var selectedAns = "QuizFreeText_" + $("#" + $(this)[0].name).data("answerid")
            //var ansValue = $(this).val();
            var ansValue = CKEDITOR.instances[selectedAns].getData();
            var obMarks = 0;
            if (ansValue != "") { IsRequiredAttemptCount++; }
            list.push({
                QuizId: QuizId, QuizQuestionId: quesId
                , QuizAnswerId: ansId, QuizAnswerText: ansValue, ObtainedMarks: obMarks
            });
        });
    }
    $('body').on('click', '.DeleteAnswerFile', function () {
        debugger;
        globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
        var fileid = $(this).attr('data-fileid');
        var resourceid = $(this).attr('data-resourceid');
        var questionid = $(this).attr('data-questionid');
        var studentid = $(this).attr('data-studentid');
        var divid = "#" + $(this).closest('div[id^="freeTextFileList_"]').attr('id');
        $(document).bind('okClicked', $(this), function (e) {
            debugger;
            $.ajax({
                type: 'POST',
                data: { resourceId: resourceid, resourceType: 'A', questionId: questionid, studentId: studentid, fileId: fileid, },
                url: '/Files/Files/DeleteQuestionAnswerFile',
                success: function (result) {
                    debugger;
                    globalFunctions.showMessage('success', translatedResources.deletedSuccess);
                    $(divid).html(result);
                },
                error: function (msg) {
                    //globalFunctions.onFailure();
                }
            });
        });
    });
});