﻿$(document).ready(function () {

    initPagination();
    

    $('[data-toggle="tooltip"]').tooltip({
        //trigger: 'focus',
        html: true
    });
    //assignment details page
    $('[data-toggle="tooltip"]').tooltip();

    $(document).on("keyup", "#searchContent", function (e) {
        if (e.which == 13)
            initPagination();
    })
});

function initPagination() {
    var grid = new DynamicPagination("GroupAssignments");
    var searchText = globalFunctions.isValueValid($("#searchContent").val()) ? $("#searchContent").val() : ""
    var settings = {
        url: '/Assignments/Assignment/LoadTeacherGroupAssignments?searchString=' + searchText
    };
    grid.init(settings);
}