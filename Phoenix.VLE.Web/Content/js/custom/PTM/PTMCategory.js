﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();
var PTMCategoryList = function () {

    var init = function () {
        loadCategorySetGrid();
        $("#tbl-CategorySet_filter").hide();
    },

        loadCategoryPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.CategorySet;
            if (mode == 'Add') {
                globalFunctions.loadPopup(source, '/PTM/PTMCategoryList/InitAddEditCategorySetForm?SchoolGradeId=' + id, title, 'SchoolSkillSet-dailog');
            }
            else
                globalFunctions.loadPopup(source, '/PTM/PTMCategoryList/InitAddEditCategorySetForm?CategoryId=' + id, title, 'SchoolSkillSet-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
                //$("#tbl-SuggestionCategories_filter").addClass("d-none");
                $("#tbl-CategorySet_filter").hide();
                $('#tbl-CategorySet').DataTable().search('').draw();
                $("#CategorySetListSearch").on("input", function (e) {
                    e.preventDefault();
                    $('#tbl-CategorySet').DataTable().search($(this).val()).draw();

                });
            });
        },

        addCategoryPopup = function (source) {
            var id = $("#SchoolGradeId").val();
            loadCategoryPopup(source, translatedResources.add, id);
        },

        editCategoryPopup = function (source, id) {
            loadCategoryPopup(source, translatedResources.edit, id);
        },

        deleteCategoryData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('PTMCategoryList', '/PTM/PTMCategoryList/DeleteCategorySetData', 'tbl-CategorySet', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        loadCategorySetGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "CategoryName", "sTitle": translatedResources.CategorySet, "sWidth": "30%", "sClass": "wordbreak" },
                { "mData": "CategoryDescription", "sTitle": translatedResources.CategoryDescription, "sWidth": "30%", "sClass": "wordbreak" },
                { "mData": "CreatedBy", "sTitle": translatedResources.CreatedBy, "sWidth": "15%", "sClass": "open-details-control wordbreak" },
                { "mData": "IsApproved", "sTitle": translatedResources.IsApproved, "sWidth": "15%" },
                { "mData": "ApprovedBy", "sTitle": translatedResources.ApprovedBy, "sWidth": "10%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "10%" }
            );
            initCategorySetGrid('tbl-CategorySet', _columnData, '/PTM/PTMCategoryList/LoadCategorySetGrid?SchoolGradeId=' + $("#SchoolGradeId").val());
        },

        initCategorySetGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 4,
                    'orderable': false
                }]
            };
            grid.init(settings);
        };

    return {
        init: init,
        addCategoryPopup: addCategoryPopup,
        editCategoryPopup: editCategoryPopup,
        deleteCategoryData: deleteCategoryData,
        onSaveSuccess: onSaveSuccess,
        loadCategorySetGrid: loadCategorySetGrid
    };
}();

$(document).ready(function () {
    PTMCategoryList.init();


    $(document).on('click', '#btnAddCategorySet', function () {
        PTMCategoryList.addCategoryPopup($(this)); //"#SchoolGradeId"
    });
    //$("#tbl-SuggestionCategories_filter").addClass("d-none");
    $("#tbl-CategorySet_filter").hide();
    $('#tbl-CategorySet').DataTable().search('').draw();
    $("#CategorySetListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-CategorySet').DataTable().search($(this).val()).draw();

    });

    $(document).on("change", 'input:checkbox[name=IsApproved]', function () {
        if (this.checked) {
            $("input:checkbox[name=IsActive]").prop("checked", true);
        }
    });
    $(document).on("change", 'input:checkbox[name=IsActive]', function () {
        if (!this.checked && $("input:checkbox[name=IsApproved]").is(":checked")) {
            $("input:checkbox[name=IsApproved]").prop("checked", false);
        }
    });
});



