﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();
var PTMDeclineReasonList = function () {

    var init = function () {
        loadDeclineReasonSetGrid();
        $("#tbl-DeclineReasonSet_filter").hide();
    },

        loadDeclineReasonPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.DeclineReasonSet;
            if (mode == 'Add') {
                globalFunctions.loadPopup(source, '/PTM/PTMDeclineReason/InitAddEditDeclineReasonSetForm?SchoolGradeId=' + id, title, 'SchoolSkillSet-dailog');
            }
            else
                globalFunctions.loadPopup(source, '/PTM/PTMDeclineReason/InitAddEditDeclineReasonSetForm?DeclineReasonId=' + id, title, 'SchoolSkillSet-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
                //$("#tbl-SuggestionCategories_filter").addClass("d-none");
                $("#tbl-DeclineReasonSet_filter").hide();
                $('#tbl-DeclineReasonSet').DataTable().search('').draw();
                $("#DeclineReasonSetListSearch").on("input", function (e) {
                    e.preventDefault();
                    $('#tbl-DeclineReasonSet').DataTable().search($(this).val()).draw();

                });
            });
        },

        addDeclineReasonPopup = function (source) {
            var id = $("#SchoolGradeId").val();
            loadDeclineReasonPopup(source, translatedResources.add, id);
        },

        editDeclineReasonPopup = function (source, id) {
            loadDeclineReasonPopup(source, translatedResources.edit, id);
        },

        deleteDeclineReasonData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('PTMDeclineReason', '/PTM/PTMDeclineReason/DeleteDeclineReasonSetData', 'tbl-DeclineReasonSet', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        loadDeclineReasonSetGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "DeclineReasonName", "sTitle": translatedResources.DeclineReasonSet, "sWidth": "30%", "sClass": "wordbreak" },
                { "mData": "DeclineReasonDescription", "sTitle": translatedResources.DeclineReasonDescription, "sWidth": "30%", "sClass": "wordbreak" },
                { "mData": "CreatedBy", "sTitle": translatedResources.CreatedBy, "sWidth": "15%", "sClass": "open-details-control wordbreak" },
                { "mData": "IsApproved", "sTitle": translatedResources.IsApproved, "sWidth": "15%" },
                { "mData": "ApprovedBy", "sTitle": translatedResources.ApprovedBy, "sWidth": "10%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "10%" }
            );
            initDeclineReasonSetGrid('tbl-DeclineReasonSet', _columnData, '/PTM/PTMDeclineReason/LoadDeclineReasonSetGrid?SchoolGradeId=' + $("#SchoolGradeId").val());
        },

        initDeclineReasonSetGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 4,
                    'orderable': false
                }]
            };
            grid.init(settings);
        };

    return {
        init: init,
        addDeclineReasonPopup: addDeclineReasonPopup,
        editDeclineReasonPopup: editDeclineReasonPopup,
        deleteDeclineReasonData: deleteDeclineReasonData,
        onSaveSuccess: onSaveSuccess,
        loadDeclineReasonSetGrid: loadDeclineReasonSetGrid
    };
}();

$(document).ready(function () {
    PTMDeclineReasonList.init();


    $(document).on('click', '#btnAddDeclineReasonSet', function () {
        PTMDeclineReasonList.addDeclineReasonPopup($(this)); //"#SchoolGradeId"
    });
    //$("#tbl-SuggestionCategories_filter").addClass("d-none");
    $("#tbl-DeclineReasonSet_filter").hide();
    $('#tbl-DeclineReasonSet').DataTable().search('').draw();
    $("#DeclineReasonSetListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-DeclineReasonSet').DataTable().search($(this).val()).draw();

    });

    $(document).on("change", 'input:checkbox[name=IsApproved]', function () {
        if (this.checked) {
            $("input:checkbox[name=IsActive]").prop("checked", true);
        }
    });
    $(document).on("change", 'input:checkbox[name=IsActive]', function () {
        if (!this.checked && $("input:checkbox[name=IsApproved]").is(":checked")) {
            $("input:checkbox[name=IsApproved]").prop("checked", false);
        }
    });
});
