﻿var HelthChk = false; var isMSelect = true;
var updateStudentDetailsFunctions = function () {
    var onSaveSuccess = function (data) {
        if (data == true) {
            globalFunctions.showSuccessMessage(translatedResources.UpdateDetails)
            window.location.reload();
        }
        else {

            globalFunctions.showErrorMessage(translatedResources.NoUpdateDetailsMsg)

        }

    };
    return {
        onSaveSuccess: onSaveSuccess
    }

}();


$(function () {

    //$("#FeeSponserCompanyVisiable").attr("disabled", true);
    if ($("#FeeSponserCompanyVisiable").val() == "4" || $("#FeeSponserCompanyVisiable").val() == "5") {
        $(".MediaCompanyVisiable").show();
    }
    else {
        $(".MediaCompanyVisiable").hide();
    }

    $("#ParentfeeSponsor_PreferredContact").attr("disabled", true);
    //$("#FirstlanguageDropdownlist").attr("disabled", true);
    //$("#OtherlanguageDropdownlist").attr("disabled", true);
    $('.date-picker2').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: false,
        widgetPositioning: {
            vertical: 'bottom'
        }
        //minDate: minDateData
    });
    $('.date-picker1').datetimepicker({
        format: 'DD-MMM-YYYY',
        widgetPositioning: {
            vertical: 'bottom'
        }
        //minDate: minDateData
        // defaultDate: Date.parse($("#hdnStartDate").val()),

        // maxDate: $('.date-picker2').data("DateTimePicker").date()
    }).on("dp.change", function (e) {
        $('.date-picker2').data("DateTimePicker").minDate(e.date);
    });
    $('.VisaExpiryDatecls').datetimepicker({
        format: 'DD-MMM-YYYY',
        useCurrent: false,
        widgetPositioning: {
            vertical: 'bottom'
        }
        //minDate: minDateData
    }).on("dp.change", function (e) {
        $('.VisaIssueDatecls').data("DateTimePicker").maxDate(e.date);
    });;
    $('.VisaIssueDatecls').datetimepicker({
        format: 'DD-MMM-YYYY',
        widgetPositioning: {
            vertical: 'bottom'
        }
        //minDate: minDateData
        // defaultDate: Date.parse($("#hdnStartDate").val()),

        // maxDate: $('.date-picker2').data("DateTimePicker").date()
    }).on("dp.change", function (e) {
        $('.VisaExpiryDatecls').data("DateTimePicker").minDate(e.date);
    });
    /**** JQuery code for prev next navigation tab *******/
    jQuery('body').on('click', '.next-tab', function () {
        debugger
        var next = jQuery('.nav-tabs .active').parents("li").next();
        if (next.length) {
            next.find('a').trigger('click');
        } else {
            jQuery('#myTabs a:first').tab('show');
        }
    });

    jQuery('body').on('click', '.previous-tab', function () {
        debugger
        var prev = jQuery('.nav-tabs .active').parents("li").prev();
        if (prev.length) {
            prev.find('a').trigger('click');
        } else {
            jQuery('#myTabs a:last').tab('show');
        }
    });
})
function chkMotherDetails(element) {
    if (element.checked == true) {
        {
            $('#MCountryOfResidencyDropDownList').val($("#CountryOfResidencyDropDownList").val());
            $("#MCountryOfResidencyDropDownList").selectpicker('refresh');
            var FCountryId = $("#CountryOfResidencyDropDownList").val();
            commonDropCNFill(FCountryId, 2);
            $('#MResidentialCityDropDownList').selectpicker('val', $("#ResidentialCityDropDownList").val());
            var CityId = $("#ResidentialCityDropDownList").val();
            CommonCTFill(CityId, 2)
            $('#MAreaDropDownList').selectpicker('val', $("#AreaDropDownList").val());
            $('#MStreet').val($("#Street").val());
            $('#MBuilding').val($("#Building").val());
            $('#MApartNo').val($("#ApartNo").val());
            $('#MCityEmirateDropDownList').val($("#CityEmirateDropDownList").val());
            $("#MCityEmirateDropDownList").selectpicker('refresh');
            $('#MPoBox').val($("#PoBox").val());
            $('#MMobileNo').val($("#MobileNo").val());
            $('#MEmailAddress').val($("#EmailAddress").val());
            $('#MOccupation').val($("#Occupation").val());
            $('#MCompanyDropDownList').val($("#CompanyDropDownList").val());
            $("#MCompanyDropDownList").selectpicker('refresh');
            $(".MotherDetails").addClass("active");
        }

    }
    //element.checked ? document.getElementById("LevyFee").disabled = true : document.getElementById("LevyFee").disabled = false;
}
function chkGurdianDetails(element) {
    if (element.checked == true) {
        {
            $('#GCountryOfResidencyDropDownList').val($("#CountryOfResidencyDropDownList").val());
            $("#GCountryOfResidencyDropDownList").selectpicker('refresh');
            var FCountryId = $("#CountryOfResidencyDropDownList").val();
            commonDropCNFill(FCountryId, 3);
            $('#GResidentialCityDropDownList').selectpicker('val', $("#ResidentialCityDropDownList").val());
            var CityId = $("#ResidentialCityDropDownList").val();
            CommonCTFill(CityId, 3)
            $('#GAreaDropDownList').selectpicker('val', $("#AreaDropDownList").val());
            $('#GStreet').val($("#Street").val());
            $('#GBuilding').val($("#Building").val());
            $('#GApartNo').val($("#ApartNo").val());
            $('#GCityEmirateDropDownList').val($("#CityEmirateDropDownList").val());
            $("#GCityEmirateDropDownList").selectpicker('refresh');
            $('#GPoBox').val($("#PoBox").val());
            $('#GMobileNo').val($("#MobileNo").val());
            $('#GEmailAddress').val($("#EmailAddress").val());
            $('#GOccupation').val($("#Occupation").val());
            $('#GCompanyDropDownList').val($("#CompanyDropDownList").val());
            $("#GCompanyDropDownList").selectpicker('refresh');
            $(".GurdianDetails").addClass("active");
        }

    }
    //element.checked ? document.getElementById("LevyFee").disabled = true : document.getElementById("LevyFee").disabled = false;
}
$('#CountryOfResidencyDropDownList').change(function () {
    var FCountryId = $("#CountryOfResidencyDropDownList").val();
    commonDropCNFill(FCountryId, 1);
    $("#ResidentialCityDropDownList").trigger("change");
})
$('#MCountryOfResidencyDropDownList').change(function () {
    var FCountryId = $("#MCountryOfResidencyDropDownList").val();
    commonDropCNFill(FCountryId, 2);
    $("#MResidentialCityDropDownList").trigger("change");
})
$('#GCountryOfResidencyDropDownList').change(function () {
    var FCountryId = $("#GCountryOfResidencyDropDownList").val();
    commonDropCNFill(FCountryId, 3);
    $("#GResidentialCityDropDownList").trigger("change");
})

function commonDropCNFill(FCountryId, id) {
    $.ajax({
        url: '/ParentCorner/UpdateStudentDetails/FillCityFromCountry',
        type: "GET",
        async: false,
        data: { FCountryId: FCountryId },
        success: function (response) {
            if (id == 1) {
                $("#ResidentialCityDropDownList").html("");
                $.each(response, function (i, data) {
                    $("#ResidentialCityDropDownList").append("<option value='" + data.id + "'>" + data.descr + "</option>")
                })
                $("#ResidentialCityDropDownList").selectpicker('refresh');
            }
            else if (id == 2) {
                $("#MResidentialCityDropDownList").html("");
                $.each(response, function (i, data) {
                    $("#MResidentialCityDropDownList").append("<option value='" + data.id + "'>" + data.descr + "</option>")
                })
                $("#MResidentialCityDropDownList").selectpicker('refresh');
            }
            else if (id == 3) {
                $("#GResidentialCityDropDownList").html("");
                $.each(response, function (i, data) {
                    $("#GResidentialCityDropDownList").append("<option value='" + data.id + "'>" + data.descr + "</option>")
                })
                $("#GResidentialCityDropDownList").selectpicker('refresh');
            }

        }
    });
}
$('#ResidentialCityDropDownList').change(function () {
    var CityId = $("#ResidentialCityDropDownList").val();
    CommonCTFill(CityId, 1)
})
$('#MResidentialCityDropDownList').change(function () {
    var CityId = $("#MResidentialCityDropDownList").val();
    CommonCTFill(CityId, 2)
})
$('#GResidentialCityDropDownList').change(function () {
    var CityId = $("#GResidentialCityDropDownList").val();
    CommonCTFill(CityId, 3)
})
function CommonCTFill(CityId, Cid) {
    $.ajax({
        url: '/ParentCorner/UpdateStudentDetails/FillAreaFromCity',
        type: "GET",
        async: false,
        data: { FCityId: CityId },
        success: function (response) {
            if (Cid == 1) {
                $("#AreaDropDownList").html(""); // clear before appending new list
                $.each(response, function (i, data) {
                    $("#AreaDropDownList").append("<option value='" + data.id + "'>" + data.descr + "</option>")
                })
                $("#AreaDropDownList").selectpicker('refresh');
            }
            else if (Cid == 2) {
                $("#MAreaDropDownList").html(""); // clear before appending new list
                $.each(response, function (i, data) {
                    $("#MAreaDropDownList").append("<option value='" + data.id + "'>" + data.descr + "</option>")
                })
                $("#MAreaDropDownList").selectpicker('refresh');
            }
            else if (Cid == 3) {
                $("#GAreaDropDownList").html(""); // clear before appending new list
                $.each(response, function (i, data) {
                    $("#GAreaDropDownList").append("<option value='" + data.id + "'>" + data.descr + "</option>")
                })
                $("#GAreaDropDownList").selectpicker('refresh');
            }

        }
    });
}

function chkInfectiousList() {
    debugger;
    //var InfectiousdiseaseList = "";
    //$('input[type=checkbox][name=Infectious').each(function () {
    //    InfectiousdiseaseList += "" + $(this).val() + ":" + (this.checked ? "0" : "1") + ",";

    //});

    var InfectiousdiseaseList = "";
    $('.Infectioncls').each(function () {
        debugger
        InfectiousdiseaseList += "" + $(this).attr("data-details") + ":" + (this.checked ? "0" : "1") + ",";

    });
    //var strfilter = InfectiousdiseaseList.slice(0, -1);
    //var result = strfilter.split(',')
    //var ChkArray = (result[result.length - 1])
    //var indexCheck = ChkArray.split(':');
    //if (indexCheck[1] == 0) {
    //    $('#OtherinfeciousTextdiv').show();
    //}
    //else {
    //    $('#OtherinfeciousTextdiv').css('display', 'none');
    //    $('#Otherinfecious').val('');
    //}
    $('#checklistbind').val(InfectiousdiseaseList);
}
$('#health').change(function () {
    HelthChk = true;
})
function CheckPropField() {
    if ($("#IsChangedGeneral").val() == "true") {
        if ($("#PassportVisaDetails_PassportNumber").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.PassportNumbermsg)
            return false;
        }
        else if ($("#PassportVisaDetails_PassportIssuePlace").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.PassportIssuePlacemsg)
            return false;
        }
        else if ($("#PassportVisaDetails_PassportIssueDate").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.PassportIssueDatemsg)
            return false;
        }
        else if ($("#PassportVisaDetails_PassportExpiryDate").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.PassportExpiryDatemsg)
            return false;
        }
        //else if ($("#ContactinfoDetails_EmrgencyContact").val() == "") {
        //    globalFunctions.showErrorMessage(translatedResources.EmrgencyContactmsg)
        //    return false;
        //}
        return true;
    }
    if ($("#IsChangedPersonalDetails").val() == "true") {
        if ($("#Street").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.StreetmsgF)
            return false;
        }
        else if ($("#Building").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.BuildingmsgF)
            return false;
        }
        else if ($("#ApartNo").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.ApartNomsgF)
            return false;
        }
        else if ($("#PoBox").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.PoBoxmsgF)
            return false;
        }
        else if ($("#MobileNo").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.MobileNomsgF)
            return false;
        }
        else if ($("#EmailAddress").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.EmailAddressmsgF)
            return false;
        }
        else if ($("#Occupation").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.OccupationmsgF)
            return false;
        }
        if ($("#MStreet").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.StreetmsgM)
            return false;
        }
        else if ($("#MBuilding").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.BuildingmsgM)
            return false;
        }
        else if ($("#MApartNo").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.ApartNomsgM)
            return false;
        }
        else if ($("#MPoBox").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.PoBoxmsgM)
            return false;
        }
        else if ($("#MMobileNo").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.MobileNomsgM)
            return false;
        }
        else if ($("#MEmailAddress").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.EmailAddressmsgM)
            return false;
        }
        else if ($("#MOccupation").val() == "") {
            globalFunctions.showErrorMessage(translatedResources.OccupationmsgM)
            return false;
        }
        return true;
    }
    return true;
}
function clarifyInforamtion() {
    debugger;
    var IsChkYandNallQuestionValue = ChkYandNallQuestionValue();
    var IsCheckQuestionDetails = CheckQuestionDetailsIsNullOrEmpty();
    var ChkMobileNumberContainCountryCode = CheckMobileNumberContainCountryCode();
    chkOtherLangList();
    var ismodule = CheckPropField();
    if (ChkMobileNumberContainCountryCode == true) {
        $("#IsChangedPersonalDetails").val("True");
    }
    if (ismodule == true && IsCheckQuestionDetails == true && IsChkYandNallQuestionValue == true && ChkMobileNumberContainCountryCode == true) {
        var checkinfo = $("#assurance").prop("checked")
        if (HelthChk == true && checkinfo == false) {
            globalFunctions.showErrorMessage(translatedResources.CheckCertify)
        }
        else if (HelthChk == false && checkinfo == false) {
            if ($("#IsChangedGeneral").val() == "False" && $("#IsChangedPersonalDetails").val() == "False" && $("#IsChangedHealth").val() == "False" && $("#IsChangedGeneralConsent").val() == "False") {
                globalFunctions.showSuccessMessage(translatedResources.NoChangesMsg)
            }
            else {
                $('#frmUpdateStudentDetails').submit()
            }
        }
        else if (HelthChk == true && checkinfo == true) {
            if ($("#IsChangedGeneral").val() == "False" && $("#IsChangedPersonalDetails").val() == "False" && $("#IsChangedHealth").val() == "False" && $("#IsChangedGeneralConsent").val() == "False") {
                globalFunctions.showSuccessMessage(translatedResources.NoChangesMsg)
            }
            else {
                $('#frmUpdateStudentDetails').submit()
            }
        }
        else {
            globalFunctions.showErrorMessage(translatedResources.TechnicalError)
        }
    }

}

//    var checkinfo = $("#assurance").prop("checked")
//    if (checkinfo == false)
//    {
//        globalFunctions.showErrorMessage("please check clarifyInforamtion")
//        //alert("please check clarifyInforamtion");
//        preventDefault();
//    }
//}
$('#CompanyDropDownList').change(function () {
    var CompanyId = $("#CompanyDropDownList").val();
    if (CompanyId == '151') {
        $('#OtherComapnyTextdiv').show();
    }
    else {
        $('#OtherComapnyTextdiv').css('display', 'none');
        $('#OtherComapnyText').val('');

    }
})
$('#MCompanyDropDownList').change(function () {
    var CompanyId = $("#MCompanyDropDownList").val();
    if (CompanyId == '151') {
        $('#MOtherComapnyTextdiv').show();
    }
    else {
        $('#MOtherComapnyTextdiv').css('display', 'none');
        $('#MOtherComapnyText').val('');

    }
})
$('#GCompanyDropDownList').change(function () {
    var CompanyId = $("#GCompanyDropDownList").val();
    if (CompanyId == '151') {
        $('#GOtherComapnyTextdiv').show();
    }
    else {
        $('#GOtherComapnyTextdiv').css('display', 'none');
        $('#GOtherComapnyText').val('');

    }
})
$('#FeeSponserCompanyVisiable').change(function () {
    var feesponserid = $("#FeeSponserCompanyVisiable").val();
    if (feesponserid == "4" || feesponserid == "5") {
        //$(".mediaclass_t").show();
        $(".MediaCompanyVisiable").show();
    }
    else {
        //$(".mediaclass_t").hide();
        $(".MediaCompanyVisiable").hide();
        $('#MediaOtherComapnyTextdiv').css('display', 'none');
        $('#MediaComapnyText').val('');
    }
})
$("#MediaCompanyVisiable").change(function () {
    var CompanyId = $("#MediaCompanyVisiable").val();
    if (CompanyId == '151') {
        $('#MediaOtherComapnyTextdiv').show();
    }
    else {
        $('#MediaOtherComapnyTextdiv').css('display', 'none');
        $('#MediaComapnyText').val('');

    }
})
$('#OtherNonInfeciousdetail').change(function () {
    var propInfecious = $("#OtherNonInfeciousdetail ").prop("checked")
    if (propInfecious == true) {
        $('#FamilyhistoryotherdetailTextdiv').show();
    }
    else {
        $('#FamilyhistoryotherdetailTextdiv').hide();
        $('#FamilyhistoryotherdetailText').val('');

    }
})

//$('#Allergiestxtshow').change(function () {
//    var CompanyId = $("#Allergiestxtshow").val();
//    if (CompanyId == true) {

//    }
//    else {


//    }
//})
$('.Showtxtarea').change(function () {
    debugger;
    //var temp = $(this).prop("checked");
    var temp = $(this).val();
    var cls = $(this).attr("data-custom");
    if (temp == "True") {
        $('.' + cls).filter(function () { $(this).removeClass('hide-item'); })
    }
    else {
        $('.' + cls).filter(function () { $(this).addClass('hide-item'); })
        $('.T-' + cls).val("");
    }
})
$(document).ready(function () {
    $('#FamilyhistoryotherdetailTextdiv').hide();

    if ($("#OtherNonInfeciousdetail ").prop("checked")) {
        $('#FamilyhistoryotherdetailTextdiv').show();
    }

    $(".IsDateChanged").on('dp.change', function (e) { chkModelChangedDetails(1) })


    ////Check mobole no contain country code or not 
    $('.ChkMobileNoValid').change(function () {
        var mobileNo = $(this).val().replaceAll(" ", "").replaceAll("-", "");
        if (mobileNo.length == 11 && $(this).val().match(/^[0-9]+$/) != null) {
            var countryCode = $(this).parent().find(".iti__selected-flag").attr("data-countrycode");
            if (countryCode == undefined) {
                globalFunctions.showErrorMessage("Select country code.");
                result = false;
            }
            else {
                $("#IsChangedPersonalDetails").val("True");
            }
        }
        else {
            globalFunctions.showErrorMessage("Invalid mobile number.");
        }

    });
});


function chkOtherLangList() {
    var otherLanguageList = '';
    $('input[type=checkbox][name=OtherLangcls').each(function () {
        if (this.checked) {
            otherLanguageList += "" + $(this).val() + "|";
        }
    });
    $('#MultipleOtherLanguage').val('');
    $('#MultipleOtherLanguage').val(otherLanguageList);
}

function chkModelChangedDetails(Id) {
    if (Id == 1) {
        $("#IsChangedGeneral").val(true);
    }
    if (Id == 2) {
        $("#IsChangedPersonalDetails").val(true);
    }
    if (Id == 3) {
        $("#IsChangedHealth").val(true);
    }
    if (Id == 4) {
        $("#IsChangedGeneralConsent").val(true);
    }
}


function CheckQuestionDetailsIsNullOrEmpty() {
    debugger;
    if ($("#Healthdetails_Allergies1").is(":checked") == true && $("#Healthdetails_Allergiestxt").val().trim() == "") {
        globalFunctions.showErrorMessage(translatedResources.AllergiesMsg)
        return false;
    }
    else if ($("#Healthdetails_SpecialMedication1").is(":checked") == true && $("#Healthdetails_SpecialMedicationtxt").val().trim() == "") {
        globalFunctions.showErrorMessage(translatedResources.SpecialMedicationMsg)
        return false;
    }
    else if ($("#Healthdetails_EducationRestrictions1").is(":checked") == true && $("#Healthdetails_EducationRestrictionstxt").val().trim() == "") {
        globalFunctions.showErrorMessage(translatedResources.EducationRestrictionMsg)
        return false;
    }
    else if ($("#Healthdetails_VisualDisability1").is(":checked") == true && $("#Healthdetails_VisualDisabilitytxt").val().trim() == "") {
        globalFunctions.showErrorMessage(translatedResources.VisualDisabilityMsg)
        return false;
    }
    else if ($("#Healthdetails_SpecialEducation1").is(":checked") == true && $("#Healthdetails_SpecialEducationtxt").val().trim() == "") {
        globalFunctions.showErrorMessage(translatedResources.SpecialEducationMsg)
        return false;
    }
    else if ($("#Healthdetails_HealthIssue1").is(":checked") == true && $("#Healthdetails_healthissueTextArea1").val().trim() == "") {
        globalFunctions.showErrorMessage(translatedResources.HealthIssueMsg)
        return false;
    }
    return true;
}


function ChkYandNallQuestionValue() {
    $('.questionYesNoValue').each(function () {
        var value = $(this).attr("data-details");
        if (!$("#QuestionNo_" + value).is(":checked") && !$("#QuestionYes_" + value).is(":checked")) {
            globalFunctions.showErrorMessage(translatedResources.IllnessMsg.replace('{0}', $(this).attr("data-question")))
            return false;
        }
    });
    return true;
}


//Check mobile no contain country code or not 
function CheckMobileNumberContainCountryCode() {
    var result = true;
    debugger;
    for (var no = 1; no <= 3; no++) {
        $('.phoneNumber_' + no).each(function () {
            var countryCode = $(this).parent().find(".iti__selected-flag").attr("data-countrycode");
            var mobileNo = $(this).val().replaceAll(" ", "").replaceAll("-", "");
            var onlyMobileNo = mobileNo.replace("+" +countryCode, "");
            if (onlyMobileNo.length == 11 && onlyMobileNo.match(/^[0-9]+$/) != null) {            
            if (countryCode == undefined) {
                globalFunctions.showErrorMessage("Select country code.");
                result = false;
            }
            else {
                var IscountryCodeExist = mobileNo.includes("+" + countryCode);
                if (IscountryCodeExist == false) {
                    $(this).val("+" + countryCode.trim() + mobileNo.trim());
                    result = true;
                }
            }
            }
            else {
                globalFunctions.showErrorMessage("Invalid mobile number." + onlyMobileNo);
                result = false;
            }
        });
        if (!result) {
            return result;
        }
    }
    //$("#IsChangedPersonalDetails").val("True");
    return result;
}

