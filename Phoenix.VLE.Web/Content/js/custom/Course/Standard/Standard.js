﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />


var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var Standard = function () {
    var init = function () {
        loadStandardGrid();
        $("#tbl-StandardList_filter").hide();
    },
        addStandard = function (source, StandardId) {
            loadCreatePopup(source, translatedResources.add, StandardId);
        },
        editStandard = function (source, StandardId) {
            loadCreatePopup(source, translatedResources.edit, StandardId);
        },
        LinkForStandardDetails = function (source, StandardId) {
            window.location.href = "/Course/StandardDetails/Index?StandardId=" + StandardId;
            //$.post('/StandardDetails/GetStandardDetailsList', { MSyllabusId: StandardId }, function () {

            //});
        },
        loadCreatePopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.Title;
            globalFunctions.loadPopup(source, '/Course/Standard/AddEditStandard?StandardId=' + id, title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: "Select"
                });

                if ($('#hdnParentID').val() != "0" && $('#hdnParentID').val() != undefined) {
                    $('#divParentTopic').fadeIn("slow");
                }
                else
                    $('#divParentTopic').fadeOut("slow");

            });

        },
        onSaveStandardSuccess = function (isAddmode) {
            // Standard.init();
            if (isAddmode == 'True') {
                globalFunctions.showSuccessMessage(translatedResources.AddSuccess);
            } else {
                globalFunctions.showSuccessMessage(translatedResources.EditSuccess);
            }
            $("#myModal").modal('hide');
            Standard.init();
        },
        onSaveStandardError = function (isAddmode) {
            init();
            if (isAddmode == 'True') {
                globalFunctions.showErrorMessage(translatedResources.AddError);
            } else {
                globalFunctions.showErrorMessage(translatedResources.EditError);
            }
            $("#myModal").modal('hide');
        }
    loadStandardGrid = function () {
        var _columnData = [];
        _columnData.push(
            //{ "mData": "GroupName", "sTitle": translatedResources.CourseName, "sWidth": "30%", "sClass": "wordbreak" },
            //{ "mData": "Description", "sTitle": translatedResources.Description, "sWidth": "30%", "sClass": "wordbreak" },
            //{ "mData": "ParentGroupName", "sTitle": translatedResources.TremName, "sWidth": "20%", "sClass": "wordbreak" },
            //{ "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "20%" }
            { "mData": "CourseName", "sTitle": translatedResources.GroupName, "sWidth": "30%", "sClass": "wordbreak" },
            { "mData": "Description", "sTitle": translatedResources.Description, "sWidth": "30%", "sClass": "wordbreak" },
            //{ "mData": "TremName", "sTitle": translatedResources.ParentTopic, "sWidth": "20%", "sClass": "wordbreak" },
            { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "20%" }

            //,{ "mData": "ActionsForLink", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "20%" }

        );
        initStandardGrid('tbl-StandardList', _columnData, '/Course/Standard/GetStandardMasterList');
    },
        initStandardGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnDefs: [{
                    'orderable': false,
                    'targets': [1, 2]
                }],
                
                columnSelector: false
            };
            grid.init(settings);
        },
        deleteStandard = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('Standard', '/Course/Standard/DeleteStandard', 'tbl-StandardList', source, id);
            }
        }


    return {
        init: init,
        addStandard: addStandard,
        loadCreatePopup: loadCreatePopup,
        onSaveStandardSuccess: onSaveStandardSuccess,
        onSaveStandardError: onSaveStandardError,
        loadStandardGrid: loadStandardGrid,
        editStandard: editStandard,
        deleteStandard: deleteStandard,
        LinkForStandardDetails: LinkForStandardDetails

    };
}();
$(document).ready(function () {
    Standard.init();
    $('#tbl-StandardList').DataTable().search('').draw();
    $(document).on('click', '#btnAddStandard', function () {
        Standard.addStandard($(this));
    });
    $("#StandardSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-StandardList').DataTable().search($(this).val()).draw();

    });

});
function showHideParentDDL(obj) {
    if ($(obj).is(':checked')) {
        $('#ParentID').show();
        $('#divParentTopic').fadeIn("slow");
        $('.selectpicker').selectpicker('refresh');
    }
    else {
        $('#ParentID').hide();
        $('#divParentTopic').fadeOut("slow");
    }
}