﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />

var StandardBank = function () {
    LessonArray = [];
    init = function () {
        $(".selectpicker").selectpicker('refresh');
        loadStandardBankGrid();
    },
        loadStandardBankGrid = function () {
            $.get('/Course/Standard/GetStandardBankList').then((response) => {
                $('#divStandardBankList').html('');
                $('#divStandardBankList').html(response);
                $("#hdnStandardBankId").val('');

            }).catch((error) => {
                globalFunctions.showMessage(error.NotificationType, error.Message)
            });
        },

        EditStandardBank = function (source, StandardBankId) {
        loadCreatePopup(source, 'StandardEdit', translatedResources.StandardEdit, StandardBankId);
            $("#hdnStandardBankId").val(StandardBankId);
        },

        EditUnitOrSubUnit = function (source, editType, StandardBankId) {
        var mode = editType == "UnitEdit" ? translatedResources.UnitEdit : translatedResources.SubUnitEdit;
        loadCreatePopup(source, editType, mode, StandardBankId);
        $("#hdnStandardBankId").val(StandardBankId);
        },
        DeleteStandardBank = function (StandardBankId) {
        globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
            var mStandardBank =
            {
                SB_Id: StandardBankId,
                StandardDescription: ''
            }
            $.post('/Course/Standard/AddEditStandardBank', { standardModel: mStandardBank, TransMode: 2, EditType: 'StandardDelete' }).then((response) => {
                globalFunctions.showMessage(response.NotificationType, response.Message);
                if (response.Success) {
                    loadStandardBankGrid();
                }

            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
        });
        }
    DeleteUnitOrSubUnit = function (source, deleteType, StandardBankId) {
        globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
            var mStandardBank =
            {
                SB_Id: StandardBankId,
                StandardDescription: ''
            }
            $.post('/Course/Standard/AddEditStandardBank', { standardModel: mStandardBank, TransMode: 2, EditType: deleteType }).then((response) => {
                globalFunctions.showMessage(response.NotificationType, response.Message);
                if (response.Success) {
                    loadStandardBankGrid();
                }

            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
        });
    }
    

    $(document).on('click', '#btnLesson', function () {
        if (common.IsInvalidByFormOrDivId('frmLessonPlanner')) {
            var mStandardBank =
            {
                SB_Id: $('#SB_Id').val(),
                StandardDescription: $('#StandardDescription').val(),
                UnitName: $('#UnitName').val(),
                SubUnitName:$('#SubUnitName').val()
            }
            
            $.post('/Course/Standard/AddEditStandardBank', {
                standardModel: mStandardBank, TransMode: 1, EditType: $('#edittype').val() }).then((response) => {
                globalFunctions.showMessage(response.NotificationType, response.Message);
                if (response.Success) {
                    $("#myModal").modal('hide');
                    loadStandardBankGrid();
                }

            }).catch((error) => {
                $("#myModal").modal('hide');
                globalFunctions.showMessage(error.NotificationType, error.Message)
            });
        }
    });
    btnAddStandardForAddUnit = function (source) {
        var title = translatedResources.add + ' ' + 'Standard' // translatedResources.Title;
        globalFunctions.loadPopup(source, '/Course/Standard/AddEditStandardBankForAddUnitDetails', title, 'addtask-dialog modal-lg');
        $(source).off("modalLoaded");
        $('#formModified').val(true);
        $(source).on("modalLoaded", function () {
            $('.date-picker').datetimepicker({
                format: 'DD-MMM-YYYY'
            })
                .on('dp.change', function (e) {
                    $(`#StartDate`).data("DateTimePicker").maxDate(e.date);
                    $(`#EndDate`).data("DateTimePicker").minDate(e.date);
                    $('#EndDate').next('label').addClass("active");
                });
            $(".selectpicker").selectpicker('refresh');
        });
    }

    addStandard = function (source, StandardId) {
        
        loadCreatePopup(source, translatedResources.add, StandardId);
    },
        loadCreatePopup = function (source,editType, title, id) {        
        // translatedResources.Title;
        globalFunctions.loadPopup(source, '/Course/Standard/AddEditStandardBank?StandardBankId=' + id + '&AddStaus=1&editType=' + editType, title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
        $(source).on("modalLoaded", function () {
            $(".selectpicker").selectpicker('refresh');
        });

        },
        GetUnitBasedonCourse = function (CourseId) {
            common.bindSingleDropDownByParameter(
                "#ddlUnitId",
                '/Course/Standard/GetUnitBasedonCourse',
                { CourseId: CourseId },
                "",
                ''
            );
        }
    return {
        init,
        EditStandardBank,
        EditUnitOrSubUnit,
        DeleteUnitOrSubUnit,
        DeleteStandardBank,
        addStandard: addStandard,
        btnAddStandardForAddUnit: btnAddStandardForAddUnit,
        GetUnitBasedonCourse: GetUnitBasedonCourse
    }
}();
$(document).ready(function () {

    StandardBank.init();
    $(document).on('click', '#btnAddStandard', function (id) {
        debugger
        var id = $(this).data("value");
        $('#hdnStandardBankId').val(id);
        if (id == undefined) {
            AddStatus = 0;
        }
        StandardBank.addStandard($(this));

    });
    $(document).on('click', '#btnAddStandardForAdd', function (id) {
        debugger
        $('#hdnStandardBankId').val('');
        StandardBank.btnAddStandardForAddUnit($(this));

    });
 
});
