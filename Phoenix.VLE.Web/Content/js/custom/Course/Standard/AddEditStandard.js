﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />


var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var addEditStandard = function () {
    debugger
    var init = function () {
        //if ($('#ParentID').val() != "0" && $('#ParentID').val() != undefined && $('#ParentID').val() !="")
        //    $('#chkIsSubTopic').prop('checked', true);
        //else $('#chkIsSubTopic').prop('checked', false);
    },
        saveStandard = function (e) {
            debugger
            if ($('form#frmAddEditStandard').valid()) {
                var formData = new FormData();
                debugger
                var token = $('input[name=__RequestVerificationToken]').val();
                formData.append("__RequestVerificationToken", token);
                formData.append("StandardID", $('#StandardID').val());
                formData.append("IsAddMode", $('#IsAddMode').val());
                formData.append("Description", $('#Description').val());
                // formData.append("GroupName", $('#GroupName').val());
                formData.append("Courseid", $('#Courseid').val());
                //formData.append("Termid", $('#Term').val());

                //if ($('#chkIsSubTopic').is(':checked'))
                //{
                //  formData.append("ParentID", $('#ParentID').val());
                //}
                //else
                //{
                //  formData.append("ParentID", "0");
                //}

                submitStandard(formData);

            }
        },
        submitStandard = function (formdata) {
            debugger

            console.log(JSON.stringify(formdata));
            $.ajax({
                type: 'POST',
                url: '/Course/Standard/SaveStandardDetails',
                data: formdata,
                processData: false,
                contentType: false,
                success: function (result) {
                    globalFunctions.showMessage(result.NotificationType, result.Message);
                    if (result.Success) {

                        Standard.init();
                        Standard.loadStandardGrid();
                        $("#myModal").modal("hide");
                    }
                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });
        }

    return {
        init: init,
        saveStandard: saveStandard,
        submitStandard: submitStandard
    };
}();

$(document).ready(function () {
    formElements.feSelect();
    addEditStandard.init();
    debugger
});