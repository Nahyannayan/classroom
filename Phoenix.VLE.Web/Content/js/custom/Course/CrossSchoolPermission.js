﻿//------------------------Functionality Part-1------------------------
var objTCSPermission = function () {
    var InitGrid = function () {
        objTCSPermission.GetCrossSchoolPermissionGrid(0);
    },
        GetGradeListBySchoolId = function (SchoolIds, TeacherId) {
            //var GradeIds = $("#ddlTCSGrade").val();
            //var result = common.bindSingleDropDownByParameter(
            //    "#ddlTCSGrade",
            //    '/Course/Course/GetGradeListBySchoolId',
            //    { SchoolIds: SchoolIds, TeacherId: TeacherId },
            //    "",
            //    ''
            //);
            //var IsEditMode = $("#IsEditMode").val();
            //if (IsEditMode.toBoolean() && result.length >= 0) {
            //    $("#ddlTCSGrade").val(GradeIds);
            //    $("#ddlTCSGrade").selectpicker('refresh');
            //}
        },
        AddEditCrossSchoolPermission = function (source, tcsid) {
            isItFormOrPopUp = true;
            globalFunctions.loadPopup($(source), "/Course/Course/AddEditCrossSchoolPermission?tcsid=" + tcsid, tcsid ? translatedResources.edit : translatedResources.add, 'addtask-dialog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $('select').selectpicker({
                    noneSelectedText: translatedResources.selectedText
                });
                $('.date-picker').datetimepicker({
                    format: 'DD-MMM-YYYY',
                    extraFormats: ['MM/DD/YYYY LT', 'DD/MM/YYYY LT', 'DD/MMM/YYYY LT', 'DD-MM-YYYY', 'MM-DD-YYYY']
                })
            });
        },
        SaveCrossSchoolPermission = function () {
            var IsValid = common.IsInvalidByFormOrDivId("frmCrossSchoolPermission");

            if (IsValid) {
                var TCS_ID = $("#TCS_ID").val();
                var TeacherId = $("#ddlTCSTeacher").val();
                var SchoolIds = $("#ddlTCSSchool").val();
                var IsEditMode = $("#IsEditMode").val();
                var crossSchoolPermission = {
                    TCS_ID: TCS_ID,
                    TeacherId: TeacherId,
                    SchoolIds: SchoolIds.join(','),
                    IsEditMode: IsEditMode
                };
                $.post("/Course/Course/SaveCrossSchoolPermission", { crossSchoolPermission }, function (response) {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    if (response.Success) {
                        objTCSPermission.InitGrid();
                    }
                });
            }
        },
        GetCrossSchoolPermissionGrid = function (TeacherId) {
            $.post("/Course/Course/GetCrossSchoolPermissionGrid", { TeacherId }).then(function (response) {
                $("#divTCSPermissionGrid").html('');
                $("#divTCSPermissionGrid").html(response);
                $("#tbl-TCSPermissionGrid").DataTable();
                $("#tbl-TCSPermissionGrid_filter,#tbl-TCSPermissionGrid_length").hide();
                $('#tbl-TCSPermissionGrid').DataTable().search('').draw();
            });
        },
        deleteCrossSchoolPermission = function (id) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                var token = $('input[name="__RequestVerificationToken"]').val();
                $.post('/Course/Course/SaveCrossSchoolPermission', {
                    TCS_ID: id,
                    IsDelete: true,
                    __RequestVerificationToken: token,
                })
                    .then(response => {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        if (response.Success) {
                            objTCSPermission.GetCrossSchoolPermissionGrid(0);
                        }
                    })
            })
        },
        onSuccess = function (response) {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success) {
                $('#myModal').modal('hide');
                objTCSPermission.InitGrid();
            }
        }
    return {
        InitGrid,
        GetGradeListBySchoolId,
        AddEditCrossSchoolPermission,
        SaveCrossSchoolPermission,
        GetCrossSchoolPermissionGrid,
        deleteCrossSchoolPermission,
        onSuccess
    }
}();
//---------------Click/Change Event Functionality Part-1---------------
//$(document).on('hide.bs.select', '#ddlTCSSchool', function () {
//    debugger
//    var SchoolIds = $(this).val();
//    var IsEditMode = $("#IsEditMode").val();// "True"
//    var TeacherId = $("#ddlTCSTeacher").val();
//    if (IsEditMode.toBoolean()) {
//        TeacherId = '0';
//    }
//    if (TeacherId != '') {
//        objTCSPermission.GetGradeListBySchoolId(SchoolIds.join(','), TeacherId);
//    }
//    else {
//        globalFunctions.showMessage("error", "Please select teacher.");
//    }
//});

//---------------------------------------------------------------------
$(document).on('dp.change', 'input[data-mindateid],input[data-maxdateid]', function (e) {
    if (e.date) {
        try {
            if (globalFunctions.isValueValid($(this).data('mindateid')))
                $(`#${$(this).data('mindateid')}`).data("DateTimePicker").maxDate(e.date);

            else if (globalFunctions.isValueValid($(this).data('maxdateid')))
                $(`#${$(this).data('maxdateid')}`).data("DateTimePicker").minDate(e.date);

        } catch (e) {

        }
    }
}).on("keyup","#searchTeacherTemplate", function (e) {
    e.preventDefault();
    $('#tbl-TCSPermissionGrid').DataTable().search($(this).val()).draw();
});;