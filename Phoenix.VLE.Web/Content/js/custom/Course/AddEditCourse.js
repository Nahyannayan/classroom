﻿var isFormSubmit = false;
var addEditCourse = function () {
    var init = function () {
        var tab = globalFunctions.getParameterValueFromQueryString("tab");

        if (tab == "unitcalendar") {
            $('a.unitcalendar').click();
        }
        else if (tab == "coursedescription") {
            $('a.coursedescription').click();
        }
        else if (tab == "curriculummap") {
            $('a.curriculummap').click();
        }
        else {
            $('#divCourseDescription').show();
        }

        $(document).on('focus', '#CourseHours', function (e) {
            $('#CourseHours').next('label').addClass("active");
        });
        $(document).on('blur', '#CourseHours', function (e) {
            if ($('#CourseHours').val() == "") { $('#CourseHours').next('label').removeClass("active"); }
        });
    },
        onSaveSuccess = function (response) {
           
            var EncryptedId = response.Message;
            if (Number($('#CourseId').val()) == 0) {
                globalFunctions.showMessage("success", translatedResources.SuccessMessage);

                //location.href = "/Course/Course/AddEditCourse?id=" + EncryptedId;
                $('#divCourseNabTab').html(response.RelatedHtml);
            }
            else {
                globalFunctions.showMessage("success", translatedResources.UpdateMessage);
            }
            $.get("/Course/Course/AddEditCourseById?id=" + EncryptedId).then(function (resp) {
                $('#divCourseDescription').html(resp);
                isFormSubmit = true;
                $('.selectpicker').selectpicker('refresh');
            });
            // Construct URLSearchParams object instance from current URL querystring.
            var queryParams = new URLSearchParams(window.location.search);

            // Set new or modify existing parameter value. 
            queryParams.set("id", EncryptedId);

            // Replace current querystring with the new one.
            history.replaceState(null, null, "?" + queryParams.toString());
        },
        initializeCourseDescriptionControls = function () {

            $('.selectpicker').selectpicker('refresh');
            var title = $('#Title').val();
            $('#divCourseTitle').attr("title", title);
            if (title.length > 50) {
                $('#divCourseTitle').html(title.substring(0, 50) + "...");
            }
            else
                $('#divCourseTitle').html(title);


            $('#Title').keyup(function () {
                var title = $(this).val();
                if (title.length > 50) {
                    $('#divCourseTitle').html(title.substring(0, 50) + "...");
                }
                else
                    $('#divCourseTitle').html(title);
                $('#divCourseTitle').attr("title", title);
            });


            $('.date-picker').datetimepicker({
                format: 'DD-MMM-YYYY',
                locale: translatedResources.locale
            })
                .on('dp.change', function (e) {

                    if ($(`#strEndDate`).val() != "") {
                        $(`#strStartDate`).data("DateTimePicker").maxDate(moment($(`#strEndDate`).val(), "DD-MMM-YYYY")._d);
                    }
                    
                });
            $('#strEndDate').on('focus', function () {
                $(`#strEndDate`).data("DateTimePicker").minDate(moment($(`#strStartDate`).val(), "DD-MMM-YYYY")._d);
            });
            
            if ($(`#strEndDate`).val() != "") {
                $(`#strStartDate`).data("DateTimePicker").maxDate(moment($(`#strEndDate`).val(), "DD-MMM-YYYY")._d);
                $(`#strEndDate`).data("DateTimePicker").minDate(moment($(`#strStartDate`).val(), "DD-MMM-YYYY")._d);
            }
            $('#AjaxLoader').hide();
        },
        checkcourseImageSize = function () {
            return new Promise(function (resolve, reject) {


                if (!$('#frmAddEditCourse').valid()) {


                    if (!globalFunctions.isValueValid($('#Description').val())) {

                        if ($('#Description').val().trim().length != $('#Description').val().length) {
                            $('span[data-valmsg-for="Description"]').html('<span class="field-validation-error">' + translatedResources.NoWhiteSpace + '</span>');
                        }
                        else
                            $('span[data-valmsg-for="Description"]').html('<span class="field-validation-error">' + translatedResources.Mandatory + '</span>');
                        return resolve(false);
                    }
                    else {
                        $('span[data-valmsg-for="Description"]').html('');
                    }
                    return resolve(false);
                }


                if (moment($('#strStartDate').val()) > moment($('#strEndDate').val())) {
                    globalFunctions.showWarningMessage(`Course start date should be less than course end date`);
                    return resolve(false);
                }

                if ($('#CourseHours').val().length == 0) {

                    $('span[data-valmsg-for="CourseHours"]').html(`<span class="field-validation-error">Please enter a valid number</span>`);
                    $('#CourseHours').val("");
                    $('#CourseHours').focus();
                    return resolve(false);
                }

                var flag = true;
                if ($('#CourseHours').val() != "") {

                    if (isNaN($('#CourseHours').val())) {
                        $('span[data-valmsg-for="CourseHours"]').html(`<span class="field-validation-error">Please enter a valid number</span>`);
                        $('#CourseHours').val("");
                        flag = false;
                    }
                    else
                        if (Number($('#CourseHours').val()) <= 0) {
                            $('span[data-valmsg-for="CourseHours"]').html(`<span class="field-validation-error">Please enter a value greater than  zero</span>`);
                            $('#CourseHours').val("");
                            flag = false;
                        }
                        else {
                            $('span[data-valmsg-for="CourseHours"]').html('');
                        }
                }


                if ($('#MaximumEnrollment').val() != "") {
                    if (Number($('#MaximumEnrollment').val()) <= 0) {
                        $('span[data-valmsg-for="MaximumEnrollment"]').html('<span class="field-validation-error">Please enter a value greater than  zero</span>');
                        $('#MaximumEnrollment').val("");
                        flag = false;
                    }
                    else {
                        $('span[data-valmsg-for="MaximumEnrollment"]').html('');
                    }
                }
                if (!flag)
                    return resolve(false);

                var _URL = window.URL || window.webkitURL;
                var input = $('input[name="CourseImage"]')[0];
                var file, img;
                if (input.files.length > 0) {
                    if ((file = input.files[0])) {
                        var $allowExtns = new Array("gif", "png", "jpg", "jpeg");
                        var $ext = input.value.split(".").pop().toLowerCase();
                        if ($.inArray($ext, $allowExtns) == -1) {
                            globalFunctions.showWarningMessage(`Only image files are accepted,Please select image type of ${$allowExtns.join(', ')}`);

                            return resolve(false);
                        }
                        img = new Image();
                        var width = parseInt($(input).data('min-width'));
                        var height = parseInt($(input).data('min-height'));
                        img.onload = function () {
                            if (this.width < width && this.height < height) {
                                globalFunctions.showWarningMessage(`Image height should be greate than ${height} px and width should be greater than ${width}`);
                                return resolve(false);
                            }
                            else {
                                return resolve(true);
                            }
                        };
                        img.src = _URL.createObjectURL(file);
                    }
                } else {
                    return resolve(true);
                }
            })
        },
        onFailure = function () {
            $("#btnAddEditCourse").prop('disabled', false);
            globalFunctions.onFailure();
        }
    return {
        onSaveSuccess,
        initializeCourseDescriptionControls,
        checkcourseImageSize,
        init,
        onFailure
    };
}();

$(document).on('hidden.bs.select', '#Groupids', function () {
    common.postRequest('/Course/Course/GetStudentsinGroup', { id: $(this).val() })
        .then(response => {
            common.bindDropDown('#StudentId', response, 'Text', 'Value');
        });
});
//$(document).ready(function () {
//    $(document).on('click', '#btnAddEditCourse', function (e) {
//        var form = $('#frmAddEditCourse')[0];
//        var formData = new FormData(form);
//        console.log(form);
//        if ($('#frmAddEditCourse').valid()) {
//            $.post({
//                url: "/Course/Course/CreateCourse",
//                data: formData,
//                contentType: false,
//                processData: false,
//                success: function (response) {
//                    if (Number($('#CourseId').val()) == 0) {
//                        globalFunctions.showMessage(response.NotificationType, translatedResources.SuccessMessage);
//                        var EncryptedId = response.Message;
//                        location.href = "/Course/Course/AddEditCourse?id=" + EncryptedId;
//                    }
//                    else {
//                        globalFunctions.showMessage("success", translatedResources.UpdateMessage);
//                    }

//                }
//            });
//        }
//        e.preventDefault();
//        e.stopImmediatePropagation();
//    });

//});
