﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var addEditStandardDetails = function () {
    var init = function () {
        if ($('#ParentID').val() != "0" && $('#ParentID').val() != undefined && $('#ParentID').val() != "")
            $('#chkIsSubTopic').prop('checked', true);
        else $('#chkIsSubTopic').prop('checked', false);
    },
        saveStandardDetails = function (e) {
            debugger
            //if (common.IsInvalidByFormOrDivId('frmAddEditStandardDetails')) {
            //if ($('form#frmAddEditStandardDetails').valid()) {
            var formData = new FormData();
            var token = $('input[name=__RequestVerificationToken]').val();
            formData.append("__RequestVerificationToken", token);
            formData.append("StandardDetailID", $('#StandardDetailID').val());
            //formData.append("Order", $('#Order').val());


            formData.append("StandardID", $('#hdnStandardId').val());
            formData.append("IsAddMode", $('#IsAddMode').val());
            formData.append("Description", $('#Description').val());
            //formData.append("Points", $('#Points').val());
            //formData.append("Order", $('#Order').val());
            //formData.append("Weight", $('#Weight').val());
            formData.append("ParentID", $('#ParentID').val());
            if ($('#chkIsSubTopic').is(':checked')) {
                formData.append("ParentID", $('#ParentID').val());
            }
            else {
                formData.append("ParentID", "0");
            }
            submitStandardDetails(formData);

            //}
        },
        submitStandardDetails = function (formdata) {
            $.ajax({
                type: 'POST',
                url: '/Course/StandardDetails/SaveStandardDetails',
                data: formdata,
                processData: false,
                contentType: false,
                success: function (result) {
                    globalFunctions.showMessage(result.NotificationType, result.Message);
                    if (result.Success) {
                        StandardDetails.init();
                        $("#myModal").modal("hide");
                    }

                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });
        }

    return {
        init: init,
        saveStandardDetails: saveStandardDetails,
        submitStandardDetails: submitStandardDetails
    };
}();

$(document).ready(function () {
    formElements.feSelect();
    addEditStandardDetails.init();
});