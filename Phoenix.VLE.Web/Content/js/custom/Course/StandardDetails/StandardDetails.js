﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />


var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var StandardDetails = function () {
    var init = function () {
        debugger
        var StandardId = $("#hdnStandardId").val();
        loadStandardDetailsGrid(StandardId);
        $("#tbl-StandardDetailsList_filter").hide();
    },
        addStandardDetails = function (source, StandardDetailsId) {
            loadCreatePopup(source, translatedResources.add, StandardDetailsId);
        },
        editStandardDetails = function (source, StandardDetailsId) {
            loadCreatePopup(source, translatedResources.edit, StandardDetailsId);
        },
        loadCreatePopup = function (source, mode, id) {
            debugger
            var title = mode + ' ' + translatedResources.Title;
            globalFunctions.loadPopup(source, '/Course/StandardDetails/AddEditStandardDetails?StandardDetailsId=' + id + '&StandardId=' + $("#hdnStandardId").val(), "Add Standard", 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: "Select"
                });
                if ($('#hdnParentID').val() != "0" && $('#hdnParentID').val() != undefined) {
                    $('#divParentTopic').fadeIn("slow");
                }
                else
                    $('#divParentTopic').fadeOut("slow");
            });

        },
        onSaveStandardDetailsSuccess = function (isAddmode) {
            // StandardDetails.init();
            if (isAddmode == 'True') {
                globalFunctions.showSuccessMessage(translatedResources.AddSuccess);
            } else {
                globalFunctions.showSuccessMessage(translatedResources.EditSuccess);
            }
            $("#myModal").modal('hide');
            StandardDetails.init();
        },
        LinkForLesson = function (source, StandardId) {
            debugger
            window.location.href = "/Course/Lesson/Index?StandardId=" + StandardId;

        },
        onSaveStandardDetailsError = function (isAddmode) {
            init();
            if (isAddmode == 'True') {
                globalFunctions.showErrorMessage(translatedResources.AddError);
            } else {
                globalFunctions.showErrorMessage(translatedResources.EditError);
            }
            $("#myModal").modal('hide');
        }
    loadStandardDetailsGrid = function (StandardId) {
        var _columnData = [];
        _columnData.push(
            { "mData": "Description", "sTitle": translatedResources.Description, "sWidth": "30%", "sClass": "wordbreak" },
            { "mData": "StandardGroupName", "sTitle": translatedResources.Parent, "sWidth": "30%", "sClass": "wordbreak" },
            //{ "mData": "parent", "sTitle": translatedResources.Order, "sWidth": "10%", "sClass": "wordbreak" },
            //{ "mData": "Points", "sTitle": translatedResources.Points, "sWidth": "10%", "sClass": "wordbreak" },
            //{ "mData": "Weight", "sTitle": translatedResources.Weight, "sWidth": "10%", "sClass": "wordbreak" },
            { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "10%" }
            //,
            //{ "mData": "ActionsForLink", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "20%" }
        );
        initStandardDetailsGrid('tbl-StandardDetailsList', _columnData, '/Course/StandardDetails/GetStandardDetailsList?StandardId=' + StandardId);
    },
        initStandardDetailsGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                //columnDefs: [{
                //    'orderable': false,
                //    'targets': 2
                //}],
                columnSelector: false
            };
            grid.init(settings);
        },
        deleteStandardDetails = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('StandardDetails', '/Course/StandardDetails/DeleteStandardDetails', 'tbl-StandardDetailsList', source, id);
            }
        }


    return {
        init: init,
        addStandardDetails: addStandardDetails,
        loadCreatePopup: loadCreatePopup,
        onSaveStandardDetailsSuccess: onSaveStandardDetailsSuccess,
        onSaveStandardDetailsError: onSaveStandardDetailsError,
        loadStandardDetailsGrid: loadStandardDetailsGrid,
        editStandardDetails: editStandardDetails,
        deleteStandardDetails: deleteStandardDetails,
        LinkForLesson: LinkForLesson

    };
}();
$(document).ready(function () {
    StandardDetails.init();
    $('#tbl-StandardDetailsList').DataTable().search('').draw();
    $(document).on('click', '#btnAddStandardDetails', function () {
        StandardDetails.addStandardDetails($(this));
    });
    $("#StandardDetailsSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-StandardDetailsList').DataTable().search($(this).val()).draw();

    });
});
function showHideParentDDL(obj) {
    if ($(obj).is(':checked')) {
        $('#ParentID').show();
        $('#divParentTopic').fadeIn("slow");
        $('.selectpicker').selectpicker('refresh');
    }
    else {
        $('#ParentID').hide();
        $('#divParentTopic').fadeOut("slow");
    }
}