﻿

var unitDetailConfig = function () {
    var init = function () {
        debugger
    },
        loadGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Title", "sTitle": translatedResources.title, "sWidth": "20%" },
                { "mData": "UnitGroupName", "sTitle": translatedResources.unitGroupName, "sWidth": "15%" },
                { "mData": "ControlType", "sTitle": translatedResources.controlType, "sWidth": "15%" },
                { "mData": "CourseTitle", "sTitle": translatedResources.CourseTitle, "sWidth": "40%" },
                { "mData": "Order", "sTitle": translatedResources.order, "sWidth": "5%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "bSorting": false, "sTitle": translatedResources.actions, "sWidth": "5%" }
            );
            initGrid('tblUnitDetailsGrid', _columnData, '/Course/Lesson/GetUnitDetailConfigGrid');
        },

        initGrid = function (_tableId, _columnData, _url) {

            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [3]
                }],
                searching: true
            };
            grid.init(settings);

            $("#tblUnitDetailsGrid_filter").hide();
            $('#tblUnitDetailsGrid').DataTable().search('').draw();
            $("#searchUnitDetailsGrid").on("input", function (e) {
                e.preventDefault();
                $('#tblUnitDetailsGrid').DataTable().search($(this).val()).draw();
            });
            $('#tblUnitDetailsGrid').off('DynamicGrid:InitComplete').on('DynamicGrid:InitComplete', function () {
                if (Array.from($('#tblUnitDetailsGrid').DataTable().data()).length <= 0) {
                    $('#breadcrumbActions').html(`<button type="button" id="btnTypeDetails" class="btn btn-primary btn-sm"><i class="fas fa-cog mr-2"></i>${translatedResources.assignUnitToSchool}</button>`)
                }
            })
        },
        deleteUnitConfig = function (id) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                var token = $('input[name="__RequestVerificationToken"]').val();
                $.post('/Course/Lesson/AddEditUnitDetailType', {
                    __RequestVerificationToken: token,
                    UnitDetailTypeId: id,
                    IsActive: false
                })
                    .then(response => {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        if (response.Success) {
                            loadGrid();
                        }
                    })
            })
        },
        loadUnitDetailTypeForm = function (source, untiDetailId) {
            source = $(source);
            globalFunctions.loadPopup(source, '/Course/Lesson/AddEditUnitDetailType?unitDetailTypeId=' + untiDetailId, translatedResources.addUnitConfig, 'file-dailog');
            source.off("modalLoaded");
            source.on("modalLoaded", function () {
                $('.selectpicker').selectpicker();
                $('#Title').on('change', function () {
                    $('#AttachmentKey').val($('#Title').val().trim().replaceAll(' ', ''))
                });
            });
        },
        onSaveSuccess = function (response) {
            debugger
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success) {
                $("#myModal").modal('hide');
                loadGrid();
                $('#breadcrumbActions').html('');
            }
        },
        showHideGroupDropDown = function () {
            $('#groupIdDiv,#groupNameDiv').toggle();
            var isgroupDropdownVisible = $('#groupIdDiv:visible').length > 0;
            $('#showHideGroupControl').attr('data-original-title',
                isgroupDropdownVisible ? translatedResources.addGroupName : translatedResources.selectGroupName);
            $('#showHideGroupControl i')[isgroupDropdownVisible ? 'addClass' : 'removeClass']('fa-plus');
            $('#showHideGroupControl i')[!isgroupDropdownVisible ? 'addClass' : 'removeClass']('fa-minus');

        },
        validateForm = function () {
            if (common.IsInvalidByFormOrDivId('frmAddEditUnitDetail')) {
                var isEdit = $('#UnitDetailTypeId').val() != "undefined";

                var isOrderExists;
                if (isEdit) {
                    var unitdetailTypeId = parseInt($('#UnitDetailTypeId').val());
                    isOrderExists = getTableData()
                        .filter(x => x.UnitGroupId == $("#UnitGroupId").val())
                        .some(x => x.Order == parseInt($('#Order').val()) && x.UnitDetailTypeId != unitdetailTypeId);
                } else {
                    isOrderExists = getTableData().filter(x => x.UnitGroupId == $("#UnitGroupId").val()).some(x => x.Order == parseInt($('#Order').val()));
                }

                if (isOrderExists) {
                    globalFunctions.showWarningMessage(translatedResources.orderAlreadyExists);
                    return false;
                }
                if (checkIfControlTypeAlreadyExists()) {
                    globalFunctions.showWarningMessage(translatedResources.controlAlreadyExists);
                    return false;
                }
                return true;
            } else {
                return false;
            }
        },
        checkIfControlTypeAlreadyExists = function () {
            var controlsToCheck = Array.from($('#ControlType option')
                .map((i, x) => $(x).val()))
                .filter(x => x.toLowerCase() != "textarea" && x != translatedResources.selectedText);
            var onTimeElement = $('#ControlType').val();
            var isEdit = $('#UnitDetailTypeId').val() != "undefined";

            var isControlExists = false;
            if (controlsToCheck.some(x => x == onTimeElement)) {
                if (isEdit) {
                    var unitdetailTypeId = parseInt($('#UnitDetailTypeId').val());
                    isControlExists = getTableData()
                        .some(x => x.ControlType == onTimeElement && x.UnitDetailTypeId != unitdetailTypeId);
                } else {
                    isControlExists = getTableData().some(x => x.ControlType == onTimeElement);
                }
            }

            return isControlExists;
        },
        getTableData = function () {
            return Array.from($('#tblUnitDetailsGrid').DataTable().data());
        },
        loadCourseList = function () {
            var CurriculumId = $('#ddlCurriculumId').val();
            var result = common.bindSingleDropDownByParameter(
                "#UnitCourseId",
                '/Course/Lesson/GetCourseList',
                { CurriculumId: CurriculumId }
            );
        }
    return {
        init,
        loadGrid,
        initGrid,
        deleteUnitConfig,
        loadUnitDetailTypeForm,
        onSaveSuccess,
        showHideGroupDropDown,
        validateForm,
        checkIfControlTypeAlreadyExists,
        getTableData,
        loadCourseList
    }
}();

var unitDetailTypeConfig = function () {
    var init = function () {

    },
        loadDivision = function () {
            var curriculum = $('#CurriculumId').val();
            common.bindSingleDropDownByParameter('#DivisionId', '/SchoolInfo/Division/GetDivisionUsingCurriculum', { curriculumId: curriculum }, '', translatedResources.selectedText);
        }
    return {
        init,
        loadDivision
    }
}();

$(document).ready(function () {

}).on('change', '#CurriculumId', function () {
    unitDetailTypeConfig.loadDivision();
}).on('change', '#DivisionId', function () {
    debugger
    var value = $(this).val();
    $('#ddlUnitDetailsTypeIdsDiv').html('')
    if (globalFunctions.isValueValid(value)) {
        $.get('/Course/Lesson/GetUnitDetailDropdown', { divisionId: value })
            .then(response => $('#ddlUnitDetailsTypeIdsDiv').html(response).find('select').selectpicker());
    }
}).on('click', '#btnTypeDetails', function () {
    globalFunctions.loadPopup($(this), "/Course/Lesson/GetSchoolUnitDetailsMappingPopup", translatedResources.assignUnitToSchool);
    $("#myModal .modal-dialog ").removeClass("modal-lg");
    $(this).off("modalLoaded");
    $(this).on("modalLoaded", function () {
        $('.selectpicker').selectpicker('refresh');
    });
}).on('change', '#ddlCurriculumId', function () {
    unitDetailConfig.loadCourseList();
});
