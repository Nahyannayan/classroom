﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />


var LessonMaster = function () {
    LessonArray = [];
    var AddStatus = 0;
    var TransMode = 0;
    init = function () {
        $(".selectpicker").selectpicker('refresh');
        //loadCourseGrid();
    },
        loadCourseGrid = function () {
            
            $.get('/Course/Lesson/GetTopicLessonDetail', { CourseId: $('#ddlCourseId').val() }).then((response) => {
                
                $('#divCourseGridForUnit').html('');
                $("#hdnSId").val('');
                $('#divCourseGridForUnit').html(response);
                $("#hdnSId").val('');

            }).catch((error) => {
                globalFunctions.showMessage(error.NotificationType, error.Message)
            });
        },
        EditLesson = function (source, LessonId, SubSyllabusId) {
            loadCreatePopup(source, translatedResources.add, LessonId);

            $("#hdnSId").val(SubSyllabusId);
            //$.post('/Course/Lesson/GetAddEditLessonEdit?StandardId=' + id, function (response) {
            //    
            //    if (response) {
            //        var sDATE = new Date(parseInt(response[0].StartDate.slice(6, -2)));
            //        var eDATE = new Date(parseInt(response[0].EndDate.slice(6, -2)));
            //        var StartDateval = ('' + (1 + sDATE.getMonth()) + '/' + sDATE.getDate() + '/' + sDATE.getFullYear().toString());
            //        var EndDateval = ('' + (1 + eDATE.getMonth()) + '/' + eDATE.getDate() + '/' + eDATE.getFullYear().toString());
            //        //var EndDate = smsCommon.ConvertjsonEncodeDateStringTojsonDate(response[0].EndDate).ToshortFormat();
            //        //TransMode: 2,
            //        $("#LessonId").val(id);
            //        $("#Details").val(response[0].Details);
            //        //$("#Title").val(object.Title);
            //        $("#StartDate").val(StartDateval);
            //        $("#EndDate").val(EndDateval);
            //        $('label[For="Details"]').addClass('active');
            //        // $('label[For="Title"]').addClass('active');
            //        $('label[For="StartDate"]').addClass('active');
            //        $('label[For="EndDate"]').addClass('active');
            //        // $('label[For="Standatd"]').addClass('active');
            //        // $('label[For="UnitName"]').addClass('active');
            //        $(".selectpicker").selectpicker('refresh');
            //    }
            //});
        },
        DeleteLesson = function (LessonId) {
            var mLessonMaster =
            {
                TransMode: 0,
                LessonId: LessonId,
                StandardId: $('#hdnSId').val(),
                Details: $('#Details').val(),
                StartDate: $('#StartDate').val(),
                EndDate: $('#EndDate').val()
            }
            $.post('/Course/Lesson/AddEditLesson', { objLesson: mLessonMaster }).then((response) => {
                globalFunctions.showMessage(response.NotificationType, response.Message);
                if (response.Success) {
                    loadCourseGrid();
                }

            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
        }
    

    $(document).on('click', '#btnLesson', function () {
        

        if (common.IsInvalidByFormOrDivId('frmLessonPlanner')) {
            var mLessonMaster =
            {
                TransMode: $('#LessonId').val() == 0 ? 1 : 2,
                LessonId: $('#LessonId').val(),
                StandardId: $('#ddlUnitId').val() == undefined ? $('#hdnSId').val() : $('#ddlUnitId').val(),
                Details: $('#Details').val(),
                StartDate: $('#StartDate').val(),
                EndDate: $('#EndDate').val(),
                StandardCode: $('#StandardCode').val()
            }
            
            $.post('/Course/Lesson/AddEditLesson', { objLesson: mLessonMaster }).then((response) => {
                globalFunctions.showMessage(response.NotificationType, response.Message);
                if (response.Success) {
                    
                    //$("#Showtable").html(response);
                    //$('#frmLessonMaster').html('')
                    
                    $("#StandardId").selectpicker('refresh');

                    $("#Details").val('');
                    $("#Title").val('');
                    $("#StartDate").val('');
                    $("#EndDate").val('');
                    $('label[For="Details"]').addClass('active');
                    $('label[For="Title"]').addClass('active');
                    $('label[For="StartDate"]').addClass('active');
                    $('label[For="EndDate"]').addClass('active');
                    $('label[For="Standatd"]').addClass('active');
                    $('label[For="UnitName"]').addClass('active');
                    $(".selectpicker").selectpicker('refresh');
                    //$('.selectpicker').selectpicker('refresh');
                    $('.date-picker').datetimepicker({
                        format: 'DD-MMM-YYYY'
                    });
                    $('#LessonId').val();
                    $("#myModal").modal('hide');
                    loadCourseGrid();
                    $('#hdnSId').val('');
                }

            }).catch((error) => {
                $("#myModal").modal('hide');
                globalFunctions.showMessage(error.NotificationType, error.Message)
            });
        }
    });
    btnAddStandardForAddUnit = function (source) {
        var title = translatedResources.add + ' ' + 'Standard' // translatedResources.Title;
        globalFunctions.loadPopup(source, '/Course/Lesson/AddEditLessonForAddUnitDetails', title, 'addtask-dialog modal-lg');
        $(source).off("modalLoaded");
        $('#formModified').val(true);
        $(source).on("modalLoaded", function () {
            $('.date-picker').datetimepicker({
                format: 'DD-MMM-YYYY'
            })
                .on('dp.change', function (e) {
                    $(`#StartDate`).data("DateTimePicker").maxDate(e.date);
                    $(`#EndDate`).data("DateTimePicker").minDate(e.date);
                    $('#EndDate').next('label').addClass("active");
                });
            $(".selectpicker").selectpicker('refresh');
        });
    }

    addStandard = function (source, StandardId) {
        
        loadCreatePopup(source, translatedResources.add, StandardId);
    },
        loadCreatePopup = function (source, mode, id) {
            var title = mode + ' ' + 'Standard' // translatedResources.Title;
            globalFunctions.loadPopup(source, '/Course/Lesson/AddEditLesson?StandardId=' + id, title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
        $(source).on("modalLoaded", function () {
            $('.date-picker').datetimepicker({
                format: 'DD-MMM-YYYY'
            })
                .on('dp.change', function (e) {
                    $(`#StartDate`).data("DateTimePicker").maxDate(e.date);
                    $(`#EndDate`).data("DateTimePicker").minDate(e.date);
                    $('#EndDate').next('label').addClass("active");
                });
            $(".selectpicker").selectpicker('refresh');
        });


        },
        GetUnitBasedonCourse = function (CourseId) {
            common.bindSingleDropDownByParameter(
                "#ddlUnitId",
                '/Course/Lesson/GetUnitBasedonCourse',
                { CourseId: CourseId },
                "",
                ''
            );
        }
    return {
        init,
        EditLesson,
        DeleteLesson,
        addStandard: addStandard,
        btnAddStandardForAddUnit: btnAddStandardForAddUnit,
        GetUnitBasedonCourse: GetUnitBasedonCourse
    }
}();
$(document).ready(function () {

    LessonMaster.init();
    $(document).on('click', '#btnAddStandard', function (id) {
        
        var id = $(this).data("value");
        $('#hdnSId').val(id);
        if (id == undefined) {
            AddStatus = 0;
        }
        LessonMaster.addStandard($(this));

    });
    $(document).on('click', '#btnAddStandardForAdd', function (id) {
        
        $('#hdnSId').val('');
        LessonMaster.btnAddStandardForAddUnit($(this));

    });
    //$('#LessonSearch').keypress(function (event) {
    //    var keycode = (event.keyCode ? event.keyCode : event.which);

    //    if (keycode == '13') {
    //        
           
    //        var filter = $(this).val(); // get the value of the input, which we filter on
    //        if (filter != "") {
    //            $('#catalouge .card').find('.semibold:not(:containsIN("' + filter.split(' ').pop() + '"))').parent().parent().addClass('d-none');
    //            $('#catalouge').find('.student-meta:not(:containsIN("' + filter.split(' ')[0] + '"))').parent().parent().addClass('d-none');
    //            if ($('#catalouge').find('.student-meta:not(:containsIN("' + filter.split(' ').pop() + '"))').parent().length == $('#hdnClsCount').val()

    //            ) {
    //                $('#divshowMessage').removeClass('d-none');
    //            }
    //            else {
    //                $('#divshowMessage').addClass('d-none');
    //            }
    //        }
    //        else {
    //            $('#catalouge').find('.student-meta').parent().parent().removeClass('d-none');
    //            $('#divshowMessage').addClass('d-none');
    //        }
    //    }
    //});
});
$("#ddlCourseId").change(function () {

    loadCourseGrid();
});
$(document).on("change", "#ddlCourseIdForUnit", function () {
    
    var CourseId = $(this).val();
    LessonMaster.GetUnitBasedonCourse(CourseId);
})
    .on('click', '#btnTypeDetails', function () {
        globalFunctions.loadPopup($(this), "/Course/Lesson/GetSchoolUnitDetailsMappingPopup", translatedResources.assignUnitToSchool);
        $("#myModal .modal-dialog ").removeClass("modal-lg");
        $(this).off("modalLoaded");
        $(this).on("modalLoaded", function () {
            $('.selectpicker').selectpicker('refresh');
        });
    });


var unitDetailTypeConfig = function () {
    var init = function () {

    },
        loadDivision = function () {
            var curriculum = $('#CurriculumId').val();
            common.bindSingleDropDownByParameter('#DivisionId', '/SchoolInfo/Division/GetDivisionUsingCurriculum', { curriculumId: curriculum }, '', translatedResources.selectedText);
        }
    return {
        init,
        loadDivision
    }
}();

$(document).ready(function () {

}).on('change', '#CurriculumId', function () {
    unitDetailTypeConfig.loadDivision();
}).on('change', '#DivisionId', function () {
    var value = $(this).val();
    $('#ddlUnitDetailsTypeIdsDiv').html('')
    $.get('/Course/Lesson/GetUnitDetailDropdown', { divisionId: value })
        .then(response => $('#ddlUnitDetailsTypeIdsDiv').html(response).find('select').selectpicker());
})
