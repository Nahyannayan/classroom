﻿//------------------------Functionality Part-1------------------------
var UploadStandardBank = function () {
    var InitGrid = function () {

    },
        ImportDataFromExcel = function (ImportExcelFile) {
            var data = new FormData();
            data.append("ImportExcelFile", ImportExcelFile);
            $.ajax({
                url: "/Course/Standard/ImportStandardBankDataFromExcel",
                contentType: false,
                processData: false,
                type: "POST",
                data: data,
                datatype: "json",
                success: function (response) {
                    $("#divExcelTable").html('');
                    $("#divExcelTable").html(response);
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }

            });
        },
        DeleteRow = function (ele) {
            $('.tooltip').tooltip('hide');
            $(ele).closest("tr").remove();
            var table = $("#tblImportExcel tbody>tr");
            if (table.length < 1) {
                var NoRecordTr = `<tr>
                                    <td class="pt-2 pb-2" colspan="10">No record found.</td>
                                </tr>`
                $("#tblImportExcel tbody").append(NoRecordTr);
            }
        },
        SaveImportedExcelData = function () {
            var IsValid = common.IsInvalidByFormOrDivId("frmStandardUpload");
            if (!IsValid)
                return false;

            var table = $("#tblImportExcel tbody>tr");
            var tblExcelRow = [];
            if (table.length > 0) {
                var firstRow = table[0];
                if ($(firstRow).find('td').length > 1) {
                    $.each(table, function (i, tr) {
                        var ColumnData = $(tr).find(".CanEdit")
                        if (ColumnData[0].value == '' || ColumnData[1].value == '' || ColumnData[2].value == '' ||
                            ColumnData[3].value == '' || ColumnData[4].value == '') {
                            $(tr).addClass("alert-danger");
                        }
                        else {
                            $(tr).removeClass("alert-danger");
                            tblExcelRow.push({
                                GroupName: ColumnData[0].value,
                                UnitName: ColumnData[1].value,
                                SubUnitName: ColumnData[2].value,
                                StandardCode: ColumnData[3].value,
                                StandardDescription: ColumnData[4].value
                            });

                        }
                    });
                    var CountErrorRows = $("#tblImportExcel tbody>tr.alert-danger").length;
                    if (CountErrorRows == 0) {
                        var standardUploadModel = {
                            StandardBankExcelList: tblExcelRow
                        };
                        //var uploadObjectiveExcelModel = JSON.stringify({ 'uploadObjectiveExcelModel': tblExcelRow });
                       // standardUploadModel = JSON.stringify( standardUploadModel);
                        console.log(standardUploadModel);
                        $.post("/Course/Standard/SaveImportedStandardBankExcelData", { standardUploadModel: tblExcelRow }).then((response) => {
                            if (response.Success) {
                                setTimeout(location.reload(),3000); //window.location = '/ProgressTrackerSetting/ProgressTrackerSetting/GetUploadObjectiveList'
                            }
                            globalFunctions.showMessage(response.NotificationType, response.Message);

                        }).catch((error) => {
                            globalFunctions.onFailure();
                        })
                    }
                    else {
                        globalFunctions.showMessage("error", "Error in the data please correct it.");
                        return false;
                    }
                }
                else {
                    globalFunctions.showMessage("error", "No data found to save.");
                    return false;
                }
            }
            else {
                globalFunctions.showMessage("error", "No imported data to save.");
                return false;
            }
        },
        StandardFileOnChange = function (event) {
            $("#divExcelTable").html('');
            common.removeValidationMessage(("#ImportExcelFileId"));
            var ImportExcelFile;
            if (event.length > 0) {
                ImportExcelFile = event[0];
                var extension = ImportExcelFile.name.substr((ImportExcelFile.name.lastIndexOf('.') + 1)).trim();
                if (extension == "xlsx" || extension == "xls") {
                    UploadStandardBank.ImportDataFromExcel(ImportExcelFile);
                }
                else {
                    $("#divExcelTable").html('');
                    $("#ImportExcelFileId").val("");
                    common.showValidationMessage(("#ImportExcelFileId"), "Only excel file allowed");
                    return false;
                }
            }
            else
                return false;
        },
        ClearFieldAndGrid = function () {
            $("#divExcelTable").html('');
            $("#ddlStandCourseId").val("");
            $("#ImportExcelFileId").val("");
            $(".selectpicker").selectpicker("refresh");
        }
    return {
        InitGrid,
        ImportDataFromExcel,
        DeleteRow,
        SaveImportedExcelData,
        StandardFileOnChange,
        ClearFieldAndGrid
    }
}();
//---------------Click/Change Event Functionality Part-1---------------
$("#btnDownloadImportedExcel").on('click', function (event) {
    window.location = "/Course/Standard/DownloadStandardBankExcelTemplate";
});
$(document).on("click", ".fileinput-remove-button", function () {
    $("#divExcelTable").html('');
});
//---------------------------------------------------------------------
