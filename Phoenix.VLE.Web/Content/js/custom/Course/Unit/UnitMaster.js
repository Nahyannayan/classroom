﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />

let UnitMaster = function () {

    init = function () {

        UnitMaster.UnitMasterGrid();

        $(document).on('click', '#btnSaveUnitMaster', () => {
            var mUnitMaster =
            {
                UnitId: $('#UnitId').val(),
                CourseId: $('#CourseId').val(),
                UnitName: $('#UnitName').val(),
                StartDate: $('#StartDate').val(),
                EndDate: $('#EndDate').val(),
                UnitColorCode: $('#UnitColorCode').val()
            }

            $.post('/Course/Unit/AddEditUnitMaster', { objUnitMaster: mUnitMaster })
                .then((response) => {
                    globalFunctions.showMessage(response.NotificationType, response.Message)
                    UnitMaster.UnitMasterGrid();

                    $('#divUnitMasterGrid').removeClass('d-none');
                    $('#divAddEditUnitMaster').addClass('d-none');

                })
                .catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });




        });

        $(document).on('click', '#spnBtnBacktoGrid', function () {

            $('#divUnitMasterGrid').removeClass('d-none');
            $('#divCardtitle').removeClass('d-none');

            $('#spnBtnBacktoGrid').addClass('d-none');
            $('#divAddEditUnitMaster').addClass('d-none');

            $('#btnAddUnitMaster').removeClass('d-none');

        });

        $(document).on('click', '#btnAddUnitMaster', function () {
            UnitMaster.DisplayUnitMasterAddEdit();

        });

        $(document).on('click', '.clsEditUnit', function () {
            let Id = $(this).attr('data-UnitId')
            UnitMaster.DisplayUnitMasterAddEdit(Id);

        });

        $(document).on('click', '.clsTargetToUnitDetails', function () {

            var unitId = $(this).attr('data-UnitId');
            window.open('/Course/Unit/UnitDetails?Id=' + unitId);


        });
    },
        UnitMasterGrid = function () {
            $.get('/Course/Unit/UnitMasterGrid').then((response) => {
                if ($.fn.DataTable.isDataTable($('#tblUnitMasterGrid'))) {
                    $('#tblUnitMasterGrid').DataTable().destroy();
                }
                $('#tblUnitMasterGrid').DataTable({
                    data: response.aaData,
                    "ordering": false,
                    columns: [
                        { "data": "CourseName" },
                        { "data": "UnitName", "sClass": "text-centre", "bsortable": false },
                        { "data": "StartDate", "sClass": "text-centre", "bsortable": false },
                        { "data": "EndDate", "sClass": "text-centre", "bsortable": false },
                        { "data": "UnitColorCode", "sClass": "text-centre", "bsortable": false },
                        { "data": "Action", "sClass": "text-centre", "bsortable": false },
                    ],

                });

            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });

        },
        DisplayUnitMasterAddEdit = function (Id) {
            $.get('/Course/Unit/UnitMaster', { Id: Id }).then((response) => {
                $('#divAddEditUnitMaster').html('');
                $('#btnAddUnitMaster').addClass('d-none');
                $('#divUnitMasterGrid').addClass('d-none');

                $('#divCardtitle').addClass('d-none');
                $('#spnBtnBacktoGrid').removeClass('d-none');

                $('#divAddEditUnitMaster').removeClass('d-none');
                $('#divAddEditUnitMaster').html(response);



                if (Id > 0) {
                    $('label').addClass('active');
                }
                UnitMaster.initPartial();

            });
        },
        initPartial = function () {

            $('.selectpicker').selectpicker('refresh');
            $('.date-picker').datetimepicker({
                format: 'DD-MMM-YYYY',
                minDate: '2000-01-01'
                //defaultDate: Date.parse($("#hdnStartDate").val()),
            });

            $('.colorpicker').minicolors({
                theme: 'bootstrap',
                control: 'hue',
                forrmat: 'hex'


            });

            $('[data-toggle="tooltip"]').tooltip();

        }



    return {
        init: init,
        DisplayUnitMasterAddEdit: DisplayUnitMasterAddEdit,
        UnitMasterGrid: UnitMasterGrid,
        initPartial: initPartial
    }

}();

$(document).ready(function () {
    UnitMaster.init();


});