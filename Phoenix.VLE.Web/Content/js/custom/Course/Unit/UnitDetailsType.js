﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />
var topicArray = [];
var standarddetails = [];
var standardlessons = [];
var assessmentArray = [];
var assessmentEvidenceList = [];
var assessmentLearningArray = [];
var assessmentLearningList = [];
var assessmentDeleteArray = [];
var assessmentLearningDeleteArray = [];
var formChanged = false;
var isItFormOrPopUp = false;
var lessonIdsToDelete = [];
var pageNo = 1;
var setHeight = "200";
let UnitDetailsType = function () {

    init = function () {
        CKEDITOR.on("instanceReady", function (event) {
            var editor = event.editor;
            editor.on('change', function (e) {
                formChanged = true;
            });
        });

        //for (var i in CKEDITOR.instances) {

        //    CKEDITOR.instances[i].on('change', function () {

        //        alert('called');
        //        CKEDITOR.instances[i].updateElement()
        //    });

        //}
        //for (var i in CKEDITOR.instances) {
        //    editor = CKEDITOR.instances[i];
        //    editor.on('blur', function (e) {

        //        $('#' + e.editor.name).text(e.editor.getData());

        //    });
        //}
        $(document).on('click', '#btnListStandards', function (e) {
            pageNo = 1;
            UnitDetailsType.bindUnitStandardList();
           

        });

        $('[data-toggle="popover"]').popover({ trigger: 'click' });

        $(document).on('click', '#btnTypeDetails', function (e) {
            isItFormOrPopUp = true;
            //var sdId = $('#StandardIds').val().join(',');
            var sdId = topicArray.join(',');
            var assessmentEvidenceIds = assessmentArray.join(',');
            var assessmentLearningIds = assessmentLearningArray.join(',');
            var form = $('#frmUnitDetailsType')[0];

            $('<input>').attr(
                {
                    name: "lstOfStandardIds", id: "lstOfStandardIds", type: "hidden", value: sdId
                }).appendTo($('#frmUnitDetailsType'));
            $('<input>').attr(
                {
                    name: "lstOfAssessmentEvidenceIds", id: "lstOfAssessmentEvidenceIds", type: "hidden", value: assessmentEvidenceIds
                }).appendTo($('#frmUnitDetailsType'));
            $('<input>').attr(
                {
                    name: "lstOfAssessmentLearningIds", id: "lstOfAssessmentLearningIds", type: "hidden", value: assessmentLearningIds
                }).appendTo($('#frmUnitDetailsType'));

            $('<input>').attr(
                {
                    name: "lessonIdsToDelete", id: "lessonIdsToDelete", type: "hidden", value: lessonIdsToDelete.join(',')
                }).appendTo($('#frmUnitDetailsType'));

            var formData = new FormData(form);
            console.log(form);
            $.post({
                url: "/Course/Unit/AddEditUnitTypeDetails", data: formData, contentType: false, processData: false, success: function (response) {
                    // 
                    globalFunctions.showMessage(response.NotificationType, response.Message)
                    if (response.Success) {
                        location.href = "/Course/Course/AddEditCourse?id=" + globalFunctions.getParameterValueFromQueryString('courseId') + "&tab=unitcalendar";
                    }
                }
            })

            e.preventDefault();
            e.stopImmediatePropagation();

        });

        $(document).on('click', '#btnAddAssessmentEvidence', function (e) {
            debugger;
            isItFormOrPopUp = true;
            $('#frmAssessmentEvidence').removeData("validator").removeData("unobtrusiveValidation");//remove the form validation
            $.validator.unobtrusive.parse($('#frmAssessmentEvidence'));//add the form validatio

            if (!$('#frmAssessmentEvidence').valid()) {
                return false;
            }


            var form = $('#frmAssessmentEvidence')[0];
            var formData = new FormData(form);
            $.post({
                url: "/Course/Unit/SaveAssessmentEvidence",
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    
                    if (response.Success == true) {
                        if ($('#IsAddMode').val() == 'true' || $('#IsAddMode').val() == 'True' || $('#IsAddMode').val() == true) {
                            globalFunctions.showSuccessMessage(translatedResources.AddSuccess);
                        } else {
                            globalFunctions.showSuccessMessage(translatedResources.EditSuccess);
                        }
                        assessmentArray.push(response.InsertedRowId);
                        UnitDetailsType.bindAssessmentEvidenceDetails($('#UnitId').val());
                        $("#myModal").modal('hide');
                        isItFormOrPopUp = false;
                    }
                    else {

                        if ($('#IsAddMode').val() == 'true' || $('#IsAddMode').val() == 'True' || $('#IsAddMode').val() == true) {
                            globalFunctions.showErrorMessage(translatedResources.AddError);
                        } else {
                            globalFunctions.showErrorMessage(translatedResources.EditError);
                        }
                    }
                    //location.href = "/Course/Course/AddEditCourse?id=" + globalFunctions.getParameterValueFromQueryString('courseId') + "&tab=unitcalendar";
                }
            })
            e.preventDefault();
            e.stopImmediatePropagation();

        });

        $(document).on('click', '#btnAddAssessmentLearning', function (e) {
            isItFormOrPopUp = true;
            $('#frmAssessmentLearning').removeData("validator").removeData("unobtrusiveValidation");//remove the form validation
            $.validator.unobtrusive.parse($('#frmAssessmentLearning'));//add the form validatio

            if (!$('#frmAssessmentLearning').valid()) {
                return false;
            }



            var form = $('#frmAssessmentLearning')[0];
            var formData = new FormData(form);
            $.post({
                url: "/Course/Unit/SaveAssessmentLearning",
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {

                    if (response.Success == true) {
                        if ($('#IsAddMode').val() == 'true' || $('#IsAddMode').val() == 'True' || $('#IsAddMode').val() == true) {
                            globalFunctions.showSuccessMessage(translatedResources.AddSuccess);
                        } else {
                            globalFunctions.showSuccessMessage(translatedResources.EditSuccess);
                        }
                        assessmentLearningArray.push(response.InsertedRowId);
                        UnitDetailsType.bindAssessmentLearningDetails($('#UnitId').val());
                        $("#myModal").modal('hide');
                        isItFormOrPopUp = false;
                    }
                    else {

                        if ($('#IsAddMode').val() == 'true' || $('#IsAddMode').val() == 'True' || $('#IsAddMode').val() == true) {
                            globalFunctions.showErrorMessage(translatedResources.AddError);
                        } else {
                            globalFunctions.showErrorMessage(translatedResources.EditError);
                        }
                    }
                    //location.href = "/Course/Course/AddEditCourse?id=" + globalFunctions.getParameterValueFromQueryString('courseId') + "&tab=unitcalendar";
                }
            })
            e.preventDefault();
            e.stopImmediatePropagation();

        });

    },
        populateUnits = function populateUnits(obj) {
        $('#divsearch').hide();
            $('#divTopicDetails').mCustomScrollbar('destroy');
            $('#divTopicDetails').empty();
            common.bindSingleDropDownByParameter(
                "#ddlUnitName",
                '/Course/Unit/GetUnits',
                { GroupId: $(obj).val() },
                "",
                'Select'
            );


            //$.post("/Course/Unit/GetUnits", { GroupId: $(obj).val() }, function (options) {
            //    $("#ddlUnitName").append('<option value="">Select</option>');
            //    $.each(options, function (i, Section) {
            //        $("#ddlUnitName").append('<option value="'
            //            + Section.UnitId + '">'
            //            + Section.UnitName + '</option>');
            //    });
            //$('.selectpicker').selectpicker('refresh');
            //})
        },

        populateSubUnits = function populateSubUnits(obj) {

            $('#divTopicDetails').mCustomScrollbar('destroy');
            $('#divTopicDetails').empty();
            common.bindSingleDropDownByParameter(
                "#ddlSubUnitName",
                '/Course/Unit/GetSubUnits',
                { GroupId: $(obj).val() },
                "",
                'Select'
            );


            //$("#ddlSubUnitName").empty();
            //$.post("/Course/Unit/GetSubUnits", { UnitId: $(obj).val() }, function (options) {
            //    $("#ddlSubUnitName").append('<option value="">Select</option>');
            //    $.each(options, function (i, Section) {
            //        $("#ddlSubUnitName").append('<option value="'
            //            + Section.UnitId + '">'
            //            + Section.UnitName + '</option>');
            //    });
            //    $('.selectpicker').selectpicker('refresh');
            //    });
        },
        bindUnitStandardList = function bindUnitStandardList() {
            var StandardBankId = $('#ddlSubUnitName').val();
            var GroupId = $('#GroupName').val();
            var UnitId = $('#ddlUnitName').val();
            if (GroupId == "") {
                globalFunctions.showWarningMessage('Please select standard group');
                return;
            }
           $('#divsearch').show();
            StandardBankId = StandardBankId ? StandardBankId : 0;
            UnitId = UnitId ? UnitId : 0;
            //  var strValues = values.join(',');
            $('#AjaxLoader').show();
            var standardParams = {
                UnitMasterId: $('#UTM_ID').val(),
                GroupId: GroupId,
                UnitId: UnitId,
                StandardBankId: StandardBankId,
                PageNumber: pageNo,
                SearchString: ($('#search').val() || "")
            };
            $.get("/Course/Unit/GetUnitStandardList", standardParams, function (response) {
                if (response == "" || response.length == 0) { pageNo = -1; }
              
                if (response == "" && ($('#divTopicDetails').is(':empty'))) {
                    if ($('#divTopicDetails').is(':empty')) {
                        $("#divTopicDetails").html(`<span class="no-data-found text-bold d-block mx-auto my-auto">
                                      there is no data to display</span>`);
                    }
                }
                if (response != "") {
                    $("#divTopicDetails").append("<div id='divPagNo" + pageNo+"'></div>");
                    $("#divTopicDetails").append(response);
                }
                if (pageNo != 1)
                    $("#divTopicDetails").mCustomScrollbar('destroy');

                    $("#divTopicDetails").mCustomScrollbar({
                        setHeight: "300px",
                        autoExpandScrollbar: true,
                        autoHideScrollbar: true,
                        scrollbarPosition: "inside",
                        scrollInertia: 5,
                        setTop: 0,
                        advanced: { updateOnContentResize: true },
                        callbacks: {
                            onTotalScroll: function () {
                                if (pageNo != -1) { loadStandards(); }
                            },
                            onTotalScrollOffset: 150,
                            alwaysTriggerOffsets: false

                        },
                        
                    });
                
                if (pageNo != 1) {

                    $("#divTopicDetails").mCustomScrollbar("scrollTo", "#divPagNo" + pageNo);
                 
                }

                

                // UnitDetailsType.enableTopicListCheckbox();
                //$('.selectpicker').selectpicker('refresh');
            });
        },
        populateTopics = function populateTopics(obj) {

            $.post("/Course/Unit/GetUnitTopicList", { MainSyllabusId: $(obj).val() }, function (options) {
                //  if ($(obj).val() == '' || $(obj).val() == undefined) {
                $("#divTopicDetails").html('');
                // }

                $('#UnitTopicTree').combotree({
                    data: options,
                    cascadeCheck: true,
                    labelPosition: 'top',
                    multiple: true,
                    lines: true,
                    onChange: function (node) {
                        UnitDetailsType.bindTopicList();
                    },
                    onClick: function (node) {
                        UnitDetailsType.bindTopicList();

                    }
                });
                //  $('#UnitTopicTree').combotree('setValues', selectedValues);
            });
        },
        unitTopicChange = function unitTopicChange(obj, id) {
            if ($(obj).is(':checked')) {
                const index = topicArray.indexOf(Number(id));
                if (index > -1) {
                    $(obj).prop("checked", false);
                    return false;
                }
                topicArray.push(Number(id));

                var topic = new standarddetail(0, Number(id), $(obj).attr('data-topic'),
                    $(obj).attr('data-subtopic'), $(obj).attr('data-maintopic'), $(obj).attr('data-standardcode'));

                standarddetails.push(topic);
            }
            else {
                const index = topicArray.indexOf(Number(id));
                if (index > -1) {
                    topicArray.splice(index, 1);
                }
                standarddetails.splice(standarddetails.findIndex(item => item.id === Number(id)), 1);

            }
            console.log(JSON.stringify(standarddetails));
        },
        loadStandardsPopup = function loadStandardPopup(source, title) {
            isItFormOrPopUp = true;
            globalFunctions.loadPopup($(source), '/Course/Unit/GetStandardPopUpDetails?UTM_ID=' + $(source).data("unitid"),
                title, 'addtask-dialog modal-xl');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                var popoverEnabler = new LanguageTextEditPopover();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: translatedResources.selectedText
                });
                $('#search').keyup(function (e) {
                    if (e.which == 13) {
                        $('#divTopicDetails').mCustomScrollbar('destroy');
                        $('#divTopicDetails').empty();

                        pageNo = 1;
                        UnitDetailsType.bindUnitStandardList();


                        //$('.standarddetails').show()

                        //var filter = $(this).val();

                        //$('#products').find('.standarddetails:not(:contains("' + filter + '"))').hide();

                        //var grouplist = $(".group-list-standard-search");
                        //grouplist.each(function (index, element) {

                        //    var searchLen = $(element).find('.standarddetails:not(:contains("' + filter + '"))').length;
                        //    var searchTotalLen = $(element).find('.standarddetails').length;
                        //    if (searchLen == searchTotalLen) {
                        //        if ($(element).next('.norecords').length == 0)
                        //            $(element).after('<h5 class="norecords">No records found</h5>');
                        //        $(element).hide();
                        //    }
                        //    else {
                        //        $(element).show();
                        //        $(element).next('.norecords').remove();
                        //    }

                        //});
                    }
                });
                $('#btnAddStandard').click(function () {
                    console.log(JSON.stringify(standarddetails));
                    UnitDetailsType.bindStandardDetails(false);
                });
            });
        },
        loadAssesmentEvidencePopup = function loadAssesmentEvidencePopup(source, title, id) {
            isItFormOrPopUp = true;
            globalFunctions.loadPopup($(source), '/Course/Unit/GetAssessmentEvidencePopUpDetails?UTM_ID=' + $(source).data("unitid") + '&id=' + id,
                title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                var popoverEnabler = new LanguageTextEditPopover();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: translatedResources.selectedText
                });

            });
        },
        loadAssesmentLearningPopup = function loadAssesmentLearningPopup(source, title, id) {
            isItFormOrPopUp = true;
            globalFunctions.loadPopup($(source), '/Course/Unit/GetAssessmentLearningPopUpDetails?UTM_ID=' + $(source).data("unitid") + '&id=' + id,
                title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                var popoverEnabler = new LanguageTextEditPopover();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: translatedResources.selectedText
                });

            });
        },
        bindTopicList = function bindTopicList() {
            var values = $('#UnitTopicTree').combotree('getValues');
            var strValues = values.join(',');
            $.post("/Course/Unit/GetUnitTopicListView", { SubSyllabusId: strValues, UnitMasterId: $('#UTM_ID').val() }, function (response) {
                $("#divTopicDetails").html('');
                $("#divTopicDetails").append(response);
                UnitDetailsType.enableTopicListCheckbox();
                $('.selectpicker').selectpicker('refresh');
            });
        },
        standarddetail = function standarddetail(scmid, id, text, subtopic, maintopic, standardcode) {
            this.scmid = scmid;
            this.id = id;
            this.text = text;
            this.subtopic = subtopic;
            this.maintopic = maintopic;
            this.standardcode = standardcode;
        },
        bindStandardDetails = function bindStandardDetails(onload, attacchmentCount = 0) {
            //   
            if (onload == false) {
                if ($('#GroupName').val() == "") {
                    globalFunctions.showWarningMessage('Please select standard group');
                    return;
                }
                //else
                //    if ($('#ddlUnitName').val() == "") {
                //        globalFunctions.showWarningMessage("Please select unit");
                //        return;
                //    }
                //else
                //    if ($('#ddlSubUnitName').val() == "") {
                //        globalFunctions.showWarningMessage("Please select sub unit");
                //        return;
                //    }
                var flag = false;
                $("td input[class=chkstandard]").each(function () {
                    if ($(this).is(':checked') && !$(this).is(':disabled')) {
                        flag = true;
                    }
                });
                if (flag == false) {
                    globalFunctions.showWarningMessage('Please select standard(s)');
                    return false;
                }
            }
            var dt = JSON.stringify({
                standarddetails: standarddetails, AttachmentCount: attacchmentCount
            });
            $.ajax({
                url: '/Course/Unit/GetSelectedStandardDetails',
                type: 'POST',
                data: dt,
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    $("#divSelectedTopics").html('');
                    $("#divSelectedTopics").append(result);
                    $('.selectpicker').selectpicker('refresh');
                }
            });
            $("#myModal").modal('hide');

        },
        bindStandardLessons = function bindStandardLessons(onload, attacchmentCount = 0) {
            //   
            if (onload == false) {
                if ($('#GroupName').val() == "") {
                    alert('Please select standard group');
                    return;
                }
                //else
                //    if ($('#UnitTopicTree').combotree('getValues') == "") {
                //        alert("Please select topic(s)");
                //        return;
                //    }
            }
            var dt = JSON.stringify({
                standarddetails: standardlessons, AttachmentCount: attacchmentCount
            });
            $.ajax({
                url: '/Course/Unit/GetSelectedStandardDetails',
                type: 'POST',
                data: dt,
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    $("#divSelectedTopics").html('');
                    $("#divSelectedTopics").append(result);
                    $('.selectpicker').selectpicker('refresh');
                }
            });
            $("#myModal").modal('hide');

        },
        deletedSelectedStandard = function deletedSelectedStandard(obj, id, scmid) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                const index = topicArray.indexOf(Number(id));
                if (index > -1) {
                    topicArray.splice(index, 1);
                }
                if ($(obj).closest('ul').find('li').length == 1) {
                    $(obj).closest('ul').parent('div.maintopic').remove();

                }
                else {
                    $(obj).remove();
                }
                if (id > 0) { standarddetails.splice(standarddetails.findIndex(item => item.id === Number(id)), 1); }
                else { standarddetails.splice(standarddetails.findIndex(item => item.scmid === Number(scmid)), 1); }
                if (Number(scmid) > 0) {
                    lessonIdsToDelete.push(scmid);
                    $(obj).remove();
                }
            });
        }
        ,
        enableTopicListCheckbox = function () {
            // standarddetails

            $("td input[class=chkstandard]").each(function () {
                var std_id = $(this).data("stdid");
                const index = topicArray.indexOf(Number(std_id));
                if (index > -1) {
                    $(this).prop("checked", true);
                }
                $('.divtargeted').each(function () {

                    var flag = false;
                    $(this).find('div.divprogressbar').each(function () {
                        var std_id = $(this).data("stdid");

                        const index = topicArray.indexOf(Number(std_id));
                        if (index > -1) {
                            $(this).width("100%");
                            $(this).attr("aria-valuenow", "100");
                            flag = true;
                            $(this).closest('div.divtargeted').find('span.spantargeted').html("(1)");
                        }
                    });

                });

            });
        },
        onSaveAssessmentEvidence = function (IsAddMode) {
            // schoolBanner.init();
            if (IsAddMode == 'True') {
                globalFunctions.showSuccessMessage(translatedResources.AddSuccess);
            } else {
                globalFunctions.showSuccessMessage(translatedResources.EditSuccess);
            }
            $("#myModal").modal('hide');
            //  schoolBanner.init();
        },
        onSaveAssessmentEvidenceError = function (IsAddMode) {
            init();
            if (IsAddMode == 'True') {
                globalFunctions.showErrorMessage(translatedResources.AddError);
            } else {
                globalFunctions.showErrorMessage(translatedResources.EditError);
            }
            $("#myModal").modal('hide');
        },
        bindAssessmentEvidenceDetails = function bindAssessmentEvidenceDetails(unitId) {
        debugger;
            isItFormOrPopUp = false;
            var dt = JSON.stringify({
                unitId: unitId, removedIds: assessmentDeleteArray.join(',')
            });

            $.ajax({
                url: '/Course/Unit/GetAssessmentEvidenceList',
                type: 'POST',
                data: dt,
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    $("#divAssessmentEvidenceList").html('');
                    $("#divAssessmentEvidenceList").append(result);
                }
            });

        }
        , deletedAssessmentEvidence = function deletedAssessmentEvidence(obj, id) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                isItFormOrPopUp = false;
                const index = assessmentArray.indexOf(Number(id));

                if (index > -1) {
                    assessmentDeleteArray.push(id);
                    assessmentArray.splice(index, 1);
                    $(obj).remove();
                }
            });
        },
        bindAssessmentLearningDetails = function bindAssessmentLearningDetails(unitId) {
            isItFormOrPopUp = false;
            var dt = JSON.stringify({
                unitId: unitId, removedIds: assessmentLearningDeleteArray.join(',')
            });

            $.ajax({
                url: '/Course/Unit/GetAssessmentLearningList',
                type: 'POST',
                data: dt,
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    $("#divAssessmentLearningList").html('');
                    $("#divAssessmentLearningList").append(result);
                }
            });

        }
        , deletedAssessmentLearning = function deletedAssessmentLearning(obj, id) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                isItFormOrPopUp = false;
                const index = assessmentLearningArray.indexOf(Number(id));

                if (index > -1) {
                    assessmentLearningDeleteArray.push(id);
                    assessmentLearningArray.splice(index, 1);
                    $(obj).remove();
                }
            });
        }

    return {
        init: init
        , populateTopics: populateTopics
        , unitTopicChange: unitTopicChange
        , deletedSelectedStandard: deletedSelectedStandard
        , bindStandardDetails: bindStandardDetails
        , loadStandardsPopup: loadStandardsPopup
        , bindTopicList: bindTopicList
        , enableTopicListCheckbox: enableTopicListCheckbox
        , loadAssesmentEvidencePopup: loadAssesmentEvidencePopup
        , onSaveAssessmentEvidence: onSaveAssessmentEvidence
        , onSaveAssessmentEvidenceError: onSaveAssessmentEvidenceError
        , bindAssessmentEvidenceDetails: bindAssessmentEvidenceDetails
        , deletedAssessmentEvidence: deletedAssessmentEvidence
        , loadAssesmentLearningPopup: loadAssesmentLearningPopup
        , bindAssessmentLearningDetails: bindAssessmentLearningDetails
        , deletedAssessmentLearning: deletedAssessmentLearning
        , populateUnits: populateUnits
        , populateSubUnits: populateSubUnits
        , bindUnitStandardList: bindUnitStandardList
        , bindStandardLessons: bindStandardLessons
    }

}();

$(document).ready(function () {
    UnitDetailsType.init();
    var $form = $('form'),
        origForm = $form.serialize();


    $('form :input').on('change input', function () {
        formChanged = $form.serialize() !== origForm;
    });
    window.onbeforeunload = function () {
        if (isItFormOrPopUp)
            return this.undefined;
        if (this.assessmentDeleteArray.length > 0 || this.assessmentLearningDeleteArray.length > 0 || formChanged)
            return 'Are you sure you want to leave?';

    };

});

var inCallback = false,
    isReachedScrollEnd = false;

function loadStandards() {
    if (pageNo > -1 && !inCallback) {
        inCallback = true;
        pageNo++;

        UnitDetailsType.bindUnitStandardList(pageNo);
        inCallback = false;
    }
}

