﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />
//var courseId = $('#cid').val();
let UnitCalendarMaster = function () {

    init = function (courseId) {

        $('body').tooltip({
            selector: '[data-toggle="tooltip"], [title]:not([data-toggle="popover"])',
            trigger: 'hover',
            container: 'body'
        }).on('click mousedown mouseup', '[data-toggle="tooltip"], [title]:not([data-toggle="popover"])', function () {
            $('[data-toggle="tooltip"], [title]:not([data-toggle="popover"])').tooltip('hide');
        });

        UnitCalendarMaster.UnitCalendarGrid(courseId);

        $(document).on('click', '#btnSaveUnitMaster', () => {
            var isDatesValid = false;
            var mUnitMaster =
            {
                UnitId: $('#UnitId').val(),
                CourseId: $('#CourseId').val(),
                UnitName: $('#UnitName').val(),
                StartDate: $('#StartDay').val(),
                EndDate: $('#EndDay').val(),
                UnitColorCode: $('#UnitColorCode').val(),
                ParentId: $('#ParentId').val()
            }

            $('#frmUnitMasterAddEdit').removeData("validator").removeData("unobtrusiveValidation");//remove the form validation
            $.validator.unobtrusive.parse($('#frmUnitMasterAddEdit'));//add the form validatio

            if (!$('#frmUnitMasterAddEdit').valid()) {
                if (mUnitMaster.UnitColorCode == "" || mUnitMaster.UnitColorCode == undefined) {
                    globalFunctions.showMessage("error", "Please select unit color");
                    return false;
                }
                return false;
            }
            else {
                
                var unitStartDate = new moment(mUnitMaster.StartDate);
                var unitEndDate = new moment(mUnitMaster.EndDate);

                var _courseStartDate = new Date($('#CourseStartDate').val());
                var _courseEndate = new Date($('#CourseEndDate').val());

                if (unitStartDate >= _courseStartDate && unitStartDate <= _courseEndate &&
                    unitEndDate >= _courseStartDate && unitEndDate <= _courseEndate
                ) {

                    isDatesValid = true;
                }
             

                var diff = unitEndDate.diff(unitStartDate, 'days');
                if (diff < 6) {
                    globalFunctions.showMessage("error", "End Week cannot be less then Start Week");
                    return false;
                }

                
                if (!isDatesValid) {
                    globalFunctions.notyDltConfirm(`${translatedResources.TheWeekSelectedMessaage}`, () => {
                        var order = $('.unitcalendarsort').data("sort"); //1 asc //0 desc

                        $.post('/Course/Unit/AddEditUnitMaster', { objUnitMaster: mUnitMaster })
                            .then((response) => {
                                globalFunctions.showMessage(response.NotificationType, response.Message);
                                UnitCalendarMaster.UnitCalendarGrid($('#CourseId').val(), Number(order) == 1 ? 0 : 1);
                                UnitCalendarMaster.resetForm(order);
                            })
                            .catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });


                    });

                }
                else {
                    var order = $('.unitcalendarsort').data("sort"); //1 asc //0 desc

                    $.post('/Course/Unit/AddEditUnitMaster', { objUnitMaster: mUnitMaster })
                        .then((response) => {
                            globalFunctions.showMessage(response.NotificationType, response.Message);
                            UnitCalendarMaster.UnitCalendarGrid($('#CourseId').val(), Number(order) == 1 ? 0 : 1);
                            UnitCalendarMaster.resetForm(order);
                        })
                        .catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
                }
              
            }
            
        });

        $(document).on('click', '#spnBtnBacktoGrid', function () {

            $('#divUnitMasterGrid').removeClass('d-none');
            $('#divCardtitle').removeClass('d-none');

            $('#spnBtnBacktoGrid').addClass('d-none');
            $('#divAddEditUnitMaster').addClass('d-none');

            $('#btnAddUnitMaster').removeClass('d-none');

        });

        $(document).on('click', '#btnAddUnitMaster', function () {

            UnitCalendarMaster.DisplayUnitMasterAddEdit();

        });

        $(document).on('click', '.clsEditUnit', function () {
            let Id = $(this).attr('data-UnitId')
            UnitCalendarMaster.DisplayUnitMasterAddEdit(Id);

        });

        $(document).on('click', '.clsTargetToUnitDetails', function () {

            var unitId = $(this).attr('data-UnitId');
            window.open('/Course/Unit/UnitDetails?Id=' + unitId);


        });
    },
        UnitCalendarGrid = function (Id, order = 1) {
            $.get('/Course/Unit/UnitCalendarGrid', { Id: Id, order: order }).then((response) => {
                $('#divUnitCalendarGrid').html('');
                $('#divUnitCalendarGrid').html(response);
                UnitCalendarMaster.initPartial();
            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });

        },
        DisplayUnitMasterAddEdit = function (Id) {
            $.get('/Course/Unit/UnitCalendarMaster', { Id: Id }).then((response) => {
                $('#divAddEditUnitMaster').html('');
                $('#btnAddUnitMaster').addClass('d-none');
                $('#divUnitMasterGrid').addClass('d-none');

                $('#divCardtitle').addClass('d-none');
                $('#spnBtnBacktoGrid').removeClass('d-none');

                $('#divAddEditUnitMaster').removeClass('d-none');
                $('#divAddEditUnitMaster').html(response);



                if (Id > 0) {
                    $('label').addClass('active');
                }
                UnitCalendarMaster.initPartial();

            });
        },
        sort = function (obj, courseId) {
            var order = $(obj).data("sort"); //1 asc //0 desc
            $(obj).data("sort", Number(order) == 1 ? 0 : 1);
            UnitCalendarMaster.UnitCalendarGrid(courseId, order);
        },
        initPartial = function () {

            $('.selectpicker').selectpicker('refresh');
            $('.date-picker').datetimepicker({
                format: 'DD-MMM-YYYY',
                minDate: '2000-01-01'
                //defaultDate: Date.parse($("#hdnStartDate").val()),
            });

            $('.colorpicker').minicolors({
                theme: 'bootstrap',
                control: 'hue',
                forrmat: 'hex'
                , swatches: ['#00FFFF', '#000000', '#0000FF'
                    , '#FF00FF', '#808080', '#008000'
                    , '#00FF00', '#800000', '#000080'
                    , '#808000', '#800080', '#FF0000'
                    , '#C0C0C0', '#008080', '#FFFF00', '#4caf50', '#ffeb3b', '#7A4FB7', '#ff9800', '#ef9a9a', '#90caf9']


            });


        }
        , initializeControls = function () {

            //$('[data-toggle="tooltip"]').tooltip({
            //    trigger: 'hover'
            //}); 
            //$('.time-picker').datetimepicker({
            //    format: 'LT'
            //});
            //$('.date-picker').datetimepicker({
            //    format: 'MM/DD/YYYY'
            //});
            //var dt = new Date();
            //var cDate = ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear();
            //var cTime = dt.toLocaleString('en-US', {
            //    hour: 'numeric',
            //    minute: 'numeric',
            //    hour12: true
            //})
            //$('.date-picker').val(cDate);
            //$('.time-picker').val(cTime);


            $('.paymentDue-toast').addClass('animated fadeInRight');
            $('body').on('click', '.close-toast', function () {
                $(this).parent().parent().fadeOut();
            })
            //$('[data-toggle="popover-hover"]').popover({
            //    html: true,
            //    trigger: 'hover',
            //    placement: 'left',
            //    container: '#groups-popover',
            //});
            //$('[data-toggle="popover"]').popover({
            //    html: true,
            //    trigger: 'hover',
            //    placement: 'right',
            //    container: '#assignment-popover'
            //});

            $(".colorpicker").minicolors({
                theme: 'bootstrap',
                control: 'hue',
                format: 'hex'
                , swatches: ['#00FFFF', '#000000', '#0000FF'
                    , '#FF00FF', '#808080', '#008000'
                    , '#00FF00', '#800000', '#000080'
                    , '#808000', '#800080', '#FF0000'
                    , '#C0C0C0', '#008080', '#FFFF00', '#4caf50', '#ffeb3b', '#7A4FB7', '#ff9800', '#ef9a9a', '#90caf9']
            });

            $(".tabs-list li a").click(function (e) {
                e.preventDefault();
            });

            $(".tabs-list li").click(function () {
                var tabid = $(this).find("a").attr("href");
                $(".tabs-list li,.tabs div.tab").removeClass("active"); // removing active class from tab

                $(".tab").hide(); // hiding open tab
                $(tabid).show(); // show tab
                $(this).addClass("active"); //  adding active class to clicked tab

            });



            $(".resources").mCustomScrollbar({
                autoHideScrollbar: true,
                autoExpandScrollbar: true,
                scrollbarPosition: "outside",
            });
            $(".sidebar-fixed .list-group").mCustomScrollbar({
                setHeight: "86%",
                autoExpandScrollbar: true,
                autoHideScrollbar: true,
                scrollbarPosition: "outside"
            });

            $('.loader-wrapper').fadeOut(500, function () {
                //$('.loader-wrapper').remove();
            });
            $(".notification-list").mCustomScrollbar({
                //setHeight: "76%",
                autoExpandScrollbar: true,
                scrollbarPosition: "inside",
                autoHideScrollbar: true,
            });
            $('.search-container').click(function () {
                $('.search-field').addClass('search-input');
                $('.close-icon').addClass('show-icon');
                $('.filter-title').toggleClass('hide-title');
            });
            $('.close-icon').click(function () {
                $('.close-icon').removeClass('show-icon');
            })
            $('.list-pattern .list-view').click(function () {
                let gridView = $('.grid-list-card .grid-wrapper');
                let listView = $('.grid-list-card .list-wrapper');
                $('.list-pattern .grid-view').removeClass('active');
                $('.list-pattern .list-view').addClass('active');
                gridView.addClass('hide-item ');
                listView.removeClass('hide-item');
            });
            $('.list-pattern .grid-view').click(function () {
                let gridView = $('.grid-list-card .grid-wrapper');
                let listView = $('.grid-list-card .list-wrapper');
                $('.list-pattern .list-view').removeClass('active');
                $('.list-pattern .grid-view').addClass('active');
                gridView.removeClass('hide-item');
                listView.addClass('hide-item');
            });


        },
        editUnit = function editUnit(unitId) {
            var order = $('.unitcalendarsort').data('sort');
            $.get('/Course/Unit/UnitMasterPartial',
                { CourseId: $('#CourseId').val(), UnitId: unitId, ParentId: 0, order: Number(order) }
            ).then((response) => {
                $('#divUnitMasterPartial').html('');
                $('#divUnitMasterPartial').html(response);
                $('#UnitName').focus();
                UnitCalendarMaster.initializeUnitCalendarControls();
                UnitCalendarMaster.initializeValidation();
                $('.selectpicker').selectpicker('refresh');
                // $('.unitcalendarsort').data('sort', Number(order));
            });
        },
        deleteUnit = function deleteUnit(unitId, unitname) {

            var mUnitMaster =
            {
                UnitId: unitId,
                CourseId: $('#CourseId').val(),
                UnitName: unitname,
                StartDate: new Date(),
                EndDate: new Date(),
                UnitColorCode: ''
            };
            console.log(mUnitMaster);
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                $.post('/Course/Unit/DeleteUnitById', { objUnitMaster: mUnitMaster })
                    .then((response) => {

                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        var order = $('.unitcalendarsort').data("sort"); //1 asc //0 desc
                        UnitCalendarMaster.UnitCalendarGrid($('#CourseId').val(), Number(order) == 1 ? 0 : 1);
                        UnitCalendarMaster.initializeUnitCalendarControls();
                        UnitCalendarMaster.initializeValidation();
                        UnitCalendarMaster.resetForm(order);
                    }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
            });
        },
        addEditSubUnit = function addEditSubUnit(unitId, unitname, parentId, startDate, endDate) {
            var order = $('.unitcalendarsort').data('sort');
            $.get('/Course/Unit/UnitMasterPartial',
                { CourseId: $('#CourseId').val(), UnitId: unitId, ParentId: parentId, order: Number(order), StartDate: startDate, EndDate: endDate }
            ).then((response) => {
                $('#divUnitMasterPartial').html('');
                $('#divUnitMasterPartial').html(response);
                $('#spnParentUnitName').html(unitname);
                $('#UnitName').focus();
                UnitCalendarMaster.initializeUnitCalendarControls();
                UnitCalendarMaster.initializeValidation();
                $('.selectpicker').selectpicker('refresh');
                // $('.unitcalendarsort').data('sort',Number(order));
            });

        },
        resetForm = function resetForm(order) {



            $.get('/Course/Unit/UnitMasterPartial',
                { CourseId: $('#CourseId').val(), UnitId: 0, ParentId: 0, order: Number(order), StartDate: '', EndDate: '' }
            ).then((response) => {
                $('#divUnitMasterPartial').html('');
                $('#divUnitMasterPartial').html(response);
                $('#UnitName').val('');
                $('#StartDay').val('');
                $('#EndDay').val('');
                $('#UnitColorCode').minicolors('value', '');
                $('#UnitId').val('0');
                $('#ParentId').val('0');
                $('#spnParentUnitName').html('');
                $('.selectpicker').selectpicker('refresh');
                $('#UnitName').focus();
                //$('.unitcalendarsort').data('sort', Number(order));
            });
        },
        initializeValidation = function initializeValidation() {
            $.validator.setDefaults({
                ignore: ".ignore"
            });

            //Not working, need to check in more detail
            jQuery.validator.addMethod("noSpace", function (value, element) {
                //console.log(value.indexOf(" "));
                return value != '' || value.trim().length != 0;
            }, "No space please and don't leave it empty");

        }


    return {
        init: init,
        DisplayUnitMasterAddEdit: DisplayUnitMasterAddEdit,
        UnitCalendarGrid: UnitCalendarGrid,
        initPartial: initPartial,
        sort: sort,
        initializeUnitCalendarControls: initializeControls,
        editUnit: editUnit,
        initializeValidation: initializeValidation,
        deleteUnit: deleteUnit,
        addEditSubUnit: addEditSubUnit,
        resetForm: resetForm
    }

}();

(function () {
    // hold onto the drop down menu                                             
    var dropdownMenu;

    // and when you show it, move it to the body                                     
    $(document).on('show.bs.dropdown', '.btn-unitcalendar', function (e) {

        // grab the menu        
        dropdownMenu = $(e.target).find('.dropdown-menu');

        // detach it and append it to the body
        $('body').append(dropdownMenu.detach());

        // grab the new offset position
        var eOffset = $(e.target).offset();

        // make sure to place it where it would normally go (this could be improved)
        dropdownMenu.css({
            'display': 'block',
            'top': eOffset.top + $(e.target).outerHeight(),
            'left': eOffset.left
        });
    });

    // and when you hide it, reattach the drop down, and hide it normally                                                   
    $(document).on('hide.bs.dropdown', '.btn-unitcalendar', function (e) {
        $(e.target).append(dropdownMenu.detach());
        dropdownMenu.hide();
    });
})();