var unitCalendar=[];
var unitListJson;
var weekListJson;
var resources = [];
var units = [];
var unitDurations = [];
var months = [];
var academicWeeks = 0;

function generateCalendarData() {
    
    months = getMonthList((unitCalendar.YearStartDate), (unitCalendar.YearEndDate));
    for (var i = 0; i < unitCalendar.UnitDetails.length; i++) {
        var _unit = unitCalendar.UnitDetails[i];
        units.push(new unit(_unit.UnitId, _unit.UnitName,_unit.UnitInfoURL,_unit.ParentId,_unit.StartDate,_unit.EndDate));
        resources.push(new resource(_unit.UnitId, '', _unit.UnitColorCode))
        var _unitDuration = new unitDuration(units[i], resources[i], (_unit.StartDate), (_unit.EndDate),(_unit.ParentId));
        unitDurations.push(_unitDuration);
    } 
}

function getMonthList(startDate, endDate) {
    var start = startDate.split('-');
    var end = endDate.split('-');
    var startYear = parseInt(start[0]);
    var endYear = parseInt(end[0]);
    var _months = [];
    var dayNumber = '01';

    for (var i = startYear; i <= endYear; i++) {
        var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
        var startMon = i === startYear ? parseInt(start[1]) - 1 : 0;
        for (var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j + 1) {
            var month = j + 1;
            var displayMonth = month < 10 ? '0' + month : month;
           
            dayNumber = (i === startYear && j === startMon) ? start[2] : dayNumber;
            _months.push([i, displayMonth, dayNumber].join('-'));
        }
    }
    return _months;
}

function resource(id, name, unitDurationColor, parentId) {
    this.id = id;
    this.name = name;
    this.parentId = parentId;
    this.unitDurationColor = unitDurationColor;
    
}

function unit(id, name, unitInfoURL, parentId,startDate,endDate) {
    this.id = id;
    this.name = name;
    this.parentId = parentId;
    this.unitInfoURL = unitInfoURL;
    this.startDate = startDate;
    this.endDate = endDate;
}

function unitDuration(unit, resource, start_date, end_date, parentId) {
    this.unit = unit;
    this.resource = resource;
    this.start_date = start_date;
    this.end_date = end_date;
    this.parentId = parentId;
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? { r: parseInt(result[1], 16), g: parseInt(result[2], 16), b: parseInt(result[3], 16) } : null;
}

// Adds the given number of days to the date
Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

// Returns the array of dates between two dates
function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(new Date(currentDate));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}


// Format two dates to a week string (jan 19 - 25 || jan 26 - feb 1)
function formatDatesToWeekString(start_date, end_date) {
    var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
    var dateString = "";
    var getStartYear = start_date.getFullYear();
    var getEndYear = end_date.getFullYear();
    dateString = getStartYear+" ";
    dateString = dateString + months[start_date.getMonth()] + " ";
    dateString = dateString + start_date.getDate() + " - ";

    if (start_date.getMonth() !== end_date.getMonth()) {
        if (getStartYear != getEndYear) {
            dateString = dateString + " " + getEndYear+" ";
        }
        dateString = dateString + months[end_date.getMonth()] + " ";
    }
    
    dateString = dateString + end_date.getDate();
    return dateString;
}

// Gets the string value of the given day
function getStringValueOfDay(day) {
    console.log("day"+day);
    var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    return days[day];
    
}

// Gets the string value of the given month
function getStringValueOfMonth(month) {
    var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
    return months[month];
}

// Calculate number of days between two dates.
function calculateDaysBetweenDates(start_date, end_date) {
    //Source: http://stackoverflow.com/questions/3224834/get-difference-between-2-dates-in-javascript
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;

    // Discard the time and time-zone information.
    var utc1 = Date.UTC(start_date.getFullYear(), start_date.getMonth(), start_date.getDate());
    var utc2 = Date.UTC(end_date.getFullYear(), end_date.getMonth(), end_date.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

// Returns an array of resources for the given unit
function getResourcesPerUnit(unit) {
    var unitDurationsAllocation = [];
    for (j = 0; j < unitDurations.length; j++) {
        if (unitDurations[j].unit.id === unit.id) {
            unitDurationsAllocation.push(unitDurations[j]);
        }
    }

    var unitResources = [];
    for (k = 0; k < unitDurationsAllocation.length; k++) {
        var inArray = $.inArray(unitDurationsAllocation[k].resource.name, unitResources);
        if (inArray === -1) {
            unitResources.push(unitDurationsAllocation[k].resource.name);
        }
    }

    return unitResources;
}

// Returns the (style)top value of the unitDuration
function determineUnitDurationTop(unitDuration) {
    var unitResources = getResourcesPerUnit(unitDuration.unit);
    unitResources.sort();
    var index = $.inArray(unitDuration.resource.name, unitResources);
    return (index * 40) + "px";
}

// Returns the row height based on the number of resources per unit
function calculateRowHeight(unit) {
    var unitResources = getResourcesPerUnit(unit);
    var numberOfRows = 0;
    if (unitResources.length === 0) {
        numberOfRows = 1;
    }
    numberOfRows = unitResources.length;
    return (numberOfRows * 40);
}

// Builds the X axis of the schedule (Time axis)
function buildXAxis() {
    
    var weekNo = 1;
    var x_axis_weeks = document.getElementsByClassName('x-axis-weeks')[0];
    var index = 0;
    var monthsarray = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];

    for (var i = 0; i < months.length; i++)
    {
        var yr = new Date(months[i]).getFullYear();
        var mn = new Date(months[i]).getMonth();
        var weekCount = 0;
        for (j2 = 0; j2 < unitCalendar.WeekList.length; j2++) {

            var _Week = unitCalendar.WeekList[j2];//2019-04-01
            
            var _WeekStartDate = convertToDate(_Week.WeekStart);
            var _WeekEndDate = convertToDate(_Week.WeekEnd);
            if (mn == _WeekStartDate.getMonth() && yr == _WeekStartDate.getFullYear()) {
                weekCount += 1;
            }
        }

      
        var x_axis_week = document.createElement('div');
        x_axis_week.setAttribute("class", "x-axis-week");
        x_axis_week.style.width = (weekCount * 40) + "px";
   
        x_axis_week.innerHTML = "<span style='font-size:12px;'>" + monthsarray[mn] + " " + "<span style='font-size:10px;'>"+ yr+"</span>";
     //   index += 7;
        x_axis_weeks.appendChild(x_axis_week);

        for (j = 0; j < unitCalendar.WeekList.length; j++) {

            var _Week = unitCalendar.WeekList[j];//2019-04-01
            var _WeekNo = _Week.Description;
            var _WeekId = _Week.Id;
            var _WeekStartDate = convertToDate(_Week.WeekStart);
            var _WeekEndDate = convertToDate(_Week.WeekEnd);
            if (mn == _WeekStartDate.getMonth())
            {
                
                var x_axis_days = document.getElementsByClassName('x-axis-days')[0];

                var x_axis_day = document.createElement('div');
                x_axis_day.setAttribute("class", "x-axis-day");

                x_axis_day.innerHTML = "" + (_WeekId);
                x_axis_day.setAttribute("data-toggle", "tooltip");
                x_axis_day.setAttribute("title", moment(_WeekStartDate).format('Do MMM, YYYY') + " - " + moment(_WeekEndDate).format('Do MMM, YYYY'));
                x_axis_days.appendChild(x_axis_day);
            }
        }
        
    }
}

// Builds the Y Axis of the schedule (Projects or Resources)
function buildYAxis() {
    
    var yAxis = document.getElementsByClassName('y-axis')[0];


    var parentUnits = units.filter(function (el) {
        return el.parentId == 0;
    });


   
    for (i = 0; i < parentUnits.length; i++) {

        var yAxisDiv = document.createElement('div');
        yAxisDiv.setAttribute("class", "d-flex justify-content-between y-axis-div");
        yAxisDiv.style.height = calculateRowHeight(parentUnits[i]) + 'px';
        var editURL = '<span><a href="javascript:void(0);"  onclick="UnitCalendarMaster.addEditSubUnit(0,\'' + parentUnits[i].name + '\','
            + parentUnits[i].id + ',\'' + (new moment(parentUnits[i].startDate)).format("YYYY-MM-DD") + '\',\''
            + (new moment(parentUnits[i].endDate)).format("YYYY-MM-DD") + '\');"><img src="../../Content/VLE/img/svg/unit-plus.svg" /></a> &nbsp;' +
            '<a href="javascript:void(0);" onclick="UnitCalendarMaster.editUnit(' + parentUnits[i].id + ');"><img src="../../Content/VLE/img/svg/unit-edit.svg" /></a> &nbsp;'
            + '<a href="javascript:void(0);" onclick="UnitCalendarMaster.deleteUnit(' + parentUnits[i].id + ',\'' + parentUnits[i].name + '\');">'
            + '<img src="../../Content/VLE/img/svg/unit-delete.svg" /></a></span>';


        var actions = '<div class="btn-group btn-unitcalendar dropright ml-2">'
            + '<a href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
            + '<i class="fas fa-ellipsis-v"></i></a>'
            + '<div class="dropdown-menu z-depth-1 border-0">'
            + '<a class="dropdown-item py-2" href="javascript:void(0);" onclick="UnitCalendarMaster.addEditSubUnit(0,\'' + parentUnits[i].name + '\','
            + parentUnits[i].id + ',\'' + (new moment(parentUnits[i].startDate)).format("YYYY-MM-DD") + '\',\''
            + (new moment(parentUnits[i].endDate)).format("YYYY-MM-DD") + '\');"><img src="../../Content/VLE/img/svg/unit-plus.svg" alt="Add" class="svg mr-2" />' + translatedResources.add +'</a>'
            + '<a class="dropdown-item py-2" href="javascript:void(0);" onclick="UnitCalendarMaster.editUnit(' + parentUnits[i].id + ');"><img src="../../Content/VLE/img/svg/unit-edit.svg" alt="Edit" class="svg mr-2" />' + translatedResources.edit+'</a>'
            + '<a class="dropdown-item py-2" href="javascript:void(0);" onclick="UnitCalendarMaster.deleteUnit(' + parentUnits[i].id + ',\'' + parentUnits[i].name + '\');"><img src="../../Content/VLE/img/svg/unit-delete.svg" alt="Delete" class="svg mr-2" />' + translatedResources.Delete+'</a>'
            + '</div></div>';

        yAxisDiv.innerHTML = '<span>' + parentUnits[i].unitInfoURL+'</span>'  + actions;//"<a href='/course/unit/unitinfo?Id=" + units[i].id + "'>" + units[i].name + "</a>";
        yAxis.appendChild(yAxisDiv);

        var childUnits = units.filter(function (el) {
            return el.parentId == parentUnits[i].id;
        });
       
        for (var k = 0; k < childUnits.length; k++)
        {
            var _yAxisDiv = document.createElement('div');
            _yAxisDiv.setAttribute("class", "d-flex justify-content-between y-axis-div");
            _yAxisDiv.style.height = calculateRowHeight(parentUnits[i]) + 'px';

            //var editURL = '<span><a href="javascript:void(0);" onclick="UnitCalendarMaster.addEditSubUnit(' + childUnits[k].id + ',\'' + parentUnits[i].name + '\',' + childUnits[k].parentId + ',\''
            //    + (new moment(parentUnits[i].startDate)).format("YYYY-MM-DD") + '\',\''
            //    + (new moment(parentUnits[i].endDate)).format("YYYY-MM-DD") + '\');"><img src="../../Content/VLE/img/svg/unit-edit.svg" /></a> &nbsp;'
            //    + '<a href="javascript:void(0);" onclick="UnitCalendarMaster.deleteUnit(' + childUnits[k].id + ',\'' + childUnits[k].name + '\');">'
            //    + '<img src="../../Content/VLE/img/svg/unit-delete.svg" /></a></span>';


            var actions = '<div class="btn-group btn-unitcalendar dropright ml-2"><a href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                + '<i class="fas fa-ellipsis-v"></i></a><div class="dropdown-menu z-depth-1 border-0">'
                + '<a class="dropdown-item py-2" href="javascript:void(0);" onclick="UnitCalendarMaster.addEditSubUnit(' + childUnits[k].id + ',\'' + parentUnits[i].name + '\',' + childUnits[k].parentId + ',\''
                + (new moment(parentUnits[i].startDate)).format("YYYY-MM-DD") + '\',\'' + (new moment(parentUnits[i].endDate)).format("YYYY-MM-DD") + '\');">'
                + '<img src="../../Content/VLE/img/svg/unit-edit.svg" alt="Edit" class="svg mr-2"/>Edit</a>'
                + '<a class="dropdown-item py-2" href="javascript:void(0);" onclick="UnitCalendarMaster.deleteUnit(' + childUnits[k].id + ',\'' + childUnits[k].name + '\');">'
                + '<img src="../../Content/VLE/img/svg/unit-delete.svg" alt="Delete" class="svg mr-2"/>Delete</a></div></div>';
            console.log(actions);
            _yAxisDiv.innerHTML = '<span class="sub-course">' + childUnits[k].unitInfoURL + '</span>' + actions;
            yAxis.appendChild(_yAxisDiv);

        }
    }
}

function editUnit(unitId) {
    alert('called');
}

// Build the schedule
function buildUnitCalendarSchedule() {
    
    var unitCalendarSchedule = document.getElementsByClassName('schedule')[0];

    var _units = units.filter(function (el) {
        return el.parentId == 0;
    });

    for (var i = 0; i < _units.length; i++) {
        var gridRow = document.createElement('div');
        gridRow.setAttribute("id", "p" + _units[i].id);
        gridRow.setAttribute("class", "grid-row");
        unitCalendarSchedule.appendChild(gridRow);

            for (var i3 = 0; i3 < unitCalendar.WeekList.length; i3++)
            {
                var Week = unitCalendar.WeekList[i3];
                console.log('week start ' + Week.WeekStart);
                var _WeekStartDate = moment(Week.WeekStart);
                var gridColumn = document.createElement('div');
            gridColumn.setAttribute("class", "grid-column");
                gridColumn.setAttribute("id", "p" + units[i].id + "-d" + (_WeekStartDate).format("D")
                    + "-m" + ((_WeekStartDate).format("M") + 1)+"-w"+i3);

                // Sets the color of the background of the grid column
                //if ((_WeekStartDate).format("M") != (_WeekStartDate).add(7,'days').format("M")) {
                //gridColumn.style.backgroundColor = '#f7f7f7';
                //} else
                {
                gridColumn.style.backgroundColor = 'rgb(252, 252, 252)';
            }

            gridRow.appendChild(gridColumn);

            var height = calculateRowHeight(units[i]);
            gridColumn.style.height = height + 'px';
                gridRow.style.height = height + 'px';


        }
        //}
        buildSubUnitCalendarSchedule(_units[i].id);
    }
}

function buildSubUnitCalendarSchedule(parentId) {
    var unitCalendarSchedule = document.getElementsByClassName('schedule')[0];
    var _units = units.filter(function (el) {
        return el.parentId == parentId;
    });
    
    for (var i = 0; i < _units.length; i++) {
        var gridRow = document.createElement('div');
        gridRow.setAttribute("id", "p" + _units[i].id);
        gridRow.setAttribute("class", "grid-row");
        unitCalendarSchedule.appendChild(gridRow);

        for (var i3 = 0; i3 < unitCalendar.WeekList.length; i3++) {
            var Week = unitCalendar.WeekList[i3];
            console.log('week start ' + Week.WeekStart);
            var _WeekStartDate = moment(Week.WeekStart);
            var gridColumn = document.createElement('div');
            gridColumn.setAttribute("class", "grid-column");
            gridColumn.setAttribute("id", "p" + units[i].id + "-d" + (_WeekStartDate).format("D")
                + "-m" + ((_WeekStartDate).format("M") + 1) + "-w" + i3);

            // Sets the color of the background of the grid column
            //if ((_WeekStartDate).format("M") != (_WeekStartDate).add(7, 'days').format("M")) {
            //    gridColumn.style.backgroundColor = '#f7f7f7';
            //} else
            {
                gridColumn.style.backgroundColor = 'rgb(252, 252, 252)';
            }

            gridRow.appendChild(gridColumn);

            var height = calculateRowHeight(units[i]);
            gridColumn.style.height = height + 'px';
            gridRow.style.height = height + 'px';


        }
        //}
    }
}

// Populate the schedule with unitDurations

function populateUnitCalendarSchedule()
{
    console.log("unit duration" + unitDurations.length);

    var _unitDurations = unitDurations.filter(function (el) {
        return el.parentId == 0;
    });


    for (var i = 0; i < _unitDurations.length; i++)
    {
        var unitRow = document.getElementById("p" + _unitDurations[i].unit.id);
        var unitDuration = document.createElement("div");
        unitDuration.setAttribute("class", "assignment");

        var startday = new Date(months[0]).getDay();
        console.log("startday :" + _unitDurations[i].start_date);
        console.log("unit start day :" + (moment(_unitDurations[i].start_date).format("D")));


        var weekInCurrentMonth = moment(_unitDurations[i].end_date).diff(moment(_unitDurations[i].start_date), 'days');
        unitDuration.style.width = ((weekInCurrentMonth/7) * 40) + "px";
        unitDuration.style.height = "10px";
        unitDuration.style.background = _unitDurations[i].resource.unitDurationColor;
        var top = determineUnitDurationTop(_unitDurations[i]);
        unitDuration.style.top = top;
        var weekInCurrentMonth = 0;
        
        //box 40px * 4
        var weeks = moment(_unitDurations[i].start_date).diff(moment(months[0]), 'days');
        unitDuration.style.left = ((Math.abs(weeks) / 7) * 40) + "px";
        unitDuration.setAttribute("data-toggle", "tooltip");
        unitDuration.setAttribute("title", moment(_unitDurations[i].start_date).format('Do MMM, YYYY') + " - " + moment(_unitDurations[i].end_date).format('Do MMM, YYYY'));
        unitDuration.innerHTML = _unitDurations[i].resource.name;
        unitRow.appendChild(unitDuration);

        populatechildDurations(_unitDurations[i].unit.id);

    }
}
function populatechildDurations(parentId) {
    
    var _unitDurations = unitDurations.filter(function (el) {
        return el.parentId == parentId;
    });

    for (var i = 0; i < _unitDurations.length; i++) {
        var unitRow = document.getElementById("p" + _unitDurations[i].unit.id);
        var unitDuration = document.createElement("div");
        unitDuration.setAttribute("class", "assignment");

        var startday = new Date(months[0]).getDay();
        console.log("startday :" + _unitDurations[i].start_date);
        console.log("unit start day :" + (moment(_unitDurations[i].start_date).format("D")));


        var weekInCurrentMonth = moment(_unitDurations[i].end_date).diff(moment(_unitDurations[i].start_date), 'days');
        unitDuration.style.width = ((weekInCurrentMonth / 7) * 40) + "px";
        unitDuration.style.height = "10px";
        unitDuration.style.background = _unitDurations[i].resource.unitDurationColor;
        var top = determineUnitDurationTop(_unitDurations[i]);
        unitDuration.style.top = top;
        var weekInCurrentMonth = 0;

        //box 40px * 4
        var weeks = moment(_unitDurations[i].start_date).diff(moment(months[0]), 'days');
        unitDuration.style.left = ((Math.abs(weeks) / 7) * 40) + "px";
        unitDuration.setAttribute("data-toggle", "tooltip");
        unitDuration.setAttribute("title", moment(_unitDurations[i].start_date).format('Do MMM, YYYY') + " - " + moment(_unitDurations[i].end_date).format('Do MMM, YYYY'));
        unitDuration.innerHTML = _unitDurations[i].resource.name;
        unitRow.appendChild(unitDuration);

    }
}
function convertToDate(dt) {
    var parsedDate = new Date(parseInt(dt.substr(6)));
    var jsDate = new Date(parsedDate); //Date object
    return jsDate;
}

