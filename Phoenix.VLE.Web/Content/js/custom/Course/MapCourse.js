﻿
//------------------------Change Group Functionality-------------------------
var homeSchoolId = 0;
var headerHtml = `<div id="{0}" class="bg-primary rounded-1x text-white"><h6 class="p-2">{1}</h6></div>`
var mapCourse = function () {
    var init = function () {
        common.init();
        mapCourse.gradeDropDown = $('#ddlGrade');
        $('#breadcrumbActions').html(`<button type="button" id="btnTypeDetails" data-toggle="modal" data-target="#fluidModalRightSuccessDemo" class="btn btn-primary btn-sm"><i class="fas fa-plus mr-2"></i>${translatedResources.includeStudentFromOtherSchools}</button>`);
        if (globalFunctions.isValueValid(stuLs) && stuLs.length > 0) {
            var gradeIds = stuLs
                .map((e) => e.GradeId)
                .filter((value, index, self) => self.indexOf(value) === index);

            mapCourse.schoolList = stuLs.map((e) => e.SchoolId + '|' + e.SchoolName)
                .filter((value, index, self) => self.indexOf(value) === index);
            mapCourse.gradeDropDown.val(gradeIds).selectpicker('refresh');
            mapCourse.fillGroupDdl(gradeIds).then(response => {
                var selectedStudent = {
                    studentList: response.studentList.filter(x => stuLs.some(y => y.StudentId == x.StudentId))
                };
                var unSelectedStudent = {
                    studentList: response.studentList.filter(x => !stuLs.some(y => y.StudentId == x.StudentId)),
                    SectionList: response.SectionList
                }
                mapCourse.bindStudentList(unSelectedStudent);
                mapCourse.bindMoveToGroupStudentList(selectedStudent);
            });

        }
        mapCourse.leadGroupAssignGrid(teacherAssignData);
    },
        gradeDropDown = undefined,
        schoolList = [],
        fillGroupDdl = function (gradeIds) {
            var gradeIdsData = Array.from($('select.gradeidsList').map(function () { return $(this).val(); }));
            var gradeIds = gradeIds ? gradeIds.join(',') : mapCourse.gradeDropDown.val().join(',');
            if (gradeIdsData.length > 0)
                gradeIds = gradeIds + ',' + gradeIdsData.join(',');
            return common.postRequest('/Course/Course/GetStudentByGradeIds', { id: gradeIds })
        },
        bindStudentList = function (response) {
            var result = response;
            var ddlSection = $('#ddlSection');
            ddlSection.empty();
            ddlSection.append("<option value=''>" + translatedResources.selectedText + "</option>");
            $.each(result.SectionList, function (i, item) {
                var SectionOptionText = `(${item.SchoolShortName}) ${item.GradeDisplay} | ${item.SectionName}`;
                ddlSection.append($('<option></option>').val(item.SectionId).html(SectionOptionText));
            });
            ddlSection.selectpicker('refresh');

            var StudentList = $('#StudentList');
            StudentList.html("");
            $.each(result.studentList, function (i, item) {
                var htmlString = '';
                if (StudentList.find(`input[data-schoolId="${item.SchoolId}"]`).length <= 0)
                    htmlString = headerHtml.replace('{0}', item.SchoolId).replace('{1}', item.SchoolName);

                htmlString += `<div class='stud-listalign custom-control custom-checkbox ml-1' data-search-text='${item.StudentName} ${item.GradeDisplay} | ${item.SectionName}' data-searchsection-text='${item.SectionId}'>
                    <input type='hidden' class='gradeId' value='${item.SchoolGradeId}' />
                    <input type='hidden' class='studentId' value='${item.StudentId}' />
                    <input onchange='mapCourse.setSelectAll(this,".stud-listalign");' type='checkbox' class='custom-control-input' id='${item.StudentId}' value='${item.StudentId}' data-sectionid='${item.SectionId}' data-schoolId = '${item.SchoolId}' data-schoolname = '${item.SchoolName}' data-studentgroupid='${item.StudentGroupId}'>
                    <label class='custom-control-label' for='${item.StudentId}'>${item.StudentName} <span>${item.GradeDisplay} | ${item.SectionName}</span></label>
                    </div>`;
                StudentList.append(htmlString);
            });
            $('#divAllocateGroup').show();
            mapCourse.setCustomScrollbar();
        },
        setCustomScrollbar = function () {
            $(".student-allocation-wrapper").mCustomScrollbar({
                setHeight: "350",
                autoExpandScrollbar: true
            });
        },
        bindMoveToGroupStudentList = function (response) {
            var result = response;
            var MoveToGroupStudentList = $('#SelectedStudentList');
            MoveToGroupStudentList.html("");
            $.each(result.studentList, function (i, item) {
                var htmlString = '';
                if (MoveToGroupStudentList.find(`input[data-schoolId="${item.SchoolId}"]`).length <= 0)
                    htmlString = headerHtml.replace('{0}', item.SchoolId).replace('{1}', item.SchoolName);

                htmlString += "<div class='stud-listalign custom-control custom-checkbox ml-1' data-search-text='" + item.StudentName + " " + item.GradeDisplay + " | " + item.SectionName + "' data-searchsection-text='" + item.SectionId + "'>" +
                    "<input type='hidden' class='gradeId' name ='Students[" + i + "].GradeId' value='" + item.SchoolGradeId + "' />" +
                    "<input type='hidden' class='studentId' name ='Students[" + i + "].StudentId' value='" + item.StudentId + "' />" +
                    "<input type='checkbox' class='custom-control-input' id='" + item.StudentId + "' value='" + item.StudentId + "' data-sectionid='" + item.SectionId + "' data-schoolId = '" + item.SchoolId + "' data-studentgroupid='" + item.StudentGroupId + "'>" +
                    "<label class='custom-control-label' for='" + item.StudentId + "'> " + item.StudentName + " <span>" + item.GradeDisplay + " | " + item.SectionName + "</span></label>" +
                    "</div>"
                MoveToGroupStudentList.append(htmlString);
            })
        },
        addStudentInList = function () {
            var $selectedStudentList = $("#StudentList input[type='checkbox']:checked").parent('.custom-checkbox');
            mapCourse.addRemoveNameAttrToStudent($selectedStudentList, true);
            if ($selectedStudentList != null && $selectedStudentList.length > 0) {
                $($selectedStudentList).addClass('added-stud-listalign').show();
                $("#SelectedStudentList").append($selectedStudentList);
                $("#SelectedStudentList").html($("#SelectedStudentList .custom-checkbox").sort(function (a, b) {
                    return $(a).children('.custom-control-input').data('schoolid') == $(b).children('.custom-control-input').data('schoolid') ? 0 : $(a).children('.custom-control-input').data('schoolid') < $(b).children('.custom-control-input').data('schoolid') ? -1 : 1;
                }));
                if ($("#StudentList input[type='checkbox']").parent('.custom-checkbox').length == 0) {
                    $("#chkSelectAllStudentToAdd").prop('checked', false);
                }
                $("#SelectedStudentList input[type='checkbox']:enabled").prop('checked', $('#chkSelectAllStudentToRemove').is(':checked'));
                $(document).on("change", "#SelectedStudentList input[type='checkbox']", function (e) {
                    mapCourse.setSelectAll(this, '.added-stud-listalign');
                });
                mapCourse.appendSchoolHeader('SelectedStudentList');
                mapCourse.appendSchoolHeader('StudentList');
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.selectStudentAdd);
            }
        },
        removeStudentFromList = function () {
            var $selectedStudentList = $("#SelectedStudentList input[type='checkbox']:checked").parent('.custom-checkbox');
            if ($selectedStudentList != null && $selectedStudentList.length > 0) {
                $($selectedStudentList).removeClass('added-stud-listalign');
                $("#StudentList").append($selectedStudentList);
                $("#StudentList").html($("#StudentList .custom-checkbox").sort(function (a, b) {
                    return $(a).children('.custom-control-input').data('schoolid') == $(b).children('.custom-control-input').data('schoolid') ? 0 : $(a).children('.custom-control-input').data('schoolid') < $(b).children('.custom-control-input').data('schoolid') ? -1 : 1;
                }));
                if ($("#SelectedStudentList input[type='checkbox']").parent('.custom-checkbox').length == 0) {
                    $("#chkSelectAllStudentToRemove").prop('checked', false);
                }
                $("#StudentList input[type='checkbox']").prop('checked', $('#chkSelectAllStudentToAdd').is(':checked'));
                mapCourse.addRemoveNameAttrToStudent($selectedStudentList, false);
                mapCourse.appendSchoolHeader('StudentList');
                mapCourse.appendSchoolHeader('SelectedStudentList');
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.selectStudentRemove);
            }
        },
        searchInSelectedStudentList = function (e) {
            var searchText = $(e).val().toLowerCase();
            $('#SelectedStudentList div.stud-listalign').each(function () {
                var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
                $(this).toggle(showCurrentLi);
            });
        },
        searchInStudentList = function () {
            var searchSection = $('#ddlSection').val().join(",");
            var searchText = $('#studentSearchBox').val().toLowerCase();
            $('#StudentList div.stud-listalign').each(function () {
                var showCurrentLi;
                if (globalFunctions.isValueValid(searchSection) && globalFunctions.isValueValid(searchText))
                    showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1 &&
                        searchSection.indexOf($(this).attr('data-searchsection-text')) !== -1;

                else if (globalFunctions.isValueValid(searchSection))
                    showCurrentLi = searchSection.indexOf($(this).attr('data-searchsection-text')) !== -1;

                else if (globalFunctions.isValueValid(searchText))
                    showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;

                $(this).toggle(showCurrentLi);
            });
            stuLs.map((e) => e.SchoolId)
                .filter((value, index, self) => self.indexOf(value) === index)
                .forEach(x => {
                    if ($(`#StudentList input[data-schoolid="${x}"]:visible`).length > 0)
                        $('#' + x).show();
                    else
                        $('#' + x).hide();
                });
        },
        saveChangeGroupData = function (ChangeGroupData) {
            var ChangeGroupDataList = JSON.stringify({ 'ChangeGroupDataList': ChangeGroupData });
            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: '/SubjectSetting/SubjectSetting/SaveChangeGroupData',
                data: ChangeGroupDataList,
                success: function (response) {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    $(".closeRightDrawer").trigger("click");
                    mapCourse.bindStudentList();
                    mapCourse.bindMoveToGroupStudentList();
                },
                failure: function (response) {
                    $('#result').html(response);
                }
            });
        },
        changeSelectedStudentGroup = function (ChangeSelectedStudentGroup) {
            var changeSelectedStudentGroup = JSON.stringify({ 'changeSelectedStudentGroup': ChangeSelectedStudentGroup });
            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: '/SubjectSetting/SubjectSetting/ChangeSelectedStudentGroup',
                data: changeSelectedStudentGroup,
                success: function (response) {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    mapCourse.bindStudentList();
                    mapCourse.bindMoveToGroupStudentList();
                },
                failure: function (response) {
                    $('#result').html(response);
                }
            });
        },
        addRemoveNameAttrToStudent = function (elements, isAdd) {
            elements = elements.toArray();
            if (isAdd) {
                elements = elements.concat($("#SelectedStudentList input[type='checkbox']").parent('.custom-checkbox').toArray());
            } else {
                var ele = $("#SelectedStudentList input[type='checkbox']:not(:checked)").parent('.custom-checkbox').toArray();
                ele.forEach(function (e, i) {
                    $(e).find('input[class=gradeId]').attr('name', "Students[" + i + "].GradeId");
                    $(e).find('input[class=studentId]').attr('name', "Students[" + i + "].StudentId");
                })
            }
            elements.forEach(function (e, i) {
                $(e).find('input[class=gradeId]')[isAdd ? 'attr' : 'removeAttr']('name', "Students[" + i + "].GradeId");
                $(e).find('input[class=studentId]')[isAdd ? 'attr' : 'removeAttr']('name', "Students[" + i + "].StudentId");
            })
        },
        onSaveSuccess = function (response) {
            $('.loader-wrapper').fadeOut(500, function () {
            });
            globalFunctions.showMessage(response.NotificationType, response.Message)
            if (response.Success) {
                location.href = "/Course/Course/mapCourse?id=" + courseId;
            }
        },
        checkSelectedStudents = function () {
            var response = true;
            if (!$('#frmAddEditCourse').valid()) {
                response = false;
            }
            else {
                if ($("#SelectedStudentList .custom-checkbox").length == 0) {
                    globalFunctions.showWarningMessage(translatedResources.pleaseSelectStudent);
                    response = false;
                } else if (mapCourse.getAssignGridData().length == 0) {
                    globalFunctions.showWarningMessage(translatedResources.selectTeacher);
                    response = false;
                }
                else {
                    $.ajax({
                        type: "GET",
                        url: '/Course/course/IsGroupNameUnique',
                        data: { title: $('#GroupName').val(), previousGroupName: $('#previousGroupName').val() },
                        dataType: 'json',
                        async: false,
                        success: function (res) {
                            if (!res) {
                                globalFunctions.showWarningMessage(translatedResources.groupAlreadyExist);
                            } else {
                                $('.loader-wrapper').fadeIn(500, function () { });
                            }
                            response = res
                        },
                        error: function (error) {
                            globalFunctions.onFailure();
                            response = false;
                        }
                    });
                    //$.get('/Course/course/IsGroupNameUnique', { title: $('#GroupName').val(), previousGroupName: $('#previousGroupName').val() })
                    //    .then(response => {
                    //        if (!response) {
                    //            globalFunctions.showWarningMessage(translatedResources.groupAlreadyExist);
                    //        } else {
                    //            $('.loader-wrapper').fadeIn(500, function () { });
                    //        }
                    //        return resolve(response);
                    //    });
                }
            }
            return response
        },
        setSelectAll = function setSelectAll(obj, cssClass) {
            if (cssClass == '.stud-listalign') {
                if ($(obj).parent().hasClass("added-stud-listalign")) {

                    if (!$(obj).is(':checked')) {
                        $('#chkSelectAllStudentToRemove').prop('checked', false);
                    }
                    else {
                        var totalCount = $(".added-stud-listalign").length
                        var selectedCount = $(".added-stud-listalign").find("input:checked").length;
                        if ((totalCount) == (selectedCount)) { $('#chkSelectAllStudentToRemove').prop('checked', true); }
                    }

                }
                else {
                    if (!$(obj).is(':checked')) {
                        $('#chkSelectAllStudentToAdd').prop('checked', false);
                    }
                    else {
                        var totalCount = $(cssClass).length;
                        var selectedCount = $(cssClass).find("input:checked").length;
                        if ((totalCount - 1) == (selectedCount + 1)) { $('#chkSelectAllStudentToAdd').prop('checked', true); }
                    }
                }
            }

        },
        GetSchoolListByTeacher = function (TeacherId) {
            var selectedSchools = $('#otherSchoolList').val();
            var response = common.bindSingleDropDownByParameter(
                "#otherSchoolList",
                '/Course/Course/GetSchoolListByTeacherId',
                { TeacherId: TeacherId },
                "",
                translatedResources.selectedText,
                false, undefined, undefined,
                true
            );
            $('#otherSchoolList').val(selectedSchools).selectpicker('refresh');
        },
        GetGradeBySchoolId = function (SchoolId) {
            //var TeacherId = $('#ddlTeacher').val();
            var TeacherId = mapCourse.getAssignGridData().map(function (i) { return i.TeacherId });
            common.bindSingleDropDownByParameter(
                "#ddlGrade",
                '/Course/Course/GeteGradeListPassByschoolIds',
                { SchoolId: SchoolId, TeacherId: TeacherId.join(',') },
                "",
                '',
                false, undefined, undefined,
                true
            );
        },
        appendSchoolHeader = function (studentListId) {
            Array.from($(`#${studentListId} input[type="checkbox"]`)
                .map((i, e) => $(e).data('schoolid')))
                .filter((value, index, self) => self.indexOf(value) === index)
                .forEach(school => {
                    if ($(`#${studentListId}`).find(`div[id="${school}"]`).length <= 0) {
                        if (mapCourse.schoolList.some(x => x.split('|')[0] == school)) {
                            var schoolName = mapCourse.schoolList.find(x => x.split('|')[0] == school).split('|')[1];
                            var html = headerHtml.replace('{0}', school).replace('{1}', schoolName);
                            $(`#${studentListId} input[data-schoolid="${school}"]:first`).parent().before(html);
                        }
                    }

                });

            //to remove school label when no student is present 
            mapCourse.schoolList.forEach(schools => {
                var schoolId = schools.split('|')[0];
                if ($(`#${studentListId} input[data-schoolid="${schoolId}"]`).length <= 0) {
                    $(`#${studentListId}`).find(`div[id="${schoolId}"]`).remove();
                }
            });
        },
        leadGroupAssignGrid = function (data) {
            data.forEach(x => {
                x.TeacherName = x.IsAdmin ? '<i class="fas fa-user"></i> ' + x.TeacherName.replace(/(<([^>]+)>)/ig, '') : x.TeacherName;
                x['Actions'] = x.IsAdmin ? '' : `
                <div class="tbl-actions">
                    <a class="table-action-icon-btn" onclick="mapCourse.editGroupAssign(${x.TeacherId})"><img src="/Content/vle/img/svg/pencil-big.svg"></a>
                    <a class="table-action-icon-btn" onclick="mapCourse.deleteGroupAssign(${x.TeacherId})"><img src="/Content/vle/img/svg/delete.svg"></a>
                </div>`;
            });
            var grid = new DynamicGrid('groupAssignGrid');
            var _columnData = [];
            _columnData.push(
                { "mData": "TeacherName", "sTitle": translatedResources.teacherName, "sWidth": "30%" },
                { "mData": "AssignFrom", "sTitle": translatedResources.assignmentFrom, "sWidth": "20%" },
                { "mData": "AssignTo", "sTitle": translatedResources.assignmentTo, "sWidth": "20%" },
                { "mData": "IsCoreTeacher", "sTitle": translatedResources.isCoreTeacher, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "bSorting": false, "sTitle": translatedResources.action, "sWidth": "20%" }
            );
            var settings = {
                columnData: _columnData,
                data: data,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [3],
                }],
                "iDisplayLength": 5,
                searching: true
            };
            grid.init(settings);
            $("#groupAssignGrid_filter").hide();
            $('#groupAssignGrid').DataTable().search('').draw();
            $('#groupAssignTeacherFields,#teacherNameBadge').html('');
            data.forEach((x, i) => {
                var editDeleteHtml = '', adminIcon = '', badgeClass = '';
                Object.keys(x).forEach(key => {
                    if (key != "Actions") {
                        $("<input />", { type: "hidden", value: (typeof (x[key]) === "string" ? x[key].replace(/(<([^>]+)>)/ig, '') : x[key]), name: `GroupAssignTeachers[${i}].${key}` }).appendTo("#groupAssignTeacherFields");
                    }
                })
                if (!x.IsAdmin) {
                    editDeleteHtml = `<span onclick="mapCourse.editGroupAssign(${x.TeacherId}, true)" class="cursor-pointer fas fa-pencil-alt fa-pencil-alt fas pl-2 pr-2"/> 
                   <span onclick="mapCourse.deleteGroupAssign(${x.TeacherId})" class="cursor-pointer fas fa-trash"/></span>`;
                }
                if (x.IsUserFromCurrentSchool) {
                    badgeClass = x.IsAdmin ? 'badge-primary' : 'badge-info';
                } else {
                    badgeClass = x.IsAdmin ? 'badge-warning' : 'badge-inprogress';
                }
                var fromto = x.AssignFrom ? `${x.AssignFrom} - ${x.AssignTo}` : '';
                var html = `<h5  data-toggle="tooltip" data-original-title="${x.IsAdmin ? '' : fromto}" ${x.IsAdmin ? `` : `onclick="mapCourse.editGroupAssign(${x.TeacherId}, true)"`} class="p-2 cursor-pointer"><span href="javascript:;" class="badge badge-pill ${badgeClass} pb-2 pl-3 pr-3 pt-2">${adminIcon} ${x.TeacherName}${editDeleteHtml}</h5>`;

                $("#teacherNameBadge").append(html);
            });
            resetGroupAssignForm();
        },
        editGroupAssign = function (teacherId, isBadgeEdit) {

            if (isBadgeEdit) {
                $('#assignTeacherDrawer').modal('show');
            }

            var data = getAssignGridData().find(x => x.TeacherId == teacherId);
            $('#ddlTeacherList').val(data.TeacherId).attr("disabled", "disabled").selectpicker('refresh');
            $("#hddTeacherId").val(data.TeacherId);
            $('#AssignFrom').val(data.AssignFrom);
            $('#AssignTo').val(data.AssignTo);
            $('#isCoreOrSubstitute').prop('checked', data.IsCoreTeacher).change();
            $('#groupAssignForm').find('label').addClass('active');
        },
        deleteGroupAssign = function (teacherId) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                var data = getAssignGridData().filter(x => x.TeacherId != teacherId);
                leadGroupAssignGrid(data);
            });
        },
        addEditTeacher = function () {
            if (!common.IsInvalidByFormOrDivId('groupAssignForm'))
                return;

            var data = getAssignGridData();

            if (globalFunctions.isValueValid($("#hddTeacherId").val())) {
                data = data.filter(x => x.TeacherId != $("#hddTeacherId").val());
            }

            var object = {
                TeacherName: $('#ddlTeacherList option:selected').text(),
                TeacherId: $('#ddlTeacherList').val(),
                AssignFrom: $('#AssignFrom').val(),
                AssignTo: $('#AssignTo').val(),
                IsCoreTeacher: $('#isCoreOrSubstitute').prop('checked'),
                IsAdmin: $('#ddlTeacherList option:selected').data('isuseradmin').toBoolean(),
                IsUserFromCurrentSchool: $('#ddlTeacherList option:selected').data('iscurrentschool')
            }

            if (data.some(x => x.TeacherId == object.TeacherId)) {
                globalFunctions.showWarningMessage(translatedResources.teacherSelected);
                return;
            }

            data.push(object);
            leadGroupAssignGrid(data);            
        },
        resetGroupAssignForm = function () {
            $('#ddlTeacherList').removeAttr("disabled");
            common.ResetForm('groupAssignForm')
        },
        getAssignGridData = function () {
            return Array.from($('#groupAssignGrid').DataTable().data());
        }
    return {
        init: init,
        fillGroupDdl: fillGroupDdl,
        addStudentInList: addStudentInList,
        removeStudentFromList: removeStudentFromList,
        bindStudentList: bindStudentList,
        bindMoveToGroupStudentList: bindMoveToGroupStudentList,
        saveChangeGroupData: saveChangeGroupData,
        changeSelectedStudentGroup: changeSelectedStudentGroup,
        setCustomScrollbar: setCustomScrollbar,
        searchInStudentList: searchInStudentList,
        searchInSelectedStudentList: searchInSelectedStudentList,
        addRemoveNameAttrToStudent: addRemoveNameAttrToStudent,
        onSaveSuccess: onSaveSuccess,
        checkSelectedStudents: checkSelectedStudents,
        setSelectAll: setSelectAll,
        GetSchoolListByTeacher: GetSchoolListByTeacher,
        GetGradeBySchoolId: GetGradeBySchoolId,
        appendSchoolHeader: appendSchoolHeader,
        schoolList: schoolList,
        gradeDropDown: gradeDropDown,
        leadGroupAssignGrid: leadGroupAssignGrid,
        editGroupAssign: editGroupAssign,
        deleteGroupAssign: deleteGroupAssign,
        addEditTeacher: addEditTeacher,
        resetGroupAssignForm: resetGroupAssignForm,
        getAssignGridData: getAssignGridData
    }
}();
//------------------------Click/Change Event Functionality Of Change Group-------------------
$(document).on("hide.bs.select", "#ddlGrade,#otherSchoolGrade", function (e) {
    var ddlGrade = mapCourse.gradeDropDown.val();
    //var ddlTeacher = $('#ddlTeacher').val();
    //var ddlTeacher = mapCourse.getAssignGridData().map(function (i) { return i.TeacherId });
    var gradeIds = $('.gradeidsList').map(function () { return $(this).val(); });

    //if (!globalFunctions.isValueValid(ddlTeacher)) {
    //    globalFunctions.showWarningMessage(translatedResources.selectTeacher);
    //    return false;
    //}

    if (!globalFunctions.isValueValid(ddlGrade)) {
        globalFunctions.showWarningMessage(translatedResources.selectGrade);
        return false;
    }

    $('#StudentList').html('');
    //$('#SelectedStudentList').html('');
    mapCourse.fillGroupDdl()
        .then(response => {
            mapCourse.schoolList = response.studentList.map((e) => e.SchoolId + '|' + e.SchoolName)
                .filter((value, index, self) => self.indexOf(value) === index);
            var selectedStudent = {
                studentList: response.studentList.filter(x => stuLs.some(y => y.StudentId == x.StudentId))
            };
            var unSelectedStudent = {
                studentList: response.studentList.filter(x => !stuLs.some(y => y.StudentId == x.StudentId)),
                SectionList: response.SectionList
            }
            mapCourse.bindStudentList(unSelectedStudent);
            mapCourse.bindMoveToGroupStudentList(selectedStudent);
        });
});

$(document).on("change", "#chkSelectAllStudentToAdd", function (e) {
    var $inputs = $('#StudentList input[type=checkbox]:visible');
    if (e.originalEvent === undefined) {
        var allChecked = true;
        $inputs.each(function () {
            allChecked = allChecked && this.checked;
        });
        this.checked = allChecked;
    } else {
        $inputs.prop('checked', this.checked);
    }
});

$(document).on("change", "#ddlSection", function () {
    mapCourse.searchInStudentList();
});

$(document).on("change", "#chkSelectAllStudentToRemove", function (e) {
    var $inputs = $('#SelectedStudentList input[type=checkbox]:visible');
    if (e.originalEvent === undefined) {
        var allChecked = true;
        $inputs.each(function () {
            allChecked = allChecked && this.checked;
        });
        this.checked = allChecked;
    } else {
        $inputs.prop('checked', this.checked);
    }
});

$(document).on("click", "#btnSaveGroup", function () {
    console.log("--------- Mandatory Subject-----------------");
    var SubjectGradeList = [];
    var MandatoryList = $('select.ddlMandatory');
    for (var i = 0; i < MandatoryList.length; i++) {
        var SubjectGrade_Id = MandatoryList[i].id;
        var SubjectGroup_Id = MandatoryList[i].selectedOptions[0].value;
        if (SubjectGrade_Id != "" && SubjectGroup_Id != "") {
            SubjectGradeList.push({
                SSD_ACD_ID: $("#ddlAcademicYear").val(),
                SSD_GRD_ID: $("#ddlGrade").val(),
                SSD_SBG_ID: SubjectGrade_Id,
                SSD_SGR_ID: SubjectGroup_Id,
                SSD_SCT_ID: $("#hdnSectionId").val(),
                SSD_STU_ID: $("#hdnStudentId").val(),
                IsOptional: false
            });
        }
    }
    console.log("---------Optional Subject-----------------");
    var OptionalList = $("select.ddlOptionalGroup");
    for (var i = 0; i < OptionalList.length; i++) {
        var SubjectGrade_Id = $(OptionalList[i]).attr("data-subjectid");
        var SubjectGroup_Id = OptionalList[i].selectedOptions[0].value;
        if (SubjectGrade_Id != "" && SubjectGroup_Id != "") {
            SubjectGradeList.push({
                SSD_ACD_ID: $("#ddlAcademicYear").val(),
                SSD_GRD_ID: $("#ddlGrade").val(),
                SSD_SBG_ID: SubjectGrade_Id,
                SSD_SGR_ID: SubjectGroup_Id,
                SSD_SCT_ID: $("#hdnSectionId").val(),
                SSD_STU_ID: $("#hdnStudentId").val(),
                IsOptional: true
            });
        }
    }
    mapCourse.saveChangeGroupData(SubjectGradeList);
});

$(document).on("click", "#btnSaveStudentToMove", function (e) {
    var $inputs = $('#SelectedStudentList input[type=checkbox]:enabled');
    var optionNameSelectedValue = "";
    var StudentGroupIds = [];
    $inputs.each(function (i, elem) {
        StudentGroupIds.push($(this).val());
    });

    var ChangeSelectedStudentGroup = {
        CourseId: $("#ddlMoveToGroup").val(),
        StudentGroupIds: StudentGroupIds
    }
    if (StudentGroupIds.length > 0) {
        mapCourse.changeSelectedStudentGroup(ChangeSelectedStudentGroup);
    }
    else {
        globalFunctions.showWarningMessage('No student data to change group.');
    }
});

$(document)
    .ready(function () {
        mapCourse.init();
        $('#btnBack').show().on('click', function () {
            location.href = "/Course/Course/mapCourse";
        });
    })
    .on('change', '#ddlSchoolList', function () {
        common.bindSingleDropDownByParameter('#ddlGrade', '/Course/Course/GetGradesBySchoolId', { schoolId: $(this).val() });
    })


//$('#ddlTeacher').on('hide.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    //var TeacherIds = $(this).val();
$('#assignTeacherDrawer').on('hide.bs.modal', function () {
    mapCourse.resetGroupAssignForm();
    var TeacherIds = mapCourse.getAssignGridData().map(function (i) { return i.TeacherId });
    if (globalFunctions.isValueValid(TeacherIds)) {
        mapCourse.GetSchoolListByTeacher(TeacherIds.join(','));
    }
});
$(document).on('hide.bs.select', '#otherSchoolList', function (e, clickedIndex, isSelected, previousValue) {
    var SchoolIds = $(this).val().join(',');
    //var TeacherId = $('#ddlTeacher').val();
    var TeacherId = mapCourse.getAssignGridData().map(function (i) { return i.TeacherId });
    var selectedGrades = $('#otherSchoolGrade').val();
    if (globalFunctions.isValueValid(TeacherId) && globalFunctions.isValueValid(SchoolIds)) {
        common.bindSingleDropDownByParameter(
            "#otherSchoolGrade",
            '/Course/Course/GeteGradeListPassByschoolIds',
            { SchoolId: SchoolIds, TeacherId: TeacherId.join(',') },
            "",
            translatedResources.selectedText,
            false, undefined, undefined,
            true
        );
        $('#otherSchoolGrade').val(selectedGrades).selectpicker('refresh');
    }
});

$(document).on('dp.change', 'input[data-mindateid],input[data-maxdateid]', function (e) {
    try {
        if (globalFunctions.isValueValid($(this).data('mindateid'))) {
            var minDate = $(`#${$(this).data('mindateid')}`);
            minDate.data("DateTimePicker").maxDate(e.date);
            //$(this).data("DateTimePicker").maxDate(new Date());
        }

        else if (globalFunctions.isValueValid($(this).data('maxdateid'))) {
            $(`#${$(this).data('maxdateid')}`).data("DateTimePicker").minDate(e.date);
            //$(`#${$(this).data('maxdateid')}`).data("DateTimePicker").maxDate(new Date());
        }
    } catch (e) {

    }

})
