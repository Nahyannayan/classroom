﻿
var course = function () {
    var init = function () {

        loadCourseGrid();
    },
        loadCourseGrid = function () {
            alert('called');
            $.get('/Course/Course/LoadCourse', { curriculumId: $('#CurriculumId').val() }).then((response) => {
                $('#divCourseGrid').html('');
                $('#divCourseGrid').html(response);

            }).catch((error) => {
                globalFunctions.showMessage(error.NotificationType, error.Message)
            });

            //var _columnData = [];
            //_columnData.push(
            //    { "mData": "Title", "sTitle": translatedResources.Title, "sWidth": "20%" },
            //    { "mData": "Description", "sTitle": translatedResources.Description, "sWidth": "30%" },
            //    { "mData": "CourseType", "sTitle": translatedResources.CourseType, "sWidth": "10%" },
            //    { "mData": "StartDate", "sTitle": translatedResources.StartDate, "sWidth": "10%" },
            //    { "mData": "EndDate", "sTitle": translatedResources.EndDate, "sWidth": "10%" }
            //);
            //if (isCustomPermissionEnabled.toBoolean()) {
            //    _columnData.push({ "mData": "Actions", "sClass": "text-center no-sort", "sortable": false, "sTitle": translatedResources.actions, "sWidth": "10%" });
            //}
            //initCourseGrid('tbl-course', _columnData, '/course/course/LoadCourse?curriculumId=' + $('#CurriculumId').val());
        },

        initCourseGrid = function (_tableId, _columnData, _url) {

            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [1, 2]
                }],
                searching: true
            };
            grid.init(settings);

            $("#tbl-course_filter").hide();
            $('#tbl-course').DataTable().search('').draw();
            $("#courseListSearch").on("input", function (e) {
                e.preventDefault();
                $('#tbl-course').DataTable().search($(this).val()).draw();
            });
        },
        loadAddEditCourseView = function (id) {

            $.get('/Course/Course/AddEditCourseById', { id: id }).then((response) => {
                $('#divRenderPartial').html('');
                $('#divRenderPartial').html(response);

            }).catch((error) => {
                globalFunctions.showMessage(error.NotificationType, error.Message)
            });
        },
        loadUnitCalenarView = function (id) {
            $.get('/Course/Course/unitcalendar', { id: id }).then((response) => {
                $('#divRenderPartial').html('');
                $('#divRenderPartial').html(response);

            }).catch((error) => {
                globalFunctions.showMessage(error.NotificationType, error.Message)
            });
        },
        loadCurriculumMapView = function (id) {
            $.get('/Course/Course/curriculummap', { id: id }).then((response) => {
                $('#divRenderPartial').html('');
                $('#divRenderPartial').html(response);

            }).catch((error) => {
                globalFunctions.showMessage(error.NotificationType, error.Message)
            });
        },
        loadCourse = function () {
            var grid = new DynamicPagination("CourseGrid");
            var settings = {
                url: '/Course/Course/LoadCourse?curriculumId=' + $('#CurriculumId').val()
            };
            grid.init(settings);
        }
    return {
        init,
        loadCourseGrid,
        initCourseGrid,
        loadAddEditCourseView,
        loadUnitCalenarView,
        loadCurriculumMapView,
        loadCourse
    };
}();
