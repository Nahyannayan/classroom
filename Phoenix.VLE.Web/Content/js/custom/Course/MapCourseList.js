﻿var mapCourseList = function () {
    var init = function () {
        loadMapCourseGrid();
    },
        loadMapCourseGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "CourseTitle", "sTitle": translatedResources.courseTitle, "sWidth": "20%" },
                { "mData": "SchoolGroupName", "sTitle": translatedResources.schoolGroupName, "sWidth": "20%" },
                { "mData": "CourseType", "sTitle": translatedResources.courseType, "sWidth": "10%" },
                { "mData": "CreatedOn", "sTitle": translatedResources.createdOn, "sWidth": "13%" },
                { "mData": "CreatedByName", "sTitle": translatedResources.createdBy, "sWidth": "16%" },
                { "mData": "TeacherName", "sTitle": translatedResources.teacher, "sWidth": "16%" }
            );
            if (isCustomPermissionEnabled.toBoolean()) {
                _columnData.push({ "mData": "Actions", "sClass": "text-center no-sort", "sortable": false, "sTitle": translatedResources.actions, "sWidth": "5%" });
            }
            initMapCourseGrid('tbl-mapcourse', _columnData, '/course/course/LoadCourseMappingList?curriculumId=' + $('#CurriculumId').val());
        },

        initMapCourseGrid = function (_tableId, _columnData, _url) {

            //var grid = new DynamicGrid(_tableId);
            //var settings = {
            //    columnData: _columnData,
            //    url: _url,
            //    columnSelector: false,
            //    columnDefs: [{
            //        'orderable': false,
            //        'targets': [1, 2]
            //    }],
            //    searching: true
            //};
            //grid.init(settings);

            //$("#tbl-mapcourse_filter").hide();
            //$('#tbl-mapcourse').DataTable().search('').draw();
            //$("#courseMapListSearch").on("input", function (e) {
            //    e.preventDefault();
            //    $('#tbl-mapcourse').DataTable().search($(this).val()).draw();
            //});
        },
        loadSchoolGroups = function (group) {
            var grid = new DynamicPagination("tbl-mapcourse");
            var settings = {
                url: '/Course/Course/LoadCourseMappingList?Id=' + group
            };
            grid.init(settings);
        }
    return {
        init,
        loadMapCourseGrid,
        initMapCourseGrid,
        loadSchoolGroups
    };
}();