﻿var courseCatalogue = function () {
    var init = function () {
        $('#courseList li.courseTitle:visible:first').click()
    },
        searchCourses = function () {
            var searchText = $('#courseSearch').val().toLowerCase();
            $('#courseList li.courseTitle').each(function () {
                var showCurrentLi;

                showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;

                $(this).toggle(showCurrentLi);
            });
            $('#courseList li.courseTitle:visible:first').click()
        },
        showMainScreen = function () {
            $('#courseInformation').attr("style", "display: flex!important");
            $('#courseGroupInformation').hide();
            $('a[onclick="courseCatalogue.showMainScreen()"]').attr('onclick', "globalFunctions.back()");
            $('.breadcrumbs li:last').remove();
            $('.breadcrumbs li:last a').removeAttr('onclick');
        },
        getUnits = function (courseId, element) {
            $.get('/course/Catalogue/GetUnitDetails?courseId=' + courseId)
                .then(response => {
                    element.html(response);
                    $(".subject-grp-wrapper .subject-grp .subject-subwrapper").mCustomScrollbar();
                });
        },
        getCourseGroupInfo = function (group) {
            $.get('/course/Catalogue/GroupInfo?id=' + group)
                .then(response => {
                    $('#courseGroupInformation').html(response);
                    $('.breadcrumbs li:last')
                        .html('<a onclick="courseCatalogue.showMainScreen()" href="javascript: void(0)">' + $('.breadcrumbs li:last').text() + '</a>');
                    $('.breadcrumbs li:last').after(`<li class="active">${translatedResources.groupName} - ${$('#schoolGroupName').val()} </li>`);

                    $('#courseInformation').attr("style", "display: none !important");
                    $('#courseGroupInformation').show();
                    $('a[onclick="globalFunctions.back()"]').attr('onclick', "courseCatalogue.showMainScreen()");
                });
        }
    return {
        init,
        searchCourses,
        showMainScreen,
        getUnits,
        getCourseGroupInfo
    }
}();

$(document).on('keyup', '#courseSearch', function (e) {
    if (e.keyCode == 13)
        courseCatalogue.searchCourses();
}).on('click', '.courseTitle', function () {
    var element = $(this).attr("rel");
    $(`li[rel="${element}"] ul`)[$(`li[rel="${element}"] ul:visible`).length > 0 ? "slideUp" : "slideDown"](500);
})
$(document).ready(function () {
    $(".sidebar-fixed .list-group").mCustomScrollbar({
        setHeight: "86%",
        autoExpandScrollbar: true,
        autoHideScrollbar: true,
        scrollbarPosition: "outside"
    });
    $(".subject-name-list").mCustomScrollbar({
        autoExpandScrollbar: true,
        autoHideScrollbar: true,
    })
    $('.courseTitle').click(function () {
        $("#courseList").mCustomScrollbar("scrollTo", $(this));
        var courseId = $(this).data("course");
        var unitsDiv = $(`#catalouge-${courseId}`);
        var html = unitsDiv.html();
        if (html.length <= 0) {
            courseCatalogue.getUnits(courseId, unitsDiv);
        }
    })
    //----------Select the first tab and div by default

    //toggleable right div scrollbar
    $(".subject-grp-wrapper .subject-grp .subject-subwrapper").mCustomScrollbar({
    });

    courseCatalogue.init();
}).on('click', '.right-drawer-link', function () {
    $("#" + $(this).data('target') + "_").toggleClass("show");

    $("#" + $(this).data('target')).toggleClass("show-icon");

    if (!$(`#catalouge${$(this).data('unitid')}`).hasClass('show'))
        $(`button[data-target="#catalouge${$(this).data('unitid')}"]`).click();;
}).on('click', ".subject-grp-icon", function () {
    $("#" + this.id + "_").removeClass("show");
    $("#" + this.id).toggleClass("show-icon");
});