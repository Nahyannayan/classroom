﻿var ClassListObject = function () {
    var Init = function () {
        // smsCommon.init();
    },
        StudentOnReportMasterArray = [],
        StudentOnReportDetailArray = [],
        LoadStudentOnReport = (studentId) => {
            $.get('/ClassList/ClassList/LoadStudentOnReportForm', { studentId: studentId })
                .then(x => {
                    $('#studentOnReportDiv').html(x);
                    loadSideDrawer('studentOnReportDiv', '45%');
                    $('#StudentId').val(studentId);
                    $('#IsActive')[0].checked = true;
                    var ttm_id = smsCommon.GetQueryStringValue('strtt_id');
                    if (globalFunctions.isValueValid(ttm_id)) {
                        $('#GroupId').val(ttm_id);
                    } else {
                        $('#GradeId').val($('#Grades').val());
                        $('#SectionId').val($('#Section').val());
                    }
                })
        },
        OnStudentOnReportSaveSuccess = (e) => {
            if (e.NotificationType) {
                globalFunctions.showMessage(e.NotificationType, e.Message);
            } else {
                $('#studentOnReportFormGrid').html(e);
                $('#studentOnReportForm')[0].reset();
                $('#Id').val('');
                $('#IsActive')[0].checked = true;
                $('#radioButtons').show();
                var ttm_id = smsCommon.GetQueryStringValue('strtt_id');
                if (globalFunctions.isValueValid(ttm_id)) {
                    $.get("/ClassList/ClassList/GetStudentCard", { tt_id: ttm_id })
                        .then(response => {
                            $('#dvClassList').html('');
                            $('#dvClassList').html(response);
                            $('[data-toggle="tooltip"]').tooltip();
                        });
                } else {
                    $('#Section').trigger('change');
                }
                setTimeout(() => $('.onreport').bootstrapToggle('destroy').bootstrapToggle(), 500);
            }
        },
        EditStudentOnReport = (id) => {
            if (ClassListObject.StudentOnReportMasterArray.some(x => x.Id == id)) {
                var object = ClassListObject.StudentOnReportMasterArray.find(x => x.Id == id)
                $('#Id').val(id);
                $('#onReportFromDate').datetimepicker('date', object.FromDate);
                $('#onReportToDate').datetimepicker('date', object.ToDate);
                $('#onReportToDesc').val(object.Description);
                $('#radioButtons').hide();
                $('#IsActive')[0].checked = object.IsActive;
            }
        },
        OnStudentOnReportDetailsSaveSuccess = (e) => {
            if (e.NotificationType) {
                globalFunctions.showMessage(e.NotificationType, e.Message);
            } else {
                $('#studentOnReportDetailsFormGrid').html(e);
                $('#studentOnReportDetailsForm')[0].reset();
                $('#onReportDetailId').val('');
            }
        },
        ViewStudentOnReportDetails = (id, studentId, element) => {
            studentId = studentId || $("#StudentId").val();
            $.get('/ClassList/ClassList/LoadStudentOnReportDetailsForm', { studentId: studentId, studentOnReportMasterId: id })
                .then(x => {
                    $('#studentOnReportDetailsFormDiv').html(x).show();
                    if (globalFunctions.isValueValid($("#StudentId").val())) {
                        $('#reportBackBtn').show();
                        $('#StudentOnReportForm').hide();
                        $('.formElement').hide()
                    }
                    $('#onReportDetailStudentId').val(studentId);
                    $('#StudentOnReportMasterId').val(id);

                    if (globalFunctions.isValueValid(element)) {
                        $('#PeriodNo').val($(element).data('periodno'))
                        loadSideDrawer('studentOnReportDetailsFormDiv', '40%');
                        if ($('#attendancedate').is(":visible")) {
                            $('#CreatedOn').val($('#attendancedate').val());
                        } else {
                            $('#CreatedOn').val($(element).data('entrydate'));
                        }
                    }
                });
        },
        showStudentOnReportMainForm = () => {
            if (globalFunctions.isValueValid($("#StudentId").val())) {
                $('#studentOnReportDetailsFormDiv').hide();
                $('#StudentOnReportForm').show();
            }
        },
        EditStudentOnReportDetails = (id) => {
            if (ClassListObject.StudentOnReportDetailArray.some(x => x.Id == id)) {
                var object = ClassListObject.StudentOnReportDetailArray.find(x => x.Id == id)
                $('#onReportDetailId').val(id);
                $('#onReportDetailToDesc').val(object.Description);
            }
        },
        GetStudentCard = function () {

            var GroupId = $('#ddlStudentGroupId').val() != '' && $('#ddlStudentGroupId').val() != undefined ? $('#ddlStudentGroupId').val() : 0;
            var GradeId = $('#ddlStudentGradeId').val() != '' && $('#ddlStudentGradeId').val() != undefined ? $('#ddlStudentGradeId').val() : 0;
            var SectionId = $('#ddlStudentSectionId').val() != '' && $('#ddlStudentSectionId').val() != undefined ? $('#ddlStudentSectionId').val() : 0;
            var IsAllDdlEnabled = $('#ddlStudentGroupId').val() != undefined && $('#ddlStudentGradeId').val() != undefined
                && $('#StudentSectionId').val() != undefined ? true : false;

            $.ajax({
                type: 'GET',
                url: "/ClassList/ClassList/GetStudentCard",
                data: { GroupId, GradeId, SectionId, IsAllDdlEnabled },
                success: function (response) {
                    $('#dvClassList').html('');
                    $('#dvClassList').html(response);
                    $('[data-toggle="tooltip"]').tooltip();
                },
                error: function (ex) {
                    globalFunctions.onFailure();
                }
            });
            //$.get("/ClassList/ClassList/GetStudentCard", { GroupId, GradeId, SectionId, IsAllDdlEnabled })
            //    .then(response => {
            //        $('#dvClassList').html('');
            //        $('#dvClassList').html(response);
            //        $('[data-toggle="tooltip"]').tooltip();
            //    });
        },
        ShowListView = function () {
            $("#listBehaviour").removeClass('grid-list-unclick');
            $("#listBehaviour").addClass('grid-list-click');
            $("#gridBehaviour").addClass('grid-list-unclick');
        },
        ShowGridView = function () {
            $("#gridBehaviour").removeClass('grid-list-unclick');
            $("#gridBehaviour").addClass('grid-list-click');
            $("#listBehaviour").addClass('grid-list-unclick');
        },
        GetCourseWiseSchoolGroups = function (StudentId) {
            $.get("/ClassList/ClassList/CourseWiseSchoolGroup", { StudentId })
                .then(response => {
                    $('#divStudentGroupChange').html('');
                    $('#divStudentGroupChange').html(response);
                    $('.selectpicker').selectpicker('refresh');
                });
        },
        onSaveSuccess = function (response) {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success) {
                $(".closeRightDrawer").trigger('click');
                $('#divStudentGroupChange').html('');
            }
        },
        onFailure = function () {
            globalFunctions.onFailure();
        },
        GetWeeklyTimeTableDetail = function (StudentId, WeekCount) {
            $.get("/ClassList/ClassList/GetWeeklyTimeTableDetail", { StudentId, WeekCount })
                .then(response => {
                    $('#divStudentTimeTableDashboard').html('');
                    $('#divStudentTimeTableDashboard').html(response);
                    $('.selectpicker').selectpicker('refresh');
                });
        },
        GetQuickContactDetail = function (StudentId) {
            $.get("/ClassList/ClassList/GetQuickContactDetail", { StudentId })
                .then(response => {
                    $("#modelQuickContact").modal("show");
                    $("#QuickContactTitle").html('Contact Information');
                    $('#divQuickContact').html('');
                    $('#divQuickContact').html(response);
                    $('.selectpicker').selectpicker('refresh');
                });
        },
        GetStudentFindMeDetail = function (StudentId) {
            $.get("/ClassList/ClassList/GetStudentFindMeDetail", { StudentId })
                .then(response => {
                    $("#modelQuickContact").modal("show");
                    $("#QuickContactTitle").html('Find Me - Details');
                    $('#divQuickContact').html('');
                    $('#divQuickContact').html(response);
                    $('.selectpicker').selectpicker('refresh');
                });
        },
        GetStudentListBySearch = function (SearchString = '') {
            if ($.trim(SearchString) != '') {
                $.get("/ClassList/ClassList/StudentDetailBySearch", { SearchString })
                    .then(response => {
                        $('#divSearchStudent').html('');
                        $('#divSearchStudent').html(response);
                        $("#divSearchStudent ul>li").highlight(SearchString.trim());
                        $('.searchResult').removeClass('d-none');
                    });
            }
        }
    return {
        Init,
        StudentOnReportMasterArray,
        StudentOnReportDetailArray,
        LoadStudentOnReport,
        OnStudentOnReportSaveSuccess,
        EditStudentOnReport,
        OnStudentOnReportDetailsSaveSuccess,
        ViewStudentOnReportDetails,
        showStudentOnReportMainForm,
        EditStudentOnReportDetails,
        GetStudentCard,
        ShowListView,
        ShowGridView,
        GetCourseWiseSchoolGroups,
        onSaveSuccess,
        onFailure,
        GetWeeklyTimeTableDetail,
        GetQuickContactDetail,
        GetStudentFindMeDetail,
        GetStudentListBySearch
    }
}();

$(document).on('click', 'input[name="WeekMonthRange"]', function () {
    $('#onReportFromDate,#onReportToDate').val('');
    if (['1', '2'].some(x => x == $('input[name="WeekMonthRange"]:checked').val())) {
        $('#onReportFromDate').datetimepicker('date', new Date())
        if ($('input[name="WeekMonthRange"]:checked').val() == '1') {
            $('#onReportToDate').datetimepicker('date', new Date(new Date().setDate(new Date().getDate() + 7)))
        } else if ($('input[name="WeekMonthRange"]:checked').val() == '2') {
            $('#onReportToDate').datetimepicker('date', new Date(new Date().setMonth(new Date().getMonth() + 1)))
        }
    }
})

$(document).on('change', '#ddlStudentGroupId', function () {
    $("#searchClassList").val('');
    ClassListObject.GetStudentCard();
});
$(document).on('change', '#ddlStudentGradeId', function () {
    var StudentGradeId = $(this).val() != '' && $(this).val() != undefined ? $(this).val() : 0;
    if (StudentGradeId > 0) {
        var result = common.bindSingleDropDownByParameter(
            "#ddlStudentSectionId",
            '/ClassList/ClassList/FetchSectionListByGrade',
            { GradeId: $(this).val() },
            "",
            'Select'
        );

        if ($("#ddlStudentSectionId option").length > 1) {
            $("#ddlStudentSectionId option")[1].selected = true;
            $("#ddlStudentSectionId").selectpicker('refresh');
        }
    }
    $("#ddlStudentSectionId").trigger('change');
});
$(document).on('change', '#ddlStudentSectionId', function () {
    $("#searchClassList").val('');
    ClassListObject.GetStudentCard();
});

$(document).on('click', '#btnChangeGroup', function () {
    $("#frmChangeGroup").submit();
});

$(document).on('click', '#btnPrevious', function () {
    var WeekCount = parseInt($("#hdnWeekCount").val()) - 1;
    var StudentNo = $("#StudentNo").val();
    $("#hdnWeekCount").val(WeekCount);
    ClassListObject.GetWeeklyTimeTableDetail(StudentNo, WeekCount);
});
$(document).on('click', '#btnNext', function () {
    var WeekCount = parseInt($("#hdnWeekCount").val()) + 1;
    var StudentNo = $("#StudentNo").val();
    $("#hdnWeekCount").val(WeekCount);
    ClassListObject.GetWeeklyTimeTableDetail(StudentNo, WeekCount);
});

var _changeInterval = null;
$(document).on('keyup', '#searchClassList', function () {
    clearInterval(_changeInterval);
    _changeInterval = setInterval(function () {
        var SearchString = $("#searchClassList").val() != undefined ? $("#searchClassList").val() : '';
        if ($.trim(SearchString) != '') {
            ClassListObject.GetStudentListBySearch(SearchString);
        }
        else {
            $('.searchResult').addClass('d-none');
        }
        clearInterval(_changeInterval);
    }, 1000);
})

$(document).on('change', '#toggler', function () {
    // on change of state
    var AttType = $("#toggler").is(":checked") ? "" : "M";
    GetAttendanceProfileDetail($("#dueDatePicker").val(), AttType);
})