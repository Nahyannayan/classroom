﻿
var classListIndex = function () {
    init = function () {

        //Multiple Images Selection 
        $(".image-checkbox").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-checked');
            }
            else {
                $(this).removeClass('image-checkbox-checked');
            }
        });

        // sync the state to the input
        $(".image-checkbox").on("click", function (e) {
            $(this).toggleClass('image-checkbox-checked');
            var $checkbox = $(this).find('input[type="checkbox"]');
            $checkbox.prop("checked", !$checkbox.prop("checked"))

            e.preventDefault();
        });

        //Extending the :contains selector property to be case in-sensitive for the card search option. 
        $.extend($.expr[":"], {
            "containsIN": function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });
        //Search option for students cards
        $('#searchClassList_Old').keyup(function () {

            $('.clsstudetailsblock').removeClass('d-none');
            var filter = $(this).val(); // get the value of the input, which we filter on
            $('#divClasslist').find('.clsstudetails:not(:containsIN("' + filter + '"))').parent().parent().parent().parent().addClass('d-none');
            if ($('#divClasslist').find('.clsstudetails:not(:containsIN("' + filter + '"))').parent().parent().parent().parent().length == $('#hdnTotalCount').val()) {
                $('#span-stud-not-found').removeClass('d-none');
            }
            else {
                $('#span-stud-not-found').addClass('d-none');
            }
            if ($('#divClasslist').find('.clsstudetails').length == 0) {
                $('#span-stud-not-found').addClass('d-none');
            }
        })

        // Ellipsis for for student name 

        $(document).on('mouseenter', ".tooltip-name", function () {
            var $this = $(this);
            if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
                $this.tooltip({
                    title: $this.text(),
                    placement: "top"
                });
                $this.tooltip('show');
            }
        });
        $('.hide-name').css('width', $('.hide-name').parent().width());

        $(document).on('click', '#h3StuDetails,#btnPrevStudent,#btnNextStudent,#divStuDetails', function () {
            var id = $(this).attr('data-stuId');
            var IsOpenBySearch = $(this).attr('data-isopenbysearch');
            classListIndex.LoadClassListTab(id, IsOpenBySearch)
        });

        $(document).on('click', '#zoomBtn', function () {
            $('.zoom-btn-sm').toggleClass('scale-out');
            if (!$('.zoom-card').hasClass('scale-out')) {
                $('.zoom-card').toggleClass('scale-out');
            }
        });

        $(document).on('click', '.zoom-btn-sm', function () {
            var btn = $(this);
            var card = $('.zoom-card');

            if ($('.zoom-card').hasClass('scale-out')) {
                $('.zoom-card').toggleClass('scale-out');
            }
            if (btn.hasClass('zoom-btn-person')) {
                card.css('background-color', '#d32f2f');
            } else if (btn.hasClass('zoom-btn-doc')) {
                card.css('background-color', '#fbc02d');
            } else if (btn.hasClass('zoom-btn-tangram')) {
                card.css('background-color', '#388e3c');
            } else if (btn.hasClass('zoom-btn-report')) {
                card.css('background-color', '#1976d2');
            } else {
                card.css('background-color', '#7b1fa2');
            }
        });


        $(document).on('change', '#chkWeeklyMonthlyToggler,.date-picker', function () {
            classListIndex.ResetAllSections();

        });



    },

        ajax_call = function (_ttid, _grade, _section) {
            var url = "/ClassList/ClassList/GetStudentCard";
            $.ajax({
                type: 'GET',
                url: url,
                data: { tt_id: _ttid, grade: _grade, section: _section },
                success: function (result) {
                    $('#dvClassList').html('');
                    $('#dvClassList').html(result);
                    $('[data-toggle="tooltip"]').tooltip();
                },
                error: function (ex) {
                    globalFunctions.onFailure();
                }
            });

            return false;

        },
        LoadClassListTab = function (stu_id, IsOpenBySearch = false, IsOnNewTab = false) {
            var url = !IsOnNewTab ? "/ClassList/ClassList/GetStudentDetails" : "/ClassList/ClassList/GetStudentSiblingDetail";
            $.get(url, { stu_id: stu_id, IsOpenBySearch: IsOpenBySearch }).then((response) => {
                $('#divClasslistSearch').addClass('d-none');
                $('#dvClassList').addClass('d-none');
                $('#dvClassListSearch').addClass('d-none');
                $('#divClassSectionTab').removeClass('d-none');

                $('#divClassSectionTab').html("");
                if ($('#toggler')[0] != undefined) {

                }
                $(".student-parent-info .accordion .card-body .content").mCustomScrollbar('destroy');
                $("#sibling-details .card-body .content").mCustomScrollbar('destroy');
                $("#AssessmentDetailstable").mCustomScrollbar('destroy');

                $('#divClassSectionTab').html(response);
                //$('#chkWeeklyMonthlyToggler').bootstrapToggle('destroy')
                //$('#chkWeeklyMonthlyToggler').bootstrapToggle()

                $(".student-parent-info .accordion .card-body .content").mCustomScrollbar({
                    setHeight: "167",
                    autoExpandScrollbar: true,
                    scrollbarPosition: "inside",
                    autoHideScrollbar: true,
                });
                $("#sibling-details .card-body .content").mCustomScrollbar({
                    setHeight: "110",
                    autoExpandScrollbar: true,
                    scrollbarPosition: "outside",
                    autoHideScrollbar: true
                });
                $("#AssessmentDetailstable").mCustomScrollbar({
                    setHeight: "285",
                    autoExpandScrollbar: true,
                    scrollbarPosition: "inside",
                    autoHideScrollbar: true
                });
                //$('[data-toggle="tooltip"]').tooltip();

                $('.date-picker').datetimepicker({
                    format: 'DD-MMM-YYYY',
                    maxDate: new Date()
                    //debug: true
                }).on('dp.change', function (e) {
                    classListIndex.ResetAllSections();

                });

                $(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');


            }).catch(error => globalFunctions.showMessage(error.NotificationType, error.Message));

        }

    ResetAllSections = function () {
        var selectedDate = $("#dueDatePicker").val();
        var hdnSTU_ID = $("#hdnSTU_ID").val();

        var url = "";
        if ($("#chkWeeklyMonthlyToggler").prop("checked") == false) {
            url = "/Attendance/Attendance/GetAttendenceListMonthly";
        } else {
            url = "/Attendance/Attendance/GetAttendenceListWeekly";
        }



        $.ajax({
            type: 'GET',
            url: url,
            data: { selectedDate: selectedDate, STU_ID: hdnSTU_ID },
            success: function (response) {
                $('#divAttList').empty();
                $('#divAttList').html(response);
            },
            error: function (ex) {
                globalFunctions.onFailure();
            }
        });


        var url_1 = "";
        if ($("#chkWeeklyMonthlyToggler").prop("checked") == false) {
            url_1 = "/Attendance/Attendance/GetAttendenceChartMonthly";
        } else {
            url_1 = "/Attendance/Attendance/GetAttendenceBySession";
        }

        //Load Session divAttBySession
        $.ajax({
            type: 'GET',
            url: url_1,
            data: { selectedDate: selectedDate, STU_ID: hdnSTU_ID },
            success: function (response) {
                $('#divAttBySession').empty();
                $('#divAttBySession').html(response);
            },
            error: function (ex) {
                globalFunctions.onFailure();
            }
        });


        //Load Session
        $.ajax({
            type: 'GET',
            url: "/Attendance/Attendance/GetAttendenceSessionCode",
            data: { selectedDate: selectedDate, STU_ID: hdnSTU_ID },
            success: function (response) {
                $('#divAttBySessionCode').empty();
                $('#divAttBySessionCode').html(response);
            },
            error: function (ex) {
                globalFunctions.onFailure();
            }
        });


    },
        BacktoClassList = function () {
            $('#divClasslistSearch').removeClass('d-none');
            $('#dvClassList').removeClass('d-none');
            $('#dvClassListSearch').removeClass('d-none');
            $('#divClassSectionTab').addClass('d-none');
            var newUrl = window.location.href.replace(window.location.search, '');
            history.replaceState(null, null, newUrl);
            $('a[onclick="classListIndex.BacktoClassList()"]').attr('onclick', "globalFunctions.back()");
        }
    return {
        ajax_call: ajax_call,
        init: init,
        LoadClassListTab: LoadClassListTab,
        ResetAllSections: ResetAllSections,
        BacktoClassList: BacktoClassList
    }

}();

$(document).ready(function () {

    classListIndex.init();

});
