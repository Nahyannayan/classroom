﻿var sessionGradeList = "";
var DivisionConfiguration = function () {
    var Init = function () {
        common.init();
    },
        DivisionConfigurationArray = [],

        EditGrade = function (id) {
            var object = DivisionConfiguration.DivisionConfigurationArray.find(x => x.DivisionId == id);
            $("#DivisionId").val(id);
            sessionGradeList = object.GradeList;
            if (typeof (object.GradeList) != undefined && object.GradeList) {
                var dataarray = object.GradeList.replace(/^[,\s]+|[,\s]+$/g, '').split(/[\s,]+/);
                $("#ddlRuleDeletionGradeId").val(dataarray);
            }
            else $("#ddlRuleDeletionGradeId").val("");

            $("#DivisionName").val(object.DivisionName);
            $('#DivisionName').next('label').addClass("active");
            $('.selectpicker').selectpicker('refresh');

            var dropdown = $("#ddlRuleDeletionGradeId")[0].options;
            var ConfigurationArray = DivisionConfiguration.DivisionConfigurationArray;
            DivisionConfiguration.GetDisabledGrade();
            for (Configuration = 0; Configuration < ConfigurationArray.length; Configuration++) {
                if (typeof (object.GradeList) != undefined && object.GradeList) {
                    var dataarray = object.GradeList.replace(/^[,\s]+|[,\s]+$/g, '').split(/[\s,]+/);
                    for (array1 = 0; array1 < dataarray.length; array1++) {
                        for (i = 0; i < dropdown.length; i++) {
                            if (dataarray[array1] == $("#ddlRuleDeletionGradeId")[0].options[i].value) {
                                $("#ddlRuleDeletionGradeId")[0].options[i].disabled = false;
                                $('.selectpicker').selectpicker('refresh');
                            }
                        }
                    }
                }
            }
            $("#DivisionName").focus();
        },
        SaveGradeContent = () => {
            var Id = $("#DivisionId").val();

            $('#frmAddEditDivision').removeData("validator").removeData("unobtrusiveValidation");//remove the form validation
            $.validator.unobtrusive.parse($('#frmAddEditDivision'));//add the form validatio

            if (!$('#frmAddEditDivision').valid()) {
                return false;
            }

            if ($.trim($("#DivisionName").val()) == "") {
                globalFunctions.showWarningMessage("Please enter division name");
                $("#DivisionName").focus();
                return false;
            }
            else if ($('#ddlRuleDeletionGradeId').val() == "") {
                globalFunctions.showWarningMessage("Please select Grade(s)");
                $("#ddlRuleDeletionGradeId").focus();
                return false;
            }
            var object = {
                DivisionName: $("#DivisionName").val(),
                GradeList: $('#ddlRuleDeletionGradeId').val().join(','),
                DivisionId: $("#DivisionId").val(),
                DataMode: Id == "" || Id =="0" ? 'Add' : 'Edit',
                CurriculumId: 1
            }


            $.post('/SchoolInfo/Division/SaveGradeDetails', object)
                .then(response => {
                    
                    if (response.Success == true) {
                        if ($("#DivisionId").val() == "0" || $("#DivisionId").val() == "") {
                            object.DivisionId = response.InsertedRowId;
                        }
                        else {
                            DivisionConfiguration.DivisionConfigurationArray.find(x => x.DivisionId == Number(Id)).GradeList = object.GradeList;
                        }
                    }
                    $.post("/SchoolInfo/Division/GetDivisionDetails", function (result) {
                        $("#showTable").html(result);
                        if ($("#DivisionId").val() == "0" || $("#DivisionId").val() =="") {
                            DivisionConfiguration.DivisionConfigurationArray.push(object);
                        }
                        DivisionConfiguration.GetDisabledGrade();
                    });
                    if (response.Success == true) {
                        if ($("#DivisionId").val() == "0" || $("#DivisionId").val() == "") {
                            globalFunctions.showSuccessMessage(translatedResources.SuccessMessage);
                        }
                        else
                            globalFunctions.showSuccessMessage(translatedResources.UpdateMessage);

                    }
                    
                    $("#DivisionName").val('');
                    $("#ddlRuleDeletionGradeId").val('');
                    $('label[for="DivisionName"]').addClass('active');
                    $("#DivisionId").val('');
                    $('.selectpicker').selectpicker('refresh');
                })
                .fail(error => globalFunctions.showSuccessMessage(error.NotificationType, error.Message));
        },
        ClearGradeContent = () => {
            $("#DivisionName").val('');
            $("#ddlRuleDeletionGradeId").val('');
            $('.selectpicker').selectpicker('refresh');
            $("#DivisionId").val('');
            DivisionConfiguration.GetDisabledGrade();

        },
        GetDisabledGrade = () => {
            var dropdown = $("#ddlRuleDeletionGradeId")[0].options;
            var ConfigurationArray = DivisionConfiguration.DivisionConfigurationArray;
          
            for (Configuration = 0; Configuration < ConfigurationArray.length; Configuration++) {
                if (typeof (ConfigurationArray[Configuration].GradeList) != undefined && ConfigurationArray[Configuration].GradeList) {
                    var dataarray = ConfigurationArray[Configuration].GradeList.replace(/^[,\s]+|[,\s]+$/g, '').split(/[\s,]+/);
                    for (array = 0; array < dataarray.length; array++) {
                        for (i = 0; i < dropdown.length; i++) {
                            if (dataarray[array] == $("#ddlRuleDeletionGradeId")[0].options[i].value) {
                                $("#ddlRuleDeletionGradeId")[0].options[i].disabled = true;
                                $('.selectpicker').selectpicker('refresh');
                            }
                        }
                    }
                }
            }
        }


    return {
        Init,
        DivisionConfigurationArray,
        EditGrade: EditGrade,
        SaveGradeContent,
        ClearGradeContent,
        GetDisabledGrade
    }
}();
$(document).ready(function () {

    DivisionConfiguration.Init();
    DivisionConfiguration.GetDisabledGrade();
});
