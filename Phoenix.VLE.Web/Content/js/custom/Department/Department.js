﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();

var objDepartment = function () {
    var init = function () {
        objDepartment.GetDepartmentDetail();
    },
        addDepartmentPopup = function (source) {
            var curriculumId = $('#CurriculumId').val();
            var title = translatedResources.add + ' ' + translatedResources.DepartmentTitle;
            globalFunctions.loadPopup(source, '/SchoolInfo/Department/InitAddEditDepartmentForm?id=0' + '&curriculumId=' + curriculumId, title, 'gradingtemplate-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
                $('#hdnCurriculumId').val($('#CurriculumId').val())
                $('.selectpicker').selectpicker('refresh');
            });
        },
        editDepartmentPopup = function (source, id) {
            var curriculumId = $('#CurriculumId').val();
            var title = translatedResources.edit + ' ' + translatedResources.DepartmentTitle;
            globalFunctions.loadPopup(source, '/SchoolInfo/Department/InitAddEditDepartmentForm?id=' + id + '&curriculumId=' + curriculumId, title, 'gradingtemplate-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
            });
        },
        deleteDepartment = function (source, id) {
            debugger
            $.post("/SchoolInfo/Department/CanDeleteDepartment", { departmentId: id }, function (response) {
                if (response) {
                    globalFunctions.notyDeleteConfirm($(source), translatedResources.deleteConfirm);
                    $(document).bind('okToDelete', $(source), function (e) {
                        $.post("/SchoolInfo/Department/DeleteDepartment", { DepartmentId: id }, function (response) {
                            if (response.Success) {
                                objDepartment.GetDepartmentDetail();
                                //var table = $('#tbl-Department').DataTable();
                                //table.row($(source).parent('tr')).remove().draw();
                            }
                            globalFunctions.showMessage(response.NotificationType, response.Message);
                        });
                    });
                }
                else {
                    globalFunctions.showWarningMessage("Cannot be deleted, Department linked to Course.");
                }
            });
        },
        GetDepartmentDetail = function () {
            var curriculumId = $('#CurriculumId').val();
            $.post('/SchoolInfo/Department/GetDepartmentDetail', { curriculumId: curriculumId })
                .then(response => {
                    $('#divDepartmentGrid').html('');
                    $('#divDepartmentGrid').html(response);
                    $('#tblDepartmentGrid').DataTable({
                        "bLengthChange": false,
                        //"bFilter": false,
                        //"bInfo": false,
                        "pageLength": 5,
                        //"ordering": false,
                        //"paging": false,
                        "autoWidth": false,
                        "columnDefs": [
                            { "width": "70%", "targets": 0 },
                            { "width": "30%", "targets": 1, "orderable": false },
                        ]
                    });
                    $("#tblDepartmentGrid_filter").hide();
                    $('#tblDepartmentGrid').DataTable().search('').draw();
                })
        },
        onSaveSuccess = function (response) {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },
        onFailure = function () {
            globalFunctions.onFailure();
        }
    return {
        init,
        addDepartmentPopup,
        editDepartmentPopup,
        deleteDepartment,
        GetDepartmentDetail,
        onSaveSuccess,
        onFailure
    }
}();

$(document).on('click', '#btnAddDepartment', function () {
    objDepartment.addDepartmentPopup($(this));
});
$(document).on('click', '#btnAddUpdateDepartment', function () {
    var IsValid = common.IsInvalidByFormOrDivId('frmAddDepartment');
    if (IsValid) {
        $("#frmAddDepartment").submit();
    }
});
$(document).ready(function () {
    objDepartment.init();
});
$(document).on("input", "#inputDepartmentSearch", function (e) {
    e.preventDefault();
    $('#tblDepartmentGrid').DataTable().search($(this).val()).draw();
});
