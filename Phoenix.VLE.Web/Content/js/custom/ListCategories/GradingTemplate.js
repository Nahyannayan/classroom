﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();

var gradingTemplates = function () {

    var init = function () {
        loadGradingTemplateGrid();
    },

        loadGradingTemplatePopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.gradingTemplate;
            globalFunctions.loadPopup(source, '/ListCategories/GradingTemplate/InitAddEditGradingTemplateForm?id=' + id, title, 'gradingtemplate-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
            });
        },

        addGradingTemplatePopup = function (source) {
            loadGradingTemplatePopup(source, translatedResources.add);
        },

        openGradingTemplateItem = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                $.post('/Shared/Shared/SetSession', { key: "GradingTemplateId", value: id }, function (data) {
                    if (typeof id != 'undefined' && data == true)
                        window.open('/ListCategories/GradingTemplateItem','_self');
                    else {
                        globalFunctions.onFailure();
                    }
                });
            }
        },

        editGradingTemplatePopup = function (source, id) {
            loadGradingTemplatePopup(source, translatedResources.edit, id);
        },

        deleteGradingTemplateData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('GradingTemplate', '/ListCategories/GradingTemplate/DeleteGradingTemplateData', 'tbl-GradingTemplate', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        loadGradingTemplateGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "GradingTemplateTitle", "sTitle": translatedResources.templateName, "sWidth": "20%" },
                { "mData": "GradingTemplateDesc", "sTitle": translatedResources.templateDescription, "sWidth": "40%" },
                { "mData": "Logo", "sClass": "text-center no-sort", "sTitle": translatedResources.logo, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initGradingTemplateGrid('tbl-GradingTemplate', _columnData, '/ListCategories/GradingTemplate/LoadGradingTemplateGrid');

            $("#tbl-GradingTemplate_filter").hide();
            $("#GradingTemplateListSearch").on("input", function (e) {
                e.preventDefault();
                $('#tbl-GradingTemplate').DataTable().search($(this).val()).draw();
            });
        },
        validationGradingTemplateImage = function (e) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'];
            var size = e.files[0].size;
            if ($.inArray($('input[type=file]').val().split('.').pop().toLowerCase(), fileExtension) == -1 || size > 5000) {
                globalFunctions.showErrorMessage(translatedResources.UploadFileMessage);
                $('input[type=file]').val('');
            }
        },
        submitGradingTemplateForm = function () {
            var formData = new FormData();
            var file = document.getElementById("GradingImage").files[0];
            var token = $('input[name=__RequestVerificationToken]').val();
            formData.append("__RequestVerificationToken", token);
            formData.append("GradingImage", file);
            formData.append("GradingTemplateTitle", $('#GradingTemplateTitle').val());
            formData.append("GradingTemplateDesc", $('#GradingTemplateDesc').val());
            formData.append("AllowAdditionalGrade", $('#AllowAdditionalGrade').prop('checked'));
            formData.append("IsActive", $('#IsActive').prop('checked'));
            formData.append("GradingTemplateId", $('#GradingTemplateId').val());
            formData.append("GradingTemplateXml", $('#GradingTemplateXml').val());
            formData.append("SchoolId", $('#SchoolId').val());
            formData.append("IsAddMode", $('#IsAddMode').val());
            $.ajax({
                type: 'POST',
                url: '/ListCategories/GradingTemplate/SaveGradingTemplateData',
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    globalFunctions.showMessage(result.NotificationType, result.Message);
                    console.log(result.Success);
                    if (result.Success == true) {
                        $('#myModal').modal('hide');
                        gradingTemplates.init();
                    }

                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });
           // $("#myModal").modal('hide');
        },
        initGradingTemplateGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 2,
                    'orderable': false
                }]
            };
            grid.init(settings);
        };

    return {
        init: init,
        addGradingTemplatePopup: addGradingTemplatePopup,
        editGradingTemplatePopup: editGradingTemplatePopup,
        openGradingTemplateItem: openGradingTemplateItem,
        deleteGradingTemplateData: deleteGradingTemplateData,
        onSaveSuccess: onSaveSuccess,
        loadGradingTemplateGrid: loadGradingTemplateGrid,
        validationGradingTemplateImage: validationGradingTemplateImage,
        submitGradingTemplateForm: submitGradingTemplateForm
    };
}();

$(document).ready(function () {
    gradingTemplates.init();

    $(document).on('click', '#btnAddGradingTemplate', function () {
        gradingTemplates.addGradingTemplatePopup($(this));
    });

    $("#tbl-GradingTemplate_filter").hide();
    $('#tbl-GradingTemplate').DataTable().search('').draw();
    $("#GradingTemplateListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-GradingTemplate').DataTable().search($(this).val()).draw();
    });
    $(document).on('click', '#btnAddUpdateGradingTemplate', function () {
        gradingTemplates.submitGradingTemplateForm();
    });
});





