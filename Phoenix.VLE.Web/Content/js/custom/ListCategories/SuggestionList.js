﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();
var suggestionList = function () {

    var init = function () {
        loadSuggestionListGrid();
        $("#tbl-UserSuggestionList_filter").hide();
    },
    loadSuggestionListGrid = function () {
        var _columnData = [];
        _columnData.push(
            { "mData": "Title", "sTitle": translatedResources.title, "sWidth": "25%", "sClass": "wordbreak" },
            { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "25%", "sClass": "open-details-control wordbreak" },
            { "mData": "Type", "sTitle": translatedResources.type, "sWidth": "20%" },
            { "mData": "UserName", "sTitle": translatedResources.userName, "sWidth": "20%" },
            { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.downloadAttachment, "sWidth": "10%" }

        );
        initSuggestionCategoryGrid('tbl-UserSuggestionList', _columnData, '/ListCategories/SuggestionList/LoadSuggestionGridBySchool');
    },
    downloadAttachment = function (source,file) {
        window.open("/ListCategories/SuggestionList/DownloadAttachment?fileName=" + file, "_blank");
    },
    initSuggestionCategoryGrid = function (_tableId, _columnData, _url) {
        var grid = new DynamicGrid(_tableId);
        var settings = {
            columnData: _columnData,
            url: _url,
            columnSelector: false
        };
        grid.init(settings);
    },

    downloadSuggestionsData = function (source) {

        window.location.href = '/SuggestionBox/Suggestions/DownloadStudentSuggestionDetails';
    };

    return {
        init: init,
        downloadAttachment: downloadAttachment,
        loadSuggestionListGrid: loadSuggestionListGrid,
        downloadSuggestionsData: downloadSuggestionsData
    };
}();

$(document).ready(function () {
    suggestionList.init();
    $("#tbl-UserSuggestionList_filter").hide();
    $('#tbl-UserSuggestionList').DataTable().search('').draw();
    $("#UserSuggestionListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-UserSuggestionList').DataTable().search($(this).val()).draw();
    });

    $(document).on("DynamicGrid:InitComplete", "#tbl-UserSuggestionList", function () {
        if ($("#tbl-UserSuggestionList tbody tr td:first").hasClass('dataTables_empty')) {
            $("#exportSuggestions_admin").hide();
        }
    });

    $("#exportSuggestions_admin").on("click", function () {
        suggestionList.downloadSuggestionsData();
    });
});



