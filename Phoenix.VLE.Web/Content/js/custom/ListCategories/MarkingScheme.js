﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var popoverEnabler = new LanguageTextEditPopover();
var formElements = new FormElements();
var markingSchemes = function () {
    var init = function () {

        loaMarkingSchemeGrid();
        $("#tbl-MarkingScheme_filter").hide();
    },

        loadMarkingSchemePopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.markingScheme;
            globalFunctions.loadPopup(source, '/ListCategories/MarkingScheme/InitAddEditmarkingSchemeForm?id=' + id, title, 'subject-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
                formElements.feColorpicker();
            });
        },

        addMarkingSchemePopup = function (source) {
            loadMarkingSchemePopup(source, translatedResources.add);
        },

        editMarkingSchemePopup = function (source, id) {
            loadMarkingSchemePopup(source, translatedResources.edit, id);
        },

        deleteMarkingSchemePopup = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('MarkingScheme', '/ListCategories/MarkingScheme/DeleteMarkingSchemeData', 'tbl-MarkingScheme', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        loaMarkingSchemeGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "MarkingName", "sTitle": translatedResources.mark, "sWidth": "25%", "sClass": "wordbreak" },
                { "mData": "Description", "sTitle": translatedResources.meaning, "sWidth": "25%", "sClass": "wordbreak" },
                { "mData": "ColorCode", "sTitle": translatedResources.colorCode, "sWidth": "15%", "sClass": "open-details-control text-center" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "15%" }
            );
            initMarkingSchemeGrid('tbl-MarkingScheme', _columnData, '/ListCategories/MarkingScheme/LoadMarkingSchemeGrid');
        },
       
        initMarkingSchemeGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 3,
                    'orderable': false
                }],
            };
            grid.init(settings);
        };
    

    return {
        init: init,     
        loaMarkingSchemeGrid: loaMarkingSchemeGrid,
        addMarkingSchemePopup: addMarkingSchemePopup,
        onSaveSuccess: onSaveSuccess,
        editMarkingSchemePopup: editMarkingSchemePopup,
        deleteMarkingSchemePopup: deleteMarkingSchemePopup       
    };

}();


$(document).ready(function () {
    markingSchemes.init();

    $(document).on('click', '#btnAddMarkingScheme', function () {
        markingSchemes.addMarkingSchemePopup($(this));
    });
    $('#tbl-MarkingScheme').DataTable().search('').draw();
    $("#MarkListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-MarkingScheme').DataTable().search($(this).val()).draw();
    });
});