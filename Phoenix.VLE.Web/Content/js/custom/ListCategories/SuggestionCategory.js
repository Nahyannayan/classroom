﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();
var suggestionCategories = function () {

    var init = function () {
        loadSuggestionCategoryGrid();
        $("#tbl-SuggestionCategories_filter").hide();
    },

        loadCategoryPopup = function (source, mode, id) {           
            var title = mode + ' ' + translatedResources.suggestionCategory;
            globalFunctions.loadPopup(source, '/ListCategories/SuggestionCategory/InitAddEditSuggestionCategoryForm?id=' + id, title, 'suggestioncategory-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
                //$("#tbl-SuggestionCategories_filter").addClass("d-none");
                $("#tbl-SuggestionCategories_filter").hide();
                $('#tbl-SuggestionCategories').DataTable().search('').draw();
                $("#SuggestionCategoriesListSearch").on("input", function (e) {
                    e.preventDefault();
                    $('#tbl-SuggestionCategories').DataTable().search($(this).val()).draw();

                });
            });
        },

        addCategoryPopup = function (source) {
            loadCategoryPopup(source, translatedResources.add);
        },

        editCategoryPopup = function (source, id) {
            loadCategoryPopup(source, translatedResources.edit, id);
        },

        deleteSuggestionCategoryData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('SuggestionCategory', '/ListCategories/SuggestionCategory/DeleteSuggestionCategoryData', 'tbl-SuggestionCategories', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        loadSuggestionCategoryGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "SuggestionCategoryName", "sTitle": translatedResources.suggestionCategory, "sWidth": "50%", "sClass": "wordbreak" },
                { "mData": "FormattedCreatedOn", "sTitle": translatedResources.createdOn, "sWidth": "15%", "sClass": "open-details-control wordbreak" },
                { "mData": "CreatedByName", "sTitle": translatedResources.createdBy, "sWidth": "15%" },
                { "mData": "IsActive", "sTitle": translatedResources.active, "sWidth": "10%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "10%" }
            );
            initSuggestionCategoryGrid('tbl-SuggestionCategories', _columnData, '/ListCategories/SuggestionCategory/LoadSuggestionCategoriesGrid');
        },

        initSuggestionCategoryGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 4,
                    'orderable': false
                }]
            };
            grid.init(settings);
        };

    return {
        init: init,
        addCategoryPopup: addCategoryPopup,
        editCategoryPopup: editCategoryPopup,
        deleteCategoryData: deleteSuggestionCategoryData,
        onSaveSuccess: onSaveSuccess,
        loadSuggestionCategoryGrid: loadSuggestionCategoryGrid
    };
}();

$(document).ready(function () {
    suggestionCategories.init();


    $(document).on('click', '#btnAddSuggestionCategory', function () {
        suggestionCategories.addCategoryPopup($(this));
    });
    //$("#tbl-SuggestionCategories_filter").addClass("d-none");
    $("#tbl-SuggestionCategories_filter").hide();
    $('#tbl-SuggestionCategories').DataTable().search('').draw();
    $("#SuggestionCategoriesListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-SuggestionCategories').DataTable().search($(this).val()).draw();

    });
});



