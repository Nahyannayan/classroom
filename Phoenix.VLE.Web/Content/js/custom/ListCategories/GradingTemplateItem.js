﻿/// <reference path="../../common/global.js" />
var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var gradingTemplateItems = function () {

    var init = function () {
        loadGradingTemplateItemGrid();
    },

        loadGradingTemplateItemPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.gradingTemplateItem + ' - ' + $('#hdnGradingTemplateTitle').val();
            var parameters = "?id=" + id + "&templateId=" + $('#hdnGradingTemplateId').val();
            globalFunctions.loadPopup(source, '/ListCategories/GradingTemplateItem/InitAddEditGradingTemplateItemForm' + parameters, title, 'gradingtemplateItem-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                formElements.feColorpicker();
                popoverEnabler.attachPopover();
            });
        },

        addGradingTemplateItemPopup = function (source) {
            loadGradingTemplateItemPopup(source, translatedResources.add);
        },

        editGradingTemplateItemPopup = function (source, id) {
            loadGradingTemplateItemPopup(source, translatedResources.edit, id);
        },

        deleteGradingTemplateItemData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('GradingTemplateItem', '/ListCategories/GradingTemplateItem/DeleteGradingTemplateItemData', 'tbl-GradingTemplateItem', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },
        checkScoreFromTo = function () {
                if (Number($('#ScoreFrom').val()) > Number($('#ScoreTo').val())) {
                    globalFunctions.showWarningMessage("Score From must be less than Score To");
                    $('#ScoreFrom').focus();
                    return false;
            }
            if (Number($('#ScoreFrom').val()) > 100 || Number($('#ScoreTo').val() > 100)) {
                globalFunctions.showWarningMessage("Score From / Score To must be less than or equal to 100");
                return false;
            }
        },

        loadGradingTemplateItemGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "ShortLabel", "sTitle": translatedResources.shortLabel, "sWidth": "20%" },
                { "mData": "GradingItemDescription", "sTitle": translatedResources.itemDescription, "sWidth": "30%" },
                { "mData": "ScoreFrom", "sTitle": translatedResources.ScoreFrom, "sWidth": "15%" },
                { "mData": "ScoreTo", "sTitle": translatedResources.ScoreTo, "sWidth": "15%" },
                { "mData": "GradingColor", "sClass": "text-center",  "sTitle": translatedResources.gradingColor, "sWidth": "10%" },
                { "mData": "Percentage", "sClass": "text-center",  "sTitle": translatedResources.percentage, "sWidth": "10%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "10%" }
            );
            initGradingTemplateItemGrid('tbl-GradingTemplateItem', _columnData, '/ListCategories/GradingTemplateItem/LoadGradingTemplateItemGrid');

            $("#tbl-GradingTemplateItem_filter").hide();
            $("#GradingTemplateItemListSearch").on("input", function (e) {
                e.preventDefault();
                $('#tbl-GradingTemplateItem').DataTable().search($(this).val()).draw();
            });
        },

        initGradingTemplateItemGrid = function (_tableId, _columnData, _url) {
            _url = _url + "?templateId=" + $('#hdnGradingTemplateId').val();
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 4,
                    'orderable': false
                }]
            };
            grid.init(settings);
        };

    return {
        init: init,
        addGradingTemplateItemPopup: addGradingTemplateItemPopup,
        editGradingTemplateItemPopup: editGradingTemplateItemPopup,
        deleteGradingTemplateItemData: deleteGradingTemplateItemData,
        onSaveSuccess: onSaveSuccess,
        checkScoreFromTo: checkScoreFromTo,
        loadGradingTemplateItemGrid: loadGradingTemplateItemGrid
    };
}();

$(document).ready(function () {
    gradingTemplateItems.init();

    $(document).on('click', '#btnAddGradingTemplateItem', function () {
        gradingTemplateItems.addGradingTemplateItemPopup($(this));
    });

    $("#tbl-GradingTemplateItem_filter").hide();
    $('#tbl-GradingTemplateItem').DataTable().search('').draw();
    $("#GradingTemplateItemListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-GradingTemplateItem').DataTable().search($(this).val()).draw();
    });
});

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}


