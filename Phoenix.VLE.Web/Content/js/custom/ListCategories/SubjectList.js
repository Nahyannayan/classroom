﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();

var subjects = function () {

    var init = function () {
        loadSubjectGrid();
    },

        loadSubjectPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.subject;
            globalFunctions.loadPopup(source, '/ListCategories/SubjectList/InitAddEditSubjectForm?id=' + id, title, 'subject-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                formElements.feColorpicker();
                popoverEnabler.attachPopover();
            });
        },

        addSubjectPopup = function (source) {
            loadSubjectPopup(source, translatedResources.add);
        },

        editSubjectPopup = function (source, id) {
            loadSubjectPopup(source, translatedResources.edit, id);
        },

        deleteSubjectData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('Subject', '/ListCategories/SubjectList/DeleteSubjectData', 'tbl-Subject', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        loadSubjectGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "SubjectName", "sTitle": translatedResources.subject, "sWidth": "40%", "sClass": "wordbreak" },
                { "mData": "SubjectCode", "sTitle": translatedResources.subjectCode, "sWidth": "15%", "sClass": "open-details-control wordbreak" },
                { "mData": "SubjectColorCode", "sTitle": translatedResources.subjectColorCode, "sWidth": "25%" },
                { "mData": "SubjectTypeName", "sTitle": translatedResources.type, "sWidth": "10%", "sClass": "wordbreak" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "10%" }
            );
            initSubjectGrid('tbl-Subject', _columnData, '/ListCategories/SubjectList/LoadSubjectGrid');
        },

        initSubjectGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': 4
                }],
            };
            grid.init(settings);
        };

    return {
        init: init,
        addSubjectPopup: addSubjectPopup,
        editSubjectPopup: editSubjectPopup,
        deleteSubjectData: deleteSubjectData,
        onSaveSuccess: onSaveSuccess,
        loadSubjectGrid: loadSubjectGrid
    };
}();

$(document).ready(function () {
    subjects.init();

    $(document).on('click', '#btnAddSubject', function () {
        subjects.addSubjectPopup($(this));
    });

    //$("#tbl-Subject_filter").hide();
    $('#tbl-Subject').DataTable().search('').draw();
    $("#SubjectListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-Subject').DataTable().search($(this).val()).draw();
    });
});



