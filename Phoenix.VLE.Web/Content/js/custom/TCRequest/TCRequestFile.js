﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var listRecordings = [];
//var audio_context;
//var recorder;
//Get TCRequest Data.
function saveTcRequestDetails() {
    var res = $('#TransferBSUID').val().split("|");
    var iSGemsTC = false;
    var transferBSUID = res[0];
    if (res[1] == "GEMS") {
        iSGemsTC = true
    }
    var list = [];
    $('.questionDetails input[type="radio"]:checked').each(function () {
        var SelectCoursesList = new Object();
        SelectCoursesList.TransferQuestionID = $(this).parents(".questionDetails").find(".TransferQuestionID").val();
        SelectCoursesList.Answer = $(this).val();
        list.push(SelectCoursesList);
    });
    var model = {        
        LastAttDate: $('#LastDateofAttendance').val(),
        LeaveDate: $('#LastDateofAttendance').val(),
        TotalDays: $('#TotalDays').val(),
        TotalPresentDays: $('#TotalPresentDays').val(),
        TransferReasonID: $('#TransferReasonID').val(),
        TransferTypeID: 1,
        ISGemsTC: iSGemsTC,
        TransferBSUID: transferBSUID,
        TransferFeedback: $('#TransferFeedback').val(),
        TransferZone: $('#TransferZone').val(),
        IsOnlineTransfer: $('#IsOnlineTransfer').val(),
        AnswerList: list
    };
    if (model.LastAttDate !== "" && model.TransferReasonID !== "" && model.TransferFeedback !== "" && model.TransferBSUID != "" && list.length == parseFloat($("#QuestionCount").val())) {
        submitTCRequestDetails(model);
    }
    else {
        globalFunctions.showMessage("error", translatedResources.FillData );
    }
}

//Save TCRequest Data.
function submitTCRequestDetails(model) {
    $.ajax({
        type: 'POST',
        contentType: "application/json",
        data: JSON.stringify(model),
        url: "/ParentCorner/TCRequest/SaveTCRequest",
        success: function (result) {
           
            var data = JSON.parse(result.events);
            if (data.success == "true") {
                globalFunctions.showMessage("success", data.message);
                $('#requestPopup').modal('show');
            }
            else {
                globalFunctions.showMessage("error", data.message);
            }
        },
        error: function (msg) {
            globalFunctions.onFailure();
        }
    });
}

function playpauseMP3(objAnchor) {
    
    var audioElement = $('#player');
    audioElement.attr('src', objAnchor.getAttribute("audio"));
    $('#player').play();
}

function HifzTrackerFileUpload(HifzTrackerId, VersesName, FileName, studentId) {
    ClearHifzTrackerData();
    $("#SelectedHifzTrackerId").val(HifzTrackerId);
    $("#SelectedVerseNo").val(VersesName);
    $(".SelectedFileName").text(FileName);
    $("#SelectedStudentId").val(studentId)
    $('#fileUploadtracker').modal({
        //show: true,
        keyboard: false,
        backdrop: 'static'
    });
}

function uploadAudioFromBlob(assetID, blob) {

    var sizeInMB = parseFloat((listRecordings[0].size / (1024 * 1024)).toFixed(2));
    if (sizeInMB <= 5) {
        $(".btn-outline-secondary").click();
        var fd = new FormData();
        fd.append('HifzTrackerId', $("#SelectedHifzTrackerId").val());
        fd.append('VerseNo', $("#SelectedVerseNo").val());
        fd.append('StudentID', $("#SelectedStudentId").val());
        fd.append('IsCompleted', false);
        $.each(listRecordings, function (idx, file) {
            fd.append("recordings", file, file.name);
        });

        $.ajax({
            type: 'POST'
            , url: "/ParentCorner/HifzTracker/SaveHifzFile"
            , data: fd
            , processData: false
            , contentType: false
            , dataType: 'json'
            , cache: false
            , success: function (json) {
                var data = JSON.parse(json.opResult), msg = "", succ = "";
                succ = data.success.toLowerCase(); msg = data.message;
                if (succ == "true") {
                    globalFunctions.showSuccessMessage(msg);
                } else {
                    globalFunctions.showErrorMessage(msg);
                }
                $(".btn-outline-secondary").click();
                $("#fileUploadtracker").modal('hide');
                $("#recordingslist").html('');

                GetHifzTrackerFileByStudetId($("#SelectedStudentId").val());
                //GetHifzToDoStudetId($("#SelectedStudentId").val());
                //GetHifzCompletedStudetId($("#SelectedStudentId").val());
            }
            , error: function (jqXHR, textStatus, errorThrown) {
                globalFunctions.showErrorMessage(translatedResources.TechnicalError);
            }
        });
    }
    else {
        globalFunctions.showErrorMessage(translatedResources.HifzTrackerFileSize);
    }
    
}

function GetHifzTrackerFileByStudetId(studentId) {
   
    $.ajax({
        type: 'GET',
        contentType: "HTML",
        url: "/ParentCorner/HifzTracker/GetHifzTrackerFilesByStudentId?studentId=" + studentId,
        success: function (result) {
            $("#resource-" + studentId).html("");
            $("#resource-" + studentId).html(result);
            //$("#resource-" + studentId).addClass("in active");
        },
        error: function (msg) {
            globalFunctions.onFailure();
        }
    });
}
//section for student ToDo
function GetHifzToDoStudetId(studentId) {
   
    $.ajax({
        type: 'GET',
        contentType: "HTML",
        url: "/ParentCorner/HifzTracker/GetHifzTrackerByStudentId?studentId=" + studentId + "&status=todo",
        success: function (result) {
            $(".todo-" + studentId).html("");
            $(".todo-" + studentId).html(result);
            //$("#resource-" + studentId).addClass("in active");
        },
        error: function (msg) {
            globalFunctions.onFailure();
        }
    });
}
//section for completed 
function GetHifzCompletedStudetId(studentId) {
   
    $.ajax({
        type: 'GET',
        contentType: "HTML",
        url: "/ParentCorner/HifzTracker/GetHifzTrackerByStudentId?studentId=" + studentId + "&status=completed",
        success: function (result) {
            $(".completed-" + studentId).html("");
            $(".completed-" + studentId).html(result);
            //$("#resource-" + studentId).addClass("in active");
        },
        error: function (msg) {
            globalFunctions.onFailure();
        }
    });
}

function ClearHifzTrackerData() {
    $(".btn-outline-secondary").click();
    $("#stop-btn").click();
    $("#recordingslist").html('');
    $("#fileUploadtracker").modal('hide');
}

// Recording related function
var audio_context;
var recorder;
var audio_stream;

function Initialize() {
    try {
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
       // navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
        navigator.getUserMedia =(navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia);
        window.URL = window.URL || window.webkitURL;

        console.log('Audio context is ready !');
        console.log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
    } catch (e) {
        alert(translatedResources.TechnicalError);
    }
}

/**
 * Starts the recording process by requesting the access to the microphone.
 * Then, if granted proceed to initialize the library and store the stream.
 *
 * It only stops when the method stopRecording is triggered.
 */
function startRecording() {
    // One-liner to resume playback when user interacted with the page.
    audio_context = new AudioContext;
    document.querySelector('button').addEventListener('click', function () {
        audio_context.resume().then(() => {
            console.log('Playback resumed successfully');
        });
    });
    // Access the Microphone using the navigator.getUserMedia method to obtain a stream
    navigator.getUserMedia({ audio: true }, function (stream) {
        // Expose the stream to be accessible globally
        audio_stream = stream;
        // Create the MediaStreamSource for the Recorder library
        var input = audio_context.createMediaStreamSource(stream);
        console.log('Media stream succesfully created');

        // Initialize the Recorder Library
        recorder = new Recorder(input);
        console.log('Recorder initialised');

        // Start recording !
        recorder && recorder.record();
        console.log('Recording...');

        // Disable Record button and enable stop button !
        document.getElementById("start-btn").disabled = true;
        document.getElementById("stop-btn").disabled = false;
    }, function (e) {
        console.error('No live audio input: ' + e);
    });
}

/**
 * Stops the recording process. The method expects a callback as first
 * argument (function) executed once the AudioBlob is generated and it
 * receives the same Blob as first argument. The second argument is
 * optional and specifies the format to export the blob either wav or mp3
 */
function stopRecording(callback, AudioFormat) {
    // Stop the recorder instance
    recorder && recorder.stop();    
    console.log('Stopped recording.');
    // Stop the getUserMedia Audio Stream !

    //navigator.mediaDevices.getUserMedia({ audio: true, video: true })
    //    .then(mediaStream => {
    //        document.querySelector('video').srcObject = mediaStream;
    //        // Stop the audio stream after 5 seconds
    //        setTimeout(() => {
    //            const tracks = mediaStream.getAudioTracks()
    //            tracks[0].stop()
    //        }, 5000)
    //    })

    audio_stream.getAudioTracks()[0].stop();

    // Disable Stop button and enable Record button !
    document.getElementById("start-btn").disabled = false;
    document.getElementById("stop-btn").disabled = true;

    // Use the Recorder Library to export the recorder Audio as a .wav file
    // The callback providen in the stop recording method receives the blob
    if (typeof (callback) == "function") {

        /**
         * Export the AudioBLOB using the exportWAV method.
         * Note that this method exports too with mp3 if
         * you provide the second argument of the function
         */
        var recordingblob = null;
        recorder && recorder.exportWAV(function (blob) {
            callback(blob);
            recordingblob = blob;
            
            var formData = new FormData(document.getElementById("file-3")[0]);

            if (recordingblob) {
                var recording = new Blob([recordingblob], { type: "audio/wav" });
                formData.append("fileUploader", recording);
            }
            // create WAV download link using audio data blob
            // createDownloadLink();

            // Clear the Recorder to start again !
            recorder.clear();
        }, (AudioFormat || "audio/wav"));
    }
}

// Initialize everything once the window loads
window.onload = function () {
    // Prepare and check if requirements are filled
    Initialize();

    // One-liner to resume playback when user interacted with the page.
    document.querySelector('button').addEventListener('click', function () {
        audio_context.resume().then(() => {
            console.log('Playback resumed successfully');
        });
    });
    // Handle on start recording button
    var start_btn = document.getElementById("start-btn");
    if (start_btn) {
        start_btn.addEventListener("click", function () {
            startRecording();
        }, false);
    }

    // Handle on stop recording button
    var stop_btn = document.getElementById("stop-btn");
    if (stop_btn) {
        stop_btn.addEventListener("click", function () {
            // Use wav format
            var _AudioFormat = "audio/wav";
            // You can use mp3 to using the correct mimetype
            //var AudioFormat = "audio/mpeg";

            stopRecording(function (AudioBLOB) {
                
                var url = URL.createObjectURL(AudioBLOB);
                var li = document.createElement('li');
                var au = document.createElement('audio');
                var hf = document.createElement('a');
                var i = document.createElement('i');
                var btn = document.createElement('a');
                btn.setAttribute('onClick', "uploadAudioFromBlob('1', '" + url + "')");
                btn.setAttribute('class', 'btn btn-primary m-0');
                btn.innerHTML = translatedResources.Save;
                au.controls = true;
                au.src = url;
                hf.onclick = function () {
                    $("#recordingslist").html('');     
                    //var deletedFileName = this.parentElement.closest('li').className;
                    //var parentElement = this.parentElement.parentElement;
                    //var childElement = this.parentElement;
                    //globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);
                    //$(document).bind('okClicked', this, function (e) {
                    //    $("#recordingslist").html('');                        
                    //});
                };
                hf.className = 'deleteRecording mx-2 btn btn-primary mt-1';
                hf.innerHTML = translatedResources.Delete;
                //i.className = 'far fa-trash-alt';
                hf.append(i);
                var FileName = new Date().toISOString() + '.wav';
                //hf.download = FileName;
                //hf.innerHTML = hf.download;
                li.className = FileName + " align-items-center d-flex mb-2";
                li.appendChild(au);
                li.appendChild(hf);
                li.appendChild(btn);
                
                if (recordingslist.length == undefined)
                    recordingslist.prepend(li);
                else recordingslist[recordingslist.length - 1].prepend(li);
                var myFile = blobToFile(AudioBLOB, FileName);
                function blobToFile(theBlob, fileName) {
                    //A Blob() is almost a File() - it's just missing the two properties below which we will add
                    theBlob.lastModifiedDate = new Date();
                    theBlob.name = fileName;
                    //return new Blob([theBlob], { type: 'audio/wav' });
                    //return new File([theBlob], fileName);
                    return theBlob;
                }                
                listRecordings = [];
                listRecordings.push(myFile)
               // recordingslist.appendChild(btn);
                //hf.href = url;
                // Important:
                // Change the format of the file according to the mimetype
                // e.g for audio/wav the extension is .wav
                //     for audio/mpeg (mp3) the extension is .mp3
                //hf.download = new Date().toISOString() + '.wav';
                
                //hf.innerHTML = hf.download;
                //li.appendChild(au);
                //recordingslist.appendChild(li);
                //recordingslist.appendChild(btn);
            }, _AudioFormat);
        }, false);
    }   
};
//*******************************************

$(document).ready(function () {

    $("#home-tab-link").click(function () {
        $(".btn-outline-secondary").click();
    });

    $("#home-tab-browse").click(function () {
        $("#recordingslist").html('');
    });

    $(".uploaderWidget").change(function () { $(this).find(".fas.fa-file").css("color", "gray") });

    $(".cloaseHifzTrackerFileUpload").click(function () {        
        ClearHifzTrackerData();
    });
    $("#stop-btn").hide();

    $("#start-btn").click(function () {        
        $("#stop-btn").show();
        $("#start-btn").hide();
        $("#recordingslist").html('');
        $(".fileUploader").attr("disabled", true)
    });
    $("#stop-btn").click(function () {       
        $("#start-btn").show();
        $("#stop-btn").hide();
        $(".fileUploader").attr("disabled", false)
    });

    $(".closeTcRequestModel").click(function () {
        window.location = "/ParentCorner/TCRequest";
    });

    $('.paymentDue-toast').addClass('animated fadeInRight');
    $('body').on('click', '.close-toast', function () {
        $(this).parent().parent().fadeOut();
    })

    $(".sidebar-fixed .list-group").mCustomScrollbar({
        setHeight: "86%",
        autoExpandScrollbar: true,
        autoHideScrollbar: true,
        scrollbarPosition: "outside"
    });

    $('.loader-wrapper').fadeOut(500, function () {
        //$('.loader-wrapper').remove();
    });
    $(".notification-list").mCustomScrollbar({
        //setHeight: "76%",
        autoExpandScrollbar: true,
        scrollbarPosition: "inside",
        autoHideScrollbar: true,
    });


    $(".fileUploader").fileinput({
        language: translatedResources.SystemLanguageCode,
        title: translatedResources.BrowseFile,
        theme: "fas",
        uploadUrl: "/ParentCorner/HifzTracker/SaveHifzFile",
        uploadExtraData: function () {
            return {
                HifzTrackerId: $("#SelectedHifzTrackerId").val(),
                VerseNo: $("#SelectedVerseNo").val(),
                IsCompleted: false,
                StudentID: $("#SelectedStudentId").val()
            };
        },
        showUpload: true,
        showCaption: false,
        showRemove: false,
        minFileCount: 1,
        maxFileCount: 8,  
        maxFileSize: 5120,
        browseClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
        removeClass: "btn btn-sm m-0 z-depth-0",
        uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
        cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
        overwriteInitial: false,
        allowedFileExtensions: translatedResources.FileExtensions,
        initialPreviewAsData: true, // defaults markup
        initialPreviewConfig: [
            {
                frameAttr: {
                    title: translatedResources.CustomTitle                  
                }
            }],
        fileActionSettings: {
            showZoom: false,
            showUpload: false,
            //indicatorNew: "",
            showDrag: false
        },
        preferIconicPreview: false, // this will force thumbnails to display icons for following file extensions        
        previewSettings: {
            image: { width: "100%", height: "auto" },
            /* html: { width: "50px", height: "auto" },
            other: { width: "50px", height: "auto" } */
        },
      msgSizeTooLarge: 'Total File size (<b>{size}</b>) exceeds maximum allowed upload size of <b>{maxSize}</b>. Please retry your upload!'
    }).off('filebatchselected').on('filebatchselected', function (event, files) {
        var size = 0;
        $.each(files, function (ind, val) {
            //console.log($(this).attr('size'));
            size = size + $(this).attr('size');
        });

        var maxFileSize = $(this).data().fileinput.maxFileSize,
            msg = $(this).data().fileinput.msgSizeTooLarge,
            formatSize = (s) => {
                i = Math.floor(Math.log(s) / Math.log(1024));
                sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                out = (s / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i];
                return out;
            };

        if ((size / 1024) > maxFileSize) {
            msg = msg.replace('{name}', 'all files');
            msg = msg.replace('{size}', formatSize(size));
            msg = msg.replace('{maxSize}', formatSize(maxFileSize * 1024 /* Convert KB to Bytes */));
            //$('li[data-thumb-id="thumb-postedFile-0"]').html(msg);
            if (files.length > 1) {
                $(".kv-fileinput-error.file-error-message").html('');
                $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                $(".close.kv-error-close").on('click', function (e) {
                    $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                    //$(".fileinput-upload").removeClass('d-none');
                });
            }
            else {
                $(".kv-fileinput-error.file-error-message").html('');
                $(".kv-fileinput-error.file-error-message").append(`<button type="button" class="close kv-error-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button><ul><li data-thumb-id="thumb-error" data-file-id="thumb-error">`+ msg + `</li></ul>`).attr("style", "");
                $(".close.kv-error-close").on('click', function (e) {
                    $(".kv-fileinput-error.file-error-message").attr("style", "display:none").html('');
                    //$(".fileinput-upload").removeClass('d-none');
                });
            }

            $(".fileinput-upload").attr('disabled', true);
        }
    }).off('fileuploaded').on("fileuploaded", function (e, response) {
       

        //globalFunctions.showMessage(data.response.NotificationType, data.response.Message);
        var data = JSON.parse(response.response.opResult), msg = "", succ = "";
        succ = data.success.toLowerCase(); msg = data.message;
        if (succ == "true") {
            globalFunctions.showSuccessMessage(msg);
        } else {
            globalFunctions.showErrorMessage(msg);
        }
        $(".btn-outline-secondary").click();
        $("#fileUploadtracker").modal('hide');
        $("#recordingslist").html('');
       
        GetHifzTrackerFileByStudetId($("#SelectedStudentId").val());
        //GetHifzToDoStudetId($("#SelectedStudentId").val());
        //GetHifzCompletedStudetId($("#SelectedStudentId").val());
    });
})
