﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();
var studentSuggestion = function () {

    var init = function () {
        loadSuggestionListGrid();
        $("#tbl-UserSuggestionList_filter").hide();
    },
        loadSuggestionListGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Title", "sTitle": translatedResources.title, "sWidth": "25%", "sClass": "wordbreak" },
                { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "25%", "sClass": "open-details-control wordbreak" },
                { "mData": "Type", "sTitle": translatedResources.type, "sWidth": "20%" },
                { "mData": "UserName", "sTitle": translatedResources.userName, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.downloadAttachment, "sWidth": "10%" }

            );
            initSuggestionCategoryGrid('tbl-UserSuggestionList', _columnData, '/SuggestionBox/Suggestions/LoadSuggestionGrid');

        },
        downloadAttachment = function (source, file) {
            var filePath = $(this).data('filepath');
            window.open(filePath, "_blank");
        },
        initSuggestionCategoryGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false
            };
            grid.init(settings);
        };

    return {
        init: init,
        downloadAttachment: downloadAttachment,
        loadSuggestionListGrid: loadSuggestionListGrid
    };
}();

$(document).ready(function () {
    studentSuggestion.init();
    $("#tbl-UserSuggestionList_filter").hide();
    $('#tbl-UserSuggestionList').DataTable().search('').draw();
    $("#UserSuggestionListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-UserSuggestionList').DataTable().search($(this).val()).draw();
    });
});



