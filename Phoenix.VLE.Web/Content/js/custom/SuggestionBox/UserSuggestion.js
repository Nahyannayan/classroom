﻿/// <reference path="../../common/global.js" />
var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var suggestion = function () {

    var init = function () {
        loadSuggestionCategoryGrid();
        $("#tbl-UserSuggestions_filter").hide();
    },

        loadSuggestionPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.suggestion;
            globalFunctions.loadPopup(source, '/SuggestionBox/Suggestions/InitAddEditSuggestionForm?id=' + id, title, 'suggestion-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: translatedResources.select
                });
                $(document).on('click', '#downloadAttachment', function () {
                    var filename = $(this).data("filepath");
                    window.open(filename, "_blank");
                });
                $(document).on('click', '#deleteAttachment', function () {
                    $('.attachments-preview').hide();
                    $('#FileName').val('');
                });
            });
        },

        addSuggestionPopup = function (source) {
            loadSuggestionPopup(source, translatedResources.add);
        },
        saveSuggestion = function (e) {
            if ($('form#frmAddSuggestion').valid()) {
                var formData = new FormData();
                var suggestionFile = document.getElementById("dropSuggestionFile").files[0];
                formData.append("suggestionFile", suggestionFile);
                formData.append("SuggestionId", $('#SuggestionId').val());
                formData.append("UserId", $('#UserId').val());
                formData.append("IsAddMode", $('#IsAddMode').val());
                formData.append("Title", $('#Title').val());
                formData.append("Description", $('#Description').val());
                formData.append("TypeId", $('#TypeId').val());
                formData.append("FileName", $('#FileName').val());
                $.ajax({
                    type: 'POST',
                    url: '/SuggestionBox/Suggestions/InsertUpdateSuggestion',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result.Success == true)
                            $('#myModal').modal('hide');
                        globalFunctions.showMessage(result.NotificationType, result.Message);
                        //loadSuggestionCategoryGrid();
                        suggestion.loadSuggestionsGrid();
                    },
                    error: function (msg) {
                        globalFunctions.onFailure();
                    }
                });
                $("#myModal").modal('hide');
            }
        },
        validate = function (source) {
            var result = false;

        },
        editSuggestionPopup = function (source, id) {
            loadSuggestionPopup(source, translatedResources.edit, id);
        },

        deleteSuggestionData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        type: 'POST',
                        url: '/SuggestionBox/Suggestions/DeleteSuggestionData',
                        data: { id: id },
                        success: function (result) {
                            if (result.Success == true)
                                globalFunctions.showSuccessMessage(translatedResources.deletedSuccess)
                            suggestion.loadSuggestionsGrid();
                        },
                        error: function (msg) {
                            globalFunctions.onFailure();
                        }
                    });
                })

            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },
        downloadAttachment = function (file) {
            window.open("/SuggestionBox/Suggestions/DownloadAttachment?fileName=" + file, "_blank");
        },
        loadSuggestionsGrid = function () {
            var grid = new DynamicPagination("suggestionsPanel");
            var settings = {
                url: '/SuggestionBox/Suggestions/LoadSuggestionsPanel/?pageIndex=1'
            };
            grid.init(settings);
        },
        loadSuggestionCategoryGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Title", "sTitle": translatedResources.title, "sWidth": "20%", "sClass": "wordbreak" },
                { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "30%", "sClass": "open-details-control wordbreak" },
                { "mData": "Type", "sTitle": translatedResources.type, "sWidth": "30%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initSuggestionCategoryGrid('tbl-UserSuggestions', _columnData, '/SuggestionBox/Suggestions/LoadSuggestionGridByUser');
            $("#tbl-UserSuggestions_filter").hide();
            $('#tbl-UserSuggestions').DataTable().search('').draw();
            $("#SuggestionListSearch").on("input", function (e) {
                e.preventDefault();
                $('#tbl-UserSuggestions').DataTable().search($(this).val()).draw();

            });
        },

        initSuggestionCategoryGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 3,
                    'orderable': false
                }]
            };
            grid.init(settings);
        },

        downloadSuggestionsData = function (source) {
            
            window.location.href = '/SuggestionBox/Suggestions/DownloadStudentSuggestionDetails';
        };

    return {
        init: init,
        addSuggestionPopup: addSuggestionPopup,
        editSuggestionPopup: editSuggestionPopup,
        saveSuggestion: saveSuggestion,
        deleteSuggestionData: deleteSuggestionData,
        onSaveSuccess: onSaveSuccess,
        loadSuggestionsGrid: loadSuggestionsGrid,
        downloadAttachment: downloadAttachment,
        loadSuggestionCategoryGrid: loadSuggestionCategoryGrid,
        downloadSuggestionsData: downloadSuggestionsData
    };
}();

$(document).ready(function () {
    //var grid = new DynamicPagination("suggestionsPanel");
    //var settings = {
    //    url: '/SuggestionBox/Suggestions/LoadSuggestionsPanel/?pageIndex=1'
    //};
    //grid.init(settings);
    suggestion.loadSuggestionsGrid();
    $(".descriptionPopup").on("click", function () {
        var desc = $(this).data("desc");
        $("#fullDescription").html(desc);
        $("#readMoreTitle").html("Description");
    });
    $(".titlePopup").on("click", function () {
        var desc = $(this).data("title");
        $("#fullDescription").html(desc);
        $("#readMoreTitle").html("Title");
    });
    $(document).on('click', '#btnAddSuggestion', function () {
        suggestion.addSuggestionPopup($(this));
    });
    $(document).on('click', '#btnSaveSuggestion', function () {
        suggestion.saveSuggestion($(this));
    });
    $("#tbl-UserSuggestions_filter").hide();
    $('#tbl-UserSuggestions').DataTable().search('').draw();
    $("#SuggestionListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-UserSuggestions').DataTable().search($(this).val()).draw();

    });
});



