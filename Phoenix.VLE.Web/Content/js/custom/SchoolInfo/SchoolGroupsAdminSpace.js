﻿var pageSize = 8;
var teacherGroup = function () {
    var init = function () {

    },

        loadSchoolGroupData = function (source, searchString, isSearchCall, schoolId, teacherIds, classGroupIds, groupTypeIds,courseIds) {
            
            var indexValue = 0;
            var searchTxt = searchString;
            var _schoolId = schoolId;
            var _teacherIds = teacherIds;
            var _classGroupIds = classGroupIds;
            var _groupTypeIds = groupTypeIds;
            var _courseIds = courseIds;
            var reportObject = {
                PageNumber: 1,
                SchoolId: schoolId,
                TeacherIds: teacherIds.toString(),
                ClassGroupIds: classGroupIds.toString(),
                GroupTypeIds: groupTypeIds.toString(),
                CourseIds: courseIds.toString(),
                SearchString: searchString
            };
            if ($(source).children().length == 0 || isSearchCall) {
                loadPartials(source, '/Reports/GetSchoolGroupsForAdminSpace', reportObject);
                $(source).off("partialLoaded");
                $(source).on("partialLoaded", function () {

                    $("#mis-groups").removeClass('owl-hidden');
                    source.trigger('destroy.owl.carousel');
                    applyCarousel(source, { navigation: true, margin: 8, items: 1, slideBy: 1 });
                    enableCarouselNav(source);
                    hideNextPrevIcon(source);
                    $(source).off("click", ".owl-next").on("click", ".owl-next", function () {
                        //debugger;

                        var carouselIndex = 0;
                        if (indexValue > 0) {
                            carouselIndex = indexValue + 1;
                            indexValue = carouselIndex;
                        }
                        else {
                            carouselIndex = carouselIndex + 1;
                            indexValue = carouselIndex;
                        }

                        var pageIndex = $(source).find(".item").length;
                        var totalRecordsCount = $(source).find(':first .row.item').data('totalrow');
                        $(source).find(':first .row.item').data('pageindex', (pageIndex + 1));
                        var totalPageIndex = Math.ceil(totalRecordsCount / pageSize);


                        if (pageIndex == carouselIndex) {
                            var schoolGroupObject = {
                                PageNumber: pageIndex + 1,
                                SchoolId: _schoolId,
                                TeacherIds: _teacherIds.toString(),
                                ClassGroupIds: _classGroupIds.toString(),
                                GroupTypeIds: _groupTypeIds.toString(),
                                CourseIds: _courseIds.toString(),
                                SearchString: searchTxt
                            };

                            $.ajax({
                                type: "POST",
                                url: "/Reports/GetSchoolGroupsForAdminSpace",
                                data: schoolGroupObject,
                                success: function (result) {
                                    if (result.trim() !== "") {
                                        source.trigger("add.owl.carousel", [result]).trigger("refresh.owl.carousel");
                                        source.trigger('to.owl.carousel', pageIndex);
                                        enableCarouselNav(source);
                                    } else {
                                        $(source).find(".owl-next").addClass("disabled");
                                    }
                                    //To disable next button once reached to last page
                                    if (totalPageIndex == (pageIndex + 1)) {
                                        $(source).find(".owl-next").addClass("disabled");
                                        $(source).off("click", ".owl-next");
                                    }
                                }
                            });
                        }
                        else {
                            //To disable next button once reached to last page
                            if (totalPageIndex == (pageIndex + 1)) {
                                $(source).find(".owl-next").addClass("disabled");
                                $(source).off("click", ".owl-next");
                            }
                            else {
                                $(source).find(".owl-next").removeClass("disabled");
                            }
                            return false;
                        }
                    });
                    $(source).off("click", ".owl-prev").on("click", ".owl-prev", function () {
                        indexValue = indexValue - 1;
                    });
                });
            }


        },
        loadOtherGroupsData = function (source, searchString, isSearchCall) {
            var indexValue = 0;
            var searchTxt = searchString;
            if ($(source).children().length == 0 || isSearchCall) {
                loadPartials(source, "/SchoolGroups/GetOtherGroupsWithPagination?pageIndex=1&searchString=" + encodeURIComponent(searchString));
                $(source).off("partialLoaded").on("partialLoaded", function () {
                    $("#bespoke-groups").removeClass('owl-hidden');
                    source.trigger('destroy.owl.carousel');
                    applyCarousel(source, { navigation: true, margin: 8, items: 1, slideBy: 1 });
                    enableCarouselNav(source);
                    hideNextPrevIcon(source);
                    $(source).off("click", ".owl-next").on("click", ".owl-next", function () {
                        //debugger;

                        var carouselIndex = 0;
                        if (indexValue > 0) {
                            carouselIndex = indexValue + 1;
                            indexValue = carouselIndex;
                        }
                        else {
                            carouselIndex = carouselIndex + 1;
                            indexValue = carouselIndex;
                        }
                        var pageIndex = $(source).find(".item").length;
                        var totalRecordsCount = $(source).find(':first .row.item').data('totalrow');
                        $(source).find(':first .row.item').data('pageindex', (pageIndex + 1));
                        var totalPageIndex = Math.ceil(totalRecordsCount / pageSize);


                        if (pageIndex == carouselIndex) {
                            $.ajax({
                                url: "/SchoolGroups/GetOtherGroupsWithPagination?pageIndex=" + (pageIndex + 1) + "&searchString=" + encodeURIComponent(searchTxt),
                                success: function (result) {
                                    if (result.trim() !== "") {
                                        source.trigger("add.owl.carousel", [result]).trigger("refresh.owl.carousel");
                                        source.trigger('to.owl.carousel', pageIndex);
                                        enableCarouselNav(source);
                                    } else {
                                        $(source).find(".owl-next").addClass("disabled");
                                    }
                                    //To disable next button once reached to last page
                                    if (totalPageIndex == (pageIndex + 1)) {
                                        $(source).find(".owl-next").addClass("disabled");
                                        $(source).off("click", ".owl-next");
                                    }
                                }
                            });
                        }
                        else {
                            //To disable next button once reached to last page
                            if (totalPageIndex == (pageIndex + 1)) {
                                $(source).find(".owl-next").addClass("disabled");
                                $(source).off("click", ".owl-next");
                            }
                            else {
                                $(source).find(".owl-next").removeClass("disabled");
                            }
                            return false;
                        }
                    });
                    $(source).off("click", ".owl-prev").on("click", ".owl-prev", function () {
                        indexValue = indexValue - 1;
                    });

                });
            }

        },
        loadArchivedSchoolGroupsData = function (source, searchString, isSearchCall) {
            var indexValue = 0;
            var searchTxt = searchString;
            if ($(source).children().length == 0 || isSearchCall) {
                loadPartials(source, "/SchoolGroups/GetArchivedGroupsWithPagination?pageIndex=1&searchString=" + encodeURIComponent(searchString));
                $(source).off("partialLoaded").on("partialLoaded", function () {

                    $("#archived-school-groups").removeClass('owl-hidden');
                    source.trigger('destroy.owl.carousel');
                    applyCarousel(source, { navigation: true, margin: 8, items: 1, slideBy: 1 });
                    enableCarouselNav(source);
                    hideNextPrevIcon(source);
                    $(source).off("click", ".owl-next").on("click", ".owl-next", function () {
                        //debugger;

                        var carouselIndex = 0;
                        if (indexValue > 0) {
                            carouselIndex = indexValue + 1;
                            indexValue = carouselIndex;
                        }
                        else {
                            carouselIndex = carouselIndex + 1;
                            indexValue = carouselIndex;
                        }

                        var pageIndex = $(source).find(".item").length;
                        var totalRecordsCount = $(source).find(':first .row.item').data('totalrow');
                        $(source).find(':first .row.item').data('pageindex', (pageIndex + 1));
                        var totalPageIndex = Math.ceil(totalRecordsCount / pageSize);


                        if (pageIndex == carouselIndex) {

                            $.ajax({
                                url: "/SchoolGroups/GetArchivedGroupsWithPagination?pageIndex=" + (pageIndex + 1) + "&searchString=" + encodeURIComponent(searchTxt),
                                success: function (result) {
                                    if (result.trim() !== "") {
                                        source.trigger("add.owl.carousel", [result]).trigger("refresh.owl.carousel");
                                        source.trigger('to.owl.carousel', pageIndex);
                                        enableCarouselNav(source);
                                    } else {
                                        $(source).find(".owl-next").addClass("disabled");
                                    }
                                    //To disable next button once reached to last page
                                    if (totalPageIndex == (pageIndex + 1)) {
                                        $(source).find(".owl-next").addClass("disabled");
                                        $(source).off("click", ".owl-next");
                                    }
                                }
                            });
                        }
                        else {
                            //To disable next button once reached to last page
                            if (totalPageIndex == (pageIndex + 1)) {
                                $(source).find(".owl-next").addClass("disabled");
                                $(source).off("click", ".owl-next");
                            }
                            else {
                                $(source).find(".owl-next").removeClass("disabled");
                            }
                            return false;
                        }
                    });
                    $(source).off("click", ".owl-prev").on("click", ".owl-prev", function () {
                        indexValue = indexValue - 1;
                    });

                });
            }

        },
        loadArchivedBeSpokeGroupsData = function (source, searchString, isSearchCall) {
            var indexValue = 0;
            var searchTxt = searchString;
            if ($(source).children().length == 0 || isSearchCall) {
                loadPartials(source, "/SchoolGroups/GetArchivedBespokeGroupsWithPagination?pageIndex=-1&searchString=" + encodeURIComponent(searchString));
                $(source).off("partialLoaded").on("partialLoaded", function () {
                    $("#archived-school-groups").removeClass('owl-hidden');
                    source.trigger('destroy.owl.carousel');
                    applyCarousel(source, { navigation: true, margin: 8, items: 1, slideBy: 1 });
                    enableCarouselNav(source);
                    hideNextPrevIcon(source);
                    $(source).off("click", ".owl-next").on("click", ".owl-next", function () {
                        //debugger;

                        var carouselIndex = 0;
                        if (indexValue > 0) {
                            carouselIndex = indexValue + 1;
                            indexValue = carouselIndex;
                        }
                        else {
                            carouselIndex = carouselIndex + 1;
                            indexValue = carouselIndex;
                        }

                        var pageIndex = $(source).find(".item").length;
                        var totalRecordsCount = $(source).find(':first .row.item').data('totalrow');
                        $(source).find(':first .row.item').data('pageindex', (pageIndex + 1));
                        var totalPageIndex = Math.ceil(totalRecordsCount / pageSize);


                        if (pageIndex == carouselIndex) {

                            $.ajax({
                                url: "/SchoolGroups/GetArchivedBespokeGroupsWithPagination?pageIndex=" + (pageIndex + 1) + "&searchString=" + encodeURIComponent(searchTxt),
                                success: function (result) {
                                    if (result.trim() !== "") {
                                        source.trigger("add.owl.carousel", [result]).trigger("refresh.owl.carousel");
                                        source.trigger('to.owl.carousel', pageIndex);
                                        enableCarouselNav(source);
                                    } else {
                                        $(source).find(".owl-next").addClass("disabled");
                                    }
                                    //To disable next button once reached to last page
                                    if (totalPageIndex == (pageIndex + 1)) {
                                        $(source).find(".owl-next").addClass("disabled");
                                        $(source).off("click", ".owl-next");
                                    }
                                }
                            });
                        }
                        else {
                            //To disable next button once reached to last page
                            if (totalPageIndex == (pageIndex + 1)) {
                                $(source).find(".owl-next").addClass("disabled");
                                $(source).off("click", ".owl-next");
                            }
                            else {
                                $(source).find(".owl-next").removeClass("disabled");
                            }
                            return false;
                        }
                    });
                    $(source).off("click", ".owl-prev").on("click", ".owl-prev", function () {
                        indexValue = indexValue - 1;
                    });

                });
            }
        },
        saveStudentDetails = function () {

        },
        loadGroupQuizPopup = function (source, id, groupId, moduleId, folderId) {
            var title = translatedResources.addgroupquiz;
            var grId = groupId; //$("#SectionId").val();
            globalFunctions.loadPopup(source, '/Files/Files/InitAddEditGroupQuiz?id=' + id + '&grId=' + grId + '&pfId=' + folderId + '&moduleId=' + moduleId, title, 'file-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {

            });

            $('.selectpicker').selectpicker('refresh');
        },

        createZipPackage = function (source) {
            var file = FileDownloader.getInstance();
            file.createZipFile(source, "/Schoolstructure/SchoolGroups/CreateZipFile?id=" + $(source).data("id"));
        },
        addnewSchoolGroup = function (id) {
            window.location.assign("/SchoolStructure/SchoolGroups/AddEditSchoolGroupForm?id=" + id);
        },
        enableCarouselNav = function (source) {
            $(source).find(".owl-nav, .owl-next").removeClass("disabled");
        },
        hideNextPrevIcon = function (source) {
            //var totalRecordsCount = parseInt($(source).find("#hdnTotalCount").val());
            var totalRecordsCount = $(source).find(':first .row.item').data('totalrow');
            if (totalRecordsCount <= pageSize || isNaN(totalRecordsCount)) {
                $(source).find(".owl-nav, .owl-prev, .owl-next").addClass("disabled");
            }
            else {
                $(source).find(".owl-nav, .owl-next").removeClass("disabled");
            }
        },
        applyCarousel = function (source, options) {
            var settings = {
                navigation: false,
                autoplay: false,
                nav: true,
                dots: false,
                loop: false,
                autoplayHoverPause: true,
                rtl: direction,
                mouseDrag: false
            };
            $(source).owlCarousel(Object.freeze($.extend(settings, options)));
        },

        loadPartials = function (source, url, data) {
            $(source).load(url,data, function () {
                $(source).trigger("partialLoaded");
            });
        },
        loadCoursesBySchoolGroupIds = function (groupIds) {
            var courseObject = {
                SchoolGroupIds: groupIds.toString()
            };
            $.ajax({
                type: "POST",
                url: "/SchoolInfo/Reports/GetCoursesBySchoolGroupIds",
                data: courseObject,
                success: function (data) {
                    $.each(data, function (index, value) {
                        // APPEND OR INSERT DATA TO SELECT ELEMENT.
                        $('#schoolCourseDropdown').empty();
                        $('#schoolCourseDropdown').append('<option value="' + value.CourseId + '">' + value.Title + '</option>');
                        
                        $('#schoolCourseDropdown').selectpicker("refresh");
                       
                    });
                },
                error: function () {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        DeleteOtherGroups = function DeleteOtherGroups(deletebespoke) {
            globalFunctions.notyDeleteConfirm(deletebespoke, translatedResources.deleteConfirm);
            $(document).bind('okToDelete', deletebespoke, function (e) {
                var groupId = $(deletebespoke).attr('data-grpid');
                $.post("/SchoolStructure/SchoolGroups/DeleteBespokeGroup", { groupId: groupId }, function (response) {
                    if (response.Success === true) {
                        teacherGroup.loadOtherGroupsData($("#bespoke-groups"), "", true);
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                    } else {
                        globalFunctions.onFailure();
                    }

                });
            });
        };


    return {
        init: init,
        saveStudentDetails: saveStudentDetails,
        loadSchoolGroupData: loadSchoolGroupData,
        loadOtherGroupsData: loadOtherGroupsData,
        loadGroupQuizPopup: loadGroupQuizPopup,
        loadArchivedSchoolGroupsData: loadArchivedSchoolGroupsData,
        loadArchivedBeSpokeGroupsData: loadArchivedBeSpokeGroupsData,
        enableCarouselNav: enableCarouselNav,
        createZipPackage: createZipPackage,
        addnewSchoolGroup: addnewSchoolGroup,
        hideNextPrevIcon: hideNextPrevIcon,
        DeleteOtherGroups: DeleteOtherGroups,
        loadCoursesBySchoolGroupIds: loadCoursesBySchoolGroupIds
    };
}();

function archiveSchoolGroup() {

    var _selectedMembers = $("#activeGroupList input[type='checkbox']:checked");
    if (_selectedMembers.length == 0) {
        globalFunctions.showWarningMessage(translatedResources.SelectGroupMessage);
    }
    else {
        //Only to assign source, please follow standard way as in other js files for confirmation popup
        var source = $("#activeGroupList");
        globalFunctions.notyDeleteConfirm(source, translatedResources.archiveConfirm);
        $(document).bind('okToDelete', source, function (e) {
            if ($('#selectAllUnassigned').is(':checked')) {
                $('#selectAllUnassigned').prop('checked', false);
            }
            var selectedMembers = $("#activeGroupList input[type='checkbox']:checked");
            var assignmentsToArchive = [];
            if (selectedMembers != null && selectedMembers.length > 0) {
                selectedMembers.each(function (i, elem) {
                    assignmentsToArchive = assignmentsToArchive + elem.value + ',';
                });
                $.ajax({
                    type: 'POST',
                    data: { assignmentsToArchive: assignmentsToArchive },
                    url: '/SchoolStructure/SchoolGroups/UpdateArchiveGroup',
                    success: function (result) {
                        loadGroupList();
                        // location.reload();
                    },
                    error: function (data) { }
                });
            }
            else {
                globalFunctions.showWarningMessage(translatedResources.PleaseGroupToArchive);
            }
        });
    }

}

function loadGroupList() {
    loadActiveGroup();
    loadArchiveGroup();
}

function loadActiveGroup() {
    $.ajax({
        url: '/SchoolStructure/SchoolGroups/GetActiveSchoolGroup',
        success: function (result) {
            $('#activeGroupList').html(result);
        },
        error: function (data) { }
    });
}

function loadArchiveGroup() {
    $.ajax({
        url: '/SchoolStructure/SchoolGroups/GetArchivedSchoolGroup',
        success: function (result) {
            $('#archiveGroupList').html(result);
        },
        error: function (data) { }
    });
}

$(document).ready(function () {

   
    $(document).on('click', '#btnAddGroupQuiz', function () {

        var groupId = $(this).data('groupid');
        var folderId = $(this).data('folderid');
        var moduleId = $(this).data('moduleid');
        teacherGroup.loadGroupQuizPopup($(this), 0, groupId, moduleId, folderId);
    });
   

    $(document).on('keyup', '#availableMemberSearch', function (e) {
        var searchText = $(this).val().toLowerCase();
        $('#activeGroupList ul li').each(function () {
            var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
            $(this).toggle(showCurrentLi);
        });
    });

    $(document).on('keyup', '#groupMemberSearch', function (e) {
        var searchText = $(this).val().toLowerCase();
        $('#archiveGroupList ul li').each(function () {
            var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
            $(this).toggle(showCurrentLi);
        });
    });

    $(document).on("keypress", "#searchSchoolGroupContent", function (e) {
        if (e.which == 13) {
            var searchText = $(this).val().toLowerCase();

            var schoolId = $("#SchoolId").val();
            var teacherIds = $('#teachersDropdownGroup').val();
            var classGroupIds = $('#classGroupsDropdown').val();
            var groupTypeIds = $('#groupsTypeDropdown').val();
            var courseIds = $('#schoolCourseDropdown').val();

            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchText, true, schoolId, teacherIds, classGroupIds, groupTypeIds, courseIds);
        }
    });

    $('body').on('click', '#tab-schoolGroups', function () {

        var searchString = '';
        var searchText = '';
        var schoolId = $("#SchoolId").val();
        var teacherIds = $('#teachersDropdownGroup').val();
        var classGroupIds = $('#classGroupsDropdown').val();
        var groupTypeIds = $('#groupsTypeDropdown').val();
        var courseIds = $('#schoolCourseDropdown').val();

        searchText = $('#searchSchoolGroupContent').val();
        if (searchText != '') {
            $('#searchSchoolGroupContent').val('');
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, true, schoolId, teacherIds, classGroupIds, groupTypeIds, courseIds);
        }
        else {
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, false, schoolId, teacherIds, classGroupIds, groupTypeIds, courseIds);
        }

    });

    $('#classGroupsDropdown').on('change', function (event) {
        var searchString = '';
        var searchText = '';
        var schoolId = $("#SchoolId").val();
        var teacherIds = $('#teachersDropdownGroup').val();
        var classGroupIds = $('#classGroupsDropdown').val();
        var groupTypeIds = $('#groupsTypeDropdown').val();
        var courseIds = $('#schoolCourseDropdown').val();

        //teacherGroup.loadCoursesBySchoolGroupIds(classGroupIds);

        searchText = $('#searchSchoolGroupContent').val();
        if (searchText != '') {
            $('#searchSchoolGroupContent').val('');
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, true, schoolId, teacherIds, classGroupIds, groupTypeIds, courseIds);
        }
        else {
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, true, schoolId, teacherIds, classGroupIds, groupTypeIds, courseIds);
        }

    });

    $('#otherGroupsDropdown').on('change', function (event) {
        var searchString = '';
        var searchText = '';
        var schoolId = $("#SchoolId").val();
        var teacherIds = $('#teachersDropdownGroup').val();
        var otherGroupIds = $('#otherGroupsDropdown').val();
        var groupTypeIds = $('#groupsTypeDropdown').val();
        var courseIds = $('#schoolCourseDropdown').val();

        //teacherGroup.loadCoursesBySchoolGroupIds(classGroupIds);

        searchText = $('#searchSchoolGroupContent').val();
        if (searchText != '') {
            $('#searchSchoolGroupContent').val('');
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, true, schoolId, teacherIds, otherGroupIds, groupTypeIds, courseIds);
        }
        else {
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, true, schoolId, teacherIds, otherGroupIds, groupTypeIds, courseIds);
        }

    });

    $('#groupsTypeDropdown').on('change', function (event) {
        var searchString = '';
        var searchText = '';
        var schoolId = $("#SchoolId").val();
        var teacherIds = '';
        var classGroupIds = '';
        var groupTypeIds = $('#groupsTypeDropdown').val();
        var courseIds = '';
        if (groupTypeIds == "2") {
            $("#otherGroupsDDL").removeClass("d-none");
            $("#otherGroupsDDL").addClass("d-block");
            $("#classGroupsDDL").addClass("d-none");
            $("#classGroupsDDL").removeClass("d-block");
        }
        else {
            $("#classGroupsDDL").removeClass("d-none");
            $("#classGroupsDDL").addClass("d-block");
            $("#otherGroupsDDL").addClass("d-none");
            $("#otherGroupsDDL").removeClass("d-block");
        }

        $(".bs-deselect-all").click();

        searchText = $('#searchSchoolGroupContent').val();
        if (searchText != '') {
            $('#searchSchoolGroupContent').val('');
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, true, schoolId, teacherIds, classGroupIds, groupTypeIds, courseIds);
        }
        else {
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, true, schoolId, teacherIds, classGroupIds, groupTypeIds, courseIds);
        }

    });

    $('#schoolCourseDropdown').on('change', function (event) {
        var searchString = '';
        var searchText = '';
        var schoolId = $("#SchoolId").val();
        var teacherIds = $('#teachersDropdownGroup').val();
        var classGroupIds = $('#classGroupsDropdown').val();
        var groupTypeIds = $('#groupsTypeDropdown').val();
        var courseIds = $('#schoolCourseDropdown').val();

        searchText = $('#searchSchoolGroupContent').val();
        if (searchText != '') {
            $('#searchSchoolGroupContent').val('');
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, true, schoolId, teacherIds, classGroupIds, groupTypeIds, courseIds);
        }
        else {
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, true, schoolId, teacherIds, classGroupIds, groupTypeIds, courseIds);
        }

    });

    $('#teachersDropdownGroup').on('change', function (event) {
        var searchString = '';
        var searchText = '';
        var schoolId = $("#SchoolId").val();
        var teacherIds = $('#teachersDropdownGroup').val();
        var classGroupIds = $('#classGroupsDropdown').val();
        var groupTypeIds = $('#groupsTypeDropdown').val();
        var courseIds = $('#schoolCourseDropdown').val();

        searchText = $('#searchSchoolGroupContent').val();
        if (searchText != '') {
            $('#searchSchoolGroupContent').val('');
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, true, schoolId, teacherIds, classGroupIds, groupTypeIds, courseIds);
        }
        else {
            teacherGroup.loadSchoolGroupData($("#mis-groups"), searchString, true, schoolId, teacherIds, classGroupIds, groupTypeIds, courseIds);
        }

    });

    

    $(document).on('click', '#btnAddNewGroup', function () {
        groupDetail.loadNewGroupCreatePopup($(this), translatedResources.add);

    });

    //On click of group header section redirect to group detail page
    $('body').off('click', '.group-header').on('click', '.group-header', function (e) {
        if (e.target !== this)
            return;

        var id = $(this).data('id');
        location.href = "/SchoolStructure/SchoolGroups/GroupDetails?grpId=" + id;
    });

    if ($("#groupsTypeDropdown").val() == "2") {
        var _groupTypeName = $("select[name=groupsTypeDropdown] option[value='2']").text();
        $('.groupTypeDDL .bootstrap-select .filter-option .filter-option-inner-inner').text(_groupTypeName);
    }

   
});




