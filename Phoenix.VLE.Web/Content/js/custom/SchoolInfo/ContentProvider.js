﻿/// <reference path="../../common/global.js" />
//var popoverEnabler = new LanguageTextEditPopover();
var contentProvider = function () {

    var init = function () {
        loadContentLibraryGrid();
        $("#tbl-ContentLibrary_filter").hide();
        $("#tbl-ContentProvider_filter").hide();
    },
        loadContentLibraryGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "ContentName", "sTitle": translatedResources.contentName },
                { "mData": "Description", "sTitle": translatedResources.description, "sClass": "open-details-control " },
                { "mData": "SubjectName", "sTitle": translatedResources.subject },
                { "mData": "SchoolName", "sTitle": translatedResources.schoolname, "sClass": "open-details-control " },
                { "mData": "Status", "sTitle": translatedResources.status },
                { "mData": "CreatedOn", "sTitle": translatedResources.createdOn },
                { "mData": "Assign", "sClass": "text-center no-sort", "sTitle": translatedResources.assign},
                { "mData": "Approve", "sClass": "text-center no-sort", "sTitle": translatedResources.approve },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions }


            );
            initContentLibraryGrid('tbl-ContentLibrary', _columnData, '/SchoolInfo/ContentLibrary/LoadContentLibraryGrid');
            var _columnData = [];
            _columnData.push(
                { "mData": "ContentName", "sTitle": translatedResources.contentName, "sWidth": "50%", "sClass": "wordbreak" },
                //{ "mData": "CreatedOn", "sTitle": translatedResources.createdOn, "sWidth": "20%" },
                { "mData": "Assign", "sClass": "text-center no-sort", "sTitle": translatedResources.assign, "sWidth": "50%" }

            );
            initContentLibraryGrid('tbl-ContentProvider', _columnData, '/SchoolInfo/ContentLibrary/LoadContentProviderGrid');
            $("#tbl-ContentProvider_filter").hide();
            $('#tbl-ContentProvider').DataTable().search('').draw();
            $("#ContentProviderSearch").on("input", function (e) {
                e.preventDefault();
                $('#tbl-ContentProvider').DataTable().search($(this).val()).draw();
            });

            $("#tbl-ContentLibrary_filter").hide();
            $('#tbl-ContentLibrary').DataTable().search('').draw();
            $("#ContentLibrarySearch").on("input", function (e) {
                e.preventDefault();
                $('#tbl-ContentLibrary').DataTable().search($(this).val()).draw();
            });
        },
        addContentLibraryPopup = function (source) {
            loadContentLibraryPopup(source, translatedResources.add);
        },
        downloadAttachment = function (source, file) {
            window.open("/Shared/Shared/DownloadFile?filePath=" + file, "_blank");
        },
        editContentLibraryPopup = function (source, id) {
            loadContentLibraryPopup(source, translatedResources.edit, id);
        },
        loadContentLibraryPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.contentresource;
            globalFunctions.loadPopup(source, '/SchoolInfo/ContentLibrary/InitAddContentResourceForm?id=' + id, title, 'suggestion-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                $('select').selectpicker({
                    noneSelectedText: translatedResources.PleaseSelect
                });
            });
        },
        deleteContentResourceData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('ContentResource', '/SchoolInfo/ContentLibrary/DeleteContentResourceData', 'tbl-ContentLibrary', source, id);
            }
        },
        assignContentLibrary = function (source, id) {
            //debugger;
            var OperationType = source.is(':checked');
            var sourceId = "#assign_" + id;
            var value = $(sourceId).val();
            $.ajax({
                type: "POST",
                url: "/SchoolInfo/ContentLibrary/AssignContentResourceToSchool",
                data: { id: id, status: OperationType },
                success: function (response) {
                    //debugger;
                    loadContentLibraryGrid();
                    //if (response) { globalFunctions.showMessage(result.NotificationType, result.Message);}
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });

        },
        changeContentLibraryStatus = function (source, id) {
            //debugger;
            var OperationType = source.is(':checked');
            var sourceId = "#assign_" + id;
            var value = $(sourceId).val();
            $.ajax({
                type: "POST",
                url: "/SchoolInfo/ContentLibrary/ChangeContentLibraryStatus",
                data: { id: id, status: OperationType },
                success: function (response) {
                    //debugger;
                    loadContentLibraryGrid();
                    //if (response) { globalFunctions.showMessage(result.NotificationType, result.Message);}
                },
                error: function (error) {
                    globalFunctions.onFailure();
                }
            });

        },
        saveSuggestion = function (e) {
            if ($('form#frmAddContentResource').valid()) {
                var formData = new FormData();
                var resourceFile = document.getElementById("dropContentImage").files[0];
                formData.append("resourceFile", resourceFile);
                formData.append("IsAddMode", $('#IsAddMode').val());
                formData.append("ContentName", $('#ContentName').val());
                formData.append("Description", $('#Description').val());
                formData.append("SubjectIds", $('#SubjectIds').val());
                formData.append("ContentId", $('#ContentId').val());
                formData.append("RedirectUrl", $('#RedirectUrl').val());
                $.ajax({
                    type: 'POST',
                    url: '/ContentLibrary/Resources/InsertUpdateContentResource',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result.Success == true)
                            $('#myModal').modal('hide');
                        globalFunctions.showMessage(result.NotificationType, result.Message);
                        loadContentLibraryGrid();
                    },
                    error: function (msg) {
                        globalFunctions.onFailure();
                    }
                });
                $("#myModal").modal('hide');
            }
        },
        initContentLibraryGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false
            };
            grid.init(settings);
        };

    return {
        init: init,
        addContentLibraryPopup: addContentLibraryPopup,
        editContentLibraryPopup: editContentLibraryPopup,
        saveSuggestion: saveSuggestion,
        downloadAttachment: downloadAttachment,
        changeContentLibraryStatus: changeContentLibraryStatus,
        assignContentLibrary: assignContentLibrary,
        deleteContentResourceData: deleteContentResourceData,
        loadContentLibraryGrid: loadContentLibraryGrid
    };
}();

$(document).ready(function () {
    contentProvider.init();
    $(document).on('click', '#btnAddContentLibrary', function () {
        //debugger;
        contentProvider.addContentLibraryPopup($(this));
    });
    $(document).on('click', '#btnSaveContentData', function () {
        //debugger
        contentProvider.saveSuggestion($(this));
    });
    $(document).on('click', '.content-library.download-file', function () {
        //debugger;
        var filePath = $(this).data('filepath');
        var schoolCode = $(this).data('schoolcode');
        globalFunctions.downloadSharepointFile('ResourceLibrary', filePath, schoolCode,'false','3')
    });
    
    $("#tbl-ContentProvider_filter").hide();
    $('#tbl-ContentProvider').DataTable().search('').draw();
    $("#ContentProviderSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-ContentProvider').DataTable().search($(this).val()).draw();
    });

    $("#tbl-ContentLibrary_filter").hide();
    $('#tbl-ContentLibrary').DataTable().search('').draw();
    $("#ContentLibrarySearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-ContentLibrary').DataTable().search($(this).val()).draw();
    });
});



