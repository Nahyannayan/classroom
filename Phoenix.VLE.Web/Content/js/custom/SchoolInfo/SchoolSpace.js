﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var schoolSpaces = function () {
    var init = function () {
        loaSchoolSpaceGrid();
        $("#tbl-Space_filter").hide();
    },

        loadSchoolSpacePopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.spaceName;
            globalFunctions.loadPopup(source, '/SchoolInfo/SchoolSpace/InitAddEditSchoolSpaceForm?id=' + id, title, 'subject-dailog');
            //debugger;
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                popoverEnabler.attachPopover();
            });

        },

        addSchoolSpacePopup = function (source) {
            loadSchoolSpacePopup(source, translatedResources.add);
        },

        editSchoolSpacePopup = function (source, id) {
            loadSchoolSpacePopup(source, translatedResources.edit, id);

        },

        selectIconBackground = function () {
            var icon = '#Icon_' + $("#CategoryId").val();
            $(icon).css('background-color', '#C8CCD0');
        },
        deleteSchoolSpaceData = function (source, id) {
            //debugger;
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('SchoolSpace', '/SchoolInfo/SchoolSpace/DeleteSchoolSpaceData', 'tbl-Space', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        loaSchoolSpaceGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Title", "sTitle": translatedResources.title, "sWidth": "45%", "sClass": "wordbreak" },
                //{ "mData": "Editors", "sTitle": translatedResources.editors, "sWidth": "10%" },
                { "mData": "IconCssClass", "sTitle": translatedResources.icon, "sWidth": "15%" },
                { "mData": "Resources", "sTitle": translatedResources.resources, "sWidth": "30%" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initSchoolSpaceGrid('tbl-Space', _columnData, '/SchoolInfo/SchoolSpace/LoadSchoolSpaceGrid');
        },
        selectIconClass = function (source, id) {
            $("#CategoryId").val(id);
            $(".IconCssClass").css('background-color', '#fff');
            $(source).css('background-color', '#C8CCD0');
        },
        initSchoolSpaceGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 3,
                    'orderable': false
                }]
            };
            grid.init(settings);
        };
    return {
        init: init,
        loaSchoolSpaceGrid: loaSchoolSpaceGrid,
        addSchoolSpacePopup: addSchoolSpacePopup,
        onSaveSuccess: onSaveSuccess,
        editSchoolSpacePopup: editSchoolSpacePopup,
        deleteSchoolSpaceData: deleteSchoolSpaceData,
        selectIconClass: selectIconClass,
        selectIconBackground: selectIconBackground
    };

}();


$(document).ready(function () {
    schoolSpaces.init();
    $(document).on('click', '#btnAddSchoolSpace', function () {
        schoolSpaces.addSchoolSpacePopup($(this));
        
    });

    $("#tbl-Space_filter").hide();
    $('#tbl-Space').DataTable().search('').draw();
    $("#SpaceListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-Space').DataTable().search($(this).val()).draw();
    });
});



