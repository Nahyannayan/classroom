﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var stDate, edDate;
var chartOptions;
var assignmentChartOptions;
var abuseGraphOptions;

var reports = function () {
    var _dataTableLangSettings = {
        "sEmptyTable": translatedResources.noData,
        "sInfo": translatedResources.sInfo,
        "sInfoEmpty": translatedResources.sInfoEmpty,
        "sInfoFiltered": translatedResources.sInfoFiltered,
        "sInfoPostFix": "",
        "sInfoThousands": ",",
        "sLengthMenu": translatedResources.sLengthMenu,
        "sLoadingRecords": "Loading...",
        "sProcessing": "Processing...",
        "sSearch": translatedResources.search,
        "sZeroRecords": translatedResources.sZeroRecords,
        "oPaginate": {
            "sFirst": translatedResources.first,
            "sLast": translatedResources.last,
            "sNext": translatedResources.next,
            "sPrevious": translatedResources.previous
        },
        "oAria": {
            "sSortAscending": translatedResources.sSortAscending,
            "sSortDescending": translatedResources.sSortDescending
        }
    };
    var isFilterChanged = true;
    var init = function () {
    },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },
        fillReportData = function (data) {

            $.get('/SchoolInfo/Reports/LoadAssignmentChart', function (data) {
                if (data.length > 0 && !data.every(item => item.Value === 0)) {
                    $("#AssignmentChartDiv").empty();
                    $("#AssignmentChartContainer").show();
                    //reports.bindAssignmentChart(data);

                    reports.initializeAssignmentChartOptions(data);
                    reports.createAssignmentReportChart();
                } else {
                    $("#AssignmentChartContainer").hide();
                    $("#AssignmentChartDiv").html(`<span class="no-data-found d-block mx-auto my-auto">${translatedResources.NoData}</span>`);

                }
            });

            $.get('/SchoolInfo/Reports/LoadFilterGroupReport', function (data) {
                $('#GroupReportContainer').html(data);
            });

            $.get('/SchoolInfo/Reports/LoadFilterTeacherAssignmentReport', function (data) {
                $('#TeacherAssignmentsReportContainer').html(data);
            });
            $.get('/SchoolInfo/Reports/LoadFilterStudentAssignmentReport', function (data) {
                $('#StudentAssignmentsReportContainer').html(data);
            });

            var schoolId = $("#SchoolId").val();
            var teacherId = $('#teacherListDropdown option:selected').val();
            //if (teacherId == '' || teacherId == undefined) {
            //    $.get('/SchoolInfo/Reports/LoadQuizReport', function (data) {
            //        //check data length and if all values are zero then show no record found
            //        if (data.length > 0 && !data.every(item => item.Value === 0)) {
            //            $("#QuizReportChartDiv").empty();
            //            $("#QuizReportChartContainer").show();
            //            //reports.bindQuizReportChart(data);

            //            reports.initializeQuizReportChartOptions(data);
            //            reports.createQuizReportChart();
            //        } else {
            //            $("#QuizReportChartContainer").hide();
            //            $("#QuizReportChartDiv").html('<span class="no-data-found d-block mx-auto my-auto">There is no data to display</span>');
            //        }

            //    });
            //}
            //else {
            //    $.get('/SchoolInfo/Reports/GetQuizReportDataWithDateRange', { 'userId': teacherId, 'schoolId': schoolId, 'startDate': stDate, 'endDate': edDate }, function (data) {
            //        //check data length and if all values are zero then show no record found
            //        if (data.length > 0 && !data.every(item => item.Value === 0)) {
            //            $("#QuizReportChartDiv").empty();
            //            $("#QuizReportChartContainer").show();
            //            //reports.bindQuizReportChart(data);

            //            reports.initializeQuizReportChartOptions(data);
            //            reports.createQuizReportChart();
            //        } else {
            //            $("#QuizReportChartContainer").hide();
            //            $("#QuizReportChartDiv").html('<span class="no-data-found d-block mx-auto my-auto">There is no data to display</span>');
            //        }

            //    });
            //}

        },
        fillAssignmentsByGroupReportData = function (data) {
            $.get('/SchoolInfo/Reports/LoadGroupReport', function (data) {
                $('#GroupReportContainer').html(data);
            });
        },
        //fillChattersByGroupReportData = function (data) {
        //    $.get('/SchoolInfo/Reports/LoadChatterGroupReport', function (data) {
        //        $('#ChatterGroupReportContainer').html(data);
        //    });
        //},
        fillTeacherAssignmentsReportData = function (data) {
            $.get('/SchoolInfo/Reports/LoadTeacherAssignmentReport', function (data) {
                $('#TeacherAssignmentsReportContainer').html(data);
            });
        },
        fillStudentAssignmentsReportData = function (data) {
            $.get('/SchoolInfo/Reports/LoadStudentAssignmentReport', function (data) {
                $('#StudentAssignmentsReportContainer').html(data);
            });
        },

        initiateQuizReport = function () {

            var table = $('#tbl-QuizReport').DataTable({
                "iDisplayLength": 5,
                "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
                "oLanguage": _dataTableLangSettings
            });
        },
        setStartDateEndDate = function (startDate, endDate) {
            stDate = startDate;
            edDate = endDate;
        },
        bindFirstTimeLoginsData = function (data) {
            if (data.length > 0) {
                $("#TotalLoginTillDate").html("<span class='arabictxt'>" + data[0].TotalLoginTillDate + "</span>");
                $("#TotalLogins").html(data[0].TotalLogins);
                $("#TeacherLogins").html(data[0].TeacherLogin);
                $("#StudentLogins").html(data[0].StudentLogin);
                $("#ParentLogins").html(data[0].ParentLogin);
            }
        },
        getAssignmentChartData = function (data) {
            if (data.length > 0 && !data.every(item => item.Value === 0)) {
                $("#AssignmentChartDiv").empty();
                $("#AssignmentChartContainer").show();
                //reports.bindAssignmentChart(data);

                reports.initializeAssignmentChartOptions(data);
                reports.createAssignmentReportChart();
            } else {
                $("#AssignmentChartContainer").hide();
                $("#AssignmentChartDiv").html(`<span class="no-data-found d-block mx-auto my-auto">${translatedResources.NoData}</span>`);

            }
        },

        getQuizReportChartData = function (data) {
           
            if (data.length > 0 && !data.every(item => item.Value === 0)) {
                $("#QuizReportChartDiv").empty();
                $("#QuizReportChartContainer").show();
                //reports.bindQuizReportChart(data);

                reports.initializeQuizReportChartOptions(data);
                reports.createQuizReportChart();
            } else {
                $("#QuizReportChartContainer").hide();
                $("#QuizReportChartDiv").html(`<span class="no-data-found d-block mx-auto my-auto">${translatedResources.NoData}</span>`);
            }
        },

        bindAssignmentChart = function (data) {
            $("#AssignmentChartContainer").dxPieChart({
                size: {
                    width: 400,
                    height: 350
                },
                palette: ['#fd8d3c', '#6baed6'],
                //dataSource: data,
                dataSource: $.map(data, function (item) {
                    item.Value = item.Value === 0 ? null : item.Value;
                    return item;
                }),
                legend: {
                    //title:"Assignment",
                    verticalAlignment: "bottom",
                    horizontalAlignment: "center"
                },

                series: [
                    {
                        argumentField: "Name",
                        valueField: "Value",
                        label: {
                            visible: true,
                            connector: {
                                visible: true,
                                width: 1
                            },
                            position: "inside"
                        },
                        border: {
                            visible: true,
                            color: "#FFFFFF",
                            width: 3
                        }

                    }
                ],
                onPointClick: function (e) {
                    var point = e.target;

                    toggleVisibility(point);
                },
                onLegendClick: function (e) {
                    var arg = e.target;

                    toggleVisibility(this.getAllSeries()[0].getPointsByArg(arg)[0]);
                }
            });

            function toggleVisibility(item) {
                if (item.isVisible()) {
                    item.hide();
                } else {
                    item.show();
                }
            }
        },
        bindQuizReportChart = function (data) {

            $("#QuizReportChartContainer").dxChart({
                dataSource: data,
                //title: "Group Quiz Report",
                series: {
                    argumentField: "GroupName",
                    valueField: "TotalQuiz",
                    name: "Quiz Count",
                    type: "bar",
                    color: '#ffaa66'
                },
                argumentAxis: {
                    label: {
                        overlappingBehavior: "rotate",
                        rotationAngle: -45
                    }
                },
                //argumentAxis: {
                //    label: {
                //        customizeText: function () {
                //            return this.valueText.substring(0, 2).toUpperCase() + '..';
                //        }
                //    }
                //},
                legend: {
                    visible: false
                },
                valueAxis: {
                    tickInterval: 1
                },
                tooltip: {
                    enabled: true,
                    shared: true,
                    customizeTooltip: function (info) {
                        return {
                            html: "<div class='quiz-tooltip'><span class='caption'>" + info.originalArgument + " </span> " +
                                "<div class='tooltip-body'><div class='quiz-tooltip'> <span>" + translatedResources.TotalQuiz + " </span>" +
                                info.originalValue +
                                "</div></div>"
                        };
                    }
                },
                onPointClick: function (e) {
                    e.target.select();
                    var groupId = e.target.data.GroupId;
                    //window.open("/Files/Files/SchoolGroups?grId=" + btoa(groupId) + "&startDate=" + btoa(stDate) + "&endDate=" + btoa(edDate) + "");
                },

            });
        },
        initializeQuizReportChartOptions = function (data) {
            chartOptions = {
                dataSource: data,
                commonSeriesSettings: {
                    barPadding: 0.5,
                    argumentField: "GroupName",
                    type: "bar"
                },
                onExported: e => {

                    $("#QuizReportChartContainer").dxChart('dispose');
                    reports.createQuizReportChart();
                },
                //title: "Group Quiz Report",
                series: {
                    argumentField: "GroupName",
                    valueField: "TotalQuiz",
                    name: "Quiz Count",
                    type: "bar",
                    color: '#ffaa66'
                },
                argumentAxis: {
                    label: {
                        overlappingBehavior: "rotate",
                        rotationAngle: -45
                    }
                },
                //argumentAxis: {
                //    label: {
                //        customizeText: function () {
                //            return this.valueText.substring(0, 2).toUpperCase() + '..';
                //        }
                //    }
                //},
                legend: {
                    visible: false
                },
                valueAxis: {
                    tickInterval: 1
                },
                tooltip: {
                    enabled: true,
                    shared: true,
                    customizeTooltip: function (info) {
                        return {
                            html: "<div class='quiz-tooltip'><span class='caption'>" + info.originalArgument + " </span> " +
                                "<div class='tooltip-body'><div class='quiz-tooltip'> <span>" + translatedResources.TotalQuiz + " </span>" +
                                info.originalValue +
                                "</div></div>"
                        };
                    }
                },
                onPointClick: function (e) {
                    e.target.select();
                    var groupId = e.target.data.GroupId;
                    //window.open("/Files/Files/SchoolGroups?grId=" + btoa(groupId) + "&startDate=" + btoa(stDate) + "&endDate=" + btoa(edDate) + "");
                },
                export: {
                    enabled: true,
                    printingEnabled: false,
                    formats: ["PDF"],
                    fileName: translatedResources.QuizReportGraph
                }
            }
        },

        createQuizReportChart = function (data) {
            $("#QuizReportChartContainer").dxChart(chartOptions).dxChart("instance")
        },
        initializeAssignmentChartOptions = function (data) {
            assignmentChartOptions = {
                size: {
                    width: 400,
                    height: 350
                },
                palette: ['#fd8d3c', '#6baed6'],
                //dataSource: data,
                dataSource: $.map(data, function (item) {
                    item.Value = item.Value === 0 ? null : item.Value;
                    return item;
                }),
                onExported: e => {

                    $("#AssignmentChartContainer").dxPieChart('dispose');
                    reports.createAssignmentReportChart();
                },
                legend: {
                    //title:"Assignment",
                    verticalAlignment: "bottom",
                    horizontalAlignment: "center"
                },
                series: [
                    {
                        argumentField: "Name",
                        valueField: "Value",
                        label: {
                            visible: true,
                            connector: {
                                visible: true,
                                width: 1
                            },
                            position: "inside"
                        },
                        border: {
                            visible: true,
                            color: "#FFFFFF",
                            width: 3
                        }

                    }
                ],
                onPointClick: function (e) {
                    var point = e.target;

                    toggleVisibility(point);
                },
                onLegendClick: function (e) {
                    var arg = e.target;

                    toggleVisibility(this.getAllSeries()[0].getPointsByArg(arg)[0]);
                },
                export: {
                    enabled: true,
                    printingEnabled: false,
                    formats: ["PDF"],
                    fileName: translatedResources.Assignments
                }
            }
        },
        createAssignmentReportChart = function () {
            $("#AssignmentChartContainer").dxPieChart(assignmentChartOptions).dxPieChart("instance")
        },

        loadAssignmentsByGroupData = function (schoolId, startDate, endDate, userId, groupId) {
            stDate = startDate;
            edDate = endDate;
            var reportObject = {
                SchoolId: schoolId,
                StartDate: startDate,
                EndDate: endDate,
                UserId: userId,
                GroupId: groupId
            };
            $.ajax({
                type: "POST",
                url: "/SchoolInfo/Reports/LoadGroupAssignmentReportFilteredData",
                data: reportObject,
                success: function (data) {
                    reports.fillAssignmentsByGroupReportData(data);
                },
                error: function () {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        loadChattersByGroupData = function (schoolId, startDate, endDate, userId, groupId) {
            stDate = startDate;
            edDate = endDate;
            var reportObject = {
                SchoolId: schoolId,
                StartDate: startDate,
                EndDate: endDate,
                UserId: userId,
                GroupId: groupId
            };
            $.ajax({
                type: "POST",
                url: "/SchoolInfo/Reports/LoadGroupChatterReportFilteredData",
                data: reportObject,
                success: function (data) {
                    reports.fillChattersByGroupReportData(data);
                },
                error: function () {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        loadTeacherAssignmentsReportData = function (schoolId, startDate, endDate, userId, groupId) {
            stDate = startDate;
            edDate = endDate;
            var reportObject = {
                SchoolId: schoolId,
                StartDate: startDate,
                EndDate: endDate,
                UserId: userId,
                GroupId: groupId
            };
            $.ajax({
                type: "POST",
                url: "/SchoolInfo/Reports/LoadTeacherAssignmentReportFilteredData",
                data: reportObject,
                success: function (data) {
                    reports.fillTeacherAssignmentsReportData(data);
                },
                error: function () {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        loadStudentAssignmentsReportData = function (schoolId, startDate, endDate, userId, groupId) {
            stDate = startDate;
            edDate = endDate;
            var reportObject = {
                SchoolId: schoolId,
                StartDate: startDate,
                EndDate: endDate,
                UserId: userId,
                GroupId: groupId
            };
            $.ajax({
                type: "POST",
                url: "/SchoolInfo/Reports/LoadStudentAssignmentReportFilteredData",
                data: reportObject,
                success: function (data) {
                    reports.fillStudentAssignmentsReportData(data);
                },
                error: function () {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },

        loadDashBoard = function (schoolId, startDate, endDate, userId, groupId) {
            stDate = startDate;
            edDate = endDate;
            var reportObject = {
                SchoolId: schoolId,
                StartDate: startDate,
                EndDate: endDate,
                UserId: userId,
                GroupId: groupId
            };
            $.ajax({
                type: "POST",
                //url: "/SchoolInfo/Reports/ReportsData",
                url: "/SchoolInfo/Reports/AssignmentReportsData",
                data: reportObject,
                success: function (data) {
                    reports.fillReportData(data);
                },
                error: function () {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },

        getReportFilters = function () {
            return {
                startDate: globalFunctions.toSystemReadableDate( $("#FromDate").val()),
                endDate: globalFunctions.toSystemReadableDate($("#ToDate").val()),
                schoolId: $("#SchoolId").val(),
                userId: $("#UserId").val()
            }
        },
        loadQuizReportSection = function (userId,startDate, endDate) {

            var schoolId = $("#SchoolId").val();
            var teacherId = $('#teacherListDropdown option:selected').val();
            if (teacherId == '' || teacherId == undefined) {
                $.get('/SchoolInfo/Reports/GetQuizReportDataWithDateRange', { 'userId': userId, 'schoolId': schoolId, 'startDate': startDate, 'endDate': endDate }, function (data) {
                    //check data length and if all values are zero then show no record found

                    if (data.length > 0 && !data.every(item => item.Value === 0)) {
                        $("#QuizReportChartDiv").empty();
                        $("#QuizReportChartContainer").show();

                        reports.initializeQuizReportChartOptions(data);
                        reports.createQuizReportChart();
                    } else {
                        $("#QuizReportChartContainer").hide();
                        $("#QuizReportChartDiv").html(`<span class="no-data-found d-block mx-auto my-auto">${translatedResources.NoData}</span>`);
                    }

                });
            }
            else {
                $.get('/SchoolInfo/Reports/GetQuizReportDataWithDateRange', { 'userId': teacherId, 'schoolId': schoolId, 'startDate': startDate, 'endDate': endDate }, function (data) {
                    //check data length and if all values are zero then show no record found

                    if (data.length > 0 && !data.every(item => item.Value === 0)) {
                        $("#QuizReportChartDiv").empty();
                        $("#QuizReportChartContainer").show();

                        reports.initializeQuizReportChartOptions(data);
                        reports.createQuizReportChart();
                    } else {
                        $("#QuizReportChartContainer").hide();
                        $("#QuizReportChartDiv").html(`<span class="no-data-found d-block mx-auto my-auto">${translatedResources.NoData}</span>`);
                    }

                });
            }
        },

        loadSections = function (source) {
            $(".admin-section.section-container").removeClass("active");
            //$(source).addClass("active");
            var sectionInfo = $(source).data();
            if (isFilterChanged || $(sectionInfo.datacontainer).is(":empty")) {
                $.ajax({
                    url: sectionInfo.url,
                    data: getReportFilters(),
                    success: function (result) {
                        if (sectionInfo.section == "quiz") {
                            $("#quiz-card").addClass("btn-primary");
                            $("#logins-data-card").removeClass("btn-primary");
                            $("#assignment-card").removeClass("btn-primary");
                            $("#quiz-card").addClass("btn-primary");
                            $(".quiz-section").removeClass("d-none");
                            $(".quiz-section").addClass("d-block");
                            $(".assignment-section").addClass("d-none");
                            $(".login-section").removeClass("d-block");
                            $(".login-section").addClass("d-none");

                            if (result.length > 0 && !result.every(item => item.Value === 0)) {
                                $("#QuizReportChartDiv").empty();
                                $("#QuizReportChartContainer").show();
                                initializeQuizReportChartOptions(result);
                                createQuizReportChart();
                            } else {
                                $("#QuizReportChartContainer").hide();
                                $("#QuizReportChartDiv").html('<span class="no-data-found d-block mx-auto my-auto"></span>');
                            }
                        } else {
                            $("#logins-data-card").addClass("btn-primary");
                            $("#quiz-card").removeClass("btn-primary");
                            $("#assignment-card").removeClass("btn-primary");
                            $(".login-section").removeClass("d-none");
                            $(".login-section").addClass("d-block");
                            $(".quiz-section").removeClass("d-block");
                            $(".quiz-section").addClass("d-none");
                            $(".assignment-section").addClass("d-none");

                            $(sectionInfo.container).html(result);
                        }
                            
                    },
                    error: function () {
                        globalFunctions.onFailure();
                    }
                })
            }
            else {
                $(sectionInfo.container).addClass("d-none");
            }
        }
    return {
        init: init,
        loadDashBoard: loadDashBoard,
        loadQuizReportSection: loadQuizReportSection,
        loadAssignmentsByGroupData: loadAssignmentsByGroupData,
        loadTeacherAssignmentsReportData: loadTeacherAssignmentsReportData,
        loadStudentAssignmentsReportData: loadStudentAssignmentsReportData,
        setStartDateEndDate: setStartDateEndDate,
        bindFirstTimeLoginsData: bindFirstTimeLoginsData,
        initiateQuizReport: initiateQuizReport,
        getAssignmentChartData: getAssignmentChartData,
        getQuizReportChartData: getQuizReportChartData,
        bindQuizReportChart: bindQuizReportChart,
        onSaveSuccess: onSaveSuccess,
        fillReportData: fillReportData,
        fillAssignmentsByGroupReportData: fillAssignmentsByGroupReportData,
        fillTeacherAssignmentsReportData: fillTeacherAssignmentsReportData,
        fillStudentAssignmentsReportData: fillStudentAssignmentsReportData,
        initializeQuizReportChartOptions: initializeQuizReportChartOptions,
        createQuizReportChart: createQuizReportChart,
        initializeAssignmentChartOptions: initializeAssignmentChartOptions,
        createAssignmentReportChart: createAssignmentReportChart,
        bindAssignmentChart: bindAssignmentChart,
        loadSections: loadSections
    };
}();

$(document).ready(function () {

    // to initiate quiz report
    //reports.initiateQuizReport();

    $(document).on('click', '#assignment-card', function () {

        $("#assignment-card").addClass("btn-primary");
        $("#logins-data-card").removeClass("btn-primary");
        $("#quiz-card").removeClass("btn-primary");
        $(".assignment-section").removeClass("d-none");
        $(".login-section").removeClass("d-block");
        $(".login-section").addClass("d-none");
        $(".quiz-section").removeClass("d-block");
        $(".quiz-section").addClass("d-none");
       

        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        var schoolId = $("#SchoolId").val();

        $('#GroupReportContainer').empty();
        $('#TeacherAssignmentsReportContainer').empty();
        $('#StudentAssignmentsReportContainer').empty();

        reports.loadDashBoard(schoolId, fromDate, toDate, userId, null);
    });

    $(document).on('click', '#quiz-card', function () {

        $("#quiz-card").addClass("btn-primary");
        $("#logins-data-card").removeClass("btn-primary");
        $("#assignment-card").removeClass("btn-primary");
        $(".quiz-section").removeClass("d-none");
        $(".assignment-section").addClass("d-none");
        $(".assignment-section").removeClass("d-block");
        $(".login-section").addClass("d-none");
        $(".login-section").removeClass("d-block");


        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        //var schoolId = $("#SchoolId").val();

        $('#GroupReportContainer').empty();
        $('#TeacherAssignmentsReportContainer').empty();
        $('#StudentAssignmentsReportContainer').empty();

        reports.loadQuizReportSection(userId,fromDate, toDate);
    });

    //$(document).on('click', '#btnFilter', function () {
    //    var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
    //    var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
    //    var userId = $("#UserId").val();
    //    var schoolId = $("#SchoolId").val();

    //    $('#GroupReportContainer').empty();
    //    $('#TeacherAssignmentsReportContainer').empty();
    //    $('#StudentAssignmentsReportContainer').empty();

    //    reports.loadDashBoard(schoolId, fromDate, toDate, userId, null);
    //});

    $(document).on('click', '#btnFilterGroupAssignmentData', function () {
        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        var schoolId = $("#SchoolId").val();

        $('#GroupReportContainer').empty();

        reports.loadAssignmentsByGroupData(schoolId, fromDate, toDate, userId, null);
    });

    $(document).on('click', '#btnFilterGroupChattersData', function () {
        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        var schoolId = $("#SchoolId").val();

        $('#ChatterGroupReportContainer').empty();

        reports.loadChattersByGroupData(schoolId, fromDate, toDate, userId, null);
    });

    $(document).on('click', '#btnFilterTeacherAssignmentsData', function () {
        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        var schoolId = $("#SchoolId").val();

        $('#TeacherAssignmentsReportContainer').empty();

        reports.loadTeacherAssignmentsReportData(schoolId, fromDate, toDate, userId, null);
    });

    $(document).on('click', '#btnFilterStudentAssignmentsData', function () {
        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        var schoolId = $("#SchoolId").val();

        $('#StudentAssignmentsReportContainer').empty();

        reports.loadStudentAssignmentsReportData(schoolId, fromDate, toDate, userId, null);
    });

    $(document).on('click', '#btnFilterAbuseReportData', function () {
        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        var schoolId = $("#SchoolId").val();

        $('#AbuseReportContainer').empty();

        reports.loadSchoolAbuseReportData(schoolId, fromDate, toDate, userId, null);
    });

    $(document).on('click', '.login-count', function () {
        var loginType = $(this).data("type");
        window.open("/Admin/Users?loginType=" + btoa(loginType) + "&startDate=" + btoa(stDate) + "&endDate=" + btoa(edDate) + "");
    });

    $(document).on('click', '.group-name-label', function () {
        var groupId = $(this).data("groupid");
        window.open("/Assignments/Assignment/GroupAssignments?startDate=" + btoa(stDate) + "&endDate=" + btoa(edDate) + "&groupId=" + groupId + "");
    });
    $(document).on('click', '.chatter-group-name-label', function () {
        var groupId = $(this).data("groupid");
        window.open("/SchoolInfo/Blog/Chatters?grpId=" + groupId + "");
    });

    $(document).on('click', '.assignment-by-teacher-label', function () {
        var teacherId = $(this).data("teacherid");
        var groupId = "";
        window.open("/Assignments/Assignment/TeacherGroupAssignments?startDate=" + btoa(stDate) + "&endDate=" + btoa(edDate) + "&teacherId=" + teacherId + "");
    });

    $(document).on('click', '.assignment-by-student-label', function () {
        var studentId = $(this).data("studentid");
        window.open("/Assignments/Assignment/StudentGroupAssignments?startDate=" + btoa(stDate) + "&endDate=" + btoa(edDate) + "&studentId=" + studentId + "");
    });

    $(document).on('click', 'td.details-control', function () {

        var schoolId = $("#SchoolId").val();
        var teacherId = $(this).parent().find('input[type="hidden"]').val();

        $.ajax({
            type: 'GET',
            url: '/SchoolInfo/Reports/GetChildQuizReportData',
            data: {
                'schoolId': schoolId,
                'startDate': globalFunctions.toSystemReadableDate( $("#FromDate").val()),
                'endDate': globalFunctions.toSystemReadableDate($("#ToDate").val()),
                'userId': teacherId
            },
            success: function (response) {

                $('#childQuizReportModalView').empty();
                $('#childQuizReportModalView').html(response).find('.modal').modal({
                    show: true
                });

                $('#tbl-child-quiz-report').DataTable({
                    "iDisplayLength": 5,
                    "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
                    "oLanguage": _dataTableLangSettings
                });
            },
            error: function () {
            },
            beforeSend: function () {
            },
            complete: function () {
            }
        });
    });

    $(document).on('click', '.quiz-report-label', function () {
        var quizId = $(this).data("quizid");
        var groupId = $(this).data("groupid");
        var isForm = false;
        window.open("/Files/Files/GroupStudentDetails?id=" + quizId + "&groupId=" + groupId + "&isForm=" + isForm + "");
    });

    $('#teacherListDropdown').on('change', function (event) {

        var schoolId = $("#SchoolId").val();
        var teacherId = $('#teacherListDropdown option:selected').val();

        if (teacherId != '' && teacherId != undefined) {
            $.ajax({
                type: 'GET',
                url: '/SchoolInfo/Reports/GetQuizReportDataWithDateRange',
                data: {
                    'userId': teacherId,
                    'schoolId': schoolId,
                    'startDate': globalFunctions.toSystemReadableDate( $("#FromDate").val()),
                    'endDate': globalFunctions.toSystemReadableDate($("#ToDate").val())
                },
                success: function (result) {

                    if (result.length > 0 && !result.every(item => item.Value === 0)) {
                        $("#QuizReportChartDiv").empty();
                        $("#QuizReportChartContainer").show();
                        //reports.bindQuizReportChart(result);

                        reports.initializeQuizReportChartOptions(result);
                        reports.createQuizReportChart();
                    } else {
                        $("#QuizReportChartContainer").hide();
                        $("#QuizReportChartDiv").html('<span class="no-data-found d-block mx-auto my-auto"></span>');
                    }
                },
                error: function (data) { }
            });
        }
        else {
            $("#QuizReportChartContainer").hide();
            $("#QuizReportChartDiv").html('<span class="no-data-found d-block mx-auto my-auto"></span>');
        }
    });

    $(document).on('click', '#btnGroupAssignmentsLoadMore', function () {
        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        var schoolId = $("#SchoolId").val();

        window.open("/SchoolInfo/Reports/GroupAssignmentReport?fromDate=" + btoa(fromDate) + "&toDate=" + btoa(toDate) + "&userId=" + btoa(userId) + "&schoolId=" + btoa(schoolId));
    });

    $(document).on('click', '#btnGroupChattersLoadMore', function () {
        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        var schoolId = $("#SchoolId").val();

        window.open("/SchoolInfo/Reports/GroupChatterReport?fromDate=" + btoa(fromDate) + "&toDate=" + btoa(toDate) + "&userId=" + btoa(userId) + "&schoolId=" + btoa(schoolId));
    });

    $(document).on('click', '#btnTeacherAssignmentsLoadMore', function () {
        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        var schoolId = $("#SchoolId").val();

        window.open("/SchoolInfo/Reports/TeacherAssignmentReport?fromDate=" + btoa(fromDate) + "&toDate=" + btoa(toDate) + "&userId=" + btoa(userId) + "&schoolId=" + btoa(schoolId));
    });

    $(document).on('click', '#btnStudentAssignmentsLoadMore', function () {
        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        var schoolId = $("#SchoolId").val();

        window.open("/SchoolInfo/Reports/StudentAssignmentReport?fromDate=" + btoa(fromDate) + "&toDate=" + btoa(toDate) + "&userId=" + btoa(userId) + "&schoolId=" + btoa(schoolId));
    });

    $(document).on('click', '#btnAbuseReportDataLoadMore', function () {
        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var userId = $("#UserId").val();
        var schoolId = $("#SchoolId").val();

        window.open("/SchoolInfo/Reports/AbuseReport?fromDate=" + btoa(fromDate) + "&toDate=" + btoa(toDate) + "&userId=" + btoa(userId) + "&schoolId=" + btoa(schoolId));
    });


});