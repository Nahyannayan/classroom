﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var stDate, edDate, stDateLive, edDateLive;
var _dataTableLangSettings = {
    "sEmptyTable": translatedResources.noData,
    "sInfo": translatedResources.sInfo,
    "sInfoEmpty": translatedResources.sInfoEmpty,
    "sInfoFiltered": translatedResources.sInfoFiltered,
    "sInfoPostFix": "",
    "sInfoThousands": ",",
    "sLengthMenu": translatedResources.sLengthMenu,
    "sLoadingRecords": "Loading...",
    "sProcessing": "Processing...",
    "sSearch": translatedResources.search,
    "sZeroRecords": translatedResources.sZeroRecords,
    "oPaginate": {
        "sFirst": translatedResources.first,
        "sLast": translatedResources.last,
        "sNext": translatedResources.next,
        "sPrevious": translatedResources.previous
    },
    "oAria": {
        "sSortAscending": translatedResources.sSortAscending,
        "sSortDescending": translatedResources.sSortDescending
    }
};

var eventsReport = function () {
   
    var init = function () {
    },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },
        loadPlannerDetailsPopup = function (source, id) {
            var currentDate = new Date();
         
            var title = translatedResources.planner;
            globalFunctions.loadPopup(source, '/Planner/MyPlanner/LoadPlannerDetails?id=' + id + "&timeOffset=" + currentDate.getTimezoneOffset(), title, 'myPlanner-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                //$(document).on('click', '#eventAttachmentViewer', function () {
                //    var fileName = $('#ResourceFile').val();
                //    planner.downloadAttachment(fileName);
                //});
                //$(document).on('click', '#editPlanner', function () {
                //    var event = $('#EventId').val();
                //    var id = btoa(event);
                //    planner.editPlanner($(this), id);
                //});

                $('#lightgallery').lightGallery({
                    selector: '.galleryImg',
                    share: false
                });

                $(document).on('click', '#ahrefDeleteEventFile', function () {
                    globalFunctions.notyConfirm($(this), translatedResources.deleteConfirm);

                    var eventId = $(this).attr('data-EventId');

                    $(document).bind('okClicked', $(this), function (e) {
                        $.post('/Planner/MyPlanner/DeleteEventFile', { eventId: eventId }, function (response) {
                            $('.resource-data').addClass('d-none');
                        });
                    });
                });

            });
        },
        fillTimetableEventsReportData = function (data) {
            $.get('/SchoolInfo/Reports/LoadTimetableEventsReport', function (data) {
                $('#TimetableEventsReportContainer').html(data);
                    $("#tbl-timetable-events").DataTable({
                        "iDisplayLength": 10,
                        "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
                        "oLanguage": _dataTableLangSettings
                    });

                    $("#tbl-timetable-events_length").hide();
                    $("#tbl-timetable-events_filter").hide();
                    $("#TimetableEventsSearch").on("input", function (e) {
                        e.preventDefault();
                        $('#tbl-timetable-events').DataTable().search($(this).val()).draw();
                    });
            });
        },
        fillLiveSessionsReportData = function (data) {
            $.get('/SchoolInfo/Reports/LoadLiveSessionsReport', function (data) {
                $('#LiveSessionsReportContainer').html(data);
                var LiveSessionsDataTable = $("#tbl-liveSession-events").DataTable({
                    "iDisplayLength": 10,
                    "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
                    "initComplete": function (settings, json) {
                        $("#liveSessions-filter-section").removeClass("d-none");
                    },
                    "oLanguage": _dataTableLangSettings
                });

                $("#tbl-liveSession-events_length").hide();
                $("#tbl-liveSession-events_filter").hide();
                $("#LiveSessionsSearch").on("input", function (e) {
                    e.preventDefault();
                    $('#tbl-liveSession-events').DataTable().search($(this).val()).draw();
                });

                if (!LiveSessionsDataTable.data().any()) {
                    $("#liveSessions-filter-section").removeClass("d-none");
                }
            });
        },
        loadTimetableEventsReportData = function (teacherIds, schoolId, groupIds, startDate, endDate) {
            stDate = startDate;
            edDate = endDate;
            var reportObject = {
                TeacherIds: teacherIds.toString(),
                SchoolId: schoolId,
                GroupIds: groupIds.toString(),
                StartDate: startDate,
                EndDate: endDate
            };
            $.ajax({
                type: "POST",
                url: "/SchoolInfo/Reports/LoadTimetableEventsReportFilteredData",
                data: reportObject,
                success: function (data) {
                    eventsReport.fillTimetableEventsReportData(data);
                },
                error: function () {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },

        loadLiveSessionsReportData = function (teacherIds, schoolId, groupIds, startDate, endDate) {
            stDateLive = startDate;
            edDateLive = endDate;
            var reportObject = {
                TeacherIds: teacherIds.toString(),
                SchoolId: schoolId,
                GroupIds: groupIds.toString(),
                StartDate: startDate,
                EndDate: endDate
            };
            $.ajax({
                type: "POST",
                url: "/SchoolInfo/Reports/LoadLiveSessionsReportFilteredData",
                data: reportObject,
                success: function (data) {
                    eventsReport.fillLiveSessionsReportData(data);
                },
                error: function () {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        setStartDateEndDateLive = function (startDate, endDate) {
            stDateLive = startDate;
            edDateLive = endDate;
        },
        setStartDateEndDate = function (startDate, endDate) {
            stDate = startDate;
            edDate = endDate;
        }
      
       
    return {
        init: init,
        setStartDateEndDate: setStartDateEndDate,
        setStartDateEndDateLive: setStartDateEndDateLive,
        loadTimetableEventsReportData: loadTimetableEventsReportData,
        fillTimetableEventsReportData: fillTimetableEventsReportData,
        loadLiveSessionsReportData: loadLiveSessionsReportData,
        fillLiveSessionsReportData: fillLiveSessionsReportData,
        loadPlannerDetailsPopup: loadPlannerDetailsPopup

    };
}();

$(document).ready(function () {

    $('#schoolGroupListDropdown').on('change', function (event) {
        var fromDate = globalFunctions.toSystemReadableDate($("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var teacherIds = $('#teacherListDropdown').val();
        var schoolId = $("#SchoolId").val();
        var groupIds = $('#schoolGroupListDropdown').val();

        $('#TimetableEventsReportContainer').empty();

        eventsReport.loadTimetableEventsReportData(teacherIds, schoolId, groupIds, fromDate, toDate);


    });

    $('#teacherListDropdown').on('change', function (event) {
        var fromDate = globalFunctions.toSystemReadableDate($("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var teacherIds = $('#teacherListDropdown').val(); 
        var schoolId = $("#SchoolId").val();
        var groupIds = $('#schoolGroupListDropdown').val();

        $('#TimetableEventsReportContainer').empty();
        
        eventsReport.loadTimetableEventsReportData(teacherIds, schoolId, groupIds, fromDate, toDate);
       
        
    });

    $(document).on('click', '#btnFilterTimetableEvents', function () {
       
        var fromDate = globalFunctions.toSystemReadableDate($("#FromDate").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDate").val());
        var teacherIds = $('#teacherListDropdown').val();
        var schoolId = $("#SchoolId").val();
        var groupIds = $('#schoolGroupListDropdown').val();

        $('#TimetableEventsReportContainer').empty();
       
        eventsReport.loadTimetableEventsReportData(teacherIds, schoolId, groupIds, fromDate, toDate);
        

    });

    $('#schoolGroupsDropdownLive').on('change', function (event) {
        var fromDate = globalFunctions.toSystemReadableDate($("#FromDateLive").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDateLive").val());
        var teacherIds = $('#teachersDropdownLive').val();
        var schoolId = $("#SchoolId").val();
        var groupIds = $('#schoolGroupsDropdownLive').val();

        $('#LiveSessionsReportContainer').empty();

        eventsReport.loadLiveSessionsReportData(teacherIds, schoolId, groupIds, fromDate, toDate);

    });

    $('#teachersDropdownLive').on('change', function (event) {
        var fromDate = globalFunctions.toSystemReadableDate( $("#FromDateLive").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDateLive").val());
        var teacherIds = $('#teachersDropdownLive').val();
        var schoolId = $("#SchoolId").val();
        var groupIds = $('#schoolGroupsDropdownLive').val();

        $('#LiveSessionsReportContainer').empty();

        eventsReport.loadLiveSessionsReportData(teacherIds, schoolId, groupIds, fromDate, toDate);

    });

    $(document).on('click', '#btnFilterLiveSessions', function () {

        var fromDate = globalFunctions.toSystemReadableDate($("#FromDateLive").val());
        var toDate = globalFunctions.toSystemReadableDate($("#ToDateLive").val());
        var teacherIds = $('#teachersDropdownLive').val();
        var schoolId = $("#SchoolId").val();
        var groupIds = $('#schoolGroupsDropdownLive').val();

        $('#LiveSessionsReportContainer').empty();

        eventsReport.loadLiveSessionsReportData(teacherIds, schoolId, groupIds, fromDate, toDate);

    });

    $('body').on('click', '#tab-liveSessions', function () {

        var fromDate = globalFunctions.toSystemReadableDate($("#FromDateLive").val());
        var toDate = globalFunctions.toSystemReadableDate( $("#ToDateLive").val());
        var teacherIds = $('#teachersDropdownLive').val();
        var schoolId = $("#SchoolId").val();
        var groupIds = $('#schoolGroupsDropdownLive').val();

        $('#LiveSessionsReportContainer').empty();

        eventsReport.loadLiveSessionsReportData(teacherIds, schoolId, groupIds, fromDate, toDate);

    });

    $(document).on('click', '.live-sessions-label', function () {
        var eventId = $(this).data("eventid");
        eventsReport.loadPlannerDetailsPopup($(this), eventId);
    });


});