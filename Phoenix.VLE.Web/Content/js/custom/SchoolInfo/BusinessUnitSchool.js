﻿var schoolFunctions = function () {
    var initPagination = function (sourceId) {
        var grid = new DynamicPagination("divBusinessSchoolUnit");
        var settings = {
            url: '/SchoolInfo/Schools/GetAllSchools?pageIndex=' + $("#PageIndexNumber").val() + "&searchString=" + $(".search-school.search-field").val()
        };
        grid.init(settings);
    }

    toggleClassRoomStatus = function (source, schoolId) {
        var confirmMessage = $(source).is(":checked") ? translatedResources.activeConfirmMsg : translatedResources.deactiveConfirmMsg;
        globalFunctions.notyConfirm($(source), confirmMessage);
        $(source).off("okClicked");
        $(source).on("okClicked", function () {
            $.ajax({
                url: "/SchoolInfo/Schools/UpdateSchoolClassroomStatus",
                data: { schoolId: schoolId },
                type: "POST",
                success: function (data) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                    if (!data.Success)
                        $(source).prop("checked", !$(source).prop("checked"));
                    refreshToggleButtons();
                },
                error: function () {
                    globalFunctions.onFailure();
                }
            });
        });

        $(source).off("cancelClicked");
        $(source).on("cancelClicked", function () {
            $(source).prop("checked", !$(this).prop("checked"));
            refreshToggleButtons();
        });
    },

        refreshToggleButtons = function () {
            $("input[type=checkbox].chk-bootstrap-toggle").bootstrapToggle("destroy").bootstrapToggle();
        };

    return {
        initPagination: initPagination,
        toggleClassRoomStatus: toggleClassRoomStatus
    }
}();

$(function () {
    schoolFunctions.initPagination();

    $(document).on('keyup', "input.search-school.search-field", function (e) {
        e.stopPropagation();
        if (e.keyCode === 13) {
            $("#PageIndexNumber").val('1');
            schoolFunctions.initPagination();
        }
    });

    $(document).on("click", "#btnSchoolSearch", function () {
        $("#PageIndexNumber").val('1');
        schoolFunctions.initPagination();
    });
});
