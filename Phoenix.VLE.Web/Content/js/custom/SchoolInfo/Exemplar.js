﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var MyExemplarWork = 1;
var exemplar = function () {
    var init = function () {

        if (translatedResources.IsAdmin == "True") {
            InitializeCheckBox();
            loadExemplarGrid();
        }
        loadPageControlEvent();

        if (translatedResources.IsAdmin == "False") {
            ReloadPostOnSaveandUpdate();
            LoadGroups();
        }
        $("#TaggedStudent").title = translatedResources.SelectStudent;
        $("#tag-students-add").title = translatedResources.SelectStudent;
       
        
    },
        loadExemplarGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "PostName", "sTitle": translatedResources.title, "sWidth": "60%" },
                { "mData": "Status", "sClass": "text-left text-sm-center no-sort", "sTitle": translatedResources.status, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-left text-sm-center no-sort", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            LoadPendingPost('tbl-ExemplarPost', _columnData, '/SchoolInfo/ExemplarWall/GetPendingForApprovalPost');
        },
        LoadPendingPost = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [2]
                }],
                searching: true
            };
            grid.init(settings);

            $("#" + _tableId + "_filter").hide();
            $('#' + _tableId).DataTable().search('').draw();
            $("#ExemplarPostSearch").on("input", function (e) {
                e.preventDefault();
                $('#' + _tableId).DataTable().search($(this).val()).draw();
            });
        },
        loadPageControlEvent = function () {
            postGroupToggle();
           

        },
        onGroupSelect = function () {
           
            //if (globalFunctions.isValueValid($('#ddlGroup').val())) {
                clearForm();
                $("#GroupId").val($('#ddlGroup').val());
               
                loadBlogListForGroup();
                $(".blog-editor").hide();

           // }
        },
        loadBlogListForGroup = function () {

            $.ajax({
                url: "/SchoolInfo/ExemplarWall/GetExemplarListByGroupId?SchoolLevelId=" + $('#SchoolLevel').val() + "&CourseId=" + $('#ddlcourse').val() + "&GroupId=" + $("#GroupId").val() + "&MyExemplarWork=" + MyExemplarWork + "&SearchText=" + SearchText + "&DepartmentId=" + $("#ddldepartment").val(),
                async: false,
                success: function (result) {
                    //  
                    if (result !== '') {
                        $(".wall-carousel").owlCarousel('destroy');
                        $('.wall-carousel').empty();
                        $("#exemplarList").html('');
                        $("#exemplarList").html(result);
                        OwlCarousel();
                        loadPageControlEvent();
                        initGrid();
                    }
                    else {
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                },
                error: function (msg) {
                   
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        onSchoolLevelSelect = function () {
            //$("#ddldepartment").empty();
           
            FillGroup();
            loadBlogListForGroup();
            return false;
        },
        onCourseChanged = function () {
           // $("#ddldepartment").empty();
            $("#GroupId").val(0);
            
            FillGroup();
            loadBlogListForGroup();
            return false;
        },
        onDepartmentChanged = function () {
            //$("#ddldepartment").empty();
            //$("#GroupId").val(0);

            //FillGroup();
            loadBlogListForGroup();
            return false;
        },
        onShowMessage = function (message) {
           
            globalFunctions.showErrorMessage(message);
            loadPageControlEvent();
        },
        clearForm = function () {
            $("#BlogId").val("");
            $("#IsAddMode").val("");
            $("#Title").val("");
            $("#Description").val("");
            $("#BlogImage").val("");
            $("#EmbedUrl").val("");
            $('#ExemplarImage').fileinput('clear');
            $("#ExemplarWallModel_SchoolId").val("");

            $('#ExemplarWallModel_BlogTypeId').val("");
            $('#GroupId').val("");

            $("#EmbededVideoLink").val("");
            $("#PostTitle").val();
            $("#AdditionalDocPath").val("");
            $("#PostDescription").val("");
            $("#ExemplarWallModel_IsPublish").val("");

            $("#ExemplarWallModel_CreatedBy").val("");
            $("#ExemplarWallModel_CreatedOn").val("");
            $("#ExemplarWallModel_SortOrder").val("");

            $("#ExemplarImage").fileinput('clear');
            $('input[name=__RequestVerificationToken]').val("");

            $("#ddlgroupAdd").val("");
            $("#tag-students-add").val("");
            $("#IsDepartmentWall_Add").val("");
            $("#IsCourseWall_Add").val("");
            $("#SchoolLevelId_Add").val("");
            $("#ReferenceLink").val("");


        },
        loadAddEditPopup = function (source, id) {
            var title = id === undefined ? translatedResources.CreateExemplary : translatedResources.UpdateExemplary;
           
            globalFunctions.loadPopup(source, '/SchoolInfo/ExemplarWall/AddExemplarForm?id=' + id, title, 'folder-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
              
              //  LoadGroups();
                $(".selectpicker").selectpicker({ noneSelectedText: translatedResources.select, refresh: "refresh" });
                RefreshKrajerr();
                var Gstring = $("#TempGroupList").val();
                var array = Gstring.split(',');
                $('select[name=GroupsList]').val(array);
                $('.selectpicker').selectpicker('refresh');
                $('#TaggedGroup').val(array);
                $('#TaggedGroup').selectpicker('refresh');
                if (FillStudentOnUpdate.length>0)
                FillStudentOnUpdate(Gstring);
               
                var Sstring = $("#TempStudentList").val();
                var Sarray = Sstring.split(',');
                $('select[name=TaggedStudent]').val(Sarray);
                $('.selectpicker').selectpicker('refresh');
                $('#TaggedStudent').selectpicker('refresh');
            });
        },

        editExemplar = function (id) {
        },
        createExemplar = function () {
        },
        onSaveExemplar = function (e) {

            if ($('form#frmAddExemplarWall').valid()) {
               
                var exemplarWallId = $("#ExemplarWallId").val();
                var schoolId = $("#SchoolId").val();
                var isAddMode = $("#IsAddMode").val();
                var blogTypeId = $('#BlogTypeId').val();
                var groupId = $('#GroupId').val();

                var embedUrl = $("#EmbededVideoLink").val();
                var title = $("#PostTitle").val();
                var OldImage = $("#AdditionalDocPathHDN").val();
                var description = $("#PostDescription").val();
                var publish = $("#IsPublish").val();

                var createdBy = $("#CreatedBy").val();
                var createOn = $("#CreatedOn").val();
                var sortOrder = $("#SortOrder").val(); 3
                
                var imageFile = $('#ExemplarImageUpdate').fileinput('getFileList');
                
                var token = $('input[name=__RequestVerificationToken]').val();

                var taggedGroup = $("#TaggedGroup").val();
                var taggedStudent = $("#TaggedStudent").val();
                var isDepartmentWall = $("#IsDepartmentWall").prop("checked");
                var isCourseWall = $("#IsCourseWall").prop("checked");
                var schoolLevelId = $("#SchoolLevelId").val();
                var referenceLink = $("#ReferenceLink").val();

                var FileNames = $("#FileNames").val();
                var ParentSharableLink = $("#ParentSharableLink").val();
                if (embedUrl !== '') {
                    var reg = new RegExp("^(?:<iframe[^>]*)(?:(?:\/>)|(?:>.*?<\/iframe>))");
                    
                    if (reg.test(embedUrl) === false) {
                        exemplar.onShowMessage(translatedResources.embedUrlMessage);
                        $("#createWorkPost").show();
                        return;
                    }
                }
                
                if (referenceLink !== '') {
                    var reg = new RegExp("^((ftp|http|https):\/\/)?www\.([A-z]+)\.([A-z]{2,})");
                   
                    if (reg.test(referenceLink) === false) {
                        exemplar.onShowMessage(translatedResources.UrlMessage);
                        $("#createWorkPost").show();
                        return;
                    }
                }

                var formData = new FormData();
                formData.append("__RequestVerificationToken", token);
                formData.append("ExemplarWallId", exemplarWallId);
                formData.append("SchoolId", schoolId);
                formData.append("IsAddMode", isAddMode);
                formData.append("BlogTypeId", blogTypeId);
                formData.append("GroupId", groupId);
               
                for (i = 0; i < imageFile.length; i++) {
                    formData.append("PostedImage", imageFile[i]);
                }
                formData.append("PostedImage", imageFile);
                var encodedData = window.btoa(embedUrl);
                formData.append("EmbededVideoLink", encodedData);
                formData.append("PostTitle", title);
                formData.append("PostDescription", description);
                formData.append("IsPublish", publish);
                formData.append("TaggedGroup", taggedGroup);
                formData.append("TaggedStudent", taggedStudent);
                formData.append("IsDepartmentWall", isDepartmentWall);
                formData.append("IsCourseWall", isCourseWall);
                formData.append("SchoolLevelId", schoolLevelId);
                var encodedReferenceLin = window.btoa(referenceLink);
                formData.append("ReferenceLink", encodedReferenceLin);
                formData.append("CreatedBy", createdBy);
                formData.append("CreatedOn", createOn);
                formData.append("SortOrder", sortOrder);
                formData.append("AdditionalDocPath", OldImage);
                formData.append("FileNames", FileNames);
                formData.append("ParentSharableLink", ParentSharableLink);

                var bannedWordCheck = globalFunctions.validateBannedWordsFormData('frmAddExemplarWall');
                if (!bannedWordCheck)
                    return;

                $.ajax({
                    url: '/SchoolInfo/ExemplarWall/SaveExemplarData',
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.Success == true) {
                           
                            if (isAddMode == "True") {
                                data.Message = translatedResources.addSuccessMsg;
                                exemplar.onSaveSuccess(data);
                            }
                            else {
                                data.Message = translatedResources.updateSuccessMsg;
                                exemplar.onSaveSuccess(data);
                            }
                        }
                        else {
                            globalFunctions.showErrorMessage(translatedResources.ErrorMessage);
                            ReloadPostOnSaveandUpdate();
                        }
                    },
                    error: function (data, xhr, status) {
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },
        deleteExemplar = function (source, id, createrId) {
           
            $("#ExemplarWallIdForDelete").val(id);

            var loggedInUser = $("#LoggedInUser").val();
            $("#TempCreatedId").val(createrId);
            if (loggedInUser == createrId) {
                $("#IsSelfCreator").hide();
            }
            else {
                $("#IsSelfCreator").show();
            }

        },
        onSaveSuccess = function (data) {
          
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
               
                $("#myModal").modal('toggle');
               
                exemplar.clearForm();
                ReloadPostOnSaveandUpdate();
            }
        },
        onDeleteSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $(".blog-editor").slideUp(250);
                loadBlogListForGroup();
                $(".modal-backdrop").hide();
            }

        },
        onUpdateSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $(".blog-editor").slideUp(250);
                loadBlogListForGroup();
               
                $(".modal-backdrop").hide();
                $(".modal-open").css("overflow", "auto");
                
            }
        },
        onShareSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $(".blog-editor").slideUp(250);
                loadBlogListForGroup();
                $(".modal-backdrop").hide();
                $("#sharePost").hide();
            }
        },
        updateSortOrder = function (data) {

            $.ajax({
                url: "/SchoolInfo/ExemplarWall/UpdateWallSortOrder",
                data: { 'Ids': data },
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    $("#createWorkPost").show();
                },
                error: function (msg) {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        viewFile = function (source, id) {
            
            window.open("/Document/Viewer/Index?id=" + id + "&module=blog", "_blank");
           
        },
        deleteFile = function () {
            $("#BlogImage").val("");
            $("#FileContainerDiv").hide();
           
        },
        deleteExemplarFile = function (postId, fileName, uploadedfilepath, counter) {

            var FileToDelete = uploadedfilepath;
            $.ajax({
                type: 'POST',
                data: { postId: postId, fileName: fileName, UploadedFilePath: uploadedfilepath },
                url: '/SchoolInfo/ExemplarWall/DeletePostFile',
                success: function (result) {
                   
                    if (result.success) {
                        var OldImage = $("#AdditionalDocPathHDN").val();
                        var separator = ",";
                        var List;
                        var values = OldImage.split(separator);
                        for (var i = 0; i < values.length; i++) {
                            if (values[i] == FileToDelete) {
                                values.splice(i, 1);
                                List = values.join(separator);
                            }
                        }
                        $("#AdditionalDocPathHDN").val(List);
                        $("#" + counter).remove();
                       
                    }
                },
                error: function (msg) {
                    quiz.onFailure;
                }
            });
        },
        ExemplarInputControlSettings = {

            CheckImage: {

                'doc': true,
                'docx': true,
                'xls': true,
                'xlsx': true,
                'ppt': true,
                'pptx': true,
                'pdf': true,
                'zip': true,
                'rar': true,
                'htm': true,
                'html': true,
                'txt': true,
                'rtf': true,
                'mov': true,
                'mp3': true,
                'mp4': true,
               
                'jpg': false,
                'jpeg': false,
                'JPEG': false,
                'gif': true,
                'png': false
            },
            fileActionSettings: {
                showZoom: false,
                showUpload: false,
                //indicatorNew: "",
                showDrag: false
            },
            previewFileIconSettings: { // configure your icon file extensions
                'doc': '<i class="fas fa-file-word ic-file-word color-doc"></i>',
                'docx': '<i class="fas fa-file-word ic-file-word color-doc"></i>',
                'xls': '<i class="fas fa-file-excel ic-file-excel color-excel"></i>',
                'xlsx': '<i class="fas fa-file-excel ic-file-excel color-excel"></i>',
                'ppt': '<i class="fas fa-file-powerpoint ic-file-ppt color-ppt"></i>',
                'pptx': '<i class="fas fa-file-powerpoint ic-file-ppt color-ppt"></i>',
                'pdf': '<i class="fas fa-file-pdf ic-file-pdf color-pdf "></i>',
                'zip': '<i class="fas fa-file-archive ic-file text-primary"></i>',
                'rar': '<i class="fas fa-file-archive ic-file text-primary"></i>',
                'htm': '<i class="fas fa-file-code ic-file text-primary"></i>',
                'html': '<i class="fas fa-file-code ic-file text-primary"></i>',
                'txt': '<i class="fas fa-file-alt ic-file color-txt"></i>',
                'rtf': '<i class="fas fa-file-alt ic-file color-txt"></i>',
                'mov': '<i class="fas fa-file-video ic-file color-video"></i>',
                'mp3': '<i class="fas fa-file-audio ic-file color-audio"></i>',
                'mp4': '<i class="fas fa-file-video ic-file color-video"></i>',
                // note for these file types below no extension determination logic
                // has been configured (the keys itself will be used as extensions)
                'jpg': '<i class="fas fa-file-image ic-file-img color-image"></i>',
                'jpeg': '<i class="fas fa-file-image ic-file-img color-image"></i>',
                'JPEG': '<i class="fas fa-file-image ic-file-img color-image"></i>',
                'gif': '<i class="fas fa-file-image ic-file-img color-image"></i>',
                'png': '<i class="fas fa-file-image ic-file-img color-image"></i>'
            },
            previewSettings: {
                image: { width: "50px", height: "auto" },
                html: { width: "50px", height: "auto" },
                other: { width: "50px", height: "auto" }
            },
            previewFileExtSettings: { // configure the logic for determining icon file extensions
                'doc': function (ext) {
                    return ext.match(/(doc|docx)$/i);
                },
                'xls': function (ext) {
                    return ext.match(/(xls|xlsx)$/i);
                },
                'ppt': function (ext) {
                    return ext.match(/(ppt|pptx)$/i);
                },
                'zip': function (ext) {
                    return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
                },
                'htm': function (ext) {
                    return ext.match(/(htm|html)$/i);
                },
                'txt': function (ext) {
                    return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
                },
                'mov': function (ext) {
                    return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
                },
                'mp3': function (ext) {
                    return ext.match(/(mp3|wav)$/i);
                }

            }
        };

    return {
        init: init,
        onGroupSelect: onGroupSelect,
        loadExemplarGrid: loadExemplarGrid,
        loadPageControlEvent: loadPageControlEvent,
        onShowMessage: onShowMessage,
        onSaveSuccess: onSaveSuccess,
        onUpdateSuccess: onUpdateSuccess,
        onShareSuccess: onShareSuccess,
        onDeleteSuccess: onDeleteSuccess,
        editExemplar: editExemplar,
        createExemplar: createExemplar,
        onSaveExemplar: onSaveExemplar,
        clearForm: clearForm,
        updateSortOrder: updateSortOrder,
        ExemplarInputControlSettings: ExemplarInputControlSettings,
        deleteExemplar: deleteExemplar,
        deleteFile: deleteFile,
        viewFile: viewFile,
        onSchoolLevelSelect: onSchoolLevelSelect,
        onCourseChanged: onCourseChanged,
        onDepartmentChanged: onDepartmentChanged,
        loadAddEditPopup: loadAddEditPopup,
        deleteExemplarFile: deleteExemplarFile
    };

}(jQuery, window);

$(function () {
    exemplar.init();
    initGrid();

    if ($(window).width() > 768) {
        //to make the group list section fixed on scroll
        var fixmeTop = $('.sticky-group').offset() != undefined ? $('.sticky-group').offset().top : 0;
        $(window).scroll(function () {
            var currentScroll = $(window).scrollTop();
            if (currentScroll >= fixmeTop) {
                $('.sticky-group').css({
                    position: 'fixed',
                    top: '80px',
                    //left: '0'
                });
            } else {
                $('.sticky-group').css({
                    position: 'static'
                });
            }
        });
    }


    $("#searchGroup").off("keyup").on("keyup", function () {
        //  
        var value = $(this).val().toLowerCase();
        $(".search-item").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    });
    $('.search-toggle').off("click").on('click', function () {
        $('.search-wrapper').slideToggle('fast');
        $('.search-wrapper input').focus();
    });

    

    $('body').on('click', '#btnSaveExemplarPost', function (e) {
        exemplar.onSaveExemplar($(this));
    });



    $(".btn.share").off("click").on("click", function () {
        //  
        var schoolId = $("#ExemplarWallModel_SchoolId").val();
        var groupId = $('#GroupId').val();
        var PostId = $('#PID').val();
        var token = $('input[name=__RequestVerificationToken]').val();
        var isDepartmentWall = $("#IsDepartmentWall_Share").val();
        var isCourseWall = $("#IsCourseWall_Share").val();
        var schoolLevelId = $("#SchoolLevelId_Share").val();
        var sharedBy = $("#ExemplarWallModel_CreatedBy").val();
        var shareDate = $("#ExemplarWallModel_CreatedOn").val();


        var formData = new FormData();
        formData.append("__RequestVerificationToken", token);
        formData.append("SchoolId", schoolId);
        formData.append("GroupId", groupId);
        formData.append("PostId", PostId);
        formData.append("ShareWithDepartmentWall", isDepartmentWall);
        formData.append("ShareWithCourseWall", isCourseWall);
        formData.append("SharingDate", schoolLevelId);
        formData.append("SharedByUserId", sharedBy);
        formData.append("SchoolLevelId", shareDate);


        $.ajax({
            url: '/SchoolInfo/ExemplarWall/ShareExemplarData',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.Success == true) {
                    data.Message = "Shared Successfully";
                    exemplar.onShareSuccess(data);
                    exemplar.clearForm();


                }
                else if (data.Success == false) {
                    globalFunctions.showErrorMessage(translatedResources.ErrorMessage);
                }
            },
            error: function (data, xhr, status) {
                //  
                globalFunctions.showErrorMessage();
            }
        });

    });

    $(document).on("click", ".readmore", function () {
        //  
        $("#ReadMoreModel").hide();
        var description = $(this).attr('data-val');
        if (description !== '') {
            $(".readMoreBody").html('');
            $(".readMoreBody").html(description);
            $("#ReadMoreModel").show();
        }
    });

    $(document).on("click", ".delete", function () {
        var exemplarWallId = $("#ExemplarWallIdForDelete").val();
        var deleteReason = $("#DeleteReason").val();

        var isDeleted = true;

        var loggedInUser = $("#LoggedInUser").val();
        var createrId = $("#TempCreatedId").val();
        if (loggedInUser !== createrId) {
            if (deleteReason === '') {
                exemplar.onShowMessage("Delete Reason is Required !", translatedResources.description);
                return;
            }
        }
        var formData = new FormData();

        formData.append("ExemplarWallId", exemplarWallId);
        formData.append("DeleteReason", deleteReason);
        formData.append("IsDeleted", isDeleted);
        var bannedWordCheck = globalFunctions.validateBannedWordsFormData('blogFormDelete');
        if (!bannedWordCheck)
            return;
        $.ajax({
            url: "/SchoolInfo/ExemplarWall/DeleteExemplar",
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data !== '') {
                    var val = $('#GroupId').val();
                    if (val != "") {
                        var element = $('#' + val);
                    }
                    
                    data.Message = translatedResources.deletedSuccess;
                    exemplar.onDeleteSuccess(data);
                   
                    OwlCarousel();
                    exemplar.loadPageControlEvent();
                    initGrid();
                    $("body").removeClass("modal-open");

                }
                else {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            },
            error: function (msg) {
               
                globalFunctions.showMessage("error", translatedResources.technicalError);
            }
        });
    });

    $(document).on('click', '.wallView', function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        exemplar.viewFile($(this), id);
    });

});


$(document).on('click', '#MyExemplarWork', function () {

    if (MyExemplarWork == 0) {
        $('#MyExemplarWork')[0].innerText = translatedResources.ShowAllExemplaryWork;
        MyExemplarWork = 1;
    } else {
        $('#MyExemplarWork')[0].innerText = translatedResources.ShowAllExemplaryWork;
        MyExemplarWork = 0;
    }
    $.ajax({
        url: "/SchoolInfo/ExemplarWall/GetExemplarListByGroupId?SchoolLevelId=" + $('#SchoolLevel').val() + "&CourseId=" + $('#ddlcourse').val() + "&GroupId=" + $("#GroupId").val() + "&MyExemplarWork=" + MyExemplarWork + "&SearchText=" + SearchText + "&DepartmentId=" + $("#ddldepartment").val(),
        async: false,
        success: function (result) {
            if (result !== '') {

                $(".wall-carousel").owlCarousel('destroy');
                $('.wall-carousel').empty();
                $("#exemplarList").html('');
                $("#exemplarList").html(result);
                OwlCarousel();
                initGrid();
                exemplar.loadPageControlEvent();
            }
            else {
                globalFunctions.showMessage("error", translatedResources.technicalError);
            }
        },
        error: function (msg) {
            globalFunctions.showMessage("error", translatedResources.technicalError);
        }
    });
})

var SearchText = '';

$(document).on('change', '#txtExemplarSearchArea', function () {
    SearchText = $(this).val();
    $.ajax({
        url: "/SchoolInfo/ExemplarWall/GetExemplarListByGroupId?SchoolLevelId=" + $('#SchoolLevel').val() + "&CourseId=" + $('#ddlcourse').val() + "&GroupId=" + $("#GroupId").val() + "&MyExemplarWork=" + MyExemplarWork + "&SearchText=" + SearchText + "&DepartmentId=" + $("#ddldepartment").val(),
        async: false,
        success: function (result) {
            //  
            if (result !== '') {
                $(".wall-carousel").owlCarousel('destroy');
                $('.wall-carousel').empty();
                $("#exemplarList").html('');
                $("#exemplarList").html(result);
                OwlCarousel();
                initGrid();
                exemplar.loadPageControlEvent();
            }
            else {
                globalFunctions.showMessage("error", translatedResources.technicalError);
            }
        },
        error: function (msg) {
            
            globalFunctions.showMessage("error", translatedResources.technicalError);
        }
    });
})

function postGroupToggle() {
    
    let groupWrapperWidth = $('.groupWrapper').width();
    let groupWrapperHeight = "calc(100vh - 140px)";
    $('.sticky-group').width(groupWrapperWidth);
    if ($(window).width() < 768) {
        $('.groupSelection').removeClass('show');
        $('.groupSelection .dropdown-menu').removeClass('show position-relative');
        $('.groupSelection .dropdown-menu').addClass("z-depth-3");
        $(".groupWrapper .dropdown-menu").mCustomScrollbar("disable", true);
        //$('.sticky-group').width(groupWrapperWidth);
    } else {
        $('.groupSelection').addClass('show');
        $('.groupSelection .dropdown-menu').addClass('show position-relative');
        $('.groupSelection .dropdown-menu').removeClass("z-depth-3");
        $(".groupWrapper .dropdown-menu").mCustomScrollbar({
            autoHideScrollbar: true,
            autoExpandScrollbar: true,
            scrollbarPosition: "outside",
            setHeight: groupWrapperHeight
        });
        //$('.sticky-group').width(groupWrapperWidth);
    }

    $("#createWorkPost").hide();

}

$(window).resize(function () {
    postGroupToggle();
});

function clearAddForm() {
    $("#BlogId").val("");
    $("#IsAddMode").val("");
    $("#Title").val("");
    $("#Description").val("");
    $("#BlogImage").val("");
    $("#EmbedUrl").val("");
    $('#ExemplarImage').fileinput('clear');


    $(":checkbox").prop('checked', false).parent().removeClass('active');
    $("#ExemplarWallModel_SchoolId").val("");

    $('#ExemplarWallModel_BlogTypeId').val("");
    $('#GroupId').val("");

    $("#EmbededVideoLink").val("");
    $("#PostTitle").val("");
    $("#AdditionalDocPath").val("");
    $("#PostDescription").val("");
    $("#ExemplarWallModel_IsPublish").val("");

    $("#ExemplarWallModel_CreatedBy").val("");
    $("#ExemplarWallModel_CreatedOn").val("");
    $("#ExemplarWallModel_SortOrder").val("");

    $("#ExemplarImage").fileinput('clear');



    $("#ddlgroupAdd").val('default');
    $("#ddlgroupAdd").selectpicker("refresh");

    $("#tag-students-add").val('default');
    $("#tag-students-add").selectpicker("refresh");

    $("#SchoolLevelId_Add").val('default');
    $("#SchoolLevelId_Add").selectpicker("refresh");


    $("#IsDepartmentWall_Add").val("");
    $("#IsCourseWall_Add").val("");

    $("#ReferenceLink").val("");
}

function initGrid() {
    if (document.getElementsByClassName('grid').length > 0) {
        var grid = new Muuri('.grid', {
            dragEnabled: true,
            layout: {
                fillGaps: true,
                alignRight: direction,
            },
            layoutOnInit: true
        }).on('move', function () {
            debugger;
            saveLayout(grid);
            
        });

       
        window.addEventListener('load', function () {
            grid.refreshItems().layout();
        });

        var layout = window.localStorage.getItem('layout');
        if (layout) {
            loadLayout(grid, layout);
        } else {
            grid.layout(true);
        }

        $('.wallView > img').on("load", function () {
            grid.refreshItems().layout();

        });
    }
}

$(document).on("change", "#PostTitle", function () {
    this.value = $.trim(this.value);
});

$(document).on("change", "#PostDescription", function () {
    this.value = $.trim(this.value);
});

function serializeLayout(grid) {
    //  
    debugger;
    var itemIds = grid.getItems().map(function (item) {
        return item.getElement().getAttribute('data-id');
    });
    return JSON.stringify(itemIds);
}

function saveLayout(grid) {
    debugger;
    var layout = serializeLayout(grid);
    window.localStorage.setItem('layout', layout);
}

function loadLayout(grid, serializedLayout) {
   
    var layout = JSON.parse(serializedLayout);
    var currentItems = grid.getItems();
    var currentItemIds = currentItems.map(function (item) {
        return item.getElement().getAttribute('data-id');
    });
    var newItems = [];
    var itemId;
    var itemIndex;

    for (var i = 0; i < layout.length; i++) {
        itemId = layout[i];
        itemIndex = currentItemIds.indexOf(itemId);
        if (itemIndex > -1) {
            newItems.push(currentItems[itemIndex]);
        }
    }
    
}

function BindDepartment() {
    $.ajax({
        type: 'POST',

        url: '/SchoolInfo/ExemplarWall/GetDepartment', // we are calling json method

        dataType: 'json',

        data: { id: $("#SchoolLevel").val() },
       
        success: function (departments) {
            $.each(departments, function (i, department) {

                $("#ddldepartment").append('<option value="' + department.Value + '">' +
                    department.Text + '</option>');
               

            });
            $('#ddldepartment').selectpicker('refresh');
        },
        error: function (ex) {
            
            globalFunctions.showMessage("error", translatedResources.technicalError);
        }
    });
}

var SelectedGroupId = 0;

function FillGroup() {

    var CourseId = $("#ddlcourse").val();
    //if ($("#ddlcourse").val() == 0) {
        $("#ddlGroup").val(0) ;
    //}
    $.ajax({
        url: '/SchoolInfo/ExemplarWall/GetSchoolGroupBySchoolLevelId?levelId=' + $("#SchoolLevel").val() + '&courseId=' + CourseId,
        success: function (result) {
          
            var markup = "";
            $("#ddlGroup").html(markup);
            markup += "<option value=''>" + translatedResources.groupdropdownmsg+"</option>";
            for (var x = 0; x < result.length; x++) {
                markup += "<option value=" + result[x].Value + ">" + result[x].Text + "</option>";
            }
            $("#ddlGroup").html(markup).show();
            $('#ddlGroup').selectpicker('refresh');
        },
        error: function (xhr, textStatus, error) {
        }
    })
}

$(document).on('click', '.approveExemplarPost', function () {
    var id = $(this).data("exemplarwallid");
    $.ajax({
        url: '/SchoolInfo/ExemplarWall/UpdateExemplarPostStatusFromPendingToApprove?ExemplarPostId=' + id,
        success: function (data) {
            if (data == 1) {
                exemplar.loadExemplarGrid();
            } else {
                //console.log(data);
            }
        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure();
        }
    });
});

function ReloadPostOnSaveandUpdate() {
    $.ajax({
        url: "/SchoolInfo/ExemplarWall/GetExemplarListByGroupId?SchoolLevelId=" + $('#SchoolLevel').val() + "&CourseId=" + $('#ddlcourse').val() + "&GroupId=" + $("#GroupId").val() + "&MyExemplarWork=" + MyExemplarWork + "&SearchText=" + SearchText + "&DepartmentId=" + $("#ddldepartment").val(),
        async: true,
        success: function (result) {

            if (result !== '') {
                $(".wall-carousel").owlCarousel('destroy');
                $('.wall-carousel').empty();
                $("#exemplarList").html('');
                $("#exemplarList").html(result);
                OwlCarousel();
                initGrid();

            }
            else {
                globalFunctions.showMessage("error", translatedResources.technicalError);
            }
        },
        error: function (msg) {
            globalFunctions.showMessage("error", translatedResources.technicalError);
        }
    });
}

function OwlCarousel() {
    $(".wall-carousel").owlCarousel({
        margin: 20,
        autoplay: false,
        nav: false,
        dots: true,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1,
        video: true,
        lazyLoad: true
    });
}

function GetValueFromDB(checkboxstatus, operation) {
    $.ajax({
        url: '/SchoolInfo/ExemplarWall/GetApproveStatusDetail?Status=' + checkboxstatus + '&Operation=' + operation,
        success: function (data) {
            if (data.Status) {
                $('#chkApprovalRight').prop('checked', 'checked');
            } else {
                $('#chkApprovalRight').prop('checked', false);
            }
        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure();
        }
    });
}

function InitializeCheckBox() {
    GetValueFromDB($('#chkApprovalRight').prop('checked'), 'GET');
}

$('#chkApprovalRight').change(function () {
    GetValueFromDB($('#chkApprovalRight').prop('checked'), 'UPDATE');
});

function FillStudentOnUpdate(SchoolGroupId) {

    $.ajax({
        url: "/SchoolInfo/ExemplarWall/GetStudentByGroupId",
        data: { SchoolGroupId: SchoolGroupId },
        cache: false,
        type: "POST",
        async: false,
        success: function (data) {
            $("#TaggedStudent").title = translatedResources.SelectStudent;

            var markup = "";

            for (var x = 0; x < data.length; x++) {
                markup += "<option data-thumbnail=" + data[x].StudentImage + " value=" + data[x].Id + ">" + data[x].UserName + "</option>";
            }
            $("#TaggedStudent").html(markup).show();
            if (data.length > 0) {
                initSelectPicker();
            }

            $('#TaggedStudent').selectpicker('refresh');
        },
        error: function (reponse) {
            globalFunctions.showMessage("error", translatedResources.technicalError);
        }
    });
}

function initSelectPicker() {
    if ($(".select-picker-withImg").length) {
        //  
        const BASE_URL = "img/";
        const $_SELECT_PICKER = $('.select-picker-withImg');

        $_SELECT_PICKER.find('option').each((idx, elem) => {
            const $OPTION = $(elem);
            const IMAGE_URL = $OPTION.attr('data-thumbnail');

            if (IMAGE_URL) {
                $OPTION.attr('data-content', "<img src='%i'/> %s".replace(/%i/, IMAGE_URL).replace(/%s/, $OPTION.text()))
            }
           

        });

        $_SELECT_PICKER.selectpicker();
    };
}

function RefreshKrajerr() {
    $("#ExemplarImageUpdate").fileinput({
        language: translatedResources.locale,
        title: translatedResources.DragAndDropMessage,
        theme: "fas",
        showUpload: false,
        showCaption: false,
        showMultipleNames: false,
        uploadUrl: "/Quiz/Quiz/UploadQuizFiles",
        multiple: true,
        minFileCount: 1,
        maxFileCount: 10,

        fileActionSettings: {
            showZoom: false,
            showUpload: false,
            //showRemove: true,
            //indicatorNew: "",
            showDrag: false
        },

        uploadAsync: true,
        browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
        removeClass: "btn btn-sm m-0 z-depth-0 d-none",
        uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
        cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
        overwriteInitial: false,
        //previewFileIcon: '<i class="fas fa-file"></i>',
        initialPreviewAsData: false, // defaults markup
        initialPreviewConfig: [{
            frameAttr: {
                title: 'My Custom Title',
            }
        }],
        preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
        previewFileIconSettings: fileInputControlSettings.previewFileIconSettings,
        previewSettings: fileInputControlSettings.previewSettings,
        previewFileExtSettings: fileInputControlSettings.previewFileExtSettings
    });
}

function LoadGroups() {
    $.ajax({
        url: '/SchoolInfo/ExemplarWall/GetAllGroupBySchoolId',
        success: function (result) {
            var markup = "";
            $("#ddlGroup").html(markup);
            markup += "<option value=''>" + translatedResources.groupdropdownmsg +"</option>";
            for (var x = 0; x < result.length; x++) {
                markup += "<option value=" + result[x].Value + ">" + result[x].Text + "</option>";
            }
            $("#ddlGroup").html(markup).show();
            $('#ddlGroup').selectpicker('refresh');
        },
        error: function (xhr, textStatus, error) {
        }
    })
}

$(document).on('click', '.btnRejectPlan', function () {
    var id = $(this).data("exemplarwallid");
    $.ajax({
        url: '/SchoolInfo/ExemplarWall/RejectExemplarPost?ExemplarPostId=' + id,
        type: 'POST',
        success: function (data) {
            if (true) {
                exemplar.loadExemplarGrid();
            }
        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure();
        }
    });
})

$('body').on('click', '#DeleteExemplarFiles', function () {
    
    var fileName = $(this).attr('data-fileName');
    var uploadedfilepath = $(this).attr('data-uploadedPath');
    var postId = $(this).attr('data-ExemplarId');
    var counter = $(this).attr('data-counter');
    exemplar.deleteExemplarFile(postId, fileName, uploadedfilepath, counter);

});

$('body').on('click', '#Download', function (e) {
    
    var fileName = $(this).attr('data-val');

  
    window.open(fileName);

});

$('body').on('click', '#NewDownload', function (e) {
   
    var fileName = $(this).attr('data-val');

   
    window.open(fileName);
  
});

$(document).on('click', '.btnViewAttachment', function () {
    var docpath = $(this).data("additionaldocpath");
    var filenames = $(this).data("filenames");
    var shareablelink = $(this).data("shareablelink");
    $('#viewAttachmentBody').html('');
    $.ajax({
        url: '/SchoolInfo/ExemplarWall/viewAttachment?docpath=' + docpath + "&filenames=" + filenames + "&shareablelink=" + shareablelink,
        type: 'POST',
        success: function (data) {
           
            $('.viewAttachmentBody').html(data.planHTML);
        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure();
        }
    });
})