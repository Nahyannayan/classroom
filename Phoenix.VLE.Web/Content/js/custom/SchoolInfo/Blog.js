﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var blog = function () {
    var init = function () {
        loadBlogGrid();

    },

        loadBlogPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.header;
            globalFunctions.loadPopup(source, '/SchoolInfo/Blog/InitAddEditBlogForm?id=' + id, title, 'blog-dailog modal-xl');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                // debugger;
                //CKEDITOR.replace('Summary', {
                //    htmlencodeoutput: false
                //});
                CKEDITOR.replace('Description', {
                    htmlencodeoutput: false
                });
                initFileInput();
                //formElements.feSelect();
                $('select').selectpicker();
                disableCategery($("#BlogTypeId").val());
            });


        },

        addBlogPopup = function (source) {
            loadBlogPopup(source, translatedResources.add);

        },

        editBlogPopup = function (source, id) {
            loadBlogPopup(source, translatedResources.edit, id);
        },

        deleteBlogData = function (source, id) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('Blog', '/SchoolInfo/Blog/DeleteBlogData', 'tbl-Blog', source, id);
            }
        },

        publishBlogData = function (source, id) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/PublishBlog",
                    data: { 'id': id },
                    async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            loadBlogGrid();
                        }

                        setTimeout(function () {
                            //globalFunctions.pageLoadingFrame("hide");
                        }, 100);
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },

        viewBlogData = function (source, id) {
            window.open("/schoolinfo/blog/viewblog?bgId=" + id, "_blank");
        },

        unPublishBlogData = function (source, id) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/UnPublishBlog",
                    data: { 'id': id },
                    async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            loadBlogGrid();
                        }

                        setTimeout(function () {
                            //globalFunctions.pageLoadingFrame("hide");
                        }, 100);
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        onShowMessage = function (message) {
            //debugger;
            globalFunctions.showErrorMessage(message);
        },

        postedImage = function (source) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
            if ($.inArray($(source).val().split('.').pop().toLowerCase(), fileExtension) === -1) {
                //alert(translatedResources.extensionalert + fileExtension);
                $(source).val("");
                return false;
            }
            else {
                var imageToUpload = $(source).get(0);
                var image = imageToUpload.files;
                var size = image[0].size / 1024 / 1024;
                if (size > 2) {
                    alert(translatedResources.sizealert);
                    $(source).val("");
                    return false;
                }
            }
        },

        loadBlogGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Title", "sTitle": translatedResources.title, "sWidth": "10%", "sClass": "wordbreak" },
                { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "20%", "sClass": "wordbreak" },
                { "mData": "BlogImage", "sTitle": translatedResources.blogimage, "sWidth": "20%" },
                { "mData": "Publish", "sTitle": translatedResources.publish, "sWidth": "10%" },
                { "mData": "Author", "sTitle": translatedResources.createdby, "sWidth": "10%" },
                { "mData": "CreatedOn", "sTitle": translatedResources.createdon, "sWidth": "15%" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "15%" }
            );
            initBlogGrid('tbl-Blog', _columnData, '/SchoolInfo/Blog/LoadBlogGrid');
        },

        likeBlog = function (id, islike) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/LikeBlog",
                    data: { 'id': id, 'isLike': islike },
                    async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            getBlogDetail();
                        }
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },

        likeComment = function (id, islike) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/LikeComment",
                    data: { 'id': id, 'isLike': islike },
                    async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            getBlogDetail();
                        }
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },

        getBlogDetail = function () {
            var blogId = $("#blogDetail").data("id");
            
            $.ajax({
                type: 'GET',
                url: "/SchoolInfo/blog/BlogDetail?bgId=" + blogId,
                success: function (result) {
                    if (result === '')
                        globalFunctions.showMessage("error", result.Message);
                    else {
                        $("#blogDetail").html('');
                        $("#blogDetail").html(result);
                    }
                },
                error: function (msg) {
                    //globalFunctions.pageLoadingFrame("hide");
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
            
        },
        viewFile = function (source, id) {
            //source.preventDefault();
            if (globalFunctions.isValueValid(id)) {
                //window.open("/Document/Viewer/Index?id=" + id + "&module=file", "_blank");
                window.location.href = "/Files/Files/DownloadFile/?id=" + id;
            }
        },
        initBlogGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                processing: true,
                columnDefs: [{
                    'targets': 6,
                    'orderable': false
                }]
            };
            grid.init(settings);
        };
    return {
        init: init,
        loadBlogGrid: loadBlogGrid,
        addBlogPopup: addBlogPopup,
        onSaveSuccess: onSaveSuccess,
        editBlogPopup: editBlogPopup,
        deleteBlogData: deleteBlogData,
        postedImage: postedImage,
        publishBlogData: publishBlogData,
        unPublishBlogData: unPublishBlogData,
        viewBlogData: viewBlogData,
        onShowMessage: onShowMessage,
        likeBlog: likeBlog,
        viewFile: viewFile,
        likeComment: likeComment
    };

}();


$(document).ready(function () {
    
    blog.init();
  
    $('#btnAddBlog').off('click').on('click',  function () {
        //debugger;
        blog.addBlogPopup($(this));
       
    });

    $('#BlogTypeId').off('change').on('change',  function () {
        var selected = $(this).val();
        disableCategery(selected);
    });

    //$(document).on('change', '#PostedImage', function () {
    //    blog.postedImage($(this));
    //});
    $('#tbl-Blog').DataTable().search('').draw();
    $("#BlogListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-Blog').DataTable().search($(this).val()).draw();
    });

    $(document).on('click', '#btnViewFile', function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        blog.viewFile($(this), id);
    });
});

function disableCategery(id) {
    if (id == '2') {
        $("#CategoryId").parent('.dropdown').addClass("disabled");
    }
    else {
        $("#CategoryId").parent('.dropdown').removeClass("disabled");
    }
}


var initFileInput = function () {
    //debugger;
    var img = ''; //$("#PostedImage").attr("value");
    $("#PostedImage").fileinput({
        language: translatedResources.locale,
        title: translatedResources.DragAndDropMessage,
        theme: "fas",
        showUpload: false,
        showCaption: false,
        showMultipleNames: false,
        uploadUrl: "/SchoolInfo/Blog/SaveBlogData",
        uploadExtraData: function () {
            return {
                __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
                BlogId: $("#BlogId").val(),
                SchoolId: $("#SchoolId").val(),
                IsAddMode: $("#IsAddMode").val(),
                CategoryId: $('#CategoryId option:selected').val(),
                PostedImage: $("#PostedImage").get(0).files[0],
                Title: $("#Title").val(),
                Summary: $("<div />").text(CKEDITOR.instances.Summary.getData()).html(),
                Description: $("<div />").text(CKEDITOR.instances.Description.getData()).html()
            };
        },
        fileActionSettings: {
            showZoom: false,
            showUpload: false,
            //indicatorNew: "",
            showDrag: false
        },
        allowedFileExtensions: ["jpg", "jpeg", "gif", "png"],
        uploadAsync: true,
        browseClass: "btn btn-sm m-0 z-depth-0 btn-browse",
        removeClass: "btn btn-sm m-0 z-depth-0",
        uploadClass: "btn btn-sm m-0 z-depth-0",
        initialPreview: [img],
        overwriteInitial: false,
        //previewFileIcon: '<i class="fas fa-file"></i>',
        initialPreviewAsData: false, // defaults markup
        initialPreviewConfig: [{
            frameAttr: {
                title: "CustomTitle",
            }
        }],
        preferIconicPreview: false, // this will force thumbnails to display icons for following file extensions
        previewFileIconSettings: { // configure your icon file extensions
            'doc': '<i class="fas fa-file-word ic-file-word"></i>',
            'xls': '<i class="fas fa-file-excel ic-file-excel"></i>',
            'ppt': '<i class="fas fa-file-powerpoint ic-file-ppt"></i>',
            'pdf': '<i class="fas fa-file-pdf ic-file-pdf"></i>',
            'zip': '<i class="fas fa-file-archive ic-file"></i>',
            'htm': '<i class="fas fa-file-code ic-file"></i>',
            'txt': '<i class="fas fa-file-alt ic-file"></i>',
            'mov': '<i class="fas fa-file-video ic-file"></i>',
            'mp3': '<i class="fas fa-file-audio ic-file"></i>',
            // note for these file types below no extension determination logic
            // has been configured (the keys itself will be used as extensions)
            'jpg': '<i class="fas fa-file-image ic-file-img"></i>',
            'jpeg': '<i class="fas fa-file-image ic-file-img"></i>',
            'gif': '<i class="fas fa-file-image ic-file-img"></i>',
            'png': '<i class="fas fa-file-image ic-file-img"></i>'
        },
        previewSettings: {
            image: { width: "50px", height: "auto" },
            html: { width: "50px", height: "auto" },
            other: { width: "50px", height: "auto" }
        },
        previewFileExtSettings: { // configure the logic for determining icon file extensions
            'doc': function (ext) {
                return ext.match(/(doc|docx)$/i);
            },
            'xls': function (ext) {
                return ext.match(/(xls|xlsx)$/i);
            },
            'ppt': function (ext) {
                return ext.match(/(ppt|pptx)$/i);
            },
            'zip': function (ext) {
                return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
            },
            'htm': function (ext) {
                return ext.match(/(htm|html)$/i);
            },
            'txt': function (ext) {
                return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
            },
            'mov': function (ext) {
                return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
            },
            'mp3': function (ext) {
                return ext.match(/(mp3|wav)$/i);
            }
        }

    });
};

