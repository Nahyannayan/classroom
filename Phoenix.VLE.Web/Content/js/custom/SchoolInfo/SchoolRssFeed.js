﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
window.onload = function () {
    $('.loader-wrapper').fadeOut(500, function () {
        //$('.loader-wrapper').remove(); 
    });

}
var SchoolRssFeed = function () {
    var init = function () {
        loadRSSFeed();


    },


        loadUserRolePopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.userRole;
            globalFunctions.loadPopup(source, '/Home/RssFeedAddEdit?id=' + id, title, 'UserRole-dailog');

        },

        addUserRolePopup = function (source) {
            loadUserRolePopup(source, translatedResources.add);
        },



        editUserRolePopup = function (source, id) {
            loadUserRolePopup(source, translatedResources.edit, id);
        },



        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },



        loadRSSFeed = function () {

            //debugger;
            $.ajax({
                type: "GET",
                url: "/Home/GetRSSFeedList",
                dataType: "json",
                error: function () {
                    // alert("Unable To load RSS Feed")
                },
                success: function (data) {
                    console.log(data);
                    $('#divRSSFeed').html('');
                    var html = '<ul data-role="listview" data-filter="true" class="web-links">';

                    for (var i = 0; i < data.length; i++) {

                        html += '<li>';
                        html += '<a href="http://' + data[i].RSSLink + '" target="_blank">';
                        html += ' <h6><i class="fas fa-external-link-alt"></i> ' + data[i].RSSTitle + '</h6><p> ' + data[i].RSSDescription + '</p>';
                        html += '</a>';
                        html += '</li>';
                    }
                    $('#divRSSFeed').append(html);
                }



            });

        }


    return {
        init: init,
        addUserRolePopup: addUserRolePopup,
        editUserRolePopup: editUserRolePopup,
        onSaveSuccess: onSaveSuccess,
        loadRSSFeed: loadRSSFeed

    };
}();

$(document).ready(function () {
    SchoolRssFeed.init();
    $('.smiley').on('click', function () {
        $('.smiley.active').removeClass('active');
        $(this).addClass('active');
    });
    $(".event-data").mCustomScrollbar({
        setHeight: "300",
    })

    $(document).on('click', '#rssfeed', function () {
        //debugger;
        SchoolRssFeed.addUserRolePopup($(this));
    });

    $('a.hoverinfo[data-toggle="tooltip"]').tooltip({
        trigger: 'hover',
        html: true,
        template: '<div class="tooltip school-fullname" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
    })
    $("#siteBanner").owlCarousel({
        margin: 0,
        dots: false,
        autoplay: true,
        items: 1,
        nav: true,
        loop: true
    });


});



