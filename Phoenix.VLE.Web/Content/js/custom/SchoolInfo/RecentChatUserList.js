﻿
//var chatUser = function () {
//    var page = 1,
//    inCallback = false,
//    isReachedScrollEnd = false;
//    var init = function () {
//        /////////// search contact from group member list
//        $('#txtContactSearch').keyup(function (e) {
//            var searchString = $('#txtContactSearch').val();
//            if ($('#txtContactSearch').val().length > 2) {
//                SearchContact(searchString);
//            }
//            else if ((e.keyCode == 8 || e.keyCode == 46) && searchString.length < 3) {
//                searchString = "";
//                SearchContact(searchString);
//            }
//        });
       
//        //refreshCommentSection();

//    },
//    scrollHandler = function () {
//        if (isReachedScrollEnd == false &&
//            ($('#divChatWindow').scrollTop() <= 0)) {
//            loadRecentChat();
//        }
//    },
//    loadRecentChat = function () {
//            //debugger;
//        var loadMoreRowsUrl = "/SchoolInfo/Chat/GetRecentChat";
//        if (page > -1 && !inCallback) {
//            inCallback = true;
//            page++;
//            $.ajax({
//                type: 'GET',
//                url: loadMoreRowsUrl,
//                data: { 'pageNum': page, toUserById: $('#hdntoUserId').val(), groupId: 0 },
//                success: function (data, textstatus) {
//                    if (data != '') {
//                        $("#divChatWindow").append(data);
//                    }
//                    else {
//                        page = -1;
//                    }

//                    inCallback = false;

//                },
//                error: function (XMLHttpRequest, textStatus, errorThrown) {
//                    alert(errorThrown);
//                }
//            });
//        }

//        },
//    return {
//        init: init,
//        loadRecentChat: loadRecentChat,
//        scrollHandler: scrollHandler
//    };
//}();

$(function () {
 
    inCallback = false,
    isReachedScrollEnd = false;
    var p = '';
    $('#divChatWindow').scroll(scrollHandler);
    $('#txtSearch').keyup(function () {
        var yourInput = $(this).val();
        re = /[\\":]/gi;
        var isSplChar = re.test(yourInput);
        if (isSplChar) {
            var no_spl_char = yourInput.replace(/[\\":]/gi, '');
            $(this).val(no_spl_char);
        } else
            if ($('#txtSearch').val().length > 2) {
                $.ajax({
                    url: "/Chat/Search",
                    data: "{'searchText':'" + $('#txtSearch').val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        var val = '<ul id="userlist">';
                        if (data && data.length == 0) {
                            val += '<li>' + '<p>' + translatedResources.NoDataFound +'</p>' + '</li>'
                            $("#divautocomplete").css({ "overflow-y": "hidden" });

                        }
                        else {
                            $("#divautocomplete").css({ "overflow-y": "auto" });

                        }
                        $.each(data, function (i, item) {
                            val += '<li><a href="#" id=' + item.ToUserId + ' data-online="' + item.IsOnline + '"  data-name ="' + item.ToUser + '" data-src = ' + item.ToUserProfileImage + ' onclick="BindRecentList(' + item.ToUserId + ')" class = "d-flex align-items-center w-100 p-2 border-bottom"><div class="defaultAvatar rounded-circle overflow-hidden mr-3"><img class= "img-fluid" src = ' + item.ToUserProfileImage + ' alt = ' + item.ToUser + ' /></div><div class="student-info d-flex flex-1 justify-content-between"><h3 class="name m-0 text-medium">' + item.ToUser + '</h3><span class="small text-medium"><img src="/Content/img/svg/message.svg" class="svg"/>Chat now</span></div></a></li>'
                        })
                        val += '</ul>'
                        $('#divautocomplete').show();
                        $('#divautocomplete').html(val);
                        $('#userlist li').click(function () {
                            $('#divautocomplete').hide();
                        })
                    },
                    error: function (response) {
                        alert(response.responseText);
                    }
                });
            }
    })
    $(document).mouseup(function (e) {
        var closediv = $("#divautocomplete");
        if (closediv.has(e.target).length == 0) {
            closediv.hide();
        }
    });
    function scrollHandler() {
        if ($("#divChatWindow")[0].scrollHeight != $("#divChatWindow")[0].clientHeight) {
            if (isReachedScrollEnd == false && ($("#divChatWindow").scrollTop() <= 0)) {
                p = $("#divChatWindow")[0].scrollHeight;
                w = $("#divChatWindow")[0].clientHeight;
                //alert(p); alert(w);
                loadWallData(p, w);
            }
        }

     
    }

    function loadWallData(p, w) {
        var groupId = 0;
        if ($('#GroupId').val() != '') {
            groupId = parseInt($('#GroupId').val());
        }
        

        var loadMoreRowsUrl = "/SchoolInfo/Chat/GetRecentChat";
        if (page > -1 && !inCallback) {
            inCallback = true;
            page++;
            $.ajax({
                type: 'GET',
                url: loadMoreRowsUrl,
                data: { 'pageNum': page, toUserById: $('#hdntoUserId').val(), groupId: groupId },
                success: function (data, textstatus) {
                    if (data != '') {
                        loadRecentChatData(data, p, w);
                      
                    }
                    else {
                        page = -1;
                    }

                    inCallback = false;

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
        }
    }
});

loadRecentChatData = function (result, p, w) {
    var divChat1 = '';
    for (i = 0; i < result.length; i++) {
        console.log(result[i]);
        grpId = result[i].GroupId;
        //var encodedName = result[i].FromUser;
        var encodedName = result[i].FromUserId;
        var encodedMsg = result[i].ChatMessage;
        var color = '#1fce56';
        if (result[i].ChatTypeId == 1) {
            if (result[i].IsRead == false) {
                color = 'gray';
            }
        }
       
        var CurrUser = $('#hdnLoginUser').val();

        var dateString = result[i].MessageDateTime;
        var seconds = new Date(parseInt(/^\/Date\((.*?)\)\/$/.exec(dateString)[1], 10));
        var date = new Date(seconds);
        var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
        var am_pm = date.getHours() >= 12 ? "PM" : "AM";
        hours = hours < 10 ? "0" + hours : hours;
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        time = hours + ":" + minutes + "" + am_pm;
        var date = new Date(seconds).toLocaleDateString()
        console.log(seconds);
        var today = new Date().toLocaleDateString();
        //debugger;
        //if (previousDate == '') {
        //    previousDate = date
        //    var cenDate = '';
        //    if (today == date) {
        //        cenDate = '<div class="text-center mb-4 text-bold"><span>Today</span></div>';
        //    } else {
        //        cenDate = '<div class="text-center mb-4 text-bold"><span>' + date + '</span></div>';
        //    }
        //    divChat1 += cenDate;
        //}
        //if (date != previousDate) {
        //    var cenDate = '';
        //    if (today == date) {
        //        cenDate = '<div class="text-center mb-4 text-bold"><span>Today</span></div>';
        //    } else {
        //        cenDate = '<div class="text-center mb-4 text-bold"><span>' + date + '</span></div>';
        //    }
        //    previousDate = date;
        //    divChat1 += cenDate;
          
        //}
     
            if (result[i].MessageTypeId == "1") {
                if (CurrUser == encodedName) {

                    divChat = '<div class="self-msg d-flex align-items-center flex-row-reverse mb-4">' +
                        '<div class="avatar rounded-circle overflow-hidden  ml-3 chatAvatar"> ' +
                        '<img src="' + result[i].FromUserProfileImage + '" alt="" class="img-fluid">' +
                        ' </div>' +
                        '<span class="chat-text text-medium ml-2 mr-2"> ' +
                        '' + linkify(result[i].ChatMessage) + ' ' +
                        '  </span>' +
                        '<span class="chat-check ml-2 mr-2"><i id="isread" class="fas fa-check" style="color:' + color + ';"></i></span>' +
                        '<span class="chat-time " title="' + date + '">   ' +
                        '' + time + ' ' +
                        '</span>' +
                        '</div>';
                }
                else {
                    divChat = '<div class="other-msg d-flex align-items-center flex-row mb-4">' +
                        '<div class="avatar rounded-circle overflow-hidden  ml-3 chatAvatar"> ' +
                        '<img src="' + result[i].FromUserProfileImage + '" alt="" class="img-fluid" title="' + result[i].FromUser + '">' +
                        ' </div>' +
                        '<span class="chat-text text-medium ml-2 mr-2"> ' +
                        '' + linkify(result[i].ChatMessage) + ' ' +
                        '  </span>' +
                        '<span class="chat-check ml-2 mr-2"><i class="fas fa-check"></i></span>' +
                        '<span class="chat-time " title="' + date + '">   ' +
                        '' + time + ' ' +
                        '</span>' +
                        '</div>';
                }

            }
            else {
                var color = '#1fce56';
                divChat = ImageSend(result[i].FromUser, result[i].FromUserProfileImage, result[i].FromUserProfileImage, result[i].FileName, result[i].ChatMessage, CurrUser, time, color, encodedName)
            }
       
        divChat1 += divChat;
        //$("#divChatWindow").append(divChat); 

    }
    $("#divChatWindow").prepend(divChat1); 
    if ($("#divChatWindow")[0].scrollHeight != $("#divChatWindow")[0].clientHeight) {
        $("#divChatWindow")[0].scrollTop = $("#divChatWindow")[0].scrollHeight - (p + w) ;
    }
    else {
        $("#divChatWindow")[0].scrollTop = $("#divChatWindow")[0].scrollHeight - (p+w);
    }
}


$(document).ready(function () {
    //chatUser.init();
});
