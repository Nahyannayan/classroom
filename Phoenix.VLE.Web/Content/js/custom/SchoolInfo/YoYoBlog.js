﻿/// <reference path="../../ckeditor/config.js" />
/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var yoYoBlog = function () {

    var init = function () {
        loadPageControlEvent();
    },
        loadPageControlEvent = function () {
            postGroupToggle();
            pageEvents();

        },

        onGroupSelect = function (source, id) {
            ////debugger;
            $(".groupSelection .dropdown-item").removeClass("active");

            $(source).addClass("active");
            if (globalFunctions.isValueValid(id)) {
                //console.log(id);
                $("#GroupId").val(id);
                $('#groupName').text($(source).children('h4').html().replace(/\s/g, ' ').split('<')[0]);
                loadBlogListForGroup();
                clearForm();
                $(".blog-editor").hide();
            }
        },

        loadBlogListForGroup = function () {
            var groupId = $("#GroupId").val();

            $.ajax({
                type: 'POST',
                url: "/SchoolInfo/Blog/GetBlogListByGroupId",
                data: { 'groupId': groupId },
                success: function (result) {
                    if (result !== '') {
                        debugger;
                        $("#blogList").html('');
                        $("#blogList").html(result);
                        loadPageControlEvent();
                    }
                    else {
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }


                },
                error: function (msg) {
                    //globalFunctions.pageLoadingFrame("hide");
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        loadWallListForGroup = function () {
            var groupId = 0;
            if ($("#GroupId").val() != "") {
                groupId = $("#GroupId").val();
            }


            $.ajax({
                type: 'POST',
                url: "/SchoolInfo/Blog/GetWallListByGroupId",
                data: { 'groupId': groupId },
                success: function (result) {
                    if (result !== '') {
                        $("#myWallList").html('');
                        $("#myWallList").html(result);
                        //loadPageControlEvent();
                    }
                    else {
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }


                },
                error: function (msg) {
                    //globalFunctions.pageLoadingFrame("hide");
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },

        loadWallListForGroupById = function (groupId) {
            $("#GroupId").val(groupId);
            $.ajax({
                type: 'POST',
                url: "/SchoolInfo/Blog/GetWallListByGroupId",
                data: { 'groupId': groupId },
                success: function (result) {
                    if (result !== '') {
                        $("#myWallList").html('');
                        $("#myWallList").html(result);
                    }
                    else {
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }


                },
                error: function (msg) {
                    //globalFunctions.pageLoadingFrame("hide");
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },

        editBlogPopup = function (id) {
            $.ajax({
                type: 'POST',
                url: "/SchoolInfo/Blog/EditYoYoBlogForm",
                data: { 'id': id },
                async: true,
                success: function (result) {
                    if (result !== '') {

                        $("#addEditYoYoBlog").html('');
                        $("#addEditYoYoBlog").html(result);
                        loadPageControlEvent();
                        $('.selectpicker').selectpicker();
                        $(".expand-blog-editor").trigger("click");
                        $('#groupName').text($(".dropdown-item.active").children('h4').html().replace(/\s/g, '').split('<')[0]);
                        //$("#CloseDiscussionDate").datetimepicker({ minDate: false}).removeAttr('readonly');
                        //$('#CloseDiscussionDate').datetimepicker({
                        //    format: 'DD-MMM-YYYY',
                        //    //maxDate: endDate,
                        //    minDate: null

                        //});
                    }
                    else {
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                },
                error: function (msg) {
                    //globalFunctions.pageLoadingFrame("hide");
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        myWallEditBlogPopup = function (id) {

            $("#createBlogPopup").html('');

            $.ajax({
                type: 'POST',
                url: "/SchoolInfo/Blog/MyWallEditBlog",
                data: { 'id': id },
                async: true,
                success: function (result) {
                    if (result !== '') {
                        $("#createBlogPopup").html('');
                        $("#createBlogPopup").html(result);

                        // loadPageControlEvent();

                        //$('.selectpicker').selectpicker();
                        //$(".expand-blog-editor").trigger("click");
                        //$('#groupName').text($(".dropdown-item.active").children('h4').html().replace(/\s/g, '').split('<')[0]);
                    }
                    else {
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }

                },
                error: function (msg) {
                    //globalFunctions.pageLoadingFrame("hide");
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });

        },
        editBlogDiscussionClose = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, "Are you sure you want to close discussion?");
                $(document).bind('okToDelete', $(source), function (e) {
                    var groupId = $("#GroupId").val();

                    $.ajax({
                        type: 'POST',
                        url: "/SchoolInfo/Blog/UpdateDiscloseDate",
                        data: { 'id': id, 'groupId': groupId },
                        async: true,
                        success: function (result) {
                            if (result !== '') {
                                $("#blogList").html('');
                                $("#blogList").html(result);
                                loadPageControlEvent();
                            }
                            else {
                                globalFunctions.showMessage("error", translatedResources.technicalError);
                            }
                        },
                        error: function (msg) {
                            //globalFunctions.pageLoadingFrame("hide");
                            globalFunctions.showMessage("error", translatedResources.technicalError);
                        }
                    });
                });
            }
        },


        deleteBlogData = function (source, id) {
            // //debugger;
            if (globalFunctions.isValueValid(id)) {

                globalFunctions.notyDeleteConfirm(source, translatedResources.DeleteBlog);
                $(document).bind('okToDelete', $(source), function (e) {

                    $.ajax({
                        type: 'POST',
                        url: "/SchoolInfo/Blog/DeleteBlogData",
                        data: { 'id': id },
                        //async: false,
                        success: function (result) {
                            if (result.Success === false)
                                globalFunctions.showMessage("error", result.Message);
                            else {
                                loadBlogListForGroup();
                            }
                        },
                        error: function (msg) {
                            //globalFunctions.pageLoadingFrame("hide");
                            globalFunctions.showMessage("error", translatedResources.technicalError);
                        }
                    });

                });
            }
        },

        publishBlogData = function (id) {
            // //debugger;
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/PublishBlog",
                    data: { 'id': id },
                    //async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            loadBlogListForGroup();
                        }

                        setTimeout(function () {
                            //globalFunctions.pageLoadingFrame("hide");
                        }, 100);
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },

        unPublishBlogData = function (id) {
            // //debugger;
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/UnPublishBlog",
                    data: { 'id': id },
                    //async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            loadBlogListForGroup();
                        }


                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },

        likeBlog = function (id, islike) {
            // //debugger;
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/LikeBlog",
                    data: { 'id': id, 'isLike': islike },
                    //async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            loadBlogListForGroup();
                        }
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },
        likeMyWall = function (id, islike) {

            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/LikeBlog",
                    data: { 'id': id, 'isLike': islike },
                    //async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            loadWallBlogListForBlog(id, function (dataresult) {
                                var commentId = '#mainBlog-' + id;
                                var chatter = $(dataresult).find(commentId).closest('.chatter-wrapper');
                                if (chatter.length === 0) {
                                    globalFunctions.showMessage("error", translatedResources.CommentNotAllowed);
                                    location.reload();
                                    return;
                                }
                                var existingChatter = $(commentId).closest('.chatter-wrapper');
                                existingChatter.find('.likeWithCount').html(chatter.find('.likeWithCount').html() || '');
                                existingChatter.find('.commentWithCount').html(chatter.find('.commentWithCount').html());
                                existingChatter.find('.comment-list-container').html(chatter.find('.comment-list-container').html() || '');
                                existingChatter.find('.comment-section').html(chatter.find('.comment-section').html() || '');
                                existingChatter.find('.commentText').each(function (t, i) { $(i).html(linkify($(i).text())); });
                                //$(commentId).closest('.chatter-wrapper').html($(result).find(commentId).closest('.chatter-wrapper').html());
                            });
                        }
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },
        likeComment = function (id, blogId, islike) {
            // //debugger;
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/LikeComment",
                    data: { 'id': id, 'blogId': blogId, 'isLike': islike },
                    //async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            loadCommentListForBlog(blogId);
                        }
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },


        load_more = function (e) {
            //debugger;
            var list = $(e).closest('ul').find('.show-more-comments').toggleClass("d-none");
            val = $(e).text();
            $(e).html($(e).attr('data-preval'));
            $(e).attr('data-preval', val);
        },
        likeWallComment = function (id, blogId, islike) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/LikeComment",
                    data: { 'id': id, 'blogId': blogId, 'isLike': islike },
                    //async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            loadWallBlogListForBlog(blogId, function (dataresult) {
                                var commentId = '#mainBlog-' + blogId;
                                var chatter = $(dataresult).find(commentId).closest('.chatter-wrapper');
                                var existingChatter = $(commentId).closest('.chatter-wrapper');
                                var res = existingChatter.find('.comment-list-container').find('.show-more-comments:visible').length === 0;
                                existingChatter.find('.commentWithCount').html(chatter.find('.commentWithCount').html());
                                existingChatter.find('.comment-list-container').html(chatter.find('.comment-list-container').html() || '');
                                existingChatter.find('.comment-section').html(chatter.find('.comment-section').html() || '');
                                existingChatter.find('.commentText').each(function (t, i) { $(i).html(linkify($(i).text())); });
                                if (!res) {
                                    existingChatter.find("#loadMore").trigger("click");
                                }
                                //$(commentId).closest('.chatter-wrapper').html($(result).find(commentId).closest('.chatter-wrapper').html());
                            });
                            //loadWallCommentListForBlog(blogId);
                        }
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },
        CheckWhiteSpace = function (text, id) {
            if (!(text).replace(/\s/g, '').length) {
                $('#' + id).next('[type=submit]').addClass('disabled');
                return false;
            }
            return true;
        },

        PostComment = function (source, id) {
            // debugger;
            if (!this.checkWhiteSpace($("#Comment_" + id).val(), "#Comment_" + id))
                return;
            if (globalFunctions.isValueValid(id)) {

                var token = $('input[name=__RequestVerificationToken]').val();
                var comment = $("#Comment_" + id).val();

                var formDataComment = new FormData();
                formDataComment.append("__RequestVerificationToken", token);
                formDataComment.append("BlogId", id);
                formDataComment.append("Comment", comment);

                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/SaveComment",
                    data: formDataComment,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {

                            $("#Comment_" + id).val('');
                            $(source).addClass('disabled');
                            $(source).closest('.blog-post').toggleClass('blog-post-show-comment');
                            loadBlogListForGroup();

                        }
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },
        PostWallComment = function (source, id) {

            if (!this.checkWhiteSpace($("#Comment_" + id).val(), "Comment_" + id))
                return;
            if (globalFunctions.isValueValid(id)) {

                var token = $('input[name=__RequestVerificationToken]').val();
                var comment = $("#Comment_" + id).val();

                var str = comment.split(" ");
                console.log(str);
                for (var i = 0; i < str.length; i++) {
                    if (_bannedWords.includes(str[i])) {
                        globalFunctions.showWarningMessage(str[i] + translatedResources.CommentNotAllowed);
                        return;
                    }
                }

                var isublish = $("#SApprove_" + id).val();


                console.log(token); console.log(comment);
                var formDataComment = new FormData();
                formDataComment.append("__RequestVerificationToken", token);
                formDataComment.append("BlogId", id);
                formDataComment.append("Comment", comment);
                formDataComment.append("IsPublish", isublish);

                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/SaveComment",
                    data: formDataComment,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", translatedResources.CommentNotAllowed);
                        else {

                            $("#Comment_" + id).val('');
                            $(source).addClass('disabled');
                            $(source).closest('.blog-post').toggleClass('blog-post-show-comment');
                            //issue:#C20D0:1618
                            globalFunctions.showSuccessMessage(translatedResources.CommentPostSuccess);
                            loadWallBlogListForBlog(id, function (dataresult) {

                                var commentId = '#mainBlog-' + id;
                                var chatter = $(dataresult).find(commentId).closest('.chatter-wrapper');
                                if (chatter.length === 0) {
                                    globalFunctions.showMessage("error", translatedResources.CommentNotAllowed);
                                    location.reload();
                                    return;
                                }
                                $.get('/SchoolInfo/Blog/RecentHistory?groupId=' + $('#SectionId').val(), function (data) {
                                    $('.chat-list-card').html(data);
                                    $('.chat-list-card').find(".chatListWrap").mCustomScrollbar({
                                        setHeight: "250px",
                                        autoExpandScrollbar: true,
                                        scrollbarPosition: "outside",
                                        autoHideScrollbar: true
                                    });
                                    $(".activity-list").mCustomScrollbar({
                                        autoExpandScrollbar: true,
                                        scrollbarPosition: "inside",
                                        autoHideScrollbar: true
                                    });
                                });
                                var existingChatter = $(commentId).closest('.chatter-wrapper');
                                existingChatter.find('.commentWithCount').html(chatter.find('.commentWithCount').html());
                                if (existingChatter.find('.comment-list-container').length > 0) {
                                    existingChatter.find('.comment-list-container').html(chatter.find('.comment-list-container').html());
                                    existingChatter.find('.comment-section').html(chatter.find('.comment-section').html() || '');
                                    existingChatter.find('.commentText').each(function (t, i) { $(i).html(linkify($(i).text())); });
                                }
                                else {
                                    existingChatter.find('.card-group').append(chatter.find('.comment-list-container'));
                                    existingChatter.find('.commentText').each(function (t, i) { $(i).html(linkify($(i).text())); });
                                }

                                //$(commentId).closest('.chatter-wrapper').html($(result).find(commentId).closest('.chatter-wrapper').html());
                            });

                        }
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },
        loadCommentListForBlog = function (id) {

            $.ajax({
                type: 'GET',
                url: "/SchoolInfo/blog/GetYoYoBlogComments?blogId=" + id,
                success: function (result) {
                    $("#commentSection_" + id).html("");
                    $("#commentSection_" + id).html(result);
                    loadBlogListForGroup();
                },
                error: function (msg) {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        loadWallCommentListForBlog = function (id) {

            $.ajax({
                type: 'GET',
                url: "/SchoolInfo/blog/GetYoYoBlogComments?blogId=" + id,
                success: function (result) {
                    $("#commentSection_" + id).html("");
                    $("#commentSection_" + id).html(result);
                    loadWallListForGroup();
                },
                error: function (msg) {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        loadWallBlogListForBlog = function (id, callback) {
            $.ajax({
                type: 'POST',
                url: "/SchoolInfo/Blog/GetWallModelOnSearch",
                data: { 'id': id, 'type': 'BLOG' },
                success: function (data) {
                    if (callback) { callback(data); }
                },
                error: function (msg) {
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });
        },
        publishComment = function (source, blogId, id) {
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/publishcomment",
                    data: { 'id': id },
                    //async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            loadBlogListForGroup(); //To update comment count on blog
                        }

                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },

        unPublishComment = function (source, blogId, id) {
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/unpublishcomment",
                    data: { 'id': id },
                    //async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            loadWallListForGroup(); //To update comment count on blog
                        }


                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },

        deleteBlog = function (source, id) {
            // //debugger;

            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        type: 'POST',
                        url: "/SchoolInfo/blog/deleteblog",
                        data: { 'id': id },
                        //async: false,
                        success: function (result) {
                            if (result.Success === false)
                                globalFunctions.showMessage("error", result.Message);
                            else {

                                loadWallListForGroup(); //To update comment count on blog
                                globalFunctions.showSuccessMessage('Blog Deleted Successfully');
                            }

                        },
                        error: function (msg) {
                            //globalFunctions.pageLoadingFrame("hide");
                            globalFunctions.showMessage("error", translatedResources.technicalError);
                        }
                    });
                });
            }

        },



        deleteComment = function (source, blogId, id) {
            // //debugger;
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        type: 'POST',
                        url: "/SchoolInfo/blog/deletecomment",
                        data: { 'id': id },
                        //async: false,
                        success: function (result) {
                            if (result.Success === false)
                                globalFunctions.showMessage("error", result.Message);
                            else {
                                loadBlogListForGroup(); //To update comment count on blog
                            }

                        },
                        error: function (msg) {
                            //globalFunctions.pageLoadingFrame("hide");
                            globalFunctions.showMessage("error", translatedResources.technicalError);
                        }
                    });
                });
            }
        },
        deleteWallComment = function (source, blogId, id) {
            // //debugger;
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        type: 'POST',
                        url: "/SchoolInfo/blog/deletecomment",
                        data: { 'id': id },
                        //async: false,
                        success: function (result) {
                            if (result.Success === false)
                                globalFunctions.showMessage("error", result.Message);
                            else {
                                globalFunctions.showSuccessMessage("Comment Deleted successfully");
                                //loadWallListForGroup(); //To update comment count on blog
                                loadWallBlogListForBlog(blogId, function (dataresult) {
                                    var commentId = '#mainBlog-' + blogId;
                                    var chatter = $(dataresult).find(commentId).closest('.chatter-wrapper');
                                    var existingChatter = $(commentId).closest('.chatter-wrapper');
                                    var res = existingChatter.find('.comment-list-container').find('.show-more-comments:visible').length === 0;
                                    existingChatter.find('.commentWithCount').html(chatter.find('.commentWithCount').html());
                                    existingChatter.find('.comment-list-container').remove();
                                    if (existingChatter.find('.comment-list-container').length > 0) {
                                        existingChatter.find('.comment-list-container').html(chatter.find('.comment-list-container').html());
                                        existingChatter.find('.comment-section').html(chatter.find('.comment-section').html() || '');
                                        existingChatter.find('.commentText').each(function (t, i) { $(i).html(linkify($(i).text())); });
                                        if (!res) {
                                            existingChatter.find("#loadMore").trigger("click");
                                        }
                                    }
                                    else {
                                        existingChatter.find('.card-group').append(chatter.find('.comment-list-container'));
                                        existingChatter.find('.commentText').each(function (t, i) { $(i).html(linkify($(i).text())); });
                                        if (!res) {
                                            existingChatter.find("#loadMore").trigger("click");
                                        }
                                    }
                                    //$(commentId).closest('.chatter-wrapper').html($(result).find(commentId).closest('.chatter-wrapper').html());
                                });
                            }

                        },
                        error: function (msg) {
                            //globalFunctions.pageLoadingFrame("hide");
                            globalFunctions.showMessage("error", translatedResources.technicalError);
                        }
                    });
                });
            }
        },
        deleteChat = function (source, groupId, toUserId) {
            // //debugger;

            globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
            $(document).bind('okToDelete', source, function (e) {
                $.ajax({
                    url: "/Chat/DeleteChatConversation",
                    data: { 'groupId': groupId, 'toUserId': toUserId },
                    type: "GET",
                    success: function (result) {

                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            $('#divChatWindow').empty();
                            globalFunctions.showSuccessMessage("Conversation Deleted successfully");
                            window.location.reload();
                        }

                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            });

        },

        publishWallComment = function (source, blogId, id) {
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/publishcomment",
                    data: { 'id': id },
                    //async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            globalFunctions.showSuccessMessage("Comment publish successfully");
                            loadWallBlogListForBlog(blogId, function (dataresult) {
                                var commentId = '#mainBlog-' + blogId;
                                var chatter = $(dataresult).find(commentId).closest('.chatter-wrapper');
                                var existingChatter = $(commentId).closest('.chatter-wrapper');
                                var res = existingChatter.find('.comment-list-container').find('.show-more-comments:visible').length === 0;
                                existingChatter.find('.commentWithCount').html(chatter.find('.commentWithCount').html());
                                existingChatter.find('.comment-list-container').html(chatter.find('.comment-list-container').html() || '');
                                existingChatter.find('.comment-section').html(chatter.find('.comment-section').html() || '');
                                existingChatter.find('.commentText').each(function (t, i) { $(i).html(linkify($(i).text())); });

                                if (!res) {
                                    existingChatter.find("#loadMore").trigger("click");

                                }
                                //$(commentId).closest('.chatter-wrapper').html($(result).find(commentId).closest('.chatter-wrapper').html());
                            }); //To update comment count on blog
                        }

                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },

        unPublishWallComment = function (source, blogId, id) {
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/unpublishcomment",
                    data: { 'id': id },
                    //async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            globalFunctions.showSuccessMessage("Comment Unpublish successfully");
                            loadWallBlogListForBlog(blogId, function (dataresult) {
                                var commentId = '#mainBlog-' + blogId;
                                var chatter = $(dataresult).find(commentId).closest('.chatter-wrapper');
                                var existingChatter = $(commentId).closest('.chatter-wrapper');
                                var res = existingChatter.find('.comment-list-container').find('.show-more-comments:visible').length === 0;
                                existingChatter.find('.commentWithCount').html(chatter.find('.commentWithCount').html());
                                existingChatter.find('.comment-list-container').html(chatter.find('.comment-list-container').html() || '');
                                existingChatter.find('.comment-section').html(chatter.find('.comment-section').html() || '');
                                existingChatter.find('.commentText').each(function (t, i) { $(i).html(linkify($(i).text())); });

                                if (!res) {
                                    existingChatter.find("#loadMore").trigger("click");

                                }
                                //$(commentId).closest('.chatter-wrapper').html($(result).find(commentId).closest('.chatter-wrapper').html());
                            }); //To update comment count on blog
                        }


                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },


        onShowMessage = function (message) {
            ////debugger;
            globalFunctions.showErrorMessage(message);
            loadPageControlEvent();
        },

        clearForm = function () {

            $("#BlogId").val('');
            //$('#BlogTypeId').val('');
            $('#CategoryId').val('');
            $('#ModerationRequired').prop('checked', false);
            $('#EnableCommentById').val(0);
            $("#Title").val('');
            $("#PostedImage").val('');
            $("#EmbedUrl").val('');
            $("#CloseDiscussionDate").val('');
            $('.selectpicker').selectpicker('refresh');
            $('#PostedImage').fileinput('clear');
            $('#PostedDocuments').fileinput('clear');
            CKEDITOR.instances.Description.setData('');
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $(".blog-editor").slideToggle(250);
                loadBlogListForGroup();
            }
        };

    return {
        init: init,
        loadPageControlEvent: loadPageControlEvent,
        onShowMessage: onShowMessage,
        onSaveSuccess: onSaveSuccess,
        clearForm: clearForm,
        editBlogPopup: editBlogPopup,
        myWallEditBlogPopup: myWallEditBlogPopup,
        editBlogDiscussionClose: editBlogDiscussionClose,
        deleteBlogData: deleteBlogData,
        publishBlogData: publishBlogData,
        unPublishBlogData: unPublishBlogData,
        onGroupSelect: onGroupSelect,
        loadBlogListForGroup: loadBlogListForGroup,
        likeBlog: likeBlog,
        likeComment: likeComment,
        PostComment: PostComment,
        loadCommentListForBlog: loadCommentListForBlog,
        publishComment: publishComment,
        unPublishComment: unPublishComment,
        deleteComment: deleteComment,
        checkWhiteSpace: CheckWhiteSpace,
        likeMyWall: likeMyWall,
        PostWallComment: PostWallComment,
        deleteWallComment: deleteWallComment,
        deleteChat: deleteChat,
        likeWallComment: likeWallComment,
        loadWallCommentListForBlog: loadWallCommentListForBlog,
        loadWallListForGroupById: loadWallListForGroupById,
        loadWallBlogListForBlog: loadWallBlogListForBlog,
        deleteBlog: deleteBlog,
        publishWallComment: publishWallComment,
        unPublishWallComment: unPublishWallComment,
        load_more: load_more
    };
}();

$(document).ready(function () {
    yoYoBlog.init();


    //Landing to chatter with group id from other pages
    var val = $('#SchoolGroupId').val();
    if (globalFunctions.isValueValid(val)) {
        var element = $('#' + val);
        //yoYoBlog.onGroupSelect(element, val);
        $(".groupSelection .dropdown-item").removeClass("active");
        $(element).addClass("active");
        $("#GroupId").val(val);
        $('#groupName').text($(element).children('h4').html().replace(/\s/g, ' ').split('<')[0]);
        $(".blog-editor").hide();

        var objDivPosition = element[0].offsetTop;
        $('.groupWrapper .dropdown-menu').mCustomScrollbar("scrollTo", objDivPosition);
    }

});

$(window).resize(function () {
    postGroupToggle();
});

function postGroupToggle() {
    //blog wall group toggle for mobile
    let groupWrapperWidth = $('.groupWrapper').width();
    let groupWrapperHeight = "calc(100vh - 140px)";
    $('.sticky-group').width(groupWrapperWidth);
    if ($(window).width() < 768) {
        $('.groupSelection').removeClass('show');
        $('.groupSelection .dropdown-menu').removeClass('show position-relative');
        $('.groupSelection .dropdown-menu').addClass("z-depth-3");
        $(".groupWrapper .dropdown-menu").mCustomScrollbar("disable", true);
        //$('.sticky-group').width(groupWrapperWidth);
    } else {
        $('.groupSelection').addClass('show');
        $('.groupSelection .dropdown-menu').addClass('show position-relative');
        $('.groupSelection .dropdown-menu').removeClass("z-depth-3");
        $(".groupWrapper .dropdown-menu").mCustomScrollbar({
            autoHideScrollbar: true,
            autoExpandScrollbar: true,
            scrollbarPosition: "outside",
            setHeight: groupWrapperHeight
        });
        //$('.sticky-group').width(groupWrapperWidth);
    }
}

function disableCategery(id) {
    if (id == '2') {
        $("#CategoryId").parent('.dropdown').addClass("disabled");
        $("#CategoryId").parent('.dropdown').siblings(".select-label").addClass("disabled");
        $('#CategoryId').val('');
        $('.selectpicker').selectpicker('refresh');
    }
    else {
        $("#CategoryId").parent('.dropdown').removeClass("disabled");
        $("#CategoryId").parent('.dropdown').siblings(".select-label").removeClass("disabled");
    }
}

function pageEvents() {
    var nowTemp = new Date();
    var stDate = new Date(nowTemp.getFullYear(2010) - 10, (nowTemp.getMonth()), (nowTemp.getDate() - 15), 0, 0, 0, 0);
    var endDate = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), nowTemp.getDate(), 0, 0, 0, 0);
    //Chatter image uploader
    $('#imgUploader').off('click').on('click', function () {
        ////debugger;
        $('#chatterImgUploader').animate({
            right: '0'
        });

        $('#chatterDocUploader').animate({
            right: '-300'
        });
    });

    $('.closepanel').on('click', function () {
        $('#chatterImgUploader').animate({
            right: '-300'
        });

        $('#chatterDocUploader').animate({
            right: '-300'
        });
    });

    //Chatter document uploader
    $('#docUploader').off('click').on('click', function () {
        ////debugger;
        $('#chatterDocUploader').animate({
            right: '0'
        });

        $('#chatterImgUploader').animate({
            right: '-300'
        });

    });


    var profileImg = $("#profileImage");
    if (profileImg.length > 0) {
        $("#blogAuthor").attr("src", profileImg.attr("src"));
    }

    $('.blogList').off('click', '.blog-post > .card-footer > .input-placeholder, .blog-post > .blog-post-comment > .blog-post-textarea > button[type="reset"]').on('click', '.blog-post > .card-footer > .input-placeholder, .blog-post > .blog-post-comment > .blog-post-textarea > button[type="reset"]', function (event) {
        var $panel = $(this).closest('.blog-post');
        $comment = $panel.find('.blog-post-comment');
        $comment.find('.btn:first-child').addClass('disabled');
        $comment.find('textarea').val('');
        $panel.toggleClass('blog-post-show-comment');
        if ($panel.hasClass('blog-post-show-comment')) {
            $comment.find('textarea').focus();
        }
        $("#commentAuthor").attr("src", profileImg.attr("src"));

    });
    $('.blog-post-comment > .blog-post-textarea > textarea').off('keyup').on('keyup', function (event) {
        var $comment = $(this).closest('.blog-post-comment');

        $comment.find('button[type="submit"]').addClass('disabled');
        if ($(this).val().length >= 1) {
            $comment.find('button[type="submit"]').removeClass('disabled');
        }
    });
    $(".expand-blog-editor").off("click").on("click", function () {
        $(".blog-editor").slideToggle(250);
        //CKEDITOR.config.height = '180px';
        $('#CloseDiscussionDate').datetimepicker({
            format: 'DD-MMM-YYYY',
            minDate: stDate
        });
        CKEDITOR.replace('Description', {
            htmlencodeoutput: false,
            height: '180px'
        });

        //disableCategery($("#BlogTypeId").val());
        //$("a#cke_28").hide();
    });

    //$('#BlogTypeId').off('change').on('change', function () {
    //    var selected = $(this).val();
    //    disableCategery(selected);
    //});

    if ($(window).width() > 768) {
        //to make the group list section fixed on scroll
        var fixmeTop = $('.sticky-group').offset() != undefined ? $('.sticky-group').offset().top : 0;
        $(window).scroll(function () {
            var currentScroll = $(window).scrollTop();
            if (currentScroll >= fixmeTop) {
                $('.sticky-group').css({
                    position: 'fixed',
                    top: '80px',
                    //left: '0'
                });
            } else {
                $('.sticky-group').css({
                    position: 'static'
                });
            }
        });
    }

    $("#PostedImage").fileinput({
        language: translatedResources.locale,
        title: translatedResources.DragAndDropMessage,
        theme: "fas",
        showUpload: true,
        showCaption: false,
        showRemove: true,
        showCancel: false,
        browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse btn-file btn-file",
        //browseClass: "btn btn-outline-primary rounded btn-sm m-0 z-depth-0 btn-browse",
        removeClass: "btn btn-sm m-0 z-depth-0",
        uploadClass: "btn btn-sm m-0 z-depth-0",
        minFileCount: 1,
        maxFileCount: 1,
        overwriteInitial: false,
        allowedFileExtensions: JSON.parse($("#AllowedImageExtension").val()),
        maxFileSize: translatedResources.fileSizeAllow,
        fileActionSettings: {
            showZoom: false,
            showUpload: false,
            //indicatorNew: "",
            showDrag: true
        }
    });

    $("#PostedDocuments").fileinput({
        language: translatedResources.locale,
        theme: "fas",
        showUpload: false,
        showCaption: false,
        showMultipleNames: true,
        showCancel: false,
        allowedFileExtensions: JSON.parse($("#AllowedFileExtension").val()),
        minFileCount: 1,
        maxFileCount: 10,
        browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse btn-file btn-file",
        //browseClass: "btn btn-outline-primary rounded btn-sm m-0 z-depth-0 btn-browse",
        //removeClass: "btn btn-sm m-0 z-depth-0",
        uploadClass: "btn btn-sm m-0 z-depth-0",
        maxFileSize: 204800,
        uploadAsync: true,
        overwriteInitial: false,
        fileActionSettings: {
            showZoom: false,
            showUpload: false,
            //indicatorNew: "",
            showDrag: true
        },
        initialPreviewAsData: true, // defaults markup
        initialPreviewConfig: [{
            frameAttr: {
                title: 'My Custom Title',
            }
        }],
        preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
        previewFileIconSettings: { // configure your icon file extensions
            'doc': '<i class="fas fa-file-word ic-file-word"></i>',
            'xls': '<i class="fas fa-file-excel ic-file-excel"></i>',
            'xlsx': '<i class="fas fa-file-excel ic-file-excel"></i>',
            'ppt': '<i class="fas fa-file-powerpoint ic-file-ppt"></i>',
            'pptx': '<i class="fas fa-file-powerpoint ic-file-ppt"></i>',
            'pdf': '<i class="fas fa-file-pdf ic-file-pdf"></i>',
            'txt': '<i class="fas fa-file-alt ic-file"></i>',
            'htm': '<i class="fas fa-file-code ic-file"></i>',
            'csv': '<i class="fas fa-file-csv ic-file-csv"></i>'
        },
        previewSettings: {
            image: { width: "50px", height: "auto" },
            html: { width: "50px", height: "auto" },
            other: { width: "50px", height: "auto" }
        },
        previewFileExtSettings: { // configure the logic for determining icon file extensions
            'doc': function (ext) {
                return ext.match(/(doc|docx)$/i);
            },
            'xls': function (ext) {
                return ext.match(/(xls|xlsx)$/i);
            },
            'ppt': function (ext) {
                return ext.match(/(ppt|pptx)$/i);
            },
            'txt': function (ext) {
                return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
            }
        }
    });

    $(".fileinput-upload-button").click(function () {
        $('.file-upload-widget').animate({
            right: '-300'
        });
    });

    $(".btn.submit").off("click").on("click", function () {
        //debugger;
        var blogId = $("#BlogId").val();
        var schoolId = $("#SchoolId").val();
        var isAddMode = $("#IsAddMode").val();
        var blogTypeId = $('#BlogTypeId').val();
        var categoryId = $('#CategoryId option:selected').val();
        //var groupId = $('#BlogEdit_GroupId option:selected').val();
        var groupId = $('#GroupId').val();
        var enableCommentId = $('#EnableCommentById option:selected').val();
        var embedUrl = $("#EmbedUrl").val();
        var title = $("#Title").val();
        var description = CKEDITOR.instances.Description.getData();
        var publish = $(this).data("publish");

        var imageFile = $("#PostedImage").get(0).files;
        var docFiles = $("#PostedDocuments").get(0).files;
        var moderationRequired = $('#ModerationRequired').prop('checked');
        var CloseDiscussionDate = $('#CloseDiscussionDate').val();
        var token = $('input[name=__RequestVerificationToken]').val();
        ////debugger;
        console.log(enableCommentId);

        //if (blogTypeId == "") {
        //    yoYoBlog.onShowMessage(translatedResources.blogtypemessage);
        //    return;
        //}   else


        if (groupId == "") {
            yoYoBlog.onShowMessage(translatedResources.groupmessage);
            return;
        }
        else if (title.trim() == '') {
            yoYoBlog.onShowMessage(translatedResources.titlemessage);
            return;
        }
        else if (!description.replace(/<[^>]*>/gi, '').length) {
            yoYoBlog.onShowMessage(translatedResources.descriptionmessage);
            return;
        }
        else if (embedUrl.trim() !== '') {
            var reg = new RegExp($("#EmbedUrl").data("val-regex-pattern"));
            //console.log(reg);
            if (reg.test(embedUrl) === false) {
                yoYoBlog.onShowMessage(translatedResources.embedUrlMessage);
                return;
            }
        }

        var formData = new FormData();
        formData.append("__RequestVerificationToken", token);
        formData.append("BlogId", blogId);
        formData.append("SchoolId", schoolId);
        formData.append("IsAddMode", isAddMode);
        formData.append("BlogTypeId", blogTypeId);
        formData.append("CategoryId", categoryId);
        formData.append("GroupId", groupId);
        formData.append("EnableCommentById", enableCommentId);
        formData.append("PostedImage", imageFile[0]);
        formData.append("ModerationRequired", moderationRequired);
        formData.append("CloseDiscussionDate", CloseDiscussionDate);
        for (var i = 0; i < docFiles.length; i++) {
            formData.append("PostedDocuments", docFiles[i]);
        }

        formData.append("Title", title.trim());
        formData.append("EmbedUrl", embedUrl);
        formData.append("IsPublish", publish == '1' ? "true" : "false");
        formData.append("Description", $("<div />").text(description).html());
        // formData.append("RegistrationDate", CloseDiscussionDate); //temp aasude date ek  model madun remove kar ok

        var bannedWordCheck = globalFunctions.validateBannedWordsFormData('blogForm');
        if (!bannedWordCheck)
            return;

        $.ajax({
            url: '/SchoolInfo/Blog/SaveBlogData',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.Success == true) {
                    yoYoBlog.onSaveSuccess(data);
                    yoYoBlog.clearForm();

                }
                else if (data.Success == false) {
                    globalFunctions.showErrorMessage(translatedResources.ErrorMessage);
                }
            },
            error: function (data, xhr, status) {
                ////debugger;
                globalFunctions.showErrorMessage();
            },
        });

    });

    //For svg images init
    $('img.svg').each(function () {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });

    $("#searchGroup").off("keyup").on("keyup", function () {
        ////debugger;
        var value = $(this).val().toLowerCase();
        $(".search-item").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    });
    $('.search-toggle').off("click").on('click', function () {
        $('.search-wrapper').slideToggle('fast');
        $('.search-wrapper input').focus();
    });



}

$(document).ready(function () {
  
})