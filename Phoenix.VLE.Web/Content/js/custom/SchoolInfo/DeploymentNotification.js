﻿var schoolNotificationFunctions = function () {
    var initDeploymentNotificationForm = function (source, id) {
        var popupHeader = globalFunctions.isValueValid(id) ? translatedResources.editDeploymentNotification : translatedResources.addDeploymentNotification;
        globalFunctions.loadPopup($(source), "/SchoolInfo/DeploymentNotification/InitDeploymentNotificationForm?id=" + id, popupHeader);
    },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal("hide");
                $("#btnAddeditnotification").remove()
                refreshGrid();
            }
        },

        saveDeploymentNotification = function () {
            var message = $("#NotificationMessage").val().trim();
            if (!globalFunctions.isValueValid(message)) {
                $("#NotificationMessage").val('');
            }
            $("form#frmDeploymentNotification").submit();
        }

    refreshGrid = function () {
        $("#divSchoolNotificationList").load("/SchoolInfo/DeploymentNotification/GetDeploymentNotifications");
    };

    return {
        initDeploymentNotificationForm: initDeploymentNotificationForm,
        onSaveSuccess: onSaveSuccess,
        saveDeploymentNotification: saveDeploymentNotification
    }
}();
