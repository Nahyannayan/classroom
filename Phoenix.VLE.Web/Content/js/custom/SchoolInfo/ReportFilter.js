﻿$(document).ready(function () {
    //debugger

    $('ul#userProfilePartial li:first-child').remove();

    var selectedSchoolId = $("#hdnSelectedSchoolId").val();
    var schoolName = $("select[name=SchoolId] option[value='"+selectedSchoolId + "']").text();
    $('.bootstrap-select .filter-option .filter-option-inner-inner').text(schoolName);
    $('select[name=SchoolId]').val(selectedSchoolId);

    var nowTemp = new Date();
    //var stDate = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), (nowTemp.getDate() - 15), 0, 0, 0, 0);
    //var endDate = new Date(nowTemp.getFullYear(), (nowTemp.getMonth()), nowTemp.getDate(), 0, 0, 0, 0);
    
    
    var _stDate = $("#hdnStartDate").val();
    var _tmpStartDate = _stDate.split('/');
    var _edDate = $("#hdnEndDate").val();
    var _tmpEndDate = _edDate.split('/');

    var stDate = new Date(nowTemp.getFullYear(), parseInt(_tmpStartDate[0]) - 1, parseInt(_tmpStartDate[1]), 0, 0, 0, 0);
    var endDate = new Date(nowTemp.getFullYear(), parseInt(_tmpEndDate[0]) - 1, parseInt(_tmpEndDate[1]), 0, 0, 0, 0);

    var StartDate = $('#FromDate').datetimepicker({
        format: 'DD-MMM-YYYY',
        // minDate: (new Date()-90),
        maxDate: endDate,
        locale: translatedResources.locale,
        date: stDate
    }).on('dp.change', function (ev) {
        console.log(EndDate.viewDate().valueOf());
        console.log(ev.date.valueOf());
        if (ev.date.valueOf() > EndDate.viewDate().valueOf()) {
            globalFunctions.showWarningMessage('@ResourceManager.GetString("Planner.MyPlanner.StartTimeWarning")');
            $('#FromDate').data("DateTimePicker").date(ev.oldDate);
        }
        else if (ev.date.valueOf() < EndDate.viewDate().valueOf()) {
            var maxDt = new Date(ev.date);
            maxDt.setDate(maxDt.getDate() + 90)
            //console.log(maxDt);
            if (maxDt.valueOf() <= EndDate.viewDate().valueOf()) {
                $('#ToDate').data("DateTimePicker").maxDate(maxDt);

            }
            else {
                $('#ToDate').data("DateTimePicker").maxDate(endDate);
            }
        }
    }).data("DateTimePicker");


    var EndDate = $('#ToDate').datetimepicker({
        format: 'DD-MMM-YYYY',
        maxDate: endDate,
        locale: translatedResources.locale,
        date: endDate
    }).on('dp.change', function (ev) {

        if (ev.date.valueOf() < StartDate.viewDate().valueOf()) {
            globalFunctions.showWarningMessage('@ResourceManager.GetString("Planner.MyPlanner.EndDateWarning")');
            $('#ToDate').data("DateTimePicker").date(ev.oldDate);
        }
        else if (ev.date.valueOf() <= StartDate.viewDate().valueOf()) {

            var minDt = new Date(ev.date);
            minDt.setDate(minDt.getDate() - 90)
            //console.log(minDt);
            if (minDt.valueOf() >= StartDate.viewDate().valueOf()) {
                $('#FromDate').data("DateTimePicker").minDate(minDt);
                $('#ToDate').data("DateTimePicker").minDate(minDt);
            }
            else {
                $('#FromDate').data("DateTimePicker").minDate(stDate);
                $('#ToDate').data("DateTimePicker").minDate(stDate);
            }
        }
    }).data("DateTimePicker");
    //$("#FromDate").val(globalFunctions.getFormattedDate(stDate));
    //$("#ToDate").val(globalFunctions.getFormattedDate(endDate));


});