﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
//var popoverEnabler = new LanguageTextEditPopover();
var schoolTheme = function () {

    var init = function () {
        loadThemeSettingGrid();
    },

        loadAssignThemePopup = function (source, mode, id) {
            var title = mode;
            globalFunctions.loadPopup(source, '/SchoolInfo/Themes/InitAssignThemeForm?id=' + id, title, 'student-theme-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $('.selectpicker').selectpicker('refresh');

                //formElements.feSelect();
                //popoverEnabler.attachPopover();
            });
        },
        assignThemePopup = function (source) {
            loadAssignThemePopup(source, translatedResources.AssignTheme);
        },

        editThemePopup = function (source, id) {
            loadAssignThemePopup(source, translatedResources.edit, id);
        },

        deleteUserRoleData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('UserRole', '/Users/UserRole/DeleteUserRoleData', 'tbl-UserRole', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        loadThemeSettingGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "SchoolGradeName", "sClass": "text-center", "sTitle": translatedResources.schoolGradeName, "sWidth": "20%" },
                { "mData": "ThemeName", "sTitle": translatedResources.ThemeName, "sWidth": "35%" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.Actions, "sWidth": "10%" }
            );
            initThemeSettingGrid('tbl-Theme', _columnData, '/SchoolInfo/Themes/LoadThemeSettingGrid?curriculumId=' + $("#ddlCurriculumId").val());
        },

        initThemeSettingGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                processing: true,
                searching: true,
                simpleGrid: true,
                pageSize: 10
            };
            grid.init(settings);
        };

    return {
        init: init,
        assignThemePopup: assignThemePopup,
        editThemePopup: editThemePopup,
        onSaveSuccess: onSaveSuccess,
        loadThemeSettingGrid: loadThemeSettingGrid
    };
}();

$(document).ready(function () {
    schoolTheme.init();
    $('.selectpicker').selectpicker({ size: 10 });
    $(document).on('click', '#btnAssignTheme', function () {
        schoolTheme.assignThemePopup($(this));

    });


    //$("#tbl-UserRole_filter").hide();
    $('#tbl-Theme').DataTable().search('').draw();
    $("#ThemeSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-Theme').DataTable().search($(this).val()).draw();
    });
    $(document).on('change', '#ThemeId', function () {
        var themeId = $(this).val();
        if (globalFunctions.isValueValid(themeId)) {
            $.ajax({
                url: '/SchoolInfo/Themes/GetPreviewImage?themeId=' + themeId,
                success: function (response) {
                    $(".divThemePreview").removeClass("d-none");
                    $('.themePreview').attr('href', response.PreviewURL);
                    $('#themePreviewImg').attr('src', response.PreviewURL);
                    $('#themePreviewDownload').attr({ 'download': response.PreviewURL, "href": response.PreviewURL });

                }
            });
        }
        else
            $(".divThemePreview").addClass("d-none");
    });
});



