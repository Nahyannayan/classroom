﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var schools = function () {

    var init = function () {
        loadSchoolGrid();
    },        

        loadSchoolGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "SchoolName", "sTitle": translatedResources.schoolName, "sWidth": "80%" },               
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initSchoolGrid('tbl-SchoolList', _columnData, '/SchoolInfo/SchoolList/LoadSchoolGrid');
        },

        openUserListPage = function (id) {
            if (globalFunctions.isValueValid(id)) {
                $.post('/Shared/Shared/SetSession', { key: "CurrentSchoolId", value: id }, function (data) {
                    if (typeof id != 'undefined' && data == true)
                        window.open('/Users/UserList', '_blank');
                    else {
                        globalFunctions.onFailure();
                    }
                });
            }
        },

        initSchoolGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnDefs: [{
                    'orderable': false,
                    'targets': 1
                }],
                columnSelector: false
            };
            grid.init(settings);
        };

    return {
        init: init,
        loadSchoolGrid: loadSchoolGrid,
        openUserListPage: openUserListPage
    };
}();

$(document).ready(function () {
    schools.init();
    $("#tbl-SchoolList_filter").hide();
    $('#tbl-SchoolList').DataTable().search('').draw();
    $("#SchoolListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-SchoolList').DataTable().search($(this).val()).draw();
    });
});



