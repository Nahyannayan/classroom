﻿
var schoolReport = function () {
    var init = function () {

    },
        loadGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Name", "sTitle": translatedResources.title, "sWidth": "20%" },
                { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "40%" },
                { "mData": "FileName", "sTitle": translatedResources.fileName, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "bSorting": false, "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initGrid('tblReportLists', _columnData, '/SchoolInfo/SchoolReport/LoadReportList');
        },

        initGrid = function (_tableId, _columnData, _url) {

            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [3]
                }],
                searching: true
            };
            grid.init(settings);

            $("#tblReportLists_filter").hide();
            $('#tblReportLists').DataTable().search('').draw();
            $("#searchReportLists").on("input", function (e) {
                e.preventDefault();
                $('#tblReportLists').DataTable().search($(this).val()).draw();
            });
        },
        deleteSchoolReport = function (id) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                $.get('/SchoolInfo/SchoolReport/DeleteSchoolReport', { id })
                    .then(response => {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        if (response.Success) {
                            loadGrid();
                        }
                    })
            })
        }
    return {
        init,
        loadGrid,
        initGrid,
        deleteSchoolReport
    }
}();

var viewReport = function () {
    init = function () {
        $('.selectpicker').selectpicker('refresh');
        $('.date-picker').each((index, element) => {
            const $el = $(element)
            let options = {
                format: 'DD-MMM-YYYY',
                minDate: '2000-01-01'
            }
            if ($el.attr('data-format')) {
                options.format = $el.attr('data-format')
                $el.removeAttr('data-format')
            }
            if ($el.attr('data-view-mode')) {
                options['viewMode'] = $el.attr('data-view-mode')
                $el.removeAttr('data-view-mode')
            }
            $el.datetimepicker(options)
        });
        setTimeout(() => {
            $('#filtersDiv').find("select.selectpicker").each(function () {
                var mainDiv = $(this).closest("div.sort-dropdown");
                var label = mainDiv.find("label.select-label");
                var labelWidth = label.width();
                if (labelWidth > 60) {
                    var extraWidth = labelWidth - 60;
                    var finalWidth = Math.round(labelWidth + extraWidth) + 5;
                    mainDiv.find("button.dropdown-toggle").attr("style", "padding-left: " + finalWidth + "px");
                }
            })
        }, 800);
    },
        bindReportDetailsDropDown = function (id) {
            $('#filtersDiv').html('');
            if ($(".field-form:visible").length > 0) {
                $(".btn-filter").click();
            }
            common.bindSingleDropDownByParameter('#ddlReport', '/SchoolInfo/SchoolReport/GetReportDetailsUsingModuleId', { id }, '', translatedResources.selectedText, false, 'ItemId', 'ItemName');
        },
        bindReportFilters = function (reportDetailId) {
            $('#filtersDiv').html('');
            if (globalFunctions.isValueValid(reportDetailId)) {
                $.get('/SchoolInfo/SchoolReport/GetReportFilters', { reportDetailId })
                    .then(response => {
                        $('#filtersDiv').html(response);
                        viewReport.init();

                        var element = $('#filtersDiv').find('select:first');

                        if (element.length > 0) {
                            var id = element.attr("id");
                            var filterCode = element.data('filtercode');
                            var nextElement = element.data('nextdropdown');
                            var filterParameter = getSelectDropdownParameterAndValue();
                            common.bindSingleDropDownByParameter("#" + id, '/SchoolInfo/SchoolReport/GetReportFilter', { filterCode, filterParameter }, '', translatedResources.selectedText, false, 'value', 'Name', true);
                            setDropDownValueAndTriggerChange(id);
                        } else {
                            var element = $('#filtersDiv').find('input[type!="hidden"]:first');
                            if (element.is('input') && (element.attr('type') === 'checkbox' || element.attr('type') === 'radio')) {
                                var filterCode = element.data('filtercode');
                                var nextElement = element.data('nextdropdown');
                                var filterParameter = getSelectDropdownParameterAndValue();
                                var parentDiv = element.closest('div[data-filter]');
                                var controlCode = parentDiv.data('filter');
                                $.post('/SchoolInfo/SchoolReport/GetReportFilterControl', { filterCode, filterParameter, controlCode })
                                    .then(response => {
                                        parentDiv.html(response);
                                        setDropDownValueAndTriggerChange(nextElement);
                                        $('[data-toggle="toggle"]').bootstrapToggle()
                                    });
                            }
                        }
                        if ($(".field-form:hidden").length > 0)
                            $(".btn-filter").click();
                        $('#documentViewerDiv').fadeOut()
                    });
            }
        },
        setDropDownValueAndTriggerChange = function (dropdownId) {
            var dropdown = $(`[name="${dropdownId}"`);
            if (dropdown.is('select')) {
                dropdown.find("option:contains('" + translatedResources.selectedText + "')").remove();
                if (dropdown.find("option").length > 0) {
                    dropdown.val(dropdown.find('option:eq(0)').val()).trigger('change');
                    dropdown.selectpicker('refresh');
                }
            } else if (dropdown.is('input') && dropdown.hasClass("date-picker")) {
                dropdown.data("DateTimePicker").date(new Date()).maxDate(new Date())
            } else if (dropdown.is('input') && dropdown.hasClass("custom-control-input") && (dropdown.attr('type') === 'checkbox' || dropdown.attr('type') === 'radio')) {
                dropdown.trigger('change');
            }
        },
        loadNextDropDownData = function (element, event) {
            var element = $(element);
            var nextElement = element.data('nextdropdown');
            var nextElementId = $(`[name="${element.data('nextdropdown')}"`);

        if (nextElementId.is('select')) {
            var filterCode = element.data('nextdropdowncode');
            var filterParameter = getSelectDropdownParameterAndValue();
            common.bindSingleDropDownByParameter(nextElementId, '/SchoolInfo/SchoolReport/GetReportFilter', { filterCode, filterParameter }, '', translatedResources.selectedText, false, 'value', 'Name', true);
        } else if (nextElementId.is('input') && (nextElementId.attr('type') === 'checkbox' || nextElementId.attr('type') === 'radio')) {
            var filterCode = element.data('nextdropdowncode');
            var filterParameter = getSelectDropdownParameterAndValue();
            var parentDiv = nextElementId.closest('div[data-filter]');
            var controlCode = parentDiv.data('filter');
            $.post('/SchoolInfo/SchoolReport/GetReportFilterControl', { filterCode, filterParameter, controlCode })
                .then(response => {
                    parentDiv.html(response);
                    setDropDownValueAndTriggerChange(nextElement);
                    $('[data-toggle="toggle"]').bootstrapToggle()
                });
        } else {
            
        }
        setDropDownValueAndTriggerChange(nextElement);
        },
        getCurriculumValue = function () {
            return $('#CurriculumId').val();
        },
        getSelectDropdownParameterAndValue = function () {
            var filterParameter = 'CurriculumId:' + getCurriculumValue();
            var paramValue = Array.from($('select[id^="ctr_"]')).map(function (e) {
                var ele = $(e);
                if (globalFunctions.isValueValid(ele.val())) {
                    // to check if value is array the join else get direct value 
                    var value = Array.isArray(ele.val()) ? ele.val().join('#') : ele.val()

                    var param = ele.data('filtercode') + ":" + value;
                    return param;
                } else {
                    return "";
                }
            }).filter(x => globalFunctions.isValueValid(x)).join("|");
            if (paramValue.length > 0)
                filterParameter = filterParameter + "|" + paramValue;
            return filterParameter;
        },
        getParameterValues = function () {
        var parameterNValue = [];
            $('input[name^="ctr_"][type="text"],select[name^="ctr_"]').each(function (e) {
                var ele = $(this);
                // to check if value is array the join else get direct value 
                var value = Array.isArray(ele.val()) ? ele.val().join(',') : ele.val()
                parameterNValue.push({ parameter: ele.data('spparameter'), value: value });
                //return `${ele.data('spparameter')} = '${value}'`
            })
        $('input[name^="ctr_"][type="checkbox"],input[name^="ctr_"][type="radio"]').each(function () {
            var ele = $(this);
            //to push data for Checked controls or for the switch
            if (ele.prop('checked') || globalFunctions.isValueValid(ele.data('toggle'))) {
                var object = {};
                // to check if value is array the join else get direct value 
                var value = Array.isArray(ele.val()) ? ele.val().join(',') : ele.val();
                var parameter = ele.data('spparameter');
                if (globalFunctions.isValueValid(ele.data('toggle'))) {
                    value = ele.prop('checked') ? 1 : 2;
                }

                if (parameterNValue.some(x => x.parameter == parameter)) {
                    object = parameterNValue.find(x => x.parameter == parameter);
                    object.value = object.value + "," + value;
                } else {
                    object["parameter"] = parameter;
                    object["value"] = value;
                    parameterNValue.push(object);
                }
            }
        })

        var paramValue = parameterNValue.map(function (e) { return `${e.parameter} = '${e.value}'`; }).join(',');
        console.log(paramValue);
            return paramValue;

        },
        loadReport = function () {
            var object = {
                ReportDetailId: $('#ddlReport').val(),
                ReportParamterListString: getParameterValues(),
                IsCrystalReport: $('#IsCr').val()
            };
            $('#documentViewerDiv').fadeOut();
            $.post('/SchoolInfo/SchoolReport/GetReportToView', { requestModel: object })
                .then(response => {
                    if (response.Success === false) {
                        $('#reportsDiv').fadeOut();
                        $('#reportsDataNotPresent').fadeIn();
                    } else {
                        $('#reportsDiv').fadeIn();
                        $('#reportsDataNotPresent').fadeOut();
                        $('#reportsIframe').attr('src', response);
                    }
                })
                .catch(e => globalFunctions.onFailure());

        }
    return {
        init,
        bindReportDetailsDropDown,
        bindReportFilters,
        setDropDownValueAndTriggerChange,
        getCurriculumValue,
        loadNextDropDownData,
        getSelectDropdownParameterAndValue,
        getParameterValues,
        loadReport
    }
}();

$(document).on('dp.change', 'input[data-mindateid],input[data-maxdateid]', function (e) {
    try {
        if (globalFunctions.isValueValid($(this).data('mindateid'))) {
            var minDate = $(`#${$(this).data('mindateid')}`);
            minDate.data("DateTimePicker").maxDate(e.date);
            $(this).data("DateTimePicker").maxDate(new Date());
        }

        else if (globalFunctions.isValueValid($(this).data('maxdateid'))) {
            $(`#${$(this).data('maxdateid')}`).data("DateTimePicker").minDate(e.date);
            $(`#${$(this).data('maxdateid')}`).data("DateTimePicker").maxDate(new Date());
        }
    } catch (e) {

    }

})