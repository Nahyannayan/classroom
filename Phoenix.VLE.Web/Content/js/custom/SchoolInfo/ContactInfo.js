﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();

var contactinfo = function () {
    var init = function () {
        formElements.feSelect();
    },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        };

    return {
        init: init,
        onSaveSuccess: onSaveSuccess
    };
}();

$(document).ready(function () {
    contactinfo.init();
});