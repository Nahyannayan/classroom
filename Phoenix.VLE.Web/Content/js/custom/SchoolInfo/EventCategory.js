﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();
var eventCategory = function () {

    var init = function () {
        loadEventCategoryGrid();
        $("#tbl-EventCategory_filter").hide();
    },
        loadEventCategoryGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Name", "sTitle": translatedResources.eventCategory, "sWidth": "20%", "sClass": "wordbreak" },
                { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "20%", "sClass": "wordbreak" },
                { "mData": "ColorCode", "sTitle": translatedResources.colorCode, "sWidth": "20%", "sClass": "open-details-control wordbreak" },
                { "mData": "IsActive", "sTitle": translatedResources.status, "sWidth": "20%", "sClass": "open-details-control wordbreak" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initEventCategoryGrid('tbl-EventCategory', _columnData, '/SchoolInfo/EventCategory/LoadEventCategoryGrid');

        },
        addEventCategoryPopup = function (source) {
            loadEventCategoryPopup(source, translatedResources.add);
        },

        editEventCategoryPopup = function (source, id) {
            loadEventCategoryPopup(source, translatedResources.edit, id);
        },
        loadEventCategoryPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.eventCategory;
            globalFunctions.loadPopup(source, '/SchoolInfo/EventCategory/InitAddEditEventCategoryForm?id=' + id, title, 'suggestion-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $(".colorpicker").minicolors({
                    theme: 'bootstrap',
                    control: 'hue',
                    format: 'hex'
                });

                $("#tbl-EventCategory_filter").hide();
                $('#tbl-EventCategory').DataTable().search('').draw();
                $("#EventCategorySearch").on("input", function (e) {
                    e.preventDefault();
                    $('#tbl-EventCategory').DataTable().search($(this).val()).draw();
                });

                popoverEnabler.attachPopover();
            });
        },
        deleteEventCategory = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('EventCategory', '/SchoolInfo/EventCategory/DeleteEventCategoryData', 'tbl-EventCategory', source, id);
            }
        },

        onSaveSuccess = function (e) {
            if (e.Success == true) {
                $("#myModal").modal('hide');
                globalFunctions.showMessage(e.NotificationType, e.Message);
                loadEventCategoryGrid();
                $("#tbl-EventCategory_filter").hide();
            }
        },
        initEventCategoryGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false
            };
            grid.init(settings);
        };

    return {
        init: init,
        addEventCategoryPopup: addEventCategoryPopup,
        editEventCategoryPopup: editEventCategoryPopup,
        onSaveSuccess: onSaveSuccess,
        deleteEventCategory: deleteEventCategory,
        loadEventCategoryGrid: loadEventCategoryGrid
    };
}();

$(document).ready(function () {
    eventCategory.init();
    $(document).on('click', '#btnAddEventCategory', function () {
        eventCategory.addEventCategoryPopup($(this));
    });


    $("#tbl-EventCategory_filter").hide();
    $('#tbl-EventCategory').DataTable().search('').draw();
    $("#EventCategorySearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-EventCategory').DataTable().search($(this).val()).draw();
    });
});



