﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var blogComment = function () {
    var init = function () {
        //refreshCommentSection();
        $('.blog-post > .card-footer > .input-placeholder, .blog-post > .blog-post-comment > .blog-post-textarea > button[type="reset"]').off("click").on('click', function (event) {
            var $panel = $(this).closest('.blog-post');
            $comment = $panel.find('.blog-post-comment');
            $comment.find('.btn:first-child').addClass('disabled');
            $comment.find('textarea').val('');
            $panel.toggleClass('blog-post-show-comment');
            if ($panel.hasClass('blog-post-show-comment')) {
                $comment.find('textarea').focus();
            }
        });
        $('.blog-post-comment > .blog-post-textarea > textarea').off('keyup').on('keyup', function (event) {
            var $comment = $(this).closest('.blog-post-comment');

            $comment.find('button[type="submit"]').addClass('disabled');
            if ($(this).val().length >= 1) {
                $comment.find('button[type="submit"]').removeClass('disabled');
            }
        });
    },
    onCommentSaveSuccess = function (data) {
        globalFunctions.showMessage(data.NotificationType, data.Message);
        if (data.Success) {
            $("#Comment").val('');
            $('.blog-post').toggleClass('blog-post-show-comment');
            $('#btnSubmitComment').addClass('disabled');
            refreshCommentSection();
        }
    },
        refreshCommentSection = function () {
            //debugger;
            var section = $("#commentSection");
            var blogId = section.attr("data-blogId");
            $.ajax({
                type: 'GET',
                url: "/SchoolInfo/blog/getblogcomments?blogId=" + blogId,
                async: false,
                success: function (result) {
                    $("#commentSection").html("");
                    $("#commentSection").html(result);
                },
                error: function (msg) {
                   
                    globalFunctions.showMessage("error", translatedResources.technicalError);
                }
            });

        },

        likeComment = function (id, islike) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/LikeComment",
                    data: { 'id': id, 'isLike': islike },
                    async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            refreshCommentSection();
                        }
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },

        publishComment = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/publishcomment",
                    data: { 'id': id },
                    async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            refreshCommentSection();
                        }

                        setTimeout(function () {
                            //globalFunctions.pageLoadingFrame("hide");
                        }, 100);
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },
        unPublishComment = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                $.ajax({
                    type: 'POST',
                    url: "/SchoolInfo/blog/unpublishcomment",
                    data: { 'id': id },
                    async: false,
                    success: function (result) {
                        if (result.Success === false)
                            globalFunctions.showMessage("error", result.Message);
                        else {
                            refreshCommentSection();
                          
                        }

                        setTimeout(function () {
                            //globalFunctions.pageLoadingFrame("hide");
                        }, 100);
                    },
                    error: function (msg) {
                        //globalFunctions.pageLoadingFrame("hide");
                        globalFunctions.showMessage("error", translatedResources.technicalError);
                    }
                });
            }
        },
        deleteComment = function (source, id) {
            // debugger;
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm); 
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        type: 'POST',
                        url: "/SchoolInfo/blog/deletecomment",
                        data: { 'id': id },
                        async: false,
                        success: function (result) {
                            if (result.Success === false)
                                globalFunctions.showMessage("error", result.Message);
                            else {
                                refreshCommentSection();
                            }

                            setTimeout(function () {
                                //globalFunctions.pageLoadingFrame("hide");
                            }, 100);
                        },
                        error: function (msg) {
                            //globalFunctions.pageLoadingFrame("hide");
                            globalFunctions.showMessage("error", translatedResources.technicalError);
                        }
                    });
                });
            }
        }

    return {
        init: init,
        onCommentSaveSuccess: onCommentSaveSuccess,
        publishComment: publishComment,
        unPublishComment: unPublishComment,
        deleteComment: deleteComment,
        refreshCommentSection: refreshCommentSection,
        likeComment: likeComment
    };

}();

$(document).ready(function () {
    blogComment.init();
});