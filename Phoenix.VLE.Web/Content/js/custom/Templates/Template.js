﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var template = function () {

    var init = function () {
        //debugger;
        loadTemplateGrid();
        //addNewField(0);
    },
        loadTemplateGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Title", "sTitle": translatedResources.title, "sWidth": "20%" },
                { "mData": "Description", "sTitle": translatedResources.description, "sWidth": "20%" },
                { "mData": "TemplateType", "sTitle": translatedResources.templateType, "sWidth": "10%" },
                { "mData": "Period", "sTitle": translatedResources.period, "sWidth": "10%" },
                { "mData": "Status", "sClass": "text-center no-sort", "sTitle": translatedResources.status, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initGrid('tbl-Template', _columnData, '/Document/PlanTemplate/LoadTemplateGrid');
        },
        initGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [5]
                }],
                searching: true
            };
            grid.init(settings);

            $("#" + _tableId + "_filter").hide();
            $('#' + _tableId).DataTable().search('').draw();
            $("#templateSearch").on("input", function (e) {
                e.preventDefault();
                $('#' + _tableId).DataTable().search($(this).val()).draw();
            });
        },
        completeTemplateProcess = function () {
            var id = $("#TemplateId").val();
            $.ajax({
                url: '/document/plantemplate/CompleteTemplateProcess?templateId=' + id,
                type: 'GET',
                success: function (data) {
                    location.href = "/document/plantemplate";
                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        deleteTemplate = function (source, id) {

            if (globalFunctions.isValueValid(id)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        url: '/document/plantemplate/DeleteTemplate',
                        type: 'POST',
                        data: { 'id': id },
                        success: function (data) {
                            //debugger;
                            //console.log(data);
                            if (data) {
                                loadTemplateGrid();
                            }

                        },
                        error: function (data, xhr, status) {
                            //debugger;
                            globalFunctions.onFailure();
                        }

                    });
                });
            }
        },
        loadTemplateField = function (id) {
            $.ajax({
                url: '/document/plantemplate/GetTemplateFieldByTemplateId?templateId=' + id,
                type: 'GET',
                success: function (data) {
                    //debugger;
                    if (data !== "") {
                        $("#TemplateFields").html("");
                        $("#TemplateFields").html(data);
                    }
                    else {
                        globalFunctions.onFailure();
                    }
                },
                error: function (data, xhr, status) {
                    //debugger;
                    globalFunctions.onFailure();
                }

            });
        },
        onSaveSuccess = function (data) {
            //console.log(data);
            globalFunctions.showMessage(data.NotificationType, data.Message);
            var id = $("#TemplateId").val();
            var redirect = $("#btnNext").data("redirect");
            $("#encryptedId").val(data.Identifier);
            //console.log(redirect);
            if (redirect === 'false') {
                if (globalFunctions.isValueValid(id)) {
                    if (id !== '0') {
                        loadTemplateField(id);
                    }
                    else {
                        id = data.InsertedRowId;
                        $("#TemplateId").val(id);
                        loadTemplateField(id);
                        $("#btn-submit").text("Update");
                        $("#btnNext").show();
                    }
                }
            }
            else {
                location.href = "/Document/PlanTemplate/TemplateEditor?id=" + data.Identifier;
            }

        },
        addNewField = function (elemNum) {

            if (elemNum == 0) {
                $("#TemplateFields").append("<div class='element row' id='div_0'></div>");

                $("#div_0").append(
                    "<div class='col-lg-3 col-md-3 col-12'>" +
                    "<input type='hidden' id='TemplateField_0_TemplateId' name='TemplateFields[0].TemplateId' value='0'>" +
                    "<input type='hidden' id='TemplateField_0_IsActive' name='TemplateFields[0].IsActive' value='true'>" +
                    "<input type='text' class='responseInput form-control' placeholder='Field' id='TemplateField_0_Field' maxlength='150' name='TemplateFields[0].Field' required='required'>" +
                    "</div>" +
                    "<div class='col-lg-3 col-md-3 col-12 d-flex'>" +
                    "<div class='md-form md-outline flex-1'>" +
                    "<input type='text' class='form-control' placeholder='Placeholder' id='TemplateField_0_Placeholder' readonly='readonly' maxlength='150' name='TemplateFields[0].Placeholder' required='required'>" +
                    "</div>" +
                    "<div class='custom-control font-small ml-3 text-medium'>" +
                    "<input type='checkbox' class='Label custom-control-input' id='TemplateField_0_IsLabel' name='TemplateFields[0].IsLabel'>" +
                    "<label class='custom-control-label mt-1 pt-1 pl-1' for='TemplateField_0_IsLabel'>Is Label</label>" +
                    "</div>" +
                    "</div>" +
                    "<div class='col-lg-3 col-md-3 col-12'>" +
                    "<select id='TemplateField_0_CertificateColumnId' class='form-control form-control-alternative' maxlength='150' name='TemplateFields[0].CertificateColumnId'></select>" +
                    "</div>" +
                    "<div class='col-lg-3 col-md-3 col-12'>" +
                    "<div class='custom-control custom-checkbox custom-control-inline my-4 my-md-0'>" +
                    "<input type='checkbox' class='DefaultValue custom-control-input' id='TemplateField_0_DefaultFieldValue' name='TemplateFields[0].DefaultFieldValue'>" +
                    "<label class='custom-control-label' for='TemplateField_0_DefaultFieldValue'>Mark To Default Value</label>" +
                    "<div class='col-md-0'>" +
                    "<button class='add btn btn-outline-primary m-0' type='button'><i class='fas fa-plus'></i>" +
                    "</div>" +
                    "</div>" +
                    "</div>");

                FillDropDown("TemplateField_" + elemNum + "_CertificateColumnId");
            }
            else {
                $(".element:last").after("<div class='element row' id='div_" + elemNum + "'></div>");

                $("#div_" + elemNum).append(
                    "<div class='col-lg-3 col-md-3 col-12'>" +
                    "<div class='md-form md-outline'>" +
                    "<input type='hidden' id='TemplateField_" + elemNum + "_TemplateId' name='TemplateFields[" + elemNum + "].TemplateId' value='0' >" +
                    "<input type='hidden' id='TemplateField_" + elemNum + "_IsActive' name='TemplateFields[" + elemNum + "].IsActive' value='true'>" +
                    "<input type='text' class='responseInput form-control' placeholder='Field' id='TemplateField_" + elemNum + "_Field' maxlength='150' name='TemplateFields[" + elemNum + "].Field' required='required'>" +
                    "</div>" +
                    "</div>" +
                    "<div class='col-lg-3 col-md-3 col-12 d-flex'>" +
                    "<div class='md-form md-outline flex-1'>" +
                    "<input type='text' class='form-control' placeholder='Placeholder' id='TemplateField_" + elemNum + "_Placeholder' readonly='readonly' maxlength='150' name='TemplateFields[" + elemNum + "].Placeholder' required='required'>" +
                    "</div>" +
                    "<div class='custom-control font-small ml-3 text-medium'>" +
                    "<input type='checkbox' class='Label custom-control-input' id='TemplateField_" + elemNum + "_IsLabel' name='TemplateFields[" + elemNum + "].IsLabel'>" +
                    "<label class='custom-control-label mt-1 pt-1 pl-1' for='TemplateField_" + elemNum + "_IsLabel'>Is Label</label>" +
                    "</div>" +
                    "</div>" +
                    "<div class='col-lg-3 col-md-3 col-12'>" +
                    "<select id='TemplateField_" + elemNum + "_CertificateColumnId' class='form-control form-control-alternative' maxlength='150' name='TemplateFields[" + elemNum + "].CertificateColumnId'></select>" +
                    "</div>" +
                    "<div class='col-lg-3 col-md-3 col-12'>" +
                    "<div class='custom-control custom-checkbox custom-control-inline my-4 my-md-0'>" +
                    "<input type='checkbox' class='DefaultValue custom-control-input' id='TemplateField_" + elemNum + "_DefaultFieldValue' name='TemplateFields[" + elemNum + "].DefaultFieldValue'>" +
                    "<label class='custom-control-label' for='TemplateField_" + elemNum + "_DefaultFieldValue'>Mark To Default Value</label>" +
                    "<div class='col-md-0'>" +
                    "<button type='button' id='remove_" + elemNum + "' class='remove btn btn-outline-danger m-0'><i class='fas fa-plus'></i></button>" +
                    "</div>" +
                    "</div>" +
                    "</div>");

                FillDropDown("TemplateField_" + elemNum + "_CertificateColumnId");
            }



        };

    return {
        init: init,
        loadTemplateGrid: loadTemplateGrid,
        initGrid: initGrid,
        completeTemplateProcess: completeTemplateProcess,
        deleteTemplate: deleteTemplate,
        onSaveSuccess: onSaveSuccess,
        addNewField: addNewField,
        loadTemplateField: loadTemplateField
    };
}();

$(function () {
    template.init();

    $('body').on('click', '#add-more', function () {
        //  debugger;
        // Finding total number of elements added
        var total_element = $(".element").length;

        // last <div> with element class id
        var lastid = $(".element:last").attr("id");
        var split_id = lastid.split("_");
        //debugger;
        var nextindex = Number(split_id[1]) + 1;
        template.addNewField(nextindex);

    });

    $("#btn-submit").click(function () {
        if (!$("#frmAddTempalte")[0].checkValidity()) {
            $("#frmAddTempalte")[0].reportValidity();

        }
        $(this).data("redirect", "false");
    });

    //$("#btnNext").click(function (e) {
    //    var id = $("#encryptedId").val();
    //    if (!$("#frmAddTempalte")[0].checkValidity()) {
    //        $("#frmAddTempalte")[0].reportValidity();
    //        $(this).data("redirect", "false");
    //    }
    //    else {
    //        //location.href = "/Document/PlanTemplate/TemplateEditor?id=" + id;
    //        $(this).data("redirect", "true");
    //    }

    //});


    function CheckFieldCount() {
        var Isinvalid = false;
        $(".btnDeTag").each(function () {
            var string = document.getElementById('DemoRichEdit_View').innerHTML;

            if ((string.match('-', "g") || []).length) {
                var valSplit = $(this).val().split("-");
                if (valSplit.length > 1) {
                    for (i = 0; i < valSplit.length; i++) {
                        var count = (string.match(valSplit[i], "g") || []).length;
                        console.log($(this).val() + '-' + count);
                        if (count == 0) {
                            Isinvalid = true;
                        }
                    }
                }
            } else {
                var count = (string.match($(this).val(), "g") || []).length;
                console.log($(this).val() + '-' + count);
                if (count == 0) {
                    Isinvalid = true;
                }
            }
        });

        return Isinvalid;
    }

    $("#btnSave").click(function () {
        var allPlaceholderExist = $("#placeholderExist").val();
        var totalBtnCnt = $(".btnDeTag").length;
        var btnClickedCnt = $(".btnDeTag[data-clickCnt='1']").length;
        //console.log(totalBtnCnt);
        //console.log(btnClickedCnt);
        //console.log(CheckFieldCount());

        if (CheckFieldCount()) {
            globalFunctions.showWarningMessage(translatedResources.assignplaceholder);
        }
        else if ((totalBtnCnt == btnClickedCnt || allPlaceholderExist == "1")) {
            DemoRichEdit.commands.fileSave.execute();
            //template.completeTemplateProcess();
            globalFunctions.showSuccessMessage(translatedResources.updateSuccessMsg);
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.assignplaceholder);
        }
    });

    $("#btnDone").click(function () {
        var allPlaceholderExist = $("#placeholderExist").val();
        var totalBtnCnt = $(".btnDeTag").length;
        var btnClickedCnt = $(".btnDeTag[data-clickCnt='1']").length;

        if (CheckFieldCount()) {
            globalFunctions.showWarningMessage(translatedResources.assignplaceholder);
        }
        else if (totalBtnCnt == btnClickedCnt || allPlaceholderExist == "1") {
            DemoRichEdit.commands.fileSave.execute();
            template.completeTemplateProcess();
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.assignplaceholder);
        }
    });

    $("#btnBack").show();

    $(document).on('click', '#btnBack', function () {
        location.href = "/document/plantemplate/addedittemplatedetail?id=@EncryptDecryptHelper.EncryptUrl(Model.TemplateId)";
    });
    //window.onbeforeunload = function (e) {
    //   /// alert("yes its working");
    //    // Cancel the event as stated by the standard.
    //    event.preventDefault();
    //    // Chrome requires returnValue to be set.
    //    event.returnValue = '';
    //};
    $('.loader-wrapper').fadeOut(2000, function () {
        // alert('loader-wrapper');
        //$('.loader-wrapper').remove(); 
    });

    $('body').on('click', '.remove', function () {
        //debugger;
        var source = $(this);
        var id = this.id;
        var split_id = id.split("_");
        var deleteindex = split_id[1];

        console.log(source);
        // Remove <div> with id
        //$("#div_" + deleteindex).remove();

        var templateId = $("#TemplateField_" + deleteindex + "_TemplateId").val();
        var text = $("#TemplateField_" + deleteindex + "_Field").val().trim();
        if (templateId === '0' && text === '') {
            $("#div_" + deleteindex).remove();
        }
        else {

            globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
            $(document).bind('okToDelete', source, function (e) {
                if (templateId === '0' && text !== '') {
                    $("#div_" + deleteindex).remove();

                }
                else {
                    $("#div_" + deleteindex).addClass('d-none');
                    //$("#div_" + deleteindex).find("div.d-flex").addClass('disabled');
                    $("#TemplateField_" + deleteindex + "_IsActive").val(false);
                }
            });
        }

    });

    $('body').on('focusout', ".responseInput", function () {
        var id = this.id;
        var split_id = id.split("_");
        var currId = split_id[1];
        var text = $(this).val().trim();

        if (text != '') {
            var isLabel = $("#TemplateField_" + currId + "_IsLabel").prop('checked');
            $("#TemplateField_" + currId + "_IsLabel").val(isLabel);
            if (!isLabel) {
                text = text.replace(/\s/g, '-').trim();
                text = "__" + text + "__";
            }
            //debugger;
            $("#TemplateField_" + currId + "_Placeholder").val(text);
        }
        else {
            $("#TemplateField_" + currId + "_Placeholder").val('');
        }
    });

    $('body').on('change', ".Label", function () {
        // debugger;
        var id = this.id;
        var split_id = id.split("_");
        var currId = split_id[1];

        var text = $("#TemplateField_" + currId + "_Field").val().trim();
        var isLabel = $(this).prop('checked');
        $(this).val(isLabel);

        if (text != '') {
            if (!isLabel) {
                text = text.replace(/\s/g, '-').trim();
                text = "__" + text + "__";
            }
            $("#TemplateField_" + currId + "_Placeholder").val(text);
        }
        else {
            $("#TemplateField_" + currId + "_Placeholder").val('');
        }
    });

    $('body').on('change', ".DefaultValue", function () {

        var id = this.id;
        var split_id = id.split("_");
        var currId = split_id[1];

        var text = $("#TemplateField_" + currId + "_CertificateColumnId").val().trim();
        var isLabel = $(this).prop('checked');
        $(this).val(isLabel);
        if (isLabel) {
            $("#TemplateField_" + currId + "_CertificateColumnId").prop('disabled', false);
        }
        else {
            $("#TemplateField_" + currId + "_CertificateColumnId").prop('disabled', 'disabled');
        }
        $("#TemplateField_" + currId + "_CertificateColumnId").selectpicker('refresh');
    });


});

window.addEventListener('load', function () {
    console.log('Pageloaded complately');
    $('.DefaultValue:checkbox:checked').each(function () {
        var id = this.id;
        var split_id = id.split("_");
        var currId = split_id[1];
        FillDropDown("TemplateField_" + currId + "_CertificateColumnId");
        $("#TemplateField_" + currId + "_CertificateColumnId").removeAttr('disabled');
        //console.log($("#hdn_TemplateField_" + currId + "_CertificateColumnId").val());
        $("#TemplateField_" + currId + "_CertificateColumnId").val($("#hdn_TemplateField_" + currId + "_CertificateColumnId").val());
        $("#TemplateField_" + currId + "_CertificateColumnId").selectpicker('refresh');
    });
    $('.DefaultValue:checkbox:unchecked').each(function () {

        var id = this.id;
        var split_id = id.split("_");
        var currId = split_id[1];
        FillDropDown("TemplateField_" + currId + "_CertificateColumnId");
        $("#TemplateField_" + currId + "_CertificateColumnId").prop('disabled', 'disabled');
        $("#TemplateField_" + currId + "_CertificateColumnId").selectpicker('refresh');
    });
});
var JsonResult = null;
function FillDropDown(dropDownId) {
    if (JsonResult != null || JsonResult != undefined) {
        $("#" + dropDownId).empty();
        $("#" + dropDownId).prop('disabled', 'disabled');
        $("#" + dropDownId).prop("title", 'Select Column');
        $.each(JsonResult, function () {
            $("#" + dropDownId).append($("<option />").val(this.Value).text(this.Text));
        });
        $("#" + dropDownId).selectpicker('refresh');
        return;
    }

    $.ajax({
        type: "Get",
        url: "GetCertificateColumns",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#" + dropDownId).empty();
            $("#" + dropDownId).prop('disabled', 'disabled');
            $("#" + dropDownId).prop("title", 'Select Column');
            JsonResult = data;
            $.each(data, function () {
                $("#" + dropDownId).append($("<option />").val(this.Value).text(this.Text));

            });
            $("#" + dropDownId).selectpicker('refresh');
        },
        error: function (xhr, status, error) { }
    })
}