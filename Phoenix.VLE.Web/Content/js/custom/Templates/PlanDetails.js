﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
var planDetail = function () {

    var init = function () {

        //     InitializeCheckBox();
        var actionToggleView = $("<ul />", { "class": "list-pattern" })
            .append($("<li />", { "class": "type-list", text: translatedResources.View }))
            .append($("<li />", { "class": "grid-view active", onclick: "planDetail.toggleViewStyle(this)", "data-toggle": "grid" }))
            .append($("<li />", { "class": "list-view", onclick: "planDetail.toggleViewStyle(this)", "data-toggle": "list" }));
        var span = document.getElementsByClassName("list-pattern");
        if (span.length == 0)
            $('#breadcrumbActions').append(actionToggleView);

        globalFunctions.setCookie("selectedView", "grid");
    },
        loadPlanGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "PlanSchemeName", "sTitle": translatedResources.planschemename, "sWidth": "60%" },
                { "mData": "Status", "sClass": "text-left text-sm-center no-sort", "sTitle": translatedResources.status, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-left text-sm-center no-sort", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initGrid('tbl-PlanScheme', _columnData, '/Document/PlanScheme/GetApproveLessonPlan');
        },
        initGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': [2]
                }],
                searching: true
            };
            grid.init(settings);

            $("#" + _tableId + "_filter").hide();
            $('#' + _tableId).DataTable().search('').draw();
            $("#lessonPlanSearch").on("input", function (e) {
                e.preventDefault();
                $('#' + _tableId).DataTable().search($(this).val()).draw();
            });
        },
        initPlanSchemeStatus = function (source, planSchemeDetailId) {
            // 
            globalFunctions.loadPopup($(source), "/Document/PlanScheme/InitPlanSchemeStatusForm?planSchemeId=" + planSchemeDetailId, translatedResources.updateTemplateStatus);
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                $(".selectpicker").selectpicker();
            });
        },
        toggleViewStyle = function (source) {

            if (!$(source).hasClass("active")) {
                $("ul.list-pattern li").removeClass("active");
                $(source).addClass("active");
                $(".list-wrapper").toggleClass("hide-item", $(source).data("toggle") == "grid");
                $(".grid-wrapper").toggleClass("hide-item", $(source).data("toggle") == "list");
                if ($(source).data("toggle") == "grid") {
                    globalFunctions.setCookie("selectedView", "grid");
                }
                else {
                    globalFunctions.setCookie("selectedView", "list");

                }
            }
        },
        assignForApproval = function (source, planSchemeDetailId) {

            if (globalFunctions.isValueValid(planSchemeDetailId)) {
                globalFunctions.notyDeleteConfirm(source, translatedResources.approvalmsg);
                $(document).bind('okToDelete', source, function (e) {
                    $.ajax({
                        url: '/document/planscheme/AssignPlanSchemeForApproval',
                        type: 'POST',
                        data: { 'planSchemeDetailId': planSchemeDetailId },
                        success: function (data) {
                            // 
                            //console.log(data);
                            if (data) {
                                loadPlanGrid();
                            }

                        },
                        error: function (data, xhr, status) {
                            // 
                            globalFunctions.onFailure();
                        }

                    });
                });
            }
        },
        deletePlan = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                //globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
                //$(document).bind('okToDelete', source, function (e) {
                $.ajax({
                    url: '/document/planscheme/DeletePlan',
                    type: 'POST',
                    data: { 'id': id },
                    success: function (data) {
                        // 
                        //console.log(data);
                        if (data) {
                            //  $('#deletePopup').modal('toggle');
                            //loadPlanGrid();
                        }

                    },
                    error: function (data, xhr, status) {
                        // 
                        globalFunctions.onFailure();
                    }

                });
                // });
            }
        },
        loadPlanField = function (id) {
            var groupid = 0;
            if ($('#selectedGroup').val() == "")
                groupid = 0
            else
                groupid = $('#selectedGroup').val();
            $.ajax({
                url: '/document/planscheme/GetTemplateFieldByTemplateId?templateId=' + id + '&groupid=' + groupid,
                type: 'GET',
                success: function (data) {
                    // 
                    if (data !== "") {
                        $("#TemplateFields").html("");
                        $("#TemplateFields").html(data);
                        $("#fillDataTitle").removeClass("d-none");

                        $("#btnPreview").removeClass("d-none");
                        $('#btnPreview').data('TemplateId', id);
                        $('.select-picker').selectpicker('refresh');

                        
                        //$('.date-picker').datepicker('refresh');
                    }
                    else {
                        globalFunctions.onFailure();
                    }
                },
                error: function (data, xhr, status) {
                    // 
                    globalFunctions.onFailure();
                }

            });
        },
        onStatusSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            $("#myModal").modal('hide');
            var isTemplateEditor = $("#templateEditor").val();
            if (isTemplateEditor == '1') {
                location.href = "/document/planscheme";
            }
            else {

                loadPlanGrid();
            }
        },
        onSaveSuccess = function (data) {
            //console.log(data);
            globalFunctions.showMessage(data.NotificationType, data.Message);
            //var id = $("#PlanSchemeDetailId").val();
            if (data.Success) {
                location.href = "/document/planscheme/planschemeeditor?id=" + data.Identifier;
            }

        };

    return {
        init: init,
        loadPlanGrid: loadPlanGrid,
        toggleViewStyle: toggleViewStyle,
        initGrid: initGrid,
        assignForApproval: assignForApproval,
        deletePlan: deletePlan,
        onSaveSuccess: onSaveSuccess,
        loadPlanField: loadPlanField,
        onStatusSaveSuccess: onStatusSaveSuccess,
        initPlanSchemeStatus: initPlanSchemeStatus
    };
}();
$(function () {

    planDetail.init();

});
$('#AjaxLoader').show();

function GetValueFromDB(checkboxstatus, operation) {
    $.ajax({
        url: '/Document/PlanScheme/GetApproveStatusDetail?Status=' + checkboxstatus + '&Operation=' + operation,
        success: function (data) {
            if (data.Status) {
                $('#chkApprovalRight').prop('checked', 'checked');
            } else {
                $('#chkApprovalRight').prop('checked', false);
            }
        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure();
        }
    });
}

$('#chkApprovalRight').change(function () {
    GetValueFromDB($('#chkApprovalRight').prop('checked'), 'UPDATE');
})

function RefreshLeftHandFilterOption(textboxid, unorderlistid, tagName, dvid, spanid) {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById(textboxid);
    filter = input.value.toUpperCase();
    ul = document.getElementById(unorderlistid);
    li = ul.getElementsByTagName('li');
    var counter = 0;
    if (dvid != "") document.getElementById(dvid).setAttribute('style', 'display:block !important');
    if (spanid != "") document.getElementById(spanid).setAttribute('style', 'display:none !important');
    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName(tagName)[0];
        txtValue = a.textContent.trim() || a.innerText.trim();
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            //li[i].style.display = 'none !important';
            li[i].setAttribute('style', 'display:none !important');
            counter++;
        }
    }

    if (li.length == counter) {
        if (tagName == "p") document.getElementById(dvid).setAttribute('style', 'display:none !important');
        document.getElementById(spanid).setAttribute('style', 'display:block !important');
    }

}

function Refreshplanscheme(searchstring) {
    if (searchstring != null && searchstring != undefined && searchstring != "") {
        var DynamicParameter = '&sortBy=' + $('.selSortBy').children("option:selected").val() + GetFilterOptionParameter();
        $.ajax({
            url: '/Document/PlanScheme/LoadPlanGrid?selectedSearchParameterId=' + AddSelectedParameterIdInArray() + '&selectedGroupList=' + AddSelectedParameterInArray() + '&PlanSchemeId=20&pageIndex=1&searchString=' + searchstring + DynamicParameter + '&IsSearchByName=true',
            type: 'GET',
            success: function (data) {
                $('#PlanSchemeGrid').html('');
                $('#PlanSchemeGrid').html(data.planHTML);
            },
            error: function (data, xhr, status) {
                alert(status);
                globalFunctions.onFailure();
            }
        });

    } else {
        loadplanscheme();
    }
}

function AddSelectedParameterInArray() {
    var selectedSearchParameter = '';
    $.each($("input:checkbox.customCheckBox:checked"), function () {
        let groupName = $(this).attr('data-text');
        selectedSearchParameter = selectedSearchParameter + groupName + ',';
    })
    return selectedSearchParameter;
}

function AddSelectedParameterIdInArray() {
    var selectedSearchParameterId = '';
    $.each($("input:checkbox.customCheckBox:checked"), function () {
        let controlid = $(this).attr('data-id');
        selectedSearchParameterId = selectedSearchParameterId + controlid + ',';
    })
    return selectedSearchParameterId;
}

function GetFilterOptionParameter() {

    var SelectedOption = "";
    var DynamicParameter = "";

    var len = $("input:checkbox.customCheckBox:checked").length;
    if (len > 0) {

        $('.FilterOption:checkbox:checked').each(function () {
            var FiltercheckboxType = $(this).data('checkboxtype');

            if (FiltercheckboxType == "SharedWithMe") {
                DynamicParameter = DynamicParameter + "&SharedWithMe=true";
            } else if (FiltercheckboxType == "SharedWithMe" && IsUnChecked == true) {
                DynamicParameter = DynamicParameter + "&SharedWithMe=false";
            }

            if (FiltercheckboxType == "CreatedByMe") {
                DynamicParameter = DynamicParameter + "&CreatedByMe=true";
            } else if (FiltercheckboxType == "CreatedByMe" && IsUnChecked == true) {
                DynamicParameter = DynamicParameter + "&CreatedByMe=false";
            }

        });
        $('.chkMIS:checkbox:checked').each(function () {
            SelectedOption = SelectedOption + $(this).attr('data-id') + ',';
        });
        if (SelectedOption != "") {
            DynamicParameter = DynamicParameter + "&MISGroupId=" + SelectedOption;
        }

        SelectedOption = "";
        $('.chkOther:checkbox:checked').each(function () {
            SelectedOption = SelectedOption + $(this).attr('data-id') + ',';
        });
        if (SelectedOption != "") {
            DynamicParameter = DynamicParameter + "&OtherGroupId=" + SelectedOption;
        }

        SelectedOption = "";
        $('.chkCourse:checkbox:checked').each(function () {
            SelectedOption = SelectedOption + $(this).attr('data-id') + ',';
        });
        if (SelectedOption != "") {
            DynamicParameter = DynamicParameter + "&CourseId=" + SelectedOption;
        }

        SelectedOption = "";
        $('.chkGrade:checkbox:checked').each(function () {
            SelectedOption = SelectedOption + $(this).attr('data-id') + ',';
        });
        if (SelectedOption != "") {
            DynamicParameter = DynamicParameter + "&GradeId=" + SelectedOption;
        }
    }
    return DynamicParameter;
}

function GetDailyTimeTable(selectedDate) {
    if (selectedDate != undefined) {
        $.ajax({
            url: '/Document/PlanScheme/GetTimeTableList?UserId=0&SelectDate=' + selectedDate,
            type: 'GET',
            success: function (data) {
                $("#TemplateField_ddlperiod").empty();
                $('#TemplateField_ddlperiod').prop("title", 'Select a period');
                JsonResult = data;
                $.each(data, function () {
                    $('#TemplateField_ddlperiod').append($("<option />").val(this.Value).text(this.Value));

                });
                $("#TemplateField_ddlperiod option").each(function () {
                    if ($(this).text() == $('#TemplateField_ddlperiod').data('value')) {
                        $(this).attr("selected", "selected");
                    }
                });
                $('#TemplateField_ddlperiod').selectpicker('refresh');
            },
            error: function (data, xhr, status) {
                alert(status);
                globalFunctions.onFailure();
            }
        });
    }
}

function FillGrade(groupid) {
    $.ajax({
        url: '/Document/PlanScheme/GetGradeList?Groupid=' + groupid,
        type: 'GET',
        success: function (data) {
            $('#TemplateField_ddlGrade').html('');
            JsonResult = data;
            var optionHTML = '';
            $.each(data, function () {
                 
                if ($('#TemplateField_ddlGrade').data('value') != undefined) {
                    if ($('#TemplateField_ddlGrade').data('value') != "") {
                        if ($('#TemplateField_ddlGrade').data('value').trim() == this.Value.trim()) {
                            $('#TemplateField_ddlGrade').removeAttr("title");
                            optionHTML = optionHTML + "<option value=" + this.Value + " selected> " + this.Value + "</option>";
                        } else {
                            optionHTML = optionHTML + "<option value=" + this.Value + " > " + this.Value + "</option>";
                        }
                    }
                    else {
                        optionHTML = optionHTML + "<option value=" + this.Value + " > " + this.Value + "</option>";
                    }
                } else {
                    optionHTML = optionHTML + "<option value=" + this.Value + " > " + this.Value + "</option>";
                }
            });

            $('#TemplateField_ddlGrade').html(optionHTML);
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (data, xhr, status) {
            alert(status);
            globalFunctions.onFailure();
        }
    });

}

$(document).off('change', '#TemplateId').on('change', '#TemplateId', function () {
    var id = $(this).val();
    if (id != 0) {
        planDetail.loadPlanField(id);
    }
    else {
        $("#TemplateFields").html("");
        $("#fillDataTitle").addClass("d-none");
    }
})

$("#btn-submit").click(function () {
    if (!$("#frmAddPlanScheme")[0].checkValidity()) {
        $("#frmAddPlanScheme")[0].reportValidity();
    }

});

$(document).on("keyup", ".searchInput", function () {
        var index = $(this).attr("data-index");
        RefreshLeftHandFilterOption($(this).attr("id"), "ul" + index, "p", "dv" + index, "sp" + index);
    });

$(document).on('change', '.customCheckBox', function () {
        var IsUnChecked = false;
        if (!this.checked) {
            IsUnChecked = true;
        }
        console.log('checkbox event fire.');
        var DynamicParameter = GetFilterOptionParameter();
        var grid = new DynamicPagination("PlanSchemeGrid");
        var settings = {
            url: '/Document/PlanScheme/LoadPlanGrid?selectedSearchParameterId=' + AddSelectedParameterIdInArray() + '&selectedGroupList=' + AddSelectedParameterInArray() + '&PlanSchemeId=20&pageIndex=1' + DynamicParameter
        };
        grid.init(settings);

    })

$(document).on("click", "#selectedFilterList .btn-close", function () {
        console.log('event fire');
        
        var chkbxId = $(this).attr('data-id');
        $("#" + chkbxId).prop("checked", false);
        console.log(chkbxId);

        var DynamicParameter = '&sortBy=' + $('.selSortBy').children("option:selected").val() + GetFilterOptionParameter();
        var grid = new DynamicPagination("PlanSchemeGrid");
        var settings = {
            url: '/Document/PlanScheme/LoadPlanGrid?selectedSearchParameterId=' + AddSelectedParameterIdInArray() + '&selectedGroupList=' + AddSelectedParameterInArray() + '&PlanSchemeId=20&pageIndex=1&searchString=' + $('#txtsearchContent').val() + DynamicParameter
        };
        grid.init(settings);
    });

$(document).on('change', "#txtsearchContent", function () {
        Refreshplanscheme($('#txtsearchContent').val());
    });

$(document).on('change', '.selSortBy', function () {
        //alert($('.selSortBy').children("option:selected").val());
        console.log('event fire');
        var DynamicParameter = '&sortBy=' + $('.selSortBy').children("option:selected").val() + GetFilterOptionParameter();
        $.ajax({
            url: '/Document/PlanScheme/LoadPlanGrid?selectedSearchParameterId=' + AddSelectedParameterIdInArray() + '&selectedGroupList=' + AddSelectedParameterInArray() + '&PlanSchemeId=20&pageIndex=1&searchString=' + $('#txtsearchContent').val() + DynamicParameter + '&IsSortBySelected=true',
            type: 'GET',
            success: function (data) {

                $('#PlanSchemeGrid').html('');
                $('#PlanSchemeGrid').html(data.planHTML);
                $('.selSortBy').selectpicker('refresh');

            },
            error: function (data, xhr, status) {
                alert(status);
                globalFunctions.onFailure();
            }
        });

    })

$(document).on('keyup', '#txtGrade', function () {

        RefreshLeftHandFilterOption('txtGrade', 'lstGradeOption', "label", "", "spGrade");
    });

$(document).on('click', '#btnGradeClear', function () {
        $('#txtGrade').val('');
        RefreshLeftHandFilterOption('txtGrade', 'lstGradeOption', "label", "", "spGrade");
    })

$(document).on('keyup', '#txtCourse', function () {
        RefreshLeftHandFilterOption('txtCourse', 'lstCourse', "label", "", "spCourse");
    });

$(document).on('click', '#btnCourseClear', function () {
        $('#txtCourse').val('');
        RefreshLeftHandFilterOption('txtCourse', 'lstCourse', "label", "", "spCourse");
    })

$(document).on('keyup', '#txtOtherGroup', function () {
        RefreshLeftHandFilterOption('txtOtherGroup', 'lstOtherGroup', "label", "", "spOtherGroup");
    });

$(document).on('click', '#btnOtherClear', function () {
        $('#txtOtherGroup').val('');
        RefreshLeftHandFilterOption('txtOtherGroup', 'lstOtherGroup', "label", "", "spOtherGroup");
    })

$(document).on('keyup', '#txtMISGroup', function () {
        RefreshLeftHandFilterOption('txtMISGroup', 'lstMISGroup', "label", "", "spMISGroup");
    });

$(document).on('click', '#btnMISClear', function () {
        $('#txtMISGroup').val('');
        RefreshLeftHandFilterOption('txtMISGroup', 'lstMISGroup', "label", "", "spMISGroup");
    })

$(document).on('keyup', '#txtFilterOption', function () {
        RefreshLeftHandFilterOption('txtFilterOption', 'lstFilterOption', "label", "", "spFilterOption");
    });

$(document).on('click', '#btnFilterClear', function () {
        $('#txtFilterOption').val('');
        RefreshLeftHandFilterOption('txtFilterOption', 'lstFilterOption', "label", "", "spFilterOption");
    })

$(document).on('click', '.btnEditPlanScheme', function () {
        location.href = '/document/planScheme/AddPlanSchemeDetail?id=' + $(this).data('planschemeid');
    });

$(document).on('click', '.btnDeletePlanScheme', function () {
        $('#deletePopup .OkToDelete').data('planschemeid', $(this).data('planschemeid'));
        $('#deletePopup').modal('show');
    });

$(document).on('click', '.OkToDelete', function () {
        planDetail.deletePlan('', $(this).data('planschemeid'));
        $('#deletePopup').modal('hide');
        loadplanscheme();
        //LoadFilterPanel();
    })

$(document).on('click', '.btnApproved', function () {

        $('#SendForApprovalPopup .OkToSend').data('planschemeid', $(this).data('planschemeid'));
        $('#SendForApprovalPopup').modal('show');
    })

$(document).on('click', '.OkToSend', function () {

        $.ajax({
            url: '/document/planscheme/AssignPlanSchemeForApproval',
            type: 'POST',
            data: { 'planSchemeDetailId': $(this).data('planschemeid') },
            success: function (data) {
                if (data) {
                    loadplanscheme();
                }
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure();
            }
        });
        $('#SendForApprovalPopup').modal('hide');
    })

$(document).on('click', '.DeleteSharedLesson', function () {
        var PlanSchemeId = $('#hdnPlanSchemeId').val();
        $.ajax({
            url: '/document/planscheme/DeleteSharedLessonLink?PlanSchemeId=' + PlanSchemeId + '&TeacherId=' + $(this).data('teacherid'),
            type: 'POST',
            success: function (data) {
                if (data) {
                    // 
                    $('#modal_Loader').html(data.planHTML);
                    $('#hdnPlanSchemeId').val(PlanSchemeId);
                    initSelectPicker();
                    loadplanscheme();
                    //LoadFilterPanel();();
                }
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure();
            }
        });

    });

$(document).on('click', '.OkToShare', function () {
        var PlanSchemeId = $('#hdnPlanSchemeId').val();
        // alert($('#selectedTeachers').val());

        if ($('#selectedTeachers').val() != undefined && $('#selectedTeachers').val() != "") {
            $.ajax({
                url: '/document/planscheme/SaveSharedPlanView?PlanSchemeId=' + PlanSchemeId + '&SelectedTeacherList=' + $('#selectedTeachers').val(),
                type: 'POST',
                success: function (data) {
                    if (data) {
                        // 
                        $('#modal_Loader').html(data.planHTML);
                        $('#hdnPlanSchemeId').val(PlanSchemeId);
                        initSelectPicker();
                        loadplanscheme();
                        //LoadFilterPanel();();
                    }
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure();
                }
            });
        }
    })

function initSelectPicker() {
        if ($(".select-picker-withImg").length) {
            //const BASE_URL = $('base[ href ]').attr('href');
            const BASE_URL = "img/"
            const $_SELECT_PICKER = $('.select-picker-withImg');

            $_SELECT_PICKER.find('option').each((idx, elem) => {
                const $OPTION = $(elem);
                const IMAGE_URL = $OPTION.attr('data-thumbnail');

                if (IMAGE_URL) {
                    $OPTION.attr('data-content', "<img src='%i'/> %s".replace(/%i/, IMAGE_URL).replace(/%s/, $OPTION.text()))
                }

                console.warn('option:', idx, $OPTION)

            });

            $_SELECT_PICKER.selectpicker();
        };
    }

$(document).on('click', '.btnSharePlanScheme', function () {

        globalFunctions.loadPopup($(this), "/Document/PlanScheme/LoadSharedPlanView?planSchemeId=" + $(this).data('planschemeid'), "Share lesson plan with other teachers", '');
        $(this).off('modalLoaded');
        $(this).on('modalLoaded', function () {
            //selectpicker option with image

            initSelectPicker();
            $('.selectpicker').selectpicker();
            $('#hdnPlanSchemeId').val($(this).data('planschemeid'));
        });
    })

$(document).on('click', '#btnLessonGroup', function () {
        globalFunctions.loadPopup($(this), "/Document/PlanScheme/GetSchoolGroupByUserId", '');
        $(this).off('modalLoaded');
        $(this).on('modalLoaded', function () {
            //selectpicker option with image

            initSelectPicker();
            $('.selectpicker').selectpicker();
            $('#hdnPlanSchemeId').val($(this).data('planschemeid'));
        });
        //End --> Assign Group Functionality
    })
    
$(document).off('change', '#selectedGroup').on('change', '#selectedGroup', function () {
        loadUnitStructure();
    })

$(document).on('click', '#btnPreview', function () {
        //alert($(this).data("TemplateId"));
        var id = $(this).data("TemplateId");
        window.open("/Document/Viewer/Index?id=" + id + "&module=template", "_blank");
    });

$(document).on('click', '.viewLessonPlan', function () {
        var id = $(this).data("plandetailid");
        window.open("/Document/Viewer/Index?id=" + id + "&module=lessonplan", "_blank");
    })

$(document).on('click', '.approveLessonPlan', function () {
        var id = $(this).data("plandetailid");
        $.ajax({
            url: '/document/planscheme/ChangePlanApproveStatus?planSchemeDetailId=' + id,
            type: 'POST',
            success: function (data) {
                planDetail.loadPlanGrid();
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure();
            }
        });
    })
   
function hideviewoption() {
    if (this.document.getElementsByClassName('list-pattern').length > 0) {
        $('.list-pattern').addClass('d-none');
    } else {
        hideviewoption();
    }
}

function recFunction() {
    setTimeout(
        function () {
            hideviewoption();
        }, 5000);
}

$(document).on('click', '.btnRejectPlan', function () {
    
    var id = $(this).data("planschemeid");
    $.ajax({
        url: '/document/planscheme/RejectLessonPlan?planSchemeDetailId=' + id,
        type: 'POST',
        success: function (data) {
            planDetail.loadPlanGrid();
        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure();
        }
    });
})
