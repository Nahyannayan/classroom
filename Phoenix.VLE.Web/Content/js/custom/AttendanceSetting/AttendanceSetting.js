﻿var AttSetting = function () {
    Init = function () {

    },
        LoadRightDrawer = function (id) {
            var loadDrawerObject = [
                { id: "ParameterSetting", url: "/AttendanceSetting/AttendanceSetting/ParameterSetting" },
                { id: "AttendanceType", url: "/AttendanceSetting/AttendanceSetting/AttendanceType" },
                { id: "AttendancePeriod", url: "/AttendanceSetting/AttendanceSetting/AttendancePeriod" },
                { id: "LeaveApprovalPermission", url: "/AttendanceSetting/AttendanceSetting/LeaveApprovalPermission" }
            ]
            if (loadDrawerObject.some(x => x.id == id)) {
                var object = loadDrawerObject.find(x => x.id == id);
                $("#divAttendanceSettingDrawer").html('');
                $.get(object.url, function (response) {
                    $("#divAttendanceSettingDrawer").append(response);
                    $('.selectpicker').selectpicker('refresh');
                });
            }
            
        }
    return {
        Init,
        LoadRightDrawer
    }
}();

//----------Event Handling Functionality--------------
$(document).on("click", ".trigger-rightDrawer", function () {
    AttSetting.LoadRightDrawer($(this).data("model"));
});
//----------End Event Handling Functionality----------