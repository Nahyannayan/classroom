﻿
AttendanceType = function () {
    var Init = function () {
        common.init();
        $('#divBack').hide();
    },
        AttendanceTypeArray = [],
        DeleteAttendanceType = function (ATT_ID, element) {
            common.notyDeleteConfirm(translatedResources.deleteConfirm, () => {
                $.post("/AttendanceSetting/AttendanceSetting/SaveAttendanceType", { AttendanceTypeId: AttendanceTypeId, StartDate: new Date(), EndDate: new Date(), DataMode: "Delete" }, function (response) {
                    if (response.Success) {
                        $(element).closest('tr').remove();
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                    }
                }).fail(error => globalFunctions.showMessage(error.NotificationType, error.Message));;
            });
        },
        SaveAttendanceType = () => {
            if (common.IsInvalidByFormOrDivId("frmAttendanceType")) {
                $("#frmAttendanceType").submit();
            }
        },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                AttendanceType.GetAttendanceTypeList();
            }
        },
        onFailure = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);

        },
        Back = function (status) {
            if (status) {
                $("#AddBtn").show();
                $('#showTable').show();
                $('#divBack').hide();
                $('#showForm').html('');
            }
            else {
                $("#AddBtn").hide();
                $('#showTable').hide();
                $('#divBack').show();
            }
        },
        LoadAttendancePeriod = function (gradeId) {
            $.get('/AttendanceSetting/AttendanceSetting/AttendancePeriodGrid', { gradeId: gradeId }, function (response) {
                $('#divAttendancePeriodResult').html('');
                $('#divAttendancePeriodResult').html(response);
                $('.selectpicker').selectpicker('refresh');
            });
        },
        AddEditAttendanceType = function (AttendanceConfigurationTypeID) {
            AttendanceType.Back(false);
            $.get("/AttendanceSetting/AttendanceSetting/AddEditAttendanceType", { AttendanceConfigurationTypeID }, function (response) {
                $("#showForm").html('');
                $("#showForm").html(response);
                $('.selectpicker').selectpicker('refresh');
            }).fail(error => globalFunctions.showMessage(error.NotificationType, error.Message));;
        },
        GetAttendanceTypeList = function () {
            AttendanceType.Back(true);
            $.post("/AttendanceSetting/AttendanceSetting/GetAttendanceType", function (response) {
                $("#showTable").html(response);
                $("#divAttendancePeriodResult").html('');
            });
        }
    return {
        Init,
        AttendanceTypeArray,
        DeleteAttendanceType,
        SaveAttendanceType,
        Back,
        LoadAttendancePeriod,
        onSaveSuccess,
        onFailure,
        AddEditAttendanceType,
        GetAttendanceTypeList
    }
}();
$(document).ready(function () {
    AttendanceType.Init();
});

