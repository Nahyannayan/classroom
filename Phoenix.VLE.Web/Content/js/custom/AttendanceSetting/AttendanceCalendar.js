﻿var comboTree1 = undefined;
var GradeSectionTree = undefined;
var calendar;
var WEEKEND1 = "";
var WEEKEND2 = "";
//----------------Functionality----------------------
var objAttendanceCalendar = function () {
    init = () => {
        $('#attenCalender').mCustomScrollbar({
            setHeight: "600",
            autoExpandScrollbar: true,
            scrollbarPosition: "inside",
            autoHideScrollbar: true,
        });
        //objAttendanceCalendar.bindCalendarEvent();
        objAttendanceCalendar.loadCalendarListView();
        objAttendanceCalendar.editCalendarEvent(0);
    },
        editCalendarEvent = (SCH_ID) => {
            $.post("/AttendanceSetting/AttendanceSetting/EditCalendarEvent", { SCH_ID: SCH_ID }, function (response) {
                $("#attenCalenderForm").html('');
                $("#attenCalenderForm").append(response);
                $('.selectpicker').selectpicker('refresh');
            });
        },
        getGradeAndSectionList = (selectedValues) => {
            $.post("/AttendanceSetting/AttendanceSetting/GetGradeAndSectionList", function (options) {
                $('#GradeSectionTree').combotree({
                    data: options,
                    cascadeCheck: true,
                    labelPosition: 'top',
                    multiple: true,
                    lines: true
                });
                $('#GradeSectionTree').combotree('setValues', selectedValues);
            });
        },
        saveCalendarEvent = () => {
            var IsValid = common.IsInvalidByFormOrDivId('frmCalendarEvent');
            if (IsValid) {

                var SCH_ID = $("#SCH_ID").val();
                var SCH_WEEKEND1_WORK = $("#setWorkingFriday").is(":checked") ? $("#setWorkingFriday").next('label').text() : "";
                var SCH_WEEKEND2_WORK = $("#setWorkingSaturday").is(":checked") ? $("#setWorkingSaturday").next('label').text() : "";

                var selectedSectionList = "";
                if (SCH_ID == '0') {
                    var SelectedComboItemValue = $('#GradeSectionTree').combotree('getValues');
                    selectedSectionList = SelectedComboItemValue.length == 0 ? "" : SelectedComboItemValue;
                }

                var attendanceCalendar = {
                    SCH_ID: SCH_ID,
                    SCH_DTFROM: $("#SCH_DTFROM").val(),
                    SCH_DTTO: $("#SCH_DTTO").val(),
                    SCH_REMARKS: $("#SCH_REMARKS").val(),
                    SCH_TYPE: $("#ddlSCH_TYPE").val(),
                    //SCH_bGRADEALL: $("#SCH_bGRADEALL").is(":checked"),
                    SCH_WEEKEND1_WORK: SCH_WEEKEND1_WORK,
                    SCH_bWEEKEND1_LOG_BOOK: $("#SCH_bWEEKEND1_LOG_BOOK").is(":checked"),
                    SCH_WEEKEND2_WORK: SCH_WEEKEND2_WORK,
                    SCH_bWEEKEND2_LOG_BOOK: $("#SCH_bWEEKEND2_LOG_BOOK").is(":checked"),
                    selectedSectionList: selectedSectionList
                }
                $.post("/AttendanceSetting/AttendanceSetting/SaveCalendarEvent", { attendanceCalendar: attendanceCalendar }, function (response) {
                    if (response.Success) {
                        objAttendanceCalendar.init();
                        //if ($("#toggler").is(':checked')) {
                        //    objAttendanceCalendar.bindCalendarEvent();
                        //}
                        //else {
                        //    objAttendanceCalendar.loadCalendarListView();
                        //}
                    }
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                });
            }
        },
        bindCalendarEvent = () => {
            var ACD_ID = $("#ddlAcademicYearFilter").val();
            var myDate = new Date();
            $.post("/AttendanceSetting/AttendanceSetting/GetCalendarEvent", { ACD_ID: ACD_ID }, function (response) {
                var eventArray = [];
                if (response.eventList.length) {
                    $.each(response.eventList, function (i, obj) {
                        var events = new Object();
                        var startDate = new Date(obj.SCH_DTFROM.match(/\d+/)[0] * 1);
                        var endDate = new Date(obj.SCH_DTTO.match(/\d+/)[0] * 1);
                        endDate.setDate(endDate.getDate() + 1)
                        events.id = obj.SCH_ID;
                        events.title = obj.SCH_REMARKS;
                        events.start = startDate;
                        events.end = endDate;
                        events.className = 'eventContent';
                        events.description = obj.SCH_REMARKS;
                        eventArray.push(events);
                    });
                }

                myDate = new Date(response.academicYearDetail.ACD_STARTDT.match(/\d+/)[0] * 1);
                var AYStartDate = new Date(response.academicYearDetail.ACD_STARTDT.match(/\d+/)[0] * 1);
                var AYEndDate = new Date(response.academicYearDetail.ACD_ENDDT.match(/\d+/)[0] * 1);
                AYEndDate.setDate(AYEndDate.getDate() + 1);
                $("#divCalender").html('');
                var calendarEl = document.getElementById('divCalender');
                calendar = new FullCalendar.Calendar(calendarEl, {
                    plugins: ['interaction', 'dayGrid', 'bootstrap'],
                    defaultView: 'dayGridMonth',
                    header: {
                        left: 'title',
                        center: '',
                        right: 'prev,next'
                    },
                    defaultDate: response.CurrentDefaultDate,
                    displayEventTime: false,
                    contentHeight: 'auto',
                    editable: false,
                    themeSystem: 'bootstrap',
                    events: eventArray,
                    fixedWeekCount: false,
                    eventClick: function (info) {
                        var id = info.event.id;
                        objAttendanceCalendar.editCalendarEvent(id);
                    },
                    selectable: true,
                    select: function (info) {
                        info.end.setDate(info.end.getDate() - 1);
                        $('#SCH_DTFROM').val(info.start.toShortFormat());
                        $('#SCH_DTTO').val(info.end.toShortFormat());
                    },
                    validRange: {
                        start: AYStartDate,
                        end: AYEndDate
                    },
                    //dateClick: function (info) {
                    //    info.end.setDate(info.end.getDate() - 1);
                    //    $('#SCH_DTFROM').val(info.date.toShortFormat());
                    //    $('#SCH_DTTO').val(info.date.toShortFormat());
                    //},
                    //height: '50%',
                    //aspectRatio: 1.8,
                    /* navLinks: true,*/ // can click day/week names to navigate views
                    /* eventLimit: true,*/ // allow "more" link when too many events
                    //eventStartEditable: false
                });
                calendar.render();
                objAttendanceCalendar.bindEventDescriptionToTooltip();
                WEEKEND1 = response.SchoolWeekEnd.WEEKEND1;
                WEEKEND2 = response.SchoolWeekEnd.WEEKEND2;
                if (WEEKEND1 != '') {
                    $(`.fc-${WEEKEND1.substring(0, 3).toLowerCase()} span.fc-day-number`).addClass('weekend-holiday');
                    $('.fc-day.fc-' + WEEKEND1.substring(0, 3).toLowerCase()).css('background-color', 'rgba(162, 121, 194, 0.4) !important');
                }
                if (WEEKEND2 != '') {
                    $(`.fc-${WEEKEND2.substring(0, 3).toLowerCase()} span.fc-day-number`).addClass('weekend-holiday');
                    $('.fc-day.fc-' + WEEKEND2.substring(0, 3).toLowerCase()).css('background-color', 'rgba(162, 121, 194, 0.4) !important');
                }
                $(".btn-group .fc-next-button").on("click", function () {
                    if (WEEKEND1 != '') {
                        $(`.fc-${WEEKEND1.substring(0, 3).toLowerCase()} span.fc-day-number`).addClass('weekend-holiday');
                        $('.fc-day.fc-' + WEEKEND1.substring(0, 3).toLowerCase()).css('background-color', 'rgba(162, 121, 194, 0.4) !important');
                    }
                    if (WEEKEND2 != '') {
                        $(`.fc-${WEEKEND2.substring(0, 3).toLowerCase()} span.fc-day-number`).addClass('weekend-holiday');
                        $('.fc-day.fc-' + WEEKEND2.substring(0, 3).toLowerCase()).css('background-color', 'rgba(162, 121, 194, 0.4) !important');
                    }
                    objAttendanceCalendar.bindEventDescriptionToTooltip();
                });
                $(".btn-group .fc-prev-button").on("click", function () {
                    if (WEEKEND1 != '') {
                        $(`.fc-${WEEKEND1.substring(0, 3).toLowerCase()} span.fc-day-number`).addClass('weekend-holiday');
                        $('.fc-day.fc-' + WEEKEND1.substring(0, 3).toLowerCase()).css('background-color', 'rgba(162, 121, 194, 0.4) !important');
                    }
                    if (WEEKEND2 != '') {
                        $(`.fc-${WEEKEND2.substring(0, 3).toLowerCase()} span.fc-day-number`).addClass('weekend-holiday');
                        $('.fc-day.fc-' + WEEKEND2.substring(0, 3).toLowerCase()).css('background-color', 'rgba(162, 121, 194, 0.4) !important');
                    }
                    objAttendanceCalendar.bindEventDescriptionToTooltip();
                });
                objAttendanceCalendar.editCalendarEvent(0);
            });
        },
        loadCalendarListView = () => {
            $.post("/AttendanceSetting/AttendanceSetting/LoadCalendarListView", function (response) {
                $("#attCalenderList").html('');
                $("#attCalenderList").append(response);
                $('.selectpicker').selectpicker('refresh');
            });
        },
        deleteCalendarEvent = (SCH_ID, element) => {
            globalFunctions.notyConfirm($(element), translatedResources.deleteConfirm);
            $(element).unbind('okClicked');
            $(element).bind('okClicked', element, function (e) {
                $.post("/AttendanceSetting/AttendanceSetting/DeleteCalendarEvent", { SCH_ID: SCH_ID }, function (response) {
                    if (response.Success) {
                        var table = $('#tblCalendarEvent').DataTable();
                        table.row($(element).parents('tr')).remove().draw();
                    }
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                });
            });
        },
        bindEventDescriptionToTooltip = () => {
            var eventConTooltipList = $(".fc-title");
            eventConTooltipList.each((index, element) => {
                $(element).parent().attr({
                    "data-toggle": "tooltip",
                    "data-original-title": $(element)[0].innerText
                });
            });
            $('[data-toggle="tooltip"]').tooltip();
        }
    return {
        init: init,
        editCalendarEvent: editCalendarEvent,
        getGradeAndSectionList: getGradeAndSectionList,
        saveCalendarEvent: saveCalendarEvent,
        bindCalendarEvent: bindCalendarEvent,
        loadCalendarListView: loadCalendarListView,
        deleteCalendarEvent: deleteCalendarEvent,
        bindEventDescriptionToTooltip: bindEventDescriptionToTooltip
    }
}();
//----------End Functionality-------------------------

//----------Event Handling Functionality--------------
$(document).on("click", "#setWorkingWeekends", function () {
    if (this.checked) {
        $('#fridayWorking').show(1000);
        $('#saturdayWorking').show(1000);
    }
    else {
        $('#fridayWorking').hide(1000);
        $('#saturdayWorking').hide(1000);
    }
});
//$(document).on("change", "#ddlAcademicYearFilter", function () {
//    if ($("#toggler").is(':checked')) {
//        objAttendanceCalendar.bindCalendarEvent();
//    }
//    else {
//        objAttendanceCalendar.loadCalendarListView();
//    }
//});
//$(document).on("change", "#toggler", function () {
//    if ($(this).is(':checked')) {
//        objAttendanceCalendar.bindCalendarEvent();
//        $("#attenCalender").removeClass('d-none');
//        $("#attCalenderList").addClass('d-none');
//    }
//    else {
//        objAttendanceCalendar.loadCalendarListView();
//        $("#attenCalender").addClass('d-none');
//        $("#attCalenderList").removeClass('d-none');
//    }
//});

//----------End Event Handling Functionality----------
