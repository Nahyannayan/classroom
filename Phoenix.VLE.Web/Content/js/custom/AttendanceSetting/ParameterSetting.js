﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />
var parameterSetting = function () {
    addEditParameterSetting = (ParamaterId) => {
        debugger
        parameterSetting.showHidePageContent(true);
        $.post("/AttendanceSetting/AttendanceSetting/AddEditParameterSetting", { ParamaterId }, function (response) {
            $("#divAddEditParameterSetting").html('');
            $("#divAddEditParameterSetting").append(response);
            $(".selectpicker").selectpicker('refresh');
        });
    },
        loadParameterGrid = (Acd_id) => {
            debugger
            var Att_TypeID = $("#toggler").is(":checked") ? 1 : 2;
            $.post("/AttendanceSetting/AttendanceSetting/LoadParameterGrid", { Att_TypeID, Att_TypeID }, function (response) {
                $("#divParameterGrid").html('');
                $("#divParameterGrid").append(response);
            });
        },
        saveParameterSetting = () => {
            var APD_APM_ID = $("#toggler").is(":checked") ? 1 : 2;
            var parameterSettingArr = {
                ParamaterId: $("#ParamaterId").val(),
                AcdId: $("#AcdId").val(),
                APD_PARAM_DESCR: $("#APD_PARAM_DESCR").val(),
                ParameterTypeId: $("#ParameterTypeId").val() == "PRESENT" ? 1 : 2,
                ParameterDesc: $("#ParameterDesc").val(),
                ReportParameterDesc: $("#ReportParameterDesc").val(),
                ParameterDisplayCode: $("#ParameterDisplayCode").val(),
                ParameterDisplayOrder: $("#ParameterDisplayOrder").val()
            }
            
            debugger
            var IsValid = common.IsInvalidByFormOrDivId("frmParamSetting");
            if (IsValid) {
                $.post("/AttendanceSetting/AttendanceSetting/SaveParameterSetting", { parameterSetting: parameterSettingArr }, function (response) {
                    if (response.Success) {
                        parameterSetting.showHidePageContent(false);
                        parameterSetting.loadParameterGrid(0);
                    }
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                });
            }
        },
        deleteParameterSetting = (ParamaterId, element) => {
            common.notyDeleteConfirm(translatedResources.deleteConfirm, () => {
                $.post("/AttendanceSetting/AttendanceSetting/DeleteParameterSetting", { ParamaterId }, function (response) {
                    if (response.Success) {
                        var table = $("#tblParameterSetting").DataTable();
                        var DeletedTr = $(element).closest('tr');
                        table.row(DeletedTr).remove().draw(false);
                    }
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                });
            });
        },
        showHidePageContent = (IsShow = true) => {
            if (IsShow) {
                $("#divParaSettingGrid").hide();
                $('#btnBackDrawer').find('i').removeClass('fa-user-cog');
                $('#btnBackDrawer').find('i').addClass('fa-arrow-left');
            }
            else {
                $("#divAddEditParameterSetting").html('');
                $("#divParaSettingGrid").show();
                $('#btnBackDrawer').find('i').addClass('fa-user-cog');
                $('#btnBackDrawer').find('i').removeClass('fa-arrow-left');
            }
        }
    return {
        addEditParameterSetting: addEditParameterSetting,
        loadParameterGrid: loadParameterGrid,
        saveParameterSetting: saveParameterSetting,
        showHidePageContent: showHidePageContent,
        deleteParameterSetting: deleteParameterSetting
    }
}();
//----------Event Handling Functionality--------------
$(document).ready(function () {
    $('#toggler').bootstrapToggle('destroy').bootstrapToggle();
    parameterSetting.loadParameterGrid(0);

    $("#btnBackDrawer").on('click', function () {
        if ($(this).find('i').hasClass('fa-arrow-left')) {
            parameterSetting.showHidePageContent(false);
        }
    });
    $("#toggler").unbind('change').on("change", function (event) {
        parameterSetting.loadParameterGrid(0);
    });
    $("#btnBackDrawer").on('click', function () {
        if ($(this).find('i').hasClass('fa-arrow-left')) {
            parameterSetting.showHidePageContent(false);
        }
    });
});
//----------End Event Handling Functionality----------