﻿
var LeaveApproval = function () {
    var Init = function () {
        //smsCommon.init();
        LeaveApproval.loadLeaveApproval();
    },
        LeaveApprovalList = [],
        LeaveApprovalGradesList = [],
        loadLeaveApprovalGrid = ACD_ID => {
            $.get('/AttendanceSetting/AttendanceSetting/LoadLeaveApprovalGrid', { ACD_ID })
                .then(response => $('#leaveApprovalGrid').html(response))
                .catch(error => globalFunctions.showMessage(error.NotificationType, error.Message));
        },
        onSuccess = (response) => {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            LeaveApproval.loadLeaveApproval();            
        },
        onFailure = (response) => {
            globalFunctions.showMessage(response.NotificationType, response.Message);
        },
        GetLeaveApproval = (salId) => {
            if (LeaveApproval.LeaveApprovalList.some(x => x.SalId == salId)) {
                var object = LeaveApproval.LeaveApprovalList.find(x => x.SalId == salId);
                $('#SalId').val(object.SalId);
                $('#ddlAcademicYearForm').val(object.SAL_ACD_ID);
                smsCommon.loadGradesWithoutStream('#ddlGrades', object.SAL_ACD_ID)
                    .then(x => {
                        $('#ddlGrades').val(object.GradeId).selectpicker('refresh');
                    })
                $('#fromDate').val(object.FromDateStr);
                $('#toDate').val(object.ToDateStr);
                $('#ddlStaffList').val(object.EmployeeId);                
                $('#txtNoOfdays').val(object.SAL_NDAYS);
                $('.selectpicker').selectpicker('refresh');
                LeaveApproval.showHideLeaveApprovalForm(true);
            }
        },
        GetAddLeaveApproval = () => {
            $('#frmLeaveApproval')[0].reset();
            $('#SalId').val('');
            $('.selectpicker').selectpicker('refresh');
            LeaveApproval.showHideLeaveApprovalForm(true);
        },
        validateForm = function () {
            var isValid = smsCommon.validateByFormId('frmLeaveApproval');
            var isInRange = smsCommon.validateDateRangeTxt('#fromDate', '#toDate')
            if (isValid || !(isInRange))
                return false;
        },
        showHideLeaveApprovalForm = (isFormVisible) => {
            $('#divEditLeaveApproval,#LeaveApprovalMainGRid').hide();
            if (isFormVisible) {
                $('#divEditLeaveApproval').show();
                smsCommon.ResetFormValidation('frmLeaveApproval');
            } else {
                $('#LeaveApprovalMainGRid').show();
            }
        },
        loadLeaveApproval = () => {
            debugger
            //var acdid = $('#ddlAcademicYear').val();
            //if (globalFunctions.isValueValid(acdid)) {
                LeaveApproval.loadLeaveApprovalGrid(0);
                LeaveApproval.showHideLeaveApprovalForm(false);
            //}
        }
    return {
        Init,
        LeaveApprovalList,
        LeaveApprovalGradesList,
        loadLeaveApprovalGrid,
        onSuccess,
        onFailure,
        GetLeaveApproval,
        GetAddLeaveApproval,
        validateForm,
        showHideLeaveApprovalForm,
        loadLeaveApproval
    }
}();

$(document).ready(function () {
    LeaveApproval.Init();
});

$(document).on('change', '#ddlAcademicYear', function () {
    if (globalFunctions.isValueValid($(this).val()))
        LeaveApproval.loadLeaveApprovalGrid($(this).val());
});

$(document).on('change', '#ddlAcademicYearForm', function () {
    if (globalFunctions.isValueValid($(this).val())) {
        smsCommon.loadGradesWithoutStream('#ddlGrades', $(this).val());
    }
});