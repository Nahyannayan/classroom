﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />


var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var schoolBadge = function () {
    var init = function () {
        loadBadgeGrid();
        $("#tbl-SchoolBadgeList_filter").hide();
    },
        addSchoolBadge = function (source, BadgeId) {
            loadCreatePopup(source, translatedResources.add, BadgeId);
        },
        editSchoolBadge = function (source, BadgeId) {
            loadCreatePopup(source, translatedResources.edit, BadgeId);
        },
        loadCreatePopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.SchoolBadge;
            globalFunctions.loadPopup(source, '/SchoolInfo/SchoolBadge/AddEditSchoolBadge?BadgeId=' + id, title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: "Select"
                });
            });
        },
        onSaveBadgeSuccess = function (isAddmode) {          
           // schoolBadge.init();
            if (isAddmode == 'True') {
                globalFunctions.showSuccessMessage(translatedResources.AddSuccess);
            } else {
                globalFunctions.showSuccessMessage(translatedResources.EditSuccess);
            }
            $("#myModal").modal('hide');
            schoolBadge.init();
        },
        onSaveBadgeError = function (isAddmode) {
            init();
            if (isAddmode == 'True') {
                globalFunctions.showErrorMessage(translatedResources.AddError);
            } else {
                globalFunctions.showErrorMessage(translatedResources.EditError);
            }
            $("#myModal").modal('hide');
        }
        loadBadgeGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Title", "sTitle": translatedResources.Title, "sWidth": "20%", "sClass": "wordbreak" },
                { "mData": "Description", "sTitle": translatedResources.Description, "sWidth": "40%", "sClass": "wordbreak" },
                { "mData": "UploadedImage", "sTitle": translatedResources.Image, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initBadgeGrid('tbl-SchoolBadgeList', _columnData, '/SchoolInfo/SchoolBadge/LoadSchoolBadges');
        },
        initBadgeGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnDefs: [{
                    'orderable': false,
                    'targets': 2
                }],
                columnSelector: false
            };
            grid.init(settings);
        },
        deleteSchoolBadge = function (source,id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('SchoolBadge', '/SchoolInfo/SchoolBadge/DeleteSchoolBadge', 'tbl-SchoolBadgeList', source, id);
            }
        }
    

    return {
        init: init,
        addSchoolBadge: addSchoolBadge,
        loadCreatePopup: loadCreatePopup,
        onSaveBadgeSuccess: onSaveBadgeSuccess,
        onSaveBadgeError: onSaveBadgeError,
        loadBadgeGrid: loadBadgeGrid,
        editSchoolBadge: editSchoolBadge,
        deleteSchoolBadge: deleteSchoolBadge

    };
}();
$(document).ready(function () {
    schoolBadge.init();
    $('#tbl-SchoolBadgeList').DataTable().search('').draw();
    $(document).on('click', '#btnAddSchoolBadge', function () {
        schoolBadge.addSchoolBadge($(this));
    });
    $("#SchoolBadgeSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-SchoolBadgeList').DataTable().search($(this).val()).draw();

    });
});