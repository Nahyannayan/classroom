﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();

var ReportingSubTerm = function () {
    var init = function () {
        ReportingSubTerm.GetReportingSubTermDetail();
    },
        loadReportingSubTermPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.ReportingSubTerm;
            globalFunctions.loadPopup(source, '/SchoolInfo/ReportingTerm/InitAddEditReportingSubTermForm?id=' + id, title, 'gradingtemplate-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
            });
        },

        addReportingSubTermPopup = function (source) {
            ReportingSubTerm.loadReportingSubTermPopup(source, translatedResources.add);
        },
        editReportingSubTermPopup = function (source, id) {
            ReportingSubTerm.loadReportingSubTermPopup(source, translatedResources.edit, id);
        },
        GetReportingSubTermDetail = function () {
            $.post('/SchoolInfo/ReportingTerm/GetReportingSubTermDetail')
                .then(response => {
                    $('#divReportingSubTermGrid').html('');
                    $('#divReportingSubTermGrid').html(response);
                    $('#tbl-ReportingSubTerm').DataTable({
                        "bLengthChange": false,
                        //"bFilter": false,
                        //"bInfo": false,
                        "pageLength": 10,
                        "columnDefs": [
                            { "width": "25%", "targets": 0 },
                            { "width": "30%", "targets": 1 },
                            { "width": "15%", "targets": 2, "sType": "date-uk" },
                            { "width": "15%", "targets": 3, "sType": "date-uk" },
                            { "width": "15%", "targets": 4, "orderable": false }
                        ]
                    });
                })
        },
        deleteReportingSubTermDetail = function (ReportingSubTermId) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                $.post('/SchoolInfo/ReportingTerm/DeleteReportingSubTermDetail', { ReportingSubTermId })
                    .then(response => {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        if (response.Success) {
                            init();
                        }
                    });
            });
        },
        onSaveSuccess = function (response) {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },
        onFailure = function () {
            globalFunctions.onFailure();
        },
        lockUnlockSubTerm = function (ReportingSubTermId, IsLock = '') {
            IsLock = IsLock != '' ? IsLock.toBoolean() : false;
            IsLock = IsLock ? false : true;
            $.post('/SchoolInfo/ReportingTerm/LockUnlockSubTerm', { ReportingSubTermId, IsLock })
                .then(response => {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    if (response.Success) {
                        init();
                    }
                });
        }
    return {
        init,
        loadReportingSubTermPopup,
        addReportingSubTermPopup,
        editReportingSubTermPopup,
        GetReportingSubTermDetail,
        deleteReportingSubTermDetail,
        onSaveSuccess,
        onFailure,
        lockUnlockSubTerm
    }
}();

$(document).on('click', '#btnAddReportingSubTerm', function () {
    ReportingSubTerm.addReportingSubTermPopup($(this));
});
$(document).on('click', '#btnAddUpdateSubTerm', function () {
    var IsValid = common.IsInvalidByFormOrDivId('frmAddReportingSubTerm');
    if (IsValid) {
        $("#frmAddReportingSubTerm").submit();
    }
});
$(document).ready(function () {
    ReportingSubTerm.init();
}); 
