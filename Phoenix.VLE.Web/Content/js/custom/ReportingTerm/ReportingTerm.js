﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();

var ReportingTerm = function () {
    var init = function () {
        ReportingTerm.GetReportingTermDetail();
    },
        loadReportingTermPopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.ReportingTerm;
            globalFunctions.loadPopup(source, '/SchoolInfo/ReportingTerm/InitAddEditReportingTermForm?id=' + id, title, 'gradingtemplate-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
            });
        },

        addReportingTermPopup = function (source) {
            ReportingTerm.loadReportingTermPopup(source, translatedResources.add);
        },
        editReportingTermPopup = function (source, id) {
            ReportingTerm.loadReportingTermPopup(source, translatedResources.edit, id);
        },
        GetReportingTermDetail = function () {
            $.post('/SchoolInfo/ReportingTerm/GetReportingTermDetail')
                .then(response => {
                    $('#divReportingTermGrid').html('');
                    $('#divReportingTermGrid').html(response);
                    $('#tbl-ReportingTerm').DataTable({
                        "bLengthChange": false,
                        //"bFilter": false,
                        //"bInfo": false,
                        "pageLength": 10,
                        "columnDefs": [
                            { "width": "35%", "targets": 0, "orderable": true },
                            { "width": "15%", "targets": 1, "orderable": true },
                            { "width": "15%", "targets": 2, "orderable": true, "sType": "date-uk" },
                            { "width": "15%", "targets": 3, "orderable": true, "sType": "date-uk" },
                            { "width": "20%", "targets": 4, "orderable": false }
                        ]
                    });
                })
        },
        deleteReportingTermDetail = function (ReportingTermId) {
            globalFunctions.notyDltConfirm(translatedResources.deleteConfirm, () => {
                $.post('/SchoolInfo/ReportingTerm/DeleteReportingTermDetail', { ReportingTermId })
                    .then(response => {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        if (response.Success) {
                            init();
                        }
                    });
            });
        },
        onSaveSuccess = function (response) {
            globalFunctions.showMessage(response.NotificationType, response.Message);
            if (response.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },
        onFailure = function () {
            globalFunctions.onFailure();
        },
        openReportingSubTerm = function (id) {
            if (globalFunctions.isValueValid(id)) {
                $.post('/Shared/Shared/SetSession', { key: "SessionReportingTermId", value: id }, function (data) {
                    if (typeof id != 'undefined' && data == true)
                        window.open('/SchoolInfo/ReportingTerm/ReportingSubTerm', '_self');
                    else {
                        globalFunctions.onFailure();
                    }
                });
            }
        },
        lockUnlockTerm = function (ReportingTermId, IsLock = '') {
            IsLock = IsLock != '' ? IsLock.toBoolean() : false;
            IsLock = IsLock ? false : true;
            $.post('/SchoolInfo/ReportingTerm/LockUnlockTerm', { ReportingTermId, IsLock })
                .then(response => {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    if (response.Success) {
                        init();
                    }
                });
        }
    return {
        init,
        loadReportingTermPopup,
        addReportingTermPopup,
        editReportingTermPopup,
        GetReportingTermDetail,
        deleteReportingTermDetail,
        onSaveSuccess,
        onFailure,
        openReportingSubTerm,
        lockUnlockTerm
    }
}();

$(document).on('click', '#btnAddReportingTerm', function () {
    ReportingTerm.addReportingTermPopup($(this));
});
$(document).on('click', '#btnAddUpdateTerm', function () {
    var IsValid = common.IsInvalidByFormOrDivId('frmAddReportingTerm');
    if (IsValid) {
        $("#frmAddReportingTerm").submit();
    }
});
$(document).ready(function () {
    ReportingTerm.init();
});

