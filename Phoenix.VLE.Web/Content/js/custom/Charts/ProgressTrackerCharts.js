$(document).ready(function(){
    $("body").on("click", ".btn-filter", function () {
        $(".field-form").slideToggle();
        $(this).toggleClass('closed');
        $(this).hasClass("closed") ? ($(this).html('Hide Filter <i class="fas fa-chevron-up ml-2"></i>')) : ($(this).html('Show Filter <i class="fas fa-chevron-down ml-2"></i>'))
    });

    $('#btnFilterCharts').click(function () {
        ajax_GetFilteredCharts();
    });

});

function ajax_GetFilteredCharts() {

    var params = {
        academicYear: $('#academicYear').val(),
        subject: $('#subject').val(),
        classId: $('#class').val(),
        assessment: $('#assessment').val(),
        teacher: $('#teacher').val()
    }
    console.log(params);
    $('#AjaxLoader').fadeIn(500, function () { });
    $.ajax({
        url: '/ChartsDashboard/StudentProgressTracker/ChartFilter',
        method: 'post',
        data: params,
        success: function (data) {
            $('#section-charts').html(data);
            $('#AjaxLoader').fadeOut(500, function () { });
        }
    });
}



