$(document).ready(function(){
    $("body").on("click", ".btn-filter", function () {
        $(".field-form").slideToggle();
        $(this).toggleClass('closed');
        $(this).hasClass("closed") ? ($(this).html('Hide Filter <i class="fas fa-chevron-up ml-2"></i>')) : ($(this).html('Show Filter <i class="fas fa-chevron-down ml-2"></i>'))
    });

    $('#btnFilterCharts').click(function () {
        ajax_GetCharts();
    });
    ajax_GetStudents();
    $('#class').change(function () {
        ajax_GetStudents();
    });

    $('#assessment').change(function () {
        ajax_GetStudents();
    });

    $('#academicYear').change(function () {
        ajax_GetStudents();
    });

    //$('#student').change(function () {
    //    ajax_GetStudent();
    //});

});

function ajax_GetStudents() {

    var params = {
        classId: $('#class').val(),
        assessment: $('#assessment').val(),
        academicYear: $('#academicYear').val()
        //subject: $('#subject').val(),
        
        
        //teacher: $('#teacher').val()
    }
    console.log(params);
    
    $.ajax({
        url: '/ChartsDashboard/StudentPredictionAssesment/GetStudentDetail',
        method: 'post',
        data: params,
        success: function (data) {
            $('#student').empty();
            $('#student').append('<option value="">Select Student</option>');
            $.each(data, function (i, v) {
                $('#student').append('<option value="' + v.Id + '">' + v.Name + '</option>');
            });
            $('#student').selectpicker('refresh');
        }
    });
}

//function ajax_GetStudent() {
//    $.ajax({
//        url: '/ChartsDashboard/StudentPredictionAssesment/GetStudent',
//        method: 'post',
//        data: {
//            studentId:$('#student').val()
//        },
//        success: function (data) {
//            $('#studentimg').attr('src', data.PhotoPath);
//        }
//    });
//}


function ajax_GetCharts() {
    
    var params = {
        classId: $('#class').val(),
        assessment: $('#assessment').val(),
        academicYear: $('#academicYear').val(),
        studentId: $('#student').val()
        //subject: $('#subject').val(),


        //teacher: $('#teacher').val()
    }
    console.log(params);
    if ($('#student').val() == "") {
        //alert('Select Student.');
        //globalFunctions.showMessage("warning", "Select Student");
        globalFunctions.showWarningMessage('Select Student.');
    }
    else {
        $('#AjaxLoader').fadeIn(500, function () { });
        $.ajax({
            url: '/ChartsDashboard/StudentPredictionAssesment/GetAllCharts',
            method: 'post',
            data: params,
            success: function (data) {
                $('#section-charts').html(data);
                $('#AjaxLoader').fadeOut(500, function () { });
            }
        });
    }
}



