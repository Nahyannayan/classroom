﻿$(document).ready(function () {

    $("body").on("click", "#viewlist", function () {
        ajax_GetAssessmentData();
    });

    createAvgScoreColumnChart();
    //createDonutChart();
    createPredictionScoreColumnChart();
    //createAssmntScoreColumnChart();
    createAssmntColumnChart();

});

$(document).bind("kendo:skinChange", createAvgScoreColumnChart);
//$(document).bind("kendo:skinChange", createDonutChart);
$(document).bind("kendo:skinChange", createPredictionScoreColumnChart);
//$(document).bind("kendo:skinChange", createAssmntScoreColumnChart);
$(document).bind("kendo:skinChange", createAssmntColumnChart);

function ajax_GetAssessmentData() {

    var params = {
        classId: $('#class').val(),
        assessment: $('#assessment').val(),
        academicYear: $('#academicYear').val(),
        studentId: $('#student').val()
        //subject: $('#subject').val(),


        //teacher: $('#teacher').val()
    }
    console.log(params);
    $('#AjaxLoader').fadeIn(500, function () { });
        $.ajax({
            url: '/ChartsDashboard/StudentPredictionAssesment/GetAllAssessment',
            method: 'post',
            data: params,
            success: function (data) {
                //console.log(JSON.stringify(data));
                $('#assdetails').empty();
                
                $.each(data, function (i, v) {
                    console.log(JSON.stringify(v));
                    $('#assdetails').append('<tr><td>' + v.AcademicYear + '</td><td>' + v.Subject + '</td><td>' + v.Teacher + '</td><td>' + v.Class + '</td><td>' + v.Percentage + '</td></tr></tr>')
                });
                $('#AjaxLoader').fadeOut(500, function () { });
            }
        });
}



function createAvgScoreColumnChart() {
    if (columnAvgScoreChartData!="" && columnAvgScoreChartData!=null && columnAvgScoreChartData.length > 0) {
        var dataChart = [];

        $("#chartAvgScoreColumn_overlay").hide();

        $("#chartAvgScoreColumn").kendoChart({
            theme: "blueOpal",
            background: "white",
            //title: {
            //    text: "Avg Score by Class and Prediction"
            //},
            legend: {
                position: "bottom"
            },
            seriesDefaults: {
                type: "column",
                spacing: 0,
                labels: {
                    visible: true,
                    color: "black"

                }
            },
            transitions: true,
            seriesColors: ["#954F72", "#FFC000"],
            series: $.each(columnAvgScoreChartData[0].LabelNameValue, function (i, v) {
                dataChart.push(v);

            }),
            valueAxis: {
                labels: {
                    format: "{0}"
                },
                line: {
                    visible: false
                },
                min: 0,
                max: 120,
                majorUnit: 50,
                //axisCrossingValue: 0
            },
            categoryAxis: {
                categories: columnAvgScoreChartData[0].categories,
                line: {
                    visible: false
                },
                majorGridLines: {
                    visible: false
                }
                //labels: {
                //    padding: { top: 4 }
                //}
            },
            tooltip: {
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #"
            }
        });
    }
    else {
        $("#chartAvgScoreColumn_overlay").show();
        //$("#chartAvgScoreColumn").css("height", 400);
    }  
}

//function createDonutChart() {
//    $("#chartDonut").kendoChart({
//        theme: "blueOpal",
//        background: "white",
//        title: {
//            text: "Student Count by Prediction %"
//        },
//        //legend: {
//        //    position: "top"
//        //},
//        seriesDefaults: {
//            labels: {
//                template: "#= value # (#= kendo.format('{0:P}', percentage)#)",
//                position: "outsideEnd",
//                visible: true,
//                background: "transparent"
//            }
//        },
//        transitions: true,
//        series: [{
//            type: "donut",
//            data: donutChartData
//        }],
//        tooltip: {
//            visible: true,
//            template: "#= value # (#= kendo.format('{0:P}', percentage) #)"
//        }
//    });
//}

function createPredictionScoreColumnChart() {
    if (predictionScoreColumnChartData != "" && predictionScoreColumnChartData != null && predictionScoreColumnChartData.length > 0) {
        $("#chartPredictionScoreColumn_overlay").hide();

        var chartData = [];
        var categoryChart = [];

        for (var i = 0; i < predictionScoreColumnChartData.length; i++) {
            categoryChart.push(predictionScoreColumnChartData[i].Subject);
        }
        for (var i = 0; i < predictionScoreColumnChartData.length; i++) {
            chartData.push(predictionScoreColumnChartData[i].AvgPercentage);
        }

        if (predictionScoreColumnChartData.length > 10) {
            var chartHeight = predictionScoreColumnChartData.length * 50;
            //$("#chartBar").css("height", chartHeight);
        }

        $("#chartPredictionScoreColumn").kendoChart({
            //theme: "blueOpal",
            background: "white",
            //title: {
            //    text: "Predicted Score"
            //},
            legend: {
                visible: false,
                position:"bottom"
            },
            seriesDefaults: {
                type: "column",
                spacing: 0,
                labels: {
                    visible: true,
                    color: "black"
                }
            },
            transitions: true,
            series: [{
                name: "Score",
                data: chartData,
                
            }],
            valueAxis: {
                labels: {
                    format: "{0}"
                },
                min: 0,
                max:120,
                //majorUnit: 500,
                line: {
                    visible: false
                },
                minorGridLines: {
                    visible: false
                }
            },
            categoryAxis: {
                categories: categoryChart,
                majorGridLines: {
                    visible: false
                },
                line: {
                    visible: false
                },
                labels: {
                    padding: { top: 4 },
                    rotation: 310
                }
            },
            tooltip: {
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #"
            }
        });
    }
    else {
        $("#chartPredictionScoreColumn_overlay").show();
        //$("#chartPredictionScoreColumn").css("height", 400);
    }
}

//function createAssmntScoreColumnChart() {
    
//    if (predictionScoreColumnChartData != "" && predictionScoreColumnChartData != null && predictionScoreColumnChartData.length > 0) {
//        alert('see ' + JSON.stringify(predictionScoreColumnChartData[0].LabelNameValue));
//        alert(predictionScoreColumnChartData[0].categories.length);
//        var gg = [];
//        for (var ii = 0; ii < predictionScoreColumnChartData[0].categories.length; ii++) {
//            gg.push(predictionScoreColumnChartData[0].categories[ii]);
//            alert(gg);
//        }
        

//        var chartData = [];

//        $("#chartAssesmentColumn").kendoChart({
//            theme: "blueOpal",
//            background: "white",
//            title: {
//                text: "Assessment vs Prediction by Academic Year"
//            },
//            legend: {
//                position: "top"
//            },
//            seriesDefaults: {
//                /* Column */
//                type: "column",
//                spacing: 0,
//                labels: {
//                    visible: true,
//                    color: "black"

//                }

//                /* Area */
//                //type: "area",
//                //area: {
//                //    line: {
//                //        style: "smooth"
//                //    }
//                //},
//                //markers: {
//                //    visible: true
//                //}

//                /* Line */
//                //type: "line",
//                //style: "smooth"
//            },
//            series: $.each(predictionScoreColumnChartData[0].LabelNameValue, function (i, v) {
//                chartData.push(v);
//            }),
//            valueAxis: {
//                labels: {
//                    format: "{0}"
//                },
//                line: {
//                    visible: false
//                },
//                min: 0,
//                //max: 100,
//                //majorUnit: 50,
//                axisCrossingValue: 0
//            },
//            categoryAxis: {
//                categories: predictionScoreColumnChartData[0].categories,
//                line: {
//                    visible: false
//                },
//                labels: {
//                    padding: { top: 4 }
//                }
//            },
//            tooltip: {
//                visible: true,
//                format: "{0}%",
//                template: "#= series.name #: #= value #"
//            }
//        });
//    }
//}

function createAssmntColumnChart() {
    if (assmntColumnChartData != "" && assmntColumnChartData != null && assmntColumnChartData.length > 0) {
        $("#chartAssesmentColumn_overlay").hide();

        $('#viewlist').show();
        var chartData = [];

        $("#chartAssesmentColumn").kendoChart({
            theme: "bootstrap",
            background: "white",
            //title: {
            //    text: "Assessment vs Prediction by Academic Year"
            //},
            legend: {
                position: "bottom"
            },
            seriesDefaults: {
                /* Column */
                type: "column",
                spacing: 1,
                labels: {
                    visible: false,
                    color: "black"

                }

                /* Area */
                //type: "area",
                //area: {
                //    line: {
                //        style: "smooth"
                //    }
                //},
                //markers: {
                //    visible: true
                //}

                /* Line */
                //type: "line",
                //style: "smooth"
            },
            series: $.each(assmntColumnChartData[0].LabelNameValue, function (i, v) {
                chartData.push(v);
            }),
            valueAxis: {
                labels: {
                    format: "{0}"
                },
                line: {
                    visible: false
                },
                min: 0,
                max: 120,
                //majorUnit: 25,
                axisCrossingValue: 0
            },
            categoryAxis: {
                categories: assmntColumnChartData[0].categories,
                line: {
                    visible: false
                },
                majorGridLines: {
                    visible: false
                }
                //labels: {
                //    padding: { top: 4 }
                //}
            },
            tooltip: {
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #"
            }
        });
    }
    else {
        $('#viewlist').hide();
        $("#chartAssesmentColumn_overlay").show();
        //$("#chartAssesmentColumn").css("height", 400);
    }
}