﻿$(document).ready(function () {

    createColumnChart();
    createDonutChart();
    createSubjectColumnChart();
    createAssmntColumnChart();

});

$(document).bind("kendo:skinChange", createColumnChart);
$(document).bind("kendo:skinChange", createDonutChart);
$(document).bind("kendo:skinChange", createSubjectColumnChart);
$(document).bind("kendo:skinChange", createAssmntColumnChart);


function createColumnChart() {

    var dataChart = [];

    if (avgScoreChartData[0].LabelNameValue.length > 0) {

        $("#chartColumn_overlay").hide();

        $("#chartColumn").kendoChart({
            background: "white",
            legend: {
                position: "bottom"
            },
            seriesDefaults: {
                type: "column",
                spacing: 0,
                labels: {
                    visible: true,
                    color: "black"
                }
            },
            transitions: true,
            seriesColors: ["#954F72", "#FFC000"],
            series: $.each(avgScoreChartData[0].LabelNameValue, function (i, v) {
                dataChart.push(v);
            }),
            valueAxis: {
                labels: {
                    format: "{0}"
                },
                line: {
                    visible: false
                },
                min: 0,
                max: 100,
                majorUnit: 50,
                //axisCrossingValue: 0,
                //majorGridLines: {
                //    visible: false,
                //},
            },
            categoryAxis: {
                categories: avgScoreChartData[0].categories,
                majorGridLines: {
                    visible: false
                },
                //labels: {
                //    padding: { top: 4 }
                //}
            },
            tooltip: {
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #"
            }
        });
    }
    else {
        $("#chartColumn_overlay").show();
        //$("#chartColumn").css("height", 400);
    }    
}

function createDonutChart() {

    if (studentCountChartData.length > 0) {

        $("#chartDonut_overlay").hide();

        $("#chartDonut").kendoChart({
            theme: "blueopal", //bootstrap
            background: "white",
            //title: {
            //    text: "Student Count by Prediction %"
            //},
            //legend: {
            //    position: "top"
            //},
            seriesDefaults: {
                labels: {
                    template: "#= value # (#= kendo.format('{0:P}', percentage)#)",
                    position: "outsideEnd",
                    visible: true,
                    background: "transparent"
                }
            },
            transitions: true,
            series: [{
                type: "donut",
                data: studentCountChartData
            }],
            tooltip: {
                visible: true,
                template: "#= value # (#= kendo.format('{0:P}', percentage) #)"
            }
        });
    }
    else {
        $("#chartDonut_overlay").show();
        //$("#chartDonut").css("height", 400);
    }
}

function createSubjectColumnChart() {

    if (subjectChartData.length > 0) {

        $("#chartPredictedScoreBar_overlay").hide();

        var chartData = [];
        var categoryChart = [];

        for (var i = 0; i < subjectChartData.length; i++) {
            categoryChart.push(subjectChartData[i].Subject);
        }
        for (var i = 0; i < subjectChartData.length; i++) {
            chartData.push(subjectChartData[i].StudentCount);
        }

        $("#chartPredictedScoreBar").kendoChart({
            //theme: "blueOpal",
            background: "white",
            //title: {
            //    text: "Predicted Score Below 75%"
            //},
            legend: {
                visible: false
            },
            seriesDefaults: {
                type: "column",
                spacing: 0,
                labels: {
                    visible: true,
                    color: "black"
                }
            },
            transitions: true,
            seriesColors: ["#1CC88A"],
            series: [{
                name: "Student Count",
                data: chartData
            }],
            valueAxis: {
                labels: {
                    format: "{0}"
                },
                min: 0,
                //majorUnit: 500,
                line: {
                    visible: false
                },
                minorGridLines: {
                    visible: false
                }
            },
            categoryAxis: {
                categories: categoryChart,
                majorGridLines: {
                    visible: false
                },
                line: {
                    visible: false
                },
                labels: {
                    padding: { top: 4 },
                    rotation: 310
                }
            },
            tooltip: {
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #"
            }
        });
    }
    else {
        $("#chartPredictedScoreBar_overlay").show();
        //$("#chartPredictedScoreBar").css("height", 400);
    }
}

function createAssmntColumnChart() {

    if (assmntAvgScoreChartData[0].LabelNameValue.length > 0) {

        $("#chartAssesmentColumn_overlay").hide();

        var chartData = [];

        $("#chartAssesmentColumn").kendoChart({
            theme: "bootstrap",
            background: "white",
            //title: {
            //    text: "Assessment  wise Avg Score by Year"
            //},
            legend: {
                position: "bottom"
            },
            seriesDefaults: {
                /* Column */
                type: "column",
                spacing: 1,
                labels: {
                    visible: false,
                    color: "black"
                }
            },
            series: $.each(assmntAvgScoreChartData[0].LabelNameValue, function (i, v) {
                chartData.push(v);
            }),
            valueAxis: {
                labels: {
                    format: "{0}"
                },
                line: {
                    visible: false
                },
                min: 0,
                //max: 100,
                //majorUnit: 50,
                axisCrossingValue: 0
            },
            categoryAxis: {
                categories: assmntAvgScoreChartData[0].categories,
                majorGridLines: {
                    visible: false
                },
            },
            tooltip: {
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #"
            }
        });
    }
    else {
        $("#chartAssesmentColumn_overlay").show();
        //$("#chartAssesmentColumn").css("height", 400);
    }
}

