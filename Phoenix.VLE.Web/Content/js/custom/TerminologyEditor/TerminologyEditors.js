﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />
//var popoverEnabler = new LanguageTextEditPopover();
var terminologyEditors = function () {

    var init = function () {
        loadTerminologyEditorGrid();
        $("#tbl-TerminologyEditors_filter").hide();

    },

        loadTerminologyEditorPopup = function (source, mode, id) {
            var title = mode + ' '+translatedResources.Terminology;
            globalFunctions.loadPopup(source, '/TerminologyEditor/TerminologyEditor/InitAddEditTerminologyEditorForm?id=' + id, title, 'terminologyeditor-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                //popoverEnabler.attachPopover();
            });
        },

        addTerminologyEditorPopup = function (source) {
            loadTerminologyEditorPopup(source, translatedResources.add);
        },

        editTerminologyEditorPopup = function (source, id) {
            loadTerminologyEditorPopup(source, translatedResources.edit, id);
        },

        deleteTerminologyEditorData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('TerminologyEditors', '/TerminologyEditor/TerminologyEditor/DeleteTerminologyEditorData', 'tbl-TerminologyEditors', source, id);
            }
        },

        onFailure = function (data) {
            console.log("Failed");
            console.log(data);
            globalFunctions.showMessage(data.NotificationType, data.Message);
        },
        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        loadTerminologyEditorGrid = function () {
            console.log(translatedResources);
            var _columnData = [];
            _columnData.push(
                { "mData": "OldTerm", "sTitle": translatedResources.OldTerm, "sWidth": "45%" },
                { "mData": "NewTerm", "sTitle": translatedResources.NewTerm, "sWidth": "45%", "sClass": "open-details-control" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "10%" }
            );
            initTerminologyEditorGrid('tbl-TerminologyEditors', _columnData, '/TerminologyEditor/TerminologyEditor/LoadTerminologyEditorGrid');
        },

        initTerminologyEditorGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'orderable': false,
                    'targets': 2
                }],
            };
            grid.init(settings);
        };

    return {
        init: init,
        addTerminologyEditorPopup: addTerminologyEditorPopup,
        editTerminologyEditorPopup: editTerminologyEditorPopup,
        deleteTerminologyEditorData: deleteTerminologyEditorData,
        onSaveSuccess: onSaveSuccess,
        onFailure: onFailure,
        loadTerminologyEditorGrid: loadTerminologyEditorGrid
    };
}();

$(document).ready(function () {
    terminologyEditors.init();


    $(document).on('click', '#btnAddTerminologyEditor', function () {
        terminologyEditors.addTerminologyEditorPopup($(this));
    });
    //$("#tbl-TerminologyEditors_filter").addClass("d-none");
    $("#tbl-TerminologyEditors_filter").hide();
    $('#tbl-TerminologyEditors').DataTable().search('').draw();
    $("#TerminologyEditorsListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-TerminologyEditors').DataTable().search($(this).val()).draw();
    });
});



