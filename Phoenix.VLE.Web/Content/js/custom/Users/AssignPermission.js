﻿var formElements = new FormElements();
var userId = $('#UserID').val();

var userRoleInfo = function () {
    var loadUserRoleInfo = function () {
        var grid = new DynamicGrid('tbl_UserRoleDataList');
        var settings = {
            enableEditorOnCellDoubleClick: true,
            columnData: [
                { data: "RoleName", editable: true, editorType: 'text' },
                { data: "Actions", }
            ],
            simpleGrid: true,
            defaultSorting: true,
            columnWidths: [
                { "width": "75%", "targets": 0 },
                { "width": "25%", "targets": 1, 'orderable': false },
            ]
        }
        grid.init(settings);
    };
    var addNewRow = function () {
        var grid = new DynamicGrid('tbl_UserRoleDataList');
        if (grid != undefined) {
            grid.addNewEditableRow();
            var td = $('#tbl_UserRoleDataList > ul > li:last');
            var delButton = '<ul class="horizontal-list panel-controls-small">' +
                '<li><a class="glyphicon glyphicon-remove btnDelete" href="javascript:void(0);" onclick="deleteUserRole($(this), null, event);"></a></li>' +
                '</ul>';
            td.append(delButton);
            $('#tbl_UserRoleDataList > ul > li:last').attr('data-id', "-1");
            var rowPosition = $('#tbl_UserRoleDataList ul:last').position();
            $('#userRoleDataList').scrollTop(rowPosition.top);
        }
    };
    return {
        loadUserRoleInfo: loadUserRoleInfo,
        addNewRow: addNewRow
    };
}();

$(document).ready(function () {
    formElements.feSelect();
    loadMenuTree();
    globalFunctions.enableCascadedDropdownList("ddlApplications", "ddlModules", '/Users/AssignPermission/GetModuleList');
    //userRoleInfo.loadUserRoleInfo();
    //$('#rolesFortitle').html(translatedResources.roles);
    //if (translatedResources.roleId != undefined && translatedResources.roleId != '') {
    //    loadMenuTreeEdit(translatedResources.roleId, translatedResources.userId)
    //}
    //else {
    //    loadMenuTree();
    //}

    $('#tbl_UserRoleDataList ul li a').each(function () {
        var td_roleId = $(this).data('id').RoleId;
        if (translatedResources.roleId != undefined && translatedResources.roleId != '') {
            if (td_roleId == translatedResources.roleId) {
                $(this).addClass('active');
                $('#btnAdd').hide();
            }
            if (translatedResources.userName != undefined && translatedResources.userName != '') {
                $('#rolesFortitle').html('');
                $('#rolesFortitle').html(translatedResources.rolesFor + " " + translatedResources.userName);
            }
        }
    });
    $(document).on('click', '#tbl_UserRoleDataList ul li a', function () {
        $('#tbl_UserRoleDataList ul li > a').removeClass('active');
        $(this).addClass('active');
        if (translatedResources.roleId != undefined && translatedResources.roleId != '') {

            $('#moduleListtitle').html('');
            $('#moduleListtitle').html(translatedResources.moduleListFor + " " + $(this).data('id').RoleName);
            loadMenuTreeEdit($(this).data('id').RoleId, translatedResources.userId);
        }
        else
            loadPermissions();
    });

    $(document).on("change", "#ddlPermissionType", function () {
        var searchText = $(this).val().toLowerCase();
        $('#tbl_permissionDataList .permission-checkbox').each(function () {
            var showCurrentLi = $(this).attr('data-categoryid').toLowerCase().indexOf(searchText) !== -1;
            $(this).toggle(showCurrentLi);
        });
    });

    $(document).on("change", "#ddlModules", function () {
        loadMenuTree();
        var role = $('#tbl_UserRoleDataList ul li a.active');
        var selectedRole = role.data('id');
        var parentId = "";
        if (translatedResources.roleId !== undefined && translatedResources.roleId !== '') {
            var userId = translatedResources.userId;
        }

        if (selectedRole !== "" && selectedRole !== undefined) {
            loadPermissionList($(this).val(), $(this).textIfEnabledInDOM(), selectedRole.RoleId, parentId, userId);
        }
        else {
            globalFunctions.showMessage("warning", translatedResources.userRoleErrorMsg);
        }
    });

    $(document).on("change", "#ddlApplications", function () {
        loadMenuTree();

    });
    $('#tbl_permissionDataList').hide();

    $("#ddlApplications").trigger('change');
});

function deleteUserRole(source, id, event) {
    if (id == "" || id == null) {
        source.closest('tr').remove();
        return;
    }
    var params = { id: id };
    globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
    event.stopPropagation();
    $(document).unbind('okToDelete');
    $(document).bind('okToDelete', source, function (e) {
        $.ajax({
            url: "/UserRoles/UserRoles/DeleteUserRole",
            type: 'POST',
            async: false,
            contentType: 'application/json',
            data: JSON.stringify(params),
            success: function (data) {
                if (data.Success) {
                    source.closest('tr').remove();
                    globalFunctions.showMessage("success", translatedResources.updateSuccessMsg);
                }
                else
                    globalFunctions.showMessage("error", translatedResources.roleDeleteMsg);
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure(data, xhr, status); //Error Function
            }
        });
    });
}

//function for updateing select all permission
function updateAllPermission(source) {
    if (!$('#selectall').is(':checked')) {
        updatePermission(source);
        return;
    }

    globalFunctions.notyConfirm(source, translatedResources.selectAllPermissionMessage);
    $(document).unbind('okClicked');
    $(document).bind('okClicked', source, function (e) {
        updatePermission(source);
    });
    $(document).unbind('cancelClicked');
    $(document).bind('cancelClicked', source, function (e) {
        var chkBtn = $(e.target);
        chkBtn.prop('checked', !chkBtn.is(':checked'));
    });
}
///function for updating menu permission
function updatePermission(source) {
    var mappingDetails = [];
    var selectedRole = $('#tbl_UserRoleDataList ul li a.active').data('id');

    //console.log(source.parent().data("categoryid"));
    if (source.parent().data("categoryid") == 2) {
        if ($(source).parent().siblings().data("categoryid") == 1 && !$(source).parent().siblings().children("input:checkbox").is(":checked")) {
            //console.log($(source).parent().siblings().data("categoryid"));
            $(source).parent().siblings().children("input:checkbox").trigger("click");
        }
    }

    if (source.parent().data("categoryid") == 1) {

        if ($(source).parent().siblings().data("categoryid") == 2 && $(source).parent().siblings().children("input:checkbox").is(":checked")) {
            //console.log($(source).parent().siblings().data("categoryid"));
            $(source).parent().siblings().children("input:checkbox").trigger("click");
        }
    }


    var OperationType = source.is(':checked') ? "I" : "D";
    if (translatedResources.roleId != undefined && translatedResources.roleId != '') {
        var userId = translatedResources.userId;
    }
    if (source.data('action') == "selectAll") {
        $("#tbl_permissionDataList .permission-checkbox input:checkbox:visible").each(function () {
            $(this).prop('checked', source.is(':checked'));
            mappingDetails.push({
                PermissionId: $(this).data('permissionid'),
                GrantPermission: source.is(':checked')
            });
        });
    }
    else {
        $('#selectall').prop('checked', false);
        mappingDetails.push({
            PermissionId: source.data('permissionid'),
            GrantPermission: source.is(':checked')
        });
    }
    if (selectedRole.RoleId != "" && selectedRole.RoleId != undefined) {
        var saveparams = {
            mappingDetails: mappingDetails, operationType: OperationType, userId: userId, userRoleId: selectedRole.RoleId
        };
        $.ajax({
            type: 'POST',
            url: '/Users/AssignPermission/UpdatePermissionTypes',
            async: false,
            contentType: 'application/json',
            data: JSON.stringify(saveparams),
            success: function (data) {
                if (!($(source).parent().siblings().data("categoryid") == 1
                    && !$(source).parent().siblings().children("input:checkbox").is(":checked"))
                    && !($(source).parent().siblings().data("categoryid") == 2
                        && $(source).parent().siblings().children("input:checkbox").is(":checked"))) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);
                }

            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure(data, xhr, status);
            }
        });
    }
    else {
        globalFunctions.showMessage("error", translatedResources.userRoleErrorMsg);
    }
}


function loadPermissions() {
    $.ajax({
        type: 'POST',
        url: '/Users/AssignPermission/InitMenuPermission',
        async: false,
        contentType: 'application/json',
        success: function (data) {

            $('#divPermissionList').html(data);

            var node = $('#divShowMenuTree').tree('getSelected');
            if (node != null)
                showModuleRolePermission(node);
            $('#tbl_permissionDataList ').hide($('#tbl_permissionDataList .permission-checkbox').length == 0);
            $('.selectpicker').selectpicker('refresh');

        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}

function showModuleRolePermission(node) {
    var moduleId = node["id"];
    var moduleName = node["text"];
    var role = $('#tbl_UserRoleDataList ul li a.active');
    var selectedRole = role.data('id');
    var parentId = "";
    if (translatedResources.roleId != undefined && translatedResources.roleId != '') {
        var userId = translatedResources.userId;
    }
    if (node['children'] != undefined) {
        parentId = node["id"];
    }
    if (selectedRole != "" && selectedRole != undefined) {
        loadPermissionList(moduleId, moduleName, selectedRole.RoleId, parentId, userId);
    }
    else {
        globalFunctions.showMessage("warning", translatedResources.userRoleErrorMsg);
    }

}

function loadMenuTree() {
    var moduleId = $('#ddlModules').val();
    var moduleCode = $('#ddlApplications').val();

    if (moduleId > 0) moduleCode = "";
    $('#divShowMenuTree').tree({
        url: '/Users/AssignPermission/GetModuleTree?moduleid=' + $('#ddlModules').val() + '&moduleCode=' + moduleCode,
        loadFilter: function (rows) {
            return convert(rows);
        },
        onLoadSuccess: function () {
            ExpanTree();
            $(".action-userRole")[0].click();
        },
        onClick: function (node) {
            showModuleRolePermission(node);
        }
    });

}
function loadMenuTreeEdit(roleId, userId) {
    editedRoleId = roleId;
    selectedUserId = userId;
    $('#divShowMenuTree').tree({
        url: '/Users/AssignPermission/GetModuleTree?moduleid=' + $('#ddlModules').val() + '&moduleCode=',
        //url: '/UserRoles/AssignUserRoles/GetRolePermissionModuleTree?roleId=' + editedRoleId + '&userId=' + selectedUserId,
        loadFilter: function (rows) {
            return convert(rows);
        },
        onLoadSuccess: function () {
            ExpanTree();
        },
        onClick: function (node) {
            showModuleRolePermission(node, roleId, userId);
        }
    });
}
function ExpanTree() {
    $('#divShowMenuTree').tree('expandAll');
}

function showCheckedPermission(element) {
    $('#tbl_permissionDataList >.permission-checkbox input[type="checkbox"]:not(:checked)').closest('div').toggle(element.is(':not(:checked)'));
}

function loadPermissionList(moduleId, moduleName, roleId, parentId, userId) {
    $.ajax({
        type: 'POST',
        url: '/Users/AssignPermission/InitRolePermission',
        data: {
            moduleID: moduleId, moduleName: moduleName, roleId: roleId, parentId: parentId, userId: userId
        },
        success: function (data) {
            console.log(data);
            $('#panel-permissioninfo').html('');
            $('#panel-permissioninfo').html(data);
            $('#selectall').prop('checked', false);
            formElements.feSelect();
            $('.selectpicker').selectpicker('refresh');

        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}


function highlight(id) {
   
    $(".action-userRole").removeClass('text-primary text-semibold');
    $('#'+id).addClass('text-primary text-semibold');
    
}