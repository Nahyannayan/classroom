﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var staffUsers = function () {

    var init = function (schoolId) {
        loadStaffGrid(schoolId);
    },

        loadMappingPopup = function (source, id, displayName) {
            var title = displayName;
            globalFunctions.loadPopup(source, '/Users/UserList/InitMapUserLocationsForm?id=' + id, title, 'UserRole-dailog');

        },

        loadStaffGrid = function (schoolId) {
            var _columnData = [];
            _columnData.push(
                { "mData": "UserDisplayName", "sTitle": translatedResources.userDisplayName, "sWidth": "50%", "sClass": "wordbreak" },
                { "mData": "UserName", "sTitle": translatedResources.userName, "sWidth": "30%", "sClass": "wordbreak" },
                { "mData": "UserTypeName", "sTitle": translatedResources.userTypeName, "sWidth": "20%", "sClass": "wordbreak" }     
                //{ "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "20%" }
            );
            initStaffGrid('tbl-StaffList', _columnData, '/Users/UserList/LoadStaffGrid?schoolId=' + schoolId);
        },

        initStaffGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnDefs: [{
                    'orderable': false,
                    'targets': 2
                }],
                columnSelector: false
            };
            grid.init(settings);
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        };

    return {
        init: init,
        loadStaffGrid: loadStaffGrid,
        loadMappingPopup: loadMappingPopup,
        onSaveSuccess: onSaveSuccess
    };
}();

$(document).ready(function () {
    staffUsers.init(-123); //negative value used for no data , because 0 is used for all users irrespective of schoolId

    $("#SchoolId").on("change", function () {
        var selectedSchool = $(this).val();
        if (selectedSchool == "") {
            selectedSchool = -123; //negative value used for no data , because 0 is used for all users irrespective of schoolId
        }

        staffUsers.init(selectedSchool);
    });

    schoolMappingCommon.init('/Users/UserList/SaveUserLocationMapData', '/Users/UserList/GetCities/', '/Users/UserList/GetBusinessUnits/');
});