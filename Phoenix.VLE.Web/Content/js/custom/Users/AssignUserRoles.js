﻿var formElements = new FormElements();
var editedRoleId = "";
var selectedUserId = "";
$(document).ready(function () {
    formElements.feSelect();
    loadUserList();
    loadMenuTree();

    $("#usertype").change(function () {
        loadUserList();
    });

    $('#availableUserSearch').off("keyup").on('keyup',  function (e) {
       
        var searchText = $(this).val().toLowerCase();
        $('#loaduser ul li> a').each(function () {
            var showCurrentLi = $(this).attr('data-search-text').toLowerCase().indexOf(searchText) !== -1;
            if (showCurrentLi == true) {
                $(this).show();
                $(this).addClass("d-inline-block");
            }
            else {
                $(this).removeClass("d-inline-block");
                $(this).hide();
            }
        });
    });

    $(document).on('click','.selectUser', function (e) {
        showUserRole($(this).data('userid'), $(this).data('username'));

        $('#loaduser ul li > a').each(function () {
            $(this).removeClass('active');
        });
        $(this).addClass('active');
    });

    $(document).on('click', '.selectRole', function (e) {
        if ($(this).data('userid') != '' && $(this).data('userid') != undefined) {
            addUserRole($(this).data('roleid'), $(this).data('userid'), $(this).data('username'));
        }
        else {
            globalFunctions.showWarningMessage(translatedResources.SelectUserMsg);
        }
    });

});

function loadUserList() {
    $.ajax({
        url: '/Users/AssignUserRoles/InitUserList',
        type: 'POST',
        data: { usertypeid: 3 },
        success: function (data) {
            $("#loaduser").html(data);
            $(".user-list").mCustomScrollbar({
                setHeight: "580",
                scrollbarPosition: "inside"
            });
            $('#loaduser ul li > a').addClass("py-2 d-inline-block");
        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure(data, xhr, status);  //Error Function
        }
    });
}

function showUserRole(userId, userName) {
    $.ajax({
        type: 'POST',
        url: '/Users/AssignUserRoles/InitUserRoleList',
        data: { userId: userId, username: userName },
        success: function (data) {
            $("#divloadRole").html(data);
            //$("#rolestitle").html(userName);
            
        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure(data, xhr, status);  //Error Function
        }
    });
}
//Role Add
function addUserRole(roleId, userId, userName) {
    if (userId !== "") {
        $.ajax({
            type: 'POST',
            url: '/Users/AssignUserRoles/SaveUserRoleMapping',
            data: { userId: userId, roleId: roleId, username: userName, operationType: 'I' },
            success: function (data) {
                if (data.Success !== undefined || data.Success === false) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);

                } else {
                    $("#divUserRolePermission").html(data);
                    showUserRole(userId, userName);
                    globalFunctions.showSuccessMessage("Updated Successfully", translatedResources.addSuccessMsg);
                }

            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure(data, xhr, status);  //Error Function
            }
        });
    }
    else {
        globalFunctions.showMessage("error", translatedResources.userErrorMsg);
    }
}
///assgined user
function deleteUserRole(source, roleId, userId, userName) {
    globalFunctions.notyDeleteConfirm(source, translatedResources.deleteConfirm);
    $(document).unbind('okToDelete');
    $(document).bind('okToDelete', source, function (e) {
        $.ajax({
            type: 'POST',
            url: '/Users/AssignUserRoles/SaveUserRoleMapping',
            data: { userId: userId, roleId: roleId, username: userName, operationType: 'D' },
            success: function (data) {
                //if (data.Success !== undefined || data.Success === false) {
                //    globalFunctions.ShowMessage(data.NotificationType, data.Message);

                //} else {
                //    globalFunctions.ShowMessage("success", translatedResources.updateSuccessMsg);
                //    if ($("#myModal").is(':visible')) {
                //        $("#showAssignPermissions").html(data);
                //        $('#showAssignPermissions #tbl_permissionDataList > tbody > tr > td .btnEdit').remove();
                //    }
                //    $("#divUserRolePermission").html(data);
                //    showUserRole(userId, userName);
                //}

                if (data.Success !== undefined || data.Success === false) {
                    globalFunctions.showMessage(data.NotificationType, data.Message);

                } else {
                    $("#divUserRolePermission").html(data);
                    showUserRole(userId, userName);
                    globalFunctions.showSuccessMessage("Updated successfully", translatedResources.addSuccessMsg);
                }
            },
            error: function (data, xhr, status) {
                globalFunctions.onFailure(data, xhr, status);  //Error Function
            }
        });
    });
}

function editUserRole(roleId, userId, userName) {
    var parameter = "userId=" + userId + "&roleId=" + roleId + "&userName=" + userName;
    window.open("/Users/UserRoles/CustomizeRole?" + parameter, "_blank");
}

function loadMenuTree(roleId, userId) {
    editedRoleId = roleId;
    selectedUserId = userId;
    $('#divShowMenuTree').tree({
        url: '/Users/AssignUserRoles/GetRolePermissionModuleTree?roleId=' + editedRoleId + '&userId=' + selectedUserId,
        loadFilter: function (rows) {
            return convert(rows);
        },
        onClick: function (node) {
            showModuleRolePermission(node, roleId, userId);
        }
    });
}

function showModuleRolePermission(node, roleId, userId) {
    var moduleid = node["id"];
    var modulename = node["text"];
    if (roleId !== "") {
        if (node['children'] === undefined) {
            $.ajax({
                type: 'POST',
                url: '/Users/UserRoles/InitRolePermission',
                data: { moduleID: moduleid, moduleName: modulename, roleId: roleId, userId: userId, parentId: null },
                success: function (data) {
                    $('#panel-permissioninfo').html(data);
                },
                error: function (data, xhr, status) {
                    globalFunctions.onFailure(data, xhr, status);
                }
            });
        }
        else {
            globalFunctions.ShowMessage("error", translatedResources.parentclickerrorMsg);
        }
    }
    else {
        globalFunctions.ShowMessage("error", translatedResources.userRoleErrorMsg);
    }
}

function updatePermission(source) {
    var mappingDetails = [];
    var OperationType = source.is(':checked') ? "I" : "D";
    if (source.data('action') === "selectAll") {
        $("#tbl_permissionDataList tbody tr").each(function () {
            $(this).find('input[type="checkbox"]').prop('checked', source.is(':checked'));
            mappingDetails.push({
                PermissionId: $(this).find('input[type="checkbox"]').data('permissionid'),
                GrantPermission: source.is(':checked'),
            });
        });
    }
    else {
        mappingDetails.push({
            PermissionId: source.data('permissionid'),
            GrantPermission: source.is(':checked'),
        });
    }
    var saveparams = { mappingDetails: mappingDetails, operationType: OperationType, userId: selectedUserId, userRoleId: editedRoleId };
    $.ajax({
        type: 'POST',
        url: '/Users/UserRoles/UpdatePermissionTypes',
        async: false,
        contentType: 'application/json',
        data: JSON.stringify(saveparams),
        success: function (data) {
            globalFunctions.ShowMessage("success", translatedResources.updateSuccessMsg);
        },
        error: function (data, xhr, status) {
            globalFunctions.onFailure(data, xhr, status);
        }
    });
}


