﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var userFeelingStatus = function () {

    var onSubmitUserFeelings = function onSubmitUserFeelings(data) {
        if (data.Logo == null)
            data.Logo ="024-happy-12.png";
        $('#imgFeelingSrc').attr('src', "/Content/VLE/img/smileys/" + data.Logo);
        $("#imgProfileFeelingSrc").attr('src', "/Content/VLE/img/smileys/" + data.Logo);
        $('#spnFeelings').html(data.FeelingType);
        $('#spnStudentFeelings').html(data.FeelingType);
        $("#basicExampleModal").modal('hide');

        $("span.userMood img").attr('src', "/Content/VLE/img/smileys/" + data.Logo);
    },
        loadUserStatus = function (source) {

            var title = translatedResources.ShoutOut;
            //  var title = "Shout Out";
            globalFunctions.loadPopup(source, '/Home/UserStatus', title, 'UserRole-dailog');
        },
        loadUserAvatar = function (source) {
            debugger;
            var title = translatedResources.EditProfile;
            //  var title = "Edit Profile";
            globalFunctions.loadPopup(source, '/Home/LoadProfileAvatar', title, 'UserRole-dailog');
        },
        uploadUserProfile = function () {
            debugger;
            var formData = new FormData();
            var file = document.getElementById("ProfilePhoto").files[0];
            formData.append("ProfilePhoto", file);
            formData.append("AvatarId", $('#AvatarId').val());
            formData.append("AvatarLogo", $('#AvatarLogo').val());
            $.ajax({
                type: 'POST',
                url: '/Home/SaveUserProfile',
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {

                    // If logged in user is Admin
                    if ($("input#hdnUsrTypeAdmin").val().toLowerCase() == true) {
                        $('#userProfilePartial').html(result);
                    }
                    else {
                       // $('#usrProfSection').empty();
                        $("#idAvatarImage").empty();
                        $('#usrProfSection').html(result);
                    }

                    //globalFunctions.showMessage(result.NotificationType, result.Message);
                    
                    //setTimeout(function () { location.reload(); }, 2000);
                },
                error: function (msg) {
                    globalFunctions.onFailure();
                }
            });
            $("#myModal").modal('hide');
            //setTimeout(function () { location.reload(); },2000);
        },
        validationImage = function (e) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'];

            if ($.inArray($('input[type=file]').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                globalFunctions.showErrorMessage(translatedResources.ImageValidation);
                $('input[type=file]').val('');
            }
        },
        toggleActiveClass = function (e) {
            $('div.smiley').removeClass('active');
            $(e).addClass('active');

        },
        onSaveStatusSuccess = function (data) {
            
            var status = translatedResources.UpdateStatus;
            // var status = "Update your status";
            if (data.CureentStatus != null && data.CureentStatus != "") {
                status = data.CureentStatus;
            }

            $('#prgShoutoutStatus').html(status);
            $('#prgShoutout').html(status + '<a href="javascript:void(0)" id="btnshoutout" style="py-0 d-inline-block bg-transparent"> <i class="far fa-edit"></i></a>');
            $('#prgdashboardShoutOut').html(status);
            $("#myModal").modal('hide');
        };
    return {
        loadUserStatus: loadUserStatus,
        onSaveStatusSuccess: onSaveStatusSuccess,
        uploadUserProfile: uploadUserProfile,
        loadUserAvatar: loadUserAvatar,
        onSubmitUserFeelings: onSubmitUserFeelings,
        validationImage: validationImage,
        toggleActiveClass: toggleActiveClass
    };
}();

$(document).ready(function () {
    $(document).on('click', '#btnshoutout', function () {
        userFeelingStatus.loadUserStatus($(this));
    });

    $(document).on('click', '#IdimgUserFeeling', function () {
        var imgScrId = $(this).attr('data-imgId');
        $('#hdnFeelId').val(imgScrId);
        $(this).addClass('active');
    });
    $(document).on('click', '#userProfileAvatar', function () {
        var imgScrId = $(this).attr('data-imgId');
        var avtarName = $(this).attr('data-avtarName');
        $('#AvatarId').val(imgScrId);
        $('#AvatarLogo').val(avtarName);
        $(this).addClass('active');
    });
    $(document).on('click', '.smiley', function (e) {
        $('#frmUserFeelings .smiley.active').removeClass('active');
        $(this).not('.divStudentBadge').addClass('active');
    });
});



