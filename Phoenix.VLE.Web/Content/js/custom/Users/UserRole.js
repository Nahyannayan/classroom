﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var userRoles = function () {

    var init = function () {
        loadUserRoleGrid();
    },

        loadUserRolePopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.userRole;
            globalFunctions.loadPopup(source, '/Users/UserRole/InitAddEditUserRoleForm?id=' + id, title, 'UserRole-dailog');
            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                formElements.feSelect();
                popoverEnabler.attachPopover();
            });
        },

        addUserRolePopup = function (source) {
            loadUserRolePopup(source, translatedResources.add);
        },

        editUserRolePopup = function (source, id) {
            loadUserRolePopup(source, translatedResources.edit, id);
        },

        deleteUserRoleData = function (source, id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('UserRole', '/Users/UserRole/DeleteUserRoleData', 'tbl-UserRole', source, id);
            }
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        viewUsersForRole = function (source, userRoleId) {
            var title = translatedResources.users;
            globalFunctions.loadPopup(source, '/Users/UserRole/GetUsersListView', title, 'UserRole-dailog modal-lg');
            $(source).off("modalLoaded");

            $(source).on("modalLoaded", function () {
                var _columnData = [];
                _columnData.push(
                    { "mData": "UserName", "sTitle": translatedResources.userName, "sWidth": "40%" },
                    { "mData": "UserDisplayName", "sTitle": translatedResources.fullName, "sWidth": "60%" }
                );
                initUserListForRoleGrid('tbl-UserListForRole', _columnData, '/Users/UserRole/UserListForRole?id=' + userRoleId);
            });
        },

        initUserListForRoleGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);            
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                processing: true,
                searching: true
            };
            grid.init(settings);
        },

        loadUserRoleGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "UserRoleName", "sClass": "text-center", "sTitle": translatedResources.userRole, "sWidth": "50%" },
                { "mData": "UserCount", "sTitle": translatedResources.userCount, "sWidth": "20%" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "30%" }
            );
            initUserRoleGrid('tbl-UserRole', _columnData, '/Users/UserRole/LoadUserRoleGrid');
        },

        initUserRoleGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                processing: true,
                searching: true
            };
            grid.init(settings);
        };

    return {
        init: init,
        addUserRolePopup: addUserRolePopup,
        editUserRolePopup: editUserRolePopup,
        deleteUserRoleData: deleteUserRoleData,
        onSaveSuccess: onSaveSuccess,
        loadUserRoleGrid: loadUserRoleGrid,
        viewUsersForRole: viewUsersForRole
    };
}();

$(document).ready(function () {
    userRoles.init();

    $(document).on('click', '#btnAddUserRole', function () {
        userRoles.addUserRolePopup($(this));
    });
    $('#tbl-UserRole').DataTable().search('').draw();
    $("#UserRoleListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-UserRole').DataTable().search($(this).val()).draw();
    });


});



