﻿/// <reference path="../../common/global.js" />
var popoverEnabler = new LanguageTextEditPopover();
var Error = function () {

    var init = function () {
        LoadSchoolGoalGrid();
        $("#tbl-SchoolSkillSet_filter").hide();
    },

        loadCategoryPopup = function (source, mode, id) {
            debugger
            var title = translatedResources.LogFiles;
           
            globalFunctions.loadPopup(source, '/Users/ErrorLog/InitAddLogForm?id=' + id, title, 'SchoolSkillSet-dailog');

            $(source).off("modalLoaded");
            $(source).on("modalLoaded", function () {
                popoverEnabler.attachPopover();
                //$("#tbl-SuggestionCategories_filter").addClass("d-none");
                $("#tbl-SchoolSkillSet_filter").hide();
                $('#tbl-SchoolGoal').DataTable().search('').draw();
                $("#SchoolSkillSetListSearch").on("input", function (e) {
                    e.preventDefault();
                    $('#tbl-SchoolGoal').DataTable().search($(this).val()).draw();

                });
            });
        },

        addCategoryPopup = function (source) {
           
        },

        editCategoryPopup = function (source, id) {
            loadCategoryPopup(source, translatedResources.edit, id);
        },

        deleteSchoolSkillSetData = function (source, id) {
           
        },

        onSaveSuccess = function (data) {
            globalFunctions.showMessage(data.NotificationType, data.Message);
            if (data.Success) {
                $("#myModal").modal('hide');
                init();
            }
        },

        LoadSchoolGoalGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "User", "sTitle": translatedResources.User, "sWidth": "50%", "sClass": "wordbreak" },
                { "mData": "ErrorText", "sTitle": translatedResources.ErrorMessage, "sWidth": "15%", "sClass": "open-details-control wordbreak" },
                { "mData": "UserIpAddress", "sTitle": translatedResources.UserIpAddress, "sWidth": "15%" },
                { "mData": "WebServerIpAddress", "sTitle": translatedResources.WebServerIpAddress, "sWidth": "10%" },
                { "mData": "ReportingDate", "sTitle": translatedResources.ReportingDate, "sWidth": "10%" },
                { "mData": "Actions", "sClass": "text-center no-sort", "sTitle": translatedResources.actions, "sWidth": "10%" }
            );
            initSchoolSkillSetGrid('tbl-SchoolGoal', _columnData, '/Users/ErrorLog/LoadErrorLogs');
        },

        initSchoolSkillSetGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnSelector: false,
                columnDefs: [{
                    'targets': 5,
                    'orderable': false
                }]
            };
            grid.init(settings);
        };

    return {
        init: init,
        addCategoryPopup: addCategoryPopup,
        editCategoryPopup: editCategoryPopup,
        deleteCategoryData: deleteSchoolSkillSetData,
        onSaveSuccess: onSaveSuccess,
        LoadSchoolGoalGrid: LoadSchoolGoalGrid
    };
}();

$(document).ready(function () {
    Error.init();


    
    //$("#tbl-SuggestionCategories_filter").addClass("d-none");
    $("#tbl-SchoolSkillSet_filter").hide();
    $('#tbl-SchoolGoal').DataTable().search('').draw();
    $("#SchoolSkillSetListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-SchoolGoal').DataTable().search($(this).val()).draw();

    });
});



