﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />

var users = function () {

    var init = function () {
        loadUserGrid();
    },        

        loadUserGrid = function () {
            var schoolId = $('#hdnSchoolId').val();
            var _columnData = [];
            _columnData.push(
                { "mData": "UserDisplayName", "sTitle": translatedResources.userDisplayName, "sWidth": "30%", "sClass": "wordbreak" },
                { "mData": "UserName", "sTitle": translatedResources.userName, "sWidth": "30%", "sClass": "wordbreak" },              
                { "mData": "UserTypeName", "sTitle": translatedResources.userTypeName, "sWidth": "20%", "sClass": "wordbreak" },  
                { "mData": "UserStatus", "sTitle": translatedResources.userstatus, "sWidth": "20%", "sClass": "wordbreak" }
            );
            initUserGrid('tbl-UserList', _columnData, '/Users/UserList/LoadUserGrid?schoolId=' + schoolId);
        },

        initUserGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnDefs: [{
                    'orderable': false,
                    'targets': 3
                }],
                columnSelector: false
            };
            grid.init(settings);
        };

    return {
        init: init,
        loadUserGrid: loadUserGrid
    };
}();

$(document).ready(function () {
    users.init();

    //var _target = $('#tbl-UserList');
    //_target.off('DynamicGrid:RowCallback');
    //_target.on('DynamicGrid:RowCallback', function (e, row, data, index) {
    //    $(row).data('id', data.Id);
    //    var loginAsButton = "<button type='button' class='table-action-icon-btn'><i class='fas fa-sign-in-alt'></i></button>";
    //    var actionButtonsHtmlTemplate = "<div class='tbl-actions'>" + loginAsButton + "<button type='button' class='table-action-icon-btn' onclick='subjects.editSubjectPopup($(this),{0})'><i class='fas fa-pencil-alt'></i></button><button type='button' class='table-action-icon-btn' onclick='subjects.deleteSubjectData($(this),{0})'><i class='far fa-trash-alt'></i></button></div>";
    //    $(row).find('td:last').html(actionButtonsHtmlTemplate);
    //});

    $("#tbl-UserList_filter").hide();
    $('#tbl-UserList').DataTable().search('').draw();
    $("#UserListSearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-UserList').DataTable().search($(this).val()).draw();
    });

});



