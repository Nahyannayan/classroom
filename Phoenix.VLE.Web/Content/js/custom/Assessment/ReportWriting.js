﻿

var reportWriting = function () {
    var init = function () {
        if ($('#reportWritingReportCard option').length <= 1) {
            $('#reportWritingAcademicYearDDL').trigger('change');
        }
    },
        viewReportWriting = function (element) {
            if (!smsCommon.IsInvalidByFormOrDivId('reportWritingFiltersForm'))
                return;
            var studentId = $(element).data("studentId") || $('#reportWritingStudent').val();
            $.get('/Assessment/Assessment/GetReportWriting',
                {
                    acdId: $('#reportWritingAcademicYearDDL').val(),
                    gradeId: $('#reportWritingGrades').val(),
                    rpfId: $('#reportWritingReportSchedule').val(),
                    studentId: studentId,
                    rsmId: $('#reportWritingReportCard').val(),
                    sectionId: $('#reportWritingSection').val()
                })
                .then(response => {
                    $('#reportWritingDiv').html(response);
                    $("#reportWritingStudent").val(studentId).selectpicker('refresh');
                    $(".toggle-row").click(function (event) {
                        event.stopPropagation();
                        if ($(this).children("i").hasClass('fa-plus-square')) {
                            $(this).children("i").removeClass('fa-plus-square').addClass('fa-minus-square');
                        } else {
                            $(this).children("i").removeClass('fa-minus-square').addClass('fa-plus-square');
                        }
                        var $target = $(event.target);
                        if ($target.closest("td").attr("colspan") > 1) {
                            $target.slideUp();
                        } else {
                            $target.closest("tr").next().find(".subjectList").slideToggle();
                        }
                    });
                    smsCommon.init();
                    var index = $("#reportWritingStudent").prop('selectedIndex');
                    var length = document.getElementById('reportWritingStudent').options.length;
                    //to navigate student list using dropdown
                    if (index <= 1) {
                        $('#btnPrevStudent').addClass("d-none");
                    } else {
                        $('#btnPrevStudent').removeClass("d-none");
                        $('#btnPrevStudent').data("studentId", document.getElementById('reportWritingStudent').options[index - 1].value);
                    }

                    if (index >= length) {
                        $('#btnNextStudent').addClass("d-none");
                    } else {
                        $('#btnNextStudent').removeClass("d-none");
                        $('#btnNextStudent').data("studentId", document.getElementById('reportWritingStudent').options[index + 1].value);
                    }
                    if (!globalFunctions.isValueValid(element))
                        $('#reportWritingFiltersForm,#reportWritingDiv').slideToggle();
                });
        },
        validateForm = function (formId) {
            if (smsCommon.IsInvalidByFormOrDivId(formId, true))
                return true;
            globalFunctions.showWarningMessage(translatedResources.pleaseResolveError);
            $('.border-danger').each(function (i, e) {
                if ($('a.' + $(e).attr('data-tr') + ' > i').hasClass('fa-plus-square'))
                    $('a.' + $(e).attr('data-tr')).click();
            })
            return false;
        },
        OpenComments = function (id) {
            var values = $('#' + id).val();
            $('#prgComments').html(values);
            $('.commentbox').modal('show');
        },
        OpenCommentModal = function (id, stuid) {
            studentId = stuid;
            $('#commentBoxid').val(id);
            if (id == $('#commentBoxid').val()) {
                $('#tblassessmentComment tbody tr td input[type="checkbox"]').each(function () {
                    $(this).prop('checked', false);
                });
            }
            else {
                $('#ddlassessmentCategory').val("").selectpicker('refresh');
                $('#divJtableComments').empty();
                $('#commentBoxid').val(id);
            }
            $('.modal.selectCategory').modal('show');
        },
        onSuccess = function (response) {
            smsCommon.onSuccess(response);
            if (response.Success && !($('#btnNextStudent').hasClass('d-none'))) {
                $('#btnNextStudent').click();
            }
        }
    return{
        init,
        viewReportWriting,
        validateForm,
        OpenComments,
        AddComments,
        OpenCommentModal,
        onSuccess
    }
}();

$(document)
    .on('change', "#reportWritingAcademicYearDDL", function () {
        
        var academicYearId = $(this).val();

        smsCommon.bindSingleDropDownByParameterAsync('#reportWritingReportCard',
                '/Assessment/Assessment/FetchReportHeaders',
                { id: academicYearId },
                '',
                window.translatedResources.selectOption)
            .then(() => window.setDropDownValueAndTriggerChange('reportWritingReportCard'));

    }).on('change', "#reportWritingReportCard", function () {
        var academicYearId = $('#reportWritingAcademicYearDDL').val();
        var rsmId = $(this).val();

        smsCommon.bindSingleDropDownByParameterAsync('#reportWritingGrades',
            '/Assessment/Assessment/GetGradesAccess',
            { acd_id: academicYearId, rsm_Id: rsmId },
            '',
            window.translatedResources.selectOption)
            .then(() => window.setDropDownValueAndTriggerChange('reportWritingGrades'));

        smsCommon.bindSingleDropDownByParameter('#reportWritingReportSchedule',
            '/Assessment/Assessment/FetchReportSchedule',
            { rsm_id: rsmId },
            '',
            window.translatedResources.selectOption, true);
    })
    .on('change', "#reportWritingGrades", function () {

        smsCommon.bindSingleDropDownByParameterAsync('#reportWritingSection',
            '/Assessment/Assessment/GetSectionAccess',
            { ACD_ID: $("#reportWritingAcademicYearDDL").val(), GRD_ID: $(this).val() },
            '',
            window.translatedResources.selectOption)
            .then(() => window.setDropDownValueAndTriggerChange('reportWritingSection'));
        
    })
    .on('change', '#reportWritingSection', function () {
        smsCommon.bindSingleDropDownByParameter('#reportWritingStudent',
            '/Behaviour/Behaviour/GetStudentByGrade',
            { grade: $('#reportWritingGrades').val(), section: $(this).val(), concatStudentNo: true },
            '',
            window.translatedResources.selectOption, true);
        $('.selectpicker').selectpicker('refresh');
        $('#reportWritingDiv').html("");
    })
    .ready(function () {
    });