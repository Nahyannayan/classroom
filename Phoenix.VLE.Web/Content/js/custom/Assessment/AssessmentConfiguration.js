﻿
var AssessmentConfigurationSetting = function () {
    var Init = () => {
        $('.selectpicker').selectpicker('refresh');
        AssessmentConfigurationSetting.loadAssessmentSetting();
    },
        //-------------------------------Load Assessment Report Grid----------------------------
        loadAssessmentSetting = function () {
            var grid = new DynamicPagination("ReportCardGrid");
            var settings = {
                url: '/Assessment/AssessmentConfiguration/ReportCardGrid'
            };
            grid.init(settings);
        },
        //-------------------------------Load Assessment Config form---------------------------
        loadAssessmentConfigForm = (AssessmentMasterId) => {

            $.get('/Assessment/AssessmentConfiguration/ReportCardForm', { AssessmentMasterId: AssessmentMasterId })
                .done((response) => {
                    AssessmentConfigurationSetting.showReportConfig(true);
                    $('#reportConfiguration').html(response);
                    $(".selectpicker").selectpicker('refresh');                   
                    AssessmentConfigurationSetting.GetAssessmentColumnGrid(AssessmentMasterId);
                })
                .fail((error) =>
                    AssessmentConfigurationSetting.onXhrError(error));
        },
        GetAssessmentColumnGrid = function (AssessmentMasterId) {
            $.get('/Assessment/AssessmentConfiguration/GetAssessmentColumnGrid', { AssessmentMasterId })
                .done((response) => {
                    $('#divAssessmentColumnGrid').html(response);
                })
                .fail((error) =>
                    AssessmentConfigurationSetting.onXhrError(error));
        },
        showReportConfig = (isVisible) => {
            if (isVisible) {
                $("#reportConfiguration").removeClass('d-none');
                $("#mainPageAssessmentConfig").addClass("d-none");
            } else {
                $("#reportConfiguration").addClass('d-none');
                $("#reportConfiguration").html("");
                $("#mainPageAssessmentConfig").removeClass("d-none");
            }
        },
        //-------------------------------Ajax error---------------------------------------------
        onXhrError = (xhr) => {
            globalFunctions.onFailure(undefined, xhr)
        },
        //-------------------------------Delete and re-draw table ------------------------------
        DeleteHeaderNames = (index) => {
            smsCommon.notyDeleteConfirm(translatedResources.deleteConfirm, () => {
                if (globalFunctions.isValueValid(index)) {
                    if (AssessmentConfigurationSetting.Headers.some(e => e.Index == index)) {
                        var object = AssessmentConfigurationSetting.Headers.find(e => e.Index == index);
                        object.DataMode = smsCommon.Delete;
                        AssessmentConfigurationSetting.HeaderPostRequestPromis();
                    }
                }
            })
        },
        //-------------------------------Header Form Marks display type setting ----------------
        ShowHideMarksSetting = (isVisible) => {
            if (!(isVisible)) {
                $('#txtMinMarks,#txtMaxMarks').removeAttr('data-integer').removeAttr('data-decimal').removeAttr('data-unsignedint').removeAttr('data-required', "");
                $('#txtMinMarks,#txtMaxMarks').removeData('integer').removeData('decimal').removeData('unsignedint').removeData('required', "");
            }
            $('.minMarks')[isVisible ? 'fadeIn' : 'fadeOut']();
        },
        MinMaxMarksValidation = (min, max) => {
            return parseFloat(min) < parseFloat(max);
        },
        ControlTypeChange = controlTypeValue => {
            debugger
            switch (controlTypeValue) {
                case "0":
                    $("#divMark").removeClass('d-none');
                    break;
                case "1":
                    $("#divGrade").removeClass('d-none');
                    common.bindSingleDropDownByParameter(
                        "#ddlGTM_Grade",
                        '/Assessment/AssessmentConfiguration/GetGradeTemplateMasterList',
                        { CurriculumId: $("#CurriculumId").val() },
                        "",
                        'Select'
                    );
                    break;
                case "2":
                    $("#divComment").removeClass('d-none');
                    break;
                default:
                    $("#divMark").addClass('d-none');
                    $("#divGrade").addClass('d-none');
                    $("#divComment").addClass('d-none');
            }
            $('.selectpicker').selectpicker('refresh');
        },
        GetCourseListByGrade = function (SchoolGradeIds) {
            common.bindSingleDropDownByParameter(
                "#ddlAssessmentConfigCourseId",
                '/Assessment/AssessmentConfiguration/GetCourseListByGrade',
                { SchoolGradeIds: SchoolGradeIds },
                "",
                ""
            );
        },
        //-------------------------------Report Headers Array-----------------------------------
        AssessmentColumnList = [],
        getNewIndexOfList = function () {
            if (AssessmentConfigurationSetting.AssessmentColumnList.length <= 0)
                return 1;
            else
                return AssessmentConfigurationSetting.AssessmentColumnList.length + 1
        },
        SaveAssessmentColumn = function () {
            var IsValid = common.IsInvalidByFormOrDivId("headerForm");
            if (IsValid) {
                var table = $('#tbl-AssessmentColumnGrid').DataTable();

                $("#ddlAssessmentConfigCourseId option:selected").each(function () {
                    var $this = $(this);
                    if ($this.length) {
                        var currentIndex = AssessmentConfigurationSetting.getNewIndexOfList();
                        var CourseText = $this.text();
                        var CourseValue = $this.val();
                        var IsLock = $("#IsLock").is(":checked") ? "Yes" : "No";
                        var FromMark = $("#ddlAssessmentConfigControlType").val() == "0" ? $("#AssessFromMark").val() : "";
                        var ToMark = $("#ddlAssessmentConfigControlType").val() == "0" ? $("#AssessToMark").val() : "";
                        var Action = `<a class="table-action-text-link" onclick="AssessmentConfigurationSetting.EditAssessmentColumn(${currentIndex})"><i class="fa fa-pen"></i></a>`;
                        if ($("#AssessIndexId").val() == "0") {
                            table.row.add([
                                currentIndex,
                                $("#ColumnName").val(),
                                CourseText,
                                $("#ddlAssessmentConfigControlType option:selected").text(),
                                IsLock,
                                FromMark,
                                ToMark,
                                $("#AssessComment").val(),
                                Action
                            ]).draw(false);
                            //---------------------------------------------------------------------
                            AssessmentConfigurationSetting.AssessmentColumnList.push({
                                index: currentIndex,
                                CourseId: CourseValue,
                                ASC_Description: $("#ColumnName").val(),
                                ControlType: $("#ddlAssessmentConfigControlType").val(),
                                GradeTemplateMasterId: $("#ddlGTM_Grade").val(),
                                FromMark: $("#AssessFromMark").val(),
                                ToMark: $("#AssessToMark").val(),
                                ASC_Order: $("#AssessOrder").val(),
                                ASC_ActiveStatus: $("#IsLock").is(":checked"),
                                AssessComment: $("#AssessComment").val()
                            });
                        }
                        else {
                            var CurrIndex = parseInt($("#AssessIndexId").val()) - 1;
                            var temp = table.row(CurrIndex).data();
                            temp[1] = $("#ColumnName").val();
                            temp[2] = CourseText;
                            temp[3] = $("#ddlAssessmentConfigControlType option:selected").text();
                            temp[4] = IsLock;
                            temp[5] = FromMark;
                            temp[6] = ToMark;
                            temp[7] = $("#AssessComment").val();
                            table.row(CurrIndex).data(temp).draw();
                            //---------------------------------------------------------------------------
                            $.each(AssessmentConfigurationSetting.AssessmentColumnList, function (i, val) {
                                if (val.index == $("#AssessIndexId").val()) {
                                    val.CourseId = CourseValue;
                                    val.ASC_Description = $("#ColumnName").val();
                                    val.ControlType = $("#ddlAssessmentConfigControlType").val();
                                    val.GradeTemplateMasterId = $("#ddlGTM_Grade").val();
                                    val.FromMark = $("#AssessFromMark").val();
                                    val.ToMark = $("#AssessToMark").val();
                                    val.ASC_Order = $("#AssessOrder").val();
                                    val.ASC_ActiveStatus = $("#IsLock").is(":checked");
                                    val.AssessComment = $("#AssessComment").val();
                                }
                            });
                        }
                    }
                });

                AssessmentConfigurationSetting.ClearAssessmentColumnFrom();
            }
        },
        EditAssessmentColumn = function (indexId) {
            var data = AssessmentConfigurationSetting.AssessmentColumnList;
            $.each(data, function (i, val) {

                if (val.index == indexId) {
                    $("#ddlAssessmentConfigCourseId").removeAttr('multiple');
                    $("#ColumnName").val(AssessmentConfigurationSetting.AssessmentColumnList[i].ASC_Description);
                    $("#ddlAssessmentConfigCourseId").val(AssessmentConfigurationSetting.AssessmentColumnList[i].CourseId);
                    $("#ddlAssessmentConfigControlType").val(AssessmentConfigurationSetting.AssessmentColumnList[i].ControlType);
                    $(".selectpicker").selectpicker('refresh');
                    $("#ddlAssessmentConfigControlType").trigger("change");
                    $("#AssessFromMark").val(AssessmentConfigurationSetting.AssessmentColumnList[i].FromMark);
                    $("#AssessToMark").val(AssessmentConfigurationSetting.AssessmentColumnList[i].ToMark);
                    $("#ddlGTM_Grade").val(AssessmentConfigurationSetting.AssessmentColumnList[i].GradeTemplateMasterId);
                    $("#AssessComment").val(AssessmentConfigurationSetting.AssessmentColumnList[i].AssessComment);
                    $('#IsLock').prop('checked', AssessmentConfigurationSetting.AssessmentColumnList[i].ASC_ActiveStatus);
                    $("#AssessOrder").val(AssessmentConfigurationSetting.AssessmentColumnList[i].ASC_Order);
                    $("#AssessComment").val(AssessmentConfigurationSetting.AssessmentColumnList[i].AssessComment);
                    $("#AssessIndexId").val(val.index);
                    $(".selectpicker").selectpicker('refresh');
                }
            });
        },
        ClearAssessmentColumnFrom = function () {
            $("#ColumnName").val("");
            $("#ddlAssessmentConfigCourseId").val("");
            $("#ddlAssessmentConfigControlType").val("0");
            $("#ddlAssessmentConfigControlType").trigger("change");
            $("#AssessFromMark").val("");
            $("#AssessToMark").val("");
            $("#ddlGTM_Grade").val("");
            $("#AssessComment").val("");
            $('#IsLock').prop('checked', false);
            $("#AssessOrder").val("");
            $("#AssessIndexId").val("0");
            $(".selectpicker").selectpicker('refresh');
        },
        FinalSaveAssessmentConfig = function () {
            var IsValid = common.IsInvalidByFormOrDivId("mainAssessmentForm");
            if (IsValid) {

                if (AssessmentConfigurationSetting.AssessmentColumnList.length > 0) {
                    var assessmentConfigSetting = {
                        AssessmentMasterId: $("#AssessmentMasterId").val(),
                        ASM_Description: $("#ASM_Description").val(),
                        SchoolGradeIds: $("#ddlAssessmentConfigGradeId").val().join(','),
                        AssessmentColumnList: AssessmentConfigurationSetting.AssessmentColumnList
                    };

                    $.post("/Assessment/AssessmentConfiguration/FinalSaveAssessmentConfig", { assessmentConfigSetting: assessmentConfigSetting }, function (response) {
                        globalFunctions.showMessage(response.NotificationType, response.Message);
                        if (response.Success) {
                            AssessmentConfigurationSetting.showReportConfig(false);
                            AssessmentConfigurationSetting.loadAssessmentSetting();
                        }
                    });
                }
                else
                    globalFunctions.showErrorMessage("Add atleast one column details.");
            }
        }
    return {
        Init,
        loadAssessmentSetting,
        loadAssessmentConfigForm,
        GetAssessmentColumnGrid,
        showReportConfig,
        DeleteHeaderNames,
        ShowHideMarksSetting,
        MinMaxMarksValidation,
        ControlTypeChange,
        onXhrError,
        AssessmentColumnList,
        getNewIndexOfList,
        SaveAssessmentColumn,
        EditAssessmentColumn,
        ClearAssessmentColumnFrom,
        FinalSaveAssessmentConfig,
        GetCourseListByGrade
    }
}();

$(document).ready(() => {
    AssessmentConfigurationSetting.Init();
});
$(document).on('change', '#ddlAssessmentConfigControlType', function () {
    debugger
    $("#divMark").addClass('d-none');
    $("#divGrade").addClass('d-none');
    $("#divComment").addClass('d-none');
    var markType = $(this).val();
    AssessmentConfigurationSetting.ControlTypeChange(markType);
});

$(document).on('hide.bs.select', '#ddlAssessmentConfigGradeId', function () {
    var SchoolGradeIds = $(this).val();
    AssessmentConfigurationSetting.GetCourseListByGrade(SchoolGradeIds.join(','));
});