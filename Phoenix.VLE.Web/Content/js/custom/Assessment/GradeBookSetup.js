﻿
//------------------------Functionality Part-1------------------------
var GlobalTemp_GSD_ID = 1;
var GradeBookGradeScaleDetailList = [];

var objGradeBook = function () {
    var init = function () {

    },
        loadGradeBook = function (RSD_ID) {

            var gradeBookSetup =
            {
                GBM_ACD_ID: $("#ddlGradeBookAcademicYear").val(),
                GBM_TRM_ID: $("#ddlGradeBookTerm").val(),
                GBM_GRD_ID: $("#ddlGradeBookGrade").val(),
                GBM_SBG_ID: $("#ddlGradeBookSubject").val(),
                GBM_SGR_ID: $("#ddlGradeBookSubjectGroup").val(),
                GBM_RSM_ID: $("#ddlGradeBookReportHeader").val(),
                GBM_RPF_ID: $("#ddlGradeBookReportSchedule").val(),
                GBM_RSD_ID: RSD_ID
            };

            $.post("/Assessment/Assessment/LoadGradeBook", { gradeBookSetup }, function (response) {
                $("#divLoadGradeBook").html('');
                $("#divLoadGradeBook").html(response);
                $('.selectpicker').selectpicker('refresh');
                $("#hdnGBM_RSD_ID").val(RSD_ID);
            });
        },
        bindGradeBookSetup = function (GBM_ID) {
            var gradeBookSetup =
            {
                GBM_ACD_ID: $("#ddlGradeBookAcademicYear").val(),
                GBM_TRM_ID: $("#ddlGradeBookTerm").val(),
                GBM_GRD_ID: $("#ddlGradeBookGrade").val(),
                GBM_SBG_ID: $("#ddlGradeBookSubject").val(),
                GBM_SGR_ID: $("#ddlGradeBookSubjectGroup").val(),
                GBM_RSM_ID: $("#ddlGradeBookReportHeader").val(),
                GBM_RPF_ID: $("#ddlGradeBookReportSchedule").val(),
                GBM_ID: GBM_ID,
                GBM_RSD_ID: $("#hdnGBM_RSD_ID").val()
            };
            $.post("/Assessment/Assessment/BindGradeBookSetup", { gradeBookSetup }, function (response) {
                $('#divBtnAddGBSetup').addClass('d-none');
                $("#divBindGradeBookSetup").html('');
                $("#divBindGradeBookSetup").html(response);
                $('.selectpicker').selectpicker('refresh');
            });
        },
        ShowHideGradeScaleBackButton = function (isVisible) {
            if (isVisible) {
                $('#btnBackDrawer').removeClass('d-none');
                $("#divGradeBookForm").addClass('d-none');
                $("#divGradeBookScale").removeClass('d-none');
                //$(".drawer-footer").removeClass('d-none');
            } else {
                $('#btnBackDrawer').addClass('d-none');
                $("#divGradeBookForm").removeClass('d-none');
                $("#divGradeBookScale").addClass('d-none');
                $("#divGradeBookScale").html('');
                //$(".drawer-footer").addClass('d-none');
            }
        },
        loadGradeBookScale = function () {
            $("#divGradeBookScale").html('');
            $.post("/Assessment/Assessment/LoadGradeBookScale", function (response) {
                $("#divGradeBookScale").html(response);
                ShowHideGradeScaleBackButton(true);
            });
        },
        addGradeScaleDetail = function () {
            var Temp_GSD_ID = $("#Temp_GSD_ID").val();
            var GSD_ID = $("#GSD_ID").val();
            var GRADE_DESCRIPTION = $("#Grade_Description").val();
            var RANGEFROM = $("#RangeFrom").val();
            var RANGETO = $("#RangeTo").val();
            var IsEdited = undefined;
            var IsValid = smsCommon.IsInvalidByFormOrDivId("frmGradeScaleDetail");
            if (IsValid) {
                if (parseFloat(RANGEFROM) < parseFloat(RANGETO)) {
                    if (Temp_GSD_ID == '0' && GSD_ID == '0') {
                        GradeBookGradeScaleDetailList.push({
                            Temp_GSD_ID: GlobalTemp_GSD_ID,
                            GSD_ID: 0,
                            GSD_GSM_ID: 0,
                            RANGEFROM: RANGEFROM,
                            RANGETO: RANGETO,
                            GRADE_DESCRIPTION: GRADE_DESCRIPTION,
                            DISPLAY_ORDER: 0,
                            IsDeleted: false
                        })
                        IsEdited = false;
                    }
                    if (Temp_GSD_ID != '0' && GSD_ID == '0') {
                        $.each(GradeBookGradeScaleDetailList, function (index, item) {
                            if (item.Temp_GSD_ID == Temp_GSD_ID) {
                                GradeBookGradeScaleDetailList[index].RANGEFROM = RANGEFROM;
                                GradeBookGradeScaleDetailList[index].RANGETO = RANGETO;
                                GradeBookGradeScaleDetailList[index].GRADE_DESCRIPTION = GRADE_DESCRIPTION;
                            }
                        });
                        IsEdited = true;
                    }
                    if (GSD_ID != '0') {
                        $.each(GradeBookGradeScaleDetailList, function (index, item) {
                            if (item.GSD_ID == GSD_ID) {
                                GradeBookGradeScaleDetailList[index].RANGEFROM = RANGEFROM;
                                GradeBookGradeScaleDetailList[index].RANGETO = RANGETO;
                                GradeBookGradeScaleDetailList[index].GRADE_DESCRIPTION = GRADE_DESCRIPTION;
                            }
                        });
                        IsEdited = true;
                    }
                    if (IsEdited) {
                        var editedRow = $("#tblGradeScaleDetail tbody>tr.EditScaleDetail");
                        $(editedRow).find('td').eq(0).html(GRADE_DESCRIPTION);
                        $(editedRow).find('td').eq(1).html(RANGEFROM);
                        $(editedRow).find('td').eq(2).html(RANGETO);
                        $("#tblGradeScaleDetail tbody>tr.EditScaleDetail").removeClass('EditScaleDetail');
                        objGradeBook.refreshGradeScaleDetail();
                    }
                    else {
                        var updatedRow = "<tr>" +
                            "<td class='pt-2 pb-2 text-center'>" + GRADE_DESCRIPTION + "</td>" +
                            "<td class='pt-2 pb-2 text-center'>" + RANGEFROM + "</td>" +
                            "<td class='pt-2 pb-2 text-center'>" + RANGETO + "</td>" +
                            "<td class='pt-2 pb-2 text-center tbl-actions'>" +
                            "<a href='javascript:void(0)' class='mr-2' onclick='objGradeBook.editGradeScaleDetail(0," + GlobalTemp_GSD_ID + ",this);' data-toggle='tooltip' title='Edit'><i class='fas fa-pencil-alt'></i></a>" +
                            "<a href='#' data-toggle='tooltip' title='Delete' class='mr-3' onclick='objGradeBook.deleteGradeScaleDetail(0," + GlobalTemp_GSD_ID + ",this)'><i class='fa fa-trash-alt'></i></a>" +
                            "</td>" +
                            "</tr>";
                        if ($("#tblGradeScaleDetail tbody").find('.NoRecord').length > 0) {
                            $("#tblGradeScaleDetail tbody").find('.NoRecord').remove();
                        }
                        $("#tblGradeScaleDetail tbody").append(updatedRow);
                        objGradeBook.refreshGradeScaleDetail();
                        GlobalTemp_GSD_ID = GlobalTemp_GSD_ID + 1;
                    }
                }
                else {
                    globalFunctions.showMessage("error", "RangeFrom should not be less than RangeTo.");
                }
            }
        },
        editGradeScaleDetail = function (GSD_ID, Temp_GSD_ID, element) {
            $("#btnAddScaleDetail").html("UPDATE");
            if (GSD_ID != '0' && GSD_ID != 0) {
                $.each(GradeBookGradeScaleDetailList, function (index, item) {
                    if (item.GSD_ID == GSD_ID) {
                        $("#Temp_GSD_ID").val('0');
                        $("#GSD_ID").val(GSD_ID);
                        $("#Grade_Description").val(GradeBookGradeScaleDetailList[index].GRADE_DESCRIPTION);
                        $("#RangeFrom").val(GradeBookGradeScaleDetailList[index].RANGEFROM);
                        $("#RangeTo").val(GradeBookGradeScaleDetailList[index].RANGETO);
                    }
                });
            }
            else if (Temp_GSD_ID != '0' && Temp_GSD_ID != 0) {
                $.each(GradeBookGradeScaleDetailList, function (index, item) {
                    if (item.Temp_GSD_ID == Temp_GSD_ID) {
                        $("#Temp_GSD_ID").val(Temp_GSD_ID);
                        $("#GSD_ID").val('0');
                        $("#Grade_Description").val(GradeBookGradeScaleDetailList[index].GRADE_DESCRIPTION);
                        $("#RangeFrom").val(GradeBookGradeScaleDetailList[index].RANGEFROM);
                        $("#RangeTo").val(GradeBookGradeScaleDetailList[index].RANGETO);
                    }
                });
            }
            $(element).closest('tr').addClass('EditScaleDetail');
        },
        deleteGradeScaleDetail = function (GSD_ID, Temp_GSD_ID, element) {
            if (GSD_ID != '0' && GSD_ID != 0) {
                $.each(GradeBookGradeScaleDetailList, function (index, item) {
                    if (item.GSD_ID == GSD_ID) {
                        GradeBookGradeScaleDetailList[index].IsDeleted = true;
                        $(element).closest("tr").remove();
                    }
                });
            }
            else {
                $.each(GradeBookGradeScaleDetailList, function (index, item) {
                    if (item.Temp_GSD_ID == Temp_GSD_ID) {
                        GradeBookGradeScaleDetailList.splice(index, 1);
                        $(element).closest("tr").remove();
                    }
                });
            }
        },
        saveGradeBookSetup = function () {
            if (!$("#divGradeBookScale").hasClass('d-none')) {
                var IsValid = smsCommon.IsInvalidByFormOrDivId("frmGradeScale");
                if (IsValid) {
                    var gradeBookGradeScale = {
                        GSM_ID: $("#GSM_ID").val(),
                        GSM_DESCRIPTION: $("#GSM_Description").val(),
                        GradeBookGradeScaleDetailList: GradeBookGradeScaleDetailList
                    };
                    if (GradeBookGradeScaleDetailList.length > 0) {
                        $.post("/Assessment/Assessment/SaveGradeScaleAndDetail", { gradeBookGradeScale }, function (response) {
                            globalFunctions.showMessage(response.NotificationType, response.Message);
                            if (response.Success) {
                                var result = smsCommon.bindSingleDropDownByParameter(
                                    "#ddlGBM_GSM_ID",
                                    '/Assessment/Assessment/BindGradeScaleDropdownList',
                                    "",
                                    "",
                                    'Select'
                                );
                                objGradeBook.ShowHideGradeScaleBackButton(false);
                            }
                        });
                    }
                    else {
                        globalFunctions.showMessage("error", "No records of grade scale detail to save.");
                    }
                }
            }
            else if (!$("#divGradeBookForm").hasClass('d-none')) {
                var IsValid = smsCommon.IsInvalidByFormOrDivId("frmGradeBookSetup");
                var MIN_MARK = $("#GBM_MIN_MARK").val() == '' ? '0' : $("#GBM_MIN_MARK").val();
                var MAX_MARK = $("#GBM_MAX_MARK").val() == '' ? '0' : $("#GBM_MAX_MARK").val();

                if (IsValid) {
                    $("#frmGradeBookSetup").submit();
                }
            }
        },
        saveGradeBookSetupOnSuccess = function (response) {
            if (response.Success) {
                $(".closeRightDrawer").trigger('click');
                $("#divLoadGradeBook").html('');
                GradeBook.ViewStudents();
            }
            globalFunctions.showMessage(response.NotificationType, response.Message);
        },
        deleteGradeBookSetup = function (GBM_ID, element) {
            smsCommon.notyDeleteConfirm(translatedResources.deleteConfirm, () => {
                $.post("/Assessment/Assessment/DeleteGradeBookSetup", { GBM_ID }, function (response) {
                    if (response.Success) {
                        $(element).closest("tr").remove();
                    }
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                });
            });
        },
        addGradeScale = function () {
            objGradeBook.hideShowGradeScaleDetail(true);
            objGradeBook.refreshGradeScaleDetail();
            $("#GSM_ID").val('0');
            $("#GSM_Description").val('');
            GradeBookGradeScaleDetailList = [];
        },
        editGradeScale = function (GSM_ID, element) {
            var GSM_Description = $(element).closest("tr").find('td').eq(0).html();
            $("#GSM_ID").val(GSM_ID);
            $("#GSM_Description").val(GSM_Description);
            objGradeBook.hideShowGradeScaleDetail(true);
            $.get("/Assessment/Assessment/GetGradeScaleDetailList", { GSM_ID }, function (response) {
                if (response.length > 0) {
                    GradeBookGradeScaleDetailList = response;
                    $("#tblGradeScaleDetail tbody").html('');
                    $.each(response, function (index, item) {
                        var updatedRow = "<tr>" +
                            "<td class='pt-2 pb-2 text-center'>" + item.GRADE_DESCRIPTION + "</td>" +
                            "<td class='pt-2 pb-2 text-center'>" + item.RANGEFROM + "</td>" +
                            "<td class='pt-2 pb-2 text-center'>" + item.RANGETO + "</td>" +
                            "<td class='pt-2 pb-2 text-center tbl-actions'>" +
                            "<a href='javascript:void(0)' class='mr-2' onclick='objGradeBook.editGradeScaleDetail(" + item.GSD_ID + "," + item.Temp_GSD_ID + ",this);' data-toggle='tooltip' title='Edit'><i class='fas fa-pencil-alt'></i></a>" +
                            "<a href='#' data-toggle='tooltip' title='Delete' class='mr-3' onclick='objGradeBook.deleteGradeScaleDetail(" + item.GSD_ID + "," + item.Temp_GSD_ID + ",this)'><i class='fa fa-trash-alt'></i></a>" +
                            "</td>" +
                            "</tr>";
                        if ($("#tblGradeScaleDetail tbody").find('.NoRecord').length > 0) {
                            $("#tblGradeScaleDetail tbody").find('.NoRecord').remove();
                        }
                        $("#tblGradeScaleDetail tbody").append(updatedRow);
                    });
                }
            });
        },
        deleteGradeScale = function (GSM_ID, element) {
            smsCommon.notyDeleteConfirm(translatedResources.deleteConfirm, () => {
                $.post("/Assessment/Assessment/DeleteGradeScaleAndDetail", { GSM_ID }, function (response) {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    if (response.Success) {
                        $(element).closest("tr").remove();
                    }
                });
            });
        },
        hideShowGradeScaleDetail = function (isShow) {
            if (isShow) {
                $(".divScaleGrid").addClass('d-none');
                $("#divScaleDetail").removeClass('d-none');
            }
            else {
                $(".divScaleGrid").removeClass('d-none');
                $("#divScaleDetail").addClass('d-none');
            }
        },
        refreshGradeScaleDetail = function () {
            $("#Temp_GSD_ID").val('0');
            $("#GSD_ID").val('0');
            $("#Grade_Description").val('');
            $("#RangeFrom").val('');
            $("#RangeTo").val('');
            $("#btnAddScaleDetail").html("ADD");
        },
        bindProcessingRuleSetup = function (RSD_ID) {
            var gradeBookSetup =
            {
                GBM_ACD_ID: $("#ddlGradeBookAcademicYear").val(),
                GBM_TRM_ID: $("#ddlGradeBookTerm").val(),
                GBM_GRD_ID: $("#ddlGradeBookGrade").val(),
                GBM_SBG_ID: $("#ddlGradeBookSubject").val(),
                GBM_SGR_ID: $("#ddlGradeBookSubjectGroup").val(),
                GBM_RSM_ID: $("#ddlGradeBookReportHeader").val(),
                GBM_RPF_ID: $("#ddlGradeBookReportSchedule").val(),
                GBM_RSD_ID: RSD_ID
            };
            $.post("/Assessment/Assessment/BindProcessingRuleSetup", { gradeBookSetup }, function (response) {
                $("#divLoadGradeBook").html('');
                $("#divLoadGradeBook").html(response);
                $('.selectpicker').selectpicker('refresh');
            });
        },
        saveProcessingRuleSetup = function () {
            var IsValid = smsCommon.validateByFormId("frmProcessingRuleSetup");
            if (IsValid) {
                return false;
            }
            else {
                var PRS_GBM_IDS = $("#ddlPRS_GBM_IDS").val();
                PRS_GBM_IDS = PRS_GBM_IDS.join("|");
                processingRuleSetup = {
                    PRS_ID: $("#PRS_ID").val(),
                    PRS_RSD_ID: $("#PRS_RSD_ID").val(),
                    PRS_GBM_IDS: PRS_GBM_IDS,
                    PRS_CalculationType: $("#ddlPRS_CalculationType").val()
                }

                $.post("/Assessment/Assessment/SaveProcessingRuleSetup", { processingRuleSetup }, function (response) {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    if (response.Success) {
                        $(".closeRightDrawer").trigger('click');
                        $("#divLoadGradeBook").html('');
                    }
                });
            }
        }
    return {
        init: init,
        loadGradeBook: loadGradeBook,
        bindGradeBookSetup: bindGradeBookSetup,
        ShowHideGradeScaleBackButton: ShowHideGradeScaleBackButton,
        loadGradeBookScale: loadGradeBookScale,
        addGradeScaleDetail: addGradeScaleDetail,
        refreshGradeScaleDetail: refreshGradeScaleDetail,
        editGradeScaleDetail: editGradeScaleDetail,
        deleteGradeScaleDetail: deleteGradeScaleDetail,
        saveGradeBookSetup: saveGradeBookSetup,
        editGradeScale: editGradeScale,
        hideShowGradeScaleDetail: hideShowGradeScaleDetail,
        addGradeScale: addGradeScale,
        deleteGradeScale: deleteGradeScale,
        saveGradeBookSetupOnSuccess: saveGradeBookSetupOnSuccess,
        deleteGradeBookSetup: deleteGradeBookSetup,
        bindProcessingRuleSetup: bindProcessingRuleSetup,
        saveProcessingRuleSetup: saveProcessingRuleSetup
    }
}();

//---------------Click/Change Event Functionality Part-1---------------

$(document).on('click', "#GBM_bON_PORTAL", function (event) {
    if ($(this).is(":checked")) {
        $("#divIsDisplayParentStudent").removeClass('d-none');
    }
    else {
        $("#divIsDisplayParentStudent").addClass('d-none');
    }
});

$(document).on('change', "#ddlGBM_EntryMode", function (event) {
    if ($(this).val() == 'M') {
        $("#divShowByMark").removeClass('d-none');
        $("#divShowByGrade").addClass('d-none');
        $("#ddlGBM_MarkType").prop("selectedIndex", 0);
        $('.selectpicker').selectpicker('refresh');
        $("#GBM_MIN_MARK").val(0);
        $("#GBM_MAX_MARK").val(0);
    }
    else if ($(this).val() == 'D') {
        $("#divShowByMark").addClass('d-none');
        $("#divShowByGrade").removeClass('d-none');
        var result = smsCommon.bindSingleDropDownByParameter(
            "#ddlGBM_GSM_ID",
            '/Assessment/Assessment/BindGradeScaleDropdownList',
            "",
            "",
            'Select',
            true
        );
    }
    else {
        $("#divShowByMark").addClass('d-none');
        $("#divShowByGrade").addClass('d-none');
    }
});
//---------------End Click/Change Event Functionality Part-1------------
