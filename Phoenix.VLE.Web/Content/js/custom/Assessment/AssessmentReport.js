﻿
function RequestToPdfView(ACD_ID, rpF_ID, StudentNumber) {
    $.ajax({
        url: '/ParentCorner/AssessmentReport/RequestToPdfView',
        type: "GET",
        data: { ACD_ID: ACD_ID, rpF_ID: rpF_ID, StudentNumber: StudentNumber },
        contentType: "application/json; charset=UTF-8",
        success: function (response) {
            if (response != null) {     // this sets the value from the response
                //window.open("data:application/pdf," + escape(response.url));
                var url = response.androidURL;
                window.open(url, '_blank');
            } else {
                alert("something is wrong");

            }
        }
    });
}
function GetStudentinfo(Studentnumber) {
    $('#getstudid').val(Studentnumber);
    $.ajax({
        url: '/ParentCorner/AssessmentReport/GetStudentinfo',
        type: "GET",
        data: { Studentnumber: Studentnumber },
        contentType: "html",
        success: function (response) {
            if (response != null) { // this sets the value from the response
                $('#divpdfInfoModellist').html('');
                $('#divpdfInfoModellist').html(response);
                $("#ReportYearType").selectpicker('refresh');
                globalFunctions.convertImgToSvg();
            } else {
                alert("something is wrong");

            }
        }
    });
}
function StudentYearDropdownlist() {
    var SelectedYearId = $('#ReportYearType :Selected').val();
    var StudenntNumber = $('#getstudid').val();
    $.ajax({
        url: '/ParentCorner/AssessmentReport/GetStudentPdfList',
        type: "GET",
        data: { StudenntNumber: StudenntNumber, SelectedYearId: SelectedYearId },
        success: function (response) {
            if (response != null) { // this sets the value from the response

                //$('#PdfFileCount1').html('');

                $('#divPdfListInfo').html('');
                $('#divPdfListInfo').html(response);
                $('#PdfFileCount1').html('')
                var str = $('#PdfFileCount1').val();

                if (str === undefined) {
                    $('#PdfNoOfList').html('<span> ' + translatedResources.ReportsIn+'</span> ');
                }
                else {
                    $('#PdfNoOfList').html('<span> ' + translatedResources.ShowingReportsCount.replace('{0}', str) + '</span> ');
                }
                globalFunctions.convertImgToSvg();
            }
        }
    });
}
