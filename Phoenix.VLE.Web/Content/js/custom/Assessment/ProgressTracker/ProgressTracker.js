﻿let comboTree1 = undefined;
let ParentNameForSBID;
let PreviousName;
let ParentNameForNonSBID;
let lCountForStandardBankId = 0;
let lCount = 0;
var _dataTableLangSettings = {
    "sEmptyTable": translatedResources.noData,
    "sInfo": translatedResources.sInfo,
    "sInfoEmpty": translatedResources.sInfoEmpty,
    "sInfoFiltered": translatedResources.sInfoFiltered,
    "sInfoPostFix": "",
    "sInfoThousands": ",",
    "sLengthMenu": translatedResources.sLengthMenu,
    "sLoadingRecords": "Loading...",
    "sProcessing": "Processing...",
    "sSearch": translatedResources.search,
    "sZeroRecords": translatedResources.sZeroRecords,
    "oPaginate": {
        "sFirst": translatedResources.first,
        "sLast": translatedResources.last,
        "sNext": translatedResources.next,
        "sPrevious": translatedResources.previous
    },
    "oAria": {
        "sSortAscending": translatedResources.sSortAscending,
        "sSortDescending": translatedResources.sSortDescending
    }
};
let ProgressTracker = function () {
    let treeResponse;
    let ProgressTrackerModel;
    let StudentProgressMapper = [];
    let AssignmentLessonGrading = [];
    init = function () {
        $('.time-picker').datetimepicker({
            format: 'LT'
        })
        $('.date-picker').datetimepicker({
            format: 'MM/DD/YYYY'
        })
        var dt = new Date();
        var cDate = ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear();
        var cTime = dt.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
        $('.date-picker').val(cDate)
        $('.time-picker').val(cTime)

        $(".colorpicker").minicolors({
            theme: 'bootstrap',
            control: 'hue',
            format: 'hex'
        })

        $(".sidebar-fixed .list-group").mCustomScrollbar({
            setHeight: "86%",
            autoExpandScrollbar: true,
            autoHideScrollbar: true,
            scrollbarPosition: "outside"
        })

        ProgressTracker.getCourseList()

        $(document).on('change', '#CourseId', function () {
            let courseId = $(this).val();
            ProgressTracker.getCourseGroupByCourseId(courseId);
            ProgressTracker.getCourseTopicsByCourseId(courseId);

        })

        $(document).on('click', '.btn-filter', function () {
            $(".field-form").slideToggle();
            $(this).toggleClass('closed');
            $(this).hasClass("closed") ? ($(this).html(translatedResources.showFilter+' <i class="fas fa-chevron-down ml-2"></i>')) : ($(this).html(translatedResources.hideFilter+' <i class="fas fa-chevron-up ml-2"></i>'))
        })

        $(document).on('click', '#btnViewProgressTracker', function () {
            let unitId;
            let courseId = $('#CourseId').val()
            let courseGroupId = $('#CourseGroupId').val()
            let txtvalue = $('#txtTopics').val()
            let unitIds = [];
            let ParentId;
            if (comboTree1 != undefined && txtvalue != "") {
                unitId = comboTree1.getSelectedItemsId() == false || comboTree1.getSelectedItemsId() == undefined ? "" : comboTree1.getSelectedItemsId()
            }
            let index = ProgressTracker.treeResponse.findIndex(x => x.SubSyllabusId == unitId)
            unitIds.push(unitId)
            if (index > -1) {
                ParentId = ProgressTracker.treeResponse[index].ParentId;
                if (ParentId > 0) {
                    unitIds.push(ParentId)
                }
                else {
                    ProgressTracker.treeResponse.filter(t => t.ParentId == unitId).forEach((c) => {

                        unitIds.push(c.SubSyllabusId)
                    })

                }
            }


            ProgressTracker.getProgressTracker(courseId, courseGroupId, unitIds.join(','))
        })
        $('.btn-filter').trigger('click');

        $(document).on('change', '.clsChkStudent', function () {

            $(".clsChkStudent:checked").length > 1 ? $('#spnSelectionCount').html($(".clsChkStudent:checked").length + " Selected") : $('#spnSelectionCount').html("");
            $(".clsChkStudent:checked").length > 1 ? $('#divSelectionDetails').removeClass('d-none') : $('#divSelectionDetails').addClass('d-none')
            $(".clsChkStudent:checked").length > 0 ? $('#ulbtnGradingButton').removeClass("disabled") : $('#ulbtnGradingButton').addClass("disabled")



        });
        $(document).on('click', '#btnclrSelection', function () {
            $(".clsChkStudent").prop("checked", false).trigger('change');

        })

        $(document).on('click', '.clstdstatus', function () {
            $(".clstdstatus").removeClass("active");
            $("#chkBulkStudentSelection").prop("checked", false).trigger("change");
            $("#chkBulkLessonSelection").prop("checked", false).trigger("change");
            $(".clsChkStudent").prop("checked", false);
            $(".clschkLesson").prop("checked", false);
            let studentId = $(this).attr("data-studentId")
            let lessonId = $(this).attr("data-lessonId")
            $(`#chkLesson_${lessonId}`).prop("checked", true);
            $(`#chkStudent_${studentId}`).prop("checked", true).trigger("change");
            $(this).hasClass('no-grade') ? "" : $(this).addClass('active');

        });

        $(document).on('click', '#btnSaveProgressTracker', function () {
            $.post("/Assessment/ProgressTracker/StudentProgressMapper",
                {
                    groupId: $('#CourseGroupId').val(),
                    assessmentStudents: ProgressTracker.StudentProgressMapper
                }).then((response) => {
                    globalFunctions.showMessage(response.NotificationType, response.Message);
                    if (response.Success) {
                        $('#btnViewProgressTracker').trigger('click');

                    }

                }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });;



        });

        $(document).on('click', '.clsopenMPAttachment', function () {
            let studentId = $(this).attr("data-studentId");
            let lessonId = $(this).attr("data-lessonId");
            $('#divAttachmentMP').empty();
            $.get('/Assessment/ProgressTracker/GetStudentProgressAttachments', { lessonId: lessonId, studentId: studentId }).then((response) => {
                $('#divAttachmentMP').html(response);
                let studentName = ProgressTracker.ProgressTrackerModel.StudentList.filter(x => x.StudentId == studentId)[0].StudentName;
                let lessonName = ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.LessonId == lessonId)[0].StandardCode == "" ? ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.LessonId == lessonId)[0].StandardDescription : ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.LessonId == lessonId)[0].StandardCode;

                $('#mpTitle').html(`${studentName} Attachment for ${lessonName}`);

            })

        });

        $(document).on("click", ".clsbtnProgressAttachmentSave", function (e) {
            var form = $('#frmProgressTrackerEvidence')[0];
            var formData = new FormData(form);
            let studentId = $('#StudentId').val();
            let lessonId = $('#LessonId').val();
            $.post({
                url: "/Assessment/ProgressTracker/SaveStudentProgressAttachment",
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    globalFunctions.showMessage(response.IsSuccess.NotificationType, response.IsSuccess.Message);
                    $('#mpStudentFileAttachments').modal('toggle')

                    $(`#btnFileUpload_${lessonId}_${studentId}`).attr('data-count', response.evidenceCount);
                    $(`#smlViewEvidence_${lessonId}_${studentId}`).html(`View Evidence(${response.evidenceCount})`)
                    //location.href = "/Course/Course/AddEditCourse?id=" + globalFunctions.getParameterValueFromQueryString('courseId') + "&tab=unitcalendar";
                }
            })
            e.preventDefault();
            e.stopImmediatePropagation();

        })



        $(document).on('click', '.clsopenAssignemntView', function () {
            let studentId = $(this).attr("data-studentId");
            let lessonId = $(this).attr("data-lessonId");
            $('#divAssignmentGradeMP').empty();
            $.get('/Assessment/ProgressTracker/GetAssignmentsForStudentLesson', { lessonId: lessonId, studentId: studentId }).then((response) => {

                $('#divAssignmentGradeMP').html(response);
                let studentName = ProgressTracker.ProgressTrackerModel.StudentList.filter(x => x.StudentId == studentId)[0].StudentName;
                let lessonName = ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.LessonId == lessonId)[0].StandardDescription; //ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.LessonId == lessonId)[0].StandardCode == "" ? ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.LessonId == lessonId)[0].StandardDescription : ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.LessonId == lessonId)[0].StandardCode;

                $('#mpAssignmentTitle').html(`${studentName}`);
                $("#h3LessonNameForAssignment").html(`${lessonName}`);
                if ($.fn.DataTable.isDataTable($('#tblAssignmentLessonGrade'))) {
                    $('#tblAssignmentLessonGrade').DataTable().destroy();
                }
                $('#tblAssignmentLessonGrade').dataTable({
                    "bPaginate": false,
                    "bSort": false,
                    "bFilter": true,
                    "bInfo": false,
                    "oLanguage": _dataTableLangSettings
                });

                ProgressTracker.AssignmentChart();

            })

        });


        $(document).on('click', '.clsStudentLessonMP', function () {
            $('.clschkLesson').prop("checked", false)
            $('.clsChkStudent').prop("checked", false)
            $('#chkBulkStudentSelection').prop("checked", false).trigger("change")
            $('#chkBulkLessonSelection').prop("checked", false).trigger("change")
            let studentId = $(this).attr("data-studentId")
            let lessonId = $(this).attr("data-lessonId")

            let s = ProgressTracker.ProgressTrackerModel.StudentList.filter(x => x.StudentId == studentId)[0]
            $("#imgStudentImgSrc").attr("src", s.StudentImageUrl)
            $("#hStudentName").html(s.StudentName)
            $("#pdispayGrade").html(`Grade ${s.GradeDisplay}`)
            $("#accordionEx1").empty();
            $("#accordionEx1").html(ProgressTracker.generateAccordion(s))
            //$('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();





        })

        $(document).on("click", ".clsGradingItemTemplate", function () {
            let mainTopicAppended = $(this).attr("data-lessonontopic");
            let subTopicAppended = $(this).attr("data-Subtopiconlesson");
            let gradingId = $(this).attr("data-gradingid");
            let studentId = $(this).attr("data-studentId");

            let symbol;
            let color;

            let gradingidx = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList.findIndex(x => x.GradingTemplateItemId == gradingId);
            if (gradingidx > -1) {
                color = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[gradingidx].GradingColor;
                symbol = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[gradingidx].ShortLabel;

            }
            if ($(`.clschkLessonFor_${mainTopicAppended}_${subTopicAppended}:checked`).length > 0) {
                $(`#chkStudent_${studentId}`).prop("checked", true);
                $(`.clschkLessonFor_${mainTopicAppended}_${subTopicAppended}:checked`).each(function () {
                    let lessonId = $(this).attr("data-lessonId")
                    $(`#chkLesson_${lessonId}`).prop("checked", true);
                    $(`#spnLesson_${lessonId}`).html(symbol).css("background-color", color)

                });
                $('#ulbtnGradingButton').removeClass("disabled")
                $(`#gradingMark_${gradingId}`).trigger("click");

            }
            else {
                globalFunctions.showWarningMessage(translatedResources.SelectLessonMsg)
            }


        });

        $(document).on("click", "#btnMPSaveStudentLessonProgress", function () {
            if (!$("#ulbtnGradingButton").hasClass("disabled")) {
                $("#btnSaveProgressTracker").trigger("click");
                $("#studentDetailPopup").modal("toggle")
            }
            else {
                globalFunctions.showWarningMessage("No records to update")
            }
          
        })

        $(document).on("change", "#chkBulkStudentSelection", function () {
            if ($(this).is(":checked")) {
                $('.clsChkStudent').prop("checked", true).trigger("change")
            }
            else {
                $('.clsChkStudent').prop("checked", false).trigger("change")
            }

        })

        $(document).on("change", "#chkBulkLessonSelection", function () {
            if ($(this).is(":checked")) {
                $('.clschkLesson').prop("checked", true)
            }
            else {
                $('.clschkLesson').prop("checked", false)
            }

        })

        $(document).on("keypress", "#searchAccordion", function (e) {
            if (e.which == 13) {
                $('.card .obective-card').parent().show();
                var filter = $(this).val(); // get the value of the input, which we filter on
                $('.card .obective-card').find(".clsLessonParagraph:not(:contains(" + filter + "))").parent().parent().parent().css('display', 'none');


            }
        });



    },
        getCourseList = function () {
            $.get('/Assessment/ProgressTracker/GetCourseList').then((response) => {
                response.forEach((x) => {
                    $('#CourseId').append(`<option value="${x.Value}">${x.Text}</option>`).selectpicker('refresh');
                });

            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });

        },
        getCourseGroupByCourseId = function (id) {
            $('#CourseGroupId').empty()
            $.get('/Assessment/ProgressTracker/GetCourseGroupByCourse', { courseId: id }).then((response) => {

                $('#CourseGroupId').append(`<option value="">` + translatedResources.selectedText + `</option>`)
                response.forEach((x) => {
                    $('#CourseGroupId').append(`<option value="${x.Value}">${x.Text}</option>`)
                });

                $('#CourseGroupId').selectpicker('refresh');

            }).catch((error) => {
                globalFunctions.showMessage(error.NotificationType, error.Message)
            });
        },
        getCourseTopicsByCourseId = function (id) {

            $.get('/Assessment/ProgressTracker/GetTopicsByCourseId', { courseId: id }).then((response) => {
                ProgressTracker.treeResponse = response;
                let topicTree = [];
                if (response.length > 0) {
                    $('#divCourse').removeClass('col-lg-6 ').addClass('col-lg-3')
                    $('#divCourseGroup').removeClass('col-lg-6 ').addClass('col-lg-3')
                    response.filter(p => p.ParentId == 0).forEach((prnt) => {

                        let objTree = { id: prnt.SubSyllabusId, title: prnt.SubSyllabusDescription, subs: [] }
                        response.filter(x => x.ParentId == prnt.SubSyllabusId).forEach((child) => {


                            let subs = { id: child.SubSyllabusId, title: child.SubSyllabusDescription }
                            if (objTree.id == prnt.SubSyllabusId) {
                                objTree.subs.push(subs)
                            }




                        })
                        topicTree.push(objTree)
                    })
                    console.log(topicTree)


                    if (comboTree1 != undefined) {
                        if (comboTree1.length != 0) {
                            comboTree1.destroy();
                        }
                    }


                    comboTree1 = $('#txtTopics').comboTree({
                        source: topicTree,
                        //isMultiple: true,
                        //cascadeSelect: true
                    });
                    $(".comboTreeArrowBtn").attr("type", "button")
                    //comboTree1 = $('#txtTopics').comboTree('loadData', options);
                    $('.clsdivTopics').removeClass('d-none');
                }
                else {
                    $('.clsdivTopics').addClass('d-none');
                }

            }).catch((error) => {

                globalFunctions.showMessage(error.NotificationType, error.Message)
            });

        },

        getProgressTracker = function (courseId, courseGroupId, unitId) {

            $.get('/Assessment/ProgressTracker/DisplayTracker', { courseId: courseId, courseGroupId: courseGroupId, unitIds: unitId }).then((response) => {
                $('.btn-filter').trigger('click')
                $('#divDisplayTracker').empty()
                $('#divDisplayTracker').html(response)
                let courseName = $('#CourseId').find('option:selected').text();
                let GroupName = $('#CourseGroupId').find('option:selected').text();
                let unitName = comboTree1.getSelectedItemsTitle()
                $('#tdCourseText').html(courseName)
                $('#tdGrpText').html(GroupName)
                $('#tdUnitText').html(unitName)
                ProgressTracker.StudentProgressMapper.length = 0;
                ParentNameForSBID = undefined;
                PreviousName = undefined;
                ParentNameForNonSBID = undefined;
                lCount = 0;
                lCountForStandardBankId = 0;
                $('#tbodyStudentProgressTrack').html(ProgressTracker.LessonProgressMapping())

                // $('#breadcrumbActions').html(`<button id="btnSaveProgressTracker" type="button" class="btn btn-sm btn-primary m-0 mr-3">Save</button>`)
                //ProgressTracker.StudentProgressMapper = ProgressTracker.ProgressTrackerModel.StudentList.map((p) => {
                //    return { Id: p.Id, LessonId: p.LessonId, StudentId: p.StudentId, GradingTemplateId: p.GradingTemplateId}
                // })
                //$('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="popover"]').popover({
                    placement: 'top',
                    trigger: 'hover'
                });
                // $(".table-container").mCustomScrollbar("destroy").mCustomScrollbar({ axis: "xy", autoHideScrollbar:true })
            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) })

        },
        LessonProgressMapping = function () {


            let LessonProgress;

            let ParentId = ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.StandardBankId == 0).length > 0 ? ProgressTracker.ProgressTrackerModel.LessonList[0].ParentId : 0;
            if (ParentId > 0) {
                let Idx = ProgressTracker.treeResponse.findIndex(x => x.SubSyllabusId == ParentId);
                ParentName = ProgressTracker.treeResponse[Idx].SubSyllabusDescription;
            }

            ProgressTracker.ProgressTrackerModel.LessonList.forEach((l) => {

                LessonProgress += `<tr>${ProgressTracker.LessonByPassForSBID(l)} ${ProgressTracker.StudentProgressMapping(l)}</tr>`


            });
            return LessonProgress;



        },

        LessonByPassForSBID = function (l) {

            if (ParentNameForSBID != l.MainTopic) {
                lCountForStandardBankId = lCountForStandardBankId + 1;
                ParentNameForSBID = l.MainTopic
                return ProgressTracker.LessonHeaderGroupForStandardBankId(ParentNameForSBID, lCountForStandardBankId)
            }

        },

        StudentProgressMapping = function (lesson) {
            let appendStudentGradingTemp;

            ProgressTracker.ProgressTrackerModel.StudentList.filter((i, idx) => {
                let Midx = ProgressTracker.ProgressTrackerModel.StudentList.findIndex(x => x.StudentId == i.StudentId)
                return Midx == idx;
            }).forEach((s) => {
                let stuLessonId = ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == lesson.LessonId).length > 0 ? ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == lesson.LessonId)[0].LessonId : 0;
                let stuGradingTempId = ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == lesson.LessonId).length > 0 ? ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == lesson.LessonId)[0].GradingTemplateId : 0;
                let AssignmentGradingTempId = ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == lesson.LessonId).length > 0 ? ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == lesson.LessonId)[0].AssignmentGradingTemplateItemId : 0;
                let AttachmentCount = ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == lesson.LessonId).length > 0 ? ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == lesson.LessonId)[0].AttachmentCount : 0;
                if (stuLessonId == lesson.LessonId) {

                    let GradingColor;
                    let GradingTemplateItemSymbol;
                    let GradingDescription;

                    let AssignmentGradingColor;
                    let AssignmentGradingTemplateItemSymbol;
                    let AssignmentGradingDescription;

                    let grdidx = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList.findIndex(x => x.GradingTemplateItemId == stuGradingTempId);
                    if (grdidx > -1) {
                        GradingColor = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[grdidx].GradingColor;
                        GradingTemplateItemSymbol = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[grdidx].ShortLabel;
                        GradingDescription = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[grdidx].GradingItemDescription;
                    }
                    let Agrdidx = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList.findIndex(x => x.GradingTemplateItemId == AssignmentGradingTempId);
                    if (Agrdidx > -1) {
                        AssignmentGradingColor = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[Agrdidx].GradingColor;
                        AssignmentGradingTemplateItemSymbol = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[Agrdidx].ShortLabel;
                        AssignmentGradingDescription = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[Agrdidx].GradingItemDescription;
                    }
                    let spnAssignmentonGrade = Agrdidx > -1 ? `<span id="spnAssignmentGrading_${lesson.LessonId}_${s.StudentId}" class="d-block font-small text-medium" data-toggle="tooltip" title="${AssignmentGradingDescription}">(${AssignmentGradingTemplateItemSymbol})</span>` : ``;
                    if (stuGradingTempId > 0 && grdidx > -1) {
                        appendStudentGradingTemp += `<td id="tdStudentStatus_${lesson.LessonId}_${s.StudentId}" data-lessonId="${lesson.LessonId}" data-studentId="${s.StudentId}" class="min-width py-3 clstdstatus border-bottom border-right" style="--grade-color: ${GradingColor}">
                                    <div class="remark text-left position-relative">
                                        <p class="text-bold grade" id="chkStudentStatus_${lesson.LessonId}_${s.StudentId}">${GradingTemplateItemSymbol} ${spnAssignmentonGrade}<span class="d-block font-small text-medium">${GradingDescription}</span>
                                        </p>
                                        <a href="javascript:void(0);" id="viewAssignment_${lesson.LessonId}_${s.StudentId}"  class="rounded-4x small text-semibold d-block text-center py-1 clsopenAssignemntView" data-studentId="${s.StudentId}" data-lessonId="${lesson.LessonId}" data-toggle="modal" data-target="#mpStudentGradeAssignment" style="color:${GradingColor}">` + translatedResources.ViewAssignments+`</a>
                                        <a href="javascript:void(0);" id="btnFileUpload_${lesson.LessonId}_${s.StudentId}" class="attachment position-absolute top-0 clsopenMPAttachment" data-studentId="${s.StudentId}" data-lessonId="${lesson.LessonId}" data-count="${AttachmentCount}" data-toggle="modal" data-target="#mpStudentFileAttachments">
                                            <i class="fas fa-paperclip"></i>
                                        </a>
                                    </div>
                                </td>`
                    }
                    else if (AssignmentGradingTempId > 0 && Agrdidx > -1) {
                        appendStudentGradingTemp += ` <td id="tdStudentStatus_${lesson.LessonId}_${s.StudentId}" data-lessonId="${lesson.LessonId}" data-studentId="${s.StudentId}" class="min-width py-3 clstdstatus border-bottom border-right" style="--grade-color: ${AssignmentGradingColor}">
                                    <div class="remark text-left position-relative">
                                        <p class="text-bold grade" id="chkStudentStatus_${lesson.LessonId}_${s.StudentId}">${AssignmentGradingTemplateItemSymbol} <span class="d-block font-small text-medium">${AssignmentGradingDescription}</span></p>
                                        <a href="javascript:void(0);" id="viewAssignment_${lesson.LessonId}_${s.StudentId}" class="rounded-4x small text-semibold d-block text-center py-1 clsopenAssignemntView" data-studentId="${s.StudentId}" data-lessonId="${lesson.LessonId}" data-toggle="modal" data-target="#mpStudentGradeAssignment" style="color:${AssignmentGradingColor}">` + translatedResources.ViewAssignments+`</a>
                                        <a href="javascript:void(0);" id="btnFileUpload_${lesson.LessonId}_${s.StudentId}" class="attachment position-absolute top-0 clsopenMPAttachment" data-studentId="${s.StudentId}" data-lessonId="${lesson.LessonId}" data-count="${AttachmentCount}" data-toggle="modal" data-target="#mpStudentFileAttachments">
                                            <i class="fas fa-paperclip"></i>
                                        </a>
                                    </div>
                                </td>`
                    }
                    else {
                        appendStudentGradingTemp += `<td id="tdStudentStatus_${lesson.LessonId}_${s.StudentId}" data-lessonId="${lesson.LessonId}" data-studentId="${s.StudentId}" class="min-width py-3 no-grade clstdstatus border-bottom border-right">
                            <div class="remark text-left position-relative">
                                <p class="text-bold grade" id="chkStudentStatus_${lesson.LessonId}_${s.StudentId}">U <span class="d-block font-small text-medium">` + translatedResources.Unassigned+`</span></p>
                                <a href="javascript:void(0);" id="viewAssignment_${lesson.LessonId}_${s.StudentId}" class="rounded-4x small text-semibold d-block text-center py-1 clsopenAssignemntView" data-studentId="${s.StudentId}" data-lessonId="${lesson.LessonId}" data-toggle="modal" data-target="#mpStudentGradeAssignment">` + translatedResources.ViewAssignments+`</a>
                                <a href="javascript:void(0);" id="btnFileUpload_${lesson.LessonId}_${s.StudentId}" class="attachment position-absolute top-0 clsopenMPAttachment" data-studentId="${s.StudentId}" data-lessonId="${lesson.LessonId}" data-count="${AttachmentCount}" data-toggle="modal" data-target="#mpStudentFileAttachments">
                                            <i class="fas fa-paperclip"></i>
                                  </a>
                            </div>
                        </td>`
                    }

                }
                else {
                    appendStudentGradingTemp += `<td id="tdStudentStatus_${lesson.LessonId}_${s.StudentId}" data-lessonId="${lesson.LessonId}" data-studentId="${s.StudentId}" class="min-width py-3 no-grade clstdstatus">
                            <div class="remark text-left position-relative">
                                <p class="text-bold grade" id="chkStudentStatus_${lesson.LessonId}_${s.StudentId}">U <span class="d-block font-small text-medium">` + translatedResources.Unassigned+`</span></p>
                                <a href="javascript:void(0);" class="rounded-4x small text-semibold d-block text-center py-1 clsopenAssignemntView" data-studentId="${s.StudentId}" data-lessonId="${lesson.LessonId}" data-toggle="modal" data-target="#mpStudentGradeAssignment">` + translatedResources.ViewAssignments +`</a>
                                <a href="javascript:void(0);" id="btnFileUpload_${lesson.LessonId}_${s.StudentId}" class="attachment position-absolute top-0 clsopenMPAttachment" data-studentId="${s.StudentId}" data-lessonId="${lesson.LessonId}" data-count="${AttachmentCount}" data-toggle="modal" data-target="#mpStudentFileAttachments">
                                            <i class="fas fa-paperclip"></i>
                                  </a>
                            </div>
                        </td>`
                }

            });
            return appendStudentGradingTemp;
        },
        AppendGradeOnStudentLesson = function (gradingTempItemId) {
            if ($(".clsChkStudent:checked").length == 0 && $(".clschkLesson:checked").length == 0) {
                globalFunctions.showWarningMessage(translatedResources.SelectLessonStudentMsg)

                return false;
            }
            else if ($(".clschkLesson:checked").length == 0) {
                globalFunctions.showWarningMessage(translatedResources.SelectLessonMsg)

                return false;
            }
            else if ($(".clsChkStudent:checked").length == 0) {
                globalFunctions.showWarningMessage(translatedResources.SelectStudentMsg)

                return false;
            }

            let gradingTempIndex = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList.findIndex(x => x.GradingTemplateItemId == gradingTempItemId);
            let gradeTempItemToAppend = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[gradingTempIndex];

            $(".clschkLesson:checked").each(function () {
                let chkLessonId = $(this).attr('data-id');
                $(".clsChkStudent:checked").each(function () {
                    let studentId = $(this).attr('data-studentId');
                    let AssignmentGradingTempId = ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == studentId && stuL.LessonId == chkLessonId).length > 0 ? ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == studentId && stuL.LessonId == chkLessonId)[0].AssignmentGradingTemplateItemId : 0;
                    let Agrdidx = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList.findIndex(x => x.GradingTemplateItemId == AssignmentGradingTempId);
                    let AssGradingColor;
                    let AssGradingTemplateItemSymbol;
                    let AssGradingDescription
                    if (Agrdidx > -1) {
                        AssGradingColor = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[Agrdidx].GradingColor;
                        AssGradingTemplateItemSymbol = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[Agrdidx].ShortLabel;
                        AssGradingDescription = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[Agrdidx].GradingItemDescription;
                    }
                    let spnAssignmentOnGrades = Agrdidx > -1 ? `<span id="spnAssignmentGrading_${chkLessonId}_${studentId}" class="d-block font-small text-medium"  data-toggle="tooltip" title="${AssGradingDescription}">(${AssGradingTemplateItemSymbol})</span>` : ``;

                    $(`#chkStudentStatus_${chkLessonId}_${studentId}`).html(`${gradeTempItemToAppend.ShortLabel} ${spnAssignmentOnGrades} <span class="d-block font-small text-medium">${gradeTempItemToAppend.GradingItemDescription}</span>`);
                    $(`#tdStudentStatus_${chkLessonId}_${studentId}`).removeClass("no-grade").css("--grade-color", `${gradeTempItemToAppend.GradingColor}`).addClass("active");
                    $(`#viewAssignment_${chkLessonId}_${studentId}`).css("color", "");
                    let pIdx = ProgressTracker.StudentProgressMapper.findIndex(x => x.StudentId == studentId && x.LessonId == chkLessonId);
                    let sIdx = ProgressTracker.ProgressTrackerModel.StudentList.findIndex(x => x.StudentId == studentId && x.LessonId == chkLessonId);
                    let progressTrackerId = sIdx > -1 ? ProgressTracker.ProgressTrackerModel.StudentList[sIdx].Id : 0;

                    if (pIdx > -1) {
                        ProgressTracker.StudentProgressMapper[pIdx].GradingTemplateId = gradeTempItemToAppend.GradingTemplateItemId;

                    }
                    else {

                        ProgressTracker.StudentProgressMapper.push({ Id: progressTrackerId, LessonId: chkLessonId, StudentId: studentId, GradingTemplateId: gradeTempItemToAppend.GradingTemplateItemId })
                    }

                })

                //$(`#chklessonforAssignment_${chkLessonId}`).prop("checked", true);
                //$(`#spnLesson_${chkLessonId}`).html(gradeTempItemToAppend.ShortLabel).css("background-color", gradeTempItemToAppend.GradingColor)
                $("#btnSaveProgressTracker").removeClass("disabled")

            });
        },

        LessonHeaderGroupForStandardBankId = function (ParentName, lCount) {
            let lessonHeader = ``;
            //if (lCount === 1) {

            let tblheight = (ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.MainTopic == ParentName).length * 132)
            lessonHeader += `<th rowspan="${ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.MainTopic == ParentName).length}" class="fixed-column border-bottom-dashed border-right-dashed course-panel">
                                <table style="height: ${tblheight}px;">
                                    <tr style="height: 132px;">
                                        <td rowspan="${ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.MainTopic == ParentName).length}" class="vertical-main-header">
                                         <span class="text-uppercase text-semibold py-3">${ParentName}</span></td>
                                        ${ProgressTracker.getLessonHeaderTableForStandardBankId(ParentName)}
                                    </tr>
                                   
                                </table>
                            </th>`
            //}
            return lessonHeader;
        },
        getLessonHeaderTableForStandardBankId = function (ParentName) {
            let td = ``;
            let childCount = 0;
            let SubTopicModel = ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.MainTopic == ParentName);
            SubTopicModel.filter((i, idx) => {

                let SubIdx = SubTopicModel.findIndex(x => x.SubTopic == i.SubTopic);
                return SubIdx == idx;


            }).forEach((l) => {

                let subHeading = l.SubTopic;
                childCount = childCount + 1;
                td += `${ProgressTracker.getLessonHeaderforChildForStandardBankId(subHeading, childCount)}`
            });

            return td;

        },
        getLessonHeaderforChildForStandardBankId = function (subHeading, childCount) {
            let childStructure = ``;
            let childRowSpan = ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.SubTopic == subHeading).length;
            let trmSubtopicDescription = subHeading != null && subHeading != "" ? ((subHeading).length > 20 ? (subHeading).substring(0, 20) + "..." : subHeading) : "";
            if (childCount === 1) {

                childStructure += `<td rowspan="${childRowSpan}" class="vertical-subheader">
                                  <span class="text-capitalize py-3 text-medium">${trmSubtopicDescription}</span>
                                  </td>${ProgressTracker.getLessonForChildForStandardBankId(subHeading)}`

                return childStructure;
            }
            else {
                childStructure += `<tr style="height: 132px;"><td rowspan="${childRowSpan}" class="vertical-subheader">
                                  <span class="text-capitalize py-3 text-medium">${trmSubtopicDescription}</span>
                                  </td>${ProgressTracker.getLessonForChildForStandardBankId(subHeading)}</tr>`

                return childStructure;
            }

        },
        getLessonForChildForStandardBankId = function (childSubHeading) {

            let childLesson = ``;
            let countChildLessons = 0;
            ProgressTracker.ProgressTrackerModel.LessonList.filter(c => c.SubTopic == childSubHeading).forEach((cl) => {
                countChildLessons = countChildLessons + 1
                childLesson += `${ProgressTracker.getLessonStructureForStandardBankId(cl.LessonId, countChildLessons)}`
            })
            return childLesson;
        },
        getLessonStructureForStandardBankId = function (lessonId, countChildLessons) {
            let LessonDescription = ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.LessonId == lessonId).length > 0 ? ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.LessonId == lessonId)[0].StandardDescription : "";
            let standardCode = ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.LessonId == lessonId).length > 0 ? ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.LessonId == lessonId)[0].StandardCode : "";
            let standardDscription = LessonDescription.length > 15 ? LessonDescription.substring(0, 10) : LessonDescription
            let DisplayCodDesc = standardCode != "" && standardCode != null ? standardCode : standardDscription
            let LessonId = lessonId;
            let lessonStruct = ``;
            if (countChildLessons === 1) {
                lessonStruct += `<td data-toggle="popover" title="${DisplayCodDesc}" data-content="${LessonDescription}">
                              <span class="left-title text-right">${DisplayCodDesc}</span>
                            </td>
                            <td>
                            <div class="custom-control custom-checkbox">
                             <input type="checkbox" data-id="${LessonId}" class="custom-control-input clschkLesson" id="chkLesson_${LessonId}">
                             <label class="custom-control-label" for="chkLesson_${LessonId}"></label>
                            </div>
                            </td>`


                return lessonStruct;
            }
            else {
                lessonStruct += `<tr style="height: 132px;"><td data-toggle="popover" title="${DisplayCodDesc}" data-content="${LessonDescription}">
                            <span data-toggle="popover" title="Lesson Description" data-content="${LessonDescription}" class="left-title text-right">${DisplayCodDesc}</span>
                            </td>
                            <td>
                            <div class="custom-control custom-checkbox">
                             <input type="checkbox" data-id="${LessonId}" class="custom-control-input clschkLesson" id="chkLesson_${LessonId}">
                             <label class="custom-control-label" for="chkLesson_${LessonId}"></label>
                            </div>
                            </td></tr>`


                return lessonStruct;
            }
        },
        AssignmentChart = function () {
            let ctx = $("#assignmentGradingChart")[0]
            if (ProgressTracker.AssignmentLessonGrading.length > 0) {
                let GradeCaculus = [];
                ProgressTracker.AssignmentLessonGrading.map((t) => {

                    let divisibleValue = ((t.ScoreFrom + t.ScoreTo) / 2);
                    let object = {
                        MidValue: divisibleValue,
                        GradeSymbol: t.ShortLabel,
                        GradeColor: t.GradingColor,
                        AssignmentDate: t.DisplayDueDateLable
                    }
                    GradeCaculus.push(object);



                })

                let assignmentChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: ProgressTracker.AssignmentLessonGrading.map((x) => {
                            return x.DisplayDueDateLable
                        }),
                        datasets: [{
                            label: translatedResources.AssignmentGrades,
                            data: GradeCaculus.map((g) => { return g.MidValue }),
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    max: 100
                                }
                            }]
                        },
                        tooltips: {

                            callbacks: {
                                label: function (t, d) {
                                    return GradeCaculus.filter(x => x.MidValue == t.yLabel && x.AssignmentDate == t.xLabel)
                                        .map((y) => { return y.GradeSymbol })[0];
                                },
                                labelColor: function (t, d) {
                                    return {
                                        borderColor: GradeCaculus.filter(x => x.MidValue == t.yLabel && x.AssignmentDate == t.xLabel)
                                            .map((y) => { return y.GradeColor })[0],
                                        backgroundColor: GradeCaculus.filter(x => x.MidValue == t.yLabel && x.AssignmentDate == t.xLabel)
                                            .map((y) => { return y.GradeColor })[0]
                                    };
                                }
                            }
                        }
                    }
                });
            }


        },
        generateAccordion = function (s) {
            let acccordion = ``;
            let countParent = 0;
            ProgressTracker.ProgressTrackerModel.LessonList.filter((i, idx) => {
                let MIdx = ProgressTracker.ProgressTrackerModel.LessonList.findIndex(x => x.MainTopic == i.MainTopic);
                return MIdx == idx;

            }).forEach((m) => {
                countParent = countParent + 1;
                acccordion += ` <div class="card">  <!-- Card header -->
                        <div class="card-header p-0" role="tab" id="headingTwo1">
                            <a class="p-3" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo${countParent}"
                               aria-expanded="true" aria-controls="collapseTwo${countParent}">
                                <h5 class="mb-0">
                                    <span>${m.MainTopic}</span>
                                </h5>
                                <span class="plus float-right"></span>
                            </a>
                        </div>${ProgressTracker.generateAccordionCardBody(m.MainTopic, countParent, s)}</div>`

            })
            return acccordion;
        },

        generateAccordionCardBody = function (mainTopic, countPasser, s) {
            let accordionChild = ``;

            accordionChild += `<div id="collapseTwo${countPasser}" class="collapse show" role="tabpanel" aria-labelledby="headingTwo${countPasser}"
                             data-parent="#accordionEx1">
                            <div class="px-2 px-sm-5 pb-3">
                             <div class="accordion topics-accordion" id="topicAccordion${countPasser}" role="tablist" aria-multiselectable="true">
                                ${ProgressTracker.generateAccodionForSubTopic(mainTopic, countPasser, s)}
                             </div>
                             </div>
                             </div>`

            return accordionChild;

        },
        generateAccodionForSubTopic = function (mainTopic, countPasser, s) {
            let accordioSubTopic = ``;
            let countSubtopic = 0;
            ProgressTracker.ProgressTrackerModel.LessonList.filter((i, idx) => {
                let SMIdx = ProgressTracker.ProgressTrackerModel.LessonList.findIndex(x => x.MainTopic == mainTopic && x.SubTopic == i.SubTopic);
                return SMIdx == idx;

            }).forEach((sub) => {
                countSubtopic = countSubtopic + 1;
                let subTopicHeading = sub.SubTopic == "" || sub.SubTopic == null ? sub.MainTopic : sub.SubTopic;
                accordioSubTopic += `<div class="card"> <!-- Card header -->
                                        <div class="card-header p-0" role="tab" id="heading${countPasser}_${countSubtopic}">
                                            <a class="p-3" data-toggle="collapse" data-parent="#topicAccordion${countPasser}" href="#collapse${countPasser}_${countSubtopic}"
                                               aria-expanded="true" aria-controls="collapse${countPasser}_${countSubtopic}">
                                                <h5 class="mb-0 bullet">
                                                    <span class="">${subTopicHeading}</span>
                                                </h5>
                                                <span class="plus float-right"></span>
                                            </a>
                                        </div>
                                          ${ProgressTracker.generateAccordionObjective(mainTopic, countPasser, countSubtopic, sub.SubTopic, s)}
                                        </div>`

            })

            return accordioSubTopic;
        },
        generateAccordionObjective = function (mainTopic, countPasser, countSubPasser, subTopic, s) {
            let accordionObjective = ``;
            accordionObjective += `<div id="collapse${countPasser}_${countSubPasser}" class="collapse show" role="tabpanel" aria-labelledby="heading${countPasser}_${countSubPasser}"
                                             data-parent="#topicAccordion${countPasser}">
                                            <div class="row pt-3 mx-2"> 
                                              <div class="col-12 d-flex justify-content-end align-items-center flex-wrap">
                                                    <!--<small class="text-dark text-semibold mr-3 mb-3">1 Selected</small>
                                                    <a class="mr-3 mb-3">
                                                        <small class="btn-link text-medium">Clear Selection</small>
                                                    </a>-->
                                                    
                                                    <div class="grade-box mb-3">
                                                       ${ProgressTracker.generateGradingTemplateItem(mainTopic, subTopic, s)}
                                                    </div>
                                                </div>
                                               ${ProgressTracker.generateObjectiveExplanation(mainTopic, subTopic, s)}

                                           </div>
                                        </div>`
            return accordionObjective;
        },

        generateGradingTemplateItem = function (mainTopic, subTopic, s) {
            let gradingButton = ``;
            let MaintopicOnLesson = (mainTopic).split(" ").join('').replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-').replace('&', '');
            let subTopicIdentified = subTopic == "" || subTopic == null ? "Undefined" : (subTopic).split(" ").join('').replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-').replace('&', '');
            let clsbuttongroupIdentifier = "clsGradeBtn_" + MaintopicOnLesson + "_" + subTopicIdentified;

            ProgressTracker.ProgressTrackerModel.GradingTemplateItemList.forEach((g) => {

                gradingButton += ` <button class="grade ${clsbuttongroupIdentifier}  clsGradingItemTemplate" data-studentId = "${s.StudentId}" data-lessonontopic="${MaintopicOnLesson}" data-Subtopiconlesson="${subTopicIdentified}" data-gradingid="${g.GradingTemplateItemId}" style="color:${g.GradingColor}">${g.ShortLabel}</button>`

            })
            return gradingButton;



        },

        generateObjectiveExplanation = function (mainTopic, subTopic, s) {
            let appendObjectiveExp = ``;
            let MaintopicOnLesson = (mainTopic).split(" ").join('').replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-').replace('&', '');
            let subTopicIdentified = subTopic == "" || subTopic == null ? "Undefined" : (subTopic).split(" ").join('').replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-').replace('&', '');
            ProgressTracker.ProgressTrackerModel.LessonList.filter(x => x.MainTopic == mainTopic && x.SubTopic == subTopic).forEach((l) => {
                let standardDescription = l.StandardDescription.length > 50 ? l.StandardDescription.substring(0, 50) : l.StandardDescription;
                let popoverHeader = subTopic == null || subTopic == "" ? mainTopic : subTopic;
                let savedGradingColor = "#80808073";
                let savedShortLabel = "U";
                let stuLessonId = ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == l.LessonId).length > 0 ? ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == l.LessonId)[0].LessonId : 0;

                let stuGradingTempId = ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == l.LessonId).length > 0 ? ProgressTracker.ProgressTrackerModel.StudentList.filter(stuL => stuL.StudentId == s.StudentId && stuL.LessonId == l.LessonId)[0].GradingTemplateId : 0;
                let AttachmentLessonCount = $(`#btnFileUpload_${l.LessonId}_${s.StudentId}`).attr("data-count");
                let gradingidx = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList.findIndex(x => x.GradingTemplateItemId == stuGradingTempId);
                //if (gradingidx > -1) {
                //    savedGradingColor = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[gradingidx].GradingColor;
                //    savedShortLabel = ProgressTracker.ProgressTrackerModel.GradingTemplateItemList[gradingidx].ShortLabel;

                //}
                if (!$(`#tdStudentStatus_${l.LessonId}_${s.StudentId}`).hasClass("no-grade")) {
                    savedGradingColor = $(`#tdStudentStatus_${l.LessonId}_${s.StudentId}`).css("--grade-color");

                    savedShortLabel = $(`#chkStudentStatus_${l.LessonId}_${s.StudentId}`).html().split(" ")[0];
                }
                appendObjectiveExp += `<div class="col-lg-6 col-md-12">
                                                    <div class="card obective-card">
                                                        <div class="p-3 d-flex justify-content-between align-items-center">
                                                            <div class="d-flex align-items-center">
                                                                <span class="grade text-white" style="background-color:${savedGradingColor}" id=spnLesson_${l.LessonId}>${savedShortLabel}</span>
                                                                <h6 class="m-0 text-bold text-color-purple">`+ translatedResources.OBJECTIVE+`</h6>
                                                            </div>
                                                            <div class="check-item p-0 m-0">
                                                                <label for="group-value8">
                                                                    <input type="checkbox" data-lessonId="${l.LessonId}" dat-studentId="${s.StudentId}" class="clschkLessonFor_${MaintopicOnLesson}_${subTopicIdentified}" id="chklessonforAssignment_${l.LessonId}" data-toggle="toggle" data-on="Show" data-off="Hide" data-onstyle="primary" data-size="small" data-width="95">
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="card-body px-3 pb-3">
                                                            <p class="text-medium text-dark m-0 clsLessonParagraph" data-toggle="popover" title="${popoverHeader}" data-content="${l.StandardDescription}">
                                                              ${standardDescription}
                                                            </p>
                                                        </div>
                                                        <div class="p-3 border-top d-flex justify-content-between align-items-center flex-wrap">
                                                            <div class="d-flex justify-content-start flex-wrap">
                                                                <a class="mr-3 clsopenAssignemntView" data-studentId="${s.StudentId}" data-lessonId="${l.LessonId}" data-toggle="modal" data-target="#mpStudentGradeAssignment">
                                                                    <small class="text-semibold btn-link">`+ translatedResources.ViewAssignments +`</small>
                                                                </a>
                                                                <a class="clsopenMPAttachment" data-studentId="${s.StudentId}" data-lessonId="${l.LessonId}" data-toggle="modal" data-target="#mpStudentFileAttachments">
                                                                    <small class="text-semibold btn-link" id="smlViewEvidence_${l.LessonId}_${s.StudentId}">` + translatedResources.Evidence+`(${AttachmentLessonCount})</small>
                                                                </a>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>`

            })
            return appendObjectiveExp;

        }




    return {
        init: init,
        getCourseList: getCourseList,
        getCourseGroupByCourseId: getCourseGroupByCourseId,
        getCourseTopicsByCourseId: getCourseTopicsByCourseId,
        getProgressTracker: getProgressTracker,
        treeResponse: treeResponse,
        ProgressTrackerModel: ProgressTrackerModel,
        LessonProgressMapping: LessonProgressMapping,
        StudentProgressMapping: StudentProgressMapping,
        AppendGradeOnStudentLesson: AppendGradeOnStudentLesson,
        StudentProgressMapper: StudentProgressMapper,
        LessonHeaderGroupForStandardBankId: LessonHeaderGroupForStandardBankId,
        getLessonHeaderTableForStandardBankId: getLessonHeaderTableForStandardBankId,
        getLessonHeaderforChildForStandardBankId: getLessonHeaderforChildForStandardBankId,
        getLessonForChildForStandardBankId: getLessonForChildForStandardBankId,
        getLessonStructureForStandardBankId: getLessonStructureForStandardBankId,
        LessonByPassForSBID: LessonByPassForSBID,
        AssignmentLessonGrading: AssignmentLessonGrading,
        AssignmentChart: AssignmentChart,
        generateAccordion: generateAccordion,
        generateAccordionCardBody: generateAccordionCardBody,
        generateAccodionForSubTopic: generateAccodionForSubTopic,
        generateAccordionObjective: generateAccordionObjective,
        generateGradingTemplateItem: generateGradingTemplateItem,
        generateObjectiveExplanation: generateObjectiveExplanation,
    }


}();

$(document).ready(function () {

    ProgressTracker.init();

});