﻿
//----------------------------------------Grade Book Entry Object----------------------------------------
var GradeBookEntry = function () {
    var Init = function () {

        $('[data-toggle="popover"]').popover({
            trigger: 'hover'
        });

        $(".rightCol").mCustomScrollbar({
            axis: 'x',
            autoHideScrollbar: true,
            autoExpandScrollbar: true
        });

        //$("#examMarks").keyup(function () {
        //    if ($('#examMarks').val() < 0 || $('#examMarks').val() > 50) {
        //        $('#errorMsg').show();
        //    }
        //    else {
        //        $('#errorMsg').hide();
        //    }
        //});

    },
        //---------------------------------These Array are set on _LoadGradeBookEntryForm view ---------
        StudentList = [],
        HeaderList = [],
        AssessmentDataList = [],
        CommentsToAdd = [],
        GradeBookEntryList = [],
        GradeBookEntrySubHeader = [],
        SubHeaderList = [],
        SubHeaderData = [],
        //----------------------------------------------------------------------------------------------
        populateStudentName = index => {
            if (GradeBookEntry.StudentList.some(e => e.Index == index)) {
                var object = GradeBookEntry.StudentList.find(e => e.Index == index);
                $('#stuname').html(object.STU_NAME)
                setTimeout(() => GradeBookEntry.hideAndShowControls(), 500);
                GradeBookEntry.setStudentIndexForBackForward(object);
            }
        },
        populateReportName = index => {
            var object = GradeBookEntry.HeaderList.find(e => e.Index == index);
            var reportName = object.IS_PREV ? `${object.RSD_SUB_DESC} (${object.RSD_HEADER})` :
                object.IsGBM ? object.GBM_DESCR :
                    globalFunctions.isValueValid(object.RSD_SUB_DESC) ? object.RSD_SUB_DESC :
                        object.RSD_HEADER
            $('#rsdName').html(reportName);
            setTimeout(() => GradeBookEntry.hideAndShowControls(), 500);
            GradeBookEntry.setReportIndexForBackForward(object);
        },
        setStudentIndexForBackForward = (object) => {
            $('.stuUp')[object.Index == 0 ? 'hide' : 'show']();
            $('.stuUp').data('index', object.Index - 1)

            $('.stuDown')[object.Index == (GradeBookEntry.StudentList.length - 1) ? 'hide' : 'show']();
            $('.stuDown').data('index', object.Index + 1)

            $('#stuname').data('index', object.Index);
            $('#CommentStudentId').val(object.STU_ID);
        },
        setReportIndexForBackForward = (object) => {
            $('.hedUp')[object.Index == 0 ? 'hide' : 'show']();
            $('.hedUp').data('index', object.Index - 1)

            $('.hedDown')[object.Index == (GradeBookEntry.HeaderList.length - 1) ? 'hide' : 'show']();
            $('.hedDown').data('index', object.Index + 1)

            $('#rsdName').data('index', object.Index);
        },
        hideAndShowControls = () => {

            var studentIndex = $('#stuname').data('index');
            var headerIndex = $('#rsdName').data('index');
            var studentObj = GradeBookEntry.StudentList.find(e => e.Index == studentIndex);
            var headerIndexedObject = GradeBookEntry.HeaderList.find(e => e.Index == headerIndex)
            var headerObj = GradeBookEntry.HeaderList.find(e => (e.RSD_ID == headerIndexedObject.GBM_RSD_ID || e.Index == headerIndex));

            var assessmentObject = undefined;
            if (GradeBookEntry.GradeBookEntryList.some(e => e.STU_ID == studentObj.STU_ID && e.RSD_ID == headerObj.RSD_ID)) {
                assessmentObject = GradeBookEntry.GradeBookEntryList.find(e => e.STU_ID == studentObj.STU_ID && e.RSD_ID == headerObj.RSD_ID)
            } else {
                assessmentObject = GradeBookEntry.AssessmentDataList.find(e => e.STU_ID == studentObj.STU_ID && e.RSD_ID == headerObj.RSD_ID)
            }

            var subHeaderData = undefined;
            if (headerIndexedObject.IsGBM) {
                if (GradeBookEntry.GradeBookEntrySubHeader.some(e => e.GBD_GBM_ID == headerIndexedObject.GBM_ID && e.STU_ID == studentObj.STU_ID)) {
                    subHeaderData = GradeBookEntry.GradeBookEntrySubHeader.find(e => e.GBD_GBM_ID == headerIndexedObject.GBM_ID && e.STU_ID == studentObj.STU_ID)
                } else {
                    subHeaderData = GradeBookEntry.SubHeaderData.find(e => e.GBD_GBM_ID == headerIndexedObject.GBM_ID && e.STU_ID == studentObj.STU_ID)
                }
            }

            $('#gradeEntryHiddenStudentId').val(studentObj.STU_ID);
            $('#gradeEntryHiddenHeaderId').val(headerIndexedObject.IsGBM ? headerIndexedObject.GBM_ID : headerObj.RSD_ID);

            //==============================To Reset the form for next cell===============================================
            $('#CommonComments,label[for="CommonComments"],#saveAllAtOnce').show();
            $('#examMarks').removeClass('border-danger').removeAttr('IV');
            $('#absentButton').removeClass('btn-outline-primary');
            $('#errorMsg').hide();
            $('#CommonComments').val('');
            smsCommon.removeValidationMessage($('#examMarks'))
            //============================================================================================================

            if (headerIndexedObject.GBM_ENTRY_MODE == 'D' || headerIndexedObject.RSD_RESULT == 'D') {
                $('#EntryDdl').html(headerIndexedObject.IsGBM ? headerIndexedObject.GradeScale : headerObj.RSP_DESCR);
                if (subHeaderData && subHeaderData.GBD_GRADE) {
                    $('#EntryDdl').val(subHeaderData.GBD_GRADE);
                } else if (assessmentObject && assessmentObject.GRADING && !(headerIndexedObject.IsGBM)) {
                    $('#EntryDdl').val(assessmentObject.GRADING);
                } else {
                    $('#EntryDdl').val('');
                }
                var comment = headerIndexedObject.IsGBM && subHeaderData ? subHeaderData.GBD_COMMENT :
                    assessmentObject ? assessmentObject.COMMENTS : '';

                $('#CommonComments').val(comment);
                $('label[for="CommonComments"]').addClass('active');
                $('#EntryDdl').selectpicker('refresh');
            } else if (headerIndexedObject.GBM_ENTRY_MODE == 'M' || headerIndexedObject.RSD_RESULT == 'M') {

                $('#examMarks').data('type', headerIndexedObject.IsGBM ? headerIndexedObject.GBM_MARK_TYPE : headerObj.VALIDATION_TYPE.substr(0, 3));
                $('#examMarks').data('min', headerIndexedObject.IsGBM ? headerIndexedObject.GBM_MIN_MARK : headerObj.VALIDATION_MIN);
                $('#examMarks').data('max', headerIndexedObject.IsGBM ? headerIndexedObject.GBM_MAX_MARK : headerObj.VALIDATION_MAX);
                $('.maxMsg').html(translatedResources.MaxMarks.replace('{0}', headerIndexedObject.IsGBM ? headerIndexedObject.GBM_MAX_MARK : headerObj.VALIDATION_MAX))

                if (subHeaderData && subHeaderData.GBD_MARK) {
                    $('#examMarks').val(subHeaderData.GBD_MARK);
                    $('label[for="examMarks"]').addClass('active');

                } else if (assessmentObject && assessmentObject.MARK && !(headerIndexedObject.IsGBM)) {
                    $('#examMarks').val(assessmentObject.MARK);
                    $('label[for="examMarks"]').addClass('active');
                } else {
                    $('#examMarks').val('');
                }
                var comment = headerIndexedObject.IsGBM && subHeaderData ? subHeaderData.GBD_COMMENT :
                    assessmentObject ? assessmentObject.COMMENTS : '';
                $('#CommonComments').val(comment);
                $('label[for="CommonComments"]').addClass('active');

            } else if (headerObj.RSD_RESULT == 'C') {
                if (subHeaderData && subHeaderData.GBD_COMMENT) {
                    $('#MarkComments').val(subHeaderData.GBD_COMMENT);
                    $('label[for="MarkComments"]').addClass('active');
                } else if (assessmentObject && assessmentObject.COMMENTS && !(headerIndexedObject.IsGBM)) {
                    $('#MarkComments').val(assessmentObject.COMMENTS);
                    $('label[for="MarkComments"]').addClass('active');
                } else {
                    $('#MarkComments').val('');
                }
                $('#CommonComments,label[for="CommonComments"],#saveAllAtOnce').hide();
            }

            var isAbsent = headerIndexedObject.IsGBM ? subHeaderData && subHeaderData.GBD_bIS_PRESENT : assessmentObject &&  assessmentObject.bABSENT
            if (isAbsent)
                $('#absentButton').addClass('btn-outline-primary');

            $('.ctrl').hide();
            $(`#Entry${headerIndexedObject.IsGBM ? headerIndexedObject.GBM_ENTRY_MODE : headerIndexedObject.RSD_RESULT}`).show();

            GradeBookEntry.highlightCell(studentObj.STU_ID, headerObj.RSD_ID, headerIndexedObject.IsGBM ? headerIndexedObject.GBM_ID : undefined);
        },
        highlightCell = (studentId, headerId, gbmId) => {
            //==============================To remove the row highlighter ====================================================================
            $('.gradebook-table').removeClass('bg-light');

            //==============================To add the row highlighter for selected student ==================================================
            $(`.t_${studentId}`).addClass('bg-light');

            if (globalFunctions.isValueValid(gbmId)) {
                $('.align-middle').removeClass('bg-gradient-info');
                //==============================To add the row highlighter for selected cell =================================================
                $(`.${studentId}.${headerId}.${gbmId}`).addClass('bg-gradient-info');
                //============================== To auto scroll on X Axis ====================================================================
                $(".rightCol").mCustomScrollbar("scrollTo", $(`.${studentId}.${headerId}.${gbmId}`));

                //============================== To auto scroll on Y Axis ====================================================================
                $('html,body').stop().animate({
                    scrollTop: $(`.${studentId}.${headerId}.${gbmId}`).offset().top - 200
                }, 1000)
            } else {
                $('.align-middle').removeClass('bg-gradient-info');
                //==============================To add the row highlighter for selected cell =================================================
                $(`.${studentId}.${headerId}`).addClass('bg-gradient-info');

                //============================== To auto scroll on X Axis ====================================================================
                $(".rightCol").mCustomScrollbar("scrollTo", $(`.${studentId}.${headerId}`));

                //============================== To auto scroll on Y Axis ====================================================================
                $('html,body').stop().animate({
                    scrollTop: $(`.${studentId}.${headerId}`).offset().top - 200
                }, 1000)
            }
            //GradeBookEntry.populateDataFromEntry();
        },
        OpenComments = () => {
            var values = $('#MarkComments').val();
            $('#prgComments').html(values);
            $('.modal.commentbox').modal('show');
        },
        OpenCommentModal = () => {
            if (!(globalFunctions.isValueValid($('#CurrentStudentIdForModel').val()))) {
                $('#CurrentStudentIdForModel').val($('#gradeEntryHiddenStudentId').val())
            }
            if ($('#CurrentStudentIdForModel').val() == $('#gradeEntryHiddenStudentId').val()) {
                $('#tblassessmentComment tbody tr td input[type="checkbox"]').each(function () {
                    $(this).prop('checked', false);
                });
            }
            else {
                $("#divJtableComments").mCustomScrollbar('destroy')
                $('#ddlassessmentCategory').val("");
                $('#divJtableComments').empty();
                $('#CurrentStudentIdForModel').val($('#gradeEntryHiddenStudentId').val())
            }

            $('.modal.selectCategory').modal('show');
        },
        populateDataFromEntry = () => {

            var studentIndex = $('#stuname').data('index');
            var headerIndex = $('#rsdName').data('index');

            var studentObject = GradeBookEntry.StudentList.find(x => x.Index == studentIndex);

            var headerIndexedObject = GradeBookEntry.HeaderList.find(e => e.Index == headerIndex)
            var headerObject = GradeBookEntry.HeaderList.find(e => (e.RSD_ID == headerIndexedObject.GBM_RSD_ID || e.Index == headerIndex));


            var entryObject = undefined;
            if (globalFunctions.isValueValid(headerIndexedObject.IsGBM)) {
                if (GradeBookEntry.GradeBookEntrySubHeader.some(x => x.GBD_GBM_ID == headerIndexedObject.GBM_ID && x.STU_ID == studentObject.STU_ID)) {
                    entryObject = GradeBookEntry.GradeBookEntrySubHeader.find(x => x.GBD_GBM_ID == headerIndexedObject.GBM_ID && x.STU_ID == studentObject.STU_ID)
                } else {
                    entryObject = GradeBookEntry.SubHeaderData.find(x => x.GBD_GBM_ID == headerIndexedObject.GBM_ID && x.STU_ID == studentObject.STU_ID);
                }
            } else {
                if (GradeBookEntry.GradeBookEntryList.some(x => x.RSD_ID == headerObject.RSD_ID && x.STU_ID == studentObject.STU_ID)) {
                    entryObject = GradeBookEntry.GradeBookEntryList.find(x => x.RSD_ID == headerObject.RSD_ID && x.STU_ID == studentObject.STU_ID)
                } else {
                    entryObject = GradeBookEntry.AssessmentDataList.find(x => x.RSD_ID == headerObject.RSD_ID && x.STU_ID == studentObject.STU_ID);
                }
            }
            //var subHeaderData = undefined;
            //if (headerIndexedObject.IsGBM) {
            //    subHeaderData = GradeBookEntry.SubHeaderData.find(e => e.GBM_ID == headerIndexedObject.GBD_GBM_ID && e.STU_ID == studentObj.STU_ID)
            //}

            if (entryObject) {
                if (headerIndexedObject.GBM_ENTRY_MODE == 'D' || headerIndexedObject.RSD_RESULT == 'D') {
                    $('#EntryDdl').val(entryObject.GBD_GRADE || entryObject.GRADING);
                    $('#CommonComments').val(entryObject.GBD_COMMENT || entryObject.COMMENTS);
                    $('#EntryDdl').selectpicker('refresh');
                } else if (headerIndexedObject.GBM_ENTRY_MODE == 'M' || headerIndexedObject.RSD_RESULT == 'M') {
                    $('#examMarks').val(entryObject.GBD_MARK || entryObject.MARK);
                    $('label[for="examMarks"]').addClass('active');
                    $('#CommonComments').val(entryObject.GBD_COMMENT || entryObject.COMMENTS);
                } else if (headerObject.RSD_RESULT == 'C') {
                    $('#MarkComments').val(entryObject.GBD_COMMENT || entryObject.COMMENTS);
                    $('label[for="MarkComments"]').addClass('active');
                }
            }

        },
        populateDataTable = (object, type) => {
            var studentId = object.STU_ID;/*$('#gradeEntryHiddenStudentId').val();*/
            var headerId = $('#gradeEntryHiddenHeaderId').val();

            //to identify weater it is subheader or header
            var isGbm = globalFunctions.isValueValid(object.GBD_GBM_ID);

            //to highlight the button for absent 
            var isAbsent = isGbm ? object.GBD_bIS_PRESENT : object.bABSENT
            if (isAbsent)
                $('#absentButton').addClass('btn-outline-primary');
            else
                $('#absentButton').removeClass('btn-outline-primary');

            var subHeaderObject = GradeBookEntry.GradeBookEntrySubHeader.find(x => x.GBD_GBM_ID == headerId);

            var data = globalFunctions.isValueValid(subHeaderObject) ? (type == 'C' || object.GBD_bIS_PRESENT ? object.GBD_COMMENT : type == 'D' ? object.GBD_GRADE : object.GBD_MARK) :
                type == 'C' || object.bABSENT ? object.COMMENTS : type == 'D' ? object.GRADING : object.MARK;

            var subheaderId = '';
            if (globalFunctions.isValueValid(subHeaderObject)) {
                var objecSubHeader = GradeBookEntry.HeaderList.find(x => x.GBM_ID == subHeaderObject.GBD_GBM_ID)
                headerId = objecSubHeader.GBM_RSD_ID;
                subheaderId = `[data-gbm-id="${subHeaderObject.GBD_GBM_ID}"]`;
            } else if (isGbm) {
                var objecSubHeader = GradeBookEntry.HeaderList.find(x => x.GBM_ID == headerId)
                headerId = objecSubHeader.GBM_RSD_ID;
                subheaderId = `[data-gbm-id="${objecSubHeader.GBM_ID}"]`;
            }

            if ($(`a[data-stu-id="${studentId}"][data-rsd-id="${headerId}"]${subheaderId}[data-target="SubExamMarks"]`).length > 0) {
                if (type == 'C')
                    $(`td[data-stu-id="${studentId}"][data-rsd-id="${headerId}"]${subheaderId}[data-target="SubExamMarks"]`)
                        .html(`<div class="grade-value"><a href="javascript:void(0)" data-toggle="popover" title="${data}" data-placement="top"><i class="fas fa-comment-alt"></i></a></div>`);
                else
                    $(`td[data-stu-id="${studentId}"][data-rsd-id="${headerId}"]${subheaderId}[data-target="SubExamMarks"]`).html('');
            }

            if (globalFunctions.isValueValid(data)) {
                if (!(type == 'C')) {
                    $(`td[data-stu-id="${studentId}"][data-rsd-id="${headerId}"]${subheaderId}[data-target="SubExamMarks"]`).html('').append
                    //[globalFunctions.isValueValid(object.GBD_COMMENT || object.COMMENTS) && $('#gradeEntryHiddenStudentId').val() == object.STU_ID ? 'append' : 'text']
                        ((object.GBD_bIS_PRESENT || object.bABSENT) && $('#gradeEntryHiddenStudentId').val() == object.STU_ID ?
                            `<div class="grade-value"><a href="javascript:void(0)" data-toggle="popover" title="${object.GBD_COMMENT || object.COMMENTS}" data-placement="top"><i class="fas fa-user-minus text-danger"></i></a></div>` :
                            globalFunctions.isValueValid(object.GBD_COMMENT || object.COMMENTS) && $('#gradeEntryHiddenStudentId').val() == object.STU_ID ?
                                `<div class="grade-value"><span class="d-block"> ${data}</span><a href="javascript:void(0)" data-toggle="popover" title="${object.GBD_COMMENT || object.COMMENTS}" data-placement="top"><i class="fas fa-comment-alt"></i></a></div>` :

                                `<div class="grade-value"><span class="d-block">${data}</span></div>`);
                } else {
                    $(`td[data-stu-id="${studentId}"][data-rsd-id="${headerId}"]${subheaderId}[data-target="SubExamMarks"]`)
                        .html((object.GBD_bIS_PRESENT || object.bABSENT) && $('#gradeEntryHiddenStudentId').val() == object.STU_ID ?
                            `<div class="grade-value"><a href="javascript:void(0)" data-toggle="popover" title="${object.GBD_COMMENT || object.COMMENTS}" data-placement="top"><i class="fas fa-user-minus text-danger"></i></a></div>` :
                            `<div class="grade-value"><a href="javascript:void(0)" data-toggle="popover" title="${data}" data-placement="top"><i class="fas fa-comment-alt"></i></a></div>`
                        );
                }
                if (!($(`td[data-stu-id="${studentId}"][data-rsd-id="${headerId}"]${subheaderId}[data-target="SubExamMarks"]`).hasClass('bg-gradient-primary')))
                    $(`td[data-stu-id="${studentId}"][data-rsd-id="${headerId}"]${subheaderId}[data-target="SubExamMarks"]`).addClass('bg-gradient-primary');
            } else {
                $(`td[data-stu-id="${studentId}"][data-rsd-id="${headerId}"]${subheaderId}[data-target="SubExamMarks"]`)
                    .html(`
                            <div class="status-dd dropright mr-2 mr-sm-0 grade-value">
                                <a href="javascript:void(0)" class="trigger-rightDrawer rightDrawer plusBtn" data-stu-id="${studentId}" data-rsd-id="${headerId}" ${globalFunctions.isValueValid(subheaderId) ? subheaderId.replace('[', '').replace(']', '') : ``} data-target="SubExamMarks">
                                    <i class="far fa-plus-square text-dark"></i>
                                </a>
                            </div>
                    `);
            }

            $('[data-toggle="popover"]').popover({
                trigger: 'hover'
            });
        },
        SaveGradeBookEntry = (isAbsent) => {

            if (!(smsCommon.IsInvalidByFormOrDivId('gradeBookEntryForm')))
                return;

            if ($('#examMarks:visible').length && globalFunctions.isValueValid($('#examMarks').attr('IV')))
                return;


            var saveObject = {};
            var studentIndex = $('#stuname').data('index');
            var headerIndex = $('#rsdName').data('index');

            var studentObject = GradeBookEntry.StudentList.find(x => x.Index == studentIndex);
            var headerObject = GradeBookEntry.HeaderList.find(x => x.Index == headerIndex);

            if (globalFunctions.isValueValid(isAbsent)) {
                if (headerObject.GBM_ENTRY_MODE == 'C' || headerObject.RSD_RESULT == 'C') {
                    $('#MarkComments').val(translatedResources.Absent);
                } else {
                    $('#CommonComments').val(translatedResources.Absent);
                }
            }

            var object;
            if (globalFunctions.isValueValid(headerObject.IsGBM)) {
                var object = GradeBookEntry.GradeBookEntrySubHeader.find(x => x.GBD_GBM_ID == headerObject.GBM_ID && x.STU_ID == studentObject.STU_ID);
                var wasAbsent;
                if (object) {
                    if (isAbsent) {
                        wasAbsent = object.GBD_bIS_PRESENT;
                    }
                    GradeBookEntry.GradeBookEntrySubHeader = GradeBookEntry.GradeBookEntrySubHeader.filter(e => e != object);
                }

                //var HeaderObject = GradeBookEntry.HeaderList.find(x => x.RSD_ID == headerObject.GBM_RSD_ID);

                var subHeaderData = GradeBookEntry.SubHeaderData.find(x => x.GBD_GBM_ID == headerObject.GBM_ID && x.STU_ID == studentObject.STU_ID)

                if (subHeaderData) {
                    if (headerObject.GBM_ENTRY_MODE == 'D') {
                        if (subHeaderData.GBD_GRADE == $('#EntryDdl').val() && subHeaderData.GBD_COMMENT == (globalFunctions.isValueValid($('#CommonComments').val()) ? $('#CommonComments').val() : null))
                            return;
                    } else if (headerObject.GBM_ENTRY_MODE == 'M') {
                        if (subHeaderData.GBD_MARK == $('#examMarks').val() && subHeaderData.GBD_COMMENT == (globalFunctions.isValueValid($('#CommonComments').val()) ? $('#CommonComments').val() : null))
                            return;
                    }
                }

                var subHeaderObject = {
                    GBD_ID: globalFunctions.isValueValid(subHeaderData) ? subHeaderData.GBD_ID : 0,
                    GBD_GBM_ID: headerObject.GBM_ID,
                    GBD_MARK_TYPE: headerObject.GBM_MARK_TYPE,
                    GBD_MARK: globalFunctions.isValueValid(isAbsent) ? '' : $('#examMarks').val(),
                    GBD_GRADE: globalFunctions.isValueValid(isAbsent) ? '' : $('#EntryDdl').val(),
                    GBD_COMMENT: headerObject.GBM_ENTRY_MODE == 'C' ? $('#MarkComments').val() : wasAbsent ? "" : $('#CommonComments').val(),
                    GBD_bIS_PRESENT: wasAbsent ? false : globalFunctions.isValueValid(isAbsent),
                    STU_ID: studentObject.STU_ID
                };

                var data = headerObject.GBM_ENTRY_MODE == 'C' || subHeaderObject.GBD_bIS_PRESENT ? subHeaderObject.GBD_COMMENT :
                    headerObject.GBM_ENTRY_MODE == 'D' ? subHeaderObject.GBD_GRADE :
                        subHeaderObject.GBD_MARK;

                if (globalFunctions.isValueValid(data)) {
                    GradeBookEntry.GradeBookEntrySubHeader.push(subHeaderObject);
                }

                
                GradeBookEntry.populateDataTable(subHeaderObject, headerObject.GBM_ENTRY_MODE);
            } else {
                var object = GradeBookEntry.GradeBookEntryList.find(x => x.RSD_ID == headerObject.RSD_ID && x.STU_ID == studentObject.STU_ID);
                var wasAbsent;
                if (object) {
                    if (isAbsent) {
                        wasAbsent = object.bABSENT;
                    }
                    GradeBookEntry.GradeBookEntryList = GradeBookEntry.GradeBookEntryList.filter(e => e != object);
                }

                var assessmentData = GradeBookEntry.AssessmentDataList.find(e => e.RSD_ID == headerObject.RSD_ID && e.STU_ID == studentObject.STU_ID)

                if (assessmentData) {
                    if (headerObject.RSD_RESULT == 'D') {
                        if (assessmentData.GRADING == $('#EntryDdl').val() && assessmentData.COMMENTS == (globalFunctions.isValueValid($('#CommonComments').val()) ? $('#CommonComments').val() : null))
                            return;
                    } else if (headerObject.RSD_RESULT == 'M') {
                        if (assessmentData.MARK == $('#examMarks').val() && assessmentData.COMMENTS == (globalFunctions.isValueValid($('#CommonComments').val()) ? $('#CommonComments').val() : null))
                            return;
                    } else if (headerObject.RSD_RESULT == 'C') {
                        if (assessmentData.COMMENTS == $('#MarkComments').val())
                            return;
                    }
                }

                var GRD_ID = $('#ddlGradeBookGrade').val();
                var ACD_ID = $('#ddlGradeBookAcademicYear').val();
                var SGR_ID = $('#ddlGradeBookSubjectGroup').val();
                var SCT_ID = '0';
                var TRM_ID = $('#ddlGradeBookTerm').val();
                var prv = $('#ddlGradeBookEntryPreviousReportSchedule').val().join('|')
                var AOD_IDs = $('#ddlGradeBookEntryPreviousOptionalHeaders').val().join("|");
                var rsm_id = $('#ddlGradeBookReportHeader').val();
                var SBG_ID = globalFunctions.isValueValid($('#ddlGradeBookSubject').val()) ? $('#ddlGradeBookSubject').val() : 0;
                var RPF_ID = globalFunctions.isValueValid($('#ddlGradeBookReportSchedule').val()) ? $('#ddlGradeBookReportSchedule').val() : 0;

                var saveObject = {
                    RST_ID: globalFunctions.isValueValid(assessmentData) ? assessmentData.RST_ID : "0",
                    RPF_ID: RPF_ID,
                    RSD_ID: headerObject.RSD_ID,
                    ACD_ID: ACD_ID,
                    GRD_ID: GRD_ID,
                    STU_ID: studentObject.STU_ID,
                    RSS_ID: RPF_ID,
                    SGR_ID: SGR_ID,
                    SBG_ID: SBG_ID,
                    SCT_ID: SCT_ID,
                    TYPE_LEVEL: "",
                    MARK: globalFunctions.isValueValid(isAbsent) ? '' : $('#examMarks').val(),
                    COMMENTS: headerObject.RSD_RESULT == 'C' ? $('#MarkComments').val() : wasAbsent ? "" : $('#CommonComments').val(),
                    GRADING: globalFunctions.isValueValid(isAbsent) ? '' : $('#EntryDdl').val(),
                    bABSENT: wasAbsent ? false : globalFunctions.isValueValid(isAbsent)
                }
                var data = headerObject.RSD_RESULT == 'C' || saveObject.bABSENT ? saveObject.COMMENTS :
                    headerObject.RSD_RESULT == 'D' ? saveObject.GRADING :
                        saveObject.MARK
                if (globalFunctions.isValueValid(data)) {
                    GradeBookEntry.GradeBookEntryList.push(saveObject);
                }
                
                GradeBookEntry.populateDataTable(saveObject, headerObject.RSD_RESULT);
            }
            console.log(GradeBookEntry.GradeBookEntryList);
            console.log(GradeBookEntry.GradeBookEntrySubHeader);
        },
        SaveAllAtOnce = () => {

            if (!(smsCommon.IsInvalidByFormOrDivId('gradeBookEntryForm')))
                return;

            if ($('#examMarks:visible').length && globalFunctions.isValueValid($('#examMarks').attr('IV')))
                return;

            var count = 0;
            var headerId = $('#gradeEntryHiddenHeaderId').val();
            var headerObjectOfSubHeaderOrHeader = GradeBookEntry.HeaderList.find(x => (x.RSD_ID == headerId || x.GBM_ID == headerId));
            var headerObject = GradeBookEntry.HeaderList.find(x =>
                (x.RSD_ID == (headerObjectOfSubHeaderOrHeader.IsGBM ? headerObjectOfSubHeaderOrHeader.GBM_RSD_ID : headerObjectOfSubHeaderOrHeader.RSD_ID)));

            // select all option is available for header type 'M'
            if (headerObjectOfSubHeaderOrHeader.GBM_ENTRY_MODE == 'C' || headerObject.RSD_RESULT == 'C')
                return;

            var GRD_ID = $('#ddlGradeBookGrade').val();
            var ACD_ID = $('#ddlGradeBookAcademicYear').val();
            var SGR_ID = $('#ddlGradeBookSubjectGroup').val();
            var SCT_ID = '0';
            var TRM_ID = $('#ddlGradeBookTerm').val();
            var prv = $('#ddlGradeBookEntryPreviousReportSchedule').val().join('|')
            var AOD_IDs = $('#ddlGradeBookEntryPreviousOptionalHeaders').val().join("|");
            var rsm_id = $('#ddlGradeBookReportHeader').val();
            var SBG_ID = globalFunctions.isValueValid($('#ddlGradeBookSubject').val()) ? $('#ddlGradeBookSubject').val() : 0;
            var RPF_ID = globalFunctions.isValueValid($('#ddlGradeBookReportSchedule').val()) ? $('#ddlGradeBookReportSchedule').val() : 0;
            GradeBookEntry.StudentList.forEach(x => {
                var comment = /*globalFunctions.isValueValid(object) && globalFunctions.isValueValid(object.COMMENTS) && headerObject.RSD_RESULT != 'C' ? object.COMMENTS :*/
                    (headerObjectOfSubHeaderOrHeader.GBM_ENTRY_MODE == 'C' || headerObject.RSD_RESULT == 'C') ? $('#MarkComments').val() :
                        $('#gradeEntryHiddenStudentId').val() == x.STU_ID ? $('#CommonComments').val() : '';
                if (globalFunctions.isValueValid(headerObjectOfSubHeaderOrHeader.IsGBM)) {

                    if (GradeBookEntry.SubHeaderData.some(e => e.GBD_GBM_ID == headerObjectOfSubHeaderOrHeader.GBM_ID &&
                        e.STU_ID == x.STU_ID ))
                        return;

                    if (GradeBookEntry.GradeBookEntrySubHeader.some(e =>
                        e.GBD_GBM_ID == headerObjectOfSubHeaderOrHeader.GBM_ID && e.STU_ID == x.STU_ID &&
                        (globalFunctions.isValueValid(e.GBD_bIS_PRESENT) ||
                            headerObjectOfSubHeaderOrHeader.GBM_ENTRY_MODE == 'M' && globalFunctions.isValueValid(e.GBD_MARK) ||
                            headerObjectOfSubHeaderOrHeader.GBM_ENTRY_MODE == 'D' && globalFunctions.isValueValid(e.GBD_GRADE) ||
                            headerObject.RSD_RESULT == 'C' && globalFunctions.isValueValid(e.GBD_COMMENT))
                        ))
                        return;

                    var object = GradeBookEntry.GradeBookEntrySubHeader.find(e => e.GBD_GBM_ID == headerObjectOfSubHeaderOrHeader.GBM_ID && e.STU_ID == x.STU_ID);
                    if (object)
                        GradeBookEntry.GradeBookEntrySubHeader = GradeBookEntry.GradeBookEntrySubHeader.filter(e => e != object);

                    //var HeaderObject = GradeBookEntry.HeaderList.find(e => e.RSD_ID == headerObjectOfSubHeaderOrHeader.GBM_RSD_ID);

                    var subHeaderData = GradeBookEntry.SubHeaderData.find(e => e.GBD_GBM_ID == headerObjectOfSubHeaderOrHeader.GBM_ID && e.STU_ID == x.STU_ID)

                    var subHeaderObject = {
                        GBD_ID: globalFunctions.isValueValid(subHeaderData) ? subHeaderData.GBD_ID : 0,
                        GBD_GBM_ID: headerObjectOfSubHeaderOrHeader.GBM_ID,
                        GBD_MARK_TYPE: headerObjectOfSubHeaderOrHeader.GBM_MARK_TYPE,
                        GBD_MARK: $('#examMarks').val(),
                        GBD_GRADE: $('#EntryDdl').val(),
                        GBD_COMMENT: comment,
                        STU_ID: x.STU_ID
                    };

                    var data = headerObject.RSD_RESULT == 'C' ? subHeaderObject.GBD_COMMENT : headerObjectOfSubHeaderOrHeader.GBM_ENTRY_MODE == 'D' ? subHeaderObject.GBD_GRADE : subHeaderObject.GBD_MARK
                    if (globalFunctions.isValueValid(data)) {
                        GradeBookEntry.GradeBookEntrySubHeader.push(subHeaderObject);
                        count++;
                    }

                    //GradeBookEntry.GradeBookEntrySubHeader.push(subHeaderObject);
                    GradeBookEntry.populateDataTable(subHeaderObject, headerObjectOfSubHeaderOrHeader.GBM_ENTRY_MODE);
                    
                } else {

                    if (GradeBookEntry.AssessmentDataList.some(e => e.RSD_ID == headerObject.RSD_ID && e.STU_ID == x.STU_ID))
                        return;

                    if (GradeBookEntry.GradeBookEntryList.some(e => e.RSD_ID == headerObject.RSD_ID && e.STU_ID == x.STU_ID &&
                            (headerObject.RSD_RESULT == 'M' &&  globalFunctions.isValueValid(e.MARK) ||
                            headerObject.RSD_RESULT == 'D' &&  globalFunctions.isValueValid(e.GRADING) ||
                            headerObject.RSD_RESULT == 'C' &&  globalFunctions.isValueValid(e.COMMENTS) ||
                            globalFunctions.isValueValid(e.bABSENT))))
                        return;

                    var object = GradeBookEntry.GradeBookEntryList.find(e => e.RSD_ID == headerObject.RSD_ID && e.STU_ID == x.STU_ID);
                    if (object)
                        GradeBookEntry.GradeBookEntryList = GradeBookEntry.GradeBookEntryList.filter(e => e != object);

                    var assessmentData = GradeBookEntry.AssessmentDataList.find(e => e.RSD_ID == headerObject.RSD_ID && e.STU_ID == x.STU_ID)

                    var saveObject = {
                        RST_ID: globalFunctions.isValueValid(assessmentData) ? assessmentData.RST_ID : "0",
                        RPF_ID: RPF_ID,
                        RSD_ID: headerId,
                        ACD_ID: ACD_ID,
                        GRD_ID: GRD_ID,
                        STU_ID: x.STU_ID,
                        RSS_ID: RPF_ID,
                        SGR_ID: SGR_ID,
                        SBG_ID: SBG_ID,
                        SCT_ID: SCT_ID,
                        TYPE_LEVEL: "",
                        MARK: $('#examMarks').val(),
                        COMMENTS: comment,
                        GRADING: $('#EntryDdl').val()
                    }
                    var data = headerObject.RSD_RESULT == 'C' ? saveObject.COMMENTS : headerObject.RSD_RESULT == 'D' ? saveObject.GRADING : saveObject.MARK
                    if (globalFunctions.isValueValid(data)) {
                        GradeBookEntry.GradeBookEntryList.push(saveObject);
                        count++;
                    }

                    //GradeBookEntry.GradeBookEntryList.push(saveObject);

                    GradeBookEntry.populateDataTable(saveObject, headerObject.RSD_RESULT);                    
                }
                //}
            });
            //console.log(GradeBookEntry.GradeBookEntryList);
            //console.log(GradeBookEntry.GradeBookEntrySubHeader);
            //if (globalFunctions.isValueValid(headerObjectOfSubHeaderOrHeader.IsGBM)) {
            //    var object = GradeBookEntry.GradeBookEntrySubHeader.filter(x => x.GBD_GBM_ID == headerId);
            //    if (object && object.length > 0) {
            //        globalFunctions.showSuccessMessage(`${object.length} - ${translatedResources.showSuccessMessage}`)
            //    }
            //} else {
            //    var object = GradeBookEntry.GradeBookEntryList.filter(x => x.RSD_ID == headerId);
            //    if (object && object.length > 0) {
            //        globalFunctions.showSuccessMessage(`${object.length} - ${translatedResources.showSuccessMessage}`)
            //    }
            //}
            if (count > 0) {
                globalFunctions.showSuccessMessage(`${count} records ${translatedResources.showSuccessMessage}`)
            } else {
                globalFunctions.showSuccessMessage(`1 records ${translatedResources.showSuccessMessage}`)
            }
            
        },
        FinalSubmit = () => {
            smsCommon.postRequest('/Assessment/Assessment/GradeBookCUD', {
                AssessmentDatas: GradeBookEntry.GradeBookEntryList,
                GradeBookDetails: GradeBookEntry.GradeBookEntrySubHeader,
                bEdit: '0'
            }).then(x => {
                globalFunctions.showMessage(x.NotificationType, x.Message);                
                GradeBook.ViewStudents();
            })
        }
    return {
        Init,
        StudentList,
        HeaderList,
        AssessmentDataList,
        CommentsToAdd,
        GradeBookEntryList,
        GradeBookEntrySubHeader,
        SubHeaderList,
        SubHeaderData,
        populateStudentName,
        populateReportName,
        hideAndShowControls,
        highlightCell,
        OpenComments,
        OpenCommentModal,
        setStudentIndexForBackForward,
        setReportIndexForBackForward,
        populateDataFromEntry,
        SaveGradeBookEntry,
        SaveAllAtOnce,
        populateDataTable,
        FinalSubmit
    }
}();
//-------------------------------------------------------------------------------------------------------

//----------------------------------------Grade Book Entry Events----------------------------------------
$(document).ready(function () {
    GradeBookEntry.Init();
});