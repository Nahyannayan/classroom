﻿/// <reference path="../../../common/global.js" />
/// <reference path="../../../common/plugins.js" />
let isAddNew = false;
let NotifyCourseGrade = [];
let ProgressTrackerSetup = function () {
    ProgressTrackerSetupArray = [];
    ProgressRules = [];
    ProgressGridResponse = [];
    CourseIdOnGrid = [];
    GradeIdOnGrid = [];
    NotifyGrade = [];
    NotifyCourse = [];
    var Pid = 0;
    let courseList;
    let gradeTemplateList;
    let gradeSlabList;
    let progressSetupModel;
    init = function () {
        $('#breadcrumbActions').append(`<ul  class="list-pattern">
                         <li class="m-0 mr-3 d-none" id="liCurriculumn">
                        <select id="ddlCurriculumnList" class="selectpicker"></select>
                       </li> `)

        $('#tblProgressSetupGrid_filter').hide()
        ProgressTrackerSetup.ProgressSetupGrid()
        $(document).on("change", "#ddlCurriculumnList", function () {

            ProgressTrackerSetup.GetProgressSetupList($(this).val())
        })

        $(document).on('click', '#btnAddProgressSetup', function () {
            isAddNew = true;
            ProgressTrackerSetup.AddEditProgressSetup();

        });

        $(document).on('click', '#btnCancelProgressSetup', function () {

            $('#divProgressSetupGrid').removeClass('d-none');
            $('#divCardtitle').removeClass('d-none');

            $('#spnBtnBacktoGrid').addClass('d-none');
            $('#divAddEditProgressSetUp').addClass('d-none');

            $('#btnAddProgressSetup').removeClass('d-none');
            $("#liCurriculumn").removeClass("d-none");
            $("#ProgressSetupSearch").parent().removeClass("d-none")
        });

        $(document).on('change', '#CourseId', function () {
            let courseId = $(this).val().join(',');
            ProgressTrackerSetup.GradeList(courseId)

            if ($('#Id').val() == 0) {

                $('#GradeTemplateId').empty();
                $('#GradeSlabId').empty();

                $('#GradeTemplateId').append(`<option value="">Select</option>`);
                $('#GradeSlabId').append(`<option value="">Select</option>`);

                ProgressTrackerSetup.gradeTemplateList.forEach((gt) => {
                    $('#GradeTemplateId').append(`<option value="${gt.ItemId}">${gt.ItemName}</option>`).selectpicker('refresh');

                });

                ProgressTrackerSetup.gradeSlabList.forEach((gs) => {
                    $('#GradeSlabId').append(`<option value="${gs.ItemId}">${gs.ItemName}</option>`).selectpicker('refresh');

                });
            }




        });

        $(document).on('click', '.clsEditSetup', function () {
            let Id = $(this).attr('data-PSId');
            ProgressTrackerSetup.AddEditProgressSetup(Id)


        })
        $(document).on('click', '.clsDeleteSetup', function () {
            var objProgressSetup = {
                Id: $(this).attr('data-PSId')
            };


            $.post('/Assessment/ProgressTracker/SaveProgressSetup', {
                objProgressSetup: objProgressSetup

            }).then((response) => {
                globalFunctions.showMessage(response.NotificationType, response.Message)
                ProgressTrackerSetup.ProgressSetupGrid()
                $('#spnBtnBacktoGrid').trigger('click');


            });
        });
        $(document).on('click', '#btnSaveProgressSetup', function () {
            debugger
            if (common.IsInvalidByFormOrDivId('frmProgressSetup')) {
                var objProgressSetup = {
                    Id: $('#Id').val(),
                    CourseIds: $('#CourseId').val().join(','),
                    GradeIds: $('#SchoolGradeId').val().join(','),
                    GradeTemplateId: $('#GradeTemplateId').val(),
                    GradeSlabId: $('#GradeSlabId').val(),
                    ShowCodeAsHeader: $('#ShowCodeAsHeader').is(':checked'),
                    ShowAsDropDown: $('#ShowAsDropDown').is(':checked'),
                    ProgressRuleId: $("#ProgressRuleId").val(),
                    StartDate: $("#StartDate").val(),
                    EndDate: $("#EndDate").val(),
                    Steps: $("#Steps").val(),
                    ProgressRuleGradingTemplateId: $("#ProgressRuleGradingTemplateId").val(),
                    ProgressSetupRules: ProgressTrackerSetup.ProgressRules
                };


                $.post('/Assessment/ProgressTracker/SaveProgressSetup', {
                    objProgressSetup: objProgressSetup

                }).then((response) => {
                    globalFunctions.showMessage(response.NotificationType, response.Message)
                    ProgressTrackerSetup.ProgressSetupGrid()
                    $('#btnCancelProgressSetup').trigger('click');


                }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
            }
        });




        $(document).on("click", "#btnAddRulesList", function () {
            if (!$('#tblProgressSetupRules').DataTable().data().count()) {
                $(this).attr("rowindex", "");
            }
            let prGradingtempId = $("#PRGradingTemplateId").val();
            let prGradingTemp = $("#PRGradingTemplateId").find("option:selected").text();
            let steps = $("#Steps").val();
            let startDate = $("#StartDate").val();
            let endDate = $("#EndDate").val();
            let progressruleId = $(this).attr("data-progressRuleId");
            let isValidDateRange = true;
            if (new Date(startDate) > new Date(endDate)) {
                globalFunctions.showWarningMessage("Start cannot be greater than end date")
                return false;
            }
            ProgressTrackerSetup.ProgressRules.map((x) => {
                if (new Date(x.StartDate) <= new Date(startDate) && new Date(x.EndDate) >= new Date(endDate)) {

                    isValidDateRange = false;
                }

            });
            if (!isValidDateRange) {
                globalFunctions.showWarningMessage("Start and End date range already present, Please edit to change")
                return isValidDateRange;
            }
            if ($(this).attr("rowindex") != "") {

                $('#tblProgressSetupRules').DataTable().row($(this).attr('rowindex')).data([`${prGradingTemp}`,
                `${steps}`,
                `${startDate}`,
                `${endDate}`,
                `<a href="javascript:void(0);" data-grdingId="${prGradingtempId}" data-toggle="tooltip" title="Edit" class="clsRuleEdit">
                  <i class='fas fa-pencil-alt pr-2'></i>
                  </a>
                  <a href="javascript:void(0);" data-toggle="tooltip" title="Delete" class="mr-3 clsRuleDelete">
                    <i class="fa fa-trash-alt"></i>
                   </a>`]).draw();
            }
            else {
                if ($("#PRGradingTemplateId").val() != "" && $("#Steps").val() != "" && $("#StartDate").val() != "" && $("#EndDate").val() != "") {
                    //if (ProgressTrackerSetup.ProgressRules.findIndex(x => x.StartDate == startDate && x.EndDate == endDate) > -1) {
                    //    globalFunctions.showWarningMessage("Start and End date range already present, Please edit to change")
                    //    return false;
                    //}

                    let rowCatch = $('#tblProgressSetupRules').DataTable().row.add([
                        `${prGradingTemp}`,
                        `${steps}`,
                        `${startDate}`,
                        `${endDate}`,
                        `<a href="javascript:void(0);" data-grdingId="${prGradingtempId}" data-toggle="tooltip" title="Edit" class="clsRuleEdit">
                  <i class='fas fa-pencil-alt pr-2'></i>
                  </a>
                  <a href="javascript:void(0);" data-toggle="tooltip" title="Delete" class="mr-3 clsRuleDelete">
                    <i class="fa fa-trash-alt"></i>
                   </a>`]);

                    $('#tblProgressSetupRules').DataTable().row(rowCatch).column(0).nodes().to$().addClass('text-center');
                    $('#tblProgressSetupRules').DataTable().row(rowCatch).column(1).nodes().to$().addClass('text-center');
                    $('#tblProgressSetupRules').DataTable().row(rowCatch).column(2).nodes().to$().addClass('text-center');
                    $('#tblProgressSetupRules').DataTable().row(rowCatch).column(3).nodes().to$().addClass('text-center');
                    $('#tblProgressSetupRules').DataTable().row(rowCatch).column(4).nodes().to$().addClass('text-center');
                    $('#tblProgressSetupRules').DataTable().row(rowCatch).draw();
                }
                else {

                    globalFunctions.showWarningMessage("Please select & enter values to add rules")
                }
            }
            let objProgressRule = {
                ProgressRuleId: progressruleId,
                GradingTemplateId: prGradingtempId,
                StartDate: startDate,
                EndDate: endDate,
                Steps: steps
            }
            let pridx = ProgressTrackerSetup.ProgressRules.findIndex(x => x.GradingTemplateId == objProgressRule.GradingTemplateId)
            if (pridx > -1) {
                ProgressTrackerSetup.ProgressRules[pridx].StartDate = objProgressRule.StartDate
                ProgressTrackerSetup.ProgressRules[pridx].EndDate = objProgressRule.EndDate
                ProgressTrackerSetup.ProgressRules[pridx].Steps = objProgressRule.Steps
            }
            else {
                ProgressTrackerSetup.ProgressRules.push(objProgressRule)
            }

            $("#PRGradingTemplateId").val("")
            $("#PRGradingTemplateId").attr('disabled', false);
            $('#PRGradingTemplateId').selectpicker('refresh');
            $("#Steps").val("");
            $("#StartDate").val("");
            $("#EndDate").val("");
            $(this).attr("rowindex", "");
        });

        $(document).on('click', '.clsRuleDelete', function (event) {
            let ProgressRuleId = $(this).attr("data-progressRuleId");
            if (ProgressRuleId != undefined && ProgressRuleId > 0) {
                let isdeletidx = ProgressTrackerSetup.ProgressRules.findIndex(x => x.ProgressRuleId == ProgressRuleId)
                if (isdeletidx > -1) {
                    ProgressTrackerSetup.ProgressRules[isdeletidx].IsDeleted = true
                }
            }
            $('#tblProgressSetupRules').DataTable().row($(event.target).closest('tr')).remove().draw();
        });

        $(document).on('click', '.clsRuleEdit', function (event) {
            const row = $('#tblProgressSetupRules').DataTable().row($(event.target).closest('tr'));
            let gringId = $(this).attr("data-grdingId");
            let ProgressRuleId = $(this).attr("data-progressRuleId")
            $('#btnAddRulesList').attr('rowindex', row.index());
            $('#btnAddRulesList').attr('progressRuleId', ProgressRuleId);
            $("#PRGradingTemplateId").val(gringId).attr('disabled', true).selectpicker("refresh");
            $("#Steps").val(row.data()[1]);
            $("#StartDate").val(row.data()[2]);
            $("#EndDate").val(row.data()[3]);
        });

        $(document).on("change", "#SchoolGradeId", function () {
            ProgressTrackerSetup.NotifyGrade.length = 0;
            ProgressTrackerSetup.NotifyCourse.length = 0;
            $(this).val().map((gr) => {
                let grdMapindex = ProgressTrackerSetup.GradeIdOnGrid.findIndex(xg => xg.GradeId == gr);
                let gName = "";
                $("#CourseId").val().map((c) => {

                    let crMapindex = ProgressTrackerSetup.CourseIdOnGrid.findIndex(xc => xc.CourseId == c);

                    if (grdMapindex > -1 && crMapindex > -1) {
                        let cName = ProgressTrackerSetup.CourseIdOnGrid[crMapindex].CourseName;
                        gName = ProgressTrackerSetup.GradeIdOnGrid[grdMapindex].GradeName;
                        let notifyCourseIndex = ProgressTrackerSetup.NotifyCourse.findIndex(xN => xN == cName)
                        if (notifyCourseIndex > -1) {
                            ProgressTrackerSetup.NotifyCourse[notifyCourseIndex] = cName
                        }
                        else {
                            ProgressTrackerSetup.NotifyCourse.push(cName);

                        }

                    }


                })
                let notifygradeIndex = ProgressTrackerSetup.NotifyGrade.findIndex(xNG => xNG == gName)
                if (notifygradeIndex > -1) {
                    ProgressTrackerSetup.NotifyGrade[notifygradeIndex] = gName
                }
                else {
                    ProgressTrackerSetup.NotifyGrade.push(gName);

                }


            })

        });

        $(document).on('hidden.bs.select', '#SchoolGradeId', function () {
            debugger;
            let courseToNotify = ProgressTrackerSetup.NotifyCourse.join(",")
            let gradettoNotify = ProgressTrackerSetup.NotifyGrade.join(",")
            if (isAddNew) {

                $.get("/Assessment/ProgressTracker/ValidateProgressSetupCourseGrade", { courseIds: $("#CourseId").val().join(','), gradeIds: $("#SchoolGradeId").val().join(',') }).then((response) => {
                    if (response.length > 0) {
                        let Courses = response.filter((i, idx) => {

                            let Cidx = response.findIndex(x => x.CourseId == i.CourseId);
                            return Cidx == idx;
                        }).map((x) => {

                            return x.CourseName

                        }).join(",");

                        let Grades = response.filter((i, idx) => {

                            let Grdidx = response.findIndex(x => x.SchoolGradeId == i.SchoolGradeId);
                            return Grdidx == idx;
                        }).map((x) => {

                            return x.GradeDisplay

                        }).join(",");

                        console.log(Courses);
                        console.log(Grades);
                        ProgressTrackerSetup.triggerWaringOnCourseGrade(Courses, Grades)
                    }
                }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });



            }
        });
    },
        ProgressSetupGrid = function () {
            $.get('/Assessment/ProgressTracker/ProgressSetupGrid').then((response) => {
                ProgressTrackerSetup.ProgressGridResponse = response.result;
                $("#ddlCurriculumnList").empty();
                $("#ddlCurriculumnList").append(`<option value="">Select</option>`);

                response.schoolCurriculum.forEach((ddl) => {
                    $('#ddlCurriculumnList').append(`<option value="${ddl.Value}">${ddl.Text}</option>`).selectpicker('refresh');
                })
                $("#ddlCurriculumnList").val($("#ddlCurriculumnList option:eq(1)").val()).selectpicker('refresh').trigger("change")
                if (response.schoolCurriculum.length > 1) {
                    $("#liCurriculumn").removeClass("d-none")
                }

                ProgressTrackerSetup.ProgressGridResponse.map((x) => {
                    let courseId = x.CourseIds.split(',');
                    let courseName = x.CourseName.split(',');

                    let gradeId = x.GradeIds.split(',');
                    let grdName = x.GradeDisplay.split(',');

                    for (let i = 0; i < courseId.length; i++) {
                        let objCourseMaponGrid = {
                            CourseId: courseId[i],
                            CourseName: courseName[i].replace("&amp;", "&")

                        }
                        ProgressTrackerSetup.CourseIdOnGrid.push(objCourseMaponGrid);
                    }
                    for (let j = 0; j < gradeId.length; j++) {
                        let objGradeMaponGrid = {
                            GradeId: gradeId[j],
                            GradeName: grdName[j].replace("&amp;", "&")

                        }
                        ProgressTrackerSetup.GradeIdOnGrid.push(objGradeMaponGrid);
                    }




                })
                if ($.fn.DataTable.isDataTable($('#tblProgressSetupGrid'))) {
                    $('#tblProgressSetupGrid').DataTable().destroy();
                }
                $('#tblProgressSetupGrid').DataTable({
                    data: response.dataList.aaData,
                    "bLengthChange": false,
                    "bFilter": true,

                    columns: [
                        { "data": "CourseName" },
                        { "data": "GradeDisplay" },
                        { "data": "GradeTemplate", "sClass": "text-centre", "bsortable": false },
                        { "data": "ShowCodeAsHeader", "sClass": "text-centre", "bsortable": false },
                        { "data": "ShowAsDropDown", "sClass": "text-centre", "bsortable": false },
                        { "data": "Action", "sClass": "text-centre", "bsortable": false },
                    ],
                    columnDefs: [{
                        "targets": 'no-sort',
                        "bSort": false,
                        "order": []
                    }],

                });
                $('#tblProgressSetupGrid_filter').hide();
                setTimeout(() => $('#tblProgressSetupGrid').closest('div').addClass('table-responsive'), 800);
            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });


        },
        GradeList = function (courseId) {
            $.get('/ProgressTracker/GetGradesByCourseId', { courseIds: courseId }).then((response) => {
                $('#SchoolGradeId').empty()
                $('#SchoolGradeId').append(`<option value="">Select</option>`);
                response.forEach((x) => {
                    $('#SchoolGradeId').append(`<option value="${x.SchoolGradeId}">${x.GradeDisplay}</option>`).selectpicker('refresh');
                });
                if ($('#Id').val() > 0) {
                    $('#SchoolGradeId').val(ProgressTrackerSetup.progressSetupModel.GradeIds.split(',')).selectpicker('refresh');
                }
            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
        },
        GetProgressSetupList = function (curriculumId) {
            $.get("/ProgressTracker/GetProgressSetupList", { curriculumId: curriculumId }).then((response) => {

                ProgressTrackerSetup.courseList = response.courseList;
                ProgressTrackerSetup.gradeTemplateList = response.gradingTemplateList;
                ProgressTrackerSetup.gradeSlabList = response.gradingSlabList;


            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });


        },
        AddEditProgressSetup = function (Id) {
            $.get('/Assessment/ProgressTracker/AddEditProgressSetup', { Id: Id }).then((response) => {
                $('#divAddEditProgressSetUp').empty();
                $('#btnAddProgressSetup').addClass('d-none');
                $("#ProgressSetupSearch").parent().addClass('d-none');
                $('#divProgressSetupGrid').addClass('d-none');
                $("#liCurriculumn").addClass("d-none");
                $('#divCardtitle').addClass('d-none');

                $('#spnBtnBacktoGrid').removeClass('d-none');
                $('#divAddEditProgressSetUp').removeClass('d-none');
                $('#divAddEditProgressSetUp').html(response);

                $('#CourseId').empty()
                $('#CourseId').append(`<option value="">Select</option>`);

                ProgressTrackerSetup.courseList.forEach((c) => {
                    $('#CourseId').append(`<option value="${c.Value}">${c.Text}</option>`)

                });
                $("#PRGradingTemplateId").empty()
                $("#PRGradingTemplateId").append(`<option value="">Select</option>`);
                ProgressTrackerSetup.gradeTemplateList.forEach((gt) => {
                    $("#PRGradingTemplateId").append(`<option value="${gt.ItemId}">${gt.ItemName}</option>`).selectpicker('refresh');
                });
                if ($('#Id').val() > 0) {

                    ProgressTrackerSetup.gradeTemplateList.forEach((gt) => {
                        $('#GradeTemplateId').append(`<option value="${gt.ItemId}">${gt.ItemName}</option>`).selectpicker('refresh');
                        $("#ProgressRuleGradingTemplateId").append(`<option value="${gt.ItemId}">${gt.ItemName}</option>`).selectpicker('refresh');
                    });

                    ProgressTrackerSetup.gradeSlabList.forEach((gs) => {
                        $('#GradeSlabId').append(`<option value="${gs.ItemId}">${gs.ItemName}</option>`).selectpicker('refresh');

                    });

                    $('#CourseId').val(ProgressTrackerSetup.progressSetupModel.CourseIds.split(','))
                    ProgressTrackerSetup.GradeList(ProgressTrackerSetup.progressSetupModel.CourseIds)

                    $('#GradeTemplateId').val(ProgressTrackerSetup.progressSetupModel.GradeTemplateId).trigger("change")
                    $('#GradeSlabId').val(ProgressTrackerSetup.progressSetupModel.GradeSlabId)
                }

                $('.selectpicker').selectpicker('refresh');
                $('.date-picker').datetimepicker({
                    format: 'DD-MMM-YYYY',
                    minDate: '2000-01-01',
                    ignoreReadonly: true
                    //defaultDate: Date.parse($("#hdnStartDate").val()),
                });

                if ($.fn.DataTable.isDataTable($('#tblProgressSetupRules'))) {
                    $('#tblProgressSetupRules').DataTable().destroy();
                }
                $('#tblProgressSetupRules').DataTable({
                    "bLengthChange": false,
                });
                $('[data-toggle="tooltip"]').tooltip();
                $('#btnAddRulesList').attr('rowindex', "");
                ProgressTrackerSetup.ProgressRules = ProgressTrackerSetup.progressSetupModel.ProgressSetupRules.map((x) => {

                    return {
                        ProgressRuleId: x.ProgressRuleId,
                        GradingTemplateId: x.GradingTemplateId,
                        StartDate: x.DisplayStartDate,
                        EndDate: x.DisplayEndDate,
                        Steps: x.Steps, IsDeleted: x.IsDeleted
                    }

                });


            }).catch((error) => { globalFunctions.showMessage(error.NotificationType, error.Message) });
        },
        triggerWaringOnCourseGrade = function (CourseName, GradeDisplay) {
            noty({
                text: `Progress Setup already present for Course ${CourseName} and Grade ${GradeDisplay}`,
                layout: 'topCenter',
                modal: true,
                closeWith: ['button'],
                buttons: [
                    {
                        type: "button",
                        addClass: 'btn btn-outline-primary',
                        text: translatedResources.ok,
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }
                ]

            });
        }

    return {

        init: init,
        ProgressSetupGrid: ProgressSetupGrid,
        GradeList: GradeList,
        GetProgressSetupList: GetProgressSetupList,
        courseList: courseList,
        gradeTemplateList: gradeTemplateList,
        gradeSlabList: gradeSlabList,
        AddEditProgressSetup: AddEditProgressSetup,
        progressSetupModel: progressSetupModel,
        ProgressRules: ProgressRules,
        triggerWaringOnCourseGrade: triggerWaringOnCourseGrade,
        ProgressGridResponse: ProgressGridResponse,
        CourseIdOnGrid: CourseIdOnGrid,
        GradeIdOnGrid: GradeIdOnGrid,
        NotifyGrade: NotifyGrade,
        NotifyCourse: NotifyCourse
    }
}();

$(document).ready(function () {
    ProgressTrackerSetup.init();
    $("#ProgressSetupSearch").on("input", function (e) {
        e.preventDefault();
        $('#tblProgressSetupGrid').DataTable().search($(this).val()).draw();

    });
});