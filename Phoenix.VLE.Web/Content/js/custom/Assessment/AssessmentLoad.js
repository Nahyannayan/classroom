﻿var CommentsToAdd = [];
var Sectionsval = 0;
$(document).ready(function () {


    $('#divrowbtnsave').hide();
    $('[data-toggle="tooltip"]').tooltip();
    LoadTerms();
    // LOADING ReportHeaders
    function LoadReportHeader() {
        $("#ReportHeaders").empty();
        try {
            var url = "/Assessment/Assessment/FetchReportHeaders";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { id: $("#AcademicYearDDL").val() },
                success: function (Sections) {
                    $("#ReportHeaders").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#ReportHeaders").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('ReportHeaders');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });
            return false;
        } catch (e) {
            alert(e);
        }
    }
    //-----------------------------------------
    // LOADING Grades 
    function LoadGrades() {
        $("#Grades").empty();
        try {
            var url = "/Assessment/Assessment/GetGradesAccess";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYearDDL").val(), rsm_id: $("#ReportHeaders").val() },
                success: function (Sections) {
                    $("#Grades").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Grades").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Grades');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });
            return false;
        } catch (e) {
            alert(e);
        }
    }

    $("#ReportHeaders").change(function () {
        $("#Subjects").empty();
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();
        LoadGrades();
        LoadReportSchedule();
    });

    function LoadTerms() {
        $("#Term").empty();
        try {
            var url = "/Assessment/Assessment/FetchTerms";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYearDDL").val() },
                success: function (Sections) {
                    $("#Term").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Term").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Term');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });
            return false;
        } catch (e) {
            alert(e);
        }
    }

    $("#AcademicYearDDL").change(function () {
        $("#Grades").empty();
        $("#Subjects").empty();
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();

        $("#Subjects").append('<option value="">Select</option>');
        $("#SubjectGroups").append('<option value="">Select</option>');
        $("#Grades").append('<option value="">Select</option>');
        $("#ReportSchedule").append('<option value="">Select</option>');
        LoadTerms();
    });

    $("#Term").change(function () {
        $("#Subjects").empty();
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();
        LoadReportHeader();
    });
    //-----------------------------------------
    // LOADING Subjects
    function LoadSubjects() {
        $("#Subjects").empty();
        try {
            var url = "/Assessment/Assessment/GetSubjectsByGrade";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYearDDL").val(), grd_id: $("#Grades").val() },
                success: function (Sections) {
                    $("#Subjects").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Subjects").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Subjects');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });
            return false;
        } catch (e) {
            alert(e);
        }
    }

    $("#Grades").change(function () {
        if ($('#gradeEntry-tab').hasClass('active')) {
            $("#PreviousReportSchedule").empty();
            $("#OptionalHeaders").empty();
            $("#SubjectGroups").empty();
            $("#SubjectCategory").empty();
            LoadSubjects();
            $("#PreviousReportSchedule").append('<option value="">Select</option>');
            $("#OptionalHeaders").append('<option value="">Select</option>');
            $.post('/Assessment/Assessment/GetAssessmentOptionalLists',
                { ACD_ID: $('#AcademicYearDDL').val(), GRD_ID: $(this).val() },
                function (response) {

                    for (var i = 0; i < response.PreviousResult.length; i++) {

                        $("#PreviousReportSchedule").append('<option value="'
                            + response.PreviousResult[i].RPF_ID + '">'
                            + response.PreviousResult[i].DISPLAY_TEXT + '</option>');
                    }

                    for (var i = 0; i < response.OptionResult.length; i++) {

                        $("#OptionalHeaders").append('<option value="'
                            + response.OptionResult[i].AOM_ID + '">'
                            + response.OptionResult[i].AOM_DESCR + '</option>');
                    }
                    $('.selectpicker').selectpicker('refresh');
                });
            return false;
        }
        else {
            LoadSection();
        }
    });
    //-----------------------------------------
    // LOADING Sections
    function LoadSection() {
        $("#Section").empty();
        try {
            var url = "/Assessment/Assessment/GetSectionAccess";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { ACD_ID: $("#AcademicYearDDL").val(), GRD_ID: $("#Grades").val() },
                success: function (Sections) {
                    $("#Section").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Section").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Section');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });
            return false;
        } catch (e) {
            alert(e);
        }

    }
    //-----------------------------------------


    $("#Subjects").change(function () {
        LoadSubjectGroups();
        LoadSubjectCategory();
        // alert($("#SubjectCategory").val())


    });
    //-----------------------------------------
    // LOADING Subject Category
    function LoadSubjectCategory() {
        $("#SubjectCategory").empty();
        try {
            var url = "/Assessment/Assessment/FetchSubjectCategory";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { sbg_id: $("#Subjects").val() },
                success: function (Sections) {
                    $("#SubjectCategory").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#SubjectCategory").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('SubjectCategory');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });
            return false;
        } catch (e) {
            alert(e);
        }
    }
    //-----------------------------------------
    // LOADING Report Schedule
    function LoadReportSchedule() {
        $("#ReportSchedule").empty();
        try {
            var url = "/Assessment/Assessment/FetchReportSchedule";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { rsm_id: $("#ReportHeaders").val() },
                success: function (Sections) {
                    $("#ReportSchedule").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#ReportSchedule").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('ReportSchedule');

                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });
            return false;
        } catch (e) {
            alert(e);
        }

    }

    $('#ddlassessmentCategory').on('change', function () {
        var value = $(this).val();
        $.get('/Assessment/Assessment/GetCommentByCategoryId', { CAT_ID: value, STU_ID: studentId }, function (response) {
            $('#divJtableComments').html(response);
        });
    });

    $(document).on('focus', 'input, select, textarea ,.td_data', function () { $(this).addClass('elm_edit'); });
    //LoadSubjectGroupsBYSubjectGroup();
    // LOADING Subject Groups
    function LoadSubjectGroups() {

        $("#SubjectGroups").empty();
        try {
            var url = "/Assessment/Assessment/GetSubjectGroupBySubject";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { sbg_id: $("#Subjects").val(), grd_id: $("#Grades").val() },
                success: function (Sections) {
                    if (globalFunctions.isValueValid(Sections)) {
                        $("#SubjectGroups").append('<option value="0">Select</option>');
                        Sectionsval = Sections[0].Value;
                        //alert(Sectionsval)
                        $.each(Sections, function (i, Section) {
                            $("#SubjectGroups").append('<option value="'
                                + Section.Value + '">'
                                + Section.Text + '</option>');
                        });
                        $('.selectpicker').selectpicker('refresh');
                        setDropDownValueAndTriggerChange('SubjectGroups');

                        var value = $("#strtt_id").val();
                        if (value == "0" ? 0 : value)
                            if (value != 0) {
                                LoadSubjectGroupsBYSubjectGroup(value)
                            }
                    }
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                },

            });
            return false;
        } catch (e) {
            alert(e);
        }
    }
    function LoadSubjectGroupsBYSubjectGroup(id) {
        //Sectionsval ='@(ViewBag.strtt_id)'
        try {
            var url = "/Assessment/Assessment/GetHeaderBySubjectCategory";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { id: id },
                success: function (Sections) {
                    if (Sections.length > 0) {
                        //if (Sections[0].SGR_ACD_ID != 0 || Sections[0].SGR_ACD_ID != null) {
                        //    $("#AcademicYearDDL").val(Sections[0].SGR_ACD_ID);
                        //}
                        var Data = Sections[0].SGR_GRD_ID; //dynamically generated 
                        var arrGrdSec = [];
                        //alert(Data);
                        $.each(Data.split(/\|/), function (i, val) {
                            //alert(val);
                            arrGrdSec.push(val);
                        })
                        //if (Sections[0].SGR_GRD_ID != 0 || Sections[0].SGR_GRD_ID != null) {
                        //    $("#Grades").val(arrGrdSec[0]);
                        //}
                        //if (Sections[0].SGR_SBG_ID != 0 || Sections[0].SGR_SBG_ID != null) {
                        //    $("#Subjects").val(Sections[0].SGR_SBG_ID);
                        //}

                    }
                    $('.selectpicker').selectpicker('refresh');
                    if ($('#formTutor-tab').hasClass('active')) {

                        var _GRD_ID = arrGrdSec[0] == "" || arrGrdSec[0] == null ? $('#Grades').val() : arrGrdSec[0];
                        var _ACD_ID = Sections[0].SGR_ACD_ID == "" || Sections[0].SGR_ACD_ID == null ? $('#AcademicYearDDL').val() : Sections[0].SGR_ACD_ID;
                        var _SBG_ID = Sections[0].SGR_SBG_ID == "" || Sections[0].SGR_SBG_ID == null ? $('#Subjects').val() : Sections[0].SGR_SBG_ID;
                        var _RPF_ID = $('#ReportSchedule').val() == "" || $('#ReportSchedule').val() == null ? 0 : $('#ReportSchedule').val();
                        var _SGR_ID = $('#SubjectGroups').val() == "" || $('#SubjectGroups').val() == null ? 0 : $('#SubjectGroups').val();
                        var _RSM_ID = $('#ReportHeaders').val();
                        var _SCT_ID = $('#Section').val() == "" || $('#Section').val() == null ? 0 : $('#Section').val();
                        var _AOD_IDS = $('#OptionalHeaders').val().toString().replace(",", "|"); //NEED TO SET THIS
                        var TRM_ID = $("#Term").val();
                        IsReportPublish(_RSM_ID, _RPF_ID, _ACD_ID, _GRD_ID, _SCT_ID, TRM_ID);
                        LoadOptionalHeaders(_GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS)
                        $('#divrowbtnsave').show();

                    }

                    //ViewStudents();
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });
            return false;
        } catch (e) {
            alert(e);
        }
    }

});


function datatableInit() {
    if (!$.fn.dataTable.isDataTable('#example')) {
        $('#example').DataTable({
            "paging": false,
            "fixedHeader": true,
            //"ordering": false,
            "columnDefs": [
                {
                    "width": "8%",
                    "targets": 0,
                    "orderable": false
                },
                {
                    "width": "25%",
                    "targets": 1,
                    "orderable": true
                },
                {
                    "targets": "no-sort",
                    "orderable": false
                }
            ]
        });
    }
}

onAssessmentSaveSuccess = function (data) {
    globalFunctions.showMessage(data.NotificationType, data.Message);
    if (data.Success) {
        $('#btn_View').click();
    }
}

function ViewStudents() {
    if ($('#gradeEntry-tab').hasClass('active')) {
        if (validate_Dropdowns($('#AcademicYearDDL')) && validate_Dropdowns($('#ReportHeaders')) && validate_Dropdowns($('#Grades')) && validate_Dropdowns($('#Subjects')) && validate_Dropdowns($('#SubjectGroups')) && validate_Dropdowns($('#ReportSchedule'))) {

            var _GRD_ID = $('#Grades').val();
            var _ACD_ID = $('#AcademicYearDDL').val();
            var _SBG_ID = $('#Subjects').val() == "" || $('#Subjects').val() == null ? 0 : $('#Subjects').val();
            var _RPF_ID = $('#ReportSchedule').val() == "" || $('#ReportSchedule').val() == null ? 0 : $('#ReportSchedule').val();
            var _SGR_ID = $('#SubjectGroups').val() == "" || $('#SubjectGroups').val() == null ? 0 : $('#SubjectGroups').val();
            var _RSM_ID = $('#ReportHeaders').val();
            var _SCT_ID = $('#Section').val() == "" || $('#Section').val() == null ? 0 : $('#Section').val();
            var _AOD_IDS = $('#OptionalHeaders').val().toString().replace(",", "|"); //NEED TO SET THIS
            var TRM_ID = $("#Term").val();
            IsReportPublish(_RSM_ID, _RPF_ID, _ACD_ID, _GRD_ID, _SCT_ID, TRM_ID);
            LoadOptionalHeaders(_GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS)
            $('#divrowbtnsave').show();
        }
    }
    else {
        if (validate_Dropdowns($('#AcademicYearDDL')) && validate_Dropdowns($('#ReportHeaders')) && validate_Dropdowns($('#Grades')) && validate_Dropdowns($('#Section')) && validate_Dropdowns($('#ReportSchedule'))) {

            var _GRD_ID = $('#Grades').val();
            var _ACD_ID = $('#AcademicYearDDL').val();
            var _SBG_ID = $('#Subjects').val() == "" || $('#Subjects').val() == null ? 0 : $('#Subjects').val();
            var _RPF_ID = $('#ReportSchedule').val() == "" || $('#ReportSchedule').val() == null ? 0 : $('#ReportSchedule').val();
            var _SGR_ID = $('#SubjectGroups').val() == "" || $('#SubjectGroups').val() == null ? 0 : $('#SubjectGroups').val();
            var _RSM_ID = $('#ReportHeaders').val();
            var _SCT_ID = $('#Section').val() == "" || $('#Section').val() == null ? 0 : $('#Section').val();
            var _AOD_IDS = $('#OptionalHeaders').val().toString().replace(",", "|"); //NEED TO SET THIS
            var TRM_ID = $("#Term").val();
            IsReportPublish(_RSM_ID, _RPF_ID, _ACD_ID, _GRD_ID, _SCT_ID, TRM_ID);
            LoadOptionalHeaders(_GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS)
            $('#divrowbtnsave').show();
        }
    }
}
//CHAINING OF MULTIPLE AJAX CALLS TO GET THE DESIRED RESULT SET
function LoadOptionalHeaders(_GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS) {
    $.ajax({
        type: "GET",
        url: "/Assessment/Assessment/GetReportHeaderOptional",
        data: { AOD_IDs: _AOD_IDS },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (OptionalHeaderResults) {
            LoadOptionalAssessmentData(OptionalHeaderResults, _GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS)
        },
        error: function (error) {

        }
    });
    return true;
}

function LoadOptionalAssessmentData(OptionalHeaderResults, _GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS) {
    $.ajax({
        type: "GET",
        url: "/Assessment/Assessment/GetAssessmentDataOptional",
        data: { ACD_ID: _ACD_ID, RPF_ID: _RPF_ID, RSM_ID: _RSM_ID, SBG_ID: _SBG_ID, SGR_ID: _SGR_ID, GRD_ID: _GRD_ID, SCT_ID: _SCT_ID, AOD_IDs: _AOD_IDS },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (OptionalDataResults) {
            LoadHeaders(OptionalHeaderResults, OptionalDataResults, _GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS)
        },
        error: function (error) {

        }
    });
    return true;
}

function LoadHeaders(OptionalHeaderResults, OptionalDataResults, _GRD_ID, _ACD_ID, _SBG_ID, _RPF_ID, _SGR_ID, _RSM_ID, _SCT_ID, _AOD_IDS) {
    $.ajax({
        type: "GET",
        url: "/Assessment/Assessment/GetReportHeaders",
        data: { GRD_ID: _GRD_ID, ACD_ID: _ACD_ID, SBG_ID: _SBG_ID, RPF_ID: _RPF_ID, RSM_ID: _RSM_ID, prv: $('#PreviousReportSchedule').val().toString() },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            LoadStudentList(OptionalHeaderResults, OptionalDataResults, results, _GRD_ID, _ACD_ID, _SGR_ID, _SBG_ID, _RSM_ID, _RPF_ID, _SCT_ID, _AOD_IDS)
        },
        error: function (error) {
        }
    });
    return true;
}

function LoadStudentList(OptionalHeaderResults, OptionalDataResults, Headerresult, _GRD_ID, _ACD_ID, _SGR_ID, _SBG_ID, _RSM_ID, _RPF_ID, _SCT_ID, _AOD_IDS) {
    $.ajax({
        type: "GET",
        url: "/Assessment/Assessment/GetStudentList",
        data: { GRD_ID: _GRD_ID, ACD_ID: _ACD_ID, SGR_ID: _SGR_ID, SCT_ID: _SCT_ID },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            LoadAssessmentData(OptionalHeaderResults, OptionalDataResults, Headerresult, results, _ACD_ID, _SBG_ID, _RPF_ID, _RSM_ID, _AOD_IDS);

        },
        error: function (error) {
            //   globalFunctions.onFailure();
        }
    });
    return true;
}

function LoadAssessmentData(OptionalHeaderResults, OptionalDataResults, Headerresult, results, _ACD_ID, _SBG_ID, _RPF_ID, _RSM_ID, _AOD_IDS) {
    var headerth = "";
    $.ajax({
        type: "GET",
        url: "/Assessment/Assessment/GetAssessmentData",
        data: { ACD_ID: _ACD_ID, SBG_ID: _SBG_ID, RPF_ID: _RPF_ID, RSM_ID: _RSM_ID, prv: $('#PreviousReportSchedule').val().toString() },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (assess_data) {
            //===================HEADER START=====================================
            $('#LoadAssessment_H_tr').html("");
            var items = Headerresult;
            //PLACE OPTIONAL HEADERS HERE
            var headerlayout = "";
            for (var i = 0; i < OptionalHeaderResults.length; i++) {
                headerth = OptionalHeaderResults[i].AOM_DESCR;
                headerlayout += "<th class='font-weight-light text-center no-sort border-right text-uppercase' width='200px'>";
                headerlayout += headerth;
                headerlayout += "</th>";
            }
            for (var i = 0; i < items.length; i++) {
                if (items[i].RSD_SUB_DESC == '') {
                    headerth = items[i].RSD_HEADER;
                }
                else {
                    if (items[i].IS_PREV) {
                        headerth = items[i].RSD_SUB_DESC + " (" + items[i].RSD_HEADER + ")";
                    }
                    else {
                        headerth = (items[i].RSD_bLock == 1) ? items[i].RSD_SUB_DESC + "<i class='fa fa-lock pr-2 pl-2' data-toggle='tooltip' title='Blocked For Entry'></i>" : items[i].RSD_SUB_DESC;
                    }
                }
                headerlayout += "<th class='font-weight-light text-center no-sort  border-right text-uppercase' width='200px'>";
                headerlayout += headerth;
                headerlayout += "</th>";
            }

            $('#LoadAssessment_H_tr').append(headerlayout);
            $('#LoadAssessment_H_tr').prepend("<th class='font-weight-light text-center bg-white text-primary border-right table-first text-uppercase' width='10%'>Student Name <div class='table-seperator'>&nbsp;</div></th>");

            //===================HEADER END=====================================

            //===================BODY START=====================================
            var layout = "";
            var header = "";
            //
            if (assess_data.length >= 0) {

                for (var j = 0; j < results.length; j++) {   //loop for tr                
                    layout += "<tr class='tr_data'>";
                    //layout += "<td class='p-0 freezecol assessment-first-col'>  <div class='row'> <div class='col-lg-4 col-md-4 col-sm-4 col-4 pl-3'><div class='user-action nopad'><img class='stud-grid-profile' src='" + results[j].STU_PHOTO + "'/></div></div>";
                    //layout += "<div class='col-lg-8 col-md-8 col-sm-8 col-8 pl-2'><div class='row'> <div class='col-lg-12 col-12 pt-2'><h6>" + results[j].STU_NAME + "</h6></div><div class='col-lg-12 col-12 pt-2'><p class='text-bold'>" + results[j].STU_NO + "</p></div></div></div></div>";

                    layout += "<td class='p-0 freezecol assessment-first-col bg-white'>"
                    layout += "<div class='col-lg-12 col-md-12 col-12'>"; // Student Information Loop Start
                    layout += "<div class='bg-white p-2 d-flex align-items-start h-100'>";
                    layout += "<div class='student-img mr-2'>  <img src='" + results[j].STU_PHOTO + "' alt='" + results[j].STU_NAME + "' title='" + results[j].STU_NAME + "' /></div>";
                    layout += "<div class='d-flex justify-content-between w-100 student-details'>";
                    layout += "<div>";
                    layout += "<h4 class='mt-1'>" + results[j].STU_NAME + " <span class='stdnt-id sub-heading d-block'>ID: " + results[j].STU_NO + "</span></h4>";
                    layout += "</div>";
                    layout += "</div>";
                    layout += "</div>";
                    layout += "</div>";
                    layout += "<div class='table-seperator'>&nbsp;</div>";
                    layout += "</td>";


                    //PLACE OPTIONAL COLUMN CODES HERE
                    for (var i = 0; i < OptionalHeaderResults.length; i++) {
                        layout += "<td class='text-center p-2 td_optional'> "
                        for (var k = 0; k < OptionalDataResults.length; k++) {
                            if (results[j].STU_ID == OptionalDataResults[k].STU_ID) {
                                if (OptionalHeaderResults[i].AOM_ID == OptionalDataResults[k].AOM_ID) {
                                    layout += OptionalDataResults[k].OPTIONAL_VALUE
                                }
                            }
                        }
                        layout += "</td>"
                    }
                    for (var i = 0; i < Headerresult.length; i++) {    //loop for td
                        if (Headerresult[i].RSD_SUB_DESC == '') {
                            header = Headerresult[i].RSD_HEADER;
                        }
                        else {
                            header = Headerresult[i].RSD_SUB_DESC;
                        }
                        if (Headerresult[i].IS_PREV) {
                            layout += "<td class='text-center p-2 td_Prev'><div class='form-group focused m-0'>";
                        }
                        else if (Headerresult[i].RSD_bLock) {
                            layout += "<td class='text-center p-2 td_Locked'><div class='form-group focused m-0'>";
                        }
                        else {
                            layout += "<td class='text-center p-2 td_data'><div class='form-group focused m-0'>";
                        }

                        layout += "<input type='hidden' class='hf_stu_id' value='" + results[j].STU_ID + "'>";
                        layout += "<input type='hidden' class='hf_rsd_id' value='" + Headerresult[i].RSD_ID + "'>";
                        if (Headerresult[i].RSD_RESULT === "C") {

                            if (Headerresult[i].CSSCLASS.toLowerCase() === "textboxmulti") {
                                var headerId = Headerresult[i].IS_PREV ? "Prv_" + Headerresult[i].RSD_HEADER + "_" + Headerresult[i].RSM_ID : Headerresult[i].RSD_HEADER + "_" + Headerresult[i].RSM_ID;
                                if (assess_data.length == 0) {
                                    layout += "<textarea cols='0' rows='0' data-header=" + headerId + " data-stuid=" + results[j].STU_ID + " id=txt_" + results[j].STU_NO + "_" + headerId + " class='form-control form-control-alternative custom-textarea hf_comments' spellcheck='true'></textarea>";
                                }
                                else {
                                    var already_created = 0;
                                    for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                        if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //   --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                            if (already_created != 1) {
                                                if (assess_data[k].COMMENTS != "" || assess_data[k].COMMENTS != null) {
                                                    layout += "<textarea  rows='0' data-header=" + headerId + " data-stuid=" + results[j].STU_ID + " id=txt_" + results[j].STU_NO + "_" + headerId + " class='form-control form-control-alternative custom-textarea hf_comments' spellcheck='true'>" + assess_data[k].COMMENTS + "</textarea>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                    for (var k = 0; k < assess_data.length; k++) { //for filling empty text boxes
                                        //if (!$('#toggler').prop('checked')) {
                                        if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {
                                                if (already_created != 1) {
                                                    layout += "<textarea cols='0' rows='0' data-header=" + headerId + " data-stuid=" + results[j].STU_ID + " id=txt_" + results[j].STU_NO + "_" + headerId + " class='form-control form-control-alternative custom-textarea hf_comments' spellcheck='true'></textarea>";
                                                    already_created = 1;
                                                }
                                            }
                                        }

                                        //For students in data list without the header data
                                        if (results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (!Headerresult[i].RSD_ID.includes(assess_data[k].RSD_ID)) {
                                                if (already_created != 1) {
                                                    layout += "<textarea cols='0' rows='0' data-header=" + headerId + " data-stuid=" + results[j].STU_ID + " id=txt_" + results[j].STU_NO + "_" + headerId + " class='form-control form-control-alternative custom-textarea hf_comments' spellcheck='true'></textarea>";
                                                    already_created = 1;
                                                }
                                            }
                                        } else {
                                            if (already_created != 1) {
                                                layout += "<textarea cols='0' rows='0' data-header=" + headerId + " data-stuid=" + results[j].STU_ID + " id=txt_" + results[j].STU_NO + "_" + headerId + " class='form-control form-control-alternative custom-textarea hf_comments' spellcheck='true'></textarea>";
                                                already_created = 1;
                                            }
                                        }

                                        //For students in data list without the header data
                                        //modified by syed 20JAN2020: previously outside of for loop
                                        if (results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (!Headerresult[i].RSD_ID.includes(assess_data[k].RSD_ID)) {
                                                if (already_created != 1) {
                                                    layout += "<textarea cols='0' rows='0' data-header=" + headerId + " data-stuid=" + results[j].STU_ID + " id=txt_" + results[j].STU_NO + "_" + headerId + " class='form-control form-control-alternative custom-textarea hf_comments' spellcheck='true'></textarea>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }

                                }
                                layout += "  <a href='javascript:void(0)' data-header=" + headerId + " onclick='OpenComments(" + results[j].STU_NO + ",this)'><i class='fa fa-eye text-muted' style='position: absolute;'></i></a>";
                                if (!Headerresult[i].IS_PREV) {
                                    layout += "  <a  href='javascript:void(0)' data-header=" + headerId + " onclick='OpenCommentModal(" + results[j].STU_NO + ",this)'><img src='/Content/img/SIMS/document-edit.png' class='img-document-entry' /></a>";
                                }
                            }
                            else if (Headerresult[i].CSSCLASS.toLowerCase() === "textboxsmall") {
                                if (assess_data.length == 0) {
                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_comments'>";
                                }
                                //else {
                                //    for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                //        if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                //            layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_comments' value='" + assess_data[k].COMMENTS + "'>";
                                //        }
                                //    }
                                //}
                                else {
                                    var already_created = 0;
                                    for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                        if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //   --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                            if (already_created != 1) {
                                                if (assess_data[k].COMMENTS != "" || assess_data[k].COMMENTS != null) {
                                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_comments' value='" + assess_data[k].COMMENTS + "'>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                    for (var k = 0; k < assess_data.length; k++) { //for filling empty text boxes
                                        //if (!$('#toggler').prop('checked')) {
                                        if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {
                                                if (already_created != 1) {
                                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_comments' value=''>";
                                                    already_created = 1;
                                                }
                                            }
                                        }

                                        //For students in data list without the header data
                                        if (results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (!Headerresult[i].RSD_ID.includes(assess_data[k].RSD_ID)) {
                                                if (already_created != 1) {
                                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_comments' value=''>";
                                                    already_created = 1;
                                                }
                                            }
                                        } else {
                                            if (already_created != 1) {
                                                layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_comments' value=''>";
                                                already_created = 1;
                                            }
                                        }

                                        //For students in data list without the header data
                                        if (results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (!Headerresult[i].RSD_ID.includes(assess_data[k].RSD_ID)) {
                                                if (already_created != 1) {
                                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_comments' value=''>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        } //COMMENTS



                        else if (Headerresult[i].RSD_RESULT === "D") {  //DROPDOWN                            
                            if (Headerresult[i].RSP_DESCR === null) {
                                layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_comments disabled' placeholder='No Dropdown Assigned' spellcheck='true' >";
                            }
                            else {
                                if (assess_data.length == 0) {
                                    layout += "<select class='form-control form-control-alternative table-select hf_comments' data_selected='' type='dropdown'>";
                                    layout += Headerresult[i].RSP_DESCR;
                                    layout += "</select>";
                                }
                                else {
                                    var already_created = 0;

                                    for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                        if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                            if (already_created != 1) {
                                                if (assess_data[k].COMMENTS != "" || assess_data[k].COMMENTS != null) {
                                                    layout += "<select class='form-control form-control-alternative table-select hf_comments' data_selected='" + assess_data[k].COMMENTS + "' type='dropdown'>";
                                                    layout += Headerresult[i].RSP_DESCR;
                                                    layout += "</select>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                    for (var k = 0; k < assess_data.length; k++) { //for filling empty dropdown                                       
                                        if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {
                                                if (already_created != 1) {
                                                    layout += "<select class='form-control form-control-alternative table-select hf_comments' data_selected='' type='dropdown'>";
                                                    layout += Headerresult[i].RSP_DESCR;
                                                    layout += "</select>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                        //For students not in data list
                                        if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (already_created != 1) {
                                                layout += "<select class='form-control form-control-alternative table-select hf_comments' data_selected='' type='dropdown'>";
                                                layout += Headerresult[i].RSP_DESCR;
                                                layout += "</select>";
                                                already_created = 1;
                                            }
                                        }
                                        //For students in data list without the header data
                                        if (results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (!Headerresult[i].RSD_ID.includes(assess_data[k].RSD_ID)) {
                                                if (already_created != 1) {
                                                    layout += "<select class='form-control form-control-alternative table-select hf_comments' data_selected='' type='dropdown'>";
                                                    layout += Headerresult[i].RSP_DESCR;
                                                    layout += "</select>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (Headerresult[i].RSD_RESULT === "L") {
                            if (assess_data.length == 0) {
                                layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark' disabled>";
                            }
                            else {
                                var already_created = 0;
                                for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                    if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                        if (already_created != 1) {
                                            if (assess_data[k].MARK != "" || assess_data[k].MARK != null) {
                                                layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark' disabled value='" + assess_data[k].MARK + "'>";
                                                already_created = 1;
                                            }
                                        }
                                    }
                                }
                                for (var k = 0; k < assess_data.length; k++) { //for filling empty text boxes                                   
                                    if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                        if (Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {
                                            if (already_created != 1) {
                                                layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_mark' disabled>";
                                                already_created = 1;
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        else if (Headerresult[i].RSD_RESULT === "G") {
                            for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data                            
                                if (assess_data.length == 0) {
                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_grading'>";
                                }
                                else {
                                    var already_created = 0;
                                    for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                        if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                            if (already_created != 1) {
                                                if (assess_data[k].GRADING != "" || assess_data[k].GRADING != null) {
                                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_grading' value='" + assess_data[k].GRADING + "'>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                    for (var k = 0; k < assess_data.length; k++) { //for filling empty text boxes                                        
                                        if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {
                                                if (already_created != 1) {
                                                    layout += "<input type='text' id='term-input' class='form-control form-control-alternative hf_grading'>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            if (assess_data.length == 0) {
                                layout += "<input type='text' id='term-input' data-min='" + Headerresult[i].VALIDATION_MIN + "' data-max='" + Headerresult[i].VALIDATION_MAX + "' class='form-control form-control-alternative hf_mark " + Headerresult[i].VALIDATION_TYPE + "'>";
                            }
                            for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data                             
                                if (assess_data.length == 0) {
                                    layout += "<input type='text' id='term-input' data-min='" + Headerresult[i].VALIDATION_MIN + "' data-max='" + Headerresult[i].VALIDATION_MAX + "' class='form-control form-control-alternative hf_mark  " + Headerresult[i].VALIDATION_TYPE + "'>";
                                }
                                else {
                                    var already_created = 0;
                                    for (var k = 0; k < assess_data.length; k++) {    //loop for getting the data
                                        if (results[j].STU_ID == assess_data[k].STU_ID && Headerresult[i].RSD_ID == assess_data[k].RSD_ID && Headerresult[i].RSD_HEADER == assess_data[k].RSD_HEADER) {  //  && header == assess_data[k].RSD_HEADER --NEEDS TO BE HERE BUT FOR DEV PURPOSE COMMENTING
                                            if (already_created != 1) {
                                                if (assess_data[k].MARK != "" || assess_data[k].MARK != null) {
                                                    layout += "<input type='text' id='term-input' data-min='" + Headerresult[i].VALIDATION_MIN + "' data-max='" + Headerresult[i].VALIDATION_MAX + "' class='form-control form-control-alternative hf_mark  " + Headerresult[i].VALIDATION_TYPE + "' value='" + assess_data[k].MARK + "'>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                    for (var k = 0; k < assess_data.length; k++) { //for filling empty text boxes                                        
                                        if (!results[j].STU_ID.includes(assess_data[k].STU_ID)) {
                                            if (Headerresult[i].RSD_ID == assess_data[k].RSD_ID) {
                                                if (already_created != 1) {
                                                    layout += "<input type='text' id='term-input' data-min='" + Headerresult[i].VALIDATION_MIN + "' data-max='" + Headerresult[i].VALIDATION_MAX + "' class='form-control form-control-alternative hf_mark  " + Headerresult[i].VALIDATION_TYPE + "'>";
                                                    already_created = 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        layout += "</div></td>";
                    }
                    layout += "</tr>";
                }
                $('#LoadAssessment_B_tbody').html(layout);
            }
            else {
                $('#LoadAssessment_H_tr').html("");
                $('#divrowbtnsave').hide();
                globalFunctions.showMessage('warning', "No Data Available");
            }
            //===================BODY END=====================================
            SetDropdownValues();
            $('.td_Prev').find(":input").prop("disabled", true); //FOR BLOCKING ENTRIES FOR PREVIOUS SCHEDULES
            $("td.td_Locked input, td.td_Locked select, td.td_Locked textarea").prop('disabled', true); //FOR BLOCKING ENTRIES FOR LOCKED HEADERS.
            $('[data-toggle="tooltip"]').tooltip();
            $('#Datagrid').slideToggle('slow');
            $('.Searchmore').removeClass('d-none');
            $('#loadGrid').removeClass('d-none');
            $("#toggler").parent().addClass('d-none');
        },
        error: function (error) {
            //   globalFunctions.onFailure();
        }
    });
    return true;
}

function SetDropdownValues() {
    $(".table-select").each(function () {
        if ($(this).attr('data_selected') != "") {
            $(this).val($(this).attr('data_selected'));
        }
    });
}
function IsReportPublish(RSM_ID, RPF_ID, ACD_ID, GRD_ID, SCT_ID, TRM_ID) {
    $.get("/Assessment/Assessment/IsReportPublish", { RSM_ID, RPF_ID, ACD_ID, GRD_ID, SCT_ID, TRM_ID }, function (response) {
        if (response) {
            $("#divBtnSave").addClass('d-none');
            $("#errorMsgLockEntry").removeClass('d-none');
        }
        else {
            $("#divBtnSave").removeClass('d-none');
            $("#errorMsgLockEntry").addClass('d-none');
        }
    });
}

function ValidateAssessmentData(e) {
    if ($('#LoadAssessment_B_tbody').find('input[iv="IV"]').length > 0) {
        globalFunctions.showWarningMessage(translatedResources.invalidEntry);
        e.abort();
    }
}

function SaveAssessmentData() {

    var RST_ID = "0";
    var STU_ID = "";
    var RPF_ID = $('#ReportSchedule').val();
    var RSD_ID = "";
    var ACD_ID = $('#AcademicYearDDL').val();
    var GRD_ID = $('#Grades').val();
    var RSS_ID = $('#ReportSchedule').val();
    var SGR_ID = $('#SubjectGroups').val() == "" || $('#SubjectGroups').val() == null ? 0 : $('#SubjectGroups').val();
    var SBG_ID = $('#Subjects').val() == "" || $('#Subjects').val() == null ? 0 : $('#Subjects').val();
    var SCT_ID = $('#Section').val() == "" || $('#Section').val() == null ? 0 : $('#Section').val();
    var TYPE_LEVEL = "";
    if ($('#formTutor-tab').hasClass('active')) {
        TYPE_LEVEL = "Core";
    }
    var MARK = "";
    var COMMENTS = "";
    var GRADING = "";
    var data_xml = "<DATA>";
    $('.tr_data td.elm_edit ').each(function () {
        STU_ID = $(this).find(".hf_stu_id").val();
        RSD_ID = $(this).find(".hf_rsd_id").val();
        MARK = $(this).find(".hf_mark").val() == 'undefined' ? 'undefined' : $(this).find(".hf_mark").val();
        COMMENTS = $(this).find(".hf_comments").val() == 'undefined' ? 'undefined' : $(this).find(".hf_comments").val();
        GRADING = $(this).find(".hf_grading").val() == 'undefined' ? 'undefined' : $(this).find(".hf_grading").val();
        if (MARK != 'undefined' && COMMENTS != "") {
            data_xml += "<STUDENT>";
            data_xml += "<RST_ID>" + RST_ID + "</RST_ID>";
            data_xml += "<RPF_ID>" + RPF_ID + "</RPF_ID>";
            data_xml += "<RSD_ID>" + RSD_ID + "</RSD_ID>";
            data_xml += "<ACD_ID>" + ACD_ID + "</ACD_ID>";
            data_xml += "<GRD_ID>" + GRD_ID + "</GRD_ID>";
            data_xml += "<STU_ID>" + STU_ID + "</STU_ID>";
            data_xml += "<RSS_ID>" + RSS_ID + "</RSS_ID>";
            data_xml += "<SGR_ID>" + SGR_ID + "</SGR_ID>";
            data_xml += "<SBG_ID>" + SBG_ID + "</SBG_ID>";
            data_xml += "<SCT_ID>" + SCT_ID + "</SCT_ID>";
            data_xml += "<TYPE_LEVEL>" + TYPE_LEVEL + "</TYPE_LEVEL>";
            data_xml += "<MARK>" + MARK + "</MARK>";
            data_xml += "<COMMENTS>" + COMMENTS + "</COMMENTS>";
            data_xml += "<GRADING>" + GRADING + "</GRADING>";
            data_xml += "</STUDENT>";
        }

    });
    data_xml += "</DATA>";
    $("#hf_Student_XML").val(data_xml);
}

/*-------------------------------------------ASSESSMENT COMMENT SECTION--------------------------------------------------*/
var Stu_No = 0;
var studentId = 0;
var dataheader = "";

function OpenCommentModal(studentNo, header) {
    if (Stu_No == studentNo) {
        $('#tblassessmentComment tbody tr td input[type="checkbox"]').each(function () {
            $(this).prop('checked', false);
        });
    }
    else {
        $('#ddlassessmentCategory').val("");
        $('#divJtableComments').empty();
    }
    Stu_No = studentNo;
    dataheader = $(header).attr('data-header');

    var txtvalue = $('#txt_' + Stu_No + '_' + dataheader);
    studentId = $('#txt_' + Stu_No + '_' + dataheader).attr('data-stuid');

    $('.selectCategory').modal('show');
}

function SelectComments(cmdid) {
    var Comments = $('#td_' + cmdid).html().trim();
    if ($('#chk_' + cmdid).is(":checked")) {
        CommentsToAdd.push(Comments);
    }
    else {
        var index = CommentsToAdd.indexOf(Comments);
        if (index > -1) {
            CommentsToAdd.splice(index, 1);
        }
    }
}

function OpenComments(studentNo, header) {
    var headerval = $(header).attr('data-header');
    var values = $('#txt_' + studentNo + '_' + headerval).val();
    $('#prgComments').html(values);
    $('.commentbox').modal('show');
}

function AddComments() {
    var Comments
    if (globalFunctions.isValueValid($('#commentBoxid').val()))
        Comments = $('#' + $('#commentBoxid').val()).val();
    else
        Comments = $('#txt_' + Stu_No + '_' + dataheader).val();

    for (var i = 0; i < CommentsToAdd.length; i++) {
        Comments = Comments + CommentsToAdd[i].trim();
    }
    if (globalFunctions.isValueValid($('#commentBoxid').val())) {
        $('#' + $('#commentBoxid').val()).val(Comments);
        studentId = undefined;
    }
    else
        $('#txt_' + Stu_No + '_' + dataheader).val(Comments);
    globalFunctions.showSuccessMessage("Comments Added");
    CommentsToAdd.length = 0;
    $('.selectCategory').modal('hide');
    $('#commentBoxid').val('');
}

$(function () {
    $('#tdSection').hide();
    $('#formTutor-tab,#gradeEntry-tab,#reportWriting-tab').click(function () {
        if ($(this).attr("id") == 'gradeEntry-tab') {
            $('#tdSubject').show();
            $('#tdSubjectGroup').show();
            $('#tdSection').hide();
            $('#Grades').val(0);
            $('#Section').val(0);
            $('.selectpicker').selectpicker('refresh');
            $('#divoptionalheaders').show();
            $('#LoadAssessment_H_tr').html("");
            $('#LoadAssessment_B_tbody').html("");
            $('#divrowbtnsave').hide();

        } else if ($(this).attr("id") == 'formTutor-tab') {
            $('#tdSubject').hide();
            $('#tdSubjectGroup').hide();
            $('#tdSection').show();
            $('#Subjects').val(0);
            $('#SubjectGroups').val(0);
            $('#Grades').val(0);
            $('.selectpicker').selectpicker('refresh');
            $('#divoptionalheaders').hide();
            $('#PreviousReportSchedule').empty();
            $('#OptionalHeaders').empty();
            $('#LoadAssessment_H_tr').html("");
            $('#LoadAssessment_B_tbody').html("");
            $('#divrowbtnsave').hide();
        }
        else if ($(this).attr("id") == 'reportWriting-tab') {
            if ($('#reportWritingDiv').css('display') != 'none' && !$('#reportWriting-tab').hasClass('active'))
                $('#aHrefBacktoClassList').click();
        }
        if (!$('#toggleGridsearch').hasClass("d-none"))
            $('#toggleGridsearch').click();
    })

});

$(function () {
    $(document).on('keyup', '.Int', function () {
        var regex = smsCommon.IntegerRegex;
        var IsValidInput = regex.test(this.value);
        var IsInRange = parseInt($(this).data('min')) <= parseInt(this.value) && parseInt($(this).data('max')) >= parseInt(this.value);
        $(this)[IsValidInput && IsInRange ? 'removeClass' : 'addClass']('border-danger')
        $(this).closest('div').find('span.field-validation-error').remove();
        if (!(IsInRange)) {
            $(this).closest('div').append(`<span class="field-validation-error">Range between ${parseInt($(this).data('min'))} - ${parseInt($(this).data('max'))}</span>`);
        }
        if (!(IsValidInput && IsInRange)) {
            $(this).attr('IV', 'IV');
        } else {
            $(this).removeAttr('IV');
        }
    });
    $(document).on('keyup', '.Uns', function () {
        var regex = smsCommon.UnsignedintegerRegex;
        var IsValidInput = regex.test(this.value);
        var IsInRange = parseInt($(this).data('min')) <= parseInt(this.value) && parseInt($(this).data('max')) >= parseInt(this.value);
        $(this)[IsValidInput && IsInRange ? 'removeClass' : 'addClass']('border-danger')
        $(this).closest('div').find('span.field-validation-error').remove();
        if (!(IsInRange)) {
            $(this).closest('div').append(`<span class="field-validation-error">Range between ${parseInt($(this).data('min'))} - ${parseInt($(this).data('max'))}</span>`);
        }
        if (!(IsValidInput && IsInRange)) {
            $(this).attr('IV', 'IV');
        } else {
            $(this).removeAttr('IV');
        }
    });
    $(document).on('keyup', '.Dec', function () {
        var regex = smsCommon.DecimalRegex;
        var IsValidInput = regex.test(this.value);
        var IsInRange = parseFloat($(this).data('min')) <= parseFloat(this.value) && parseFloat($(this).data('max')) >= parseFloat(this.value);
        $(this)[IsValidInput && IsInRange ? 'removeClass' : 'addClass']('border-danger')
        $(this).closest('div').find('span.field-validation-error').remove();
        if (!(IsInRange)) {
            $(this).closest('div').append(`<span class="field-validation-error">Range between ${$(this).data('min')} - ${$(this).data('max')}</span>`);
        }
        if (!(IsValidInput && IsInRange)) {
            $(this).attr('IV', 'IV');
        } else {
            $(this).removeAttr('IV');
        }
    });
})