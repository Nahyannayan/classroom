﻿
var GradeBook = function () {
    var Init = function () {
        $('.selectpicker').selectpicker('refresh');
        $("#ddlGradeBookAcademicYear").trigger('change');
    },
        showGradeBookEntry = (isVisible) => {
            if (isVisible) {
                $('.gradeBookFilter:visible').slideToggle('slow')
                $('.gradeBookEntry:hidden').slideToggle('slow')
            } else {
                if (GradeBookEntry.GradeBookEntryList.length > 0 || GradeBookEntry.GradeBookEntrySubHeader.length > 0) {
                    smsCommon.notyDeleteConfirm(translatedResources.UnloadConfirmMsg, () => {
                        $('.gradeBookFilter:hidden').slideToggle('slow')
                        $('.gradeBookEntry:visible').slideToggle('slow')
                    });
                } else {
                    $('.gradeBookFilter:hidden').slideToggle('slow')
                    $('.gradeBookEntry:visible').slideToggle('slow')
                }
            }
        },
        ViewStudents = function () {
            var GRD_ID = $('#ddlGradeBookGrade').val();
            var ACD_ID = $('#ddlGradeBookAcademicYear').val();
            var SGR_ID = $('#ddlGradeBookSubjectGroup').val();
            var SCT_ID = '0';
            var TRM_ID = $('#ddlGradeBookTerm').val();
            var prv = $('#ddlGradeBookEntryPreviousReportSchedule').val().join('|')
            var AOD_IDs = $('#ddlGradeBookEntryPreviousOptionalHeaders').val().join("|");
            var rsm_id = $('#ddlGradeBookReportHeader').val();
            var SBG_ID = globalFunctions.isValueValid($('#ddlGradeBookSubject').val()) ? $('#ddlGradeBookSubject').val() : 0;
            var RPF_ID = globalFunctions.isValueValid($('#ddlGradeBookReportSchedule').val()) ? $('#ddlGradeBookReportSchedule').val() : 0;
            var selectedOptions = [];
            if (globalFunctions.isValueValid($('#ddlGradeBookAcademicYear').val()))
                selectedOptions.push($('#ddlGradeBookAcademicYear option:selected').text());

            if (globalFunctions.isValueValid($('#ddlGradeBookTerm').val()))
                selectedOptions.push($('#ddlGradeBookTerm option:selected').text());

            if (globalFunctions.isValueValid($('#ddlGradeBookReportHeader').val()))
                selectedOptions.push($('#ddlGradeBookReportHeader option:selected').text());

            if (globalFunctions.isValueValid($('#ddlGradeBookGrade').val()))
                selectedOptions.push($('#ddlGradeBookGrade option:selected').text());

            if (globalFunctions.isValueValid($('#ddlGradeBookSubject').val()))
                selectedOptions.push($('#ddlGradeBookSubject option:selected').text());

            if (globalFunctions.isValueValid($('#ddlGradeBookSubjectGroup').val()))
                selectedOptions.push($('#ddlGradeBookSubjectGroup option:selected').text());

            if (globalFunctions.isValueValid($('#ddlGradeBookReportSchedule').val()))
                selectedOptions.push($('#ddlGradeBookReportSchedule option:selected').text());

            if (globalFunctions.isValueValid($('#ddlGradeBookEntryPreviousReportSchedule').val()))
                selectedOptions.push($('#ddlGradeBookEntryPreviousReportSchedule').val().map(x => $('#ddlGradeBookEntryPreviousReportSchedule option[value="' + x + '"]').text()).join(','));

            if (globalFunctions.isValueValid($('#ddlGradeBookEntryPreviousOptionalHeaders').val()))
                selectedOptions.push($('#ddlGradeBookEntryPreviousOptionalHeaders').val().map(x => $('#ddlGradeBookEntryPreviousOptionalHeaders option[value="' + x + '"]').text()).join(','));
            $.post('/Assessment/Assessment/GetGradeBookEntryForm',
                {
                    GBM_GRD_ID: GRD_ID,
                    GBM_ACD_ID: ACD_ID,
                    GBM_SGR_ID: SGR_ID,
                    GBM_SCT_ID: SCT_ID,
                    GBM_RSM_ID: rsm_id,
                    GBM_AOD_IDs: AOD_IDs,
                    GBM_RPF_ID: RPF_ID,
                    GBM_SBG_ID: SBG_ID,
                    GBM_TRM_ID: TRM_ID,
                    GBM_PRV_SCHEDULE: prv,
                    SelectedOptions: selectedOptions
                })
                .then((response) => {
                    if ($('#DivGradeBookEntry').html(response).find('div.noData').length > 0) {
                        globalFunctions.showWarningMessage(translatedResources.noData);
                    } else {
                        $('.selectpicker').selectpicker('refresh');
                        GradeBookEntry.GradeBookEntrySubHeader = [];
                        GradeBookEntry.GradeBookEntryList = [];
                        setTimeout(() => GradeBook.calculateWidth(), 800);
                        showGradeBookEntry(true);
                    }
                })
                .catch(error => globalFunctions.showMessage(error.NotificationType, error.Message))

        },
        calculateWidth = () => {
            let count = $('[class*="exam-header"]').length;
            eh = 0;
            console.log("count -> " + count);
            for (var i = 1; i <= count; i++) {
                //var txt = eh;
                //var str = eh + i;
                var str = $('.exam-header-' + i).outerWidth();
                $('.exam-' + i).outerWidth(str);
                $('.exam-' + i).parent().each((e, a) => $(a).outerWidth(str));
                //console.log(str);
            }
            count = $('[class*="subheader-"]').length
            for (var i = 1; i <= count; i++) {
                //var txt = eh;
                //var str = eh + i;
                var str = $('.subheader-' + i).outerWidth();
                $('.subHeader-data-' + i).outerWidth(str);
                //$('.subHeader-data-' + i).parent().each((e, a) => $(a).width(str));
            }
        }
    return {
        Init,
        showGradeBookEntry,
        ViewStudents,
        calculateWidth
    }
}();

//----------------------------Event Handlers------------------------------------------
$(document).ready(function () {
    GradeBook.Init();
});

$(document).on('change', "#ddlGradeBookAcademicYear", function (event) {
    var result = smsCommon.bindMultipleDropDownByParameter(
        ["#ddlGradeBookTerm", "#ddlGradeBookReportHeader"],
        '/Assessment/Assessment/LoadGradBookTopFilter',
        { ACD_ID: $(this).val(), DropdownId: 'AcademicYear' },
        ["", ""],
        ['Select', 'Select'],
        true
    );
    $("#ddlGradeBookReportHeader").trigger('change');
});

$(document).on('change', "#ddlGradeBookReportHeader", function (event) {
    var result = smsCommon.bindMultipleDropDownByParameter(
        ["#ddlGradeBookGrade", "#ddlGradeBookReportSchedule"],
        '/Assessment/Assessment/LoadGradBookTopFilter',
        { ACD_ID: $("#ddlGradeBookAcademicYear").val(), RSM_ID: $(this).val(), DropdownId: 'ReportHeader' },
        ["", ""],
        ['Select', 'Select'],
        true
    );
    $("#ddlGradeBookGrade").trigger('change');
});

$(document).on('change', "#ddlGradeBookGrade", function (event) {
    var result = smsCommon.bindMultipleDropDownByParameter(
        ["#ddlGradeBookSubject"],
        '/Assessment/Assessment/LoadGradBookTopFilter',
        { ACD_ID: $("#ddlGradeBookAcademicYear").val(), GRD_ID: $(this).val(), DropdownId: 'Grade' },
        [""],
        ['Select'],
        true
    );
    $("#ddlGradeBookSubject").trigger('change');
    //------------------------Populate Addition Column drop-down-------------------------------
    $.get('/Assessment/Assessment/GetAssessmentOptionalLists', { ACD_ID: $("#ddlGradeBookAcademicYear").val(), GRD_ID: $(this).val() })
        .then(response => {
            smsCommon.bindDropDown('#ddlGradeBookEntryPreviousReportSchedule', response.PreviousResult, 'DISPLAY_TEXT', 'RPF_ID', '', 'Select')
            smsCommon.bindDropDown('#ddlGradeBookEntryPreviousOptionalHeaders', response.OptionResult, 'AOM_DESCR', 'AOM_ID', '', 'Select')
        })
        .catch(error => globalFunctions.showMessage(error));
      
    //-----------------------------------------------------------------------------------------
});

$(document).on('change', "#ddlGradeBookSubject", function (event) {
    var result = smsCommon.bindMultipleDropDownByParameter(
        ["#ddlGradeBookSubjectGroup"],
        '/Assessment/Assessment/LoadGradBookTopFilter',
        { ACD_ID: $("#ddlGradeBookAcademicYear").val(), GRD_ID: $("#ddlGradeBookGrade").val(), SBG_ID: $(this).val(), DropdownId: 'Subject' },
        [""],
        ['Select'],
        true
    );
   
});
var value = $("#strtt_id").val();
if (value == "0" ? 0 : value)
if (value != 0) {
    LoadSubjectGroupsBYSubjectGroup(value)
}
function LoadSubjectGroupsBYSubjectGroup(id) {
    //Sectionsval ='@(ViewBag.strtt_id)'
    try {
        var url = "/Assessment/Assessment/GetHeaderBySubjectCategory";
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: url,
            contentType: 'application/json;charset=utf-8',
            data: { id: id },
            success: function (Sections) {
                if (Sections.length > 0) {
                    var Data = Sections[0].SGR_GRD_ID; //dynamically generated 
                    var arrGrdSec = [];
                    //alert(Data);
                    $.each(Data.split(/\|/), function (i, val) {
                        //alert(val);
                        arrGrdSec.push(val);
                    })
                    //var GRD_ID = $('#ddlGradeBookGrade').val();
                    //var ACD_ID = $('#ddlGradeBookAcademicYear').val();
                    //var SGR_ID = $('#ddlGradeBookSubjectGroup').val();
                    var GRD_ID = arrGrdSec[0] == "" || arrGrdSec[0] == null ? $('#ddlGradeBookGrade').val() : arrGrdSec[0];
                    var ACD_ID = Sections[0].SGR_ACD_ID == "" || Sections[0].SGR_ACD_ID == null ? $('#ddlGradeBookAcademicYear').val() : Sections[0].SGR_ACD_ID;
                    var SGR_ID = $('#ddlGradeBookSubjectGroup').val();//Sections[0].SGR_SBG_ID == "" || Sections[0].SGR_SBG_ID == null ? $('#ddlGradeBookSubjectGroup').val() : Sections[0].SGR_SBG_ID;

                    var SCT_ID = '0';
                    var TRM_ID = $('#ddlGradeBookTerm').val();
                    var prv = $('#ddlGradeBookEntryPreviousReportSchedule').val().join('|')
                    var AOD_IDs = $('#ddlGradeBookEntryPreviousOptionalHeaders').val().join("|");
                    var rsm_id = $('#ddlGradeBookReportHeader').val();
                    var SBG_ID = globalFunctions.isValueValid(Sections[0].SGR_SBG_ID == "" || Sections[0].SGR_SBG_ID == null) ? $('#ddlGradeBookSubject').val() : Sections[0].SGR_SBG_ID;
                    var RPF_ID = globalFunctions.isValueValid($('#ddlGradeBookReportSchedule').val()) ? $('#ddlGradeBookReportSchedule').val() : 0;
                    $.post('/Assessment/Assessment/GetGradeBookEntryForm',
                        {
                            GBM_GRD_ID: GRD_ID,
                            GBM_ACD_ID: ACD_ID,
                            GBM_SGR_ID: SGR_ID,
                            GBM_SCT_ID: SCT_ID,
                            GBM_RSM_ID: rsm_id,
                            GBM_AOD_IDs: AOD_IDs,
                            GBM_RPF_ID: RPF_ID,
                            GBM_SBG_ID: SBG_ID,
                            GBM_TRM_ID: TRM_ID,
                            GBM_PRV_SCHEDULE: prv
                        })
                        .then((response) => {
                            if ($('#DivGradeBookEntry').html(response).find('div.noData').length > 0) {
                                globalFunctions.showWarningMessage(translatedResources.noData);
                            } else {
                                $('.selectpicker').selectpicker('refresh');
                                GradeBookEntry.GradeBookEntrySubHeader = [];
                                GradeBookEntry.GradeBookEntryList = [];
                                setTimeout(() => GradeBook.calculateWidth(), 800);
                                GradeBook.showGradeBookEntry(true);
                                
                            }
                        })
                        .catch(error => globalFunctions.showMessage(error.NotificationType, error.Message))

                }


                //ViewStudents();
            },
            error: function (ex) {
                alert('Failed.' + ex);
            }
        });
        return false;
    } catch (e) {
        alert(e);
    }
}
//----------------------------Grade Book Entry Handlers-------------------------------
$(document).on('click', '.plusBtn', function () {
    var stuId = $(this).data('stu-id');
    var rsdId = $(this).data('rsd-id');
    var gbmId = $(this).data('gbm-id');

    var object = GradeBookEntry.StudentList.find(e => e.STU_ID == stuId);
    $('#stuname').html(object.STU_NAME)

    GradeBookEntry.setStudentIndexForBackForward(object);

    if (globalFunctions.isValueValid(gbmId)) {
        object = GradeBookEntry.HeaderList.find(e => e.GBM_ID == gbmId)
    } else {
        object = GradeBookEntry.HeaderList.find(e => e.RSD_ID == rsdId);
    }
    $('#rsdName').html(object.IsGBM ? object.GBM_DESCR : object.RSD_SUB_DESC)
    GradeBookEntry.setReportIndexForBackForward(object);
    GradeBookEntry.hideAndShowControls(object);    
});

//------------- To navigate from student list from side drawer -----------------------
$(document).on('click', '.stuUp , .stuDown', function () {
    var index = $(this).data('index');
    if (GradeBookEntry.StudentList.some(e => e.Index == index))
        GradeBookEntry.populateStudentName(index);
});

//------------- To navigate from report list from side drawer ------------------------
$(document).on('click', '.hedUp , .hedDown', function () {
    var index = $(this).data('index');
    if (GradeBookEntry.HeaderList.some(e => e.Index == index))
        GradeBookEntry.populateReportName(index);
});

//------------- To Load default comments table in model pop-up ------------------------
$(document).on('change', '#ddlassessmentCategory', function () {
    var value = $(this).val();
    var studentId = $('#CommentStudentId').val();
    $.get('/Assessment/Assessment/GetCommentByCategoryId', { CAT_ID: value, STU_ID: studentId }, function (response) {
        $('#divJtableComments').html(response);
        $('#tblassessmentComment tbody tr td input[type="checkbox"]').attr('onclick', '');
        $('#btnaddComments').attr('onclick', '');
        //$("#divJtableComments").mCustomScrollbar({
        //    axis: 'x',
        //    autoHideScrollbar: true,
        //    autoExpandScrollbar: true
        //});
    });
});

$(document).on('click', '#tblassessmentComment tbody tr td input[type="checkbox"]', function () {
    var Comments = $('#td_' + this.id.split('_')[1]).html().trim();
    if ($(`#${this.id}`).is(":checked")) {
        GradeBookEntry.CommentsToAdd.push(Comments);
    }
    else {
        var index = GradeBookEntry.CommentsToAdd.indexOf(Comments);
        if (index > -1) {
            GradeBookEntry.CommentsToAdd.splice(index, 1);
        }
    }
});

$(document).on('click', '#btnaddComments', function () {
    var Comments = $('#MarkComments').val();

    if (GradeBookEntry.CommentsToAdd.length == 0) {
        globalFunctions.showWarningMessage(translatedResources.noDataToSave);
        return;
    }

    for (var i = 0; i < GradeBookEntry.CommentsToAdd.length; i++) {
        Comments = `${Comments} ${GradeBookEntry.CommentsToAdd[i].trim()}`;
    }
    $('#MarkComments').val(Comments);
    $('label[for="MarkComments"]').addClass('active');
    globalFunctions.showSuccessMessage(translatedResources.addSuccessMsg);
    GradeBookEntry.CommentsToAdd = [];
    GradeBookEntry.SaveGradeBookEntry();
    $('.selectCategory').modal('hide');
});

//------------- To navigate from student and header list using -----------------------
//------------- keyboard short cut keys using shift + (navigating key)----------------
function KeyPress(e) {

    var evtobj = window.event ? event : e

    if ((evtobj.keyCode == 38 || evtobj.keyCode == 40 || evtobj.keyCode == 37 || evtobj.keyCode == 39) && evtobj.shiftKey) {
        //Check if current record is absent marked then avoid auto save when 
        //navigating using keys
        //if (!(GradeBookEntry.GradeBookEntryList.some(x => x.RSD_ID == $('#gradeEntryHiddenHeaderId').val() && x.STU_ID == $('#gradeEntryHiddenStudentId').val() && x.bABSENT))) {
        //    GradeBookEntry.SaveGradeBookEntry();
        //}
        var headerId = $('#gradeEntryHiddenHeaderId').val();
        var headerObject = GradeBookEntry.HeaderList.find(x => (x.RSD_ID == headerId || x.GBM_ID == headerId));
        if (headerObject) {
            if (headerObject.GBM_ENTRY_MODE == 'D' || headerObject.RSD_RESULT == 'D') {
                if (globalFunctions.isValueValid($('#EntryDdl').val()))
                    $('#EntryDdl').trigger('change');
            } else if (headerObject.GBM_ENTRY_MODE == 'M' || headerObject.RSD_RESULT == 'M') {
                if (globalFunctions.isValueValid($('#examMarks').val()))
                    $('#examMarks').trigger('change');
            } else if (headerObject.RSD_RESULT == 'C') {
                if (globalFunctions.isValueValid($('#MarkComments').val()))
                    $('#MarkComments').trigger('change');
            }
        }
    }

    if (evtobj.keyCode == 38 && evtobj.shiftKey/*evtobj.ctrlKey*/) {
        //var index = parseInt($('#stuname').data('index')) - 1;
        //if (GradeBookEntry.StudentList.some(e => e.Index == index))
        //    GradeBookEntry.populateStudentName(index);
        $('.stuUp').trigger('click');
    } else if (evtobj.keyCode == 40 && evtobj.shiftKey/*evtobj.ctrlKey*/) {
        //var index = parseInt($('#stuname').data('index')) + 1;
        //if (GradeBookEntry.StudentList.some(e => e.Index == index))
        //    GradeBookEntry.populateStudentName(index);
        $('.stuDown').trigger('click');
    } else if (evtobj.keyCode == 37 && evtobj.shiftKey/*evtobj.ctrlKey*/) {
        //var index = parseInt($('#rsdName').data('index')) - 1;
        //if (GradeBookEntry.HeaderList.some(e => e.Index == index))
        //    GradeBookEntry.populateReportName(index);
        $('.hedUp').trigger('click');
    } else if (evtobj.keyCode == 39 && evtobj.shiftKey/*evtobj.ctrlKey*/) {
        //var index = parseInt($('#rsdName').data('index')) + 1;
        //if (GradeBookEntry.HeaderList.some(e => e.Index == index))
        //    GradeBookEntry.populateReportName(index);
        $('.hedDown').trigger('click');
    }
    if ((evtobj.keyCode == 38 || evtobj.keyCode == 40 || evtobj.keyCode == 37 || evtobj.keyCode == 39) && evtobj.shiftKey) {
        GradeBookEntry.populateDataFromEntry();
    }

}
document.onkeydown = KeyPress;
//------------------------------------Key Up Event For Marks Validation ---------------------------------
$(document).on('keyup', '#examMarks', function () {
    var regex = '';
    var msg = '';
    var isValuePresent = globalFunctions.isValueValid(this.value);
    if ($(this).data('type') == 'Int') {
        regex = smsCommon.IntegerRegex;
        msg = translatedResources.IntegerMsg;
    }
    else if ($(this).data('type') == 'Uns') {
        regex = smsCommon.UnsignedintegerRegex;
        msg = translatedResources.UnsignedIntMsg;
    }
    else if ($(this).data('type') == 'Dec') {
        regex = smsCommon.DecimalRegex;
        msg = translatedResources.DecimalMsg;
    }
    var IsValidInput = regex.test(this.value);
    var IsInRange;

    if (($(this).data('type') == 'Dec')) {
        IsInRange = parseFloat($(this).data('min')) <= parseFloat(this.value) && parseFloat($(this).data('max')) >= parseFloat(this.value)
    } else {
        IsInRange = parseInt($(this).data('min')) <= parseInt(this.value) && parseInt($(this).data('max')) >= parseInt(this.value);
    }
    smsCommon.removeValidationMessage(this);
    $(this)[(IsValidInput && IsInRange) || !isValuePresent ? 'removeClass' : 'addClass']('border-danger')
    if (!(IsValidInput) && isValuePresent) {        
        smsCommon.showValidationMessage(this, msg);
    }
    //$('#errorMsg')[IsInRange ? 'hide' : 'show']();
    if (!(IsInRange) && isValuePresent) {
        smsCommon.showValidationMessage(this, translatedResources.MinMarksGreaterThenMaxMarks.replace('{0}', $(this).data('min')).replace('{1}', $(this).data('max')));
        //$('#errorMsg').html();
    }
    if (!(IsValidInput && IsInRange) && isValuePresent) {
        $(this).attr('IV', 'IV');
    } else {
        $(this).removeAttr('IV');
    }
});
//-------------------------------------------------------------------------------------------------------

window.addEventListener("beforeunload", function (event) {
    if (GradeBookEntry.GradeBookEntryList.length > 0 || GradeBookEntry.GradeBookEntrySubHeader.length > 0) {
        event.preventDefault(); // If you prevent default behavior in Mozilla Firefox prompt will be allways shown
        event.returnValue = translatedResources.UnloadConfirmMsg;
        // Chrome requires returnValue to be set
    }
});