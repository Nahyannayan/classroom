﻿var mrkAttendance = undefined;
var Sectionsval = 0;
$(document).ready(function () {
    //setDropDownValueAndTriggerChange('AcademicYear');
    LoadTerms();
    LoadAssessmentActivity();
    function LoadTerms() {
        $("#Term").empty();
        try {
            var url = "/Assessment/Assessment/FetchTerms";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYearDDL").val() },
                success: function (Sections) {
                    $("#Term").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Term").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Term');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }
    }
    $("#AcademicYearDDL").change(function () {
        $("#Grades").empty();
        $("#Subjects").empty();
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();
        LoadTerms();
        LoadAssessmentActivity();

    });
    //-----------------------------------------
    // LOADING Grades
    function LoadGrades() {
        $("#Grades").empty();
        try {
            var url = "/Assessment/Assessment/GetGradesAccess";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYearDDL").val(), rsm_id: '-1' },
                success: function (Sections) {
                    $("#Grades").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Grades").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Grades');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    }
    $("#Term").change(function () {
        $("#Subjects").empty();
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();
        LoadGrades();
    });
    //-----------------------------------------
    // LOADING Subjects
    function LoadSubjects() {
        $("#Subjects").empty();
        var grade = $("#Grades").val().split("|")[0];
        try {
            var url = "/Assessment/Assessment/GetSubjectsByGrade";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { acd_id: $("#AcademicYearDDL").val(), grd_id: grade },
                success: function (Sections) {
                    $("#Subjects").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#Subjects").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('Subjects');
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }
    }
    $("#Grades").change(function () {
        $("#SubjectGroups").empty();
        $("#SubjectCategory").empty();
        LoadSubjects();
    });
    //-----------------------------------------
    // LOADING Subject Groups
    function LoadSubjectGroups() {
        $("#SubjectGroups").empty();
        var grade = $("#Grades").val().split("|")[0];
        try {
            var url = "/Assessment/Assessment/GetSubjectGroupBySubject";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { sbg_id: $("#Subjects").val(), grd_id: grade },
                success: function (Sections) {
                    $("#SubjectGroups").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#SubjectGroups").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('SubjectGroups');
                    //if ($('#SubjectGroups')[0].options.length > 1) {
                    //    ViewStudents();
                    //}
                    var value = $("#strtt_id").val();
                    if (value == "0" ? 0 : value)
                    if (value != null) {
                        LoadSubjectGroupsBYSubjectGroup(value)
                    }
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    }
    $("#Subjects").change(function () {
        LoadSubjectGroups();
    });

    //-----------------------------------------
    // LOADING LoadAssessmentActivity
    function LoadAssessmentActivity() {
        $("#AssessmentActivity").empty();
        try {
            var url = "/Assessment/Assessment/FetchAssessmentActivity";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: {},
                success: function (Sections) {
                    $("#AssessmentActivity").append('<option value="0">Select</option>');
                    $.each(Sections, function (i, Section) {
                        $("#AssessmentActivity").append('<option value="'
                            + Section.Value + '">'
                            + Section.Text + '</option>');
                    });
                    $('.selectpicker').selectpicker('refresh');
                    setDropDownValueAndTriggerChange('AssessmentActivity');
                   
                    //if ($('#SubjectGroups')[0].options.length > 1) {
                    //    ViewStudents();
                    //}
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });

            return false;
        } catch (e) {
            alert(e);
        }

    }
    function LoadSubjectGroupsBYSubjectGroup() {
        Sectionsval = $("#SubjectGroups").val();
        //alert($("#SubjectGroups").val())
        try {
            var url = "/Assessment/Assessment/GetHeaderBySubjectCategory";
            $.ajax({
                type: 'GET',
                dataType: "json",
                url: url,
                contentType: 'application/json;charset=utf-8',
                data: { id: Sectionsval },
                success: function (Sections) {
                    if (Sections.length > 0) {
                        //if (Sections[0].SGR_ACD_ID != 0 || Sections[0].SGR_ACD_ID != null) {
                        //    $("#AcademicYearDDL").val(Sections[0].SGR_ACD_ID);
                        //}
                        ////if (Sections[0].SGR_GRD_ID != 0 || Sections[0].SGR_GRD_ID != null) {
                        ////    $("#Grades").val(Sections[0].SGR_GRD_ID);
                        ////}
                        //if (Sections[0].SGR_SBG_ID != 0 || Sections[0].SGR_SBG_ID != null) {
                        //    $("#Subjects").val(Sections[0].SGR_SBG_ID);
                        //}
                        var Data = Sections[0].SGR_GRD_ID; //dynamically generated 
                        var arrGrdSec = [];
                        //alert(Data);
                        $.each(Data.split(/\|/), function (i, val) {
                            //alert(val);
                            arrGrdSec.push(val);
                        })


                        var _GRD_ID = arrGrdSec[0] == "" || arrGrdSec[0] == null ? $('#Grades').val() : arrGrdSec[0];
                        var _ACD_ID = Sections[0].SGR_ACD_ID == "" || Sections[0].SGR_ACD_ID == null ? $('#AcademicYearDDL').val() : Sections[0].SGR_ACD_ID;
                        var _SBG_ID = Sections[0].SGR_SBG_ID == "" || Sections[0].SGR_SBG_ID == null ? $('#Subjects').val() : Sections[0].SGR_SBG_ID;
                        var _TRM_ID = $('#Term').val();
                        var _SGR_ID = $('#SubjectGroups').val();
                        var _CAM_ID = $('#AssessmentActivity').val();
                        var _TRM_ID = $('#Term').val();
                        var section = arrGrdSec[1];
                        $.get("/Assessment/Assessment/ViewListMArkEntry", { ACD_ID: _ACD_ID, CAM_ID: _CAM_ID, GRD_ID: _GRD_ID, STM_ID: section, TRM_ID: _TRM_ID, SGR_ID: _SGR_ID, SBG_ID: _SBG_ID }, function (response) {

                            $('#divMarkEntry').hide();
                            $('#divViewMarkEntry').html(response);

                            $('#divViewMarkEntry').show();
                            $('#divMarkEntryAOL').hide();

                            $("#strtt_id").val('');

                        });
                    }
                    else {
                        $.get("/Assessment/Assessment/ViewListMArkEntry", { ACD_ID: _ACD_ID, CAM_ID: _CAM_ID, GRD_ID: _GRD_ID, STM_ID: section, TRM_ID: _TRM_ID, SGR_ID: _SGR_ID, SBG_ID: _SBG_ID }, function (response) {

                            $('#divMarkEntry').hide();
                            $('#divViewMarkEntry').html(response);

                            $('#divViewMarkEntry').show();
                            $('#divMarkEntryAOL').hide();

                            $("#strtt_id").val('');

                        });

                    }
                    //setDropDownValueAndTriggerChange('SubjectGroups');
                    //ViewStudents();
                },
                error: function (ex) {
                    alert('Failed.' + ex);
                }
            });
            return false;
        } catch (e) {
            alert(e);
        }
    }
    
});
function datatableInit() {

    if (!$.fn.dataTable.isDataTable('#example')) {
        $('#example').DataTable({
            "paging": false,
            //"ordering": false,
            "columnDefs": [
                {
                    "width": "8%",
                    "targets": 0,
                    "orderable": false
                },
                {
                    "width": "25%",
                    "targets": 1,
                    "orderable": true
                },
                {
                    "targets": "no-sort",
                    "orderable": false
                }
            ]
        });
    }

}

function ViewStudents() {
    if (validate_Dropdowns($('#AcademicYearDDL')) && validate_Dropdowns($('#Term')) && validate_Dropdowns($('#Grades')) && validate_Dropdowns($('#Subjects')) && validate_Dropdowns($('#SubjectGroups'))) {
        
        var Data = $('#Grades').val(); //dynamically generated 
        var arrGrdSec = [];
        //alert(Data);
        $.each(Data.split(/\|/), function (i, val) {
            //alert(val);
            arrGrdSec.push(val);
        })
        var _GRD_ID = arrGrdSec[0];
        var _ACD_ID = $('#AcademicYearDDL').val();
        var _SBG_ID = $('#Subjects').val();
        var _TRM_ID = $('#Term').val();
        var _SGR_ID = $('#SubjectGroups').val();
        var _CAM_ID = $('#AssessmentActivity').val();
        var _TRM_ID = $('#Term').val();
        var section = arrGrdSec[1];
        $.get("/Assessment/Assessment/ViewListMArkEntry", { ACD_ID: _ACD_ID, CAM_ID: _CAM_ID, GRD_ID: _GRD_ID, STM_ID: section, TRM_ID: _TRM_ID, SGR_ID: _SGR_ID, SBG_ID: _SBG_ID }, function (response) {

            $('#divMarkEntry').hide();
            $('#divViewMarkEntry').html(response);

            $('#divViewMarkEntry').show();
            $('#divMarkEntryAOL').hide();



        });

        // LoadHeaders(_GRD_ID, _ACD_ID, _SBG_ID, _TRM_ID, _SGR_ID, _CAM_ID);
    }
}

function BacktoView() {
    $('#divViewMarkEntry').show();
    $('#divMarkEntryAOL').hide();
}

function MarkAsAOLEXAM(marks) {
    var IsAOLEXAM = $(marks).attr('data-isAOLExam');
    var CAS_ID = $(marks).attr('data-CASId');
    var Isbwithoutskill = $(marks).attr('data-bwithoutskill');
    var slabId = $(marks).attr('data-slabId');
    var EntryType = $(marks).attr('data-entrytype');
    var minMarks=  $(marks).attr("data-minMarks");
    var maxMarks=  $(marks).attr("data-maxMarks");
    if (IsAOLEXAM == true) {


        $.get('/Assessment/Assessment/GetMarkEntryAOL', { IsAOLEXAM: IsAOLEXAM, CAS_ID: CAS_ID, Isbwithoutskill: Isbwithoutskill }, function (response) {
            $('#divViewMarkEntry').hide();
            $('#divMarkEntryAOL').html(response);
            $('#divMarkEntryAOL').show();

        });

    }
    else {

        $.get('/Assessment/Assessment/GetMarkEntryAOL', { IsAOLEXAM: IsAOLEXAM, CAS_ID: CAS_ID, Isbwithoutskill: Isbwithoutskill, SlabId: slabId, Type: EntryType, minMarks: minMarks, maxMarks: maxMarks }, function (response) {
            $('#divViewMarkEntry').hide();
            $('#divMarkEntryAOL').html(response);
            $('#divMarkEntryAOL').show();

        });

    }
   

}

function MarkAttendance(marks) {
    mrkAttendance = marks;
    var CAS_DESC = $(marks).attr('data-description');
    var IsAOLEXAM = $(marks).attr('data-isAOLExam');
    var CAS_ID = $(marks).attr('data-CASId');
    var Isbwithoutskill = $(marks).attr('data-bwithoutskill');
    var slabId = $(marks).attr('data-slabId');
    var EntryType = $(marks).attr('data-entrytype');
    var minMarks = $(marks).attr("data-minMarks");
    var maxMarks = $(marks).attr("data-maxMarks");
    if (IsAOLEXAM == true) {


        $.get('/Assessment/Assessment/MarkAttendanceDetails', { IsAOLEXAM: IsAOLEXAM, CAS_ID: CAS_ID, Isbwithoutskill: Isbwithoutskill, CAS_DESC: CAS_DESC  }, function (response) {
            $('#divViewMarkEntry').hide();
            $('#divMarkEntryAOL').html(response);
            $('#divMarkEntryAOL').show();
            $('.clsddlMarkAttendance').selectpicker('refresh')
           // $('#tblmarkattendance').dataTable();
            if ($.fn.DataTable.isDataTable($('#tblmarkattendance'))) {
                $('#tblmarkattendance').dataTable().destroy();
            }
            $('#tblmarkattendance').dataTable(
                {
                    "bPaginate": false,
                    "ordering": false,
                    "bInfo": false
                });
        });

    }
    else {

        $.get('/Assessment/Assessment/MarkAttendanceDetails', { IsAOLEXAM: IsAOLEXAM, CAS_ID: CAS_ID, Isbwithoutskill: Isbwithoutskill, SlabId: slabId, Type: EntryType, minMarks: minMarks, maxMarks: maxMarks, CAS_DESC: CAS_DESC }, function (response) {
            $('#divViewMarkEntry').hide();
            $('#divMarkEntryAOL').html(response);
            $('#divMarkEntryAOL').show();
            $('.clsddlMarkAttendance').selectpicker('refresh');

            if ($.fn.DataTable.isDataTable($('#tblmarkattendance'))) {
                $('#tblmarkattendance').dataTable().destroy();
            }
            $('#tblmarkattendance').dataTable(
                {
                    "bPaginate": false,
                    "ordering": false,
                    "bInfo": false
                });
         
        });

    }

   


}
function SaveMarkAttendance() {
    var markAttendance = [];
    var casId = $('#btnSaveMarkAttendance').attr('data-casid')
    $('.trModelDetails').each(function () {

        var casId = $(this).attr('data-casid');
        var staId = $(this).attr('data-staid');
        var stuId = $(this).attr('data-stuId');
        var dropdownvalue = $('#ddlAttendance_'+stuId).val();


        var objectAttendance = {

            bATTENDED: dropdownvalue,
            CAS_ID: casId,
            STA_ID: staId,
            STU_ID: stuId

        }

        markAttendance.push(objectAttendance);

    });

    $.post('/Assessment/Assessment/SaveMarkAttendance', { markAttendance: markAttendance, CAS_ID: casId }, function (response) {
        globalFunctions.showMessage(response.NotificationType, response.Message);
        if (response.Success) {
            MarkAsAOLEXAM(mrkAttendance);
        }
        //BacktoView();
        console.log(response);
    });




}
function gettextboxChange(skillSetVal) {
    
    var SkillMarks = $(skillSetVal).val();
    var SkillName = $(skillSetVal).attr('data-stu_skillname');
    var Stuno = $(skillSetVal).attr('data-stu_no');

    SKILL_MARK.push(SkillMarks);
    SKILL_NAME.push(SkillName);
    STU_NO.push(Stuno);




}


/*-----------------------------------------------SAVE FOR MARK AOL EXAM------------------------*/
function SaveMarkAsAOLEXAM() {

    var markEntryAOLData = [];//to pass as modl object to actionresult

    var count = $('#hdnSkillCount').val();//to get the skill count for looping
    var Isbwithoutskill = $('#hdnisbwithoutskill').val();
    var CAS_ID = $('#hdnCAS_ID').val();
    $('#tblMarkEntryAOL tbody tr').each(function () {

        var StuNo = parseInt($(this).find(".clstdstuno").html().trim());
        var StuName = $(this).find(".clsstuname").html().trim();
        var stuSkill = [];
        var j = 0;
        $(this).find(".clsstuSkillInput").each(function () {
            j++;
            var stuskill_name = $(this).attr('data-SkillName');
            stuSkill.push(stuskill_name);



        });

        for (var i = 0; i <= count; i++) {
            var SkillMarks = $('#txt_' + StuNo + '_' + stuSkill[i]).val();
            var SkillName = stuSkill[i];
            var stuId = $('#txt_' + StuNo + '_' + stuSkill[i]).attr('data-stu_Id');
            var Sta_Id = $('#txt_' + StuNo + '_' + stuSkill[i]).attr('data-sta_Id');
            var skillOrder = $('#txt_' + StuNo + '_' + stuSkill[i]).attr('data-skillOrder');
            if (SkillMarks != undefined || SkillMarks != null) {
                var markEntry = {
                    STA_ID: Sta_Id,
                    STU_ID: stuId,
                    STU_NO: StuNo,
                    STU_NAME: StuName,
                    SKILL_NAME: SkillName,
                    SKILL_MARK: SkillMarks,
                    SKILL_GRADE: "",
                    SKILL_MAX_MARK: 0.0,
                    MARKS: 0.0,
                    STA_GRADE: "",
                    IS_ENABLED: "",
                    WITHOUTSKILLS_MARKS: "",
                    WITHOUTSKILLS_GRADE: "",
                    WITHOUTSKILLS_MAXMARKS: 0,
                    CHAPTER: "",
                    FEEDBACK: "",
                    WOT: "",
                    TARGET: "",
                    bATTENDED: false,
                    STU_STATUS: "",
                    SKILL_ORDER: skillOrder
                }

                markEntryAOLData.push(markEntry);
            }


        }

    });


    $.post('/Assessment/Assessment/SaveMarkEntryList', { markEntryAOLData: markEntryAOLData, Isbwithoutskill: Isbwithoutskill, CAS_ID: CAS_ID }, function (response) {
        globalFunctions.showMessage(response.NotificationType, response.Message);
        $('#divViewMarkEntry').show();
        $('#divMarkEntryAOL').hide();
    });



}

/*-----------------------------------------------SAVE FOR MARK ENTRY DATA------------------------*/

function SaveMarkEntryData() {
    
    var hdnSlabId = $('#hdnSlabId').val();//slab id
    var hdnEntryType = $('#hdnEntryType').val();//entry type
    var hdnCAS_ID = $('#hdnCAS_ID').val();//CAS_ID
    var hdnMinMarks = $('#hdnMinMax').val();
    var hdnMaxMarks = $('#hdnMaxMax').val();
    var objListofMarkEntryData = [];
    $('#tblMarkEntryData tbody tr').each(function () {
        var StuNo = parseInt($(this).find(".clstdstuno").html().trim());
        var StuName = $(this).find(".clsstuname").html().trim();
        var STA_ID = $('#txt_' + StuNo).attr('data-sta_Id');
        var STU_ID = $('#txt_' + StuNo).attr('data-stu_Id');
        var MARKS = $('#txt_' + StuNo).val();
        var MARK_GRADE = $(this).find('.clsstumarkgrade').attr('data-markgrade');
        var MIN_MARK = hdnMinMarks;
        var MAX_MARK = hdnMaxMarks;
        var IS_ENABLED = $('#txt_' + StuNo).attr('data-ISEnabled');
        var STU_STATUS = $('#txt_' + StuNo).attr('data-Stu_Status');
        var objMarkEntryData = {

            STA_ID: STA_ID,
            STU_ID: STU_ID,
            STU_NO: StuNo,
            STU_NAME: StuName,
            MARKS: MARKS,
            MARK_GRADE: MARK_GRADE,
            MIN_MARK: MIN_MARK,
            MAX_MARK: MAX_MARK,
            IS_ENABLED: IS_ENABLED,
            STU_STATUS: STU_STATUS



        }
        objListofMarkEntryData.push(objMarkEntryData);

    });
    //POST LIST OF MARKS
    $.post('/Assessment/Assessment/SaveMarkEntryData', { objListofMarkEntryData: objListofMarkEntryData, slabId: hdnSlabId, entryType: hdnEntryType, CAS_ID: hdnCAS_ID }, function (response) {
        globalFunctions.showMessage(response.NotificationType, response.Message);
        $('#divViewMarkEntry').show();
        $('#divMarkEntryAOL').hide();
    });
}