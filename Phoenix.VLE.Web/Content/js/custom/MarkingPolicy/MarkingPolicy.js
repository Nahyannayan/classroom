﻿/// <reference path="../../common/global.js" />
/// <reference path="../../common/plugins.js" />


var formElements = new FormElements();
var popoverEnabler = new LanguageTextEditPopover();
var MarkingPolicy = function () {
    var init = function () {
        loadMarkingPolicyGrid();
        $("#tbl-MarkingPolicyList_filter").hide();
    },
        addMarkingPolicy = function (source, MarkingPolicyId) {
            loadCreatePopup(source, translatedResources.add, MarkingPolicyId);
        },
        editMarkingPolicy = function (source, MarkingPolicyId) {
            loadCreatePopup(source, translatedResources.edit, MarkingPolicyId);
        },
        loadCreatePopup = function (source, mode, id) {
            var title = mode + ' ' + translatedResources.MarkingPolicy;
            globalFunctions.loadPopup(source, '/SchoolInfo/MarkingPolicy/AddEditMarkingPolicy?MarkingPolicyId=' + id, title, 'addtask-dialog modal-lg');
            $(source).off("modalLoaded");
            $('#formModified').val(true);
           
           
            $(source).on("modalLoaded", function () {
                $(".colorpicker").minicolors({
                    theme: 'bootstrap',
                    control: 'hue',
                    format: 'hex'
                });
                formElements.feSelect();
                popoverEnabler.attachPopover();
                $('select').selectpicker({
                    noneSelectedText: "Select"
                });
                if ($('#IsAddMode').val() == "true" || $('#IsAddMode').val() == "True") {
                    $('#Weight').val("");
                }
            });
        },
        onSaveMarkingPolicySuccess = function (isAddmode) {          
           // MarkingPolicy.init();
            if (isAddmode == 'True') {
                globalFunctions.showSuccessMessage(translatedResources.AddSuccess);
            } else {
                globalFunctions.showSuccessMessage(translatedResources.EditSuccess);
            }
            $("#myModal").modal('hide');
            MarkingPolicy.init();
        },
        onSaveMarkingPolicyError = function (isAddmode) {
            init();
            if (isAddmode == 'True') {
                globalFunctions.showErrorMessage(translatedResources.AddError);
            } else {
                globalFunctions.showErrorMessage(translatedResources.EditError);
            }
            $("#myModal").modal('hide');
        }
        loadMarkingPolicyGrid = function () {
            var _columnData = [];
            _columnData.push(
                { "mData": "Symbol", "sTitle": translatedResources.Symbol, "sWidth": "15%", "sClass": "wordbreak" },
                { "mData": "Description", "sTitle": translatedResources.Description, "sWidth": "40%", "sClass": "wordbreak" },
                { "mData": "Weight", "sTitle": translatedResources.Weight, "sWidth": "15%" },
                { "mData": "ColorCode", "sClass": "text-center", "sTitle": translatedResources.ColorCode, "sWidth": "15%" },
                { "mData": "Actions", "sClass": "text-center", "sTitle": translatedResources.actions, "sWidth": "15%" }
            );
            initMarkingPolicyGrid('tbl-MarkingPolicyList', _columnData, '/SchoolInfo/MarkingPolicy/LoadMarkingPolicys');
        },
        initMarkingPolicyGrid = function (_tableId, _columnData, _url) {
            var grid = new DynamicGrid(_tableId);
            var settings = {
                columnData: _columnData,
                url: _url,
                columnDefs: [{
                    'orderable': false,
                    'targets': 2
                }],
                columnSelector: false
            };
            grid.init(settings);
        },
        deleteMarkingPolicy = function (source,id) {
            if (globalFunctions.isValueValid(id)) {
                globalFunctions.deleteItem('MarkingPolicy', '/SchoolInfo/MarkingPolicy/DeleteMarkingPolicy', 'tbl-MarkingPolicyList', source, id);
            }
        }
    

    return {
        init: init,
        addMarkingPolicy: addMarkingPolicy,
        loadCreatePopup: loadCreatePopup,
        onSaveMarkingPolicySuccess: onSaveMarkingPolicySuccess,
        onSaveMarkingPolicyError: onSaveMarkingPolicyError,
        loadMarkingPolicyGrid: loadMarkingPolicyGrid,
        editMarkingPolicy: editMarkingPolicy,
        deleteMarkingPolicy: deleteMarkingPolicy

    };
}();
$(document).ready(function () {
    MarkingPolicy.init();
    $(document).on('click', '#btnAddMarkingPolicy', function () {
        MarkingPolicy.addMarkingPolicy($(this));
    });
    $("#MarkingPolicySearch").on("input", function (e) {
        e.preventDefault();
        $('#tbl-MarkingPolicyList').DataTable().search($(this).val()).draw();

    });
});