﻿
var parent = function () {
    var init = function () {
        loadSchoolGroupData($("#other-groups"));
        //comment the below code for new dashboard
        loadAssignments($(".assignment-count-content"));
    },
        getStudentDetails = function (studentId) {

            $.ajax({
                type: 'POST',
                url: '/Parent/SetCurrentSelectedStudent',
                data: { studentId: studentId },
                success: function (result) {
                    if (result) {
                        window.location.reload();//.href = "/Parent/Dashboard";                        
                    }
                },
                error: function (msg) {
                }
            });
        },
        selectChild = function () {
            $('#AjaxLoader').show();
            $("#selectChild").load('/Parent/LoadChildList', function () {
                $('#AjaxLoader').hide();
                $('#modalSelectChild').one('shown.bs.modal', function () {
                });
            });
            $('#modalSelectChild').modal('show');
        },
        loadSchoolGroupData = function (source) {
            loadPartials(source, "/Parent/GetSchoolGroupWithPagination?pageIndex=1");
            $(source).off("partialLoaded");
            $(source).on("partialLoaded", function () {
                applyCarousel(source, { navigation: true, margin: 15, items: 1, slideBy: 1 });
                enableCarouselNav(source);
                $(source).on("click", ".owl-next", function () {
                    var carouselIndex = $(source).data("owl.carousel")._current + 1;
                    var pageIndex = $(source).find(".item").length;
                    if (pageIndex == carouselIndex) {
                        $.ajax({
                            url: "/Parent/GetSchoolGroupWithPagination?pageIndex=" + (pageIndex + 1),
                            success: function (result) {
                                if (result.trim() !== "") {
                                    source.trigger("add.owl.carousel", [result]).trigger("refresh.owl.carousel");
                                    source.trigger('to.owl.carousel', pageIndex)
                                    enableCarouselNav(source);
                                } else
                                    $(source).find(".owl-next").addClass("disabled");

                                
                            }
                        })
                    }
                });
            });

        },
        saveStudentDetails = function () {

        },
        enableCarouselNav = function (source) {
            $(source).find(".owl-nav, .owl-next").removeClass("disabled");
        },

        applyCarousel = function (source, options) {
            var settings = {
                navigation: false,
                autoplay: false,
                nav: true,
                dots: false,
                loop: false,
                autoplayHoverPause: true,
                rtl: direction
            };
            $(source).owlCarousel(Object.freeze($.extend(settings, options)));
        },
        loadAssignments = function (source) {
            loadPartials(source, "/Parent/GetStudentDashboardAssignments", true)
            $(source).off("partialLoaded");
            $(source).on("partialLoaded", function () {
                applyCarousel($("#assignments_new"), { margin: 0, items: 1, mouseDrag: false, touchDrag: false, pullDrag: false, onInitialized: assgnCounter_New, onTranslated: assgnCounter_New });
                applyCarousel($("#assignments_in-progress"), { margin: 0, items: 1, mouseDrag: false, touchDrag: false, pullDrag: false, onInitialized: assgnCounter_Progress, onTranslated: assgnCounter_Progress });
                applyCarousel($("#assignments_overdue"), { margin: 0, items: 1, mouseDrag: false, touchDrag: false, pullDrag: false, onInitialized: assgnCounter_Overdue, onTranslated: assgnCounter_Overdue });
                applyCarousel($("#assignments_completed"), { margin: 0, items: 1, mouseDrag: false, touchDrag: false, pullDrag: false, onInitialized: assgnCounter_Completed, onTranslated: assgnCounter_Completed });
            });
        },

        loadPartials = function (source, url) {
            $(source).load(url, function () {
                $(source).trigger("partialLoaded");

                //To resolve Ipad group menu issue, Deepak Singh on 11/Nov/2020
                changeGroupAction();
            });
        };

    assgnCounter_Completed = function (event) {
        var element = event.target; // DOM element, the .owl-carousel
        var items = event.item.count; // Number of items
        var item = event.item.index + 1; // Position of the current item

        // it loop is true then reset counter from 1
        if (item > items) {
            item = item - items
        }
        $('#counter-completed').html(translatedResources.DashboardShowing.replace('{0}', item).replace('{1}', items));
    }

    assgnCounter_New = function (event) {
        var element = event.target; // DOM element, the .owl-carousel
        var items = event.item.count; // Number of items
        var item = event.item.index + 1; // Position of the current item

        // it loop is true then reset counter from 1
        if (item > items) {
            item = item - items
        }
        $('#counter-new').html(translatedResources.DashboardShowing.replace('{0}', item).replace('{1}', items));
    }

    assgnCounter_Overdue = function (event) {
        var element = event.target; // DOM element, the .owl-carousel
        var items = event.item.count; // Number of items
        var item = event.item.index + 1; // Position of the current item

        // it loop is true then reset counter from 1
        if (item > items) {
            item = item - items
        }
        $('#counter-overdue').html(translatedResources.DashboardShowing.replace('{0}', item).replace('{1}', items));
    }

    assgnCounter_Progress = function (event) {
        var element = event.target; // DOM element, the .owl-carousel
        var items = event.item.count; // Number of items
        var item = event.item.index + 1; // Position of the current item

        // it loop is true then reset counter from 1
        if (item > items) {
            item = item - items
        }
        $('#counter-pending').html(translatedResources.DashboardShowing.replace('{0}', item).replace('{1}', items));
    }
      
       
    return {
        init: init,
        selectChild: selectChild,
        getStudentDetails: getStudentDetails,
        saveStudentDetails: saveStudentDetails
    };
}();

$(document).ready(function () {
    parent.init();

    var winWidth = $(window).outerWidth();
    var bannerSliderHeight = $("#dashboardBanners .item").outerHeight();
    var studentInfoHeight = bannerSliderHeight - 95;

    //if (($("#dashboardBanners .item").length >= 1) && winWidth >= 768) {
    //    $(".studentDetails").height(bannerSliderHeight);
    //    //$("studentDetails .studentInfo").height(studentInfoHeight);
    //    $(".studentDetails .studentInfo").mCustomScrollbar({
    //        setHeight: studentInfoHeight,
    //        autoHideScrollbar: true,
    //        autoExpandScrollbar: true,
    //        scrollbarPosition: "outside",
    //    })
    //}

    //On click of group header section redirect to group detail page
    $('body').off('click', '.group-header').on('click', '.group-header', function (e) {
        if (e.target !== this)
            return;

        var id = $(this).data('id');
        location.href = "/SchoolStructure/SchoolGroups/GroupDetails?grpId=" + id;
    });

    
});

//To resolve Ipad group menu issue, Deepak Singh on 11/Nov/2020
function changeGroupAction() {
    var winWidth = $(window).outerWidth();
    if (winWidth == 1024) {
        $(".dropleft.group-actions").removeClass("dropleft").addClass("dropright");
    }
}

$(window).resize(function () {
    var winWidth = $(window).outerWidth();
    if (winWidth == 1024) {
        changeGroupAction();
    }
});