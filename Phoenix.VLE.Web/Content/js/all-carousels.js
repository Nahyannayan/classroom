$(function() {
    var pageDirection = $("html").attr("dir");
    if(pageDirection == "rtl") {
        direction = true
    } else {
        direction = false
    }

    $("#dashboardBanners").owlCarousel({
        margin: 0,
        autoplay: true,                
        nav: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });
    
    /* $("#schoolFeed").owlCarousel({
        margin: 0,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    }); */

    $("#circulars").owlCarousel({
        margin: 5,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });

    $("#timeline").owlCarousel({
        margin: 20,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        responsive:{
            0:{
                items: 2,
                slideBy: 2
            },
            768: {
                items: 3,
                slideBy: 3
            },
            1366: {
                items: 5,
                slideBy: 5 
            },
            1920: {
                items: 5,
                slideBy: 5
            }
        }
    });

    $("#exemplarWall").owlCarousel({
        margin: 0,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });
    
    $("#classGroups").owlCarousel({
        margin: 0,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });

    //$("#other-groups").owlCarousel({
    //    margin: 15,
    //    autoplay: false,                
    //    nav: true,
    //    dots: false,
    //    loop: false,
    //    autoplayHoverPause: true,
    //    rtl: direction,
    //    items: 1,
    //    slideBy: 1
    //});

    $("#assignmentCarousel").owlCarousel({
        margin: 0,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });

    $("#new-assignments, #inProgress-assignments, #overdue-assignments, #completed-assignments").owlCarousel({
        margin: 0,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false
    });

    $("#assignment-list").owlCarousel({
        margin: 0,
        autoplay: false,                
        nav: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        responsive:{
            0:{
                items: 1,
                slideBy: 1
            },
            1366: {
                items: 2,
                slideBy: 2 
            }
        }
    });
    
    $("#whatsNewCarousel").owlCarousel({
        margin: 20,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        responsive: {
            0: {
                items: 1,
                slideBy: 1
            },
            768: {
                items: 3,
                slideBy: 3
            },
            1366: {
                items: 4,
                slideBy: 4
            },
            1920: {
                items: 5,
                slideBy: 5
            }
        }
    });

    //$(".todaysScheduleCarousel").owlCarousel({
    //    margin: 20,
    //    autoplay: false,
    //    nav: true,
    //    dots: false,
    //    loop: false,
    //    autoplayHoverPause: true,
    //    rtl: direction,
    //    items: 4,
    //    slideBy: 4
    //});

    $("#studentChatter").owlCarousel({
        margin: 20,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 3,
        slideBy: 3
    });

    //$("#exemplarWallCarousel").owlCarousel({
    //    margin: 20,
    //    autoplay: false,                
    //    nav: true,
    //    dots: false,
    //    loop: false,
    //    autoplayHoverPause: true,
    //    rtl: direction,
    //    items: 4,
    //    slideBy: 4
    //});
    
});