$(function() {
    var pageDirection = $("html").attr("dir");
    if(pageDirection == "rtl") {
        direction = true
    } else {
        direction = false
    }

    $("#dashboardBanners").owlCarousel({
        margin: 0,
        autoplay: true,                
        nav: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });
    
    $("#schoolFeed").owlCarousel({
        margin: 0,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });

    $("#circulars").owlCarousel({
        margin: 5,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });

    $("#todo-list").owlCarousel({
        margin: 5,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false
    });

    $("#timeline").owlCarousel({
        margin: 20,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        responsive:{
            0:{
                items: 2,
                slideBy: 2
            },
            768: {
                items: 3,
                slideBy: 3
            },
            1366: {
                items: 5,
                slideBy: 5 
            },
            1920: {
                items: 5,
                slideBy: 5
            }
        }
    });

    $("#todays-updates").owlCarousel({
        margin: 0,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });

    $("#class-group").owlCarousel({
        margin: 15,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 3,
        slideBy: 3
    });

    $("#class-group-2, #other-groups").owlCarousel({
        margin: 15,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        slideBy: 1
    });
    

    $("#assignmentCarousel").owlCarousel({
        margin: 0,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1
    });
    
    $("#teacherChatter").owlCarousel({
        margin: 20,
        autoplay: true,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 3,
        slideBy: 3
    });
    
    $("#whatsNewCarousel").owlCarousel({
        margin: 20,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 4,
        slideBy: 4
    });

    $("#new-assignments, #inProgress-assignments, #overdue-assignments, #completed-assignments").owlCarousel({
        margin: 0,
        autoplay: false,                
        nav: true,
        dots: false,
        loop: false,
        autoplayHoverPause: true,
        rtl: direction,
        items: 1,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false
    });

    $(".assignment-list").owlCarousel({
        margin: 0,
        autoplay: false,                
        nav: false,
        loop: true,
        autoplayHoverPause: true,
        rtl: direction,
        responsive:{
            0:{
                items: 1,
                slideBy: 1
            },
            1366: {
                items: 2,
                slideBy: 2 
            }
        }
    });    
});