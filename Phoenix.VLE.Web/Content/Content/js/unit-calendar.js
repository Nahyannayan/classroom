var unitCalendar=[];
var unitListJson;
var weekListJson;
var resources = [];
var units = [];
var unitDurations = [];
var months = [];
var academicWeeks = 0;

function generateCalendarData() {
    
    months = getMonthList((unitCalendar.YearStartDate), (unitCalendar.YearEndDate));
    for (var i = 0; i < unitCalendar.UnitDetails.length; i++) {
        var _unit = unitCalendar.UnitDetails[i];
        units.push(new unit(_unit.UnitId, _unit.UnitName,_unit.UnitInfoURL));
        resources.push(new resource(_unit.UnitId, '', _unit.UnitColorCode))
        var _unitDuration = new unitDuration(units[i], resources[i], (_unit.StartDate), (_unit.EndDate));
        unitDurations.push(_unitDuration);
    }
}

function getMonthList(startDate, endDate) {
    var start = startDate.split('-');
    var end = endDate.split('-');
    var startYear = parseInt(start[0]);
    var endYear = parseInt(end[0]);
    var _months = [];

    for (var i = startYear; i <= endYear; i++) {
        var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
        var startMon = i === startYear ? parseInt(start[1]) - 1 : 0;
        for (var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j + 1) {
            var month = j + 1;
            var displayMonth = month < 10 ? '0' + month : month;
            _months.push([i, displayMonth, '01'].join('-'));
        }
    }
    return _months;
}

function resource(id, name, unitDurationColor) {
    this.id = id;
    this.name = name;
    this.unitDurationColor = unitDurationColor;
    //this.unitDurationColor = 'rgba(' + hexToRgb(unitDurationColor).r + ',' + hexToRgb(unitDurationColor).g
    //    + ',' + hexToRgb(unitDurationColor).r+',1)';
}

function unit(id, name,unitInfoURL) {
    this.id = id;
    this.name = name;
    this.unitInfoURL = unitInfoURL;
}

function unitDuration(unit, resource, start_date, end_date) {
    this.unit = unit;
    this.resource = resource;
    this.start_date = start_date;
    this.end_date = end_date;
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? { r: parseInt(result[1], 16), g: parseInt(result[2], 16), b: parseInt(result[3], 16) } : null;
}

// Adds the given number of days to the date
Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

// Returns the array of dates between two dates
function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(new Date(currentDate));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}


// Format two dates to a week string (jan 19 - 25 || jan 26 - feb 1)
function formatDatesToWeekString(start_date, end_date) {
    var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
    var dateString = "";
    var getStartYear = start_date.getFullYear();
    var getEndYear = end_date.getFullYear();
    dateString = getStartYear+" ";
    dateString = dateString + months[start_date.getMonth()] + " ";
    dateString = dateString + start_date.getDate() + " - ";

    

    if (start_date.getMonth() !== end_date.getMonth()) {
        if (getStartYear != getEndYear) {
            dateString = dateString + " " + getEndYear+" ";
        }
        dateString = dateString + months[end_date.getMonth()] + " ";
    }
    
    dateString = dateString + end_date.getDate();
    return dateString;
}

// Gets the string value of the given day
function getStringValueOfDay(day) {
    console.log("day"+day);
    var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    return days[day];
    //if (day === 0) {
    //    console.log(days[6]);
    //    return days[6];
    //} else {
    //    console.log(days[day - 1]);
    //    return days[day - 1];
    //}
    
}

// Gets the string value of the given month
function getStringValueOfMonth(month) {
    var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
    return months[month];
}

// Calculate number of days between two dates.
function calculateDaysBetweenDates(start_date, end_date) {
    //Source: http://stackoverflow.com/questions/3224834/get-difference-between-2-dates-in-javascript
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;

    // Discard the time and time-zone information.
    var utc1 = Date.UTC(start_date.getFullYear(), start_date.getMonth(), start_date.getDate());
    var utc2 = Date.UTC(end_date.getFullYear(), end_date.getMonth(), end_date.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

// Returns an array of resources for the given unit
function getResourcesPerUnit(unit) {
    var unitDurationsAllocation = [];
    for (j = 0; j < unitDurations.length; j++) {
        if (unitDurations[j].unit.id === unit.id) {
            unitDurationsAllocation.push(unitDurations[j]);
        }
    }

    var unitResources = [];
    for (k = 0; k < unitDurationsAllocation.length; k++) {
        var inArray = $.inArray(unitDurationsAllocation[k].resource.name, unitResources);
        if (inArray === -1) {
            unitResources.push(unitDurationsAllocation[k].resource.name);
        }
    }

    return unitResources;
}

// Returns the (style)top value of the unitDuration
function determineUnitDurationTop(unitDuration) {
    var unitResources = getResourcesPerUnit(unitDuration.unit);
    unitResources.sort();
    var index = $.inArray(unitDuration.resource.name, unitResources);
    return (index * 40) + "px";
}

// Returns the row height based on the number of resources per unit
function calculateRowHeight(unit) {
    var unitResources = getResourcesPerUnit(unit);
    var numberOfRows = 0;
    if (unitResources.length === 0) {
        numberOfRows = 1;
    }
    numberOfRows = unitResources.length;
    return (numberOfRows * 40);
}

// Builds the X axis of the schedule (Time axis)
function buildXAxis() {
    
    var weekNo = 1;
    var x_axis_weeks = document.getElementsByClassName('x-axis-weeks')[0];
    var index = 0;
    var monthsarray = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];

    for (var i = 0; i < months.length; i++)
    {
        var yr = new Date(months[i]).getFullYear();
        var mn = new Date(months[i]).getMonth();
        var weekCount = 0;
        for (j2 = 0; j2 < unitCalendar.WeekList.length; j2++) {

            var _Week = unitCalendar.WeekList[j2];//2019-04-01
            
            var _WeekStartDate = convertToDate(_Week.WeekStart);
            var _WeekEndDate = convertToDate(_Week.WeekEnd);
            if (mn == _WeekStartDate.getMonth() && yr == _WeekStartDate.getFullYear()) {
                weekCount += 1;
            }
        }

      
        var x_axis_week = document.createElement('div');
        x_axis_week.setAttribute("class", "x-axis-week");
        x_axis_week.style.width = (weekCount * 40) + "px";
   
        x_axis_week.innerHTML = monthsarray[mn] + " " + yr;
     //   index += 7;
        x_axis_weeks.appendChild(x_axis_week);


        //var firstDayOfWeek = 0;
       

        for (j = 0; j < unitCalendar.WeekList.length; j++) {

            var _Week = unitCalendar.WeekList[j];//2019-04-01
            var _WeekNo = _Week.Description;
            var _WeekId = _Week.Id;
            var _WeekStartDate = convertToDate(_Week.WeekStart);
            var _WeekEndDate = convertToDate(_Week.WeekEnd);
            if (mn == _WeekStartDate.getMonth())
            {
                
                // 2019-04-29 
                //if (_WeekStartDate.getMonth() != _WeekEndDate.getMonth())
                //    break;

                var x_axis_days = document.getElementsByClassName('x-axis-days')[0];

                var x_axis_day = document.createElement('div');
                x_axis_day.setAttribute("class", "x-axis-day");

                x_axis_day.innerHTML = "" + (_WeekId);

                x_axis_days.appendChild(x_axis_day);
            }
        }
        //weekNo = weekNo + j;
    }
}

// Builds the Y Axis of the schedule (Projects or Resources)
function buildYAxis() {
    var yAxis = document.getElementsByClassName('y-axis')[0];
    for (i = 0; i < units.length; i++) {
        var yAxisDiv = document.createElement('div');
        yAxisDiv.setAttribute("class", "y-axis-div");
        yAxisDiv.style.height = calculateRowHeight(units[i]) + 'px';
        yAxisDiv.innerHTML = units[i].unitInfoURL;//"<a href='/course/unit/unitinfo?Id=" + units[i].id + "'>" + units[i].name + "</a>";
        yAxis.appendChild(yAxisDiv);
    }
}

// Build the schedule
function buildUnitCalendarSchedule() {
    
    var unitCalendarSchedule = document.getElementsByClassName('schedule')[0];
    for (i = 0; i < units.length; i++) {
        var gridRow = document.createElement('div');
        gridRow.setAttribute("id", "p" + units[i].id);
        gridRow.setAttribute("class", "grid-row");
        unitCalendarSchedule.appendChild(gridRow);

        //for (i2 = 0; i2 < months.length; i2++)
        //{
        //    var yr = (new Date(months[i2])).getFullYear();
        //    var mn = (new Date(months[i2])).getMonth();

        //    var monthStartDt = moment([yr, mn]);


        //    var firstOfMonth = moment(monthStartDt).startOf('month')
        //    var lastOfMonth = moment(monthStartDt).endOf('month')
        //    var weekInCurrentMonth = moment(lastOfMonth).diff(moment(firstOfMonth), 'week');

        //    var daysInCurrentMonth = getDates(new Date(firstOfMonth), new Date(lastOfMonth)).length;

        //if (daysInCurrentMonth % 7 > 0) {
        //    weekInCurrentMonth += 1;
        //}

            for (i3 = 0; i3 < unitCalendar.WeekList.length; i3++)
            {
                var Week = unitCalendar.WeekList[i3];
                var _WeekStartDate = new Date(Week.WeekStart);
                var gridColumn = document.createElement('div');
            gridColumn.setAttribute("class", "grid-column");
                gridColumn.setAttribute("id", "p" + units[i].id + "-d" + (_WeekStartDate).getDate()
                    + "-m" + ((_WeekStartDate).getMonth() + 1)+"-w"+i3);

                // Sets the color of the background of the grid column
                if ((_WeekStartDate).getMonth() != (_WeekStartDate).addDays(7).getMonth()) {
                gridColumn.style.backgroundColor = '#f7f7f7';
            } else {
                gridColumn.style.backgroundColor = 'rgb(252, 252, 252)';
            }

            gridRow.appendChild(gridColumn);

            var height = calculateRowHeight(units[i]);
            gridColumn.style.height = height + 'px';
            gridRow.style.height = height + 'px';
        }
        //}
    }
}


// Populate the schedule with unitDurations

function populateUnitCalendarSchedule()
{
    console.log("unit duration"+unitDurations.length);
    for (i = 0; i < unitDurations.length; i++) {
        var unitRow = document.getElementById("p" + unitDurations[i].unit.id);
        var unitDuration = document.createElement("div");
        unitDuration.setAttribute("class", "assignment");

        var startday = new Date(months[0]).getDay();
        console.log("startday :" + startday);
        console.log("unit start day :" + (new Date(unitDurations[i].start_date).getDay()));


        var weekInCurrentMonth = moment(unitDurations[i].end_date).diff(moment(unitDurations[i].start_date), 'week');
        unitDuration.style.width = (weekInCurrentMonth * 40) + "px";
        unitDuration.style.height = "10px";
        unitDuration.style.background = unitDurations[i].resource.unitDurationColor;
        var top = determineUnitDurationTop(unitDurations[i]);
        unitDuration.style.top = top;
        var weekInCurrentMonth = 0;
        debugger;
        
        //box 40px * 4
        var weeks = moment(unitDurations[i].start_date).diff(moment(months[0]), 'week');
        unitDuration.style.left = ((Math.abs(weeks)) * 40) + "px";

        unitDuration.innerHTML = unitDurations[i].resource.name;
        unitRow.appendChild(unitDuration);

    }
}
function convertToDate(dt) {
    var parsedDate = new Date(parseInt(dt.substr(6)));
    var jsDate = new Date(parsedDate); //Date object
    return jsDate;
}

//buildYAxis();
//buildUnitCalendarSchedule();
//populateUnitCalendarSchedule();


//function populateUnitCalendarSchedule() {
//    for (i = 0; i < unitDurations.length; i++) {
//        var unitRow = document.getElementById("p" + unitDurations[i].unit.id);
//        var unitDuration = document.createElement("div");
//        unitDuration.setAttribute("class", "assignment");
//        unitDuration.style.width = (getDates(unitDurations[i].start_date, unitDurations[i].end_date).length * 40) + "px";
//        unitDuration.style.background = unitDurations[i].resource.unitDurationColor;
//        var top = determineUnitDurationTop(unitDurations[i]);
//        unitDuration.style.top = top;

//        for (i2 = 0; i2 < dates.length; i2++) {
//            var days = calculateDaysBetweenDates(dates[0], unitDurations[i].start_date);
//            unitDuration.style.left = (days * 40) + "px";
//        }

//        unitDuration.innerHTML = unitDurations[i].resource.name;
//        unitRow.appendChild(unitDuration);
//    }
//}




//// Builds the X axis of the schedule (Time axis)
//function buildXAxis() {
//    var x_axis_weeks = document.getElementsByClassName('x-axis-weeks')[0];
//    var index = 0;
//    while (index < dates.length) {
//        var x_axis_week = document.createElement('div');
//        x_axis_week.setAttribute("class", "x-axis-week");
//        console.log(dates[index]+", "+dates[index+6]);

//        if (dates[index + 6] == undefined)
//            break;
//        x_axis_week.innerHTML = formatDatesToWeekString(dates[index], dates[index + 6]);
//        index += 7;
//        x_axis_weeks.appendChild(x_axis_week);
//    }
//    var x_axis_days = document.getElementsByClassName('x-axis-days')[0];
//    for (i = 0; i < dates.length; i++) {
//        var x_axis_day = document.createElement('div');
//        x_axis_day.setAttribute("class", "x-axis-day");
//        x_axis_day.innerHTML = getStringValueOfDay(dates[i].getDay());
//        x_axis_days.appendChild(x_axis_day);
//    }
//}

//// Builds the Y Axis of the schedule (Projects or Resources)
//function buildYAxis() {
//    var yAxis = document.getElementsByClassName('y-axis')[0];
//    for (i = 0; i < units.length; i++) {
//        var yAxisDiv = document.createElement('div');
//        yAxisDiv.setAttribute("class", "y-axis-div");
//        yAxisDiv.style.height = calculateRowHeight(units[i]) + 'px';
//        yAxisDiv.innerHTML ="<a href='/course/unit/bdsUnitDetails?Id="+units[i].id+"'>"+ units[i].name+"</a>";
//        yAxis.appendChild(yAxisDiv);
//    }
//}

//// Build the schedule
//function buildUnitCalendarSchedule() {
//    var unitCalendarSchedule = document.getElementsByClassName('schedule')[0];
//    for (i = 0; i < units.length; i++) {
//        var gridRow = document.createElement('div');
//        gridRow.setAttribute("id", "p" + units[i].id);
//        gridRow.setAttribute("class", "grid-row");
//        unitCalendarSchedule.appendChild(gridRow);

//        for (i2 = 0; i2 < dates.length; i2++) {
//            var gridColumn = document.createElement('div');
//            gridColumn.setAttribute("class", "grid-column");
//            gridColumn.setAttribute("id", "p" + units[i].id + "-d" + dates[i2].getDate()
//                + "-m" + (dates[i2].getMonth() + 1));

//            // Sets the color of the background of the grid column
//            if (dates[i2].getDay() == 0 || dates[i2].getDay() == 6) {
//                gridColumn.style.backgroundColor = '#f7f7f7';
//            } else {
//                gridColumn.style.backgroundColor = 'rgb(252, 252, 252)';
//            }

//            gridRow.appendChild(gridColumn);

//            var height = calculateRowHeight(units[i]);
//            gridColumn.style.height = height + 'px';
//            gridRow.style.height = height + 'px';
//        }
//    }
//}

//// Populate the schedule with unitDurations
//function populateUnitCalendarSchedule() {
//    for (i = 0; i < unitDurations.length; i++) {
//        var unitRow = document.getElementById("p" + unitDurations[i].unit.id);
//        var unitDuration = document.createElement("div");
//        unitDuration.setAttribute("class", "assignment");
//        unitDuration.style.width = (getDates(unitDurations[i].start_date, unitDurations[i].end_date).length * 40) + "px";
//        unitDuration.style.background = unitDurations[i].resource.unitDurationColor;
//        var top = determineUnitDurationTop(unitDurations[i]);
//        unitDuration.style.top = top;

//        for (i2 = 0; i2 < dates.length; i2++) {
//            var days = calculateDaysBetweenDates(dates[0], unitDurations[i].start_date);
//            unitDuration.style.left = (days * 40) + "px";
//        }

//        unitDuration.innerHTML = unitDurations[i].resource.name;
//        unitRow.appendChild(unitDuration);
//    }
//}

