$(document).ready(function() {
    var winWidth = $(window).outerWidth();
    var bannerSliderHeight = $("#dashboardBanners .item").outerHeight();
    var studentInfoHeight = bannerSliderHeight - 95;

    if(($("#dashboardBanners .item").length >= 1) && winWidth >= 768) {
        $(".studentDetails").height(bannerSliderHeight);
        //$("studentDetails .studentInfo").height(studentInfoHeight);
        $(".studentDetails .studentInfo").mCustomScrollbar({
            setHeight: studentInfoHeight,
            autoHideScrollbar: true,
            autoExpandScrollbar: true,
            scrollbarPosition: "outside",
        })
    }
    
    if ($('.selectpicker').length) {
        $('select').selectpicker({
            noneSelectedText: "Please Select"
        });
    }

    if($('.pagination').length) {
        $('.pagination li a').addClass('arabictxt');
    }
    
    $('[data-toggle="tooltip"]').tooltip({
        //trigger: 'focus',
        html: true
    });

    //hamburger menu -----
    $('.first-button').on('click', function () {
        $('.animated-icon1').toggleClass('open');
    });
    $('.second-button').on('click', function () {
        $('.animated-icon2').toggleClass('open');
    });
    $('.third-button').on('click', function () {
        $('.animated-icon3').toggleClass('open');
    });

    
    //sidenav toggle -----
    $(".navbar-toggler").on("click",function(){
        $(".sidebar-fixed").toggleClass("slideout");
        //$(".sidenav-overlay").toggleClass("show");
    });

    $('.has-dropdown .dropdown-toggle').on("click", function(e) {
        e.stopPropagation();
        e.preventDefault();
        $(this).next('.dropdown-menu').toggle();
    });
    $('.has-dropdown .dropdown-menu a.dropdown-item').on('click', function(){
        $(this).parent().parent().find('.dropdown-menu').hide();
    });

    // apply color to svg files
    $('img.svg').each(function(){
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
            
            // Check if the viewport is set, else we gonna set it if we can.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });

    $(window).click(function(e) {
        if (e.target.class === 'show') {
        } else {
            $(".incident-details").removeClass("animate slideIn show"); 
        }  
    });
    $('body').on('click', '.trigger-rightDrawer', function () {
        $('.right-drawer').toggleClass('open');
        $('.right-drawer').animate({
            right: '0'
        })
    });

    $('body').on('click', '.closeRightDrawer', function () {
        $('.right-drawer').toggleClass('open');
        $('.right-drawer').animate({
            right: '-500'
        })
    });

    //remove notification after clicking on read btn
    $(".mark-noti-read li i").click(function(){
        $(this).closest("li").fadeOut(500, function() { $(this).remove(); });
    })

    //initialize inline CK Editor
    if($(".ck-inlineEditor").length) {
        CKEDITOR.disableAutoInline = true;
        var inlineElement = CKEDITOR.document.find( '.ck-inlineEditor' ),
            i = 0,
            element;
        while ( ( element = inlineElement.getItem( i++ ) ) ) {
            CKEDITOR.inline( element , {
                extraPlugins: 'sourcedialog'
            } );
        }
    }
    
    if($(".ck-Editor").length) {
        var ck_editor = CKEDITOR.document.find('.ck-Editor'),
            j = 0,
            element;
        while((element = ck_editor.getItem(j++))) {
            CKEDITOR.replace(element, {
                height: "300px"
            });
        }
    }
    
    //toggle checkbox appearance in grid view
    $(".grid-container input[type='checkbox']").each(function() {
        $(this).click(function(){
            $(this).closest(".check-item").toggleClass("show")
        })
    })


    //selectpicker option with image
    if($(".select-picker-withImg").length) {
        //const BASE_URL = $('base[ href ]').attr('href');
        const BASE_URL = "img/"
        const $_SELECT_PICKER = $('.select-picker-withImg');

        $_SELECT_PICKER.find('option').each((idx, elem) => {
            const $OPTION = $(elem);
            const IMAGE_URL = $OPTION.attr('data-thumbnail');

            if (IMAGE_URL) {
                $OPTION.attr('data-content', "<img src='%i'/> %s".replace(/%i/, BASE_URL + IMAGE_URL).replace(/%s/, $OPTION.text()))
            }

            console.warn('option:', idx, $OPTION)
        });

        $_SELECT_PICKER.selectpicker();
    };

});