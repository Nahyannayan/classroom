$(document).ready(function(){
    $(".fileUploader").fileinput({
        theme: "fas",
        uploadUrl: "/file-upload-batch/2",
        showUpload: true,
        showCaption: false,
        showRemove: false,
        minFileCount: 1,
        maxFileCount: 8,
        browseClass: "btn btn-outline-primary rounded z-depth-0 m-0 p-0 mt-3 btn-browse",
        removeClass: "btn btn-sm m-0 z-depth-0",
        uploadClass: "btn btn-primary rounded z-depth-0 m-0 p-0 mt-3 btn-upload",
        cancelClass: "btn btn-default float-right rounded z-depth-0 m-0 p-0 mt-3 btn-cancel",
        overwriteInitial: false,
        initialPreviewAsData: true, // defaults markup
        initialPreviewConfig: [ {
            frameAttr: {
                title: 'My Custom Title',
            }
        }],
        uploadExtraData: {
            img_key: "1000",
            img_keywords: "happy, nature"
        },
        fileActionSettings: {
            showZoom: false,
            showUpload: false,
            //indicatorNew: "",
            showDrag: false
        },
        preferIconicPreview: false, // this will force thumbnails to display icons for following file extensions
        previewSettings: {
            image: { width: "100%", height: "auto" },
            /* html: { width: "50px", height: "auto" },
            other: { width: "50px", height: "auto" } */
        },
    });
});