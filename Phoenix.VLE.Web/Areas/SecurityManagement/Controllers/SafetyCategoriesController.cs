﻿
using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SecurityManagement.Controllers
{
    [Authorize]

    public class SafetyCategoriesController : BaseController
    {
        private ISafetyCategoriesService _safetyCategoriesService;

        public SafetyCategoriesController(ISafetyCategoriesService safetyCategoriesService)
        {
            _safetyCategoriesService = safetyCategoriesService;
        }
        // GET: SecurityManagement/SafetyCategories
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadSafetyCategoriesGrid()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var safetyCategories = _safetyCategoriesService.GetSafetyCategories((int)(int)SessionHelper.CurrentSession.SchoolId);
            var actionButtonsHtmlTemplate = string.Empty;
            if (CurrentPagePermission.CanEdit)
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='safetyCategories.editCategoryPopup($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='safetyCategories.deleteCategoryData($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.DeleteRecord") + "'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a></div>";
            else
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='safetyCategories.editCategoryPopup($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.View") + "'><img src='/Content/VLE/img/svg/tbl-view.svg'></a></div>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in safetyCategories
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.SafetyCategoryId),
                              item.SafetyCategoryName,
                              item.FormattedCreatedOn,
                              item.CreatedByName,
                              IsActive = item.IsActive ? ResourceManager.GetString("Shared.Labels.Active") : ResourceManager.GetString("Shared.Labels.DeActive")
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitAddEditSafetyCategoryForm(int? id)
        {
            var model = new SafetyCategoryEdit();
            if (id.HasValue)
            {
                var category = _safetyCategoriesService.GetSafetyCategoryById(id.Value);
                EntityMapper<SafetyCategory, SafetyCategoryEdit>.Map(category, model);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
            }
            return PartialView("_AddEditSafetyCategory", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSafetyCategoryData(SafetyCategoryEdit model)
        {
            if (!CurrentPagePermission.CanEdit)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            }
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            model.ParseJsonToXml("SafetyCategoryXml");

            var result = _safetyCategoriesService.UpdateSafetyCategoryData(model);
            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "Safety Catagory")), JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSafetyCategoryData(int id)
        {
            var result = _safetyCategoriesService.DeleteSafetyCategoryData(id);
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }
    }
}