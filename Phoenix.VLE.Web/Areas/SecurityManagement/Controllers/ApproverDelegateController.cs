﻿using Phoenix.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.Enums;

namespace Phoenix.VLE.Web.Areas.SecurityManagement.Controllers
{
    public class ApproverDelegateController : BaseController
    {
        private readonly ITemplateService _templateService;
        private readonly IExemplarWallServices _exemplarWallService;
        public ApproverDelegateController(
            ITemplateService templateService, IExemplarWallServices exemplarWallService)
        {
            _templateService = templateService;
            _exemplarWallService = exemplarWallService;
        }
        public ActionResult Index()
        {
            //var departments = _exemplarWallService.GetSchoolDepartmentList(SessionHelper.CurrentSession.SchoolId);
            //ViewBag.Departments = new SelectList(departments, "SchoolDepartmentId", "SchoolDepartmentName");

            return View(GetApproverDelegates(PlanTemplateTypes.Certificate, 
                //(departments.Any() ? departments.FirstOrDefault().SchoolDepartmentId: 0)
                null));
        }

        private IEnumerable<CertificateTeacherMapping> GetApproverDelegates(PlanTemplateTypes templateType, long? departmentId)
        {
            IEnumerable<CertificateTeacherMapping> teacherMappings = new List<CertificateTeacherMapping>();
            teacherMappings = _templateService.GetSchoolTemplateApproverDelegates(SessionHelper.CurrentSession.SchoolId, templateType, departmentId);
            return teacherMappings;
        }

        [HttpPost]
        public ActionResult SaveApproverDelegates(string teacherIds, short templateTypeId/*, long departmentId*/)
        {
            var op = new OperationDetails();
            if (!SessionHelper.CurrentSession.IsAdmin)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var model = new CertificateTeacherMapping() { CreatedBy = SessionHelper.CurrentSession.Id, TeacherUserIds = teacherIds, SchoolId = SessionHelper.CurrentSession.SchoolId, TemplateTypeId = templateTypeId//, DepartmentId = departmentId 
            };
            bool result = _templateService.SaveCertificateApprovalTeacher(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetApproverDelegatePartial(PlanTemplateTypes templateType, long? departmentId)
        {
            return PartialView("_ApproverDelegateList", GetApproverDelegates(templateType, null));
        }
    }
}