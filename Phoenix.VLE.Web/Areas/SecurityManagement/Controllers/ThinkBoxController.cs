﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Phoenix.VLE.Web.Areas.SecurityManagement.Controllers
{
    [Authorize]
    public class ThinkBoxController : BaseController
    {
        private readonly IUserService _userService;
        private ISuggestionService _suggestionService;

        public ThinkBoxController(IUserService userService, ISuggestionService suggestionService)
        {
            _userService = userService;
            _suggestionService = suggestionService;
        }
        // GET: SecurityManagement/ThinkBox
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AssignUsers()
        {

            var thinkBox = new ThinkBoxEdit();

            var thinkBoxUsersList = _suggestionService.getThinkBoxUsersBySchoolId(SessionHelper.CurrentSession.SchoolId);

            var staffList = _userService.GetUserBySchool((int)(int)SessionHelper.CurrentSession.SchoolId, 3);


            thinkBox.StaffList.AddRange(staffList.Select(r => new SelectListItem
            {
                Text = $"{r.UserDisplayName} - ({r.UserName})",
                Value = r.Id.ToString()
            }).OrderBy(r => r.Text));

            thinkBox.UsersList.AddRange(staffList.Where(r => !thinkBoxUsersList.Any(a => a.UserId == r.Id)).Select(r => new SelectListItem
            {
                Text = $"{r.UserDisplayName} - ({r.UserName})",
                Value = r.Id.ToString()
            }).OrderBy(r => r.Text));

            thinkBox.SelectedUsersList.AddRange(staffList.Where(r => thinkBoxUsersList.Any(a => a.UserId == r.Id)).Select(r => new SelectListItem
            {
                Text = $"{r.UserDisplayName} - ({r.UserName})",
                Value = r.Id.ToString()
            }).OrderBy(r => r.Text));


            return View("~/Areas/SecurityManagement/Views/ThinkBox/AssignThinkBoxUsers.cshtml", thinkBox);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUpdateThinkBoxUser(ThinkBoxEdit thinkBoxEdit)
        {
            var result = false;
            var thinkBoxUsersList = _suggestionService.getThinkBoxUsersBySchoolId(SessionHelper.CurrentSession.SchoolId);
            var deselectedUsersList = thinkBoxUsersList.Where(r => thinkBoxEdit.DeselectedUsersId.Contains(r.UserId.ToString()));
            if (string.IsNullOrWhiteSpace(thinkBoxEdit.SelectedUsersId) && thinkBoxEdit.DeselectedUsersId != null)
            {
                foreach (var d in deselectedUsersList)
                {

                    _suggestionService.DeleteUserFromThinkBox(d.UserId);
                }
                return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
            }
            if (!string.IsNullOrWhiteSpace(thinkBoxEdit.SelectedUsersId))
            {
                var removeDelegateList = thinkBoxUsersList.Where(r => !thinkBoxEdit.SelectedUsersId.Contains(r.UserId.ToString()));
                foreach (var i in thinkBoxEdit.SelectedUsersId.Split(','))
                {
                    if (!string.IsNullOrWhiteSpace(i))
                    {
                        thinkBoxEdit.UserId = Convert.ToInt64(i);
                        thinkBoxEdit.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
                        thinkBoxEdit.CreatedBy = SessionHelper.CurrentSession.Id;
                        result = _suggestionService.AddUpdateThinkBoxUser(thinkBoxEdit);
                    }
                }

                foreach (var d in removeDelegateList)
                {

                    _suggestionService.DeleteUserFromThinkBox(d.UserId);
                }

                return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.SelectAppropriateAction), JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult LoadThinkBoxUsersGrid()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var thinkBoxUsersList = _suggestionService.getThinkBoxUsersBySchoolId(SessionHelper.CurrentSession.SchoolId);
            var actionButtonsHtmlTemplate = string.Empty;
            actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='thinkBox.deleteThinkBoxUser($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.DeleteRecord") + "'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a></div>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in thinkBoxUsersList
                          select new
                          {
                              Actions = CurrentPagePermission.CanEdit ? string.Format(actionButtonsHtmlTemplate, item.UserId) : String.Empty,
                              item.UserName
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteThinkBoxUser(int id)
        {
            var result = _suggestionService.DeleteUserFromThinkBox(id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
    }
}