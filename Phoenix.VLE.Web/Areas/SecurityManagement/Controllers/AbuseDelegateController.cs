﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.VLE.Web.Models;
using Phoenix.Common;
using System.Threading.Tasks;
using Phoenix.Common.Localization;

namespace Phoenix.VLE.Web.Areas.SecurityManagement.Controllers
{
    [Authorize]
    public class AbuseDelegateController : BaseController
    {

        private readonly IAbuseDelegateService _abuseDelegateService;
        private readonly IUserService _userService;

        public AbuseDelegateController(IAbuseDelegateService abuseDelegateService, IUserService userService)
        {
            _abuseDelegateService = abuseDelegateService;
            _userService = userService;
        }

        // GET: SecurityManagement/AbuseDelegate
        public ActionResult Index()
        {
            var currentUser = Helpers.CommonHelper.GetLoginUser();
            var staffList = _userService.GetUserBySchool((int)currentUser.SchoolId, 3);
            var result = _abuseDelegateService.GetAbuseContactInfoBySchoolId(currentUser.SchoolId);
            var vm = EntityMapper<AbuseContactInfo, AbuseDelegateEdit>.Map(result);

            var abuseDelegateList = _abuseDelegateService.GetAbuseDelegateBySchoolId(SessionHelper.CurrentSession.SchoolId);

            vm.SelectedDelegateId = abuseDelegateList.Count() > 0 ? string.Join(",", abuseDelegateList.Select(r => r.DelegateId).ToList()) + "," : string.Empty;

            // vm.DelegateList.Add(new SelectListItem { Text = "---Select---", Value = "0" });
            vm.StaffList.AddRange(staffList.Select(r => new SelectListItem
            {
                Text = $"{r.UserDisplayName} - ({r.UserName})",
                Value = r.Id.ToString()
            }).OrderBy(r => r.Text));

            vm.DelegateList.AddRange(staffList.Where(r => !abuseDelegateList.Any(a => a.DelegateId == r.Id)).Select(r => new SelectListItem
            {
                Text = $"{r.UserDisplayName} - ({r.UserName})",
                Value = r.Id.ToString()
            }).OrderBy(r => r.Text));

            vm.SelectedDelegateList.AddRange(staffList.Where(r => abuseDelegateList.Any(a => a.DelegateId == r.Id)).Select(r => new SelectListItem
            {
                Text = $"{r.UserDisplayName} - ({r.UserName})",
                Value = r.Id.ToString()
            }).OrderBy(r => r.Text));
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUpdateAbuseContactInfo(AbuseDelegateEdit abuseDelegateEditModel)
        {
            var result = false;
            if (!CurrentPagePermission.CanEdit)
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            ModelState.Remove("PolicePhoneNumber");
            if (!ModelState.IsValid)
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);

            var currentUser = Helpers.CommonHelper.GetLoginUser();
            abuseDelegateEditModel.SchoolId = currentUser.SchoolId;
            abuseDelegateEditModel.IsActive = true;

            if (abuseDelegateEditModel.AbuseContactInfoId > 0)
            {
                abuseDelegateEditModel.UpdatedBy = currentUser.Id;
                result = _abuseDelegateService.UpdateAbuseContactInfo(abuseDelegateEditModel);
            }
            else
            {
                abuseDelegateEditModel.CreatedBy = currentUser.Id;
                result = _abuseDelegateService.AddAbuseContactInfo(abuseDelegateEditModel);
            }
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUpdateAbuseDelegate(AbuseDelegateEdit abuseDelegateEditModel)
        {
            if (!CurrentPagePermission.CanEdit)
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            var result = false;

            abuseDelegateEditModel.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            var abuseDelegateList = _abuseDelegateService.GetAbuseDelegateBySchoolId(SessionHelper.CurrentSession.SchoolId);


            if (!string.IsNullOrWhiteSpace(abuseDelegateEditModel.SelectedDelegateId))
            {
                var removeDelegateList = abuseDelegateList.Where(r => !abuseDelegateEditModel.SelectedDelegateId.Contains(r.DelegateId.ToString()));
                foreach (var i in abuseDelegateEditModel.SelectedDelegateId.Split(','))
                {
                    if (!string.IsNullOrWhiteSpace(i))
                    {
                        abuseDelegateEditModel.DelegateId = Convert.ToInt64(i);
                        abuseDelegateEditModel.CreatedBy = SessionHelper.CurrentSession.Id;
                        abuseDelegateEditModel.UpdatedBy = SessionHelper.CurrentSession.Id;
                        abuseDelegateEditModel.IsActive = true;
                        result = _abuseDelegateService.AddAbuseDelegate(abuseDelegateEditModel);
                    }
                }

                foreach (var d in removeDelegateList)
                {

                    _abuseDelegateService.DeleteAbuseDelegate(d.AbuseDelegateId);
                }

                return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.NoDataToSave), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveMobileSettings(AbuseDelegateEdit abuseDelegateEditModel)
        {
            var op = new OperationDetails();
            if (!SessionHelper.CurrentSession.IsAdmin)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                return Json(op, JsonRequestBehavior.AllowGet);
            }

            abuseDelegateEditModel.UpdatedBy = SessionHelper.CurrentSession.Id;
            abuseDelegateEditModel.SchoolId = SessionHelper.CurrentSession.SchoolId;
            bool result = await _abuseDelegateService.SaveMobileSettings(abuseDelegateEditModel);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
    }
}