﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SecurityManagement.Controllers
{
    [Authorize]
    public class CensorFilterController : BaseController
    {
        private ICensorFilterService _censorFilterService;

        public CensorFilterController(ICensorFilterService censorFilterService)
        {
            _censorFilterService = censorFilterService;
        }
        // GET: SecurityManagement/CensorFilter
        public ActionResult Index()
        {
            ViewBag.InitialBannedWords = string.Join(";", _censorFilterService.GetBannedWords((int)SessionHelper.CurrentSession.SchoolId).Select(x => x.BannedWordName));
            return View();
        }

        public ActionResult LoadCensorUserGrid()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var users = _censorFilterService.GetCensorUsers((int)user.SchoolId);
            var actionButtonsHtmlTemplate = "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick=''><i class='far fa-trash-alt'></i></button></div>";

            var dataList = new object();
            dataList = new
            {
                aaData = (from item in users
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.CensorUserId),
                              CensorFrom = "",
                              CensorTo = "",
                              CensorDate = "",
                              CensorWord = "",
                              Severity = "",
                              IP = "",
                              Context = ""
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBannedWords()
        {
            return Json(_censorFilterService.GetBannedWords((int)SessionHelper.CurrentSession.SchoolId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveBannedWordData(string word, string previousWord)
        {
            if (!CurrentPagePermission.CanEdit)
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            var model = new BannedWord(previousWord, word);
            model.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            var result = _censorFilterService.UpdateBannedWordData(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteBannedWordData(string word)
        {
            if (!CurrentPagePermission.CanEdit)
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            var bannedWord = new BannedWord();
            bannedWord.BannedWordName = word;
            bannedWord.SchoolId =(int) SessionHelper.CurrentSession.SchoolId;
            var result = _censorFilterService.DeleteBannedWordData(bannedWord);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
    }
}