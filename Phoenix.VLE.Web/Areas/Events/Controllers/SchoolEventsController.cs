﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Events.Controllers
{
    public class SchoolEventsController : Controller
    {

        // GET: Events/SchoolEvents
        public ActionResult Index()
        {
            LoadDropdowns();
            return View();
        }
        [HttpPost]
        public ActionResult SaveSchoolEvent(SchoolEventEdit schoolEventEdit)
        {
            
            HttpPostedFileBase file = Session["resourceFile"] as HttpPostedFileBase;
            //File Upload
            if (file != null)
            {
                string strFile = (file.FileName).Replace(" ", String.Empty).Replace("(", String.Empty).Replace(")", String.Empty);
                string basePath = Constants.SchoolEventUploads;
                if (!Directory.Exists(basePath))
                    Directory.CreateDirectory(Constants.SchoolEventUploads);
                file.SaveAs(Server.MapPath(Constants.SchoolEventUploads + strFile));
                schoolEventEdit.ResourceFile = strFile;
            }


            //
            schoolEventEdit.SchoolId = SessionHelper.CurrentSession.SchoolId;
            schoolEventEdit.UserId = SessionHelper.CurrentSession.Id;
            schoolEventEdit.CreatedBy = SessionHelper.CurrentSession.Id;
            schoolEventEdit.UpdatedBy = SessionHelper.CurrentSession.Id;
            //schoolEventEdit.EventTypeId = "1";//School Event
            IEventService _eventService = new EventService();
            var result = _eventService.EventCUD(schoolEventEdit);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public void SaveResourceFile(SchoolEventEdit schoolEventEdit)
        {
            Session["resourceFile"] = schoolEventEdit.ResourceFileUploade;
        }

        public ActionResult Detail(int id)
        {
            LoadDropdowns();
            IEventService _eventService = new EventService();
            SchoolEventEdit model = _eventService.GetSchoolEvent(id).FirstOrDefault();
            return View(model);
        }

        private void LoadDropdowns()
        {
            IDurationService _durationService = new DurationService();
            ViewBag.DurationList = _durationService.GetDuration(0);
            IEventCategoryService _eventCategoryService = new EventCategoryService();
            ViewBag.EventCategoryList = _eventCategoryService.GetEventCategory(0);
        }

        public ActionResult Update(int id)
        {
            LoadDropdowns();
            IEventService _eventService = new EventService();
            SchoolEventEdit model = _eventService.GetSchoolEvent(id).FirstOrDefault();
            return View(model);
        }
    }
}