﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SuggestionBox
{
    public class SuggestionBoxAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SuggestionBox";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SuggestionBox_default",
                "SuggestionBox/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}