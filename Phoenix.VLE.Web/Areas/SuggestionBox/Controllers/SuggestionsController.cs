﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SuggestionBox.Controllers
{
    //[Authorize]
    public class SuggestionsController : BaseController
    {
        private ISuggestionCategoryService _suggestionCategoriesService;
        private ISuggestionService _suggestionService;
        private readonly IUserPermissionService _userPermissionService;
        public SuggestionsController(ISuggestionCategoryService suggestionCategoriesService, ISuggestionService suggestionService, IUserPermissionService userPermissionService)
        {
            _suggestionCategoriesService = suggestionCategoriesService;
            _suggestionService = suggestionService;
            _userPermissionService = userPermissionService;
        }
        // GET: SuggestionBox/Suggestions
        public ActionResult Index()
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ThinkBox.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            return View();
        }
        public ActionResult LoadSuggestionsPanel(int pageIndex = 1, string searchString = "", bool CreatedByMeOnly = false, string sortBy = "")
        {
            var model = new Pagination<Suggestion>();
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ThinkBox.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var suggestionList = SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent() ? _suggestionService.PaginateStudentSuggestionByUserId(pageIndex, 9, searchString, sortBy).ToList() : _suggestionService.PaginateStudentSuggestionBySchoolId(pageIndex, 9, searchString, sortBy).ToList();
            if (suggestionList.Any())
                model = new Pagination<Suggestion>(pageIndex, 9, suggestionList, suggestionList.FirstOrDefault().TotalCount);
            model.RecordCount = suggestionList.Count == 0 ? 0 : suggestionList[0].TotalCount;
            model.LoadPageRecordsUrl = "/SuggestionBox/Suggestions/LoadSuggestionsPanel?pageIndex={0}";
            model.SearchString = searchString;
            model.CreatedByMeOnly = CreatedByMeOnly;
            model.SortBy = sortBy;
            return PartialView("_StudentSuggestions", model);
        }
        public ActionResult StudentSuggestions()
        {
            if (!_suggestionService.HasThinkBoxUserPermission())
            {
                return RedirectToAction("NoPermission", "Error");
            }
            var suggestions = _suggestionService.getStudentSuggestionBySchoolId();
            return View(suggestions);
        }
        public ActionResult InitAddEditSuggestionForm(int? id)
        {
            List<SuggestionCategory> lstSuggestionCategory = _suggestionCategoriesService.GetSuggestionCategory((int)(int)SessionHelper.CurrentSession.SchoolId).ToList();
            var model = new SuggestionEdit();
            if (id.HasValue)
            {
                var suggestion = _suggestionService.getStudentSuggestionById(id.Value);
                EntityMapper<Suggestion, SuggestionEdit>.Map(suggestion, model);
            }
            else
            {
                model.IsAddMode = true;
            }
            ViewBag.lstSuggestionCategory = new SelectList(lstSuggestionCategory, "SuggestionCategoryId", "SuggestionCategoryName", model.SuggestionId);
            return PartialView("_AddEditSuggestion", model);
        }
        [HttpPost]
        public async Task<ActionResult> InsertUpdateSuggestion(SuggestionEdit model, HttpPostedFileBase suggestionFile)
        {
            if (suggestionFile != null)
            {
                
                SharePointFileView shv = new SharePointFileView();
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    shv = await azureHelper.UploadStudentFilesAsPerModuleAsync(FileModulesConstants.SuggestionBox, SessionHelper.CurrentSession.OldUserId.ToString(), suggestionFile);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    shv = SharePointHelper.UploadStudentFilesAsPerModule(ref cx, FileModulesConstants.SuggestionBox, SessionHelper.CurrentSession.OldUserId.ToString(), suggestionFile);
                }
                //model.ContentImage = UploadContentResource(resourceFile);
                model.FileName = shv.ShareableLink;
                model.PhysicalPath = shv.SharepointUploadedFileURL;
            }
            var result = _suggestionService.UpdateSuggestionCategoryData(model);
            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "Think box title")), JsonRequestBehavior.AllowGet);
            }
            if (result == 1)
            {
                var Subject = model.Title;
                string EmailBody = String.Empty;
                StreamReader reader = new StreamReader(Server.MapPath(Constants.ThinkBoxAlertTemplate), Encoding.UTF8);
                EmailBody = reader.ReadToEnd();
                EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
                var Message = model.Description;

                ICommonService _commonService = new CommonService();
                string toEmailAddrees = "";
                var thinkBoxUsersList = _suggestionService.getThinkBoxUsersBySchoolId(SessionHelper.CurrentSession.SchoolId);
                if (thinkBoxUsersList.Any())
                {
                    foreach (var item in thinkBoxUsersList)
                    {
                        //toEmailAddrees = toEmailAddrees + ";" + toEmailAddrees;
                        toEmailAddrees = toEmailAddrees + "," + item.UserEmail;
                    }
                }

                //toEmailAddrees = toEmailAddrees.Remove(toEmailAddrees.Length - 1);
                //EmailBody = reader.ReadToEnd();
                EmailBody = EmailBody.Replace("@@Description", Message);
                EmailBody = EmailBody.Replace("@@title", Subject);
                string firstName = String.IsNullOrEmpty(SessionHelper.CurrentSession.FirstName) ? "" : SessionHelper.CurrentSession.FirstName;
                string lastName = String.IsNullOrEmpty(SessionHelper.CurrentSession.LastName) ? "" : SessionHelper.CurrentSession.LastName;
                EmailBody = EmailBody.Replace("@@studentname", firstName + " " + lastName);
                SendMailView sendEmailNotificationView = new SendMailView();
                sendEmailNotificationView.FROM_EMAIL = SessionHelper.CurrentSession.Email;
                sendEmailNotificationView.TO_EMAIL = toEmailAddrees;
                sendEmailNotificationView.LOG_TYPE = "ThinkBox";
                sendEmailNotificationView.SUBJECT = Subject;
                sendEmailNotificationView.MSG = EmailBody;
                sendEmailNotificationView.LOG_USERNAME = SessionHelper.CurrentSession.UserName;
                sendEmailNotificationView.STU_ID = "11111";
                sendEmailNotificationView.LOG_PASSWORD = "tt";
                sendEmailNotificationView.PORT_NUM = "123";
                sendEmailNotificationView.EmailHostNew = "false";
                var operationalDetails = _commonService.SendEmail(sendEmailNotificationView);
            }
            var op = new OperationDetails(result == 1 ? true : false);
            if (model.IsAddMode && result == 1)
                op.Message = ResourceManager.GetString("Shared.Messages.Added");
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        private string UploadSuggestionFiles(HttpPostedFileBase suggestionFile)
        {
            string GUID = Guid.NewGuid().ToString();
            string filePath = String.Empty;
            string strFile = String.Format("{0}{1}", GUID, Path.GetExtension(suggestionFile.FileName));
            //string strFile = suggestionFile.FileName;
            string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
            string content = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir + "/Content";
            filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir + "/Content" + "/SuggestionFiles";
            //CommonHelper.CreateDestinationFolder(resourceDir);
            //CommonHelper.CreateDestinationFolder(content);
            //CommonHelper.CreateDestinationFolder(filePath);
            filePath = Path.Combine(filePath, strFile);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            suggestionFile.SaveAs(filePath);
            return strFile;
        }

        [HttpPost]
        public async Task<ActionResult> UploadSuggestionFile()
        {
            string relativePath = string.Empty;
            string GUID = Guid.NewGuid().ToString();
            string fname = String.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Get all files from Request object
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        fname = file.FileName;
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        SharePointFileView shv = new SharePointFileView();
                        if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                        {
                            AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                            await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                            shv = await azureHelper.UploadFilesAsPerModuleAsync(FileModulesConstants.SuggestionBox, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                        }
                        else
                        {
                            SharePointHelper sh = new SharePointHelper();
                            Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                            shv = SharePointHelper.UploadFilesAsPerModule(ref cx, FileModulesConstants.SuggestionBox, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                        }
                        
                        fname = shv.ShareableLink;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return Json(new OperationDetails(true, fname), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadSuggestionGridByUser()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var dataList = new object();
            var actionButtonsHtmlTemplate = string.Empty;
            var suggestions = _suggestionService.getStudentSuggestionByUserId();
            actionButtonsHtmlTemplate = "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='suggestion.editSuggestionPopup($(this),{0})'><i class='fas fa-pencil-alt'></i></button><button type='button' class='table-action-icon-btn' onclick='suggestion.deleteSuggestionData($(this),{0})'><i class='far fa-trash-alt'></i></button></div>";

            dataList = new
            {
                aaData = (from item in suggestions
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.SuggestionId),
                              item.Title,
                              item.Description,
                              item.Type,
                              item.Remark
                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSuggestionData(int id)
        {
            var result = _suggestionService.DeleteSuggestionData(id);
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadSuggestionGrid()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var suggestions = _suggestionService.getStudentSuggestionBySchoolId();
            var actionButtonsHtmlTemplate = string.Empty;
            actionButtonsHtmlTemplate = "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='studentSuggestion.downloadAttachment($(this),\"{0}\")'><i class='fa fa-download'></i></button></div>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in suggestions
                          select new
                          {
                              Actions = String.IsNullOrEmpty(item.FileName) ? "" : String.Format(actionButtonsHtmlTemplate, item.FileName),
                              item.Title,
                              item.Description,
                              item.Type,
                              item.UserName
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        public string GetFilePath(string fileName)
        {
            string resourceDir = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir;
            string content = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content";
            return PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content" + "/SuggestionFiles/" + fileName;

        }
        public void DownloadAttachment(string fileName)
        {
            string resourceDir = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir;
            string content = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content";
            string path = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content" + "/SuggestionFiles/" + fileName;
            var webClient = new WebClient();
            byte[] FileBytes = webClient.DownloadData(path);
            Response.ContentType = GetMimeType(path);
            Response.BufferOutput = true;
            Response.AppendHeader("Content-Disposition", "Attachment; Filename=" + System.IO.Path.GetFileName(path) + "");
            Response.AddHeader("Last-Modified", DateTime.Now.ToLongDateString());
            Response.BinaryWrite(FileBytes);
            Response.Flush();
            Response.End();
        }

        private string GetMimeType(string filePath)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(filePath).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }

        public ActionResult DownloadStudentSuggestionDetails()
        {
            try
            {
                string currentTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string path = string.Empty;
                string fileDownloadName = "Suggestions_" + currentTimestamp + ".csv";
                path = Server.MapPath("~/Content/Files/EmptySuggestionsFile" + currentTimestamp + ".csv");
                byte[] bytes;

                bytes = _suggestionService.ExportStudentSuggestions_CSVFile(path);

                var utf_result = System.Text.Encoding.UTF8.GetPreamble().Concat(bytes).ToArray();

                return File(utf_result, "application/csv;charset=utf-8", fileDownloadName);
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

    }
}