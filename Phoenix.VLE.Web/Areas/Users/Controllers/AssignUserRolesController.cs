﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Phoenix.VLE.Web.Areas.Users.Controllers
{
    public class AssignUserRolesController : BaseController
    {
        private IUserService _userService;
        private IUserRoleService _userRoleService;
        public AssignUserRolesController(IUserService userService, IUserRoleService userRoleService)
        {
            _userService = userService;
            _userRoleService = userRoleService;
        }
        // GET: Users/AssignUserRoles
        public ActionResult Index()
        {
            var model = new AssignUserRoleView();
          
            model.UserList = _userService.GetUserBySchool(SessionHelper.CurrentSession.SchoolId, 3).Where(i => !string.IsNullOrWhiteSpace(i.UserName)).Select(i => new ListItem()
            {
                ItemName = $"{i.UserDisplayName} ({i.UserName})",
                ItemId = i.Id.ToString()
            });


            var UserRolesList = _userRoleService.GetUserRolesBySchoolId((int)SessionHelper.CurrentSession.SchoolId);
            model.UserRolesList = UserRolesList.Select(i => new UserRolesList()
            {
                UserRoleId = i.UserRoleId,
                UserRoleName = i.UserRoleName,
                IsSelected = i.IsSelected
            });
            ViewBag.UserTypes = new SelectList(SelectListHelper.GetSelectListData(ListItems.UserTypes), "ItemId", "ItemName", 1);
            return View(model);
        }

        public JsonResult GetRolePermissionModuleTree(short roleId, short? userId)
        {
            //IUserRead userRoleServices = new UserService();
            //var RolePermissionModuleList = userRoleServices.GetRoleSISModuleData(roleId);
            //List<TreeItemView> treeList = new List<TreeItemView>();
            //RolePermissionModuleList.ForEach(l => treeList.Add(new TreeItemView() { id = l.ModuleId, parentId = l.ParentModuleId, name = l.ModuleName, url = l.ModuleUrl }));
            //return Json(treeList);
            return Json("");
        }
        public ActionResult InitUserList(short? usertypeid)
        {

            ViewBag.UserTypes = new SelectList(SelectListHelper.GetSelectListData(ListItems.UserTypes), "ItemId", "ItemName", 1);
            
            IEnumerable<ListItem> userFilterList;
            //if (SessionHelper.CurrentSession.UserTypeId == 5)
            //{
            //    userFilterList = _userService.GetUserBySchool(0, Convert.ToInt32(usertypeid)).Where(i=>!string.IsNullOrWhiteSpace(i.UserName)).Select(i => new ListItem()
            //    {
            //        ItemName = $"{i.UserDisplayName} ({i.UserName.ToString()})" ,
            //        ItemId = i.Id.ToString()
            //    });
            //}
            //else
            //{
                userFilterList = _userService.GetUserBySchool(SessionHelper.CurrentSession.SchoolId, Convert.ToInt32(usertypeid)).Where(i => !string.IsNullOrWhiteSpace(i.UserName)).Select(i => new ListItem()
                {
                    ItemName = $"{i.UserDisplayName} ({i.UserName.ToString()})",
                    ItemId = i.Id.ToString()
                });
           // }
            
           
            return PartialView("_UserListMain", userFilterList);
        }

        public ActionResult InitUserRoleList(int userId, string username = null)
        {
            List<UserRolesList> list = new List<UserRolesList>();
            var userRolesLists = _userRoleService.GetAllUserRoleMappingData(userId);
            foreach (var item in userRolesLists)
            {
                list.Add(EntityMapper<UserRoleMapping, UserRolesList>.Map(item));
            }
            ViewBag.UserID = userId;
            ViewBag.UserName = username;

            return PartialView("_AssignUserRoleMain", list);
        }

        public ActionResult InitPermissionOnEditRole(int userId, short? roleId, string userName = null)
        {
            var model = new UserRolePermissionView();
            return PartialView("_UserRolePermissionEdit", model);
        }

        [HttpPost]
        public ActionResult SaveUserRoleMapping(long userId, short roleId, string username, string operationType)
        {
            List<UserRolesList> list = new List<UserRolesList>();
            var operationDetails = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                operationDetails.Success = false;
                operationDetails.Message = LocalizationHelper.ActionNotPermittedMessage;
                operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }

            if (operationType == "I")
            {
                if (_userRoleService.CheckUserRoleMapping(userId, roleId).ToShort() == 0)
                    _userRoleService.InsertUserRoleMappingData(userId, roleId);
            }
            else if (operationType == "D")
            {
                _userRoleService.DeleteUserRoleMappingData(userId, roleId);
            }
            var userRolesLists = _userRoleService.GetAllUserRoleMappingData(Convert.ToInt32(userId)).Where(a => a.IsSelected);
            foreach (var item in userRolesLists)
            {
                list.Add(EntityMapper<UserRoleMapping, UserRolesList>.Map(item));
            }
            ViewBag.UserID = userId;
            ViewBag.UserName = username;


            return PartialView("_AssignedUserRolePermission", list);
        }
    }
}