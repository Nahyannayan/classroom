﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Users.Controllers
{
    [Authorize]
    public class UserRoleController : BaseController
    {
        // GET: Users/UserRole
        private IUserRoleService _userRoleService;

        public UserRoleController(IUserRoleService userRoleService)
        {
            _userRoleService = userRoleService;
        }

        // GET: ListCategories/UserRoleList
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadUserRoleGrid()
        {
            var UserRoles = _userRoleService.GetUserRolesBySchoolId((int)SessionHelper.CurrentSession.SchoolId);
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in UserRoles
                          where item.UserRoleId != 5  // To exclude super admin record 
                          select new
                          {
                              Actions = item.IsEditable && SessionHelper.CurrentSession.UserTypeId == 5 ? GetEditDeleteActionLinks(item) : GetRoleViewActionLink(item),
                              //item.UserRoleName,
                              UserRoleName = HtmlHelperExtensions.LocalizedLinkButtons($"<a id='btn_{item.UserRoleId}' href='javascript:void(0)' class='table-action-text-link' title='{item.UserRoleName}' onclick='userRoles.viewUsersForRole($(this),{item.UserRoleId})'>{item.UserRoleName}</a>"),
                              item.UserCount,
                              item.IsEditable,
                              item.IsActive
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        private string GetRoleViewActionLink(UserRole item)
        {
            return "<a data-toggle='tooltip' id='btn_" + item.UserRoleId + "' href='javascript:void(0)' class='mr-2' title='" + item.UserRoleName + "' onclick='userRoles.viewUsersForRole($(this)," + item.UserRoleId + ")' > <img src='/Content/vle/img/svg/tbl-view.svg' /></a>";
        }

        private string GetEditDeleteActionLinks(UserRole item)
        {
            string actionLink = string.Empty;
            actionLink = $@"<a class='mr-2' data-toggle='tooltip' href='javascript:void(0)' title='{ResourceManager.GetString("Shared.Buttons.Edit")}' onclick='userRoles.editUserRolePopup($(this), {item.UserRoleId})'><img src='/Content/vle/img/svg/tbl-edit.svg'/></a>"
                + $"<a class='mr-2' data-toggle='tooltip' href='javascript:void(0)' onclick='userRoles.deleteUserRoleData($(this), {item.UserRoleId})' title='{ResourceManager.GetString("Shared.Buttons.Delete")}'> <img src='/Content/vle/img/svg/tbl-delete.svg' /></a>";
            return actionLink;
        }

        public ActionResult InitAddEditUserRoleForm(int? id)
        {
            var model = new UserRoleEdit();
            if (id.HasValue)
            {
                var userRole = _userRoleService.GetUserRoleById(id.Value);
                EntityMapper<UserRole, UserRoleEdit>.Map(userRole, model);
            }
            else
            {
                model.IsAddMode = true;
            }

            return PartialView("_AddEditUserRole", model);
        }

        public ActionResult GetUsersListView()
        {
            return PartialView("_UsersForRole");
        }

        public ActionResult UserListForRole(int id)
        {
            var userList = _userRoleService.GetUsersForRole(id, (int)SessionHelper.CurrentSession.SchoolId);
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in userList
                          select new
                          {
                              item.UserName,
                              item.UserDisplayName
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveUserRoleData(UserRoleEdit model)
        {
            if (!ModelState.IsValid)
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);

            model.ParseJsonToXml("UserRoleXml");
            var userRoles = _userRoleService.GetUserRolesBySchoolId((int)SessionHelper.CurrentSession.SchoolId);
            var isExist = userRoles.Any(r => r.UserRoleName == model.UserRoleName);

            if (isExist && model.IsAddMode)
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.UserExistsMessage), JsonRequestBehavior.AllowGet);//added 

            var result = _userRoleService.UpdateUserRoleData(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteUserRoleData(int id)
        {
            var result = _userRoleService.DeleteUserRoleData(id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult AuditDetails(long id)
        {
            return View();
        }
    }
}