﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Users.Controllers
{
    [Authorize]
    public class ErrorLogController : BaseController
    {
        private IUserService _userService;

        public ErrorLogController(IUserService userStaffService)
        {
            _userService = userStaffService;
        }
        // GET: Users/ErrorLog
        public ActionResult Index()
        {
            ErrorLoggerEdit errorLoggerEdit = new ErrorLoggerEdit();
            return View(errorLoggerEdit);
        }
       
        public ActionResult LoadErrorLogs()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            if (SessionHelper.CurrentSession.IsAdmin)
            {
                // var lgType = EncryptDecryptHelper.DecodeBase64("ParentLogin");
                var lgType = "ParentLogin";
                DateTime stDate = Convert.ToDateTime("2019-12-15 00:00:00.000");
                //DateTime stDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64("2019-12-15 00:00:00.000"));
                DateTime edDate = Convert.ToDateTime("2019-12-31 00:00:00.000");
                // DateTime edDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64("2019-12-31 00:00:00.000"));
                int SchoolId = 131001;
              // var users = _userService.GetUserLog(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), stDate, edDate, lgType);
               var lstlogs = _userService.GetErrorLogs(SchoolId, stDate, edDate, lgType);
               

                var actionButtonsHtmlTemplate = string.Empty;
                if (CurrentPagePermission.CanEdit)
                    actionButtonsHtmlTemplate = "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='Error.editCategoryPopup($(this),{0})'><i class='fas fa-eye'></i></button></div>";
                //var dataList = new object();
             //   var CustomPermission = true; //_userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_StudentCertificate.ToString());
                var dataList = new
                {
                    aaData = (from item in lstlogs
                              select new
                              {
                                  item.User,
                                  item.ErrorText,
                                  item.UserIpAddress,
                                  item.WebServerIpAddress,
                                  ReportingDate= (item.ReportingDate == DateTime.MinValue)  ? null:item.ReportingDate.ToShortDateString(),
                                  Actions= string.Format(actionButtonsHtmlTemplate, item.ErrorLogId),
                                  //Actions = "<div class='tbl-actions'>" + (CustomPermission ? $"<a href='/Users/ErrorLog/InitAddLogForm?id={EncryptDecryptHelper.EncryptUrl(item.ErrorLogId.ToString())}' class='table-action-icon-btn'  title='Edit'><i class='fas fa-pencil-alt btnEdit'></i></a> " : "")

                              }).ToArray()
                };


                return Json(dataList, JsonRequestBehavior.AllowGet);
            }
            return View();
        }

        public ActionResult InitAddLogForm(int? id)
        {

            var files = new ErrorLoggerEdit();
            if (id.HasValue)
            {
               var data= _userService.GetErrorLogFiles(id.Value).ToList();
                files.lstErrorFiles = data;
            }
            return PartialView("_LogFiles",files);
        }

    }
}