﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;

namespace Phoenix.VLE.Web.Areas.Users.Controllers
{
    [Authorize]
    public class AssignPermissionController : BaseController
    {
        private IUserRoleService _userRoleService;
        private IUserPermissionService _userPermissionService;
        public AssignPermissionController(IUserRoleService userRoleService, IUserPermissionService userPermissionService)
        {
            _userRoleService = userRoleService;
            _userPermissionService = userPermissionService;
        }
        // GET: Users/AssignPermission
        public ActionResult Index()
        {
            var model = new UserRolePermissionView();
            model.UserRoleList = _userRoleService.GetUserRolesBySchoolId((int)SessionHelper.CurrentSession.SchoolId);
            model.PermissionType = new List<PermissionTypeView>();
            return View(model);
        }
        public ActionResult _LoadUserRoles()
        {
            var UserRoles = _userRoleService.GetUserRolesBySchoolId((int)SessionHelper.CurrentSession.SchoolId);
           
            return PartialView("_LoadUserRoles",UserRoles);
        }
        public JsonResult GetModuleTree(int? moduleid, string modulecode)
        {
            modulecode = string.IsNullOrEmpty(modulecode) ? null : modulecode;
            var systemlangaugeid = LocalizationHelper.CurrentSystemLanguageId;
            var ModuleList = _userRoleService.GetModuleStructureList(Convert.ToInt32(systemlangaugeid),moduleid, modulecode).ToList();
            List<TreeItemView> treeList = new List<TreeItemView>();
            ModuleList.ForEach(l => treeList.Add(new TreeItemView() { id = (int?)l.ModuleId, parentId = l.ParentModuleId, name = l.ModuleName, url = l.ModuleUrl }));
            return Json(treeList);
        }
        public ActionResult InitRolePermission(short? moduleID, string moduleName, short roleId, short? userId, short? parentId)
        {
            var model = new UserRolePermissionView();

            
                model.PermissionType = _userRoleService.GetAllPermissionData(roleId, Convert.ToInt32(userId), Convert.ToInt32(moduleID),false,(int)SessionHelper.CurrentSession.SchoolId);
                model.ModuleName = moduleName;
                model.ModuleID = moduleID;

            return PartialView("_RolesPermission", model);
        }

        public ActionResult InitMenuPermission()
        {
            var model = new UserRolePermissionView();
            model.PermissionType = new List<PermissionTypeView>();
            return PartialView("_RolesPermission", model);
        }
        public JsonResult GetModuleList(string id)
        {
            int systemlangaugeid = LocalizationHelper.CurrentSystemLanguageId;
            var ModuleList = _userRoleService.GetModuleList(systemlangaugeid, id).ToList();
            var selectlistModuleList = ModuleList.Select(i => new ListItem()
            {
                ItemName = i.ModuleName.ToString(),
                ItemId = i.ModuleId.ToString()
            });
            return Json(selectlistModuleList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdatePermissionTypes(List<CustomPermissionEdit> mappingDetails, string operationType, short? userId, short userRoleId)
        {
            var operationDetails = new OperationDetails();

            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var canEdit = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_AssignPermission.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            if (!canEdit)
            {
                operationDetails.Success = false;
                operationDetails.Message = LocalizationHelper.ActionNotPermittedMessage;
                operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }
            operationDetails.Success = _userRoleService.UpdatePermissionTypeDataCUD(mappingDetails, operationType, userId, userRoleId);
            operationDetails.Message = LocalizationHelper.UpdateSuccessMessage;
            operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
    }
}