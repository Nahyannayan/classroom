﻿using Phoenix.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Users.Controllers
{
    public class UserBaseController : BaseController
    {
        // GET: Users/UserBase
        public ActionResult Index()
        {
            return View();
        }
    }
}