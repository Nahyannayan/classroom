﻿using Phoenix.Common.Localization;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Threading.Tasks;
using Phoenix.VLE.Web.Models.EditModels;

namespace Phoenix.VLE.Web.Areas.Users.Controllers
{
    public class ChatterPermissionController : Controller
    {
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly IStudentListService _studentListService;
        private readonly IChatterPermissionService _chatterPermissionService;
        public ChatterPermissionController(ISchoolGroupService schoolGroupService, IStudentListService studentListService, IChatterPermissionService chatterPermissionService)
        {
            _schoolGroupService = schoolGroupService;
            _studentListService = studentListService;
            _chatterPermissionService = chatterPermissionService;
        }
        // GET: Users/ChatterPermission
        [HttpPost]
        public ActionResult GetUserListByGroup(int groupId)
        { 
            var model = new Phoenix.Models.ChatUsers();
            model.StudentList = _chatterPermissionService.GetUsersInGroup(groupId).Where(x => x.UserTypeId == 1).ToList();
            model.TeacherList = _chatterPermissionService.GetUsersInGroup(groupId).Where(x => x.UserTypeId == 3).ToList();
            var stdList = _chatterPermissionService.GetUsersInGroup(groupId);
            var teaherList = _chatterPermissionService.GetUsersInGroup(groupId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            var model = new ViewModels.ChatUsers();
            model.SchoolId = SessionHelper.CurrentSession.SchoolId;
            
            model.GroupList.Add(new SelectListItem { Text = Common.Localization.ResourceManager.GetString("School.Blog.SelectGroup"), Value = "" });
            model.GroupList.AddRange(_schoolGroupService.GetSchoolGroupsBySchoolId(model.SchoolId)
                 .Select(r => r.SchoolGroupId == model.GroupId ? new SelectListItem { Text = r.SchoolGroupName, Value = r.SchoolGroupId.ToString(), Selected = true }
                : new SelectListItem { Text = r.SchoolGroupName, Value = r.SchoolGroupId.ToString() }));
            string studentlist = model.SelectedStudentList.ToString();
            string teacherlist = model.SelectedTeacherList.ToString();
            model.SelectedStudentList.Add(new SelectListItem { Text = Common.Localization.ResourceManager.GetString("School.Blog.SelectStudent"), Value = "" });
            model.SelectedStudentList.AddRange(_chatterPermissionService.GetUsersInGroup(Convert.ToInt32(model.GroupId))
                .Select(x => new SelectListItem
                {
                    Text = (String.IsNullOrEmpty(x.FirstName) ? string.Empty : x.FirstName) +
                (String.IsNullOrEmpty(x.MiddleName) ? string.Empty : " " + x.MiddleName) +
                (String.IsNullOrEmpty(x.LastName) ? string.Empty : " " + x.LastName),
                    Value = x.StudentId.ToString()
                }));
           model.SelectedTeacherList.Add(new SelectListItem { Text = Common.Localization.ResourceManager.GetString("School.Blog.SelectStudent"), Value = "" });
           model.SelectedTeacherList.AddRange(_chatterPermissionService.GetUsersInGroup(Convert.ToInt32(model.GroupId))
               .Select(x => new SelectListItem
               {
                   Text = (String.IsNullOrEmpty(x.FirstName) ? string.Empty : x.FirstName) +
               (String.IsNullOrEmpty(x.MiddleName) ? string.Empty : " " + x.MiddleName) +
               (String.IsNullOrEmpty(x.LastName) ? string.Empty : " " + x.LastName),
                   Value = x.TeacherId.ToString()
               }).Where(x => model.UserTypeId == SessionHelper.CurrentSession.IsTeacher()));
            return View(model);
        }

        [HttpPost]
        public ActionResult SaveChatterPermission(string SelectedStudentIDs,string SelectedTeacherIDs, string adminItem,bool adminItemValue)
        {
            var model = new ChatPermission();
            model.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            model.UserIds = SelectedStudentIDs+","+ SelectedTeacherIDs;
            if(adminItem=="chat")
            {
                model.PermissionId = 1;
            }
            else if(adminItem == "emoji")
            {
                model.PermissionId = 2;
            }
            else if (adminItem == "attach")
            {
                model.PermissionId = 3;
            }
            else if (adminItem == "delete")
            {
                model.PermissionId = 4;
            }
            else if (adminItem == "copymessage")
            {
                model.PermissionId = 5;
            }
            else 
            {
                model.PermissionId = 6;
            }
            if (adminItemValue)
            {
                model.Flag = adminItemValue;
            }
            var result = _chatterPermissionService.UpdateChatterPermission(model);
            
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
    }
}