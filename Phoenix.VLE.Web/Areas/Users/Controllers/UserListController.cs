﻿using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Phoenix.VLE.Web.Areas.Users.Controllers
{
    [Authorize]
    public class UserListController : BaseController
    {
        private ILogInUserService _loginUserService;
        private ISchoolService _schoolService;
        private IUserService _userService;
        private IUserLocationMapService _userLocationMapService;


        public UserListController(ISchoolService schoolService, ILogInUserService userService, IUserService userStaffService, IUserLocationMapService userLocationMapService)
        {
            _schoolService = schoolService;
            _loginUserService = userService;
            _userService = userStaffService;
            _userLocationMapService = userLocationMapService;
        }

        // GET: ListCategories/SchoolList
        public ActionResult Index()
        {
            Session["LoginType"] = StringEnum.GetStringValue(MainModuleCodes.AdminPanel);

            var model = new SchoolInformation();
            //model = _schoolService.GetSchoolById((int)SessionHelper.CurrentSession.SchoolId);
            model.SchoolId = SessionHelper.CurrentSession.SchoolId;
            return View(model);
           
        }

        // GET: Users/UserStaffList
        public ActionResult StaffList()
        {
            ViewBag.SchoolSelectList = new SelectList(GetSchoolList(), "SchoolId", "SchoolName");

            return View();
        }

        public ActionResult LoadUserGrid(int schoolId)
        {
            var userList = _loginUserService.GetUserList(schoolId);
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in userList
                          select new
                          {
                              item.Id,
                              item.UserDisplayName,
                              item.UserName,
                              item.UserTypeName,
                              item.IsSuperAdmin,
                              UserStatus =item.UserStatus.ToLower() =="active" ? ResourceManager.GetString("Shared.UserStatus.Active") : item.UserStatus.ToLower() == "en" ? ResourceManager.GetString("Shared.UserStatus.EN") : item.UserStatus
                          }).ToArray()
            };
            var jsonData = Json(dataList, JsonRequestBehavior.AllowGet);
            jsonData.MaxJsonLength = int.MaxValue;
            return jsonData;
        }

        public ActionResult LoadStaffGrid(int schoolId)
        {
            List<User> staffList = _userService.GetUserBySchool(schoolId, (int)UserTypes.Teacher).ToList(); //UsertType.Teacher gives 3 ,which is Id for staff
            var actionButtonsHtmlTemplate = "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='staffUsers.loadMappingPopup($(this),{0},{1})'><i class='fas fa-pencil-alt'></i></button></div>";
            var dataList = new object();

            dataList = new
            {
                aaData = (from item in staffList
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.Id,"\"" + item.UserDisplayName + "\""),
                              item.Id,
                              item.UserDisplayName,
                              item.UserName,
                              item.UserTypeName,
                              item.IsSuperAdmin,
                              item.UserStatus
                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Author : Tejal Chaudhari
        /// Created Date : 17-06-2019
        /// Description : Save User Locations(BU) Mapping Data
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveUserLocationMapData(SchoolMappingCommonEdit formData)
        {
            var result = false;
            int userId = formData.RegistrationId;
            int cityId = formData.CityId;
            int countryId = formData.CountryId;
            
            List<UserLocationMap> userLocationMapList = new List<UserLocationMap>();
            
            foreach (ListItem BU in formData.SelectedBUId.FindAll(x=> x.Selected == true))
            {
                userLocationMapList.Add(new UserLocationMap
                {
                    UserId = userId,
                    CityId = cityId,
                    CountryId = countryId,
                    BusinessUnitld = int.Parse(BU.Value)
                });
            }

            //ensure that there is atleast one dummy record for delete to work properly
            if (userLocationMapList.Count() == 0)
            {
                userLocationMapList.Add(new UserLocationMap
                {
                    UserId = userId,
                    CityId = cityId,
                    CountryId = countryId,
                    BusinessUnitld = 0
                });
            }

            result = _userLocationMapService.InsertUserLocationMaps(userLocationMapList);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitMapUserLocationsForm(int? id)
        {
            var model = new SchoolMappingCommonEdit();

            ViewBag.Country = new SelectList(SelectListHelper.GetSelectListData(ListItems.IncidentCountry, SessionHelper.CurrentSession.Id.ToString()), "ItemId", "ItemName", model.CountryId);
            ViewBag.Region = new SelectList(SelectListHelper.GetSelectListData(ListItems.IncidentCity, string.Format("{0},{1}", SessionHelper.CurrentSession.Id.ToString(), model.CountryId)), "ItemId", "ItemName", model.CityId);
            
            model.RegistrationId = id.Value;

            return PartialView("~/Views/Shared/_SchoolMappingCommon.cshtml", model);

        }

        public ActionResult GetCities(int id)
        {
            var cities = SelectListHelper.GetSelectListData(ListItems.IncidentCity, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, id)).ToList();
            return Json(cities, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBusinessUnits(int id, int RefId)
        {
            var result = _userLocationMapService.GetUserLocationMapsByUserId(RefId);
            var businessUnits = SelectListHelper.GetSelectListData(ListItems.IncidentBusinessUnit, string.Format("{0},{1}", SessionHelper.CurrentSession.Id.ToString(), id));
            var filter = businessUnits.Select(x => new ListItem { Value = x.ItemId, Text = x.ItemName }).ToList();
            foreach (var items in filter)
            {
                foreach (var mapItem in result)
                {
                    if (items.Value == mapItem.BusinessUnitld.ToString())
                        items.Selected = true;
                }
            }
            
            return Json(filter, JsonRequestBehavior.AllowGet);
        }

        #region Private Methods
        private List<SchoolInformation> GetSchoolList()
        {
            long schoolId = Helpers.CommonHelper.GetLoginUser().SchoolId;
            if (schoolId <= 0)
            {
                schoolId = (int)SessionHelper.CurrentSession.SchoolId;
            }

            List<SchoolInformation> schoolList = _schoolService.GetSchoolList().ToList();

            return schoolList;
        }
        #endregion

        
    }
}