﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Users
{
    public class UsersAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Users";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Users_TeacherSpaces_default",
                "Users/TeacherSpaces/Index",
                new { Controller = "SchoolSpaces", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
               "Users_TeacherGroups_default",
               "Users/TeacherGroups/Index",
               new { Controller = "SchoolGroups", action = "Index", id = UrlParameter.Optional }
           );
            context.MapRoute(
                "Users_VLE_default",
                "Users/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}