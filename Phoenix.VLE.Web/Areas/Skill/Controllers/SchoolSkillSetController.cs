﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services.Skill.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Skill.Controllers
{
    [Authorize]
    public class SchoolSkillSetController : BaseController
    {
        private readonly ISchoolSkillSetService _schoolSkillSetService;
        public SchoolSkillSetController(ISchoolSkillSetService schoolSkillSetService)
        {
            _schoolSkillSetService = schoolSkillSetService;
        }
        // GET: Skill/SchoolSkillSet
        public ActionResult Index()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var model = new SchoolSkillSetEdit();
            model.SchoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
            var schoolGradeList = SelectListHelper.GetSelectListData(ListItems.SchoolGrade, model.SchoolId);
            model.SchoolGradeList.AddRange(schoolGradeList);
            return View(model);
        }

        public ActionResult InitAddEditSchoolSkillSetForm(int? SkillId, int? SchoolGradeId)
        {
            var model = new SchoolSkillSetEdit();
            if (SkillId.HasValue)
            {
                var category = _schoolSkillSetService.GetSchoolSkillSetById(SkillId.Value);
                EntityMapper<SchoolSkillSet, SchoolSkillSetEdit>.Map(category, model);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolGradeId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolGradeId);
            }
            return PartialView("_AddSchoolSkillSet", model);
        }

        public ActionResult LoadSchoolSkillSetGrid(int SchoolGradeId)
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            // var schoolSkillSet = _schoolSkillSetService.GetSchoolSkillSet((int)(int)SessionHelper.CurrentSession.SchoolId);
            var schoolSkillSet = _schoolSkillSetService.GetSchoolSkillSet(SchoolGradeId);
            var actionButtonsHtmlTemplate = string.Empty;
            if (CurrentPagePermission.CanEdit)
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='SchoolSkillSet.editCategoryPopup($(this),{0})' title='" + Common.Localization.ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a><a class='mr-3' data-toggle='tooltip' data-placement='bottom' class='table-action-icon-btn' onclick='SchoolSkillSet.deleteCategoryData($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.DeleteRecord") + "'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a></div>";
            else
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='SchoolSkillSet.editCategoryPopup($(this),{0})' title='" + Common.Localization.ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-view.svg'></div>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in schoolSkillSet
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.SkillId),
                              item.SkillName,
                              item.CreatedByName,
                              item.IsApproved,
                              item.ApprovedBy
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSchoolSkillSetData(SchoolSkillSetEdit model)
        {
            if (!CurrentPagePermission.CanEdit)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            }
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            model.ParseJsonToXml("SkillSetXml");

            var result = _schoolSkillSetService.UpdateSchoolSkillSet(model);
            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "Safety Catagory")), JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteSchoolSkillSetData(int id)
        {
            var result = _schoolSkillSetService.DeleteSchoolSkillSet(id);
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }
    }
}