﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;

namespace Phoenix.VLE.Web.Areas.GradeBook.Controllers
{
    public class GradeBookController : BaseController
    {
        #region DI
        private IGradeBookService gradeBookService;
        private readonly IGradingTemplateService gradingTemplateService;
        private readonly IGradingTemplateItemService gradingTemplateItemService;
        private readonly IGradeBookSetupService gradeBookSetupService;
        private readonly IStudentListService studentListService;
        private const string Add = "ADD";
        private const string Edit = "EDIT";
        private const string Delete = "DELETE";

        public GradeBookController(IGradeBookService gradeBookService, IGradingTemplateService gradingTemplateService, IGradingTemplateItemService gradingTemplateItemService,
            IGradeBookSetupService gradeBookSetupService, IStudentListService studentListService)
        {
            this.gradeBookService = gradeBookService;
            this.gradingTemplateService = gradingTemplateService;
            this.gradingTemplateItemService = gradingTemplateItemService;
            this.gradeBookSetupService = gradeBookSetupService;
            this.studentListService = studentListService;
        }
        #endregion
        // GET: GradeBook/GradeBook
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>    
        /// Author  : Ashwin Dubey
        /// Get Teacher gradebook using school group id using group will get
        /// Course and Grade of student using which we can get gradebook setup and data according to setup
        /// </summary>
        /// <param name="SchoolGroupId">School Group id</param>
        /// <returns></returns>
        public ActionResult TeacherGradeBook(string SchoolGroupId = "")
        {
            ViewBag.GradebookId = SchoolGroupId;
            var response = gradeBookService.GetTeacherGradeBook(SchoolGroupId.ToLong(), SessionHelper.CurrentSession.Id);
            return PartialView(response);
        }

        /// <summary>
        /// Author  : Ashwin Dubey
        /// To Load pop up form for teacher to able to add new row
        /// </summary>
        /// <param name="id"></param>
        /// <param name="studentIds"></param>
        /// <returns></returns>
        public PartialViewResult LoadAddNewRowForm(long id, List<long> studentIds, long? internalAssessmentId = null)
        {
            ViewBag.GradingTemplate = new SelectList(gradingTemplateService.GetGradtingTemplates((int)SessionHelper.CurrentSession.SchoolId), "GradingTemplateId", "GradingTemplateTitle");
            TeacherInternalAssessmentEdit addNewRowEdit;
            if (internalAssessmentId == null)
            {
                addNewRowEdit = new TeacherInternalAssessmentEdit
                {
                    GroupId = 0,
                    ParentInternalAssessmentId = id,
                    StudentIds = studentIds
                };
            }
            else
            {
                var result = gradeBookService.GetSubInternalAssessment(internalAssessmentId.ToLong());
                addNewRowEdit = EntityMapper<TeacherInternalAssessment, TeacherInternalAssessmentEdit>.Map(result);
                addNewRowEdit.StudentIds = studentIds;
            }
            return PartialView("_AddNewRowForm", addNewRowEdit);
        }

        /// <summary>
        /// Author  : Ashwin Dubey
        /// To Save new row data
        /// </summary>
        /// <param name="addNew"></param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult AddNewRow(TeacherInternalAssessmentEdit addNew)
        {
            addNew.CreatedBy = SessionHelper.CurrentSession.Id;
            var teacherGradeBookAddNewRow = EntityMapper<TeacherInternalAssessmentEdit, TeacherInternalAssessment>.Map(addNew);
            var isEdit = teacherGradeBookAddNewRow.InternalAssessmentId > 0;
            var result = gradeBookService.TeacherGradebookCreateNewRow(teacherGradeBookAddNewRow);
            if (result > 0)
            {
                var internalAssessmentRowModel = new InternalAssessmentRowModel();
                addNew.InternalAssessmentId = result;
                if (addNew.GradingTemplateId.HasValue)
                {
                    var gradingTemplateItems = gradingTemplateItemService.GetGradingTemplateItems(addNew.GradingTemplateId.Value, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId).ToList();
                    internalAssessmentRowModel.GradingTemplateItems = gradingTemplateItems.Select(x =>
                    new GradeBookGradingTemplate
                    {
                        InternalAssessmentId = addNew.ParentInternalAssessmentId,
                        GradingTemplateId = x.GradingTemplateId,
                        GradingTemplateItemId = x.GradingTemplateItemId,
                        GradingItemDescription = x.GradingItemDescription,
                        Percentage = x.Percentage,
                        ShortLabel = x.ShortLabel,
                        GradingColor = x.GradingColor,
                        GradingTemplateItemSymbol = x.GradingTemplateItemSymbol,
                        SortOrder = x.SortOrder,
                        ScoreFrom = x.ScoreFrom,
                        ScoreTo = x.ScoreTo
                    }).ToList();
                }

                internalAssessmentRowModel.NewRowEdit = addNew;
                internalAssessmentRowModel.StudentIds = addNew.StudentIds;
                if (isEdit)
                    internalAssessmentRowModel.InternalAssessmentScores = gradeBookService.GetInternalAssessmentScoreByAssessmentId(result);

                var newRowHtml = ControllerExtension.RenderPartialViewToString(this, "_InternalAssessmentRow", internalAssessmentRowModel);
                return Json(new OperationDetails(true) { RelatedHtml = newRowHtml, InsertedRowId = (int)result }, JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Author  : Ashwin Dubey
        /// To Save internal assessment data stored by teacher
        /// </summary>
        /// <param name="assessmentScores">Posted Json object array of scores</param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult SaveInternalAssessmentData(List<InternalAssessmentScore> assessmentScores)
        {
            assessmentScores.RemoveAll(x => x.Score == "0");
            assessmentScores.ForEach(x => x.CreatedBy = SessionHelper.CurrentSession.Id);
            var result = gradeBookService.InternalAssessmentCU(assessmentScores);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Author  : Ashwin Dubey
        /// To Get Sub section row for internal assessment
        /// </summary>
        /// <param name="schoolGroupId">School group id</param>
        /// <param name="assessmentType">Assessment Type</param>
        /// <param name="studentIds">Student Id list to pass and generate tds for there assessment</param>
        /// <returns></returns>
        public JsonResult GetSubSectionAndRow(long schoolGroupId, GradeBookAssessmentType assessmentType, List<long> studentIds)
        {
            var subSection = string.Empty;
            var subRow = string.Empty;
            StringBuilder builder = new StringBuilder();
            switch (assessmentType)
            {
                // if Assignment and Quiz then same partial view structure can be used 
                case GradeBookAssessmentType.Assignment:
                case GradeBookAssessmentType.Quiz:
                    var response = gradeBookService.GetAssignmentQuizScore(schoolGroupId, assessmentType == GradeBookAssessmentType.Assignment);
                    var sectionNames = response.Select(x => x.AssignmentQuizTitle).Distinct();

                    // to get the left sub section name to show in sub section of Assignment/Quiz
                    foreach (var title in sectionNames)
                    {
                        var rawHtml = ControllerExtension.RenderPartialViewToString(this, "_LeftAssessmentTypeSubMenu", title);
                        builder.Append(rawHtml);
                    }
                    subSection = builder.ToString();
                    ViewData["studentList"] = studentIds;
                    ViewData["assessmentType"] = assessmentType;

                    //to generate Assignment/Quiz td and return as response
                    subRow = ControllerExtension.RenderPartialViewToString(this, "_AssignmentQuizTD", response);
                    break;
                case GradeBookAssessmentType.InternalAssessment:
                    var assessmentResponse = gradeBookService.GetInternalAssessments(schoolGroupId, SessionHelper.CurrentSession.Id);

                    //if there are any internal assessment then get rule processing setup using assessment ids and set property 
                    if (assessmentResponse.InternalAssessments.Any())
                    {
                        var assessmentIds = string.Join(",", assessmentResponse.InternalAssessments.Select(x => x.InternalAssessmentId).Distinct());
                        var ruleProcessing = gradeBookSetupService.GetGradebookRuleSetups(assessmentIds);
                        assessmentResponse.GradebookRuleSetups = ruleProcessing;
                    }

                    #region Get Progress Tracker Data
                    if (assessmentResponse.InternalAssessments.Any(x => x.IsCalculatedField.ToBoolean() && !string.IsNullOrEmpty(x.ResultConsideredId) && x.ResultConsideredId.Contains(((int)GradeBookAssessmentType.ProgressTracker).ToString())))
                    {
                        assessmentResponse.InternalAssessments
                            .Where(x =>
                             x.IsCalculatedField.ToBoolean() &&
                            !string.IsNullOrEmpty(x.ResultConsideredId) &&
                            x.ResultConsideredId.Contains(((int)GradeBookAssessmentType.ProgressTracker).ToString()))
                            .ToList()
                            .ForEach(x =>
                            {
                                var progressResponse = gradeBookService.GetProgressTracker(schoolGroupId, x.StartDate, x.EndDate);
                                x.ProgressTracker = progressResponse;
                            });
                    }
                    #endregion

                    var index = 0;
                    //added index to generate elements to maintain uniqueness to avoid clash in calx 
                    assessmentResponse.InternalAssessments.ForEach(x => {                        
                        x.Index = index;
                        index += studentIds.Count;
                        x.TeacherInternalAssessments.ForEach(y =>
                        {                            
                            y.Index = index;
                            index += studentIds.Count;
                        });
                    });
                    subSection = ControllerExtension.RenderPartialViewToString(this, "_LeftInternalAssessmentSubMenu", assessmentResponse);
                    ViewData["studentList"] = studentIds;
                    subRow = ControllerExtension.RenderPartialViewToString(this, "_InternalAssessmentTD", assessmentResponse);
                    break;
                case GradeBookAssessmentType.StandardizedAssessment:
                case GradeBookAssessmentType.ExternalExamination:
                case GradeBookAssessmentType.StudentDemographic:
                    var standardExternal = gradeBookService.GetStandardExaminations(schoolGroupId, (int)assessmentType);
                    var description = standardExternal.DistinctBy(x => x.AssessmentTypeId).Select(x => x.Description);
                    foreach (var desc in description)
                    {
                        if (!string.IsNullOrEmpty(desc))
                        {
                            var rawHtml = ControllerExtension.RenderPartialViewToString(this, "_LeftAssessmentTypeSubMenu", desc);
                            builder.Append(rawHtml);
                        }
                    }
                    subSection = builder.ToString();
                    ViewData["studentList"] = studentIds;
                    ViewData["assessmentType"] = assessmentType;
                    subRow = ControllerExtension.RenderPartialViewToString(this, "_StandardAssessmentExternalExaminationTD", standardExternal);
                    break;
                case GradeBookAssessmentType.ProgressTracker:
                    var progressTrackerResult = gradeBookService.GetProgressTracker(schoolGroupId);
                    var progressTracker = progressTrackerResult.ProgressTrackerType.Distinct();
                    foreach (var item in progressTracker)
                    {
                        var desc = StringEnum.GetStringValue((GradeBookProgressTracker)item);
                        if (!string.IsNullOrEmpty(desc))
                        {
                            var rawHtml = ControllerExtension.RenderPartialViewToString(this, "_LeftAssessmentTypeSubMenu", desc);
                            builder.Append(rawHtml);
                        }
                    }
                    subSection = builder.ToString();
                    ViewData["studentList"] = studentIds;
                    ViewData["assessmentType"] = assessmentType;
                    subRow = ControllerExtension.RenderPartialViewToString(this, "_ProgressTrackerTD", progressTrackerResult);
                    break;
                default:
                    break;
            }
            return Json(new { subSection, subRow }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Author  : Ashwin Dubey
        /// To delete internal assessment created by teacher
        /// </summary>
        /// <param name="internalAssessmentId"></param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult DeleteAssessment(long internalAssessmentId, bool forceDelete = false)
        {
            var teacherInternalAssessment = new TeacherInternalAssessment { 
                InternalAssessmentId = internalAssessmentId, 
                IsDelete = true, 
                ForceDeleteAssessmentEvenIfDataExist = forceDelete };
            var result = gradeBookService.TeacherGradebookCreateNewRow(teacherInternalAssessment);
            return Json(new OperationDetails(result > 0)
            {
                InsertedRowId = (int)result,
                Message = result > 0 ? LocalizationHelper.DeleteSuccessMessage :
                           (result == -1 ? ResourceManager.GetString("Gradebook.TeacherGradeBook.DependentRecord") : LocalizationHelper.TechnicalErrorMessage)
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Author  : Ashwin Dubey
        /// To load preview of gradebook with internal assessment only
        /// </summary>
        /// <param name="gradeBook"></param>
        /// <returns></returns>
        public PartialViewResult GetGradebookPreview(GradeBookEdit gradeBook)
        {
            var subSection = string.Empty;
            var subRow = string.Empty;
            var teacherGradebook = new TeacherGradeBook();
            var studentList = studentListService.GetStudentByGradeIds(gradeBook.SchoolGradeIds.FirstOrDefault().ToString()).Take(5);
            var gradingTemplate = new List<GradeBookGradingTemplate>();
            if (gradeBook.InternalAssessments.Any())
            {
                gradeBook.InternalAssessments.ForEach(x => x.InternalAssessmentId = x.Index);
                var ids = string.Join(",", gradeBook.InternalAssessments.Select(x => x.IsCalculatedField.ToBoolean() ? x.CalculatedGradingTemplateId : x.DataEntryGradingTemplateId));
                var gradingTemplates = gradeBookSetupService.GetGradingTemplatesByIds(ids);                
                gradeBook.InternalAssessments.ForEach(x =>
                {
                    var templateId = x.IsCalculatedField.ToBoolean() ? x.CalculatedGradingTemplateId : x.DataEntryGradingTemplateId;
                    var gradingtemplate = gradingTemplates.Where(y => y.GradingTemplateId == templateId).ToList();
                    var data = gradingtemplate.Select(y => new GradeBookGradingTemplate
                    {
                        InternalAssessmentId = x.InternalAssessmentId,
                        GradingTemplateId = y.GradingTemplateId,
                        GradingTemplateItemId = y.GradingTemplateItemId,
                        GradingItemDescription = y.GradingItemDescription,
                        Percentage = y.Percentage,
                        ShortLabel = y.ShortLabel,
                        GradingColor = y.GradingColor,
                        GradingTemplateItemSymbol = y.GradingTemplateItemSymbol,
                        SortOrder = y.SortOrder,
                        IsForParent = true
                    });
                    gradingTemplate.AddRange(data);
                });
            }

            teacherGradebook.StudentLists = studentList.Select(x => new GradeBookStudentList { StudentId = x.StudentId, StudentName = x.StudentName }).ToList();
            teacherGradebook.GradingTemplates = gradingTemplate;
            teacherGradebook.AssessmentType = gradeBook.AssessmentTypeIds;
            teacherGradebook.InternalAssessments = gradeBook.InternalAssessments;
            teacherGradebook.IsPreview = true;
            foreach (var item in gradeBook.AssessmentTypeIds)
            {
                var assessment = (GradeBookAssessmentType)item;
                if(assessment == GradeBookAssessmentType.Assignment)
                {
                    var assignmentQuiz = new List<GradeBookAssignmentQuiz> { new GradeBookAssignmentQuiz { IsTopRow = true } };
                    teacherGradebook.Assignments = assignmentQuiz;
                }
                if (assessment == GradeBookAssessmentType.Quiz)
                {
                    var assignmentQuiz = new List<GradeBookAssignmentQuiz> { new GradeBookAssignmentQuiz { IsTopRow = true } };
                    teacherGradebook.Quizzes = assignmentQuiz;
                }
            }
            return PartialView("_GradebookTable", teacherGradebook);
        }       
    }
}