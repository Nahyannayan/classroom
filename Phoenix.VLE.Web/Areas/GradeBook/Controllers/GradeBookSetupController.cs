﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;

namespace Phoenix.VLE.Web.Areas.GradeBook.Controllers
{
    public class GradeBookSetupController : BaseController
    {
        #region DI
        private IGradeBookSetupService gradeBookSetupService;
        private readonly IGradingTemplateService gradingTemplateService;
        private const string Add = "ADD";
        private const string Edit = "EDIT";
        private const string Delete = "DELETE";

        public GradeBookSetupController(IGradeBookSetupService gradeBookSetupService, IGradingTemplateService gradingTemplateService)
        {
            this.gradeBookSetupService = gradeBookSetupService;
            this.gradingTemplateService = gradingTemplateService;
        }
        #endregion
        // GET: GradeBook/GradeBookSetup
        public ActionResult Index()
        {
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            var curriculum = SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}");
            ViewBag.Curriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            var list = gradeBookSetupService.GetGradeAndCourseList(curriculum.FirstOrDefault().ItemId.ToInteger(), SchoolId).ToList();
            return View(list);
        }
        public PartialViewResult GetGradeBookGridView(int curriculumId, int pageIndex = 1, string searchString = "", string GradeIds = "", string CourseIds = "", string sortBy = "", List<GradeAndCourse> gradeAndCourseList = null)
        {
            var model = new Pagination<Phoenix.Models.Entities.GradeBook>();
            ViewBag.curriculumId = curriculumId;
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            var list = gradeBookSetupService.GetGradeBookDetailPagination(curriculumId, SchoolId, pageIndex, 6, searchString, GradeIds, CourseIds, sortBy);
            if (list.Any())
                model = new Pagination<Phoenix.Models.Entities.GradeBook>(pageIndex, 6, list.ToList(), list.FirstOrDefault().TotalCount);
            model.RecordCount = list.Count() == 0 ? 0 : list.FirstOrDefault().TotalCount;
            model.LoadPageRecordsUrl = "/GradeBook/GradeBookSetup/GetGradeBookGridView?curriculumId=" + curriculumId + "&pageIndex={0}";
            model.SearchString = searchString;
            ViewBag.gradeAndCourseList = gradeAndCourseList == null ? new List<GradeAndCourse>() : gradeAndCourseList;
            return PartialView("_GradeBookGridView", model);
        }
        public PartialViewResult GetGradeBookListView(int curriculumId, int pageIndex = 1, string searchString = "", string GradeIds = "", string CourseIds = "", string sortBy = "")
        {
            var model = new Pagination<Phoenix.Models.Entities.GradeBook>();
            ViewBag.curriculumId = curriculumId;
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            var list = gradeBookSetupService.GetGradeBookDetailPagination(curriculumId, SchoolId, pageIndex, 6, searchString, GradeIds, CourseIds, sortBy);
            if (list.Any())
                model = new Pagination<Phoenix.Models.Entities.GradeBook>(pageIndex, 6, list.ToList(), list.FirstOrDefault().TotalCount);
            model.RecordCount = list.Count() == 0 ? 0 : list.FirstOrDefault().TotalCount;
            model.LoadPageRecordsUrl = "/GradeBook/GradeBookSetup/GetGradeBookListView?curriculumId=" + curriculumId + "&pageIndex={0}";
            model.SearchString = searchString;
            return PartialView("_GradeBookListView", model);
        }
        public ActionResult AddEditGradeBook(string id = "", string cid = "")
        {
            var gradeBookEditModel = GetGradeBookDetail(id, cid);
            return View(gradeBookEditModel);
        }
        public ActionResult CopyGradeBook(string id = "", string cid = "")
        {
            var gradeBookEditModel = GetGradeBookDetail(id, cid);
            gradeBookEditModel.IsCopyGradebook = true;
            gradeBookEditModel.GradeBookId = 0;
            gradeBookEditModel.GradeBookName = string.Empty;
            gradeBookEditModel.CourseIds = new List<long>();
            gradeBookEditModel.SchoolGradeIds = new List<long>();
            return View("AddEditGradeBook", gradeBookEditModel);
        }
        private GradeBookEdit GetGradeBookDetail(string id = "", string cid = "")
        {
            var gradeBookEditModel = new GradeBookEdit();
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            var curriculumId = EncryptDecryptHelper.DecryptUrl(cid).ToInteger();
            var course = SelectListHelper.GetSelectListData(ListItems.Course, "CurriculumId = " + curriculumId, $"{schoolId}").Select(x => new SelectListItem { Text = x.ItemName, Value = x.ItemId }).ToList();
            var schoolGrades = SelectListHelper.GetSelectListData(ListItems.SchoolGrade, "CurriculumId = " + curriculumId, $"{schoolId}").Select(x => new SelectListItem { Text = x.ItemName, Value = x.ItemId }).ToList();
            var AssessmentType = SelectListHelper.GetSelectListData(ListItems.AssessmentType)
                .Select(x => new SelectListItem { Value = x.ItemId, Text = x.ItemName }).ToList();
            if (!string.IsNullOrEmpty(id))
            {
                var gradebookId = EncryptDecryptHelper.DecryptUrl(id);
                var response = gradeBookSetupService.GetGradeBookDetail(gradebookId.ToLong());
                var gradeBook = EntityMapper<Phoenix.Models.Entities.GradeBook, GradeBookEdit>.Map(response);
                gradeBook.InternalAssessments.ForEach(x =>
                {
                    x.ResultsToConsider = gradeBook.InternalAssessments.Where(y => y.InternalAssessmentId == x.InternalAssessmentId).Select(v => v.ResultConsideredId).ToList();
                });
                gradeBook.InternalAssessments = gradeBook.InternalAssessments.DistinctBy(x => x.InternalAssessmentId).ToList();
                var index = 0;
                gradeBook.InternalAssessments.ForEach(x => x.Index = index++);
                //------------------------If AssessmentType is External Examination------------------------------------//
                gradeBook.ExternalExaminationIds = string.Empty;
                gradeBook.SubExternalExaminationIds = string.Empty;
                if (gradeBook.Standardizeds.Any(x => x.AssessmentTypeid == 5))
                {
                    gradeBook.ExternalExaminationIds = gradeBook.Standardizeds.Where(x => x.AssessmentTypeid == 5).FirstOrDefault().ExternalExaminationIds;
                    gradeBook.SubExternalExaminationIds = gradeBook.Standardizeds.Where(x => x.AssessmentTypeid == 5).FirstOrDefault().SubExternalExaminationIds;
                }
                //-----------------------------------------------------------------------------------------------------//
                GetGradeBookAssessmentTypeFormData(string.Empty, gradeBook.AssignmentQuizSetups, gradeBook.Standardizeds, gradeBook.InternalAssessments, gradeBook.ExternalExaminationIds);
                gradeBookEditModel = gradeBook;               
            }
            ViewBag.Course = course;
            ViewBag.SchoolGrades = schoolGrades;
            ViewBag.AssessmentType = AssessmentType;
            return gradeBookEditModel;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEditGradeBook(GradeBookEdit gradeBookEdit)
        {
            gradeBookEdit.CourseIds.RemoveAll(x => x == 0);
            gradeBookEdit.AssessmentTypeIds.RemoveAll(x => x == 0);
            gradeBookEdit.SchoolGradeIds.RemoveAll(x => x == 0);
            gradeBookEdit.Modes = gradeBookEdit.GradeBookId > 0 ? TranModes.Update : TranModes.Insert;
            gradeBookEdit.SchoolId = SessionHelper.CurrentSession.SchoolId;
            gradeBookEdit.Createdby = SessionHelper.CurrentSession.Id;
            
            var tempStandardizeds = new List<StandardizedExaminationSetup>();
            if (gradeBookEdit.Standardizeds.Any(x => x.ExaminationSetup != null && x.ExaminationSetup.Count() > 0))
            {
                var listExaminationSetup = gradeBookEdit.Standardizeds.Where(x => x.ExaminationSetup != null && x.ExaminationSetup.Count() > 0).ToList();
                foreach (var item in listExaminationSetup)
                {
                    item.ExaminationSetup.ToList().ForEach(x =>
                    {
                        tempStandardizeds.Add(new StandardizedExaminationSetup()
                        {
                            AssessmentTypeid = item.AssessmentTypeid,
                            CheckBoxDetailId = x
                        });
                    });
                }
                gradeBookEdit.Standardizeds.AddRange(tempStandardizeds);
            }

            if (gradeBookEdit.IsCopyGradebook)
            {
                gradeBookEdit.InternalAssessments.ForEach(x =>
                {
                    x.GradebookId = 0;
                    x.InternalAssessmentId = 0;
                });
                gradeBookEdit.AssignmentQuizSetups.ForEach(x =>
                {
                    x.GradebookId = 0;
                    x.AssignmentQuizSetupId = 0;
                });
            }
            gradeBookEdit.Standardizeds.RemoveAll(x => x.CheckBoxDetailId == 0);
            var gradeBook = EntityMapper<GradeBookEdit, Phoenix.Models.Entities.GradeBook>.Map(gradeBookEdit);
            var result = gradeBookSetupService.SaveGradeBookForm(gradeBook);
            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteGradeBookDetail(string GradebookId = "")
        {
            bool result = false;
            if (!string.IsNullOrEmpty(GradebookId))
            {
                var gradebookId = EncryptDecryptHelper.DecryptUrl(GradebookId);
                Phoenix.Models.Entities.GradeBook gradeBook = new Phoenix.Models.Entities.GradeBook();
                gradeBook.GradeBookId = gradebookId.ToLong();
                gradeBook.Modes = TranModes.Delete;
                result = gradeBookSetupService.DeleteGradeBookDetail(gradeBook);
            }
            OperationDetails op = new OperationDetails(result);
            if (result)
            {
                op.Message = LocalizationHelper.DeleteSuccessMessage;
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult GetGradeBookAssessmentTypeForm(string assessmentTypeIds, List<AssignmentQuizSetup> setups = null, List<StandardizedExaminationSetup> standardizeds = null, List<InternalAssessment> internalAssessments = null)
        {
            GetGradeBookAssessmentTypeFormData(assessmentTypeIds, setups, standardizeds, internalAssessments);
            return PartialView("_GradebookAssessmentTypeOptionList", assessmentTypeIds);
        }
        [NonAction]
        public void GetGradeBookAssessmentTypeFormData(string assessmentTypeIds, List<AssignmentQuizSetup> setups = null, List<StandardizedExaminationSetup> standardizeds = null, List<InternalAssessment> internalAssessments = null, string ExternalExaminationIds = "")
        {
            var ExternalSubExamination = new SelectList(string.Empty);
            if (!string.IsNullOrEmpty(ExternalExaminationIds))
            {
                ExternalSubExamination = new SelectList(SelectListHelper.GetSelectListData(ListItems.ExternalSubExamination, "ExternalExaminationId in (" + ExternalExaminationIds + ")", $"{null}").ToList(), "ItemId", "ItemName");
            }
            ViewBag.ExternalSubExamination = ExternalSubExamination;
            ViewBag.AssignmentQuizSetup = setups ?? new List<AssignmentQuizSetup>();
            ViewBag.Standardizeds = standardizeds ?? new List<StandardizedExaminationSetup>();
            ViewBag.InternalAssessment = internalAssessments ?? new List<InternalAssessment>();
            ViewBag.StandardizedAssessment = SelectListHelper.GetSelectListData(ListItems.StandardizedAssessment).ToList();
            ViewBag.ExternalExamination = new SelectList(SelectListHelper.GetSelectListData(ListItems.ExternalExamination).ToList(), "ItemId", "ItemName");

            // 7 -> Student Demographic
            ViewBag.StudentDemographic = SelectListHelper.GetSelectListData(ListItems.SubAssessmentType, "7");
            GetGradingTemplateForForm();
        }
        public JsonResult GetExternalSubExaminationList(string ExternalExaminationId = "")
        {
            var ExternalSubExamination = new SelectList(SelectListHelper.GetSelectListData(ListItems.ExternalSubExamination, "ExternalExaminationId in (" + ExternalExaminationId + ")", $"{null}").ToList(), "ItemId", "ItemName");//,
            return Json(ExternalSubExamination, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        public void GetGradingTemplateForForm()
        {
            ViewBag.FormulaData = gradeBookSetupService.GetGradebookFormulas(SessionHelper.CurrentSession.SchoolId);
            ViewBag.GradingTemplate = gradingTemplateService.GetGradtingTemplates((int)SessionHelper.CurrentSession.SchoolId)
                .Select(x => new SelectListItem { Text = x.GradingTemplateTitle.ToString(), Value = x.GradingTemplateId.ToString() }).ToList();
        }
        public PartialViewResult GetInternalAssessmentForm(int number, List<InternalAssessment> internalAssessmentList = null)
        {
            GetGradingTemplateForForm();
            InternalAssessment internalAssessment = new InternalAssessment { Index = number };
            if (internalAssessmentList != null)
            {
                var list = internalAssessmentList.Where(x => x.Index == number).ToList();
                if (list.Any())
                {
                    internalAssessment = list.FirstOrDefault();
                }
            }
            else
            {
                internalAssessmentList = new List<InternalAssessment>();
            }
            ViewData["internalAssessment"] = internalAssessmentList.Where(x => (InternalAssessmentRowType)x.RowType != InternalAssessmentRowType.Comment).ToList();
            ViewBag.FormulaData = gradeBookSetupService.GetGradebookFormulas(SessionHelper.CurrentSession.SchoolId);
            return PartialView("_InternalAssessmentForm", internalAssessment);
        }
        public PartialViewResult LoadInternalAssessmentGridData(List<InternalAssessment> internalAssessmentList = null)
        {
            if (internalAssessmentList == null)
            {
                internalAssessmentList = new List<InternalAssessment>();
            }

            return PartialView("_InternalAssessmentGridData", internalAssessmentList);
        }
        public PartialViewResult LoadFormulaForm(long FormulaId = 0)
        {
            GradebookFormulaEdit gradebookFormula = new GradebookFormulaEdit();
            if (FormulaId > 0)
            {
                var result = gradeBookSetupService.GetGradebookFormulaDetailById(FormulaId);
                if (result != null)
                {
                    gradebookFormula = EntityMapper<Phoenix.Models.Entities.GradebookFormula, GradebookFormulaEdit>.Map(result);
                }
            }
            return PartialView("_FormulaForm", gradebookFormula);
        }
        public PartialViewResult LoadFormulaFromGradebookForm(GradebookFormulaEdit gradebookFormula = null)
        {
            return PartialView("_FormulaForm", gradebookFormula);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult SaveFormula(GradebookFormulaEdit gradebookFormula)
        {
            gradebookFormula.SchoolId = SessionHelper.CurrentSession.SchoolId;
            var formula = EntityMapper<GradebookFormulaEdit, GradebookFormula>.Map(gradebookFormula);
            var result = gradeBookSetupService.GradebookFormulaCU(formula);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Formula()
        {
            return View();
        }
        public JsonResult LoadFormulas()
        {
            var result = gradeBookSetupService.GetGradebookFormulas(SessionHelper.CurrentSession.SchoolId);
            var dataList = new
            {
                aaData = (from item in result
                          select new
                          {
                              item.Assessments,
                              item.FormulaId,
                              item.Formula,
                              item.FormulaDescription,
                              Actions = GetActionLinks(item)
                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        private string GetActionLinks(GradebookFormula model)
        {
            var isPermissionEnabled = true;//SessionHelper.CurrentSession.IsAdmin || CurrentPagePermission.CanEdit;
            StringBuilder actions = new StringBuilder();
            actions.Append(isPermissionEnabled ? $"<a class='mr-2' href='javascript:void(0)' data-toggle='tooltip'  data-original-title='" + ResourceManager.GetString("Gradebook.Formula.EditFormula") + $"' onclick='formulaForm.editFormula(this, {model.FormulaId})'><img src='/Content/vle/img/svg/pencil-big.svg' /></a>" : string.Empty);

            return actions.ToString();
        }
        public ActionResult LoadRuleSetupForm(string internalAssessmentId)
        {
            var response = gradeBookSetupService.GetGradebookRuleSetups(internalAssessmentId);
            return PartialView("_RuleProcessingSetup", response.Any() ? response.FirstOrDefault() : new GradebookRuleSetup());
        }
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult RuleSetupForm(GradebookRuleSetup gradebookRules)
        {
            gradebookRules.RuleSetupMappings.RemoveAll(x => x.SubInternalAssessmentId == 0);
            gradebookRules.CreatedBy = SessionHelper.CurrentSession.Id;
            var assessments = string.Join(",", gradebookRules.RuleSetupMappings.Select(x => x.SubInternalAssessmentId));

            var response = gradeBookSetupService.ProcessingRuleSetupCU(gradebookRules);
            if (response <= 0)
                return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
            else
                return Json(new OperationDetails(true)
                {
                    RelatedHtml = gradebookRules.Formula,  
                    CssClass = response.ToString(),
                    InsertedRowId = (int)gradebookRules.InternalAssessmentId,
                    Identifier = assessments
                }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult DeleteRuleSetup(long ruleSetupId, long internalAssessmentId)
        {
            var result = gradeBookSetupService.DeleteRuleProcessingSetup(ruleSetupId);
            return Json(new OperationDetails(result) { InsertedRowId = (int)internalAssessmentId }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFormulaList()
        {
            var result = gradeBookSetupService.GetGradebookFormulas(SessionHelper.CurrentSession.SchoolId).Select(x => new SelectListItem { Text = x.FormulaDescription, Value = x.FormulaId.ToString() });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ValidateGradeandCourseCombination(string courseIds, string gradeIds)
        {
            var response = gradeBookSetupService.ValidateGradeAndCourse(courseIds, gradeIds);

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGradebookFilters(int curriculumId)
        {
            var list = gradeBookSetupService.GetGradeAndCourseList(curriculumId, SessionHelper.CurrentSession.SchoolId).ToList();
            var gridViewFilterHtml = ControllerExtension.RenderPartialViewToString(this, "_GridviewGradeSectionFilter", list);
            var listViewFilterHtml = ControllerExtension.RenderPartialViewToString(this, "_ListviewGradeSectionFilter", list);
            return Json(new { gridViewFilterHtml, listViewFilterHtml }, JsonRequestBehavior.AllowGet);
        }
    }
}