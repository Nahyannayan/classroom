﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.GradeBook
{
    public class GradeBookAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "GradeBook";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "GradeBook_default",
                "GradeBook/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}