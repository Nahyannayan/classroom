﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ParentCorner
{
    public class ParentCornerAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ParentCorner";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ParentCorner_default",
                "ParentCorner/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}