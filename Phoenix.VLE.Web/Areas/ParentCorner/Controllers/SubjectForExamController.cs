﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ParentCorner.Controllers
{
    public class SubjectForExamController : BaseController
    {
        private readonly ISetSubjectService _setSubjectService;
        private readonly IPaymentService _feePaymentService;
        private ILoggerClient _loggerClient;

        public SubjectForExamController
        (
            ISetSubjectService setSubjectService
           , IPaymentService feePaymentService
        )
        {
            _setSubjectService = setSubjectService;
            _feePaymentService = feePaymentService;
            _loggerClient = LoggerClient.Instance;
        }
        // GET: ParentCorner/SubjectForExam
        public ActionResult Index()
        {
            var viewName = "SubjectsForExam";
            List<StudentDetails> lstSubjectDetails = new List<StudentDetails>();
            try
            {
                _loggerClient.LogWarning("Inside Index");
                var studList = SessionHelper.CurrentSession.FamilyStudentList;
                var selectedStudDetls = SessionHelper.CurrentSession.CurrentSelectedStudent;
                int count = 0;
                var PCPayGateWayStudentNo = SessionExtensions.GetObject<string>("PCPayGateWayStudentNo");
                foreach (var item in studList)
                {
                    StudentDetails obj = new StudentDetails();
                    obj.StudentID = Convert.ToString(item.StudentId);
                    obj.StudentNo = Convert.ToString(item.StudentNumber);
                    obj.StudentName = item.FirstName + " " + item.LastName;
                    obj.Grade = Convert.ToString(item.GradeDisplay);

                    if (!string.IsNullOrEmpty(PCPayGateWayStudentNo))
                    {
                        if (PCPayGateWayStudentNo == item.StudentNumber)
                        {
                            obj.IsSelected = true;
                            SessionExtensions.RemoveObject("PCPayGateWayStudentNo");
                        }
                    }
                    else
                    {
                        obj.IsSelected = selectedStudDetls.StudentNumber == item.StudentNumber ? true : false;
                    }
                    if (obj.IsSelected)
                    {
                        obj.LstSubjectDetails = _setSubjectService.GetSubjectList(item.StudentNumber);
                        var lastSubDate = Convert.ToDateTime(obj.LstSubjectDetails.Select(m => m.ExamEndDate).FirstOrDefault());
                        obj.LastDateForSubmition = lastSubDate.ToString("dd MMM yyyy"); //DateTime.Today.ToString("dd MMM yyyy");
                        IEnumerable<StudentDetails> lstExamPapers = new List<StudentDetails>();
                        lstExamPapers = _setSubjectService.GetExamPaperList(item.StudentNumber);
                        IEnumerable<ExamPaper> objLstExamPaper = new List<ExamPaper>();
                        obj.LstExamPapers = lstExamPapers == null || lstExamPapers.Count() == 0 ? objLstExamPaper : lstExamPapers.Select(m => m.SubjectwiseExamPaperDetailsList).FirstOrDefault();
                        obj.TotalAmount = obj.LstExamPapers.Where(x => (x.ApprovalStatus.Trim().ToLower() == "approved from school" || x.ApprovalStatus.Trim().ToLower() == "pre approved") && (x.PaymentStatus.ToLower() == "payment due")).Sum(x => x.PaperCost);
                        obj.PaymentTypeID = lstExamPapers.Select(m => m.PaymentTypeID).FirstOrDefault();
                        obj.PaymentProcessingCharge = lstExamPapers.Select(m => m.PaymentProcessingCharge).FirstOrDefault();
                        obj.OnlinePaymentAllowed = lstExamPapers.Select(m => m.OnlinePaymentAllowed).FirstOrDefault();
                    }                   
                    lstSubjectDetails.Add(obj);
                    count++;
                }
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            return View(viewName, lstSubjectDetails);
            //return RedirectToAction("SubjectsForExam");
            //return View();
        }

        //public ActionResult SubjectsForExam()
        //{
        //    List<StudentDetails> lstSubjectDetails = new List<StudentDetails>();
        //    try
        //    {
        //        var studList = SessionHelper.CurrentSession.FamilyStudentList;
        //        int count = 0;
        //        foreach (var item in studList)
        //        {
        //            StudentDetails obj = new StudentDetails();
        //            obj.StudentID = Convert.ToString(item.StudentId);
        //            obj.StudentNo = Convert.ToString(item.StudentNumber);
        //            obj.StudentName = item.FirstName + " " + item.LastName;
        //            obj.Grade = Convert.ToString(item.GradeDisplay);
        //            obj.IsSelected = count == 0 ? true : false;
        //            obj.LastDateForSubmition = DateTime.Today.ToString("dd MMM yyyy");
        //            obj.LstSubjectDetails = _setSubjectService.GetSubjectList(item.StudentNumber);
        //            obj.LstExamPapers = _setSubjectService.GetExamPaperList(item.StudentNumber);
        //            obj.TotalAmount = obj.LstExamPapers.Where(x => x.PaymentStatus.Trim() == "Payment Due").Sum(x => x.PaperCost);
        //            lstSubjectDetails.Add(obj);
        //            count++;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerClient.LogException(ex);
        //    }
        //    return View(lstSubjectDetails);
        //}

        public ActionResult GetSubjectsByStudent(string studentNo)
        {
            IEnumerable<SubjectDetails> objSubList = new List<SubjectDetails>();
            objSubList = _setSubjectService.GetSubjectList(studentNo);
            return PartialView("_tblSubjectsGrid", objSubList);
        }

        public ActionResult GetSubjectPaperDetails(string studentNo)
        {
            StudentDetails obj = new StudentDetails();
            var studList = SessionHelper.CurrentSession.FamilyStudentList.Where(m => m.StudentNumber == studentNo).FirstOrDefault();
            obj.StudentID = Convert.ToString(studList.StudentId);
            obj.StudentNo = Convert.ToString(studList.StudentNumber);
            obj.StudentName = studList.FirstName + " " + studList.LastName;
            obj.Grade = Convert.ToString(studList.GradeDisplay);
            obj.IsSelected = true;
            obj.LastDateForSubmition = DateTime.Today.ToString("dd MMM yyyy");
            obj.LstSubjectDetails = _setSubjectService.GetSubjectList(studList.StudentNumber);
            IEnumerable<ExamPaper> lstExam = new List<ExamPaper>();
            var lstExamPapers = _setSubjectService.GetExamPaperList(studList.StudentNumber);
            obj.LstExamPapers = lstExamPapers.Select(m => m.SubjectwiseExamPaperDetailsList).FirstOrDefault();
            obj.LstExamPapers = obj.LstExamPapers == null ? lstExam : obj.LstExamPapers;
            obj.TotalAmount = obj.LstExamPapers == null ? 0 : obj.LstExamPapers.Where(x => (x.ApprovalStatus.Trim().ToLower() == "approved from school" || x.ApprovalStatus.Trim().ToLower() == "pre approved") && (x.PaymentStatus.ToLower() == "payment due")).Sum(x => x.PaperCost);
            obj.PaymentTypeID = lstExamPapers.Select(m => m.PaymentTypeID).FirstOrDefault();
            obj.PaymentProcessingCharge = lstExamPapers.Select(m => m.PaymentProcessingCharge).FirstOrDefault();
            obj.OnlinePaymentAllowed = lstExamPapers.Select(m => m.OnlinePaymentAllowed).FirstOrDefault();
            return PartialView("_StudentSubjects", obj);
        }
        public JsonResult SavePaperSelection(StudentDetails obj)
        {
            string data = string.Empty;
            //IEnumerable<PaperDetails> lstPaper = new List<PaperDetails>();
            //lstPaper = obj.LstPaperDetails;
            //lstPaper = lstPaper.Select(m =>
            //{
            //    m.PaperSetupID = m.ExamSetupID;
            //    return m;
            //});
            string jsonPaperSetupId = JsonConvert.SerializeObject(obj.LstPaperDetails.Select(m => new { m.PaperSetupID }));
            data = "{\"StudentID\":\"" + obj.StudentNo + "\",\"TotalAmount\":\"" + Convert.ToString(obj.TotalAmount) + "\",\"ExamPaperSetupID\":" + jsonPaperSetupId + "}";

            string jsonEvents = _setSubjectService.SaveSubjectSelection(data);
            var jsonResult = new { events = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemovePaperSelection(int PaperSetupId)
        {
            string data = string.Empty;
            data = "{\"examPaperSetDetID\":\"" + PaperSetupId + "\"}";
            string jsonEvents = _setSubjectService.RemoveSubjectSelection(data);
            var jsonResult = new { events = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PayOnlineExamFees(string studentNo)
        {
            string jsonEvents = ""; OnlinePayResponse objResult = new OnlinePayResponse();
            string PaymentTo = "EXAM";
            IEnumerable<StudentDetails> objExamList = new List<StudentDetails>();

            objExamList = _setSubjectService.GetExamPaperList(studentNo);
            var lstPaperDetails = objExamList.Select(m => m.SubjectwiseExamPaperDetailsList).FirstOrDefault();
            var onlinePaymentAllow = objExamList.Select(m => m.OnlinePaymentAllowed).FirstOrDefault();
            var paymentTypeID = objExamList.Select(m => m.PaymentTypeID).FirstOrDefault();
            var paymentProcessingCharge = objExamList.Select(m => m.PaymentProcessingCharge).FirstOrDefault();

            lstPaperDetails = lstPaperDetails == null ? null : lstPaperDetails.Where(x => (x.ApprovalStatus.Trim().ToLower() == "approved from school" || x.ApprovalStatus.Trim().ToLower() == "pre approved") && (x.PaymentStatus.ToLower() == "payment due")).ToList();
            var TotalAmount = lstPaperDetails.Where(x => (x.ApprovalStatus.Trim().ToLower() == "approved from school" || x.ApprovalStatus.Trim().ToLower() == "pre approved") && (x.PaymentStatus.ToLower() != "paid")).Sum(x => x.PaperCost);
            var percentageCalcln = GetPercentageAmount(TotalAmount, paymentProcessingCharge);
            TotalAmount += percentageCalcln;

            var visitorInfo = new VisitorInfo();
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();
            string IpDetails = IpAddress + ":" + HostIpAddress;
            string data = string.Empty; //string ApdId = ""; string feeCollType = "";

            List<CommonStudentFee> objVM = new List<CommonStudentFee>
            {
                new CommonStudentFee
                {
                    STU_NO = studentNo,
                    OnlinePaymentAllowed = Convert.ToString(onlinePaymentAllow),
                    PaymentTypeID = Convert.ToString(paymentTypeID),
                    PaymentProcessingCharge = paymentProcessingCharge,
                    PayingAmount = TotalAmount,
                    IpAddress = IpDetails
                }
            };
            if (objVM != null && objVM.Count() > 0)
            {
                foreach (var item in objVM)
                {
                    List<CommonFeeDetails> feeList = new List<CommonFeeDetails>();
                    foreach (var paper in lstPaperDetails)
                    {
                        CommonFeeDetails objFee = new CommonFeeDetails();
                        objFee.FeeID = Convert.ToString(paper.ExamFeeId);
                        objFee.OriginalAmount = 0;
                        objFee.DueAmount = 0;
                        objFee.PayAmount = paper.PaperCost;
                        objFee.DiscAmount = 0;
                        objFee.FeeDescription = paper.PaperName;
                        objFee.ActivityRefID = paper.ExamPaperSetDetID;
                        feeList.Add(objFee);
                    }
                    item.StudFeeDetails = feeList;
                }
                //objResult = _feePaymentService.SubmitOnlinePaymentRequest(PaymentTo, objVM, ApdId, feeCollType);
                objResult = _feePaymentService.SubmitOnlinePaymentRequestExam(PaymentTo, objVM, "/parentcorner/SubjectForExam");
            }

            jsonEvents = JsonConvert.SerializeObject(objResult);
            var jsonResult = new { result = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public double GetPercentageAmount(double num, double percentage)
        {
            var number = num;
            //The percent that we want to get.
            //i.e. We want to get 22% of 90.
            var percentToGet = percentage;

            //Turn our percent into a decimal figure.
            //22 will become 0.22 and 60 will become 0.6
            var percentAsDecimal = (percentToGet / 100);

            //Multiply the percent decimal by the number.
            var percent = percentAsDecimal * number;
            return percent;
        }
    }
}