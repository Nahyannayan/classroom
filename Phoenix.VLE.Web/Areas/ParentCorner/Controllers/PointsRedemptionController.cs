﻿using Newtonsoft.Json;
using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ParentCorner.Controllers
{
    public class PointsRedemptionController : BaseController
    {

        private ILoggerClient _loggerClient;
        private readonly IGetRewardService _rewardsService;
        private readonly IFeePaymentsService _feePaymentService;
        public PointsRedemptionController(IGetRewardService rewardsService, IFeePaymentsService feePaymentService)
        {
            _rewardsService = rewardsService;
            _feePaymentService = feePaymentService;
            _loggerClient = LoggerClient.Instance;
        }

        // GET: ParentCorner/PointsRedemption
        public ActionResult Index()
        {
            RedemptionModel objModel = new RedemptionModel();
            List<PCStudentInfo> lstStudensInfo = new List<PCStudentInfo>();
            //var selSchoolId = SessionHelper.CurrentSession.CurrentSelectedStudent.SchoolId;
            var selectedStudNo = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber;
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            int counter = 0;
            foreach (var student in studList)
            {
                PCStudentInfo objStud = new PCStudentInfo()
                {
                    StudentId = student.StudentId,
                    StudentImg = student.StudentImage,
                    StudentName = student.FirstName + " " + student.LastName,
                    StudentNo = student.StudentNumber,
                    SchoolId = student.StudentId,
                    SchoolName = student.SchoolName,
                    Grade = student.GradeDisplay,
                    Section = Convert.ToString(student.SectionName),
                    IsSelected = selectedStudNo == student.StudentNumber ? true : false
                };
                lstStudensInfo.Add(objStud);
                counter++;
            }

            objModel.UserName = SessionHelper.CurrentSession.FullName;
            objModel.UserEMailID = SessionHelper.CurrentSession.Email;
            objModel.LstStudentsInfo = lstStudensInfo;
            var selectedStud = lstStudensInfo.Where(m => m.IsSelected == true).Select(m => m.StudentNo).FirstOrDefault();
            objModel.Rewards = _rewardsService.GetRewardRedemptions(selectedStud, "0", "SCHOOL");
            List<StudentsFeeDetails> LstStudentFee = new List<StudentsFeeDetails>();
            LstStudentFee = objModel.Rewards.studentsFeeDetails;
            if (LstStudentFee != null)
            {
                LstStudentFee = LstStudentFee.Select(m =>
                {
                    m.DiscAmount = m.FeeDetail.Sum(n => n.DiscAmount);
                    m.DueAmount = m.FeeDetail.Sum(n => n.DueAmount);
                    m.PayAmount = m.FeeDetail.Sum(n => n.PayAmount);
                    m.Source = "SCHOOL";
                    m.StudentName = SessionHelper.CurrentSession.FamilyStudentList.Where(n => n.StudentNumber == m.STU_NO).Select(n => n.FirstName + " " + n.LastName).FirstOrDefault();
                    return m;
                }).ToList();
            }
            objModel.Rewards.studentsFeeDetails = LstStudentFee;
            return View(objModel);
        }

        public ActionResult GetFeeDetails(string students, string isPaySibling = "0", string paymentTo = "SCHOOL")
        {
            paymentTo = "SCHOOL";
            string[] studts = students.Split(',');
            FeeDetailsVM objVM = new FeeDetailsVM();
            IEnumerable<StudentsFeeDetails> LstStudentFee = new List<StudentsFeeDetails>();
            var studsRewards = _rewardsService.GetRewardRedemptions(studts[0], isPaySibling, paymentTo);
            LstStudentFee = studsRewards.studentsFeeDetails;
            if (LstStudentFee != null)
            {
                LstStudentFee = LstStudentFee.Select(m =>
                {
                    m.DiscAmount = m.FeeDetail.Sum(n => n.DiscAmount);
                    m.DueAmount = m.FeeDetail.Sum(n => n.DueAmount);
                    m.PayAmount = m.FeeDetail.Sum(n => n.PayAmount);
                    m.Source = paymentTo;
                    m.StudentName = SessionHelper.CurrentSession.FamilyStudentList.Where(n => n.StudentNumber == m.STU_NO).Select(n => n.FirstName + " " + n.LastName).FirstOrDefault();
                    return m;
                }).ToList();
            }

            ViewBag.Source = "SCHOOL";
            return PartialView("_FeeDetails", LstStudentFee);
        }
        public JsonResult GetScheduleFee(string studNo)
        {
            string jsonEvents = "";
            var lstScheduleFee = _feePaymentService.GetFeeSchedule(studNo);
            jsonEvents = JsonConvert.SerializeObject(lstScheduleFee);
            var jsonResult = new { scheduleFee = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRedemptionHistory(string students)
        {
            string jsonEvents = "";
            string[] studsList = students.Split();
            List<RedemptionHistory> lstRedHist = new List<RedemptionHistory>();
            var parentUserId = SessionHelper.CurrentSession.UserName;
            foreach (var stud in studsList)
            {
                var lstRes = _rewardsService.GetRedemptionHistory(stud, parentUserId);
                lstRedHist.AddRange(lstRes);
            }

            jsonEvents = JsonConvert.SerializeObject(lstRedHist);
            var jsonResult = new { redemptionHistory = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SubmitRewardsRedemption(IEnumerable<StudentsFeeDetails> objVM)
        {
            RewardRedemption rewardRedemption = new RewardRedemption();
            RedemptionResponse objResponse = new RedemptionResponse();
            string paymentTo = "SCHOOL"; string jsonEvents = "";
            var firstStud = objVM.Select(m => m.STU_NO).FirstOrDefault();
            var isPaySibling = objVM.Select(m => m.IsPaySibling).FirstOrDefault();

            rewardRedemption = _rewardsService.GetRewardRedemptions(firstStud, isPaySibling, paymentTo);

            var visitorInfo = new VisitorInfo();
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();
            string IpDetails = IpAddress + ":" + HostIpAddress;

            List<CommonStudentFee> lstFee = new List<CommonStudentFee>();
            foreach (var stFee in rewardRedemption.studentsFeeDetails)
            {
                var selStud = objVM.Where(m => m.STU_NO == stFee.STU_NO).Select(m => m.FeeDetail).FirstOrDefault();
                CommonStudentFee obj = new CommonStudentFee();
                obj.STU_NO = stFee.STU_NO;
                obj.IpAddress = IpDetails;
                obj.OnlinePaymentAllowed = stFee.OnlinePaymentAllowed;
                obj.UserMessageforOnlinePaymentBlock = stFee.UserMessageforOnlinePaymentBlock;
                obj.PaymentTypeID = stFee.PaymentTypeID;
                obj.PayingAmount = selStud.Sum(m => m.PayAmount);
                obj.PaymentProcessingCharge = stFee.PaymentProcessingCharge;
                obj.PayMode = stFee.PayMode;
                obj.DiscountDetails = stFee.DiscountDetails;

                List<CommonFeeDetails> lstFeeByStu = new List<CommonFeeDetails>();
                foreach (var fe in stFee.FeeDetail)
                {
                    var payAmount = selStud.Where(m => m.FeeID == fe.FeeID).Select(m => m.PayAmount).FirstOrDefault();
                    CommonFeeDetails objFe = new CommonFeeDetails();
                    objFe.BlockPayNow = fe.BlockPayNow;
                    objFe.AdvancePaymentAvailable = fe.AdvancePaymentAvailable;
                    objFe.FeeDescription = fe.FeeDescription;
                    objFe.FeeID = fe.FeeID;
                    objFe.DueAmount = fe.DueAmount;
                    objFe.PayAmount = payAmount;
                    objFe.DiscAmount = fe.DiscAmount;
                    objFe.OriginalAmount = fe.OriginalAmount;
                    objFe.ActivityFeeCollType = fe.ActivityFeeCollType;
                    objFe.ActivityRefID = fe.ActivityRefID;
                    objFe.AdvanceDetails = fe.AdvanceDetails;
                    lstFeeByStu.Add(objFe);
                }
                obj.StudFeeDetails = lstFeeByStu;

                lstFee.Add(obj);
            }

            rewardRedemption.FeeDetails = lstFee;

            rewardRedemption.logUserName = SessionHelper.CurrentSession.UserName;
            if (rewardRedemption != null)
            {
                objResponse = _rewardsService.PostRewardRedemptions(rewardRedemption, paymentTo);
            }

            if (objResponse.Response != null && objResponse.Response.success == "true")
            {
                var reedResponse = _rewardsService.PostRewardRedeem("SCHOOL", objResponse.RedemptionRefNo, objResponse.TotalAmount);
            }
            jsonEvents = JsonConvert.SerializeObject(objResponse);
            var jsonResult = new { rewardsResult = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SubmitRewardsReedeem(string paymentTo, string redemptionRefNo, double totalAmount)
        {
            paymentTo = "SCHOOL"; string jsonEvents = "";
            jsonEvents = _rewardsService.PostRewardRedeem(paymentTo, redemptionRefNo, totalAmount);
            var jsonResult = new { rewardsResult = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRedemptionPoints(string student, string isPaySibling = "0")
        {
            string jsonEvents = string.Empty;
            var result = _rewardsService.GetRewardRedemptions(student, isPaySibling, "SCHOOL");
            jsonEvents = JsonConvert.SerializeObject(result);
            var jsonResult = new { rewardsResult = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintFeesShedule(string id)
        {
            var lstScheduleFee = _feePaymentService.GetFeeSchedule(id);
            var student = SessionHelper.CurrentSession.FamilyStudentList.FirstOrDefault(n => n.StudentNumber == id);
            ViewBag.StudentName = (student.FirstName + " " + student.LastName).Trim();
            ViewBag.Grade = student.GradeDisplay;
            FeeSchedule fd = new FeeSchedule();
            return View(lstScheduleFee);
        }

    }
}