﻿using System.Collections.Generic;
using Phoenix.Common.Logger;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System.Linq;
using System.Web.Mvc;
using System;
using Phoenix.Common.Helpers;
using Phoenix.Common;
using System.Text;
using Newtonsoft.Json;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models.EditModels;
using PdfSharp.Pdf;
using Phoenix.Common.Helpers.Extensions;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using System.IO;
using PdfSharp;
using Phoenix.Common.Localization;

namespace Phoenix.VLE.Web.Areas.ParentCorner.Controllers
{
    public class FeeController : BaseController
    {

        private readonly IFeePaymentsService _feePaymentService;
        private readonly IFileService _fileService;
        private readonly IEnrollActivitiesService _IEnrollActivitiesService;
        private readonly IPaymentService _paymentService;
        private List<VLEFileType> _fileTypeList;
        private ILoggerClient _loggerClient;


        public FeeController(
              IFileService fileService
            , IFeePaymentsService feePaymentService
            , IEnrollActivitiesService enrollactivitiesservice
            , IPaymentService paymentService

            )
        {
            _fileService = fileService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
            _feePaymentService = feePaymentService;
            _IEnrollActivitiesService = enrollactivitiesservice;
            _paymentService = paymentService;
            _loggerClient = LoggerClient.Instance;
        }

        // GET: ParentCorner/Fee
        public ActionResult Index()
        {
            var viewName = "FeeAndPayments";
            List<FeeDetailVMRoot> lstFeeVMR = new List<FeeDetailVMRoot>();
            if (SessionHelper.CurrentSession.IsParent())
            {
                try
                {
                    _loggerClient.LogWarning("Inside Fee Index");
                    var IsTransport = false;
                    var studList = SessionHelper.CurrentSession.FamilyStudentList;
                    var cntSchool = studList.GroupBy(x => x.SchoolId).Select(x => x.Key);

                    if (cntSchool.Count() > 1)
                    {

                        IsTransport = true;
                    }
                    else
                    {
                        var lstIsTransportEnable = GetTransportFeePaymentEnabled(studList.Select(x => x.SchoolId).FirstOrDefault().ToString());
                        if (lstIsTransportEnable.Count() > 0)
                        {
                            IsTransport = true;
                        }
                    }
                    ViewBag.IsTransport = IsTransport;
                    ViewBag.AcademicYearsList = GetAcademicYearsList();
                    lstFeeVMR = GetStudentFeeDetailsBySource("SCHOOL");

                    #region "Old"
                    /*
                    var selectedStudDetls = SessionHelper.CurrentSession.CurrentSelectedStudent;
                    var currSelectedStudSchoolId = selectedStudDetls.SchoolId;
                    var isSibling = (studList.Where(m => m.SchoolId == currSelectedStudSchoolId).Count() > 1) ? "1" : "0";
                    //studList = studList.Where(m => m.SchoolId == currSelectedStudSchoolId).ToList();
                    string[] sources; List<string> list = new List<string>();
                    list.Add("SCHOOL");


                    //list.Add("ACTIVITY");
                    sources = list.ToArray();

                    foreach (string value in sources)
                    {
                        FeeDetailVMRoot objVM = new FeeDetailVMRoot();
                        List<FeeDetailsVM> lstFeeVM = new List<FeeDetailsVM>();
                        int i = 0;
                        IEnumerable<PaymentType> lstPaymTyp = new List<PaymentType>();
                        lstPaymTyp = _feePaymentService.GetPaymentTypes(selectedStudDetls.StudentNumber, value);
                        foreach (var item in studList)
                        {
                            FeeDetailsVM objFDM = new FeeDetailsVM();
                            objFDM.Section = Convert.ToString(item.SectionName);
                            objFDM.Grade = item.GradeDisplay;
                            objFDM.SchoolName = item.SchoolName;
                            objFDM.SchoolId = item.SchoolId;
                            objFDM.StudentId = item.StudentId;
                            objFDM.StudentNo = (item.StudentNumber);
                            objFDM.StudentImg = item.StudentImage;
                            objFDM.StudentName = item.FirstName + " " + item.LastName;
                            objFDM.IsSelected = item.SchoolId == currSelectedStudSchoolId ? true : false;

                            IEnumerable<FeeSchedule> lstfeeSch = new List<FeeSchedule>();
                            //IEnumerable<PaymentType> lstPaymTyp = new List<PaymentType>();
                            lstfeeSch = _feePaymentService.GetFeeSchedule(item.StudentNumber);
                            //lstPaymTyp = _feePaymentService.GetPaymentTypes(selectedStudDetls.StudentNumber, value);
                            objFDM.PaymentTypeId = lstPaymTyp.Count() > 0 ? lstPaymTyp.FirstOrDefault().PaymentTypeID : 0;
                            objFDM.ProviderTypeId = lstPaymTyp.Count() > 0 ? lstPaymTyp.FirstOrDefault().ProviderTypeID : 0;
                            //objFDM.TotalOutstanding = lstfeeSch.Select(m => m.Amount).Sum();
                            objFDM.TotalOutstanding = _feePaymentService.GetTotalOutstanding(item.StudentNumber, value, false).Select(m => m.OutstandingAmount).FirstOrDefault();
                            objFDM.LstFeeSchedule = lstfeeSch;
                            objFDM.LstPaymentType = lstPaymTyp;
                            //objFDM.StudentFeeDetls = i == 0 ? _feePaymentService.GetFeeDetails(item.StudentNumber, value, objFDM.ProviderTypeId, isSibling) : new List<StudentsFeeDetails>();

                            //foreach (var fe in objFDM.StudentFeeDetls)
                            //{
                            //    fe.DueAmount = fe.FeeDetail.Sum(m => m.DueAmount);
                            //    fe.DiscAmount = fe.FeeDetail.Sum(m => m.DiscAmount);
                            //    fe.PayAmount = fe.FeeDetail.Sum(m => m.PayAmount);
                            //    fe.Source = value;
                            //}

                            //string allDate = lstAcademicYear.Select(m => m.ACDID).FirstOrDefault();
                            //string[] strDate = allDate.Split('|');
                            //var startDate = Convert.ToDateTime(strDate[0]);
                            //var endDate = Convert.ToDateTime(strDate[1]);
                            //objFDM.FromDatePaymHist = startDate;
                            //objFDM.ToDatePaymHist = endDate;

                            DateTime now = DateTime.Now;
                            objFDM.FromDate = new DateTime(now.Year, now.Month, 1);
                            objFDM.ToDate = objFDM.FromDate.AddMonths(1).AddDays(-1);

                            lstFeeVM.Add(objFDM);
                            ++i;
                        }
                        objVM.Source = value;
                        objVM.data = lstFeeVM;
                        lstFeeVMR.Add(objVM);
                    }

                    ViewBag.AcademicYearsList = GetAcademicYearsList();
                    _loggerClient.LogWarning("out Fee Index");
                    */
                    #endregion
                }
                catch (Exception ex)
                {
                    _loggerClient.LogException(ex);
                }
            }
            return View(viewName, lstFeeVMR);
            //return RedirectToAction("FeeAndPayments");
        }


        /// <summary>
        /// Get student fee details.
        /// </summary>
        /// <param name="feeType"></param>
        /// <returns></returns>
        public ActionResult GetStudentFeeDetails(string feeType)
        {
            List<FeeDetailVMRoot> lstFeeVMR = new List<FeeDetailVMRoot>();
            if (SessionHelper.CurrentSession.IsParent())
            {
                try
                {
                    _loggerClient.LogWarning("Inside Fee Index");
                    var studList = SessionHelper.CurrentSession.FamilyStudentList;
                    ViewBag.AcademicYearsList = GetAcademicYearsList();
                    lstFeeVMR = GetStudentFeeDetailsBySource(feeType);
                }
                catch (Exception ex)
                {
                    _loggerClient.LogException(ex);
                }
            }
            return PartialView("FeePaymentsStudDetails", lstFeeVMR);
        }


        public List<FeeDetailVMRoot> GetStudentFeeDetailsBySource(string sourceType)
        {

            List<FeeDetailVMRoot> lstFeeVMR = new List<FeeDetailVMRoot>();
            if (SessionHelper.CurrentSession.IsParent())
            {
                try
                {
                    _loggerClient.LogWarning("GetStudentFeeDetailsBySource() starting.");

                    var studList = SessionHelper.CurrentSession.FamilyStudentList;
                    var selectedStudDetls = SessionHelper.CurrentSession.CurrentSelectedStudent;
                    var currSelectedStudSchoolId = selectedStudDetls.SchoolId;
                    var isSibling = (studList.Where(m => m.SchoolId == currSelectedStudSchoolId).Count() > 1) ? "1" : "0";
                    //studList = studList.Where(m => m.SchoolId == currSelectedStudSchoolId).ToList();
                    string[] sources; List<string> list = new List<string>();
                    list.Add(sourceType);
                    var cntSchool = studList.GroupBy(x => x.SchoolId).Select(x => x.Key);

                    //list.Add("ACTIVITY");
                    sources = list.ToArray();

                    foreach (string value in sources)
                    {
                        FeeDetailVMRoot objVM = new FeeDetailVMRoot();
                        List<FeeDetailsVM> lstFeeVM = new List<FeeDetailsVM>();
                        int i = 0;
                        IEnumerable<PaymentType> lstPaymTyp = new List<PaymentType>();
                        lstPaymTyp = _feePaymentService.GetPaymentTypes(selectedStudDetls.StudentNumber, value);
                        foreach (var item in studList)
                        {
                            FeeDetailsVM objFDM = new FeeDetailsVM();
                            objFDM.Section = Convert.ToString(item.SectionName);
                            objFDM.Grade = item.GradeDisplay;
                            objFDM.SchoolName = item.SchoolName;
                            objFDM.SchoolId = item.SchoolId;
                            objFDM.StudentId = item.StudentId;
                            objFDM.StudentNo = (item.StudentNumber);
                            objFDM.StudentImg = item.StudentImage;
                            objFDM.StudentName = item.FirstName + " " + item.LastName;
                            objFDM.IsSelected = item.SchoolId == currSelectedStudSchoolId ? true : false;

                            //IEnumerable<FeeSchedule> lstfeeSch = new List<FeeSchedule>();
                            //IEnumerable<PaymentType> lstPaymTyp = new List<PaymentType>();
                            //if(sourceType=="SCHOOL")
                            //{
                            //    lstfeeSch = _feePaymentService.GetFeeSchedule(item.StudentNumber);
                            //}                            
                            //lstPaymTyp = _feePaymentService.GetPaymentTypes(selectedStudDetls.StudentNumber, value);
                            objFDM.PaymentTypeId = lstPaymTyp.Count() > 0 ? lstPaymTyp.FirstOrDefault().PaymentTypeID : 0;
                            objFDM.ProviderTypeId = lstPaymTyp.Count() > 0 ? lstPaymTyp.FirstOrDefault().ProviderTypeID : 0;
                            //objFDM.TotalOutstanding = lstfeeSch.Select(m => m.Amount).Sum();
                            objFDM.TotalOutstanding = _feePaymentService.GetTotalOutstanding(item.StudentNumber, value, false).Select(m => m.OutstandingAmount).FirstOrDefault();
                            //objFDM.LstFeeSchedule = lstfeeSch;
                            objFDM.LstPaymentType = lstPaymTyp;

                            DateTime now = DateTime.Now;
                            objFDM.FromDate = new DateTime(now.Year, now.Month, 1);
                            objFDM.ToDate = objFDM.FromDate.AddMonths(1).AddDays(-1);

                            lstFeeVM.Add(objFDM);
                            ++i;
                        }
                        objVM.Source = value;
                        objVM.data = lstFeeVM;
                        lstFeeVMR.Add(objVM);
                    }
                    _loggerClient.LogWarning("out Fee Index");
                }
                catch (Exception ex)
                {
                    _loggerClient.LogException(ex);
                }
            }
            return lstFeeVMR;
        }

        // Fee and Payments START
        //public ActionResult FeeAndPayments()
        //{
        //    List<FeeDetailVMRoot> lstFeeVMR = new List<FeeDetailVMRoot>();
        //    if (SessionHelper.CurrentSession.IsParent())
        //    {
        //        try
        //        {
        //            _loggerClient.LogWarning("Inside Fee Index");
        //            var studList = SessionHelper.CurrentSession.FamilyStudentList;
        //            string[] sources = { "SCHOOL", "TRANSPORT", "ACTIVITY" };
        //            foreach (string value in sources)
        //            {
        //                FeeDetailVMRoot objVM = new FeeDetailVMRoot();
        //                List<FeeDetailsVM> lstFeeVM = new List<FeeDetailsVM>();
        //                int i = 0;
        //                foreach (var item in studList)
        //                {
        //                    FeeDetailsVM objFDM = new FeeDetailsVM();
        //                    objFDM.Section = Convert.ToString(item.SectionName);
        //                    objFDM.Grade = item.GradeDisplay;
        //                    objFDM.SchoolName = item.SchoolName;
        //                    objFDM.SchoolId = item.SchoolId;
        //                    objFDM.StudentId = item.StudentId;
        //                    objFDM.StudentNo = (item.StudentNumber);
        //                    objFDM.StudentImg = item.StudentImage;
        //                    objFDM.StudentName = item.FirstName + " " + item.LastName;
        //                    objFDM.IsSelected = i == 0 ? true : false;

        //                    IEnumerable<FeeSchedule> lstfeeSch = new List<FeeSchedule>();
        //                    IEnumerable<PaymentType> lstPaymTyp = new List<PaymentType>();
        //                    lstfeeSch = _feePaymentService.GetFeeSchedule(item.StudentNumber);
        //                    lstPaymTyp = _feePaymentService.GetPaymentTypes((item.StudentNumber), value);
        //                    objFDM.PaymentTypeId = lstPaymTyp.Count() > 0 ? lstPaymTyp.FirstOrDefault().PaymentTypeID : 0;
        //                    objFDM.ProviderTypeId = lstPaymTyp.Count() > 0 ? lstPaymTyp.FirstOrDefault().ProviderTypeID : 0;
        //                    //objFDM.TotalOutstanding = lstfeeSch.Select(m => m.Amount).Sum();
        //                    objFDM.TotalOutstanding = _feePaymentService.GetTotalOutstanding(item.StudentNumber, value, false).Select(m => m.OutstandingAmount).FirstOrDefault();
        //                    objFDM.LstFeeSchedule = lstfeeSch;
        //                    objFDM.LstPaymentType = lstPaymTyp;
        //                    objFDM.StudentFeeDetls = i == 0 ? _feePaymentService.GetFeeDetails(item.StudentNumber, value, objFDM.ProviderTypeId, "0") : new List<StudentsFeeDetails>();

        //                    foreach (var fe in objFDM.StudentFeeDetls)
        //                    {
        //                        fe.DueAmount = fe.FeeDetail.Sum(m => m.DueAmount);
        //                        fe.DiscAmount = fe.FeeDetail.Sum(m => m.DiscAmount);
        //                        fe.PayAmount = fe.FeeDetail.Sum(m => m.PayAmount);
        //                        fe.Source = value;
        //                    }
        //                    lstFeeVM.Add(objFDM);
        //                    ++i;
        //                }
        //                objVM.Source = value;
        //                objVM.data = lstFeeVM;
        //                lstFeeVMR.Add(objVM);
        //            }
        //            _loggerClient.LogWarning("Out Fee Index");
        //        }
        //        catch (Exception ex)
        //        {
        //            _loggerClient.LogException(ex);
        //        }
        //    }
        //    return View(lstFeeVMR);
        //}

        public JsonResult GetScheduleFee(string studNo)
        {
            string jsonEvents = "";
            var lstScheduleFee = _feePaymentService.GetFeeSchedule(studNo);
            jsonEvents = JsonConvert.SerializeObject(lstScheduleFee);
            var jsonResult = new { scheduleFee = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPaymentType(string studNo, string source)
        {
            string jsonEvents = "";
            var lstPaymentType = _feePaymentService.GetPaymentTypes(studNo, source);
            jsonEvents = JsonConvert.SerializeObject(lstPaymentType);
            var jsonResult = new { paymentTypes = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPaymentTypeOnStudent(string studNo, string source)
        {
            IEnumerable<PaymentType> lstPaymentType = new List<PaymentType>();
            lstPaymentType = _feePaymentService.GetPaymentTypes(studNo, source);
            ViewBag.Source = source;
            ViewBag.StudentNo = studNo;
            return PartialView("_ListPaymentType", lstPaymentType);
        }
        public JsonResult GetDiscountOnAmount(string studNo, int paymentTypeID, double amount, int feeID)
        {
            string jsonEvents = "";
            jsonEvents = _feePaymentService.GetDiscountDetails(studNo, amount, paymentTypeID, feeID);
            var jsonResult = new { events = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetFeeDetails(string students, string paymentTo, int providerTypeID, string isPaySibling = "0")
        {
            string[] studts = students.Split(',');
            FeeDetailsVM objVM = new FeeDetailsVM();
            IEnumerable<StudentsFeeDetails> AllStudentFeeList = new List<StudentsFeeDetails>();
            AllStudentFeeList = _feePaymentService.GetFeeDetails(studts[0], paymentTo, providerTypeID, isPaySibling);

            List<StudentsFeeDetails> LstStudentFee = new List<StudentsFeeDetails>();

            foreach (string studentno in studts)
            {
                if (AllStudentFeeList.Any(x => x.STU_NO == studentno))
                    LstStudentFee.Add(AllStudentFeeList.Where(x => x.STU_NO == studentno).FirstOrDefault());
            }


            LstStudentFee = LstStudentFee.Select(m =>
            {
                m.DiscAmount = m.FeeDetail.Sum(n => n.DiscAmount);
                m.DueAmount = m.FeeDetail.Sum(n => n.DueAmount);
                m.PayAmount = m.FeeDetail.Sum(n => n.PayAmount);
                m.Source = paymentTo;
                m.StudentName = SessionHelper.CurrentSession.FamilyStudentList.Where(n => n.StudentNumber == m.STU_NO).Select(n => n.FirstName + " " + n.LastName).FirstOrDefault();
                return m;
            }).ToList();

            foreach (var itm in LstStudentFee)
            {
                foreach (var fe in itm.FeeDetail)
                {
                    string advanceDetails = string.Empty; string rangeValues = string.Empty;
                    if (fe.AdvanceDetails.Count() > 0)
                    {
                        advanceDetails = String.Join(", ", fe.AdvanceDetails.Where(m => m.Id != 0).Select(m => m.Month));
                        advanceDetails = advanceDetails.Replace("-", " ");
                        rangeValues = String.Join(",", fe.AdvanceDetails.Where(m => m.Id != 0).Select(m => m.FeeAmount));
                    }
                    fe.RangeLables = advanceDetails;
                    fe.RangeValues = rangeValues;
                }
            }
            return PartialView("_StudentFeeDetails", LstStudentFee);
        }
        public JsonResult CheckIsPaySibling(string PreSelectedStudent, string SelectedStudent)
        {
            //string jsonEvents = "";
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            var PreSelSchlId = studList.Where(m => m.StudentNumber == PreSelectedStudent).Select(m => m.SchoolId).FirstOrDefault();
            var isSibling = studList.Where(m => m.StudentNumber == SelectedStudent && m.SchoolId == PreSelSchlId).Select(m => m.SchoolId).FirstOrDefault();
            var jsonResult = new { IsSibling = isSibling };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetActivityDetails(string StudentNo, int ActivityId)
        {
            //ActivityId = 309; StudentNo = "12300400127471";
            EnrolllActivityModel objVM = new EnrolllActivityModel();
            var activity = _IEnrollActivitiesService.GetActivity(StudentNo, 0, ActivityId).FirstOrDefault();
            objVM = activity;
            return PartialView("_ActivityDetails", objVM);
        }

        [HttpPost]
        public JsonResult SubmitToPay(IEnumerable<StudentsFeeDetails> objVM)
        {
            string jsonEvents = "";
            OnlinePayResponse objResult = new OnlinePayResponse();
            if (objVM != null && objVM.Count() > 0)
            {
                var visitorInfo = new VisitorInfo();
                string IpAddress = visitorInfo.GetIpAddress();
                string HostIpAddress = visitorInfo.GetClientIPAddress();
                string IpDetails = IpAddress + ":" + HostIpAddress;


                IEnumerable<StudentsFeeDetails> LstStudentFee = new List<StudentsFeeDetails>();
                string StudNo = objVM.FirstOrDefault().STU_NO;
                string Source = objVM.FirstOrDefault().Source;
                string IsPaySibling = objVM.FirstOrDefault().IsPaySibling;
                int PaymentTypeId = Convert.ToInt32(objVM.FirstOrDefault().PaymentTypeID);
                //var TotalAmount = objVM.Sum(n => n.PayingAmount);

                IEnumerable<PaymentType> lstPaymTyp = new List<PaymentType>();
                lstPaymTyp = _feePaymentService.GetPaymentTypes((StudNo), Source);
                var providerTypeId = lstPaymTyp.Where(m => m.PaymentTypeID == PaymentTypeId).Select(m => m.ProviderTypeID).FirstOrDefault();
                LstStudentFee = _feePaymentService.GetFeeDetails(StudNo, Source, providerTypeId, IsPaySibling);

                foreach (var item in objVM)
                {
                    //var lstPaymentType = _feePaymentService.GetPaymentTypes(item.STU_NO, item.Source);                    
                    item.IpAddress = IpDetails;
                    var feeIdForDiiscount = 5; // as suggest by Jacob discount is available for tution fee only
                    var feeStudent = LstStudentFee.Where(m => m.STU_NO == item.STU_NO).FirstOrDefault();
                    item.UserMessageforOnlinePaymentBlock = feeStudent.UserMessageforOnlinePaymentBlock;
                    item.OnlinePaymentAllowed = Convert.ToString(feeStudent.OnlinePaymentAllowed);
                    double discAmount = 0.0000;
                    if (item.FeeDetail.Any(m => m.FeeID == "5"))
                    {
                        discAmount = item.FeeDetail.Where(m => m.FeeID == "5").Sum(m => m.PayAmount + m.DiscAmount);
                    }

                    List<DiscountDetails> objDiscDet = new List<DiscountDetails>();
                    string jsonDisc = _feePaymentService.GetDiscountDetails(item.STU_NO, discAmount, PaymentTypeId, feeIdForDiiscount);
                    DiscountDetailsRoot objDisc = JsonConvert.DeserializeObject<DiscountDetailsRoot>(jsonDisc);
                    item.DiscountDetails = objDisc != null && objDisc.data != null ? objDisc.data.DiscountDetails : objDiscDet;

                    item.PayMode = feeStudent.PayMode;
                    IEnumerable<FeeDetail> FeeDetls = new List<FeeDetail>();
                    FeeDetls = LstStudentFee.Where(m => m.STU_NO == item.STU_NO).Select(n => n.FeeDetail).FirstOrDefault();
                    foreach (var fee in item.FeeDetail)
                    {
                        //var discountDetails = _feePaymentService.GetDiscountDetails(item.STU_NO, fee.PayAmount, Convert.ToInt32(item.PaymentTypeID), Convert.ToInt32(fee.FeeID));
                        FeeDetail feeStud = new FeeDetail();
                        if (Source.ToLower() == "activity")
                        {
                            feeStud = FeeDetls.Where(m => m.FeeID == fee.FeeID && m.ActivityRefID == fee.ActivityRefID).FirstOrDefault();
                        }
                        else
                        {
                            feeStud = FeeDetls.Where(m => m.FeeID == fee.FeeID).FirstOrDefault();
                        }

                        fee.BlockPayNow = Convert.ToBoolean(feeStud.BlockPayNow);
                        fee.AdvancePaymentAvailable = Convert.ToBoolean(feeStud.AdvancePaymentAvailable);
                        fee.AdvanceDetails = feeStud.AdvanceDetails.ToList();
                        fee.OriginalAmount = Convert.ToDouble(feeStud.OriginalAmount);
                        fee.DueAmount = Convert.ToDouble(feeStud.DueAmount);
                        fee.FeeDescription = feeStud.FeeDescription;
                        fee.ActivityFeeCollType = feeStud.ActivityFeeCollType;
                        fee.ActivityRefID = feeStud.ActivityRefID;
                        fee.PayAmount += fee.DiscAmount;
                    }
                }
                // calling common payment service
                List<CommonStudentFee> objCommonStudFee = new List<CommonStudentFee>();
                foreach (var feeStudent in objVM.Where(m => m.PayingAmount > 0).ToList())
                {
                    CommonStudentFee objStud = new CommonStudentFee();
                    objStud.STU_NO = feeStudent.STU_NO;
                    objStud.UserMessageforOnlinePaymentBlock = feeStudent.UserMessageforOnlinePaymentBlock;
                    objStud.OnlinePaymentAllowed = Convert.ToString(feeStudent.OnlinePaymentAllowed);
                    objStud.PaymentTypeID = Convert.ToString(feeStudent.PaymentTypeID);
                    objStud.PaymentProcessingCharge = feeStudent.PaymentProcessingCharge;
                    objStud.PayingAmount = feeStudent.PayingAmount;
                    objStud.IpAddress = IpDetails;
                    objStud.PayMode = feeStudent.PayMode;

                    List<CommonFeeDetails> newList = feeStudent.FeeDetail.Select(e => new CommonFeeDetails { BlockPayNow = e.BlockPayNow, AdvancePaymentAvailable = e.AdvancePaymentAvailable, AdvanceDetails = e.AdvanceDetails, DiscAmount = e.DiscAmount, DueAmount = e.DueAmount, PayAmount = e.PayAmount, OriginalAmount = e.OriginalAmount, FeeDescription = e.FeeDescription, FeeID = e.FeeID, ActivityFeeCollType = e.ActivityFeeCollType, ActivityRefID = e.ActivityRefID }).ToList();
                    objStud.StudFeeDetails = newList;

                    objStud.DiscountDetails = feeStudent.DiscountDetails;
                    objCommonStudFee.Add(objStud);
                }
                if (Source.ToLower() == "activity")
                {
                    var feeColType = objCommonStudFee.Select(m => m.StudFeeDetails.Select(n => n.ActivityFeeCollType).FirstOrDefault()).FirstOrDefault();
                    objResult = _paymentService.SubmitOnlinePaymentRequestMulti(Source, objCommonStudFee, "/parentcorner/fee", feeColType);
                }
                else
                {
                    objResult = _paymentService.SubmitOnlinePaymentRequest(Source, objCommonStudFee, "", "/parentcorner/fee");
                }

                //objResult = _feePaymentService.SubmitOnlinePaymentRequest(objVM);
            }

            jsonEvents = JsonConvert.SerializeObject(objResult);
            var jsonResult = new { result = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PaymentRedirectionProcess(string Type = "", string ID = "", string SRC = "")
        {
            var refid = SessionExtensions.GetObject<string>("PCPayGatewayRefId");
            var sorce = SessionExtensions.GetObject<string>("PCPayGateWaySource");
            var backUrl = SessionExtensions.GetObject<string>("PCPayGateWayReturnTo");
            //remove session
            //SessionExtensions.RemoveObject("PCPayGatewayRefId");
            //SessionExtensions.RemoveObject("PCPayGateWaySource");
            //SessionExtensions.RemoveObject("PCPayGateWayReturnTo");

            var studList = SessionHelper.CurrentSession.FamilyStudentList;

            PaymentSummary objModel = new PaymentSummary();
            if (sorce.ToLower() == "school" || sorce.ToLower() == "exam")
            {
                objModel = _paymentService.GetPaymentSummary(Type, refid, sorce);
                if (objModel.IsPaymentSuccessful != null)
                {
                    objModel.SchoolFeePaymentResultDetails = objModel.SchoolFeePaymentResultDetails.Select(m =>
                    {
                        m.StudentNo = Convert.ToString(m.StudentId);
                        m.StudentSection = studList.Where(p => p.StudentNumber == Convert.ToString(m.StudentId)).Select(p => p.SectionName).FirstOrDefault();
                        m.Grade = studList.Where(p => p.StudentNumber == Convert.ToString(m.StudentId)).Select(p => p.GradeDisplay).FirstOrDefault(); ;
                        return m;
                    }).ToList();
                    objModel.TotalAmount = (objModel.SchoolFeePaymentResultDetails.Sum(m => m.PaidAmount));
                }
            }
            else if (sorce.ToLower() == "transport")
            {
                objModel = _paymentService.GetPaymentSummaryTransport(Type, refid, sorce);
                if (objModel.IsPaymentSuccessful != null)
                {
                    objModel.SchoolFeePaymentResultDetails = objModel.SchoolFeePaymentResultDetails.Select(m =>
                    {
                        m.StudentNo = Convert.ToString(m.StudentId);
                        m.StudentSection = studList.Where(p => p.StudentNumber == Convert.ToString(m.StudentId)).Select(p => p.SectionName).FirstOrDefault();
                        m.Grade = studList.Where(p => p.StudentNumber == Convert.ToString(m.StudentId)).Select(p => p.GradeDisplay).FirstOrDefault(); ;
                        return m;
                    }).ToList();
                    objModel.TotalAmount = (objModel.SchoolFeePaymentResultDetails.Sum(m => m.PaidAmount));
                }
            }
            else if (sorce.ToLower() == "activity")
            {
                objModel = _paymentService.GetPaymentSummaryActivity(Type, refid, sorce, "FC");
                if (objModel.IsPaymentSuccessful != null)
                {
                    objModel.ActivityPaymentResultDetails = objModel.ActivityPaymentResultDetails.Select(m =>
                    {
                        m.StudentNo = Convert.ToString(m.StudentId);
                        m.StudentSection = studList.Where(p => p.StudentNumber == Convert.ToString(m.StudentId)).Select(p => p.SectionName).FirstOrDefault();
                        m.Grade = studList.Where(p => p.StudentNumber == Convert.ToString(m.StudentId)).Select(p => p.GradeDisplay).FirstOrDefault(); ;
                        return m;
                    }).ToList();
                    objModel.TotalAmount = (objModel.ActivityPaymentResultDetails.Sum(m => m.PaidAmount));
                }
            }

            objModel.BackUrl = backUrl;
            objModel.ReferenceNo = refid;
            return View("PaymentSummary", objModel);
        }

        #region Statement of Accounts and Payment History

        //public ActionResult StatementOfAccounts(IEnumerable<FeeDetailVMRoot> lstStudFeeDetails)
        //{

        //    return PartialView("StatementOfAccounts", lstStudFeeDetails);
        //}

        //academic year
        public List<AssessmentdropModel> GetAcademicYearsList()
        {
            List<AssessmentdropModel> Dropmodel = new List<AssessmentdropModel>();
            try
            {
                _loggerClient.LogWarning("Inside GetAcademicYearsList");
                StringBuilder sb = new StringBuilder();
                var studList = SessionHelper.CurrentSession.FamilyStudentList;
                var schoolAcademicYearId = studList.Select(m => m.SchoolAcademicYearId).FirstOrDefault();
                _loggerClient.LogWarning("SchoolAcademicYearId :" + Convert.ToString(schoolAcademicYearId));
                _loggerClient.LogWarning("SchoolId :" + SessionHelper.CurrentSession.SchoolId.ToString());
                var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);

                var AcademicYearList = SelectListHelper.GetSelectListData(ListItems.StudentFeesAcademicYears, string.Format("{0},{1}", SessionHelper.CurrentSession.SchoolId, schoolAcademicYearId)).ToList();

                sb.Append("AcdYearList" + Environment.NewLine);

                foreach (var item1 in AcademicYearList)   // ....... DropDownlist Binding
                {
                    _loggerClient.LogWarning("Counter");
                    AssessmentdropModel asm = new AssessmentdropModel();
                    asm.ItemName = item1.ItemId;
                    asm.ACDID = item1.ItemName;
                    Dropmodel.Add(asm);
                }

                _loggerClient.LogWarning("AcademicYearList :" + Convert.ToString(string.Join(",", AcademicYearList)));
                _loggerClient.LogWarning("Out GetAcademicYearsList");
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            return Dropmodel;
        }
        // Get Transport Fee Enable Config
        public List<AssessmentdropModel> GetTransportFeePaymentEnabled(string schooid)
        {
            List<AssessmentdropModel> Dropmodel = new List<AssessmentdropModel>();
            try
            {
                _loggerClient.LogWarning("Inside GetAcademicYearsList");
                StringBuilder sb = new StringBuilder();
                //var studList = SessionHelper.CurrentSession.FamilyStudentList;
                //var schoolAcademicYearId = studList.Select(m => m.SchoolAcademicYearId).FirstOrDefault();
                //_loggerClient.LogWarning("SchoolAcademicYearId :" + Convert.ToString(schoolAcademicYearId));
                _loggerClient.LogWarning("SchoolId :" + schooid);
                //var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);

                var IsTransportFeePaymentEnabled = SelectListHelper.GetSelectListData(ListItems.IsTransportFeePaymentEnabled, string.Format("{0}", schooid)).ToList();

                sb.Append("IsTransportFeePaymentEnabled middle" + Environment.NewLine);

                foreach (var item1 in IsTransportFeePaymentEnabled)   // ....... DropDownlist Binding
                {
                    _loggerClient.LogWarning("Counter");
                    AssessmentdropModel asm = new AssessmentdropModel();
                    asm.ItemName = item1.ItemId;
                    asm.ACDID = item1.ItemName;
                    Dropmodel.Add(asm);
                }

                _loggerClient.LogWarning("IsTransportFeePaymentEnabled :" + Convert.ToString(string.Join(",", IsTransportFeePaymentEnabled)));
                _loggerClient.LogWarning("Out IsTransportFeePaymentEnabled");
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            return Dropmodel;
        }

        public ActionResult GetAccountDetails(string students, string fromDate = "", string toDate = "", string type = "")
        {
            IEnumerable<AccountStatementDetail> accountDetailsList = new List<AccountStatementDetail>();
            try
            {
                _loggerClient.LogWarning("Inside GetAccountDetails");
                string source = string.IsNullOrEmpty(type) ? "SCHOOL" : type; string student = string.Empty;
                string[] studts = students.Split(',');
                _loggerClient.LogWarning("GetPaymentHistory - 0");
                if (string.IsNullOrEmpty(fromDate))
                {
                    DateTime now = DateTime.Now;
                    var startDate = new DateTime(now.Year, now.Month, 1);
                    var endDate = startDate.AddMonths(1).AddDays(-1);
                    fromDate = startDate.ToString("dd/MMM/yyyy");
                    toDate = endDate.ToString("dd/MMM/yyyy");
                }
                else
                {
                    var startDate = Convert.ToDateTime(fromDate);
                    var endDate = Convert.ToDateTime(toDate);
                    fromDate = startDate.ToString("dd/MMM/yyyy");
                    toDate = endDate.ToString("dd/MMM/yyyy");
                }
                _loggerClient.LogWarning("GetPaymentHistory - 1");
                List<AccountStatementDetail> lstAccountDetails = new List<AccountStatementDetail>();
                foreach (var stud in studts)
                {
                    student = stud;
                    var accountDetailsLst = _paymentService.GetAccountDetails(student, fromDate, toDate, source);
                    lstAccountDetails.AddRange(accountDetailsLst);
                }
                _loggerClient.LogWarning("GetPaymentHistory - 2");
                accountDetailsList = lstAccountDetails.OrderByDescending(m => m.RefNo).ToList();
                var TotalCredit = accountDetailsList.Sum(m => m.Credit); var TotalDebit = accountDetailsList.Sum(m => m.Debit);
                _loggerClient.LogWarning("GetPaymentHistory - 3" + TotalCredit);
                var diff = Convert.ToString(TotalCredit - TotalDebit);
                _loggerClient.LogWarning("GetPaymentHistory - 4" + diff);
                ViewBag.AdvanceDueLabel = Convert.ToDecimal(diff) > 0 ? $"{ResourceManager.GetString("FeePayment.Fee.Advance")} : {diff}" : $"{ResourceManager.GetString("FeePayment.Fee.Due")} : {diff.Replace("-", "")}";
                _loggerClient.LogWarning("Out GetAccountDetails");
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            return PartialView("_AccountDetails", accountDetailsList);
        }

        /// <summary>
        /// Author:tejalben
        /// Summery:Get account details in pdf format.
        /// Date: 30 July 2020
        /// </summary>
        /// <param name="students"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult GetAccountDetailInPdf(string students, string fromDate = "", string toDate = "", string type = "")
        {
            IEnumerable<AccountStatementDetail> accountDetailsList = new List<AccountStatementDetail>();
            _loggerClient.LogWarning("Inside GetAccountDetails");
            string source = string.IsNullOrEmpty(type) ? "SCHOOL" : type; string student = string.Empty;
            string[] studts = students.Split(',');
            if (string.IsNullOrEmpty(fromDate))
            {
                DateTime now = DateTime.Now;
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                fromDate = startDate.ToString("dd/MMM/yyyy");
                toDate = endDate.ToString("dd/MMM/yyyy");
            }
            else
            {
                var startDate = Convert.ToDateTime(fromDate);
                var endDate = Convert.ToDateTime(toDate);
                fromDate = startDate.ToString("dd/MMM/yyyy");
                toDate = endDate.ToString("dd/MMM/yyyy");
            }
            List<AccountStatementDetail> lstAccountDetails = new List<AccountStatementDetail>();
            foreach (var stud in studts)
            {
                student = stud;
                var accountDetailsLst = _paymentService.GetAccountDetails(student, fromDate, toDate, source);
                lstAccountDetails.AddRange(accountDetailsLst);
            }
            accountDetailsList = lstAccountDetails;
            var TotalCredit = accountDetailsList.Sum(m => m.Credit); var TotalDebit = accountDetailsList.Sum(m => m.Debit);
            var diff = Convert.ToString(TotalCredit - TotalDebit);
            ViewBag.AdvanceDueLabel = Convert.ToDecimal(diff) > 0 ? ("Advance : " + diff) : ("Due : " + diff.Replace("-", ""));
            _loggerClient.LogWarning("Out GetAccountDetails");

            //return PartialView("_AccountDetails", accountDetailsList);
            string pdfhtml = "";
            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_GetAccountDetailsInPdf", accountDetailsList);
            PdfDocument pdf = new PdfDocument();
            var config = new PdfGenerateConfig()
            {
                MarginBottom = 10,
                MarginLeft = 20,
                MarginRight = 20,
                MarginTop = 10,
                PageSize = PageSize.A4
            };
            pdf = PdfGenerator.GeneratePdf(pdfhtml, config);
            pdf.Info.Title = "AccountDetails_" + DateTime.Now.ToString("dd-MMM-yyyy");
            // Send PDF to browser
            MemoryStream stream = new MemoryStream();
            pdf.Save(stream, false);
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + pdf.Info.Title + ".pdf");
            Response.BinaryWrite(stream.ToArray());
            Response.Flush();
            stream.Close();
            Response.End();
            return null;
        }
        public ActionResult GetAccountSummary(string students, string fromDate = "", string toDate = "", string type = "")
        {
            IEnumerable<AccountStatementSummary> lstAccountDetails = new List<AccountStatementSummary>();
            try
            {
                string source = string.IsNullOrEmpty(type) ? "SCHOOL" : type; string student = string.Empty;
                string[] studts = students.Split(',');
                _loggerClient.LogWarning("Inside AccountStatementSummary");
                if (string.IsNullOrEmpty(fromDate))
                {
                    var lstAcademicYear = GetAcademicYearsList();
                    string allDate = lstAcademicYear.Select(m => m.ACDID).FirstOrDefault();
                    string[] strDate = allDate.Split('|');
                    var startDate = Convert.ToDateTime(strDate[0]);
                    var endDate = Convert.ToDateTime(strDate[1]);
                    fromDate = startDate.ToString("dd/MMM/yyyy");
                    toDate = endDate.ToString("dd/MMM/yyyy");
                }
                else
                {
                    var startDate = Convert.ToDateTime(fromDate);
                    var endDate = Convert.ToDateTime(toDate);
                    fromDate = startDate.ToString("dd/MMM/yyyy");
                    toDate = endDate.ToString("dd/MMM/yyyy");
                }
                _loggerClient.LogWarning("AccountStatementSummary - 1");
                List<AccountStatementSummary> accountDetailsL = new List<AccountStatementSummary>();
                foreach (var stud in studts)
                {
                    student = stud;
                    var accountDetailsLst = _paymentService.GetAccountSummary(student, fromDate, toDate, source);
                    accountDetailsL.AddRange(accountDetailsLst);
                }
                lstAccountDetails = accountDetailsL;
                var TotalCredit = lstAccountDetails.Sum(m => m.Credit); var TotalDebit = lstAccountDetails.Sum(m => m.Debit);
                var diff = Convert.ToString(TotalCredit - TotalDebit);
                ViewBag.AdvanceDueLabel = Convert.ToDecimal(diff) > 0 ? $"{ResourceManager.GetString("FeePayment.Fee.Advance")} : {diff}" : $"{ResourceManager.GetString("FeePayment.Fee.Due")} : {diff.Replace("-", "")}";
                _loggerClient.LogWarning("Out TotalCredit" + TotalCredit);
                _loggerClient.LogWarning("Out TotalDebit" + TotalDebit);
                _loggerClient.LogWarning("Out diff");
                _loggerClient.LogWarning("Out GetAccountSummary");
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            return PartialView("_AccountSummary", lstAccountDetails);
        }

        /// <summary>
        /// Author:tejalben
        /// Summery:Get account summery in pdf
        /// Date:30 July 2020
        /// </summary>
        /// <param name="students"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult GetAccountSummeryInPdf(string students, string fromDate = "", string toDate = "", string type = "")
        {
            IEnumerable<AccountStatementSummary> lstAccountDetails = new List<AccountStatementSummary>();
            string source = string.IsNullOrEmpty(type) ? "SCHOOL" : type; string student = string.Empty;
            string[] studts = students.Split(',');
            if (string.IsNullOrEmpty(fromDate))
            {
                _loggerClient.LogWarning("Inside GetAccountSummary");
                var lstAcademicYear = GetAcademicYearsList();
                string allDate = lstAcademicYear.Select(m => m.ACDID).FirstOrDefault();
                string[] strDate = allDate.Split('|');
                var startDate = Convert.ToDateTime(strDate[0]);
                var endDate = Convert.ToDateTime(strDate[1]);
                fromDate = startDate.ToString("dd/MMM/yyyy");
                toDate = endDate.ToString("dd/MMM/yyyy");
            }
            else
            {
                var startDate = Convert.ToDateTime(fromDate);
                var endDate = Convert.ToDateTime(toDate);
                fromDate = startDate.ToString("dd/MMM/yyyy");
                toDate = endDate.ToString("dd/MMM/yyyy");
            }
            List<AccountStatementSummary> accountDetailsL = new List<AccountStatementSummary>();
            foreach (var stud in studts)
            {
                student = stud;
                var accountDetailsLst = _paymentService.GetAccountSummary(student, fromDate, toDate, source);
                accountDetailsL.AddRange(accountDetailsLst);
            }
            lstAccountDetails = accountDetailsL;
            var TotalCredit = lstAccountDetails.Sum(m => m.Credit); var TotalDebit = lstAccountDetails.Sum(m => m.Debit);
            var diff = Convert.ToString(TotalCredit - TotalDebit);
            ViewBag.AdvanceDueLabel = Convert.ToDecimal(diff) > 0 ? $"{ResourceManager.GetString("FeePayment.Fee.Advance")} : {diff}" : $"{ResourceManager.GetString("FeePayment.Fee.Due")} : {diff.Replace("-", "")}";
            _loggerClient.LogWarning("Out GetAccountSummary");

            //return PartialView("_AccountSummary", lstAccountDetails);
            string pdfhtml = "";
            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_GetAccountSummeryInPdf", lstAccountDetails);
            PdfDocument pdf = new PdfDocument();
            var config = new PdfGenerateConfig()
            {
                MarginBottom = 10,
                MarginLeft = 20,
                MarginRight = 20,
                MarginTop = 10,
                PageSize = PageSize.A4
            };
            pdf = PdfGenerator.GeneratePdf(pdfhtml, config);
            pdf.Info.Title = "AccountSummery_" + DateTime.Now.ToString("dd-MMM-yyyy");
            // Send PDF to browser
            MemoryStream stream = new MemoryStream();
            pdf.Save(stream, false);
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + pdf.Info.Title + ".pdf");
            Response.BinaryWrite(stream.ToArray());
            Response.Flush();
            stream.Close();
            Response.End();
            return null;
        }

        public ActionResult GetPaymentHistory(string students, string fromDate = "", string toDate = "", string type = "", string status = "")
        {
            IEnumerable<PaymentHistory> lstAccountDetails = new List<PaymentHistory>();
            try
            {
                string source = string.IsNullOrEmpty(type) ? "SCHOOL" : type; string student = string.Empty;
                string[] studts = students.Split(',');
                _loggerClient.LogWarning("GetPaymentHistory - 1");
                if (string.IsNullOrEmpty(fromDate))
                {
                    //var lstAcademicYear = GetAcademicYearsList();
                    //string allDate = lstAcademicYear.Select(m => m.ACDID).FirstOrDefault();
                    //string[] strDate = allDate.Split('|');
                    //var startDate = Convert.ToDateTime(strDate[0]);
                    //var endDate = Convert.ToDateTime(strDate[1]);
                    DateTime now = DateTime.Now;
                    var startDate = new DateTime(now.Year, now.Month, 1);
                    var endDate = startDate.AddMonths(1).AddDays(-1);
                    fromDate = startDate.ToString("dd/MMM/yyyy");
                    toDate = endDate.ToString("dd/MMM/yyyy");
                }
                else
                {
                    var startDate = Convert.ToDateTime(fromDate);
                    var endDate = Convert.ToDateTime(toDate);
                    fromDate = startDate.ToString("dd/MMM/yyyy");
                    toDate = endDate.ToString("dd/MMM/yyyy");
                }
                _loggerClient.LogWarning("GetPaymentHistory - 2");
                List<PaymentHistory> accountDetailsL = new List<PaymentHistory>();
                foreach (var stud in studts)
                {
                    student = stud;
                    var accountDetailsLst = _paymentService.GetPaymentHistory(student, fromDate, toDate, source);
                    accountDetailsL.AddRange(accountDetailsLst);
                }
                _loggerClient.LogWarning("GetPaymentHistory - 3");
                lstAccountDetails = !string.IsNullOrEmpty(status) ? accountDetailsL.Where(m => m.Status.ToLower() == status.ToLower()).ToList() : accountDetailsL;
                lstAccountDetails = lstAccountDetails.OrderByDescending(m => m.RefNo).ToList();
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            return PartialView("_PaymentHistory", lstAccountDetails);
        }

        public JsonResult SendEmailOfAccountStatements(string students, string fromDate = "", string toDate = "", string type = "")
        {
            string jsonEvents = "";
            string[] studts = students.Split(',');
            OnlineFeePayment obj = new OnlineFeePayment();
            foreach (var item in studts)
            {
                obj = _paymentService.SendEmailStatementOfAccounts(item, fromDate, toDate, type);
            }
            jsonEvents = JsonConvert.SerializeObject(obj);
            var jsonResult = new { events = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult PrintFeesShedule(string id)
        {
            var lstScheduleFee = _feePaymentService.GetFeeSchedule(id);
            var student = SessionHelper.CurrentSession.FamilyStudentList.FirstOrDefault(n => n.StudentNumber == id);
            ViewBag.StudentName = (student.FirstName + " " + student.LastName).Trim();
            ViewBag.Grade = student.GradeDisplay;
            FeeSchedule fd = new FeeSchedule();
            return View(lstScheduleFee);
        }

        //payment history and account statement in fee payment
        public ActionResult GetPaymentHistoryView()
        {
            List<FeeDetailsVM> lstFeeStud = new List<FeeDetailsVM>();
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            var selectedStudDetls = SessionHelper.CurrentSession.CurrentSelectedStudent;
            var currSelectedStudSchoolId = selectedStudDetls.SchoolId;
            int i = 0; //var isTransport = false;
            DateTime now = DateTime.Now; string fromDate = ""; string toDate = ""; string source = "SCHOOL"; string status = "";
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            fromDate = startDate.ToString("dd/MMM/yyyy");
            toDate = endDate.ToString("dd/MMM/yyyy");
            // START code for payemnt history
            IEnumerable<PaymentHistory> lstAccountDetails = new List<PaymentHistory>();
            List<PaymentHistory> accountDetailsL = new List<PaymentHistory>();
            string student = string.Empty;
            //foreach (var stud in studList)
            //{
            student = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber;
            var accountDetailsLst = _paymentService.GetPaymentHistory(student, fromDate, toDate, source);
            accountDetailsL.AddRange(accountDetailsLst);
            //}
            lstAccountDetails = !string.IsNullOrEmpty(status) ? accountDetailsL.Where(m => m.Status.ToLower() == status.ToLower()).ToList() : accountDetailsL;
            lstAccountDetails = lstAccountDetails.OrderByDescending(m => m.RefNo).ToList();
            // END code for payment history
            //var cntSchool = studList.GroupBy(x => x.SchoolId).Select(x => x.Key);
            //if (cntSchool.Count() > 1)
            //{
            //    isTransport = true;
            //}
            //else
            //{
            //    var lstIsTransportEnable = GetTransportFeePaymentEnabled(studList.Select(x => x.SchoolId).FirstOrDefault().ToString());
            //    if (lstIsTransportEnable.Count() > 0)
            //    {
            //        isTransport = true;
            //    }
            //}
            //ViewBag.IsTransport = isTransport;

            foreach (var item in studList)
            {
                FeeDetailsVM objFDM = new FeeDetailsVM();
                objFDM.Section = Convert.ToString(item.SectionName);
                objFDM.Grade = item.GradeDisplay;
                objFDM.SchoolName = item.SchoolName;
                objFDM.SchoolId = item.SchoolId;
                objFDM.StudentId = item.StudentId;
                objFDM.StudentNo = (item.StudentNumber);
                objFDM.StudentImg = item.StudentImage;
                objFDM.StudentName = item.FirstName + " " + item.LastName;
                objFDM.IsSelected = item.StudentNumber == selectedStudDetls.StudentNumber ? true : false;


                objFDM.FromDate = startDate;
                objFDM.ToDate = endDate;

                lstFeeStud.Add(objFDM);
                ++i;
            }
            ViewBag.LstPaymentHistory = lstAccountDetails;
            return PartialView("PaymentHistory", lstFeeStud);
        }
        public ActionResult GetAccountStatementView()
        {
            List<FeeDetailsVM> lstFeeStud = new List<FeeDetailsVM>();
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            var selectedStudDetls = SessionHelper.CurrentSession.CurrentSelectedStudent;
            var currSelectedStudSchoolId = selectedStudDetls.SchoolId;
            int i = 0; var isTransport = false;
            var cntSchool = studList.GroupBy(x => x.SchoolId).Select(x => x.Key);
            if (cntSchool.Count() > 1)
            {
                isTransport = true;
            }
            else
            {
                var lstIsTransportEnable = GetTransportFeePaymentEnabled(studList.Select(x => x.SchoolId).FirstOrDefault().ToString());
                if (lstIsTransportEnable.Count() > 0)
                {
                    isTransport = true;
                }
            }
            ViewBag.IsTransport = isTransport;
            DateTime now = DateTime.Now; string fromDate = ""; string toDate = ""; string source = "SCHOOL"; string status = "";
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            fromDate = startDate.ToString("dd/MMM/yyyy");
            toDate = endDate.ToString("dd/MMM/yyyy");
            // START Account details 
            IEnumerable<AccountStatementDetail> accountDetailsList = new List<AccountStatementDetail>();
            List<AccountStatementDetail> lstAccountDetails = new List<AccountStatementDetail>();
            string student = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber; ;
            var accountDetailsLst = _paymentService.GetAccountDetails(student, fromDate, toDate, source);
            lstAccountDetails.AddRange(accountDetailsLst);
            accountDetailsList = lstAccountDetails.OrderByDescending(m => m.RefNo).ToList();
            var TotalCredit = accountDetailsList.Sum(m => m.Credit); var TotalDebit = accountDetailsList.Sum(m => m.Debit);
            var diff = Convert.ToString(TotalCredit - TotalDebit);
            ViewBag.AdvanceDueLabel = Convert.ToDecimal(diff) > 0 ? $"{ResourceManager.GetString("FeePayment.Fee.Advance")} : {diff}" : $"{ResourceManager.GetString("FeePayment.Fee.Due")} : {diff.Replace("-", "")}";
            ViewBag.LstAccountDetails = accountDetailsList;
            // END Account details 

            //START Account summary 
            IEnumerable<AccountStatementSummary> lstAccountSummary = new List<AccountStatementSummary>();
            List<AccountStatementSummary> accountDetailSummary = new List<AccountStatementSummary>();
            var accountSummaryLst = _paymentService.GetAccountSummary(student, fromDate, toDate, source);
            accountDetailSummary.AddRange(accountSummaryLst);
            lstAccountSummary = accountDetailSummary;
            var TotalCreditSummary = accountDetailSummary.Sum(m => m.Credit); var TotalDebitSummary = accountDetailSummary.Sum(m => m.Debit);
            var diffSummary = Convert.ToString(TotalCreditSummary - TotalDebitSummary);
            ViewBag.AdvanceDueLabelSummary = Convert.ToDecimal(diffSummary) > 0 ? ("Advance : " + diffSummary) : ("Due : " + diff.Replace("-", ""));
            ViewBag.LstAccountSummary = accountDetailSummary;
            //END Account summary

            foreach (var item in studList)
            {
                FeeDetailsVM objFDM = new FeeDetailsVM();
                objFDM.Section = Convert.ToString(item.SectionName);
                objFDM.Grade = item.GradeDisplay;
                objFDM.SchoolName = item.SchoolName;
                objFDM.SchoolId = item.SchoolId;
                objFDM.StudentId = item.StudentId;
                objFDM.StudentNo = (item.StudentNumber);
                objFDM.StudentImg = item.StudentImage;
                objFDM.StudentName = item.FirstName + " " + item.LastName;
                objFDM.IsSelected = item.StudentNumber == selectedStudDetls.StudentNumber ? true : false;
                objFDM.FromDate = startDate;
                objFDM.ToDate = endDate;

                lstFeeStud.Add(objFDM);
                ++i;
            }
            ViewBag.AcademicYearsList = GetAcademicYearsList();
            return PartialView("StatementOfAccounts", lstFeeStud);
        }

        // PRINT account details  
        public ActionResult PrintAccountDetails(string students, string fromDate = "", string toDate = "", string type = "")
        {
            IEnumerable<AccountStatementDetail> accountDetailsList = new List<AccountStatementDetail>();
            _loggerClient.LogWarning("Inside GetAccountDetails");
            string source = string.IsNullOrEmpty(type) ? "SCHOOL" : type; string student = string.Empty;
            string[] studts = students.Split(',');
            if (string.IsNullOrEmpty(fromDate))
            {
                DateTime now = DateTime.Now;
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                fromDate = startDate.ToString("dd/MMM/yyyy");
                toDate = endDate.ToString("dd/MMM/yyyy");
            }
            else
            {
                var startDate = Convert.ToDateTime(fromDate);
                var endDate = Convert.ToDateTime(toDate);
                fromDate = startDate.ToString("dd/MMM/yyyy");
                toDate = endDate.ToString("dd/MMM/yyyy");
            }
            List<AccountStatementDetail> lstAccountDetails = new List<AccountStatementDetail>();
            foreach (var stud in studts)
            {
                student = stud;
                var accountDetailsLst = _paymentService.GetAccountDetails(student, fromDate, toDate, source);
                lstAccountDetails.AddRange(accountDetailsLst);
            }
            accountDetailsList = lstAccountDetails;
            var TotalCredit = accountDetailsList.Sum(m => m.Credit); var TotalDebit = accountDetailsList.Sum(m => m.Debit);
            var diff = Convert.ToString(TotalCredit - TotalDebit);
            ViewBag.AdvanceDueLabel = Convert.ToDecimal(diff) > 0 ? $"{ResourceManager.GetString("FeePayment.Fee.Advance")} : {diff}" : $"{ResourceManager.GetString("FeePayment.Fee.Due")} : {diff.Replace("-", "")}";
            _loggerClient.LogWarning("Out GetAccountDetails");
            var studentDet = SessionHelper.CurrentSession.FamilyStudentList.FirstOrDefault(n => n.StudentNumber == students);
            ViewBag.StudentName = (studentDet.FirstName + " " + studentDet.LastName).Trim();
            ViewBag.Grade = studentDet.GradeDisplay;
            return View("_PrintAccountDetails", accountDetailsList);

        }
        public ActionResult PrintAccountSummary(string students, string fromDate = "", string toDate = "", string type = "")
        {
            IEnumerable<AccountStatementSummary> lstAccountDetails = new List<AccountStatementSummary>();
            string source = string.IsNullOrEmpty(type) ? "SCHOOL" : type; string student = string.Empty;
            string[] studts = students.Split(',');
            if (string.IsNullOrEmpty(fromDate))
            {
                _loggerClient.LogWarning("Inside GetAccountSummary");
                var lstAcademicYear = GetAcademicYearsList();
                string allDate = lstAcademicYear.Select(m => m.ACDID).FirstOrDefault();
                string[] strDate = allDate.Split('|');
                var startDate = Convert.ToDateTime(strDate[0]);
                var endDate = Convert.ToDateTime(strDate[1]);
                fromDate = startDate.ToString("dd/MMM/yyyy");
                toDate = endDate.ToString("dd/MMM/yyyy");
            }
            else
            {
                var startDate = Convert.ToDateTime(fromDate);
                var endDate = Convert.ToDateTime(toDate);
                fromDate = startDate.ToString("dd/MMM/yyyy");
                toDate = endDate.ToString("dd/MMM/yyyy");
            }
            List<AccountStatementSummary> accountDetailsL = new List<AccountStatementSummary>();
            foreach (var stud in studts)
            {
                student = stud;
                var accountDetailsLst = _paymentService.GetAccountSummary(student, fromDate, toDate, source);
                accountDetailsL.AddRange(accountDetailsLst);
            }
            lstAccountDetails = accountDetailsL;
            var TotalCredit = lstAccountDetails.Sum(m => m.Credit); var TotalDebit = lstAccountDetails.Sum(m => m.Debit);
            var diff = Convert.ToString(TotalCredit - TotalDebit);
            ViewBag.AdvanceDueLabel = Convert.ToDecimal(diff) > 0 ? $"{ResourceManager.GetString("FeePayment.Fee.Advance")} : {diff}" : $"{ResourceManager.GetString("FeePayment.Fee.Due")} : {diff.Replace("-", "")}";
            _loggerClient.LogWarning("Out GetAccountSummary");
            var studentDet = SessionHelper.CurrentSession.FamilyStudentList.FirstOrDefault(n => n.StudentNumber == students);
            ViewBag.StudentName = (studentDet.FirstName + " " + studentDet.LastName).Trim();
            ViewBag.Grade = studentDet.GradeDisplay;
            return View("_PrintAccountSummary", lstAccountDetails);
        }
    }
}