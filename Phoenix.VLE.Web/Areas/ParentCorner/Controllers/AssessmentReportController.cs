﻿using Phoenix.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.Common.Logger;
using Phoenix.Common;
using System.Text;
using System.Net;

namespace Phoenix.VLE.Web.Areas.ParentCorner.Controllers
{
    public class AssessmentReportController : BaseController
    {
        private readonly IAssessmentReport _IAssessmentReport;
        private ILoggerClient _loggerClient;
        public AssessmentReportController(IAssessmentReport iassessmentReport)
        {
            _IAssessmentReport = iassessmentReport;
        }
        // GET: ParentCorner/GetAssessmentReport
        public ActionResult Index()
        {
            StringBuilder sb = new StringBuilder();
            _loggerClient = LoggerClient.Instance;
            var viewname = "AssessmentReportsList";
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            AssessmentModel Assessmentmodel = new AssessmentModel();
            List<StudentList> studentlist = new List<StudentList>();
            try
            {
                foreach (var item in studList)
                {
                    StudentList model = new StudentList();
                    model.StudentFirstName = item.FirstName + " " + item.LastName;
                    model.StudentNumber = item.StudentNumber;
                    model.StudentLastName = item.LastName;
                    model.StudentId = item.StudentId;
                    model.SchooolId = item.SchoolId;
                    model.schoolAcademicYearid = item.SchoolAcademicYearId;
                    studentlist.Add(model);
                }
                Assessmentmodel.studentList = studentlist;  //......Student List Binding
                var StudInfo = Assessmentmodel.studentList.FirstOrDefault();
                var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
                var AcademicYearList = SelectListHelper.GetSelectListData(ListItems.Assessment, string.Format("{0},{1}", SessionHelper.CurrentSession.SchoolId, StudInfo.schoolAcademicYearid)).ToList();
                List<AssessmentdropModel> Dropmodel = new List<AssessmentdropModel>();
                sb.Append("AcdYearList" + Environment.NewLine);

                foreach (var item1 in AcademicYearList)   // ....... DropDownlist Binding
                {
                    AssessmentdropModel asm = new AssessmentdropModel();
                    string AcdYear = Convert.ToDateTime(item1.ItemName).Year.ToString();
                    int number = int.Parse(AcdYear);
                    string number1 = Convert.ToString(number + 1);
                    asm.ItemName = AcdYear + "-" + number1;
                    asm.ACDID = item1.ItemId;
                    Dropmodel.Add(asm);
                }
                string SchoolAcademicYearId = studList.FirstOrDefault().PhoenixSchoolAcademicYearId.ToString();
                IEnumerable<PdfInfoModel> pdfInfoModel = _IAssessmentReport.GetPdfListReport(StudInfo.StudentNumber, SchoolAcademicYearId);
                Assessmentmodel.pdfInfoModellist = pdfInfoModel.ToList();            //pdf file List
                List<SelectListItem> selectList1 = new List<SelectListItem>();
                selectList1.AddRange(Dropmodel.Select(x => new SelectListItem { Value = x.ACDID.ToString(), Text = x.ItemName }).ToList());
                Assessmentmodel.AcademicYear = SchoolAcademicYearId;
                Assessmentmodel.dropAcdemicYear = selectList1;
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }

            return View(viewname, Assessmentmodel);
            //return RedirectToAction("GetAssessmentReport");
        }
        //Start Assessment Functinality
        public ActionResult GetAssessmentReport()
        {
            StringBuilder sb = new StringBuilder();
            _loggerClient = LoggerClient.Instance;
            var viewname = "AssessmentReportsList";
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            AssessmentModel Assessmentmodel = new AssessmentModel();
            List<StudentList> studentlist = new List<StudentList>();
            try
            {
                foreach (var item in studList)
                {
                    StudentList model = new StudentList();
                    model.StudentFirstName = item.FirstName + " " + item.LastName;
                    model.StudentNumber = item.StudentNumber;
                    model.StudentLastName = item.LastName;
                    model.StudentId = item.StudentId;
                    model.SchooolId = item.SchoolId;
                    model.schoolAcademicYearid = item.SchoolAcademicYearId;
                    studentlist.Add(model);
                }
                Assessmentmodel.studentList = studentlist;  //......Student List Binding
                var StudInfo = Assessmentmodel.studentList.FirstOrDefault();
                var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
                var AcademicYearList = SelectListHelper.GetSelectListData(ListItems.Assessment, string.Format("{0},{1}", SessionHelper.CurrentSession.SchoolId, StudInfo.schoolAcademicYearid)).ToList();
                List<AssessmentdropModel> Dropmodel = new List<AssessmentdropModel>();
                sb.Append("AcdYearList" + Environment.NewLine);

                foreach (var item1 in AcademicYearList)   // ....... DropDownlist Binding
                {
                    AssessmentdropModel asm = new AssessmentdropModel();
                    string AcdYear =Convert.ToDateTime(item1.ItemName).Year.ToString();
                    int number = int.Parse(AcdYear);
                    string number1 = Convert.ToString(number + 1);
                    asm.ItemName = AcdYear + "-" + number1;
                    asm.ACDID = item1.ItemId;
                    Dropmodel.Add(asm);
                }
                string SchoolAcademicYearId = studList.FirstOrDefault().PhoenixSchoolAcademicYearId.ToString();
                IEnumerable<PdfInfoModel> pdfInfoModel = _IAssessmentReport.GetPdfListReport(StudInfo.StudentNumber, SchoolAcademicYearId);
                Assessmentmodel.pdfInfoModellist = pdfInfoModel.ToList();            //pdf file List
                List<SelectListItem> selectList1 = new List<SelectListItem>();
                selectList1.AddRange(Dropmodel.Select(x => new SelectListItem { Value = x.ACDID.ToString(), Text = x.ItemName }).ToList());
                Assessmentmodel.AcademicYear = SchoolAcademicYearId;
                Assessmentmodel.dropAcdemicYear = selectList1;
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
           
            return View(viewname, Assessmentmodel);
        }
        public ActionResult GetStudentinfo(string StudentNumber)
        {
            
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            AssessmentModel Assessmentmodel = new AssessmentModel();
            var StudInfo = studList.Where(x => x.StudentNumber == StudentNumber).FirstOrDefault();
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var AcademicYearList = SelectListHelper.GetSelectListData(ListItems.Assessment, string.Format("{0},{1}", SessionHelper.CurrentSession.SchoolId, StudInfo.SchoolAcademicYearId)).ToList();
           
            List<AssessmentdropModel> Dropmodel = new List<AssessmentdropModel>();
            foreach (var item1 in AcademicYearList)
            {
                AssessmentdropModel asm = new AssessmentdropModel();
                string AcdYear = Convert.ToDateTime(item1.ItemName).Year.ToString();
                int number = int.Parse(AcdYear);
                string number1 = (number + 1).ToString();
                asm.ItemName = AcdYear + "-" + number1;
                asm.ACDID = item1.ItemId;
                Dropmodel.Add(asm);
            }
            string SchoolAcademicYearId = StudInfo.PhoenixSchoolAcademicYearId.ToString();
            IEnumerable<PdfInfoModel> pdfInfoModel = _IAssessmentReport.GetPdfListReport(StudInfo.StudentNumber, SchoolAcademicYearId);
            Assessmentmodel.pdfInfoModellist = pdfInfoModel.ToList();
            List<SelectListItem> selectList1 = new List<SelectListItem>();
            selectList1.AddRange(Dropmodel.Select(x => new SelectListItem { Value = x.ACDID.ToString(), Text = x.ItemName }).ToList());
            Assessmentmodel.AcademicYear = SchoolAcademicYearId;
            Assessmentmodel.dropAcdemicYear = selectList1;
            return PartialView("_PdfInfoPartialView", Assessmentmodel);
        }
        public JsonResult RequestToPdfView(string ACD_ID, string rpF_ID, string StudentNumber)
        {
            PdfUrlModel model = new PdfUrlModel();
            string UserName = SessionHelper.CurrentSession.UserName;
            IEnumerable<PdfUrlModel> pdfUrlModel = _IAssessmentReport.RequestToPdfView(ACD_ID, rpF_ID, UserName, StudentNumber);
            foreach (var item in pdfUrlModel.ToList())
            {
                model.id = item.id;
                model.url = item.url;
                model.androidURL = item.androidURL;
                model.isBlockReportView = item.isBlockReportView;
                model.alertMsg = item.alertMsg;
            }
            return Json(model, JsonRequestBehavior.AllowGet);

        }
        public ActionResult RequestToPdfDownload(string ACD_ID, string rpF_ID, string StudentNumber, string Filename)
        {
            PdfUrlModel model = new PdfUrlModel();
            string UserName = SessionHelper.CurrentSession.UserName;
            IEnumerable<PdfUrlModel> pdfUrlModel = _IAssessmentReport.RequestToPdfView(ACD_ID, rpF_ID, UserName, StudentNumber);
            string Url = pdfUrlModel.FirstOrDefault().androidURL;
            if (!string.IsNullOrWhiteSpace(Url))
            {

                WebClient webClient = new WebClient();
                var fileBytes = webClient.DownloadData(new Uri(Url));

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, Filename + ".pdf");
            }
               return null;
        }
        public ActionResult GetStudentPdfList(string StudenntNumber, string SelectedYearId )
        {
            IEnumerable<PdfInfoModel>pdfInfoModel = _IAssessmentReport.GetPdfListReport(StudenntNumber, SelectedYearId);
            return PartialView("_PdfListInfo", pdfInfoModel);
        }

    }
}