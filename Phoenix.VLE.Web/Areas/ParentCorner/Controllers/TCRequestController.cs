﻿using Newtonsoft.Json;
using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services.ParentCorner.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ParentCorner.Controllers
{
    public class TCRequestController : BaseController
    {
        private readonly ITCRequestService _ITCRequestService;
        readonly string SaledForceCRMURL = ConfigurationManager.AppSettings["SaledForceCRMURL"];
        public TCRequestController(ITCRequestService ITCRequestService)
        {
            _ITCRequestService = ITCRequestService;
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:28 July 2020
        /// Get tc request list.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            StudentTcDetailsRoot studentTcDetails = _ITCRequestService.GetTcRequestStatusByStudentNo(SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber);
            TcRequestmodel model = new TcRequestmodel();
            if (studentTcDetails.responseCode!=null && Convert.ToString(studentTcDetails.responseCode)== "NA")
            {                
                ViewBag.TcRequestMessage = studentTcDetails.responseCode.ToString();
                ViewBag.RedirectUrl = SaledForceCRMURL.ToString() + "?studentID=" + EncryptDecryptHelper.EncryptBySalt(SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber.ToString()) + "&BSU=" + EncryptDecryptHelper.EncryptBySalt(SessionHelper.CurrentSession.CurrentSelectedStudent.SchoolId.ToString()) + "";
                return View("SaveTCRequest", model);
            }
            else if(studentTcDetails.message!=null &&  studentTcDetails.message.ToString() == "No data found")
            {                
                model.DropdownReasonForTransfer = new SelectList(_ITCRequestService.GetReasonList(), "ReasonID", "Reason");
                model.DropdownListTranferdSchool = new SelectList(_ITCRequestService.GetBusinessUnitList(), "SchoolId", "SchoolName");
                model.DropdownListExitInterview = new SelectList(_ITCRequestService.GetQuestionList(), "QuestionId", "Question");
                return View("SaveTCRequest", model);
            }
            
            if (studentTcDetails.success == "false" && studentTcDetails.message == null && studentTcDetails.responseCode == null)
            {
                return RedirectToAction("SaveTCRequest");
            }
            return View(studentTcDetails);
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Get business list.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetBusinessunitList()
        {
            var Result = _ITCRequestService.GetBusinessUnitList();
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Get reason list. 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetReasonList()
        {
            var Result = _ITCRequestService.GetReasonList();
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Get question list.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetQuestionList()
        {
            var Result = _ITCRequestService.GetQuestionList();
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Get transfer type list.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetTransferTypeList()
        {
            var Result = _ITCRequestService.GetTransferTypeList();
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Get TC request data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SaveTCRequest()
        {
            TcRequestmodel model = new TcRequestmodel();
            model.DropdownReasonForTransfer = new SelectList(_ITCRequestService.GetReasonList(), "ReasonID", "Reason");
            model.DropdownListTranferdSchool = new SelectList(_ITCRequestService.GetBusinessUnitList(), "SchoolId", "SchoolName");
            model.DropdownListExitInterview = new SelectList(_ITCRequestService.GetQuestionList(), "QuestionId", "Question");
            return View("SaveTCRequest", model);
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Save TC request data
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveTCRequest(TCRequestEdit obj)
        {
            obj.StudentID = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber;
            obj.IsOnlineTransfer = true;
            string jsonEvents = _ITCRequestService.SaveTCRequest(obj);
            var jsonResult = new { events = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Author:Tejalben
        /// Date:27 July 2020
        /// Save TC exit question data.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveTCExitQuestion(TCExitQuestion obj)
        {
            string jsonEvents = _ITCRequestService.SaveTCExitQuestion(obj);
            var jsonResult = new { events = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }        
    }
}