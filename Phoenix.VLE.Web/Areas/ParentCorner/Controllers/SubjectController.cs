﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Services.SelectSubject.Contracts;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ParentCorner.Controllers
{
    public class SubjectController : Controller
    {
        private readonly ISelectSubjectService _ISelectSubjectService;
        private readonly IGetStudentInfoService _IGetStudentInfoService;
        private readonly IPhoenixAPIParentCornerService _IPhoenixAPIParentCornerService;
        //readonly string OptionGuidelines = ConfigurationManager.AppSettings["TwsOptionGuidelines"];
        public SubjectController(ISelectSubjectService SelectSubjectService, IGetStudentInfoService GetStudentInfoService,IPhoenixAPIParentCornerService PhoenixAPIParentCornerService)
        {
            _ISelectSubjectService = SelectSubjectService;
            _IGetStudentInfoService = GetStudentInfoService;
            _IPhoenixAPIParentCornerService = PhoenixAPIParentCornerService;
        }
        // GET: ParentCorner/Subject
        public ActionResult Index()
        {
            SelectSubjectDashaboard subjectFilter = new SelectSubjectDashaboard();
            List<GetOption> Getoption = new List<GetOption>();
            StudentSubjectInfo modelinfo = new StudentSubjectInfo();
            //"12501000024241"
            var selectedstudent = SessionHelper.CurrentSession.CurrentSelectedStudent;
            //StudentDetail selectedstudent = new StudentDetail();
            //selectedstudent.StudentNumber = "12501000024241";
            Getoption = _ISelectSubjectService.GetCourseWithCategoryList(selectedstudent.StudentNumber);           
            modelinfo.IsCourseAlreadySelected = Getoption.Where(x => x.OptionDetailList.Any(y => y.IsSelect == 1)).ToList().Count > 0 ? true : false;
            StudentInfo studentInfo = _IGetStudentInfoService.GetStudentInfo(selectedstudent.StudentNumber, "EN", "SCHOOL");
            string ViewName = "SelectSubject";
            modelinfo.OptionGuidelines = _IPhoenixAPIParentCornerService.TwsOptionGuidelines;
            modelinfo.AcademicYear = studentInfo.STU_ACADEMIC_YEAR;
            modelinfo.Grade = studentInfo.STU_GRADE;
            modelinfo.StudentNumber = selectedstudent.StudentNumber;
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            List<StudentListInfo> studentlist = new List<StudentListInfo>();
            foreach (var item in studList)
            {
                StudentListInfo model = new StudentListInfo();
                model.StudentFirstName = item.FirstName + " " + item.LastName;
                model.StudentNumber = item.StudentNumber;
                model.StudentLastName = item.LastName;
                model.StudentId = item.StudentId;
                model.SchooolId = item.SchoolId;
                model.schoolAcademicYearid = item.SchoolAcademicYearId;
                studentlist.Add(model);
            }
            modelinfo.Getoption = Getoption;
            subjectFilter.StudentlistInfo = studentlist;
            subjectFilter.Studentsubjectinfo = modelinfo;
            return View(ViewName, subjectFilter);
        }
        public ActionResult GetCourseList(string StudentNumebr)
        {
            SelectSubjectDashaboard subjectFileter = new SelectSubjectDashaboard();
            StudentSubjectInfo studentSubjectInfo = new StudentSubjectInfo();
            //"12501000024241"
            studentSubjectInfo.StudentNumber = StudentNumebr;
            studentSubjectInfo.Getoption = _ISelectSubjectService.GetCourseWithCategoryList(StudentNumebr);
            StudentInfo studentInfo = _IGetStudentInfoService.GetStudentInfo(StudentNumebr, "EN", "SCHOOL");
            studentSubjectInfo.AcademicYear = studentInfo.STU_ACADEMIC_YEAR;
            studentSubjectInfo.Grade = studentInfo.STU_GRADE;
            studentSubjectInfo.OptionGuidelines = _IPhoenixAPIParentCornerService.TwsOptionGuidelines;
            studentSubjectInfo.IsCourseAlreadySelected = studentSubjectInfo.Getoption.Where(x => x.OptionDetailList.Any(y => y.IsSelect == 1)).ToList().Count > 0 ? true : false;
            return PartialView("_GetOptionView", studentSubjectInfo);

        }

        public JsonResult ConfirmSelection(List<SelectCoursesList> values)
        {
            var CourseListInfo = values;
            var selectedstudent = SessionHelper.CurrentSession.UserName;
            SaveCourseModel obj = new SaveCourseModel();
            List<SaveCourse> obj1 = new List<SaveCourse>();
            obj.StudentID = CourseListInfo.FirstOrDefault().StudentId;
            obj.User = SessionHelper.CurrentSession.UserName;
            foreach (var item in CourseListInfo)
            {
                SaveCourse Sc = new SaveCourse();
                Sc.OptionID = item.OptionID;
                Sc.SubjectGradeID = item.SubjectGradeID;
                obj1.Add(Sc);
            }
            obj.SelectCoursesList = obj1;
            //string objResult = string.Empty;
            var objResult = _ISelectSubjectService.PostCourseSelection(obj);
            var jsonResult = new { events = objResult };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }
    }
}