﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Services.ParentCorner.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ParentCorner.Controllers
{
    public class HifzTrackerController : BaseController
    {       

        private readonly IHifzTrackerService _hifzTrackerService;
        private List<VLEFileType> _fileTypeList;
        private readonly IFileService _fileService;
        
        public HifzTrackerController(IHifzTrackerService hifzTrackerService, IFileService fileService)
        {
            _hifzTrackerService = hifzTrackerService;
            _fileService = fileService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
        }
        /// <summary>
        /// Author:Tejalben
        /// Summery:Get hifz tracker list.
        /// Date:02 Aug 2020 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var currentStudent = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber;
            //var studList = _hifzTrackerService.GetHifzTrackerList(SessionHelper.CurrentSession.FamilyStudentList.Select(x => x.StudentNumber).ToList());
            var studList = _hifzTrackerService.GetHifzTrackerList(new List<string>() { currentStudent }).ToList();
            ViewBag.FileExtension = _fileTypeList.Where(m => m.DocumentType == "Audio" || m.DocumentType == "Video" || m.DocumentType== "Recording").Select(r => r.Extension).ToList();
            return View("HifzTracker", studList);
        }

        /// <summary>
        /// Author:Tejalben
        /// Summery:Save hifz tracker file.
        /// Date:02 Aug 2020
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveHifzFile(HifzTrackerFileEdit hifzTrackerFileEdit, List<HttpPostedFileBase> recordings)
        {            
            string result = string.Empty;
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string uname = Request["uploadername"];
            HttpFileCollectionBase files = Request.Files;
            hifzTrackerFileEdit.StudentID = hifzTrackerFileEdit.StudentID;
            if (hifzTrackerFileEdit.HifzTrackerId > 0)
            {
                result = _hifzTrackerService.SaveHifzFile(files, hifzTrackerFileEdit);
            }
            var jsonResult = new { opResult = result };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Author:Tejalben
        /// Summery:Get hifz file using student id.
        /// Date: 16 Aug 2020
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public ActionResult GetHifzTrackerFilesByStudentId(string studentId)
        {
            var studList = _hifzTrackerService.GetHifzTrackerList(new List<string>() { studentId }).ToList();            
            return PartialView("_HifzTrackerDetails", studList.OrderByDescending(x=>x.HifzRelationId));
        }
        public ActionResult GetHifzTrackerByStudentId(string studentId,string status = "")
        {
            IEnumerable<HifzTrackerEdit> lsthifzTracker = new List<HifzTrackerEdit>();
            var studList = _hifzTrackerService.GetHifzTrackerList(new List<string>() { studentId }).ToList();

            if (status == "todo")
            {
                lsthifzTracker = studList.Where(m => m.UploadedFileName == "").ToList();
            }
            else if(status == "completed")
            {
                lsthifzTracker = studList.Where(m => m.UploadedFileName != "").ToList();
            }
            return PartialView("_HiFzFileDetails", lsthifzTracker.OrderByDescending(x=>x.HifzRelationId));
        }

        public ActionResult DownloadHifzTrackerFileInPdf(string fileUrl)
        {            
            if (!string.IsNullOrWhiteSpace(fileUrl))
            {

                WebClient webClient = new WebClient();
                var fileBytes = webClient.DownloadData(new Uri(fileUrl));

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "HifzTrackerFile" + ".pdf");
            }
            return null;
        }
    }
}