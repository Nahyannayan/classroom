﻿using DevExpress.DataProcessing;
using Newtonsoft.Json;
using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Logger;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Models.EditModels.MyFiles;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ParentCorner.Controllers
{
    public class EnrollActivitiesController : BaseController
    {
        private readonly IEnrollActivitiesService _IEnrollActivitiesService;
        private readonly IPaymentService _paymentService;
        private ILoggerClient _loggerClient;

        public EnrollActivitiesController(IEnrollActivitiesService enrollactivitiesservice, IPaymentService paymentService)
        {
            _IEnrollActivitiesService = enrollactivitiesservice;
            _paymentService = paymentService;
            _loggerClient = LoggerClient.Instance;
        }
        // GET: ParentCorner/EnrollActivities
        public ActionResult Index(string StudentNumber = "")
        {
            _loggerClient.LogWarning("EnrollActivity Index method call.");
            //Add Student
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            List<StudentEnrollList> StudentNameEnrollLst = new List<StudentEnrollList>();
            foreach (var item in studList)
            {
                StudentEnrollList StudeEnrollName = new StudentEnrollList();
                StudeEnrollName.studentName = item.FirstName + " " + item.LastName;
                StudeEnrollName.studentNo = item.StudentNumber;
                StudentNameEnrollLst.Add(StudeEnrollName);
            }
            //End Student
            AllActivities AllStudentActivityList = new AllActivities();
            var viewname = "EnrollActivities";
            var selectedstudent = SessionHelper.CurrentSession.CurrentSelectedStudent;
            EnrolllActivityModel model = new EnrolllActivityModel();

            List<EnrolllActivityModel> AvailUpcomingEnrolllActivityList = _IEnrollActivitiesService.GetListOfActivities(selectedstudent.StudentNumber);
            List<EnrolllActivityModel> FilterActivities = new List<EnrolllActivityModel>();
            #region Avalible activity
            _loggerClient.LogWarning("UTC time:" + DateTime.UtcNow.ConvertUtcToLocalTime().Value.Date + " Current time:"+DateTime.Now);

            FilterActivities = AvailUpcomingEnrolllActivityList.Where(m => Convert.ToDateTime(m.eventRequestStartDate) <= DateTime.UtcNow.ConvertUtcToLocalTime().Value.Date 
            && Convert.ToDateTime(m.eventRequestEndDate).Date >= DateTime.UtcNow.ConvertUtcToLocalTime().Value.Date && string.IsNullOrEmpty(m.studentId) 
            && (string.IsNullOrEmpty(m.apdId) || m.apdId == "0")).ToList();

            FilterActivities = FilterActivities.GroupBy(g => g.aldId)
            .Select(s => new EnrolllActivityModel
    {
        aldId = s.Key,
        groupName = s.Select(ss => ss.groupName).FirstOrDefault(),
        groupId = s.Select(ss => ss.groupId).FirstOrDefault(),
        groupMaxLimit = s.Select(ss => ss.groupMaxLimit).FirstOrDefault(),
        activityIconPath = s.Select(ss => ss.activityIconPath).FirstOrDefault(),
        feeCollType = s.Select(ss => ss.feeCollType).FirstOrDefault(),
        eventName = s.Select(ss => ss.eventName).FirstOrDefault(),
        isApprovalRequired = s.Select(ss => ss.isApprovalRequired).FirstOrDefault(),
        eventDescription = s.Select(ss => ss.eventDescription).FirstOrDefault(),
        eventDate = s.Select(ss => ss.eventDate).FirstOrDefault(),
        feeTypeId = s.Select(ss => ss.feeTypeId).FirstOrDefault(),
        accountId = s.Select(ss => ss.accountId).FirstOrDefault(),
        eventId = s.Select(ss => ss.eventId).FirstOrDefault(),
        taxCode = s.Select(ss => ss.taxCode).FirstOrDefault(),
        maximumCount = s.Select(ss => ss.maximumCount).FirstOrDefault(),
        allowMultiple = s.Select(ss => ss.allowMultiple).FirstOrDefault(),
        availableSeats = s.Select(ss => ss.availableSeats).FirstOrDefault(),
        eventCost = s.Select(ss => ss.eventCost).FirstOrDefault(),
        applicableGrades = string.Join(",", s.Select(ss => ss.applicableGrades)),
        bsuId = s.Select(ss => ss.bsuId).FirstOrDefault(),
        eventRequestStartDate = s.Select(ss => ss.eventRequestStartDate).FirstOrDefault(),
        eventRequestEndDate = s.Select(ss => ss.eventRequestEndDate).FirstOrDefault(),
        eventLocation = s.Select(ss => ss.eventLocation).FirstOrDefault(),
        studentId = s.Select(ss => ss.studentId).FirstOrDefault(),
        paymentStatus = s.Select(ss => ss.paymentStatus).FirstOrDefault(),
        enrollmentStatus = s.Select(ss => ss.enrollmentStatus).FirstOrDefault(),
        eventStartDate = s.Select(ss => ss.eventStartDate).FirstOrDefault(),
        eventEndDate = s.Select(ss => ss.eventEndDate).FirstOrDefault(),
        apdId = s.Select(ss => ss.apdId).FirstOrDefault(),
        eventRequestedOn = s.Select(ss => ss.eventRequestedOn).FirstOrDefault(),
        feedbackRating = s.Select(ss => ss.feedbackRating).FirstOrDefault(),
        feedbackComments = s.Select(ss => ss.feedbackComments).FirstOrDefault(),
        showUnsubscribe = s.Select(ss => ss.showUnsubscribe).FirstOrDefault(),
        paymentProcessingCharge = s.Select(ss => ss.paymentProcessingCharge).FirstOrDefault(),
        couponCount = s.Select(ss => ss.couponCount).FirstOrDefault(),
        paymentTypeID = s.Select(ss => ss.paymentTypeID).FirstOrDefault(),
        onlinePaymentAllowed = s.Select(ss => ss.onlinePaymentAllowed).FirstOrDefault(),
        showPayOnline = s.Select(ss => ss.showPayOnline).FirstOrDefault(),
        isTermsExist = s.Select(ss => ss.isTermsExist).FirstOrDefault(),
        termAndConditions = s.Select(ss => ss.termAndConditions).FirstOrDefault(),
    }).ToList();

            var FilterGrouplist = FilterActivities.Select(x => new { x.groupId, x.groupName, x.groupMaxLimit }).Distinct();
            List<EnrollActivitiesDashaboard> ActivitiesByGroup = new List<EnrollActivitiesDashaboard>();
            foreach (var available in FilterGrouplist)
            {
                EnrollActivitiesDashaboard ActivityGroup = new EnrollActivitiesDashaboard();// Load AvailableActivity List
                ActivityGroup.StudentNumber = selectedstudent.StudentNumber;
                ActivityGroup.groupMaxLimit = available.groupMaxLimit;
                ActivityGroup.GroupName = available.groupName;
                ActivityGroup.GroupId = available.groupId;
                ActivityGroup.Activity = FilterActivities.Where(m => m.groupName == available.groupName).ToList();
                ActivitiesByGroup.Add(ActivityGroup);
            }

            AllStudentActivityList.Avaliable = ActivitiesByGroup;
            #endregion

            #region Upcoming activity
            FilterActivities = AvailUpcomingEnrolllActivityList.Where(m => Convert.ToDateTime(m.eventRequestStartDate) > DateTime.UtcNow.ConvertUtcToLocalTime().Value.Date && string.IsNullOrEmpty(m.studentId) && (string.IsNullOrEmpty(m.apdId) || (m.apdId == "0"))).ToList();

            FilterActivities = FilterActivities.GroupBy(g => g.aldId)
             .Select(s => new EnrolllActivityModel
    {
        aldId = s.Key,
        groupName = s.Select(ss => ss.groupName).FirstOrDefault(),
        groupId = s.Select(ss => ss.groupId).FirstOrDefault(),
        groupMaxLimit = s.Select(ss => ss.groupMaxLimit).FirstOrDefault(),
        activityIconPath = s.Select(ss => ss.activityIconPath).FirstOrDefault(),
        feeCollType = s.Select(ss => ss.feeCollType).FirstOrDefault(),
        eventName = s.Select(ss => ss.eventName).FirstOrDefault(),
        isApprovalRequired = s.Select(ss => ss.isApprovalRequired).FirstOrDefault(),
        eventDescription = s.Select(ss => ss.eventDescription).FirstOrDefault(),
        eventDate = s.Select(ss => ss.eventDate).FirstOrDefault(),
        feeTypeId = s.Select(ss => ss.feeTypeId).FirstOrDefault(),
        accountId = s.Select(ss => ss.accountId).FirstOrDefault(),
        eventId = s.Select(ss => ss.eventId).FirstOrDefault(),
        taxCode = s.Select(ss => ss.taxCode).FirstOrDefault(),
        maximumCount = s.Select(ss => ss.maximumCount).FirstOrDefault(),
        allowMultiple = s.Select(ss => ss.allowMultiple).FirstOrDefault(),
        availableSeats = s.Select(ss => ss.availableSeats).FirstOrDefault(),
        eventCost = s.Select(ss => ss.eventCost).FirstOrDefault(),
        applicableGrades = string.Join(",", s.Select(ss => ss.applicableGrades)),
        bsuId = s.Select(ss => ss.bsuId).FirstOrDefault(),
        eventRequestStartDate = s.Select(ss => ss.eventRequestStartDate).FirstOrDefault(),
        eventRequestEndDate = s.Select(ss => ss.eventRequestEndDate).FirstOrDefault(),
        eventLocation = s.Select(ss => ss.eventLocation).FirstOrDefault(),
        studentId = s.Select(ss => ss.studentId).FirstOrDefault(),
        paymentStatus = s.Select(ss => ss.paymentStatus).FirstOrDefault(),
        enrollmentStatus = s.Select(ss => ss.enrollmentStatus).FirstOrDefault(),
        eventStartDate = s.Select(ss => ss.eventStartDate).FirstOrDefault(),
        eventEndDate = s.Select(ss => ss.eventEndDate).FirstOrDefault(),
        apdId = s.Select(ss => ss.apdId).FirstOrDefault(),
        eventRequestedOn = s.Select(ss => ss.eventRequestedOn).FirstOrDefault(),
        feedbackRating = s.Select(ss => ss.feedbackRating).FirstOrDefault(),
        feedbackComments = s.Select(ss => ss.feedbackComments).FirstOrDefault(),
        showUnsubscribe = s.Select(ss => ss.showUnsubscribe).FirstOrDefault(),
        paymentProcessingCharge = s.Select(ss => ss.paymentProcessingCharge).FirstOrDefault(),
        couponCount = s.Select(ss => ss.couponCount).FirstOrDefault(),
        paymentTypeID = s.Select(ss => ss.paymentTypeID).FirstOrDefault(),
        onlinePaymentAllowed = s.Select(ss => ss.onlinePaymentAllowed).FirstOrDefault(),
        showPayOnline = s.Select(ss => ss.showPayOnline).FirstOrDefault(),
        isTermsExist = s.Select(ss => ss.isTermsExist).FirstOrDefault(),
        termAndConditions = s.Select(ss => ss.termAndConditions).FirstOrDefault(),
    }).ToList();
            FilterGrouplist = FilterActivities.Select(x => new { x.groupId, x.groupName, x.groupMaxLimit }).Distinct();
            ActivitiesByGroup = new List<EnrollActivitiesDashaboard>();
            foreach (var available in FilterGrouplist)
            {
                EnrollActivitiesDashaboard ActivityGroup = new EnrollActivitiesDashaboard();// Load AvailableActivity List
                ActivityGroup.StudentNumber = selectedstudent.StudentNumber;
                ActivityGroup.groupMaxLimit = available.groupMaxLimit;
                ActivityGroup.GroupName = available.groupName;
                ActivityGroup.GroupId = available.groupId;
                ActivityGroup.Activity = FilterActivities.Where(m => m.groupName == available.groupName).ToList();
                ActivitiesByGroup.Add(ActivityGroup);
            }
            AllStudentActivityList.Upcoming = ActivitiesByGroup;
            #endregion

            #region Inprocess activity
            List<EnrolllActivityModel> InProcessActivities = new List<EnrolllActivityModel>();
            InProcessActivities = AvailUpcomingEnrolllActivityList.Where(m => String.IsNullOrEmpty(m.paymentStatus) == false && m.paymentStatus.ToUpper() != Constants.PaymentStatusCompleted && Convert.ToDateTime(m.eventRequestEndDate).Date >= DateTime.UtcNow.ConvertUtcToLocalTime().Value.Date).ToList();

            InProcessActivities.ForEach((item) =>
            {
                item.EnrolledSiblingStudentIDs = StudentNameEnrollLst.Where(x => x.studentNo == item.studentId).FirstOrDefault().studentNo;
                item.EnrolledSiblingNames = StudentNameEnrollLst.Where(x => x.studentNo == item.studentId).FirstOrDefault().studentName;
            });
            AllStudentActivityList.InProcess.AddRange(InProcessActivities);

            #endregion

            #region Enroll  Activity
            var EnrollActivityList = AvailUpcomingEnrolllActivityList.Where(m => m.paymentStatus != null && m.paymentStatus.ToUpper() == Constants.PaymentStatusCompleted).ToList();

            EnrollActivityList.ForEach((item) =>
            {
                item.EnrolledSiblingStudentIDs = StudentNameEnrollLst.Where(x => x.studentNo == item.studentId).FirstOrDefault().studentNo;
                item.EnrolledSiblingNames = StudentNameEnrollLst.Where(x => x.studentNo == item.studentId).FirstOrDefault().studentName;
            });

            AllStudentActivityList.Enrolled = EnrollActivityList;
            #endregion
            return View(viewname, AllStudentActivityList);
        }

        public JsonResult RequestToEnroll(int aldId, string Studentnumber)
        {
            RequestEnroll Result = _IEnrollActivitiesService.RequestEnrollInfo(aldId, Studentnumber);
            Result.studentNo = Studentnumber;

            return Json(Result, JsonRequestBehavior.AllowGet);

        }
        public ActionResult SendRequest(string stu_no, string alD_ID, int studentCount, string radioselect, int id)
        {
            bool Result = _IEnrollActivitiesService.SendRequest(stu_no, alD_ID, studentCount, radioselect, id, null);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SelectMonthFilter(int NumberOfMonths, string StudentNumber)
        {
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            List<StudentEnrollList> StudentNameEnrollLst = new List<StudentEnrollList>();
            foreach (var item in studList)
            {
                StudentEnrollList StudeEnrollName = new StudentEnrollList();
                StudeEnrollName.studentName = item.FirstName + " " + item.LastName;
                StudeEnrollName.studentNo = item.StudentNumber;
                StudentNameEnrollLst.Add(StudeEnrollName);
            }
            List<EnrolllActivityModel> Enrolled = new List<EnrolllActivityModel>();
            DateTime Monthcount = DateTime.Today.AddMonths(-NumberOfMonths);
            var selectedstudent = SessionHelper.CurrentSession.CurrentSelectedStudent;
            List<EnrolllActivityModel> EnrolllActivityData = _IEnrollActivitiesService.GetListOfActivities(selectedstudent.StudentNumber);
            Enrolled = EnrolllActivityData.Where(m => m.paymentStatus != null && m.paymentStatus.ToUpper() == Constants.PaymentStatusCompleted).ToList();
            foreach (var StuList in Enrolled)
            {
                StuList.StuEnrollListt = StudentNameEnrollLst;
            }

            Enrolled.ForEach((item) =>
            {
                item.EnrolledSiblingStudentIDs = StudentNameEnrollLst.Where(x => x.studentNo == item.studentId).FirstOrDefault().studentNo;
                item.EnrolledSiblingNames = StudentNameEnrollLst.Where(x => x.studentNo == item.studentId).FirstOrDefault().studentName;
            });
            return PartialView("_EnrolledActivityView", Enrolled);
        }
        public ActionResult ActivityFeedback(int StarRating, int ActivityId)
        {
            ActiveFeedback feedback = new ActiveFeedback();
            feedback.ActivityId = ActivityId;
            feedback.RatingValue = StarRating;
            feedback.ParentUserId = SessionHelper.CurrentSession.UserName;
            feedback.Comments = "";
            feedback.FeedbackId = 0;
            bool Result = _IEnrollActivitiesService.PostActivityFeedback(feedback);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UnsubscribeActivity(int APD_ID)
        {
            bool Result = _IEnrollActivitiesService.PostUnsubscribeActivity(APD_ID);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PayOnlinePayment(string StudentID, string onlinePaymentAllowed, string paymentTypeID, string eventCost, string feeTypeId, string eventName, string paymentProcessingCharge, string apdId, string feeCollType)
        {
            string Source = "ACTIVITY";
            string jsonEvents = "";
            int i = 0;
            OnlinePayResponse objResult = new OnlinePayResponse();
            List<string> StudentNumberList = new List<string>();
            StudentNumberList = StudentID.Split('|').ToList(); //Multiple Student
            var visitorInfo = new VisitorInfo();
            List<CommonStudentFee> LstStudentsFee = new List<CommonStudentFee>();
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();
            string IpDetails = IpAddress + ":" + HostIpAddress;
            //If More then 1 Studemt the  total Event Cost for 2 Student is 200 Then objAMT.PayingAmount=100 For each student
            //objAMT.PayingAmount=eventCost/no of student
            //CommonfeeDetails.PayAmount=eventCost/no of student


            foreach (var item in StudentNumberList)
            {

                CommonStudentFee objAMT = new CommonStudentFee();
                CommonFeeDetails CommonfeeDetails = new CommonFeeDetails();
                List<CommonFeeDetails> feedetail = new List<CommonFeeDetails>();
                objAMT.STU_NO = item.ToString();
                objAMT.OnlinePaymentAllowed = onlinePaymentAllowed;
                objAMT.UserMessageforOnlinePaymentBlock = "";
                objAMT.PaymentTypeID = paymentTypeID;
                objAMT.PayingAmount = Convert.ToDouble(eventCost);
                objAMT.IpAddress = IpDetails;
                objAMT.PaymentProcessingCharge = Convert.ToDouble(paymentProcessingCharge);
                objAMT.PayMode = "";
                objAMT.DiscountDetails = null;
                CommonfeeDetails.BlockPayNow = false;
                CommonfeeDetails.AdvancePaymentAvailable = false;
                CommonfeeDetails.FeeID = feeTypeId;
                CommonfeeDetails.FeeDescription = eventName;
                CommonfeeDetails.DueAmount = 0;
                CommonfeeDetails.PayAmount = Convert.ToDouble(eventCost);
                CommonfeeDetails.OriginalAmount = 0;
                CommonfeeDetails.DiscAmount = 0;
                feedetail.Add(CommonfeeDetails);
                objAMT.StudFeeDetails = feedetail;
                LstStudentsFee.Add(objAMT);
                i++;
            }
            objResult = _paymentService.SubmitOnlinePaymentRequest(Source, LstStudentsFee, apdId, "/parentcorner/EnrollActivities", feeCollType);
            jsonEvents = JsonConvert.SerializeObject(objResult);
            var jsonResult = new { result = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRequestAndPayOnlineDetails(RequestAndPayOnlineEdit requestAndPayOnline)
        {
            bool IsRequestEnrolled = false;
            if (requestAndPayOnline.EnrollmentRequestType == 3)
            {
                IsRequestEnrolled = _IEnrollActivitiesService.SendRequest(requestAndPayOnline.StudentNumber.Replace("|", ","), requestAndPayOnline.AlD_ID, requestAndPayOnline.Couponcount, requestAndPayOnline.Radioselect, requestAndPayOnline.Id, requestAndPayOnline.Comment);
            }
            var activity = _IEnrollActivitiesService.GetActivity(requestAndPayOnline.StudentNumber.Split('|').FirstOrDefault().ToString(), Convert.ToInt32(requestAndPayOnline.AlD_ID),0);

            string Source = "ACTIVITY";
            string jsonEvents = "";
            int i = 0;
            OnlinePayResponse objResult = new OnlinePayResponse();
            List<string> StudentNumberList = new List<string>();
            StudentNumberList = requestAndPayOnline.StudentNumber.Split('|').ToList(); //Multiple Student
            var visitorInfo = new VisitorInfo();
            List<CommonStudentFee> LstStudentsFee = new List<CommonStudentFee>();
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();
            string IpDetails = IpAddress + ":" + HostIpAddress;
            foreach (var item in StudentNumberList)
            {
                List<CommonFeeDetails> feedetail = new List<CommonFeeDetails>();
                CommonStudentFee objAMT = new CommonStudentFee();
                CommonFeeDetails CommonfeeDetails = new CommonFeeDetails();
                objAMT.STU_NO = item.ToString();
                objAMT.OnlinePaymentAllowed = requestAndPayOnline.OnlinePaymentAllowed.ToString();
                objAMT.UserMessageforOnlinePaymentBlock = "";
                objAMT.PaymentTypeID = requestAndPayOnline.PaymentTypeID.ToString();
                objAMT.PayingAmount = StudentNumberList.Count > 0 ? Convert.ToDouble(requestAndPayOnline.EventCost / StudentNumberList.Count) : Convert.ToDouble(requestAndPayOnline.EventCost);
                objAMT.IpAddress = IpDetails;
                objAMT.PaymentProcessingCharge = Convert.ToDouble(requestAndPayOnline.PaymentProcessingCharge);
                objAMT.PayMode = "";
                objAMT.DiscountDetails = null;
                CommonfeeDetails.BlockPayNow = false;
                CommonfeeDetails.AdvancePaymentAvailable = false;
                CommonfeeDetails.FeeID = requestAndPayOnline.FeeTypeId.ToString();
                CommonfeeDetails.FeeDescription = requestAndPayOnline.EventName;
                CommonfeeDetails.DueAmount = 0;
                CommonfeeDetails.PayAmount = StudentNumberList.Count > 0 ? Convert.ToDouble(requestAndPayOnline.EventCost / StudentNumberList.Count) : Convert.ToDouble(requestAndPayOnline.EventCost);
                CommonfeeDetails.OriginalAmount = 0;
                CommonfeeDetails.DiscAmount = 0;
                CommonfeeDetails.ActivityRefID = Convert.ToInt16(activity.Where(x => x.studentId == item).FirstOrDefault().apdId);
                feedetail.Add(CommonfeeDetails);
                objAMT.StudFeeDetails = feedetail;
                LstStudentsFee.Add(objAMT);
                i++;
            }
            objResult = _paymentService.SubmitOnlinePaymentRequestMulti(Source, LstStudentsFee, "/parentcorner/EnrollActivities", requestAndPayOnline.FeeCollType);
            jsonEvents = JsonConvert.SerializeObject(objResult);
            var jsonResult = new { result = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SendRequest(RequestAndPayOnlineEdit requestAndPayOnline)
        {
            bool IsRequestEnrolled = false;
            
            IsRequestEnrolled = _IEnrollActivitiesService.SendRequest(requestAndPayOnline.StudentNumber.Replace("|", ","), requestAndPayOnline.AlD_ID, requestAndPayOnline.Couponcount, requestAndPayOnline.Radioselect, requestAndPayOnline.Id, requestAndPayOnline.Comment);
            //IsRequestEnrolled = _IEnrollActivitiesService.SendRequest(requestAndPayOnline.StudentNumber.Replace("|", ","), requestAndPayOnline.AlD_ID, requestAndPayOnline.StudentNumber.Split('|').Count(), requestAndPayOnline.Radioselect, requestAndPayOnline.Id, requestAndPayOnline.Comment);


            return Json(IsRequestEnrolled, JsonRequestBehavior.AllowGet);
        }

    }
}