﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ParentCorner.Controllers
{
    public class LeaveController : BaseController
    {
        //private readonly ISchoolSpaceService _schoolSpaceService;
        //private readonly ITeacherDashboardService _teacherDashboardService;
        private readonly IStudentService _studentService;
        private readonly IUserService _userService;
        private readonly ILeaveService _leaveService;
        private readonly IFeePaymentsService _feePaymentService;
        //ICalendarService _calendarService;
        //private readonly IEventCategoryService _eventCategoryService;
        private readonly IFileService _fileService;
        private List<VLEFileType> _fileTypeList;
        List<GalleryFile> galleryFiles = new List<GalleryFile>();
        private ILoggerClient _loggerClient;

        public LeaveController(
              //  ITeacherDashboardService teacherDashboardService
              //, ISchoolSpaceService schoolSpaceService
              IStudentService studentService, IUserService userService
            , IEventCategoryService eventCategoryService
            , ILeaveService leaveService
            , IFileService fileService
            , IFeePaymentsService feePaymentService
            //,ICalendarService calendarService
            )
        {
            //_teacherDashboardService = teacherDashboardService;
            //_schoolSpaceService = schoolSpaceService;
            _studentService = studentService;
            _userService = userService;
            //_eventCategoryService = eventCategoryService;
            _leaveService = leaveService;
            _fileService = fileService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
            _feePaymentService = feePaymentService;
            //_calendarService = calendarService;
        }
        // GET: Users/SchoolSpaces

        // GET: ParentCorner/Leave
        public ActionResult Index(string selectSection = "")
        {
            var viewName = "LeaveDetails";
            string isStudSel = string.Empty; string isLType = string.Empty;
            if (!string.IsNullOrEmpty(selectSection))
            {
                string[] strSel = selectSection.Split(new char[] { '-' });
                isLType = strSel[0];
                isStudSel = strSel[1];
            }

            string username = string.Empty, language = string.Empty;
            username = SessionHelper.CurrentSession.UserName;
            ICommonService _commonService = new CommonService();
            var currentLanguage = _commonService.GetUserCurrentLanguage(SessionHelper.CurrentSession.LanguageId);
            language = Convert.ToString(currentLanguage.SystemLanguageCode);
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            LeaveRequest objVM = new LeaveRequest();
            List<StudentsLeaveDetails> objModel = new List<StudentsLeaveDetails>();
            var selectedStudent = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber;
            foreach (var item in studList)
            {
                StudentsLeaveDetails obj = new StudentsLeaveDetails()
                {
                    StudentNo = item.StudentNumber,
                    StudentName = item.FirstName + " " + item.LastName,
                    IsSelected = !string.IsNullOrEmpty(isStudSel) ? (isStudSel == item.StudentNumber ? true : false) : (selectedStudent == item.StudentNumber ? true : false),
                    IsSelLeaveType = !string.IsNullOrEmpty(isLType) ? isLType == "LR" && (isStudSel == item.StudentNumber) ? true : false : false,
                    LstLeaveDetails = _leaveService.GetLeaveRequestsByStudentlId(item.StudentNumber, username, language).Where(m => m.Leave_Type != null && m.Approval_Status != null).ToList(),
                    LstLeaveTypes = _leaveService.GetLeaveTypes("LEAVE_TYPE", item.SchoolId, item.PhoenixSchoolAcademicYearId),
                };
                objModel.Add(obj);
            }
            ViewBag.FileExtension = _fileTypeList.Where(m => m.DocumentType == "Document" || m.DocumentType == "Image").Select(r => r.Extension).ToList();
            ViewBag.StudList = objModel;
            return View(viewName,objModel);
            //return RedirectToAction("LeaveDetails");
        }

        //Leave request START
        public ActionResult LeaveDetails(string selectSection = "")
        {
            string isStudSel = string.Empty; string isLType = string.Empty;
            if (!string.IsNullOrEmpty(selectSection))
            {
                string[] strSel = selectSection.Split(new char[] { '-' });
                isLType = strSel[0];
                isStudSel = strSel[1];
            }
            
            string username = string.Empty, language = string.Empty;
            username = SessionHelper.CurrentSession.UserName;
            ICommonService _commonService = new CommonService();
            var currentLanguage = _commonService.GetUserCurrentLanguage(SessionHelper.CurrentSession.LanguageId);
            language = Convert.ToString(currentLanguage.SystemLanguageCode);
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            LeaveRequest objVM = new LeaveRequest();
            List<StudentsLeaveDetails> objModel = new List<StudentsLeaveDetails>();
            int cnt = 0;
            foreach (var item in studList)
            {
                StudentsLeaveDetails obj = new StudentsLeaveDetails()
                {
                    StudentNo = item.StudentNumber,
                    StudentName = item.FirstName + " " + item.LastName,
                    IsSelected = !string.IsNullOrEmpty(isStudSel) ? (isStudSel == item.StudentNumber ? true : false) : (cnt == 0 ? true : false),
                    IsSelLeaveType = !string.IsNullOrEmpty(isLType) ? isLType == "LR" && (isStudSel == item.StudentNumber) ? true : false : false,
                    LstLeaveDetails = _leaveService.GetLeaveRequestsByStudentlId(item.StudentNumber, username, language).Where(m=>m.Leave_Type != null && m.Approval_Status != null).ToList(),
                    LstLeaveTypes = _leaveService.GetLeaveTypes("LEAVE_TYPE", item.SchoolId, item.PhoenixSchoolAcademicYearId),
                };
                objModel.Add(obj);
                cnt++;
            }
            ViewBag.FileExtension = _fileTypeList.Where(m => m.DocumentType == "Document" || m.DocumentType == "Image").Select(r => r.Extension).ToList();
            ViewBag.StudList = objModel;
            return View(objModel);
        }
        public ActionResult LeaveApplicationModal(string LeaveType, string StudNo, int RefId = 0, string FromDate = "")
        {
           
            string username = string.Empty, language = string.Empty;
            username = SessionHelper.CurrentSession.UserName;
            ICommonService _commonService = new CommonService();
            var currentLanguage = _commonService.GetUserCurrentLanguage(SessionHelper.CurrentSession.LanguageId);
            language = Convert.ToString(currentLanguage.SystemLanguageCode);
            var item = SessionHelper.CurrentSession.FamilyStudentList.Where(m=>m.StudentNumber == StudNo).FirstOrDefault();
            LeaveRequest objVM = new LeaveRequest();
                   
            StudentsLeaveDetails obj = new StudentsLeaveDetails()
            {
                LstLeaveDetails = _leaveService.GetLeaveRequestsByStudentlId(StudNo, username, language).Where(m => m.Leave_Type != null && m.Approval_Status != null).ToList(),
            };
            obj.LstLeaveTypes = _leaveService.GetLeaveTypes("LEAVE_TYPE", item.SchoolId, item.PhoenixSchoolAcademicYearId);

            if (!string.IsNullOrEmpty(FromDate))
            {
              objVM = obj.LstLeaveDetails.Where(m => m.Ref_Id == RefId && m.Leave_Type == LeaveType && m.From_Date == FromDate).FirstOrDefault();
              objVM.UploadedImageName = objVM.Attachment_FILE_NAME;
            }
            
            objVM.StudentNo = item.StudentNumber;
            objVM.Leave_Type = LeaveType;
            objVM.StudentName = item.FirstName + " " + item.LastName;
            string leavTyp = !string.IsNullOrEmpty(objVM.Attendance_Leave_Type) ? objVM.Attendance_Leave_Type.ToLower() : "";
            
            ViewBag.LeaveReasonList = obj.LstLeaveTypes;
            ViewBag.FileExtension = _fileTypeList.Where(m => m.DocumentType.ToLower() == "document" || m.DocumentType.ToLower() == "image").Select(r => r.Extension).ToList();
            string isEdit = "view"; string leavReasonId = null;
            if (string.IsNullOrEmpty(FromDate))
            {
                isEdit = "write"; 
            }
            else { 
                isEdit  =  objVM.Enable_Button == 1 ? "edit" : "view";
                if(objVM.Attendance_Leave_Type == null)
                {
                    leavReasonId = "";
                }
                else if (objVM.Attendance_Leave_Type.ToLower()=="absent")
                {
                    leavReasonId = Convert.ToString(obj.LstLeaveTypes.Where(m => m.Id == objVM.APD_ID).Select(m => m.Id).FirstOrDefault());
                }
                else
                {
                    leavReasonId = obj.LstLeaveTypes.Where(m => m.Descr.ToLower() == objVM.Attendance_Leave_Type.ToLower()).Select(m => m.Id).FirstOrDefault().ToString();
                }
                
            }
            objVM.IsAddMode = isEdit == "view" ? false : true;
           
            objVM.LeaveReasonId = leavTyp != "" ? leavReasonId.ToString() : "";
            ViewBag.IsAddViewEdit = isEdit;           
            return PartialView("_LeaveApplicationModal", objVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult LeaveRequestSubmit()
        {
            //TokenResult Tokanresult = _leaveService.GetAuthorizationTokenAsync();
            //string access_token = Tokanresult.token_type + " " + Tokanresult.access_token;
            AttachemtStatus stsRetain = AttachemtStatus.RETAIN;
            AttachemtStatus stsDelete = AttachemtStatus.DELETE;
            AttachemtStatus stsUpdate = AttachemtStatus.UPDATE;
            AttachemtStatus stsInsert = AttachemtStatus.INSERT;
            HttpFileCollectionBase files = Request.Files;
            string jsonEvents = ""; string fname = string.Empty;
            LeaveApplication objMod = new LeaveApplication();
            objMod.SLA_Id = Convert.ToInt32(Request.Params["refId"]);
            objMod.FromDT = Request.Params["Fromdate"];
            objMod.ToDT = Request.Params["ToDate"];
            objMod.Remark = Request.Params["remark"];
            objMod.SLA_Type = Request.Params["SLAType"] == "Leave Request" ? "LR" : "LN";
            objMod.StudentNo = Request.Params["stuId"];
            objMod.LeaveReasonId = Convert.ToInt32(Request.Params["Leavetype"]);
            objMod.BEdit = Convert.ToBoolean(Request.Params["isEdit"]);
            string existingFile  = Convert.ToString(Request.Params["UploadedImageName"]);
            if (string.IsNullOrEmpty(objMod.SLA_Type))
            {
                objMod.SLA_Type = "LR";
            }
            
            if (files.Count > 0)
            {
                HttpPostedFileBase file = files[0];
                // Checking for Internet Explorer  
                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    fname = file.FileName;
                }
                // Get the complete folder path and store the file inside it.  
                fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                file.SaveAs(fname);

                objMod.AttachmentStatus = objMod.SLA_Id > 0 ? stsUpdate.ToString() : "";
                //jsonEvents = _leaveService.LeaveRequestSubmit(files, objMod);

            }
            else
            {
                objMod.AttachmentStatus = objMod.SLA_Id > 0 ? (!string.IsNullOrEmpty(existingFile) ? stsRetain.ToString() : stsDelete.ToString()) : "";
                //jsonEvents = _leaveService.EditLeaveRequest(objMod);
            }
            

            jsonEvents = _leaveService.LeaveRequestSubmit(files, objMod);
            var jsonResult = new { events = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveLeaveApplication(LeaveRequest model, HttpPostedFileBase BadgeFile)
        {
            var jsonResult = new { events = "" };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
    }
}