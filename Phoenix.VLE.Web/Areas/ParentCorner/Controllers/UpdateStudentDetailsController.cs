﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.Logger;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Services.ParentCorner.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebGrease.Css.Extensions;

namespace Phoenix.VLE.Web.Areas.ParentCorner.Controllers
{
    public class UpdateStudentDetailsController : Controller
    {
        private readonly IGetStudentInfoService _GetStudentInfoService;
        private readonly IStudentUpdateDetailsService _StudentUpdateDetailsService;
        public List<SelectListItem> SelectCompanyList;
        private ILoggerClient _loggerClient;
        string systemLanguageCode = string.Empty;
        public UpdateStudentDetailsController(IGetStudentInfoService GetStudentInfoService, IStudentUpdateDetailsService StudentUpdateDetailsService)
        {
            _GetStudentInfoService = GetStudentInfoService;
            _StudentUpdateDetailsService = StudentUpdateDetailsService;
            _loggerClient = LoggerClient.Instance;
            systemLanguageCode= LocalizationHelper.CurrentSystemLanguage.SystemLanguageCode;
        }


        /// <summary>
        /// Get update student details.
        /// </summary>
        /// <returns>student details</returns>
        [HttpGet]
        public ActionResult Index()
        {
            StringBuilder sb = new StringBuilder();
            StudentUpdateDetailGeneral Studentupdatedetail = new StudentUpdateDetailGeneral();
            try
            {
                FamilyHistory familyHistory = new FamilyHistory();
                FatherDetails fatherdetails = new FatherDetails();
                MotherDetails motherdetails = new MotherDetails();
                GuardianDetails guardianDetails = new GuardianDetails();
                ParentFeeSponsor parentFeeSponsor = new ParentFeeSponsor();
                HealthDetails healthDetails = new HealthDetails();
                List<HistoryOfIllnessInfectious> historyOfIllnessInfectious = new List<HistoryOfIllnessInfectious>();
                List<HistoryOfIllnessNonInfectious> historyOfIllnessNonInfectious = new List<HistoryOfIllnessNonInfectious>();
                LanguageDetails languageDetails = new LanguageDetails();
                GetStudentDetail Getstudentdetails = new GetStudentDetail();
                ContactInfoDetails ContactinfoDetails = new ContactInfoDetails();
                List<SelectListItem> selectList = new List<SelectListItem>();
                var selectedstudent = SessionHelper.CurrentSession.CurrentSelectedStudent;              
                
                var Student_Info = _GetStudentInfoService.GetStudentInfo(selectedstudent.StudentNumber, systemLanguageCode, SessionHelper.CurrentSession.BusinessUnitType);
                Studentupdatedetail.STU_ACD_ID = Student_Info.STU_ACD_ID;
                Studentupdatedetail.Stu_BSU_Id = Student_Info.Stu_BSU_Id;
                Studentupdatedetail.StuId = Student_Info.StuId;
                Studentupdatedetail.STU_FullName = Student_Info.STU_FullName;
                Studentupdatedetail.STU_GENDER = Student_Info.STU_GENDER;
                int Acdid = Student_Info.STU_ACD_ID;
                string bsuid = Student_Info.Stu_BSU_Id;
                string Country_id = Student_Info.STU_EMIRATES_ID;
                sb.Append("Step 1" + Environment.NewLine);
                #region Company Dropdown List
                var MasterCompanyList = _StudentUpdateDetailsService.GetMasterList("COMPANY_M", bsuid, Convert.ToString(Acdid), "", "");
                List<SelectListItem> SelectCompanyList = new List<SelectListItem>();
                SelectListItem Company = new SelectListItem();
                Company.Value = "0";
                Company.Text = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.Company");
                SelectCompanyList.Add(Company);
                SelectCompanyList.AddRange(MasterCompanyList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                fatherdetails.CompanyDropDownList = SelectCompanyList;
                motherdetails.CompanyDropDownList = SelectCompanyList;
                guardianDetails.CompanyDropDownList = SelectCompanyList;
                parentFeeSponsor.MediaNetworkDropDownList = SelectCompanyList;
                sb.Append("Step 2" + Environment.NewLine);
                #endregion

                //Get fee sponsor contact details.
                #region Add FeeSponser DropDown List
                var MasterFeeSponserList = _StudentUpdateDetailsService.GetMasterList("FEE_SPONSOR_MASTER", bsuid, "", "", "");
                List<SelectListItem> SelecFeeSponserList = new List<SelectListItem>();
                SelectListItem FeeSponser = new SelectListItem();
                FeeSponser.Value = "0";
                FeeSponser.Text = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.FeeSponser");
                SelecFeeSponserList.Add(FeeSponser);
                SelecFeeSponserList.AddRange(MasterFeeSponserList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                parentFeeSponsor.FeeSponsorDropDownList = SelecFeeSponserList;
                sb.Append("Step 3" + Environment.NewLine);
                #endregion

                //Get primary contact details.
                #region Add PreContact Dropdown List
                var MasterPrefContactList = _StudentUpdateDetailsService.GetMasterList("PREF_CONTACT", bsuid, "", "", "");
                List<SelectListItem> SelecPrefContactList = new List<SelectListItem>();
                SelectListItem PrefContact = new SelectListItem();
                PrefContact.Value = "0";
                PrefContact.Text = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.PreferredContact");
                SelecPrefContactList.Add(PrefContact);
                SelecPrefContactList.AddRange(MasterPrefContactList.Select(x => new SelectListItem { Value = x.descr, Text = x.descr }).ToList());
                parentFeeSponsor.PreferredContactDropDownList = SelecPrefContactList;
                parentFeeSponsor.PreferredContactDropDownList.ForEach((item) =>
                {
                    item.Selected = string.Equals(item.Text,Student_Info.STU_PREFCONTACT);
                });

                sb.Append("Step 4" + Environment.NewLine);
                #endregion

                //Get primary details.
                #region Add PrimaryContact dropdown List
                //var MasterPrimaryContactList = _StudentUpdateDetailsService.GetMasterList("PREF_CONTACT", bsuid, "", "", "");
                List<SelectListItem> SelecPrimaryContactList = new List<SelectListItem>();
                SelectListItem PrimaryContact = new SelectListItem();
                PrimaryContact.Value = "0";
                PrimaryContact.Text = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.PrimaryContact");
                SelecPrimaryContactList.Add(PrimaryContact);
                SelecPrimaryContactList.AddRange(MasterPrefContactList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                sb.Append("Step 5" + Environment.NewLine);
                #endregion

                //Get get blood group details.
                #region Add BloodGroup Dropdown List
                var MasterBloodGrouptList = _StudentUpdateDetailsService.GetMasterList("BLOOD_GROUP", bsuid, "", "", "");
                List<SelectListItem> SelecBloodtList = new List<SelectListItem>();
                SelectListItem BloodGroup = new SelectListItem();
                BloodGroup.Value = "0";
                BloodGroup.Text = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.BloodGroup");
                SelecBloodtList.Add(BloodGroup);
                SelecBloodtList.AddRange(MasterBloodGrouptList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                healthDetails.BloodGroupDownList = SelecBloodtList;
                sb.Append("Step 6" + Environment.NewLine);
                #endregion

                #region Add Language Dropdown List
                var MasterlanguagetList = _StudentUpdateDetailsService.GetMasterList("LANGUAGE_M", bsuid, "", "", "");

                //Get first language details.
                #region FristLanguageDropDownList
                List<SelectListItem> SelecLanguageList = new List<SelectListItem>();
                SelectListItem LanguageDetails = new SelectListItem();
                LanguageDetails.Value = "0";
                LanguageDetails.Text = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.FirstLanguage");
                SelecLanguageList.Add(LanguageDetails);
                SelecLanguageList.AddRange(MasterlanguagetList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                languageDetails.FristLanguageDropDownList = SelecLanguageList;
                sb.Append("Step 7" + Environment.NewLine);
                #endregion

                //Get other language details.
                #region Language Details
                List<SelectListItem> SelecOtherLanguageList = new List<SelectListItem>();
                SelectListItem OtherLanguageDetails = new SelectListItem();
                OtherLanguageDetails.Value = "0";
                OtherLanguageDetails.Text = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.OtherLanguage");
                SelecOtherLanguageList.Add(OtherLanguageDetails);
                SelecOtherLanguageList.AddRange(MasterlanguagetList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                languageDetails.OtherLanguageDropDownList = SelecOtherLanguageList;
                var otheLang = Student_Info.STU_OTHLANG.Split(',').ToList();
                languageDetails.OtherLanguageDropDownList.ForEach((item) =>
                {
                //    var selectedOtheLang = item.Text;
                //    selectedOtheLang = selectedOtheLang.Replace(" Language", "");
                    item.Selected = otheLang.Contains(item.Text.Trim());
                });
                sb.Append("Step 8" + Environment.NewLine);
               
                Studentupdatedetail.Languagedetails = languageDetails;
                Studentupdatedetail.Languagedetails.FirstLanguage = Student_Info.STU_FIRSTLANG;
                Studentupdatedetail.Languagedetails.OtherLanguage = "0";

                if (!string.IsNullOrEmpty(Student_Info.STU_FIRSTLANG))
                {
                    Studentupdatedetail.Languagedetails.FirstLanguage = Studentupdatedetail.Languagedetails.FristLanguageDropDownList.FirstOrDefault(x => x.Text == Student_Info.STU_FIRSTLANG).Value;
                }
                else
                {
                    Studentupdatedetail.Languagedetails.FirstLanguage = "0";
                }
                #endregion

                #endregion

                //Get get student details.
                #region GetStudentDetail Or Right Corner
                Studentupdatedetail.Geetstudentdetails = new GetStudentDetail();
                Studentupdatedetail.Geetstudentdetails.Name = Student_Info.STU_FullName;
                Studentupdatedetail.Geetstudentdetails.StudentImagePath = Student_Info.STU_PHOTOPATH;
                Studentupdatedetail.Geetstudentdetails.StudentNumber = Student_Info.StuId;
                Studentupdatedetail.Geetstudentdetails.ID = Student_Info.Stu_BSU_Id;
                Studentupdatedetail.Geetstudentdetails.Grade = Student_Info.STU_GRADE;
                Studentupdatedetail.Geetstudentdetails.NameAsNationalId = Student_Info.STU_PASPRTNAME;
                if (Student_Info.STU_GENDER == "F")
                {
                    Studentupdatedetail.Geetstudentdetails.gender = ResourceManager.GetString("Quiz.Report.Female");
                }
                else
                {
                    Studentupdatedetail.Geetstudentdetails.gender = ResourceManager.GetString("Quiz.Report.Male");
                }
                sb.Append("Step 9" + Environment.NewLine);
                Studentupdatedetail.Geetstudentdetails.Religion = Student_Info.STU_RELIGION;
                Studentupdatedetail.Geetstudentdetails.DOB = Student_Info.STU_DOB.ToString("dd MMMM yyyy");
                Studentupdatedetail.Geetstudentdetails.Joiningdate = Student_Info.STU_DOJ.ToString("dd MMMM yyyy");
                Studentupdatedetail.Geetstudentdetails.PalceOfBirth = !string.IsNullOrEmpty(Student_Info.STU_PLACE_OF_BIRTH) ? Student_Info.STU_PLACE_OF_BIRTH : "N/A";
                Studentupdatedetail.Geetstudentdetails.CountryOfBirth = !string.IsNullOrEmpty(Student_Info.STU_COUNTRY_OF_BIRTH) ? Student_Info.STU_COUNTRY_OF_BIRTH : "N/A";
                Studentupdatedetail.Geetstudentdetails.Nationality = Student_Info.STU_NATIONALITY;
                Studentupdatedetail.Geetstudentdetails.Nationality2 = "N/A";
                Studentupdatedetail.Geetstudentdetails.stU_EmiratesID = Student_Info.STU_EMIRATES_ID;
                Studentupdatedetail.Geetstudentdetails.stU_EM_ID_EXP_DATE = Student_Info.stU_EM_ID_EXP_DATE;
                Studentupdatedetail.Geetstudentdetails.premisesID = Student_Info.stu_PremisesID;
                Studentupdatedetail.Geetstudentdetails.olU_ID = Student_Info.olU_ID;
                Studentupdatedetail.Geetstudentdetails.stu_phone = Student_Info.STU_EMG_CONTACT;
                Studentupdatedetail.Geetstudentdetails.stu_email = Student_Info.STU_EMAIL;
                sb.Append("Step 10" + Environment.NewLine);
                #endregion

                //Get parent details of student.
                #region Parent Details
                var parentUpdateDetails = _StudentUpdateDetailsService.GetParentUpdateDetails(selectedstudent.StudentNumber);
                string Countryid = parentUpdateDetails.fatheR_COUNTRY_ID;
                string Cityid = parentUpdateDetails.fatheR_CITY;
                var MasterCountryList = _StudentUpdateDetailsService.GetMasterList("COUNTRY_M", bsuid, Convert.ToString(Acdid), "", "");
                List<SelectListItem> SelecCountrytList = new List<SelectListItem>();
                SelectListItem country = new SelectListItem();
                SelecCountrytList.AddRange(MasterCountryList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                fatherdetails.CountryOfResidencyDropDownList = SelecCountrytList;
                sb.Append("Step 11" + Environment.NewLine);
                var MasterCityList = _StudentUpdateDetailsService.GetMasterList("CITY_M", bsuid, Convert.ToString(Acdid), "", parentUpdateDetails.fatheR_COUNTRY_ID);
                List<SelectListItem> SelecCitytList = new List<SelectListItem>();
                SelecCitytList.AddRange(MasterCityList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                fatherdetails.CityEmirateDropDownList = SelecCitytList;
                fatherdetails.ResidentialCityDropDownList = SelecCitytList;
                sb.Append("Step 12" + Environment.NewLine);
                var MasterAreaList = _StudentUpdateDetailsService.GetMasterList("AREA_M", bsuid, Convert.ToString(Acdid), "", parentUpdateDetails.fatheR_STATE_ID);
                List<SelectListItem> SelecAreatList = new List<SelectListItem>();
                SelectListItem Area = new SelectListItem();
                Area.Value = "0";
                Area.Text = "Area";
                SelecAreatList.Add(Area);
                SelecAreatList.AddRange(MasterAreaList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                fatherdetails.AreaDropDownList = SelecAreatList;
                sb.Append("Step 13" + Environment.NewLine);

                //Get parent fee sponsor details of student.
                #region ParentfeeSponsor Details
                Studentupdatedetail.ParentfeeSponsor = parentFeeSponsor;
                var ParentfeeSponsorValue = Studentupdatedetail.ParentfeeSponsor.FeeSponsorDropDownList.Where(x => x.Text == Student_Info.STU_FEE_SPONSOR).FirstOrDefault();
                Studentupdatedetail.ParentfeeSponsor.FeeSponsor = ParentfeeSponsorValue != null ? ParentfeeSponsorValue.Value : "0";
                TempData["FeesponceId"] = Studentupdatedetail.ParentfeeSponsor.FeeSponsor;
                Studentupdatedetail.PrefferedContact = Student_Info.STU_PREFCONTACT;
                if (Student_Info.STU_PRIMARYCONTACT == "F")
                {
                    Studentupdatedetail.ParentfeeSponsor.PrimaryContact = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.Father");
                }
                else if (Student_Info.STU_PRIMARYCONTACT == "M")
                {
                    Studentupdatedetail.ParentfeeSponsor.PrimaryContact = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.Mother");
                }
                else if (Student_Info.STU_PRIMARYCONTACT == "G")
                {
                    Studentupdatedetail.ParentfeeSponsor.PrimaryContact = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.Guardian");
                }
                else
                {
                    Studentupdatedetail.ParentfeeSponsor.PrimaryContact = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.Other");
                }
                Studentupdatedetail.ParentfeeSponsor.PreferredContact = Student_Info.STU_PREFCONTACT;
                #endregion

                //Get father details of student.
                Studentupdatedetail.FatherDetails = fatherdetails;
                Studentupdatedetail.FatherDetails.FatherName = parentUpdateDetails.fatheR_NAME;
                Studentupdatedetail.FatherDetails.FirstName = parentUpdateDetails.fatheR_FIRSTNAME;
                Studentupdatedetail.FatherDetails.MiddalName = parentUpdateDetails.fatheR_MIDNAME;
                Studentupdatedetail.FatherDetails.LastName = parentUpdateDetails.fatheR_LASTNAME;
                Studentupdatedetail.FatherDetails.CountryOfResidency = parentUpdateDetails.fatheR_COUNTRY_ID;
                Studentupdatedetail.FatherDetails.ResidentialCity = parentUpdateDetails.fatheR_STATE_ID;
                Studentupdatedetail.FatherDetails.Area = parentUpdateDetails.fatheR_AREA_ID;
                Studentupdatedetail.FatherDetails.Street = parentUpdateDetails.fatheR_STREET;
                Studentupdatedetail.FatherDetails.Building = parentUpdateDetails.fatheR_BLDG;
                Studentupdatedetail.FatherDetails.ApartNo = parentUpdateDetails.fatheR_APARTMENT;
                Studentupdatedetail.FatherDetails.CityEmirate = parentUpdateDetails.fatheR_CITY;
                Studentupdatedetail.FatherDetails.PoBox = parentUpdateDetails.fatheR_POBOX;
                Studentupdatedetail.FatherDetails.MobileNo = parentUpdateDetails.fatheR_MOBILE;
                Studentupdatedetail.FatherDetails.EmailAddress = parentUpdateDetails.fatheR_EMAIL;
                Studentupdatedetail.FatherDetails.Occupation = parentUpdateDetails.fatheR_OCCUPATION;
                bool ChkComList = SelectCompanyList.Any(x => x.Text == parentUpdateDetails.fatheR_COMPANY);
                sb.Append("Step 14" + Environment.NewLine);
                if (ChkComList == false)
                {
                    Studentupdatedetail.FatherDetails.Company = "0";
                }
                else
                {
                    Studentupdatedetail.FatherDetails.Company = !string.IsNullOrEmpty(parentUpdateDetails.fatheR_COMPANY) ? SelectCompanyList.FirstOrDefault(x => x.Text == parentUpdateDetails.fatheR_COMPANY).Value : "0";
                }
                if (Studentupdatedetail.FatherDetails.Company == "0")
                {
                    Studentupdatedetail.FatherDetails.Company = "0";
                    Studentupdatedetail.FatherDetails.OtherComapnyText = parentUpdateDetails.fatheR_COMPANY;
                }
                sb.Append("Step 15" + Environment.NewLine);
                #endregion

                //Get mother details of student.
                #region MotherDetails
                string MCountryid = parentUpdateDetails.motheR_COUNTRY_ID;
                string MCityid = parentUpdateDetails.motheR_STATE_ID;
                var MasterMCountryList = _StudentUpdateDetailsService.GetMasterList("COUNTRY_M", bsuid, Convert.ToString(Acdid), "", "");
                List<SelectListItem> SelecMCountrytList = new List<SelectListItem>();
                SelectListItem mcountry = new SelectListItem();
                SelecMCountrytList.AddRange(MasterMCountryList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                motherdetails.CountryOfResidencyDropDownList = SelecMCountrytList;
                var MasterMCityList = _StudentUpdateDetailsService.GetMasterList("CITY_M", bsuid, Convert.ToString(Acdid), "", MCountryid);
                List<SelectListItem> SelecMCitytList = new List<SelectListItem>();
                SelectListItem MCity = new SelectListItem();
                MCity.Value = "0";
                MCity.Text = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.ResidentialAddressCity");
                SelecMCitytList.Add(MCity);
                SelecMCitytList.AddRange(MasterMCityList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                motherdetails.CityEmirateDropDownList = SelecMCitytList;
                var MasterMAreaList = _StudentUpdateDetailsService.GetMasterList("AREA_M", bsuid, Convert.ToString(Acdid), "", MCityid);
                List<SelectListItem> SelectMAreaList = new List<SelectListItem>();
                SelectListItem MArea = new SelectListItem();
                MArea.Value = "0";
                MArea.Text = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.Area");
                SelectMAreaList.Add(MArea);
                SelectMAreaList.AddRange(MasterMAreaList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                motherdetails.AreaDropDownList = SelectMAreaList;
                motherdetails.ResidentialCityDropDownList = SelecMCitytList;
                sb.Append("Step 16" + Environment.NewLine);
                Studentupdatedetail.Motherdetails = motherdetails;
                Studentupdatedetail.Motherdetails.MotherName = parentUpdateDetails.motheR_NAME;
                Studentupdatedetail.Motherdetails.FirstName = parentUpdateDetails.motheR_FIRSTNAME;
                Studentupdatedetail.Motherdetails.MiddalName = parentUpdateDetails.motheR_MIDNAME;
                Studentupdatedetail.Motherdetails.LastName = parentUpdateDetails.motheR_LASTNAME;
                Studentupdatedetail.Motherdetails.CountryOfResidency = parentUpdateDetails.motheR_COUNTRY_ID;
                Studentupdatedetail.Motherdetails.ResidentialCity = parentUpdateDetails.motheR_STATE_ID;
                Studentupdatedetail.Motherdetails.Area = parentUpdateDetails.motheR_AREA_ID;
                Studentupdatedetail.Motherdetails.Street = parentUpdateDetails.motheR_STREET;
                Studentupdatedetail.Motherdetails.Building = parentUpdateDetails.motheR_BLDG;
                Studentupdatedetail.Motherdetails.ApartNo = parentUpdateDetails.motheR_APARTMENT;
                Studentupdatedetail.Motherdetails.CityEmirate = parentUpdateDetails.motheR_CITY;
                Studentupdatedetail.Motherdetails.PoBox = parentUpdateDetails.motheR_POBOX;
                Studentupdatedetail.Motherdetails.MobileNo = parentUpdateDetails.motheR_MOBILE;
                Studentupdatedetail.Motherdetails.EmailAddress = parentUpdateDetails.motheR_EMAIL;
                Studentupdatedetail.Motherdetails.Occupation = parentUpdateDetails.motheR_OCCUPATION;
                bool ChkMComList = SelectCompanyList.Any(x => x.Text == parentUpdateDetails.motheR_COMPANY);
                if (ChkMComList == false)
                {
                    Studentupdatedetail.Motherdetails.Company = "0";
                }
                else
                {
                    Studentupdatedetail.Motherdetails.Company = !string.IsNullOrEmpty(parentUpdateDetails.motheR_COMPANY) ? SelectCompanyList.FirstOrDefault(x => x.Text == parentUpdateDetails.motheR_COMPANY).Value : "0";
                }
                if (Studentupdatedetail.Motherdetails.Company == "0")
                {
                    Studentupdatedetail.Motherdetails.Company = "0";
                    Studentupdatedetail.Motherdetails.MOtherComapnyText = parentUpdateDetails.motheR_COMPANY;
                }
                sb.Append("Step 17" + Environment.NewLine);

                #endregion

                //Get guardian details of student.
                #region GuardianDetails
                string GCountryid = parentUpdateDetails.guardiaN_COUNTRY_ID;
                string GCityid = parentUpdateDetails.guardiaN_STATE_ID;
                var MasterGCountryList = _StudentUpdateDetailsService.GetMasterList("COUNTRY_M", bsuid, Convert.ToString(Acdid), "", "");
                List<SelectListItem> SelecGCountrytList = new List<SelectListItem>();
                SelectListItem GCountry = new SelectListItem();
                SelecGCountrytList.AddRange(MasterGCountryList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                guardianDetails.CountryOfResidencyDropDownList = SelecGCountrytList;
                var MasterGAreaList = _StudentUpdateDetailsService.GetMasterList("AREA_M", bsuid, Convert.ToString(Acdid), "", GCityid);
                List<SelectListItem> SelecGAreaList = new List<SelectListItem>();
                SelectListItem GArea = new SelectListItem();
                GArea.Value = "0";
                GArea.Text = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.Area");
                SelecGAreaList.Add(GArea);
                SelecGAreaList.AddRange(MasterGAreaList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                guardianDetails.AreaDropDownList = SelecGAreaList;
                var MasterGCityList = _StudentUpdateDetailsService.GetMasterList("CITY_M", bsuid, Convert.ToString(Acdid), "", GCountryid);
                List<SelectListItem> SelecGCitytList = new List<SelectListItem>();
                SelectListItem GCity = new SelectListItem();
                GCity.Value = "0";
                GCity.Text = ResourceManager.GetString("StudentUpdateDetails.StudentUpdateDetail.ResidentialAddressCity");
                SelecGCitytList.Add(GCity);
                SelecGCitytList.AddRange(MasterGCityList.Select(x => new SelectListItem { Value = x.id.ToString(), Text = x.descr }).ToList());
                guardianDetails.CityEmirateDropDownList = SelecGCitytList;
                guardianDetails.ResidentialCityDropDownList = SelecGCitytList;
                Studentupdatedetail.GuardianDetails = guardianDetails;
                Studentupdatedetail.GuardianDetails.MotherName = parentUpdateDetails.guardiaN_NAME;
                Studentupdatedetail.GuardianDetails.FirstName = parentUpdateDetails.guardiaN_FIRSTNAME;
                Studentupdatedetail.GuardianDetails.MiddalName = parentUpdateDetails.guardiaN_MIDNAME;
                Studentupdatedetail.GuardianDetails.LastName = parentUpdateDetails.guardiaN_LASTNAME;
                Studentupdatedetail.GuardianDetails.CountryOfResidency = parentUpdateDetails.guardiaN_COUNTRY_ID;
                Studentupdatedetail.GuardianDetails.ResidentialCity = parentUpdateDetails.guardiaN_STATE_ID;
                Studentupdatedetail.GuardianDetails.Area = parentUpdateDetails.guardiaN_AREA_ID;
                Studentupdatedetail.GuardianDetails.Street = parentUpdateDetails.guardiaN_STREET;
                Studentupdatedetail.GuardianDetails.Building = parentUpdateDetails.guardiaN_BLDG;
                Studentupdatedetail.GuardianDetails.ApartNo = parentUpdateDetails.guardiaN_APARTMENT;
                Studentupdatedetail.GuardianDetails.CityEmirate = parentUpdateDetails.guardiaN_CITY;
                Studentupdatedetail.GuardianDetails.PoBox = parentUpdateDetails.guardiaN_POBOX;
                Studentupdatedetail.GuardianDetails.MobileNo = parentUpdateDetails.guardiaN_MOBILE;
                Studentupdatedetail.GuardianDetails.EmailAddress = parentUpdateDetails.guardiaN_EMAIL;
                Studentupdatedetail.GuardianDetails.Occupation = parentUpdateDetails.guardiaN_OCCUPATION;
                bool ChkGComList = SelectCompanyList.Any(x => x.Text == parentUpdateDetails.guardiaN_COMPANY);
                if (ChkGComList == false)
                {
                    Studentupdatedetail.GuardianDetails.Company = "0";
                }
                else
                {
                    Studentupdatedetail.GuardianDetails.Company = !string.IsNullOrEmpty(parentUpdateDetails.guardiaN_COMPANY) ? SelectCompanyList.FirstOrDefault(x => x.Text == parentUpdateDetails.guardiaN_COMPANY).Value : "0";
                }
                if (Studentupdatedetail.GuardianDetails.Company == "0")
                {
                    Studentupdatedetail.GuardianDetails.Company = "0";
                    Studentupdatedetail.GuardianDetails.GOtherComapnyText = parentUpdateDetails.guardiaN_COMPANY;
                }
                sb.Append("Step 18" + Environment.NewLine);
                #endregion

                //Get passport and visa details of student.
                #region Student PassportVisaDetails
                Studentupdatedetail.PassportVisaDetails = new PassportVISADetails();
                Studentupdatedetail.PassportVisaDetails.FullNameInPassport = Student_Info.STU_PASPRTNAME;
                Studentupdatedetail.PassportVisaDetails.PassportNumber = Student_Info.STU_PASPRTNO;
                Studentupdatedetail.PassportVisaDetails.PassportIssuePlace = Student_Info.STU_PASPRTISSPLACE;
                Studentupdatedetail.PassportVisaDetails.PassportIssueDate = Student_Info.STU_PASPRT_ISS_DATE;
                Studentupdatedetail.PassportVisaDetails.PassportExpiryDate = Student_Info.STU_PASPRT_EXP_DATE;
                Studentupdatedetail.PassportVisaDetails.VisaNumber = Student_Info.STU_VISA_NO;
                Studentupdatedetail.PassportVisaDetails.VisaIssueDate = Student_Info.STU_VISA_ISSUE_DATE;
                Studentupdatedetail.PassportVisaDetails.VisaExpiryDate = Student_Info.STU_VISA_EXPIRY_DATE;
                Studentupdatedetail.PassportVisaDetails.VisatIssuePlace = Student_Info.STU_VISA_ISSUE_PACE;
                Studentupdatedetail.PassportVisaDetails.IssuAuthority = Student_Info.STU_VISA_ISSUING_AUTHORIT;
                sb.Append("Step 19" + Environment.NewLine);
                #endregion

                //Get contact details of student.
                #region Student ContactDetails
                ContactInfoDetails Contactinfo = new ContactInfoDetails();
                Contactinfo.EmrgencyContact = Student_Info.STU_EMG_CONTACT;
                Contactinfo.Studentemail = Student_Info.STU_EMAIL;
                Contactinfo.MobileNumber = Student_Info.STU_EMG_CONTACT;
                Studentupdatedetail.ContactinfoDetails = Contactinfo;
                #endregion

                //Get language details of student.
               

                //Get health details of student.
                #region Student Health Details
                var healthrestricted = _StudentUpdateDetailsService.getHealthrestricted(selectedstudent.StudentNumber);
                #region noninfecious strock,hypertension fill
                GetStudentDiseasesDetail getStudentDiseasesDetail = _StudentUpdateDetailsService.getStudentDiseasesDetail(selectedstudent.StudentNumber);
                familyHistory.Diabetes = getStudentDiseasesDetail.diS_DIABETES;
                familyHistory.Hypertension = getStudentDiseasesDetail.diS_HYPERTENSION;
                familyHistory.stroke = getStudentDiseasesDetail.diS_STROKE;
                familyHistory.Tuberculosis = getStudentDiseasesDetail.diS_TUBERCULOSIS;
                familyHistory.other = getStudentDiseasesDetail.diS_OTHER;
                familyHistory.otherdetail = getStudentDiseasesDetail.diS_OTHER_DETAILS;
                healthDetails.otherinfecioustxt = getStudentDiseasesDetail.diS_INF_DETAILS;
                Studentupdatedetail.Familyhistory = familyHistory;
                #endregion

                Studentupdatedetail.Healthdetails = healthDetails;
                Studentupdatedetail.Healthdetails.InsuranceNumber = healthrestricted._healthCardNo;
                sb.Append("Step 20" + Environment.NewLine);
                if (!string.IsNullOrEmpty(healthrestricted._bloodGroup) && healthrestricted._bloodGroup != "--")
                {
                    Studentupdatedetail.Healthdetails.BloodGroup = Studentupdatedetail.Healthdetails.BloodGroupDownList.FirstOrDefault(x => x.Text == healthrestricted._bloodGroup).Value;
                }
                Studentupdatedetail.Healthdetails.healthissueTextArea = healthrestricted.stU_HEALTH;
                Studentupdatedetail.Healthdetails.Myknowledge = (!string.IsNullOrEmpty(healthrestricted.stU_ENRICH) && healthrestricted.stU_ENRICH == "Yes") ? true : false;
                Studentupdatedetail.Healthdetails.Allergies = healthrestricted._STU_bALLERGIES == "Yes" ? true : false;
                Studentupdatedetail.Healthdetails.Allergiestxt = healthrestricted._txtSTU_bAllergies;
                Studentupdatedetail.Healthdetails.SpecialMedicationtxt = healthrestricted._txtPMedication;
                Studentupdatedetail.Healthdetails.SpecialMedication = healthrestricted._STU_bRCVSPMEDICATION == "Yes" ? true : false;
                Studentupdatedetail.Healthdetails.EducationRestrictions = healthrestricted._STU_bPRESTRICTIONS == "Yes" ? true : false;
                Studentupdatedetail.Healthdetails.EducationRestrictionstxt = healthrestricted.stU_PHYSICAL;
                Studentupdatedetail.Healthdetails.VisualDisabilitytxt = healthrestricted.stU_Visual_Disability;
                Studentupdatedetail.Healthdetails.VisualDisability = (!string.IsNullOrEmpty(healthrestricted.stU_bVisual_disability) && healthrestricted.stU_bVisual_disability == "Yes") ? true : false;
                Studentupdatedetail.Healthdetails.SpecialEducationtxt = healthrestricted._txtspclEduNeeds;
                Studentupdatedetail.Healthdetails.SpecialEducation = healthrestricted.stU_bSEN == "Yes" ? true : false;
                Studentupdatedetail.Healthdetails.healthissueTextArea1 = healthrestricted.stU_HEALTH;
                Studentupdatedetail.Healthdetails.HealthIssue = healthrestricted.stU_bHRESTRICTIONS == "Yes" ? true : false;
                sb.Append("Step 21" + Environment.NewLine);
                #endregion

                //Get history of illness details of student.
                #region Student HistoryOfIllness
                var infectiousDiseases = _StudentUpdateDetailsService.getStudentDiseases(selectedstudent.StudentNumber, true);
                var nonInfectiousDiseases = _StudentUpdateDetailsService.getStudentDiseases(selectedstudent.StudentNumber, false);
                sb.Append("Step 22" + Environment.NewLine);
                infectiousDiseases.ForEach(x =>
                {
                    HistoryOfIllnessInfectious historyOfIllness = new HistoryOfIllnessInfectious();
                    EntityMapper<GetStudentDiseases, HistoryOfIllnessInfectious>.Map(x, historyOfIllness);
                    historyOfIllnessInfectious.Add(historyOfIllness);
                });
                sb.Append("Step 23" + Environment.NewLine);
                nonInfectiousDiseases.ForEach(x =>
                {
                    HistoryOfIllnessNonInfectious historyOfIllness = new HistoryOfIllnessNonInfectious();
                    EntityMapper<GetStudentDiseases, HistoryOfIllnessNonInfectious>.Map(x, historyOfIllness);
                    historyOfIllnessNonInfectious.Add(historyOfIllness);
                });
                sb.Append("Step 24" + Environment.NewLine);
                Studentupdatedetail.HistoryOfIllnessInfectious = historyOfIllnessInfectious;
                Studentupdatedetail.HistoryOfIllnessNonInfectious = historyOfIllnessNonInfectious;
                Studentupdatedetail.Infectious = JsonConvert.SerializeObject(historyOfIllnessInfectious);
                Studentupdatedetail.Noninfectious = JsonConvert.SerializeObject(historyOfIllnessNonInfectious);
                TempData["Infection"] = historyOfIllnessInfectious;
                TempData["NonInfection"] = historyOfIllnessNonInfectious;
                sb.Append("Step 25" + Environment.NewLine);
                #endregion

                //Get general consent details of student.
                #region Generalconsent
                Studentupdatedetail.Generalconsent = new GeneralConsent();
                Studentupdatedetail.Generalconsent.InternalMaterials = Student_Info.STU_IS_INTERNAL_PUB;
                Studentupdatedetail.Generalconsent.ExternalMaterial = Student_Info.STU_IS_MARKETING_PUB;
                Studentupdatedetail.Generalconsent.InformationDirectory = Student_Info.STU_IS_FAMILY_PHONE;
                Studentupdatedetail.Generalconsent.Permission = Student_Info.STU_IS_FIELD_TRIPS;
                sb.Append("Step 26" + Environment.NewLine);
                #endregion
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            finally
            {
                _loggerClient.LogWarning(sb.ToString());
            }
            var viewname = "UpdateStudentDetailsView";
            return View(viewname, Studentupdatedetail);
        }

        /// <summary>
        /// Update student details
        /// </summary>
        /// <param name="model">Update student details model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateInformation(StudentUpdateDetailGeneral model)
        {
            StringBuilder sb = new StringBuilder();
            bool result = false;            

            #region StudentInfoPost
            try
            {
                //Save general details of student.
                if (model.IsChangedGeneral)
                {
                    StudentInfoPost studentInfoPost = new StudentInfoPost();
                    string studentPostData = String.Empty;
                    studentInfoPost.stU_NO = model.StuId;
                    studentInfoPost.stU_BSU_ID = model.Stu_BSU_Id;
                    studentInfoPost.strGivenName = model.STU_FullName;
                    studentInfoPost.gender = model.STU_GENDER;
                    studentInfoPost.strReligion = null;
                    studentInfoPost.stU_DOB = Convert.ToDateTime(model.Geetstudentdetails.DOB);
                    studentInfoPost.stU_COB = model.Geetstudentdetails.CountryOfBirth;
                    studentInfoPost.stU_NATIONALITY = model.Geetstudentdetails.Nationality;
                    studentInfoPost.stU_NATIONALITY1 = model.Geetstudentdetails.Nationality2;
                    studentInfoPost.stU_PASPRTNAME = model.Geetstudentdetails.NameAsNationalId;
                    studentInfoPost.stU_PASPRTNO = model.PassportVisaDetails.PassportNumber;
                    studentInfoPost.stU_PASPRTISSPLACE = model.PassportVisaDetails.PassportIssuePlace;
                    studentInfoPost.stU_PASPRTISSDATE = !string.IsNullOrEmpty(model.PassportVisaDetails.PassportIssueDate) ? Convert.ToDateTime(model.PassportVisaDetails.PassportIssueDate) : Convert.ToDateTime("01/01/1755");
                    studentInfoPost.stU_PASPRTEXPDATE = !string.IsNullOrEmpty(model.PassportVisaDetails.PassportExpiryDate) ? Convert.ToDateTime(model.PassportVisaDetails.PassportExpiryDate) : Convert.ToDateTime("01/01/1755");
                    studentInfoPost.stU_VISANO = model.PassportVisaDetails.VisaNumber;
                    studentInfoPost.stU_VISAISSPLACE = model.PassportVisaDetails.VisatIssuePlace;
                    studentInfoPost.stU_VISAISSDATE = !string.IsNullOrEmpty(model.PassportVisaDetails.VisaIssueDate) ? Convert.ToDateTime(model.PassportVisaDetails.VisaIssueDate) : Convert.ToDateTime("01/01/1755");
                    studentInfoPost.stU_VISAEXPDATE = !string.IsNullOrEmpty(model.PassportVisaDetails.VisaExpiryDate) ? Convert.ToDateTime(model.PassportVisaDetails.VisaExpiryDate) : Convert.ToDateTime("01/01/1755");
                    studentInfoPost.stU_VISAISSAUTH = model.PassportVisaDetails.IssuAuthority;
                    studentInfoPost.stU_EmiratesID = model.Geetstudentdetails.stU_EmiratesID;
                    studentInfoPost.stU_EM_ID_EXP_DATE = !string.IsNullOrEmpty(model.PassportVisaDetails.VisaExpiryDate) ? Convert.ToDateTime(model.PassportVisaDetails.VisaExpiryDate) : Convert.ToDateTime("01/01/1755");
                    studentInfoPost.premisesID = model.Geetstudentdetails.premisesID;
                    studentInfoPost.stU_FIRSTLANG = model.Languagedetails.FirstLanguage;
                    studentInfoPost.stU_OTHLANG = model.MultipleOtherLanguage;
                    studentInfoPost.emerGenyNumber = model.ContactinfoDetails.EmrgencyContact;
                    studentInfoPost.olU_ID = int.Parse(!string.IsNullOrEmpty(model.Geetstudentdetails.olU_ID) ? model.Geetstudentdetails.olU_ID : "0");
                    sb.Append("Step 27" + Environment.NewLine);
                    studentInfoPost.stu_email = model.ContactinfoDetails.Studentemail;
                    studentPostData = JsonConvert.SerializeObject(studentInfoPost);
                    result = _StudentUpdateDetailsService.UpdateStudentDetails(studentPostData, "Student");
                }
                #endregion

                //Save parent details of student.
                #region ParentInfoPost
                if (model.IsChangedPersonalDetails)
                {
                    var MasterCompanyList = _StudentUpdateDetailsService.GetMasterList("COMPANY_M", model.Stu_BSU_Id, Convert.ToString(model.STU_ACD_ID), "", "");
                    string parentPostData = string.Empty;
                    ParentInfoPost parentInfoPost = new ParentInfoPost();
                    parentInfoPost.stU_NO = model.Geetstudentdetails.StudentNumber;
                    parentInfoPost.stU_PREFCONTACT = model.Geetstudentdetails.stu_prefcontact;
                    parentInfoPost.stS_FFIRSTNAME = model.FatherDetails.FirstName;
                    parentInfoPost.stS_FMIDNAME = model.FatherDetails.MiddalName;
                    parentInfoPost.stS_FLASTNAME = model.FatherDetails.LastName;
                    parentInfoPost.stS_FCOMSTREET = model.FatherDetails.Street;
                    parentInfoPost.stS_FCOMAREA = model.FatherDetails.Area;  
                    parentInfoPost.stS_FCOMBLDG = model.FatherDetails.Building;
                    parentInfoPost.stS_FCOMAPARTNO = model.FatherDetails.ApartNo;
                    parentInfoPost.stS_FCOMPOBOX = model.FatherDetails.PoBox;
                    parentInfoPost.stS_FMOBILE = model.FatherDetails.MobileNo;
                    parentInfoPost.stS_FEMAIL = model.FatherDetails.EmailAddress;
                    if (string.IsNullOrEmpty(model.FatherDetails.OtherComapnyText))
                    {
                        parentInfoPost.stS_F_COMP_ID = int.Parse(model.FatherDetails.Company);// compnay dropdwonlist 
                        sb.Append("Step 28" + Environment.NewLine);
                        if (Convert.ToInt32(model.FatherDetails.Company) == 0)
                        {
                            parentInfoPost.stS_FCOMPANY = null;
                        }
                        else
                        {
                            parentInfoPost.stS_FCOMPANY = !string.IsNullOrEmpty(model.FatherDetails.Company) ? MasterCompanyList.FirstOrDefault(x => x.id == int.Parse(model.FatherDetails.Company)).descr : null;
                        }
                    }
                    else
                    {
                        parentInfoPost.stS_FCOMPANY = model.FatherDetails.OtherComapnyText;
                        parentInfoPost.stS_F_COMP_ID = 0;
                    }
                    sb.Append("Step 29" + Environment.NewLine);
                    parentInfoPost.stS_FOCC = model.FatherDetails.Occupation;
                    parentInfoPost.stS_FEMIR = model.FatherDetails.CityEmirate;
                    parentInfoPost.stS_FCOMCOUNTRY = model.FatherDetails.CountryOfResidency;
                    parentInfoPost.stS_FCOMAREA_ID = !string.IsNullOrEmpty(model.FatherDetails.Area) ? int.Parse(model.FatherDetails.Area) : 0;
                    parentInfoPost.stS_FCOMSTATE_ID = !string.IsNullOrEmpty(model.FatherDetails.ResidentialCity) ? int.Parse(model.FatherDetails.ResidentialCity) : 0;
                    parentInfoPost.stS_MFIRSTNAME = model.Motherdetails.FirstName;
                    parentInfoPost.stS_MMIDNAME = model.Motherdetails.MiddalName;
                    parentInfoPost.stS_MLASTNAME = model.Motherdetails.LastName;
                    parentInfoPost.stS_MCOMCOUNTRY = model.Motherdetails.CountryOfResidency;
                    parentInfoPost.stS_MCOMAREA_ID = !string.IsNullOrEmpty(model.Motherdetails.Area) ? int.Parse(model.Motherdetails.Area) : 0;
                    parentInfoPost.stS_MCOMSTATE_ID = !string.IsNullOrEmpty(model.Motherdetails.ResidentialCity) ? int.Parse(model.Motherdetails.ResidentialCity) : 0;
                    parentInfoPost.stS_MCOMSTREET = model.Motherdetails.Street;
                    parentInfoPost.stS_MCOMAREA = model.Motherdetails.Area;
                    parentInfoPost.stS_MCOMBLDG = model.Motherdetails.Building;
                    parentInfoPost.stS_MCOMAPARTNO = model.Motherdetails.ApartNo;
                    parentInfoPost.stS_MCOMPOBOX = model.Motherdetails.PoBox;
                    parentInfoPost.stS_MMOBILE = model.Motherdetails.MobileNo;
                    parentInfoPost.stS_MEMAIL = model.Motherdetails.EmailAddress;
                    if (string.IsNullOrEmpty(model.Motherdetails.MOtherComapnyText))
                    {
                        parentInfoPost.stS_M_COMP_ID = int.Parse(model.Motherdetails.Company);
                        sb.Append("Step 30" + Environment.NewLine);
                        if (Convert.ToInt32(model.Motherdetails.Company) == 0)
                        {
                            parentInfoPost.stS_MCOMPANY = null;
                        }
                        else
                        {
                            parentInfoPost.stS_MCOMPANY = !string.IsNullOrEmpty(model.Motherdetails.Company) ? MasterCompanyList.FirstOrDefault(x => x.id == int.Parse(model.Motherdetails.Company)).descr : null;
                        }
                    }
                    else
                    {
                        parentInfoPost.stS_MCOMPANY = model.Motherdetails.MOtherComapnyText;
                        parentInfoPost.stS_M_COMP_ID = 0;
                    }
                    sb.Append("Step 31" + Environment.NewLine);
                    parentInfoPost.stS_MOCC = model.Motherdetails.Occupation;
                    parentInfoPost.stS_MEMIR = model.Motherdetails.CityEmirate;
                    parentInfoPost.stS_GFIRSTNAME = model.GuardianDetails.FirstName;
                    parentInfoPost.stS_GMIDNAME = model.GuardianDetails.MiddalName;
                    parentInfoPost.stS_GLASTNAME = model.GuardianDetails.LastName;
                    parentInfoPost.stS_GCOMSTREET = model.GuardianDetails.Street;
                    parentInfoPost.stS_GCOMAREA = model.GuardianDetails.Area;
                    parentInfoPost.stS_GCOMBLDG = model.GuardianDetails.Building;
                    parentInfoPost.stS_GCOMAPARTNO = model.GuardianDetails.ApartNo;
                    parentInfoPost.stS_GCOMPOBOX = model.GuardianDetails.PoBox;
                    parentInfoPost.stS_GMOBILE = model.GuardianDetails.MobileNo;
                    parentInfoPost.stS_GEMAIL = model.GuardianDetails.EmailAddress;
                    if (string.IsNullOrEmpty(model.GuardianDetails.GOtherComapnyText))
                    {
                        parentInfoPost.stS_G_COMP_ID = int.Parse(model.GuardianDetails.Company);
                        sb.Append("Step 32" + Environment.NewLine);
                        if (Convert.ToInt32(model.GuardianDetails.Company) == 0)
                        {
                            parentInfoPost.stS_GCOMPANY = null;
                        }
                        else
                        {
                            parentInfoPost.stS_GCOMPANY = !string.IsNullOrEmpty(model.GuardianDetails.Company) ? MasterCompanyList.FirstOrDefault(x => x.id == int.Parse(model.GuardianDetails.Company)).descr : null;
                        }
                    }
                    else
                    {
                        parentInfoPost.stS_GCOMPANY = model.GuardianDetails.GOtherComapnyText;
                        parentInfoPost.stS_G_COMP_ID = 0;
                    }
                    sb.Append("Step 33" + Environment.NewLine);
                    parentInfoPost.stS_G_COMP_ID = Convert.ToInt32(model.GuardianDetails.Company);
                    parentInfoPost.stS_GOCC = model.GuardianDetails.Occupation;
                    parentInfoPost.stS_GEMIR = model.GuardianDetails.CityEmirate;
                    parentInfoPost.stS_GCOMCOUNTRY = model.GuardianDetails.CountryOfResidency;
                    parentInfoPost.stS_GCOMAREA_ID = !string.IsNullOrEmpty(model.GuardianDetails.Area) ? int.Parse(model.GuardianDetails.Area) : 0;
                    parentInfoPost.stS_GCOMSTATE_ID = !string.IsNullOrEmpty(model.GuardianDetails.ResidentialCity) ? int.Parse(model.GuardianDetails.ResidentialCity) : 0;
                    parentInfoPost.stS_FEESPONSOR = model.ParentfeeSponsor.FeeSponsor!=null? model.ParentfeeSponsor.FeeSponsor : TempData["FeesponceId"] as string;
                    parentInfoPost.stU_PREFCONTACT = model.PrefferedContact;
                    sb.Append("Step 34" + Environment.NewLine);
                    parentInfoPost.stU_EMAIL = model.Geetstudentdetails.stu_email;
                    parentPostData = JsonConvert.SerializeObject(parentInfoPost);
                    result = _StudentUpdateDetailsService.UpdateStudentDetails(parentPostData, "Parent");
                    sb.Append("Step 35" + Environment.NewLine);
                }
                #endregion

                //Save health details of student.
                #region Health
                if (model.IsChangedHealth)
                {
                    var MasterBloodGrouptList = _StudentUpdateDetailsService.GetMasterList("BLOOD_GROUP", model.Stu_BSU_Id, "", "", "");
                    StudentHealthInfoPost studentHealthInfoPost = new StudentHealthInfoPost();
                    var selectedstudent = SessionHelper.CurrentSession.CurrentSelectedStudent;
                    List<StduentInfection> StduentInfectionList = new List<StduentInfection>();
                    sb.Append("Step 36" + Environment.NewLine);
                    if (model.Chekboxjson != null)
                    {
                        string infecious = model.Chekboxjson.TrimEnd(',');
                        string[] InfectionFilter = infecious.Split(',');
                        foreach (var item in InfectionFilter)
                        {
                            StduentInfection StuInfection = new StduentInfection();
                            string[] Id_Status = item.Split(':');
                            StuInfection.stU_NO = selectedstudent.StudentNumber;
                            StuInfection.diS_ID = Convert.ToInt32(Id_Status[0]);
                            StuInfection.diS_STATUS = Convert.ToInt32(Id_Status[1]) == 0 ? true : false;
                            StduentInfectionList.Add(StuInfection);
                        }
                        studentHealthInfoPost.stU_Infection = StduentInfectionList;
                    }
                    else
                    {

                        var infection = TempData["Infection"];
                        var Noninfection = TempData["NonInfection"];
                        foreach (var item in (List<HistoryOfIllnessInfectious>)TempData["Infection"])
                        {
                            StduentInfection StuInfection = new StduentInfection();
                            StuInfection.stU_NO = selectedstudent.StudentNumber;
                            StuInfection.diS_ID = item.diS_ID;
                            StuInfection.diS_STATUS = item.isYChecked == 1 ? true : false;
                            StduentInfectionList.Add(StuInfection);
                        }
                        foreach (var item in (List<HistoryOfIllnessNonInfectious>)TempData["NonInfection"])
                        {
                            StduentInfection StuInfection = new StduentInfection();
                            StuInfection.stU_NO = selectedstudent.StudentNumber;
                            StuInfection.diS_ID = item.diS_ID;
                            StuInfection.diS_STATUS = item.isYChecked == 1 ? true : false;
                            StduentInfectionList.Add(StuInfection);
                        }
                        studentHealthInfoPost.stU_Infection = StduentInfectionList;

                    }
                    sb.Append("Step 37" + Environment.NewLine);
                    studentHealthInfoPost.stU_Health.stU_NO = model.StuId;
                    studentHealthInfoPost.stU_Health.stU_BSU_ID = model.Stu_BSU_Id;
                    studentHealthInfoPost.stU_Health.healthCardNo = model.Healthdetails.InsuranceNumber;
                    if (model.Healthdetails.BloodGroup == "0")
                    {
                        studentHealthInfoPost.stU_Health.bloodGroup = null;
                    }
                    else
                    {
                        studentHealthInfoPost.stU_Health.bloodGroup = !string.IsNullOrEmpty(model.Healthdetails.BloodGroup) ? MasterBloodGrouptList.FirstOrDefault(x => x.id == int.Parse(model.Healthdetails.BloodGroup)).descr : null;
                    }
                    sb.Append("Step 38" + Environment.NewLine);
                    studentHealthInfoPost.stU_Health.stU_bALLERGIES = model.Healthdetails.Allergies;
                    studentHealthInfoPost.stU_Health.txtSTU_bAllergies = model.Healthdetails.Allergiestxt;
                    studentHealthInfoPost.stU_Health.stU_bRCVSPMEDICATION = model.Healthdetails.SpecialMedication.ToString();
                    studentHealthInfoPost.stU_Health.txtPMedication = model.Healthdetails.SpecialMedicationtxt;
                    studentHealthInfoPost.stU_Health.stU_bPRESTRICTIONS = model.Healthdetails.EducationRestrictions;
                    studentHealthInfoPost.stU_Health.txtSTU_bPRESTRICTIONS = model.Healthdetails.EducationRestrictionstxt;
                    studentHealthInfoPost.stU_Health.txtbHealthAware = model.Healthdetails.healthissueTextArea1;
                    studentHealthInfoPost.stU_Health.stU_bHRESTRICTIONS = model.Healthdetails.HealthIssue;
                    studentHealthInfoPost.stU_Health.bVisualDisab = model.Healthdetails.VisualDisability;
                    studentHealthInfoPost.stU_Health.txtVisual = model.Healthdetails.VisualDisabilitytxt;
                    sb.Append("Step 39" + Environment.NewLine);
                    studentHealthInfoPost.stU_Health.stU_EduNeeds = model.Healthdetails.SpecialEducation;
                    studentHealthInfoPost.stU_Health.txtspclEduNeeds = model.Healthdetails.SpecialEducationtxt;
                    studentHealthInfoPost.stU_Health.olU_ID = 0;
                    studentHealthInfoPost.stU_Disease.stU_NO = model.StuId;
                    studentHealthInfoPost.stU_Disease.diS_DIABETES = model.Familyhistory.Diabetes;
                    studentHealthInfoPost.stU_Disease.diS_HYPERTENSION = model.Familyhistory.Hypertension;
                    studentHealthInfoPost.stU_Disease.diS_STROKE = model.Familyhistory.stroke;
                    studentHealthInfoPost.stU_Disease.diS_TUBERCULOSIS = model.Familyhistory.Tuberculosis;
                    studentHealthInfoPost.stU_Disease.diS_OTHER = model.Familyhistory.other;
                    studentHealthInfoPost.stU_Disease.diS_OTHER_DETAILS = model.Familyhistory.otherdetail;
                    studentHealthInfoPost.stU_Disease.diS_INF_DETAILS = model.Healthdetails.otherinfecioustxt;
                    sb.Append("Step 40" + Environment.NewLine);
                    string studentHealthPostData = JsonConvert.SerializeObject(studentHealthInfoPost);
                    result = _StudentUpdateDetailsService.UpdateStudentDetails(studentHealthPostData, "Health");
                }
                #endregion

                //Save Consent details of student.
                #region Consent
                if (model.IsChangedGeneralConsent)
                {
                    StudentConsentInfoPost studentConsentInfoPost = new StudentConsentInfoPost();
                    studentConsentInfoPost.stU_NO = model.Geetstudentdetails.StudentNumber;
                    studentConsentInfoPost.stU_is_internal_pub = model.Generalconsent.InternalMaterials;
                    studentConsentInfoPost.stU_is_family_phone = model.Generalconsent.InformationDirectory;
                    studentConsentInfoPost.stU_is_marketing_pub = model.Generalconsent.ExternalMaterial;
                    studentConsentInfoPost.stU_is_field_Trips = model.Generalconsent.Permission;
                    string studentConsentPostData = JsonConvert.SerializeObject(studentConsentInfoPost);
                    result = _StudentUpdateDetailsService.UpdateStudentDetails(studentConsentPostData, "Consent");
                    sb.Append("Step 41" + Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            finally
            {
                _loggerClient.LogWarning(sb.ToString());
            }
            #endregion
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get list of cities.
        /// </summary>
        /// <param name="FCountryId"></param>
        /// <returns></returns>
        public JsonResult FillCityFromCountry(string FCountryId)
        {
            var selectedstudent = SessionHelper.CurrentSession.CurrentSelectedStudent;
            var Student_Info = _GetStudentInfoService.GetStudentInfo(selectedstudent.StudentNumber, systemLanguageCode, SessionHelper.CurrentSession.BusinessUnitType);
            int Acdid = Student_Info.STU_ACD_ID;
            string bsuid = Student_Info.Stu_BSU_Id;
            var MasterCityList = _StudentUpdateDetailsService.GetMasterList("CITY_M", bsuid, Convert.ToString(Acdid), " ", FCountryId);
            return Json(MasterCityList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get list of area.
        /// </summary>
        /// <param name="FCityId"></param>
        /// <returns></returns>
        public JsonResult FillAreaFromCity(string FCityId)
        {
            var selectedstudent = SessionHelper.CurrentSession.CurrentSelectedStudent;
            var Student_Info = _GetStudentInfoService.GetStudentInfo(selectedstudent.StudentNumber, systemLanguageCode, SessionHelper.CurrentSession.BusinessUnitType);
            int Acdid = Student_Info.STU_ACD_ID;
            string bsuid = Student_Info.Stu_BSU_Id;
            var MasterAreaList = _StudentUpdateDetailsService.GetMasterList("AREA_M", bsuid, Convert.ToString(Acdid), " ", FCityId);
            return Json(MasterAreaList, JsonRequestBehavior.AllowGet);
        }

    }
}
