﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common.Models;
using Phoenix.Common.Localization;
using System.Web.Script.Serialization;
using Phoenix.VLE.Web.Models.EditModels;
using System.Threading;

namespace Phoenix.VLE.Web.Areas.SchoolGroup.Controllers
{
    [Authorize]
    public class SchoolGroupController : BaseController
    {
        // GET: SchoolGroup/SchoolGroup
        private ISchoolGroupService _schoolGroupService;
        private IStudentListService _studentListService;
        private ISchoolService _schoolservice;
        private ITeacherDashboardService _teacherDashboard;

        public SchoolGroupController(ISchoolGroupService schoolGroupService, IStudentListService studentListService, ISchoolService studentService, ITeacherDashboardService teacherDashboard)
        {
            _schoolGroupService = schoolGroupService;
            _studentListService = studentListService;
            _schoolservice = studentService;
            _teacherDashboard = teacherDashboard;
        }
        public ActionResult Index(int pageIndex = 1)
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var schoolGroups = _schoolGroupService.GetSchoolGroups(pageIndex, 5).Result.ToList();
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var objPage = new Pagination<Phoenix.Models.SchoolGroup>(pageIndex, 12, schoolGroups, 11);
            if (SessionHelper.CurrentSession.IsStudent())
            {

                return View("StudentGroupIndex", objPage);

            }

            return View(objPage);


        }

        //public ActionResult LoadSchoolGroups()
        //{
        //    var schoolGroups = _schoolGroupService.GetSchoolGroups(1, 5);
        //    var dataList = new object();
        //    dataList = new
        //    {
        //        aaData = (from item in schoolGroups
        //                  select new
        //                  {
        //                      Actions = string.Format(HtmlHelperExtensions.ActionButtonsHtmlTemplate,
        //                      HtmlHelperExtensions.LocalizedLinkButton(new HyperlinkAttributes()
        //                      {
        //                          InnerIconCssClass = "fas fa-external-link-alt",
        //                          OnClick = "schoolGroup.addnewSchoolGroup(" + item.SchoolGroupId + ")",
        //                          TitleResourceKey = "SchoolGroup.SchoolGroup.OpenSchoolGroup"
        //                      }) + "" + HtmlHelperExtensions.LocalizedDeleteLinkButton(new HyperlinkAttributes()
        //                      {
        //                          OnClick = "SchoolGroup.SchoolGroup.DeleteSchoolGroup($(this)," + item.SchoolGroupId + ")",
        //                      })),
        //                      GroupName = item.SchoolGroupName,
        //                      GroupDesc = item.SchoolGroupDescription,
        //                      Subject = item.SubjectName
        //                  }).ToArray()
        //    };
        //    return Json(dataList, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult InitAddEditSchoolGroupForm(int? id)
        //{
        //    var model = new SchoolGroupEdit();
        //    if (id.HasValue)
        //    {
        //        int groupId = id.GetValueOrDefault();
        //        var template = _schoolGroupService.GetSchoolGroupByID(id.Value);
        //        ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName", SessionHelper.CurrentSession.SchoolId);
        //        ViewBag.StudentInGroups = _studentListService.GetStudentsInGroup(groupId);
        //        EntityMapper<Phoenix.Models.SchoolGroup, SchoolGroupEdit>.Map(template, model);
        //    }
        //    else
        //    {
        //        model.IsAddMode = true;
        //        ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName", SessionHelper.CurrentSession.SchoolId);
        //        model.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
        //    }

        //    return View(model);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult SaveGroupDetails(SchoolGroupEdit model)
        //{
        //    if (string.IsNullOrWhiteSpace(model.SchoolGroupName))
        //    {
        //        return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
        //    }

        //    var groupId = _schoolGroupService.UpdateSchoolGroupDetails(model);
        //    if (groupId < 0)
        //    {
        //        return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "School Group Name")), JsonRequestBehavior.AllowGet);
        //    }
        //    if (model.IsAddMode)
        //    {
        //        List<GroupMemberMapping> addTeacher = new List<GroupMemberMapping> { new GroupMemberMapping { GroupId = groupId, MemberId = SessionHelper.CurrentSession.Id, IsManager = true } };
        //        _schoolGroupService.AddUpdateMembersToGroup(addTeacher);
        //    }
        //    var result = false;
        //    if (groupId != 0)
        //        result = true;
        //    var operationDetails = new OperationDetails(result);
        //    operationDetails.InsertedRowId = groupId;
        //    return Json(operationDetails, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult GetStudentsNotInGroup(int groupId)
        //{
        //    if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
        //    {
        //        var lstStudents = _schoolservice.GetStudentForTeacher(SessionHelper.CurrentSession.Id, "");
        //        return PartialView("_getStudents", lstStudents);
        //    }
        //    else
        //    {
        //        var lstStudents = _studentListService.GetStudentsNotInGroup((int)SessionHelper.CurrentSession.SchoolId, groupId);
        //        return PartialView("_getStudents", lstStudents);
        //    }
        //}

        //public ActionResult GetStudentsInGroup(int groupId)
        //{
        //    var lstStudents = _studentListService.GetStudentsInGroup(groupId);
        //    var result = new { Result = ConvertViewToString("_GetUnAssignedMembers", lstStudents), StudentList = lstStudents };
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //    //return PartialView("_GetUnAssignedMembers", lstStudents);
        //}

        //public ActionResult GetStudentByGroupId(int groupid = 0, int formGroupId = 0)
        //{
        //    var strGroupid = groupid.ToString();
        //    var StudentList = _studentListService.GetStudentsInSelectedGroups(strGroupid);
        //    List<ListItem> studentList = new List<ListItem>();

        //    var assignedMembers = _schoolGroupService.GetAssignedMembers(formGroupId).ToList();

        //    foreach (var student in StudentList)
        //    {
        //        var exist = assignedMembers.Where(r => r.MemberId == student.Id).FirstOrDefault();
        //        if (exist == null)
        //        {
        //            studentList.Add(new ListItem { ItemId = student.Id.ToString(), ItemName = $"{student.FirstName} {student.LastName}" });
        //        }
        //    }
        //    return PartialView("_GetUnAssignedMembers", studentList);
        //}

        //public ActionResult GetStudentinSelectedList(string[] studentIds)
        //{
        //    List<Student> objStudentList = new List<Student>();

        //    string Id = "";
        //    if (studentIds != null)
        //    {
        //        Id = string.Join(",", studentIds);
        //    }

        //    objStudentList = _studentListService.GetStudentDetailsByIds(Id).ToList();

        //    var result = new { Result = ConvertViewToString("_GetUnAssignedMembers", objStudentList) };
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //private string ConvertViewToString(string viewName, object model)
        //{
        //    ViewData.Model = model;
        //    using (StringWriter writer = new StringWriter())
        //    {
        //        ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
        //        ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
        //        vResult.View.Render(vContext, writer);
        //        return writer.ToString();
        //    }
        //}

        //public JsonResult GetSubjectSchoolGroups(int[] id)
        //{
        //    int? id1 = 1;
        //    var subjectId = id1.HasValue ? id1.Value : 0;

        //    long schoolId = SessionHelper.CurrentSession.SchoolId;
        //    var schoolGroups = _schoolGroupService.GetSubjectSchoolGroups(subjectId, schoolId);

        //    var schoolGroupList = schoolGroups.Select(i => new ListItem()
        //    {
        //        ItemName = i.SchoolGroupDescription == string.Empty ? i.SchoolGroupName.Replace("_", "/") : i.SchoolGroupDescription.Replace("_", "/"),
        //        ItemId = i.SchoolGroupId.ToString()
        //    });
        //    return Json(schoolGroupList, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult GetMembersByType(string memberType)
        //{
        //    if (memberType == "Student" && (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin))
        //    {
        //        var lstStudents = _schoolservice.GetStudentForTeacher(SessionHelper.CurrentSession.Id, "");
        //        //var result = new { Result = ConvertViewToString("_getStudents", lstStudents) };
        //        //return Json(result, JsonRequestBehavior.AllowGet);
        //        return PartialView("_getStudents", lstStudents);

        //    }
        //    return View();
        //}

        //public ActionResult GetMemberPage(int groupId)
        //{
        //    Session["GroupId"] = groupId;
        //    var userTypes = SelectListHelper.GetSelectListData(ListItems.UserTypes);
        //    var userIdToRemove = (int)UserTypes.Parent;
        //    var userTypeToRemove = userTypes.Where(m => m.ItemId.Trim() == userIdToRemove.ToString()).FirstOrDefault();
        //    userTypes.Remove(userTypeToRemove);
        //    userTypes.Add(new ListItem
        //    {
        //        ItemId = "0",
        //        ItemName = "Everyone"
        //    });
        //    userTypes.Add(new ListItem
        //    {
        //        ItemId = (Convert.ToInt32(userTypes.OrderBy(m => m.ItemId).LastOrDefault().ItemId) + 1).ToString(),
        //        ItemName = "Other Groups"
        //    });
        //    ViewBag.MemberTypes = new SelectList(userTypes, "ItemId", "ItemName");
        //    GroupMemberViewModel groupMemberViewModel = new GroupMemberViewModel();
        //    //var LstofSchoolGroups = _teacherDashboard.GetTeacherDashboard().SchoolGroups;
        //    var LstofSchoolGroups = _schoolGroupService.GetSchoolGroupsBySchoolId(SessionHelper.CurrentSession.SchoolId);

        //    groupMemberViewModel.AssignedMemberList = _schoolGroupService.GetAssignedMembers(groupId).ToList();
        //    //  ViewBag.SchoolGroup = new SelectList(LstofSchoolGroups, "SchoolGroupId", "SchoolGroupName");
        //    List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
        //    lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
        //    List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
        //    lstSelectListGroup.Add(new SelectListGroup
        //    {
        //        Name = "Class Group",
        //        Disabled = false
        //    });
        //    lstSelectListGroup.Add(new SelectListGroup
        //    {
        //        Name = "Home Tutor Group",
        //        Disabled = false
        //    });
        //    lstSelectListGroup.Add(new SelectListGroup
        //    {
        //        Name = "Other Groups",
        //        Disabled = false
        //    });

        //    var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
        //    {
        //        Value = x.SchoolGroupId.ToString(),
        //        Text = x.SchoolGroupName,
        //        Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
        //        //Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
        //    }).ToList();
        //    ViewBag.SchoolGroup = groupedOptions;

        //    //ViewBag.SchoolGroup = new SelectList(lstSelectListGroup, "SchoolGroupId", "SchoolGroupName");




        //    groupMemberViewModel.UnAssignedMemberList = new List<ListItem>();
        //    // var result = new { Result = ConvertViewToString("_GetMemberDetails", groupMemberViewModel) };
        //    // return Json(result, JsonRequestBehavior.AllowGet);
        //    return PartialView("_GetMemberDetails", groupMemberViewModel);
        //}

        //public ActionResult GetUnassignedMembers(int? memberTypeId, string groupId)
        //{

        //    string[] group = groupId.Split(',');
        //    int GroupId = Convert.ToInt32(Session["GroupId"]);
        //    var schoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
        //    var unassignedMembers = new List<ListItem>();
        //    unassignedMembers = _schoolGroupService.GetUnassignedMembers(schoolId, SessionHelper.CurrentSession.Id, memberTypeId, GroupId, groupId).ToList();
        //    //var unassignedMembers = _schoolGroupService.GetUnassignedMembers(schoolId, SessionHelper.CurrentSession.Id, memberTypeId, Convert.ToInt32(item)).ToList();
        //    //var AssignedMemberList = _schoolGroupService.GetAssignedMembers(GroupId).ToList();
        //    //var unassignedMembers = list1.GroupBy(i => i.ItemId).Select(i => i.FirstOrDefault()).ToList();
        //    //var result = unassignedMembers.Where(p => !AssignedMemberList.Any(p2 => p2.MemberId.ToString() == p.ItemId.ToString()));
        //    return PartialView("_GetUnAssignedMembers", unassignedMembers.ToList());
        //}

        //public ActionResult GetAssignedMembers(int groupId)
        //{
        //    var assignedMembers = _schoolGroupService.GetAssignedMembers(groupId).ToList();
        //    return PartialView("_GetAssignedMembers", assignedMembers);
        //}

        //[HttpPost]
        //public ActionResult AddUpdateMembersToGroup(string membersToAdd)
        //{
        //    var js = new JavaScriptSerializer();
        //    List<GroupMemberMapping> memberList = js.Deserialize<List<GroupMemberMapping>>(membersToAdd);
        //    var result = _schoolGroupService.AddUpdateMembersToGroup(memberList);
        //    return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        //}

    }


}