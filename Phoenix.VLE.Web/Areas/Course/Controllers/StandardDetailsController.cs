﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Course.Controllers
{
    public class StandardDetailsController : BaseController
    {
        // GET: Course/Standard

        private IStandardDetailsService _standardDetailsService;
        private IStandardService _standardService;
        public StandardDetailsController(IStandardDetailsService standardDetailsService, IStandardService standardService)
        {
            _standardDetailsService = standardDetailsService;
            _standardService = standardService;
        }
        public ActionResult Index(string StandardId = "", string CourseName = "")
        {
            var Standard = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl((StandardId)));
            ViewBag.SID = Standard;
            ViewBag.Course_name = CourseName;
            return View();
        }

        public ActionResult AddEditStandardDetails(Int64? StandardDetailsId, Int64? StandardId)
        {
            var model = new StandardDetailsEdit();
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;
            IEnumerable<MainSyllabusIdList> lstStandard = new List<MainSyllabusIdList>();
            lstStandard = _standardService.MainSyllabusIdList();
            ViewBag.StandardList = lstStandard;
            ViewBag.ParentTopic = _standardService.GetParentList(StandardId);
            //ViewBag.ParentTopic = ParentTopic;
            if (StandardDetailsId.HasValue)
            {
                var standardDetails = new StandardDetails();
                standardDetails = _standardDetailsService.GetStandardDetailsById(StandardDetailsId);
                EntityMapper<StandardDetails, StandardDetailsEdit>.Map(standardDetails, model);
                model.IsAddMode = false;
            }
            else
            {
                model.IsAddMode = true;
            }
            return PartialView("_AddEditStandardDetails", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveStandardDetails(StandardDetailsEdit model)
        {
            var result = _standardDetailsService.SaveUpdateStandardDetails(model);
            return Json(new OperationDetails(result.Success), JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetStandardDetailsList(int? StandardId)
        {
            var lstStandardDetails = _standardDetailsService.GetStandardDetailsList().Where(x => x.StandardID == StandardId).ToList();

            var actionButtonsHtmlTemplate = "<div class='tbl-actions'><a type='button' class='table-action-text-link clsEditUnit' onclick='StandardDetails.editStandardDetails($(this),{0})'><i class='fas fa-pencil-alt mr-2'></i></a>" +
                                             "<a class='table-action-text-link clsEditUnit' title='Add Lesson' href='/Course/Lesson/Index?StandardId={1}&SubTopic={2}'><i class='fas fa-plus'></i></a>" +
                                            "</div>";
            //var ButtonForStandardDetails = "<div class='tbl-actions'><button type='button' class='btn btn-sm btn-primary waves-effect waves-light' onclick='StandardDetails.LinkForLesson($(this),{0})'>Add Lesson</button>" +
            //                               "</div>";
            var courseid = EncryptDecryptHelper.EncryptUrl(StandardId);
            var dataList = new object();

            dataList = new
            {
                aaData = (from item in lstStandardDetails
                          select new
                          {
                              Actions = CurrentPagePermission.CanEdit ? string.Format(actionButtonsHtmlTemplate, item.StandardDetailID, EncryptDecryptHelper.EncryptUrl(item.StandardDetailID), item.COR_TITLE) : string.Empty,
                              //ActionsForLink = string.Format(ButtonForStandardDetails, EncryptDecryptHelper.EncryptUrl(item.StandardID)),
                              //ActionsForLink = "<div class='tbl-actions'><a style='color:#fff' class='btn btn-sm btn-primary waves-effect waves-light' href='/Course/Lesson/Index?StandardId="
                              //      + EncryptDecryptHelper.EncryptUrl(item.StandardDetailID)
                              //      + "'>" + "Add Lesson"
                              //      + "</div>",
                              item.Description,
                              //item.Order,
                              //item.Points,
                              //item.Weight,
                              item.StandardGroupName
                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DeleteStandardDetails(int StandardDetailsID)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var standardDetails = _standardDetailsService.GetStandardDetailsById(StandardDetailsID);
            var result = _standardDetailsService.DeleteStandardDetails(standardDetails);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}