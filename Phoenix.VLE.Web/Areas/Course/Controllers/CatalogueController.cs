﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Course.Controllers
{
    public class CatalogueController : BaseController
    {
        private readonly ICourseCatalogueService courseCatalogueService;
        private readonly IUnitService unitService;

        public CatalogueController(ICourseCatalogueService courseCatalogueService, IUnitService unitService)
        {
            this.courseCatalogueService = courseCatalogueService;
            this.unitService = unitService;
        }
        // GET: CourseCatalogue/Catalogue
        public ActionResult Index()
        {
            var memberId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var isStudent = SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent();
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            var courseCatalogue = courseCatalogueService.GetCourseCatalogueInformationByTeacher(memberId, isStudent).ToList();
            if (courseCatalogue.Any())
            {
                var courseId = courseCatalogue.FirstOrDefault().CourseId;
                var units = courseCatalogueService.GetCatalogueUnits(courseId, memberId, isStudent).ToList();
                if (units.Any())
                {
                    units = units.AsHierarchy(c => c.UnitId, c => c.ParentId, (c, p) => c = p, (c, p) => p.Units.Add(c), 0).ToList();
                    courseCatalogue.FirstOrDefault().Units = units;
                }
            }
            ViewBag.weekList = unitService.GetUnitWeekList(schoolId);
            return View(courseCatalogue);
        }
        public ActionResult GroupInfo(string id)
        {
            //292174
            var groupId = EncryptDecryptHelper.DecryptUrl(id).ToLong();
            var result = courseCatalogueService.GetCatalogueInfoUsingGroupId(groupId);
            return PartialView(result ?? new Phoenix.Models.GroupCatalogue());
        }
        public ActionResult GetUnitDetails(long courseId)
        {
            var memberId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var isStudent = SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent();
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            var result = courseCatalogueService.GetCatalogueUnits(courseId, memberId, isStudent).ToList();
            ViewBag.weekList = unitService.GetUnitWeekList(schoolId);
            result = result.AsHierarchy(c => c.UnitId, c => c.ParentId, (c, p) => c = p, (c, p) => p.Units.Add(c), 0).ToList();
            return PartialView("_Units", result ?? new List<Phoenix.Models.CatalogueUnits>());
        }
    }
}