﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Phoenix.VLE.Web.Areas.Course.Models;
using Phoenix.Common.Localization;
using Phoenix.Common.Enums;

namespace Phoenix.VLE.Web.Areas.Course.Controllers
{
    public class LessonController : BaseController
    {
        private readonly ILessonService _ILessonService;
        private readonly IDivisionService divisionService;

        public LessonController(ILessonService LessonService, IDivisionService divisionService)
        {
            _ILessonService = LessonService;
            this.divisionService = divisionService;
        }
        // GET: Course/Lesson/LessonCourse
        //public ActionResult Index(string StandardId = "", string SubTopic = "")
        //{
        //    List<Lesson> obj = new List<Lesson>();
        //    //ViewBag.StandardDetails = new SelectList(_ILessonService.GetStandardDetails(), "StandardId", "StandardName");
        //    //ViewBag.UnitDetails = new SelectList(_ILessonService.GetUnitDetails(), "UnitId", "UnitName");
        //    obj = _ILessonService.GetLessonDetails().Where(x => x.StandardId == Convert.ToString(StandardId)).ToList();
        //    //var Standard = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl((StandardId)));
        //    ViewBag.StandID = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl((StandardId)));
        //    ViewBag.SubTopic = SubTopic;
        //    return View(obj);
        //}
        [HttpPost]
        public ActionResult AddEditLesson(Lesson objLesson)
        {
            var Result = _ILessonService.AddEditLesson(objLesson);
            return Json(new OperationDetails(Result), JsonRequestBehavior.AllowGet);

        }
        //public ActionResult GetAddEditLesson(int? StandardId)
        //{
        //    var obj = _ILessonService.GetLessonDetails().Where(x => x.StandardId == Convert.ToString(StandardId)).ToList();
        //    return PartialView("_LessonList", obj);

        //}
        //public ActionResult GetAddEditLessonEdit(int? StandardId)
        //{
        //    var obj = _ILessonService.GetLessonDetails().Where(x => x.LessonId == (StandardId)).ToList();
        //    return Json((obj), JsonRequestBehavior.AllowGet);

        //}
        public ActionResult LessonCourse()
        {
            var courseList = new SelectList(SelectListHelper.GetSelectListData(ListItems.Course, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            // var result = _ILessonService.GetTopicLessonDetail(17873);
            ViewBag.TeacherCourse = courseList;
            //var list = new List<LessonCourse>();
            //if (result != null)
            //{

            //    list = result.Where(x => x.ParentId == 0).DistinctBy(x => x.UnitMasterId).ToList();
            //}
            //ViewBag.ParentList = list;
            return View();
        }
        public ActionResult GetTopicLessonDetail(Int64? CourseId)
        {
            var result = _ILessonService.GetTopicLessonDetail(Convert.ToInt64(CourseId));
            var list = new List<LessonCourse>();
            if (result != null)
            {

                list = result.Where(x => x.ParentId == 0).DistinctBy(x => x.UnitMasterId).ToList();
            }
            ViewBag.ParentList = list;
            return PartialView("_CourseGrid", result);
        }
        public ActionResult AddEditLesson(Int64? StandardId, Int64? AddStaus)
        {
            Lesson model = new Lesson();

            if (StandardId.HasValue)
            {
                model = _ILessonService.GetLessonDetails().Where(x => x.LessonId == StandardId).FirstOrDefault();
                if (model != null)
                {
                    model.Details = model.Details;
                    model.StandardId = model.StandardId;
                    model.StartDate = model.StartDate;
                    model.EndDate = model.EndDate;
                }
            }
            else
            {
                model.IsAddMode = true;
            }
            var courseList = new SelectList(SelectListHelper.GetSelectListData(ListItems.Course, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.TeacherCourse = courseList;
            ViewBag.AddStaus = AddStaus;
            return PartialView("_AddEditLesson", model);
        }
        public ActionResult AddEditLessonForAddUnitDetails()
        {
            Lesson model = new Lesson();
            var courseList = new SelectList(SelectListHelper.GetSelectListData(ListItems.Course, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.TeacherCourse = courseList;
            return PartialView("_AddEditLessonUnit", model);
        }
        public ActionResult GetUnitBasedonCourse(Int64? CourseId)
        {

            var data = new SelectList(_ILessonService.GetUnitBasedonCourse(Convert.ToInt64(CourseId)), "UnitId", "UnitName");
            return Json((data), JsonRequestBehavior.AllowGet);
        }
        #region School Unit Detail type Mapping
        public PartialViewResult GetSchoolUnitDetailsMappingPopup()
        {
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            var curriculum = SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{schoolId}");
            ViewBag.Curriculum = new SelectList(curriculum, "ItemId", "ItemName", curriculum.FirstOrDefault().ItemId);
            ViewBag.Division = new SelectList(divisionService.GetDivisionDetails(schoolId, curriculum.FirstOrDefault().ItemId.ToInteger()), "DivisionId", "DivisionName");
            //var response = _ILessonService.GetSchoolUnitDetailTypes(SessionHelper.CurrentSession.SchoolId);
            //return PartialView("_SchoolUnitDetailsMapping", response);
            return PartialView("_SchoolUnitDetailsMapping");
        }
        public PartialViewResult GetUnitDetailDropdown(int divisionId)
        {
            var response = _ILessonService.GetSchoolUnitDetailTypes(SessionHelper.CurrentSession.SchoolId, divisionId);
            return PartialView("_UnitDetailDropdown", response);
        }
        public JsonResult SaveSchoolUnitDetailsMapping(SchoolUnitDetailTypeMapping typeMapping)
        {
            typeMapping.SchoolId = SessionHelper.CurrentSession.SchoolId;
            var response = _ILessonService.SchoolUnitDetailsTypeCD(typeMapping);
            return Json(new OperationDetails(response), JsonRequestBehavior.AllowGet);
        }
        public ActionResult UnitDetailConfig()
        {
            return View();
        }
        public JsonResult GetUnitDetailConfigGrid()
        {
            var response = _ILessonService.GetSchoolUnitDetailType(new SchoolUnitDetailType { SchoolId = SessionHelper.CurrentSession.SchoolId });
            var dataList = new
            {
                aaData = (from item in response
                          select new
                          {
                              item.UnitDetailTypeId,
                              item.Title,
                              item.UnitGroupName,
                              item.Description,
                              item.Order,
                              item.ControlType,
                              item.UnitGroupId,
                              item.AttachmentKey,
                              item.CourseTitle,
                              Actions = GetActionLinks(item)
                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        private string GetActionLinks(SchoolUnitDetailType model)
        {
            string actions = string.Empty;
            actions += $"<a class='mr-2' href='javascript:void(0);' data-toggle='tooltip' onclick='unitDetailConfig.loadUnitDetailTypeForm(this,{model.UnitDetailTypeId})'  data-original-title='" + ResourceManager.GetString("Shared.Buttons.Edit") + $"' )'><img src='/Content/vle/img/svg/pencil-big.svg' /></a>";
            //if (model.SecondaryReportId != 0)
            // if (!model.RecordExists)
            actions += $"<a class='mr-2' href='javascript:void(0)' onclick='unitDetailConfig.deleteUnitConfig({model.UnitDetailTypeId})' data-toggle='tooltip'  data-original-title='" + ResourceManager.GetString("Shared.Buttons.Delete") + $"' )'><img src='/Content/vle/img/svg/delete.svg' /></a>";
            return actions;
        }
        [HttpGet]
        public ActionResult AddEditUnitDetailType(int? unitDetailTypeId = null)
        {
            var untiDetailTypeId = new SchoolUnitDetailType() { IsActive = true };
            ViewBag.Curriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.GroupName = new SelectList(SelectListHelper.GetSelectListData(ListItems.UnitGroupName), "ItemId", "ItemName");
            if (unitDetailTypeId != null)
            {
                untiDetailTypeId = _ILessonService.GetSchoolUnitDetailType(new SchoolUnitDetailType
                {
                    UnitDetailTypeId = unitDetailTypeId,
                    SchoolId = SessionHelper.CurrentSession.SchoolId
                }).FirstOrDefault();
            }
            ViewBag.ControlType = Enum.GetValues(typeof(UnitControlType)).Cast<UnitControlType>()
                .Select(x => new SelectListItem { Text = StringEnum.GetStringValue(x), Value = x.ToString(), Selected = x.ToString() == untiDetailTypeId.ControlType });
            return PartialView(untiDetailTypeId);
        }
        public ActionResult GetCourseList(long CurriculumId = 0)
        {
            var SchoolId = SessionHelper.CurrentSession.SchoolId;
            var CourseList = new SelectList(SelectListHelper.GetSelectListData(ListItems.Course, "CurriculumId = " + CurriculumId, $"{SchoolId}"), "ItemId", "ItemName");
            return Json(CourseList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult AddEditUnitDetailType(SchoolUnitDetailType schoolUnit)
        {
            schoolUnit.SchoolId = SessionHelper.CurrentSession.SchoolId;
            var result = _ILessonService.SaveSchoolUnitDetailType(schoolUnit);
            return Json(new OperationDetails(result)
            {
                Message = result && !schoolUnit.IsActive ? LocalizationHelper.DeleteSuccessMessage :
                result && schoolUnit.UnitDetailTypeId != null ? LocalizationHelper.UpdateSuccessMessage :
                result && schoolUnit.UnitDetailTypeId == null ? LocalizationHelper.AddSuccessMessage :
                LocalizationHelper.TechnicalErrorMessage
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}