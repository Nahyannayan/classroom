﻿using Newtonsoft.Json;
using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Areas.Course.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Phoenix.VLE.Web.Extensions;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharp;
using PdfSharp.Pdf;
using System.Text;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using HtmlToOpenXml;
using Phoenix.Web.Helpers;

namespace Phoenix.VLE.Web.Areas.Course.Controllers
{
    [Authorize]
    public class CourseController : BaseController
    {
        private readonly ICourseService courseService;
        private readonly IUserPermissionService userPermissionService;
        private readonly ISchoolService schoolService;
        private readonly ISchoolGroupService schoolGroupService;
        private readonly IStudentListService studentListService;
        private readonly IAttachmentService attachmentService;
        private readonly IUnitService _iUnitService;
        private readonly IFileService fileService;
        private List<VLEFileType> _fileTypeList;

        public CourseController(ICourseService courseService, IUserPermissionService userPermissionService, ISchoolService schoolService,
            ISchoolGroupService schoolGroupService, IStudentListService studentListService, IAttachmentService attachmentService, IUnitService iUnitService, IFileService fileService)
        {
            this.courseService = courseService;
            this.userPermissionService = userPermissionService;
            this.schoolService = schoolService;
            this.schoolGroupService = schoolGroupService;
            this.studentListService = studentListService;
            this.attachmentService = attachmentService;
            this._iUnitService = iUnitService;
            this.fileService = fileService;
            _fileTypeList = this.fileService.GetFileTypes().ToList();
        }
        // GET: Course/Course
        public ActionResult Index()
        {
            ViewBag.Curriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            return View();
        }
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult CreateCourse(CourseEdit courseEdit)
        {
            //convert arabic dates
            courseEdit.StartDate = courseEdit.strStartDate.ToSystemReadableDate();
            courseEdit.EndDate = courseEdit.strEndDate.ToSystemReadableDate();

            var storeDirectory = Constants.CourseFilesDir + "/School_" + SessionHelper.CurrentSession.SchoolId;
            var courseImageKeyName = Phoenix.Models.Entities.AttachmentKey.CourseImage.EnumToString();

            courseEdit.AttachmentList.RemoveAll(x => x == null);

            courseEdit.AttachmentList = StorageHelper.AddGoogleDrivePermission(this, courseEdit.CloudFileList);
            courseEdit.AttachmentList.AddRange(StorageHelper.SaveAllFileAttachment(Request.Files.GetMultiple("AttachmentList"), Phoenix.Models.Entities.AttachmentKey.Course.EnumToString(), storeDirectory, courseEdit.CourseId));
            if (courseEdit.IsSystemImageUpload && !string.IsNullOrEmpty(courseEdit.SystemImageList))
            {
                var systemImageAttachment = new Phoenix.Models.Entities.Attachment()
                {
                    AttachmentKey = courseImageKeyName,
                    AttachmentType = AttachmentType.SystemImage,
                    AttachmentPath = courseEdit.SystemImageList,
                    AttachedToId = courseEdit.CourseId,
                    FileExtension = System.IO.Path.GetExtension(courseEdit.SystemImageList),
                    FileName = System.IO.Path.GetFileName(courseEdit.SystemImageList),
                    Mode = TranModes.Insert
                };
                courseEdit.AttachmentList.Add(systemImageAttachment);
            }
            else
            {
                courseEdit.AttachmentList.AddRange(StorageHelper.SaveFileAttachment(courseEdit.CourseImage, courseImageKeyName, storeDirectory, courseEdit.CourseId));
            }
            #region To check if new course image uploaded and remove previous image
            var fileName = string.Empty;
            if (courseEdit.AttachmentList.Any(x => x.AttachmentKey == courseImageKeyName) && courseEdit.CourseId != 0)
            {
                var courseOld = courseService.GetCourseDetails(courseEdit.CourseId);
                if (courseOld != null)
                {
                    var attachment = courseOld.AttachmentList.FirstOrDefault(x => x.AttachmentKey == courseImageKeyName);
                    if (attachment != null)
                    {
                        fileName = attachment.AttachmentType == AttachmentType.SystemImage ? "" : PhoenixConfiguration.Instance.WriteFilePath + attachment.AttachmentPath;
                        var newAttachment = courseEdit.AttachmentList.FirstOrDefault(x => x.AttachmentKey == courseImageKeyName);
                        newAttachment.AttachmentId = attachment.AttachmentId;
                        newAttachment.Mode = Phoenix.Models.Entities.TranModes.Update;
                    }
                }
            }
            #endregion

            courseEdit.SchoolId = SessionHelper.CurrentSession.SchoolId;
            var course = EntityMapper<CourseEdit, Phoenix.Models.Course>.Map(courseEdit);
            var result = courseService.CourseCU(course);

            //to delete preview certificate image
            if (result.Success && !string.IsNullOrEmpty(fileName) && System.IO.File.Exists(fileName) && !fileName.Contains("Resources\\Courses\\GroupImages"))
                System.IO.File.Delete(fileName);
            result.Message = EncryptDecryptHelper.EncryptUrl(result.InsertedRowId);
            result.RelatedHtml = ControllerExtension.RenderPartialViewToString(this, "_CourseNavTab", (long)result.InsertedRowId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CourseNavTab(long id)
        {
            return PartialView("_CourseNavTab", id);
        }
        //public ActionResult LoadCourse(long curriculumId)
        //{            
        //    var isCustomPermissionEnabled = userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_StudentCertificate.ToString());
        //    var editLinkHref = "/course/course/AddEditCourse?id=";
        //    var unitCalendarLink = "/course/unit/unitcalendar?id=";
        //    var courseList = courseService.GetCourses(curriculumId, SessionHelper.CurrentSession.SchoolId);
        //    var dataList = new
        //    {
        //        aaData = (from item in courseList
        //                  select new
        //                  {
        //                      Title = GetCourseUnitCalendarLink(isCustomPermissionEnabled,item.Title,unitCalendarLink+ EncryptDecryptHelper.EncryptUrl(item.CourseId)),
        //                      item.Description,
        //                      CourseType = Enum.GetName(typeof(CourseType), Convert.ToInt32(item.CourseType)),
        //                      StartDate = item.StartDate.FormatDate(),
        //                      EndDate = item.EndDate.FormatDate(),
        //                      Actions = GetCourseActionLinks(isCustomPermissionEnabled, editLinkHref + EncryptDecryptHelper.EncryptUrl(item.CourseId))
        //                  }).ToArray()
        //    };

        //    return Json(dataList, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult LoadCourseMappingList(string Id, int pageIndex = 1, string searchString = "")
        //{
        //    var model = new Pagination<CourseMapping>();
        //    var editUrl = "/course/course/AddEditMapCourse?id=";
        //    ViewBag.isCustomPermissionEnabled = userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_StudentCertificate.ToString());
        //    if (!string.IsNullOrEmpty(Id))
        //    {
        //        var courseId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(Id));
        //        var courseGroups = courseService.GetCourseMappings(SessionHelper.CurrentSession.SchoolId, courseId, pageIndex, 12, searchString);
        //        if (courseGroups.Any())
        //            model = new Pagination<CourseMapping>(pageIndex, 12, courseGroups.ToList(), courseGroups.FirstOrDefault().TotalCount);
        //        model.RecordCount = courseGroups.Count() == 0 ? 0 : courseGroups.FirstOrDefault().TotalCount;
        //        model.LoadPageRecordsUrl = "/Course/Course/LoadCourseMappingList?Id=" + Id + "&pageIndex={0}";
        //        model.SearchString = searchString;
        //        model.PageRecords.ForEach(x =>
        //        {
        //            x.EditUrl = string.Concat(editUrl, EncryptDecryptHelper.EncryptUrl(x.SchoolGroupId));
        //        });
        //    }
        //    return PartialView("_CourseGroupList", model);
        //}

        public ActionResult LoadCourse(long curriculumId, int pageIndex = 1, string searchString = "")
        {
            var PageSize = 8;
            var model = new Pagination<CourseEdit>();
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var isCustomPermissionEnabled = userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_StudentCertificate.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);


            var editLinkHref = "/course/course/AddEditCourse?id=";
            var unitCalendarLink = "/course/unit/unitcalendar?id=";
            var courseGroups = "/course/course/MapCourse?Id=";
            var courseList = courseService.GetCourses(curriculumId, SessionHelper.CurrentSession.SchoolId, PageSize, pageIndex, searchString).ToList();

            List<CourseEdit> courseEdits = new List<CourseEdit>();
            courseList.ForEach(x =>
            {
                var courseid = EncryptDecryptHelper.EncryptUrl(x.CourseId);
                var newCourse = new CourseEdit()
                {
                    UnitCalendarURL = (unitCalendarLink + courseid),
                    Description = x.Description == null ? "" : x.Description,
                    CourseType = Enum.GetName(typeof(CourseType), Convert.ToInt32(x.CourseType)),
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    EditURL = (editLinkHref + courseid),
                    ViewCourseGroups = (courseGroups + courseid),
                    Title = x.Title,
                    CourseImageName = x.CourseImageName
                };
                courseEdits.Add(newCourse);
            });

            if (courseList.Any())
                model = new Pagination<CourseEdit>(pageIndex, PageSize, courseEdits.ToList(), courseList.FirstOrDefault().TotalCount);
            model.RecordCount = courseList.Count() == 0 ? 0 : courseList.FirstOrDefault().TotalCount;
            model.LoadPageRecordsUrl = "/Course/Course/LoadCourse?curriculumId=" + curriculumId + "&pageIndex={0}";
            model.SearchString = searchString;

            ViewBag.isCustomPermissionEnabled = isCustomPermissionEnabled;

            return PartialView("_CourseListPartial", model);
        }
        [NonAction]
        private string GetCourseActionLinks(bool isPermissionEnabled, string link)
        {
            string actions = string.Empty;
            actions += "<div class='tbl-actions'>"
               + (isPermissionEnabled && SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin ? $"<a type class='table-action-icon-btn'  title='" + ResourceManager.GetString("Shared.Buttons.Edit") + $"' href='{link}'><i class='fas fa-pen'></i></a>" : string.Empty)
                + "</div>";
            return actions;
        }
        [NonAction]
        private string GetCourseUnitCalendarLink(bool isPermissionEnabled, string title, string link)
        {
            string actions = string.Empty;
            actions += "<div>"
               + (isPermissionEnabled && SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin
               ? $"<a title='" +
               title + $"'" +
               $" href='{link}'>{title}</a>" : string.Empty)
                + "</div>";
            return actions;
        }
        public ActionResult AddEditCourse(string id = "")
        {
            ViewBag.CourseEncryptedId = id.ReEncryptUrl();
            ViewBag.CourseId = EncryptDecryptHelper.DecryptUrl(id).ToLong();
            return View();
        }

        public ActionResult EditCourse(string id = "")
        {
            ViewBag.CourseEncryptedId = id.ReEncryptUrl();
            ViewBag.CourseId = EncryptDecryptHelper.DecryptUrl(id).ToLong();
            return View("AddEditCourse");
        }

        public ActionResult AddEditCourseById(string id = "")
        {
            Phoenix.Models.Course course = new Phoenix.Models.Course();
            var userId = SessionHelper.CurrentSession.Id;
            ViewBag.Curriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.DepartmentList = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolDepartment, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.StudentList = new SelectList(string.Empty);
            if (string.IsNullOrEmpty(id))
            {
                course.CreatedBy = userId;
                course.StartDate = DateTime.Now;
                /// course.EndDate = DateTime.Now.AddDays(1);
            }
            else
            {
                var courseId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(id));
                course = courseService.GetCourseDetails(courseId);
            }
            var result = EntityMapper<Phoenix.Models.Course, CourseEdit>.Map(course);
            return PartialView("_AddEditCourse", result);
        }
        public JsonResult GetStudentByGradeIds(string id)
        {
            var studentList = studentListService.GetStudentByGradeIds(id);
            var sectionList = studentList.Select(x => new { x.SectionId, x.SectionName, x.GradeDisplay, x.SchoolGradeId, x.SchoolShortName }).Distinct().ToList();
            return Json(new { studentList, SectionList = sectionList }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult IsCourseTitleUnique(string title, string PreviousTitle)
        {
            if (title.ToLower() == PreviousTitle.ToLower())
                return Json(false, JsonRequestBehavior.AllowGet);

            var result = courseService.IsCourseTitleUnique(title);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult IsGroupNameUnique(string title, string previousGroupName)
        {
            if (title.ToLower() == previousGroupName.ToLower())
                return Json(true, JsonRequestBehavior.AllowGet);

            var result = courseService.IsGroupNameUnique(title);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MapCourse(string Id = "")
        {
            ViewBag.CourseId = EncryptDecryptHelper.DecryptUrl(Id);
            return View();
        }
        public ActionResult LoadCourseMappingList(string Id, int pageIndex = 1, string searchString = "")
        {
            var model = new Pagination<CourseMapping>();
            var courseId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(Id));
            var encryptedCourse = EncryptDecryptHelper.EncryptUrl(courseId.ToString());
            ViewBag.CourseId = encryptedCourse;
            var editUrl = "/course/course/AddEditMapCourse?corId=" + encryptedCourse + "&id=";
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.isCustomPermissionEnabled = userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_StudentCertificate.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            if (!string.IsNullOrEmpty(Id))
            {
                var courseGroups = courseService.GetCourseMappings(SessionHelper.CurrentSession.SchoolId, courseId, pageIndex, 12, searchString);
                if (courseGroups.Any())
                    model = new Pagination<CourseMapping>(pageIndex, 12, courseGroups.ToList(), courseGroups.FirstOrDefault().TotalCount);
                model.RecordCount = courseGroups.Count() == 0 ? 0 : courseGroups.FirstOrDefault().TotalCount;
                model.LoadPageRecordsUrl = "/Course/Course/LoadCourseMappingList?Id=" + encryptedCourse + "&pageIndex={0}";
                model.SearchString = searchString;
                model.PageRecords.ForEach(x =>
                {
                    x.EditUrl = string.Concat(editUrl, EncryptDecryptHelper.EncryptUrl(x.SchoolGroupId));
                });
            }
            return PartialView("_CourseGroupList", model);
        }
        public ActionResult AddEditMapCourse(string corId = "", string id = "")
        {
            var courseMapping = new CourseMappingEdit();
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            ViewBag.CourseId = corId;
            var courseId = EncryptDecryptHelper.DecryptUrl(corId);
            var Courses = SelectListHelper.GetSelectListData(ListItems.Course, schoolId).FirstOrDefault(x => x.ItemId == courseId);
            if (Courses != null)
            {
                courseMapping.CourseId = Convert.ToInt64(Courses.ItemId);
                courseMapping.CourseTitle = Courses.ItemName;
            }
            var Grades = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolGrade, schoolId), "ItemId", "ItemName");
            var completeTeacherList = courseService.GetSchoolTeachersBySchoolId(schoolId.ToString(), string.Empty);
            var SchoolList = new SelectList(string.Empty);
            var newSchoolGroup = new List<GroupGrade>() { new GroupGrade { } };
            if (!string.IsNullOrEmpty(id))
            {
                var courseMapid = EncryptDecryptHelper.DecryptUrl(id);
                var courseMap = courseService.GetCourseMappingDetail(Convert.ToInt64(courseMapid));
                var courseMapEdit = EntityMapper<Phoenix.Models.CourseMapping, CourseMappingEdit>.Map(courseMap);
                if (courseMap != null)
                {
                    //var teacherIds = string.Join(",", courseMap.TeacherIds);
                    var teacherIds = string.Join(",", courseMap.GroupAssignTeachers.Select(x => x.TeacherId));
                    var schoolAssignedToTeachers = courseService.GetSchoolListByTeacherId(teacherIds).ToList();
                    SchoolList = new SelectList(schoolAssignedToTeachers, "Id", "SchoolName");

                    var schoolIds = string.Join(",", schoolAssignedToTeachers.Select(x => x.Id));
                    var schoolViseGradeIds = courseService.GetGradeListBySchoolId(schoolIds, 0);
                    ViewBag.SelectedSchools = schoolAssignedToTeachers;
                    ViewBag.SelectedGrades = schoolViseGradeIds;
                    ViewBag.SelectSchools = courseMapEdit.Students.DistinctBy(x => x.SchoolId).Where(x => x.SchoolId != SessionHelper.CurrentSession.SchoolId).ToList();
                    ViewBag.SelectGrades = courseMapEdit.Students.DistinctBy(x => x.GradeId).ToList();
                }
                courseMapping = courseMapEdit;
            }
            ViewBag.Grades = Grades;
            ViewBag.TeacherList = completeTeacherList;
            ViewBag.SchoolList = SchoolList;
            //ViewBag.SchoolListHtml = ControllerExtension.RenderPartialViewToString(this, "_CourseGroupTemplateFields");
            return View(courseMapping);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveCourseMap(CourseMapping courseMapping)
        {
            courseMapping.CreatedBy = SessionHelper.CurrentSession.Id;
            var result = courseService.CourseMappingCUD(courseMapping);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CurriculumMap(string id = "")
        {
            var courseId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(id));

            var model = _iUnitService.GetCourseUnitMasterByCourseId(SessionHelper.CurrentSession.SchoolId, courseId);
            var unitDetails = _iUnitService.GetUnitDetailsByCourseId(courseId);
            ViewData["UnitDetails"] = unitDetails;
            ViewData["CourseId"] = id;
            Session["CurriculumMapModel"] = model;
            Session["unitDetails"] = unitDetails;
            ViewData["IsExport"] = false;
            return PartialView("_CurriculumMap", model);
        }

        public JsonResult DeleteAttachment(long id, string filepath)
        {
            filepath = PhoenixConfiguration.Instance.WriteFilePath + filepath;
            var result = attachmentService.DeleteAttachment(id);
            if (System.IO.File.Exists(filepath) && result &&  !filepath.Contains("Resources/Courses/GroupImages"))
                System.IO.File.Delete(filepath);
            return Json(id, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGradesBySchoolId(long schoolId)
        {
            var grades = SelectListHelper.GetSelectListData(ListItems.SchoolGrade, schoolId).Select(x => new { Text = x.ItemName, Value = x.ItemId });
            return Json(grades, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UnitCalendar(string id = "")
        {
            Phoenix.Models.Course model = new Phoenix.Models.Course();
            var CourseId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(id));
            model = courseService.GetCourseDetails(CourseId);

            var result = EntityMapper<Phoenix.Models.Course, CourseEdit>.Map(model);

            return PartialView("_UnitCalendar", result);
        }
        public ActionResult DownLoadAttachment(string id = "", string FilePath = "", string FileName = "")
        {
            if (!string.IsNullOrWhiteSpace(FilePath))
            {

                string path = PhoenixConfiguration.Instance.ReadFilePath + FilePath;

                WebClient myWebClient = new WebClient();
                byte[] myDataBuffer = myWebClient.DownloadData(path);
                Response.BufferOutput = true;

                return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, FileName);
            }
            else
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.FileNotFound), JsonRequestBehavior.AllowGet);

        }

        #region Teacher Cross School Permission
        public ActionResult CrossSchoolPermission()
        {
            return View();
        }
        public ActionResult AddEditCrossSchoolPermission(long tcsid = 0)
        {
            CrossSchoolPermission crossSchoolPermission = new CrossSchoolPermission();
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            ViewBag.GradeList = new SelectList(string.Empty);
            if (tcsid > 0)
            {
                var list = courseService.GetCrossSchoolPermissionList(SessionHelper.CurrentSession.Id, schoolId, tcsid);
                if (list.Any())
                    crossSchoolPermission = list.FirstOrDefault();
                //ViewBag.GradeList = new SelectList(courseService.GetGradeListBySchoolId(crossSchoolPermission.SchoolIds, 0), "SchoolGradeId", "SchoolGradeDisplay");
            }
            ViewBag.TeacherList = new SelectList(schoolService.GetSchoolTeachersBySchoolId(schoolId), "Id", "TeacherName");
            ViewBag.SchoolList = new SelectList(schoolService.GetSchoolList(), "SchoolId", "SchoolName");
            return PartialView("_AddEditCrossSchoolPermission", crossSchoolPermission);
        }
        public ActionResult GetGradeListBySchoolId(string SchoolIds, long TeacherId = 0)
        {
            var list = new SelectList(courseService.GetGradeListBySchoolId(SchoolIds, TeacherId), "SchoolGradeId", "SchoolGradeDisplay");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveCrossSchoolPermission(CrossSchoolPermission crossSchoolPermission)
        {
            crossSchoolPermission.UserId = SessionHelper.CurrentSession.Id;
            var result = courseService.SaveCrossSchoolPermission(crossSchoolPermission);
            if (crossSchoolPermission.IsDelete)
            {
                return Json(new OperationDetails(result) { Message = LocalizationHelper.DeleteSuccessMessage }, JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(result) { Message = crossSchoolPermission.TCS_ID == 0 ? LocalizationHelper.AddSuccessMessage : LocalizationHelper.UpdateSuccessMessage }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCrossSchoolPermissionGrid(long TeacherId = 0)
        {
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            var list = courseService.GetCrossSchoolPermissionList(TeacherId, SchoolId).ToList();
            return PartialView("_CrossSchoolPermissionGrid", list);
        }
        public JsonResult GetSchoolListByTeacherId(string TeacherId)
        {
            var response = courseService.GetSchoolListByTeacherId(TeacherId).ToList();
            response.RemoveAll(x => x.Id == SessionHelper.CurrentSession.SchoolId);
            var list = new SelectList(response, "Id", "SchoolName");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GeteGradeListPassByschoolIds(string SchoolId, string TeacherId)
        {
            var list = new SelectList(courseService.GetGradeListBySchoolId(SchoolId, 0).ToList(), "SchoolGradeId", "SchoolGradeDisplay");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CurriculumMap Export to PDF/Word

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadCurriculumMapToPDF")]
        public ActionResult DownloadCurriculumMapDataAsPDF(FormCollection form)
        {

            string pdfhtml;
            string pdfTitle;
            string downloadFileName;
            ViewBag.ReportType = "PDF";
            var CourseId = form["CourseId"];
            var CourseName = form["CurriculumCourseName"];
            var Model = Session["CurriculumMapModel"] as IEnumerable<UnitMasterEdit>;
            var unitDetails = Session["unitDetails"] as IEnumerable<UnitDetailsTypeEdit>;


            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_CurriculumMapExport", Model, new ViewDataDictionary { { "IsExport", true }, { "UnitDetails", unitDetails }, { "CourseId", CourseId } });

            pdfTitle = "Curriculum Map";
            downloadFileName = "CurriculumMap_"+ CourseName+".pdf";

            var config = new PdfGenerateConfig()
            {
                MarginBottom = 20,
                MarginLeft = 20,
                MarginRight = 20,
                MarginTop = 20,
            };
            //config.PageOrientation = PageOrientation.Landscape;
            config.PageSize = PageSize.A4;


            PdfDocument pdf = new PdfDocument();


            pdf = PdfGenerator.GeneratePdf(pdfhtml, config);
            pdf.Info.Title = pdfTitle;
            MemoryStream stream = new MemoryStream();
            pdf.Save(stream, false);
            byte[] file = stream.ToArray();
            stream.Write(file, 0, file.Length);
            stream.Position = 0;

            return File(stream, "application/pdf", downloadFileName);

        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadCurriculumMapToWordFile")]
        public ActionResult DownloadCurriculumMapDataAsWordFile(FormCollection form)
        {

            string pdfhtml;
            var CourseId = form["CourseId"];
            var CourseName = form["CurriculumCourseName"];
            var Model = Session["CurriculumMapModel"] as IEnumerable<UnitMasterEdit>;
            var unitDetails = Session["unitDetails"] as IEnumerable<UnitDetailsTypeEdit>;

            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE HTML PUBLIC \" -//W3C//DTD HTML 4.0 Transitional//EN\">" +
                      "<html><head><title></title></head><body>");
            //sb.Append("<div style=\"text-align:center\"><font size=\"large\"><b>" + CourseName + "</b></font></div>");

            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_CurriculumMapExport", Model, new ViewDataDictionary { { "IsExport", true }, { "UnitDetails", unitDetails }, { "CourseId", CourseId } });
            sb.Append(pdfhtml);
            sb.Append("</body></html>");


            return File(EPPlusHelper.HtmlToWord(sb.ToString()),
         "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "CurriculumMap_"+ CourseName + ".docx");


        }


        #endregion

    }

}