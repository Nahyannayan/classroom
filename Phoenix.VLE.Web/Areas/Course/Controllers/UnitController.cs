﻿using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Areas.Course.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Phoenix.Common.Helpers.Extensions;
using DevExpress.XtraCharts;

namespace Phoenix.VLE.Web.Areas.Course.Controllers
{
    public class UnitController : BaseController
    {
        private readonly ICourseService _courseService;
        private readonly IUnitService _iUnitService;
        private readonly IStandardService _standardService;
        private readonly IAssessmentEvidenceService _assessmentEvidenceService;
        private readonly IAssessmentLearningService _assessmentLearningService;


        public UnitController(IUnitService iUnitService,
            IStandardService standardService, 
            ICourseService courseService, 
            IAssessmentEvidenceService assessmentEvidenceService
            ,IAssessmentLearningService assessmentLearningService
            )
        {
            _courseService = courseService;
            _iUnitService = iUnitService;
            _standardService = standardService;
            _assessmentEvidenceService = assessmentEvidenceService;
            _assessmentLearningService = assessmentLearningService;
        }
        // GET: Course/Unit
        public ActionResult Index()
        {
            return View("UnitMaster");
        }

        #region Unit Master

        public ActionResult UnitMaster(long Id = 0)
        {
            UnitMasterEdit objUnitMaster = new UnitMasterEdit();
            ViewBag.Course = new SelectList(SelectListHelper.GetSelectListData(ListItems.Course, null), "ItemId", "ItemName");
            if (Id > 0)
            {
                objUnitMaster = _iUnitService.GetUnitMasterDetailsByUnitId(Id);

            }

            return PartialView("_AddEditUnitMaster", objUnitMaster);
        }

        public ActionResult UnitMasterGrid()
        {
            var result = _iUnitService.GetUnitMasterDetails().ToList();
            var dataList = new object();

            dataList = new
            {
                aaData = (from item in result
                          select new
                          {
                              CourseName = item.CourseName,
                              UnitName = $"<a  class='table-action-text-link clsTargetToUnitDetails' data-toggle='tooltip' title='' data-UnitId='{ item.UnitId }'><span class'btn-primary'>{item.UnitName}</span></a>",
                              StartDate = item.StartDate.ToString("dd MMMM yyyy"),
                              EndDate = item.EndDate.ToString("dd MMMM yyyy"),
                              UnitColorCode = $"<input type='text' value='' class='form-control rounded-circle z-depth-0' style='background-color:{item.UnitColorCode};width:50px;' disabled/>",
                              Action = "<a class='table-action-text-link clsEditUnit' data-toggle='tooltip' title='Edit' data-UnitId=" + item.UnitId + "><i class='fas fa-pencil-alt'></i></a>"

                          }).ToArray()



            };

            var dtjsResult = Json(dataList, JsonRequestBehavior.AllowGet);
            dtjsResult.MaxJsonLength = int.MaxValue;

            return dtjsResult;
        }

        [HttpPost]
        public ActionResult AddEditUnitMaster(UnitMasterEdit objUnitMaster)
        {
            var result = _iUnitService.AddEditUnitMaster(objUnitMaster, TransactionModes.Insert, Convert.ToString(SessionHelper.CurrentSession.Id));
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Unit Details Type

        public ActionResult UnitDetails(long Id = 0)
        {
            var result = _iUnitService.GetUnitDetailsType(Id).ToList();
            var lstGroupName = new List<UnitDetailsTypeEdit>();
            if (result != null)
            {
                lstGroupName = result.GroupBy(e => new { e.GroupName }).Select(e => new UnitDetailsTypeEdit
                {
                    GroupName = e.Key.GroupName

                }).ToList();
            }
            ViewBag.GroupList = lstGroupName;

            //ViewBag.Standards = new MultiSelectList(
            //    SelectListHelper.GetSelectListData(ListItems.Standard, null), "ItemId", "ItemName", new[] { 3, 22 });

            result.ForEach(x => { x.UnitId = Id; });
            return View("UnitDetails", result);

        }

        [HttpPost]
        public ActionResult AddEditUnitTypeDetails(List<UnitDetailsTypeEdit> objUnitTypeDetails, string lstOfStandardIds,string lstOfAssessmentEvidenceIds, string lstOfAssessmentLearningIds,string lessonIdsToDelete)
        {
            var UnitId = objUnitTypeDetails.FirstOrDefault().UnitId;
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            foreach (var item in objUnitTypeDetails)
            {
                var storeDirectory = Constants.UnitDetailsDir + "/School_" + schoolId + "/" + item.UnitId;
                item.AttachmentList = StorageHelper.SaveFileAttachment(item.File, item.AttachmentKey, storeDirectory, UnitId);
                item.File = null;
            }

            var result = _iUnitService.AddEditUnitDetailsType(objUnitTypeDetails, schoolId, lstOfStandardIds, lstOfAssessmentEvidenceIds, lstOfAssessmentLearningIds, lessonIdsToDelete, SessionHelper.CurrentSession.Id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult UnitInfo(string Id = "", string CourseId = "",string UnitTitle="")
        {
            ViewBag.UnitId = Id;
            ViewBag.CourseId = CourseId;
            ViewBag.UnitTitle = UnitTitle;
            return View();
        }
        public PartialViewResult GetUnitInfo(string Id = "", string CourseId = "",string UnitTitle = "")
        {

            var UnitId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(Id));
            var result = _iUnitService.GetUnitDetailsType(UnitId).ToList();
            var selectedStandardDetails = _iUnitService.GetStandardDetailsById(UnitId);
            var assessmentEvidences = _assessmentEvidenceService.GetAssessmentEvidenceList(UnitId);
            var assessmentLearningList = _assessmentLearningService.GetAssessmentLearningList(UnitId);
            var lstGroupName = result.GroupBy(e => new { e.GroupName }).Select(e => new UnitDetailsTypeEdit
            {
                GroupName = e.Key.GroupName

            }).ToList();

            ViewBag.GroupList = lstGroupName;
            ViewBag.CourseId = CourseId;
            ViewBag.StandardDetails = selectedStandardDetails;
            ViewBag.assessmentEvidences = assessmentEvidences;
            ViewBag.assessmentLearning = assessmentLearningList;
            ViewBag.UnitTitle = UnitTitle;
            
            result.ForEach(x => { x.UnitId = UnitId; });
            return PartialView("_UnitInfo", result);

        }

       
        public ActionResult DeleteUnitStandardDetailsById(long Id)
        {
            var result = _iUnitService.DeleteUnitStandardDetailsById(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteUnitById(UnitMasterEdit objUnitMaster)
        {
            objUnitMaster.StartDate = DateTime.Now;
            objUnitMaster.EndDate = DateTime.Now;
            var result = _iUnitService.AddEditUnitMaster(objUnitMaster, TransactionModes.Delete, Convert.ToString(SessionHelper.CurrentSession.Id));
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetStandardDetails(long UTM_ID = 0,int AttachmentCount =0,bool isExport=false)
        {
            var selectedStandardDetails = _iUnitService.GetStandardDetailsById(UTM_ID);
            var list = new List<TopicTreeView>();

            if (selectedStandardDetails != null)
            {
                list = selectedStandardDetails.DistinctBy(x => x.maintopic).ToList();
            }
            ViewBag.MainTopicList = list;
            ViewBag.Mode = "View";
            ViewBag.isExport = isExport;
            ViewBag.AttachmentCount= AttachmentCount;
            return PartialView("_SelectedStandardDetails", selectedStandardDetails);
        }
        public ActionResult GetUnitTopicList(long MainSyllabusId=0)
        {

            List<TopicTreeItemView> objJsonResult = new List<TopicTreeItemView>();
            var childList = _iUnitService.GetUnitTopicList(MainSyllabusId);
            var parentList = childList.Where(e => e.ParentID == 0).ToList();

            foreach (var parent in parentList)
            {
                List<TopicTreeItemView> objChildJsonResult = new List<TopicTreeItemView>();
                var Id = parentList.Where(e => e.TopicID == parent.TopicID).Select(e => e.TopicID).FirstOrDefault();
                if (!string.IsNullOrEmpty(Id.ToString()))
                {
                    var title = parentList.Where(e => e.TopicID == parent.TopicID).Select(e => e.Topic_Description).FirstOrDefault();
                    var lstChild = childList.Where(e => e.ParentID == Id).ToList();
                    if (lstChild.Any())
                    {
                        foreach (var child in lstChild)
                        {
                            objChildJsonResult.Add(new TopicTreeItemView
                            {
                                id = Convert.ToString(child.TopicID),
                                text = child.Topic_Description
                            });
                        }
                    }
                    objJsonResult.Add(new TopicTreeItemView
                    {
                        id = Id,
                        text = title,
                        state = "closed",
                        children = objChildJsonResult
                    });
                }
            }

            return Json(objJsonResult, JsonRequestBehavior.AllowGet);

        }

      

        //public ActionResult GetUnitTopicListView(string SubSyllabusId, long UnitMasterId)
        //{
        //    var result = _iUnitService.GetUnitTopicStandardDetails(SubSyllabusId, UnitMasterId);
        //    var list = new List<UnitTopicStandardDetails>();
        //    if (result != null)
        //    {
        //        list = result.DistinctBy(x => x.ParentID).ToList();
        //    }
        //    ViewBag.ParentList = list;
        //    return PartialView("_UnitTopicList", result);
        //}

        public ActionResult UnitCalendar(string id = "")
        {
            Phoenix.Models.Course model = new Phoenix.Models.Course();
            var CourseId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(id));
            model = _courseService.GetCourseDetails(CourseId);

            var result = EntityMapper<Phoenix.Models.Course, CourseEdit>.Map(model);

            return View("UnitCalendar", result);
        }
        public PartialViewResult UnitMasterPartial(long CourseId = 0, long UnitId = 0, long ParentId = 0, int order = 0, DateTime? StartDate = null, DateTime? EndDate = null)
        {
            var WeekList = _iUnitService.GetUnitWeekList(SessionHelper.CurrentSession.SchoolId);
            var StartWeek = WeekList;
            var EndWeek = WeekList;

            if (ParentId > 0)//for child unit 
            {
                if (WeekList.Any(x => x.WeekStart == StartDate) && WeekList.Any(x => x.WeekEnd == EndDate))
                {
                    StartWeek = StartWeek.Where(x => x.WeekEnd <= EndDate && x.WeekStart >= StartDate);
                    EndWeek = EndWeek.Where(x => x.WeekEnd <= EndDate && x.WeekStart >= StartDate);
                }
                else
                {
                    if (!WeekList.Any(x => x.WeekStart == StartDate))
                    {
                        var startWeek = WeekList.Where(x => x.WeekStart <= StartDate && x.WeekEnd >= StartDate).FirstOrDefault();
                        if (startWeek != null)
                            StartDate = startWeek.WeekStart;
                    }
                    if (!WeekList.Any(x => x.WeekEnd == EndDate))
                    {
                        var endWeek = WeekList.Where(x => x.WeekStart <= EndDate && x.WeekEnd >= EndDate).FirstOrDefault();
                        if (endWeek != null)
                            EndDate = endWeek.WeekEnd;
                    }
                    StartWeek = StartWeek.Where(x => x.WeekEnd <= EndDate && x.WeekStart >= StartDate);
                    EndWeek = EndWeek.Where(x => x.WeekEnd <= EndDate && x.WeekStart >= StartDate);

                }
                //    if (!WeekList.Any(x => x.WeekStart == StartDate) || !WeekList.Any(x => x.WeekEnd == EndDate))
                //{
                //    var parentWeekStart = WeekList.Where(x => x.WeekStart <= StartDate && x.WeekEnd >= EndDate).FirstOrDefault();
                //    var parentWeekEnd = WeekList.Where(x => x.WeekStart <= StartDate && x.WeekEnd >= EndDate).FirstOrDefault();

                //    if (parentWeekStart != null && parentWeekEnd != null)
                //    {
                //        StartWeek = StartWeek.Where(x => x.WeekEnd <= parentWeekEnd.WeekEnd && x.WeekStart >= parentWeekStart.WeekStart);
                //        EndWeek = EndWeek.Where(x => x.WeekEnd <= parentWeekEnd.WeekEnd && x.WeekStart >= parentWeekStart.WeekStart);
                //    }
                //    else if (parentWeekStart != null)
                //    {
                //        StartWeek = StartWeek.Where(x => x.WeekEnd <= EndDate && x.WeekStart >= parentWeekStart.WeekStart);
                //        EndWeek = EndWeek.Where(x => x.WeekEnd <= EndDate && x.WeekStart >= StartDate);
                //    }
                //    else if (parentWeekEnd != null)
                //    {
                //        EndWeek = EndWeek.Where(x => x.WeekEnd <= parentWeekEnd.WeekEnd && x.WeekStart >= parentWeekStart.WeekStart);
                //        StartWeek = StartWeek.Where(x => x.WeekEnd <= EndDate && x.WeekStart >= StartDate);
                //    }
                //    else
                //    {
                //        StartWeek = StartWeek.Where(x => x.WeekEnd <= EndDate && x.WeekStart >= StartDate);
                //        EndWeek = EndWeek.Where(x => x.WeekEnd <= EndDate && x.WeekStart >= StartDate);
                //    }

                //}
                
            }
            UnitMasterEdit model = new UnitMasterEdit();
            if (UnitId > 0)
            {
                model = _iUnitService.GetUnitMasterDetailsByUnitId(UnitId);
                if(!WeekList.Any(x=>x.WeekStart ==model.StartDate))
                {
                    var startWeek = WeekList.Where(x => x.WeekStart <= model.StartDate && x.WeekEnd >= model.StartDate).FirstOrDefault();
                    if(startWeek !=null)
                    model.StartDate = startWeek.WeekStart;
                }
                if (!WeekList.Any(x => x.WeekEnd == model.EndDate))
                {
                    var endWeek = WeekList.Where(x => x.WeekStart <= model.EndDate && x.WeekEnd >= model.EndDate).FirstOrDefault();
                    if (endWeek != null)
                        model.EndDate = endWeek.WeekEnd;
                }
            }
            model.CourseId = CourseId;
            model.ParentId = ParentId;
            ViewBag.StartWeek = StartWeek;
            ViewBag.EndWeek = EndWeek;
            ViewBag.order = order;
            return PartialView("_UnitMaster", model);
        }
        public PartialViewResult UnitCalendarGrid(long Id = 0, Int16 order = 1)
        {
            var model = _iUnitService.GetUnitCalendarByCourseId(SessionHelper.CurrentSession.SchoolId, Id);
            if (model != null)
            {
                if (model.UnitDetails != null)
                {
                    if (model.UnitDetails.Any())
                    {
                        foreach (var unit in model.UnitDetails)
                        {
                            unit.UnitInfoURL = "<a class='text-truncate' href='/course/unit/unitinfo?Id="
                                    + EncryptDecryptHelper.EncryptUrl(unit.UnitId)
                                    + "&courseId=" + EncryptDecryptHelper.EncryptUrl(Id)+ "&UnitTitle=" + unit.UnitName+ "'>" + unit.UnitName
                                    + "</a>";
                        }
                    }
                    model.UnitDetails = (order == 1 ? model.UnitDetails.OrderBy(u => u.StartDate) : model.UnitDetails.OrderByDescending(u => u.StartDate));
                }
            }
            return PartialView("_UnitCalendarGrid", model);
        }

        #region Assessment Evidence
        [HttpPost]
        public ActionResult SaveAssessmentEvidence(AssessmentEvidenceEdit model)
        {
            var userId = SessionHelper.CurrentSession.Id;

            var storeDirectory = Constants.UnitDetailsDir + "/School_" + SessionHelper.CurrentSession.SchoolId + "/" + model.UnitId;
            model.AttachmentList.RemoveAll(x => x == null);
            model.AttachmentList.AddRange(StorageHelper.SaveAllFileAttachment(Request.Files.GetMultiple("AttachmentList"), model.AttachmentKey, storeDirectory, model.UnitId));

            var result = _assessmentEvidenceService.SaveUpdateAssessmentEvidenceDetails(model, userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetAssessmentEvidenceList(long unitId,string removedIds="")
        {
            var list = _assessmentEvidenceService.GetAssessmentEvidenceList(unitId);
            ViewBag.RemovedIds = removedIds;
            return PartialView("_AssessmentEvidenceList", list);
        }
        public ActionResult AssessmentEvidenceList(long unitId,bool isExport =false)
        {
            ViewBag.Mode = "View";
            ViewBag.isExport = isExport;
            var list = _assessmentEvidenceService.GetAssessmentEvidenceList(unitId);
            return PartialView("_AssessmentEvidenceList", list);
        }
        public PartialViewResult GetAssessmentEvidencePopUpDetails(long UTM_ID, string id = "")
        {
            var model = new AssessmentEvidenceEdit();
            var userId = SessionHelper.CurrentSession.Id;
            if (string.IsNullOrEmpty(id))
            {
                model.IsAddMode = true;
            }
            else
            {
                var assessmentId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(id));
                var assessmentModel = _assessmentEvidenceService.GetAssessmentEvidenceDetailsById(assessmentId);
               model = EntityMapper<AssessmentEvidence, AssessmentEvidenceEdit>.Map(assessmentModel);
                model.IsAddMode = false;
            }
            model.UserId = userId;
            model.UnitId = UTM_ID;
            model.AttachmentKey = Phoenix.Models.Entities.AttachmentKey.AssessmentEvidence.EnumToString();

            return PartialView("_AssessmentEvidence", model);
        }
        #endregion
        
        #region assessment learning
        [HttpPost]
        public ActionResult SaveAssessmentLearning(AssessmentLearningEdit model)
        {
            var userId = SessionHelper.CurrentSession.Id;

            var storeDirectory = Constants.UnitDetailsDir + "/School_" + SessionHelper.CurrentSession.SchoolId + "/" + model.UnitId;
            model.AttachmentList.RemoveAll(x => x == null);
            model.AttachmentList.AddRange(StorageHelper.SaveAllFileAttachment(Request.Files.GetMultiple("AttachmentList"), model.AttachmentKey, storeDirectory, model.UnitId));

            var result = _assessmentLearningService.SaveUpdateAssessmentLearningDetails(model, userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetAssessmentLearningList(long unitId,string removedIds = "")
        {
            var list = _assessmentLearningService.GetAssessmentLearningList(unitId);
            ViewBag.RemovedIds = removedIds;
            return PartialView("_AssessmentLearningList", list);
        }
        public ActionResult AssessmentLearningList(long unitId,bool isExport=false)
        {
            ViewBag.Mode = "View";
            ViewBag.isExport = isExport;
            var list = _assessmentLearningService.GetAssessmentLearningList(unitId);
            return PartialView("_AssessmentLearningList", list);
        }
        public PartialViewResult GetAssessmentLearningPopUpDetails(long UTM_ID, string id = "")
        {
            var model = new AssessmentLearningEdit();
            var userId = SessionHelper.CurrentSession.Id;
            if (string.IsNullOrEmpty(id))
            {
                model.IsAddMode = true;
            }
            else
            {
                var assessmentId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(id));
                var assessmentModel = _assessmentLearningService.GetAssessmentLearningDetailsById(assessmentId);
                model = EntityMapper<AssessmentLearning, AssessmentLearningEdit>.Map(assessmentModel);
                model.IsAddMode = false;
            }
            model.UserId = userId;
            model.UnitId = UTM_ID;
            model.AttachmentKey = Phoenix.Models.Entities.AttachmentKey.AssessmentLearning.EnumToString();

            return PartialView("_AssessmentLearning", model);
        }
        #endregion

        #region standardbank

        public PartialViewResult GetStandardPopUpDetails(long UTM_ID)
        {
            var model = new StandardEdit();
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            IEnumerable<Standard> result = _standardService.GetStandardGroupName(UTM_ID, schoolId);
            ViewBag.StandardList = result;
            ViewBag.UTM_ID = UTM_ID;
            return PartialView("_Standard", model);
        }
        public ActionResult GetUnits(long GroupId = 0)
        {
            var result = new SelectList(_iUnitService.GetUnitsByGroup(GroupId), "UnitId", "UnitName");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSubUnits(long GroupId = 0)
        {
            var result = new SelectList(_iUnitService.GetSubUnitsById(GroupId), "UnitId", "UnitName");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetUnitStandardList(long UnitMasterId,long GroupId, long UnitId, long StandardBankId,int PageNumber=1,string SearchString="")
        {
            var SchoolId = SessionHelper.CurrentSession.SchoolId;
            var result = _iUnitService.GetUnitTopicStandardDetails(UnitMasterId, GroupId,  UnitId, StandardBankId,PageNumber, SearchString, SchoolId);
            var ParentList = new List<UnitTopicStandardDetails>();
            if (result != null && result.Count() > 0)
            {
                ParentList = result.DistinctBy(x => x.PARENT_TOPIC_DESCRIPTION).ToList();
            }
            else
                return Content(string.Empty);
            ViewBag.ParentList = ParentList;
            return PartialView("_UnitTopicList", result);
        }
        public PartialViewResult GetSelectedStandardDetails(List<TopicTreeView> standarddetails, int AttachmentCount = 0)
        {
            var list = new List<TopicTreeView>();
            if (standarddetails != null)
            {
                list = standarddetails.DistinctBy(x => x.maintopic).ToList();
            }
            ViewBag.MainTopicList = list;
            ViewBag.Mode = "Edit";
            ViewBag.AttachmentCount = AttachmentCount;
            return PartialView("_SelectedStandardDetails", standarddetails);
        }

        #endregion
    }
}