﻿using NPOI.SS.UserModel;
using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Course.Controllers
{
    /// <summary>
    /// author       : syed
    /// created date : 15 May 2020
    /// Description  : to insert or list standard master details to UI
    /// </summary>
    public class StandardController : BaseController
    {
        // GET: Course/Standard

        private IStandardService _standardService;
        private ISIMSCommonService _SIMSCommonService;

        public StandardController(IStandardService standardService, ISIMSCommonService SIMSCommonService)
        {
            _standardService = standardService;
            _SIMSCommonService = SIMSCommonService;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddEditStandard(Int64? StandardId)
        {
            var model = new StandardEdit();
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;
            IEnumerable<Standard> ParentTopic = _standardService.GetStandardMasterList(Convert.ToInt32(itemId));
            IEnumerable<SchoolTermList> lstSchoolTerm = new List<SchoolTermList>();
           

            ViewBag.TeacherCourse = new SelectList(_SIMSCommonService.GetTeacherCourse(SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId), "COR_ID", "COR_DESCR");
            if (StandardId.HasValue)
            {
                Standard standard = new Standard();
                standard = _standardService.GetStandardMasterDetailsById(StandardId);
                EntityMapper<Standard, StandardEdit>.Map(standard, model);
                model.IsAddMode = false;
            }
            else
            {
                model.IsAddMode = true;
            }
            ViewBag.ParentTopic = ParentTopic;
            return PartialView("_AddEditStandard", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveStandardDetails(StandardEdit model)
        {
            var result = _standardService.SaveUpdateStandardMasterDetails(model);
            return Json(new OperationDetails(result.Success), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStandardMasterList()
        {
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;
            var lstStandard = _standardService.GetStandardMasterList(Convert.ToInt32(itemId));

            var actionButtonsHtmlTemplate = "<div class='tbl-actions'><a  class='table-action-text-link clsEditUnit' onclick='Standard.editStandard($(this),{0})'><i class='fas fa-pencil-alt mr-2'></i></a>" + "&nbsp;&nbsp;&nbsp;&nbsp;" +
                                              "<a  class='table-action-text-link clsEditUnit' title='Add Standard' href='/Course/StandardDetails/Index?StandardId={1}&CourseName={2}'><i class='fas fa-plus'></i></a>" +
                                            "</div>";
            //var ButtonForStandardDetails = "<div class='tbl-actions'><button type='button' class='btn btn-sm btn-primary waves-effect waves-light' onclick='Standard.LinkForStandardDetails($(this),{0})'>Add Standard</button>" +
            //                                "</div>";
            var dataList = new object();

            dataList = new
            {
                aaData = (from item in lstStandard
                          select new
                          {
                              Actions = CurrentPagePermission.CanEdit ? string.Format(actionButtonsHtmlTemplate, item.StandardID, EncryptDecryptHelper.EncryptUrl(item.StandardID), item.CourseName) : string.Empty,

                              ////ActionsForLink = string.Format(ButtonForStandardDetails, item.StandardID),
                              //ActionsForLink = "<div class='tbl-actions'><a style='color:#fff' class='btn btn-sm btn-primary waves-effect waves-light' href='/Course/StandardDetails/Index?StandardId="
                              //      + EncryptDecryptHelper.EncryptUrl(item.StandardID)
                              //      + "'>" + "Add Standard"
                              //      + "</div>",
                              //ActionsForLink = "<a href='/course/unit/unitinfo?Id=" + EncryptDecryptHelper.EncryptUrl(item.StandardID) "<a/>"
                              item.Description,
                              item.CourseName
                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteStandardMaster(int StandardID)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var standard = _standardService.GetStandardMasterDetailsById(StandardID);
            var result = _standardService.DeleteStandardMaster(standard);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Upload Standard
        public ActionResult UploadStandard()
        {
            ViewBag.CourseList = new SelectList(SelectListHelper.GetSelectListData(ListItems.Course, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            return View();
        }
        [HttpPost]
        public ActionResult ImportDataFromExcel(HttpPostedFileBase ImportExcelFile)
        {
            string fname = ImportExcelFile.FileName;
            var contentLength = ImportExcelFile.ContentLength;
            var extension = System.IO.Path.GetExtension(ImportExcelFile.FileName);
            fname = Guid.NewGuid() + "_" + fname;
            string ObjectiveExcelFileName = fname;
            string serverPath = Server.MapPath("~/Resources/Standard/StandardUpload/");
            fname = Path.Combine(serverPath, fname);
            if (Directory.Exists(serverPath))
                Directory.Delete(serverPath, true);
            Directory.CreateDirectory(serverPath);
            ImportExcelFile.SaveAs(fname);

            StandardExcelUploadData standardExcelUploadData = new StandardExcelUploadData();
            if (System.IO.File.Exists(fname))
            {
                var sh = fname.GetFileStream();
                var headerRow = sh.GetRow(0);
                int colCount = headerRow.LastCellNum;
                if (colCount == 9)
                {
                    List<int> numericColumnIndexList = new List<int>();
                    numericColumnIndexList.Add(5);
                    numericColumnIndexList.Add(6);
                    numericColumnIndexList.Add(7);
                    numericColumnIndexList.Add(8);
                    standardExcelUploadData.ExcelData = ConvertExcelToDataTable(sh, numericColumnIndexList);
                    standardExcelUploadData.IsColumnMatch = true;
                }
                else
                {
                    standardExcelUploadData.ExcelData = new DataTable();
                    standardExcelUploadData.IsColumnMatch = false;
                }
            }
            return PartialView("_LoadStandardExcel", standardExcelUploadData);
        }
        public static DataTable ConvertExcelToDataTable(ISheet sh, List<int> numericColumnIndexList)
        {
            try
            {
                var dtExcelTable = new DataTable();
                dtExcelTable.Rows.Clear();
                dtExcelTable.Columns.Clear();
                var headerRow = sh.GetRow(0);
                int colCount = headerRow.LastCellNum;
                for (var c = 0; c < colCount; c++)
                    dtExcelTable.Columns.Add(headerRow.GetCell(c).ToString());

                dtExcelTable.Columns.Add("IsError", System.Type.GetType("System.Boolean"));

                var i = 1;
                var currentRow = sh.GetRow(i);
                while (currentRow != null)
                {
                    var dr = dtExcelTable.NewRow();
                    dr["IsError"] = false;
                    for (var j = 0; j < ((NPOI.HSSF.UserModel.HSSFRow)currentRow).RowRecord.LastCol; j++)
                    {
                        var cell = currentRow.GetCell(j);

                        try
                        {
                            if (cell != null)
                                switch (cell.CellType)
                                {
                                    case CellType.Numeric:
                                        dr[j] = DateUtil.IsCellDateFormatted(cell)
                                            ? cell.DateCellValue.ToString("dd/MMM/yyyy")
                                            : cell.NumericCellValue.ToString(CultureInfo.InvariantCulture);
                                        break;
                                    case CellType.String:
                                        if (numericColumnIndexList.IndexOf(j) == -1)
                                        {
                                            dr[j] = cell.StringCellValue;
                                        }
                                        else
                                        {
                                            dr[j] = string.Empty;
                                            dr["IsError"] = true;
                                        }

                                        break;
                                    case CellType.Blank:
                                        dr[j] = string.Empty;
                                        break;
                                }
                        }
                        catch (Exception)
                        {
                            dr["IsError"] = true;
                        }
                    }
                    dtExcelTable.Rows.Add(dr);
                    i++;
                    currentRow = sh.GetRow(i);
                }
                return dtExcelTable;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public ActionResult SaveImportedExcelData(StandardUploadModel standardUploadModel)
        {
            standardUploadModel.SchoolId = SessionHelper.CurrentSession.SchoolId; 
            standardUploadModel.UserId = SessionHelper.CurrentSession.Id;
            var result = _standardService.BulkStandardUpload(standardUploadModel);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public FileResult DownloadObjectiveExcelTemplate()
        {
            string fileName = "StandardUploadTemplate.xls";
            string filePath = Path.Combine(Server.MapPath("~/Content/Files"), fileName);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        
        #endregion

        #region Upload Standard Bank
        public ActionResult UploadStandardBank()
        {
            return View();
        }

        public FileResult DownloadStandardBankExcelTemplate()
        {
            string fileName = "StandardBankUploadTemplate.xlsx";
            string filePath = Path.Combine(Server.MapPath("~/Content/Files"), fileName);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult ImportStandardBankDataFromExcel(HttpPostedFileBase ImportExcelFile)
        {
            string fname = ImportExcelFile.FileName;
            var contentLength = ImportExcelFile.ContentLength;
            var extension = System.IO.Path.GetExtension(ImportExcelFile.FileName);
            fname = Guid.NewGuid() + "_" + fname;
            string ObjectiveExcelFileName = fname;
            string serverPath = Server.MapPath("~/Resources/Standard/StandardBankUpload/");
            fname = Path.Combine(serverPath, fname);
            if (Directory.Exists(serverPath))
                Directory.Delete(serverPath, true);
            Directory.CreateDirectory(serverPath);
            ImportExcelFile.SaveAs(fname);

            StandardBankExcelUploadData standardExcelUploadData = new StandardBankExcelUploadData();
            if (System.IO.File.Exists(fname))
            {
                var sh = fname.GetFileStream();
                var headerRow = sh.GetRow(0);
                int colCount = headerRow.LastCellNum;
                if (colCount == 5)
                {
                    List<int> numericColumnIndexList = new List<int>();
                   
                    standardExcelUploadData.ExcelData = ConvertStandardBankExcelToDataTable(sh, numericColumnIndexList);
                    standardExcelUploadData.IsColumnMatch = true;
                }
                else
                {
                    standardExcelUploadData.ExcelData = new DataTable();
                    standardExcelUploadData.IsColumnMatch = false;
                }
            }
            return PartialView("_LoadStandardBankExcel", standardExcelUploadData);
        }
        [HttpPost]
        public ActionResult SaveImportedStandardBankExcelData(List<StandardBankExcelModel> standardUploadModel)
        {
            StandardBankUploadModel model = new StandardBankUploadModel();
            model.StandardBankExcelList.AddRange(standardUploadModel);
            model.SchoolId = SessionHelper.CurrentSession.SchoolId;
           // standardUploadModel.UserId = SessionHelper.CurrentSession.Id;
            var result = _standardService.BulkStandardBankUpload(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public static DataTable ConvertStandardBankExcelToDataTable(ISheet sh, List<int> numericColumnIndexList)
        {
            try
            {
                var dtExcelTable = new DataTable();
                dtExcelTable.Rows.Clear();
                dtExcelTable.Columns.Clear();
                var headerRow = sh.GetRow(0);
                int colCount = headerRow.LastCellNum;
                for (var c = 0; c < colCount; c++)
                    dtExcelTable.Columns.Add(headerRow.GetCell(c).ToString());

                dtExcelTable.Columns.Add("IsError", System.Type.GetType("System.Boolean"));

                var i = 1;
                var currentRow = sh.GetRow(i);
                while (currentRow != null)
                {
                    var dr = dtExcelTable.NewRow();
                    dr["IsError"] = false;
                    for (var j = 0; j < colCount; j++)
                    {
                        var cell = currentRow.GetCell(j);

                        try
                        {
                            if (cell != null)
                                switch (cell.CellType)
                                {
                                    case CellType.Numeric:
                                        dr[j] = DateUtil.IsCellDateFormatted(cell)
                                            ? cell.DateCellValue.ToString("dd/MMM/yyyy")
                                            : cell.NumericCellValue.ToString(CultureInfo.InvariantCulture);
                                        break;
                                    case CellType.String:
                                        if (numericColumnIndexList.IndexOf(j) == -1)
                                        {
                                            dr[j] = cell.StringCellValue;
                                        }
                                        else
                                        {
                                            dr[j] = string.Empty;
                                            dr["IsError"] = true;
                                        }

                                        break;
                                    case CellType.Blank:
                                        dr[j] = string.Empty;
                                        break;
                                }
                        }
                        catch (Exception)
                        {
                            dr["IsError"] = true;
                        }
                    }
                    dtExcelTable.Rows.Add(dr);
                    i++;
                    currentRow = sh.GetRow(i);
                }
                return dtExcelTable;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult StandardBank()
        {

            return View();
        }
        public PartialViewResult GetStandardBankList()
        {
            //var result = new List<StandardBankExcelModel>();
           // result.Add(new StandardBankExcelModel {SB_Id =1, GroupName= "Common Core - English",UnitName="Reading Literature",SubUnitName="Craft and Structure",StandardDescription="Determine the meaning"});
            //result.Add(new StandardBankExcelModel { SB_Id = 2, GroupName = "Common Core - English", UnitName = "Reading Literature", SubUnitName = "Craft and Structure", StandardDescription = "Analyze how a particular sentence, chapter, scene, or stanza fits into the overall structure of a text and contributes to the development of the theme, setting, or plot." });
            var result  = _standardService.GetStandardBankList(Convert.ToInt64(SessionHelper.CurrentSession.SchoolId));
            var list = new List<StandardBankExcelModel>();
            if (result != null)
            {
                list = result.DistinctBy(x => x.GroupName).ToList();
            }
            ViewBag.ParentList = list;

            return PartialView("_StandardBankList", result);
        }

        public ActionResult AddEditStandardBank(Int64 StandardBankId, Int64 AddStaus,string editType)
        {
            var model = _standardService.GetStandardBankList(Convert.ToInt64(SessionHelper.CurrentSession.SchoolId), StandardBankId).FirstOrDefault();
            ViewBag.AddStaus = AddStaus;
            ViewBag.editType = editType;
            return PartialView("_EditStandardBank",model);
        }
        [HttpPost]
        public ActionResult AddEditStandardBank(StandardBankExcelModel standardModel,string TransMode,string EditType)
        {
            var Result = _standardService.AddEditStandardBank(TransMode, EditType,standardModel);
            return Json(new OperationDetails(Result), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}