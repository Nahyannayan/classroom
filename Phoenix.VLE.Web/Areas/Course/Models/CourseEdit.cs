﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Models;
using Phoenix.Common.CustomAttributes;
namespace Phoenix.VLE.Web.Areas.Course.Models
{
    [ResourceMappingRoot(Path = "Course.Course")]
    public class CourseEdit : Attachments
    {
        public long CourseId { get; set; }
        public long AcademicYearId { get; set; }
        public long SchoolId { get; set; }
        public long CurriculumId { get; set; }
        //[Remote("IsCourseTitleUnique", "course", AreaReference.UseCurrent, ErrorMessage = "Title already exists", AdditionalFields = "PreviousTitle")]
        public string Title { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public string CourseType { get; set; }
        public long DepartmentId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long CreatedBy { get; set; }
        public string EditURL { get; set; }
        public string ViewCourseGroups { get; set; }
        public string UnitCalendarURL { get; set; }
        public int TotalCount { get; set; }
        public string CourseImageName { get; set; }
        public HttpPostedFileBase CourseImage { get; set; }
        public int MaximumEnrollment { get; set; }
        public double? CourseHours { get; set; }

        //public long[] Groupids { get; set; }
        //public long[] StudentId { get; set; }
        //public long[] TeacherId { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }
    }
    [ResourceMappingRoot(Path = "Course.MapCourse")]
    public class CourseMappingEdit
    {
        public CourseMappingEdit()
        {
            Students = new List<CourseStudentAndGrade>();
            GroupTeachers = new List<GroupTeacher>();
            GroupGrades = new List<GroupGrade>();
            GroupAssignTeachers = new List<GroupAssignTeacherDuration>();
        }
        public long CourseId { get; set; }
        public string CourseTitle { get; set; }
        public long[] TeacherIds { get; set; }
        public string TeacherName { get; set; }
        public long SchoolGroupId { get; set; }
        public long[] GradeIds { get; set; }
        public string SchoolGroupName { get; set; }
        public int CourseType { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public IEnumerable<CourseStudentAndGrade> Students { get; set; }
        public IEnumerable<GroupTeacher> GroupTeachers { get; set; }
        public IEnumerable<GroupGrade> GroupGrades { get; set; }
        public IEnumerable<GroupAssignTeacherDuration> GroupAssignTeachers { get; set; }
        public int TotalCount { get; set; }
        public string EditUrl { get; set; }
    }
}