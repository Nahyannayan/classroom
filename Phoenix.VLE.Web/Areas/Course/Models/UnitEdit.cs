﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.CustomAttributes;
using Phoenix.Models.Entities;
namespace Phoenix.VLE.Web.Areas.Course.Models
{
    [ResourceMappingRoot(Path = "Course.Unit")]
    public class UnitMasterEdit
    {
        public long UnitId { get; set; }
        public long CourseId { get; set; }
        public string UnitName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string UnitColorCode { get; set; }

        public string CourseName { get; set; }
        public string UnitInfoURL { get; set; }
        public string StartWeek { get; set; }
        public string EndWeek { get; set; }

        public long ParentId { get; set; }
    }

    public class UnitDetailsTypeEdit: Attachments
    {
        public long UnitId { get; set; }
        public long UnitTypeId { get; set; }
        public long UnitDetailsId { get; set; }
        public string Title { get; set; }
        public string ControlType { get; set; }
        public string GroupName { get; set; }
        public int TypeOrder { get; set; }
        public bool HasAttachment { get; set; }
        public string AttachmentKey { get; set; }        
        public string Description { get; set; }
        public long StandardIds { get; set; }
        [AllowHtml]
        public string ContentDescription { get; set; }
        public string lstOfSelectedStandardIds { get; set; }
        public bool IsRequired { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public HttpPostedFileBase File { get; set; }

    }

    public class UnitCalendarEdit
    {
        public int Acd_Id { get; set; }
        public string YearStartDate { get; set; }
        public string YearEndDate { get; set; }
        public IEnumerable<UnitMasterEdit> UnitDetails { get; set; }
        public CourseEdit CourseDetails { get; set; }
        public IEnumerable<UnitWeekEdit> WeekList { get; set; }
    }
    public class UnitWeekEdit
    {
        public int Id { get; set; }
        public DateTime WeekStart { get; set; }
        public DateTime WeekEnd { get; set; }
        public string Description { get; set; }
    }


}