﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Areas.Course.Models
{
    public class Lesson
    {
        public int TransMode { get; set; }
        public long LessonId { get; set; }
        public string UnitName { get; set; }
        public string StandardName { get; set; }
        public string StandardId { get; set; }
        public long UnitId { get; set; }
        public bool IsAddMode { get; set; }
        public string Title { get; set; }

        public string Details { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int LSP_ID { get; set; }

        public string StandardCode { get; set; }

    }

    public class UnitDetails
    {
        public long UnitId { get; set; }
        public string UnitName { get; set; }
    }
    public class StandardDetailsValue
    {
        public long StandardId { get; set; }
        public string StandardName { get; set; }
    }
    public class StandardDetailsList
    {
        public long StandardId { get; set; }
        public string StandardName { get; set; }
    }
    public class SchoolUnitDetailTypeMapping : UnitDetails
    {
        public long SchoolId { get; set; }
        public int UnitDetailTypeId { get; set; }
        public string UDT_GROUP_NAME { get; set; }
        public int[] UnitDetailsTypeIds { get; set; }
        public int DivisionId { get; set; }
    }
    [ResourceMappingRoot(Path = "Course.UnitDetailConfig")]
    public class SchoolUnitDetailType
    {
        public int? UnitDetailTypeId { get; set; }
        public string Title { get; set; }
        public string ControlType { get; set; }
        public int UnitGroupId { get; set; }
        public string UnitGroupName { get; set; }
        public bool AttachmentRequired { get; set; }
        public string AttachmentKey { get; set; }
        public string Description { get; set; }
        public long SchoolId { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
        public bool RecordExists { get; set; }
        public List<long> UnitCourseId { get; set; }
        public string CourseIds { get; set; }
        public string CourseTitle { get; set; }
    }
}