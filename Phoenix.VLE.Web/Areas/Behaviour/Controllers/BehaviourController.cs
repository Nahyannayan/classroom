﻿using Phoenix.Common.Helpers;
using SMS.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using SMS.Web.Areas.SMS.ViewModels;
using System.Net;
using System.IO.Compression;
using System.Text;
using System.Globalization;
using Newtonsoft.Json;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models.Entities;
using Phoenix.Common.Localization;
using DevExpress.DataProcessing;
using Phoenix.Common.Enums;
using SMS.Web.Areas.SMS.Model;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharp;
using PdfSharp.Pdf;
using Phoenix.Common.ViewModels;

namespace Phoenix.VLE.Web.Areas.Behaviour.Controllers
{
    [Authorize]
    public class BehaviourController : BaseController
    {

        private IBehaviourService _BehaviourService;
        private readonly ICommonService commonService;
        private readonly ISIMSCommonService _SIMSCommonService;
        private readonly IBehaviourSetupService behaviourSetupService;
        private readonly ITemplateService templateService;
        private const string Add = "Add";
        private const string Edit = "Edit";
        private const string Delete = "Delete";
        public BehaviourController(IBehaviourService BehaviourService, ISIMSCommonService _SIMSCommonService,
            ICommonService commonService, IBehaviourSetupService behaviourSetupService, ITemplateService templateService)
        {
            _BehaviourService = BehaviourService;
            this.commonService = commonService;
            this._SIMSCommonService = _SIMSCommonService;
            this.behaviourSetupService = behaviourSetupService;
            this.templateService = templateService;
        }

        // GET: SIMS/Behaviour
        public ActionResult Index()
        {
            return RedirectToAction("StudentBehaviour");
        }
        public JsonResult GetCourseGroupByCourseId(long courseid)
        {
            var list = SelectListHelper.GetSelectListData(ListItems.CourseGroupList, $"{courseid},{SessionHelper.CurrentSession.Id}");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FetchSectionListByGrade(string GradeId)
        {
            var list = new SelectList(SelectListHelper.GetSelectListData(ListItems.Section, $"{GradeId}"), "ItemId", "ItemName");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchSubCategory(int id)
        {
            //var Userdetail = Helpers.CommonHelper.GetLoginUser();
            //var schoolid = Userdetail.SchoolId;
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var SubCategory = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourSubCategory, string.Format("{0},{1}", id, schoolid)), "ItemId", "ItemName");
            return Json(SubCategory, JsonRequestBehavior.AllowGet);
        }
        public ViewResult LoadBehaviour(int tt_id = 0, string grade = null, string section = null, string stu_id = null)
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, SessionHelper.CurrentSession.ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");
            var BehaviourVM = new BehaviourVM();
            // BehaviourVM.classList = _BehaviourService.GetStudentList(username, tt_id, grade, section);
            BehaviourVM.behaviour = _BehaviourService.LoadBehaviourByStudentId(stu_id);
            return View("Behaviour", BehaviourVM);
        }
        public ActionResult GetStudentList(int tt_id = 0, string grade = null, string section = null, string stu_id = null, int IstudentBehaviour = 0, long GroupId = 0, bool IsFilterByGroup = false)
        {
            var username = SessionHelper.CurrentSession.UserName;
            var list = SelectListHelper.GetSelectListData(ListItems.SchoolGrade, $"{SessionHelper.CurrentSession.SchoolId}");
            ViewBag.Grades = list;
            var BehaviourVM = new BehaviourVM();

            if (IstudentBehaviour == 1)
            {
                BehaviourVM.classList = _BehaviourService.GetBehaviourClassList(username, tt_id, grade, section, GroupId, IsFilterByGroup);
                ViewBag.Category = new SelectList(string.Empty);// new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");
                BehaviourVM.classList.ForEach(x =>
                {
                    if (x.IncidentDate != null)
                    {
                        x.IncidentDate = Convert.ToDateTime(x.IncidentDate.Value).ConvertUtcToLocalTime();
                    }
                    if (x.ModifiedOn != null)
                    {
                        x.ModifiedOn = Convert.ToDateTime(x.ModifiedOn.Value).ConvertUtcToLocalTime();
                    }
                });
                //BehaviourVM.classList.ForEach(x => x.IncidentDate = Convert.ToDateTime(x.IncidentDate).ConvertUtcToLocalTime());
                //var result = new { Result = ConvertViewToString("_StudentBehaviourList", BehaviourVM) };
                return PartialView("_StudentBehaviourList", BehaviourVM);
                //return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //BehaviourVM.classList = _BehaviourService.GetStudentList(username, tt_id, grade, section);
                BehaviourVM.behaviour = _BehaviourService.LoadBehaviourByStudentId(stu_id);
                return PartialView("_BehaviourBodyPartial", BehaviourVM);

            }

        }
        public ActionResult GetStudentListBygGroupId(long groupId)
        {
            var behaviour = _BehaviourService.GetBehaviourStudentListByGroupId(groupId);
            behaviour.ForEach(x => x.IncidentDate = Convert.ToDateTime(x.IncidentDate).ConvertUtcToLocalTime());
            return PartialView("_StudentBehaviourList", new BehaviourVM { classList = behaviour });
        }
        public ActionResult LoadBehaviourHistoryGrid(string stu_id)
        {

            var behaviourHistory = _BehaviourService.LoadBehaviourByStudentId(stu_id);
            var dataList = new object();
            string editbutton = "<a   onclick='" +
                "editBehaviour($(this),{0})'><i class='fa fa-pen'></i></a>";

            dataList = new
            {
                aaData = (from item in behaviourHistory
                          select new
                          {

                              item.Student_Name,
                              item.Type,
                              item.Comments,
                              item.Recorded_by,
                              item.Recorded_Date,
                              editbutton = string.Format(editbutton, item.Behaviour_ID),
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditBehaviourForm(int? id)
        {

            var schoolid = SessionHelper.CurrentSession.SchoolId;
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");

            //List<SelectListItem> priorityli = new List<SelectListItem>();
            //priorityli.Add(new SelectListItem { Text = "High", Value = "High" });
            //priorityli.Add(new SelectListItem { Text = "Medium", Value = "Medium" });
            //priorityli.Add(new SelectListItem { Text = "Low", Value = "Low" });
            //ViewBag.PriorityEdit = priorityli;
            var model = new BehaviourVM();
            if (id.HasValue)
            {
                var behaviourDetails = _BehaviourService.GetBehaviourById(id.Value);
                //var _tmp=  model.behaviourDetails.FirstOrDefault();
                //_tmp.Recorded_By
                ////EntityMapper<BehaviourDetails, BehaviourVM>.Map(category, model);
                //model.behaviourDetails.FirstOrDefault().Recorded_By = category.UserId.ToString();
                //var duedate = Convert.ToDateTime(category.EndDate).ToString("dd/MM/yyyy");
                //model.EndDate = duedate;
            }

            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, SessionHelper.CurrentSession.ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            //ViewBag.IncidentGrades = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.Grade, SessionHelper.CurrentSession.ACD_ID), "ItemId", "ItemName");
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null).Where(e => e.ItemName == "Negative Behavior"), "ItemId", "ItemName");

            return PartialView("_AddEditBehaviourPartial", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveBehaviourDetail(BehaviourVM BehaviourVM, string listOfStudentIds, string[] ddlOtherStudentList, string[] ddlwitnessStudentList, string[] ddlwitnessstaffstring, string txtWitnessComment)
        {
            string StudentIds = string.Empty;
            string otherStudentIds = string.Empty;
            string WitnessStudentIds = string.Empty;
            string otherwithnessstaff = string.Empty;
            if (ddlOtherStudentList != null)
            {
                otherStudentIds = string.Join(",", ddlOtherStudentList);
            }
            if (ddlwitnessStudentList != null)
            {
                WitnessStudentIds = string.Join(",", ddlwitnessStudentList);
            }
            if (ddlwitnessstaffstring != null)
            {
                otherwithnessstaff = string.Join(",", ddlwitnessstaffstring);
            }
            var behaviourDetails = BehaviourVM.behaviourDetails;
            //  var Userdetail = SMS.Web.Helpers.CommonHelper.GetLoginUser();
            int Userid = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var result = false;
            string relativePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {

                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;
                        var extension = System.IO.Path.GetExtension(file.FileName);

                        //content for file upload
                        string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                        string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.StudentBehaviour;
                        //string fileModule = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFilesDir;
                        string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.StudentBehaviour + "/User_" + SessionHelper.CurrentSession.Id;

                        Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                        Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);

                        Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                        fname = Guid.NewGuid() + "_" + fname;
                        relativePath = Constants.AssignmentFilesDir + "/User_" + SessionHelper.CurrentSession.Id + "/" + fname;
                        fname = Path.Combine(filePath, fname);
                        file.SaveAs(fname);
                        behaviourDetails.DocPath = relativePath;
                        behaviourDetails.FileName = file.FileName;
                    }



                }
                catch (Exception e)
                {



                }




            }
            behaviourDetails.otherStudentEnvolved = otherStudentIds;
            behaviourDetails.WitnessStaffIds = otherwithnessstaff;
            behaviourDetails.WitnessStudentIds = WitnessStudentIds;
            behaviourDetails.WitnessStatement = txtWitnessComment;
            behaviourDetails.Recorded_Date = DateTime.Now;
            behaviourDetails.Followup_Date = DateTime.Now;
            result = _BehaviourService.InsertBehaviourDetails(behaviourDetails, schoolid.ToString());


            //behaviourDetails.Recorded_Date = DateTime.Now;
            //behaviourDetails.Incident_Date = DateTime.Now;
            //behaviourDetails.Followup_Date = DateTime.Now;



            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetStudentByGrade(string grade = "0", string section = "0", bool concatStudentNo = false)
        {
            var currentUser = Helpers.CommonHelper.GetLoginUser();
            var username = currentUser.UserName;

            var studentDetails = _BehaviourService.GetStudentList(username, 0, grade, section).ToList();
            List<SelectListItem> studentSelectList = studentDetails.ConvertAll(a =>
            {
                return new SelectListItem()
                {
                    Text = a.Student_Name.ToString() + (concatStudentNo ? $" - ({a.Student_No})" : string.Empty),
                    Value = a.Student_ID,
                    Selected = false
                };
            });
            return Json(studentSelectList, JsonRequestBehavior.AllowGet);
        }

        //Added studentbehaviour region for add edit behaviour of particular  student 
        #region StudentBehaviour
        public ActionResult StudentBehaviour()
        {
            BehaviourVM objBehaviourVM = new BehaviourVM();
            var TeacherId = SessionHelper.CurrentSession.UserTypeId == 5 ? 0 : SessionHelper.CurrentSession.Id;
            var SchoolId = SessionHelper.CurrentSession.SchoolId;
            var list = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolGrade, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.GradeList = list;
            ViewBag.CourseList = new SelectList(string.Empty);
            ViewBag.Curriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.CourseGroupList = new SelectList(_BehaviourService.GetCourseGroupList(TeacherId, SchoolId), "SchoolGroupId", "SchoolGroupName");
            if (SessionHelper.CurrentSession.IsTeacher())
            {
                ViewBag.CourseList = new SelectList(SelectListHelper.GetSelectListData(ListItems.CoursesList, $"{SessionHelper.CurrentSession.Id}"), "ItemId", "ItemName");
            }
            var strtt_id = "";
            if (Session["SIMSTimeTableSchedule"] != null)
            {
                strtt_id = Convert.ToString(Session["SIMSTimeTableSchedule"]);
            }
            if (strtt_id != "0" && !string.IsNullOrWhiteSpace(strtt_id))
            {
                return RedirectToAction("LoadStudentBehaviour", "Behaviour", new { strtt_id = strtt_id });
            }
            else
                return View("StudentBehaviourIndex", objBehaviourVM);
        }
        [HttpPost]
        public ActionResult SelectedBehaviour(int studentId = 0)
        {
            StudentBehaviourVM objStudentBehavior = new StudentBehaviourVM();
            objStudentBehavior.BehaviourList = _BehaviourService.GetStudentBehaviorByStudentId(studentId).ToList();
            objStudentBehavior.IsSelectMode = true;
            ///var result = new { Result = ConvertViewToString("_StudentBehaviourModelList", objStudentBehavior) };

            //return Json(result, JsonRequestBehavior.AllowGet);
            return PartialView("_StudentBehaviourModelList", objStudentBehavior);
        }
        public ActionResult AddEditStudentBehaviour(long StudentId = 0, int BehaviourId = 0, string txtAreaBehaviourComment = "")
        {
            var a = Request.Files.Count;
            var fileName = Request.Files[0].FileName;
            string relativePath = string.Empty;
            StringBuilder sb = new StringBuilder();
            string docpath = string.Empty;
            string FileName = string.Empty;
            List<StudentBehaviourFiles> objFiles = new List<StudentBehaviourFiles>();
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;
                        var contentLength = file.ContentLength;
                        var extension = System.IO.Path.GetExtension(file.FileName);

                        if (contentLength != 0)
                        {


                            //HttpPostedFileBase file = files[i];
                            //string fname = file.FileName;
                            //var extension = System.IO.Path.GetExtension(file.FileName);

                            //content for file upload
                            string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                            string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.StudentBehaviour;
                            //string fileModule = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFilesDir;
                            string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.StudentBehaviour + "/User_" + SessionHelper.CurrentSession.Id;

                            Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                            Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);

                            Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                            fname = Guid.NewGuid() + "_" + fname;
                            relativePath = Constants.StudentBehaviour + "/User_" + SessionHelper.CurrentSession.Id + "/" + fname;
                            fname = Path.Combine(filePath, fname);
                            file.SaveAs(fname);
                            docpath = relativePath;
                            sb.Append(relativePath);
                            FileName = file.FileName;
                            objFiles.Add(new StudentBehaviourFiles { StudentId = StudentId, BehaviourId = BehaviourId, FileName = FileName, UploadedFilePath = docpath });
                        }
                    }



                }
                catch (Exception e) { }
            }
            var isActionperformed = _BehaviourService.InsertUpdateStudentBehavior(objFiles, StudentId, BehaviourId, txtAreaBehaviourComment);
            // var result = _BehaviourService.InsertBulkStudentBehaviour(studentId.ToString(), behaviourId);
            StudentBehaviourVM objStudentBehavior = new StudentBehaviourVM();
            if (isActionperformed)
            {

                return Json(new OperationDetails(isActionperformed), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new OperationDetails(isActionperformed), JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult StudentBehaviourList(int categoryId = 0)
        {
            categoryId = categoryId == 0 ? 1 : categoryId;
            StudentBehaviourVM obj = new StudentBehaviourVM();
            obj.BehaviourList = _BehaviourService.GetListOfStudentBehaviour().Where(e => e.IsActive == 1 && e.SchoolId == SessionHelper.CurrentSession.SchoolId && e.CategoryId == categoryId).ToList();
            var ItemName = SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null).Where(e => e.ItemId == categoryId.ToString()).Select(e => e.ItemName).FirstOrDefault();
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");
            ViewBag.ItemName = ItemName;
            obj.CategoryId = categoryId;
            return PartialView("_StudentBehaviourModelList", obj);
        }
        public ActionResult GetSelectedBehaviour(int behaviourId = 0)
        {
            return View();
        }
        [HttpPost]
        public ActionResult BulkInsert(string[] lstofstudentId, int BehaviourId = 0, string txtAreaBehaviourComment = "")
        {
            string relativePath = string.Empty;
            string docpath = string.Empty;
            string FileName = string.Empty;
            List<StudentBehaviourFiles> objFiles = new List<StudentBehaviourFiles>();
            string Id = "";
            if (lstofstudentId != null)
            {
                Id = string.Join(",", lstofstudentId);

            }
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {

                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;
                        var extension = System.IO.Path.GetExtension(file.FileName);

                        //content for file upload
                        string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                        string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.StudentBehaviour;
                        //string fileModule = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFilesDir;
                        string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.StudentBehaviour + "/User_" + SessionHelper.CurrentSession.Id;

                        Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                        Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);

                        Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                        fname = Guid.NewGuid() + "*" + fname;
                        relativePath = Constants.StudentBehaviour + "/User_" + SessionHelper.CurrentSession.Id + "/" + fname;
                        fname = Path.Combine(filePath, fname);
                        file.SaveAs(fname);
                        docpath = relativePath;
                        FileName = file.FileName;

                        List<long> TagIds = Id.Split(',').Select(long.Parse).ToList();
                        foreach (var s in TagIds)
                        {
                            objFiles.Add(new StudentBehaviourFiles { StudentId = s, BehaviourId = BehaviourId, FileName = FileName, UploadedFilePath = docpath });

                        }
                    }



                }
                catch (Exception e) { }
            }
            var isActionperformed = _BehaviourService.InsertBulkStudentBehaviour(objFiles, Id, BehaviourId, txtAreaBehaviourComment);
            if (isActionperformed)
            {
                return Json(new OperationDetails(isActionperformed), JsonRequestBehavior.AllowGet);
            }

            return null;
        }
        private string ConvertViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter writer = new StringWriter())
            {
                ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }
        public ActionResult EditStudentBehaviourType(int behaviourId = 0)
        {
            StudentBehaviourVM objStudentBehaviour = new StudentBehaviourVM();
            if (behaviourId != 0)
            {
                objStudentBehaviour.BehaviourList = _BehaviourService.GetListOfStudentBehaviour().Where(e => e.BehaviourId == behaviourId).ToList();
                objStudentBehaviour.IsSelectMode = true;
            }
            else
            {
                objStudentBehaviour.BehaviourList = _BehaviourService.GetListOfStudentBehaviour().ToList();


            }
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");
            return PartialView("_StudentBehaviourTypeAddEdit", objStudentBehaviour);
        }
        public ActionResult EditStudentTypeById(int behaviourId = 0, string behaviourType = "", int behaviourPoint = 0, int categoryId = 0)
        {
            if (behaviourId > 0)
            {
                var IsActionPerformed = _BehaviourService.UpdateBehaviourTypes(behaviourId, behaviourType, behaviourPoint, categoryId);

                return Json(new OperationDetails(IsActionPerformed), JsonRequestBehavior.AllowGet);

            }

            return null;


        }
        public ActionResult OnselectLoadBehaviourLogo(int behaviourId = 0)
        {
            if (behaviourId > 0)
            {
                var BehaviourLogo = _BehaviourService.GetListOfStudentBehaviour().Where(e => e.BehaviourId == behaviourId).FirstOrDefault();
                return Json(BehaviourLogo.BehaviourLogo, JsonRequestBehavior.AllowGet);
            }
            return null;

        }
        public ViewResult LoadStudentBehaviour(string strtt_id = "", string grade = null, string section = null, string stu_id = null)
        {
            int tt_id = 0;
            if (!string.IsNullOrWhiteSpace(strtt_id))
            {
                tt_id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
            }
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var TeacherId = SessionHelper.CurrentSession.Id;
            var username = SessionHelper.CurrentSession.UserName;
            ViewBag.CourseGroupList = new SelectList(_BehaviourService.GetCourseGroupList(TeacherId, schoolid), "SchoolGroupId", "SchoolGroupName");
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, SessionHelper.CurrentSession.ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");
            var BehaviourVM = new BehaviourVM();
            BehaviourVM.classList = _BehaviourService.GetBehaviourClassList(username, tt_id, grade, section);
            BehaviourVM.classList.ForEach(x =>
            {
                if (x.IncidentDate != null)
                {
                    x.IncidentDate = Convert.ToDateTime(x.IncidentDate).ConvertUtcToLocalTime();
                }
                if (x.ModifiedOn != null)
                {
                    x.ModifiedOn = Convert.ToDateTime(x.ModifiedOn).ConvertUtcToLocalTime();
                }
            });
            BehaviourVM.behaviour = _BehaviourService.LoadBehaviourByStudentId(stu_id);
            ViewBag.tt_Id = tt_id;
            return View("StudentBehaviourIndex", BehaviourVM);
        }
        public FileResult DownloadAttachment(int studentId = 0, int UploadedId = 0)
        {
            if (studentId > 0 & UploadedId > 0)
            {

                var FileDetails = _BehaviourService.GetFileDetailsByStudentId(studentId).ToList();
                var FileUploaded = FileDetails.Where(e => e.MeritUploaded == UploadedId).FirstOrDefault();

                try
                {
                    if (FileUploaded != null)
                    {
                        if (!string.IsNullOrWhiteSpace(FileUploaded.MeritUpload))
                        {
                            string path = PhoenixConfiguration.Instance.ReadFilePath + FileUploaded.MeritUpload;
                            var rawFileName = Path.GetFileName(path);
                            var filename = rawFileName.Substring(Guid.NewGuid().ToString().Length);
                            WebClient myWebClient = new WebClient();
                            byte[] myDataBuffer = myWebClient.DownloadData(path);
                            return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
                        }


                    }
                }
                catch (Exception e)
                {

                }




            }

            return null;


        }
        public ActionResult Download(long studentId = 0)
        {

            var FileDetails = _BehaviourService.GetFileDetailsByStudentId(studentId).ToList();
            //var Filecount = FileDetails.ToList();
            using (var memoryStream = new MemoryStream())
            {
                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    for (int i = 0; i < FileDetails.Count(); i++)
                    {

                        ziparchive.CreateEntryFromFile(PhoenixConfiguration.Instance.ReadFilePath + FileDetails[i].MeritUpload, FileDetails[i].MeritType);
                    }
                }
                return File(memoryStream.ToArray(), "application/zip", "Attachments.zip");
            }
        }
        public ActionResult DeleteStudentBehaviour(long studentId = 0, int BehaviourId = 0)
        {
            StudentBehaviourVM objStudentBehavior = new StudentBehaviourVM();
            var IsActionPerformed = _BehaviourService.DeleteStudentBehaviourMapping(studentId, BehaviourId);
            OperationDetails op = new OperationDetails(IsActionPerformed);
            if (IsActionPerformed)
            {
                op.Message = LocalizationHelper.DeleteSuccessMessage;
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult AddEditStudentBehaviour(long MeritId = 0, long StudentId = 0, string gradeId = "", long GroupId = 0)
        {
            StudentBehaviourMerit objStudentBehaviourMerit = new StudentBehaviourMerit();
            objStudentBehaviourMerit.StudentId = StudentId;
            objStudentBehaviourMerit.GradeId = gradeId;
            objStudentBehaviourMerit.GroupId = GroupId;
            var Category = SelectListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null).Where(x => x.ItemId != "5");
            ViewBag.Category = new SelectList(Category, "ItemId", "ItemName");
            if (MeritId > 0)
            {
                var MeritDetails = _BehaviourService.GetMeritDetails(MeritId).FirstOrDefault();
                objStudentBehaviourMerit.MeritId = MeritId;
                objStudentBehaviourMerit.MeritCategoryId = MeritDetails.MeritCategoryId;
                objStudentBehaviourMerit.MeritSubCategoryId = MeritDetails.MeritSubCategoryId;
                objStudentBehaviourMerit.MeritRemarks = MeritDetails.MeritRemarks;
                objStudentBehaviourMerit.MeritType = MeritDetails.MeritType;
                objStudentBehaviourMerit.MeritUpload = MeritDetails.MeritUpload;
                objStudentBehaviourMerit.StudentId = StudentId;
                objStudentBehaviourMerit.MeritUploaded = MeritDetails.MeritUploaded;
                objStudentBehaviourMerit.IncidentDate = Convert.ToDateTime(MeritDetails.IncidentDate).ConvertUtcToLocalTime();
                objStudentBehaviourMerit.AttachmentId = MeritDetails.AttachmentId;
            }
            return PartialView("_AddEditStudentBehaviour", objStudentBehaviourMerit);
        }
        [HttpPost]
        public ActionResult SaveMeritDeMerit(StudentBehaviourMerit objStudentBehaviourMerit)
        {
            //convert arabic dates
            objStudentBehaviourMerit.IncidentDate = objStudentBehaviourMerit.strIncidentDate.ToSystemReadableDate();
            MeritDemerit objmeritDemerit = new MeritDemerit();
            var fileName = string.Empty;
            var schoolid = SessionHelper.CurrentSession.SchoolId;//Userdetail.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var clm_id = SessionHelper.CurrentSession.CLM_ID;
            var storeDirectory = Constants.StudentBehaviour + "/School_" + SessionHelper.CurrentSession.SchoolId;

            objStudentBehaviourMerit.UserId = SessionHelper.CurrentSession.Id;
            objStudentBehaviourMerit.AttachmentList.RemoveAll(x => x == null);
            objStudentBehaviourMerit.AttachmentList.AddRange(StorageHelper.SaveAllFileAttachment(Request.Files.GetMultiple("sbFileUpload"), Phoenix.Models.Entities.AttachmentKey.StudentBehaviour.EnumToString(), storeDirectory, 0));
            if (objStudentBehaviourMerit.AttachmentList.Any())
            {
                if (objStudentBehaviourMerit.IsExistFile)
                {
                    objStudentBehaviourMerit.AttachmentList.FirstOrDefault().Mode = TranModes.Update;
                    objStudentBehaviourMerit.AttachmentList.FirstOrDefault().AttachmentId = objStudentBehaviourMerit.AttachmentId;
                    fileName = PhoenixConfiguration.Instance.WriteFilePath + objStudentBehaviourMerit.MeritUpload;
                }
            }
            objStudentBehaviourMerit.IncidentDate = Convert.ToDateTime(objStudentBehaviourMerit.IncidentDate.Value).ConvertLocalToUTC();
            objStudentBehaviourMerit.MeritType = objStudentBehaviourMerit.MeritCategoryId == 1 ? "Achieves" : (objStudentBehaviourMerit.MeritCategoryId == 2 ? "Merit" : "De-Merit");
            var objMeritDemeritList = JsonConvert.DeserializeObject<List<CategoryDetails>>(objStudentBehaviourMerit.MeritDemertIds[1]);
            objmeritDemerit.objMeritDemerit = objStudentBehaviourMerit;
            objmeritDemerit.objListOfCategories = objMeritDemeritList;


            var result = _BehaviourService.InsertMeritDemerit(SessionHelper.CurrentSession.SchoolId.ToString(), 0, DateTime.Now.ConvertLocalToUTC(), objmeritDemerit);
            OperationDetails op = new OperationDetails(result);
            if (result && !string.IsNullOrEmpty(fileName) && System.IO.File.Exists(fileName))
            {
                System.IO.File.Delete(fileName);
            }
            if (objStudentBehaviourMerit.MeritId == 0 && result)
            {
                op.Message = LocalizationHelper.AddSuccessMessage;
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetMeritDetails(long studentId = 0, string type = "", string gradeId = "")
        {
            List<SubCategories> objLstSubCategories = new List<SubCategories>();
            objLstSubCategories = _BehaviourService.GetMeritCategoryByStudent(SessionHelper.CurrentSession.SchoolId, studentId).ToList();

            if (type.Equals("1"))
            {
                objLstSubCategories = objLstSubCategories.Where(e => e.CategoryScore > 0).ToList();
            }
            else if (type.Equals("-1"))
            {
                objLstSubCategories = objLstSubCategories.Where(e => e.CategoryScore < 0).ToList();
            }

            ViewBag.EditMode = true;

            return PartialView("_BehaviourSubCategories", objLstSubCategories);
        }
        public ActionResult GetSubCategoriesByCategoryId(long categoryId = 0, int subCategory = 0, string gradeId = "", long GroupId = 0)
        {
            List<SubCategories> objLstSubCategories = new List<SubCategories>();
            objLstSubCategories = _BehaviourService.GetSubCategoriesByCategoryId(categoryId, SessionHelper.CurrentSession.SchoolId.ToString(), gradeId, GroupId).ToList();
            ViewBag.SubCategoryId = subCategory;
            return PartialView("_BehaviourSubCategories", objLstSubCategories);
        }
        #endregion


        public PartialViewResult LoadStudentOnReportForm(long studentId)
        {
            var result = _BehaviourService.GetStudentOnReportMDetail(studentId);
            ViewBag.AcademicYearId = SessionHelper.CurrentSession.ACD_ID;
            ViewBag.SchoolId = SessionHelper.CurrentSession.SchoolId;
            ViewBag.StudentId = studentId;
            return PartialView("~/Areas/Behaviour/Views/Behaviour/_LoadStudentOnReportForm.cshtml", result);
        }

        public ActionResult SaveStudentOnReport(StudentOnReportMaster studentOnReportMaster)
        {
            studentOnReportMaster.AcademicYearId = SessionHelper.CurrentSession.ACD_ID;
            studentOnReportMaster.SchoolId = SessionHelper.CurrentSession.SchoolId;
            studentOnReportMaster.CreatedBy = SessionHelper.CurrentSession.UserName;
            if (!string.IsNullOrEmpty(studentOnReportMaster.GroupId))
                studentOnReportMaster.GroupId = EncryptDecryptHelper.Decrypt(studentOnReportMaster.GroupId);
            var result = _BehaviourService.StudentOnReportMCU(studentOnReportMaster);
            if (result > 0)
            {
                var newRecords = _BehaviourService.GetStudentOnReportMDetail(studentOnReportMaster.StudentId);
                return PartialView("_LoadStudentOnReportGrid", newRecords);
            }
            else if (result == -10)
                return Json(new OperationDetails(false, "StudentAlreadyExistForReporting"), JsonRequestBehavior.AllowGet);
            else
                return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult LoadStudentOnReportDetailsForm(long studentId, long? studentOnReportMasterId, string groupId = null)
        {
            var parameters = new StudentOnReportDetailsParameter
            {
                StudentId = studentId,
                SchoolId = SessionHelper.CurrentSession.SchoolId,
                StudentOnReportMasterId = studentOnReportMasterId,
                CreatedBy = SessionHelper.CurrentSession.UserName,
                GroupId = groupId,
            };
            var result = _BehaviourService.GetStudentOnReportDetails(parameters);
            return PartialView("_LoadStudentOnReportDetailsForm", result);
        }
        public ActionResult SaveStudentOnReportDetails(StudentOnReportDetail studentOnReportDetail)
        {
            studentOnReportDetail.CreatedBy = SessionHelper.CurrentSession.UserName;
            //if (!string.IsNullOrEmpty(studentOnReportDetail.GroupId))
            //    studentOnReportDetail.GroupId = EncryptDecryptHelper.Decrypt(studentOnReportDetail.GroupId);
            var result = _BehaviourService.StudentOnReportDetailsCU(studentOnReportDetail);
            if (result > 0)
            {
                var parameters = new StudentOnReportDetailsParameter
                {
                    StudentId = studentOnReportDetail.StudentId,
                    AcademicYear = SessionHelper.CurrentSession.ACD_ID,
                    SchoolId = SessionHelper.CurrentSession.SchoolId,
                    StudentOnReportMasterId = studentOnReportDetail.StudentOnReportMasterId,
                    GroupId = studentOnReportDetail.GroupId,
                };
                var newRecords = _BehaviourService.GetStudentOnReportDetails(parameters);
                return PartialView("_LoadStudentOnReportDetailsGrid", newRecords);
            }
            else
                return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult GetStudentCard(int tt_id = 0, string grade = null, string section = null)
        {

            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var username = Userdetail.UserName;
            //var list = _BehaviourService.GetClassList(username, tt_id, grade, section);
            var list = _BehaviourService.GetStudentList(username, tt_id, grade, section).ToList();
            TempData["classList"] = list.ToList();
            return PartialView("_StudentCardPartial", list);
        }
        public JsonResult GetGradeWhenCurriculumChange(int curriculumId)
        {
            var result = SelectListHelper.GetSelectListData(ListItems.SchoolGrade, $"CurriculumId = {curriculumId}", $"{SessionHelper.CurrentSession.SchoolId}");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Behavior Certificate Schedule 
        public ActionResult StudentCertificateList()
        {
            var curriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.Curriculum = curriculum;
            string baseUri = GetBaseUrl();
            ViewBag.JSONFileUrl = baseUri + "/pixie/";
            ViewBag.FilePath = PhoenixConfiguration.Instance.ReadFilePath;
            var curriculumId = curriculum.FirstOrDefault().Value;
            ViewBag.Certificates = behaviourSetupService.GetCertificateSchedulings(null, curriculumId.ToLong(), SessionHelper.CurrentSession.SchoolId);
            ViewBag.Grades = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolGrade, $"CurriculumId = {curriculumId}", $"{SessionHelper.CurrentSession.SchoolId}").ToList(), "ItemId", "ItemName");
            return View();
        }
        public ActionResult GetStudentListBySchedule(string section, int scheduleType, long currId)
        {
            ViewBag.Certificates = behaviourSetupService.GetCertificateSchedulings(null, currId, SessionHelper.CurrentSession.SchoolId, scheduleType);
            var result = _BehaviourService.GetStudentPointCategory(section, scheduleType);
            return PartialView("GetStudentList", result);
        }
        //public PartialViewResult GetCertificateStudentCard(int scheduleType, long? certificateScheduleType)
        //{
        //    var result = _BehaviourService.GetStudentPointCategory(SessionHelper.CurrentSession.SchoolId, SessionHelper.CurrentSession.ACD_ID, scheduleType);
        //    if (certificateScheduleType.HasValue)
        //    {
        //        return PartialView("_GetCertificateStudentCard", result.Where(x => x.CertificateId == certificateScheduleType.Value).ToList());
        //    }
        //    return PartialView("_GetCertificateStudentCard", result);
        //}
        public ActionResult PrintCertificate(List<long> studentId, int certificateId)
        {
            var template = templateService.GetTemplateDetail(certificateId, (int)SessionHelper.CurrentSession.SchoolId, StringEnum.GetStringValue(PlanTemplateTypes.Certificate), string.Empty, null, true, true);
            var result = templateService.GetTemplateFieldData(certificateId, studentId);
            List<string> imageString = new List<string>();
            if (result.Count() > 0)
            {
                string certificateFilePath = PhoenixConfiguration.Instance.WriteFilePath + result.FirstOrDefault().FileName;

                string previewFilePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.TemplateCertificatePreviewDir;
                Common.Helpers.CommonHelper.CreateDestinationFolder(previewFilePath);
                previewFilePath += "/User_" + SessionHelper.CurrentSession.Id;

                Common.Helpers.CommonHelper.CreateDestinationFolder(previewFilePath);
                StringBuilder json = new StringBuilder();
                StringBuilder finalJson = new StringBuilder();
                foreach (var resultMapping in result.GroupBy(x => x.UserId))
                {
                    json.Append(System.IO.File.Exists(certificateFilePath) ? System.IO.File.ReadAllText(certificateFilePath) : "");
                    foreach (var item in resultMapping)
                    {
                        json = json.Replace(item.PlaceHolder, item.ColumnData);
                    }
                    finalJson.Append(json);
                    imageString.Add(json.ToString());
                    json.Clear();
                }

                Common.Helpers.CommonHelper.DeleteFile(previewFilePath + "/" + template.FileName);
                System.IO.File.WriteAllText(previewFilePath + "/" + template.FileName, finalJson.ToString());
            }

            var pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_CertificatePrintPartial", imageString);
            string pdfTitle = string.Empty;
            string downloadFileName = string.Empty;
            var config = new PdfGenerateConfig()
            {
                MarginBottom = 20,
                MarginLeft = 20,
                MarginRight = 20,
                MarginTop = 20,
            };
            //config.PageOrientation = PageOrientation.Landscape;
            config.PageSize = PageSize.A4;
            PdfDocument pdf = new PdfDocument();
            pdf = PdfGenerator.GeneratePdf(pdfhtml, config);
            pdf.Info.Title = pdfTitle;
            MemoryStream stream = new MemoryStream();
            pdf.Save(stream, false);
            byte[] file = stream.ToArray();
            stream.Write(file, 0, file.Length);
            stream.Position = 0;
            return File(stream, "application/pdf", downloadFileName);
        }
        [NonAction]
        private string GetBaseUrl()
        {
            var request = Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            var scheme = Request.IsLocal ? "http" : "https";

            if (appUrl != "/")
                appUrl = "/" + appUrl;

            var baseUrl = string.Format("{0}://{1}/", scheme, request.Url.Authority);

            return baseUrl; //this will return complete url
        }
        [HttpGet]
        public JsonResult GetCertificateFieldData(int templateId, long studentId)
        {
            var result = templateService.GetTemplateFieldData(templateId, studentId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetCertificateFieldData(int templateId, List<long> studentId)
        {
            var result = templateService.GetTemplateFieldData(templateId, studentId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendEmail(List<StudentPointCategory> student, string sectionIds, string date = null, bool isNotification = false)
        {
            List<string> listOfStudentEmailNotSend = new List<string>();
            List<CertificateProcessLog> processLogs = new List<CertificateProcessLog>();
            var operationDetails = new List<OperationDetails>();
            if (student.All(x => x.IsEmailOther))
            {
                string fileSavePath = $"{PhoenixConfiguration.Instance.WriteFilePath}{Constants.BehaviorCertificateDir}/{SessionHelper.CurrentSession.SchoolCode}/{SessionHelper.CurrentSession.Id}/";
                Common.Helpers.CommonHelper.CreateDestinationFolder(fileSavePath);

                var certificateSchedules = student.Select(x => x.CertificateScheduleId).Distinct();
                foreach (var certficateSchedule in certificateSchedules)
                {
                    var attachments = string.Empty;
                    foreach (var item in student.Where(x => x.CertificateScheduleId == certficateSchedule))
                    {
                        //var pdfBinary = Convert.FromBase64String(item.FileArray);

                        var fname = $"{System.Text.RegularExpressions.Regex.Replace(item.StudentName, @"[^0-9a-zA-Z]+", "")}_{DateTime.Now.Ticks.ToString()}.pdf";
                        var relativePath = Constants.BehaviorCertificateDir + $"/{ SessionHelper.CurrentSession.SchoolCode}/{ SessionHelper.CurrentSession.Id}" + "/" + fname;
                        fname = Path.Combine(fileSavePath, fname);
                        try
                        {
                            //using (var fs = new FileStream(fname, FileMode.Create))
                            //using (var writer = new BinaryWriter(fs))
                            //{
                            //    writer.Write(pdfBinary, 0, pdfBinary.Length);
                            //    writer.Close();
                            //}
                            item.FilePath = PhoenixConfiguration.Instance.ReadFilePath + relativePath;
                            var baseUrl = GetBaseUrl();
                            var fileUrl = $"{baseUrl}/Behaviour/Behaviour/Certificate?sid={EncryptDecryptHelper.EncryptUrl($"{item.CertificateScheduleId},{(string.IsNullOrEmpty(date) ? DateTime.Now :  date.ToDateTime()).FormatDate()},{item.StudentID}")}";
                            attachments += $"</br><a href='{fileUrl}'>{item.StudentName} - Certificate</a>";

                            var processlog = new CertificateProcessLog
                            {
                                ActionType = CertificateStudentAction.EmailOther,
                                CreatedBy = SessionHelper.CurrentSession.Id,
                                StudentId = item.StudentID,
                                TemplateId = item.CertificateScheduleId
                            };
                            processLogs.Add(processlog);
                        }
                        catch (Exception ex) { }
                    }
                    string EmailBody = "";
                    StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(Constants.BehaviorCertificateEmailBody), Encoding.UTF8);
                    EmailBody = reader.ReadToEnd();
                    EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
                    EmailBody = EmailBody.Replace("@@Attachments", attachments);
                    EmailDetail sendEmailNotificationView = new EmailDetail();
                    sendEmailNotificationView.ToEmail = student.FirstOrDefault(x => x.CertificateScheduleId == certficateSchedule).OtherEmail;
                    sendEmailNotificationView.CCEmail = "";
                    sendEmailNotificationView.EmailType = "PCR_BEHAVIOR_CERTIFICATE";
                    sendEmailNotificationView.Subject = "Behavior Certificate - " + student.FirstOrDefault(x => x.CertificateScheduleId == certficateSchedule).CetificateDescription;
                    sendEmailNotificationView.Message = EmailBody;
                    var operation = commonService.SendEmailCirculation(sendEmailNotificationView);
                    operationDetails.Add(operation);
                }
            }
            else
            {
                student.ForEach(x =>
                {
                    if (!SendEmail(x))
                    {
                        listOfStudentEmailNotSend.Add(x.StudentName);
                    }
                    else
                    {
                        var processlog = new CertificateProcessLog
                        {
                            ActionType = x.IsEmailOther ? CertificateStudentAction.EmailOther : CertificateStudentAction.Email,
                            CreatedBy = SessionHelper.CurrentSession.Id,
                            StudentId = x.StudentID,
                            TemplateId = x.CertificateScheduleId
                        };
                        processLogs.Add(processlog);
                    }
                });
            }


            var result = _BehaviourService.CertificateProcessLogCU(processLogs);

            var op = new OperationDetails(result);
            if (result)
            {
                var message = ResourceManager.GetString("Behaviour.CertificateSchedule.EmailSentSuccess");
                if (student.All(x => x.IsEmailOther))
                {
                    message = message.Replace("{0}", string.IsNullOrEmpty(student.FirstOrDefault().OtherEmail) ? "0" : student.FirstOrDefault().OtherEmail.Split(',').Count().ToString());
                    message = message.Replace("{1}", ResourceManager.GetString("Behaviour.CertificateSchedule.Teacher"));
                }
                else
                {
                    message = message.Replace("{0}", operationDetails.Count(x => x.Success).ToString());
                    message = message.Replace("{1}", ResourceManager.GetString("Behaviour.CertificateSchedule.Student"));
                }
                op.Message = message;
                if (isNotification)
                {
                    var UpdatedRecords = _BehaviourService.GetStudentPointCategory(null, CertificateScheduleId: student.FirstOrDefault().CertificateScheduleId, date: date);
                    op.RelatedHtml = ControllerExtension.RenderPartialViewToString(
                    this,
                    "_CertificateStudentCardPartial",
                    UpdatedRecords.Where(x => x.CertificateScheduleId == (int)student.FirstOrDefault().CertificateScheduleId).ToList());
                }
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        private bool SendEmail(StudentPointCategory student)
        {
            var pdfBinary = Convert.FromBase64String(student.FileArray);

            string fileSavePath = $"{PhoenixConfiguration.Instance.WriteFilePath}{Constants.BehaviorCertificateDir}/{SessionHelper.CurrentSession.SchoolCode}/{SessionHelper.CurrentSession.Id}/";
            Common.Helpers.CommonHelper.CreateDestinationFolder(fileSavePath);

            //fileSavePath += $"{fileSavePath}/{SessionHelper.CurrentSession.SchoolCode}";
            //Common.Helpers.CommonHelper.CreateDestinationFolder(fileSavePath);

            //fileSavePath += $"{fileSavePath}/{SessionHelper.CurrentSession.Id}";
            //Common.Helpers.CommonHelper.CreateDestinationFolder(fileSavePath);

            var fname = $"{student.StudentName}_{DateTime.Now.Ticks.ToString()}.pdf";
            var relativePath = Constants.BehaviorCertificateDir + "/" + fname;
            fname = Path.Combine(fileSavePath, fname);
            try
            {
                using (var fs = new FileStream(fname, FileMode.Create))
                using (var writer = new BinaryWriter(fs))
                {
                    writer.Write(pdfBinary, 0, pdfBinary.Length);
                    writer.Close();
                }
                student.FilePath = PhoenixConfiguration.Instance.ReadFilePath + relativePath;
                var attachment = $"<a href='{student.FilePath}'>View Certificate</a>";
                var sendEmailNotificationView = _BehaviourService.GenerateEmailTemplateForBehaviorCertificate("Behavior Certificate", string.Empty, student.StudentID.ToString(), string.Empty, attachment);
                //if (student.IsEmailOther)
                //{
                //    EmailHelper.SendAsyncEmail(this, student, "_CertificateEmailTempate", student.OtherEmail, "Certificate");
                //}
                //else
                //{
                //    EmailHelper.SendAsyncEmail(this, student, "_CertificateEmailTempate", student.StudentEmail, "Certificate");
                //}
                bool isSuccess = commonService.SendEmailNotifications(sendEmailNotificationView);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CertificateProcessLogCU(List<CertificateProcessLog> processLogs)
        {
            processLogs.ForEach(x =>
            {
                x.ActionType = CertificateStudentAction.Print;
                x.CreatedBy = SessionHelper.CurrentSession.Id;
            });
            var result = _BehaviourService.CertificateProcessLogCU(processLogs);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult BehaviorCertificate(string sid)
        {
            var studentPointCategory = new List<StudentPointCategory>();
            ViewBag.JSONFileUrl = GetBaseUrl();
            if (!string.IsNullOrEmpty(sid))
            {
                var value = EncryptDecryptHelper.DecryptUrl(sid);
                if (!string.IsNullOrEmpty(value) && value.Contains(','))
                {
                    var splitVal = value.Split(',');
                    var certificateScheduleId = splitVal[0].ToLong();
                    var date = splitVal[1];
                    ViewBag.Date = date;
                    studentPointCategory = _BehaviourService.GetStudentPointCategory(null, CertificateScheduleId: certificateScheduleId, date: date).ToList();
                }
            }
            return View("BehaviourCertificate", studentPointCategory);
        }
        [AllowAnonymous]
        public ActionResult Certificate(string sid)
        {
            var studentPointCategory = new StudentPointCategory();
            var template = new Phoenix.Models.Template();
            if (!string.IsNullOrEmpty(sid))
            {
                var value = EncryptDecryptHelper.DecryptUrl(sid);
                if (!string.IsNullOrEmpty(value) && value.Contains(','))
                {
                    var splitVal = value.Split(',');
                    var certificateScheduleId = splitVal[0].ToLong();
                    var date = splitVal[1];
                    var studentId = splitVal[2].ToLong();
                    ViewBag.Date = date;
                    studentPointCategory = _BehaviourService.GetStudentPointCategory(null, CertificateScheduleId: certificateScheduleId, date: date, studentId: studentId).FirstOrDefault();
                    ViewBag.StudentName = $"{studentPointCategory.StudentName}_{studentPointCategory.CetificateDescription}";
                    var dynamicParameters = "#DATE#:" + date.ToDateTime().ToString("yyyy-MM-dd");
                    var templateMapping = templateService.GetTemplateFieldData(studentPointCategory.CertificateId.ToInteger(), new List<long> { studentPointCategory.StudentID }, dynamicParameters);
                    string baseUri = GetBaseUrl();
                    template.JSONFileUrl = baseUri + "pixie/";
                    if (templateMapping.Any() && (studentPointCategory.Certificates.Count() <=0 || studentPointCategory.Certificates.Any(x => string.IsNullOrEmpty(x.FilePath) && x.ActionType == CertificateStudentAction.EmailOther )))
                    {
                        var certificateTemplate = PhoenixConfiguration.Instance.WriteFilePath + studentPointCategory.FilePath;

                        string previewFilePath = $"{PhoenixConfiguration.Instance.WriteFilePath}{Constants.BehaviorCertificateDir}/{SessionHelper.CurrentSession.SchoolCode}/";
                        Common.Helpers.CommonHelper.CreateDestinationFolder(previewFilePath);

                        previewFilePath += "/User_" + SessionHelper.CurrentSession.Id;
                        Common.Helpers.CommonHelper.CreateDestinationFolder(previewFilePath);

                        var fname = $"{System.Text.RegularExpressions.Regex.Replace(studentPointCategory.StudentName, @"[^0-9a-zA-Z]+", "")}_{DateTime.Now.Ticks.ToString()}.json";

                        template.FilePath = $"{PhoenixConfiguration.Instance.ReadFilePath}{Constants.BehaviorCertificateDir}/{SessionHelper.CurrentSession.SchoolCode}/User_{SessionHelper.CurrentSession.Id}/{fname}";
                        fname = Path.Combine(previewFilePath, fname);
                        string json = System.IO.File.Exists(certificateTemplate) ? System.IO.File.ReadAllText(certificateTemplate) : "";
                        foreach (var item in templateMapping)
                        {
                            json = json.Replace(item.PlaceHolder, item.ColumnData);
                        }

                        System.IO.File.WriteAllText(fname, json);
                        var processlogs = new List<CertificateProcessLog> {
                            new CertificateProcessLog
                            {
                                ActionType = CertificateStudentAction.EmailOther,
                                CreatedBy = SessionHelper.CurrentSession.Id,
                                StudentId = studentPointCategory.StudentID,
                                TemplateId = studentPointCategory.CertificateScheduleId,
                                FilePath = template.FilePath.Replace(PhoenixConfiguration.Instance.ReadFilePath,string.Empty)
                            } 
                        };
                        var result = _BehaviourService.CertificateProcessLogCU(processlogs);
                    }
                    else
                    {
                        template.FilePath = $"{PhoenixConfiguration.Instance.ReadFilePath}{studentPointCategory.Certificates.FirstOrDefault().FilePath}";
                    }
                }
            }
            return View(template);
        }
        #endregion
    }
}