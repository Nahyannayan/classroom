﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.Helpers;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common;
using BehaviourActionFollowups = SMS.Web.Areas.SMS.Model.BehaviourActionFollowup;

namespace Phoenix.VLE.Web.Areas.Behaviour.Controllers
{
    public class IncidentController : BaseController
    {
        private readonly IIncidentService incidentService;
        private readonly ISchoolService schoolService;
        private readonly IStudentListService studentListService;

        public IncidentController(IIncidentService incidentService, ISchoolService schoolService, IStudentListService studentListService)
        {
            this.incidentService = incidentService;
            this.schoolService = schoolService;
            this.studentListService = studentListService;
        }
        //// GET: Behaviour/Incident
        //public ActionResult Index()
        //{
        //    return View();
        //}

        #region Incident Entry 
        public ActionResult IncidentDashboard()
        {
            ViewBag.Curriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            return View();
        }
        public JsonResult GetIncidentList(long academicYearId, string month, bool isFA)
        {
            int monthInt = month != "0" ? DateTime.ParseExact(month, "MMM", CultureInfo.CurrentCulture).Month : 0;
            var list = incidentService.GetIncidentList(SessionHelper.CurrentSession.SchoolId, academicYearId, monthInt, isFA);
            ViewBag.isFa = isFA;
            var incidentList = list.IncidentLists.ToList();
            incidentList.ForEach(x =>
            {
                x.Incident_Date = Convert.ToDateTime(x.Incident_Date).ConvertUtcToLocalTime();
                x.Recorded_On = Convert.ToDateTime(x.Recorded_On.ConvertUtcToLocalTime());
                x.CurriculumId = (int)academicYearId;
            });
            var table = ControllerExtension.RenderPartialViewToString(this, "~/Areas/Behaviour/Views/Incident/PartialViews/IncidentParentTable.cshtml", list.IncidentLists);
            var barChart = ControllerExtension.RenderPartialViewToString(this, "~/Areas/Behaviour/Views/Incident/PartialViews/BarGraph.cshtml", list.LineChart);
            var pieChart = ControllerExtension.RenderPartialViewToString(this, "~/Areas/Behaviour/Views/Incident/PartialViews/PieChart.cshtml", list.PieChart);
            return Json(new { table, barChart, pieChart }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddEditIncident(string id, string curr)
        {
            long incidentid = Convert.ToInt64(string.IsNullOrEmpty(id) ? "0" : EncryptDecryptHelper.DecryptUrl(id));
            int curriculumId = Convert.ToInt32(string.IsNullOrEmpty(curr) ? "0" : EncryptDecryptHelper.DecryptUrl(curr));
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var model = new IncidentListModel();
            if (incidentid > 0)
            {
                var incidentModel = incidentService.GetIncident(incidentid);
                if (incidentModel.Incident != null)
                {
                    var behaviourDetail = incidentModel.Incident;
                    model.IncidentId = behaviourDetail.IncidentId;
                    model.Incident_Date = Convert.ToDateTime(behaviourDetail.Incident_Date).ConvertUtcToLocalTime();
                    model.Incident_DateString = Convert.ToDateTime(model.Incident_Date).ToString("dd-MMM-yyyy");
                    model.Incident_Type = behaviourDetail.Incident_Type;
                    model.Recorded_On = Convert.ToDateTime((behaviourDetail.Recorded_On).ConvertUtcToLocalTime());
                    model.Reported_By = behaviourDetail.Reported_By;
                    model.Incident_Time = Convert.ToDateTime(model.Incident_Date).ToString("h:mm tt");
                    model.Reported_ById = behaviourDetail.Reported_ById;
                    model.Incident_CategoryId = behaviourDetail.Incident_CategoryId;
                    model.Incident_SubCategoryId = behaviourDetail.Incident_SubCategoryId;
                    model.Incident_Remarks = behaviourDetail.Incident_Remarks;
                    model.IncidentStudentLists = incidentModel.StudentLists;
                    model.IncidentWitnesses = incidentModel.Witnesses;


                    var SubCategory = SelectListHelper.GetSelectListData(ListItems.BehaviourSubCategory, string.Format("{0},{1}", model.Incident_CategoryId, schoolid)).ToList();
                    ViewBag.SubCategory = new SelectList(SubCategory, "ItemId", "ItemName", model.Incident_SubCategoryId);
                    if (SubCategory.Any())
                    {
                        var subCategory = SubCategory.FirstOrDefault().ItemName;
                        model.Points = Convert.ToInt32(subCategory.Split('|')[1].Split('.')[0]);
                    }
                }
            }
            model.CurriculumId = curriculumId;
            var list = SelectListHelper.GetSelectListData(ListItems.SchoolGrade, $"CurriculumId = {curriculumId}", $"{SessionHelper.CurrentSession.SchoolId}").ToList();
            ViewBag.Grades = list;
            model.Incident_CategoryId = 5;
            ViewBag.Category = new SelectList(SelectListHelper.GetSelectListData(ListItems.BehaviourSubCategory, $"{model.Incident_CategoryId},{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.IncidentStaffList = new SelectList(incidentService.GetSchoolTeachersBySchoolId(schoolid), "TeacherId", "TeacherName");
            ViewBag.CategoryLevel = null;
            return View(model);
        }
        [HttpPost]
        public JsonResult SaveIncidentDetails(IncidentEntry incidentEntry)
        {
            incidentEntry.IncidentDate = incidentEntry.IncidentDate.Value.AddHours(incidentEntry.IncidentTime.ToDateTime().Hour);
            incidentEntry.IncidentDate = incidentEntry.IncidentDate.Value.AddMinutes(incidentEntry.IncidentTime.ToDateTime().Minute);
            incidentEntry.IncidentDate = Convert.ToDateTime(incidentEntry.IncidentDate.Value).ConvertLocalToUTC();
            incidentEntry.DataMode = incidentEntry.BehaviourId == 0 ? "Add" : "Edit";
            incidentEntry.SchoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
            incidentEntry.StaffId = SessionHelper.CurrentSession.Id;
            incidentEntry.EntryDate = Convert.ToDateTime(DateTime.Now.ConvertLocalToUTC());
            var response = incidentService.IncidentEntryCUD(incidentEntry);
            if (response.Replace("\"", "") == "1" || Convert.ToInt32(response.Replace("\"", "")) > 1)
                if (Convert.ToInt32(response.Replace("\"", "")) > 1)
                    return Json(new OperationDetails(true, Phoenix.Common.Localization.LocalizationHelper.AddSuccessMessage) { InsertedRowId = Convert.ToInt32(response.Replace("\"", "")) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new OperationDetails(true, Phoenix.Common.Localization.LocalizationHelper.UpdateSuccessMessage), JsonRequestBehavior.AllowGet);
            else
                return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }
        public JsonResult FetchSubCategory(int id)
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var SubCategory = SelectListHelper.GetSelectListData(ListItems.BehaviourSubCategory, string.Format("{0},{1}", id, schoolid));
            return Json(SubCategory, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSections(string gradeId)
        {
            var result = SelectListHelper.GetSelectListData(ListItems.Section, gradeId).Select(x => new { Text = x.ItemName, Value = x.ItemId });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStudentListUsingGradeSection(long gradeId, long sectionId)
        {
            var result = studentListService.GetStudentByGradeSection(gradeId, sectionId).Select(x => new { Text = x.StudentName, Value = x.StudentId });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult GetIncidentListFilter(List<IncidentListModel> incidentLists, bool isFA)
        {
            ViewBag.isFa = isFA;
            return PartialView("~/Areas/Behaviour/Views/Incident/PartialViews/IncidentParentTable.cshtml", incidentLists);
        }
        public PartialViewResult GetIncidentStudentList(long incidentId)
        {
            var list = incidentService.GetStudentByIncidentId(incidentId);
            return PartialView("~/Areas/Behaviour/Views/Incident/PartialViews/IncidentChildTable.cshtml", list);
        }

        //#region Incident Action
        public PartialViewResult GetBehaviourAction(long IncidentId, long StudentId)
        {
            var behaviour = incidentService.GetBehaviourAction(IncidentId, StudentId);
            behaviour.IncidentId = IncidentId;
            behaviour.StudentId = StudentId;
            return PartialView("~/Areas/Behaviour/Views/Incident/PartialViews/_AddEditAction.cshtml", behaviour);
        }
        public PartialViewResult GetBehaviourActionFollowups(long incidentId, long actionId)
        {
            var list = incidentService.GetBehaviourActionFollowups(incidentId, actionId);
            return PartialView("~/Areas/Behaviour/Views/Incident/PartialViews/AddEditActionTable.cshtml", list);
        }
        public JsonResult GetFollowUpStaffList(long designationId)
        {
            var list = incidentService.GetFollowUpStaffs(SessionHelper.CurrentSession.SchoolId, designationId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        //public PartialViewResult GetBehaviourActionFollowups(long incidentId, long actionId)
        //{
        //    var list = _BehaviourService.GetBehaviourActionFollowups(incidentId, actionId);
        //    return PartialView("~/Areas/Behaviour/Views/Behaviour/PartialViews/AddEditActionTable.cshtml", list);
        //}
        public ActionResult GetBehaviourActionFollowupForm(long incidentId, long studentId)
        {

            ViewBag.designationList = new SelectList(incidentService.GetFollowUpDesignations(SessionHelper.CurrentSession.SchoolId, incidentId, SessionHelper.CurrentSession.Id), "DESIGNATION_ID", "DESIGNATION_DESCRIPTION");
            var behaviour = incidentService.GetBehaviourAction(incidentId, studentId);
            BehaviourActionFollowups behaviourActionFollowup = new BehaviourActionFollowups();
            behaviourActionFollowup.IncidentId = incidentId;
            var _behaviour = behaviour.ActionDetails.FirstOrDefault(x => x.ActionDate != null);
            if (_behaviour != null)
            {
                behaviourActionFollowup.ActionId = _behaviour.StudentInvolvedId;
                behaviourActionFollowup.ActionDate = _behaviour.ActionDate;
            }

            if (behaviourActionFollowup.ActionId == 0)
                return Json("", JsonRequestBehavior.AllowGet);
            else
                return PartialView("~/Areas/Behaviour/Views/Incident/PartialViews/_ActionForwardingForm.cshtml", behaviourActionFollowup);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveBehaviourAction(IEnumerable<ActionDetails> ActionDetails, long IncidentId, long StudentId)
        {
            var _ActionDetails = ActionDetails.ToList();
            //_ActionDetails.RemoveAll(x => x.ActionDate == null);
            var action = new ActionModel
            {
                ActionDetails = _ActionDetails,
                IncidentId = IncidentId,
                StudentId = StudentId
            };
            var nothingToSave = Common.Localization.LocalizationHelper.NoDataToSave;
            if (_ActionDetails.Count() == 0)
                return Json(new OperationDetails(false, nothingToSave), JsonRequestBehavior.AllowGet);
            var response = incidentService.ActionCUD(action);
            return Json(new OperationDetails(response) { InsertedRowId = (int)StudentId }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[CheckPermissionToUpdate, OperationAuditFilter]
        public JsonResult SaveBehaviourActionFollowUp(BehaviourActionFollowup behaviourActionFollowup)
        {
            behaviourActionFollowup.DataMode = behaviourActionFollowup.ActionDetailsId == 0 ? "Add" : "Edit";
            behaviourActionFollowup.Action_CurrentUser_DesignationId = SessionHelper.CurrentSession.Id;
            var response = incidentService.ActionFollowUpCUD(behaviourActionFollowup);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        //#endregion
        #endregion
    }
}