﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.Common.Localization;

namespace Phoenix.VLE.Web.Areas.Behaviour.Controllers
{
    public class BehaviourSetupController : BaseController
    {
        private readonly IBehaviourSetupService behaviourSetupService;
        private readonly ISchoolService schoolService;
        private readonly IIncidentService incidentService;
        private readonly ITemplateService templateService;

        public BehaviourSetupController(IBehaviourSetupService behaviourSetupService, ISchoolService schoolService, IIncidentService incidentService
            , ITemplateService templateService)
        {
            this.behaviourSetupService = behaviourSetupService;
            this.schoolService = schoolService;
            this.incidentService = incidentService;
            this.templateService = templateService;
        }
        // GET: Behaviour/BehaviourSetup
        public ActionResult Index()
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var ACD_ID = SessionHelper.CurrentSession.ACD_ID;
            var username = SessionHelper.CurrentSession.UserName;
            ViewBag.MainCategory = new SelectList(SelectListHelper.GetSelectListData(ListItems.BehaviourMainCategory), "ItemId", "ItemName");
            return View();
        }
        public ActionResult SubCategoryGrid(long MainCategoryId)
        {
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            var SubCategoryList = behaviourSetupService.GetSubCategoryList(MainCategoryId, SchoolId);

            var dataList = new object();
            dataList = new
            {
                aaData = (from item in SubCategoryList
                          select new
                          {
                              Actions = "<a data-toggle='tooltip' data-placement='bottom' href='javascript:void(0)' onclick='objBehaviourSetup.AddEditSubCategory(" + item.SubCategoryID+");'" +
                              " class='trigger-rightDrawer mr-1' data-target='BehaviourSetupDrawer' title='Edit'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a>" +
                                       "<a data-toggle='tooltip' data-placement='bottom' href = '#' title = 'Delete' class='mr-3' onclick='objBehaviourSetup.DeleteSubCategory(" + item.SubCategoryID+ ",this);'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a>",
                              item.MainCategoryName,
                              item.SubCategoryName,
                              item.GradeDisplay,
                              item.CategoryScore,
                              item.SubCategoryID,
                              item.CategoryImagePath,
                              item.IsDeletedImage,
                              item.GradeIds,
                              item.MainCategoryID,
                              item.BehaviourCategoryXml
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);

            //return PartialView("_SubCategoryGrid", SubCategoryList);
        }
        public ActionResult AddEditSubCategory(BehaviourSetup behaviourSetup)
        {
            if (!string.IsNullOrEmpty(behaviourSetup.CategoryImagePath))
            {
                behaviourSetup.CategoryImagePath = Path.Combine(PhoenixConfiguration.Instance.ReadFilePath + behaviourSetup.CategoryImagePath);
            }
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            ViewBag.MainCategory = new SelectList(SelectListHelper.GetSelectListData(ListItems.BehaviourMainCategory), "ItemId", "ItemName");
            ViewBag.GradeList = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolGrade, SchoolId), "ItemId", "ItemName");
            return PartialView("_AddEditSubCategory", behaviourSetup);
        }
        public ActionResult SaveSubCategory(BehaviourSetup behaviourSetup)
        {
            behaviourSetup.ParseJsonToXml("BehaviourCategoryXml");
            behaviourSetup.SchoolId = SessionHelper.CurrentSession.SchoolId;
            var DATAMODE = behaviourSetup.SubCategoryID > 0 ? "EDIT" : "ADD";

            var fileName = Request.Files[0].FileName;
            string relativePath = string.Empty;
            string FileName = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpPostedFileBase file = Request.Files[0];
                    string fname = file.FileName;
                    var contentLength = file.ContentLength;
                    var extension = System.IO.Path.GetExtension(file.FileName);

                    if (contentLength != 0)
                    {
                        string filePath = Path.Combine(PhoenixConfiguration.Instance.WriteFilePath + Constants.BehaviourSubCategory);
                        Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                        fname = Guid.NewGuid() + "_" + fname;
                        relativePath = Constants.BehaviourSubCategory + "/" + fname;
                        fname = Path.Combine(filePath, fname);
                        file.SaveAs(fname);
                        FileName = file.FileName;
                    }
                }
                catch (Exception e)
                {
                    return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
                }
            }
            behaviourSetup.CategoryImagePath = relativePath;
            var result = behaviourSetupService.SaveSubCategory(behaviourSetup, DATAMODE);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteSubCategory(long SubCategoryID)
        {
            BehaviourSetup behaviourSetup = new BehaviourSetup();
            behaviourSetup.SubCategoryID = SubCategoryID;
            behaviourSetup.SchoolId = SessionHelper.CurrentSession.SchoolId;
            behaviourSetup.CategoryImagePath = string.Empty;
            var result = behaviourSetupService.SaveSubCategory(behaviourSetup, "DELETE");
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        #region Action Hierarchy

        public ActionResult ActionHierarchy()
        {
            DesignationViewModel designation = new DesignationViewModel();
            designation.Designations = behaviourSetupService.GetDesignations(SessionHelper.CurrentSession.SchoolId);
            designation.SelectedDesignations = designation.Designations;
            designation.DesignationRoutes = behaviourSetupService.GetDesignationsRoutings(SessionHelper.CurrentSession.SchoolId);
            return View(designation);
        }
        public PartialViewResult GetActionHierarchyByDesignationId(long? designationId = null)
        {
            var result = behaviourSetupService.GetDesignationsRoutings(SessionHelper.CurrentSession.SchoolId, designationId);
            return PartialView("_ActionHierarchyBucketList", result);
        }

        public PartialViewResult GetActionHierarchyBuckets(List<DesignationsRouting> routings)
        {
            return PartialView("_ActionHierarchyBucketList", routings);
        }

        public JsonResult SaveActionHierarchy(List<DesignationsRouting> routings)
        {
            routings.ForEach(x => x.DesignationSchoolId = SessionHelper.CurrentSession.SchoolId);
            var result = behaviourSetupService.DesignationBySchoolCUD(new DesignationsRoutingCUD { Routings = routings });
            if (result.Any())
                return Json(result, JsonRequestBehavior.AllowGet);
            else
                return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Certificate Schedule
        public ActionResult CertificateSchedule()
        {
            ViewBag.Curriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            return View("CertificateSchedulings");
        }
        public JsonResult LoadCertificateSchedule(long curriculumId)
        {
            IEnumerable<CertificateScheduling> templateList = new List<CertificateScheduling>();
            templateList = behaviourSetupService.GetCertificateSchedulings(null, curriculumId, SessionHelper.CurrentSession.SchoolId);
            var dataList = new
            {
                aaData = (from item in templateList
                          select new
                          {
                              item.Description,
                              item.CertificateType,
                              item.Points,
                              Email = GetTemplateActionLinks(item, false, item.IsEmail),
                              Print = GetTemplateActionLinks(item, false, item.IsPrint),
                              EmailOther = GetTemplateActionLinks(item, false, item.IsEmailOther),
                              ScheduleType = StringEnum.GetStringValue((ScheduleType)item.ScheduleType).ToLower() == "yearly" 
                              ? ResourceManager.GetString("Behaviour.CertificateSchedule.Yearly") 
                              : StringEnum.GetStringValue((ScheduleType)item.ScheduleType).ToLower() == "monthly" 
                              ? ResourceManager.GetString("Behaviour.CertificateSchedule.Monthly") 
                              : StringEnum.GetStringValue((ScheduleType)item.ScheduleType).ToLower() == "weekly" 
                              ? ResourceManager.GetString("Behaviour.CertificateSchedule.Weekly") 
                              : StringEnum.GetStringValue((ScheduleType)item.ScheduleType),
                              ScheduleTypeId = item.ScheduleType,
                              Actions = GetTemplateActionLinks(item, true)
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        private string GetTemplateActionLinks(CertificateScheduling model, bool isAction, bool isCheckUncheck = false)
        {
            bool isPermissionAssigned = CurrentPagePermission.CanEdit;
            string actions;
            if (isAction)
            {
                actions = "<div class='tbl-actions'>" +
                    (isPermissionAssigned && model.IsActive ? $"<a href='javascript:void(0)' class='table-action-icon-btn' onclick=\"certificateScheduling.AddEditCertificateSchedule(this,'{EncryptDecryptHelper.EncryptUrl(model.CertificateSchedulingId.ToString())}')\" title='Edit'><img src = '/Content/vle/img/svg/pencil-big.svg' /></ a ></a>"
                  + (isPermissionAssigned && model.IsActive ? $"&nbsp; <a type='button' class='table-action-icon-btn' onclick='certificateScheduling.deleteCertificateSchedule({model.CertificateSchedulingId}); ' title='Delete'><img src = '/Content/vle/img/svg/delete.svg' /></ a ></a>" : string.Empty)
                  : string.Empty)
                   + "</div>";
            }
            else
            {
                var successIcon = "<i class='far fa-check-square fa-2x text-success'></i>";
                var dangerIcon = "<i class='far fa-window-close fa-2x text-danger'>";
                actions = isCheckUncheck ? successIcon : dangerIcon;
            }
            return actions;
        }
        public ActionResult CertificateSchedulingsCUD(string id = null)
        {
            CertificateScheduling certificateScheduling;
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            if (string.IsNullOrEmpty(id))
                certificateScheduling = new CertificateScheduling() { ScheduleType = (int)ScheduleType.Yearly };
            else
                certificateScheduling = behaviourSetupService.GetCertificateSchedulings(Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(id)), SessionHelper.CurrentSession.ACD_ID, SessionHelper.CurrentSession.SchoolId).FirstOrDefault();
            ViewBag.CertificateType = templateService.GetTemplatesBySchoolId((int)SessionHelper.CurrentSession.SchoolId, null, true, StringEnum.GetStringValue(PlanTemplateTypes.Certificate), null)
                .Select(x => new SelectListItem { Text = x.Title, Value = x.TemplateId.ToString() });
            ViewBag.StaffList = new SelectList(incidentService.GetSchoolTeachersBySchoolId(schoolId), "TeacherId", "TeacherName");
            return PartialView(certificateScheduling);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult CertificateSchedulingsCUD(CertificateScheduling certificateScheduling)
        {
            certificateScheduling.ParseJsonToXml("CertificateSchedulingXml");
            certificateScheduling.SchoolId = SessionHelper.CurrentSession.SchoolId;
            if (!certificateScheduling.IsEmailOther)
                certificateScheduling.EmailOtherStaffId = null;
            else
                certificateScheduling.EmailOtherStaffId = certificateScheduling.EmailOtherStaffId;
            certificateScheduling.CreatedBy = SessionHelper.CurrentSession.Id;
            var result = behaviourSetupService.CertificateSchedulingCUD(certificateScheduling);
            var success = result > 0;
            return Json(new OperationDetails(success) { 
                InsertedRowId = Convert.ToInt32(result) ,
                Message = success && certificateScheduling.CertificateSchedulingId == 0 ? Common.Localization.LocalizationHelper.AddSuccessMessage :
                success ? Common.Localization.LocalizationHelper.UpdateSuccessMessage : Common.Localization.LocalizationHelper.TechnicalErrorMessage
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}