﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Behaviour
{
    public class BehaviourAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Behaviour";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {

            context.MapRoute(
               "Behaviour_default",
               "Behaviour/{controller}/{action}/{id}",
                 new { action = "Index", id = UrlParameter.Optional }
           );

        }
    }
}