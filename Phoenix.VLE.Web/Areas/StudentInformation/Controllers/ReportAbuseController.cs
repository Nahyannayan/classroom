﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.StudentInformation.Controllers
{
    public class ReportAbuseController : BaseController
    {

        private readonly IAbuseDelegateService abuseDelegateService;
        private readonly ISafetyCategoriesService safetyCategoriesService;
        private readonly IStudentService _studentService;

        public ReportAbuseController(IAbuseDelegateService abuseDelegateService, ISafetyCategoriesService safetyCategoriesService, IStudentService studentService)
        {
            this.abuseDelegateService = abuseDelegateService;
            this.safetyCategoriesService = safetyCategoriesService;
            _studentService = studentService;
        }
        // GET: Student/ReportAbuse
        public ActionResult Index()
        {
            AbuseContactInfo abuseContactInfo = abuseDelegateService.GetAbuseContactInfoBySchoolId(SessionHelper.CurrentSession.SchoolId);
            ViewBag.SafetyCategories = safetyCategoriesService.GetSafetyCategories(Convert.ToInt32((int)SessionHelper.CurrentSession.SchoolId));
            return View(abuseContactInfo);
        }

        [HttpPost]
        public ActionResult SendMessage(FormCollection frm)
        {   
            var Subject = frm["Subject"];
            var Message = frm["Message"];
            string EmailBody = "";
            var student = _studentService.GetStudentByUserId(SessionHelper.CurrentSession.Id);
            IAbuseDelegateService _abuseDelegateService = new AbuseDelegateService();
            ICommonService _commonService = new CommonService();
            AbuseContactInfo abuseContactInfo = _abuseDelegateService.GetAbuseContactInfoBySchoolId(SessionHelper.CurrentSession.SchoolId);
            IEnumerable<AbuseDelegate> abuseDelegates = _abuseDelegateService.GetAbuseDelegateBySchoolId(SessionHelper.CurrentSession.SchoolId);            
            string toEmailAddrees = "";
            toEmailAddrees = abuseContactInfo.ContactEmail;
            if (abuseDelegates != null && abuseDelegates.Count() > 0)
            {
                foreach (var item in abuseDelegates)
                {
                    //toEmailAddrees = toEmailAddrees + ";" + toEmailAddrees;
                    toEmailAddrees = toEmailAddrees + "," + item.Email;
                }
            }
        
            //toEmailAddrees = toEmailAddrees.Remove(toEmailAddrees.Length - 1);
            StreamReader reader = new StreamReader(Server.MapPath(Constants.ReportAbuseEmailTemplate), Encoding.UTF8);
            EmailBody = reader.ReadToEnd();
            EmailBody = EmailBody.Replace("@@category", Subject);
            EmailBody = EmailBody.Replace("@@message", Message);
            EmailBody = EmailBody.Replace("@@studentname", student.FirstName+" "+student.LastName+" ("+SessionHelper.CurrentSession.UserName+") "+student.GradeDisplay+"/"+student.SectionName);
            SendMailView sendEmailNotificationView = new SendMailView();
            sendEmailNotificationView.FROM_EMAIL = SessionHelper.CurrentSession.Email;
            sendEmailNotificationView.TO_EMAIL = toEmailAddrees;
            sendEmailNotificationView.LOG_TYPE = "Report Abuse";
            sendEmailNotificationView.SUBJECT = "A new concern has been reported by student";
            sendEmailNotificationView.MSG = EmailBody;
            sendEmailNotificationView.LOG_USERNAME = SessionHelper.CurrentSession.UserName;
            sendEmailNotificationView.STU_ID = "11111";
            sendEmailNotificationView.LOG_PASSWORD = "tt";
            sendEmailNotificationView.PORT_NUM = "123";
            sendEmailNotificationView.EmailHostNew = "false";
            var operationalDetails = _commonService.SendEmail(sendEmailNotificationView);
            //to record email activity
            var abuseReportLedger = new AbuseReportLedger
            {
                Message = Message,
                SafetyCategoryId = safetyCategoriesService
                                    .GetSafetyCategories(Convert.ToInt32((int)SessionHelper.CurrentSession.SchoolId))
                                    .FirstOrDefault(x => x.SafetyCategoryName == Subject)
                                    .SafetyCategoryId,
                CreatedBy = (int)SessionHelper.CurrentSession.Id,
                UpdateBy = (int)SessionHelper.CurrentSession.Id,
                IsActive = true,
                IsEmail = false
            };
            if (operationalDetails.Success)
            {
                abuseReportLedger.IsEmail = true;
                operationalDetails.Message = ResourceManager.GetString("SecurityManagement.ReportAbuse.SuccessMessage");// "Your message has been send successfully.";
            }
            var isAddedInDB = abuseDelegateService.AddAbuseReportLedger(abuseReportLedger);
            return Json(new { NotificationType = operationalDetails.Success, Message = operationalDetails.Message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendSOS()
        {

            string EmailBody = "";
            IAbuseDelegateService _abuseDelegateService = new AbuseDelegateService();
            ICommonService _commonService = new CommonService();
            var student = _studentService.GetStudentByUserId(SessionHelper.CurrentSession.Id);
            AbuseContactInfo abuseContactInfo = _abuseDelegateService.GetAbuseContactInfoBySchoolId(SessionHelper.CurrentSession.SchoolId);
            IEnumerable<AbuseDelegate> abuseDelegates = _abuseDelegateService.GetAbuseDelegateBySchoolId(SessionHelper.CurrentSession.SchoolId);
            string toEmailAddrees = "";
            toEmailAddrees = abuseContactInfo.ContactEmail;
            if (abuseDelegates != null && abuseDelegates.Count() > 0)
            {
                foreach (var item in abuseDelegates)
                {
                    //toEmailAddrees = toEmailAddrees + ";" + toEmailAddrees;
                    toEmailAddrees = toEmailAddrees + "," + item.Email;
                }
            }
            //toEmailAddrees = "nikhil.kshemkalyani@neosofttech.com";
            toEmailAddrees = toEmailAddrees.Remove(toEmailAddrees.Length - 1);

            StreamReader reader = new StreamReader(Server.MapPath(Constants.ReportAbuseSOSEmailTemplate), Encoding.UTF8);
          
            EmailBody = reader.ReadToEnd();
            EmailBody = EmailBody.Replace("{StudentName}", SessionHelper.CurrentSession.FullName);
            EmailBody = EmailBody.Replace("{studentgrade}", student.GradeDisplay + "/" + student.SectionName);
            SendMailView sendEmailNotificationView = new SendMailView();
            sendEmailNotificationView.FROM_EMAIL = SessionHelper.CurrentSession.Email;
            sendEmailNotificationView.TO_EMAIL = toEmailAddrees;
            sendEmailNotificationView.LOG_TYPE = "Report Abuse";
            sendEmailNotificationView.SUBJECT = "A new concern has been reported by student";
            sendEmailNotificationView.MSG = EmailBody;
            sendEmailNotificationView.LOG_USERNAME = SessionHelper.CurrentSession.UserName;
            sendEmailNotificationView.STU_ID = "11111";
            sendEmailNotificationView.LOG_PASSWORD = "tt";
            sendEmailNotificationView.PORT_NUM = "123";
            sendEmailNotificationView.EmailHostNew = "false";
            var operationalDetails = _commonService.SendEmail(sendEmailNotificationView);
            var abuseReportLedger = new AbuseReportLedger
            {
                Message = $"Student {SessionHelper.CurrentSession.FullName} from {SessionHelper.CurrentSession.SchoolName} has requested for help.",
                SafetyCategoryId = 0,
                CreatedBy = (int)SessionHelper.CurrentSession.Id,
                UpdateBy = (int)SessionHelper.CurrentSession.Id,
                IsActive = true,
                IsEmail = false
            };
            if (operationalDetails.Success)
            {
                operationalDetails.Message = ResourceManager.GetString("SecurityManagement.ReportAbuse.SuccessMessage");//"Your message has been send successfully.";
                abuseReportLedger.IsEmail = true;
            }
            var isAddedInDB = abuseDelegateService.AddAbuseReportLedger(abuseReportLedger);
            return Json(new { NotificationType = operationalDetails.Success, Message = operationalDetails.Message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AbuseReportList()
        {
            return View();

        }

        public ActionResult LoadAbuseReportByUserGrid()
        {
            var abuseReports = abuseDelegateService.GetAbuseReportById();
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in abuseReports
                          select new
                          {
                              Message = !string.IsNullOrWhiteSpace(item.Message)?item.Message: ResourceManager.GetString("SecurityManagement.ReportAbuse.NA"),
                              CreatedOn= item.CreatedOn.FormatDate()
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteAbuseReportData(int id)
        {
            var result = abuseDelegateService.DeleteAbuseReportLedger(id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
    }
}