﻿using DevExpress.DataProcessing;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Phoenix.VLE.Web.Areas.StudentInformation.Controllers
{

    public class StudentInformationController : BaseController
    {
        private List<VLEFileType> _fileTypeList;
        private ISchoolService _SchoolService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly IStudentCertificateService _studentCertificateService;
        private readonly IStudentService _studentService;
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly ITeacherDashboardService _teacherService;
        public readonly IStudentSkillSetService _studentSkillSetService;
        private readonly IStudentAcademicService _studentAcademicService;
        private readonly IAssignmentService _assignmentService;
        private readonly IFileService _fileService;
        private readonly IStudentGoalService _studentGoalService;
        private readonly IStudentAcheivementService _studentAcheivementService;
        private readonly ISchoolBadgeService _schoolBadgeService;
        private readonly ITemplateService _templateService;
        private readonly IGetStudentInfoService _getStudentInfoService;
        private readonly IAssessmentReport _assessmentReport;
        public StudentInformationController(ISchoolService schoolService, ITeacherDashboardService teacherService,
            IStudentService studentService, IStudentSkillSetService studentSkillSetService,
            IStudentCertificateService studentCertificateService, IFileService fileService,
            IStudentAcademicService studentAcademicService, IAssignmentService assignmentService,
            IStudentGoalService studentGoalService, IStudentAcheivementService studentAcheivementService,
            ISchoolBadgeService schoolBadgeService,
            IUserPermissionService userPermissionService, ISchoolGroupService schoolGroupService, ITemplateService templateService,
            IGetStudentInfoService getStudentInfoService,
            IAssessmentReport assessmentReport
            )
        {

            _SchoolService = schoolService;
            _teacherService = teacherService;
            _studentService = studentService;
            _studentSkillSetService = studentSkillSetService;
            _studentCertificateService = studentCertificateService;
            _studentAcademicService = studentAcademicService;
            _assignmentService = assignmentService;
            _studentGoalService = studentGoalService;
            _studentAcheivementService = studentAcheivementService;
            _schoolBadgeService = schoolBadgeService;
            _userPermissionService = userPermissionService;
            _schoolGroupService = schoolGroupService;
            _fileService = fileService;
            _templateService = templateService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
            _assignmentService = assignmentService;
            _getStudentInfoService = getStudentInfoService;
            _assessmentReport = assessmentReport;
        }
        public ActionResult Index()
        {
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin || SessionHelper.CurrentSession.IsMSOAdmin)
            {
                ViewBag.schoolGroups = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();

                //ViewBag.SchoolGroups = schoolGroups.Select(i => new Phoenix.Common.Models.ListItem
                //{
                //    ItemId = i.SchoolGroupId.ToString(),
                //    ItemName = !string.IsNullOrEmpty(i.TeacherGivenName) ? i.TeacherGivenName : i.SchoolGroupDescription
                //});
            }
            else if (!SessionHelper.CurrentSession.IsTeacher())
            {
                long userId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                return RedirectToAction("PortfolioHome", new { userId = EncryptDecryptHelper.EncryptUrl(userId) });
            }

            return View();

        }
        public ActionResult GetStudentBySchoolGroup(string selectedGroupData, bool isGroupSelected = false, int pageIndex = 1, string searchString = "", string sortBy = "name")
        {
            //if (schoolGroupIds == "undefined")
            //{
            //    schoolGroupIds = "";
            //}
            List<Phoenix.Models.SchoolGroup> selectedGroupList = new List<Phoenix.Models.SchoolGroup>();
            if (!string.IsNullOrEmpty(selectedGroupData)) selectedGroupList = JsonConvert.DeserializeObject<List<Phoenix.Models.SchoolGroup>>(selectedGroupData);
            string schoolGroupIds = "";
            if (isGroupSelected)
            {
                if (selectedGroupList != null)
                {
                    schoolGroupIds = string.Join(",", selectedGroupList.Select(x => x.SchoolGroupId).ToList());
                    Session["SelectedSchoolGroups"] = selectedGroupList;
                }
                else if (Session["SelectedSchoolGroups"] != null)
                {
                    selectedGroupList = (List<Phoenix.Models.SchoolGroup>)Session["SelectedSchoolGroups"];
                    schoolGroupIds = string.Join(",", selectedGroupList.Select(x => x.SchoolGroupId).ToList());
                }
            }
            else
            {
                Session["SelectedSchoolGroups"] = null;
            }
            var model = new Pagination<Student>();
            var studentList = _SchoolService.GetStudentForTeacherBySchoolGroup((int)SessionHelper.CurrentSession.Id, pageIndex, 16, schoolGroupIds, searchString, sortBy);
            if (studentList.Any())
            {
                model = new Pagination<Student>(pageIndex, 16, studentList.ToList(), studentList.First().TotalCount);
            }
            //model.LoadPageRecordsUrl = "/StudentInformation/StudentInformation/GetStudentBySchoolGroup?pageIndex={0}&schoolGroupIds=" + schoolGroupIds + "&searchString=" + searchString;
            //model.LoadPageRecordsUrl = "/StudentInformation/StudentInformation/GetStudentBySchoolGroup?pageIndex={0}" + (isGroupSelected ? "&isGroupSelected=" + isGroupSelected : string.Empty);
            model.LoadPageRecordsUrl = "/StudentInformation/StudentInformation/GetStudentBySchoolGroup?pageIndex={0}" + (isGroupSelected ? "&isGroupSelected=" + isGroupSelected : string.Empty) + "&schoolGroupIds=" + schoolGroupIds + "&searchString=" + searchString;
            model.SearchString = searchString;
            model.PageIndex = pageIndex;
            return PartialView("_StudentList", model);
        }

        public ActionResult GetStudentBySchoolGroup_old(string schoolGroupIds, int pageIndex = 1, string searchString = "")
        {
            if (schoolGroupIds == "undefined")
            {
                schoolGroupIds = "";
            }
            var model = new Pagination<Student>();
            var studentList = _SchoolService.GetStudentForTeacherBySchoolGroup((int)SessionHelper.CurrentSession.Id, pageIndex, 16, schoolGroupIds, searchString);
            if (studentList.Any())
            {
                model = new Pagination<Student>(pageIndex, 15, studentList.ToList(), studentList.First().TotalCount);
            }
            model.LoadPageRecordsUrl = "/StudentInformation/StudentInformation/GetStudentBySchoolGroup?pageIndex={0}&schoolGroupIds=" + schoolGroupIds + "&searchString=" + searchString;
            model.SearchString = searchString;
            model.PageIndex = pageIndex;
            return PartialView("_StudentList", model);
        }

        public ActionResult PortfolioHome(string userId)
        {
            userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId.ToString() : EncryptDecryptHelper.DecryptUrl(userId);
            ViewBag.SelectedPortfolioUserId = userId;
            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;
            return View("_PortfolioHome");
        }

        public ActionResult UpdateStudentDescription(long userId)
        {
            var model = _studentService.GetStudentPortfolioInformation(userId.ToInteger()).StudentDetails;
            var editModel = new StudentEdit();
            EntityMapper<Student, StudentEdit>.Map(model, editModel);
            return PartialView("_AddEditStudentDescription", editModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveStudentDescription(StudentEdit editModel)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            bool result = _studentService.SaveStudentDescription(editModel);
            return Json(new { data = new OperationDetails(result), description = editModel.Description }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentPortfolio(long userId)
        {
            StudentPortfolio studentDetails = _studentService.GetStudentPortfolioInformation(userId.ToInteger());
            ViewBag.SelectedPortfolioUserId = userId;
            return View("_StudentPortfolio", studentDetails);
        }

        [HttpPost]
        public ActionResult UpdateStudentPortfolioSectionDetails(StudentProfileSections model)
        {
            bool result = _studentService.UpdateStudentPortfolioSectionDetails(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        #region Student Skills
        public ActionResult AddEditStudentSkills(long userId)
        {
            List<StudentSkills> lstStudentSkills = new List<StudentSkills>();
            lstStudentSkills = _studentSkillSetService.GetStudentSkillsData(userId);
            return PartialView(lstStudentSkills);
        }

        public ActionResult RefreshStudentSkillsData(long userId)
        {
            List<StudentSkills> lstStudentSkills = new List<StudentSkills>();
            return PartialView("_StudentSkills", _studentSkillSetService.GetStudentSkillsData(userId));
        }

        [HttpPost]
        public ActionResult UpdateStudentSkillsData(int studentId, int skillId, TransactionModes mode)
        {
            var operationDetails = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                operationDetails.Success = false;
                operationDetails.Message = LocalizationHelper.ActionNotPermittedMessage;
                operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }
            operationDetails = _studentSkillSetService.UpdateStudentSkillsData(studentId, skillId, mode);
            operationDetails.Message = operationDetails.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            operationDetails.NotificationType = operationDetails.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitStudentSkillForm(long? userId)
        {
            var model = new StudentSkillEdit();
            if (SessionHelper.CurrentSession.IsTeacher())
                model.UserId = userId.Value;
            else model.UserId = SessionHelper.CurrentSession.Id;
            return View("_StudentSkillsForm", model);
        }

        [HttpPost]

        public ActionResult AddEditStudentSkills(StudentSkillEdit model)
        {
            var operationDetails = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                operationDetails.Success = false;
                operationDetails.Message = LocalizationHelper.ActionNotPermittedMessage;
                operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }
            model.CreatedUserId = SessionHelper.CurrentSession.Id;
            operationDetails = _studentSkillSetService.InsertStudentSkill(model);
            operationDetails.Message = operationDetails.Success ?
                SessionHelper.CurrentSession.IsStudent() ? ResourceManager.GetString("StudentInformation.Skills.SkillSentForApproval") : LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            operationDetails.NotificationType = operationDetails.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        #endregion Student Skills

        #region Student Certificates
        public ActionResult Certificates(long userId)
        {
            ViewBag.SelectedPortfolioUserId = userId;
            ViewBag.CertificateTypes = SelectListHelper.GetSelectListData(ListItems.CertificateTypes);
            ViewBag.AllowedCertificateExtensions = _fileTypeList.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();
            return View(GetStudentCertificates(userId));
        }

        private IList<StudentCertificate> GetStudentCertificates(long userId)
        {
            IList<StudentCertificate> studentCertificatesList = new List<StudentCertificate>();
            studentCertificatesList = _studentCertificateService.GetAllStudentCertificates(userId);
            if (!SessionHelper.CurrentSession.IsTeacher())
            {
                studentCertificatesList = studentCertificatesList.Where(e => e.IsApproved).ToList();
            }
            return studentCertificatesList;
        }

        public ActionResult InitCertificateInitForm(int? certificateId, long userId)
        {
            var model = new CertificateFileEdit();
            if (certificateId.HasValue)
            {
                IList<StudentCertificate> studentCertificatesList = new List<StudentCertificate>();
                studentCertificatesList = _studentCertificateService.GetAllStudentCertificates(userId);
                var certificate = studentCertificatesList.FirstOrDefault(e => e.CertificateId == certificateId.Value);
                EntityMapper<StudentCertificate, CertificateFileEdit>.Map(certificate, model);
            }
            return PartialView("_AddEditStudentCertificate", model);
        }

        public ActionResult RefreshStudentCertificates(long userId)
        {
            return PartialView("_StudentCertificates", GetStudentCertificates(userId));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveCertificateData(CertificateFileEdit model)
        {

            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var file = new CertificateFile();
            EntityMapper<CertificateFileEdit, CertificateFile>.Map(model, file);
            bool result = false;
            if (Request.Files.Count > 0)
            {
                var certificateFile = await UploadSharePointFile(Request.Files[0]);
                string extension = Path.GetExtension(certificateFile.UploadedFileName).Replace(".", "");
                var extId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;
                file.FileTypeId = extId;
                file.UserId = SessionHelper.CurrentSession.Id;
                file.FileName = certificateFile.UploadedFileName;
                file.FilePath = certificateFile.ShareableLink;
                file.PhysicalFilePath = certificateFile.SharepointUploadedFileURL;
                result = _studentCertificateService.InsertCertificateFile(file);
                op.Success = result;
                op.Message = op.Success ? SessionHelper.CurrentSession.IsStudent() ? ResourceManager.GetString("StudentInformation.Certificates.CertificateSentForApproval") : LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
                op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteUserCertificate(StudentCertificate model)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            bool result = _studentCertificateService.DeleteUserCertificate(model);
            op.Success = result;
            op.Message = op.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateCertificateStatus(StudentCertificate model)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            bool result = _studentCertificateService.UpdateCertificateStatus(model);
            op.Success = result;
            op.Message = op.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(op, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult ApproveCertificateStatus(StudentCertificate model)
        {

            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit || !SessionHelper.CurrentSession.IsTeacher())
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            model.ApprovedBy = SessionHelper.CurrentSession.Id;
            bool result = _studentCertificateService.ApproveCertificateStatus(model);
            op.Success = result;
            op.Message = op.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(op, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetStudentAssignedCertificates(long userId)
        {
            IEnumerable<Template> templates = _templateService.GetStudentAssignedCertificates(userId, PlanTemplateTypes.Certificate);
            return PartialView("_AssignedCertificateGrid", templates);
        }

        private string GetBaseUrl()
        {
            var request = Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            var scheme = Request.IsLocal ? "http" : "https";
            if (appUrl != "/")
                appUrl = "/" + appUrl;

            var baseUrl = string.Format("{0}://{1}",
       scheme, //request.Url.Scheme gives https or http
        request.Url.Authority //request.Url.Authority gives qawithexperts/com
         ); //appUrl = /questions/111/ok-this-is-url

            return baseUrl; //this will return complete url
        }

        public ActionResult ViewStudentCertificate(int templateId, long userId)
        {
            var template = _templateService.GetTemplateDetail(templateId, (int)SessionHelper.CurrentSession.SchoolId, StringEnum.GetStringValue(PlanTemplateTypes.Certificate), string.Empty, null, true, true);
            IEnumerable<TemplateFieldMapping> mappingList = new List<TemplateFieldMapping>();
            mappingList = _templateService.GetTemplateFieldData(templateId, userId);
            string baseUri = GetBaseUrl();
            string certificateFilePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.CertificateFilesDir + "/User_" + userId;
            string filePath = Path.Combine(certificateFilePath, template.FileName);
            if (!Directory.Exists(certificateFilePath))
                Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(certificateFilePath);
            if (!System.IO.File.Exists(filePath))
                System.IO.File.Copy(PhoenixConfiguration.Instance.WriteFilePath + template.FilePath, filePath, true);

            template.JSONFileUrl = baseUri + "/pixie/";
            template.FilePath = PhoenixConfiguration.Instance.ReadFilePath + "/" + Constants.CertificateFilesDir + "/User_" + userId + "/" + template.FileName;
            string json = System.IO.File.ReadAllText(filePath);
            foreach (var item in mappingList)
            {
                json = json.Replace(item.PlaceHolder, item.ColumnData);
            }
            Common.Helpers.CommonHelper.DeleteFile(template.FilePath);
            System.IO.File.WriteAllText(filePath, json);
            ViewBag.CertificateJSON = json;
            ViewBag.CertificateTypes = SelectListHelper.GetSelectListData(ListItems.CertificateTypes);
            return PartialView("_PlainCertificate", template);
        }

        [HttpPost]
        public async Task<ActionResult> SaveTeacherAssignedCertificate(HttpPostedFileBase postedFile, TemplateFieldMapping model)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var uploadFile = await UploadSharePointFile(postedFile);
            var file = new CertificateFile();
            string extension = Path.GetExtension(uploadFile.ActualFileName).Replace(".", "");
            var extId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;
            file.CertificateDescription = model.CertificateTitle;
            file.CertificateTypeId = model.CertificateTypeId;
            file.CertificateUserId = model.UserId;
            file.TemplateId = model.TemplateId;
            file.IsAutoApproval = true;
            file.FileTypeId = extId;
            file.UserId = SessionHelper.CurrentSession.Id;
            file.FileName = model.FileName;
            file.FilePath = uploadFile.ShareableLink;
            file.PhysicalFilePath = uploadFile.SharepointUploadedFileURL;
            bool result = _studentCertificateService.InsertCertificateFile(file);
            op.Success = result;
            op.Message = op.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        #endregion Student Certificates

        #region Academics
        public ActionResult Academics(string userId)
        {
            userId = EncryptDecryptHelper.DecryptUrl(userId);
            ViewBag.SelectedPortfolioUserId = Convert.ToInt64(userId);
            var studentInfo = _studentService.GetStudentByUserId(userId.ToInteger());
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var AcademicYearList = SelectListHelper.GetSelectListData(ListItems.Assessment, string.Format("{0},{1}", SessionHelper.CurrentSession.SchoolId, studentInfo.SchoolAcademicYearId)).ToList();
            List<AssessmentdropModel> Dropmodel = new List<AssessmentdropModel>();
            foreach (var item1 in AcademicYearList)
            {
                AssessmentdropModel asm = new AssessmentdropModel();
                string AcdYear = Convert.ToDateTime(item1.ItemName).Year.ToString();
                int number = int.Parse(AcdYear);
                string number1 = (number + 1).ToString();
                asm.ItemName = AcdYear + "-" + number1;
                asm.ACDID = item1.ItemId;
                Dropmodel.Add(asm);
            }
            ViewBag.SelectedStudentNumber = studentInfo.StudentNumber;
            string firstAcademicYear = Dropmodel.Select(x => x.ACDID).FirstOrDefault();
            IEnumerable<PdfInfoModel> pdfInfoModel = _assessmentReport.GetPdfListReport(studentInfo.StudentNumber, firstAcademicYear);
            var studentRepots = _studentService.GetStudentPortfolioInformation(Convert.ToInt64(userId)).StudentReports;
            List<SelectListItem> selectList1 = new List<SelectListItem>();
            selectList1.AddRange(Dropmodel.Select(x => new SelectListItem { Value = x.ACDID.ToString(), Text = x.ItemName }).ToList());
            pdfInfoModel.ForEach(x =>
            {
                x.ShowOnDashboard = studentRepots.Any(e => e.ReportId.ToString() == x.rpF_ID);
            });
            ViewBag.AcademicYear = selectList1.DistinctBy(e => e.Text);
            return View(pdfInfoModel);
        }

        public ActionResult InitAcademicsForm(int? studentAcademicId, long userId)
        {
            var model = new AcademicDocuments();
            var studentAssignmentList = new List<AssignmentStudentDetails>();
            studentAssignmentList = _assignmentService.GetAssignmentByStudentIdWithoutPagination(userId, "").ToList().FindAll(x => x.NoofAttachment > 0 && x.CompleteMarkByTeacher == true && x.GradingTemplateId > 0).
                Select(x => new AssignmentStudentDetails
                {
                    AssignmentId = x.AssignmentId,
                    AssignmentTitle = x.AssignmentTitle,
                    AssignmentDesc = x.AssignmentDesc,
                    StudentId = x.StudentId
                }).ToList();

            ViewBag.StudentAssignments = studentAssignmentList;
            if (studentAcademicId.HasValue)
            {
                model = _studentAcademicService.GetStudentAcademicDetailById(studentAcademicId.Value, userId);
            }
            model.StudentId = studentAssignmentList.Any() ? studentAssignmentList.FirstOrDefault().StudentId : 0;
            model.AssignmentId = studentAcademicId.HasValue ? studentAcademicId.Value : 0;
            //studentAssignmentList.FirstOrDefault().AssignmentId;
            return PartialView("_StudentAcademicEdit", model);
        }

        public ActionResult GetStudentAssessmentReports(string studentNumber, string selectedYearId, long userId)
        {
            IEnumerable<PdfInfoModel> pdfInfoModel = _assessmentReport.GetPdfListReport(studentNumber, selectedYearId);
            var studentReports = _studentService.GetStudentPortfolioInformation(Convert.ToInt64(userId)).StudentReports;
            pdfInfoModel.ForEach(x => x.ShowOnDashboard = studentReports.Any(e => e.ReportId.ToString() == x.rpF_ID));
            return PartialView("_StudentAssesmentReports", pdfInfoModel);
        }


        [HttpPost]
        public ActionResult DeleteAcademicFile(AcademicDocuments document)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }

            bool result = _studentAcademicService.DeleteAcademicFile(document);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStudentAssignments(long id, int pageIndex = 1, string searchString = "")
        {
            var model = new Pagination<AssignmentFile>();
            var studentAssignmentList = _studentAcademicService.GetStudentAcademicAssignments(id, searchString);
            if (studentAssignmentList.Any())
                model = new Pagination<AssignmentFile>(pageIndex, 1000, studentAssignmentList.ToList(), studentAssignmentList.ToList().Count());
            model.LoadPageRecordsUrl = "/StudentInformation/StudentInformation/GetStudentAssignments?pageIndex={0}&id=" + id + "&searchString=" + searchString;
            model.PageIndex = pageIndex;
            model.SearchString = searchString;
            return PartialView("_StudentAssignments", model);
        }

        public ActionResult ViewAssignmentFiles(int assignmentId, long userId)
        {
            var details = _studentService.GetStudentPortfolioInformation(userId);
            return PartialView("_StudentAcademicFiles", details.Academics.Where(e => e.AssignmentId == assignmentId).ToList());
        }


        public ActionResult GetStudentAssignmnetFiles(AssignmentStudentDetails model)
        {
            var stduentAssignmentFiles = _assignmentService.GetStudentAssignmentFiles(model.StudentId, model.AssignmentId);
            return PartialView("_StudentAssignmentFiles", stduentAssignmentFiles);
        }

        [HttpPost]
        public ActionResult SaveAssignmentFiles(string selectedAssignmentFiles)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            List<Phoenix.Models.AssignmentFile> lstSelectedAssignmentFiles = new List<Phoenix.Models.AssignmentFile>();
            if (!string.IsNullOrEmpty(selectedAssignmentFiles)) lstSelectedAssignmentFiles = JsonConvert.DeserializeObject<List<Phoenix.Models.AssignmentFile>>(selectedAssignmentFiles);
            bool result = _assignmentService.SaveAssignmentFiles(lstSelectedAssignmentFiles);
            op.Success = result;
            op.Message = op.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(op, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteAcademicAssignment(AssignmentStudentDetails model)
        {
            bool status = _studentAcademicService.DeleteAcademicAssignment(model);
            return Json(new OperationDetails(status), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAcademicFileDashboardStatus(AcademicDocuments documentModel)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            bool result = _studentAcademicService.ToggleDashboardAcademicFiles(documentModel);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAssignmentStatus(AssignmentStudentDetails model)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            bool result = _studentAcademicService.UpdateAssignmentStatus(model);
            op.Success = result;
            op.Message = op.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAllAssignmentStatus(string selectedAssignments, string selectedReports)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            List<AssignmentStudentDetails> lstSelectedAssignments = new List<AssignmentStudentDetails>();
            if (!string.IsNullOrEmpty(selectedAssignments)) lstSelectedAssignments = JsonConvert.DeserializeObject<List<AssignmentStudentDetails>>(selectedAssignments);
            bool result = _studentAcademicService.UpdateAllAssignmentStatus(lstSelectedAssignments);
            if (lstSelectedAssignments.Any())
                op.HeaderText = $"/StudentInformation/StudentInformation/PortfolioHome?userId={EncryptDecryptHelper.EncryptUrl(lstSelectedAssignments.FirstOrDefault().UserId)}";
            if (!string.IsNullOrEmpty(selectedReports))
            {
                List<StudentAssessmentReportView> studentAssessment = new List<StudentAssessmentReportView>();
                studentAssessment = JsonConvert.DeserializeObject<List<StudentAssessmentReportView>>(selectedReports);
                result = _studentAcademicService.UpdateAssessmentReportStatus(studentAssessment);
                op.HeaderText = studentAssessment.Any() ? $"/StudentInformation/StudentInformation/PortfolioHome?userId={EncryptDecryptHelper.EncryptUrl(studentAssessment.FirstOrDefault().UserId)}" : op.HeaderText;
            }
            op.Success = result;
            op.Message = op.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStudentAcademicFiles(long userId, char fileType)
        {
            var files = _studentAcademicService.GetStudentAcademicData(userId).Documents;
            if (fileType == 'V')
                files = files.Where(e => e.DocumentType.Equals("Video", StringComparison.OrdinalIgnoreCase)).ToList();
            else
                files = files.Where(e => !e.DocumentType.Equals("Video", StringComparison.OrdinalIgnoreCase)).ToList();
            return PartialView("_AcademicDocuments", files);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveAcademicFiles(FileEdit model, char fileType, long userId)
        {

            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var file = new Phoenix.Models.File();
            EntityMapper<FileEdit, Phoenix.Models.File>.Map(model, file);
            bool result = false;
            if (model.PostedFile != null)
            {
                string fileName = Guid.NewGuid() + "_" + model.PostedFile.FileName;
                string extension = Path.GetExtension(model.PostedFile.FileName).Replace(".", "");
                string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.AcademicFilesDir + "/" + (fileType == 'V' ? "Videos" : "Others") + "/User_" + userId;
                Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                string relativePath = Constants.AcademicFilesDir + "/" + (fileType == 'V' ? "Videos" : "Others") + "/User_" + userId + "/" + fileName;
                fileName = Path.Combine(filePath, fileName);
                var extId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;
                model.PostedFile.SaveAs(fileName);
                file.FileTypeId = extId;
                file.CreatedBy = SessionHelper.CurrentSession.Id;
                file.FileName = model.PostedFile.FileName;
                file.FilePath = relativePath;
                result = _studentAcademicService.InsertAcademicFile(file, userId);
                op.Success = result;
                op.Message = op.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
                op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Goals
        public ActionResult Goals(string userId)
        {
            userId = EncryptDecryptHelper.DecryptUrl(userId);
            ViewBag.SelectedPortfolioUserId = Convert.ToInt64(userId);
            return View();
        }

        public ActionResult GetAllStudentGoals(long userId, int pageIndex = 1, string searchString = "")
        {
            if (searchString == "undefined")
            {
                searchString = "";
            }
            var model = new Pagination<StudentGoal>();
            var studentGoals = _studentGoalService.GetAllStudentGoals(pageIndex, 12, userId, SessionHelper.CurrentSession.Id, searchString);
            if (studentGoals.Any())
            {
                model = new Pagination<StudentGoal>(pageIndex, 12, studentGoals.ToList(), studentGoals.First().TotalCount);
            }
            model.LoadPageRecordsUrl = "/StudentInformation/StudentInformation/GetAllStudentGoals?pageIndex={0}&userId=" + userId + "&searchString=" + searchString;
            model.PageIndex = pageIndex;
            model.SearchString = searchString;
            return PartialView("_StudentGoals", model);
        }

        [HttpPost]
        public ActionResult DeleteStudentGoals(long goalId)
        {
            var result = _studentGoalService.DeleteStudentGoal(goalId);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveStudentGoal(StudentGoal model)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit || !SessionHelper.CurrentSession.IsTeacher())
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            model.CreatedUserId = SessionHelper.CurrentSession.Id;

            bool result = _studentGoalService.ApproveStudentGoal(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult UpdateDashboardGoalsStatus(StudentGoal goalModel)
        {
            var op = new OperationDetails();
            goalModel.CreatedUserId = SessionHelper.CurrentSession.Id;
            bool result = _studentGoalService.UpdateDashboardGoalsStatus(goalModel);
            op.Success = result;
            op.Message = op.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAllDashboardGoalsStatus(string selectedGoals)
        {
            var op = new OperationDetails();
            List<Phoenix.Models.StudentGoal> lstSeletedGoals = new List<Phoenix.Models.StudentGoal>();
            if (!string.IsNullOrEmpty(selectedGoals)) lstSeletedGoals = JsonConvert.DeserializeObject<List<Phoenix.Models.StudentGoal>>(selectedGoals);
            bool result = _studentGoalService.UpdateAllDashboardGoalsStatus(lstSeletedGoals);
            op.Success = result;
            op.Message = op.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            op.HeaderText = lstSeletedGoals.Any() ? $"/studentinformation/studentinformation/PortfolioHome?userId={EncryptDecryptHelper.EncryptUrl(lstSeletedGoals.FirstOrDefault().UserId)}" : string.Empty;
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitStudentGoalForm(long? goalId, long userId)
        {
            StudentGoalEdit studentGoal = new StudentGoalEdit();
            if (goalId.HasValue)
            {
                var model = _studentGoalService.GetStudentGoalById(goalId.Value);
                EntityMapper<StudentGoal, StudentGoalEdit>.Map(model, studentGoal);
            }
            if (SessionHelper.CurrentSession.IsStudent())
                ViewBag.StudentGroupTeachers = SelectListHelper.GetSelectListData(ListItems.StudentGroupTeacher, userId.ToString());
            return PartialView("_StudentGoalsForm", studentGoal);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveStudentGoals(StudentGoalEdit editModel)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                editModel.TeacherId = SessionHelper.CurrentSession.Id;
            editModel.CreatedUserId = SessionHelper.CurrentSession.Id;
            bool result = _studentGoalService.InsertStudentGoal(editModel);
            op.Success = result;
            op.Message = result ? SessionHelper.CurrentSession.IsStudent() ? ResourceManager.GetString("StudentInformation.Goals.GoalSentForApproval") :
                    LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = result ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Student Endorsement
        public ActionResult Endorsement(string userId)
        {
            userId = EncryptDecryptHelper.DecryptUrl(userId);
            long selectedUserId = Convert.ToInt64(userId);
            ViewBag.SelectedPortfolioUserId = selectedUserId;
            SkillEndorsementEdit editModel = new SkillEndorsementEdit();
            editModel.StudentUserId = selectedUserId;
            ViewBag.StudentGroupTeachers = SelectListHelper.GetSelectListData(ListItems.StudentGroupTeacher, userId.ToString());
            ViewBag.StudentSkills = _studentSkillSetService.GetStudentSkillsData(selectedUserId).Select(s => new Phoenix.Common.Models.ListItem { ItemId = s.SkillId.ToString(), ItemName = s.SkillName }).ToList();
            return View(editModel);
        }


        public ActionResult InitSkillEndorsementForm(int? skillEndorsementId, long userId)
        {
            SkillEndorsementEdit editModel = new SkillEndorsementEdit();
            ViewBag.StudentGroupTeachers = SelectListHelper.GetSelectListData(ListItems.StudentGroupTeacher, userId.ToString());
            ViewBag.StudentSkills = _studentSkillSetService.GetStudentSkillsData(userId).Select(s => new Phoenix.Common.Models.ListItem { ItemId = s.SkillId.ToString(), ItemName = s.SkillName }).ToList();
            if (skillEndorsementId.HasValue)
            {
                var endorsedSkill = _studentSkillSetService.GetStudentEndorsedSkillById(skillEndorsementId.Value);
                EntityMapper<SkillEndorsement, SkillEndorsementEdit>.Map(endorsedSkill, editModel);
                editModel.SkillIds = new string[] { endorsedSkill.SkillId.ToString() };
            }
            return PartialView("_StudentSkillEndorsementForm", editModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveEndorsementDetails(SkillEndorsementEdit model)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                model.TeacherId = SessionHelper.CurrentSession.Id;
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            bool result = _studentSkillSetService.UpdateStudentSkillEndorsementDetails(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateEndorsementDetails(SkillEndorsementEdit model)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            //bool result = _studentSkillSetService.UpdateStudentSkillEndorsementDetails(model);
            return Json(new OperationDetails(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateStudentEndorsementStatus(SkillEndorsement model)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            op.Success = _studentSkillSetService.UpdateStudentEndorsementStatus(model);
            return Json(new OperationDetails(op.Success), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllEndorsedSkills(long userId, int pageIndex = 1, string searchString = "")
        {
            var model = new Pagination<SkillEndorsement>();
            ViewBag.SelectedPortfolioUserId = userId.ToString();
            var skillsList = _studentSkillSetService.GetAllStudentEndorsedSkills(userId, pageIndex, 15, searchString);
            var uniqueSkillsList = skillsList.FindAll(x => !string.IsNullOrEmpty(x.SkillId.ToString())).Select(x => new { SkillId = x.SkillId }).Distinct().ToList();
            if (skillsList.Any())
            {
                model = new Pagination<SkillEndorsement>(pageIndex, 15, skillsList.ToList(), skillsList.FirstOrDefault().TotalCount);
            }
            model.LoadPageRecordsUrl = "/StudentInformation/StudentInformation/GetAllEndorsedSkills?pageIndex={0}&userId=" + userId + "&searchString=" + searchString;
            model.PageIndex = pageIndex;
            model.SearchString = searchString;
            return PartialView("_EndorsedSkill", model);
        }

        [HttpPost]
        public ActionResult DeleteEndorsedSkill(SkillEndorsement model)
        {
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            bool result = _studentSkillSetService.DeleteEndorsedSkill(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAllEndorsedSkillStatus(string selectedSkills)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            List<Phoenix.VLE.Web.EditModels.SkillEndorsementEdit> lstSeletedSkills = new List<Phoenix.VLE.Web.EditModels.SkillEndorsementEdit>();
            if (!string.IsNullOrEmpty(selectedSkills)) lstSeletedSkills = JsonConvert.DeserializeObject<List<Phoenix.VLE.Web.EditModels.SkillEndorsementEdit>>(selectedSkills);
            bool result = _studentSkillSetService.UpdateAllEndorsedSkillStatus(lstSeletedSkills);
            op = new OperationDetails(result);
            op.HeaderText = lstSeletedSkills.Any() ? $"/studentinformation/studentinformation/PortfolioHome?userId={EncryptDecryptHelper.EncryptUrl(lstSeletedSkills.FirstOrDefault().StudentUserId)}" : string.Empty;
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateSkillRatingAndDashboardStatus(SkillEndorsementEdit editModel)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }

            bool result = _studentSkillSetService.UpdateEndorseSkillStatusRating(editModel);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region  Achievements
        public ActionResult Achievements(string userId)
        {
            userId = EncryptDecryptHelper.DecryptUrl(userId);
            ViewBag.AllowedCertificateExtensions = _fileTypeList.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();
            ViewBag.SelectedPortfolioUserId = Convert.ToInt64(userId);
            return View();
        }
        public ActionResult InitStudentAchievementFrom(long? acheivementId, long userId)
        {
            StudentAchievementEdit studentAchievementEdit = new StudentAchievementEdit();
            if (acheivementId.HasValue)
            {
                studentAchievementEdit.AchievementId = acheivementId.Value;
                var acheivement = _studentAcheivementService.GetStudentachevementById(acheivementId.Value);
                EntityMapper<StudentAchievement, StudentAchievementEdit>.Map(acheivement, studentAchievementEdit);
            }
            if (SessionHelper.CurrentSession.IsStudent())
                ViewBag.StudentGroupTeachers = SelectListHelper.GetSelectListData(ListItems.StudentGroupTeacher, userId.ToString());
            studentAchievementEdit.AchievementId = acheivementId.HasValue ? acheivementId.Value : 0;
            return PartialView("_StudentAchievementForm", studentAchievementEdit);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveStudentAchievement(StudentAchievementEdit editModel)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                editModel.TeacherId = SessionHelper.CurrentSession.Id;
            editModel.CreatedBy = SessionHelper.CurrentSession.Id;
            editModel.StudentId = editModel.UserId;
            StudentAchievement model = new StudentAchievement();
            EntityMapper<StudentAchievementEdit, StudentAchievement>.Map(editModel, model);
            bool result = false;
            model.lstAchievementFiles = new List<AchievementFiles>();
            if (Request.Files != null && Request.Files.Count > 0)
            {
                foreach (string name in Request.Files)
                {
                    HttpPostedFileBase item = Request.Files[name];
                    model.lstAchievementFiles.Add(await UploadAcheivementFile(item));

                }
            }
            result = _studentAcheivementService.InsertStudentAchievement(model);
            op.Success = result;
            op.Message = op.Success ? SessionHelper.CurrentSession.IsStudent() ? ResourceManager.GetString("StudentInformation.Achievements.AchievementSentForApproval") : LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        private async Task<AchievementFiles> UploadAcheivementFile(HttpPostedFileBase file)
        {
            var uploadedFileDetails = await UploadSharePointFile(file);

            string extension = Path.GetExtension(uploadedFileDetails.UploadedFileName).Replace(".", "");
            return new AchievementFiles()
            {
                FileName = uploadedFileDetails.ActualFileName,
                FilePath = uploadedFileDetails.ShareableLink,
                PhysicalFilePath = uploadedFileDetails.SharepointUploadedFileURL
            };
        }

        [HttpPost]
        public ActionResult DeleteAcheivement(StudentAchievement model)
        {
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            bool success = _studentAcheivementService.DeleteAcheivement(model);
            return Json(new OperationDetails(success), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteAcheivementFile(AchievementFiles acheivementFile)
        {
            var filePath = PhoenixConfiguration.Instance.WriteFilePath + acheivementFile.FilePath;
            bool success = _studentAcheivementService.DeleteAcheivementFile(acheivementFile);
            if (success)
            {
                Common.Helpers.CommonHelper.DeleteFile(filePath);
            }
            return Json(new OperationDetails(success), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> InsertAchievementFile(HttpPostedFileBase postedFile, AchievementFiles acheivementFile)
        {
            var file = await UploadAcheivementFile(postedFile);
            acheivementFile.FilePath = file.FilePath;
            acheivementFile.FileName = file.FileName;
            acheivementFile.PhysicalFilePath = file.PhysicalFilePath;
            acheivementFile.CreatedBy = SessionHelper.CurrentSession.Id;
            var success = _studentAcheivementService.InsertAchievementFile(acheivementFile);
            return Json(new OperationDetails(success), JsonRequestBehavior.AllowGet);
        }

        public ActionResult RefreshStudentAchievements(long userId)
        {
            return PartialView("_StudentAchievements", GetStudentAchievementsWithFiles(userId));
        }

        private List<StudentIncident> GetStudentAchievements(long userId, BehaviourCategoryTypes type, int pageIndex, int pageSize, string searchString)
        {
            return _studentAcheivementService.GetStudentAchievements(userId, type, pageIndex, pageSize, searchString);
        }

        public ActionResult GetStudentAchievementsWithFiles(long userId, int pageIndex = 1, string searchString = "")
        {
            var certificates = GetStudentCertificates(userId);
            ViewBag.StudentCertificates = string.IsNullOrEmpty(searchString) ? certificates : certificates.Where(e => e.CertificateDescription.ToLower().Contains(searchString.ToLower())).ToList();
            var model = new Pagination<StudentAchievement>();
            var studentAchievement = _studentAcheivementService.GetStudentAchievementsWithFiles(userId, pageIndex, 1000, searchString);

            if (studentAchievement.Any())
            {
                model = new Pagination<StudentAchievement>(pageIndex, 1000, studentAchievement.ToList(), studentAchievement.FirstOrDefault().TotalCount);
            }
            model.LoadPageRecordsUrl = "/StudentInformation/StudentInformation/GetStudentAchievementsWithFiles?pageIndex={0}&userId=" + userId + "&searchString=" + searchString;
            model.PageIndex = pageIndex;
            model.SearchString = searchString;
            ViewBag.PortfolioUserId = userId; ;
            return PartialView("_StudentAchievements", model);
        }

        [HttpPost]
        public ActionResult UpdateAchievementsPortfolioStatus(string selectedAchievements)
        {
            var op = new OperationDetails();
            List<Phoenix.Models.StudentAchievement> lstSelectedAchievements = new List<Phoenix.Models.StudentAchievement>();
            if (!string.IsNullOrEmpty(selectedAchievements)) lstSelectedAchievements = JsonConvert.DeserializeObject<List<Phoenix.Models.StudentAchievement>>(selectedAchievements);
            bool result = _studentAcheivementService.UpdateAchievementsPortfolioStatus(lstSelectedAchievements);
            op.Success = result;
            op.HeaderText = lstSelectedAchievements.Any() ? $"/studentinformation/studentinformation/PortfolioHome?userId={EncryptDecryptHelper.EncryptUrl(lstSelectedAchievements.FirstOrDefault().StudentId)}" : string.Empty;
            op.Message = op.Success ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            op.NotificationType = op.Success ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAcheivementApprovalStatus(StudentAchievement model)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            bool result = _studentAcheivementService.UpdateAcheivementApprovalStatus(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStudentMertitData(long userId, BehaviourCategoryTypes type, int pageIndex = 1, string searchString = "")
        {
            List<StudentIncident> lst = GetStudentAchievements(userId, type, pageIndex, 5, searchString);
            var model = new Pagination<StudentIncident>();
            if (lst.Any())
            {
                model = new Pagination<StudentIncident>(pageIndex, 5, lst, lst.FirstOrDefault().TotalCount);
            }
            model.LoadPageRecordsUrl = "/StudentInformation/StudentInformation/GetStudentMertitData?pageIndex={0}&userId=" + userId + "&type=" + (short)type;
            model.PageIndex = pageIndex;
            return PartialView("_StudentMerits", model);
        }



        [HttpPost]
        public ActionResult UpdateAcheivementDashboardStatus(StudentIncident model)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }

            bool result = _studentAcheivementService.UpdateAcheivementDashboardStatus(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Student Badges
        public ActionResult InitStudentBadgesForm(long userId)
        {
            List<SchoolBadge> studentBadgesList = new List<SchoolBadge>();
            studentBadgesList = _schoolBadgeService.GetUserBadges(userId).ToList();
            return PartialView("_StudentBadgesForm", studentBadgesList);
        }

        [HttpPost]
        public ActionResult SaveStudentBadges(string badgeIds, long userId)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit || !CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var studentBadges = new SchoolBadge()
            {
                BadgesIds = badgeIds,
                UserId = userId,
                AssignedBy = SessionHelper.CurrentSession.Id
            };
            bool result = _schoolBadgeService.SaveStudentBadges(studentBadges);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetStudentBadges(long userId)
        {
            return PartialView("_StudentBadges", _studentService.GetStudentProfileDetails(userId).StudentBadges);
        }
        #endregion

        #region  Group Wise Student Badges
        public ActionResult InitGroupWiseStudentBadgesForm()
        {
            int schoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
            List<SchoolBadge> studentBadgesList = new List<SchoolBadge>();
            studentBadgesList = _schoolBadgeService.GetBadgesBySchoolId(schoolId).ToList();
            return PartialView("_SchoolBadgesForm", studentBadgesList);
        }

        [HttpPost]
        public ActionResult SaveGroupWiseStudentBadges(string schoolGroupIds, string badgeIds, long userId)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit || !CurrentPagePermission.CanEdit)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var studentBadges = new StudentBadge()
            {
                schoolGroupIds = schoolGroupIds,
                badgesIds = badgeIds,
                userId = userId,
                CreatedBy = SessionHelper.CurrentSession.Id
            };
            bool result = _schoolBadgeService.SaveGroupWiseStudentBadges(studentBadges);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult DownloadFile(string id)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(id))
                {
                    var filePath = EncryptDecryptHelper.DecryptUrl(id);
                    WebClient myWebClient = new WebClient();
                    byte[] myDataBuffer = myWebClient.DownloadData(filePath);
                    Response.BufferOutput = true;

                    return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(filePath));

                }
            }
            catch
            {
                throw new FileNotFoundException();
            }
            return HttpNotFound();
        }

        public ActionResult ShowProfileSections(long studentId)
        {
            //IEnumerable<StudentProfileSections> lstStudentProfileSections = new List<StudentProfileSections>();
            //studentId = Convert.ToInt32(SessionHelper.CurrentSession.StudentId);
            ViewBag.SelectedPortfolioUserId = studentId;
            var StudentPortfolioInformation = _studentService.GetStudentPortfolioInformation(studentId);
            return PartialView("_PortfolioNavigation", StudentPortfolioInformation);
        }

        #region About Me
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveStudentAboutMe(SchoolBadge badgeDetails)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            badgeDetails.AssignedBy = SessionHelper.CurrentSession.Id;
            bool result = _studentService.SaveStudentAboutMe(badgeDetails);
            op = new OperationDetails(result);
            op.HeaderText = $"/studentinformation/studentinformation/PortfolioHome?userId={EncryptDecryptHelper.EncryptUrl(badgeDetails.UserId)}";
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AboutMe(string userId)
        {
            userId = EncryptDecryptHelper.DecryptUrl(userId);
            var studentDetails = _studentService.GetStudentProfileDetails(userId.ToInt64());
            ViewBag.FileExtension = _fileTypeList.Where(e => e.DocumentType.Equals("image", StringComparison.OrdinalIgnoreCase)).Select(r => r.Extension).ToList();
            return View(studentDetails);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveStudentProfileImages(HttpPostedFileBase postedFile, bool isCoverPhoto, int userId)
        {
            string fileName = Guid.NewGuid() + "_" + postedFile.FileName;
            string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.StudentProfilePath + SessionHelper.CurrentSession.SchoolId.ToString();
            Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
            Phoenix.Common.Helpers.CommonHelper.CreateDestinationFolder(filePath + $"/{userId}");
            string relativePath = Constants.StudentProfilePath + SessionHelper.CurrentSession.SchoolId.ToString() + $"/{userId}/{fileName}";
            fileName = Path.Combine(filePath, userId.ToString(), fileName);
            postedFile.SaveAs(fileName);
            filePath = relativePath;
            if (string.IsNullOrWhiteSpace(filePath)) return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
            Student model = new Student { StudentImage = filePath, IsCoverPhoto = isCoverPhoto, Id = userId };
            bool success = _studentService.SaveStudentProfileImages(model);
            var op = new OperationDetails(success);
            op.HeaderText = PhoenixConfiguration.Instance.ReadFilePath + filePath;
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitStudentCoverForm(long userId)
        {
            var studentDetails = _studentService.GetStudentProfileDetails(userId);
            return PartialView("_StudentProfileCover", studentDetails);
        }
        #endregion

        [HttpPost]
        public ActionResult ViewAllFiles(string assignmentFileList)
        {
            List<Phoenix.Models.AcademicDocuments> _assignmentFileList = JsonConvert.DeserializeObject<List<AcademicDocuments>>(assignmentFileList);
            return PartialView("_StudentAcademicFiles", _assignmentFileList);
        }

        #region Common Methods
        private async Task<SharePointFileView> UploadSharePointFile(HttpPostedFileBase file)
        {
            try
            {
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    return await azureHelper.UploadStudentFilesAsPerModuleAsync(FileModulesConstants.StudentPortfolio, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    return SharePointHelper.UploadStudentFilesAsPerModule(ref cx, FileModulesConstants.StudentPortfolio, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                }
            }
            catch (Exception ex)
            {
                return new SharePointFileView();
            }
        }

        public ActionResult GetCurrentStudentDetails(bool isTCRequest = false)
        {
            StudentInfo details = _getStudentInfoService.GetStudentInfo(SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber.ToString(), "En", "School");
            StudentDetailsEdit studentDetailsEdit = new StudentDetailsEdit();
            studentDetailsEdit.Religion = details.STU_RELIGION;
            studentDetailsEdit.DOB = details.STU_DOB.ToString();
            studentDetailsEdit.Name = details.STU_FullName;
            studentDetailsEdit.gender = details.STU_GENDER;
            studentDetailsEdit.Nationality = details.STU_NATIONALITY;
            studentDetailsEdit.StudentNumber = details.StuId;
            studentDetailsEdit.Nationality2 = details.STU_NATIONALITY;
            studentDetailsEdit.Grade = details.STU_GRADE;
            studentDetailsEdit.ID = details.StuId;
            studentDetailsEdit.CountryOfBirth = details.STU_NATIONALITY;
            studentDetailsEdit.PhotoPath = details.STU_PHOTOPATH;
            studentDetailsEdit.School = details.SCHOOL_NAME;
            studentDetailsEdit.EmailID = details.STU_EMAIL;
            studentDetailsEdit.PrimaryContact = details.STU_PRIMARYCONTACT;
            studentDetailsEdit.MobNo = details.PAR_MOBILE;
            studentDetailsEdit.IsTCRequest = isTCRequest;
            return PartialView("_GetStudentDetails", studentDetailsEdit);

        }
        #endregion


    }

}
