﻿using Phoenix.Common.Helpers;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.MigrationJob.Controllers
{
    public class MigrationJobController : Controller
    {
        private IMigrationJobService _IMigrationJobService;
        public MigrationJobController(IMigrationJobService MigrationJobService)
        {
            _IMigrationJobService = MigrationJobService;
        }
        // GET: MigrationJob/MigrationJob
        public ActionResult Index()
        {
            var ReportList = _IMigrationJobService.GetMigrationDetailList(SessionHelper.CurrentSession.SchoolId).OrderBy(x => x.SyncDetail).ToList();
            var GroupList = ReportList.GroupBy(e => new { e.SyncCategory }).Select(f => new MigrationDetail { SyncCategory = f.Key.SyncCategory }).OrderBy(x => x.SyncCategory).ToList();
            var CountList = _IMigrationJobService.GetMigrationDetailCountList().OrderBy(x => x.SyncDetailId).ToList();
            ViewBag.ReportList = ReportList;
            ViewBag.GroupList = GroupList;
            ViewBag.CountList = CountList;
            return View();

        }
        public ActionResult GetSyncDetailbyId(int id)
        {
            SyncDetailById obj = new SyncDetailById();
            obj.SyncDetailId = id;
            obj.SchoolId = SessionHelper.CurrentSession.SchoolId;
            obj.userid = SessionHelper.CurrentSession.Id;


            var result = _IMigrationJobService.GetSyncDetailbyId(obj);
            return Json(result, JsonRequestBehavior.AllowGet);
            //return Json(new OperationDetails(result)
            //{
            //    Message = result && !schoolUnit.IsActive ? LocalizationHelper.DeleteSuccessMessage :
            //    result && schoolUnit.UnitDetailTypeId != null ? LocalizationHelper.UpdateSuccessMessage :
            //    result && schoolUnit.UnitDetailTypeId == null ? LocalizationHelper.AddSuccessMessage :
            //    LocalizationHelper.TechnicalErrorMessage
            //}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMigrationDetailCount(int id)
        {
            var CountList = _IMigrationJobService.GetMigrationDetailCountList().Where(x=>x.SyncDetailId== id).OrderByDescending(y=>y.SyncDate).FirstOrDefault();
            return Json(CountList, JsonRequestBehavior.AllowGet);
        }

    }
}