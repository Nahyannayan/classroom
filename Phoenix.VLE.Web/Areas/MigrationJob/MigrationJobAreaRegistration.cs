﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.MigrationJob
{
    public class MigrationJobAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MigrationJob";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MigrationJobt_default",
                "MigrationJob/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}