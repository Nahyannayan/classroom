﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Attendance.Controllers
{
    public class AttendanceController : BaseController
    {
        private IAttendanceService _AttendanceService;
        private IClassListService _ClassListService;
        private ISIMSCommonService _SIMSCommonService;
        private IAttendanceSettingService _attendanceSettingService;
        public AttendanceController(IAttendanceService AttendanceService, IClassListService ClassListService, ISIMSCommonService SIMSCommonService, ITimeTableService timeTableService, IAttendanceSettingService attendanceSettingService)
        {
            _AttendanceService = AttendanceService;
            _ClassListService = ClassListService;
            _SIMSCommonService = SIMSCommonService;

            _attendanceSettingService = attendanceSettingService;


        }

        // GET: Attendance/Attendance
        public ActionResult Index1(string strtt_id = "", string entrydate = null, string grade = null, string section = null)
        {

            int tt_id = 0;
            if (!string.IsNullOrWhiteSpace(strtt_id))
            {
                tt_id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
            }

            if (!string.IsNullOrWhiteSpace(entrydate))
            {
                entrydate = EncryptDecryptHelper.Decrypt(entrydate);
            }

            ViewBag.TTM_Id = tt_id;
            ViewBag.EntryDate = entrydate;
            ViewBag.GRD_Id = grade;
            ViewBag.SCT_Id = section;
            if (tt_id != 0)
            {
                ViewBag.IsFromTT = "1";
            }
            else
            {
                ViewBag.IsFromTT = "0";
            }
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;

            var list = _AttendanceService.GetGradeSectionAccesses(schoolid, SessionHelper.CurrentSession.ACD_ID, username, isSuperUser, grd_access, "");
            //var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, SessionHelper.CurrentSession.ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list.Any() ? list.Select(x => new SelectListItem { Text = x.GradeDescription, Value = x.GradeId }) : new List<SelectListItem>();
            ViewBag.AttendanceType = new SelectList(SelectListHelper.GetSelectListData(ListItems.AttendanceType, SessionHelper.CurrentSession.ACD_ID), "ItemId", "ItemName");


            if (Session["SIMSTimeTableSchedule"] != null)
            {
                strtt_id = Convert.ToString(Session["SIMSTimeTableSchedule"]);
                entrydate = Convert.ToString(Session["SIMSTimeTableEntryDate"]);
            }
            if (strtt_id != "0" && strtt_id != "")
            {
                ViewBag.TTM_Id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
                ViewBag.EntryDate = Convert.ToString(EncryptDecryptHelper.Decrypt(entrydate));
                ViewBag.IsFromTT = "1";
                // return RedirectToAction("GetStudentList", "Attendance", new { tt_id = tt_Id });
            }

            return View("StudentList");
        }

        [HttpGet]
        public JsonResult GetStudentList(int tt_id = 0, string entrydate = null, string grade = null, string section = null, string attendanceType = "Session1")
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var list = _AttendanceService.GetAttendanceByIdAndDate(SessionHelper.CurrentSession.ACD_ID, tt_id, username, entrydate, grade, section, attendanceType).OrderBy(e => e.Student_Name);
            list.ToList().ForEach(l => { l.ALG_ID = string.IsNullOrEmpty(l.ALG_ID) ? "0" : l.ALG_ID; });
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertAttendanceDetails(string student_xml, string entry_date, int alg_id, int ttm_id = 0, string sct_id = "", string GRD_ID = "", long SHF_ID = 0, long STM_ID = 0, string AttendanceType = "")
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var result = _AttendanceService.InsertAttendanceDetails(student_xml, entry_date, username, alg_id, ttm_id, sct_id, GRD_ID, SessionHelper.CurrentSession.ACD_ID, schoolid, SHF_ID, STM_ID, AttendanceType);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult StudentHistory(int id)
        {
            var model = new MarkedAttendaceDetails();
            ViewBag.HistoryType = "W";
            string date = String.Format("{0:dd-MMM-yyyy}", DateTime.Now.Date);

            model.StudentDetail = _ClassListService.GetStudentDetails(id.ToString());
            ViewBag.Stu_Acd_Year = model.StudentDetail.Student_ACD_YEAR;
            //model.ATTENDENCE_ANALYSIS_LST = _AttendanceService.Get_ATTENDENCE_ANALYSIS(id.ToString()).ToList();

            model.AttendenceBySession = _AttendanceService.Get_AttendenceBySession(id.ToString(), Convert.ToDateTime(date)).ToList();
            model.AttendenceSessionCode = _AttendanceService.Get_AttendenceSessionCode(id.ToString(), Convert.ToDateTime(date)).ToList();
            model.AttendenceList = _ClassListService.GetAttendenceList(id.ToString(), Convert.ToDateTime(date)).ToList();

            ViewBag.HistoryDate = date;

            var AttendenceChartList = _AttendanceService.Get_AttendanceChartMain(id.ToString());
            var acd_descs = AttendenceChartList.Select(s => s.ACD_DESC).Distinct();

            int total = acd_descs.Count();
            foreach (string ACD in acd_descs)
            {
                if (total == 3)
                {
                    model.AttendenceHistory3 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
                }
                else if (total == 2)
                {
                    model.AttendenceHistory2 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
                }
                else if (total == 1)
                {
                    model.AttendenceHistory1 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
                }
                total--;
            }
            var _classListDashboard = _ClassListService.GetStudentDashboardDetails(id.ToString());


            ViewBag.OverAllPercentage = _classListDashboard.Attencence;

            return View("AttendanceDetail", model);
        }

        [HttpGet]
        public ActionResult GetAttendenceBySession(string selectedDate, string STU_ID)
        {
            var model = _AttendanceService.Get_AttendenceBySession(STU_ID, Convert.ToDateTime(selectedDate)).ToList();
            return View("AttendenceBySession", model);
        }

        [HttpGet]
        public ActionResult GetAttendenceSessionCode(string selectedDate, string STU_ID)
        {
            var model = _AttendanceService.Get_AttendenceSessionCode(STU_ID, Convert.ToDateTime(selectedDate)).ToList();
            return View("AttendenceSessionCode", model);
        }

        [HttpGet]
        public ActionResult GetAttendenceListMonthly(string selectedDate, string STU_ID)
        {
            var model = _ClassListService.GetAttendenceList(STU_ID, Convert.ToDateTime(selectedDate)).ToList();
            return View("AttendenceList", model);
        }

        [HttpGet]
        public ActionResult GetAttendenceListWeekly(string selectedDate, string STU_ID)
        {
            var model = _ClassListService.GetAttendenceList(STU_ID, Convert.ToDateTime(selectedDate)).ToList();
            return View("AttendenceListWeekly", model);
        }


        [HttpGet]
        public ActionResult GetAttendenceChartMonthly(string selectedDate, string STU_ID)
        {
            var MonthtlyChartModel = new AttendanceChartMain();
            MonthtlyChartModel.AttendanceChart = _ClassListService.GetAttendanceChart(STU_ID);

            var studentDetails = _ClassListService.GetStudentDetails(STU_ID);
            MonthtlyChartModel.ACD_YEAR_DESC = studentDetails.Student_ACD_YEAR;// Student_Profile.BasicDetails.Student_ACD_YEAR; 

            return View("~/Areas/Classlist/Views/Classlist/_StudentAttendenceChart.cshtml", MonthtlyChartModel);
        }

        #region Room Attendance

        public ActionResult RoomAttendance(string strtt_id = "", string entrydate = null)
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            ViewBag.TeacherCourse = new SelectList(_SIMSCommonService.GetTeacherCourse(SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId), "COR_ID", "COR_DESCR");

            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            //var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, SessionHelper.CurrentSession.ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            var list = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolGrade, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.GradesList = list;
            int tt_id = 0;
            if (!string.IsNullOrWhiteSpace(strtt_id))
            {
                tt_id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
            }
            if (!string.IsNullOrWhiteSpace(entrydate))
            {
                entrydate = EncryptDecryptHelper.Decrypt(entrydate);
            }
            if (string.IsNullOrWhiteSpace(strtt_id))
            {
                string strentry_date = "";
                if (Session["SIMSTimeTableSchedule"] != null)
                {
                    strtt_id = Convert.ToString(Session["SIMSTimeTableSchedule"]);
                    strentry_date = Convert.ToString(Session["SIMSTimeTableEntryDate"]);
                }
                if (strtt_id != "0" && !string.IsNullOrWhiteSpace(strtt_id))
                {
                    tt_id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
                    entrydate = Convert.ToString(EncryptDecryptHelper.Decrypt(strentry_date));
                }
                ViewBag.TTM_Id = tt_id;         //entryDate = DateTime.Now;
                ViewBag.EntryDate = entrydate;

            }


            return View();
        }

        public ActionResult RoomAttendanceDetails(string entryDate, int Coursegroupid = 0)
        {

            SubjectRoomAttendance objSubAttendance = new SubjectRoomAttendance();

            objSubAttendance.objRoomAttendance = _AttendanceService.GetRoomAttendanceDetails(Coursegroupid, entryDate).ToList();
            objSubAttendance.objTimeTable = _AttendanceService.GetRoomAttendanceHeader(Coursegroupid, Convert.ToDateTime(entryDate)).ToList();
            objSubAttendance.objTimeTable.ForEach(x =>
            {

                if (x.PeriodNo == 2)
                {
                    x.IsActive = false;
                }


            });
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;
            //objSubAttendance.lstOtherCategories = SelectSIMSListHelper.GetSelectListData(ListItems.AttendanceType),"ItemId", "ItemName");
            var AttendanceType = SelectListHelper.GetSelectListData(ListItems.AttendanceType, itemId);//itemId
            objSubAttendance.lstOtherCategories = AttendanceType;
            var subjectPeriod = objSubAttendance.objTimeTable.Select(e => e.PeriodNo).Distinct().ToList();

            objSubAttendance.objlstOptionalHeaders = objSubAttendance.objTimeTable.Where(e => !string.IsNullOrEmpty(e.OptionalHeader) && e.IsOptional).
                            GroupBy(e => new { e.OptionalHeader }).Select(f => new RoomAttendanceHeader { OptionalHeader = f.Key.OptionalHeader }).ToList();

            objSubAttendance.objRoomPeriodList = objSubAttendance.objTimeTable.GroupBy(e => new { e.PeriodNo }).Select(e => new RoomAttendanceHeader
            {
                PeriodNo = e.Key.PeriodNo


            }).OrderBy(e => e.PeriodNo).Distinct().ToList();

            objSubAttendance.objlstRoomRemarks = _AttendanceService.GetRoomAttendanceRemarksList(entryDate, SessionHelper.CurrentSession.SchoolId.ToString(), SessionHelper.CurrentSession.Id.ToString(), Coursegroupid).ToList();
            ViewBag.EntryDate = Convert.ToDateTime(entryDate);
            ViewBag.FromTimetable = 0 == 0 ? false : true;

            TempData["AcdId"] = itemId;
            return PartialView("_RoomAttendance", objSubAttendance);
        }


        //[HttpPost]

        //public ActionResult SaveRoomAttendance(List<StudentRoomAttendance> objlstofStudentAttendanceDetails, List<StudentRoomAttendance> objListOfAttendanceLog)
        //{
        //    //var academicYears = _SIMSCommonService.GetAcademicYearList(SessionHelper.CurrentSession.SchoolId.ToString(), SessionHelper.CurrentSession.CLM_ID, null);
        //    var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
        //    var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;
        //    var result = _AttendanceService.InsertUpdateRoomAttendance(SessionHelper.CurrentSession.SchoolId.ToString(), SessionHelper.CurrentSession.Id.ToString(), Convert.ToInt32(itemId), objlstofStudentAttendanceDetails, objListOfAttendanceLog);
        //    return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        //}

        public ActionResult GetSubjectsByGrade(Int32 acd_id = 0, string grd_id = "")
        {
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(SessionHelper.CurrentSession.SchoolId.ToString(), SessionHelper.CurrentSession.UserName);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            acd_id = SessionHelper.CurrentSession.ACD_ID;
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var list = _SIMSCommonService.GetSubjectsByGrade(acd_id, grd_id, SessionHelper.CurrentSession.UserName, isSuperUser);
            var subjectList = new SelectList(list, "SBG_ID", "SBG_DESCR");
            return Json(subjectList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult LoadGrade()
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;

            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, SessionHelper.CurrentSession.ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public JsonResult GetSectionByGradeId(string gradeId)
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;

            var result = _AttendanceService.GetGradeSectionAccesses(schoolid, SessionHelper.CurrentSession.ACD_ID, username, isSuperUser, grd_access, gradeId);
            if (result.Any())
            {
                return Json(result.Select(x => new { Text = x.SectionDescription, Value = x.SectionId }), JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAttendanceType(string grade = "", string entryDate = "")
        {
            var result = _AttendanceService.GetAttendanceTypeByEntryDate(SessionHelper.CurrentSession.ACD_ID, SessionHelper.CurrentSession.SchoolId.ToString(), Convert.ToDateTime(entryDate), grade);
            var JsonResult = new { AttendanceType = result };
            return Json(JsonResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAttendanceFreeze(string grade = "", string entryDate = "")
        {
            bool IsFreezed = false;
            var eDate = Convert.ToDateTime(entryDate);

            var extract_date = eDate.Date.ToString("MM/dd/yyyy");
            var FreezDetails = _attendanceSettingService.GetGradeDetails()
                               .Where(e => !string.IsNullOrEmpty(e.Grades)
                               && e.FAD_ACD_ID == (int)SessionHelper.CurrentSession.ACD_ID
                               && e.ACD_BSU_ID == (int)SessionHelper.CurrentSession.SchoolId).ToList();

            if (FreezDetails != null && FreezDetails.Count() > 0)
            {
                FreezDetails = FreezDetails.Where(e => e.FAD_DATE.Date >= eDate.Date && e.Grades.Contains(grade)).ToList();

            }

            if (FreezDetails != null && FreezDetails.Count() > 0)
            {
                IsFreezed = true;
            }
            var JsonResult = new { Freezed = IsFreezed };
            return Json(JsonResult, JsonRequestBehavior.AllowGet);
        }

        #region Daily Attendance


        public ActionResult Index(string courseGroupId, string asOnDate)
        {
            var attConfig = SelectListHelper.GetSelectListData(ListItems.AttendanceConfig, SessionHelper.CurrentSession.SchoolId);
            int configtype = attConfig.Count() > 0 ? Convert.ToInt32(attConfig.Select(e => e.ItemName).FirstOrDefault()) : 0;

            List<ParameterMappingList> objParameterMappingList = new List<ParameterMappingList>();
            if (!string.IsNullOrWhiteSpace(courseGroupId) && !string.IsNullOrWhiteSpace(asOnDate))
            {
                long? groupId = Convert.ToInt64(EncryptDecryptHelper.DecodeBase64(courseGroupId));
                var attDate = EncryptDecryptHelper.DecodeBase64(asOnDate);
                var currAcdlst = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
                var currAcdId = currAcdlst.Count() > 0 ? currAcdlst.Select(e => e.ItemId).FirstOrDefault() : null;
                if (groupId.HasValue)
                {
                    ViewBag.courseGroupId = groupId.Value;
                    ViewBag.asOnDate = attDate;
                    objParameterMappingList = _AttendanceService.GetParameterMappingByAcademicId(Convert.ToInt64(currAcdId)).ToList();
                }

            }
            if (objParameterMappingList.Count() == 0 && configtype == 1)
            {
                var currAcdlst = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
                var currAcdId = currAcdlst.Count() > 0 ? currAcdlst.Select(e => e.ItemId).FirstOrDefault() : null;
                objParameterMappingList = _AttendanceService.GetParameterMappingByAcademicId(Convert.ToInt64(currAcdId)).ToList();
            }


            ViewBag.configType = configtype;
            ViewBag.pagePermission = CurrentPagePermission;
            ViewBag.AttendanceType = new SelectList(objParameterMappingList, "Id", "Description");
            ViewBag.curriculumnList = _AttendanceService.GetCurriculumByAuthorizedStaffId(SessionHelper.CurrentSession.Id).ToList();
            return View("DailyAttendance");
        }

        public ActionResult GetGradeSessionByGradeId(long schoolGradeId = 0)
        {
            var result = _AttendanceService.GetGradeSessionByGradeId(schoolGradeId);
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetCurriculumByStaffId()
        {
            var curriculumnList = _AttendanceService.GetCurriculumByAuthorizedStaffId(SessionHelper.CurrentSession.Id).ToList();
            return Json(curriculumnList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetAttendanceGradeSection(long academicYearId = 0)
        {
            MergeServiceModelForGradeSection objMergeServiceModelForGradeSection = new MergeServiceModelForGradeSection();
            objMergeServiceModelForGradeSection = _AttendanceService.GetRequiredParamBySchoolUserId(SessionHelper.CurrentSession.SchoolId, SessionHelper.CurrentSession.Id, academicYearId);
            int configtype = objMergeServiceModelForGradeSection.AttendanceConfiguration.Count() > 0 ? Convert.ToInt32(objMergeServiceModelForGradeSection.AttendanceConfiguration.Select(e => e.TypeId).FirstOrDefault()) : 0;
            var gradeSectionDetails = objMergeServiceModelForGradeSection.AuthorizedStaffDetails;
            var parameterList = new SelectList(objMergeServiceModelForGradeSection.ParameterMappingList, "Id", "Description");
            var attendanceParameterShortCut = new SelectList(objMergeServiceModelForGradeSection.AttendanceParameterShortCut, "ParameterId", "ParameterDescription");
            var gradeSessionType = objMergeServiceModelForGradeSection.GradeSessionType;
            /*var attConfig = SelectListHelper.GetSelectListData(ListItems.AttendanceConfig, SessionHelper.CurrentSession.SchoolId);
            int configtype = attConfig.Count() > 0 ? Convert.ToInt32(attConfig.Select(e => e.ItemName).FirstOrDefault()) : 0;

            var gradeSectionDetails = _AttendanceService.GetGradeSectionByUserId(SessionHelper.CurrentSession.Id, academicYearId).ToList();
            var currentAcdId = gradeSectionDetails.Count() > 0 ? gradeSectionDetails.Select(e => e.AcademicYearId).FirstOrDefault() : 0;

            var parameterList = new SelectList(_AttendanceService.GetParameterMappingByAcademicId(academicYearId), "Id", "Description");
            var attendanceParameterShortCut = new SelectList(SelectListHelper.GetSelectListData(ListItems.AttendanceParameterShortCut, SessionHelper.CurrentSession.SchoolId), "ItemId", "ItemName");
            var result = new { gradeSectionDetails = gradeSectionDetails, parameterList = parameterList, configtype = configtype, AttendanceParameterShortCut= attendanceParameterShortCut };*/


            return Json(new { gradeSectionDetails = gradeSectionDetails, parameterList = parameterList, configtype = configtype, AttendanceParameterShortCut = attendanceParameterShortCut, gradeSessionType = gradeSessionType }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDailyAttendance(long gradeId = 0, long sectionId = 0, long sessionType = 0, DateTime? asOnDate = null, int configType = 0, long courseGroupId = 0, long academicYearId = 0)
        {
            bool IsWeeklyHoliday = false;
            var currentAcdDetails = courseGroupId > 0 && configType == 1 ? SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId) : null;
            academicYearId = academicYearId == 0 ? Convert.ToInt64(currentAcdDetails.Select(e => e.ItemId).FirstOrDefault()) : academicYearId;
            var WeeklyHolidayList = _AttendanceService.GetWeekEndBySchoolId(SessionHelper.CurrentSession.SchoolId, academicYearId, gradeId, courseGroupId, asOnDate.Value);
            var schoolWeekEnd = WeeklyHolidayList
                                 .Where(e => e.SchoolWeekend.ToUpper() == asOnDate.Value.DayOfWeek.ToString().ToUpper())
                                 .ToList();
            if (schoolWeekEnd.Count() > 0)
            {
                IsWeeklyHoliday = true;
                return Json(new { IsWeeklyHoliday = IsWeeklyHoliday, Remark = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var schoolHolidayMap = WeeklyHolidayList
                                 .Where(e => e.SchoolId == academicYearId)
                                 .ToList();
                IsWeeklyHoliday = schoolHolidayMap.Count() > 0 ? true : false;
                if (IsWeeklyHoliday)
                {
                    return Json(new { IsWeeklyHoliday = IsWeeklyHoliday, Remark = IsWeeklyHoliday ? schoolHolidayMap.Select(e => e.SchoolWeekend).FirstOrDefault() : "" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("GetDailyAttendanceDetails", new { gradeId = gradeId, sectionId = sectionId, sessionType = sessionType, asOnDate = asOnDate, configType = configType, courseGroupId = courseGroupId });
                }
            }




        }
        
        public ActionResult GetDailyAttendanceDetails(long gradeId = 0, long sectionId = 0, long sessionType = 0, DateTime? asOnDate = null, int configType = 0, long courseGroupId = 0)
        {

            ViewBag.configType = configType;
            switch (configType)
            {
                case 0:
                    var result = _AttendanceService.GetDailyAttendanceDetails(gradeId, sectionId, sessionType, asOnDate.Value, SessionHelper.CurrentSession.SchoolId).OrderBy(e => e.StudentName).ThenByDescending(e => e.AsOnDate).ToList();
                    result.ForEach(x => { x.DisplayDate = x.AsOnDate.ToString("dd-MMM-yyyy"); x.MLDisplayDate = x.AsOnDate.ToFormatedDate("dd-MMM-yyyy"); });
                    return PartialView("_DailyAttendanceDetails", result);

                case 1:
                    ClassAttendanceViewModel objClassAttendanceModel = new ClassAttendanceViewModel();
                    List<ClassAttendance> objBifurationPeriod = new List<ClassAttendance>();
                    ClassAttendanceHeaderNDetails objGetClassAttendanceHeaderNDetails = new ClassAttendanceHeaderNDetails();
                    objGetClassAttendanceHeaderNDetails = _AttendanceService.GetClassAttendanceHeaderNDetails(SessionHelper.CurrentSession.SchoolId, courseGroupId, asOnDate.Value);
                    var classAttendanceHearder = objGetClassAttendanceHeaderNDetails.ClassAttendanceHeader;  //_AttendanceService.GetClassAttendanceHeader(SessionHelper.CurrentSession.SchoolId, courseGroupId, asOnDate.Value).ToList();
                    var currAcdlst = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
                    var currAcdId = currAcdlst.Count() > 0 ? currAcdlst.Select(e => e.ItemId).FirstOrDefault() : null;
                    classAttendanceHearder.ForEach(x =>
                    {
                        if (x.SchoolGroupId == courseGroupId)
                        {
                            x.IsActive = true;

                        }
                        x.DisplayStartTime = x.StartTime.ToFormatedDate("HH:mm tt");
                        x.DisplayEndTime = x.EndTime.ToFormatedDate("HH:mm tt");
                    });

                    /*Active period*/
                    classAttendanceHearder.Where(e => e.IsActive).Select(e => new { e.PeriodNo, e.StartTime, e.EndTime }).Distinct().ToList().ForEach(x =>
                    {
                        objBifurationPeriod.Add(new ClassAttendance
                        {
                            PeriodNo = x.PeriodNo,
                            IsActive = true,
                            DisplayStartTime = x.StartTime.ToFormatedDate("HH:mm tt"),
                            DisplayEndTime = x.EndTime.ToFormatedDate("HH:mm tt")
                        });
                    });
                    /*Inactive Period*/
                    classAttendanceHearder.Where(p => !classAttendanceHearder.Where(e => e.IsActive).Select(e => e.PeriodNo).Distinct().ToList().Any(a => a == p.PeriodNo) && !p.IsActive && p.TeacherId != SessionHelper.CurrentSession.Id)
                                        .Select(p => new { p.PeriodNo, p.StartTime, p.EndTime }).Distinct().ToList().ForEach(x =>
                                        {
                                            objBifurationPeriod.Add(new ClassAttendance
                                            {
                                                PeriodNo = x.PeriodNo,
                                                IsActive = false,
                                                DisplayStartTime = x.StartTime.ToFormatedDate("HH:mm:ss"),
                                                DisplayEndTime = x.EndTime.ToFormatedDate("HH:mm:ss")
                                            });
                                        });

                    objClassAttendanceModel.ClassAttendanceHeaderList = classAttendanceHearder;//classAttendanceHearder.OrderBy(e => e.PeriodNo).ToList();//objBifurationPeriod.OrderBy(e => e.PeriodNo).ToList();
                    var classAttendanceDetails = objGetClassAttendanceHeaderNDetails.ClassAttendanceDetails;//_AttendanceService.GetClassAttendanceDetails(courseGroupId, asOnDate.Value).ToList();

                    objClassAttendanceModel.ClassAttendanceDetailsList = classAttendanceDetails;
                    objClassAttendanceModel.ClassAttendanceDetailsList.ForEach(x => { x.DisplayDate = x.AsOnDate.ToString("dd-MMM-yyyy"); x.MLDisplayDate = x.AsOnDate.ToFormatedDate("dd-MMM-yyyy"); });

                    ViewBag.AttendanceType = new SelectList(SelectListHelper.GetSelectListData(ListItems.AttendanceType, currAcdId), "ItemId", "ItemName");
                    ViewBag.AttendanceParameterShortCut = new SelectList(SelectListHelper.GetSelectListData(ListItems.AttendanceParameterShortCut, SessionHelper.CurrentSession.SchoolId), "ItemId", "ItemName");
                    ViewBag.isDaiyWeekly = objClassAttendanceModel.ClassAttendanceHeaderList.Count() > 0 ? objClassAttendanceModel.ClassAttendanceHeaderList.Select(e => e.isDailyWeekly).FirstOrDefault() : 0;
                    ViewBag.TeacherId = SessionHelper.CurrentSession.Id;
                    return PartialView("_DailyAttendanceDetails", objClassAttendanceModel);

                case 2:
                    var dtt = _AttendanceService.GetDailyAttendanceDetails(gradeId, sectionId, sessionType, asOnDate.Value, SessionHelper.CurrentSession.SchoolId).OrderBy(e => e.StudentName).ThenByDescending(e => e.AsOnDate).ToList();
                    dtt.ForEach(x => { x.DisplayDate = x.AsOnDate.ToString("dd-MMM-yyyy"); x.MLDisplayDate = x.AsOnDate.ToFormatedDate("dd-MMM-yyyy"); });
                    return PartialView("_DailyAttendanceDetails", dtt);

            }

            return null;


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveDailyAttendance(List<AttendanceStudent> objlstAttendanceStudent, long academicYearId = 0)
        {
            objlstAttendanceStudent.ForEach(x => { x.AcademicYearId = academicYearId; });
            var result = _AttendanceService.SaveDailyAttendance(objlstAttendanceStudent, SessionHelper.CurrentSession.Id, 0);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckforWeeklyHoliday(DateTime? selectedDate, long academicYearId = 0, long schoolGradeId = 0, long courseId = 0, bool IsCallFrmRoom = false)
        {
            bool IsWeeklyHoliday = false;
            var currentAcdDetails = IsCallFrmRoom ? SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId) : null;
            academicYearId = IsCallFrmRoom ? Convert.ToInt64(currentAcdDetails.Select(e => e.ItemId).FirstOrDefault()) : academicYearId;
            var WeeklyHolidayList = _AttendanceService.GetWeekEndBySchoolId(SessionHelper.CurrentSession.SchoolId, academicYearId, schoolGradeId, courseId, selectedDate.Value);
            var schoolWeekEnd = WeeklyHolidayList
                                 .Where(e => e.SchoolWeekend.ToUpper() == selectedDate.Value.DayOfWeek.ToString().ToUpper())
                                 .ToList();
            if (schoolWeekEnd.Count() > 0)
            {
                IsWeeklyHoliday = true;
                return Json(new { IsWeeklyHoliday = IsWeeklyHoliday, Remark = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var schoolHolidayMap = WeeklyHolidayList
                                 .Where(e => e.SchoolId == academicYearId)
                                 .ToList();
                IsWeeklyHoliday = schoolHolidayMap.Count() > 0 ? true : false;
                return Json(new { IsWeeklyHoliday = IsWeeklyHoliday, Remark = IsWeeklyHoliday ? schoolHolidayMap.Select(e => e.SchoolWeekend).FirstOrDefault() : "" }, JsonRequestBehavior.AllowGet);
            }

            //IsWeeklyHoilday = selectedDate.Value.DayOfWeek.ToString().ToUpper() == schoolWeekEnd.SchoolWeekend.ToUpper() ? true : false;

        }

        #endregion

        #region Class Attendance

        public ActionResult ClassAttendance()
        {


            return View("ClassAttendance");

        }
        [HttpGet]
        public ActionResult CourseDetailsByTeacherId()
        {
            var result = new SelectList(_SIMSCommonService.GetTeacherCourse(SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId), "COR_ID", "COR_DESCR");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCourseGroupByCourse(long courseId = 0)
        {

            var list = _SIMSCommonService.GetCourseGroupByCourse(courseId, SessionHelper.CurrentSession.Id);
            var subjectgroupList = new SelectList(list, "GroupId", "GroupName");
            return Json(subjectgroupList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetClassAttendanceDetails(long courseGroupId = 0, DateTime? asOnDate = null)
        {

            var classAttendanceHearder = _AttendanceService.GetClassAttendanceHeader(SessionHelper.CurrentSession.SchoolId, courseGroupId, asOnDate.Value).ToList();
            classAttendanceHearder.ForEach(x =>
            {

                if (SessionHelper.CurrentSession.Id == x.TeacherId)
                {
                    x.IsActive = true;

                }
                x.DisplayStartTime = x.StartTime.ToString("HH:mm:ss");
                x.DisplayEndTime = x.EndTime.ToString("HH:mm:ss");
            });

            var classAttendanceDetails = new { classAttendanceHearder = classAttendanceHearder, classAttendanceDetails = string.Empty };
            return Json(classAttendanceDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveClassAttendance(List<StudentRoomAttendance> objClassListDetails, long? schoolGroupId, int? isDailyWeeklyTimeTable, DateTime? entryDate)
        {
            //var academicYears = _SIMSCommonService.GetAcademicYearList(SessionHelper.CurrentSession.SchoolId.ToString(), SessionHelper.CurrentSession.CLM_ID, null);
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;
            var result = _AttendanceService.InsertUpdateRoomAttendance(SessionHelper.CurrentSession.SchoolId.ToString(), SessionHelper.CurrentSession.Id.ToString(), Convert.ToInt32(itemId), isDailyWeeklyTimeTable.Value, schoolGroupId.Value, SessionHelper.CurrentSession.Id, objClassListDetails, entryDate.Value);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetClassListForMergeConfig(long courseGroupId = 0, DateTime? asOnDate = null)
        {
            bool IsWeeklyHoliday = false;
            var currentAcdDetails = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var academicYearId =  Convert.ToInt64(currentAcdDetails.Select(e => e.ItemId).FirstOrDefault());
            var WeeklyHolidayList = _AttendanceService.GetWeekEndBySchoolId(SessionHelper.CurrentSession.SchoolId, academicYearId, 0, courseGroupId, asOnDate.Value);
            var schoolWeekEnd = WeeklyHolidayList
                                 .Where(e => e.SchoolWeekend.ToUpper() == asOnDate.Value.DayOfWeek.ToString().ToUpper())
                                 .ToList();
            if (schoolWeekEnd.Count() > 0)
            {
                IsWeeklyHoliday = true;
                return Json(new { IsWeeklyHoliday = IsWeeklyHoliday, Remark = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var schoolHolidayMap = WeeklyHolidayList
                                 .Where(e => e.SchoolId == academicYearId)
                                 .ToList();
                IsWeeklyHoliday = schoolHolidayMap.Count() > 0 ? true : false;
                if(IsWeeklyHoliday)
                {
                    return Json(new { IsWeeklyHoliday = IsWeeklyHoliday, Remark = IsWeeklyHoliday ? schoolHolidayMap.Select(e => e.SchoolWeekend).FirstOrDefault() : "" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return RedirectToAction("GetClassListForMergeConfigDetails", new { courseGroupId = courseGroupId, asOnDate = asOnDate });
                }
            }
        }

        public ActionResult GetClassListForMergeConfigDetails(long courseGroupId = 0, DateTime? asOnDate = null)
        {
            ClassAttendanceViewModel objClassAttendanceModel = new ClassAttendanceViewModel();
            List<ClassAttendance> objBifurationPeriod = new List<ClassAttendance>();
            ClassAttendanceHeaderNDetails objGetClassAttendanceHeaderNDetails = new ClassAttendanceHeaderNDetails();
            objGetClassAttendanceHeaderNDetails = _AttendanceService.GetClassAttendanceHeaderNDetails(SessionHelper.CurrentSession.SchoolId, courseGroupId, asOnDate.Value);
            var classAttendanceHearder = objGetClassAttendanceHeaderNDetails.ClassAttendanceHeader;  //_AttendanceService.GetClassAttendanceHeader(SessionHelper.CurrentSession.SchoolId, courseGroupId, asOnDate.Value).ToList();
            var currAcdlst = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var currAcdId = currAcdlst.Count() > 0 ? currAcdlst.Select(e => e.ItemId).FirstOrDefault() : null;
            classAttendanceHearder.ForEach(x =>
            {
                if (x.SchoolGroupId == courseGroupId)
                {
                    x.IsActive = true;

                }
                x.DisplayStartTime = x.StartTime.ToFormatedDate("HH:mm tt");
                x.DisplayEndTime = x.EndTime.ToFormatedDate("HH:mm tt");
            });

            /*Active period*/
            classAttendanceHearder.Where(e => e.IsActive).Select(e => new { e.PeriodNo, e.StartTime, e.EndTime }).Distinct().ToList().ForEach(x =>
            {
                objBifurationPeriod.Add(new ClassAttendance
                {
                    PeriodNo = x.PeriodNo,
                    IsActive = true,
                    DisplayStartTime = x.StartTime.ToFormatedDate("HH:mm tt"),
                    DisplayEndTime = x.EndTime.ToFormatedDate("HH:mm tt")
                });
            });
            /*Inactive Period*/
            classAttendanceHearder.Where(p => !classAttendanceHearder.Where(e => e.IsActive).Select(e => e.PeriodNo).Distinct().ToList().Any(a => a == p.PeriodNo) && !p.IsActive && p.TeacherId != SessionHelper.CurrentSession.Id)
                                .Select(p => new { p.PeriodNo, p.StartTime, p.EndTime }).Distinct().ToList().ForEach(x =>
                                {
                                    objBifurationPeriod.Add(new ClassAttendance
                                    {
                                        PeriodNo = x.PeriodNo,
                                        IsActive = false,
                                        DisplayStartTime = x.StartTime.ToFormatedDate("HH:mm:ss"),
                                        DisplayEndTime = x.EndTime.ToFormatedDate("HH:mm:ss")
                                    });
                                });

            objClassAttendanceModel.ClassAttendanceHeaderList = classAttendanceHearder; //objBifurationPeriod.OrderBy(e => e.PeriodNo).ToList();
            //classAttendanceHearder.OrderBy(e => e.PeriodNo).ToList();//objBifurationPeriod.OrderBy(e => e.PeriodNo).ToList();
            var classAttendanceDetails = objGetClassAttendanceHeaderNDetails.ClassAttendanceDetails;//_AttendanceService.GetClassAttendanceDetails(courseGroupId, asOnDate.Value).ToList();

            objClassAttendanceModel.ClassAttendanceDetailsList = classAttendanceDetails;
            objClassAttendanceModel.ClassAttendanceDetailsList.ForEach(x => { x.DisplayDate = x.AsOnDate.ToString("dd-MMM-yyyy"); x.MLDisplayDate = x.AsOnDate.ToFormatedDate("dd-MMM-yyyy"); });

            return Json(objClassAttendanceModel, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetGroupsByTimeTable(DateTime? courseDate)
        {
            bool IsWeeklyHoliday = false;
            var currentAcdDetails = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var academicYearId = Convert.ToInt64(currentAcdDetails.Select(e => e.ItemId).FirstOrDefault());
            var WeeklyHolidayList = _AttendanceService.GetWeekEndBySchoolId(SessionHelper.CurrentSession.SchoolId, academicYearId, 0, 0, courseDate.Value);
            var schoolWeekEnd = WeeklyHolidayList
                                 .Where(e => e.SchoolWeekend.ToUpper() == courseDate.Value.DayOfWeek.ToString().ToUpper())
                                 .ToList();
            if (schoolWeekEnd.Count() > 0)
            {
                IsWeeklyHoliday = true;
                return Json(new { IsWeeklyHoliday = IsWeeklyHoliday, Remark = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var schoolHolidayMap = WeeklyHolidayList
                                 .Where(e => e.SchoolId == academicYearId)
                                 .ToList();
                IsWeeklyHoliday = schoolHolidayMap.Count() > 0 ? true : false;
                if (IsWeeklyHoliday)
                {
                    return Json(new { IsWeeklyHoliday = IsWeeklyHoliday, Remark = IsWeeklyHoliday ? schoolHolidayMap.Select(e => e.SchoolWeekend).FirstOrDefault() : "" }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    var list = _AttendanceService.GetGroupsfromTimeTable(SessionHelper.CurrentSession.Id, courseDate.Value);
                    var groupList = new SelectList(list, "SchoolGroupId", "SchoolGroupName");
                    return Json(new { groupList= list,userTypeId = SessionHelper.CurrentSession.UserTypeId }, JsonRequestBehavior.AllowGet);
                }
            }

        }

        public ActionResult GetPrevAttendanceByGroupIdAndDate(long groupId = 0, DateTime? courseDate = null)
        {
            var prvClassAttendanceHearder = _AttendanceService.GetPreviousClassAttendanceHeader(SessionHelper.CurrentSession.SchoolId, groupId, courseDate.Value).ToList();
            prvClassAttendanceHearder.ForEach(x =>
            {
                if (SessionHelper.CurrentSession.Id == x.TeacherId && x.SchoolGroupId == groupId)
                {
                    x.IsActive = true;

                }
                x.DisplayStartTime = x.StartTime.ToFormatedDate("HH:mm tt");
                x.DisplayEndTime = x.EndTime.ToFormatedDate("HH:mm tt");
            });

            prvClassAttendanceHearder.OrderBy(e => e.PeriodNo).ToList();//objBifurationPeriod.OrderBy(e => e.PeriodNo).ToList();
            var prvClassAttendanceDetails = _AttendanceService.GetPreviousClassAttendanceDetails(groupId, courseDate.Value).ToList();

            var previousAttendanceForGroup = new { PreviousAttendanceHeader = prvClassAttendanceHearder, PreviousAttendanceDetails = prvClassAttendanceDetails };
            return Json(previousAttendanceForGroup, JsonRequestBehavior.AllowGet);

        }
        #endregion
    }
}