﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services.PTM.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.PTM.Controllers
{
    [Authorize]
    public class PTMCategoryListController : BaseController
    {
        // GET: PTM/PTMCateoryList
        private readonly IPTMCategoryService _IPTMcategoryService;
        public PTMCategoryListController(IPTMCategoryService iptmategoryService)
        {
            _IPTMcategoryService = iptmategoryService;
        }
        /// <summary>
        /// Author:Pranav
        /// Summery:GetCategory list.
        /// Date:22 Dec 2020 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var model = new CategoryListEdit();
            model.SchoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
            var schoolGradeList = SelectListHelper.GetSelectListData(ListItems.SchoolGrade, model.SchoolId);
            model.SchoolGradeList.AddRange(schoolGradeList);
            return View(model);

        }
        /// <summary>
        /// Author:Pranav
        /// Summery:AddEditCategory list.
        /// Date:22 Dec 2020 
        /// </summary>
        /// <returns></returns>
        public ActionResult InitAddEditCategorySetForm(int? CategoryId, int? SchoolGradeId)
        {
            var model = new CategoryListEdit();
            if (CategoryId.HasValue)
            {
                var category = _IPTMcategoryService.GetCategorySetById(CategoryId.Value);
                EntityMapper<CategoryList, CategoryListEdit>.Map(category, model);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolGradeId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolGradeId);
            }
            return PartialView("_AddCategoryList", model);
        }
        /// <summary>
        /// Author:Pranav
        /// Summery:LoadCategory list.
        /// Date:22 Dec 2020 
        /// </summary>
        /// <returns></returns>
        public ActionResult LoadCategorySetGrid(int SchoolGradeId)
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            // var schoolSkillSet = _schoolSkillSetService.GetSchoolSkillSet((int)(int)SessionHelper.CurrentSession.SchoolId);
            var CategorySet = _IPTMcategoryService.GetCategorySet(SchoolGradeId);
            var actionButtonsHtmlTemplate = string.Empty;
            if (CurrentPagePermission.CanEdit)
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='PTMCategoryList.editCategoryPopup($(this),{0})' title='" + Common.Localization.ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a><a class='mr-3' data-toggle='tooltip' data-placement='bottom' class='table-action-icon-btn' onclick='PTMCategoryList.deleteCategoryData($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.DeleteRecord") + "'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a></div>";
            else
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='PTMCategoryList.editCategoryPopup($(this),{0})' title='" + Common.Localization.ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-view.svg'></div>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in CategorySet
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.CategoryId),
                              item.CategoryName,
                              item.CategoryDescription,
                              item.CreatedBy,
                              item.IsApproved,
                              item.ApprovedBy
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Author:Pranav
        /// Summery:AddEditCategory list.
        /// Date:22 Dec 2020 
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteCategorySetData(int id)
        {
            var result = _IPTMcategoryService.DeleteCategory(id);
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Author:Pranav
        /// Summery:AddEditCategory list.
        /// Date:22 Dec 2020 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCategorySetData(CategoryListEdit model)
        {
            if (!CurrentPagePermission.CanEdit)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            }
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            model.ParseJsonToXml("CategorySetXml");

            var result = _IPTMcategoryService.UpdateCategory(model);
            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "Safety Catagory")), JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }
    }
}