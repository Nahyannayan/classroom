﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services.PTM.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.PTM.Controllers
{
    [Authorize]
    public class PTMDeclineReasonController : BaseController
    {
        // GET: PTM/PTMDeclineReason
        private readonly IPTMDeclineReasonService _IPTMDeclineReasonService;
        public PTMDeclineReasonController(IPTMDeclineReasonService iptmDeclineReasonService)
        {
            _IPTMDeclineReasonService = iptmDeclineReasonService;
        }
        /// <summary>
        /// Author:Pranav
        /// Summery:GetDeclineResone list.
        /// Date:22 Dec 2020 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {

            var user = Helpers.CommonHelper.GetLoginUser();
            var model = new DeclineReasonListEdit();
            model.SchoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
            var schoolGradeList = SelectListHelper.GetSelectListData(ListItems.SchoolGrade, model.SchoolId);
            model.SchoolGradeList.AddRange(schoolGradeList);
            return View(model);
        }
        /// <summary>
        /// Author:Pranav
        /// Summery:AddEditDeclineResone list.
        /// Date:22 Dec 2020 
        /// </summary>
        /// <returns></returns>
        public ActionResult InitAddEditDeclineReasonSetForm(int? DeclineReasonId, int? SchoolGradeId)
        {
            var model = new DeclineReasonListEdit();
            if (DeclineReasonId.HasValue)
            {
                var declineReason = _IPTMDeclineReasonService.GetDeclineReasonSetById(DeclineReasonId.Value);
                EntityMapper<DeclineReasonList, DeclineReasonListEdit>.Map(declineReason, model);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolGradeId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolGradeId);
            }
            return PartialView("_AddDeclineReason", model);
        }
        /// <summary>
        /// Author:Pranav
        /// Summery:LoadDeclineResone list.
        /// Date:22 Dec 2020 
        /// </summary>
        /// <returns></returns>
        public ActionResult LoadDeclineReasonSetGrid(int SchoolGradeId)
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            // var schoolSkillSet = _schoolSkillSetService.GetSchoolSkillSet((int)(int)SessionHelper.CurrentSession.SchoolId);
            var DeclineReasonSet = _IPTMDeclineReasonService.GetDeclineReasonSet(SchoolGradeId);
            var actionButtonsHtmlTemplate = string.Empty;
            if (CurrentPagePermission.CanEdit)
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='PTMDeclineReasonList.editDeclineReasonPopup($(this),{0})' title='" + Common.Localization.ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a><a class='mr-3' data-toggle='tooltip' data-placement='bottom' class='table-action-icon-btn' onclick='PTMDeclineReasonList.deleteDeclineReasonData($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.DeleteRecord") + "'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a></div>";
            else
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='PTMDeclineReasonList.editDeclineReasonPopup($(this),{0})' title='" + Common.Localization.ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-view.svg'></div>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in DeclineReasonSet
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.DeclineReasonId),
                              item.DeclineReasonName,
                              item.DeclineReasonDescription,
                              item.CreatedBy,
                              item.IsApproved,
                              item.ApprovedBy
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Author:Pranav
        /// Summery:DeleteDeclineResone list.
        /// Date:22 Dec 2020 
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteDeclineReasonSetData(int id)
        {
            var result = _IPTMDeclineReasonService.DeleteDeclineReason(id);
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Author:Pranav
        /// Summery:SaveDeclineResone list.
        /// Date:22 Dec 2020 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveDeclineReasonSetData(DeclineReasonListEdit model)
        {
            if (!CurrentPagePermission.CanEdit)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            }
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            model.ParseJsonToXml("DeclineReasonSetXml");

            var result = _IPTMDeclineReasonService.UpdateDeclineReason(model);
            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "Safety Catagory")), JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }
    }
}