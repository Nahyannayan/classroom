﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.PTM
{
    public class PTMmodelAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PTM";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PTM_default",
                "PTM/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}