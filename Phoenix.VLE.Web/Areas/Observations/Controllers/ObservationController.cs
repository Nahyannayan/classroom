﻿using Google.Apis.Auth.OAuth2.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.Helpers.Extensions;
using File = Phoenix.Models.File;

namespace Phoenix.VLE.Web.Areas.Observations.Controllers
{
    public class ObservationController : BaseController
    {
        private readonly IObservationService _observationService;
        private readonly IAssignmentService _assignmentService;
        private IStudentListService _studentListService;
        private IGradingTemplateService _gradingTemplateService;
        private IGradingTemplateItemService _gradingTemplateItemService;
        //private IDocumentService _documentService;
        private readonly IFileService _fileService;
        private List<VLEFileType> _fileTypeList;
        private ISchoolGroupService _schoolGroupService;
        private ISubjectService _subjectService;
        private readonly IQuizQuestionsService _quizQuestionsService;
        private readonly IUserPermissionService _userPermissionService;

        public ObservationController(IObservationService ObservationService, IAssignmentService assignmentService, IStudentListService StudentListService, ISchoolService schoolService,
        IGradingTemplateService gradingTemplateService, IGradingTemplateItemService gradingTemplateItemService,
         IFileService fileService, ISchoolGroupService schoolGroupService, ISubjectService subjectService, IQuizQuestionsService quizQuestionsService, IUserPermissionService userPermissionService)
        {
            _observationService = ObservationService;
            _assignmentService = assignmentService;
            _studentListService = StudentListService;
            _gradingTemplateService = gradingTemplateService;
            _gradingTemplateItemService = gradingTemplateItemService;
            _fileService = fileService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
            _schoolGroupService = schoolGroupService;
            _subjectService = subjectService;
            _quizQuestionsService = quizQuestionsService;
            _userPermissionService = userPermissionService;
        }

        // GET: Observation/Observation
        public ActionResult Index(int pageIndex = 1)
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var pageName = "_ObservationDetailsPage";
            if (SessionHelper.CurrentSession.IsParent())
            {
                pageName = "_ParentObservation";
            }
            else
            {
                pageName = "_ObservationDetailsPage";
            }
            return View(pageName);
        }

        public ActionResult InitAddEditAObservationForm(string id)
        {
            if (!CurrentPagePermission.CanEdit && !(SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin))
            {
                return RedirectToAction("NoPermission", "Error");
            }
            int? observationId = !string.IsNullOrWhiteSpace(id) ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id)) : 0;
            var model = new ObservationEdit();
            Session["ObservationFiles"] = null;
            Session["lstObjective"] = null;
            Session["lstSelectedMyFiles"] = null;
            Session["ObservationFiles"] = null;
            List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
            lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });


            ViewBag.GradingTemplateList = new SelectList(SelectListHelper.GetSelectListData(ListItems.GradingTemplate, SessionHelper.CurrentSession.SchoolId), "ItemId", "ItemName");
            long schoolId = Helpers.CommonHelper.GetLoginUser().SchoolId;
            if (schoolId <= 0)
            {
                schoolId = (int)SessionHelper.CurrentSession.SchoolId;
            }
            ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            if (observationId != 0)
            {
                var observation = _observationService.GetObservationById(observationId.Value);
                EntityMapper<Observation, ObservationEdit>.Map(observation, model);
                //model.lstDocuments = _observationService.GetObservationFiles(observationId.Value).ToList();
                Session["ObservationFiles"] = model.lstDocuments = observation.lstObservationFiles;
                //model.lstObjective = _observationService.GetObservationObjectives(observationId.Value).ToList();
                Session["lstObjective"] = model.lstObjective = observation.lstObjectives;
                //var gd = _gradingTemplateService.GetGradingTemplateById(model.GradingTemplateId);
                //model.GradingTemplate = gd.GradingTemplateTitle;
                //model.GradingTemplateLogo = gd.GradingTemplateLogo;
                List<Phoenix.Models.SchoolGroup> lstSchoolGroups = new List<Phoenix.Models.SchoolGroup>();
                lstSchoolGroups = _observationService.GetSelectedSchoolGroupsByObservationID(observationId.Value).ToList();
                model.SchoolGroupId = lstSchoolGroups.Select(m => m.SchoolGroupId.ToString()).ToList();
                //ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName");
                List<ListItem> lstCourse = new List<ListItem>();
                lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
                var courses = lstCourse.Select(x => new SelectListItem
                {
                    Value = x.ItemId.ToString(),
                    Text = x.ItemName,
                }).ToList();
                ViewBag.CourseList = courses;
                var coursesList = new List<Phoenix.Models.Course>();
                coursesList = _observationService.GetSchoolCoursesByObservation(observationId.Value).ToList();
                model.CourseId = coursesList.Select(m => m.CourseId.ToString()).ToList();
                //List<Subject> lstSubjects = new List<Subject>();
                //lstSubjects = _observationService.GetObservationSubjects(observationId.Value).ToList();
                //model.CourseId = lstSubjects.Select(m => m.SubjectId.ToString()).ToList();
                var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
                {
                    Value = x.SchoolGroupId.ToString(),
                    Text = x.SchoolGroupName.Length > 50 ? x.SchoolGroupName.Substring(0, 50) : x.SchoolGroupName,
                    Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                    Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
                }).ToList();
                ViewBag.lstSchoolGroup = groupedOptions;
                var StudentList = _studentListService.GetStudentsInSelectedGroups(string.Join(",", model.SchoolGroupId));
                var StudentSelectList = StudentList.Select(i => new ListItem()
                {
                    ItemName = i.FirstName + " " + i.LastName,
                    ItemId = i.StudentId.ToString()
                }).OrderBy(m => m.ItemName);
                ViewBag.StudentList = new SelectList(StudentSelectList.OrderBy(m => m.ItemName), "ItemId", "ItemName");
                List<ObservationStudent> lstAsgStudents = new List<ObservationStudent>();
                lstAsgStudents = _observationService.GetStudentsByObservationId(observationId.Value).ToList();
                //model.lstStudent = lstAsgStudents;
                model.StudentId = lstAsgStudents.Select(m => m.StudentId.ToString()).ToList();
                model.StartDate.ConvertUtcToLocalTime();
            }
            else
            {
                // ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName");
                //List<SubjectListItem> lstSubject = SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id).ToList();
                //List<SelectListItem> lstSelectListItems = new List<SelectListItem>();
                //foreach (var item in lstSubject)
                //{
                //    lstSelectListItems.Add(
                //        new SelectListItem
                //        {
                //            Text = item.ItemName,
                //            Value = item.ItemId,
                //            Selected = true
                //        }
                //        );
                //}
                //model.SubjectId = lstSubject.Select(m => m.ItemId).ToList();
                //ViewBag.SubjectList = lstSelectListItems;
                var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
                {
                    Value = x.SchoolGroupId.ToString(),
                    Text = x.SchoolGroupName.Length > 50 ? x.SchoolGroupName.Substring(0, 50) : x.SchoolGroupName,
                    Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2]))
                }).ToList();
                ViewBag.lstSchoolGroup = groupedOptions;
                List<SelectListItem> lstStudentList = new List<SelectListItem>();
                ViewBag.StudentList = lstStudentList;
                List<SelectList> lstSubjectTopics = new List<SelectList>();
                ViewBag.lstSubjectTopics = new SelectList(SelectListHelper.GetSelectListData(ListItems.MainSyllabus, string.Format("{0}", 33379)).OrderByDescending(x => x.ItemName), "ItemId", "ItemName").ToList();
                List<SelectList> lstSubjectSubTopics = new List<SelectList>();
                ViewBag.lstSubjectSubTopics = lstSubjectSubTopics;
                List<SelectList> lstObjective = new List<SelectList>();
                ViewBag.lstObjective = lstObjective;
                model.IsAddMode = true;
                model.StartDate = (DateTime)DateTime.Now.ConvertUtcToLocalTime();

                List<ListItem> lstCourse = new List<ListItem>();
                lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
                var courses = lstCourse.Select(x => new SelectListItem
                {
                    Value = x.ItemId.ToString(),
                    Text = x.ItemName.Length > 60 ? x.ItemName.Substring(0, 60) : x.ItemName,
                }).ToList();
                ViewBag.CourseList = courses;
            }
            return View("_AddEditObservation", model);
        }

        public JsonResult GetStudentsinGroup(int[] id)
        {
            IEnumerable<ListItem> StudentSelectList = new List<ListItem>();
            if (id != null && id.Any())
            {
                int systemlangaugeid = LocalizationHelper.CurrentSystemLanguageId;
                var StudentList = _studentListService.GetStudentsInSelectedGroups(string.Join(",", id));
                StudentSelectList = StudentList.Select(i => new ListItem()
                {
                    ItemName = i.FirstName + " " + i.LastName + " (" + i.UserName + ")",
                    ItemId = i.StudentId.ToString()
                });
            }
            return Json(StudentSelectList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTopicSubTopicStructure(string subjectIds)
        {
            var TopicList = _subjectService.GetTopicSubTopicStructureByMultipleSubjects(subjectIds).ToList();
            List<TopicTreeItem> treeList = new List<TopicTreeItem>();
            TopicList.ForEach(l => treeList.Add(new TopicTreeItem() { id = l.SubSyllabusId, parentId = l.ParentId, name = l.SubSyllabusDescription, isLesson = l.isLesson }));
            return Json(treeList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveObservationDetails(ObservationEdit model)
        {
            ICommonService _commonService = new CommonService();
            List<Objective> lstObjectives = new List<Objective>();
            OperationDetails op = new OperationDetails();
            if (Session["lstObjective"] != null)
            {
                lstObjectives = Session["lstObjective"] as List<Objective>;
                Session["lstObjective"] = null;
            }
            //convert arabic dates
            model.StartDate = (DateTime)model.strStartDate.ToSystemReadableDate();
            model.StartDate.ConvertLocalToUTC();
            model.CreatedById = (int)SessionHelper.CurrentSession.Id;
            model.lstDocuments = Session["ObservationFiles"] as List<ObservationFile>;
            var lstStudents = _studentListService.GetStudentDetailsByIds(string.Join(",", model.StudentIds));
            string studentInternalIds = "";
            foreach (var item in lstStudents)
            {
                studentInternalIds = studentInternalIds + (studentInternalIds == "" ? "" : "|") + item.StudentInternalId;
            }

            if (model.lstDocuments != null)
            {
                if (model.lstDocuments.Any(e => e.ResourceFileTypeId != 0))
                    model.lstDocuments = await ShareObservationFiles(model.lstDocuments);
            }

            op.Success = _observationService.UpdateObservationData(model, lstObjectives);
            if (op.Success && model.lstDocuments != null)
            {
                model.lstDocuments = model.lstDocuments.Select(x => { x.IsNewFile = false; return x; }).ToList();
                Session["ObservationFiles"] = model.lstDocuments;
            }
            if (op.Success && model.IsAddMode)
            {
                string EmailBody = "";
                StreamReader reader = new StreamReader(Server.MapPath(Constants.ObservationNotificationTemplate), Encoding.UTF8);
                EmailBody = reader.ReadToEnd();
                EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
                //EmailBody = EmailBody.Replace("@@StudentName", SessionHelper.CurrentSession.FullName);
                EmailBody = EmailBody.Replace("@@TeacherName", SessionHelper.CurrentSession.FullName);
                EmailBody = EmailBody.Replace("@@ObservationName", model.ObservationTitle);
                SendEmailNotificationView sendEmailNotificationView = new SendEmailNotificationView();
                sendEmailNotificationView.FromMail = Constants.NoReplyMail;
                sendEmailNotificationView.LogType = "ObservationNotification";
                sendEmailNotificationView.StudentId = studentInternalIds;
                sendEmailNotificationView.Subject = Constants.ObservationNotification;
                sendEmailNotificationView.Message = EmailBody;
                sendEmailNotificationView.Username = SessionHelper.CurrentSession.UserName;
                // var operationalDetails = EmailHelper.SendEmail(toEmailAddrees, Constants.AssignmentNotification, EmailBody);
                bool isSuccess = _commonService.SendEmailNotifications(sendEmailNotificationView);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        private async Task<List<ObservationFile>> ShareObservationFiles(List<ObservationFile> lstDocuments)
        {
            foreach (var file in lstDocuments)
            {
                if (file.ResourceFileTypeId == (short)ResourceFileTypes.OneDrive && file.IsNewFile)
                {
                    string response = await CloudFilesHelper.CreateOneDriveSharingLink(file.GoogleDriveFileId);
                    var data = (JObject)JsonConvert.DeserializeObject(response);
                    file.UploadedFileName = (string)data["link"]["webUrl"];
                }
                else if (file.ResourceFileTypeId == (short)ResourceFileTypes.GoogleDrive && file.IsNewFile)
                {
                    CancellationToken cancellationToken = new CancellationToken();
                    var authResult = new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).
                       AuthorizeAsync(cancellationToken).GetAwaiter().GetResult();
                    CloudFilesHelper.AddGoogleDriveUserPermission(authResult, file.GoogleDriveFileId, "reader");
                }
            }
            return lstDocuments;
        }

        [HttpGet]
        public ActionResult DeleteObservation(int observationId)
        {
            OperationDetails op = new OperationDetails();
            Observation observation = _observationService.GetObservationById(observationId);
            observation.CreatedById = SessionHelper.CurrentSession.Id;
            op.Success = _observationService.DeleteObservationData(observation);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        #region File Uploading and downloading

        [HttpPost]
        public async Task<ActionResult> UploadObservationFiles()
        {
            var result = false;
            List<ObservationFile> fileNames = new List<ObservationFile>();
            var fileEditList = new List<FileEdit>();
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Get all files from Request object
                    var postedFileList = new List<HttpPostedFileBase>();
                    HttpFileCollectionBase files = Request.Files;
                    List<SharePointFileView> sharePointFileViews = new List<SharePointFileView>();
                    for (int i = 0; i < files.Count; i++)
                    {
                        postedFileList.Add(files[i]);
                    }
                    if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                    {
                        AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                        await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                        sharePointFileViews = await azureHelper.UploadMultipleFilesAsPerModuleAsync(FileModulesConstants.ObservationFile, SessionHelper.CurrentSession.OldUserId.ToString(), postedFileList.ToArray());
                    }
                    else
                    {
                        SharePointHelper sh = new SharePointHelper();
                        Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                        sharePointFileViews = SharePointHelper.UploadMultipleFilesAsPerModule(ref cx, FileModulesConstants.ObservationFile, SessionHelper.CurrentSession.OldUserId.ToString(), postedFileList.ToArray());
                    }
                    if (sharePointFileViews.Count > 0)
                    {
                        fileNames = sharePointFileViews.Select(x => new ObservationFile
                        {
                            FileName = x.UploadedFileName,
                            UploadedFileName = x.ShareableLink,
                            PhysicalFilePath = x.SharepointUploadedFileURL,
                            ResourceFileTypeId = (short)ResourceFileTypes.SharePoint,
                            FileSizeInMB = x.FileSizeInMb,
                            FileTypeId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(Path.GetExtension(x.ActualFileName).Replace(".", ""))).TypeId,
                            FileIcon = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(Path.GetExtension(x.ActualFileName).Replace(".", ""))).Icon,
                            FileTypeName = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(Path.GetExtension(x.ActualFileName).Replace(".", ""))).TypeName,
                            IsNewFile = true

                        }).ToList();
                    }
                }
                catch (Exception ex)
                {
                }
                if (Session["ObservationFiles"] == null)
                {
                    Session["ObservationFiles"] = fileNames;
                }
                else
                {
                    List<ObservationFile> lstExistingFiles = new List<ObservationFile>();
                    lstExistingFiles = Session["ObservationFiles"] as List<ObservationFile>;
                    lstExistingFiles.AddRange(fileNames);
                    Session["ObservationFiles"] = lstExistingFiles;
                }
            }
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetuploadedFileDetails(int id)
        {
            List<ObservationFile> lstfiles = Session["ObservationFiles"] as List<ObservationFile>;
            return PartialView("_ObservationFileList", lstfiles);
        }

        public ActionResult LoadMyFiles()
        {
            List<MyFilesTreeItem> myFiles = _assignmentService.GetMyFiles(SessionHelper.CurrentSession.Id).ToList();
            List<FileTreeItem> treeList = new List<FileTreeItem>();
            myFiles.ForEach(l => treeList.Add(new FileTreeItem() { id = l.FileId, parentId = l.ParentFolderId, name = l.FileName, isFolder = l.isFolder }));
            return Json(treeList);
        }
        public ActionResult LoadSelectedMyFiles()
        {
            List<ObservationFile> lstMyFilesAsAsgFiles = new List<ObservationFile>();
            if (Session["ObservationFiles"] != null)
            {
                lstMyFilesAsAsgFiles = Session["ObservationFiles"] as List<ObservationFile>;
                Session["lstSelectedMyFiles"] = lstMyFilesAsAsgFiles;
            }

            return PartialView("_LoadMyFiles", lstMyFilesAsAsgFiles);
        }

        public ActionResult GetSelectedFile(long id, bool isFolder)
        {
            List<ObservationFile> lstMyFilesAsAsgFiles = new List<ObservationFile>();
            lstMyFilesAsAsgFiles = _observationService.GetObservationMyFiles(id, isFolder).ToList();
            List<ObservationFile> lstSelectedMyFilesAsAsgFiles = new List<ObservationFile>();
            if (Session["lstSelectedMyFiles"] == null)
            {
                Session["lstSelectedMyFiles"] = lstMyFilesAsAsgFiles;
                lstSelectedMyFilesAsAsgFiles.AddRange(lstMyFilesAsAsgFiles);
            }
            else
            {
                lstSelectedMyFilesAsAsgFiles = Session["lstSelectedMyFiles"] as List<ObservationFile>;
                lstSelectedMyFilesAsAsgFiles.AddRange(lstMyFilesAsAsgFiles);
                Session["lstSelectedMyFiles"] = lstSelectedMyFilesAsAsgFiles;
            }
            return PartialView("_LoadSelectedMyFile", lstSelectedMyFilesAsAsgFiles);
        }

        public ActionResult SaveSelectedFiles(List<ObservationFile> mappingDetails)
        {
            var operationDetails = new OperationDetails();
            List<ObservationFile> lstSelectedFiles = new List<ObservationFile>();
            if (Session["ObservationFiles"] == null)
            {
                Session["ObservationFiles"] = mappingDetails;
                lstSelectedFiles = mappingDetails;
            }
            else
            {
                lstSelectedFiles = mappingDetails;
            }
            Session["lstSelectedMyFiles"] = lstSelectedFiles;
            return PartialView("_ObservationFileList", lstSelectedFiles);
        }

        public ActionResult UpdateSelectedFiles(List<ObservationFile> mappingDetails)
        {
            var operationDetails = new OperationDetails();
            List<ObservationFile> lstFiles = new List<ObservationFile>();
            if (Session["lstSelectedMyFiles"] == null)
            {
                Session["lstSelectedMyFiles"] = mappingDetails;
            }
            else
            {
                ObservationFile obj = mappingDetails.FirstOrDefault();
                lstFiles = Session["lstSelectedMyFiles"] as List<ObservationFile>;
                lstFiles = lstFiles.Where(m => m.ObservationFileId != obj.ObservationFileId).ToList();
                Session["lstSelectedMyFiles"] = lstFiles;
            }
            operationDetails.Success = false;
            operationDetails.Message = "";
            return PartialView("_LoadSelectedMyFile", lstFiles);
        }

        public FileResult DownloadFile(int studentId = 0, int observationId = 0, int FId = 0, string Type = "")
        {
            switch (Type)
            {
                case "Teacher Observation":
                    var teacherobservation = _observationService.GetObservationFiles(observationId).ToList();
                    var Filedetails = teacherobservation.Where(e => e.ObservationFileId == FId).FirstOrDefault();
                    if (Filedetails != null && !string.IsNullOrWhiteSpace(Filedetails.FileName))
                    {
                        string path = PhoenixConfiguration.Instance.ReadFilePath + Filedetails.UploadedFileName;
                        WebClient myWebClient = new WebClient();
                        byte[] myDataBuffer = myWebClient.DownloadData(path);
                        return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, Filedetails.FileName);
                    }
                    break;
                default:
                    return null;

            }

            return null;
        }

        public ActionResult DeleteFilesFromSession(int fileId = 0, int ObservationId = 0, string fileName = "", string UploadedFilePath = "")
        {
            string relativepath = string.Empty;
            List<ObservationFile> lstfiles = Session["ObservationFiles"] as List<ObservationFile>;
            if (fileId == 0)
            {
                var file = lstfiles.FirstOrDefault(e => e.FileName.ToUpper().Trim() == fileName.ToUpper().Trim());
                if (file != null)
                {
                    lstfiles.Remove(file);
                    if (System.IO.File.Exists(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath))
                    {
                        System.IO.File.Delete(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath);

                    }
                    Session["ObservationFiles"] = lstfiles;
                }
            }
            else
            {
                long UserId = SessionHelper.CurrentSession.Id;
                var isActionPerformed = _observationService.DeleteUploadedFiles(fileId, ObservationId, UserId);
                if (isActionPerformed)
                {
                    var file = lstfiles.FirstOrDefault(e => e.ObservationFileId == fileId);
                    lstfiles.Remove(file);
                }
            }
            return PartialView("_ObservationFileList", lstfiles);
        }

        public ActionResult SaveSelectedCloudFiles(string fileIds)
        {
            var selectedFiles = new List<CloudFileListView>();
            var observationFiles = new List<ObservationFile>();
            List<ObservationFile> lstSelectedFiles = new List<ObservationFile>();
            selectedFiles = JsonConvert.DeserializeObject<List<CloudFileListView>>(fileIds);

            observationFiles = selectedFiles.Select(x => new ObservationFile
            {
                FileName = x.Name,
                UploadedFileName = x.WebViewLink,
                FileTypeId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(x.FileExtension.ToLower())).TypeId,
                FileTypeName = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(x.FileExtension.ToLower())).TypeName,
                FileIcon = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(x.FileExtension.ToLower())).Icon,
                ResourceFileTypeId = x.ResourceFileTypeId,
                GoogleDriveFileId = x.Id,
                IsNewFile = true

            }).ToList();
            if (Session["ObservationFiles"] == null)
            {
                Session["ObservationFiles"] = observationFiles;
                lstSelectedFiles = observationFiles;
            }
            else
            {
                List<ObservationFile> lstExistingFiles = new List<ObservationFile>();
                lstExistingFiles = Session["ObservationFiles"] as List<ObservationFile>;
                lstExistingFiles.AddRange(observationFiles);
                Session["ObservationFiles"] = lstExistingFiles;
                lstSelectedFiles = lstExistingFiles;
            }
            return PartialView("_ObservationFileList", lstSelectedFiles);
        }

        #endregion
        #region Curriculum tagging
        /// <summary>
        ///Author : Mahesh Chikhale
        ///Created Date : 29-August-2019
        ///Description : To open modal popup for curriculm
        /// </summary>
        /// <param name="subjectId">subjectId</param>       
        /// <returns></returns>
        public ActionResult loadTopicSubtopic(string[] subjectIds)
        {
            ViewBag.SubjectIds = string.Join(",", subjectIds);
            List<SelectListItem> lessonsList = new List<SelectListItem>();
            ViewBag.Lessons = lessonsList;
            List<Objective> lstselectedObjective = Session["lstObjective"] as List<Objective>;
            return PartialView("_TopicSubtopicStructure", lstselectedObjective);
        }
        public ActionResult GetUnitStructureView(string courseIds)
        {
            var lstselectedObjective = new List<Objective>();
            var TopicList = _subjectService.GetUnitStructure(courseIds).ToList();
            if (Session["lstObjective"] != null)
            {
                lstselectedObjective = Session["lstObjective"] as List<Objective>;
                TopicList.ForEach(x => { x.IsSelected = lstselectedObjective.Any(d => d.ObjectiveId.ToString() == x.SubSyllabusId); });
            }
            return PartialView("_UnitStructureView", TopicList);
        }
        [HttpPost]
        public ActionResult loadStudentDetails(List<string> StudentIds)
        {
            var obStudentList = new List<ObservationStudent>();
            if (StudentIds != null && StudentIds.Any())
            {
                var studentList = _studentListService.GetStudentDetailsByIds(string.Join(",", StudentIds)).ToList();
                studentList.ForEach(x => obStudentList.Add(new ObservationStudent { StudentId = x.StudentId, StudentName = x.StudentName, UserImageFilePath = x.StudentImage, StudentNumber = x.StudentNumber }));
            }
            return PartialView("_ShowSelectedStudent", obStudentList);
        }
        public ActionResult loadStudentDetailsByObservation(int observationId)
        {
            var studentList = _observationService.GetObservationStudentById(observationId).ToList();
            studentList.ForEach(x => x.UserImageFilePath = Constants.StudentImagesPath + x.UserImageFilePath);
            var jsonResult = Json(studentList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult GetSubtopicObjectives(string id, string subjectIds, bool isLesson)
        {

            List<Objective> lstObjective = new List<Objective>();
            bool isMainSyllabus = false;
            if (id.Contains('x'))
            {
                isMainSyllabus = true;
            }
            int topicId = Convert.ToInt32(id.Replace("x", ""));
            lstObjective = _subjectService.GetSubtopicObjectivesWithMultipleSubjects(topicId, isMainSyllabus, subjectIds, isLesson).ToList();
            List<Objective> lstSelectedObjective = new List<Objective>();
            if (Session["lstObjective"] == null)
            {
                Session["lstObjective"] = lstObjective;
                lstSelectedObjective = lstObjective;
            }
            else
            {
                lstSelectedObjective = Session["lstObjective"] as List<Objective>;
                List<Objective> tempList = new List<Objective>();
                foreach (var item in lstObjective)
                {
                    Objective obj = lstSelectedObjective.Where(m => m.ObjectiveId == item.ObjectiveId).FirstOrDefault();
                    if (obj == null)
                    {
                        lstSelectedObjective.Add(item);
                    }
                }
                // lstSelectedObjective.AddRange(lstObjective);
                Session["lstObjective"] = lstSelectedObjective;

            }
            return PartialView("_LoadObjectives", lstSelectedObjective);
        }

        public ActionResult UpdateObjectives(List<Objective> mappingDetails)
        {
            var operationDetails = new OperationDetails();
            List<Objective> lstObjectives = new List<Objective>();
            if (Session["lstObjective"] == null)
            {
                Session["lstObjective"] = mappingDetails;
            }
            else
            {
                Objective obj = mappingDetails.FirstOrDefault();
                lstObjectives = Session["lstObjective"] as List<Objective>;
                lstObjectives = lstObjectives.Where(m => m.ObjectiveId != obj.ObjectiveId).ToList();
                //lstObjectives.AddRange(mappingDetails);
                Session["lstObjective"] = lstObjectives;
            }
            operationDetails.Success = false;
            operationDetails.Message = "";
            return PartialView("_LoadObjectives", lstObjectives);
        }

        public ActionResult DisselectAllObjectives()
        {
            var operationDetails = new OperationDetails();
            var lstObjectives = new List<Objective>();
            Session["lstObjective"] = lstObjectives;
            operationDetails.Success = false;
            operationDetails.Message = "";
            return PartialView("_LoadObjectives", lstObjectives);
        }

        public ActionResult SaveObjectives(List<Objective> mappingDetails)
        {
            Session["lstObjective"] = mappingDetails;
            return PartialView("_ShowSelectedObjective", mappingDetails);
        }
        public ActionResult DeleteObjective(int objectiveId)
        {
            var operationDetails = new OperationDetails();
            List<Objective> lstobjectives = Session["lstObjective"] as List<Objective>;
            lstobjectives = lstobjectives.Where(m => m.ObjectiveId != objectiveId).ToList();
            Session["lstObjective"] = lstobjectives;
            operationDetails.Success = true; ;
            return PartialView("_ShowSelectedObjective", lstobjectives);
        }
        #endregion
        #region Observation Teacher Dahboard
        public ActionResult LoadObservation(List<Phoenix.Models.SchoolGroup> selectedGroupList, bool isGroupSelected = false, int pageIndex = 1, string searchString = "", string sortBy = "name")
        {
            var pageName = "";

            if (SessionHelper.CurrentSession.IsParent() || SessionHelper.CurrentSession.IsStudent())
            {
                var model = new Pagination<ObservationStudent>();
                var studentId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.StudentId;
                pageName = "_ParentObservationGroup";
                var observationList = _observationService.GetObservationStudentPaging(pageIndex, 6, Convert.ToInt64(studentId), searchString, sortBy).ToList();
                //foreach (var item in observationList)
                //{
                //    item.lstObservationFiles = _observationService.GetObservationFiles(item.ObservationId).ToList();
                //}
                if (observationList.Any())
                    model = new Pagination<ObservationStudent>(pageIndex, 6, observationList, observationList.FirstOrDefault().TotalCount);
                model.RecordCount = observationList.Count == 0 ? 0 : observationList[0].TotalCount;
                model.LoadPageRecordsUrl = "/Observations/Observation/LoadObservation?pageIndex={0}";
                model.SearchString = searchString;
                model.SortBy = sortBy;
                return PartialView(pageName, model);
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                string groupIds = "";
                if (isGroupSelected)
                {
                    if (selectedGroupList != null)
                    {
                        groupIds = string.Join(",", selectedGroupList.Select(x => x.SchoolGroupId).ToList());
                        Session["SelectedSchoolGroups"] = selectedGroupList;
                    }
                    else if (Session["SelectedSchoolGroups"] != null)
                    {
                        selectedGroupList = (List<Phoenix.Models.SchoolGroup>)Session["SelectedSchoolGroups"];
                        groupIds = string.Join(",", selectedGroupList.Select(x => x.SchoolGroupId).ToList());
                    }
                }
                else
                {
                    Session["SelectedSchoolGroups"] = null;
                }
                var model = new Pagination<Observation>();
                pageName = "_TecaherObservationGroup";
                var observationList = _observationService.GetObservationTeacherPaging(pageIndex, 6, Helpers.CommonHelper.GetLoginUser().Id, groupIds, searchString, sortBy).ToList();
                //var assignmentList = _assignmentService.GetAssignmentStudentDetails(pageIndex, 6, Helpers.CommonHelper.GetLoginUser().Id).ToList();
                foreach (var item in observationList)
                {
                    item.lstObservationStudents = _observationService.GetObservationStudentById(item.ObservationId).ToList();
                }
                if (observationList.Any())
                    model = new Pagination<Observation>(pageIndex, 6, observationList, observationList.FirstOrDefault().TotalCount);
                model.RecordCount = observationList.Count == 0 ? 0 : observationList[0].TotalCount;
                model.LoadPageRecordsUrl = "/Observations/Observation/LoadObservation?pageIndex={0}" + (isGroupSelected ? "&isGroupSelected=" + isGroupSelected : string.Empty);
                model.SearchString = searchString;
                model.SortBy = sortBy;
                //groups
                var schoolGroupList = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.IsTeacher());
                schoolGroupList.ToList().ForEach(d => { d.SchoolGroupName = string.IsNullOrEmpty(d.TeacherGivenName) ? d.SchoolGroupDescription.Replace("_", "/") : d.TeacherGivenName; });
                ViewBag.SchoolGroups = schoolGroupList;
                return PartialView(pageName, model);
            }
            return View();
        }
        public ActionResult GetObservationDetails(string id)
        {
            int observationId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            string viewName = String.Empty;
            Observation observation = new Observation();
            observation = _observationService.GetObservationById(observationId);
            //observation.lstObservationFiles = _observationService.GetObservationFiles(observationId).ToList();
            //observation.lstObjectives = _observationService.GetObservationObjectives(observationId).ToList();
            Session["ObservationFiles"] = observation.lstObservationFiles;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                viewName = "GetObservationDetails";
                //observation.lstObservationStudents = _observationService.GetObservationStudentById(observationId).ToList();
            }
            else
            {
                if (!observation.StudentIdsToAdd.Contains(SessionHelper.CurrentSession.CurrentSelectedStudent.StudentId.ToString()))
                    return RedirectToAction("NoPermission", "Error");
                viewName = "ObservationDetails";
            }
            return View(viewName, observation);
        }

        // GET: Observation/Observation
        public ActionResult ArchivedIndex(int pageIndex = 1)
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var pageName = "_ArchivedObservationDetailsPage";
            if (SessionHelper.CurrentSession.IsParent())
            {
                pageName = "_ParentObservation";
            }
            else
            {
                pageName = "_ArchivedObservationDetailsPage";
            }
            return View(pageName);
        }

        public ActionResult LoadArchivedObservation(int pageIndex = 1, string searchString = "")
        {
            var pageName = "";
            var model = new Pagination<Observation>();
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                //var model = new Pagination<Observation>();
                pageName = "_TecaherArchivedObservationGroup";
                var observationList = _observationService.GetArchivedObservationTeacherPaging(pageIndex, 6, Helpers.CommonHelper.GetLoginUser().Id, searchString).ToList();
                foreach (var item in observationList)
                {
                    item.lstObservationFiles = _observationService.GetObservationFiles(item.ObservationId).ToList();
                }
                if (observationList.Any())
                    model = new Pagination<Observation>(pageIndex, 6, observationList, observationList.FirstOrDefault().TotalCount);
                model.RecordCount = observationList.Count == 0 ? 0 : observationList[0].TotalCount;
                model.LoadPageRecordsUrl = "/Observations/Observation/LoadArchivedObservation?pageIndex={0}";
                model.SearchString = searchString;
                return PartialView(pageName, model);
            }

            return PartialView(pageName, model);
        }

        public ActionResult ArchiveObservation()
        {
            Observation objObservation = new Observation();
            int teacherID = (int)SessionHelper.CurrentSession.Id;
            objObservation = _observationService.GetObservations(teacherID);
            return PartialView("_ArchivedObservationList", objObservation);
        }

        public ActionResult GetActiveObservations()
        {
            int teacherID = (int)SessionHelper.CurrentSession.Id;
            var lstActiveObservations = _observationService.GetObservations(teacherID);
            return PartialView("_GetActiveObservations", lstActiveObservations.lstActiveObservation);
        }

        public ActionResult GetArchivedObservations()
        {
            int teacherID = (int)SessionHelper.CurrentSession.Id;
            var lstArchivedObservations = _observationService.GetObservations(teacherID);
            return PartialView("_GetArchivedObservations", lstArchivedObservations.lstArchivedObservation);
        }
        [HttpPost]
        public ActionResult UpdateActiveObservation(string observationToArchive)
        {
            string[] str = observationToArchive.Split(',');
            foreach (var observationId in str)
            {
                if (observationId != "")
                {
                    var result = _observationService.UpdateActiveObservation(Convert.ToInt64(observationId));
                }
            }
            return null;

        }

        public ActionResult RemoveUnsavedFiles()
        {
            if (Session["ObservationFiles"] != null)
            {
                List<ObservationFile> lstfiles = Session["ObservationFiles"] as List<ObservationFile>;
                foreach (var item in lstfiles.Where(e => e.IsNewFile))
                {
                    var filePath = PhoenixConfiguration.Instance.WriteFilePath + item.UploadedFileName;
                    Common.Helpers.CommonHelper.DeleteFile(filePath);
                }
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}