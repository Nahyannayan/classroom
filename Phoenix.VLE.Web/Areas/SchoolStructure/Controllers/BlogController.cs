﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SchoolStructure.Controllers
{
    public class BlogController : Controller
    {
        private readonly IBlogService _blogService;
        private readonly IBlogCommentService _blogCommentService;

        public BlogController(IBlogService blogService, IBlogCommentService blogCommentService)
        {
            _blogService = blogService;
            _blogCommentService = blogCommentService;
        }
        // GET: Users/Blogs
        public ActionResult Index(int pageNumber = 1)
        {
            var viewName = String.Empty;
            int pageSize = 5;
            var blogs = _blogService.GetTeacherBlogs((int)SessionHelper.CurrentSession.SchoolId, pageNumber, pageSize).ToList();
            var objPage = new Pagination<Blog>(pageNumber, pageSize, blogs, 11);
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                viewName = "TeacherBlog";
            }
            else if (SessionHelper.CurrentSession.IsParent() || SessionHelper.CurrentSession.IsStudent())
            {
                viewName = "ParentBlog";
            }

            return View(viewName, objPage);
        }

    }
}