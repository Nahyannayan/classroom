﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Phoenix.VLE.Web.Models.EditModels;

namespace Phoenix.VLE.Web.Areas.SchoolStructure.Controllers
{
    public class SchoolSpacesController : BaseController
    {
        private readonly ISchoolSpaceService _schoolSpaceService;
        private readonly ITeacherDashboardService _teacherDashboardService;
        private readonly IStudentService _studentService;
        private readonly IUserService _userService;
      //  private readonly ILeaveService _leaveService;
        //private readonly IFeePaymentsService _feePaymentService;
        //private readonly ICalendarService _calendarService;
        //private readonly IEventCategoryService _eventCategoryService;
        private readonly IFileService _fileService;
        private List<VLEFileType> _fileTypeList;
        List<GalleryFile> galleryFiles = new List<GalleryFile>();
        private ILoggerClient _loggerClient;

        public SchoolSpacesController(ITeacherDashboardService teacherDashboardService
            , ISchoolSpaceService schoolSpaceService
            , IStudentService studentService, IUserService userService
            //, IEventCategoryService eventCategoryService
            //, ILeaveService leaveService
            , IFileService fileService
            //, IFeePaymentsService feePaymentService
            //, ICalendarService calendarService
            )
        {
            _teacherDashboardService = teacherDashboardService;
            _schoolSpaceService = schoolSpaceService;
            _studentService = studentService;
            _userService = userService;
            //_eventCategoryService = eventCategoryService;
           // _leaveService = leaveService;
            _fileService = fileService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
            //_feePaymentService = feePaymentService;
            //_calendarService = calendarService;
        }
        // GET: Users/SchoolSpaces

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetFilteredCirculars(string fromDate, string toDate, int pageIndex = 1)
        {
            int skipCount = (pageIndex - 1) * 20;
            var model = new Pagination<Circular>();
            var circulars = GetCircularList(fromDate, toDate);
            if (circulars.Any())
            {
                var circularsShown = circulars.Count > skipCount ? circulars.Skip(skipCount).Take(20) : new List<Circular>();
                model = new Pagination<Circular>(pageIndex, 20, circularsShown.ToList(), circulars.Count);
            }
            model.LoadPageRecordsUrl = "/SchoolStructure/SchoolSpaces/GetFilteredCirculars?pageIndex={0}&fromDate=" + fromDate + "&toDate=" + toDate;
            model.SearchString = string.Empty;
            model.PageIndex = pageIndex;
            return PartialView("_CircularGrid", model);
        }

        public List<Circular> GetCircularList(string fromDate, string toDate)
        {
            string userId = "", key = "";
            List<Circular> circulars = new List<Circular>();
            _loggerClient = LoggerClient.Instance;
            try
            {
                if (SessionHelper.CurrentSession.IsParent())
                {
                    userId = SessionHelper.CurrentSession.UserName;
                    key = "Parent";
                }
                else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                {
                    userId = SessionHelper.CurrentSession.UserName;
                    key = "Staff";
                }
                else if (SessionHelper.CurrentSession.IsStudent())
                {
                    userId = SessionHelper.CurrentSession.OldUserId.ToString();
                    key = "Student";
                }

                //circulars = _calendarService.GetCircularList(userId, key, "", fromDate, toDate, "");

                if (circulars == null)
                {
                    circulars = new List<Circular>();
                }

            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            return circulars;
        }

        public ActionResult CircularsNewsletters()
        {
            return View();
        }
        public JsonResult ShowPopupDetail(int circularId)
        {
            Circular circular = new Circular();
            //circular = _calendarService.GetCircularDetails(Convert.ToString(circularId), "en");
            return Json(circular, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateNewsletterIsRead(string circularData)
        {

            string userId = "", key = " ";
            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = SessionHelper.CurrentSession.UserName;
                key = "Parent";
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                userId = SessionHelper.CurrentSession.UserName;
                key = "Staff";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                userId = SessionHelper.CurrentSession.OldUserId.ToString();
                key = "Student";
            }
            circularData = circularData.Replace("{0}", key).Replace("{1}", userId);
            bool result = false;// _calendarService.updateCircularDetails(circularData);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ResourceCategories()
        {
            List<ResourceCategory> categories = new List<ResourceCategory>();
            List<Resource> resources = new List<Resource>();
            _loggerClient = LoggerClient.Instance;

            try
            {
                string parentId = "", teacherId = "", isStaff = "";
                if (SessionHelper.CurrentSession.IsParent())
                {
                    parentId = SessionHelper.CurrentSession.UserName;
                    isStaff = "0";
                }
                else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                {
                    teacherId = SessionHelper.CurrentSession.UserName;
                    isStaff = "1";
                }
                else
                {
                    parentId = _userService.GetUserById(SessionHelper.CurrentSession.ParentId).UserName;
                    isStaff = "0";
                }
                //if (SessionHelper.CurrentSession.IsParent() || SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin || SessionHelper.CurrentSession.IsStudent())
                    //categories = _calendarService.GetResourceCategories(parentId, "en", isStaff, teacherId);

                if (categories == null)
                {
                    categories = new List<ResourceCategory>();
                }
                categories = categories.Where(c => c.schoolID == SessionHelper.CurrentSession.SchoolId.ToString()).ToList();

                if (categories.Count > 0 && categories[0].resourcesCategories != null && categories[0].resourcesCategories.Count > 0)
                {
                    for (int i = 0; i < categories[0].resourcesCategories.Count; i++)// foreach (var cat in categories[0].resourcesCategories)
                    {
                        //get resources for specific category
                        //resources = _calendarService.GetResources(parentId, SessionHelper.CurrentSession.SchoolId.ToString(), categories[0].resourcesCategories[i].categoryID.ToString(), "", "en", isStaff, teacherId);
                        for (int j = 0; j < resources.Count; j++)
                        {
                            if (categories[0].resourcesCategories[i].resources == null)
                            {
                                categories[0].resourcesCategories[i].resources = new List<Resource>();
                            }
                            var fileExt = Path.GetExtension(resources[j].resourceAttachmentUrl).Replace(".", "");
                            resources[j].FileIcon = _fileTypeList.Where(e => e.Extension == fileExt).FirstOrDefault().Icon;
                            categories[0].resourcesCategories[i].resources.Add(resources[j]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            return View(categories);
        }
        public ActionResult Resources(string id)
        {
            List<Resource> resources = new List<Resource>();
            _loggerClient = LoggerClient.Instance;
            try
            {
                string parentId = "", teacherId = "", isStaff = "";
                if (SessionHelper.CurrentSession.IsParent())
                {
                    parentId = SessionHelper.CurrentSession.UserName;
                    isStaff = "0";
                }
                else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                {
                    teacherId = SessionHelper.CurrentSession.UserName;
                    isStaff = "1";
                }
                //resources = _calendarService.GetResources(parentId, SessionHelper.CurrentSession.SchoolId.ToString(), id, "", "en", isStaff, teacherId);

                if (resources == null)
                {
                    resources = new List<Resource>();
                }
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }

            return View(resources);
        }
        ////Leave request START
        //public ActionResult LeaveDetails()
        //{
        //    string username = string.Empty, language = string.Empty;
        //    username = SessionHelper.CurrentSession.UserName;
        //    ICommonService _commonService = new CommonService();
        //    var currentLanguage = _commonService.GetUserCurrentLanguage(SessionHelper.CurrentSession.LanguageId);
        //    language = Convert.ToString(currentLanguage.SystemLanguageCode);
        //    var studList = SessionHelper.CurrentSession.FamilyStudentList;
        //    LeaveRequest objVM = new LeaveRequest();
        //    List<StudentsLeaveDetails> objModel = new List<StudentsLeaveDetails>();
        //    foreach (var item in studList)
        //    {
        //        StudentsLeaveDetails obj = new StudentsLeaveDetails()
        //        {
        //            StudentNo = item.StudentNumber,
        //            StudentName = item.FirstName + " " + item.LastName,
        //            LstLeaveDetails = _leaveService.GetLeaveRequestsByStudentlId(item.StudentNumber, username, language),
        //            LstLeaveTypes = _leaveService.GetLeaveTypes("LEAVE_TYPE", item.SchoolId, item.PhoenixSchoolAcademicYearId),
        //        };
        //        objModel.Add(obj);
        //    }
        //    // string master = "LEAVE_TYPE"; int bsuid = 125005; int acdid = 1417;
        //    // ViewBag.lstLeaveType = _leaveService.GetLeaveTypes(master, bsuid, acdid);

        //    ViewBag.FileExtension = _fileTypeList.Where(m => m.DocumentType == "Document" || m.DocumentType == "Image").Select(r => r.Extension).ToList();
        //    ViewBag.StudList = objModel;
        //    return View(objVM);
        //}
        //public ActionResult LeaveApplicationModal(string LeaveType , int RefId , string StudNo)
        //{
        //    LeaveRequest objM = new LeaveRequest();
        //    objM.StudentNo = StudNo;
        //    objM.Ref_Id = RefId;
        //    objM.Leave_Type = LeaveType;
        //    string username = string.Empty, language = string.Empty;
        //    username = SessionHelper.CurrentSession.UserName;
        //    ICommonService _commonService = new CommonService();
        //    var currentLanguage = _commonService.GetUserCurrentLanguage(SessionHelper.CurrentSession.LanguageId);
        //    language = Convert.ToString(currentLanguage.SystemLanguageCode);
        //    var studListP = SessionHelper.CurrentSession.FamilyStudentList;
        //    var studList = studListP.Where(m => m.StudentNumber == objM.StudentNo).ToList();
        //    LeaveRequest objVM = new LeaveRequest();
        //    var item = studList.Count()>0 ? studList[0] : new StudentDetail();
        //    StudentsLeaveDetails obj = new StudentsLeaveDetails()
        //    {
        //        LstLeaveDetails = _leaveService.GetLeaveRequestsByStudentlId(item.StudentNumber, username, language),              
        //    };
        //    if (objM.Ref_Id > 0)
        //    {
        //        obj.LstLeaveTypes = _leaveService.GetLeaveTypes("LEAVE_TYPE", item.SchoolId, item.PhoenixSchoolAcademicYearId);
        //        objVM = obj.LstLeaveDetails.Where(m => m.Ref_Id == objM.Ref_Id && m.Leave_Type == objM.Leave_Type).FirstOrDefault();
        //    }
        //    ViewBag.LeaveReasonList = obj.LstLeaveTypes;
        //    objVM.StudentNo = item.StudentNumber;
        //    //objVM.LeaveReasonId = obj.LstLeaveTypes.Where(m => m.Descr == objVM.Attendance_Leave_Type).Select(m => m.Id).First().ToString();
        //    ViewBag.FileExtension = _fileTypeList.Where(m => m.DocumentType == "Document" || m.DocumentType == "Image").Select(r => r.Extension).ToList();
        //    ViewBag.IsAddViewEdit = objM.Ref_Id > 0 ? (objVM.Approval_Status== "Pending" && objVM.Enable_Button == 1 ? "edit" : "view") : "write";
        //    return PartialView("_LeaveApplicationModal", objVM);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public JsonResult LeaveRequestSubmit()
        //{
        //    //TokenResult Tokanresult = _leaveService.GetAuthorizationTokenAsync();
        //    //string access_token = Tokanresult.token_type + " " + Tokanresult.access_token;
        //    HttpFileCollectionBase files = Request.Files;
        //    string jsonEvents = "";
        //    LeaveApplication objMod = new LeaveApplication();
        //    objMod.SLA_Id = Convert.ToInt32(Request.Params["refId"]);
        //    objMod.FromDT = Request.Params["Fromdate"];
        //    objMod.ToDT = Request.Params["ToDate"];
        //    objMod.Remark = Request.Params["remark"];
        //    objMod.SLA_Type = Request.Params["SLAType"] == "Leave Request" ? "LR" : "LL";
        //    objMod.StudentNo = Request.Params["stuId"];
        //    objMod.LeaveReasonId = Convert.ToInt32(Request.Params["Leavetype"]);
        //    objMod.BEdit = Convert.ToBoolean(Request.Params["isEdit"]);
        //    if (string.IsNullOrEmpty(objMod.SLA_Type))
        //    {
        //        objMod.SLA_Type = "LR";
        //    }
        //    string fname;
        //    if (files.Count > 1)
        //    {
        //        HttpPostedFileBase file = files[0];
        //        // Checking for Internet Explorer  
        //        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
        //        {
        //            string[] testfiles = file.FileName.Split(new char[] { '\\' });
        //            fname = testfiles[testfiles.Length - 1];
        //        }
        //        else
        //        {
        //            fname = file.FileName;
        //        }
        //        // Get the complete folder path and store the file inside it.  
        //        fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
        //        file.SaveAs(fname);
        //    }

        //    jsonEvents = _leaveService.LeaveRequestSubmit(files, objMod);

        //    var jsonResult = new { events = jsonEvents };
        //    return Json(jsonResult, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult SaveLeaveApplication(LeaveRequest model, HttpPostedFileBase BadgeFile)
        //{
        //    var jsonResult = new { events = "" };
        //    return Json(jsonResult, JsonRequestBehavior.AllowGet);
        //}

        //Events section START
        public ActionResult Events()
        {
            //ViewBag.lstCategory = _calendarService.GetAllEventCategories().ToList();
            return View();
        }

        public JsonResult GetEvents(string month)
        {
            string fromDate = "", toDate = "";

            DateTime now = DateTime.Now;
            if (!string.IsNullOrEmpty(month))
            {
                var m = month.Split(' ')[0];
                var year = month.Split(' ')[1];
                int monthNUmber = DateTime.Parse("1 " + m + year).Month;
                var startDate = new DateTime(Convert.ToInt32(year), monthNUmber, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                fromDate = startDate.ToString("yyyy-MM-dd");
                toDate = endDate.ToString("yyyy-MM-dd");
            }
            else
            {
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                fromDate = startDate.ToString("yyyy-MM-dd");
                toDate = endDate.ToString("yyyy-MM-dd");
            }

            string parentId = "", teacherId = "", studentId = "", isStaff = "", jsonEvents = "";
            if (SessionHelper.CurrentSession.IsParent())
            {
                parentId = SessionHelper.CurrentSession.UserName;
                isStaff = "0";
                studentId = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber.ToString();
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                teacherId = SessionHelper.CurrentSession.UserName;
                isStaff = "1";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                parentId = SessionHelper.CurrentSession.ParentUsername;
                studentId = SessionHelper.CurrentSession.OldUserId.ToString();
                isStaff = "0";
            }
            //uncomment before going live
            //jsonEvents = _calendarService.GetEvents(parentId, studentId, "1", fromDate, toDate, "en", isStaff, teacherId);

            //comment before going live
            //jsonEvents = _calendarService.GetEvents(parentId, "", "1", "2019-07-19", "2019-07-24", "en", "1", "K.HOBBS_WIS");

            var jsonResult = new { defaultDate = DateTime.Parse(fromDate), events = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEventsByDate(string from, string to, string selectDate = null)
        {
            string fromDate = "", toDate = "";

            DateTime now = DateTime.Now;
            if (!string.IsNullOrEmpty(selectDate))
            {
                DateTime oFrom = Convert.ToDateTime(selectDate);
                fromDate = oFrom.ToString("yyyy-MM-dd"); ; // "yyyy-MM-dd"
                toDate = oFrom.ToString("yyyy-MM-dd"); ; // "yyyy-MM-dd"
            }
            else if (!string.IsNullOrEmpty(from))
            {
                DateTime oFrom = Convert.ToDateTime(from);
                DateTime oTo = Convert.ToDateTime(to);
                fromDate = oFrom.ToString("yyyy-MM-dd"); // "yyyy-MM-dd"
                toDate = oTo.ToString("yyyy-MM-dd");  // "yyyy-MM-dd"
            }
            else
            {
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                fromDate = startDate.ToString("yyyy-MM-dd");
                toDate = endDate.ToString("yyyy-MM-dd");
            }

            string parentId = "", teacherId = "", studentId = "", isStaff = "", jsonEvents = "";
            if (SessionHelper.CurrentSession.IsParent())
            {
                parentId = SessionHelper.CurrentSession.UserName;
                isStaff = "0";
                studentId = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber.ToString();
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                teacherId = SessionHelper.CurrentSession.UserName;
                isStaff = "1";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                parentId = SessionHelper.CurrentSession.ParentUsername;
                studentId = SessionHelper.CurrentSession.OldUserId.ToString();
                isStaff = "0";
            }
            //uncomment before going live
            //jsonEvents = _calendarService.GetEvents(parentId, studentId, "1", fromDate, toDate, "en", isStaff, teacherId);

            //comment before going live
            //jsonEvents = _calendarService.GetEvents(parentId, "", "1", "2019-07-19", "2019-07-24", "en", "1", "K.HOBBS_WIS");

            var jsonResult = new { defaultDate = DateTime.Parse(fromDate), events = jsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }
        private string GetUserIdentifiers(out string identifier)
        {
            string _key = string.Empty;
            if (SessionHelper.CurrentSession.IsParent())
            {
                _key = "PARENT";
                identifier = SessionHelper.CurrentSession.UserName;
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                _key = "STUDENT";
                identifier = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
            }
            else identifier = string.Empty;
            return _key;
        }


        public ActionResult Gallery(string fId, string fName, string fText)
        {
            fId = EncryptDecryptHelper.DecryptUrl(fId);
            List<GalleryFile> galleryFolders = new List<GalleryFile>();
            List<GalleryFile> galleryFiles = new List<GalleryFile>();
            List<int> galleryFilesItemCount = new List<int>();
            GalleryViewModel galleryViewModel = new GalleryViewModel();
            _loggerClient = LoggerClient.Instance;
            try
            {

                string parentId = "", teacherId = "", isStaff = "0";
                if (SessionHelper.CurrentSession.IsParent())
                {
                    parentId = SessionHelper.CurrentSession.UserName;
                }
                else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                {
                    teacherId = SessionHelper.CurrentSession.UserName;
                    isStaff = "1";
                }
                else if (SessionHelper.CurrentSession.IsStudent())
                {
                    parentId = SessionHelper.CurrentSession.ParentUsername;
                }
                var fParentId = "0";
                string filterText = string.IsNullOrEmpty(fText) ? "" : fText;
               // if (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent() || SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                    //galleryFolders = _calendarService.GetGallery(parentId, "", "", "en", isStaff, teacherId, fParentId, filterText);

                int fileId = 0;
                string fileParentId = "0";
                if (string.IsNullOrEmpty(fId))
                {
                    fileId = galleryFolders.Select(x => x.FileID).FirstOrDefault();
                    fileParentId = Convert.ToString(fileId);
                }
                else
                {
                    fileParentId = fId;
                }

                if (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent() || SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                {
                    //if (fileParentId != "0")
                       // galleryFiles = _calendarService.GetGallery(parentId, "", "", "en", isStaff, teacherId, fileParentId, "");
                }

                if (galleryFolders == null)
                {
                    galleryFolders = new List<GalleryFile>();
                }
                else if (galleryFiles == null)
                {
                    galleryFiles = new List<GalleryFile>();
                }

                galleryFolders = galleryFolders.Where(c => c.SchoolId == SessionHelper.CurrentSession.SchoolId.ToString()).ToList();
                galleryFiles = galleryFiles.Where(c => c.SchoolId == SessionHelper.CurrentSession.SchoolId.ToString()).ToList();

                if (galleryFolders.Count() > 0)
                {
                    foreach (var item in galleryFolders)
                    {
                        var tempGalleryFiles = new List<GalleryFile>();
                        string fparentId = Convert.ToString(item.FileID);
                        //tempGalleryFiles = _calendarService.GetGallery(parentId, "", "", "en", isStaff, teacherId, fparentId, "");
                        galleryFilesItemCount.Add(tempGalleryFiles.Count());
                    }
                }

                galleryViewModel.GalleryFolders = galleryFolders;
                galleryViewModel.GalleryFiles = galleryFiles;
                galleryViewModel.GalleryFilesItemCount = galleryFilesItemCount;


            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }

            return View(galleryViewModel);
        }

        public ActionResult LoadGalleryFolderData(string fText)
        {
            List<GalleryFile> galleryFolders = new List<GalleryFile>();
            GalleryViewModel galleryViewModel = new GalleryViewModel();
            List<int> galleryFilesItemCount = new List<int>();
            string parentId = "", teacherId = "", isStaff = "0";
            if (SessionHelper.CurrentSession.IsParent())
            {
                parentId = SessionHelper.CurrentSession.UserName;
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                teacherId = SessionHelper.CurrentSession.UserName;
                isStaff = "1";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                parentId = SessionHelper.CurrentSession.ParentUsername;
            }
            var fParentId = "0";
            string filterText = string.IsNullOrEmpty(fText) ? "" : fText;
            //if (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent() || SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                //galleryFolders = _calendarService.GetGallery(parentId, "", "", "en", isStaff, teacherId, fParentId, filterText);
            if (galleryFolders == null)
            {
                galleryFolders = new List<GalleryFile>();
            }
            galleryFolders = galleryFolders.Where(c => c.SchoolId == SessionHelper.CurrentSession.SchoolId.ToString()).ToList();
            if (galleryFolders.Count() > 0)
            {
                foreach (var item in galleryFolders)
                {
                    var tempGalleryFiles = new List<GalleryFile>();
                    string fparentId = Convert.ToString(item.FileID);
                    //tempGalleryFiles = _calendarService.GetGallery(parentId, "", "", "en", isStaff, teacherId, fparentId, "");
                    galleryFilesItemCount.Add(tempGalleryFiles.Count());
                }
            }

            galleryViewModel.GalleryFolders = galleryFolders;
            galleryViewModel.GalleryFilesItemCount = galleryFilesItemCount;

            return PartialView("_GalleryFolderSection", galleryViewModel);
        }

        public JsonResult GetCircularDetails(string circularId)
        {
            Circular circular = new Circular();
            //circular = _calendarService.GetCircularDetails(circularId, "en");

            return Json(circular, JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        //public ActionResult TcRequestGet()
        //{
        //    var viewname = "EnrollActivities";
        //    EnrolllActivityModel model = new EnrolllActivityModel();
        //    IEnumerable<EnrolllActivityModel> EnrolllActivityData = _leaveService.GetListOfActivities("12300400132237");
        //    // model.Email = "ankush.pawar@neososftetch.com";
        //    return View(viewname, EnrolllActivityData);

        //}

    }
}