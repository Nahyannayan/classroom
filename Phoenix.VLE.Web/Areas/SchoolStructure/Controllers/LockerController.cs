﻿using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.Models;
using Phoenix.Models;
using System.Web.Script.Serialization;
using Phoenix.Common.Localization;
using Phoenix.VLE.Web.ViewModels;

namespace Phoenix.VLE.Web.Areas.SchoolStructure.Controllers
{
    public class LockerController : Controller
    {
        private ISchoolService _SchoolService;
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly ITeacherDashboardService _teacherService;
        // GET: Teacher/Locker

        public LockerController(ISchoolService schoolGroupService, ITeacherDashboardService teacherService,
            ISchoolGroupService groupService)
        {
            _SchoolService = schoolGroupService;
            _teacherService = teacherService;
            _schoolGroupService = groupService;
        }
        public ActionResult Index(string vSSId)
        {
            if (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent())
            {
                Request.RequestContext.HttpContext.Server.TransferRequest("/files/Files/Lockers");
                return Content("success");
            }
            else
            {
                if (!string.IsNullOrEmpty(vSSId))
                {
                    Request.RequestContext.HttpContext.Server.TransferRequest("/files/Files/Lockers?vSSId=" + vSSId.ReEncryptUrl());
                    return Content("success");
                }
                Student student = new Student();
                //student.SchoolGroupId = new List<string>();
                //var studentList = _SchoolService.GetStudentForTeacher((int)SessionHelper.CurrentSession.Id);
                //ViewBag.StudentList = studentList;
                var schoolGroupList = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.IsTeacher());
                student.SchoolGroupId = schoolGroupList.Select(m => m.SchoolGroupId.ToString()).ToList();

                schoolGroupList.ToList().ForEach(d => { d.SchoolGroupName = string.IsNullOrEmpty(d.TeacherGivenName) ? d.SchoolGroupDescription.Replace("_", "/") : d.TeacherGivenName; });
                ViewBag.SchoolGroups = schoolGroupList;

                return View("StudentList", student);
            }

        }


        public ActionResult GetListOfStudentDetails(List<Phoenix.Models.SchoolGroup> selectedGroupList, bool isGroupSelected = false, int pageIndex = 1, string searchString = "", string sortBy = "name")
        {
            string Id = "";
            if (isGroupSelected)
            {
                if (selectedGroupList != null )
                {
                    Id = string.Join(",", selectedGroupList.Select(x => x.SchoolGroupId).ToList());
                    Session["SelectedSchoolGroups"] = selectedGroupList;
                }
                else if (Session["SelectedSchoolGroups"] != null)
                {
                    selectedGroupList = (List<Phoenix.Models.SchoolGroup>)Session["SelectedSchoolGroups"];
                    Id = string.Join(",", selectedGroupList.Select(x => x.SchoolGroupId).ToList());
                }
            }
            else
            {
                Session["SelectedSchoolGroups"] = null;
            }
            var model = new Pagination<Student>();
            var studentList = _SchoolService.GetStudentForTeacherBySchoolGroup((int)SessionHelper.CurrentSession.Id, pageIndex, 16, Id, searchString, sortBy);
            if (studentList.Any())
            {
                model = new Pagination<Student>(pageIndex, 16, studentList.ToList(), studentList.First().TotalCount);
            }
            model.LoadPageRecordsUrl = "/SchoolStructure/Locker/GetListOfStudentDetails?pageIndex={0}"+ (isGroupSelected ? "&isGroupSelected="+ isGroupSelected : string.Empty);
            model.SearchString = searchString;
            model.PageIndex = pageIndex;
            model.SortBy = sortBy;
            return PartialView("_StudentDetails", model);
        }

        private string ConvertViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter writer = new StringWriter())
            {
                ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }

        public ActionResult GenerateVaultReport()
        {
            string filePath = Server.MapPath("~/Content/Files/VaultReportFile.xls");
            byte[] bytes = _SchoolService.GenerateStudentVaultReport(filePath);
            return File(bytes, "application/vnd.ms-excel", $"VaultReport_{DateTime.Now.Ticks}.xls");
        }
    }
}