﻿using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Phoenix.Common.Models;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using System.Threading;
using System.Globalization;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using System.Net.Http;
using System.Xml.Serialization;
using Phoenix.VLE.Web.Models;
using Google.Apis.Auth.OAuth2.Mvc;
using System.Text.RegularExpressions;

namespace Phoenix.VLE.Web.Areas.SchoolStructure.Controllers
{
    public class SchoolGroupsController : BaseController
    {
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly IStudentService _studentservice;
        private readonly ITeacherDashboardService _teacherService;
        private readonly IStudentListService _studentListService;
        private readonly IFileService _filesService;
        private ISchoolService _schoolservice;
        private readonly ICommonService _commonService;
        private readonly IGroupCourseTopicService _groupCourseTopicService;
        private readonly IUnitService _unitService;
        private readonly IFolderService _folderService;
        private readonly IPlanSchemeDetailService _planSchemeDetailService;
        private readonly IAsyncLessonService _asyncLessonService;

        public SchoolGroupsController(ISchoolGroupService schoolGroupService, IStudentService studentService, ITeacherDashboardService teacherService,
            IStudentListService studentListService, FileService fileService, ISchoolService schoolService, ICommonService commonService, IGroupCourseTopicService groupCourseTopicService, IUnitService unitService, IFolderService folderService, IPlanSchemeDetailService planSchemeDetailService, IAsyncLessonService asyncLessonService)
        {
            _studentListService = studentListService;
            _schoolGroupService = schoolGroupService;
            _studentservice = studentService;
            _teacherService = teacherService;
            _filesService = fileService;
            _schoolservice = schoolService;
            _commonService = commonService;
            _groupCourseTopicService = groupCourseTopicService;
            _unitService = unitService;
            _folderService = folderService;
            _planSchemeDetailService = planSchemeDetailService;
            _asyncLessonService = asyncLessonService;
        }

        // GET: Users/SchoolGroupsBase
        public ActionResult Index(int pageIndex = 1, int bespokepageIndex = 1)
        {
            Pagination<Phoenix.Models.SchoolGroup> objPagination = new Pagination<Phoenix.Models.SchoolGroup>();
            var fileTypes = _filesService.GetFileTypes();
            ViewBag.FileExtension = fileTypes.Where(r => r.Extension.IndexOf("svg", StringComparison.OrdinalIgnoreCase) == -1).Select(r => r.Extension).ToList();
            ViewBag.ImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();

            var ViewName = "";
            var model = new Pagination<Phoenix.Models.SchoolGroup>();
            ViewName = "NewTeacherGroups";
            model.LoadPageRecordsUrl = "/SchoolStructure/SchoolGroups";
            return View(ViewName, model);

        }

        public ActionResult SearchGroup(string searchString)
        {
            int pageIndex = 1;
            int bespokepageIndex = 1;
            Pagination<Phoenix.Models.SchoolGroup> objPagination = new Pagination<Phoenix.Models.SchoolGroup>();

            var ViewName = "";
            var model = new Pagination<Phoenix.Models.SchoolGroup>();
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var teacherGroups = _schoolGroupService.GetSchoolGroups(pageIndex, 9).Result.ToList();
                var BespokeGroup = _schoolGroupService.GetSchoolGroups(bespokepageIndex, 9, 1).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);

                if (teacherGroups.Any())
                    model = new Pagination<Phoenix.Models.SchoolGroup>(pageIndex, 9, teacherGroups, teacherGroups.FirstOrDefault().TotalCount);
                model.RecordCount = teacherGroups.Count == 0 ? 0 : teacherGroups[0].TotalCount;
                ViewName = "_SchoolGroups";

                ViewBag.BeSpokeGroup = BespokeGroup;
            }
            else
            {
                var childId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var studentGroups = _schoolGroupService.GetStudentGroups(childId, pageIndex, 6, 0, searchString).Result.ToList();
                var BespokeGroup = _schoolGroupService.GetStudentGroups(childId, bespokepageIndex, 6, 1, searchString).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);

                if (studentGroups.Any())
                    model = new Pagination<Phoenix.Models.SchoolGroup>(pageIndex, 6, studentGroups, studentGroups.FirstOrDefault().TotalCount);
                model.RecordCount = studentGroups.Count == 0 ? 0 : studentGroups[0].TotalCount;
                ViewName = "_SchoolGroups";


                ViewBag.BeSpokeGroup = BespokeGroup;
            }
            model.LoadPageRecordsUrl = "/SchoolStructure/SchoolGroups";
            return PartialView(ViewName, model);

        }

        public ActionResult InitAddEditSchoolGroupForm(int? id)
        {
            var model = new SchoolGroupEdit();
            if (id.HasValue)
            {
                var template = _schoolGroupService.GetSchoolGroupByID(id.Value);
                EntityMapper<Phoenix.Models.SchoolGroup, SchoolGroupEdit>.Map(template, model);
            }
            else
            {
                model.IsAddMode = true;
                ViewBag.SubjectList = new SelectList(SelectListHelper.GetSelectListData(ListItems.SubjectName), "ItemId", "ItemName", (int)SessionHelper.CurrentSession.SchoolId);
                model.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            }

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult SaveTeacherGivenName(SchoolGroupEdit model)
        {
            if (string.IsNullOrWhiteSpace(model.TeacherGivenName))
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            var result = _schoolGroupService.UpdateTeacherGivenName(model);

            var schoolGroup = _schoolGroupService.GetSchoolGroupByID(model.SchoolGroupId);
            var groupNewName = $"{schoolGroup.TeacherGivenName} {schoolGroup.AcademicYear}";
            return Json(new OperationDetails(result) { HeaderText = groupNewName }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetGroupsAfterUpdate()
        {
            var teacherDashboard = _teacherService.GetTeacherDashboard();
            var SchoolGroup = teacherDashboard.ClassGroups;
            var model = new Pagination<Phoenix.Models.SchoolGroup>();
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var teacherGroups = _schoolGroupService.GetSchoolGroups(1, 9).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);

                if (teacherGroups.Any())
                    model = new Pagination<Phoenix.Models.SchoolGroup>(1, 9, teacherGroups, teacherGroups.FirstOrDefault().TotalCount);
                model.RecordCount = teacherGroups.Count == 0 ? 0 : teacherGroups[0].TotalCount;
                return PartialView("_SchoolGroups", model.PageRecords);
            }
            else
            {
                return PartialView("_SchoolGroups", SchoolGroup);

            }
        }
        public ActionResult StudentsInGroup(int groupid)
        {
            var students = _studentListService.GetStudentsInGroup(groupid).ToList();
            var StudentList = new List<Student>();
            foreach (var item in students)
            {
                //item.StudentImage = Constants.StudentImagesPath + item.StudentImage;
                item.StudentImage = item.StudentImage;
                StudentList.Add(item);
            }
            return PartialView(StudentList);
        }

        public ActionResult DeleteBespokeGroup(int groupId = 0)
        {
            var UserId = SessionHelper.CurrentSession.Id;
            var result = _schoolGroupService.DeleteSchoolGroup(groupId, UserId);
            //var model = new Pagination<Phoenix.Models.SchoolGroup>();

            //if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            //{
            //    var BespokeGroup = _schoolGroupService.GetSchoolGroups(1, 9, 1, "").ToList();
            //    if (BespokeGroup.Any())
            //        model = new Pagination<Phoenix.Models.SchoolGroup>(1, 9, BespokeGroup, BespokeGroup.FirstOrDefault().TotalCount);
            //    model.RecordCount = BespokeGroup.Count == 0 ? 0 : BespokeGroup[0].TotalCount;
            //}
            //else
            //{
            //    var childId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
            //    var BespokeGroup = _schoolGroupService.GetStudentGroups(childId, 1, 9, 1, "").ToList();
            //    if (BespokeGroup.Any())
            //        model = new Pagination<Phoenix.Models.SchoolGroup>(1, 9, BespokeGroup, BespokeGroup.FirstOrDefault().TotalCount);
            //    model.RecordCount = BespokeGroup.Count == 0 ? 0 : BespokeGroup[0].TotalCount;
            //}
            //model.LoadPageRecordsUrl = "/SchoolStructure/SchoolGroups/LoadBespokeGroup?pageIndex={0}";
            //model.SearchString = "";
            //var operation = new OperationDetails(result);
            //var Jresult = new { Result = ConvertViewToString("_BespokeGroup", model), operation };

            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadSchoolGroups(int pageIndex = 1, string searchString = "")
        {
            var model = new Pagination<Phoenix.Models.SchoolGroup>();
            var oldSearchString = searchString;
            searchString = searchString.Replace(" /", "_");
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var teacherGroups = _schoolGroupService.GetSchoolGroups(pageIndex, 6, 0, searchString).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);

                if (teacherGroups.Any())
                    model = new Pagination<Phoenix.Models.SchoolGroup>(pageIndex, 6, teacherGroups, teacherGroups.FirstOrDefault().TotalCount);
                model.RecordCount = teacherGroups.Count == 0 ? 0 : teacherGroups[0].TotalCount;
            }
            else
            {
                var childId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;

                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var studentGroups = _schoolGroupService.GetStudentGroups(childId, pageIndex, 6, 0, searchString).Result.ToList(); ;
                SynchronizationContext.SetSynchronizationContext(syncContext);

                if (studentGroups.Any())
                    model = new Pagination<Phoenix.Models.SchoolGroup>(pageIndex, 6, studentGroups, studentGroups.FirstOrDefault().TotalCount);
                model.RecordCount = studentGroups.Count == 0 ? 0 : studentGroups[0].TotalCount;
            }
            model.LoadPageRecordsUrl = "/SchoolStructure/SchoolGroups/LoadSchoolGroups?pageIndex={0}";
            model.SearchString = oldSearchString;
            return PartialView("_SchoolGroups", model);
        }

        
        [HttpPost]
        public ActionResult CheckMISGroupsCountForStudentandParent(int pageIndex, string searchString = "")
        {
            bool allMISGroupsHidden = false;

            var childId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
            var result = _schoolGroupService.CheckStudentGroupsAvailable(childId, 0);
            if (result == 0)
                allMISGroupsHidden = true;
            return Json(new OperationDetails(allMISGroupsHidden, "", null), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSchoolGroupsWithPagination(int pageIndex, string searchString = "")
        {
            var viewName = "";
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                viewName = "_NewSchoolGroups";
            else
                viewName = "_NewChildParentGroups";

            return PartialView(viewName, GetSchoolGroupList(pageIndex, searchString));
        }

        private IEnumerable<Phoenix.Models.SchoolGroup> GetSchoolGroupList(int pageIndex, string searchString)
        {
            var fileModulesList = _filesService.GetFileManagementModules().ToList();
            ViewBag.ModuleDetails = fileModulesList.FirstOrDefault(r => r.ModuleName.Contains("Groups"));
            IEnumerable<Phoenix.Models.SchoolGroup> schoolGroups = new List<Phoenix.Models.SchoolGroup>();
            searchString = searchString.Replace(" /", "_");
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                schoolGroups = _schoolGroupService.GetSchoolGroups(pageIndex, 8, 0, searchString).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);
            }
            else
            {
                var childId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                schoolGroups = _schoolGroupService.GetStudentGroups(childId, pageIndex, 8, 0, searchString).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);
            }
            return schoolGroups;
        }

        public ActionResult GetOtherGroupsWithPagination(int pageIndex, string searchString = "")
        {
            var fileModulesList = _filesService.GetFileManagementModules().ToList();
            ViewBag.ModuleDetails = fileModulesList.FirstOrDefault(r => r.ModuleName.Contains("Groups"));

            var viewName = "";
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                viewName = "_NewSchoolGroups";
            else
                viewName = "_NewChildParentGroups";

            return PartialView(viewName, GetOtherGroupsList(pageIndex, searchString));
        }

        private IEnumerable<Phoenix.Models.SchoolGroup> GetOtherGroupsList(int pageIndex, string searchString)
        {
            IEnumerable<Phoenix.Models.SchoolGroup> beSpokeGroups = new List<Phoenix.Models.SchoolGroup>();
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                beSpokeGroups = _schoolGroupService.GetSchoolGroups(pageIndex, 8, 1, searchString).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);

            }
            else
            {
                var childId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                beSpokeGroups = _schoolGroupService.GetStudentGroups(childId, pageIndex, 8, 1, searchString).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);
            }
            return beSpokeGroups;
        }

        /// <summary>
        /// Add by aashish for calss School Permission
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="searchString"></param>
        /// <returns></returns>

        public ActionResult GetOtherschoolgroupsWithPagination(int pageIndex, string searchString = "")
        {
            var fileModulesList = _filesService.GetFileManagementModules().ToList();
            ViewBag.ModuleDetails = fileModulesList.FirstOrDefault(r => r.ModuleName.Contains("Groups"));

            var viewName = "";
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                viewName = "_NewSchoolGroups";
            else
                viewName = "_NewChildParentGroups";

            return PartialView(viewName, GetOtherschoolgroupsList(pageIndex, searchString));
        }

        private IEnumerable<Phoenix.Models.SchoolGroup> GetOtherschoolgroupsList(int pageIndex, string searchString)
        {
            IEnumerable<Phoenix.Models.SchoolGroup> beSpokeGroups = new List<Phoenix.Models.SchoolGroup>();
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                beSpokeGroups = _schoolGroupService.GetOtherschoolgroups(pageIndex, 8, 1, searchString).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);

            }
            else
            {
                var childId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                beSpokeGroups = _schoolGroupService.GetStudentGroups(childId, pageIndex, 8, 1, searchString).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);
            }
            return beSpokeGroups;
        }

        public ActionResult GetArchivedGroupsWithPagination(int pageIndex, string searchString = "")
        {
            var fileModulesList = _filesService.GetFileManagementModules().ToList();
            ViewBag.ModuleDetails = fileModulesList.FirstOrDefault(r => r.ModuleName.Contains("Groups"));
            return PartialView("_NewSchoolGroups", GetArchivedSchoolGroupsList(pageIndex, searchString));
        }

        private IEnumerable<Phoenix.Models.SchoolGroup> GetArchivedSchoolGroupsList(int pageIndex, string searchString)
        {
            IEnumerable<Phoenix.Models.SchoolGroup> archivedGroups = new List<Phoenix.Models.SchoolGroup>();
            searchString = searchString.Replace(" /", "_");
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                archivedGroups = _schoolGroupService.GetArchivedSchoolGroups(pageIndex, 8, 0, searchString).ToList();
            }
            return archivedGroups;
        }

        public ActionResult GetArchivedBespokeGroupsWithPagination(int pageIndex, string searchString = "")
        {
            var fileModulesList = _filesService.GetFileManagementModules().ToList();
            ViewBag.ModuleDetails = fileModulesList.FirstOrDefault(r => r.ModuleName.Contains("Groups"));
            return PartialView("_NewSchoolGroups", GetArchivedBespokeGroupsList(pageIndex, searchString));
        }

        private IEnumerable<Phoenix.Models.SchoolGroup> GetArchivedBespokeGroupsList(int pageIndex, string searchString)
        {
            IEnumerable<Phoenix.Models.SchoolGroup> archivedBespokeGroups = new List<Phoenix.Models.SchoolGroup>();
            searchString = searchString.Replace(" /", "_");
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                archivedBespokeGroups = _schoolGroupService.GetArchivedSchoolGroups(pageIndex, 8, 1, searchString).ToList();
            }

            return archivedBespokeGroups;
        }

        public ActionResult LoadBespokeGroup(int pageIndex = 1, string searchString = "")
        {
            var model = new Pagination<Phoenix.Models.SchoolGroup>();
            var oldSearchString = searchString;
            searchString = searchString.Replace(" /", "_");
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var BespokeGroup = _schoolGroupService.GetSchoolGroups(pageIndex, 9, 1, searchString).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);

                if (BespokeGroup.Any())
                    model = new Pagination<Phoenix.Models.SchoolGroup>(pageIndex, 9, BespokeGroup, BespokeGroup.FirstOrDefault().TotalCount);
                model.RecordCount = BespokeGroup.Count == 0 ? 0 : BespokeGroup[0].TotalCount;
            }
            else
            {
                var childId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var BespokeGroup = _schoolGroupService.GetStudentGroups(childId, pageIndex, 9, 1, searchString).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);

                if (BespokeGroup.Any())
                    model = new Pagination<Phoenix.Models.SchoolGroup>(pageIndex, 9, BespokeGroup, BespokeGroup.FirstOrDefault().TotalCount);
                model.RecordCount = BespokeGroup.Count == 0 ? 0 : BespokeGroup[0].TotalCount;
            }
            model.LoadPageRecordsUrl = "/SchoolStructure/SchoolGroups/LoadBespokeGroup?pageIndex={0}";
            model.SearchString = oldSearchString;
            return PartialView("_BespokeGroup", model);
        }
        public ActionResult LoadGroupMessageForm(int groupdId, string studentIds, string notifyTo, bool sendToAll = false)
        {
            var model = new GroupMessageEdit();
            model.ParentGroupId = groupdId;
            model.SelectedStudentId = studentIds;
            model.NotifyTo = notifyTo;
            model.SendToAll = sendToAll;

            if (sendToAll)
            {
                //To get all school groups for current user
                var groupedOptions = new List<SelectListItem>();
                List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
                lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
                List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
                lstSelectListGroup.Add(new SelectListGroup
                {
                    Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                    Disabled = false
                });

                lstSelectListGroup.Add(new SelectListGroup
                {
                    Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                    Disabled = false
                });
                lstSelectListGroup.Add(new SelectListGroup
                {
                    Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                    Disabled = false
                });

                groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
                {
                    Value = x.SchoolGroupId.ToString(),
                    Text = x.SchoolGroupName.Length > 40 ? x.SchoolGroupName.Substring(0, 40) : x.SchoolGroupName,
                    Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                    //Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
                }).ToList();
                model.GroupList = groupedOptions;
            }

            return PartialView("_GroupMessageForm", model);
        }
        [HttpPost]
        public async Task<ActionResult> SendGroupMessage(GroupMessageEdit groupMessage)
        {
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            string attachmentHtml = string.Empty;
            string attachmentList = string.Empty;
            var groupMembers = new List<Student>();
            if (groupMessage.PostedFiles != null && groupMessage.PostedFiles.Count() > 0)
            {
                var fileEditList = new List<FileEdit>();
                string extension = string.Empty;
                string fileName = string.Empty;
                var fileTypeList = _filesService.GetFileTypes().ToList();
                foreach (var postedFile in groupMessage.PostedFiles)
                {
                    var inputFile = postedFile;
                    extension = Path.GetExtension(inputFile.FileName).Replace(".", "");
                    double fileSizeInMb = (postedFile.ContentLength / 1024f) / 1024f;
                    var extId = fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;
                    var fileEditModel = new FileEdit()
                    {
                        FileName = postedFile.FileName,
                        PostedFile = postedFile,
                        ResourceFileTypeId = (short)ResourceFileTypes.SharePoint,
                        FileTypeId = extId,
                        FolderId = 0,
                        SectionId = groupMessage.ParentGroupId,
                        ModuleId = (int)AsyncLessonModule.Resources,
                        FileSizeInMB = fileSizeInMb,
                        CreatedBy = SessionHelper.CurrentSession.Id
                    };
                    fileEditList.Add(fileEditModel);
                }
                List<FileEdit> fileListWithPath = new List<FileEdit>();
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    fileListWithPath = await azureHelper.UploadFilesListAsPerModuleAsync(FileModulesConstants.MyFiles, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    var syncContext = SynchronizationContext.Current;
                    SynchronizationContext.SetSynchronizationContext(null);
                    fileListWithPath = SharePointHelper.UploadFilesListAsPerModule(ref cx, FileModulesConstants.MyFiles, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList);
                    SynchronizationContext.SetSynchronizationContext(syncContext);
                }


                foreach (var rs in fileListWithPath)
                {
                    var fileExt = Path.GetExtension(rs.FileName).Replace(".", "");
                    attachmentHtml += $"<br/><a href='{rs.FilePath}'>View Attachment</a>";
                    if (string.IsNullOrWhiteSpace(attachmentList))
                    {
                        attachmentList = rs.FilePath;
                    }
                    else
                    {
                        attachmentList = attachmentList + "|" + rs.FilePath;
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(groupMessage.SelectedGroups))
            {
                groupMembers = _studentListService.GetStudentsInSelectedGroups(groupMessage.SelectedGroups).ToList();
            }
            else
            {
                groupMembers = _studentListService.GetStudentsInGroup(groupMessage.ParentGroupId).ToList();
            }


            if (string.IsNullOrWhiteSpace(groupMessage.SelectedStudentId))
            {
                groupMessage.SelectedStudentId = string.Join(",", groupMembers.Select(r => r.Id).ToList());
            }

            groupMessage.Attachment = attachmentList;

            var result = _schoolGroupService.InsertGroupMessage(groupMessage);
            if (result)
            {
                var studentInternalIds = "";

                foreach (var item in groupMembers)
                {
                    if (!string.IsNullOrWhiteSpace(groupMessage.SelectedStudentId))
                    {
                        if (groupMessage.SelectedStudentId.Contains(item.Id.ToString()))
                        {
                            studentInternalIds = studentInternalIds + (string.IsNullOrWhiteSpace(studentInternalIds) ? "" : "|") + item.StudentInternalId;
                        }
                    }
                    else
                    {
                        studentInternalIds = studentInternalIds + (string.IsNullOrWhiteSpace(studentInternalIds) ? "" : "|") + item.StudentInternalId;
                    }
                }

                var sendEmailNotificationView = _schoolGroupService.GenerateEmailTemplateForSendGroupMessage(groupMessage.Title, Server.HtmlDecode(groupMessage.Message), studentInternalIds, groupMessage.NotifyTo, attachmentHtml);
                bool isSuccess = _commonService.SendEmailNotifications(sendEmailNotificationView);
            }
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        private string ConvertViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (System.IO.StringWriter writer = new System.IO.StringWriter())
            {
                ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }

        public ActionResult SchoolGroupMessages(string groupId)
        {
            var id = EncryptDecryptHelper.DecryptUrl(groupId);
            GroupMessage model = new GroupMessage();
            model.ParentGroupId = Convert.ToInt32(id);
            return View(model);
        }
        public ActionResult LoadSchoolGroupMessages(int id)
        {
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var schoolGroupMessages = _schoolGroupService.GetGroupMessageListByGroup(id, userId);
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in schoolGroupMessages
                          select new
                          {
                              item.Title,
                              Message = Regex.Replace(Server.HtmlDecode(item.Message), "<.*?>", String.Empty),
                              CreatedOn = item.CreatedOn.ConvertUtcToLocalTime().Value.ToString("dd MMMM yyyy"),
                              item.NotifyTo

                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        #region Archived School Group
        public ActionResult ArchiveGroup()
        {
            Phoenix.Models.SchoolGroup objGroup = new Phoenix.Models.SchoolGroup();
            int teacherID = (int)SessionHelper.CurrentSession.Id;
            List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
            var lstActive = _schoolGroupService.GetActiveSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true);
            var activeGroups = lstActive.lstActiveGroups.Where(x => x.ArchivedBy == null).ToList();

            //  lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            var lstArchive = _schoolGroupService.GetArchivedSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true);
            objGroup.lstActiveGroups = activeGroups;
            objGroup.lstArchivedGroups = lstArchive.lstArchivedGroups.ToList();

            return View("ArchiveGroup", objGroup);
        }
        public ActionResult GetActiveSchoolGroup()
        {

            int teacherID = (int)SessionHelper.CurrentSession.Id;
            var lstActive = _schoolGroupService.GetActiveSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true);
            return PartialView("_GetActiveSchoolGroup", lstActive.lstActiveGroups.Where(x => x.ArchivedBy == null && x.IsManager == true).ToList());
        }
        public ActionResult GetArchivedSchoolGroup()
        {

            int teacherID = (int)SessionHelper.CurrentSession.Id;
            var schoolGroup = _schoolGroupService.GetArchivedSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true);

            return PartialView("_GetArchivedSchoolGroup", schoolGroup.lstArchivedGroups.ToList());
        }
        [HttpPost]
        public ActionResult UpdateArchiveGroup(string assignmentsToArchive)
        {
            int teacherID = (int)SessionHelper.CurrentSession.Id;
            string[] str = assignmentsToArchive.Split(',');
            foreach (var assignmentId in str)
            {
                if (assignmentId != "")
                {
                    var result = _schoolGroupService.UpdateArchiveGroups(Convert.ToInt64(assignmentId), teacherID);
                }
            }
            return null;
        }
        public ActionResult ArchivedGroupIndex(int pageIndex = 1, int bespokepageIndex = 1)
        {
            string pageName = String.Empty;
            var ViewName = "";
            var model = new Pagination<Phoenix.Models.SchoolGroup>();
            ViewName = "_ArchivedGroupsDetailsByPaging";
            model.LoadPageRecordsUrl = "/SchoolStructure/SchoolGroups";
            return View(ViewName, model);
        }
        public ActionResult LoadArchivedSchoolGroups(int pageIndex = 1, string searchString = "")
        {
            var model = new Pagination<Phoenix.Models.SchoolGroup>();
            var oldSearchString = searchString;
            searchString = searchString.Replace(" /", "_");
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var teacherGroups = _schoolGroupService.GetArchivedSchoolGroups(pageIndex, 6, 0, searchString).ToList();
                if (teacherGroups.Any())
                    model = new Pagination<Phoenix.Models.SchoolGroup>(pageIndex, 6, teacherGroups, teacherGroups.FirstOrDefault().TotalCount);
                model.RecordCount = teacherGroups.Count == 0 ? 0 : teacherGroups[0].TotalCount;
            }

            model.LoadPageRecordsUrl = "/SchoolStructure/SchoolGroups/LoadArchivedSchoolGroups?pageIndex={0}";
            model.SearchString = oldSearchString;
            return PartialView("_ArchivedSchoolGroups", model);
        }
        public ActionResult LoadArchivedBespokeGroup(int pageIndex = 1, string searchString = "")
        {
            var model = new Pagination<Phoenix.Models.SchoolGroup>();
            var oldSearchString = searchString;
            searchString = searchString.Replace(" /", "_");
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var BespokeGroup = _schoolGroupService.GetArchivedSchoolGroups(pageIndex, 9, 1, searchString).ToList();
                if (BespokeGroup.Any())
                    model = new Pagination<Phoenix.Models.SchoolGroup>(pageIndex, 9, BespokeGroup, BespokeGroup.FirstOrDefault().TotalCount);
                model.RecordCount = BespokeGroup.Count == 0 ? 0 : BespokeGroup[0].TotalCount;
            }

            model.LoadPageRecordsUrl = "/SchoolStructure/SchoolGroups/LoadArchivedBespokeGroup?pageIndex={0}";
            model.SearchString = oldSearchString;
            return PartialView("_ArchiveBespokeGroup", model);
        }
        public ActionResult ArchiveGroupList()
        {
            Phoenix.Models.SchoolGroup objGroup = new Phoenix.Models.SchoolGroup();
            int teacherID = (int)SessionHelper.CurrentSession.Id;
            List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
            var lstActive = _schoolGroupService.GetActiveSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true);
            var activeGroups = lstActive.lstActiveGroups.Where(x => x.ArchivedBy == null && x.IsManager == true).ToList();
            //  lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            var lstArchive = _schoolGroupService.GetArchivedSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true);
            objGroup.lstActiveGroups = activeGroups;
            objGroup.lstArchivedGroups = lstArchive.lstArchivedGroups.ToList();
            return PartialView("_ArchiveGroupList", objGroup);
        }

        #region Big Blue Button Live Class Meeting
        private OperationDetails GetMeetingResponseObject(MeetingResponse response)
        {
            var operationalDetails = new OperationDetails();
            if (response.Returncode.Equals("success", StringComparison.OrdinalIgnoreCase))
            {
                bool updateStatus = _schoolGroupService.UpdateGroupMeeting(response);
                operationalDetails = new OperationDetails(updateStatus);
                if (updateStatus)
                    operationalDetails.Message = ResourceManager.GetString("SchoolGroup.SchoolGroup.LiveClassCreatedSuccessfully");
                operationalDetails.HeaderText = response.MeetingID;
                return operationalDetails;
            }
            else
            {
                operationalDetails.Success = false;
                operationalDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                operationalDetails.Message = ResourceManager.GetString("SchoolGroup.SchoolGroup.LiveClassCreationFailed");
            }
            return operationalDetails;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateGroupMeeting(BBBMeetingEdit editModel)
        {
            var operationalDetails = new OperationDetails();
            if (!SessionHelper.CurrentSession.IsTeacher() || !CurrentPagePermission.CanEdit)
            {
                operationalDetails.Success = false;
                operationalDetails.Message = LocalizationHelper.ActionNotPermittedMessage;
                operationalDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(operationalDetails, JsonRequestBehavior.AllowGet);
            }
            MeetingResponse response = MeetingHelper.CreateNewMeeting(editModel.MeetingName, editModel.Duration);
            response.Mode = (short)TransactionModes.Insert;
            response.MeetingName = editModel.MeetingName;
            response.SchoolGroupId = editModel.SchoolGroupId;
            TimeSpan endTimeSpan = DateTime.ParseExact(editModel.StartTime, "hh:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
            endTimeSpan = endTimeSpan.Add(new TimeSpan(0, editModel.Duration, 0));
            DateTime time = DateTime.Today.Add(endTimeSpan);
            response.EndTime = time.ToString("hh:mm tt");
            response.StartDate = editModel.StartDate;
            response.StartTime = editModel.StartTime;
            response.CreatedBy = SessionHelper.CurrentSession.Id;
            operationalDetails = GetMeetingResponseObject(response);
            return Json(operationalDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMeetingJoinUrl(string meetingId)
        {
            var meetingInfo = _schoolGroupService.GetGroupBBBMeetingInfo(meetingId);
            var operationalDetails = new OperationDetails();
            string _url = string.Empty;
            if (SessionHelper.CurrentSession.IsTeacher())
            {
                MeetingResponse response = MeetingHelper.GetMeetingInfo(meetingId);
                if (!response.Returncode.Equals("success", StringComparison.OrdinalIgnoreCase) && response.MessageKey.Equals("notFound", StringComparison.OrdinalIgnoreCase))
                {
                    operationalDetails.Success = false;
                    operationalDetails.Identifier = "CREATENEWMEETING";
                    return Json(new { Result = operationalDetails, MeetingURL = _url }, JsonRequestBehavior.AllowGet);
                }
            }
            _url = MeetingHelper.GetMeetingURL(meetingInfo.CreatedBy, meetingId, meetingInfo.AttendeePW);
            if (string.IsNullOrEmpty(_url))
            {
                operationalDetails.Success = false;
                operationalDetails.Message = ResourceManager.GetString("SchoolGroup.SchoolGroup.LiveClassNotRunning");
                operationalDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
            }
            return Json(new { Result = operationalDetails, MeetingURL = _url }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitGroupMeetingDurationForm(int schoolGroupId)
        {
            var model = new BBBMeetingEdit() { SchoolGroupId = schoolGroupId };
            return PartialView("_GroupMeetingDurationForm", model);
        }

        public ActionResult GetAllGroupBBBMeetings(int schoolGroupId)
        {
            IEnumerable<MeetingResponse> bbbMeetings = new List<MeetingResponse>();
            bbbMeetings = _schoolGroupService.GetAllGroupBBBMeetings(schoolGroupId);
            return PartialView("_BBBGroupsMeetings", bbbMeetings);
        }

        [HttpPost]
        public ActionResult RewampExpiredBBBMeeting(string meetingId)
        {
            var meetingInfo = _schoolGroupService.GetGroupBBBMeetingInfo(meetingId);
            var newMeeting = MeetingHelper.CreateNewMeeting(meetingInfo.MeetingName, Convert.ToInt16(meetingInfo.Duration));
            newMeeting.Mode = (short)TransactionModes.Update;
            newMeeting.MeetingName = meetingInfo.MeetingName;
            newMeeting.SchoolGroupId = meetingInfo.SchoolGroupId;
            newMeeting.CreatedBy = SessionHelper.CurrentSession.Id;
            newMeeting.OldMeetingId = meetingId;
            var operationDetails = GetMeetingResponseObject(newMeeting);
            if (operationDetails.Success)
                operationDetails.HeaderText = MeetingHelper.GetMeetingURL(SessionHelper.CurrentSession.Id, newMeeting.MeetingID,
                        newMeeting.AttendeePW);

            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        #endregion Live Classs

        #region Zip File Create/Downloads
        public ActionResult CreateZipFile(int id)
        {
            var op = new OperationDetails();
            var allFiles = _filesService.GetAllGroupFiles(id);
            List<string> filePathList = new List<string>();
            foreach (var file in allFiles)
            {
                var filePath = PhoenixConfiguration.Instance.WriteFilePath + file.FilePath;
                if (System.IO.File.Exists(filePath))
                {
                    filePathList.Add(filePath);
                }
            }
            if (filePathList.Count > 0)
            {
                try
                {
                    var directoryPath = PhoenixConfiguration.Instance.WriteFilePath + string.Format(Constants.SchoolGroupZipFilePath, allFiles.FirstOrDefault().ModuleId);
                    string fullFilePath = $"{directoryPath}{id.ToString()}.zip";
                    Common.Helpers.CommonHelper.CreateDestinationFolder(directoryPath);
                    if (System.IO.File.Exists(fullFilePath))
                        System.IO.File.Delete(fullFilePath);
                    DownloadHelper.DownloadZipFile(filePathList, fullFilePath);
                    op.Success = true;
                    op.InsertedRowId = id;
                    op.HeaderText = $"{PhoenixConfiguration.Instance.ReadFilePath}{string.Format(Constants.SchoolGroupZipFilePath, allFiles.FirstOrDefault().ModuleId)}{id.ToString()}.zip";
                    op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
                    op.Message = ResourceManager.GetString("SchoolGroup.SchoolGroup.ZipCreatedSuccessfully");
                }
                catch
                {
                    op.Success = false;
                    op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                    op.Message = LocalizationHelper.TechnicalErrorMessage;
                }
            }
            else
            {
                op.Success = false;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                op.Message = op.Message = ResourceManager.GetString("SchoolGroup.SchoolGroup.NoFilesToZip");
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ClearGroupFiles(int schoolGroupId)
        {
            var op = new OperationDetails();
            int result = _filesService.ClearGroupFiles(SessionHelper.CurrentSession.Id, schoolGroupId);
            var directoryPath = PhoenixConfiguration.Instance.WriteFilePath + string.Format(Constants.SchoolGroupZipFilePath, ((short)FileManagementModuleTypes.Groups).ToString());
            string fullFilePath = $"{directoryPath}{schoolGroupId.ToString()}.zip";
            if (System.IO.File.Exists(fullFilePath))
                System.IO.File.Delete(fullFilePath);
            if (result == -1)
            {
                op.Success = false;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                op.Message = ResourceManager.GetString("SchoolGroup.SchoolGroup.NoFilesToClear");
            }
            else op = new OperationDetails(result == 1);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region    Merged methods from SchoolGroup Area

        public ActionResult AddEditSchoolGroupForm(int? id)
        {
            var model = new SchoolGroupEdit();
            if (id.HasValue)
            {
                int groupId = id.GetValueOrDefault();
                var template = _schoolGroupService.GetSchoolGroupByID(id.Value);
                ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName", SessionHelper.CurrentSession.SchoolId);
                ViewBag.StudentInGroups = _studentListService.GetStudentsInGroup(groupId);
                EntityMapper<Phoenix.Models.SchoolGroup, SchoolGroupEdit>.Map(template, model);
            }
            else
            {
                model.IsAddMode = true;
                ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName", SessionHelper.CurrentSession.SchoolId);
                model.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            }

            return View(model);
        }

        public ActionResult AddEditNewGroupForm(string id)
        {
            var model = new SchoolGroupEdit();
            if (!string.IsNullOrWhiteSpace(id) && id != "0")
            {
                int groupId = Helpers.CommonHelper.GetDecryptedId(id);
                var template = _schoolGroupService.GetSchoolGroupByID(groupId);

                //SubjectId used to pass courseId while insert, Deepak Singh 21/july/2020
                template.SubjectId = template.CourseId.HasValue ? (int)template.CourseId.Value : template.SubjectId;

                ViewBag.CourseList = new SelectList(SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)), "ItemId", "ItemName");
                ViewBag.StudentInGroups = _studentListService.GetStudentsInGroup(groupId);
                EntityMapper<Phoenix.Models.SchoolGroup, SchoolGroupEdit>.Map(template, model);
                model.IsActive = true;
            }
            else
            {
                model.IsAddMode = true;
                model.IsBespokeGroup = true;
                model.IsActive = true;
                model.AutoSyncGroupMember = false;
                ViewBag.CourseList = new SelectList(SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)), "ItemId", "ItemName");
                model.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            }

            return PartialView("_AddEditNewGroupForm", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveGroupDetails(SchoolGroupEdit model)
        {
            var operationDetails = new OperationDetails();
            if (string.IsNullOrWhiteSpace(model.SchoolGroupName))
            {
                operationDetails.Success = false;
                operationDetails.Message = LocalizationHelper.ValidationFailedMessage;
                operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                return Json(new { operationDetails }, JsonRequestBehavior.AllowGet);
            }


            if (model.PostedImage != null)
            {
                //SharePointHelper sh = new SharePointHelper();
                //Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint();
                //string SharePTUploadedFilePath = string.Empty;
                //SharePTUploadedFilePath = SharePointHelper.UploadFilesAsPerModule(ref cx, SessionHelper.CurrentSession.SchoolCode, FileModulesConstants.GroupImage, model.PostedImage);
                string fileName = string.Empty;
                string relativePath = string.Empty;
                string extension = string.Empty;

                fileName = model.PostedImage.FileName.Replace(" ", "_");
                extension = Path.GetExtension(model.PostedImage.FileName).Replace(".", "");

                string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                string groupImageDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.GroupImageDir;
                string groupSchoolDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.GroupImageDir + "/School_" + SessionHelper.CurrentSession.SchoolId;
                string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.GroupImageDir + "/School_" + SessionHelper.CurrentSession.SchoolId + "/User_" + SessionHelper.CurrentSession.Id;

                Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                Common.Helpers.CommonHelper.CreateDestinationFolder(groupImageDir);
                Common.Helpers.CommonHelper.CreateDestinationFolder(groupSchoolDir);
                Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                fileName = Guid.NewGuid() + "_" + fileName;
                relativePath = Constants.GroupImageDir + "/School_" + SessionHelper.CurrentSession.SchoolId + "/User_" + SessionHelper.CurrentSession.Id + "/" + fileName;

                //model.GroupImage = SharePTUploadedFilePath;
                model.GroupImage = relativePath;
                fileName = Path.Combine(filePath, fileName);
                model.PostedImage.SaveAs(fileName);
            }

            var groupId = _schoolGroupService.UpdateSchoolGroupDetails(model);

            if (groupId < 0)
            {
                operationDetails.Success = false;
                operationDetails.Message = LocalizationHelper.ItemExistsMessage.Replace("{0}", ResourceManager.GetString("SchoolGroup.SchoolGroup.SchoolGroupName"));
                operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(new { operationDetails }, JsonRequestBehavior.AllowGet);
            }

            if (model.IsAddMode)
            {
                List<GroupMemberMapping> addTeacher = new List<GroupMemberMapping> { new GroupMemberMapping { GroupId = groupId, MemberId = SessionHelper.CurrentSession.Id, IsManager = true } };
                _schoolGroupService.AddUpdateMembersToGroup(addTeacher);
            }
            var result = false;
            if (groupId != 0)
                result = true;
            operationDetails.Success = result;
            operationDetails.NotificationType = result ? StringEnum.GetStringValue(NotificationTypeConstants.Success) : StringEnum.GetStringValue(NotificationTypeConstants.Error);
            operationDetails.Message = result ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.TechnicalErrorMessage;
            operationDetails.InsertedRowId = groupId;
            operationDetails.Identifier = EncryptDecryptHelper.EncryptUrl(groupId.ToString());

            return Json(new { operationDetails, isAddMode = model.IsAddMode }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStudentsNotInGroup(int groupId)
        {
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var lstStudents = _schoolservice.GetStudentForTeacher(SessionHelper.CurrentSession.Id, "");
                return PartialView("_getStudents", lstStudents);
            }
            else
            {
                var lstStudents = _studentListService.GetStudentsNotInGroup((int)SessionHelper.CurrentSession.SchoolId, groupId);
                return PartialView("_getStudents", lstStudents);
            }
        }

        public ActionResult GetStudentsInGroup(int groupId)
        {
            var lstStudents = _studentListService.GetStudentsInGroup(groupId);
            var result = new { Result = ConvertViewToString("_GetUnAssignedMembers", lstStudents), StudentList = lstStudents };
            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView("_GetUnAssignedMembers", lstStudents);
        }

        public ActionResult GetStudentinSelectedList(string[] studentIds)
        {
            List<Student> objStudentList = new List<Student>();

            string Id = "";
            if (studentIds != null)
            {
                Id = string.Join(",", studentIds);
            }

            objStudentList = _studentListService.GetStudentDetailsByIds(Id).ToList();

            var result = new { Result = ConvertViewToString("_GetUnAssignedMembers", objStudentList) };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberPage(int groupId)
        {
            Session["GroupId"] = groupId;
            var userTypes = SelectListHelper.GetSelectListData(ListItems.UserTypes);
            var userIdToRemove = (int)UserTypes.Parent;
            var userTypeToRemove = userTypes.Where(m => m.ItemId.Trim() == userIdToRemove.ToString()).FirstOrDefault();
            userTypes.Remove(userTypeToRemove);
            userTypes.Add(new ListItem
            {
                ItemId = "0",
                ItemName = "Everyone"
            });
            userTypes.Add(new ListItem
            {
                ItemId = (Convert.ToInt32(userTypes.OrderBy(m => m.ItemId).LastOrDefault().ItemId) + 1).ToString(),
                ItemName = "Other Groups"
            });
            ViewBag.MemberTypes = new SelectList(userTypes, "ItemId", "ItemName");
            GroupMemberViewModel groupMemberViewModel = new GroupMemberViewModel();
            //var LstofSchoolGroups = _teacherDashboard.GetTeacherDashboard().SchoolGroups;
            var LstofSchoolGroups = _schoolGroupService.GetSchoolGroupsBySchoolId(SessionHelper.CurrentSession.SchoolId);

            groupMemberViewModel.AssignedMemberList = _schoolGroupService.GetAssignedMembers(groupId).ToList();
            //  ViewBag.SchoolGroup = new SelectList(LstofSchoolGroups, "SchoolGroupId", "SchoolGroupName");
            List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
            lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });

            var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName,
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                //Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
            }).ToList();
            ViewBag.SchoolGroup = groupedOptions;

            //ViewBag.SchoolGroup = new SelectList(lstSelectListGroup, "SchoolGroupId", "SchoolGroupName");




            groupMemberViewModel.UnAssignedMemberList = new List<ListItem>();
            // var result = new { Result = ConvertViewToString("_GetMemberDetails", groupMemberViewModel) };
            // return Json(result, JsonRequestBehavior.AllowGet);
            return PartialView("_GetMemberDetails", groupMemberViewModel);
        }

        public ActionResult GetMembersByType(string memberType)
        {
            if (memberType == "Student" && (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin))
            {
                var lstStudents = _schoolservice.GetStudentForTeacher(SessionHelper.CurrentSession.Id, "");
                //var result = new { Result = ConvertViewToString("_getStudents", lstStudents) };
                //return Json(result, JsonRequestBehavior.AllowGet);
                return PartialView("_getStudents", lstStudents);

            }
            return View();
        }

        public ActionResult GetUnassignedMembers(int? memberTypeId, string groupId)
        {

            string[] group = groupId.Split(',');
            int GroupId = Convert.ToInt32(Session["GroupId"]);
            var schoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
            var unassignedMembers = new List<ListItem>();
            unassignedMembers = _schoolGroupService.GetUnassignedMembers(schoolId, SessionHelper.CurrentSession.Id, memberTypeId, GroupId, groupId).ToList();
            //var unassignedMembers = _schoolGroupService.GetUnassignedMembers(schoolId, SessionHelper.CurrentSession.Id, memberTypeId, Convert.ToInt32(item)).ToList();
            //var AssignedMemberList = _schoolGroupService.GetAssignedMembers(GroupId).ToList();
            //var unassignedMembers = list1.GroupBy(i => i.ItemId).Select(i => i.FirstOrDefault()).ToList();
            //var result = unassignedMembers.Where(p => !AssignedMemberList.Any(p2 => p2.MemberId.ToString() == p.ItemId.ToString()));
            return PartialView("_GetUnAssignedMembers", unassignedMembers.ToList());
        }

        public ActionResult GetAssignedMembers(int groupId)
        {
            var assignedMembers = _schoolGroupService.GetAssignedMembers(groupId).ToList();
            return PartialView("_GetAssignedMembers", assignedMembers);
        }

        public ActionResult GetStudentByGroupId(int groupid = 0, int formGroupId = 0)
        {
            var strGroupid = groupid.ToString();
            var StudentList = _studentListService.GetStudentsInSelectedGroups(strGroupid);
            List<ListItem> studentList = new List<ListItem>();

            var assignedMembers = _schoolGroupService.GetAssignedMembers(formGroupId).ToList();

            foreach (var student in StudentList)
            {
                var exist = assignedMembers.Where(r => r.MemberId == student.Id).FirstOrDefault();
                if (exist == null)
                {
                    studentList.Add(new ListItem { ItemId = student.Id.ToString(), ItemName = $"{student.FirstName} {student.LastName}" });
                }
            }
            return PartialView("_GetUnAssignedMembers", studentList);
        }

        [HttpPost]
        public ActionResult AddUpdateMembersToGroup(string membersToAdd, string classGroups)
        {
            var js = new JavaScriptSerializer();
            List<GroupMemberMapping> memberList = js.Deserialize<List<GroupMemberMapping>>(membersToAdd);
            memberList.ForEach(r => r.ClassGroupIds = classGroups);
            memberList = memberList.Select(e => { e.UpdatedBy = SessionHelper.CurrentSession.Id; return e; }).ToList();
            var result = _schoolGroupService.AddUpdateMembersToGroup(memberList);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubjectSchoolGroups(int[] id)
        {
            int? id1 = 1;
            var subjectId = id1.HasValue ? id1.Value : 0;

            long schoolId = SessionHelper.CurrentSession.SchoolId;
            var schoolGroups = _schoolGroupService.GetSubjectSchoolGroups(subjectId, schoolId);

            var schoolGroupList = schoolGroups.Select(i => new ListItem()
            {
                ItemName = i.SchoolGroupDescription == string.Empty ? i.SchoolGroupName.Replace("_", "/") : i.SchoolGroupDescription.Replace("_", "/"),
                ItemId = i.SchoolGroupId.ToString()
            });
            return Json(schoolGroupList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region MS Teams Integration
        public ActionResult InitMSTeamsForm(int schoolgroupId)
        {
            TeamsMeetingEdit model = new TeamsMeetingEdit();
            model.SchoolGroupId = schoolgroupId;
            ViewBag.SchoolGroupId = schoolgroupId;
            return PartialView("_MsTeamsMeetingForm", model);
        }

        private string GetMSTeamsEventsPayload(TeamsMeetingEdit meetingEdit)
        {
            var participantsList = _schoolGroupService.GetSchoolGroupUserEmailAddress(meetingEdit.SchoolGroupId.ToString(), string.Empty).ToList();
            var participantsPayloads = new List<dynamic>();
            participantsList.ForEach(e =>
            {
                participantsPayloads.Add(new
                {
                    type = "required",
                    emailAddress = new
                    {
                        address = e.MicorosoftMailAddress,
                        name = e.StudentName
                    }
                });

            });

            TimeZone curTimeZone = TimeZone.CurrentTimeZone;
            var eventPayload = new
            {
                subject = meetingEdit.MeetingName,
                body = new
                {
                    contentType = "HTML"
                },
                start = new
                {
                    dateTime = GetUTCDateTime(meetingEdit.MeetingDate, meetingEdit.MeetingStartTime),
                    timeZone = "UTC"
                },
                end = new
                {
                    dateTime = GetUTCDateTime(meetingEdit.MeetingDate, meetingEdit.MeetingEndTime),
                    timeZone = "UTC"
                },
                allowNewTimeProposals = true,
                isOnlineMeeting = true,
                onlineMeetingProvider = "teamsForBusiness",
                attendees = participantsPayloads
            };

            return JsonConvert.SerializeObject(eventPayload);
        }

        private string GetUTCDateTime(string dateString, string timeString)
        {
            TimeSpan timeSpan = DateTime.ParseExact(timeString, "hh:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
            return Convert.ToDateTime(dateString).Add(timeSpan).ConvertLocalToUTC().Value
                      .ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");

        }

        private string GetMSMeetingPayload(TeamsMeetingEdit meetingEdit)
        {
            var meetingStartDateTime = GetUTCDateTime(meetingEdit.MeetingDate, meetingEdit.MeetingStartTime);
            var meetingEndDateTime = GetUTCDateTime(meetingEdit.MeetingDate, meetingEdit.MeetingEndTime);
            var meetingBody = new
            {
                startDateTime = meetingStartDateTime,
                endDateTime = meetingEndDateTime,
                subject = meetingEdit.MeetingName
            };

            return JsonConvert.SerializeObject(meetingBody);
        }

        //[HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveMSTeamsMeeting(TeamsMeetingEdit meetingEdit)
        {
            //convert arabic dates
            meetingEdit.MeetingDate = ((DateTime)meetingEdit.MeetingDate.ToSystemReadableDate()).ToString("dd-MMM-yyyy");
            meetingEdit.MeetingStartTime = meetingEdit.MeetingStartTime.ToSystemReadableTime();
            meetingEdit.MeetingEndTime= meetingEdit.MeetingEndTime.ToSystemReadableTime();
            var operationalDetails = new OperationDetails();
            MicrosoftTeamsHelper microsoftTeamsHelper = new MicrosoftTeamsHelper();
            //string payloads = GetMSMeetingPayload(meetingEdit);
            var eventData = await microsoftTeamsHelper.CreateEvent(GetMSTeamsEventsPayload(meetingEdit));
            //var result = await microsoftTeamsHelper.CreateMeeting(payloads);
            MSTeamsReponse meetingInfo = JsonConvert.DeserializeObject<MSTeamsReponse>(eventData.Body);
            if (eventData.Body != null && !string.IsNullOrEmpty(meetingInfo.id))
            {
                meetingInfo.SchoolGroupId = meetingEdit.SchoolGroupId;
                meetingInfo.CreatedBy = SessionHelper.CurrentSession.Id;
                meetingInfo.Start.DateTime = GetUTCDateTime(meetingEdit.MeetingDate, meetingEdit.MeetingStartTime);
                meetingInfo.End.DateTime = GetUTCDateTime(meetingEdit.MeetingDate, meetingEdit.MeetingEndTime);
                bool res = _schoolGroupService.UpdateTeamsMeetingInfo(meetingInfo);
                operationalDetails = new OperationDetails(res);
                operationalDetails.InsertedRowId = meetingEdit.SchoolGroupId;
                var browserName = Request.Browser.Browser.ToLower();
                string meetingJoinUrl = (browserName.Equals("chrome") ||
                     browserName.Contains("edge")) ? meetingInfo.joinWebUrl : meetingInfo.joinUrl;
                operationalDetails.HeaderText = meetingJoinUrl;

            }
            else
            {
                operationalDetails.Success = false;
                operationalDetails.Message = eventData.Body.Contains("Error authenticating with resource") ? LocalizationHelper.ActionNotPermittedMessage : LocalizationHelper.TechnicalErrorMessage;
                operationalDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);

            }
            return Json(operationalDetails, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> CheckMSTeamsAccessToken()
        {
            var op = new OperationDetails();
            op.Success = true;
            Helpers.CommonHelper.AppendCookie("gdiveredirecturi", "driveredirect", Request.UrlReferrer.AbsoluteUri);
            var token = Session["microsofttoken"] as MicrosoftResponseTokenView;
            if (token == null)
            {
                op.Success = false;
                op.HeaderText = CloudFilesHelper.GetOneDriveAuthUrl();
            }
            else if (token.AccessDateTime.AddSeconds(token.ExpiresIn) < DateTime.Now.AddMinutes(-3))
            {
                await CloudFilesHelper.RefreshOneDriveAccesToken(apiResource: "https://graph.microsoft.com/");
            };
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public void ForceADLogin()
        {
            HttpContext.GetOwinContext().Authentication.Challenge(
                   new AuthenticationProperties { RedirectUri = "/schoolstructure/schoolgroups" },
                   OpenIdConnectAuthenticationDefaults.AuthenticationType);
        }


        public ActionResult GetAllTeamsMeetings(int schoolGroupId)
        {
            var schoolGroupMeetings = _schoolGroupService.GetAllTeamsMeetings(schoolGroupId);
            return PartialView("_PreviousGroupLessons", schoolGroupMeetings);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteTeamsSession(MSTeamsReponse model)
        {
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            bool result = await _schoolGroupService.DeleteTeamsSession(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Zoom Meeting Integration

        public ActionResult InitZoomMeetingForm(int schoolGroupId)
        {
            var meetingEdit = new ZoomMeetingEdit() { SchoolGroupId = schoolGroupId };
            ViewBag.MeetingTimes = new SelectList(SelectListHelper.GetSelectListData(ListItems.LiveSessionDuration), "ItemId", "ItemName");
            //ViewBag.MeetingTimes = Enumerable.Range(03, 30).Select(i => new SelectListItem { Value = (i * 5).ToString(), Text = (i * 5).ToString() }).ToList();
            return PartialView("_ZoomMeetingEdit", meetingEdit);
        }

        [HttpPost]
        public async Task<ActionResult> InitTimeTableSessionForm(ZoomMeetingView model)
        {
            var meetingEdit = new ZoomMeetingEdit() { SchoolGroupId = model.SchoolGroupId };
            meetingEdit.MeetingDate = Convert.ToDateTime(model.FormattedMeetingDateTime);
            meetingEdit.MeetingPassword = Common.Helpers.CommonHelper.GenerateRandomPassword();
            meetingEdit.MeetingName = model.MeetingName;
            var endTime = Convert.ToDateTime(model.EndTime).TimeOfDay;
            var startTime = Convert.ToDateTime(model.MeetingTime).TimeOfDay;
            var durationInMinutes = endTime.Subtract(startTime).TotalMinutes;
            ViewBag.MeetingTimes = new SelectList(SelectListHelper.GetSelectListData(ListItems.LiveSessionDuration), "ItemId", "ItemName", durationInMinutes);
            return PartialView("_ZoomMeetingEdit", meetingEdit);
        }

        public ActionResult SaveZoomMeeting(ZoomMeetingEdit model)
        {
            // var data = await MeetingHelper.ValidateZoomUser(SessionHelper.CurrentSession.Email);
            bool result = false;
            //convert arabic dates
            model.MeetingDate = (DateTime)model.strMeetingDate.ToSystemReadableDate();
            model.MeetingTime = model.MeetingTime.ToSystemReadableTime();
            TimeSpan timeSpan = new TimeSpan(0,0,0,0);
            if(Request.UrlReferrer.AbsolutePath.ToLower().Contains("planner"))
                timeSpan = DateTime.ParseExact(model.MeetingTime, "hh:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
            else 
                timeSpan = DateTime.ParseExact(model.MeetingTime, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;
            var formattedMeetingDateTime = Convert.ToDateTime(model.MeetingDate).Add(timeSpan).ConvertLocalToUTC().Value
                       .ToString("yyyy-MM-ddTHH:mm:ss");
            model.FormattedMeetingDateTime = formattedMeetingDateTime;
            model.ContactName = $"{SessionHelper.CurrentSession.FirstName} {SessionHelper.CurrentSession.LastName}";
            model.ZoomEmail = SessionHelper.CurrentSession.Email;
            model.SchoolGroupId = model.SchoolGroupId;
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            var meetingInfo = _schoolGroupService.CreateSynchronousLesson(model);
            result = !string.IsNullOrEmpty(meetingInfo.id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> LiveZoomMeeting(string meetingId)
        {
            if (Request.UrlReferrer == null)
                return RedirectToAction("Index");
            bool isDuplicateParticipants = false;
            var zoomMeetingInfo = _schoolGroupService.GetAllZoomMeetings(0, SessionHelper.CurrentSession.Id, EncryptDecryptHelper.DecryptUrl(meetingId)).FirstOrDefault();
            if (zoomMeetingInfo.CreatedBy != SessionHelper.CurrentSession.Id)
            {
                var result = new ZoomMeetingResponse();  //await MeetingHelper.GetMeetingParticipants(EncryptDecryptHelper.DecryptUrl(meetingId)); // API CALL if required
                string zoomUserName = $"{SessionHelper.CurrentSession.FirstName} {SessionHelper.CurrentSession.LastName} ({SessionHelper.CurrentSession.UserName})";
                if (result.participants != null)
                    isDuplicateParticipants = result.participants.Any(e => e.user_name == zoomUserName);
            }
            zoomMeetingInfo.IsPartipantsExistWithSameName = isDuplicateParticipants;
            return View("_ZoomMeetingJoin", zoomMeetingInfo);
        }

        public ActionResult GetAllGroupZoomMeetings(int schoolGroupId, int timeZoneOffset)
        {
            long userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var meetingLists = _schoolGroupService.GetAllZoomMeetings(schoolGroupId, userId);
            foreach (var item in meetingLists)
            {
                item.start_time = Phoenix.VLE.Web.Helpers.CommonHelper.ConvertUtcToLocalTime(Convert.ToDateTime(item.start_time), timeZoneOffset).ToString();
            }
            return PartialView("_ZoomMeetings", meetingLists);
        }

        public ActionResult GetGroupRecordedSessions(int groupId, string searchText, string fromDate, string toDate, int pageIndex = 1)
        {
            var model = new Pagination<RecordedSessionView>();
            var sessionList = _schoolGroupService.GetGroupRecordedSessions(groupId, pageIndex, 20, searchText, fromDate, toDate);
            if (sessionList.Any())
            {
                foreach (var item in sessionList)
                {
                    item.MeetingDate = Convert.ToDateTime(item.MeetingDateTime).ConvertUtcToLocalTime().Value;
                }
                model = new Pagination<RecordedSessionView>(pageIndex, 20, sessionList.ToList(), sessionList.FirstOrDefault().TotalCount);
            }
            model.LoadPageRecordsUrl = "/SchoolStructure/SchoolGroups/GetGroupRecordedSessions?pageIndex={0}&fromDate=" + fromDate + "&toDate=" + toDate + "&searchText="
                    + searchText + "&groupId=" + groupId;
            model.SearchString = searchText;
            model.PageIndex = pageIndex;
            return PartialView("_RecordedSessionList", model);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteLiveSession(string meetingId)
        {
            var op = new OperationDetails();
            if (!(SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin))
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }

            ZoomMeetingView liveSession = new ZoomMeetingView();
            liveSession.ZoomMeetingId = meetingId;
            liveSession.CreatedBy = SessionHelper.CurrentSession.Id;
            bool result = await _schoolGroupService.DeleteLiveSession(liveSession);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Adobe Connect Now Meeting
        public ActionResult InitConnectNowMeetingForm(int schoolGroupId)
        {
            var meetingEdit = new ConnectNowMeetingEdit() { SchoolGroupId = schoolGroupId };
            return PartialView("_ConnectNowMeetingEdit", meetingEdit);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveConnectNowMeeting(ConnectNowMeetingEdit model)
        {
            var op = new OperationDetails();
            if (!(SessionHelper.CurrentSession.IsAdmin || SessionHelper.CurrentSession.IsTeacher()))
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                op.Success = false;
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            AdobeConnectMeeting meeting = await AdobeConnectMeetingHelper.CreateConnectNowMeeting(model);
            if (!string.IsNullOrEmpty(meeting.MeetingName))
            {
                meeting.CreatedBy = SessionHelper.CurrentSession.Id;
                meeting.SchoolGroupId = model.SchoolGroupId;
                bool result = _schoolGroupService.SaveGroupAdobeMeeting(meeting);
                op = new OperationDetails(result);
            }
            else
            {
                op.Message = LocalizationHelper.TechnicalErrorMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllAdobeMeetings(int schoolGroupId)
        {
            return PartialView("_AdobeGroupMeetings", _schoolGroupService.GetAllAdobeMeetings(schoolGroupId));
        }
        #endregion

        #region Web-Ex Meetings 
        public ActionResult InitWebExMeetingForm(int schoolGroupId)
        {
            BBBMeetingEdit meetingEdit = new BBBMeetingEdit() { SchoolGroupId = schoolGroupId };
            return PartialView("_WebExMeetingEdit", meetingEdit);
        }

        [HttpPost]
        public ActionResult SaveWebExMeeting(BBBMeetingEdit meetingEdit)
        {
            var op = new OperationDetails();
            WebExMeeting meetingInfo = MeetingHelper.CreateWebExMeeting(meetingEdit);
            if (meetingInfo.Header.Response.Result.Equals("success", StringComparison.OrdinalIgnoreCase))
            {
                meetingInfo.Body.BodyContent.Duration = meetingEdit.Duration;
                meetingInfo.Body.BodyContent.MeetingName = meetingEdit.MeetingName;
                meetingInfo.Body.BodyContent.StartDate = meetingEdit.StartDate;
                meetingInfo.Body.BodyContent.StartTime = meetingEdit.StartTime;
                meetingInfo.Body.BodyContent.SchoolGroupId = meetingEdit.SchoolGroupId;
                meetingInfo.Body.BodyContent.CreatedBy = SessionHelper.CurrentSession.Id;
                bool result = _schoolGroupService.UpdateGroupWebExMeeting(meetingInfo);
                op = new OperationDetails(result);
            }
            else
            {
                op.Success = false;
                op.Message = LocalizationHelper.TechnicalErrorMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);

            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetAllWebExMeetings(int schoolGroupId)
        {
            return PartialView("_GroupWebExMeetings", _schoolGroupService.GetAllGroupWebExMeetings(schoolGroupId));
        }


        public ActionResult GetWebExMeetingUrl(string meetingKey)
        {
            var op = new OperationDetails();

            string meetingUrl = MeetingHelper.GetWebExMeetingUrl(meetingKey);
            if (string.IsNullOrEmpty(meetingUrl))
            {
                op.Success = false;
                op.Message = LocalizationHelper.TechnicalErrorMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
            }
            else
            {
                op.Success = true;
                op.HeaderText = meetingUrl;
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region School Group Details
        public ActionResult GroupDetails(string grpId)
        {
            var model = new Phoenix.Models.SchoolGroup();
            var groupId = (int)Helpers.CommonHelper.GetDecryptedIdFromEncryptedString(grpId);
            var assignedMembers = _schoolGroupService.GetAssignedMembers(groupId);
            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;

            //To check if user accessing group details belong to group or not. 16-Aug-2020 Deepak Singh
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var isUserBelongToGroup = assignedMembers.Count() > 0 ? assignedMembers.Any(r => r.MemberId == userId) : false;

            if (isUserBelongToGroup || SessionHelper.CurrentSession.IsAdmin)
            {
                model = _schoolGroupService.GetSchoolGroupByID(groupId);
                var fileTypes = _filesService.GetFileTypes();
                ViewBag.FileExtension = fileTypes.Where(r => r.Extension.IndexOf("svg", StringComparison.OrdinalIgnoreCase) == -1).Select(r => r.Extension).ToList();
                ViewBag.ImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();
                ViewBag.GroupMemberCount = assignedMembers.Where(r => r.UserTypeId == 1).ToList().Count();


                int permission = 0;
                if (SessionHelper.CurrentSession.IsAdmin || SessionHelper.CurrentSession.UserTypeId == 5)
                {
                    permission = 3;
                }
                else
                {
                    permission = _folderService.GetGroupPermission(Convert.ToInt32(SessionHelper.CurrentSession.Id), groupId);
                    Session["GroupPermission"] = permission;
                }

                //For archived groups, implemented default permission to deny add/delete features
                if (model.ArchivedBy.HasValue && model.ArchivedOn.HasValue && DateTime.Compare(model.ArchivedOn.Value.Date, DateTime.Today) <= 0)
                {
                    ViewBag.IsCustomPermission = false;
                    ViewBag.GroupPermission = 1;
                }
                else
                {
                    ViewBag.IsCustomPermission = (permission >= 2);
                    ViewBag.GroupPermission = permission;
                }
            }
            return View(model);
        }

        public ActionResult InitAddEditTopicForm(int? id, string groupId, int topicId)
        {
            var model = new GroupCourseTopicEdit();
            if (id.HasValue)
            {
                var groupCourseTopic = _groupCourseTopicService.GetById(id.Value);
                EntityMapper<Phoenix.Models.GroupCourseTopic, GroupCourseTopicEdit>.Map(groupCourseTopic, model);
                model.GroupIdValue = model.GroupId;
            }
            else
            {
                var grpId = Helpers.CommonHelper.GetDecryptedId(groupId);
                model.IsAddMode = true;
                model.TopicList = GetUnitMasterSelectListItem(grpId);
                model.GroupId = grpId;
                model.GroupIdValue = grpId;
                model.IsActive = true;

            }

            return PartialView("_InitAddEditTopicForm", model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveTopicData(GroupCourseTopicEdit model)
        {
            if (string.IsNullOrWhiteSpace(model.TopicName) || model.StartDate == null || model.EndDate == null)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            var result = 0;
            var groupCourseTopic = new GroupCourseTopic();
            EntityMapper<GroupCourseTopicEdit, GroupCourseTopic>.Map(model, groupCourseTopic);

            groupCourseTopic.GroupId = model.GroupIdValue;
            if (groupCourseTopic.GroupCourseTopicId > 0)
            {
                groupCourseTopic.UpdatedBy = SessionHelper.CurrentSession.Id;
                result = _groupCourseTopicService.Update(groupCourseTopic);
            }
            else
            {
                groupCourseTopic.CreatedBy = SessionHelper.CurrentSession.Id;
                result = _groupCourseTopicService.Insert(groupCourseTopic);
            }

            if (result < 0)
            {
                return Json(new OperationDetails(false, string.Format(Common.Localization.LocalizationHelper.ItemExistsMessage, "Unit")), JsonRequestBehavior.AllowGet);
            }
            else
            {
                var op = new OperationDetails(result == 1 ? true : false);
                if (op.Success && groupCourseTopic.GroupCourseTopicId == 0) op.Message = ResourceManager.GetString("Files.Unit.UnitAddSuccess");
                return Json(op, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteTopic(string topicId)
        {
            var id = Helpers.CommonHelper.GetDecryptedId(topicId);
            var result = _groupCourseTopicService.Delete(id, SessionHelper.CurrentSession.Id);
            var op = new OperationDetails(result);
            if (op.Success) op.Message = ResourceManager.GetString("Files.Unit.DeleteUnitMessage");
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> GetUnitMasterSelectListItem(long groupId)
        {
            var unitMasterItems = new List<SelectListItem>();
            var unitMasterData = _unitService.GetUnitMasterByGroupId(groupId);

            foreach (var item in unitMasterData)
            {
                var parentUnitMaster = new SelectListItem();
                var unitGroupList = new SelectListGroup();
                if (item.ParentId == 0)
                {
                    parentUnitMaster.Text = item.UnitName;
                    parentUnitMaster.Value = item.UnitId.ToString();

                    var childUnits = unitMasterData.Where(x => x.ParentId == item.UnitId).ToList();

                    if (childUnits.Count == 0)
                    {
                        unitMasterItems.Add(parentUnitMaster);
                    }
                    else
                    {
                        foreach (var child in childUnits)
                        {
                            var childUnitItem = new SelectListItem();

                            unitGroupList.Name = item.UnitName;

                            childUnitItem.Text = child.UnitName;
                            childUnitItem.Value = child.UnitId.ToString();
                            childUnitItem.Group = unitGroupList;
                            unitMasterItems.Add(childUnitItem);
                        }

                    }
                }
            }

            //if (unitMasterData.Count() > 0)
            //  unitMasterItems.Insert(0, new SelectListItem { Text = "Select Unit", Value = "0", Selected = true });

            return unitMasterItems;
        }

        public ActionResult GetTopicDates(long topicId)
        {
            var startDate = DateTime.Today.ToString("dd/MMM/yyyy");
            var endDate = startDate;
            var topic = _unitService.GetUnitMasterDetailsByUnitId(topicId);
            if (topic != null)
            {
                startDate = topic.StartDate.ToString("dd/MMM/yyyy");
                endDate = topic.EndDate.ToString("dd/MMM/yyyy");
            }

            var lessonPlan = _planSchemeDetailService.GetPlanSchemesByUnitId(topicId, 1).Select(r => new SelectListItem { Text = r.PlanSchemeName, Value = r.PlanSchemeDetailId.ToString() }).OrderBy(r => r.Text).ToList();


            return Json(new { st = startDate, en = endDate, lp = lessonPlan }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChatters(string groupId)
        {
            int permission = GetGroupPermission(Helpers.CommonHelper.GetDecryptedId(groupId));

            ViewBag.IsCustomPermission = (permission >= 2);
            ViewBag.GroupPermission = permission;

            return PartialView("_ChatterList");
        }

        public ActionResult GetActivitesNResources(string groupId)
        {
            var fileModulesList = _filesService.GetFileManagementModules().ToList();
            var module = fileModulesList.FirstOrDefault(r => r.ModuleName.Contains("Groups"));
            ViewBag.TopicModuleId = fileModulesList.FirstOrDefault(r => r.ModuleName.Contains("Topics")).ModuleId;

            var grId = Helpers.CommonHelper.GetDecryptedId(groupId);
            var studentUserId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : 0;

            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var model = _filesService.GetGroupFileExplorer(module.ModuleId, grId, 0, studentUserId, true).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            model.ModuleId = module.ModuleId;
            model.SectionId = grId;
            int permission = GetGroupPermission(Helpers.CommonHelper.GetDecryptedId(groupId));

            ViewBag.IsCustomPermission = (permission >= 2);
            ViewBag.GroupPermission = permission;
            return PartialView("_ActivitiesNResources", model);
        }

        public ActionResult GetMemberList(string groupId)
        {
            int permission = GetGroupPermission(Helpers.CommonHelper.GetDecryptedId(groupId));

            ViewBag.IsCustomPermission = (permission >= 2);
            ViewBag.GroupPermission = permission;
            var assignedMember = _schoolGroupService.GetAssignedMembers(Helpers.CommonHelper.GetDecryptedId(groupId)).ToList();
            return PartialView("_MemberList", assignedMember);
        }

        public ActionResult GetFolderDetailByFolderId(long folderId, string groupId)
        {
            var studentUserId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : 0;

            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var model = _filesService.GetGroupFileExplorer(0, 0, folderId, studentUserId, true).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            int permission = GetGroupPermission(Helpers.CommonHelper.GetDecryptedId(groupId));
            ViewBag.IsCustomPermission = (permission >= 2);
            ViewBag.GroupPermission = permission;

            return PartialView("_ActivitiesList", model);
        }

        public ActionResult GetTopicActivitiesList(string moduleId, string sectionId, string groupId)
        {
            var studentUserId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : 0;

            var modId = Helpers.CommonHelper.GetDecryptedId(moduleId);
            var sectId = Helpers.CommonHelper.GetDecryptedId(sectionId);

            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var model = _filesService.GetGroupFileExplorer(modId, sectId, 0, studentUserId, true).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            int permission = GetGroupPermission(Helpers.CommonHelper.GetDecryptedId(groupId));

            ViewBag.IsCustomPermission = (permission >= 2);
            ViewBag.GroupPermission = permission;

            if (modId == 2)
            {
                var fExplorer = new FileExplorer();
                fExplorer.SectionId = Helpers.CommonHelper.GetDecryptedId(groupId);
                fExplorer.Folders.AddRange(model.Folders.Where(r => r.ModuleId == modId).ToList());
                fExplorer.Files.AddRange(model.Files.Where(r => r.ModuleId == modId).ToList());
                fExplorer.GroupUrls.AddRange(model.GroupUrls.Where(r => r.ModuleId == modId).ToList());
                fExplorer.GroupQuizzes.AddRange(model.GroupQuizzes.Where(r => r.ModuleId == modId).ToList());
                fExplorer.Forms.AddRange(model.Forms.Where(r => r.ModuleId == modId).ToList());
                fExplorer.AsyncLessons.AddRange(model.AsyncLessons.Where(r => r.ModuleId == modId).ToList());
                fExplorer.Assignments.AddRange(model.Assignments.Where(r => r.GroupId == fExplorer.SectionId).ToList());
                return PartialView("_ActivitiesList", fExplorer);
            }

            return PartialView("_ActivitiesList", model);
        }

        public ActionResult GetFolderById(int folderId)
        {
            var folder = _folderService.GetById(folderId);
            return PartialView("_NavList", folder);
        }

        public ActionResult AddMemberInGroup(int memberTypeId, int groupId, bool autoSyncMember)
        {
            var model = new GroupMemberViewModel();
            var unAssignedMembers = new List<ListItem>();
            var groupedOptions = new List<SelectListItem>();

            model.GroupId = groupId;

            var userTypes = new List<ListItem>();
            userTypes.Add(new ListItem
            {
                ItemId = "1",
                ItemName = "Students"
            });
            userTypes.Add(new ListItem
            {
                ItemId = "6",
                ItemName = "Other Groups"
            });

            List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
            lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });

            //If auto Sync is false then include Other & Home tutor group in list, else only Class groups
            if (!autoSyncMember)
            {
                lstSelectListGroup.Add(new SelectListGroup
                {
                    Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                    Disabled = false
                });
                lstSelectListGroup.Add(new SelectListGroup
                {
                    Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                    Disabled = false
                });

                groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
                {
                    Value = x.SchoolGroupId.ToString(),
                    Text = x.SchoolGroupName.Length > 40 ? x.SchoolGroupName.Substring(0, 40) : x.SchoolGroupName,
                    Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                    //Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
                }).ToList();


                unAssignedMembers = _schoolGroupService.GetUnassignedMembers((int)SessionHelper.CurrentSession.SchoolId, SessionHelper.CurrentSession.Id, memberTypeId, groupId, string.Empty).ToList();

            }
            else
            {
                groupedOptions = lstSchoolGroup.Where(x => !x.IsBespokeGroup).Select(x => new SelectListItem
                {
                    Value = x.SchoolGroupId.ToString(),
                    Text = x.SchoolGroupName.Length > 40 ? x.SchoolGroupName.Substring(0, 40) : x.SchoolGroupName,
                    Group = lstSelectListGroup[0],
                    //Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
                }).ToList();

                if (memberTypeId >= 3)
                {
                    unAssignedMembers = _schoolGroupService.GetUnassignedMembers((int)SessionHelper.CurrentSession.SchoolId, SessionHelper.CurrentSession.Id, memberTypeId, groupId, string.Empty).ToList();
                }

            }


            model.UnAssignedMemberList = unAssignedMembers;
            ViewBag.MemberTypes = new SelectList(userTypes, "ItemId", "ItemName");
            ViewBag.SchoolGroup = groupedOptions;
            ViewBag.MemberTypeId = memberTypeId;

            return PartialView("_AddMemberInGroup", model);
        }

        [HttpPost]
        public ActionResult GetUnAssignedMemberDdl(int? memberTypeId, int groupId, string groupIds)
        {
            var model = new GroupMemberViewModel();
            var unAssignedMembers = new List<ListItem>();
            unAssignedMembers = _schoolGroupService.GetUnassignedMembers((int)SessionHelper.CurrentSession.SchoolId, SessionHelper.CurrentSession.Id, memberTypeId, groupId, groupIds).ToList();
            model.UnAssignedMemberList = unAssignedMembers;
            model.GroupId = groupId;
            return PartialView("_UnassignedMemberDdl", model);
        }

        public ActionResult GetGroupContactList(int groupId)
        {
            //var studentList = _studentListService.GetStudentsInGroup(groupId);
            var studentList = _schoolGroupService.GetAssignedMembers(groupId).Where(r => r.UserTypeId == 1).ToList();
            return PartialView("_ContactList", studentList);
        }

        public ActionResult GetGroupSettingHtml(int groupId)
        {
            var misGroupList = new List<Phoenix.Models.SchoolGroup>();
            var otherGroupList = new List<Phoenix.Models.SchoolGroup>();

            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                misGroupList = _schoolGroupService.GetSchoolGroups(-1, 0, 0, string.Empty).Result.ToList();
                otherGroupList = _schoolGroupService.GetSchoolGroups(-1, 0, 1, string.Empty).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);

            }
            else
            {
                var childId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                misGroupList = _schoolGroupService.GetStudentGroups(childId, -1, 0, 0, string.Empty).Result.ToList();
                otherGroupList = _schoolGroupService.GetStudentGroups(childId, -1, 0, 1, string.Empty).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);
            }

            ViewBag.GroupId = groupId;
            if (misGroupList.Count() > 0)
            {
                ViewBag.MisGroupList = misGroupList.Select(r => new SelectListItem { Text = (!string.IsNullOrWhiteSpace(r.TeacherGivenName) ? r.TeacherGivenName : r.SchoolGroupDescription.Replace("/", "_")), Value = EncryptDecryptHelper.EncryptUrl(r.SchoolGroupId.ToString()) }).ToList();
            }
            else
            {
                ViewBag.MisGroupList = new List<SelectListItem>() { new SelectListItem { Text = "Select", Value = "0" } };
            }

            if (otherGroupList.Count() > 0)
            {
                ViewBag.OtherGroupList = otherGroupList.Select(r => new SelectListItem { Text = (!string.IsNullOrWhiteSpace(r.TeacherGivenName) ? r.TeacherGivenName : r.SchoolGroupDescription.Replace("/", "_")), Value = EncryptDecryptHelper.EncryptUrl(r.SchoolGroupId.ToString()) }).ToList();
            }
            else
            {
                ViewBag.OtherGroupList = new List<SelectListItem>() { new SelectListItem { Text = "Select", Value = "0" } };
            }

            int permission = 0;
            if (SessionHelper.CurrentSession.IsAdmin || SessionHelper.CurrentSession.UserTypeId == 5)
            {
                permission = 3;
            }
            else
            {
                permission = GetGroupPermission(groupId);
            }

            return PartialView("_GroupSetting");
        }

        private int GetGroupPermission(int groupId)
        {
            int permission = 1;
            if (SessionHelper.CurrentSession.IsAdmin || SessionHelper.CurrentSession.UserTypeId == 5)
            {
                permission = 3;
            }
            else
            {
                if (Session["GroupPermission"] == null)
                {
                    permission = _folderService.GetGroupPermission(Convert.ToInt32(SessionHelper.CurrentSession.Id), groupId);
                }
                else
                {
                    permission = Helpers.CommonHelper.GetDecryptedId(Session["GroupPermission"].ToString());
                }
            }

            return permission;
        }

        public ActionResult AddEditAsyncLessonForm(string id, string sid, string mdid, string fdid, string cid, string gpid)
        {
            var model = new AsyncLessonEdit();
            var sectId = !string.IsNullOrWhiteSpace(sid) ? Helpers.CommonHelper.GetDecryptedId(sid) : Helpers.CommonHelper.GetDecryptedId(gpid);
            var modId = Helpers.CommonHelper.GetDecryptedId(mdid);
            if (!string.IsNullOrWhiteSpace(id))
            {
                var asyncLesson = _asyncLessonService.GetAsyncLessonById(Helpers.CommonHelper.GetDecryptedId(id), true);
                EntityMapper<Phoenix.Models.AsyncLesson, AsyncLessonEdit>.Map(asyncLesson, model);

                if (model.UnitId > 0)
                {
                    model.PlanSchemeList = _planSchemeDetailService.GetPlanSchemesByUnitId(model.UnitId, 1).Select(r => new SelectListItem { Text = r.PlanSchemeName, Value = r.PlanSchemeDetailId.ToString(), Selected = model.PlanSchemeId == r.PlanSchemeDetailId }).ToList();
                }
                var studentList = _schoolGroupService.GetAssignedMembers(sectId).Where(r => r.UserTypeId == 1).ToList();
                if (!string.IsNullOrEmpty(model.Students))
                {
                    model.StudentList = studentList.Select(r => new SelectListItem { Text = r.MemberName, Value = r.MemberId.ToString(), Selected = model.Students.Contains(r.MemberId.ToString()) }).OrderBy(r => r.Text).ToList();
                }
                else
                {
                    model.StudentList = studentList.Select(r => new SelectListItem { Text = r.MemberName, Value = r.MemberId.ToString() }).OrderBy(r => r.Text).ToList();
                }
            }
            else
            {

                model.SectionId = sectId;
                model.ModuleId = modId;
                model.FolderId = Helpers.CommonHelper.GetDecryptedId(fdid);
                model.CourseId = Helpers.CommonHelper.GetDecryptedId(cid);
                model.IsActive = true;
                model.IsAddMode = true;
                model.IsPublish = false;
                var studentList = _schoolGroupService.GetAssignedMembers(sectId).Where(r => r.UserTypeId == 1).ToList();
                model.StudentList = studentList.Select(r => new SelectListItem { Text = r.MemberName, Value = r.MemberId.ToString() }).OrderBy(r => r.Text).ToList();
            }

            var fileTypes = _filesService.GetFileTypes();
            ViewBag.FileExtension = fileTypes.Where(r => r.Extension.IndexOf("svg", StringComparison.OrdinalIgnoreCase) == -1).Select(r => r.Extension).ToList();

            //For group quiz to insert in groupquiz and asynclessonresourcesactivities table: Deepak Singh 1/August/2020
            ViewBag.AsyncModuleId = _filesService.GetFileManagementModules().FirstOrDefault(r => r.ModuleName == "AsyncLesson").ModuleId;

            int permission = 1;
            permission = GetGroupPermission(sectId);
            ViewBag.IsCustomPermission = (permission >= 2);
            ViewBag.GroupPermission = permission;

            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;

            model.UnitList = GetUnitMasterSelectListItem(sectId);

            model.CourseList.AddRange(new SelectList(SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)), "ItemId", "ItemName"));

            return View("AddEditAsyncLessonForm", model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveAsyncLessonData(AsyncLessonEdit model)
        {
            if (string.IsNullOrWhiteSpace(model.LessonTitle) || model.SectionId == 0 || model.ModuleId == 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            long result = 0;
            var asyncLesson = new AsyncLesson();
            EntityMapper<AsyncLessonEdit, AsyncLesson>.Map(model, asyncLesson);

            if (asyncLesson.StartDate == DateTime.MinValue && asyncLesson.EndDate == DateTime.MinValue)
            {
                asyncLesson.StartDate = DateTime.Today;
                asyncLesson.EndDate = DateTime.Today;
            }


            if (asyncLesson.AsyncLessonId > 0)
            {
                asyncLesson.UpdatedBy = SessionHelper.CurrentSession.Id;
                result = _asyncLessonService.UpdateAsyncLesson(asyncLesson);
            }
            else
            {
                asyncLesson.CreatedBy = SessionHelper.CurrentSession.Id;
                result = _asyncLessonService.InsertAsyncLesson(asyncLesson);
            }


            var notificationObj = new OperationDetails(result > 0);

            return Json(new { notificationObj, id = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAsyncLessonId(string asyncLessonId)
        {
            var id = Helpers.CommonHelper.GetDecryptedId(asyncLessonId);
            var result = _asyncLessonService.DeleteAsyncLesson(id, SessionHelper.CurrentSession.Id);
            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAsyncLessonDetail(long id, string grpId)
        {
            var model = new ViewAsyncLessonDetail();

            model.GroupId = Helpers.CommonHelper.GetDecryptedId(grpId);
            model.AsyncLessonId = id;
            model.AsyncLesson = _asyncLessonService.GetAsyncLessonById(id, true);
            model.ResourcesList.AddRange(_asyncLessonService.GetResourceActivity(id, 0, true));
            //model.StudentList.AddRange(_asyncLessonService.GetAsyncLessonStudentMappingByAsyncLessonId(id, 0, true));

            var fileTypeList = _filesService.GetFileTypes();
            model.ImageExt = fileTypeList.Where(r => r.DocumentType == "Image").Select(r => r.Extension).ToList();
            model.VideoExt = fileTypeList.Where(r => r.DocumentType == "Video").Select(r => r.Extension).ToList();
            model.DocumentExt = fileTypeList.Where(r => r.DocumentType == "Document").Select(r => r.Extension).ToList();
            model.AudioExt = fileTypeList.Where(r => r.DocumentType == "Audio").Select(r => r.Extension).ToList();

            int permission = 1;
            permission = GetGroupPermission((int)model.GroupId);
            ViewBag.IsCustomPermission = (permission >= 2);
            ViewBag.GroupPermission = permission;
            return PartialView("_ReviewAndLaunch", model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveResources(AsyncLessonResourcesActivitiesEdit editModel)
        {
            var result = 0;
            string extension = string.Empty;
            string fileName = string.Empty;
            var fileTypeList = _filesService.GetFileTypes().ToList();

            if (editModel.PostedDocuments != null)
            {
                var fileEditList = new List<FileEdit>();
                foreach (var postedFile in editModel.PostedDocuments)
                {
                    var inputFile = postedFile;
                    extension = Path.GetExtension(inputFile.FileName).Replace(".", "");
                    double fileSizeInMb = (postedFile.ContentLength / 1024f) / 1024f;
                    var extId = fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;
                    var fileEditModel = new FileEdit()
                    {
                        FileName = postedFile.FileName,
                        PostedFile = postedFile,
                        ResourceFileTypeId = (short)ResourceFileTypes.SharePoint,
                        FileTypeId = extId,
                        FolderId = 0,
                        SectionId = editModel.SectionId,
                        ModuleId = (int)AsyncLessonModule.Resources,
                        FileSizeInMB = fileSizeInMb,
                        CreatedBy = editModel.UpdatedBy = SessionHelper.CurrentSession.Id
                    };
                    fileEditList.Add(fileEditModel);
                }
                List<FileEdit> fileListWithPath = new List<FileEdit>();
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    fileListWithPath = await azureHelper.UploadFilesListAsPerModuleAsync(FileModulesConstants.MyFiles, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    var syncContext = SynchronizationContext.Current;
                    SynchronizationContext.SetSynchronizationContext(null);
                    fileListWithPath = SharePointHelper.UploadFilesListAsPerModule(ref cx, FileModulesConstants.MyFiles, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList);
                    SynchronizationContext.SetSynchronizationContext(syncContext);
                }
                if (fileListWithPath.Count > 0)
                {
                    var resourcesList = new List<AsyncLessonResourcesActivities>();
                    var stepNo = 1;
                    foreach (var rs in fileListWithPath)
                    {
                        var resources = new AsyncLessonResourcesActivities();
                        resources.AsyncLessonId = editModel.AsyncLessonId;
                        resources.SectionId = 0;
                        resources.ModuleId = (int)AsyncLessonModule.Resources;
                        resources.FolderId = 0;
                        resources.StepNo = stepNo;
                        resources.Name = rs.FileName;
                        resources.Path = rs.FilePath;
                        resources.FileTypeId = rs.FileTypeId;
                        resources.Status = 1;
                        resources.IsActive = true;
                        resources.CreatedBy = SessionHelper.CurrentSession.Id;
                        resources.FileSizeInMB = Convert.ToDecimal(rs.FileSizeInMB);
                        resources.PhysicalPath = rs.PhysicalFilePath;
                        resourcesList.Add(resources);
                        stepNo++;
                    }

                    result = _asyncLessonService.InsertResourceActivity(resourcesList);
                }
            }

            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetResourcesActivitiesAsyncLesson(long asyncLessonId, string grpId)
        {
            var model = _asyncLessonService.GetResourceActivity(asyncLessonId, SessionHelper.CurrentSession.Id, true);
            int permission = 1;
            permission = GetGroupPermission(Helpers.CommonHelper.GetDecryptedId(grpId));
            ViewBag.IsCustomPermission = (permission >= 2);
            ViewBag.GroupPermission = permission;
            return PartialView("_AsyncLessonResources", model);
        }

        public ActionResult UpdateStatusAsyncResourcesActivity(long id, int status, string type)
        {
            //Status : 0 => not viewable &locked, 1 => viewable & not locked, 2 => not viewable &not locked, 3 => viewable & locked
            int newStatus = 0;
            if (type == "viewable")
            {
                if (status == 0)
                    newStatus = 3;
                else if (status == 1)
                    newStatus = 2;
                else if (status == 3)
                    newStatus = 0;
                else
                    newStatus = 1;
            }
            else
            {
                if (status == 0)
                    newStatus = 2;
                else if (status == 1)
                    newStatus = 3;
                else if (status == 3)
                    newStatus = 1;
                else
                    newStatus = 0;
            }

            var model = new UpdateAsyncLessonResourcesActivity();
            model.ResourceActivityId = id;
            model.Status = newStatus;
            model.updatedBy = SessionHelper.CurrentSession.Id;
            model.IsStatusUpdate = true;

            var result = _asyncLessonService.UpdateResourceActivityStatus(model);
            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateStepOrderAsyncResourcesActivity(string ids)
        {
            var resourcesIds = new JavaScriptSerializer().Deserialize<int[]>(ids);
            var resourcesList = new List<AsyncLessonResourcesActivities>();
            int i = 1;
            foreach (int item in resourcesIds)
            {
                var rs = new AsyncLessonResourcesActivities();
                rs.ResourceActivityId = item;
                rs.StepNo = i;
                resourcesList.Add(rs);
                i++;
            }
            var model = new UpdateAsyncLessonResourcesActivity();
            model.updatedBy = SessionHelper.CurrentSession.Id;
            model.IsStatusUpdate = false;
            model.ResourcesActivitiesList = resourcesList;
            var result = _asyncLessonService.UpdateResourceActivityStatus(model);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAsyncResourcesActivity(long id)
        {
            var result = _asyncLessonService.DeleteResourceActivity(id, SessionHelper.CurrentSession.Id);
            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewAsyncLessonDetail(string id, string gpid)
        {
            var model = new ViewAsyncLessonDetail();
            model.GroupId = Helpers.CommonHelper.GetDecryptedId(gpid);
            model.AsyncLessonId = Helpers.CommonHelper.GetDecryptedId(id);

            model.AsyncLesson = _asyncLessonService.GetAsyncLessonById(model.AsyncLessonId, true);
            var currentUserId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            model.ResourcesList.AddRange(_asyncLessonService.GetResourceActivity(model.AsyncLessonId, currentUserId, true));
            //model.StudentList.AddRange(_asyncLessonService.GetAsyncLessonStudentMappingByAsyncLessonId(id, 0, true));

            var fileTypeList = _filesService.GetFileTypes();
            model.ImageExt = fileTypeList.Where(r => r.DocumentType == "Image").Select(r => r.Extension).ToList();
            model.VideoExt = fileTypeList.Where(r => r.DocumentType == "Video").Select(r => r.Extension).ToList();
            model.DocumentExt = fileTypeList.Where(r => r.DocumentType == "Document").Select(r => r.Extension).ToList();
            model.AudioExt = fileTypeList.Where(r => r.DocumentType == "Audio").Select(r => r.Extension).ToList();

            int permission = 1;
            permission = GetGroupPermission((int)model.GroupId);
            ViewBag.IsCustomPermission = (permission >= 2);
            ViewBag.GroupPermission = permission;
            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;
            return View(model);
        }

        public ActionResult TeacherLessonDetailView(string id, string gpid)
        {
            var model = new ViewAsyncLessonDetail();
            model.AsyncLessonId = Helpers.CommonHelper.GetDecryptedId(id);

            model = _asyncLessonService.GetAsyncLessons(model.AsyncLessonId, null, null, null, true, 1, 1);

            model.GroupId = Helpers.CommonHelper.GetDecryptedId(gpid);
            var fileTypeList = _filesService.GetFileTypes();
            model.ImageExt = fileTypeList.Where(r => r.DocumentType == "Image").Select(r => r.Extension).ToList();
            model.VideoExt = fileTypeList.Where(r => r.DocumentType == "Video").Select(r => r.Extension).ToList();
            model.DocumentExt = fileTypeList.Where(r => r.DocumentType == "Document").Select(r => r.Extension).ToList();
            model.AudioExt = fileTypeList.Where(r => r.DocumentType == "Audio").Select(r => r.Extension).ToList();

            int permission = 1;
            permission = GetGroupPermission((int)model.GroupId);
            ViewBag.IsCustomPermission = (permission >= 2);
            ViewBag.GroupPermission = permission;
            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveCommentData(AsyncLessonComments model)
        {
            if (string.IsNullOrWhiteSpace(model.Comment))
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            var result = 0;
            if (model.AsyncLessonCommentId == 0)
            {
                model.UserId = SessionHelper.CurrentSession.Id;
                model.IsActive = true;
                model.CreatedBy = SessionHelper.CurrentSession.Id;
                result = _asyncLessonService.InsertComment(model);
            }
            else
            {
                model.UserId = SessionHelper.CurrentSession.Id;
                model.IsActive = true;
                model.UpdatedBy = SessionHelper.CurrentSession.Id;
                result = _asyncLessonService.UpdateComment(model);
            }

            return Json(new OperationDetails(result > 0) { Identifier = model.AsyncLessonCommentId.ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadStudentComment(long asLsId)
        {
            var asyncLessonComments = _asyncLessonService.GetAsyncLessonComments(null, null, SessionHelper.CurrentSession.Id, asLsId);
            var dataList = new object();
            var CustomPermission = true;
            string editBtn = CustomPermission ? "<a class='mr-3' onclick='asyncLesson.editDoubtPopup($(this),{0},{1});' data-toggle='tooltip' data-placement='bottom' title='" + Phoenix.Common.Localization.ResourceManager.GetString("SchoolGroup.AsyncLesson.EditDoubt") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a>" : "";
            string deleteBtn = CustomPermission ? " <a class='mr-3' onclick='asyncLesson.deleteDoubtData($(this),{0},{1});' data-toggle='tooltip' data-placement='bottom' title='" + Phoenix.Common.Localization.ResourceManager.GetString("SchoolGroup.AsyncLesson.DeleteDoubt") + "' ><img src='/Content/VLE/img/svg/tbl-delete.svg'></a>" : "";
            dataList = new
            {
                aaData = (from item in asyncLessonComments.Where(r => r.ParentCommentId == 0).ToList()
                          select new
                          {
                              Actions = item.IsResolved ? "<div class='tbl-actions'></div>" : "<div class='tbl-actions'>" + string.Format(editBtn, item.AsyncLessonCommentId, item.ResourceActiivtyId) + string.Format(deleteBtn, item.AsyncLessonCommentId, item.ResourceActiivtyId) + "</div>",
                              item.Name,
                              Comment = !string.IsNullOrWhiteSpace(item.Comment) && item.Comment.Length > 30 ? $"{item.Comment.Substring(0, 30)}... <a href='javascript:void(0)' data-toggle='dropdown' aria-expanded='true' style='font-size:10px;'>Read more</a>{GetMoreDescription(item.Comment)}" : item.Comment,
                              Reply = GetReplyComment(item.AsyncLessonCommentId, asyncLessonComments.ToList())
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        private string GetMoreDescription(string description)
        {
            string dropHtml = @"<div class='resource-info dropdown-menu mt-n5 z-depth-1 border-light rounded-2x p-0 keepopen'>
                    <div class='column-padding py-3'>
	                    <div class='content mCustomScrollbar _mCS_6 mCS-autoHide' style='position: relative; overflow: visible;'><div id='mCSB_6' class='mCustomScrollBox mCS-light mCSB_vertical mCSB_outside' style='max-height: none;' tabindex='0'><div id='mCSB_6_container' class='mCSB_container mCS_y_hidden mCS_no_scrollbar_y' style='position:relative; top:0; left:0;' dir='ltr'>
		                    <p class='sub-heading text-semibold text-primary'>Description</p>
		                    <p class='wordbreak'>{desc}</p>
	                    </div></div><div id='mCSB_6_scrollbar_vertical' class='mCSB_scrollTools mCSB_6_scrollbar mCS-light mCSB_scrollTools_vertical mCSB_scrollTools_onDrag_expand' style='display: none;'><div class='mCSB_draggerContainer'><div id='mCSB_6_dragger_vertical' class='mCSB_dragger' style='position: absolute; min-height: 30px; top: 0px; height: 0px;'><div class='mCSB_dragger_bar' style='line-height: 30px;'></div></div><div class='mCSB_draggerRail'></div></div></div></div>
	                    <div class='d-flex justify-content-end mt-4'>
		                    <button type='button' class='btn btn-outline-primary btn-sm m-0 closeDropdown'>Close</button>
	                    </div>
                    </div>
                    </div>";
            return dropHtml.Replace("{desc}", description);
        }

        private string GetReplyComment(long id, List<AsyncLessonComments> model)
        {
            var replyComment = model.Where(r => r.ParentCommentId == id).FirstOrDefault();
            return replyComment != null ? !string.IsNullOrWhiteSpace(replyComment.Comment) && replyComment.Comment.Length > 30 ? $"{replyComment.Comment.Substring(0, 30)}... <a href='javascript:void(0)' data-toggle='dropdown' aria-expanded='true' style='font-size:10px;'>Read more</a>{GetMoreDescription(replyComment.Comment)}" : replyComment.Comment : " ";

        }

        public ActionResult EditDoubt(long id, long rsId, long asLsId)
        {
            var result = _asyncLessonService.GetAsyncLessonComments(id, rsId, SessionHelper.CurrentSession.Id, asLsId).FirstOrDefault();
            return Json(new { data = result.Comment }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteDoubt(long id)
        {
            var result = _asyncLessonService.DeleteComment(id, SessionHelper.CurrentSession.Id);
            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStudentDoubtList(long studentId, long asLsId)
        {
            var model = _asyncLessonService.GetAsyncLessonComments(null, null, studentId, asLsId);
            return PartialView("_StudentDoubtList", model);
        }

        [HttpPost]
        public async Task<ActionResult> SaveCloudFiles(string fileIds, long asyncLessonId, string parentFolderId, short resourceFileTypeId)
        {
            var selectedFiles = JsonConvert.DeserializeObject<List<CloudFileListView>>(fileIds);
            var _fileTypeList = _filesService.GetFileTypes();

            IList<AsyncLessonResourcesActivities> resourcesActivitiesList = new List<AsyncLessonResourcesActivities>();
            foreach (var item in selectedFiles)
            {
                var resourcesActivities = new AsyncLessonResourcesActivities();
                if (string.IsNullOrEmpty(item.FileExtension))
                    item.FileExtension = Helpers.CommonHelper.GetFileExtensionByMimeType(item.MimeType);
                else if (item.FileExtension.Equals("onenote", StringComparison.OrdinalIgnoreCase))
                    item.FileExtension = "one";
                resourcesActivities.AsyncLessonId = asyncLessonId;
                resourcesActivities.GoogleDriveFileId = item.Id;
                resourcesActivities.CreatedBy = SessionHelper.CurrentSession.Id;
                resourcesActivities.ModuleId = (int)AsyncLessonModule.Resources;
                resourcesActivities.SectionId = 0;
                resourcesActivities.FolderId = Helpers.CommonHelper.GetDecryptedId(parentFolderId);
                resourcesActivities.Name = item.Name;
                resourcesActivities.Path = item.WebViewLink;
                resourcesActivities.FileTypeId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(item.FileExtension.ToLower())).TypeId;
                resourcesActivities.ResourceFileTypeId = item.ResourceFileTypeId != 0 ? item.ResourceFileTypeId : resourceFileTypeId;
                resourcesActivities.IsActive = true;
                resourcesActivities.Status = 1;
                resourcesActivities.FileSizeInMB = item.Size.HasValue ? Convert.ToDecimal(item.Size.Value) : 0;
                resourcesActivitiesList.Add(resourcesActivities);
            }

            var result = _asyncLessonService.InsertResourceActivity(resourcesActivitiesList);


            #region Resource File Share With users
            if (result > 0)
            {
                var studentsList = _asyncLessonService.GetAsyncLessonStudentMappingByAsyncLessonId(asyncLessonId, null, true);
                if (studentsList != null && studentsList.Count() > 0)
                {
                    foreach (var rs in resourcesActivitiesList)
                    {
                        if (rs.ResourceFileTypeId == (short)ResourceFileTypes.OneDrive)
                            await CloudFilesHelper.ShareOneDriveFile(rs.GoogleDriveFileId, studentsList.Select(e => e.MicorosoftMailAddress), "read");
                        else if (rs.ResourceFileTypeId == (short)ResourceFileTypes.OneNote)
                        {
                            foreach (var std in studentsList)
                                await CloudFilesHelper.AssignNotebookToStudent(std.MicorosoftMailAddress, rs.GoogleDriveFileId);
                        }
                        else if (rs.ResourceFileTypeId == (short)ResourceFileTypes.GoogleDrive)
                        {
                            CancellationToken cancellationToken = new CancellationToken();
                            var authResult = new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).
                                AuthorizeAsync(cancellationToken).GetAwaiter().GetResult();
                            CloudFilesHelper.AddGoogleDriveUserPermission(authResult, string.Join(",", studentsList.Select(e => e.GoogleMailAddress)), rs.GoogleDriveFileId, "reader");
                        }
                    }
                }


            }
            #endregion
            var op = new OperationDetails(result > 0);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGroupCourseTopicsByGroupId(long groupId)
        {
            var model = _groupCourseTopicService.GetGroupCourseTopicsByGroupId(groupId, true);
            return PartialView("_SortUnits", model);
        }

        public ActionResult UpdateUnitsStepOrder(string ids)
        {
            var unitIds = new JavaScriptSerializer().Deserialize<int[]>(ids);
            var unitList = new List<GroupCourseTopic>();
            int i = 1;
            foreach (int item in unitIds)
            {
                var gct = new GroupCourseTopic();
                gct.GroupCourseTopicId = item;
                gct.SortOrder = i;
                gct.UpdatedBy = SessionHelper.CurrentSession.Id;
                unitList.Add(gct);
                i++;
            }

            var result = _groupCourseTopicService.UpdateUnitSortOrder(unitList);
            return Json(result > 0, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddGroupPrimaryTeacher(GroupPrimaryTeacher model)
        {
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            var result = _schoolGroupService.InsertGroupPrimaryTeacher(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveGroupPrimaryTeacher(long id)
        {
            var result = _schoolGroupService.DeleteGroupPrimaryTeacher(id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateSchoolGroupsToHide(SchoolGroupEdit model)
        {
            var result = _schoolGroupService.UpdateSchoolGroupsToHideClassGroups(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Live Lessons 
        public ActionResult LiveSessions()
        {
            var schoolGroups = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            ViewBag.SchoolGroups = schoolGroups.Select(i => new Phoenix.Common.Models.ListItem
            {
                ItemId = i.SchoolGroupId.ToString(),
                ItemName = !string.IsNullOrEmpty(i.TeacherGivenName) ? (i.TeacherGivenName.Length > 50 ? i.TeacherGivenName.Substring(0, 50) + "..." : i.TeacherGivenName) : (i.SchoolGroupDescription.Length > 50 ? i.SchoolGroupDescription.Substring(0, 50) + "..." : i.SchoolGroupDescription)
            });
            return View();
        }

        public ActionResult GetAllLiveSessions(string searchText, string type, string groupId, int pageIndex = 1)
        {
            var model = new Pagination<ZoomMeetingView>();
            var liveSessions = _schoolGroupService.GetAllLiveSessions(SessionHelper.CurrentSession.Id, pageIndex, 15, searchText, type, groupId);
            if (liveSessions.Any())
            {
                foreach (var session in liveSessions)
                    session.MeetingDateTime = Convert.ToDateTime(session.FormattedMeetingDateTime).ConvertUtcToLocalTime().Value;
                liveSessions = liveSessions.OrderBy(e => e.MeetingDateTime);
                model = new Pagination<ZoomMeetingView>(pageIndex, 15, liveSessions.ToList(), liveSessions.FirstOrDefault().TotalCount);
            }
            model.LoadPageRecordsUrl = "/SchoolStructure/SchoolGroups/GetAllLiveSessions?pageIndex={0}&searchText=" + searchText;
            model.SearchString = searchText;
            model.PageIndex = pageIndex;
            return PartialView("_LiveSynchronousSessions", model);
        }

        public ActionResult InitMeetingRegistrantForm(string meetingId)
        {
            ViewBag.MeetingId = meetingId;
            return PartialView("_FormMeetingRegistation");
        }

        [HttpPost]
        public ActionResult GenerateZoomMeetingURL(string meetingId)
        {
            var op = new OperationDetails();
            var model = new ZoomMeetingView();
            model.FirstName = $"{SessionHelper.CurrentSession.FirstName}";
            model.LastName = SessionHelper.CurrentSession.LastName;
            model.ZoomMeetingId = meetingId;
            model.ZoomEmail = SessionHelper.CurrentSession.Email;
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            model = _schoolGroupService.GenerateZoomMeetingJoinURL(model);
            op = new OperationDetails(!string.IsNullOrEmpty(model.MeetingURL));
            op.HeaderText = model.MeetingURL;
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUserLiveSessionInfo(string meetingUrl)
        {
            ZoomMeetingJoinView model = new ZoomMeetingJoinView();
            model.FirstName = SessionHelper.CurrentSession.FirstName;
            model.LastName = SessionHelper.CurrentSession.LastName;
            model.Email = SessionHelper.CurrentSession.Email;
            model.MeetingURL = meetingUrl;
            var response = _schoolGroupService.GetUserLiveSessionInfo(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}