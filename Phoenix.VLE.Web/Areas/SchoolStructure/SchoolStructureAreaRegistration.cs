﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SchoolStructure
{
    public class SchoolStructureAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SchoolStructure";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SchoolStructure_default",
                "SchoolStructure/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}