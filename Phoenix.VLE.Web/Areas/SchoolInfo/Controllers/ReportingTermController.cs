﻿using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Areas.Course.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Phoenix.Common.Helpers.Extensions;
using DevExpress.XtraCharts;
using Phoenix.VLE.Web.Models;
using Phoenix.Common.Localization;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class ReportingTermController : BaseController
    {
        private IDivisionService _DivisionService;
        private ISIMSCommonService _SIMSCommonService;
        private IAttendanceSettingService _attendanceSettingService;
        public ReportingTermController(IDivisionService DivisionService, ISIMSCommonService simsCommonService, IAttendanceSettingService attendanceSettingService)
        {
            _DivisionService = DivisionService;
            _SIMSCommonService = simsCommonService;
            _attendanceSettingService = attendanceSettingService;
        }
        // GET: SchoolInfo/ReportingTerm
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult InitAddEditReportingTermForm(int? id)
        {
            var model = new ReportingTermModel();
            ViewBag.SchoolDivisionList = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolDivision, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            if (id.HasValue)
            {
                var result = _DivisionService.GetReportingTermDetail(Convert.ToInt64(id), SessionHelper.CurrentSession.SchoolId).ToList();
                if (result.Any())
                {
                    model = result.FirstOrDefault();
                }
            }
            return PartialView("_AddEditReportingTerm", model);
        }
        public PartialViewResult GetReportingTermDetail()
        {
            IEnumerable<ReportingTermModel> reportingTermModel = new List<ReportingTermModel>();
            var result = _DivisionService.GetReportingTermDetail(0, SessionHelper.CurrentSession.SchoolId);
            reportingTermModel = result ?? result;
            return PartialView("_ReportingTermDetail", reportingTermModel);
        }
        public ActionResult SaveReportingTermData(ReportingTermModel reportingTermModel)
        {
            reportingTermModel.SchoolId = SessionHelper.CurrentSession.SchoolId;
            reportingTermModel.UserId = SessionHelper.CurrentSession.Id;
            reportingTermModel.DATAMODE = reportingTermModel.ReportingTermId > 0 ? "EDIT" : "ADD";
            bool result = false;
            result = _DivisionService.SaveReportingTermDetail(reportingTermModel);
            OperationDetails op = new OperationDetails(result);
            if (result)
            {
                op.Message = reportingTermModel.ReportingTermId > 0 ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.AddSuccessMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteReportingTermDetail(long ReportingTermId)
        {
            ReportingTermModel reportingTermModel = new ReportingTermModel();
            reportingTermModel.ReportingTermId = ReportingTermId;
            reportingTermModel.DATAMODE = "DELETE";
            bool result = false;
            result = _DivisionService.SaveReportingTermDetail(reportingTermModel);
            OperationDetails op = new OperationDetails(result);
            if (result)
            {
                op.Message = LocalizationHelper.DeleteSuccessMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LockUnlockTerm(long ReportingTermId, bool IsLock)
        {
            var result = _DivisionService.LockUnlockTerm(ReportingTermId, IsLock);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReportingSubTerm()
        {
            if (Session["SessionReportingTermId"] != null)
            {
                return View();
            }
            else
                return RedirectToAction("Index");

        }
        public ActionResult GetReportingSubTermDetail()
        {
            IEnumerable<ReportingSubTermModel> reportingTermModel = new List<ReportingSubTermModel>();
            if (Session["SessionReportingTermId"] != null)
            {
                reportingTermModel = _DivisionService.GetReportingSubTermDetail(Session["SessionReportingTermId"].ToInteger(), 0);
                return PartialView("_ReportingSubTermDetail", reportingTermModel);
            }
            else
                return RedirectToAction("Index");
        }
        public ActionResult InitAddEditReportingSubTermForm(int? id)
        {
            var model = new ReportingSubTermModel();
            if (id.HasValue)
            {
                var result = _DivisionService.GetReportingSubTermDetail(0, Convert.ToInt64(id)).ToList();
                if (result.Any())
                {
                    model = result.FirstOrDefault();
                }
            }
            if (Session["SessionReportingTermId"] != null)
                model.ReportingTermId = Session["SessionReportingTermId"].ToLong();

            return PartialView("_AddEditReportingSubTerm", model);
        }
        public ActionResult SaveReportingSubTermData(ReportingSubTermModel reportingSubTermModel)
        {
            reportingSubTermModel.UserId = SessionHelper.CurrentSession.Id;
            reportingSubTermModel.DATAMODE = reportingSubTermModel.ReportingSubTermId > 0 ? "EDIT" : "ADD";
            bool result = false;
            result = _DivisionService.SaveReportingSubTermDetail(reportingSubTermModel);
            OperationDetails op = new OperationDetails(result);
            if (result)
            {
                op.Message = reportingSubTermModel.ReportingSubTermId > 0 ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.AddSuccessMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteReportingSubTermDetail(long ReportingSubTermId)
        {
            ReportingSubTermModel reportingTermModel = new ReportingSubTermModel();
            reportingTermModel.ReportingSubTermId = ReportingSubTermId;
            reportingTermModel.DATAMODE = "DELETE";
            bool result = false;
            result = _DivisionService.SaveReportingSubTermDetail(reportingTermModel);
            OperationDetails op = new OperationDetails(result);
            if (result)
            {
                op.Message = LocalizationHelper.DeleteSuccessMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LockUnlockSubTerm(long ReportingSubTermId, bool IsLock)
        {
            var result = _DivisionService.LockUnlockSubTerm(ReportingSubTermId, IsLock);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
    }
}