﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class SchoolBannerController : Controller
    {
        private ISchoolService _schoolService;
        private List<VLEFileType> _fileTypeList;
        private readonly IFileService _fileService;
        private readonly ISchoolBannerService _schoolBannerService;
        private readonly IUserPermissionService _userPermissionService;
        public SchoolBannerController(ISchoolService schoolService, IFileService fileService, ISchoolBannerService schoolBannerService, IUserPermissionService userPermissionService)
        {
            _schoolService = schoolService;
            _fileService = fileService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
            _schoolBannerService = schoolBannerService;
            _userPermissionService = userPermissionService;
        }
        // GET: SchoolInfo/SchoolBanner
        public ActionResult Index()
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_SchoolBanner.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            return View();
        }
        public ActionResult AddEditSchoolBanner(Int64? bannerId)
        {
            Session["BannerFiles"] = null;
            var model = new SchoolBannerEdit();
            List<SchoolInformation> schoolList = _schoolService.GetSchoolList().ToList();
            List<SelectListItem> lstSchoolList = new SelectList(schoolList, "SchoolId", "SchoolName").ToList();
            ViewBag.SchoolList = lstSchoolList;
            ViewBag.UserTypes = new SelectList(SelectListHelper.GetSelectListData(ListItems.UserTypes), "ItemId", "ItemName");
            ViewBag.FileExtension = _fileTypeList.Where(m => m.DocumentType == "Image").Select(r => r.Extension).ToList();
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_SchoolBanner.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            if (SessionHelper.CurrentSession.UserTypeId != 5)
            {                
                model.SchoolIds = new List<string>();
                model.SchoolIds.Add(SessionHelper.CurrentSession.SchoolId.ToString());
            }

            if (bannerId.HasValue)
            {
                var schoolBanner = _schoolBannerService.GetSchoolBannerDetails(bannerId.Value, SessionHelper.CurrentSession.Id);
                EntityMapper<SchoolBanner, SchoolBannerEdit>.Map(schoolBanner, model);
                model.SchoolId = _schoolBannerService.GetBannerSchoolIds(bannerId.Value).ToList();
                model.UserTypeId = schoolBanner.UserTypes.Split(',').ToList();
                model.IsAddMode = false;
            }
            else
            {
                model.IsAddMode = true;
                model.CreatedBy = SessionHelper.CurrentSession.Id;
                model.DisplayOrder = _schoolBannerService.GetTopOrderForDisplayBanner(SessionHelper.CurrentSession.Id) + 1;
            }
            return PartialView("_AddEditSchoolBanner", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSchoolBannerDetails(SchoolBannerEdit model, HttpPostedFileBase bannerFile)
        {
            model.ParseJsonToXml("SchoolBannerXml");
            if (bannerFile != null)
            {
                TaskFile tf = UploadBannerFiles(bannerFile);
                model.UploadedImageName = tf.FileName;
                model.ActualImageName = tf.UploadedFileName;
            }
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            var result = _schoolBannerService.UpdateBannerData(model);
            Session["BannerFiles"] = null;
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public TaskFile UploadBannerFiles(HttpPostedFileBase bannerFile)
        {

            TaskFile fileDetails = new TaskFile();
            string relativePath = string.Empty;

            try
            {
                HttpPostedFileBase file = bannerFile;
                string fname = file.FileName;
                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    fname = file.FileName;
                }
                var extension = System.IO.Path.GetExtension(file.FileName);
                string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.BannerFilesDir;
                string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.BannerFilesDir + "/School_" + (int)SessionHelper.CurrentSession.SchoolId;
                Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                fname = Guid.NewGuid() + "_" + fname;
                relativePath = Constants.BannerFilesDir + "/School_" + (int)SessionHelper.CurrentSession.SchoolId + "/" + fname;
                fname = Path.Combine(filePath, fname);
                file.SaveAs(fname);
                fileDetails.FileName = file.FileName;
                fileDetails.FileExtension = extension.Replace(".", "");
                fileDetails.UploadedFileName = relativePath;
            }
            catch (Exception ex)
            {
            }
            return fileDetails;
        }

        public ActionResult LoadSchoolBanners()
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            bool IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_SchoolBanner.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var bannerList = _schoolBannerService.GetSchoolBanners(SessionHelper.CurrentSession.Id, (int)SessionHelper.CurrentSession.SchoolId);
            var actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom'  onclick='schoolBanner.editSchoolBanner($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a>" +
                                             "<a class='mr-3' onclick='schoolBanner.deleteSchoolBanner($(this), {0});' data-toggle='tooltip' data-placement='bottom' title='" + ResourceManager.GetString("Shared.Labels.DeleteRecord") + "'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a></div>";
            var dataList = new object();
           
            dataList = new
            {
                aaData = (from item in bannerList
                          select new
                          {
                              Actions = IsCustomPermission==true?string.Format(actionButtonsHtmlTemplate, item.SchoolBannerId):"",
                              item.BannerName,
                              item.ClickURL,
                              item.DisplayOrder,
                              UploadedImage = !string.IsNullOrEmpty(item.ActualImageName) ? "<img src = '" + PhoenixConfiguration.Instance.ReadFilePath + item.ActualImageName + "' class='img-thumbnail' style = 'height:40px;width:40px' />" : "",
                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteSchoolBanner(int id)
        {
            var schoolBanner = _schoolBannerService.GetSchoolBannerDetails(id, SessionHelper.CurrentSession.Id);
            schoolBanner.DeletedBy = SessionHelper.CurrentSession.Id;
            var result = _schoolBannerService.DeleteBannerData(schoolBanner);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }


}