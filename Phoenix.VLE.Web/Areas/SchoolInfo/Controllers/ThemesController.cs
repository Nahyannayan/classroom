﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    [Authorize]
    public class ThemesController : BaseController
    {
        private readonly IThemesService _themesServiceService;
        public ThemesController(IThemesService ThemesService)
        {
            _themesServiceService = ThemesService;
        }

        // GET: SchoolInfo/Themes
        public ActionResult Index()
        {
            ViewBag.CurriculumList = SelectListHelper.GetSelectListData(ListItems.Curriculum);
            return View();
        }
        public ActionResult LoadThemeSettingGrid(int? curriculumId)
        {
            int schoolId = (int)SessionHelper.CurrentSession.SchoolId;
            var SchoolThemes = _themesServiceService.GetAllSchoolThemes(schoolId, curriculumId,LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
            var dataList = new object();
            string noThemeResourceText = ResourceManager.GetString("School.Theme.ThemeNotApplied");

            var actionButtonsHtmlTemplate = "<div class='tbl-actions'> <a class='mr-3' data-toggle='tooltip' onclick='schoolTheme.editThemePopup($(this),{0})' data-placement='bottom' title='" + Common.Localization.ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a></div>";
            dataList = new
            {
                aaData = (from item in SchoolThemes
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.SchoolGradeId),
                              item.SchoolName,
                              ThemeName = string.IsNullOrEmpty(item.ThemeName) ? noThemeResourceText : item.ThemeName,
                              item.ThemeCssFileName,
                              item.ThemePreviewImage,
                              item.SchoolGradeName
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult InsertUpdateSchoolTheme(SchoolThemeEdit themes)
        {
            if (!CurrentPagePermission.CanEdit)
            {
                var op = new OperationDetails();
                op.Success = false;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            themes.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            var result = _themesServiceService.SchoolThemeCUD(themes);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult InitAssignThemeForm(int? id)
        {
            var model = new SchoolThemeEdit();
            if (id.HasValue)
            {
                model = _themesServiceService.GetSchoolThemeById(id.Value);
            }
            else
            {
                model.IsAddMode = true;
                ViewBag.SchoolGrades = SelectListHelper.GetSelectListData(ListItems.SchoolGrade, string.Format("{0},{1}", SessionHelper.CurrentSession.SchoolId, "NULL"));
            }
            model.SchoolGradeId = id.HasValue ? id.Value : 0;
            var list = _themesServiceService.GetAllSchoolThemes(null, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
            ViewBag.SchoolThemes = new SelectList(list, "ThemeId", "ThemeName").ToList();
            return PartialView("_AssignThemeForm", model);
        }
        public ActionResult GetPreviewImage(int themeId)
        {
            var list = _themesServiceService.GetAllSchoolThemes().Where(x => x.ThemeId == themeId).ToList();
            string previewImage = list.FirstOrDefault().ThemePreviewImage;
            return Json(new { PreviewURL = previewImage, DownloadURL = PhoenixConfiguration.Instance.ReadFilePath.Replace("\\", "/") + previewImage }, JsonRequestBehavior.AllowGet);
        }
    }
}