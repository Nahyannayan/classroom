﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Models;
using Phoenix.VLE.Web.ViewModels;
using Phoenix.VLE.Web.EditModels;
using Phoenix.Common.Localization;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    [Authorize]
    public class DeploymentNotificationController : BaseController
    {
        private readonly ISchoolNotificationService _schoolNotificationService;
        public DeploymentNotificationController(ISchoolNotificationService schoolNotificationService)
        {
            _schoolNotificationService = schoolNotificationService;
        }
        public ActionResult Index()
        {

            List<DeploymentNotification> notificationList = new List<DeploymentNotification>();
            notificationList = _schoolNotificationService.GetSchoolDeploymentNotificationList();
            return View(notificationList);
        }

        public ActionResult InitDeploymentNotificationForm(int? id)
        {
            var model = new DeploymentNotification();
            var editModel = new DeploymentNotificationEdit();
            if (id.HasValue)
            {
                model = _schoolNotificationService.GetSchoolDeploymentNotificationList().FirstOrDefault(e => e.DeploymentNotificationId == id.Value);
            }
            EntityMapper<DeploymentNotification, DeploymentNotificationEdit>.Map(model, editModel);
            return PartialView("_AddEditDeploymentNotification", editModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveDeploymentNotificationData(DeploymentNotificationEdit editModel)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            bool result = _schoolNotificationService.SaveDeploymentNotificationData(editModel);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDeploymentNotifications()
        {
            return PartialView("_DeploymentNotificationGrid", _schoolNotificationService.GetSchoolDeploymentNotificationList());
        }
    }
}