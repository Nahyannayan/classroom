﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.EditModels;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Helpers;
using Phoenix.Common;
using System.IO;
using Phoenix.Common.Enums;
using Phoenix.VLE.Web.ViewModels;
using File = Phoenix.Models.File;
using System.Web.Script.Serialization;
using System.Data;
using Phoenix.Common.Localization;
using Phoenix.Common.ViewModels;
using System.Text;
using Phoenix.Models.Entities;
using Phoenix.Common.Models;
using Phoenix.VLE.Web.Helpers;
using System.Threading;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class ExemplarWallController : BaseController
    {
        // GET: SchoolInfo/Exemplar

        private readonly IExemplarWallServices _exemplarWallService;
        private readonly IFileService _fileService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly IBlogService _blogService;
        private readonly ICourseService _courseService;

        public ExemplarWallController(IBlogService blogService, IExemplarWallServices exemplarWallService, IFileService fileService, IUserPermissionService userPermissionService, ISchoolGroupService schoolGroupService,
            ICourseService courseService)
        {
            _fileService = fileService;
            _userPermissionService = userPermissionService;
            _exemplarWallService = exemplarWallService;
            _schoolGroupService = schoolGroupService;
            _blogService = blogService;
            _courseService = courseService;
        }

        [HttpGet]
        public ActionResult index()
        {
            //New Code for Exemplar /*Deependra*/
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            var schoolLevelList = _exemplarWallService.GetSchoolLevelBySchoolId(SchoolId); ;

            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = ResourceManager.GetString("Exemplar.Labels.leveldropdownmsg"), Value = "0" });
            foreach (var item in schoolLevelList)
                li.Add(new SelectListItem { Text = (item.SchoolLevelName.Length > 20 ? item.SchoolLevelName.Substring(0, 20) + "..." : item.SchoolLevelName)  , Value = item.SchoolLevelId.ToString() });

            ViewData["SchoolLevel"] = li;

            var schoolDepartmentList = _exemplarWallService.GetSchoolDepartmentList(SchoolId); ;

            li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = ResourceManager.GetString("Exemplar.Labels.departmentdropdownmsg"), Value = "0" });
            foreach (var item in schoolDepartmentList)
                li.Add(new SelectListItem { Text = (item.SchoolDepartmentName.Length > 20 ? item.SchoolDepartmentName.Substring(0, 20) + "..." : item.SchoolDepartmentName) , Value = item.SchoolDepartmentId.ToString() });

            ViewData["DepartmentList"] = li;

            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var model = new ExemplarViewModelAndDTO();
            model.ExemplarWallModel.SchoolId = SchoolId;
            model.ExemplarWallModel.BlogTypeId = 3;
            model.ExemplarWallModel.IsPublish = 1;
            model.ExemplarWallModel.CreatedBy = userId;
            model.ExemplarWallModel.CreatedOn = System.DateTime.Now;
            model.ExemplarWallModel.SortOrder = 1;
            var fileTypes = _fileService.GetFileTypes();

            model.AllowedFileExtension = fileTypes.Select(r => r.Extension).ToList();
            //  ViewBag.FileExtension = fileTypes.Select(r => r.Extension).ToList();
            // var groupList = _exemplarWallService.GetSchoolGroupBasedOnSchoolLevel(0, Convert.ToInt64(SessionHelper.CurrentSession.Id), 0,SessionHelper.CurrentSession.SchoolId).OrderBy(x=>x.SchoolGroupName);
            var groupList = _schoolGroupService.GetSchoolGroupsByUserId(userId, SessionHelper.CurrentSession.IsTeacher()).OrderBy(x => x.SchoolGroupName);
            li = new List<SelectListItem>();
            foreach (var item in groupList)
                if (!string.IsNullOrEmpty(item.SchoolGroupName))
                    item.SchoolGroupName = (item.SchoolGroupName.Length > 20 ? item.SchoolGroupName.Substring(0, 20) + "..." : item.SchoolGroupName);

            model.GroupsList.AddRange(groupList);

            //var SchoolCourseList = _courseService.GetCourses(1, Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), 1000, 1, "").OrderBy(x => x.Title);
            List<ListItem> SchoolCourseList = new List<ListItem>();
            SchoolCourseList = SelectListHelper.GetSelectListData(ListItems.Course, SessionHelper.CurrentSession.SchoolId).Distinct().ToList();
            li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = ResourceManager.GetString("Exemplar.Labels.coursedropdownmsg"), Value = "0" });
            foreach (var item in SchoolCourseList)
            {
                li.Add(new SelectListItem { Text = (item.ItemName.Length > 20 ? item.ItemName.Substring(0, 20) + "..." : item.ItemName) , Value = item.ItemId.ToString() });
            }
            ViewData["SchoolCourseList"] = li;
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            Session["CanEdit"] = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ExemplarWall.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;
            return View("WallPost", model);
        }

        public ActionResult GetAllGroupBySchoolId()
        {
            var groupList = _exemplarWallService.GetSchoolGroupBasedOnSchoolLevel(0,
                0, 0, SessionHelper.CurrentSession.SchoolId).OrderBy(x => x.SchoolGroupName);

            List<SelectListItem> li = new List<SelectListItem>();
            if (groupList.Count() == 0)
            {

                return Json(li, JsonRequestBehavior.AllowGet);
            }

            foreach (var item in groupList)
            {
                if (!string.IsNullOrEmpty(item.SchoolGroupName))
                {
                    li.Add(new SelectListItem
                    {
                        Text = (item.SchoolGroupName.Length > 20 ? item.SchoolGroupName.Substring(0, 20) + "..." : item.SchoolGroupName),
                        Value = item.SchoolGroupId.ToString()
                    });
                }
            }
            return Json(li, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetExemplarListByGroupId(string SchoolLevelId, string CourseId, string GroupId, string MyExemplarWork, string SearchText, string DepartmentId)
        {
            //if (SchoolLevelId == "0")
            //    SchoolLevelId = "0";
            if (CourseId == "null" || CourseId == "")
                CourseId = "0";
            if (GroupId == "")
                GroupId = "0";
            if (DepartmentId == "")
                DepartmentId = "0";


            var model = new List<ExemplarUserDetails>();
            model.AddRange(_exemplarWallService.GetBlogsByGroupNBlotTypeId(
                 Convert.ToInt32(SchoolLevelId)
                , Convert.ToInt32(DepartmentId)
                , Convert.ToInt32(CourseId)
                , Convert.ToInt32(SessionHelper.CurrentSession.SchoolId),
                 string.IsNullOrEmpty(GroupId) ? 0 : Convert.ToInt32(GroupId), 0, 0,
                SessionHelper.CurrentSession.UserTypeId == 2 ? Convert.ToInt64(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId) : Convert.ToInt64(SessionHelper.CurrentSession.Id),
                SessionHelper.CurrentSession.IsTeacher() ? SessionHelper.CurrentSession.Id : 0,
                string.IsNullOrEmpty(SearchText) ? "" : SearchText,
                SessionHelper.CurrentSession.UserTypeId != 3 && MyExemplarWork == "1" ? true : false)
                .OrderBy(r => r.SortOrder));
            return PartialView("_NewExemplarList", model);
        }

        public ActionResult GetSchoolGroupBySchoolLevelId(string levelId, string courseId)
        {
            var model = new ExemplarViewModelAndDTO();
            List<SelectListItem> li = new List<SelectListItem>();
            List<Phoenix.Models.SchoolGroup> groupList = new List<Phoenix.Models.SchoolGroup>();
            //li.Add(new SelectListItem { Text = "Select a group", Value = "0" });
            if (string.IsNullOrEmpty(courseId) || courseId == "0")
            {
                groupList = _exemplarWallService.GetSchoolGroupBasedOnSchoolLevel(0, 0, 0, SessionHelper.CurrentSession.SchoolId).ToList();
                foreach (var item in groupList.OrderBy(x => x.SchoolGroupName))
                {
                    if (!string.IsNullOrEmpty(item.SchoolGroupName))
                    {
                        li.Add(new SelectListItem
                        {
                            Text = (item.SchoolGroupName.Length > 20 ? item.SchoolGroupName.Substring(0, 20) + "..." : item.SchoolGroupName),
                            Value = item.SchoolGroupId.ToString()
                        });
                    }
                }
                return Json(li, JsonRequestBehavior.AllowGet);
            }

            groupList = _exemplarWallService.GetSchoolGroupBasedOnSchoolLevel(0, 0
                , string.IsNullOrEmpty(courseId) ? 0 : Convert.ToInt64(courseId), SessionHelper.CurrentSession.SchoolId).ToList().OrderBy(r => r.SchoolGroupName).Take(50).ToList();
            foreach (var item in groupList.OrderBy(x => x.SchoolGroupName))
            {
                if (!string.IsNullOrEmpty(item.SchoolGroupName))
                {
                    li.Add(new SelectListItem
                    {
                        Text = (item.SchoolGroupName.Length > 20 ? item.SchoolGroupName.Substring(0, 20) + "..." : item.SchoolGroupName),
                        Value = item.SchoolGroupId.ToString()
                    });
                }
            }
            return Json(li, JsonRequestBehavior.AllowGet);
            //return PartialView("_GroupList", model);
        }

        [HttpPost]
        public ActionResult GetStudentByGroupId(int? SchoolGroupId)
        {
            List<Student> objStudent = new List<Student>();
            //SelectList obgcity = null;
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            if (SchoolGroupId.HasValue)
            {
                objStudent = _exemplarWallService.GetStudentForTeacherBySchoolGroup(Convert.ToInt32(userId), 1, 1000, SchoolGroupId.ToString(), "", "Name");
                foreach (var student in objStudent)
                {
                    student.StudentImage = string.IsNullOrEmpty(student.StudentImage) ? "/content/VLE/img/avatar-1.jpg" : Constants.StudentImagesPath + student.StudentImage;
                }

                //obgcity = new SelectList(objStudent, "Id", "UserName", 0);
            }
            return Json(objStudent);
        }

        public JsonResult GetDepartment(string id)
        {
            List<SelectListItem> departments = new List<SelectListItem>();
            // Code here..
            return Json(new SelectList(departments, "Value", "Text"));
        }

        public JsonResult GetCourse(string id)
        {
            List<SelectListItem> li = new List<SelectListItem>();
            List<Phoenix.Models.Course> CourseList = new List<Phoenix.Models.Course>();
            //li.Add(new SelectListItem { Text = "Select a group", Value = "0" });
            if (id != "0")
            {
                CourseList = _exemplarWallService.GetCourseByDepartment(Convert.ToInt64(id)).ToList();
                foreach (var item in CourseList.OrderBy(x => x.Title))
                {
                    if (!string.IsNullOrEmpty(item.Title))
                    {
                        li.Add(new SelectListItem
                        {
                            Text = (item.Title.Length > 20 ? item.Title.Substring(0, 20) + "..." : item.Title),
                            Value = item.CourseId.ToString()
                        });
                    }
                }
                return Json(li, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<ListItem> SchoolCourseList = new List<ListItem>();
                SchoolCourseList = SelectListHelper.GetSelectListData(ListItems.Course, SessionHelper.CurrentSession.SchoolId).Distinct().ToList();
                li = new List<SelectListItem>();
                //li.Add(new SelectListItem { Text = ResourceManager.GetString("Exemplar.Labels.coursedropdownmsg"), Value = "0" });
                foreach (var item in SchoolCourseList)
                {
                    li.Add(new SelectListItem { Text = (item.ItemName.Length > 20 ? item.ItemName.Substring(0, 20) + "..." : item.ItemName),  
                        Value = item.ItemId.ToString() });
                }
            }
            return Json(li, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveExemplarData(ExemplarBlogDTO model)
        {

            var exemplar = new ExemplarWallModel();
            var GroupStudentMap = new SchoolGroup_StudentMapping();
            EntityMapper<ExemplarBlogDTO, ExemplarWallModel>.Map(model, exemplar);
            if (exemplar.EmbededVideoLink != null)
            {
                byte[] data = System.Convert.FromBase64String(exemplar.EmbededVideoLink);
                exemplar.EmbededVideoLink = System.Text.ASCIIEncoding.ASCII.GetString(data);
            }
            if (exemplar.ReferenceLink != null)
            {
                byte[] data = System.Convert.FromBase64String(exemplar.ReferenceLink);
                exemplar.ReferenceLink = System.Text.ASCIIEncoding.ASCII.GetString(data);
            }
            if (string.IsNullOrWhiteSpace(model.PostTitle))
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }

            string fileName = string.Empty;
            string FileNames = string.Empty;
            string relativePath = string.Empty;
            HttpFileCollectionBase files = Request.Files;

            if (files != null)
            {
                string fileString = "";
                string ParentSharableLink = "";
                if (files.Count > 0)
                {


                    //string SharePTUploadedFilePath = string.Empty;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        //Checking file is available to save.  
                        if (file != null)
                        {
                            //HttpPostedFileViewModel postedfile = new HttpPostedFileViewModel();
                            //postedfile.ContentLength = file.ContentLength;
                            //postedfile.FileName = file.FileName;
                            //postedfile.ContentType = file.ContentType;
                            //postedfile.InputStream = file.InputStream;

                            SharePointFileView SharePointFileView = new SharePointFileView();
                            if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                            {
                                AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                                await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                                SharePointFileView = await azureHelper.UploadFileSlicePerSliceAsync(FileModulesConstants.ExemplarPostFile, SessionHelper.CurrentSession.OldUserId.ToString(), file, file.ContentLength / (1024 * 1024) == 0 ? 5 : file.ContentLength / (1024 * 1024));
                            }
                            else
                            {
                                SharePointHelper sh = new SharePointHelper();
                                Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                                SharePointFileView = SharePointHelper.UploadFileSlicePerSlice(ref cx,
                           FileModulesConstants.ExemplarPostFile, SessionHelper.CurrentSession.OldUserId.ToString(), file, file.ContentLength / (1024 * 1024) == 0 ? 5 : file.ContentLength / (1024 * 1024));
                            }
                            fileString += SharePointFileView.SharepointUploadedFileURL + ",";
                            FileNames = FileNames + file.FileName + ",";
                            ParentSharableLink += SharePointFileView.ShareableLink + ",";
                            //fileName = Guid.NewGuid() + "_" + file.FileName;
                            //string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                            //string blogContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.BlogDir;
                            //string blogPath = PhoenixConfiguration.Instance.WriteFilePath + Constants.BlogDir + "/Blog_" + model.BlogTypeId;
                            //Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                            //Common.Helpers.CommonHelper.CreateDestinationFolder(blogContent);
                            //Common.Helpers.CommonHelper.CreateDestinationFolder(blogPath);
                            //relativePath = Constants.BlogDir + "/Blog_" + model.BlogTypeId + "/" + fileName;
                            //fileName = Path.Combine(blogPath, fileName);
                            //file.SaveAs(fileName);
                            //fileSting += relativePath + ",";
                        }
                    }

                    if (model.AdditionalDocPath != null)
                    {
                        exemplar.AdditionalDocPath = model.AdditionalDocPath + "," + fileString.TrimEnd(',');
                        exemplar.FileNames = !string.IsNullOrEmpty(exemplar.FileNames) ? exemplar.FileNames + "," + FileNames.TrimEnd(',') : FileNames.TrimEnd(',');
                        exemplar.ParentSharableLink = !string.IsNullOrEmpty(exemplar.ParentSharableLink) ? exemplar.ParentSharableLink + "," + ParentSharableLink.TrimEnd(',') : ParentSharableLink.TrimEnd(',');

                    }
                    else
                    {
                        exemplar.AdditionalDocPath = fileString.TrimEnd(',');
                        exemplar.FileNames = FileNames.TrimEnd(',');
                        exemplar.ParentSharableLink = ParentSharableLink.TrimEnd(',');
                    }
                }
                else
                {
                    exemplar.AdditionalDocPath = model.AdditionalDocPath;
                    exemplar.FileNames = exemplar.FileNames;
                    exemplar.ParentSharableLink = exemplar.ParentSharableLink;
                }
            }
            else
            {
                exemplar.AdditionalDocPath = model.AdditionalDocPath;
                exemplar.FileNames = exemplar.FileNames;
                exemplar.ParentSharableLink = exemplar.ParentSharableLink;
            }
            bool result = false;
            if (model.ExemplarWallId > 0)
            {

                exemplar.IsDeleted = false;
                exemplar.UpdatedBy = SessionHelper.CurrentSession.Id;
                exemplar.UserId = SessionHelper.CurrentSession.Id;
                result = _exemplarWallService.UpdateExemplar(exemplar);
            }
            else
            {
                exemplar.CreatedBy = SessionHelper.CurrentSession.Id;
                exemplar.IsDeleted = false;
                exemplar.ExemplarWallId = 0;
                var ExemplarId = _exemplarWallService.InsertExemplar(exemplar);
                model.ExemplarWallId = ExemplarId;
                result = true;
            }
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ShareExemplarData(sharepost model)
        {
            var result = _exemplarWallService.ShareExemplarPost(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditExemplarForm(long? id)
        {
            var model = new ExemplarEdit();
            model.IsAddMode = false;
            if (id.HasValue)
            {
                long SchoolId = SessionHelper.CurrentSession.SchoolId;
                var schoolLevelList = _exemplarWallService.GetSchoolLevelBySchoolId(SchoolId); ;

                List<SelectListItem> li = new List<SelectListItem>();
                //  li.Add(new SelectListItem { Text = "Select a level", Value = "0" });
                foreach (var item in schoolLevelList)
                {
                    li.Add(new SelectListItem { Text = item.SchoolLevelName, Value = item.SchoolLevelId.ToString() });
                }

                ViewData["SchoolLevel"] = li;

                ExemplarWallModel ewm = new ExemplarWallModel();
                ewm = _exemplarWallService.GetExemplar(id.Value);
                EntityMapper<ExemplarWallModel, ExemplarEdit>.Map(ewm, model);

                var fileTypes = _fileService.GetFileTypes();
                var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
                var groupList = _schoolGroupService.GetSchoolGroupsByUserId(userId, SessionHelper.CurrentSession.IsTeacher()).OrderBy(x => x.SchoolGroupName);
                foreach (var item in groupList)
                {
                    if (!string.IsNullOrEmpty(item.SchoolGroupName))
                    {
                        item.SchoolGroupName = item.SchoolGroupName.Length > 20 ? item.SchoolGroupName.Substring(0, 20) + "..." : item.SchoolGroupName;
                    }
                }
                model.AllowedFileExtension = fileTypes.Select(r => r.Extension).ToList();

                model.GroupsList.AddRange(groupList);
                // model.StudentList.AddRange(studentList);
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                Session["CanEdit"] = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ExemplarWall.ToString()).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);

            }
            return PartialView("_NewAddEditExemplarWall", model);
        }

        public ActionResult AddExemplarForm(long? id)
        {
            var model = new ExemplarEdit();
            if (id.HasValue)
            {
                long SchoolId = SessionHelper.CurrentSession.SchoolId;
                var schoolLevelList = _exemplarWallService.GetSchoolLevelBySchoolId(SchoolId); ;

                List<SelectListItem> li = new List<SelectListItem>();
                li.Add(new SelectListItem { Text = ResourceManager.GetString("Exemplar.Labels.leveldropdownmsg"), Value = "0" });
                foreach (var item in schoolLevelList)
                    li.Add(new SelectListItem { Text = item.SchoolLevelName, Value = item.SchoolLevelId.ToString() });

                ViewData["SchoolLevel"] = li;

                ExemplarWallModel ewm = new ExemplarWallModel();
                ewm = _exemplarWallService.GetExemplar(id.Value);
                EntityMapper<ExemplarWallModel, ExemplarEdit>.Map(ewm, model);
                model.ParentSharableLink = ewm.ParentSharableLink;
                model.FileNames = ewm.FileNames;

                var fileTypes = _fileService.GetFileTypes();
                var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
                var groupList = _schoolGroupService.GetSchoolGroupsByUserId(userId, SessionHelper.CurrentSession.IsTeacher()).OrderBy(x => x.SchoolGroupName);
                foreach (var item in groupList)
                    if (!string.IsNullOrEmpty(item.SchoolGroupName))
                        item.SchoolGroupName = item.SchoolGroupName.Length > 20 ? item.SchoolGroupName.Substring(0, 20) + "..." : item.SchoolGroupName;

                model.AllowedFileExtension = fileTypes.Select(r => r.Extension).ToList();
                ViewBag.FileExtension = fileTypes.Select(r => r.Extension).ToList();
                model.GroupsList.AddRange(groupList);
                model.IsAddMode = false;
                // model.StudentList.AddRange(studentList);
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                Session["CanEdit"] = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ExemplarWall.ToString()).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);
                Session["UpdateData"] = model;
            }
            else
            {
                long SchoolId = SessionHelper.CurrentSession.SchoolId;
                var schoolLevelList = _exemplarWallService.GetSchoolLevelBySchoolId(SchoolId); ;

                List<SelectListItem> li = new List<SelectListItem>();
                li.Add(new SelectListItem { Text = ResourceManager.GetString("Exemplar.Labels.SelectLevel"), Value = "0" });
                foreach (var item in schoolLevelList)
                {
                    li.Add(new SelectListItem { Text = item.SchoolLevelName, Value = item.SchoolLevelId.ToString() });
                }

                ViewData["SchoolLevel"] = li;
                var fileTypes = _fileService.GetFileTypes();

                model.SchoolId = SchoolId;
                var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
                model.UserId = userId;
                var groupList = _exemplarWallService.GetSchoolGroupBasedOnSchoolLevel(0,
                    SessionHelper.CurrentSession.UserTypeId == 2 ? Convert.ToInt64(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId) : Convert.ToInt64(SessionHelper.CurrentSession.Id), 0, SessionHelper.CurrentSession.SchoolId).OrderBy(x => x.SchoolGroupName);
                groupList = groupList.OrderBy(x => x.SchoolGroupName);
                model.AllowedFileExtension = fileTypes.Select(r => r.Extension).ToList();
                ViewBag.FileExtension = fileTypes.Select(r => r.Extension).ToList();
                foreach (var item in groupList)
                {
                    if (!string.IsNullOrEmpty(item.SchoolGroupName))
                    {
                        item.SchoolGroupName = item.SchoolGroupName.Length > 20 ? item.SchoolGroupName.Substring(0, 20) + "..." : item.SchoolGroupName;
                    }
                }
                model.GroupsList.AddRange(groupList.ToList());

                model.IsAddMode = true;
            }
            return PartialView("_NewAddEditExemplarWall", model);
        }

        [HttpPost]
        public ActionResult DeletePostFile(string postId, string fileName, string UploadedFilePath)
        {
            //var model = new ExemplarEdit();
            //string relativepath = string.Empty;
            bool IsSuccess = false;

            if (!String.IsNullOrEmpty(postId))
            {        //Delete Files From storage
                if (System.IO.File.Exists(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath))
                {
                    System.IO.File.Delete(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath);

                }
                IsSuccess = true;
                //Save
                // model = Session["UpdateData"] as ExemplarEdit;
            }
            //return PartialView("_QuizFileList", lstfiles);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteExemplar(ExemplarWallModel model)
        {
            model.DeletedBy = SessionHelper.CurrentSession.Id;
            model.UserId = SessionHelper.CurrentSession.Id;
            var response = _exemplarWallService.DeleteExemplar(model) > 0;
            return Json(new OperationDetails(response), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateWallSortOrder(string Ids)
        {
            var BlogIds = new JavaScriptSerializer().Deserialize<int[]>(Ids);
            DataTable dt = new DataTable();
            dt.Columns.Add("BlogId");
            dt.Columns.Add("SortOrder");
            int i = 1;
            foreach (int item in BlogIds)
            {
                DataRow dr = dt.NewRow();
                dr["BlogId"] = item;
                dr["SortOrder"] = i;
                dt.Rows.Add(dr);
                i++;
            }
            var model = new ExemplarWallOrder();
            model.dataTable = dt;
            model.UpdatedBy = SessionHelper.CurrentSession.Id;
            var result = _exemplarWallService.UpdateWallSortOrder(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApproveExemplarPost()
        {
            return View();
        }

        public ActionResult UpdateExemplarPostStatusFromPendingToApprove(string ExemplarPostId)
        {
            return Json(_exemplarWallService.UpdateExemplarPostStatusFromPendingToApprove(Convert.ToInt64(ExemplarPostId)), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPendingForApprovalPost()
        {
            var ExemplarPostList = _exemplarWallService.GetPendingForApprovalExemplarPost(SessionHelper.CurrentSession.SchoolId);
            //var ExemplarPostList = _exemplarWallService.GetPendingForApprovalExemplarPost(131001);
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var CustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_PlanTemplate.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var dataList = new
            {
                aaData = (from item in ExemplarPostList
                          select new
                          {
                              PostName = item.PostTitle,
                              Status = "Pending",
                              Actions = GetPlanTemplateGridActionLink(CustomPermission, item)

                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        private string GetPlanTemplateGridActionLink(bool isCustomPermission, ExemplarWallModel item)
        {
            return " <div class='d-flex justify-content-center'> <input type='checkbox' data-ExemplarWallId=" + item.ExemplarWallId + " class='approveExemplarPost mt-1'/>" +
                "<a class='btnRejectPlan ml-3' data-ExemplarWallId=" + item.ExemplarWallId + " > <img src='/Content/vle/img/svg/tbl-delete.svg' ></a></div> ";
        }

        public ActionResult GetApproveStatusDetail(string Status, string Operation)
        {
            var result =
                 _exemplarWallService.GetApprovalStatusDetails(
                 SessionHelper.CurrentSession.SchoolId,
                 Operation, "Exemplar", Status == "true" ? true : false
                 );
            return Json(new { Status = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReadMoreExemplar(ExemplarWallModel model)
        {
            return PartialView("_ReadMore", model);
        }

        public ActionResult RejectExemplarPost(string ExemplarPostId)
        {
            var _exemplarwallmodel = new ExemplarWallModel();
            _exemplarwallmodel.ExemplarWallId = Convert.ToInt64(ExemplarPostId);
            _exemplarwallmodel.IsRejected = true;
            _exemplarwallmodel.UpdatedBy = SessionHelper.CurrentSession.Id;
            var result = _exemplarWallService.UpdateExemplar(_exemplarwallmodel);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult viewAttachment(string docpath, string filenames, string shareablelink)
        {
            ExemplarWallModel exemplar = new ExemplarWallModel();
            exemplar.AdditionalDocPath = docpath;
            exemplar.FileNames = filenames;
            exemplar.ParentSharableLink = shareablelink;
            var htmlString = ControllerExtension.RenderPartialViewToString(this, "_ViewAttachment", exemplar);
            return Json(new { planHTML = htmlString }, JsonRequestBehavior.AllowGet);
        }

    }


}