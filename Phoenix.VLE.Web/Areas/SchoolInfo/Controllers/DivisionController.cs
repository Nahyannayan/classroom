﻿using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Areas.Course.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Phoenix.Common.Helpers.Extensions;
using DevExpress.XtraCharts;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class DivisionController : BaseController
    {
        private IDivisionService _DivisionService;
        private ISIMSCommonService _SIMSCommonService;
        private IAttendanceSettingService _attendanceSettingService;
        public DivisionController(IDivisionService DivisionService, ISIMSCommonService simsCommonService, IAttendanceSettingService attendanceSettingService)
        {
            _DivisionService = DivisionService;
            _SIMSCommonService = simsCommonService;
            _attendanceSettingService = attendanceSettingService;
        }
        // GET: Division/Division
        public ActionResult Index()
        {
            var CurriculumId = 1;
            long ACD_ID = Phoenix.Common.Helpers.SessionHelper.CurrentSession.ACD_ID;
            var schoolId = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;

            List<DivisionDetails> DivisionDetails = new List<DivisionDetails>();
            DivisionDetails = _DivisionService.GetDivisionDetails(schoolId, CurriculumId).ToList();
            return View(DivisionDetails);
        }
        public ActionResult SaveGradeDetails(DivisionDetails DivisionDetails)
        {
            string username = SessionHelper.CurrentSession.UserName;
            DivisionDetails.CREATED_BY = SessionHelper.CurrentSession.Id;
            var schoolid = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            DivisionDetails.BSU_ID = schoolid;

            var result = _DivisionService.SaveDivisionDetails(DivisionDetails, DivisionDetails.DataMode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDivisionDetails()
        {
            var CurriculumId = 1;
            long ACD_ID = Phoenix.Common.Helpers.SessionHelper.CurrentSession.ACD_ID;
            var schoolid = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            ViewBag.GradeList = new SelectList(SelectListHelper.GetSelectListData(ListItems.Grade, $"{ACD_ID},{SchoolDivisionHelper.GetCurrentDivision}"), "ItemId", "ItemName");
            List<DivisionDetails> DivisionDetails = new List<DivisionDetails>();
            DivisionDetails = _DivisionService.GetDivisionDetails(schoolid, CurriculumId).ToList();
            return PartialView("_GetDivisionDetails", DivisionDetails);
        }
        public PartialViewResult AddEditDivision()
        {
            var CurriculumId = 1;
            long ACD_ID = Phoenix.Common.Helpers.SessionHelper.CurrentSession.ACD_ID;
            var schoolId = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            DivisionDetails Model = new DivisionDetails();
            Model.DataMode = "Add";
            ViewBag.GradeList = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolGrade, schoolId), "ItemId", "ItemName");
            return PartialView("_AddEditDivision", Model);
        }
        public JsonResult GetDivisionUsingCurriculum(int curriculumId )
        {
            var divisionDetails = _DivisionService.GetDivisionDetails(SessionHelper.CurrentSession.SchoolId, curriculumId)
                .Select(x => new SelectListItem { Text = x.DivisionName, Value = x.DivisionId.ToString() });
            return Json(divisionDetails, JsonRequestBehavior.AllowGet);
        }

    }
}