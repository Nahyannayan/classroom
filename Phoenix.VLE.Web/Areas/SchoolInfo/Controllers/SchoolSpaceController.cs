﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    [Authorize]
    public class SchoolSpaceController : BaseController
    {
        private readonly ISchoolSpaceService _schoolSpaceService;
        private readonly ISchoolGroupService _schoolGroupService;

        public SchoolSpaceController(ISchoolSpaceService schoolSpaceService,ISchoolGroupService schoolGroupService )
        {
            _schoolSpaceService = schoolSpaceService;
            _schoolGroupService = schoolGroupService;
        }
        // GET: School/SchoolSpace
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult TeacherSpaces()
        {
            var schoolSpaces = _schoolSpaceService.GetSchoolSpaceBySchoolId((int)SessionHelper.CurrentSession.SchoolId);
            return View(schoolSpaces);
        }
        public ActionResult StudentSpaces()
        {
            var schoolSpaces = _schoolSpaceService.GetSchoolSpaceBySchoolId((int)SessionHelper.CurrentSession.SchoolId);
            return View(schoolSpaces);
        }

        public ActionResult LoadSchoolSpaceGrid()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            //var schoolSpaces = _schoolSpaceService.GetSchoolSpaceBySchoolId(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId));
            var schoolSpaces = _schoolSpaceService.GetSchoolSpaceBySchoolId((int)user.SchoolId);
            //if(SessionHelper.CurrentSession.IsStudent())
            //{
            //    return View("StudentSpaces", schoolSpaces);
            //}
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in schoolSpaces
                          select new
                          {
                              Actions = HtmlHelperExtensions.LocalizedEditDeleteLinkButtons(item.SpaceId, "SchoolSpace", "schoolSpaces"),
                              Editors = $@"{HtmlHelperExtensions.LocalizedLinkButton(new HyperlinkAttributes()
                              {
                                  InnerIconCssClass = "fas fa-users",
                                  //OnClick = "gradingTemplates.openGradingTemplateItem(" + item.GradingTemplateId + ")",
                                  TitleResourceKey = "School.SchoolSpace.Editors"
                              })}",
                              IconCssClass = $"<span class='{item.IconCssClass}'></span>",
                              Title = $"{item.SpaceName}<span>{item.Description}</span>",
                              Resources = $@"{HtmlHelperExtensions.LocalizedLinkButton(new HyperlinkAttributes()
                              {
                                    InnerIconCssClass = "fas fa-folder",
                                  //OnClick = "gradingTemplates.openGradingTemplateItem(" + item.GradingTemplateId + ")",
                                  Title = $"{item.FolderCount}"
                              })} {HtmlHelperExtensions.LocalizedLinkButton(new HyperlinkAttributes()
                              {
                                  InnerIconCssClass = "fas fa-file-alt",
                                  //OnClick = "gradingTemplates.openGradingTemplateItem(" + item.GradingTemplateId + ")",
                                  Title = $"{item.FileCount}"
                              })}"
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitAddEditSchoolSpaceForm(int? id)
        {
            var model = new SchoolSpaceEdit();
            if (id.HasValue)
            {
                var schoolSpace = _schoolSpaceService.GetSchoolSpaceById(id.Value);
                EntityMapper<SchoolSpace, SchoolSpaceEdit>.Map(schoolSpace, model);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = (int) SessionHelper.CurrentSession.SchoolId;
            }
            ViewBag.SchoolSpaceCategory = SelectListHelper.GetSelectListData(ListItems.SchoolSpaceCategory);
            return PartialView("_AddEditSchoolSpace", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSchoolSpaceData(SchoolSpaceEdit model)
        {
            //model.ParseJsonToXml("SpaceName");
            model.IsActive = true;
            var result = _schoolSpaceService.AddOrUpdateSchoolSpaceData(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSchoolSpaceData(int id)
        {
            var result = _schoolSpaceService.DeleteSchoolSpaceData(id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

    }
}