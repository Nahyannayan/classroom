﻿using DevExpress.DataAccess.Json;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web;
using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.IO;
using Phoenix.Common.Enums;
using CrystalDecisions.CrystalReports.Engine;
using System.Data;
using CrystalDecisions.Shared;
using System.Web.Caching;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class SchoolReportController : BaseController
    {
        private readonly ISchoolReportService schoolReportService;
        private readonly ISchoolService schoolService;
        const string FileExtension = ".repx";
        private byte[] _content;
        public SchoolReportController(ISchoolReportService schoolReportService, ISchoolService schoolService)
        {
            this.schoolReportService = schoolReportService;
            this.schoolService = schoolService;
        }
        // GET: SchoolInfo/SchoolReports
        public ActionResult Index()
        {
            XtraReport report = new XtraReport()
            {
                Bands = {
                    new TopMarginBand
                    {
                        Controls =
                        {
                            new XRPictureBox
                    {
                        ImageUrl = PhoenixConfiguration.Instance.ReadFilePath + "/Content/Student/img/cambridge.png",
                        BoundsF = new RectangleF(100,100,100,100),
                        ExpressionBindings = {
                            new ExpressionBinding("ImageUrl", "[CompanyImage]")
                        },
                        ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.TopLeft,
                        Sizing = DevExpress.XtraPrinting.ImageSizeMode.AutoSize,
                        LocationF = new PointF(25,25)
                    }
                        }
                    },
            new DetailBand() {
                Controls = {
                    new XRLabel() {
                        ExpressionBindings = {
                            new ExpressionBinding("BeforePrint", "Text", "[CompanyName]")
                        },
                        WidthF = 300
                    }
                }
            }
        },
                DataSource = CreateDataSourceFromText()
                //DataMember = "Customers Test "
            };
            report.LoadLayout(@"E:\ClientProject\GBS\Dev\Phoenix.VLE.Web\Resources\SchoolReports\ADJ\Test 3.repx");
            ViewBag.reportDetailId = 10;
            return View(report);
        }
        private string getHositingUrl()
        {
            return string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")); 
        }
        private JsonDataSource CreateDataSourceFromText()
        {
            var jsonDataSource = new JsonDataSource();

            // Specify a string with JSON content
            string json = "{\"Customers\":[{\"Id\":\"ALFKI\",\"CompanyImage\":\""+ PhoenixConfiguration.Instance.ReadFilePath + "/Content/Student/img/cambridge.png" + "\",\"CompanyName\":\"Alfreds Futterkiste\",\"ContactName\":\"Maria Anders\",\"ContactTitle\":\"Sales Representative\",\"Address\":\"Obere Str. 57\",\"City\":\"Berlin\",\"PostalCode\":\"12209\",\"Country\":\"Germany\",\"Phone\":\"030-0074321\",\"Fax\":\"030-0076545\"}],\"ResponseStatus\":{}}";

            // Specify the object that retrieves JSON data
            jsonDataSource.JsonSource = new CustomJsonSource(json);
            // Retrieve data from the JSON data source to the Report Designer's Field List
            jsonDataSource.Fill();
            return jsonDataSource;
        }
        public ActionResult ViewDocument()
        {
            var jsonDataSource = new JsonDataSource();
            string json = "{ \"ColumnFields\":[{\"UserId\":644943,\"StudentNumber\":\"13100100040227\",\"StudentName\":\"Adam Erumala Billa\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"ABSENT\",\"AttendaceCode\":\"O\"},{\"UserId\":644885,\"StudentNumber\":\"13100100040158\",\"StudentName\":\"Andriya  Shijo\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"ABSENT\",\"AttendaceCode\":\"O\"},{\"UserId\":645176,\"StudentNumber\":\"13100100040547\",\"StudentName\":\"Angela Rose Dennis\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"ABSENT\",\"AttendaceCode\":\"O\"},{\"UserId\":645107,\"StudentNumber\":\"13100100040454\",\"StudentName\":\"Aron  Mathew\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"ABSENT\",\"AttendaceCode\":\"O\"},{\"UserId\":646568,\"StudentNumber\":\"13100100045209\",\"StudentName\":\"Ayaan Shinas  Ahamed\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645531,\"StudentNumber\":\"13100100041040\",\"StudentName\":\"Catherine  Vincent\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645006,\"StudentNumber\":\"13100100040319\",\"StudentName\":\"Eva  Aneesh\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645203,\"StudentNumber\":\"13100100040580\",\"StudentName\":\"Eva Pramod Pramod Kolanchery Pappu\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645612,\"StudentNumber\":\"13100100041144\",\"StudentName\":\"Farhaz Aman Al Rahim\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645487,\"StudentNumber\":\"13100100040981\",\"StudentName\":\"Hannah  Valappila\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":644936,\"StudentNumber\":\"13100100040219\",\"StudentName\":\"Johanna Saji John\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":644913,\"StudentNumber\":\"13100100040190\",\"StudentName\":\"John Thomas Kalayamkandathil John\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":644931,\"StudentNumber\":\"13100100040214\",\"StudentName\":\"Mariam Fatima Kazmi\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645159,\"StudentNumber\":\"13100100040521\",\"StudentName\":\"Naveen Jayakrishnan Pillai\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645984,\"StudentNumber\":\"13100100041627\",\"StudentName\":\"Pavani  Garg\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":644937,\"StudentNumber\":\"13100100040220\",\"StudentName\":\"Pranav  Mahendran\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":646127,\"StudentNumber\":\"13100100044700\",\"StudentName\":\"Royce  Boban\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645410,\"StudentNumber\":\"13100100040883\",\"StudentName\":\"Rufaida  Rabeef\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645150,\"StudentNumber\":\"13100100040509\",\"StudentName\":\"Sana Zahra Mohamed Mahaboobdeen\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645644,\"StudentNumber\":\"13100100041181\",\"StudentName\":\"Sanjay Deepak Kamath\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":644854,\"StudentNumber\":\"13100100040113\",\"StudentName\":\"Sivanand  Subash\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645418,\"StudentNumber\":\"13100100040891\",\"StudentName\":\"Sravya  Manoj\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645415,\"StudentNumber\":\"13100100040888\",\"StudentName\":\"Steffy Sarah John\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645396,\"StudentNumber\":\"13100100040863\",\"StudentName\":\"Svastika  Sandeep\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645464,\"StudentNumber\":\"13100100040950\",\"StudentName\":\"Vamika  Vinesh\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645066,\"StudentNumber\":\"13100100040402\",\"StudentName\":\"Varun  Rajesh\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":647077,\"StudentNumber\":\"13100100045803\",\"StudentName\":\"VIDUSHI  SHARMA\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645114,\"StudentNumber\":\"13100100040462\",\"StudentName\":\"Vinaya Loshitha Ramarajan\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":644855,\"StudentNumber\":\"13100100040116\",\"StudentName\":\"Zeba Muhammed Shameel\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645167,\"StudentNumber\":\"13100100040533\",\"StudentName\":\"Zehwah Mehek Ahammad Zubair Chaithottam House\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"},{\"UserId\":645470,\"StudentNumber\":\"13100100040959\",\"StudentName\":\"Zunairah  Mateen\",\"Grade\":\"01\",\"Section\":\"Q\",\"AttendanceDate\":\"2020-08-06T00:00:00\",\"Weightage\":100.0,\"Session\":\"SESSION1\",\"Status\":\"PRESENT\",\"AttendaceCode\":\"/\"}]}";
            //string json = "{\"Customers\":[{\"CompanyName\":\"Ram1\",\"ContactName\":\"ram\",\"ContactTitle\":\"TTT\",\"Address\":\"ABC231\",\"PostalCode\":1123,\"Country\":\"ADV1\"},{\"CompanyName\":\"Shyam\",\"ContactName\":\"shyam\",\"ContactTitle\":\"YY\",\"Address\":\"TT23\",\"PostalCode\":1232,\"Country\":\"ADV2\"},{\"CompanyName\":\"John\",\"ContactName\":\"johngmail\",\"ContactTitle\":\"johnTTT\",\"Address\":\"India\",\"PostalCode\":1233,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32 @gmail.com\",\"ContactTitle\":41,\"Address\":23,\"PostalCode\":1234,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32\",\"ContactTitle\":\"TTTT\",\"Address\":\"YYY\",\"PostalCode\":1235,\"Country\":\"ADV5\"},{\"CompanyName\":\"Bob6\",\"ContactName\":\"bob3\",\"ContactTitle\":\"UUUU\",\"Address\":\"UUUUU234\",\"PostalCode\":1236,\"Country\":\"ADV6\"},{\"CompanyName\":\"Bob7\",\"ContactName\":\"bob327\",\"ContactTitle\":\"I7\",\"Address\":\"QQQQ55\",\"PostalCode\":1237,\"Country\":\"ADV7\"},{\"CompanyName\":\"BobQQQ8\",\"ContactName\":\"bob32\",\"ContactTitle\":\"QQQQ\",\"Address\":\"FFFF8\",\"PostalCode\":1238,\"Country\":\"ADV8\"},{\"CompanyName\":\"Bob9\",\"ContactName\":\"bob9777\",\"ContactTitle\":\"FFFF9\",\"Address\":\"BBBB9\",\"PostalCode\":1239,\"Country\":\"ADV7\"},{\"CompanyName\":\"Bob10\",\"ContactName\":\"bob3210\",\"ContactTitle\":\"YYYY\",\"Address\":\"OOOO\",\"PostalCode\":11,\"Country\":\"ADV11\"}," +
            //    "{\"CompanyName\":\"Ram1\",\"ContactName\":\"ram\",\"ContactTitle\":\"TTT\",\"Address\":\"ABC231\",\"PostalCode\":1123,\"Country\":\"ADV1\"},{\"CompanyName\":\"Shyam\",\"ContactName\":\"shyam\",\"ContactTitle\":\"YY\",\"Address\":\"TT23\",\"PostalCode\":1232,\"Country\":\"ADV2\"},{\"CompanyName\":\"John\",\"ContactName\":\"johngmail\",\"ContactTitle\":\"johnTTT\",\"Address\":\"India\",\"PostalCode\":1233,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32 @gmail.com\",\"ContactTitle\":41,\"Address\":23,\"PostalCode\":1234,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32\",\"ContactTitle\":\"TTTT\",\"Address\":\"YYY\",\"PostalCode\":1235,\"Country\":\"ADV5\"},{\"CompanyName\":\"Bob6\",\"ContactName\":\"bob3\",\"ContactTitle\":\"UUUU\",\"Address\":\"UUUUU234\",\"PostalCode\":1236,\"Country\":\"ADV6\"},{\"CompanyName\":\"Bob7\",\"ContactName\":\"bob327\",\"ContactTitle\":\"I7\",\"Address\":\"QQQQ55\",\"PostalCode\":1237,\"Country\":\"ADV7\"},{\"CompanyName\":\"BobQQQ8\",\"ContactName\":\"bob32\",\"ContactTitle\":\"QQQQ\",\"Address\":\"FFFF8\",\"PostalCode\":1238,\"Country\":\"ADV8\"},{\"CompanyName\":\"Bob9\",\"ContactName\":\"bob9777\",\"ContactTitle\":\"FFFF9\",\"Address\":\"BBBB9\",\"PostalCode\":1239,\"Country\":\"ADV7\"},{\"CompanyName\":\"Bob10\",\"ContactName\":\"bob3210\",\"ContactTitle\":\"YYYY\",\"Address\":\"OOOO\",\"PostalCode\":11,\"Country\":\"ADV11\"}," +
            //    "{\"CompanyName\":\"Ram1\",\"ContactName\":\"ram\",\"ContactTitle\":\"TTT\",\"Address\":\"ABC231\",\"PostalCode\":1123,\"Country\":\"ADV1\"},{\"CompanyName\":\"Shyam\",\"ContactName\":\"shyam\",\"ContactTitle\":\"YY\",\"Address\":\"TT23\",\"PostalCode\":1232,\"Country\":\"ADV2\"},{\"CompanyName\":\"John\",\"ContactName\":\"johngmail\",\"ContactTitle\":\"johnTTT\",\"Address\":\"India\",\"PostalCode\":1233,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32 @gmail.com\",\"ContactTitle\":41,\"Address\":23,\"PostalCode\":1234,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32\",\"ContactTitle\":\"TTTT\",\"Address\":\"YYY\",\"PostalCode\":1235,\"Country\":\"ADV5\"},{\"CompanyName\":\"Bob6\",\"ContactName\":\"bob3\",\"ContactTitle\":\"UUUU\",\"Address\":\"UUUUU234\",\"PostalCode\":1236,\"Country\":\"ADV6\"},{\"CompanyName\":\"Bob7\",\"ContactName\":\"bob327\",\"ContactTitle\":\"I7\",\"Address\":\"QQQQ55\",\"PostalCode\":1237,\"Country\":\"ADV7\"},{\"CompanyName\":\"BobQQQ8\",\"ContactName\":\"bob32\",\"ContactTitle\":\"QQQQ\",\"Address\":\"FFFF8\",\"PostalCode\":1238,\"Country\":\"ADV8\"},{\"CompanyName\":\"Bob9\",\"ContactName\":\"bob9777\",\"ContactTitle\":\"FFFF9\",\"Address\":\"BBBB9\",\"PostalCode\":1239,\"Country\":\"ADV7\"},{\"CompanyName\":\"Bob10\",\"ContactName\":\"bob3210\",\"ContactTitle\":\"YYYY\",\"Address\":\"OOOO\",\"PostalCode\":11,\"Country\":\"ADV11\"}," +
            //    "{\"CompanyName\":\"Ram1\",\"ContactName\":\"ram\",\"ContactTitle\":\"TTT\",\"Address\":\"ABC231\",\"PostalCode\":1123,\"Country\":\"ADV1\"},{\"CompanyName\":\"Shyam\",\"ContactName\":\"shyam\",\"ContactTitle\":\"YY\",\"Address\":\"TT23\",\"PostalCode\":1232,\"Country\":\"ADV2\"},{\"CompanyName\":\"John\",\"ContactName\":\"johngmail\",\"ContactTitle\":\"johnTTT\",\"Address\":\"India\",\"PostalCode\":1233,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32 @gmail.com\",\"ContactTitle\":41,\"Address\":23,\"PostalCode\":1234,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32\",\"ContactTitle\":\"TTTT\",\"Address\":\"YYY\",\"PostalCode\":1235,\"Country\":\"ADV5\"},{\"CompanyName\":\"Bob6\",\"ContactName\":\"bob3\",\"ContactTitle\":\"UUUU\",\"Address\":\"UUUUU234\",\"PostalCode\":1236,\"Country\":\"ADV6\"},{\"CompanyName\":\"Bob7\",\"ContactName\":\"bob327\",\"ContactTitle\":\"I7\",\"Address\":\"QQQQ55\",\"PostalCode\":1237,\"Country\":\"ADV7\"},{\"CompanyName\":\"BobQQQ8\",\"ContactName\":\"bob32\",\"ContactTitle\":\"QQQQ\",\"Address\":\"FFFF8\",\"PostalCode\":1238,\"Country\":\"ADV8\"},{\"CompanyName\":\"Bob9\",\"ContactName\":\"bob9777\",\"ContactTitle\":\"FFFF9\",\"Address\":\"BBBB9\",\"PostalCode\":1239,\"Country\":\"ADV7\"},{\"CompanyName\":\"Bob10\",\"ContactName\":\"bob3210\",\"ContactTitle\":\"YYYY\",\"Address\":\"OOOO\",\"PostalCode\":11,\"Country\":\"ADV11\"}," +
            //    "{\"CompanyName\":\"Ram1\",\"ContactName\":\"ram\",\"ContactTitle\":\"TTT\",\"Address\":\"ABC231\",\"PostalCode\":1123,\"Country\":\"ADV1\"},{\"CompanyName\":\"Shyam\",\"ContactName\":\"shyam\",\"ContactTitle\":\"YY\",\"Address\":\"TT23\",\"PostalCode\":1232,\"Country\":\"ADV2\"},{\"CompanyName\":\"John\",\"ContactName\":\"johngmail\",\"ContactTitle\":\"johnTTT\",\"Address\":\"India\",\"PostalCode\":1233,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32 @gmail.com\",\"ContactTitle\":41,\"Address\":23,\"PostalCode\":1234,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32\",\"ContactTitle\":\"TTTT\",\"Address\":\"YYY\",\"PostalCode\":1235,\"Country\":\"ADV5\"},{\"CompanyName\":\"Bob6\",\"ContactName\":\"bob3\",\"ContactTitle\":\"UUUU\",\"Address\":\"UUUUU234\",\"PostalCode\":1236,\"Country\":\"ADV6\"},{\"CompanyName\":\"Bob7\",\"ContactName\":\"bob327\",\"ContactTitle\":\"I7\",\"Address\":\"QQQQ55\",\"PostalCode\":1237,\"Country\":\"ADV7\"},{\"CompanyName\":\"BobQQQ8\",\"ContactName\":\"bob32\",\"ContactTitle\":\"QQQQ\",\"Address\":\"FFFF8\",\"PostalCode\":1238,\"Country\":\"ADV8\"},{\"CompanyName\":\"Bob9\",\"ContactName\":\"bob9777\",\"ContactTitle\":\"FFFF9\",\"Address\":\"BBBB9\",\"PostalCode\":1239,\"Country\":\"ADV7\"},{\"CompanyName\":\"Bob10\",\"ContactName\":\"bob3210\",\"ContactTitle\":\"YYYY\",\"Address\":\"OOOO\",\"PostalCode\":11,\"Country\":\"ADV11\"}]}";
            //"{\"CompanyName\":\"Ram1\",\"ContactName\":\"ram\",\"ContactTitle\":\"TTT\",\"Address\":\"ABC231\",\"PostalCode\":1123,\"Country\":\"ADV1\"},{\"CompanyName\":\"Shyam\",\"ContactName\":\"shyam\",\"ContactTitle\":\"YY\",\"Address\":\"TT23\",\"PostalCode\":1232,\"Country\":\"ADV2\"},{\"CompanyName\":\"John\",\"ContactName\":\"johngmail\",\"ContactTitle\":\"johnTTT\",\"Address\":\"India\",\"PostalCode\":1233,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32 @gmail.com\",\"ContactTitle\":41,\"Address\":23,\"PostalCode\":1234,\"Country\":\"ADV3\"},{\"CompanyName\":\"Bob\",\"ContactName\":\"bob32\",\"ContactTitle\":\"TTTT\",\"Address\":\"YYY\",\"PostalCode\":1235,\"Country\":\"ADV5\"},{\"CompanyName\":\"Bob6\",\"ContactName\":\"bob3\",\"ContactTitle\":\"UUUU\",\"Address\":\"UUUUU234\",\"PostalCode\":1236,\"Country\":\"ADV6\"},{\"CompanyName\":\"Bob7\",\"ContactName\":\"bob327\",\"ContactTitle\":\"I7\",\"Address\":\"QQQQ55\",\"PostalCode\":1237,\"Country\":\"ADV7\"},{\"CompanyName\":\"BobQQQ8\",\"ContactName\":\"bob32\",\"ContactTitle\":\"QQQQ\",\"Address\":\"FFFF8\",\"PostalCode\":1238,\"Country\":\"ADV8\"},{\"CompanyName\":\"Bob9\",\"ContactName\":\"bob9777\",\"ContactTitle\":\"FFFF9\",\"Address\":\"BBBB9\",\"PostalCode\":1239,\"Country\":\"ADV7\"},{\"CompanyName\":\"Bob10\",\"ContactName\":\"bob3210\",\"ContactTitle\":\"YYYY\",\"Address\":\"OOOO\",\"PostalCode\":11,\"Country\":\"ADV11\"}]}\"";
            jsonDataSource.JsonSource = new CustomJsonSource(json);
            // Retrieve data from the JSON data source to the Report Designer\"s Field List
            jsonDataSource.Fill();
            var report = new XtraReport();
            //report.LoadLayout(@"E:\ClientProject\GBS\Dev\Phoenix.VLE.Web\Resources\SchoolReports\ADJ\Test 3.repx");
            report.LoadLayout(@"E:\ClientProject\GBS\Dev\Phoenix.VLE.Web\Resources\SchoolReports\ADJ\AttendaceReport.repx");
            report.DataSource = jsonDataSource;
            var cachedReportSource = new CachedReportSourceWeb(report);
            return View(report);
        }
        public ActionResult ViewReportList()
        {
            return View();
        }
        public ActionResult LoadReportList()
        {
            var isCustomPermissionEnabled = SessionHelper.CurrentSession.IsAdmin || SessionHelper.CurrentSession.IsTeacher();
            IEnumerable<ReportListModel> templateList = new List<ReportListModel>();
            templateList = schoolReportService.GetReportLists(SessionHelper.CurrentSession.SchoolId);
            var dataList = new
            {
                aaData = (from item in templateList
                          select new
                          {
                              item.Name,
                              item.Description,
                              item.FileName,
                              Actions = GetActionLinks(item)
                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        private string GetActionLinks(ReportListModel model)
        {
            string actions = string.Empty;
            if (!model.IsCrystalReport)
            {
                if (SessionHelper.CurrentSession.UserTypeId == 5)
                {
                    #if DEBUG
                        actions += $"<a class='mr-2' href='/SchoolInfo/SchoolReport/AddEditReportConfig?id={model.PrimaryReportId.EncryptUrl()}&configtype={1.EncryptUrl()}' data-toggle='tooltip'  data-original-title='" + ResourceManager.GetString("Shared.Buttons.Edit") + $"' )'><img src='/Content/vle/img/svg/pencil-big.svg' /></a>";                    
                    #else
                        actions += $"<a class='mr-2' style='display:none' href='/SchoolInfo/SchoolReport/AddEditReportConfig?id={model.PrimaryReportId.EncryptUrl()}&configtype={1.EncryptUrl()}' data-toggle='tooltip'  data-original-title='" + ResourceManager.GetString("Shared.Buttons.Edit") + $"' )'><img src='/Content/vle/img/svg/pencil-big.svg' /></a>";
                    #endif
                }
                actions += $"<a class='mr-2' href='/SchoolInfo/SchoolReport/AddEditReportConfig?id={(model.SecondaryReportId == 0 ? model.PrimaryReportId : model.SecondaryReportId).EncryptUrl()}&configtype={2.EncryptUrl()}' data-toggle='tooltip'  data-original-title='" + ResourceManager.GetString("School.SchoolReport.CreateSchoolReport") + $"' ><img src = '/Content/vle/img/svg/pencil-big.svg' /></ a > ";
                if (model.SecondaryReportId != 0)
                    actions += $"<a class='mr-2' href='javascript:void(0)' onclick='schoolReport.deleteSchoolReport({model.SecondaryReportId})' data-toggle='tooltip'  data-original-title='" + ResourceManager.GetString("Shared.Buttons.Delete") + $"' )'><img src='/Content/vle/img/svg/delete.svg' /></a>";
            }
            return actions;
        }
        public ActionResult AddEditReportConfig(string id = "",string configtype = "")
        {
            //var schoolInfo = (SchoolInformation)Session["SelectedSchoolForSuperAdmin"];
            XtraReport report = new XtraReport();
            var schoolInfo = schoolService.GetSchoolById(SessionHelper.CurrentSession.SchoolId.ToInteger());
            schoolInfo.SchoolImage = getHositingUrl() + "/Uploads/SchoolImages/" + schoolInfo.SchoolImage;
            if (!string.IsNullOrEmpty(id))
            {
                var conType = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(configtype));
                var schoolId = SessionHelper.CurrentSession.SchoolId;
                var reportDetailId = EncryptDecryptHelper.DecryptUrl(id).ToLong();
                var reportDetail = schoolReportService.GetReportDetail(reportDetailId,conType == 1, schoolId, true);
                reportDetail.SchoolInfo = schoolInfo;
                var jsonString = JsonConvert.SerializeObject(new { reportDetail.ColumnFieldsDic, reportDetail.SchoolInfo });
                var jsonDataSource = new JsonDataSource();
                jsonDataSource.JsonSource = new CustomJsonSource(jsonString);
                jsonDataSource.Fill();
                var fileName = PhoenixConfiguration.Instance.WriteFilePath + Constants.SchoolReportsDir + 
                   (conType == 1 ? Constants.SchoolReportsCommonDir : schoolInfo.SchoolShortName) + "/" + reportDetail.SchoolReports.FileName;
                if (System.IO.File.Exists(fileName))
                    report.LoadLayout(fileName);
                else
                {
                    fileName = PhoenixConfiguration.Instance.WriteFilePath + Constants.SchoolReportsDir +  Constants.SchoolReportsCommonDir + "/" + reportDetail.SchoolReports.FileName;
                    if (System.IO.File.Exists(fileName))
                        report.LoadLayout(fileName);
                }
                if (reportDetail.SchoolReports != null)
                {
                    jsonDataSource.Name = reportDetail.SchoolReports.Name;
                    report.DisplayName = reportDetail.SchoolReports.Name;
                    report.Name = reportDetail.SchoolReports.Name;
                }
                
                ViewBag.reportDetailId = reportDetailId;
                ViewBag.ConfigType = conType;
                ViewBag.SchoolInfo = schoolInfo;
                report.DataSource = jsonDataSource;
                
            }            
            return View("Index", report);
        }
        [HttpPost]
        public ActionResult SaveReportDetails(string url, long reportDetail, int configType)
        {
            var schoolReports = new SchoolReports {
                FileName = url + FileExtension,
                ReportDetailId = reportDetail.ToInteger(),
                SchoolId = SessionHelper.CurrentSession.SchoolId,
                CreatedBy = SessionHelper.CurrentSession.Id,
                ConfigType = configType,
                IsReportEdit = true };
            var response = schoolReportService.SchoolReportCU(schoolReports);
            return Json(new OperationDetails(response > 0), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewReport()
        {
            ViewBag.Modules = SelectListHelper.GetSelectListData(ListItems.ReportModule).Select(x => new SelectListItem { Text = x.ItemName, Value = x.ItemId});
            ViewBag.Curriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            return View();
        }
        public JsonResult GetReportDetailsUsingModuleId(int id)
        {
            var response = SelectListHelper.GetSelectListData(ListItems.ReportDetail, $"{id}");
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult GetReportFilters(int reportDetailId)
        {
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            var reportDetail = schoolReportService.GetReportDetail(reportDetailId, true, schoolId);
            ViewBag.IsCrystalReport = reportDetail.SchoolReports.IsCrystalReport;
            return PartialView("_ReportFilters", reportDetail.ReportParameters);
        }
        [HttpPost]
        public JsonResult GetReportFilter(string filterCode, string filterParameter)
        {
            var response = GetReportFilters(filterCode, filterParameter);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetReportFilterControl(string filterCode, string filterParameter, ReportParameter controlCode)
        {
            var response = GetReportFilters(filterCode, filterParameter).ToList();
            ViewBag.ControlData = response;
            var html = ControllerExtension.RenderPartialViewToString(this, "_ReportFilters", new List<ReportParameter> { controlCode });
            return Json(html, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        private IEnumerable<ReportFilterModel> GetReportFilters(string filterCode, string filterParameter)
        {
            filterParameter = "SchoolId:" + SessionHelper.CurrentSession.SchoolId + "|" + filterParameter;
            return schoolReportService.GetReportFilter(filterCode, filterParameter);
        }
        [HttpPost]
        public ActionResult GetReportToView(ReportParameterRequestModel requestModel)
        {
            var schoolInfo = schoolService.GetSchoolById(SessionHelper.CurrentSession.SchoolId.ToInteger());
            schoolInfo.SchoolImage = getHositingUrl() + "/Uploads/SchoolImages/" + schoolInfo.SchoolImage;
            requestModel.SchoolId = SessionHelper.CurrentSession.SchoolId;
            var result = schoolReportService.GetReportData(requestModel);
            result.SchoolInfo = schoolInfo;

            var fileName = PhoenixConfiguration.Instance.WriteFilePath + Constants.SchoolReportsDir + schoolInfo.SchoolShortName + "/" + result.FileName;
            if (!System.IO.File.Exists(fileName))
            {
                fileName = PhoenixConfiguration.Instance.WriteFilePath + Constants.SchoolReportsDir + Constants.SchoolReportsCommonDir + "/" + result.FileName;
            }

            if (requestModel.IsCrystalReport)
            {
                if (result.ColumnFieldsDic.Any(x => ((Newtonsoft.Json.Linq.JArray)x.Value).Count() > 0))
                {
                    List<CustomParameterField> parameterFieldList = new List<CustomParameterField>();
                    var dataSet = new DataSet();
                    var tempDataSet = new DataSet();
                    foreach (var item in result.ColumnFieldsDic)
                    {
                        var dataTable = ((IEnumerable<dynamic>)item.Value).EnumToDataTable(item.Key);
                        dataSet.Tables.Add(dataTable);
                    }

                    foreach (DataTable item in dataSet.Tables)
                    {
                        if (!item.TableName.ToLower().Contains("sub-"))
                            tempDataSet.Tables.Add(item.Copy());
                    }

                    ReportDocumentPersister reportDocPersister = new ReportDocumentPersister(fileName, parameterFieldList, tempDataSet, true);

                    foreach (DataTable item in dataSet.Tables)
                    {
                        if (item.TableName.ToLower().Contains("sub-"))
                        {
                            var subReportName = item.TableName.Split('-')[1];
                            reportDocPersister.SubReports.Add(subReportName, item);
                        }                            
                    }

                    return Json(GetReportViewerPageUrl(reportDocPersister), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
                }
            }
            
            string json = JsonConvert.SerializeObject(new { result.ColumnFieldsDic, result.SchoolInfo });
            var jsonDataSource = new JsonDataSource();
            jsonDataSource.JsonSource = new CustomJsonSource(json);
            // Retrieve data from the JSON data source to the Report Designer\"s Field List
            jsonDataSource.Fill();
            var report = new XtraReport();            
            if (System.IO.File.Exists(fileName))
                report.LoadLayout(fileName);
            report.DataSource = jsonDataSource;            


            return PartialView("_GetReportToView", report);
        }
        public JsonResult DeleteSchoolReport(int id)
        {
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            var schoolInfo = schoolService.GetSchoolById(schoolId.ToInteger());
            var reportDetail = schoolReportService.GetReportDetail(id, false, schoolId, false);
            var fileName = PhoenixConfiguration.Instance.WriteFilePath + Constants.SchoolReportsDir + schoolInfo.SchoolShortName + "/" + reportDetail.SchoolReports.FileName;
            if (System.IO.File.Exists(fileName))
                System.IO.File.Delete(fileName);
            var result = schoolReportService.DeleteSchoolReport(id, schoolId);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        //public ActionResult LoadReportInView(ReportDocumentPersister reportDocumentPersister)
        //{            
        //    if (reportDocumentPersister != null)
        //    {
        //        var _reportDoc = new ReportDocument();
        //        _reportDoc.Load(reportDocumentPersister.ReportPhysicalPath);

        //        if (reportDocumentPersister.SubReports != null)
        //        {
        //            for (int subRptIdx = 0; subRptIdx < reportDocumentPersister.SubReports.Count; subRptIdx++)
        //            {
        //                CustomSubReport subReport = reportDocumentPersister.SubReports[subRptIdx];
        //                if (subReport.DataSource != null)
        //                {
        //                    _reportDoc.Subreports[subReport.SubReportName].SetDataSource(subReport.DataSource);
        //                }
        //            }
        //        }

        //        if (reportDocumentPersister.IsMultipleTables)
        //        {
        //            int k = 0;
        //            foreach (DataTable dt in reportDocumentPersister.TableCollections.Tables)
        //            {
        //                _reportDoc.Database.Tables[k].SetDataSource(dt);
        //                k++;
        //            }
        //        }

        //        else if (reportDocumentPersister.DataSource != null)
        //        {
        //            _reportDoc.SetDataSource(reportDocumentPersister.DataSource);
        //        }

        //        if (reportDocumentPersister.ParameterFieldList != null)
        //        {
        //            foreach (CustomParameterField paramField in reportDocumentPersister.ParameterFieldList)
        //            {
        //                if (paramField.ParameterValue != null)
        //                {
        //                    if (IsReportParameterExists(_reportDoc, paramField.ParameterName))
        //                    {
        //                        _reportDoc.SetParameterValue(paramField.ParameterName, paramField.ParameterValue.ToString());
        //                    }
        //                }
        //            }
        //        }
        //        CrystalDecisions.Shared.ExportOptions CrExportOptions;
        //        CrExportOptions = _reportDoc.ExportOptions;
        //        {
        //            CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            CrExportOptions.DestinationOptions = reportDocumentPersister.ExportFileDestinationOptions;
        //            CrExportOptions.FormatOptions = new PdfRtfWordFormatOptions();

        //        }
        //        _content = StreamToBytes(_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat));
        //        _reportDoc.Close();
        //        _reportDoc.Dispose();
        //        GC.Collect();
        //    }
        //    return File(_content, "application/pdf");
        //}
        //private static byte[] StreamToBytes(Stream input)
        //{
        //    byte[] buffer = new byte[16 * 1024];
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        int read;
        //        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        //        {
        //            ms.Write(buffer, 0, read);
        //        }
        //        return ms.ToArray();
        //    }
        //}
        //public bool IsReportParameterExists(ReportDocument reportDoc, string parameterName)
        //{
        //    bool result = false;
        //    foreach (ParameterFieldDefinition param in reportDoc.DataDefinition.ParameterFields)
        //    {
        //        if (param.ParameterFieldName.Equals(parameterName, StringComparison.OrdinalIgnoreCase))
        //        {
        //            result = true;
        //            break;
        //        }
        //    }
        //    return result;
        //}
        public string GetReportViewerPageUrl(ReportDocumentPersister reportDocPersister)
        {
            string key = Guid.NewGuid().ToString();
            Cache cacheObj = System.Web.HttpContext.Current.Cache;

            try
            {
                cacheObj.Remove(key);
            }
            catch
            {
            }

            cacheObj.Insert(key, reportDocPersister, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(3));
            return "/ReportViewer/ReportViewer.aspx?r=" + key;
        }
    }
}