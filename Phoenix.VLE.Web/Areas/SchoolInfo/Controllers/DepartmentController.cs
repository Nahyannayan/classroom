﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class DepartmentController : Controller
    {
        private ISchoolService schoolService;
        public DepartmentController(ISchoolService _schoolService)
        {
            schoolService = _schoolService;
        }
        // GET: SchoolInfo/Department
        public ActionResult Index()
        {
            ViewBag.Curriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            return View();
        }
        public ActionResult InitAddEditDepartmentForm(int? id = 0, long curriculumId = 0)
        {
            var model = new Department();
            var DepartmentId = id > 0 ? id : null;
            ViewBag.CourseList = new SelectList(SelectListHelper.GetSelectListData(ListItems.DepartmentCourse, $"{SessionHelper.CurrentSession.SchoolId},{DepartmentId}"), "ItemId", "ItemName");
            if (id > 0)
            {
                var result = schoolService.GetDepartmentCourse(SessionHelper.CurrentSession.SchoolId, curriculumId, id).ToList();
                if (result.Any())
                {
                    model = result.FirstOrDefault();
                }
            }
            return PartialView("_AddEditDepartment", model);
        }
        public PartialViewResult GetDepartmentDetail(long curriculumId)
        {
            IEnumerable<Department> list = new List<Department>();
            var result = schoolService.GetDepartmentCourse(SessionHelper.CurrentSession.SchoolId, curriculumId, null);
            list = result ?? result;
            return PartialView("_DepartmentGrid", list);
        }
        public ActionResult SaveDepartmentData(Department department)
        {
            department.SchoolId = SessionHelper.CurrentSession.SchoolId;
            department.DATAMODE = "ADD";
            bool result = false;
            result = schoolService.DepartmentCourseCU(department);
            OperationDetails op = new OperationDetails(result);
            if (result)
            {
                op.Message = department.DepartmentId > 0 ? LocalizationHelper.UpdateSuccessMessage : LocalizationHelper.AddSuccessMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteDepartment(long DepartmentId = 0)
        {
            Department department = new Department();
            department.SchoolId = SessionHelper.CurrentSession.SchoolId;
            department.DepartmentId = DepartmentId;
            department.DATAMODE = "DELETE";
            bool result = false;
            result = schoolService.DepartmentCourseCU(department);
            OperationDetails op = new OperationDetails(result);
            if (result)
            {
                op.Message = LocalizationHelper.DeleteSuccessMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CanDeleteDepartment(long departmentId)
        {
            var result = schoolService.CanDeleteDepartment(departmentId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}