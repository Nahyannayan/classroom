﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.EditModels;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Helpers;
using Phoenix.Common;
using System.IO;
using Phoenix.Common.Enums;
using Phoenix.VLE.Web.ViewModels;
using File = Phoenix.Models.File;
using System.Web.Script.Serialization;
using System.Data;
using Phoenix.Common.Localization;
using Phoenix.Common.ViewModels;
using System.Text;
using System.Text.RegularExpressions;
using Phoenix.VLE.Web.Helpers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;
using Google.Apis.Auth.OAuth2.Mvc;
using System.Configuration;
using System.Net.Http;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class BlogController : BaseController
    {
        private readonly IBlogService _blogService;
        private readonly IBlogCommentService _blogCommentService;
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly IFileService _fileService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly IStudentListService _studentListService;
        private readonly ICommonService _commonService;
        private readonly IStudentService _studentService;
        private readonly IChatService _chatService;
        private List<FileManagementModule> _fileModuleList;
        private List<VLEFileType> _fileTypeList;

        public BlogController(IBlogService blogService,
            IBlogCommentService blogCommentService,
            ISchoolGroupService schoolGroupService,
            IFileService fileService,
            IUserPermissionService userPermissionService,
            IStudentListService studentListService,
            ICommonService commonService, IStudentService studentService, IChatService chatService)
        {
            _blogService = blogService;
            _blogCommentService = blogCommentService;
            _schoolGroupService = schoolGroupService;
            _fileService = fileService;
            _userPermissionService = userPermissionService;
            _studentListService = studentListService;
            _commonService = commonService;
            _studentService = studentService;
            _chatService = chatService;
            _fileModuleList = _fileService.GetFileManagementModules().ToList();
            _fileTypeList = _fileService.GetFileTypes().ToList();

        }

        // GET: School/Blog
        public ActionResult Index()
        {
            return View();
        }

        #region Blogs
        public ActionResult LoadBlogGrid()
        {
            var blogs = _blogService.GetBlogsBySchoolId((int)SessionHelper.CurrentSession.SchoolId, null);
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in blogs
                          select new
                          {
                              Actions = HtmlHelperExtensions.LocalizedEditDeleteLinkButtons((int)item.BlogId, "Blog", "blog"),
                              Publish = HtmlHelperExtensions.LocalizedLinkButtons($"<a onclick='blog.publishBlogData($(this),{item.BlogId})' href='javascript:void(0)' class='table-action-icon-btn' style='display:{(item.IsPublish ? "none" : "inline-block")}' title='" + ResourceManager.GetString("School.Blog.Publish") + "'><i class='fas fa-cloud-upload-alt'></i></a> <a onclick='blog.unPublishBlogData($(this),{item.BlogId})' href='javascript:void(0)' style='display:" + (item.IsPublish ? "inline-block" : "none") + "' class='table-action-icon-btn' title='" + ResourceManager.GetString("School.Blog.Unpublish") + "'><i class='fas fa-cloud-download-alt'></i></a> &nbsp; &nbsp;<a target='_blank' href='/schoolinfo/blog/viewblog?bgId={EncryptDecryptHelper.EncryptUrl(item.BlogId.ToString())}' class='table-action-icon-btn' title='" + ResourceManager.GetString("School.Blog.View") + "'><i class='fas fa-eye'></i><a>", true),
                              item.Title,
                              Description = !string.IsNullOrWhiteSpace(item.Description) && item.Description.Length > 100 ? HttpUtility.HtmlDecode(item.Description.Substring(0, 100)) : HttpUtility.HtmlDecode(item.Description),
                              BlogImage = !string.IsNullOrWhiteSpace(item.BlogImage) ? $"<img src='{PhoenixConfiguration.Instance.WriteFilePath + Url.Content(item.BlogImage)}' class='img-thumbnail' width='200' height='40' />" : item.BlogImage,
                              Author = item.CreatedByName,
                              CreatedOn = item.CreatedOn.ToString("MMMM dd yyyy")
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitAddEditBlogForm(int? id)
        {
            var model = new BlogEdit();
            if (id.HasValue)
            {
                var blog = _blogService.Get(id.Value);
                EntityMapper<Blog, BlogEdit>.Map(blog, model);

            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = SessionHelper.CurrentSession.SchoolId;
            }
            model.CategoryList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectCategory"), Value = "" });
            model.CategoryList.AddRange(_blogService.GetBlogCategories(SessionHelper.CurrentSession.SchoolId)
                .Select(r => new SelectListItem { Text = r.Category, Value = r.CategoryId.ToString() }));

            model.BlogTypeList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectType"), Value = "" });
            model.BlogTypeList.AddRange(_blogService.GetBlogTypes(null, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId)
                .Select(r => r.BlogTypeId == 1 ? new SelectListItem { Text = r.BlogType, Value = r.BlogTypeId.ToString(), Selected = true }
                : new SelectListItem { Text = r.BlogType, Value = r.BlogTypeId.ToString() }));

            model.GroupList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectGroup"), Value = "" });
            model.GroupList.AddRange(_schoolGroupService.GetSchoolGroupsBySchoolId((int)SessionHelper.CurrentSession.SchoolId)
                .Select(r => new SelectListItem { Text = $"{r.SchoolGroupName} - {r.SubjectCode}", Value = r.SchoolGroupId.ToString() }).OrderBy(r => r.Text));

            model.EnableCommentForList.AddRange(Enum.GetValues(typeof(EnableCommentForCodes)).Cast<EnableCommentForCodes>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }));


            return PartialView("_AddEditBlog", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveBlogData(BlogEdit model)
        {
            //convert arabic dates
            model.FromDate = model.strFromDate.ToSystemReadableDate();
            model.ToDate = model.strToDate.ToSystemReadableDate();
            model.ScheduledTime = model.ScheduledTime.ToSystemReadableTime();
            bool result = false;
            var blog = new Blog();
            model.CreatedOn = (DateTime)DateTime.Now.ConvertLocalToUTC();
            model.FromDate = (DateTime)Convert.ToDateTime(model.FromDate?.ToString("yyyy-MM-dd") + " " + model.ScheduledTime).ConvertLocalToUTC();
            model.ToDate = (DateTime)Convert.ToDateTime(model.ToDate?.ToString("yyyy-MM-dd") + " " + "12:00 PM").ConvertLocalToUTC();
            model.ScheduledTime = ((DateTime)Convert.ToDateTime(model.ScheduledTime).ConvertLocalToUTC()).ToString("HH:mm");
            //Convert local time to UTC

            // model.FromDate = model.FromDate.Value.ConvertLocalToUTC();


            EntityMapper<BlogEdit, Blog>.Map(model, blog);
            string fileName = string.Empty;
            string relativePath = string.Empty;
            List<string> lstFile = new List<string>();
            var fileEditList = new List<FileEdit>();
            var fileEditList1 = new List<FileEdit>();
            long blogId = 0;



            blog.CloseDiscussionDate = blog.CloseDiscussionDate == DateTime.MinValue ? null : blog.CloseDiscussionDate;
            //-- Blog update

            #region To check if new blog image uploaded and remove previous image
            if (model.PostedImage != null)
            {

                fileName = Guid.NewGuid() + "_" + model.PostedImage.FileName;
                string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                string blogContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.BlogDir;
                string blogPath = PhoenixConfiguration.Instance.WriteFilePath + Constants.BlogDir + "/Blog_" + model.BlogTypeId;
                Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                Common.Helpers.CommonHelper.CreateDestinationFolder(blogContent);
                Common.Helpers.CommonHelper.CreateDestinationFolder(blogPath);
                relativePath = Constants.BlogDir + "/Blog_" + model.BlogTypeId + "/" + fileName;
                fileName = Path.Combine(blogPath, fileName);
                model.PostedImage.SaveAs(fileName);
                blog.BlogImage = relativePath;
            }

            #endregion

            //  bool result = false;
            if (model.BlogId > 0)
            {
                blog.BlogImage = blog.BlogImage ?? String.Empty;
                blog.UpdatedBy = SessionHelper.CurrentSession.Id;
                result = _blogService.Update(blog);
                blogId = model.BlogId;
            }
            else
            {
                blog.CreatedBy = SessionHelper.CurrentSession.Id;
                blogId = _blogService.Insert(blog);
                model.BlogId = blogId;
                result = true;
            }

            if (blogId > 0)
            {
                result = true;
                HttpPostedFileBase file = null;
                var files = new File();
                int extId = 0;
                var isExist = false;
                try
                {
                    var module = _fileService.GetFileManagementModules().FirstOrDefault(r => r.ModuleName.Contains("Chatter"));
                    var deleteFiles = Request.Form["deleteFiles"].ToString().Split(';');
                    for (int i = 0; i < Request.Files.Count; i++)
                    {

                        file = Request.Files[i];
                        string fname = file.FileName;
                        if (model.PostedImage != null)
                        {
                            if (fname == model.PostedImage.FileName)
                            {
                                if (isExist == true)
                                    isExist = false;
                                else
                                    isExist = true;
                            }
                        }

                        var str = fname.Split('_'); var fileDleteName = str[0];
                        if (isExist == false)
                        {
                            if (!deleteFiles.Contains(fileDleteName))
                            {
                                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                                {
                                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                    fname = testfiles[testfiles.Length - 1];
                                }
                                else
                                {
                                    fname = file.FileName;
                                }
                                var extension = Path.GetExtension(file.FileName).Replace(".", "");
                                extId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;
                                double fileSizeInMb = (file.ContentLength / 1024f) / 1024f;
                                fileEditList.Add(new FileEdit() { FileName = fname, PostedFile = file, ResourceFileTypeId = (short)ResourceFileTypes.SharePoint, FileTypeId = extId, FileSizeInMB = fileSizeInMb });
                                fileEditList1 = new List<FileEdit>();
                                fileEditList1.Add(fileEditList[fileEditList.Count - 1]);
                                List<FileEdit> fileListWithPath = new List<FileEdit>();
                                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                                {
                                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                                    fileListWithPath = await azureHelper.UploadFilesListAsPerModuleAsync(FileModulesConstants.Blog, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList);
                                }
                                else
                                {
                                    SharePointHelper sh = new SharePointHelper();
                                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                                    fileListWithPath = SharePointHelper.UploadFilesListAsPerModule(ref cx, FileModulesConstants.Blog, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList1);
                                }

                                if (fileListWithPath.Count > 0)
                                {
                                    //lstFile.Add(fileListWithPath[0].FilePath);
                                    //lstFile.Add(fileListWithPath[0].FileName);
                                    files.FileName = file.FileName;
                                    files.FilePath = fileListWithPath[0].FilePath;
                                    files.FileTypeId = extId;
                                    files.FolderId = 0;
                                    files.IsActive = true;
                                    files.ModuleId = module.ModuleId;
                                    files.SectionId = model.BlogId;
                                    files.SchoolId = SessionHelper.CurrentSession.SchoolId;
                                    files.CreatedBy = SessionHelper.CurrentSession.Id;
                                    files.PhysicalFilePath = fileListWithPath[0].PhysicalFilePath;
                                    files.ResourceFileTypeId = fileListWithPath[0].ResourceFileTypeId;
                                    _fileService.Insert(files);

                                }
                            }
                        }


                    }


                    if (Session["ListCloudFile"] != null)
                    {
                        BlogCloudFile cloudFile = null;
                        List<BlogCloudFile> cloudFiles = (List<BlogCloudFile>)Session["ListCloudFile"];

                        cloudFiles = cloudFiles.DistinctBy(m => m.FileName).Where(x => x.BlogCloudId == 0).ToList();
                        IList<File> saveFiles = new List<File>();

                        for (int index = 0; index < cloudFiles.Count; index++)
                        {
                            cloudFile = cloudFiles[index];
                            var saveFile = new File();
                            if (string.IsNullOrEmpty(cloudFile.FileExtension))
                                cloudFile.FileExtension = Path.GetExtension(cloudFile.FileName).Replace(".", "");
                            else if (cloudFile.FileExtension.Equals("onenote", StringComparison.OrdinalIgnoreCase))
                                cloudFile.FileExtension = "one";

                            if (cloudFile.ResourceFileTypeId == (short)ResourceFileTypes.OneDrive)
                            {
                                var response = await CloudFilesHelper.CreateOneDriveSharingLink(cloudFile.CloudFileId);
                                var data = (JObject)JsonConvert.DeserializeObject(response);

                                saveFile.FilePath = (string)data["link"]["webUrl"];
                            }


                            else if (cloudFile.ResourceFileTypeId == (short)ResourceFileTypes.GoogleDrive)
                            {
                                CancellationToken cancellationToken = new CancellationToken();
                                var authResult = new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).
                                   AuthorizeAsync(cancellationToken).GetAwaiter().GetResult();
                                CloudFilesHelper.AddGoogleDriveUserPermission(authResult, cloudFile.CloudFileId, "reader");
                                saveFile.FilePath = cloudFile.UploadedFileName;
                            }
                            else if (cloudFile.ResourceFileTypeId == (short)ResourceFileTypes.OneNote)
                            {
                                await CloudFilesHelper.AssignNotebookToStudent(string.Empty, cloudFile.CloudFileId);
                                saveFile.FilePath = cloudFile.UploadedFileName;
                            }
                            saveFile.GoogleDriveFileId = cloudFile.CloudFileId;
                            saveFile.CreatedBy = SessionHelper.CurrentSession.Id;
                            saveFile.SectionId = blogId;
                            saveFile.FolderId = 0;
                            saveFile.FileName = cloudFile.FileName;
                            saveFile.FileTypeId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(cloudFile.FileExtension.ToLower())).TypeId;
                            saveFile.ResourceFileTypeId = cloudFile.ResourceFileTypeId;
                            saveFile.ModuleId = module.ModuleId;

                            saveFiles.Add(saveFile);
                        }

                        result = _fileService.SaveCloudFiles(saveFiles);
                    }




                }
                catch (Exception ex)
                {
                    var d = ex.Message.ToString();
                    return Json("Upload failed");
                }

            }

            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PublishBlog(int id)
        {
            var result = _blogService.PublishBlogById(id, true, (int)SessionHelper.CurrentSession.Id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UnPublishBlog(int id)
        {
            var result = _blogService.PublishBlogById(id, false, (int)SessionHelper.CurrentSession.Id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteBlogData(int id)
        {
            var result = _blogService.Delete(id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteBlogAttachedFile(int fileId)
        {
            var result = _fileService.Delete(fileId, SessionHelper.CurrentSession.Id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewBlog(string bgId)
        {
            var blogId = !string.IsNullOrWhiteSpace(bgId) ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(bgId)) : 0;
            var blog = _blogService.Get(blogId);
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            Session["CanEdit"] = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Chatter.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            return View(blog);
        }

        [HttpGet]
        public ActionResult BlogDetail(int bgId)
        {
            var blog = _blogService.Get(bgId);
            return PartialView("_BlogDetail", blog);
        }

        [HttpPost]
        public ActionResult UpdateDiscloseDate(int id, int groupId)
        {
            var result = _blogService.UpdateDiscloseDate(id);

            var model = GetChatterModel(groupId);
            return PartialView("_YoYoBlogList", model);

        }

        [HttpPost]
        public ActionResult ViewPostCommentUserList(long BlogId, long CommentSectioId, bool IsCommentDetailDisplay)
        {
            var result = _blogService.ViewPostCommentUserList(Convert.ToInt64(BlogId), Convert.ToInt64(CommentSectioId), IsCommentDetailDisplay, SessionHelper.CurrentSession.Id);
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_PostAndCommentLikedUserList", result);
            return Json(htmlContent, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Blog Comments
        public ActionResult GetBlogComments(int blogId)
        {
            bool? publish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
            var blogComments = _blogCommentService.GetBlogCommentByBlogId(blogId, publish);
            return PartialView("_ViewBlogComments", blogComments);
        }

        [HttpPost]
        public ActionResult PublishComment(int id)
        {
            BlogComment blogComment = new BlogComment(id);
            blogComment.IsPublish = true;
            blogComment.UpdatedBy = SessionHelper.CurrentSession.Id;
            blogComment.PublishBy = SessionHelper.CurrentSession.Id;
            var result = _blogCommentService.Update(blogComment);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UnPublishComment(int id)
        {
            BlogComment blogComment = new BlogComment(id);
            blogComment.IsPublish = false;
            blogComment.UpdatedBy = SessionHelper.CurrentSession.Id;
            blogComment.PublishBy = SessionHelper.CurrentSession.Id;
            var result = _blogCommentService.Update(blogComment);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }


        public ActionResult DeleteComment(int id)
        {
            var result = _blogCommentService.Delete(id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteBlog(int id)
        {
            var result = _blogService.Delete(id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> SaveComment(BlogCommentEdit blogCommentEdit)
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            bool result = false;
            var blogComment = new BlogComment();
            EntityMapper<BlogCommentEdit, BlogComment>.Map(blogCommentEdit, blogComment);
            bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
            // blogComment.Comment= Phoenix.VLE.Web.Helpers.CommonHelper.GetEmbeddedVideoLink(blogComment.Comment);
            int? LogedInUserId = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var blogChatter = _blogService.GetMyWallDataOnSearch(Convert.ToInt64(blogCommentEdit.BlogId), (int)SessionHelper.CurrentSession.SchoolId, null, null, null, userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId);
            var blog = blogChatter?.FirstOrDefault()?.Blog;
            bool isAllow = blog != null;

            if (!isteacher && isAllow)
                isAllow = ((blog.EnableCommentById == SessionHelper.CurrentSession.UserTypeId)
                    || blog.EnableCommentById == 3);
            else
                isAllow = true;
            if (isAllow)
            {
                if (!isteacher)
                {

                    blogComment.IsPublish = !(blog.CommentApprovedById == SessionHelper.CurrentSession.UserTypeId || blog.CommentApprovedById == 3);
                }

                //blogComment.IsPublish = true;
                //blogComment.UpdatedBy = SessionHelper.CurrentSession.Id;
                //blogComment.PublishBy = SessionHelper.CurrentSession.Id;


                if (blogCommentEdit.CommentId > 0)
                {
                    blogComment.UpdatedBy = SessionHelper.CurrentSession.Id;
                    result = _blogCommentService.Update(blogComment);
                }
                else
                {
                    blogComment.CreatedBy = SessionHelper.CurrentSession.Id;
                    blogCommentEdit.CommentId = _blogCommentService.Insert(blogComment);
                    result = true;

                    if (Request.Files.Count > 0)
                    {
                        List<string> lstFile = new List<string>();
                        string fileName = string.Empty;
                        //string relativePath = string.Empty;
                        var fileEditList = new List<FileEdit>();
                        var files = new File();
                        HttpPostedFileBase file = null;
                        int extId = 0;
                        var module = _fileService.GetFileManagementModules().FirstOrDefault(r => r.ModuleName.Contains("ChatterComment"));

                        try
                        {
                            for (int i = 0; i < Request.Files.Count; i++)
                            {
                                file = Request.Files[i];
                                string fname = file.FileName;
                                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                                {
                                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                    fname = testfiles[testfiles.Length - 1];
                                }
                                else
                                {
                                    fname = file.FileName;
                                }
                                var extension = Path.GetExtension(file.FileName).Replace(".", "");
                                extId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;
                                double fileSizeInMb = (file.ContentLength / 1024f) / 1024f;
                                fileEditList.Add(new FileEdit() { FileName = fname, PostedFile = file, ResourceFileTypeId = (short)ResourceFileTypes.SharePoint, FileTypeId = extId, FileSizeInMB = fileSizeInMb });
                            }
                            List<FileEdit> fileListWithPath = new List<FileEdit>();
                            if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                            {
                                AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                                await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                                fileListWithPath = await azureHelper.UploadFilesListAsPerModuleAsync(FileModulesConstants.BlogComment, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList);
                            }
                            else
                            {
                                SharePointHelper sh = new SharePointHelper();
                                Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                                // uploading file to the sharepoint
                                fileListWithPath = SharePointHelper.UploadFilesListAsPerModule(ref cx, FileModulesConstants.BlogComment, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList);
                            }

                            if (fileListWithPath.Count > 0)
                            {
                                files.FileName = file.FileName;
                                files.FilePath = fileListWithPath[0].FilePath;
                                files.FileTypeId = extId;
                                files.FolderId = 0;
                                files.IsActive = true;
                                files.ModuleId = module.ModuleId;
                                files.SectionId = blogCommentEdit.CommentId;
                                files.SchoolId = SessionHelper.CurrentSession.SchoolId;
                                files.CreatedBy = SessionHelper.CurrentSession.Id;
                                files.ResourceFileTypeId = fileListWithPath[0].ResourceFileTypeId;
                                files.PhysicalFilePath = fileListWithPath[0].PhysicalFilePath;
                                _fileService.Insert(files);
                            }
                        }
                        catch (Exception ex)
                        {
                            var d = ex.Message.ToString();
                            return Json("Upload failed");
                        }
                    }
                }
            }
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Chatter Blogs
        public ActionResult Chatters(string grpId)
        {
            var model = new YoYoBlogViewModel();
            var fileTypes = _fileService.GetFileTypes();

            int id = !string.IsNullOrWhiteSpace(grpId) ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(grpId)) : 0;
            if (id != 0)
            {
                model.SchoolGroupId = id;
            }
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;

            var groupList = _schoolGroupService.GetSchoolGroupsByUserId(userId, SessionHelper.CurrentSession.IsTeacher());
            model.GroupsList.AddRange(groupList);

            var groupId = groupList.Any() && groupList.Count() > 0 && id == 0 ? (int)groupList.First().SchoolGroupId : id;


            model.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            model.AllowedFileExtension.AddRange(fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList());
            // model.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            //model.AllowedImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();
            model.Chatters = GetChatterModel(groupId);

            //To add new blog
            model.BlogEdit.IsAddMode = true;
            model.BlogEdit.SchoolId = SessionHelper.CurrentSession.SchoolId;

            model.BlogEdit.CategoryList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectCategory"), Value = "" });
            model.BlogEdit.CategoryList.AddRange(_blogService.GetBlogCategories(SessionHelper.CurrentSession.SchoolId)
                .Select(r => new SelectListItem { Text = r.Category, Value = r.CategoryId.ToString() }));

            model.BlogEdit.BlogTypeList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectType"), Value = "" });
            model.BlogEdit.BlogTypeList.AddRange(_blogService.GetBlogTypes(null)
                .Select(r => new SelectListItem { Text = r.BlogType, Value = r.BlogTypeId.ToString() }).OrderBy(r => r.Text));

            model.BlogEdit.GroupList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectGroup"), Value = "" });
            model.BlogEdit.GroupList.AddRange(groupList.Select(r => new SelectListItem { Text = r.SchoolGroupName, Value = r.SchoolGroupId.ToString() }));

            model.BlogEdit.EnableCommentForList.AddRange(Enum.GetValues(typeof(EnableCommentForCodes)).Cast<EnableCommentForCodes>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }));
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            Session["CanEdit"] = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Chatter.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            return View(model);
        }

        [HttpPost]
        public ActionResult GetBlogListByGroupId(int groupId)
        {
            var model = GetChatterModel(groupId);

            return PartialView("_YoYoBlogList", model);
        }

        private List<Chatter> GetChatterModel(int groupId)
        {
            var model = new List<Chatter>();
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;


            if (SessionHelper.CurrentSession.IsAdmin)
            {

                model.AddRange(_blogService.GetChatterWithComments(null, (int)SessionHelper.CurrentSession.SchoolId, null, groupId, null, SessionHelper.CurrentSession.IsTeacher(), null, null, null, null));
            }
            else
            {

                bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
                model.AddRange(_blogService.GetChatterWithComments(null, (int)SessionHelper.CurrentSession.SchoolId, null, groupId, (int)userId, SessionHelper.CurrentSession.IsTeacher(), isCommentPublish, null, null, isCommentPublish));
            }

            return model;
        }

        public ActionResult EditYoYoBlogForm(int? id)
        {

            var model = new BlogEdit();

            if (id.HasValue)
            {
                var blog = _blogService.Get(id.Value);
                EntityMapper<Blog, BlogEdit>.Map(blog, model);

            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = SessionHelper.CurrentSession.SchoolId;
            }
            model.CategoryList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectCategory"), Value = "" });
            model.CategoryList.AddRange(_blogService.GetBlogCategories(SessionHelper.CurrentSession.SchoolId)
                .Select(r => new SelectListItem { Text = r.Category, Value = r.CategoryId.ToString() }));

            model.BlogTypeList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectType"), Value = "" });
            model.BlogTypeList.AddRange(_blogService.GetBlogTypes(null)
                .Select(r => new SelectListItem { Text = r.BlogType, Value = r.BlogTypeId.ToString() }).OrderBy(r => r.Text));

            model.EnableCommentForList.AddRange(Enum.GetValues(typeof(EnableCommentForCodes)).Cast<EnableCommentForCodes>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }));


            return PartialView("_AddEditYoYoBlog", model);
        }

        [HttpPost]
        public ActionResult LikeBlog(int id, bool isLike)
        {
            var blogLike = new BlogLike();
            blogLike.BlogId = id;
            blogLike.IsLike = isLike;
            blogLike.LikeTypeId = 1;
            blogLike.LikedBy = SessionHelper.CurrentSession.Id;

            var result = _blogService.InsertLike(blogLike);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LikeComment(int id, long blogId, bool isLike)
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
            int? LogedInUserId = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var blogChatter = _blogService.GetMyWallDataOnSearch(Convert.ToInt64(blogId), (int)SessionHelper.CurrentSession.SchoolId, null, null, null, userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId);
            var blogDetail = blogChatter?.FirstOrDefault()?.Blog;
            bool isAllow = blogDetail != null;
            if (!isteacher && isAllow)
                isAllow = ((blogDetail.EnableCommentById == SessionHelper.CurrentSession.UserTypeId) || blogDetail.EnableCommentById == 3 || blogDetail.IsPublish);
            //isAllow = ((blogDetail.EnableCommentById == SessionHelper.CurrentSession.UserTypeId) || blogDetail.EnableCommentById == 3);
            else
                isAllow = true;
            var result = false;
            if (isAllow)
            {
                var blogCommentLike = new BlogCommentLike();
                blogCommentLike.CommentId = id;
                blogCommentLike.IsLike = isLike;
                blogCommentLike.LikeTypeId = 1;
                blogCommentLike.LikedBy = SessionHelper.CurrentSession.Id;
                result = _blogCommentService.InsertCommentLike(blogCommentLike);
            }
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }


        public bool GetBlogLikeStatusForUser(int id)
        {
            var result = false;
            var blogLike = _blogService.GetBlogLikes(id, null, (int)SessionHelper.CurrentSession.Id, null).FirstOrDefault();
            if (blogLike != null)
            {
                result = blogLike.IsLike;
            }

            return result;
        }


        public bool GetCommentLikeStatusForUser(int id)
        {
            var result = false;
            var blogCommentLike = _blogCommentService.GetBlogCommentLikes(id, (int)SessionHelper.CurrentSession.Id).FirstOrDefault();
            if (blogCommentLike != null)
            {
                result = blogCommentLike.IsLike;
            }

            return result;
        }


        public ActionResult GetYoYoBlogComments(int blogId)
        {
            bool? publish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
            var blogComments = _blogCommentService.GetBlogCommentByBlogId(blogId, publish);
            blogComments = blogComments.Take(3);

            return PartialView("_YoYoBlogComment", blogComments);
        }

        #endregion

        #region Exemplar Wall
        public ActionResult Exemplar()
        {
            //New Code for Exemplar /*Deependra*/

            var studentList = _studentService.GetAll();

            //var groyp = _schoolGroupService.GetSchoolGroups();


            var model = new YoYoBlogViewModel();
            var fileTypes = _fileService.GetFileTypes();

            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var groupList = _schoolGroupService.GetSchoolGroupsByUserId(userId, SessionHelper.CurrentSession.IsTeacher());

            model.AllowedFileExtension = fileTypes.Select(r => r.Extension).ToList();

            model.GroupsList.AddRange(groupList);
            model.StudentList.AddRange(studentList);
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            Session["CanEdit"] = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ExemplarWall.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            return View(model);
        }
        public ActionResult GetExemplarListByGroupId(int groupId)
        {
            var model = new List<Blog>();
            model.AddRange(_blogService.GetBlogsByGroupNBlotTypeId(groupId, 3, true).OrderBy(r => r.SortOrder));

            return PartialView("_ExemplarList", model);
        }

        [HttpPost]
        public ActionResult SaveExemplarDataWithMap(YoYoBlogViewModel model)
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveExemplarData(BlogEdit model)
        {
            //AddSchoolGroupAndStudentMapping
            var blog = new Blog();
            var GroupStudentMap = new SchoolGroup_StudentMapping();
            EntityMapper<BlogEdit, Blog>.Map(model, blog);
            if (string.IsNullOrWhiteSpace(model.Title))
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }

            blog.BlogTypeId = 3;
            blog.SchoolId = SessionHelper.CurrentSession.SchoolId;
            blog.CloseDiscussionDate = null;
            string fileName = string.Empty;
            string relativePath = string.Empty;
            if (model.PostedImage != null)
            {
                fileName = Guid.NewGuid() + "_" + model.PostedImage.FileName;

                string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                string blogContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.BlogDir;
                string blogPath = PhoenixConfiguration.Instance.WriteFilePath + Constants.BlogDir + "/Blog_" + model.BlogTypeId;
                Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                Common.Helpers.CommonHelper.CreateDestinationFolder(blogContent);
                Common.Helpers.CommonHelper.CreateDestinationFolder(blogPath);
                relativePath = Constants.BlogDir + "/Blog_" + model.BlogTypeId + "/" + fileName;

                fileName = Path.Combine(blogPath, fileName);

                model.PostedImage.SaveAs(fileName);
                blog.BlogImage = relativePath;
            }
            bool result = false;
            if (model.BlogId > 0)
            {
                blog.UpdatedBy = SessionHelper.CurrentSession.Id;
                result = _blogService.Update(blog);
            }
            else
            {
                blog.CreatedBy = SessionHelper.CurrentSession.Id;
                var blogId = _blogService.Insert(blog);
                var mapId = _blogService.InsertMap(GroupStudentMap);
                model.BlogId = blogId;
                result = true;
            }
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditExemplarForm(int? id)
        {
            var model = new BlogEdit();
            if (id.HasValue)
            {
                var blog = _blogService.Get(id.Value);
                EntityMapper<Blog, BlogEdit>.Map(blog, model);

            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = SessionHelper.CurrentSession.SchoolId;
            }
            model.BlogTypeId = 3;

            return PartialView("_AddEditExemplar", model);
        }
        //public ActionResult DeleteExemplar(int id)
        //{
        //    var model = new Blog();
        //    var model = new Blog();
        //    model.BlogId = id;
        //    model.UpdatedBy = SessionHelper.CurrentSession.Id;
        //    var response = _blogService.DeleteExemplar(model) > 0;
        //    return Json(new OperationDetails(response), JsonRequestBehavior.AllowGet);

        //}
        public ActionResult UpdateWallSortOrder(string Ids)
        {
            var BlogIds = new JavaScriptSerializer().Deserialize<int[]>(Ids);
            DataTable dt = new DataTable();
            dt.Columns.Add("BlogId");
            dt.Columns.Add("SortOrder");
            int i = 1;
            foreach (int item in BlogIds)
            {
                DataRow dr = dt.NewRow();
                dr["BlogId"] = item;
                dr["SortOrder"] = i;
                dt.Rows.Add(dr);
                i++;
            }
            var model = new ExemplarWallOrder();
            model.dataTable = dt;
            model.UpdatedBy = SessionHelper.CurrentSession.Id;
            var result = _blogService.UpdateWallSortOrder(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region MyWall
        public ActionResult ViewMyWall(int id = 0)
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            var model = new YoYoBlogViewModel();
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            model.GroupsList = _schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher).ToList();
            var groupList = _schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher);
            var groupId = groupList.Any() && groupList.Count() > 0 && id == 0 ? (int)groupList.First().SchoolGroupId : id;

            bool enableChatter = Boolean.Parse(ConfigurationManager.AppSettings["enableChatter"]);
            model.RecentUserActivity = _chatService.GetRecentUserActivity((int)SessionHelper.CurrentSession.SchoolId, Convert.ToInt32(userId), enableChatter, 0).ToList();

            //model.MyContactsList = _chatService.GetMyContactList(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), Convert.ToInt32(userId)).ToList();

            if (enableChatter)
            {
                model.MySearchContactsList = _chatService.GetSearchInChat(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), "", SessionHelper.CurrentSession.UserTypeId, Convert.ToInt32(userId)).Take(100).ToList();
            }

            // model.Chatters = GetWallModel();

            //model.BlogEdit.CategoryList.AddRange(_blogService.GetBlogCategories(SessionHelper.CurrentSession.SchoolId)
            //    .Select(r => new SelectListItem { Text = r.Category, Value = r.CategoryId.ToString() }));
            //model.BlogEdit.SchoolId = SessionHelper.CurrentSession.SchoolId;

            var fileTypes = _fileService.GetFileTypes();
            model.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            model.AllowedFileExtension.AddRange(fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList());

            //model.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            //model.AllowedImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();

            //ViewBag.filedetails = model.BlogEdit.ListCloudFile == null ? "" : JsonConvert.SerializeObject(model.BlogEdit.ListCloudFile);

            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });
            var lstUserGroup = model.GroupsList;
            var groupedOptions = lstUserGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName,
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                //Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
            }).ToList();
            //model.GroupsList = groupedOptions; //_schoolGroupService.GetSchoolGroupsByUserId(userId, SessionHelper.CurrentSession.IsTeacher()).ToList();
            // ViewBag.GroupList = groupedOptions;
            ViewBag.lstSchoolGroup = groupedOptions;
            //ViewBag.lstCategories = model.BlogEdit.CategoryList;
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            Session["CanEdit"] = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Chatter.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            int? pageNum = 1;
            model.Chatters = GetWallModel(0, pageNum);


            return View("MyWall", model);

        }

        public ActionResult GetProjects(int? pageNum, int groupId)
        {
            pageNum = pageNum ?? 0;
            ViewBag.IsEndOfRecords = false;
            if (Request.IsAjaxRequest())
            {
                var projects = GetRecordsForPage(pageNum.Value, groupId);
                ViewBag.IsEndOfRecords = (projects.Any());
                return PartialView("_MyWallList", projects);
            }
            else
            {
                var blogData = GetWallModel();
                ViewBag.TotalNumberProjects = blogData.Count;
                ViewBag.Projects = GetRecordsForPage(pageNum.Value, groupId);
                return View("Index");
            }
        }

        public List<Chatter> GetRecordsForPage(int pageNum, int groupId)
        {
            var blogData = GetWallModel(groupId, pageNum);
            return blogData;
        }

        public ActionResult GetWallModelOnSearchPaging(int id, string type, long? groupId, int? pageNum)
        {
            pageNum = pageNum ?? 0;
            ViewBag.IsEndOfRecords = false;
            if (Request.IsAjaxRequest())
            {
                var projects = GetRecordsWallSearchForPage(id, type, groupId, pageNum);
                ViewBag.IsEndOfRecords = (projects.Any());
                return PartialView("_MyWallList", projects);
            }
            else
            {
                var blogData = GetWallModelOnSearchForPaging(id, type, groupId, pageNum);
                ViewBag.TotalNumberProjects = blogData.Count;
                ViewBag.Projects = GetRecordsWallSearchForPage(id, type, groupId, pageNum);
                return View("Index");
            }
        }

        public List<Chatter> GetRecordsWallSearchForPage(int id, string type, long? groupId, int? pageNum)
        {
            var blogData = GetWallModelOnSearchForPaging(id, type, groupId, pageNum);
            return blogData;
        }

        [HttpPost]
        public ActionResult GetStudentListByGroup(int groupId)
        {
            var model = new YoYoBlogViewModel();

            model.StudentList = _studentListService.GetStudentsInGroup(groupId).ToList();
            var stdList = _studentListService.GetStudentsInGroup(groupId);

            List<SelectListGroup> lstSelectListStudent = new List<SelectListGroup>();

            var stdOptions = stdList.Select(x => new SelectListItem
            {
                Value = x.StudentId.ToString(),
                Text = (String.IsNullOrEmpty(x.FirstName) ? string.Empty : x.FirstName) + (String.IsNullOrEmpty(x.MiddleName) ? string.Empty : " " + x.MiddleName) + (String.IsNullOrEmpty(x.LastName) ? string.Empty : " " + x.LastName)
            }).ToList();


            ViewBag.lstSelectListStudent = stdOptions.ToList();
            var jsonResult = Json(model, JsonRequestBehavior.AllowGet);

            return new JsonResult()
            {
                Data = model.StudentList,
                MaxJsonLength = Int32.MaxValue
            };


        }
        [ActionName("RecentHistory")]
        public ActionResult GetRecentHistory(long? groupId)
        {
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            bool enableChatter = Boolean.Parse(ConfigurationManager.AppSettings["enableChatter"]);
            var chat = _chatService.GetRecentUserActivity((int)SessionHelper.CurrentSession.SchoolId, Convert.ToInt32(userId), enableChatter, groupId).ToList();
            return this.PartialView("_MyRecentChat", chat);
        }
        [ActionName("MyGroup")]
        public ActionResult GetMyGroup()
        {
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var groupList = _schoolGroupService.GetSchoolGroupsByUserId(userId, SessionHelper.CurrentSession.IsTeacher()).ToList();
            return this.PartialView("_MyGroup", groupList);
        }

        public ActionResult GetDisabledGroupList(long BlogId)
        {
            var result = _schoolGroupService.GetDisabledGroupList(BlogId, SessionHelper.CurrentSession.SchoolId);
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });
            var lstUserGroup = _schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher).ToList();

            var groupedOptions = lstUserGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName,
                Disabled = (result.Where(p => p == long.Parse(x.SchoolGroupId.ToString())).Select(p => p).FirstOrDefault()) > 0 ? true : false,
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
            }).ToList();
            ViewBag.lstSchoolGroup = groupedOptions;
            YoYoBlogViewModel obj = new YoYoBlogViewModel();
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_sharedpostgrouplist", obj);
            return Json(htmlContent, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateBlogWithNewUpdate(long UpdateBlogId, long UpdatFromBlogId, bool IsApproved, bool IsRejected)
        {
            var result = _schoolGroupService.UpdateBlogWithNewUpdate(UpdateBlogId, UpdatFromBlogId, IsApproved, IsRejected);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetWallListForGroup(int groupId)
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            var model = new YoYoBlogViewModel();
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            model.GroupsList = _schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher).ToList();
            //var groupList = _schoolGroupService.GetSchoolGroupsByUserId(userId, SessionHelper.CurrentSession.IsTeacher());
            //var groupId = groupList.Any() && groupList.Count() > 0 && id == 0 ? (int)groupList.First().SchoolGroupId : id;
            //model.RecentChatUserList = _chatService.GetRecentChatUserList((int)SessionHelper.CurrentSession.SchoolId, Convert.ToInt32(userId)).ToList();

            //model.RecentUserActivity = _chatService.GetRecentUserActivity((int)SessionHelper.CurrentSession.SchoolId, Convert.ToInt32(userId)).ToList();

            ////model.MyContactsList = _chatService.GetMyContactList(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), Convert.ToInt32(userId)).ToList();
            //model.MySearchContactsList = _chatService.GetSearchInChat(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), "", SessionHelper.CurrentSession.UserTypeId, Convert.ToInt32(userId)).Take(100).ToList();

            //model.Chatters = GetWallModel();

            //model.BlogEdit.CategoryList.AddRange(_blogService.GetBlogCategories(SessionHelper.CurrentSession.SchoolId)
            //    .Select(r => new SelectListItem { Text = r.Category, Value = r.CategoryId.ToString() }));
            model.BlogEdit.SchoolId = SessionHelper.CurrentSession.SchoolId;

            ViewBag.GroupId = groupId;
            ViewBag.filedetails = model.BlogEdit.ListCloudFile == null ? "" : JsonConvert.SerializeObject(model.BlogEdit.ListCloudFile);

            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });
            var lstUserGroup = _schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher).ToList();
            var groupedOptions = lstUserGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName,
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                //Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
            }).ToList();
            //model.GroupsList = groupedOptions; //_schoolGroupService.GetSchoolGroupsByUserId(userId, SessionHelper.CurrentSession.IsTeacher()).ToList();
            // ViewBag.GroupList = groupedOptions;
            ViewBag.lstSchoolGroup = groupedOptions;
            var fileTypes = _fileService.GetFileTypes();

            model.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            model.AllowedFileExtension.AddRange(fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList());

            //ViewBag.AllowedFileExtension = model.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            //ViewBag.AllowedImageExtension = model.AllowedImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();
            model.Chatters = GetWallModel(groupId);
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            Session["CanEdit"] = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Chatter.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;
            return PartialView("_BlogGroupList", model);

        }


        public ActionResult GetBlogDetailByBlogid(int? blogid=null, int? groupId = null)
        {
            List<Chatter> model;
            var fileTypes = _fileService.GetFileTypes();

            ViewBag.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            ViewBag.AllowedFileExtension.AddRange(fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList());

            model = GetWallModelByBlogId(blogid, groupId);
            return PartialView("_MyWallList", model);

        }
        private List<Chatter> GetWallModelByBlogId(int? blogid = null, int? groupId = null, int? pageNum = 1)
        {
            var model = new List<Chatter>();
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            int? LogedInUserId = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            if (SessionHelper.CurrentSession.IsAdmin)
            {

                bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
                model.AddRange(_blogService.GetMyWallData(blogid, (int)SessionHelper.CurrentSession.SchoolId, null, groupId, (int)userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId, pageNum));
            }
            else
            {

                bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
                model.AddRange(_blogService.GetMyWallData(blogid, (int)SessionHelper.CurrentSession.SchoolId, null, groupId, (int)userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId, pageNum));
            }
            return model;
        }

        [HttpPost]
        public ActionResult GetWallListByGroupId(int groupId)
        {
            List<Chatter> model;
            var fileTypes = _fileService.GetFileTypes();

            ViewBag.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            ViewBag.AllowedFileExtension.AddRange(fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList());

            //ViewBag.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            //ViewBag.AllowedImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();
            if (groupId != 0)
            {
                model = GetWallModel(groupId);
                return PartialView("_MyWallList", model);
            }
            else
            {
                model = GetWallModel();
                return PartialView("_MyWallList", model);
            }
        }

        public ActionResult SharePost(int groupId, string groupList, int blogId)
        {
            var result = 0;
            groupList = groupList.Replace("[", ""); groupList = groupList.Replace("]", "");
            groupList = groupList.Replace("\"", "");

            var model = new ShareBlogs
            {
                BlogId = blogId,
                BlogSharedBy = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id,
                IsShared = true,
                GroupIds = groupList
            };
            result = _blogService.ShareBlogsWithGroup(model);
            var fileModule = new FileService().GetFileManagementModules().FirstOrDefault(r => r.ModuleName.Contains("Chatter"));
            var files = new FileService().GetFilesBySectionId(blogId, fileModule.ModuleId, true);

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        private List<Chatter> GetWallModel(int? groupId = null, int? pageNum = 1)
        {
            var model = new List<Chatter>();
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            int? LogedInUserId = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            if (SessionHelper.CurrentSession.IsAdmin)
            {

                bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
                model.AddRange(_blogService.GetMyWallData(null, (int)SessionHelper.CurrentSession.SchoolId, null, groupId, (int)userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId, pageNum));
            }
            else
            {

                bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
                model.AddRange(_blogService.GetMyWallData(null, (int)SessionHelper.CurrentSession.SchoolId, null, groupId, (int)userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId, pageNum));
            }
            return model;
        }

        [HttpPost]
        public ActionResult GetWallModelOnSearch(int id, string type, long? groupId, int? pageNum)
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            var model = new List<Chatter>();
            var fileTypes = _fileService.GetFileTypes();

            //ViewBag.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            //ViewBag.AllowedFileExtension.AddRange(fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList());

            ViewBag.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            ViewBag.AllowedImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();
            var userId = 0;
            if (type.ToUpper().Equals("USER"))
            {
                userId = Convert.ToInt32(id);
            }
            else
            {
                userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : Convert.ToInt32(SessionHelper.CurrentSession.Id);
            }
            int? LogedInUserId = Convert.ToInt32(SessionHelper.CurrentSession.Id);

            bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
            if (type.ToUpper().Equals("USER"))
                model.AddRange(_blogService.GetMyWallDataOnSearch(null, (int)SessionHelper.CurrentSession.SchoolId, null, null, groupId, userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId));
            else if (type.ToUpper().Equals("BLOG"))
                // Changed to get the blog during search
                model.AddRange(_blogService.GetMyWallDataOnSearch(Convert.ToInt64(id), (int)SessionHelper.CurrentSession.SchoolId, null, null, groupId, userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId));
            else if (type.ToUpper().Equals("GROUP"))
                model.AddRange(_blogService.GetMyWallDataOnSearch(null, (int)SessionHelper.CurrentSession.SchoolId, null, null, Convert.ToInt64(id), userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId));

            return PartialView("_MyWallList", model);
        }

        /// <summary>
        /// Recent Acitivity Wall
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetRecentAcitityOnWall(int id, int userId, string type)
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            var model = new List<Chatter>();
            var fileTypes = _fileService.GetFileTypes();

            ViewBag.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            ViewBag.AllowedFileExtension.AddRange(fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList());

            //ViewBag.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            //ViewBag.AllowedImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();

            int LogedInUserId = (int)SessionHelper.CurrentSession.Id;
            //if (SessionHelper.CurrentSession.IsAdmin)
            //{
            //    model.AddRange(_blogService.GetMyWallDataOnSearch(null, (int)SessionHelper.CurrentSession.SchoolId, null, null, null, null, isteacher, null, null, null, null, LogedInUserId));
            //}
            //else
            //{
            bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
            if (type.ToUpper().Equals("BLOG"))
                model.AddRange(_blogService.GetMyWallDataOnSearch(Convert.ToInt64(id), (int)SessionHelper.CurrentSession.SchoolId, null, null, null, userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId));
            //}

            return PartialView("_MyWallList", model);
        }

        /// <summary>
        ///  Redirect to Mywall based on selected blog
        /// </summary
        /// <pamam name="blogid">blogId</param>
        /// <pamam name="createdBy">blog createdBy</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetRedirectToMyWallOnBlogSearch(string blogid, string createdBy)
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            bool enableChatter = Boolean.Parse(ConfigurationManager.AppSettings["enableChatter"]);

            int userIdCreater = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(createdBy));
            string type = "blog";
            var chatterModel = new List<Chatter>();
            var fileTypes = _fileService.GetFileTypes();

            ViewBag.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            ViewBag.AllowedFileExtension.AddRange(fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList());

            //ViewBag.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            //ViewBag.AllowedImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();

            var myWallModel = new YoYoBlogViewModel();
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            myWallModel.GroupsList = _schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher).ToList();
            var groupList = _schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher);
            myWallModel.RecentChatUserList = _chatService.GetRecentChatUserList((int)SessionHelper.CurrentSession.SchoolId, Convert.ToInt32(userId)).ToList();
            myWallModel.RecentUserActivity = _chatService.GetRecentUserActivity((int)SessionHelper.CurrentSession.SchoolId, Convert.ToInt32(userId), enableChatter, 0).ToList();
            myWallModel.MySearchContactsList = _chatService.GetSearchInChat(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), "", SessionHelper.CurrentSession.UserTypeId, Convert.ToInt32(userId)).Take(100).ToList();

            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });
            var lstUserGroup = _schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher).ToList();
            var groupedOptions = lstUserGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName,
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
            }).ToList();

            ViewBag.lstSchoolGroup = groupedOptions;
            ViewBag.lstCategories = myWallModel.BlogEdit.CategoryList;

            int LogedInUserId = (int)SessionHelper.CurrentSession.Id;
            //if (SessionHelper.CurrentSession.IsAdmin)
            //{
            //    chatterModel.AddRange(_blogService.GetMyWallDataOnSearch(null, (int)SessionHelper.CurrentSession.SchoolId, null, null, null, null, isteacher, null, null, null, null, LogedInUserId));
            //}
            //else
            //{
            bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
            if (type.ToUpper().Equals("BLOG"))
                chatterModel.AddRange(_blogService.GetMyWallDataOnSearch(Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(blogid)), (int)SessionHelper.CurrentSession.SchoolId, null, null, null, userIdCreater, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId));
            //}
            myWallModel.Chatters = chatterModel;
            return View("MyWall", myWallModel);
        }

        [HttpPost]
        public JsonResult SearchOnMyWall(string searchString, long? groupId)
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : Convert.ToInt64(SessionHelper.CurrentSession.Id);

            //Note : you can bind same list from database
            var list = groupId.HasValue ?
                _blogService.GetGroupBySearchInBlog(Convert.ToInt64(SessionHelper.CurrentSession.SchoolId), searchString, userId, isteacher, groupId.Value).Distinct().ToList() :
                _blogService.GetSearchInBlog(Convert.ToInt64(SessionHelper.CurrentSession.SchoolId), searchString, userId, isteacher).Distinct().ToList();
            //Searching records from list using LINQ query
            var result = (from N in list
                          where N.SearchText.ToUpper().Contains(searchString.ToUpper())
                          select new { N.SearchId, N.SearchText, N.SearchImage, N.BlogId, N.SchoolGroupId, N.UserId });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MyContactSearch(string searchString)
        {
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var result = _chatService.GetSearchInChat(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), searchString, Convert.ToInt32(SessionHelper.CurrentSession.UserTypeId), Convert.ToInt32(userId)).Take(200).ToList();

            return Json(result?.Select(x => new { x.ToUserId, x.ToUserProfileImage, x.ToUser, encryptUserId = EncryptDecryptHelper.EncryptUrl(x.ToUserId.ToString()), type = EncryptDecryptHelper.EncryptUrl("MyContact"), x.IsOnline }), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<ActionResult> Capture(string name, int blogId, string IsPublish)
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            List<string> lstFile = new List<string>();
            try
            {
                bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
                int? LogedInUserId = Convert.ToInt32(SessionHelper.CurrentSession.Id);
                var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
                var blogChatter = _blogService.GetMyWallDataOnSearch(Convert.ToInt64(blogId), (int)SessionHelper.CurrentSession.SchoolId, null, null, null, userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId);
                var blogDetail = blogChatter?.FirstOrDefault()?.Blog;
                bool isAllow = blogDetail != null;
                if (!isteacher && isAllow) isAllow = ((blogDetail.EnableCommentById == SessionHelper.CurrentSession.UserTypeId)
                        || blogDetail.EnableCommentById == 3);
                else
                    isAllow = true;
                if (isAllow)
                {


                    string fileName = string.Empty;
                    string relativePath = string.Empty;
                    var files = Request.Files;
                    string[] imgarray = name.Split(',');
                    fileName = Guid.NewGuid() + "_" + "Camera.jpg";
                    var blogComment = new BlogComment();
                    BlogCommentEdit blogCommentEdit = new BlogCommentEdit();
                    blogCommentEdit.BlogId = blogId; blogCommentEdit.Comment = "";
                    var module = _fileService.GetFileManagementModules().FirstOrDefault(r => r.ModuleName.Contains("ChatterComment"));

                    string relativeDocPath = string.Empty;
                    var _fileTypeList = _fileService.GetFileTypes().ToList();

                    EntityMapper<BlogCommentEdit, BlogComment>.Map(blogCommentEdit, blogComment);
                    var blog = _blogService.Get(Convert.ToInt32(blogCommentEdit.BlogId));

                    blogComment.IsPublish = Convert.ToBoolean(IsPublish);
                    blogComment.UpdatedBy = SessionHelper.CurrentSession.Id;
                    blogComment.PublishBy = SessionHelper.CurrentSession.Id;

                    bool result = false;
                    if (blogCommentEdit.CommentId > 0)
                    {
                        blogComment.UpdatedBy = SessionHelper.CurrentSession.Id;
                        result = _blogCommentService.Update(blogComment);
                    }
                    else
                    {
                        blogComment.CreatedBy = SessionHelper.CurrentSession.Id;
                        blogCommentEdit.CommentId = _blogCommentService.Insert(blogComment);
                        result = true;
                        var file = new File();
                        string extension = string.Empty;
                        var fileEditList = new List<FileEdit>();
                        string resourceDir = Server.MapPath(Constants.ResourcesDir);
                        string blogContent = Server.MapPath(Constants.BlogDir);
                        string blogPath = Server.MapPath(Constants.BlogDir + "/BlogComment_" + blogCommentEdit.CommentId + "/");
                        Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                        Common.Helpers.CommonHelper.CreateDestinationFolder(blogContent);
                        Common.Helpers.CommonHelper.CreateDestinationFolder(blogPath);
                        string filePathName = Path.Combine(Server.MapPath(Constants.BlogDir + "/BlogComment_" + blogCommentEdit.CommentId + "/"), fileName);
                        byte[] imageBytes = Convert.FromBase64String(imgarray[1].ToString());
                        System.IO.File.WriteAllBytes(filePathName, imageBytes);
                        string fname = fileName;
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = fname.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = fileName;
                        }
                        extension = Path.GetExtension(fname).Replace(".", "");
                        var extId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;
                        double fileSizeInMb = (15360 / 1024f) / 1024f;
                        fileEditList.Add(new FileEdit() { FileName = fname, PostedFile = null, ResourceFileTypeId = (short)ResourceFileTypes.SharePoint, FileTypeId = extId, FileSizeInMB = fileSizeInMb });
                        List<FileEdit> fileListWithPath = new List<FileEdit>();
                        if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                        {
                            AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                            await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                            fileListWithPath = await azureHelper.UploadFilesListAsPerModuleCameraAsync(FileModulesConstants.BlogComment, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList, Constants.BlogDir + "/BlogComment_" + blogCommentEdit.CommentId + "/" + fileName);
                        }
                        else
                        {
                            SharePointHelper sh = new SharePointHelper();
                            Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                            fileListWithPath = SharePointHelper.UploadFilesListAsPerModuleCamera(ref cx, FileModulesConstants.BlogComment, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList, Constants.BlogDir + "/BlogComment_" + blogCommentEdit.CommentId + "/" + fileName);
                        }
                        System.IO.File.Delete(filePathName);

                        if (fileListWithPath.Count > 0)
                        {
                            file.FileName = fileName;
                            file.FilePath = fileListWithPath[0].FilePath;
                            file.FileTypeId = extId;
                            file.FolderId = 0;
                            file.IsActive = true;
                            file.ModuleId = module.ModuleId;
                            file.SectionId = blogCommentEdit.CommentId;
                            file.SchoolId = SessionHelper.CurrentSession.SchoolId;
                            file.CreatedBy = SessionHelper.CurrentSession.Id;
                            _fileService.Insert(file);
                        }
                    }
                }
                else
                {
                    return Json(false);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Json(lstFile);
        }


        public ActionResult DisplayNewBlogPopup()
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            var module = _fileModuleList.FirstOrDefault(r => r.ModuleName.Contains("Chatter"));
            //set permission
            ViewBag.IsCustomPermission = SetAccessPermissionForModule(module.ModuleId);
            ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();

            var model = new YoYoBlogViewModel();
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;

            model.BlogEdit.GroupList.Add(new SelectListItem { Text = ResourceManager.GetString("School.YoYoBlog.SelectGroupDefault"), Value = "" });
            model.BlogEdit.GroupList.AddRange(_schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher)
                .Select(r => new SelectListItem { Text = r.SchoolGroupName, Value = r.SchoolGroupId.ToString() }
                ));

            int selectedGroupid = Convert.ToInt32(model.BlogEdit.GroupList.Where(m => m.Selected == true).Select(r => r.Value).FirstOrDefault());

            string studentlist = model.BlogEdit.SelectedStudentList.ToString();

            //model.BlogEdit.SelectedStudentList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectStudent"), Value = "" });
            model.BlogEdit.SelectedStudentList.AddRange(_studentListService.GetStudentsInGroup(Convert.ToInt32(model.BlogEdit.GroupId))
                .Select(x => new SelectListItem
                {
                    Text = (String.IsNullOrEmpty(x.FirstName) ? string.Empty : x.FirstName) +
                (String.IsNullOrEmpty(x.MiddleName) ? string.Empty : " " + x.MiddleName) +
                (String.IsNullOrEmpty(x.LastName) ? string.Empty : " " + x.LastName),
                    Value = x.Id.ToString()
                }));

            model.BlogEdit.CategoryList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectCategory"), Value = "" });
            model.BlogEdit.CategoryList.AddRange(_blogService.GetBlogCategories(SessionHelper.CurrentSession.SchoolId)
                .Select(r => new SelectListItem { Text = r.Category, Value = r.CategoryId.ToString() }));
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });
            var lstUserGroup = _schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher).ToList();
            var groupedOptions = lstUserGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName,
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                //Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
            }).ToList();
            //model.GroupsList = groupedOptions; //_schoolGroupService.GetSchoolGroupsByUserId(userId, SessionHelper.CurrentSession.IsTeacher()).ToList();
            ViewBag.GroupList = groupedOptions;
            model.BlogEdit.SchoolId = SessionHelper.CurrentSession.SchoolId;

            var fileTypes = _fileService.GetFileTypes();

            model.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            model.AllowedFileExtension.AddRange(fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList());

            //model.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            //model.AllowedImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();

            model.BlogEdit.UserTypeId = SessionHelper.CurrentSession.UserTypeId;
            model.BlogEdit.FromDate = DateTime.Now;
            // -- Prod enhancement to increase endDate from 1 day to 30 days
            model.BlogEdit.ToDate = DateTime.Now.AddDays(30);
            // model.BlogEdit.ScheduledTime = DateTime.Now.ToString("HH:mm");
            //model.BlogEdit.ScheduledTime = model.BlogEdit.ScheduledTime.Split(' ')[1];

            model.BlogEdit.EnableCommentById = 1;
            model.BlogEdit.CommentApprovedById = 0;

            ViewBag.filedetails = model.BlogEdit.ListCloudFile == null ? "" : JsonConvert.SerializeObject(model.BlogEdit.ListCloudFile);

            Session["ListCloudFile"] = null;
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            Session["CanEdit"] = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Chatter.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            return PartialView("_MyWallAddEditBlog", model.BlogEdit);
        }




        public bool SetAccessPermissionForModule(int moduleId)
        {
            var result = false;
            var syncContext = SynchronizationContext.Current;
            switch (moduleId)
            {

                case 1: //My files
                    SynchronizationContext.SetSynchronizationContext(null);
                    result = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_MyFiles.ToString()).Result;
                    SynchronizationContext.SetSynchronizationContext(syncContext);
                    break;
                case 2: // Groups
                    SynchronizationContext.SetSynchronizationContext(null);
                    result = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ViewGroupFiles.ToString()).Result;
                    SynchronizationContext.SetSynchronizationContext(syncContext);
                    break;
                case 3: // School Space
                    SynchronizationContext.SetSynchronizationContext(null);
                    result = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ViewSchoolSpaceFiles.ToString()).Result;
                    SynchronizationContext.SetSynchronizationContext(syncContext);
                    break;
                default: // Lockers
                    SynchronizationContext.SetSynchronizationContext(null);
                    result = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ViewLockerFiles.ToString()).Result;
                    SynchronizationContext.SetSynchronizationContext(syncContext);
                    break;
            }
            return result;
        }

        public ActionResult MyWallEditBlog(int? id)
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            var model = new YoYoBlogViewModel();
            BlogCloudFile fileNames = new BlogCloudFile();
            if (id.HasValue)
            {
                var blog = _blogService.Get(id.Value);
                model.BlogEdit.IsVisibleToParent = blog.IsVisibleToParent;
                EntityMapper<Blog, BlogEdit>.Map(blog, model.BlogEdit);
                string[] blogIds;
                string[] blogFiles; string[] blogFilePaths; string[] blogFileTyps; string[] blogResouceTyps;
                string[] blogResouceTypsId; string[] blogPhysicalPaths;

                if (blog != null && !string.IsNullOrEmpty(blog.FileId) && blog.FileId.Contains(","))
                {
                    blogIds = blog.FileId?.Split(',') ?? Enumerable.Empty<string>().ToArray();
                }
                else
                {
                    blogIds = blog.FileId?.Split('|') ?? Enumerable.Empty<string>().ToArray();
                }

                if (blog != null && !string.IsNullOrEmpty(blog.FileName) && blog.FileName.Contains(","))
                {
                    blogFiles = blog.FileName?.Split(',') ?? Enumerable.Empty<string>().ToArray();
                }
                else
                {
                    blogFiles = blog.FileName?.Split('|') ?? Enumerable.Empty<string>().ToArray();
                }

                if (blog != null && !string.IsNullOrEmpty(blog.FilePath) && blog.FilePath.Contains(","))
                {
                    blogFilePaths = blog.FilePath?.Split(',') ?? Enumerable.Empty<string>().ToArray();
                }
                else
                {
                    blogFilePaths = blog.FilePath?.Split('|') ?? Enumerable.Empty<string>().ToArray();
                }

                if (blog != null && !string.IsNullOrEmpty(blog.FileType) && blog.FileType.Contains(","))
                {
                    blogFileTyps = blog.FileType?.Split(',') ?? Enumerable.Empty<string>().ToArray();
                }
                else
                {
                    blogFileTyps = blog.FileType?.Split('|') ?? Enumerable.Empty<string>().ToArray();
                }

                if (blog != null && !string.IsNullOrEmpty(blog.ResourceFileType) && blog.ResourceFileType.Contains(","))
                {
                    blogResouceTyps = blog.ResourceFileType?.Split(',') ?? Enumerable.Empty<string>().ToArray();
                }
                else
                {
                    blogResouceTyps = blog.ResourceFileType?.Split('|') ?? Enumerable.Empty<string>().ToArray();
                }

                if (blog != null && !string.IsNullOrEmpty(blog.ResourceFileTypeId) && blog.ResourceFileTypeId.Contains(","))
                {
                    blogResouceTypsId = blog.ResourceFileTypeId?.Split(',') ?? Enumerable.Empty<string>().ToArray();
                }
                else
                {
                    blogResouceTypsId = blog.ResourceFileTypeId?.Split('|') ?? Enumerable.Empty<string>().ToArray();
                }

                if (blog != null && !string.IsNullOrEmpty(blog.PhysicalFilePath) && blog.PhysicalFilePath.Contains(","))
                {
                    blogPhysicalPaths = blog.PhysicalFilePath?.Split(',') ?? Enumerable.Empty<string>().ToArray();
                }
                else
                {
                    blogPhysicalPaths = blog.PhysicalFilePath?.Split('|') ?? Enumerable.Empty<string>().ToArray();
                }



                for (int i = 0; i < blogFiles.Length; i++)
                {
                    if ((blogResouceTyps[i].ToString().ToLower() != "one drive") && (blogResouceTyps[i].ToString().ToLower() != "google drive") && (blogResouceTyps[i].ToString().ToLower() != "one note"))
                    {
                        model.BlogEdit.AttachmentList.Add(new Phoenix.Models.Entities.Attachment
                        {
                            AttachmentId = Int64.Parse(blogIds[i]),
                            AttachmentPath = blogFilePaths[i],
                            FileName = blogFiles[i],
                            //AttachmentKey = blogPhysicalPaths[i],
                            AttachmentKey = blogFilePaths[i],
                            //ResourceFileTypeId = blogResouceTypsId[i].ToShort()
                            ResourceFileTypeId = 3
                        });
                    }
                    else
                    {
                        short ResourceFileType = 0;
                        if (blogResouceTyps[i].ToString().ToLower() == "one drive")
                        {
                            ResourceFileType = (short)ResourceFileTypes.OneDrive;
                        }
                        else if (blogResouceTyps[i].ToString().ToLower() == "google drive")
                        {
                            ResourceFileType = (short)ResourceFileTypes.GoogleDrive;
                        }
                        else if (blogResouceTyps[i].ToString().ToLower() == "one note")
                        {
                            ResourceFileType = (short)ResourceFileTypes.OneNote;
                        }


                        fileNames = new BlogCloudFile
                        {
                            BlogCloudId = id.Value,
                            CloudFileId = blogIds[i],
                            FileName = blogFiles[i],
                            IsCloudFile = true,
                            FileExtension = blogFileTyps[i],
                            UploadedFileName = blogFilePaths[i],
                            DriveId = "",
                            ParentFolderId = "",
                            ResourceFileTypeId = ResourceFileType
                        };
                        model.BlogEdit.ListCloudFile.Add(fileNames);
                    }

                }
            }
            else
            {
                model.BlogEdit.IsAddMode = true;
                model.BlogEdit.SchoolId = SessionHelper.CurrentSession.SchoolId;
            }
            //Convert local time to UTC
            model.BlogEdit.ToDate = model.BlogEdit.ToDate.Value.ConvertUtcToLocalTime();
            model.BlogEdit.FromDate = Convert.ToDateTime(model.BlogEdit.FromDate?.ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(model.BlogEdit.ScheduledTime).ToString("HH:mm")).ConvertUtcToLocalTime();

            model.BlogEdit.ScheduledTime = ((DateTime)Convert.ToDateTime(model.BlogEdit.ScheduledTime).ConvertUtcToLocalTime()).ToString("HH:mm");

            Session["ListCloudFile"] = model.BlogEdit.ListCloudFile;
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_BlogCloudFileList", model.BlogEdit.ListCloudFile.DistinctBy(x => x.CloudFileId).ToList());
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            model.BlogEdit.GroupList.Add(new SelectListItem { Text = ResourceManager.GetString("School.YoYoBlog.SelectGroupDefault"), Value = "" });
            model.BlogEdit.GroupList.AddRange(_schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher)
                .Select(r => r.SchoolGroupId == model.BlogEdit.GroupId ? new SelectListItem { Text = r.SchoolGroupName, Value = r.SchoolGroupId.ToString(), Selected = true }
                : new SelectListItem { Text = r.SchoolGroupName, Value = r.SchoolGroupId.ToString() }));

            int selectedGroupid = Convert.ToInt32(model.BlogEdit.GroupList.Where(m => m.Selected == true).Select(r => r.Value).FirstOrDefault());
            List<string> selectedstring = new List<string>();
            if (!string.IsNullOrEmpty(model.BlogEdit.SelectedStudentIDs))
            {
                selectedstring = model.BlogEdit.SelectedStudentIDs.Split(',').ToList();
            }

            List<SelectListItem> SelectedStudentListTemp = new List<SelectListItem>();

            SelectedStudentListTemp.AddRange(_studentListService.GetStudentsInGroup(selectedGroupid)
                        .Select(x => new SelectListItem
                        {
                            Text = (String.IsNullOrEmpty(x.FirstName) ? string.Empty : x.FirstName) +
                        (String.IsNullOrEmpty(x.MiddleName) ? string.Empty : " " + x.MiddleName) +
                        (String.IsNullOrEmpty(x.LastName) ? string.Empty : " " + x.LastName),
                            Value = x.Id.ToString()
                        }));

            ViewBag.SelectedStudent = selectedstring;

            //model.BlogEdit.SelectedStudentList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectStudent"), Value = "" });
            model.BlogEdit.SelectedStudentList.AddRange(_studentListService.GetStudentsInGroup(selectedGroupid)
                .Select(x => selectedstring.Contains(Convert.ToString(x.Id)) ? new SelectListItem
                {
                    Text = (String.IsNullOrEmpty(x.FirstName) ? string.Empty : x.FirstName) +
                (String.IsNullOrEmpty(x.MiddleName) ? string.Empty : " " + x.MiddleName) +
                (String.IsNullOrEmpty(x.LastName) ? string.Empty : " " + x.LastName),
                    Value = x.Id.ToString(),
                    Selected = true
                } :
                new SelectListItem
                {
                    Text = (String.IsNullOrEmpty(x.FirstName) ? string.Empty : x.FirstName) +
                (String.IsNullOrEmpty(x.MiddleName) ? string.Empty : " " + x.MiddleName) +
                (String.IsNullOrEmpty(x.LastName) ? string.Empty : " " + x.LastName),
                    Value = x.Id.ToString()
                }));

            ViewBag.SelectedStudentList = _studentListService.GetStudentsInGroup(selectedGroupid);
            ViewBag.FirstSelectedStudentList = model.BlogEdit.SelectedStudentList.ToList().Where(m => m.Selected == true).Select(x => x.Text).FirstOrDefault();

            ViewBag.FirstSelectedStudentCount = model.BlogEdit.SelectedStudentList.ToList().Where(m => m.Selected == true).Count() - 1;

            //ViewBag.FirstSelectedStudentList = _studentListService.GetStudentsInGroup(selectedGroupid).ToList().Select(x => (String.IsNullOrEmpty(x.FirstName) ? string.Empty : x.FirstName) +
            //    (String.IsNullOrEmpty(x.MiddleName) ? string.Empty : " " + x.MiddleName) +
            //    (String.IsNullOrEmpty(x.LastName) ? string.Empty : " " + x.LastName)).FirstOrDefault();


            model.BlogEdit.CategoryList.Add(new SelectListItem { Text = ResourceManager.GetString("School.Blog.SelectCategory"), Value = "" });
            model.BlogEdit.CategoryList.AddRange(_blogService.GetBlogCategories(SessionHelper.CurrentSession.SchoolId)
                .Select(r => r.CategoryId == model.BlogEdit.CategoryId ? new SelectListItem { Text = r.Category, Value = r.CategoryId.ToString(), Selected = true } :
                new SelectListItem { Text = r.Category, Value = r.CategoryId.ToString() }));

            model.BlogEdit.EnableCommentForList.AddRange(Enum.GetValues(typeof(EnableCommentForCodes)).Cast<EnableCommentForCodes>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }));
            ViewBag.filedetails = model.BlogEdit.ListCloudFile == null ? "" : JsonConvert.SerializeObject(model.BlogEdit.ListCloudFile);
            model.BlogEdit.Description = HtmlToPlainText(model.BlogEdit.Description);
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });
            var lstUserGroup = _schoolGroupService.GetSchoolGroupsByUserId(userId, isteacher).ToList();
            var groupedOptions = lstUserGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName,
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                //Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
            }).ToList();
            //model.GroupsList = groupedOptions; //_schoolGroupService.GetSchoolGroupsByUserId(userId, SessionHelper.CurrentSession.IsTeacher()).ToList();
            ViewBag.GroupList = groupedOptions;
            return PartialView("_MyWallAddEditBlog", model.BlogEdit);
        }

        private static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);

            if (!(String.IsNullOrEmpty(text)))
            {
                //Remove tag whitespace/line breaks
                text = tagWhiteSpaceRegex.Replace(text, "><");
                //Replace <br /> with line breaks
                text = lineBreakRegex.Replace(text, Environment.NewLine);
                //Strip formatting
                //text = stripFormattingRegex.Replace(text, string.Empty);
            }
            return text;
        }

        public ActionResult SaveBlogCloudFiles(string fileIds, short resourceFileTypeId, string cloudFilesDetails)
        {
            var model = new BlogEdit();
            BlogCloudFile fileNames = new BlogCloudFile();

            if (cloudFilesDetails != "" && cloudFilesDetails != null)
            {
                fileNames = JsonConvert.DeserializeObject<BlogCloudFile>(cloudFilesDetails);
            }
            var selectedFiles = JsonConvert.DeserializeObject<List<CloudFileListView>>(fileIds);
            if (Session["ListCloudFile"] != null)
            {
                model.ListCloudFile = (List<BlogCloudFile>)Session["ListCloudFile"];
            }
            foreach (var item in selectedFiles)
            {
                item.FileExtension = Helpers.CommonHelper.GetFileExtensionByMimeType(item.MimeType);

                fileNames = new BlogCloudFile
                {
                    CloudFileId = item.Id,
                    FileName = item.Name,
                    IsCloudFile = true,
                    FileExtension = item.FileExtension,
                    UploadedFileName = item.WebViewLink,
                    DriveId = item.DriveId,
                    ParentFolderId = item.ParentFolderId,
                    ResourceFileTypeId = item.ResourceFileTypeId

                };
                model.ListCloudFile.Add(fileNames);
            }
            var result = new Object();


            Session["ListCloudFile"] = model.ListCloudFile;

            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_BlogCloudFileList", model.ListCloudFile.DistinctBy(x => x.CloudFileId).ToList());
            var contentList = new string[] { htmlContent };
            result = new
            {
                content = contentList,
                filenames = fileNames
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public ActionResult DeleteCloudFile(string fileName)
        {
            var model = new BlogEdit();
            if (Session["ListCloudFile"] != null)
            {
                model.ListCloudFile = (List<BlogCloudFile>)Session["ListCloudFile"];
                var lst = model.ListCloudFile.Where(m => m.FileName == fileName).ToList();
                if (lst.Count > 0)
                {
                    if (int.TryParse(lst[0].CloudFileId, out int result))
                        _fileService.Delete(result, SessionHelper.CurrentSession.Id);
                    model.ListCloudFile.Remove(lst[0]);
                    Session["ListCloudFile"] = model.ListCloudFile;
                }
            }
            return Json("true", JsonRequestBehavior.AllowGet);
        }

        public List<Chatter> GetWallModelOnSearchForPaging(int id, string type, long? groupId, int? pageNum)
        {
            bool isteacher = false;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isteacher = true;
            }
            var model = new List<Chatter>();
            var fileTypes = _fileService.GetFileTypes();

            ViewBag.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            ViewBag.AllowedFileExtension.AddRange(fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList());

            //ViewBag.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            //ViewBag.AllowedImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();

            var userId = 0;
            if (type.ToUpper().Equals("USER"))
            {
                userId = Convert.ToInt32(id);
            }
            else
            {
                userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : Convert.ToInt32(SessionHelper.CurrentSession.Id);
            }
            int? LogedInUserId = Convert.ToInt32(SessionHelper.CurrentSession.Id);

            bool? isCommentPublish = SessionHelper.CurrentSession.UserTypeId >= 3 ? (bool?)null : true;
            if (type.ToUpper().Equals("USER"))
                model.AddRange(_blogService.GetMyWallDataOnSearch(null, (int)SessionHelper.CurrentSession.SchoolId, null, null, groupId, userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId, pageNum));
            else if (type.ToUpper().Equals("BLOG"))
                // Changed to get the blog during search
                model.AddRange(_blogService.GetMyWallDataOnSearch(Convert.ToInt64(id), (int)SessionHelper.CurrentSession.SchoolId, null, null, groupId, userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId, pageNum));
            else if (type.ToUpper().Equals("GROUP"))
                model.AddRange(_blogService.GetMyWallDataOnSearch(null, (int)SessionHelper.CurrentSession.SchoolId, null, null, Convert.ToInt64(id), userId, isteacher, isCommentPublish, null, null, isCommentPublish, LogedInUserId, pageNum));

            return model;
        }
        #endregion
    }
}