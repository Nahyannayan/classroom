﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.EditModels;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Helpers;
using Phoenix.Common;
using System.IO;
using Phoenix.Common.Enums;
using Phoenix.VLE.Web.ViewModels;
using File = Phoenix.Models.File;
using System.Web.Script.Serialization;
using System.Data;
using Phoenix.Common.Localization;
using Phoenix.Common.ViewModels;
using System.Text;
using System.Threading.Tasks;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class ChatController : BaseController
    {
        private readonly IChatService _chatService;
        private readonly IBlogCommentService _blogCommentService;
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly IFileService _fileService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly IStudentListService _studentListService;
        private readonly ICommonService _commonService;
        private List<VLEFileType> _fileTypeList;
        private ILogInUserService _loginUserService;
        private List<UserChatPermission> _userChatPermission;
        public ChatController(ILogInUserService loginUserService, IChatService chatService, IBlogService blogService, IBlogCommentService blogCommentService, ISchoolGroupService schoolGroupService, IFileService fileService, IUserPermissionService userPermissionService, IStudentListService studentListService, ICommonService commonService)
        {
            _chatService = chatService;
            _blogCommentService = blogCommentService;
            _schoolGroupService = schoolGroupService;
            _fileService = fileService;
            _userPermissionService = userPermissionService;
            _studentListService = studentListService;
            _commonService = commonService;
            _loginUserService = loginUserService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
            _userChatPermission = _chatService.GetUserPermissionChat(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), Convert.ToInt32(SessionHelper.CurrentSession.Id)).ToList();
        }

        // GET: Schoolinfo/Chat/Chat
        public ActionResult Chat(string Id = "", string typem = "", string IsGroup = "", string groupId = "")
        {

            if (SessionHelper.CurrentSession.IsParent())
            {
                return Redirect("/error/nopermission");
            }
            var visitorInfo = new VisitorInfo();
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();

            List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
            var model = new ChatViewModel();
            model.IsGroup = false;
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });
            model.MyContactsList = _chatService.GetSearchInChat(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), "", Convert.ToInt32(SessionHelper.CurrentSession.UserTypeId), Convert.ToInt32(SessionHelper.CurrentSession.Id)).Take(50).ToList();
            //model.MyContactsList = _chatService.GetMyContactList(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), Convert.ToInt32(SessionHelper.CurrentSession.Id)).ToList();
            var lstOfAllSchoolGroups = _schoolGroupService.GetSchoolGroupsBySchoolId(SessionHelper.CurrentSession.SchoolId);
            var lstUserGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            var groupedOptions = lstUserGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName,
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                //Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
            }).ToList();
            ViewBag.lstSchoolGroup = groupedOptions;
            model.SchoolGroups = lstUserGroup;

            var fileTypes = _fileService.GetFileTypes();

            model.AllowedFileExtension = fileTypes.Where(r => r.DocumentType.Contains("Document")).Select(r => r.Extension).ToList();
            model.AllowedImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();

            model.UserList = _chatService.GetRecentChatUserList(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), Convert.ToInt32(SessionHelper.CurrentSession.Id)).ToList();

            model.UserList = model.UserList.OrderByDescending(m => m.MessageDateTime).ToList();
            model.ChatPermissionUser = _chatService.GetUserPermissionChat(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), Convert.ToInt32(SessionHelper.CurrentSession.Id)).ToList();


            //SessionHelper.CurrentSession.Token

            /// All user for chat group
            //model.GetUserList = _loginUserService.GetUserList(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId)).ToList();
            //model.GetUserList = model.GetUserList.Where(m => m.UserTypeId != 2).ToList();

            //model.GetUserList =_chatService.GetMyContactList(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), Convert.ToInt32(SessionHelper.CurrentSession.Id)).ToList();
            model.GetUserList = _chatService.GetSearchInChat(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), "", SessionHelper.CurrentSession.UserTypeId, (int)SessionHelper.CurrentSession.Id).Distinct().ToList();

            var loginSetting = Phoenix.VLE.Web.Helpers.CommonHelper.GetUserFeeling();


            if (SessionHelper.CurrentSession.IsStudent())
            {
                if (!string.IsNullOrWhiteSpace(loginSetting.ProfilePhoto))
                {
                    model.ProfileImage = Constants.StudentImagesPath + loginSetting.ProfilePhoto;
                }
                else if (!string.IsNullOrWhiteSpace(loginSetting.AvatarLogo))
                {
                    model.ProfileImage = Constants.ProfileAvatarPath + loginSetting.AvatarLogo;
                }
                else
                {
                    model.ProfileImage = "/Content/VLE/img/default-avatar.jpg";
                }
            }

            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                if (!string.IsNullOrWhiteSpace(loginSetting.ProfilePhoto))
                {
                    model.ProfileImage = PhoenixConfiguration.Instance.ReadFilePath + Constants.TeacherProfilePath + loginSetting.ProfilePhoto;
                }
                else if (!string.IsNullOrWhiteSpace(loginSetting.AvatarLogo))
                {
                    model.ProfileImage = Constants.ProfileAvatarPath + loginSetting.AvatarLogo;

                }
                else
                {
                    model.ProfileImage = "/Content/VLE/img/default-avatar.jpg";
                }
            }

            if (!String.IsNullOrEmpty(Id))
            {
                int userId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(Id));
                string operationtype = EncryptDecryptHelper.DecryptUrl(typem).ToString();

                if (userId > 0)
                {
                    if (operationtype == "Recent")
                    {
                        int isGroupValue = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(IsGroup));
                        int groupIdValue = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(groupId));

                        if (isGroupValue == 1)
                        {
                            if (model.UserList.Where(x => x.GroupId == groupIdValue).ToList().Count > 0)
                            {
                                model.ChatNowUserId = model.UserList.Where(x => x.GroupId == groupIdValue).ToList()[0].GroupId;
                                model.ChatNowUserName = model.UserList.Where(x => x.GroupId == groupIdValue).ToList()[0].SignalRGroupName;
                                model.IsGroup = true;
                            }
                        }
                        else
                        {
                            if (model.UserList.Where(x => x.ToUserId == userId).ToList().Count > 0)
                            {
                                model.ChatNowUserId = model.UserList.Where(x => x.ToUserId == userId).ToList()[0].ToUserId;
                                model.ChatNowUserName = model.UserList.Where(x => x.ToUserId == userId).ToList()[0].ToUser;
                            }

                        }

                    }
                    else if (operationtype == "MyContact")
                    {
                        if (model.MyContactsList.Where(x => x.ToUserId == userId).ToList().Count > 0)
                        {
                            model.ChatNowUserId = model.MyContactsList.Where(x => x.ToUserId == userId).ToList()[0].ToUserId;
                            model.ChatNowUserName = model.MyContactsList.Where(x => x.ToUserId == userId).ToList()[0].ToUser;
                        }
                    }

                }
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult RedirectGroup(string Id)
        {
            var result = "/SchoolStructure/SchoolGroups/GroupDetails?grpId=" + EncryptDecryptHelper.EncryptUrl(Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetChatHistory(string toUserById, string groupId)
        {
            var result = _chatService.GetChatHistorybyUserId(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), Convert.ToInt32(SessionHelper.CurrentSession.Id), Convert.ToInt32(toUserById), Convert.ToInt32(groupId), 1);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<ActionResult> UploadFile(ChatViewModel model)
        {
            List<string> lstFile = new List<string>();
            string fileName = string.Empty;
            string relativePath = string.Empty;
            var fileEditList = new List<FileEdit>();
            try
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    string fname = file.FileName;
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = file.FileName;
                    }
                    var extension = Path.GetExtension(file.FileName).Replace(".", "");
                    var extId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;
                    double fileSizeInMb = (file.ContentLength / 1024f) / 1024f;
                    fileEditList.Add(new FileEdit() { FileName = fname, PostedFile = file, ResourceFileTypeId = (short)ResourceFileTypes.SharePoint, FileTypeId = extId, FileSizeInMB = fileSizeInMb });
                }
                List<FileEdit> fileListWithPath = new List<FileEdit>();
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    fileListWithPath = await azureHelper.UploadFilesListAsPerModuleAsync(FileModulesConstants.Chatter, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    fileListWithPath = SharePointHelper.UploadFilesListAsPerModule(ref cx, FileModulesConstants.Chatter, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList);
                }

                if (fileListWithPath.Count > 0)
                {
                    lstFile.Add(fileListWithPath[0].FilePath);
                    lstFile.Add(fileListWithPath[0].FileName);
                }
            }
            catch (Exception ex)
            {
                var d = ex.Message.ToString();
                return Json("Upload failed");
            }
            return Json(lstFile);
        }

        [HttpPost]
        public JsonResult Search(string searchText)
        {
            //Note : you can bind same list from database
            var list = _chatService.GetSearchInChat(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), searchText, SessionHelper.CurrentSession.UserTypeId, (int)SessionHelper.CurrentSession.Id).Distinct().ToList();
            //Searching records from list using LINQ query
            var result = (from N in list
                          where N.ToUser.ToUpper().Contains(searchText.ToUpper())
                          select new { N.ToUser, N.ToUserProfileImage, N.ToUserId });
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult RecentUserList(int userId, string userName, string toUserImage)
        {
            List<Chat> model = new List<Chat>();
            model.Add(new Phoenix.Models.Chat { ToUserId = userId, ToUser = userName, ToUserProfileImage = toUserImage });
            //var dmodel = _chatService.GetRecentChatUserList(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), Convert.ToInt32(SessionHelper.CurrentSession.Id)).ToList();
            //model.AddRange(dmodel);
            return PartialView("_RecentChat", model);


        }


        public ActionResult MyContactSearch(string searchString)
        {
            //Note : you can bind same list from database
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            //var list = _blogService.GetSearchInBlog(Convert.ToInt64(SessionHelper.CurrentSession.SchoolId), searchString).Distinct().ToList();
            var result = _chatService.GetSearchInChat(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), searchString, Convert.ToInt32(SessionHelper.CurrentSession.UserTypeId), Convert.ToInt32(userId)).Take(50).ToList();
            // result = result.Select(s => s.ToUserProfileImage.Replace("XX", "1")).ToList();
            result = (from N in result
                      where N.ToUser.ToUpper().Contains(searchString.ToUpper())
                      select new Chat
                      {
                          ToUserId = N.ToUserId,
                          ToUser = N.ToUser,
                          ToUserProfileImage = N.ToUserProfileImage.Replace(@"\", @"/"),
                          IsOnline = N.IsOnline
                      }).ToList();

            //  if (result.Count > 0) result[0].MessageType = searchString;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MyGroupSearch(string searchString)
        {
            //Note : you can bind same list from database
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;

            //var list = _blogService.GetSearchInBlog(Convert.ToInt64(SessionHelper.CurrentSession.SchoolId), searchString).Distinct().ToList();
            var result = _chatService.GetSearchInChat(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), searchString, Convert.ToInt32(SessionHelper.CurrentSession.UserTypeId), Convert.ToInt32(userId)).Take(100).ToList();
            result = (from N in result
                      where N.ToUser.ToUpper().Contains(searchString.ToUpper())
                      select N).ToList();
            //Searching records from list using LINQ query
            //var result = (from N in list
            //              where N.SearchText.ToUpper().Contains(searchString.ToUpper())
            //              select new { N.ToUserId, N.ToUser, N.ToUserProfileImage });
            return PartialView("~/Areas/SchoolInfo/Views/Chat/_GroupMember.cshtml", result);
        }
        [HttpPost]
        public async Task<ActionResult> Capture(string name)
        {
            List<string> lstFile = new List<string>();
            var fileEditList = new List<FileEdit>();
            try
            {
                string fileName = string.Empty;
                string relativePath = string.Empty;
                var files = Request.Files;
                string[] imgarray = name.Split(',');
                fileName = Guid.NewGuid() + "_" + "Camera.jpg";

                string resourceDir = Server.MapPath(Constants.ResourcesDir);
                string blogContent = Server.MapPath(Constants.ChatDir);
                string blogPath = Server.MapPath(Constants.ChatDir + "/Chat_" + "1");
                Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                Common.Helpers.CommonHelper.CreateDestinationFolder(blogContent);
                Common.Helpers.CommonHelper.CreateDestinationFolder(blogPath);
                string filePathName = Path.Combine(Server.MapPath(Constants.ChatDir + "/Chat_" + "1"), fileName);
                byte[] imageBytes = Convert.FromBase64String(imgarray[1].ToString());
                System.IO.File.WriteAllBytes(filePathName, imageBytes);
                string fname = fileName;
                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = fname.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    fname = fileName;
                }
                var extension = Path.GetExtension(fname).Replace(".", "");
                var extId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;
                double fileSizeInMb = (15360 / 1024f) / 1024f;
                fileEditList.Add(new FileEdit() { FileName = fname, PostedFile = null, ResourceFileTypeId = (short)ResourceFileTypes.SharePoint, FileTypeId = extId, FileSizeInMB = fileSizeInMb });
                List<FileEdit> fileListWithPath = new List<FileEdit>();
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    fileListWithPath = await azureHelper.UploadFilesListAsPerModuleCameraAsync(FileModulesConstants.Chatter, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList, Constants.ChatDir + "/Chat_" + "1" + "/" + fileName);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    fileListWithPath = SharePointHelper.UploadFilesListAsPerModuleCamera(ref cx, FileModulesConstants.Chatter, SessionHelper.CurrentSession.OldUserId.ToString(), fileEditList, Constants.ChatDir + "/Chat_" + "1" + "/" + fileName);
                }
                System.IO.File.Delete(filePathName);
                if (fileListWithPath.Count > 0)
                {
                    lstFile.Add(fileListWithPath[0].FilePath);
                    lstFile.Add(fileListWithPath[0].FileName);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Json(lstFile);
        }
        public bool SaveImage(string ImgStr, string ImgName)
        {
            string[] i = ImgStr.Split(',');


            String path = Server.MapPath("~/Uploads"); //Path

            //Check if directory exist
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
            }

            string imageName = ImgName + ".jpg";
            string converted = i[1].Replace('-', '+');
            converted = converted.Replace('_', '/');
            //set the image path
            string imgPath = Path.Combine(path, imageName);

            byte[] imageBytes = Convert.FromBase64String(i[1].ToString());

            System.IO.File.WriteAllBytes(imgPath, imageBytes);

            return true;
        }

        public ActionResult GetGroup()
        {
            var result = _chatService.GetGroupLatest();
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetGroupMemberByGroupId(string groupId)
        {
            List<InviteChat> model = new List<InviteChat>();
            if (!string.IsNullOrEmpty(groupId))
            {
                var result = _chatService.GetGroupMemberList(Convert.ToInt32(groupId), Convert.ToInt32(SessionHelper.CurrentSession.SchoolId)).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetGroupName(string groupId)
        {

            if (!string.IsNullOrEmpty(groupId))
            {
                var result = _chatService.GetGroupName(Convert.ToInt32(groupId));
                result = result.Replace("\"", "");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetPermission(string chattype)
        {
            bool result = true;
            if (!string.IsNullOrEmpty(chattype))
            {

                if (_userChatPermission.Where(m => m.PermissionId == Convert.ToInt32(chattype)).ToList().Count > 0)
                {
                    result = false;
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DeleteGroupMember(string grpId, string grpUserid, string GroupUserName)
        {

            var groupName = _chatService.GetGroupName(Convert.ToInt32(grpId));
            groupName = groupName.Replace("\"", "");
            var newGroupName = string.Empty;

            var strNameArr = groupName.Split(',');
            if (strNameArr.Length > 0)
            {
                int keyIndex = Array.FindIndex(strNameArr, w => w.ToString() == GroupUserName);
                strNameArr = strNameArr.Where((source, index) => index != keyIndex).ToArray();

                for (int i = 0; i < strNameArr.Length; i++)
                {
                    if (string.IsNullOrEmpty(newGroupName))
                    {
                        newGroupName = strNameArr[i];
                    }
                    else
                    {
                        newGroupName = newGroupName + "," + strNameArr[i];
                    }
                }
            }
            else
            {
                newGroupName = groupName;
            }
            InviteChat inviteChatModel = new InviteChat();

            inviteChatModel.GroupUserId = Convert.ToInt32(grpUserid);
            inviteChatModel.UpdatedOn = DateTime.Now;
            inviteChatModel.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            inviteChatModel.SchoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
            inviteChatModel.InviteGroupId = Convert.ToInt32(grpId);
            inviteChatModel.InvitedOn = DateTime.Now;
            inviteChatModel.GroupJoiningTime = DateTime.Now;
            //inviteChatModel.InviteGroupName = newGroupName;
            if (!string.IsNullOrEmpty(grpId))
            {
                var result = _chatService.UpdateInviteChat(inviteChatModel);

                inviteChatModel.UpdatedOn = DateTime.Now;
                inviteChatModel.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
                inviteChatModel.SchoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
                inviteChatModel.InviteGroupId = Convert.ToInt32(grpId);
                inviteChatModel.InviteGroupName = newGroupName;
                //inviteChatModel.GroupJoiningTime = DateTime.Now;
                inviteChatModel.InvitedOn = DateTime.Now;
                inviteChatModel.GroupJoiningTime = DateTime.Now;
                if (!string.IsNullOrEmpty(grpId))
                {
                    var update = _chatService.UpdateInviteGroupChatName(inviteChatModel);
                    update = update.Replace("\"", "");
                    return Json(update, JsonRequestBehavior.AllowGet);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }


        }
        public ActionResult UpdateInviteGroupName(string grpId, string groupName)
        {
            InviteChat inviteChatModel = new InviteChat();

            //inviteChatModel.GroupUserId = Convert.ToInt32(grpUserid);
            inviteChatModel.UpdatedOn = DateTime.Now;
            inviteChatModel.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            inviteChatModel.SchoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
            inviteChatModel.InviteGroupId = Convert.ToInt32(grpId);
            inviteChatModel.InviteGroupName = groupName;
            //inviteChatModel.GroupJoiningTime = DateTime.Now;
            inviteChatModel.InvitedOn = DateTime.Now;
            inviteChatModel.GroupJoiningTime = DateTime.Now;
            if (!string.IsNullOrEmpty(grpId))
            {
                var result = _chatService.UpdateInviteGroupChatName(inviteChatModel);
                result = result.Replace("\"", "");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult DeleteChatConversation(string groupId, string toUserId)
        {
            if (String.IsNullOrEmpty(groupId)) { groupId = "0"; }
            //Note : you can bind same list from database
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var result = _chatService.DeleteChatConversation(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), Convert.ToInt32(groupId), Convert.ToInt32(userId), Convert.ToInt32(toUserId));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRecentChat(int? pageNum, int toUserById, int groupId)
        {
            pageNum = pageNum ?? 0;
            ViewBag.IsEndOfRecords = false;
            List<Chat> result = new List<Chat>();
            if (Request.IsAjaxRequest())
            {
                result = GetRecordsForPage(pageNum.Value, toUserById, groupId);
                ViewBag.IsEndOfRecords = (result.Any());
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public List<Chat> GetRecordsForPage(int pageNum, int toUserById, int groupId)
        {
            var result = _chatService.GetChatHistorybyUserId(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), Convert.ToInt32(SessionHelper.CurrentSession.Id), Convert.ToInt32(toUserById), Convert.ToInt32(groupId), pageNum).ToList();
            return result;
        }

    }
}