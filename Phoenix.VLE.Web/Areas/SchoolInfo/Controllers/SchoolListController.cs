﻿using Phoenix.Common;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    [Authorize]
    public class SchoolListController : BaseController
    {
        private ISchoolService _schoolService;

        public SchoolListController(ISchoolService schoolService)
        {
            _schoolService = schoolService;
        }

        // GET: ListCategories/SchoolList
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadSchoolGrid()
        {
            var schools = _schoolService.GetSchoolList();
            var browseUserLink = "<a href='#' onclick='schools.openUserListPage({0})' class='table-action-text-link'>" + ResourceManager.GetString("School.SchoolList.BrowseUser")+"</a>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in schools
                          select new
                          {
                              Actions = string.Format(HtmlHelperExtensions.ActionButtonsHtmlTemplate, string.Format(browseUserLink, item.SchoolId)),
                              item.SchoolName,
                              item.IsActive
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
    }
}