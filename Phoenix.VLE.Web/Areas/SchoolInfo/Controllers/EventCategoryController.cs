﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class EventCategoryController : BaseController
    {
        private readonly IEventCategoryService _eventCategoryService;
        EventCategoryService eventCategoryService = new EventCategoryService();
        private IUserPermissionService _userPermissionService;
        public EventCategoryController(IEventCategoryService eventCategoryService, IUserPermissionService userPermissionService)
        {
            _eventCategoryService = eventCategoryService;
            _userPermissionService = userPermissionService;
        }
        // GET: SchoolInfo/EventCategory
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult LoadEventCategoryGrid()
        {
            bool result = false;
            string actionButtonsHtmlTemplate = "";
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            result = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_EventCategory.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            var user = Helpers.CommonHelper.GetLoginUser();
            var eventCategory = _eventCategoryService.GetEventCategoriesBySchoolId();

            if(result == true)
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='eventCategory.editEventCategoryPopup($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='eventCategory.deleteEventCategory($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.DeleteRecord") + "'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a></div>";
            else
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='eventCategory.editEventCategoryPopup($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a></div>";

            var dataList = new object();
            dataList = new
            {
                aaData = (from item in eventCategory
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.Id),
                              item.Name,
                              item.Description,
                              ColorCode = (item.ColorCode != string.Empty) ? "<span class='d-inline-block p-3' style='background: " + item.ColorCode + "' ></span>": ResourceManager.GetString("Shared.Labels.NoColor"),
                              IsActive =item.IsActive? ResourceManager.GetString("Shared.Labels.Active"): ResourceManager.GetString("Shared.Labels.DeActive")
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InitAddEditEventCategoryForm(int? id)
        {
            bool result = false;
            var model = new EventCategoryEdit();
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            result = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_EventCategory.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            ViewBag.IsUpdatePermission = result;
            if (id.HasValue)
            {
                var category = _eventCategoryService.GetEventCategoryById(id.Value);
                EntityMapper<EventCategory, EventCategoryEdit>.Map(category, model);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
            }

            return PartialView("_AddEditEventCategory", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveEventCategoryData(EventCategoryEdit model)
        {
            if (!CurrentPagePermission.CanEdit)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            }
            model.ParseJsonToXml("EventCategoryXml");

            var result = _eventCategoryService.UpdateEventCategoryData(model);
            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "Think Box Catagory")), JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteEventCategoryData(int id)
        {
            var result = _eventCategoryService.DeleteEventCategoryData(id);
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult IsEventCategoryAvailable(string eventCategory, int id)
        {
            var result = _eventCategoryService.CheckEventCategoryAvailable(eventCategory, id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }


    }
}