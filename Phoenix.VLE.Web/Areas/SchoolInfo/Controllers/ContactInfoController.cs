﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    [Authorize]
    public class ContactInfoController : BaseController
    {
        private readonly IContactInfoService _contactInfoService;
        private readonly ICountryService _countryService;
        public ContactInfoController(IContactInfoService contactInfoService,
                                     ICountryService countryService)
        {
            _contactInfoService = contactInfoService;
            _countryService = countryService;
        }

        // GET: School/ContactInfo
        public ActionResult Index()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var result = _contactInfoService.GetContactInfoBySchoolId((int)user.SchoolId).FirstOrDefault();
            var model = EntityMapper<ContactInfo, ContactInfoEdit>.Map(result);
            var countryList = SelectListHelper.GetSelectListData(ListItems.Countries);
            var test=countryList.FirstOrDefault(x => x.ItemName == "Select Country");
            model.CountryList.AddRange(countryList);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUpdateContactInfo(ContactInfoEdit contactInfoEdit)
        {
            if (!CurrentPagePermission.CanEdit)
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            var result = false;
            if (!ModelState.IsValid)
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);

            contactInfoEdit.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;

            result = _contactInfoService.AddOrUpdateContactInfoData(contactInfoEdit);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SendDemoRequestMail(SendMailView model)
        {
            StreamReader reader = new StreamReader(Server.MapPath(Constants.RequestDemoEmailTemplate), Encoding.UTF8);
            string emailHtmlTempate = reader.ReadToEnd();
            emailHtmlTempate = emailHtmlTempate.Replace("{MESSAGE}", model.MSG);
            emailHtmlTempate = emailHtmlTempate.Replace("{ORGANIZATION}", model.Organization);
            emailHtmlTempate = emailHtmlTempate.Replace("{EMAILADDRESS}", model.FROM_EMAIL);
            emailHtmlTempate = emailHtmlTempate.Replace("{NAME}", model.SenderName);
            emailHtmlTempate = emailHtmlTempate.Replace("{CONTACTNUMBER}", model.ContactNumber);
            SendMailView sendEmailNotificationView = new SendMailView();
            sendEmailNotificationView.FROM_EMAIL = model.FROM_EMAIL;
            sendEmailNotificationView.TO_EMAIL = "support@phoenixclassroom.com";
            sendEmailNotificationView.SUBJECT = "Lead - Phoenix Classroom";
            sendEmailNotificationView.MSG = emailHtmlTempate;
            sendEmailNotificationView.LOG_TYPE = "DemoRequest";
            sendEmailNotificationView.LOG_USERNAME = model.SenderName;

            sendEmailNotificationView.STU_ID = "11111";
            sendEmailNotificationView.LOG_PASSWORD = "tt";
            sendEmailNotificationView.PORT_NUM = "123";
            sendEmailNotificationView.EmailHostNew = "false";
            ICommonService _commonService = new CommonService();
            var operationalDetails = _commonService.SendEmail(sendEmailNotificationView);
            return Json(operationalDetails, JsonRequestBehavior.AllowGet);
        }
    }
}