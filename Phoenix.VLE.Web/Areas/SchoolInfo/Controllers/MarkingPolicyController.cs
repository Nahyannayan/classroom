﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


    namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
    {
        public class MarkingPolicyController : Controller
        {
            private ISchoolService _schoolService;
            private readonly IMarkingPolicyService _MarkingPolicyService;
          
            public MarkingPolicyController(ISchoolService schoolService,  IMarkingPolicyService MarkingPolicyService)
            {
                _schoolService = schoolService;
               
                _MarkingPolicyService = MarkingPolicyService;
                
            }
            // GET: SchoolInfo/MarkingPolicy
            public ActionResult Index()
            {
                return View();
            }
            public ActionResult AddEditMarkingPolicy(Int64? MarkingPolicyId)
            {
               
                var model = new MarkingPolicyEdit();

                if (MarkingPolicyId.HasValue)
                {
                    var MarkingPolicy = _MarkingPolicyService.GetMarkingPolicyDetails(MarkingPolicyId.Value, SessionHelper.CurrentSession.Id);
                    EntityMapper<MarkingPolicy, MarkingPolicyEdit>.Map(MarkingPolicy, model);
                    model.IsAddMode = false;
                }
                else
                {
                    model.IsAddMode = true;
                    model.CreatedBy = SessionHelper.CurrentSession.Id;
                   
                }
                return PartialView("_AddEditMarkingPolicy", model);
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult SaveMarkingPolicyDetails(MarkingPolicyEdit model)
            {
               
                model.CreatedBy = SessionHelper.CurrentSession.Id;
                model.SchoolId = Convert.ToString(SessionHelper.CurrentSession.SchoolId);
                var result = _MarkingPolicyService.UpdateMarkingPolicyData(model);
                
                return Json(result, JsonRequestBehavior.AllowGet);
            }
          
            public ActionResult LoadMarkingPolicys()
            {
                var MarkingPolicyList = _MarkingPolicyService.GetMarkingPolicys(SessionHelper.CurrentSession.Id, (int)SessionHelper.CurrentSession.SchoolId);
                var actionButtonsHtmlTemplate = "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='MarkingPolicy.editMarkingPolicy($(this),{0})'><i class='fas fa-pencil-alt'></i></button>" +
                                                 "<button class='table-action-icon-btn' onclick='MarkingPolicy.deleteMarkingPolicy($(this), {0});' title='Delete record'><i class='far fa-trash-alt btnDelete'></i></button></div>";
            var ColorCodeHtmlTemplate = "<div style='height:20px;width:20px;background-color:{0}'>&nbsp;</div>";
            var dataList = new object();

                dataList = new
                {
                    aaData = (from item in MarkingPolicyList
                              select new
                              {
                                  Actions =  string.Format(actionButtonsHtmlTemplate, item.MarkingPolicyId) ,
                                  item.Symbol,
                                  item.Description,
                                  item.Weight,
                                  ColorCode = string.Format(ColorCodeHtmlTemplate, item.ColorCode)
                              }).ToArray()
                };

                return Json(dataList, JsonRequestBehavior.AllowGet);
            }
            public ActionResult DeleteMarkingPolicy(int id)
            {
                var MarkingPolicy = _MarkingPolicyService.GetMarkingPolicyDetails(id, SessionHelper.CurrentSession.Id);
                MarkingPolicy.DeletedBy = SessionHelper.CurrentSession.Id;
                var result = _MarkingPolicyService.DeleteMarkingPolicyData(MarkingPolicy);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

    }
