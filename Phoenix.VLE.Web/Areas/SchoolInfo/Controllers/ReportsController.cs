﻿using OfficeOpenXml;
using PdfSharp;
using PdfSharp.Pdf;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.Extensions;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using TheArtOfDev.HtmlRenderer.Core;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class ReportsController : Controller
    {
        private readonly ISchoolService _schoolService;
        private readonly IAssignmentService _assignmentService;
        private readonly IMyPlannerService _myPlannerService;
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly IFileService _filesService;

        public ReportsController(ISchoolService schoolService, IAssignmentService assignmentService, IMyPlannerService myPlannerService, ISchoolGroupService schoolGroupService, IFileService fileService)
        {
            this._schoolService = schoolService;
            this._assignmentService = assignmentService;
            this._myPlannerService = myPlannerService;
            this._schoolGroupService = schoolGroupService;
            this._filesService = fileService;
        }
        // GET: SchoolInfo/Reports
        public ActionResult Index()
        {
            DateTime endDate;
            long userId = SessionHelper.CurrentSession.Id;
            long schoolId = SessionHelper.CurrentSession.SchoolId;
            //long? groupId = null;
            var todayDate = DateTime.UtcNow;
            endDate = new DateTime(todayDate.Year, todayDate.Month, todayDate.Day);
            var startDate = endDate.AddDays(-15);
            var fromDate = startDate.ToString("yyyy-MM-dd");
            var toDate = endDate.ToString("yyyy-MM-dd");

            //commented the all reports get code - By Mukund Patil on 07 sept 2020
            //var reports = this._schoolService.GetReportDashboard(schoolId, startDate, endDate, userId, groupId);

            if (SessionHelper.CurrentSession.UserTypeId == 5)
            {
                var schoolList = _schoolService.GetAdminSchoolList();
                if (schoolList.Count() > 0)
                    schoolId = schoolId == 0 ? schoolList.FirstOrDefault().SchoolId : schoolId;

                ViewBag.SchoolList = schoolList;
            }

            var loginReportDetails = _schoolService.GetSchoolLoginReport(userId, fromDate, toDate, schoolId);

            if (!SessionHelper.CurrentSession.IsTeacher())
            {
                var teacherList = this._schoolService.GetSchoolTeachersBySchoolId(SessionHelper.CurrentSession.SchoolId);
                if (teacherList.Count() > 0)
                    ViewBag.TeacherList = new SelectList(teacherList, "Id", "TeacherName", teacherList.FirstOrDefault().Id);
                else
                    ViewBag.TeacherList = new SelectList(teacherList, "Id", "TeacherName");
            }

            //Session["reports"] = reports;

            return View(loginReportDetails);
        }

        public ActionResult Dashboard()
        {
            Request.RequestContext.HttpContext.Server.TransferRequest("/SchoolInfo/Reports/index");
            return Content("success");

        }
        [HttpPost]
        public JsonResult ReportsData(ReportsRequest reportsRequest)
        {
            var schoolId = reportsRequest.SchoolId != 0 ? reportsRequest.SchoolId : null;
            var reports = _schoolService.GetReportDashboard(schoolId, reportsRequest.StartDate, reportsRequest.EndDate, reportsRequest.UserId, reportsRequest.GroupId);

            Session["reports"] = reports;
            return Json(reports, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AssignmentReportsData(ReportsRequest reportsRequest)
        {
            var schoolId = reportsRequest.SchoolId != 0 ? reportsRequest.SchoolId : null;
            var reports = _schoolService.GetAssignmentReportCardData(schoolId, reportsRequest.StartDate, reportsRequest.EndDate, reportsRequest.UserId, reportsRequest.GroupId);

            Session["AssignmentReports"] = reports;
            return Json(reports, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSchoolLoginReport(long userId, string startDate, string endDate, long schoolId)
        {
            var loginReportDetails = _schoolService.GetSchoolLoginReport(userId, startDate, endDate, schoolId);
            return PartialView("_SchoolLoginReport", loginReportDetails);
        }

        public ActionResult GroupAssignmentReport(string fromDate, string toDate, string userId, string schoolId)
        {
            //DateTime endDate;

            //long schoolId = SessionHelper.CurrentSession.SchoolId;
            //long userId = SessionHelper.CurrentSession.Id;
            //var todayDate = DateTime.Now;
            //endDate = new DateTime(todayDate.Year, todayDate.Month, todayDate.Day);
            //var startDate = endDate.AddDays(-15);


            DateTime startDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(fromDate));
            DateTime endDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(toDate));
            long _userId = EncryptDecryptHelper.DecodeBase64(userId).ToInteger();
            long _schoolId = EncryptDecryptHelper.DecodeBase64(schoolId).ToInteger();


            var groupAssignments = _schoolService.GetSchoolGroupAssignmentReport(_schoolId, startDate, endDate, _userId);

            if (SessionHelper.CurrentSession.UserTypeId == 5)
            {
                ViewBag.SchoolList = _schoolService.GetAdminSchoolList();
            }

            Session["GroupAssignmentsReport"] = groupAssignments;
            ViewBag.FromDate = startDate;
            ViewBag.ToDate = endDate;
            ViewBag.SchoolId = _schoolId;

            return View(groupAssignments);
        }

        public ActionResult EventsReport()
        {
            TimetableReportsRequest filterModel = new TimetableReportsRequest();
            filterModel.UserId = SessionHelper.CurrentSession.Id;
            filterModel.SchoolId = SessionHelper.CurrentSession.SchoolId;
            //long groupId = 0;
            filterModel.IsTeacher = true;
            DateTime todayDate = (DateTime)DateTime.UtcNow.ConvertUtcToLocalTime();

            var thisWeekStart = todayDate.AddDays(-(int)todayDate.DayOfWeek);
            var thisWeekEnd = thisWeekStart.AddDays(6).AddSeconds(-1);
            filterModel.StartDate = thisWeekStart.Date;
            filterModel.EndDate = thisWeekEnd.Date;

            filterModel.PageIndex = 1;
            filterModel.PageSize = 20;
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var timetableEventsData = _myPlannerService.GetTimetableEventsReport(filterModel).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var fileTypes = _filesService.GetFileTypes();
            ViewBag.FileExtension = fileTypes.Where(r => r.Extension.IndexOf("svg", StringComparison.OrdinalIgnoreCase) == -1).Select(r => r.Extension).ToList();
            ViewBag.ImageExtension = fileTypes.Where(r => r.DocumentType.Contains("Image")).Select(r => r.Extension).ToList();

            if (!SessionHelper.CurrentSession.IsTeacher())
            {
                var teacherList = this._schoolService.GetSchoolTeachersBySchoolId(SessionHelper.CurrentSession.SchoolId);
                if (teacherList.Count() > 0)
                    ViewBag.TeacherList = new SelectList(teacherList, "Id", "TeacherName", teacherList.FirstOrDefault().Id);
                else
                    ViewBag.TeacherList = new SelectList(teacherList, "Id", "TeacherName");

                var lstOfAllSchoolGroups = _schoolGroupService.GetSchoolGroupsBySchoolId(SessionHelper.CurrentSession.SchoolId).OrderBy(x => x.SchoolGroupName);
                var listOfClassGroups = _schoolGroupService.GetSchoolGroupsBySchoolId(SessionHelper.CurrentSession.SchoolId).Where(x=>x.IsBespokeGroup == false).OrderBy(x => x.SchoolGroupName);
                var listOfOtherGroups = _schoolGroupService.GetSchoolGroupsBySchoolId(SessionHelper.CurrentSession.SchoolId).Where(x => x.IsBespokeGroup == true).OrderBy(x => x.SchoolGroupName);
                if (lstOfAllSchoolGroups.Count() > 0)
                {
                    var groupedOptions = lstOfAllSchoolGroups.Select(x => new SelectListItem
                    {
                        Value = x.SchoolGroupId.ToString(),
                        Text = x.SchoolGroupName.Trim().Length > 30 ? x.SchoolGroupName.Trim().Substring(0, 29) + "..." : x.SchoolGroupName.Trim(),
                    }).ToList();
                    ViewBag.SchoolGroupList = groupedOptions;
                }
                else
                    ViewBag.SchoolGroupList = new SelectList(lstOfAllSchoolGroups, "SchoolGroupId", "SchoolGroupName");

                List<SchoolGroupType> lstSchoolGroups = new List<SchoolGroupType>
                {
                    new SchoolGroupType {  Id = 1, GroupName = ResourceManager.GetString("Shared.Groups.ClassGroup") },
                    new SchoolGroupType {  Id = 2, GroupName = ResourceManager.GetString("Shared.Groups.OtherGroups") }
                };

                var groupTypeOptions = lstSchoolGroups.Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.GroupName,
                }).ToList();
                ViewBag.GroupsType = groupTypeOptions;

                var coursesList = _schoolGroupService.GetCoursesBySchoolId(SessionHelper.CurrentSession.SchoolId).OrderBy(x => x.Title);
                if (coursesList.Count() > 0)
                {
                    var courseOptions = coursesList.Select(x => new SelectListItem
                    {
                        Value = x.CourseId.ToString(),
                        Text = x.Title.Trim().Length > 30 ? x.Title.Trim().Substring(0, 29) + "..." : x.Title.Trim(),
                    }).ToList();
                    ViewBag.SchoolCourseList = courseOptions;
                }
                else
                    ViewBag.SchoolCourseList = new SelectList(coursesList, "CourseId", "Title");

                if (listOfClassGroups.Count() > 0)
                {
                    var classGroupOptions = listOfClassGroups.Select(x => new SelectListItem
                    {
                        Value = x.SchoolGroupId.ToString(),
                        Text = x.SchoolGroupName.Trim().Length > 30 ? x.SchoolGroupName.Trim().Substring(0, 29) + "..." : x.SchoolGroupName.Trim(),
                    }).ToList();
                    ViewBag.ClassGroupList = classGroupOptions;
                }
                else
                    ViewBag.ClassGroupList = new SelectList(listOfClassGroups, "SchoolGroupId", "SchoolGroupName");

                if (listOfOtherGroups.Count() > 0)
                {
                    var otherGroupOptions = listOfOtherGroups.Select(x => new SelectListItem
                    {
                        Value = x.SchoolGroupId.ToString(),
                        Text = x.SchoolGroupName.Trim().Length > 30 ? x.SchoolGroupName.Trim().Substring(0, 29) + "..." : x.SchoolGroupName.Trim(),
                    }).ToList();
                    ViewBag.OtherGroupList = otherGroupOptions;
                }
                else
                    ViewBag.OtherGroupList = new SelectList(listOfOtherGroups, "SchoolGroupId", "SchoolGroupName");

            }

            return View(timetableEventsData);
        }

        [HttpPost]
        public JsonResult LoadTimetableEventsReportFilteredData(TimetableReportsRequest reportsRequest)
        {
            reportsRequest.SchoolId = reportsRequest.SchoolId != 0 ? reportsRequest.SchoolId : null;
            reportsRequest.UserId = SessionHelper.CurrentSession.Id;
            reportsRequest.PageIndex = 1;
            reportsRequest.PageSize = 20;
            reportsRequest.IsTeacher = true;
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var reports = _myPlannerService.GetTimetableEventsReport(reportsRequest).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            Session["TimetableEventsReport"] = reports;

            return new JsonResult()
            {
                Data = reports,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue
            };
        }

        public ActionResult LoadTimetableEventsReport()
        {
            var timetableEvents = Session["TimetableEventsReport"] as IEnumerable<WeeklyEventView>;
            return PartialView("_TimetableEventsPartial", timetableEvents);
        }

        [HttpPost]
        public JsonResult LoadLiveSessionsReportFilteredData(TimetableReportsRequest reportsRequest)
        {
            reportsRequest.SchoolId = reportsRequest.SchoolId != 0 ? reportsRequest.SchoolId : null;
            reportsRequest.UserId = SessionHelper.CurrentSession.Id;
            reportsRequest.PageIndex = 1;
            reportsRequest.PageSize = 20;
            reportsRequest.IsOnlineMeetingEvents = true;
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);

            var reports = _myPlannerService.GetLiveSessionsReport(reportsRequest).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            Session["LiveSessionsReport"] = reports;

            return new JsonResult()
            {
                Data = reports,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue
            };
        }

        public ActionResult LoadLiveSessionsReport()
        {
            var liveEvents = Session["LiveSessionsReport"] as IEnumerable<EventView>;
            return PartialView("_LiveSessionsPartial", liveEvents);
        }

        public ActionResult GetSchoolGroupsForAdminSpace(SchoolGroupsReportRequest reportsRequest)
        {
            reportsRequest.UserId = SessionHelper.CurrentSession.Id;
            reportsRequest.PageSize = 8;
            reportsRequest.IsBeSpokeGroup = (Convert.ToInt32(reportsRequest.GroupTypeIds) == 1) ? false : true;
            reportsRequest.SearchString = (reportsRequest.SearchString != null) ? reportsRequest.SearchString : "";
            return PartialView("_NewSchoolGroups", GetSchoolGroupList(reportsRequest));
        }

        private IEnumerable<Phoenix.Models.SchoolGroup> GetSchoolGroupList(SchoolGroupsReportRequest reportsRequest)
        {
            var fileModulesList = _filesService.GetFileManagementModules().ToList();
            ViewBag.ModuleDetails = fileModulesList.FirstOrDefault(r => r.ModuleName.Contains("Groups"));
            IEnumerable<Phoenix.Models.SchoolGroup> schoolGroups = new List<Phoenix.Models.SchoolGroup>();
            reportsRequest.SearchString = reportsRequest.SearchString.Replace(" /", "_");
            
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            schoolGroups = _schoolGroupService.GetSchoolGroupsForAdminSpace(reportsRequest).Result.ToList();
            SynchronizationContext.SetSynchronizationContext(syncContext);
          
            return schoolGroups;
        }

        //[HttpPost]
        //public JsonResult GetCoursesBySchoolGroupIds(CoursesRequest courseRequest)
        //{
            
        //    var syncContext = SynchronizationContext.Current;
        //    SynchronizationContext.SetSynchronizationContext(null);
        //    var courses = _schoolGroupService.GetCoursesBySchoolGroupIds(courseRequest).Result.ToList();
        //    SynchronizationContext.SetSynchronizationContext(syncContext);

        //    return Json(courses, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public JsonResult LoadGroupAssignmentReportFilteredData(ReportsRequest reportsRequest)
        {
            var schoolId = reportsRequest.SchoolId != 0 ? reportsRequest.SchoolId : null;
            var reports = _schoolService.GetSchoolGroupAssignmentReport(schoolId, reportsRequest.StartDate, reportsRequest.EndDate, reportsRequest.UserId);

            Session["GroupAssignmentsReport"] = reports;
            return Json(reports, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadGroupReport()
        {
            var groupAssignments = Session["GroupAssignmentsReport"] as IEnumerable<TableData>;
            return PartialView("_GroupReportPartial", groupAssignments);
        }

        public ActionResult LoadFilterGroupReport()
        {
            var reports = Session["AssignmentReports"] as AssignmentReportCard;
            return PartialView("_GroupReport", reports.GroupAssignments);
        }

        public ActionResult GroupChatterReport(string fromDate, string toDate, string userId, string schoolId)
        {

            DateTime startDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(fromDate));
            DateTime endDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(toDate));
            long _userId = EncryptDecryptHelper.DecodeBase64(userId).ToInteger();
            long _schoolId = EncryptDecryptHelper.DecodeBase64(schoolId).ToInteger();

            var groupChatters = _schoolService.GetGroupChatterReport(_schoolId, startDate, endDate, _userId);

            if (SessionHelper.CurrentSession.UserTypeId == 5)
            {
                ViewBag.SchoolList = _schoolService.GetAdminSchoolList();
            }

            Session["GroupChatterReport"] = groupChatters;
            ViewBag.FromDate = startDate;
            ViewBag.ToDate = endDate;
            ViewBag.SchoolId = _schoolId;

            return View(groupChatters);
        }

        [HttpPost]
        public JsonResult LoadGroupChatterReportFilteredData(ReportsRequest reportsRequest)
        {
            var schoolId = reportsRequest.SchoolId != 0 ? reportsRequest.SchoolId : null;
            var reports = _schoolService.GetGroupChatterReport(schoolId, reportsRequest.StartDate, reportsRequest.EndDate, reportsRequest.UserId);

            Session["GroupChatterReport"] = reports;
            return Json(reports, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadChatterGroupReport()
        {
            var groupChatters = Session["GroupChatterReport"] as IEnumerable<ChatterTableData>;
            return PartialView("_ChatterGroupReportPartial", groupChatters);
        }

        public ActionResult LoadFilterChatterGroupReport()
        {
            var reports = Session["reports"] as ReportDashboard;
            return PartialView("_ChatterGroupReport", reports.GroupChatters);
        }

        public ActionResult TeacherAssignmentReport(string fromDate, string toDate, string userId, string schoolId)
        {
            DateTime startDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(fromDate));
            DateTime endDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(toDate));
            long _userId = EncryptDecryptHelper.DecodeBase64(userId).ToInteger();
            long _schoolId = EncryptDecryptHelper.DecodeBase64(schoolId).ToInteger();

            var teacherAssignments = _schoolService.GetTeacherAssignmentReport(_schoolId, startDate, endDate, _userId);

            if (SessionHelper.CurrentSession.UserTypeId == 5)
            {
                ViewBag.SchoolList = _schoolService.GetAdminSchoolList();
            }

            Session["TeacherAssignmentsReport"] = teacherAssignments;
            ViewBag.FromDate = startDate;
            ViewBag.ToDate = endDate;
            ViewBag.SchoolId = _schoolId;

            return View(teacherAssignments);
        }

        [HttpPost]
        public JsonResult LoadTeacherAssignmentReportFilteredData(ReportsRequest reportsRequest)
        {
            var schoolId = reportsRequest.SchoolId != 0 ? reportsRequest.SchoolId : null;
            var reports = _schoolService.GetTeacherAssignmentReport(schoolId, reportsRequest.StartDate, reportsRequest.EndDate, reportsRequest.UserId);

            Session["TeacherAssignmentsReport"] = reports;
            return Json(reports, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadTeacherAssignmentReport()
        {
            var teacherAssignments = Session["TeacherAssignmentsReport"] as IEnumerable<TeacherAssignment>;
            return PartialView("_TeacherAssignmentReportPartial", teacherAssignments);
        }

        public ActionResult LoadFilterTeacherAssignmentReport()
        {
            var reports = Session["AssignmentReports"] as AssignmentReportCard;
            return PartialView("_TeacherAssignmentReport", reports.TeacherAssignments);
        }

        public ActionResult StudentAssignmentReport(string fromDate, string toDate, string userId, string schoolId)
        {
            DateTime startDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(fromDate));
            DateTime endDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(toDate));
            long _userId = EncryptDecryptHelper.DecodeBase64(userId).ToInteger();
            long _schoolId = EncryptDecryptHelper.DecodeBase64(schoolId).ToInteger();

            var studentAssignments = _schoolService.GetStudentAssignmentReport(_schoolId, startDate, endDate, _userId);

            if (SessionHelper.CurrentSession.UserTypeId == 5)
            {
                ViewBag.SchoolList = _schoolService.GetAdminSchoolList();
            }

            Session["StudentAssignmentsReport"] = studentAssignments;
            ViewBag.FromDate = startDate;
            ViewBag.ToDate = endDate;
            ViewBag.SchoolId = _schoolId;

            return View(studentAssignments);
        }

        [HttpPost]
        public JsonResult LoadStudentAssignmentReportFilteredData(ReportsRequest reportsRequest)
        {
            var schoolId = reportsRequest.SchoolId != 0 ? reportsRequest.SchoolId : null;
            var reports = _schoolService.GetStudentAssignmentReport(schoolId, reportsRequest.StartDate, reportsRequest.EndDate, reportsRequest.UserId);

            Session["StudentAssignmentsReport"] = reports;
            return Json(reports, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadStudentAssignmentReport()
        {
            var studentAssignments = Session["StudentAssignmentsReport"] as IEnumerable<StudentAssignment>;
            return PartialView("_StudentAssignmentReportPartial", studentAssignments);
        }

        public ActionResult LoadFilterStudentAssignmentReport()
        {
            var reports = Session["AssignmentReports"] as AssignmentReportCard;
            return PartialView("_StudentAssignmentReport", reports.StudentAssignments);
        }


        public ActionResult LoadQuizReport(long? schoolId, long userId, DateTime startDate, DateTime endDate)
        {
            var reports = _schoolService.GetReportDashboard(schoolId, startDate, endDate, userId, null);
            return Json(reports.QuizReportData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChildQuizReportData(long? schoolId, DateTime startDate, DateTime endDate, long? userId)
        {
            var reportData = _schoolService.GetChildQuizReportData(schoolId, startDate, endDate, userId);
            return PartialView("_ChildQuizReport", reportData);
        }

        public ActionResult GetQuizReportDataWithDateRange(long? userId, long? schoolId, DateTime startDate, DateTime endDate)
        {
            var quizReportData = _schoolService.GetQuizReportDataWithDateRange(userId, schoolId, startDate, endDate);
            return Json(quizReportData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AbuseReport(string fromDate, string toDate, string userId, string schoolId)
        {
            DateTime startDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(fromDate));
            DateTime endDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(toDate));
            long _userId = EncryptDecryptHelper.DecodeBase64(userId).ToInteger();
            long _schoolId = EncryptDecryptHelper.DecodeBase64(schoolId).ToInteger();

            var abuseReportData = _schoolService.GetSchoolAbuseReport(_schoolId, startDate, endDate, _userId);

            if (SessionHelper.CurrentSession.UserTypeId == 5)
            {
                ViewBag.SchoolList = _schoolService.GetAdminSchoolList();
            }

            Session["AbuseReportData"] = abuseReportData;
            ViewBag.FromDate = startDate;
            ViewBag.ToDate = endDate;
            ViewBag.SchoolId = _schoolId;

            return View(abuseReportData);
        }

        [HttpPost]
        public JsonResult LoadAbuseReportFilteredData(ReportsRequest reportsRequest)
        {
            var schoolId = reportsRequest.SchoolId != 0 ? reportsRequest.SchoolId : null;
            var reports = _schoolService.GetSchoolAbuseReport(schoolId, reportsRequest.StartDate, reportsRequest.EndDate, reportsRequest.UserId);

            Session["AbuseReportData"] = reports;
            return Json(reports, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadAbuseReport()
        {
            var abuseReportData = Session["AbuseReportData"] as IEnumerable<Data>;
            return PartialView("_AbuseReportPartial", abuseReportData);
        }

        public ActionResult LoadFilterAbuseReport()
        {
            var reports = Session["reports"] as ReportDashboard;
            return PartialView("_AbuseReport", reports.AbuseReport);
        }

        public ActionResult LoadAssignmentChart()
        {
            var reports = Session["AssignmentReports"] as AssignmentReportCard;
            List<Data> dataObj = new List<Data>();
            if (reports.Assignments.Count() > 0)
            {
                //dataObj.Add(new Data { Name = "Total", Value = reports.Assignments.FirstOrDefault().Total });
                dataObj.Add(new Data { Name = ResourceManager.GetString("Assignment.Assignment.CompletedByStudent"), Value = reports.Assignments.FirstOrDefault().CompletedByStudent });
                dataObj.Add(new Data { Name = ResourceManager.GetString("Assignment.Assignment.CompletedByTeacher"), Value = reports.Assignments.FirstOrDefault().CompletedByTeacher });
            }
            return Json(dataObj, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadAbuseChart()
        {
            var reports = Session["reports"] as ReportDashboard;

            return Json(reports.AbuseReportGraph, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetReportDetails()
        {
            GetReportDetail model = new GetReportDetail();
            DateTime endDate;
            DateTime startDate = new DateTime();
            var todayDate = DateTime.Now;
            endDate = DateTime.Now; //new DateTime(todayDate.Year, todayDate.Month, todayDate.Day,todayDate.Hour,todayDate.Minute,todayDate.Second);
            startDate = endDate.AddDays(-1);
            model = this._schoolService.GetReportDetails(startDate, endDate);

            Session["ExcelModel"] = model;
            return View(model);
        }
        [HttpPost]
        public ActionResult GetReportDetails(int id, string fromdate, string todate)
        {
            GetReportDetail GetReportDetail = new GetReportDetail();
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
            string Filtername = string.Empty;
            dynamic report = 0;
            if (SessionHelper.CurrentSession.UserTypeId == 5)
            {
                if (id == 0)
                {
                    startDate = fromdate.ToDateTime();
                    endDate = todate.ToDateTime();
                    Filtername = fromdate + "To" + todate;
                }
                else if (id == 1)
                {
                    endDate = DateTime.Now;
                    startDate = endDate.AddDays(-30);
                    Filtername = "By Month";
                }
                else if (id == 2)
                {
                    endDate = DateTime.Now;
                    startDate = endDate.AddDays(-1);
                    Filtername = "By Day";
                }
                else if (id == 3)
                {
                    endDate = DateTime.Now;
                    startDate = endDate.AddMinutes(-60);
                    Filtername = "By Hours ";
                }
                else if (id == 4)
                {
                    endDate = DateTime.Now;
                    startDate = endDate.AddMinutes(-30);
                    Filtername = "By 30Min ";
                }
                else if (id == 5)
                {
                    endDate = DateTime.Now;
                    startDate = endDate.AddMinutes(-10);
                    Filtername = "By 10Min ";
                }
                GetReportDetail = this._schoolService.GetReportDetails(startDate, endDate);
                GetReportDetail.FilterName = Filtername;
                Session["ExcelModel"] = GetReportDetail;
                return PartialView("~/Areas/SchoolInfo/Views/Reports/_Reportdetail.cshtml", GetReportDetail);

            }
            else
            {
                ViewBag.Message = "You are not Admin";

            }
            return View("~/Areas/SchoolInfo/Views/Reports/_Reportdetail.cshtml", GetReportDetail);
        }
        public ActionResult ExportExcel()
        {
            GetReportDetail GetExcelModel = new GetReportDetail();
            var Excelreport = Session["ExcelModel"];
            List<GetReportDetail> getreportlist = new List<GetReportDetail>();
            getreportlist.Add((GetReportDetail)Excelreport);
            DataTable dt = new DataTable();
            ExcelPackage excelPackage = new ExcelPackage();
            ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add(ResourceManager.GetString("School.Report.ExcelCountReport"));
            dt.Columns.AddRange(new DataColumn[2] { new DataColumn("Filters"), new DataColumn(getreportlist[0].FilterName) });
            dt.Rows.Add(ResourceManager.GetString("School.Report.AssignmentFileCount"), getreportlist[0].AssignmentFileCount);
            dt.Rows.Add(ResourceManager.GetString("School.Report.TotalAssignment"), getreportlist[0].TotalAssignmentCount);
            dt.Rows.Add(ResourceManager.GetString("School.Report.BeSpokeGroupCreated"), getreportlist[0].BespokeGroupCreated);
            dt.Rows.Add(ResourceManager.GetString("School.Report.GroupFileCount"), getreportlist[0].GroupFileCount);
            dt.Rows.Add(ResourceManager.GetString("School.Report.GroupMaster"), getreportlist[0].MasterGroupFileCount);
            dt.Rows.Add(ResourceManager.GetString("School.Report.StudentTaskFile"), getreportlist[0].StudentTaskFilesCount);
            dt.Rows.Add(ResourceManager.GetString("School.Report.StudentAssignmentFile"), getreportlist[0].StudentAssignmentFileCount);
            dt.Rows.Add(ResourceManager.GetString("School.Report.TeacherUploadedTask"), getreportlist[0].TeacherFileCount);
            dt.Rows.Add(ResourceManager.GetString("School.Report.SchoolTaskFile"), getreportlist[0].SchoolTaskFileCount);
            dt.Rows.Add(ResourceManager.GetString("School.Report.Chatters"), getreportlist[0].ChatterCount);
            dt.Rows.Add(ResourceManager.GetString("School.Report.ChattersComments"), getreportlist[0].ChatterCommentsCount);
            dt.Rows.Add(ResourceManager.GetString("School.Report.QuizFile"), getreportlist[0].QuizCount);
            dt.Rows.Add(ResourceManager.GetString("School.Report.NumberOfQuiz"), getreportlist[0].QuizFileCount);
            worksheet.Cells["A1"].LoadFromDataTable(dt, true);
            worksheet.Cells.AutoFitColumns();
            worksheet.Cells["A1:ZZ1"].Style.Font.Bold = true;
            byte[] bin = excelPackage.GetAsByteArray();
            return File(bin, "application/vnd.ms-excel", "ReportCountExcel" + DateTime.Now + ".xls");
        }
        #region Download As PDF


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadReportToPDF")]
        public ActionResult DownloadReportDataAsPDF(FormCollection form)
        {
            string pdfhtml = "";
            string pdfTitle = "";
            string downloadFileName = "";
            string reportName = form["reportName"];
            ViewBag.ReportType = "PDF";
            //var reports = Session["reports"] as ReportDashboard;
            var reports = Session["AssignmentReports"] as AssignmentReportCard;


            switch (reportName)
            {
                case "AssignmentsByGroup":
                    {
                        var groupAssignments = Session["GroupAssignmentsReport"] as IEnumerable<TableData>;
                        if (groupAssignments == null)
                            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_GroupReport", reports.GroupAssignments);
                        else
                            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_GroupReport", groupAssignments);

                        pdfTitle = ResourceManager.GetString("School.Report.AssignmentsByGroup");
                        downloadFileName = "AssignmentsByGroup.pdf";
                        break;
                    }
                //case "ChattersByGroup":
                //    {
                //        var groupChatters = Session["GroupChatterReport"] as IEnumerable<ChatterTableData>;
                //        if (groupChatters == null)
                //            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_ChatterGroupReport", reports.GroupChatters);
                //        else
                //            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_ChatterGroupReport", groupChatters);

                //        pdfTitle = ResourceManager.GetString("School.Report.ChattersByGroup");
                //        downloadFileName = "ChatterByGroup.pdf";
                //        break;
                //    }
                case "AssignmentsByTeacher":
                    {
                        var teacherAssignments = Session["TeacherAssignmentsReport"] as IEnumerable<TeacherAssignment>;
                        if (teacherAssignments == null)
                            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_TeacherAssignmentReport", reports.TeacherAssignments);
                        else
                            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_TeacherAssignmentReport", teacherAssignments);

                        pdfTitle = ResourceManager.GetString("School.Report.AssignmentsByTeacher");
                        downloadFileName = "AssignmentsByTeacher.pdf";
                        break;
                    }
                case "AssignmentsByStudent":
                    {
                        var studentAssignments = Session["StudentAssignmentsReport"] as IEnumerable<StudentAssignment>;
                        if (studentAssignments == null)
                            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_StudentAssignmentReport", reports.StudentAssignments);
                        else
                            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_StudentAssignmentReport", studentAssignments);

                        pdfTitle = ResourceManager.GetString("School.Report.AssignmentsByStudent");
                        downloadFileName = "AssignmentsByStudent.pdf";
                        break;
                    }
                    //case "AbuseReport":
                    //    {
                    //        var abuseReports = Session["AbuseReportData"] as IEnumerable<Data>;
                    //        if (abuseReports == null)
                    //            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_AbuseReport", reports.AbuseReport);
                    //        else
                    //            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_AbuseReport", abuseReports);

                    //        pdfTitle = ResourceManager.GetString("School.Report.AbuseReport");
                    //        downloadFileName = "AbuseReport.pdf";
                    //        break;
                    //    }
            }

            PdfDocument pdf = new PdfDocument();

            pdf = PdfGenerator.GeneratePdf(pdfhtml, PageSize.A4);
            pdf.Info.Title = pdfTitle;
            MemoryStream stream = new MemoryStream();
            pdf.Save(stream, false);
            byte[] file = stream.ToArray();
            stream.Write(file, 0, file.Length);
            stream.Position = 0;

            return File(stream, "application/pdf", downloadFileName);

        }

        #endregion


        #region Download data as Excel

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadReportToExcel")]
        public ActionResult DownloadReportDataAsExcelFile(FormCollection form)
        {
            string path = string.Empty;
            string downloadFileName = "";
            byte[] bytes;
            string reportName = form["reportName"];
            //var reports = Session["reports"] as ReportDashboard;
            var reports = Session["AssignmentReports"] as AssignmentReportCard;

            path = Server.MapPath("~/Content/Files/EmptyReportFile.xls");

            switch (reportName)
            {
                case "AssignmentsByGroup":
                    {
                        var groupAssignments = Session["GroupAssignmentsReport"] as IEnumerable<TableData>;
                        if (groupAssignments == null)
                            bytes = _schoolService.GenerateAssignmentsByGroupReportXLSFile(path, reports.GroupAssignments);
                        else
                            bytes = _schoolService.GenerateAssignmentsByGroupReportXLSFile(path, groupAssignments);

                        downloadFileName = "AssignmentsByGroup.xls";
                        return File(bytes, "application/vnd.ms-excel", downloadFileName);
                    }
                //case "ChattersByGroup":
                //    {
                //        var groupChatters = Session["GroupChatterReport"] as IEnumerable<ChatterTableData>;
                //        if (groupChatters == null)
                //            bytes = _schoolService.GenerateChattersByGroupReportXLSFile(path, reports.GroupChatters);
                //        else
                //            bytes = _schoolService.GenerateChattersByGroupReportXLSFile(path, groupChatters);

                //        downloadFileName = "ChattersByGroup.xls";
                //        return File(bytes, "application/vnd.ms-excel", downloadFileName);
                //    }
                case "AssignmentsByTeacher":
                    {
                        var teacherAssignments = Session["TeacherAssignmentsReport"] as IEnumerable<TeacherAssignment>;
                        if (teacherAssignments == null)
                            bytes = _schoolService.GenerateAssignmentsByTeacherReportXLSFile(path, reports.TeacherAssignments);
                        else
                            bytes = _schoolService.GenerateAssignmentsByTeacherReportXLSFile(path, teacherAssignments);

                        downloadFileName = "AssignmentsByTeacher.xls";
                        return File(bytes, "application/vnd.ms-excel", downloadFileName);
                    }
                case "AssignmentsByStudent":
                    {
                        var studentAssignments = Session["StudentAssignmentsReport"] as IEnumerable<StudentAssignment>;
                        if (studentAssignments == null)
                            bytes = _schoolService.GenerateAssignmentsByStudentReportXLSFile(path, reports.StudentAssignments);
                        else
                            bytes = _schoolService.GenerateAssignmentsByStudentReportXLSFile(path, studentAssignments);

                        downloadFileName = "AssignmentsByStudent.xls";
                        return File(bytes, "application/vnd.ms-excel", downloadFileName);
                    }
                //case "AbuseReport":
                //    {
                //        var abuseReports = Session["AbuseReportData"] as IEnumerable<Data>;
                //        if (abuseReports == null)
                //            bytes = _schoolService.GenerateAbuseReportXLSFile(path, reports.AbuseReport);
                //        else
                //            bytes = _schoolService.GenerateAbuseReportXLSFile(path, abuseReports);

                //        downloadFileName = "AbuseReport.xls";
                //        return File(bytes, "application/vnd.ms-excel", downloadFileName);
                //    }
                default:
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        #endregion
    }
}