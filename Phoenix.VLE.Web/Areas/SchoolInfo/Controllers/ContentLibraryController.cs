﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class ContentLibraryController : BaseController
    {
        private readonly IContentLibraryService _contentLibraryService;
        ContentLibraryService contentLibraryService = new ContentLibraryService();
        public ContentLibraryController(IContentLibraryService contentLibraryService)
        {
            _contentLibraryService = contentLibraryService;
        }
        // GET: SchoolInfo/ContentLibrary
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult LoadContentLibraryGrid()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var content = _contentLibraryService.GetContentLibrary((int)SessionHelper.CurrentSession.SchoolId);
            var actionButtonsHtmlAssignTemplate = CurrentPagePermission.CanEdit ? "<div class='tbl-actions custom-checkbox'><input type='checkbox' title='"+ ResourceManager.GetString("ContentLibrary.ContentResource.Approve") + "' {1} class='custom-control-input custom-control-inline' id='assign_{0}' data-action='selectAll' onchange='contentProvider.assignContentLibrary($(this),{0})'><label class='custom-control-label text-medium mt-2 mb-0' for='assign_{0}'></label></div>":String.Empty;
            var actionButtonsHtmlApproveTemplate = CurrentPagePermission.CanEdit ? "<div class='tbl-actions custom-checkbox'><input type='checkbox' title='" + ResourceManager.GetString("ContentLibrary.ContentResource.Approve") + "' {0} class='custom-control-input custom-control-inline' id='approve_{1}' data-action='selectAll' onchange='contentProvider.changeContentLibraryStatus($(this),{1})'><label class='custom-control-label text-medium mt-2 mb-0' for='approve_{1}'></label></div>" : String.Empty;
            //var actionButtons = $"<div class='tbl-actions'><a class='ml-2' data-toggle='tooltip' data-placement='top' title='' data-original-title='" + ResourceManager.GetString("Shared.Buttons.Download") + "' onclick='globalFunctions.downloadSharepointFile('ResourceLibrary', \"{0}\" , '" + SessionHelper.CurrentSession.SchoolCode + "' ,'" + SessionHelper.CurrentSession.IsParent() + "','3')'><i class='fa fa-download'></i></button></div>";
            var actionButtons = $"<div class='tbl-actions'><a class='ml-2 content-library download-file' data-schoolcode='" + SessionHelper.CurrentSession.SchoolCode + "' data-filepath='##FILEPATH##' data-toggle='tooltip' data-placement='top' title='' data-original-title='" + ResourceManager.GetString("Shared.Buttons.Download") + "'><i class='fa fa-download'></i></button></div>";
            //var actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='ml - 2' data-toggle='tooltip' data-placement='top' title='' data-original-title='" + ResourceManager.GetString("Shared.Buttons.Download") + "' onclick='globalFunctions.downloadSharepointFile('ResourceLibrary','" + (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage ? Phoenix.VLE.Web.Helpers.AzureBlobStorageHelper.GetBlobUri(item.ContentImage) : item.ContentImage) + "', '" + SessionHelper.CurrentSession.SchoolCode + "', '" + @SessionHelper.CurrentSession.IsParent() + "','3')'><i class='fa fa-download'></i></button></div>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in content
                          select new
                          {
                              //ContentFile = Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage ? Phoenix.VLE.Web.Helpers.AzureBlobStorageHelper.GetBlobUri(item.ContentImage) : item.ContentImage,
                              Actions = string.IsNullOrEmpty(item.ContentImage) ? "" : actionButtons.Replace("##FILEPATH##", (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage ? Phoenix.VLE.Web.Helpers.AzureBlobStorageHelper.GetBlobUri(item.ContentImage) : item.ContentImage)),
                              Assign = item.IsApproved?String.Format(actionButtonsHtmlAssignTemplate, item.ContentId,item.IsAssign ? "checked='checked'" : ""):String.Empty,
                              Approve = item.SchoolId==(int)SessionHelper.CurrentSession.SchoolId?String.Format(actionButtonsHtmlApproveTemplate, item.IsApproved?"checked='checked'":"", item.ContentId) :"",
                              //ContentName = String.IsNullOrEmpty(item.RedirectUrl) ? item.ContentName : HtmlHelperExtensions.LocalizedLinkButtons($"<a  href='{item.RedirectUrl}' target='_blank' class='table-action-text-link' title='{item.ContentName}' '>{item.ContentName}</a>"),
                              item.ContentName,
                              item.Description,
                              SubjectName = GetSubjectNameById(item.ContentId.ToInteger()),
                              item.SchoolName,
                              Status = item.IsApproved ? ResourceManager.GetString("ContentLibrary.Resources.Approved") : ResourceManager.GetString("ContentLibrary.Resources.InProcess"),
                              CreatedOn = item.CreatedOn.ToFormatedDate()
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadContentProviderGrid()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var content = _contentLibraryService.GetContentProvider((int)SessionHelper.CurrentSession.SchoolId,"");
            var actionButtonsHtmlTemplate = CurrentPagePermission.CanEdit ? "<div class='custom-control custom-checkbox mb-2 permission-checkbox'><input type='checkbox' {1} title='" + ResourceManager.GetString("ContentLibrary.ContentResource.Approve") + "' class='custom-control-input custom-control-inline' id='select_{0}' data-action='selectAll' onchange='contentProvider.assignContentLibrary($(this),{0})'><label class='custom-control-label' for='select_{0}'></label></div>" : String.Empty;
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in content
                          select new
                          {
                              Assign = String.Format(actionButtonsHtmlTemplate, item.ContentId, item.IsAssign ? "checked='checked'" : ""),
                              item.ContentName,
                              CreatedOn = item.CreatedOn.ToFormatedDate()
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        private string GetSubjectNameById(int ContentId)
        {
            string subject = String.Empty;
            var subjects = _contentLibraryService.getSubjectsByContentId(ContentId);
            foreach (var n in subjects)
            {
                subject = subject + n.SelectedSubjectName + ", ";
            }
            return String.IsNullOrEmpty(subject) ? "" : subject.Remove(subject.Length - 2);
        }
        public ActionResult InitAddContentResourceForm(int? id)
        {
            var model = new ContentEdit();
            if (id.HasValue)
            {
                var resource = _contentLibraryService.getContentResourceById(id.Value);
                var subjects = _contentLibraryService.getSubjectsByContentId(id.Value);
                resource.SubjectId = subjects.Select(m => m.SelectedSubjectId.ToString()).ToList();
                EntityMapper<Content, ContentEdit>.Map(resource, model);
            }
            else
            {
                model.IsAddMode = true;
            }
            ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName", SessionHelper.CurrentSession.SchoolId);
            return PartialView("_AddEditContentResource", model);
        }
        [HttpPost]
        public ActionResult InsertUpdateContentResource(ContentEdit model, HttpPostedFileBase resourceFile)
        {
            if (resourceFile != null)
            {
                model.ContentImage = UploadContentResource(resourceFile);
            }
            foreach (var i in model.SubjectIds)
            {
                var m = i.Split(',');
            }
            var result = _contentLibraryService.InsertUpdateContentResource(model);
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }
        private string UploadContentResource(HttpPostedFileBase resourceFile)
        {
            string GUID = Guid.NewGuid().ToString();
            string filePath = String.Empty;
            string strFile = String.Format("{0}{1}", GUID, Path.GetExtension(resourceFile.FileName));
            //string strFile = suggestionFile.FileName;
            string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
            string content = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir + "/Content";
            filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir + "/Content" + "/ContentResourceFiles";
            Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
            Common.Helpers.CommonHelper.CreateDestinationFolder(content);
            Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
            filePath = Path.Combine(filePath, strFile);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            resourceFile.SaveAs(filePath);
            return strFile;
        }
        [HttpPost]
        public ActionResult DeleteContentResourceData(int id)
        {
            var result = _contentLibraryService.DeleteContentResourceData(id);
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ChangeContentLibraryStatus(int id, bool status)
        {
            var result = _contentLibraryService.ChangeContentLibraryStatus(id, status);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AssignContentResourceToSchool(int id, bool status)
        {
            var result = _contentLibraryService.AssignContentResourceToSchool(id, status);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public string GetFilePath(string fileName)
        {
            string resourceDir = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir;
            string content = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content";
            return PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content" + "/ContentResourceFiles/" + fileName;
        }
        public void DownloadAttachment(string fileName)
        {
            string resourceDir = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir;
            string content = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content";
            string path = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content" + "/SuggestionFiles/" + fileName;
            var webClient = new WebClient();
            byte[] FileBytes = webClient.DownloadData(path);
            Response.ContentType = GetMimeType(path);
            Response.BufferOutput = true;
            Response.AppendHeader("Content-Disposition", "Attachment; Filename=" + System.IO.Path.GetFileName(path) + "");
            Response.AddHeader("Last-Modified", DateTime.Now.ToLongDateString());
            Response.BinaryWrite(FileBytes);
            Response.Flush();
            Response.End();
        }
        private string GetMimeType(string filePath)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(filePath).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }

    }
}