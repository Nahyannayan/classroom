﻿using Phoenix.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Models;
using Phoenix.VLE.Web.ViewModels;
using Phoenix.VLE.Web.Services;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;

namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    [Authorize]
    public class SchoolsController : BaseController
    {
        private readonly ISchoolService _schoolService;
        public SchoolsController(ISchoolService schoolService)
        {
            _schoolService = schoolService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllSchools(int pageIndex = 1, string searchString = "")
        {
            IEnumerable<SchoolInformation> lst = _schoolService.GetAllBusinesUnitSchools(pageIndex, 10, searchString);
            var model = new Pagination<SchoolInformation>();
            if (lst.Any())
            {
                model = new Pagination<SchoolInformation>(pageIndex, 10, lst.ToList(), lst.FirstOrDefault().TotalCount);
            }
            model.LoadPageRecordsUrl = "/SchoolInfo/Schools/GetAllSchools?pageIndex={0}&searchString=" + searchString;
            model.PageIndex = pageIndex;
            return PartialView("_SchoolBusinessUnit", model);
        }

        [HttpPost]
        public ActionResult UpdateSchoolClassroomStatus(long schoolId)
        {
            var op = new OperationDetails();
            if (!SessionHelper.CurrentSession.IsAdmin)
            {
                op.Success = false;
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }

            bool result = _schoolService.UpdateSchoolClassroomStatus(schoolId);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConfigureSessions()
        {
            var sessionStatus = _schoolService.GetSchoolSessionStatus(SessionHelper.CurrentSession.SchoolId);
            return View("_ConfigureSessions", sessionStatus);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSchoolSessionStatus(SessionConfiguration model)
        {
            var op = new OperationDetails();
            if (!SessionHelper.CurrentSession.IsAdmin)
            {
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            model.SchoolId = SessionHelper.CurrentSession.SchoolId;
            var result = _schoolService.SaveSchoolSessionStatus(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        #region FOR DEPARTMENT
        [HttpPost]
        public ActionResult DepartmentCourseCU(Department model)
        {
            var op = new OperationDetails();
            if (!SessionHelper.CurrentSession.IsAdmin)
            {
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var result = _schoolService.DepartmentCourseCU(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}