﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Phoenix.VLE.Web.Areas.SchoolInfo.Controllers
{
    public class SchoolBadgeController : BaseController
    {
        private ISchoolService _schoolService;
        private List<VLEFileType> _fileTypeList;
        private readonly IFileService _fileService;
        private readonly ISchoolBadgeService _schoolBadgeService;

        public SchoolBadgeController(ISchoolService schoolService, IFileService fileService, ISchoolBadgeService schoolBadgeService, IUserPermissionService userPermissionService)
        {
            _schoolService = schoolService;
            _fileService = fileService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
            _schoolBadgeService = schoolBadgeService;

        }
        // GET: SchoolInfo/SchoolBadge
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddEditSchoolBadge(Int64? BadgeId)
        {
            Session["BadgeFiles"] = null;
            var model = new SchoolBadgeEdit();

            ViewBag.FileExtension = _fileTypeList.Where(m => m.DocumentType == "Image" || m.DocumentType == "VectorGraphics").Select(r => r.Extension).ToList();

            if (BadgeId.HasValue)
            {
                var schoolBadge = _schoolBadgeService.GetSchoolBadgeDetails(BadgeId.Value, SessionHelper.CurrentSession.Id);
                EntityMapper<SchoolBadge, SchoolBadgeEdit>.Map(schoolBadge, model);
                model.IsAddMode = false;
            }
            else
            {
                model.IsAddMode = true;
                model.CreatedBy = SessionHelper.CurrentSession.Id;
                model.DisplayOrder = _schoolBadgeService.GetTopOrderForDisplayBadge(SessionHelper.CurrentSession.Id) + 1;
            }
            return PartialView("_AddEditSchoolBadge", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSchoolBadgeDetails(SchoolBadgeEdit model, HttpPostedFileBase BadgeFile)
        {
            var op = new OperationDetails();
            bool isBadgeAlreadyExists = IsBadgeAvailable(model.Title, model.SchoolBadgeId);
            if (isBadgeAlreadyExists)
            {
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                op.Message = string.Format(ResourceManager.GetString("SchoolBadge.Badge.BadgeAlreadyExistsMessage"), model.Title);
                return Json(op, JsonRequestBehavior.AllowGet);
            }

            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            if (BadgeFile != null)
            {

                Phoenix.Common.Helpers.CommonHelper.DeleteFile(Path.Combine(Server.MapPath("~"), model.ActualImageName));
                TaskFile tf = UploadBadgeFiles(BadgeFile);
                model.UploadedImageName = tf.FileName;
                model.ActualImageName = tf.UploadedFileName;
            }
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            model.SchoolId = Convert.ToString(SessionHelper.CurrentSession.SchoolId);
            var result = _schoolBadgeService.UpdateBadgeData(model);
            Session["BadgeFiles"] = null;
            return Json(new OperationDetails(result.Success), JsonRequestBehavior.AllowGet);
        }
        public TaskFile UploadBadgeFiles(HttpPostedFileBase BadgeFile)
        {

            TaskFile fileDetails = new TaskFile();
            string relativePath = string.Empty;

            try
            {
                HttpPostedFileBase file = BadgeFile;
                string fname = file.FileName;
                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    fname = file.FileName;
                }
                var extension = System.IO.Path.GetExtension(file.FileName);
                string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.BadgeFilesDir;
                string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.BadgeFilesDir + "/School_" + (int)SessionHelper.CurrentSession.SchoolId;
                Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                fname = Guid.NewGuid() + "_" + fname;
                relativePath = Constants.BadgeFilesDir + "/School_" + (int)SessionHelper.CurrentSession.SchoolId + "/" + fname;
                fname = Path.Combine(filePath, fname);
                file.SaveAs(fname);
                fileDetails.FileName = file.FileName;
                fileDetails.FileExtension = extension.Replace(".", "");
                fileDetails.UploadedFileName = relativePath;
            }
            catch (Exception ex)
            {
            }
            return fileDetails;
        }
        public ActionResult LoadSchoolBadges()
        {
            var BadgeList = _schoolBadgeService.GetSchoolBadges(SessionHelper.CurrentSession.Id, (int)SessionHelper.CurrentSession.SchoolId);
            var actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='schoolBadge.editSchoolBadge($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a>" +
                                             "<a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='schoolBadge.deleteSchoolBadge($(this), {0});' title='" + ResourceManager.GetString("Shared.Labels.DeleteRecord") + "'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a></div>";
            var dataList = new object();

            dataList = new
            {
                aaData = (from item in BadgeList
                          select new
                          {
                              Actions = CurrentPagePermission.CanEdit ? string.Format(actionButtonsHtmlTemplate, item.SchoolBadgeId) : string.Empty,
                              item.Title,
                              item.Description,
                              item.DisplayOrder,
                              UploadedImage = !string.IsNullOrEmpty(item.ActualImageName) ? "<img src = '" + PhoenixConfiguration.Instance.ReadFilePath + item.ActualImageName + "' class='img-thumbnail' style = 'height:40px;width:40px' />" : "",
                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public bool IsBadgeAvailable(string Title, long SchoolBadgeId)
        {
            var badgeList = _schoolBadgeService.GetBadgesBySchoolId((int)SessionHelper.CurrentSession.SchoolId);
            return badgeList.Where(e => e.SchoolBadgeId != SchoolBadgeId).Any(e => e.Title.Equals(Title, StringComparison.OrdinalIgnoreCase));
        }
        public ActionResult DeleteSchoolBadge(int id)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var schoolBadge = _schoolBadgeService.GetSchoolBadgeDetails(id, SessionHelper.CurrentSession.Id);
            schoolBadge.DeletedBy = SessionHelper.CurrentSession.Id;
            var result = _schoolBadgeService.DeleteBadgeData(schoolBadge);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }

}
