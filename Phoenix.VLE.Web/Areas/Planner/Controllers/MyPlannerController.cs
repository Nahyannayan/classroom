﻿using Newtonsoft.Json;
using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Logger;
using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Planner.Controllers
{
    public class MyPlannerController : BaseController
    {
        private readonly IMyPlannerService _myPlannerService;
        private readonly IUserService _userService;
        private ISchoolGroupService _schoolGroupService;
        private IStudentListService _studentListService;
        private readonly IEventService _eventService;
        private readonly IEventCategoryService _eventCategoryService;
        private IUserPermissionService _userPermissionService;
        private readonly IFileService _fileService;
        private readonly IAssignmentService _assignmentService;
        private IAttendanceService _AttendanceService;

        private readonly ICalendarService _calendarService;

        public MyPlannerController(ISchoolGroupService schoolGroupService, IStudentListService studentListService, IUserService userService, IMyPlannerService myPlannerService, IEventService eventService, IEventCategoryService eventCategoryService, IUserPermissionService userPermissionService, IFileService fileService, IAssignmentService assignmentService, IAttendanceService attendanceService, ICalendarService calendarService)
        {
            _schoolGroupService = schoolGroupService;
            _userService = userService;
            _studentListService = studentListService;
            _myPlannerService = myPlannerService;
            _eventService = eventService;
            _eventCategoryService = eventCategoryService;
            _userPermissionService = userPermissionService;
            _fileService = fileService;
            _assignmentService = assignmentService;
            _AttendanceService = attendanceService;
            _calendarService = calendarService;
        }
        // GET: Planner/MyPlanner
        public ActionResult Index()
        {
            bool result = false;
            var eventCategories = _eventCategoryService.GetAllEventCategoryBySchool();
            if (eventCategories != null)
            {
                ViewBag.EventCategories = eventCategories;
                ViewBag.lstCategory = eventCategories.Select(i => new Phoenix.Common.Models.ListItem
                {
                    ItemId = i.Id.ToString(),
                    ItemName = !string.IsNullOrEmpty(i.Name) ? i.Name : i.Description
                });
            }
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            result = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_MyPlanner.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            ViewBag.IsCustomPermission = result;
            return View();
        }

        public ActionResult InitAddEditPlannerForm(string id, string sd)
        {
            var model = new EventEdit();
            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;
            List<EventCategoryView> lstEventCategory = new List<EventCategoryView>();
            id = String.IsNullOrEmpty(id) ? String.Empty : EncryptDecryptHelper.DecodeBase64(id);
            var _eventCategoryList = _eventCategoryService.GetAllEventCategoryBySchool();

            List<EventDurationView> lstEventDuration = _myPlannerService.GetEventDuration().ToList();

            if (_eventCategoryList != null)
            {
                lstEventCategory = _eventCategoryList.ToList();
                if (!(SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin))
                {
                    lstEventCategory = lstEventCategory.Where(e => e.MeetingTypeId != 1 && e.MeetingTypeId != 2).ToList();
                    lstEventCategory = lstEventCategory.Where(e => !e.Name.Equals(StringEnum.GetStringValue(EventCategoryEnum.SportsDay), StringComparison.OrdinalIgnoreCase)).ToList();
                    lstEventCategory = lstEventCategory.Where(e => !e.Name.Equals(StringEnum.GetStringValue(EventCategoryEnum.StaffMeeting), StringComparison.OrdinalIgnoreCase)).ToList();
                }
            }

            if (!SessionHelper.CurrentSession.TeamsSessionEnabled)
                lstEventCategory = lstEventCategory.Where(e => e.MeetingTypeId != (short)EventCategoryEnum.TeamsMeeting).ToList();
            if (!SessionHelper.CurrentSession.ZoomSessionEnabled)
                lstEventCategory = lstEventCategory.Where(e => e.MeetingTypeId != (short)EventCategoryEnum.ZoomMeeting).ToList();

            if (!String.IsNullOrEmpty(id))
            {
                List<EventEdit> eventDeatils = _myPlannerService.GetEventById(Convert.ToInt32(id), SessionHelper.CurrentSession.Id).ToList();
                if (eventDeatils.Count == 0)
                {
                    return RedirectToAction("NoPermission", "Error");
                }
                model = eventDeatils.FirstOrDefault();
                if (SessionHelper.CurrentSession.Id != model.CreatedBy)
                {
                    return RedirectToAction("NoPermission", "Error");
                }

                var _startDate = (DateTime)model.StartDate.ConvertUtcToLocalTime();
                var _endDate = (DateTime)model.EndDate.ConvertUtcToLocalTime();
                model.EventInternalUser = _myPlannerService.GetInternalEventUserByEventId(Convert.ToInt32(id)).ToList();
                model.EventExternalUser = _myPlannerService.GetExternalEventUserByEventId(Convert.ToInt32(id)).ToList();
                model.SelectedDate = model.StartDate.ToString("dd-MM-yyyy");
                model.StartDate = model.StartDate.Date;
                model.EndDate = model.EndDate.Date;
                model.StartTime = _startDate.ToString("hh:mm tt");
                model.EndTime = _endDate.ToString("hh:mm tt");
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
                model.SelectedDate = EncryptDecryptHelper.DecodeBase64(sd);
            }
            ViewBag.lstEventCategory = new SelectList(lstEventCategory, "Id", "Name", model.EventCategoryId);
            ViewBag.lstEventDuration = new SelectList(lstEventDuration, "Id", "Duration", model.EventTypeId);
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StepToAssignMember()
        {
            bool result = true;
            var operationDetails = new OperationDetails(result);
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageAssignee(string id, string categoryValue)
        {

            var objEvent = new EventEdit();

            var studentList = _userService.GetUserBySchool((int)(int)SessionHelper.CurrentSession.SchoolId, 1);

            var staffList = _userService.GetUserBySchool((int)(int)SessionHelper.CurrentSession.SchoolId, 3);


            objEvent.TeacherList.AddRange(staffList.Select(r => new SelectListItem
            {
                Text = $"{r.UserDisplayName} - ({r.UserName})",
                Value = r.Id.ToString()
            }).OrderBy(r => r.Text));

            objEvent.StudentList.AddRange(studentList.Select(r => new SelectListItem
            {
                Text = $"{r.UserDisplayName} - ({r.UserName})",
                Value = r.Id.ToString()
            }).OrderBy(r => r.Text));

            //thinkBox.UsersList.AddRange(staffList.Where(r => !thinkBoxUsersList.Any(a => a.UserId == r.Id)).Select(r => new SelectListItem
            //{
            //    Text = $"{r.UserDisplayName} - ({r.UserName})",
            //    Value = r.Id.ToString()
            //}).OrderBy(r => r.Text));

            //thinkBox.SelectedUsersList.AddRange(staffList.Where(r => thinkBoxUsersList.Any(a => a.UserId == r.Id)).Select(r => new SelectListItem
            //{
            //    Text = $"{r.UserDisplayName} - ({r.UserName})",
            //    Value = r.Id.ToString()
            //}).OrderBy(r => r.Text));
            //return View("~/Areas/Planner/Views/MyPlanner/ManageAssignees.cshtml", objEvent);
            //objEvent.UnAssignedMemberList = new List<ListItem>();
            var listOfMembers = _myPlannerService.GetInternalEventUserByEventId(Convert.ToInt32(id)).ToList();
            objEvent.EventExternalUser = _myPlannerService.GetExternalEventUserByEventId(Convert.ToInt32(id)).ToList();
            objEvent.EventId = Convert.ToInt32(id);
            objEvent.IsAddMode = id == "0" ? true : false;
            //= listofmembers.Select()
            //objEvent.AssignedMemberList=staffList.Where(r => listofmembers.Any(a => a.Id == r.Id)).Select(r => new Common.Models.ListItem
            //{
            //    ItemId = $"{r.Id} - ({r.Id})",
            //    ItemName = r.UserName.ToString()
            //}).OrderBy(r => r.ItemName);

            objEvent.AssignedMemberList.AddRange(listOfMembers.Select(r => new ListItem
            {
                ItemId = $"{r.Id}",
                ItemName = r.Name.ToString()
            }).OrderBy(r => r.ItemName));
            foreach (var item in listOfMembers)
            {
                objEvent.SelectedPlannerMemberId = item.Id + "," + objEvent.SelectedPlannerMemberId;
            }

            var userTypes = SelectListHelper.GetSelectListData(ListItems.UserTypes);
            var userIdToRemove = (int)UserTypes.Parent;
            var studentUserTypeId = (int)UserTypes.Student;
            var LstofSchoolGroups = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.IsTeacher());
            var userTypeToRemove = userTypes.Where(m => m.ItemId.Trim() == userIdToRemove.ToString()).FirstOrDefault();
            var studentUserTypeToRemove = userTypes.Where(m => m.ItemId.Trim() == studentUserTypeId.ToString()).FirstOrDefault();
            userTypes.Remove(userTypeToRemove);
            var strStaffMeeting = Common.Localization.ResourceManager.GetString("Planner.EventCategory.StaffMeeting");
            if (categoryValue == strStaffMeeting)
            {
                userTypes.Remove(studentUserTypeToRemove);
            }
            else
            {
                userTypes.Add(new ListItem
                {
                    ItemId = "0",
                    ItemName = Common.Localization.ResourceManager.GetString("Planner.MyPlanner.Everyone")
                });
                userTypes.Add(new ListItem
                {
                    ItemId = (Convert.ToInt32(userTypes.OrderBy(m => m.ItemId).LastOrDefault().ItemId) + 1).ToString(),
                    ItemName = Common.Localization.ResourceManager.GetString("Planner.MyPlanner.OtherGroups")
                });
            }

            ViewBag.SchoolGroup = new SelectList(LstofSchoolGroups, "SchoolGroupId", "SchoolGroupName");
            ViewBag.MemberTypes = new SelectList(userTypes, "ItemId", "ItemName");
            return PartialView("_ManageAssignee", objEvent);
        }
        [HttpPost]

        private string GetMSTeamsEventsPayload(string title, DateTime startDate, DateTime endDate, string userIds, bool isRecurring, string eventDays)
        {
            var participantsList = _schoolGroupService.GetUsersMailingDetails(userIds).ToList();
            var participantsPayloads = new List<dynamic>();
            participantsList.ForEach(e =>
            {
                participantsPayloads.Add(new
                {
                    type = "required",
                    emailAddress = new
                    {
                        address = e.Email,
                        name = $"{e.FirstName} {e.LastName}"
                    }
                });
            });
            var recurrenceObj = new object();
            if (isRecurring)
            {
                var daysDictionary = new Dictionary<string, string>();
                daysDictionary.Add("1", "Sunday");
                daysDictionary.Add("2", "Monday");
                daysDictionary.Add("3", "Tuesday");
                daysDictionary.Add("4", "Wednesday");
                daysDictionary.Add("5", "Thursday");
                daysDictionary.Add("6", "Friday");
                daysDictionary.Add("7", "Saturday");
                var teamsDaysPayload = string.Empty;

                eventDays.Split(',').ToList().ForEach(e =>
                {
                    teamsDaysPayload += daysDictionary[e] + ",";
                });
                recurrenceObj = new
                {
                    pattern = new
                    {
                        type = "weekly",
                        interval = 1,
                        daysOfWeek = teamsDaysPayload.Trim(',').Split(','),

                    },
                    range = new
                    {
                        type = "endDate",
                        startDate = startDate.ToString("yyyy-MM-dd"),
                        endDate = endDate.ToString("yyyy-MM-dd")
                    }
                };
            }


            var eventPayload = new
            {
                subject = title,
                body = new
                {
                    contentType = "HTML"
                },
                start = new
                {
                    dateTime = startDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'"),
                    timeZone = "UTC"
                },
                end = new
                {
                    dateTime = !isRecurring ? endDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'") : startDate.Date.Add(endDate.TimeOfDay).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'"),
                    timeZone = "UTC"
                },
                allowNewTimeProposals = true,
                isOnlineMeeting = true,
                onlineMeetingProvider = "teamsForBusiness",
                attendees = participantsPayloads,
                recurrence = isRecurring ? recurrenceObj : null
            };

            return JsonConvert.SerializeObject(eventPayload);
        }
        public async Task<ActionResult> InsertUpdatePlanner(EventEdit model, HttpPostedFileBase resourceFile)
        {
            //convert arabic dates
            //model.StartDate = (DateTime)model.strStartDate.ToSystemReadableDate();
            //model.EndDate = (DateTime)model.strEndDate.ToSystemReadableDate();
            //model.EndTime = model.EndTime.ToSystemReadableTime();
            if (resourceFile != null)
            {
                model.Extension = Path.GetExtension(resourceFile.FileName).Remove(0, 1);
                model.FileName = Path.GetFileName(resourceFile.FileName);

                var sharePointFileView = await UploadPlannerFile(resourceFile);
                model.ResourceFile = sharePointFileView.ShareableLink;
                model.PhysicalFilePath = sharePointFileView.SharepointUploadedFileURL;
            }
            else { model.ResourceFile = null; }
            model.SelectedPlannerMemberId = model.SelectedPlannerMemberId.Trim(',');
            //if (!String.IsNullOrEmpty(model.SelectedPlannerMemberId))
            //{
            //    model.SelectedPlannerMemberId = model.SelectedPlannerMemberId.Contains(',') ? model.SelectedPlannerMemberId.Remove(model.SelectedPlannerMemberId.Length - 1) : model.SelectedPlannerMemberId;
            //}
            model.StartDate = (DateTime)Convert.ToDateTime(model.StartDate.ToString("yyyy-MM-dd") + " " + model.StartTime).ConvertLocalToUTC();
            model.EndDate = string.IsNullOrEmpty(model.EndTime) ? (DateTime)Convert.ToDateTime(model.EndDate.ToString("yyyy-MM-dd") + " " + "12:00 PM") : (DateTime)Convert.ToDateTime(model.EndDate.ToString("yyyy-MM-dd") + " " + model.EndTime).ConvertLocalToUTC();
            MicrosoftTeamsHelper microsoftTeamsHelper = new MicrosoftTeamsHelper();
            if (model.OldMeetingType == (short)EventCategoryEnum.TeamsMeeting)
                await microsoftTeamsHelper.DeleteEvent(model.OnlineMeetingId);
            if (model.OnlineMeetingType == (short)EventCategoryEnum.ZoomMeeting)
            {
                if (string.IsNullOrEmpty(model.SelectedPlannerMemberId))
                {
                    var op = new OperationDetails();
                    op.Message = Common.Localization.ResourceManager.GetString("Planner.MyPlanner.SelectParticipants");
                    op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                    return Json(op, JsonRequestBehavior.AllowGet);
                }
                var formattedMeetingDateTime = model.StartDate.ToString("yyyy-MM-ddTHH:mm:ss");
                model.SelectedDate = formattedMeetingDateTime;
                if (model.IsRecurringEvent)
                {
                    model.MeetingDuration = Convert.ToInt16((model.StartDate.Date.Add(model.EndDate.TimeOfDay) - model.StartDate).TotalMinutes);
                }
                else
                    model.MeetingDuration = Convert.ToInt16((model.EndDate - model.StartDate).TotalMinutes);
                model.CreatedByUserName = $"{SessionHelper.CurrentSession.FirstName} {SessionHelper.CurrentSession.LastName}";
                model.CreatedBy = SessionHelper.CurrentSession.Id;
            }
            else if (model.OnlineMeetingType == (short)EventCategoryEnum.TeamsMeeting)
            {
                // ("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'"),
                var payloads = GetMSTeamsEventsPayload(model.Title, model.StartDate, model.EndDate,
                    model.SelectedPlannerMemberId, model.IsRecurringEvent, model.RecurringEventDays);

                var teamsMeeting = await microsoftTeamsHelper.CreateEvent(payloads);
                model.TeamMeeting = JsonConvert.DeserializeObject<MSTeamsReponse>(teamsMeeting.Body);
                if (string.IsNullOrEmpty(model.TeamMeeting.id))
                {
                    return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
                }
            }
            var result = _myPlannerService.InsertUpdateMyPlanner(model);

            //commented the email sending code for event members as per the requirement
            //string fromEmail = SessionHelper.CurrentSession.Email;
            //if (string.IsNullOrEmpty(fromEmail))
            //{
            //    fromEmail = "noreply@gemseducation.com";
            //}
            //if (result > 0)
            //{
            //    if (!String.IsNullOrEmpty(model.SelectedPlannerMemberId))
            //    {
            //        foreach (var i in model.SelectedPlannerMemberId.Split(','))
            //        {
            //            //change ActivationLink as per server
            //            var user = _userService.GetUserById(Convert.ToInt64(i));
            //            var Subject = model.Title;
            //            string EmailBody = String.Empty;
            //            var encryptEventId = EncryptDecryptHelper.EncryptUrl(result.ToString());
            //            var encryptUserId = EncryptDecryptHelper.EncryptUrl(i);
            //            var IsInternal = EncryptDecryptHelper.EncryptUrl("True");
            //            string ActivationLink = "//" + Request.Url.Host + "/Planner/MyPlanner/AcceptRequest?ed=" + encryptEventId + "&ud=" + encryptUserId + "";
            //            StreamReader reader = new StreamReader(Server.MapPath(Constants.EventAlertTemplate), Encoding.UTF8);
            //            EmailBody = reader.ReadToEnd();
            //            EmailBody = EmailBody.Replace("@@SchoolLogo", Constants.SchoolImageDir + SessionHelper.CurrentSession.SchoolImage);
            //            var Message = model.Description;
            //            ICommonService _commonService = new CommonService();
            //            //EmailBody = reader.ReadToEnd();
            //            EmailBody = EmailBody.Replace("@@StartDate", model.StartDate.ToString("dd-MMM-yyyy"));
            //            EmailBody = EmailBody.Replace("@@StartTime", model.StartTime);
            //            EmailBody = EmailBody.Replace("@@EndDate", model.EndDate.ToString("dd-MMM-yyyy"));
            //            EmailBody = EmailBody.Replace("@@EndTime", model.EndTime);
            //            EmailBody = EmailBody.Replace("@@Venue", model.Venue);
            //            EmailBody = EmailBody.Replace("@@Description", Message);
            //            EmailBody = EmailBody.Replace("@@ActivationLink", ActivationLink);
            //            EmailBody = EmailBody.Replace("@@title", Subject);
            //            string firstName = String.IsNullOrEmpty(SessionHelper.CurrentSession.FirstName) ? "" : SessionHelper.CurrentSession.FirstName;
            //            string lastName = String.IsNullOrEmpty(SessionHelper.CurrentSession.LastName) ? "" : SessionHelper.CurrentSession.LastName;
            //            EmailBody = EmailBody.Replace("@@username", firstName + " " + lastName);
            //            SendMailView sendEmailNotificationView = new SendMailView();
            //            sendEmailNotificationView.FROM_EMAIL = fromEmail;
            //            sendEmailNotificationView.TO_EMAIL = user.Email;
            //            sendEmailNotificationView.LOG_TYPE = "Event";
            //            sendEmailNotificationView.SUBJECT = Subject;
            //            sendEmailNotificationView.MSG = EmailBody;
            //            sendEmailNotificationView.LOG_USERNAME = SessionHelper.CurrentSession.UserName;
            //            sendEmailNotificationView.STU_ID = "11111";
            //            sendEmailNotificationView.LOG_PASSWORD = "tt";
            //            sendEmailNotificationView.PORT_NUM = "123";
            //            sendEmailNotificationView.EmailHostNew = "false";
            //            var operationalDetails = _commonService.SendEmail(sendEmailNotificationView);

            //        }
            //    }

            //    // commented the external user email code as of now
            //    //if (!String.IsNullOrEmpty(model.ExternalEmails))
            //    //{
            //    //    foreach (var i in model.ExternalEmails.Split(','))
            //    //    {
            //    //        //change ActivationLink as per server
            //    //        var Subject = model.Title;
            //    //        string EmailBody = String.Empty;
            //    //        var encryptEventId = EncryptDecryptHelper.EncryptUrl(result.ToString());
            //    //        var encryptEmaiId = EncryptDecryptHelper.EncryptUrl(i);
            //    //        var IsInternal = EncryptDecryptHelper.EncryptUrl("False");
            //    //        string ActivationLink = Request.Url.AbsoluteUri + "/Planner/MyPlanner/AcceptExternalRequest?ed=" + encryptEventId + "&ud=" + encryptEmaiId + "";
            //    //        StreamReader reader = new StreamReader(Server.MapPath(Constants.EventAlertTemplate), Encoding.UTF8);
            //    //        EmailBody = reader.ReadToEnd();
            //    //        EmailBody = EmailBody.Replace("@@SchoolLogo", Constants.SchoolImageDir + SessionHelper.CurrentSession.SchoolImage);
            //    //        var Message = model.Description;
            //    //        ICommonService _commonService = new CommonService();
            //    //        //EmailBody = reader.ReadToEnd();
            //    //        EmailBody = EmailBody.Replace("@@StartDate", model.SelectedDate);
            //    //        EmailBody = EmailBody.Replace("@@StartTime", model.StartTime);
            //    //        EmailBody = EmailBody.Replace("@@Description", Message);
            //    //        EmailBody = EmailBody.Replace("@@ActivationLink", ActivationLink);
            //    //        EmailBody = EmailBody.Replace("@@title", Subject);
            //    //        string firstName = String.IsNullOrEmpty(SessionHelper.CurrentSession.FirstName) ? "" : SessionHelper.CurrentSession.FirstName;
            //    //        string lastName = String.IsNullOrEmpty(SessionHelper.CurrentSession.LastName) ? "" : SessionHelper.CurrentSession.LastName;
            //    //        EmailBody = EmailBody.Replace("@@username", firstName + " " + lastName);
            //    //        SendMailView sendEmailNotificationView = new SendMailView();
            //    //        sendEmailNotificationView.FROM_EMAIL = SessionHelper.CurrentSession.Email;
            //    //        sendEmailNotificationView.TO_EMAIL = i;
            //    //        sendEmailNotificationView.LOG_TYPE = "Event";
            //    //        sendEmailNotificationView.SUBJECT = Subject;
            //    //        sendEmailNotificationView.MSG = EmailBody;
            //    //        sendEmailNotificationView.LOG_USERNAME = SessionHelper.CurrentSession.UserName;
            //    //        sendEmailNotificationView.STU_ID = "11111";
            //    //        sendEmailNotificationView.LOG_PASSWORD = "tt";
            //    //        sendEmailNotificationView.PORT_NUM = "123";
            //    //        sendEmailNotificationView.EmailHostNew = "false";
            //    //        var operationalDetails = _commonService.SendEmail(sendEmailNotificationView);
            //    //    }
            //    //}


            //    //change ActivationLink as per server
            //    //var Subject = model.Title;
            //    //string EmailBody = String.Empty;
            //    //var encryptEventId = EncryptDecryptHelper.EncryptUrl(result.ToString());
            //    //var encryptUserId = EncryptDecryptHelper.EncryptUrl(SessionHelper.CurrentSession.Id.ToString());
            //    //string ActivationLink = Request.Url.Host + "/Planner/MyPlanner/AcceptRequest?ed=" + encryptEventId + "&ud=" + encryptUserId + "";
            //    //StreamReader reader = new StreamReader(Server.MapPath(Constants.EventAlertTemplate), Encoding.UTF8);
            //    //EmailBody = reader.ReadToEnd();
            //    //EmailBody = EmailBody.Replace("@@SchoolLogo", Constants.SchoolImageDir + SessionHelper.CurrentSession.SchoolImage);
            //    //var Message = model.Description;

            //    //ICommonService _commonService = new CommonService();
            //    //string toEmailAddrees = "";
            //    //if (!String.IsNullOrEmpty(model.ExternalEmails))
            //    //{
            //    //    foreach (var i in model.ExternalEmails.Split(','))
            //    //    {
            //    //        toEmailAddrees = toEmailAddrees + "," + i;
            //    //    }
            //    //}
            //    //if (!String.IsNullOrEmpty(model.SelectedPlannerMemberId))
            //    //{
            //    //    foreach (var i in model.SelectedPlannerMemberId.Split(','))
            //    //    {
            //    //        var user = _userService.GetUserById(Convert.ToInt64(i));
            //    //        toEmailAddrees = toEmailAddrees + "," + user.Email;
            //    //    }
            //    //}
            //    ////EmailBody = reader.ReadToEnd();
            //    //EmailBody = EmailBody.Replace("@@Description", Message);
            //    //EmailBody = EmailBody.Replace("@@ActivationLink", ActivationLink);
            //    //EmailBody = EmailBody.Replace("@@title", Subject);
            //    //string firstName = String.IsNullOrEmpty(SessionHelper.CurrentSession.FirstName) ? "" : SessionHelper.CurrentSession.FirstName;
            //    //string lastName = String.IsNullOrEmpty(SessionHelper.CurrentSession.LastName) ? "" : SessionHelper.CurrentSession.LastName;
            //    //EmailBody = EmailBody.Replace("@@studentname", firstName + " " + lastName);
            //    //SendMailView sendEmailNotificationView = new SendMailView();
            //    //sendEmailNotificationView.FROM_EMAIL = SessionHelper.CurrentSession.Email;
            //    //sendEmailNotificationView.TO_EMAIL = toEmailAddrees;
            //    //sendEmailNotificationView.LOG_TYPE = "Event";
            //    //sendEmailNotificationView.SUBJECT = Subject;
            //    //sendEmailNotificationView.MSG = EmailBody;
            //    //sendEmailNotificationView.LOG_USERNAME = SessionHelper.CurrentSession.UserName;
            //    //sendEmailNotificationView.STU_ID = "11111";
            //    //sendEmailNotificationView.LOG_PASSWORD = "tt";
            //    //sendEmailNotificationView.PORT_NUM = "123";
            //    //sendEmailNotificationView.EmailHostNew = "false";
            //    //var operationalDetails = _commonService.SendEmail(sendEmailNotificationView);
            //}
            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }
        private async Task<string> UploadContentResource(HttpPostedFileBase resourceFile)
        {

            SharePointFileView shv = new SharePointFileView();
            try
            {
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    shv = await azureHelper.UploadFilesAsPerModuleAsync(FileModulesConstants.Planner, SessionHelper.CurrentSession.OldUserId.ToString(), resourceFile);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    shv = SharePointHelper.UploadFilesAsPerModule(ref cx, FileModulesConstants.Planner, SessionHelper.CurrentSession.OldUserId.ToString(), resourceFile);
                }
            }
            catch (Exception ex)
            {

                ILoggerClient _loggerClient = LoggerClient.Instance;
                _loggerClient.LogWarning("My Planner exception");
                _loggerClient.LogException(ex);
            }
            return shv.ShareableLink;
        }

        private async Task<SharePointFileView> UploadPlannerFile(HttpPostedFileBase resourceFile)
        {
            //string filePath = string.Empty;
            SharePointFileView sharePointFileView = new SharePointFileView();
            try
            {
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    sharePointFileView = await azureHelper.UploadStudentFilesAsPerModuleAsync(FileModulesConstants.Planner, SessionHelper.CurrentSession.OldUserId.ToString(), resourceFile);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    //filePath = SharePointHelper.UploadFilesAsPerModule(ref cx, SessionHelper.CurrentSession.SchoolCode, FileModulesConstants.Planner, resourceFile);
                    sharePointFileView = SharePointHelper.UploadStudentFilesAsPerModule(ref cx, FileModulesConstants.Planner, SessionHelper.CurrentSession.OldUserId.ToString(), resourceFile);
                }
            }
            catch (Exception ex)
            {
                ILoggerClient _loggerClient = LoggerClient.Instance;
                _loggerClient.LogWarning("My Planner exception");
                _loggerClient.LogException(ex);
            }
            return sharePointFileView;
        }

        public ActionResult GetUnassignedMembers(int? memberTypeId, string eventId)
        {
            var listOfMembers = _myPlannerService.GetInternalEventUserByEventId(Convert.ToInt32(eventId)).ToList();
            var schoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId);
            var loggedInUserId = SessionHelper.CurrentSession.Id;
            List<ListItem> objEvent = new List<ListItem>();
            if (memberTypeId.HasValue)
            {
                var unassignedMembers = _userService.GetUserBySchool((int)(int)SessionHelper.CurrentSession.SchoolId, (int)memberTypeId);
                unassignedMembers = unassignedMembers.Where(x => x.Id != loggedInUserId).ToList();
                objEvent.AddRange(unassignedMembers.Where(r => !listOfMembers.Any(a => r.Id == a.Id)).Select(r => new ListItem
                {
                    ItemName = $"{r.UserDisplayName} - ({r.UserName})",
                    ItemId = r.Id.ToString()
                }).OrderBy(r => r.ItemName));
            }
            return PartialView("_GetUnAssignedMembers", objEvent);
        }
        public ActionResult GetStudentsInGroup(int groupId, int eventId)
        {
            var loggedInUserId = SessionHelper.CurrentSession.Id;
            var lstStudents = _studentListService.GetStudentsWithStaffInGroup(groupId);
            lstStudents = lstStudents.Where(x => x.Id != loggedInUserId).ToList();
            var listOfMembers = _myPlannerService.GetInternalEventUserByEventId(Convert.ToInt32(eventId)).ToList();
            List<ListItem> objEvent = new List<ListItem>();
            objEvent.AddRange(lstStudents.Where(r => !listOfMembers.Any(a => r.Id == a.Id)).Select(r => new ListItem
            {
                ItemName = $"{r.FirstName + " " + r.LastName} - ({r.UserName})",
                ItemId = r.Id.ToString()
            }).OrderBy(r => r.ItemName));
            return PartialView("_GetUnAssignedMembers", objEvent);
            //var result = new { Result = ConvertViewToString("_GetUnAssignedMembers", lstStudents), StudentList = lstStudents };
            //return Json(result, JsonRequestBehavior.AllowGet);
        }
        private string ConvertViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter writer = new StringWriter())
            {
                ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }
        public JsonResult GetEvents(string month, int? categoryId, string eventType = "")
        {
            string fromDate = "", toDate = "";
            string defaultEventView = "dayGridMonth";
            DateTime now = DateTime.Now;
            if (!string.IsNullOrEmpty(month))
            {
                var day = "1";
                var m = String.Empty;
                var year = String.Empty;
                if (month.Split(' ').Length == 2)
                {
                    m = month.Split(' ')[0];
                    year = month.Split(' ')[1];
                    defaultEventView = "dayGridMonth";
                }
                if (month.Split(' ').Length == 3)
                {
                    m = month.Split(' ')[0];
                    day = month.Split(' ')[1].Remove(month.Split(' ')[1].Length - 1);
                    year = month.Split(' ')[2];
                    defaultEventView = "dayGridDay";
                }
                if (month.Split(' ').Length == 5)
                {
                    m = month.Split(' ')[0];
                    day = month.Split(' ')[1];
                    year = month.Split(',')[1];
                    defaultEventView = "dayGridWeek";
                }

                int monthNUmber = DateTime.Parse("1 " + m + year).Month;
                var startDate = new DateTime(Convert.ToInt32(year), monthNUmber, Convert.ToInt32(day));
                var endDate = DateTime.MinValue;
                if (month.Split(' ').Length == 2)
                {
                    endDate = startDate.AddMonths(1).AddDays(-1);
                }
                if (month.Split(' ').Length == 3)
                {
                    endDate = startDate.AddDays(1);
                }
                if (month.Split(' ').Length == 5)
                {
                    endDate = startDate.AddDays(6);
                }
                fromDate = startDate.ToString("yyyy-MM-dd");
                toDate = endDate.ToString("yyyy-MM-dd");
            }
            else
            {
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                fromDate = startDate.ToString("yyyy-MM-dd");
                toDate = endDate.ToString("yyyy-MM-dd");
            }

            string parentId = "", teacherId = "", studentId = "", isStaff = "", jsonEvents = "", jsonMyCommunityEvents = "", jsonStudentEvents = "", allCombinedEvents = "", studentNumber = "";
            if (SessionHelper.CurrentSession.IsParent())
            {
                parentId = SessionHelper.CurrentSession.UserName;
                isStaff = "0";
                studentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserName;
                studentNumber = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber;
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                teacherId = SessionHelper.CurrentSession.UserName;
                isStaff = "1";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                isStaff = "0";
                studentNumber = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                studentId = SessionHelper.CurrentSession.UserName;
                parentId = SessionHelper.CurrentSession.ParentUsername;
            }

            if ((eventType == "undefined" || eventType == "") && (categoryId == null || categoryId == 0))
            {
                jsonEvents = _myPlannerService.GetEvents(SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id, fromDate, toDate, categoryId);
                //jsonMyCommunityEvents = _calendarService.GetEvents(parentId, studentId, "1", fromDate, toDate, "en", isStaff, teacherId);
            }
            else if (eventType == "WholeSchool")
            {
                //jsonMyCommunityEvents = _calendarService.GetEvents(parentId, studentId, "1", fromDate, toDate, "en", isStaff, teacherId);
            }
            else if (categoryId > 0)
            {
                jsonEvents = _myPlannerService.GetEvents(SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id, fromDate, toDate, categoryId);
            }

            // to get the student events data with DateRange
            if (studentNumber != "" || eventType == "StudentTimeTable")
            {
                //if (eventType != "WholeSchool" && (categoryId == null || categoryId == 0))
                //jsonStudentEvents = _calendarService.GetStudentEvents(studentNumber, fromDate, toDate);
            }

            var finalJsonEvents = JsonConvert.SerializeObject(
                new[] { JsonConvert.DeserializeObject(jsonEvents), JsonConvert.DeserializeObject(jsonMyCommunityEvents), JsonConvert.DeserializeObject(jsonStudentEvents) }
            );

            //comment before going live
            //jsonEvents = _calendarService.GetEvents(parentId, "", "1", "2019-07-19", "2019-07-24", "en", "1", "K.HOBBS_WIS");

            var jsonResult = new { defaultView = defaultEventView, defaultDate = DateTime.Parse(fromDate), events = finalJsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllEvents(string month, string categoryIds, string eventType = "", string eventCategoryType = "", string eFromDate = "", string eToDate = "")
        {
            long userId = 0;
            bool isTeacher = false;
            bool isWeeklyEvent = false;
            int fromWeekDay = 1, toWeekDay = 7;
            string fromDate = "", toDate = "";
            string defaultEventView = "dayGridMonth";
            DateTime now = DateTime.UtcNow;
            long schoolId = SessionHelper.CurrentSession.SchoolId;

            if (!string.IsNullOrEmpty(month))
            {
                var day = "1";
                var m = String.Empty;
                var year = String.Empty;
                if (month.Split(' ').Length == 2)
                {
                    m = month.Split(' ')[0];
                    year = month.Split(' ')[1];
                    defaultEventView = "dayGridMonth";
                }
                if (month.Split(' ').Length == 3)
                {
                    m = month.Split(' ')[0];
                    day = month.Split(' ')[1].Remove(month.Split(' ')[1].Length - 1);
                    year = month.Split(' ')[2];
                    defaultEventView = "dayGridDay";
                }
                if (month.Split(' ').Length == 5)
                {
                    m = month.Split(' ')[0];
                    day = month.Split(' ')[1];
                    year = month.Split(',')[1];
                    defaultEventView = "dayGridWeek";
                }

                int monthNUmber = DateTime.Parse("1 " + m + year).Month;
                var startDate = new DateTime(Convert.ToInt32(year), monthNUmber, Convert.ToInt32(day));
                var endDate = DateTime.MinValue;
                if (month.Split(' ').Length == 2)
                {
                    endDate = startDate.AddMonths(1).AddDays(-1);
                }
                if (month.Split(' ').Length == 3)
                {
                    endDate = startDate.AddDays(1);
                }
                if (month.Split(' ').Length == 5)
                {
                    endDate = startDate.AddDays(6);
                }
                fromDate = startDate.ToString("yyyy-MM-dd");
                toDate = endDate.ToString("yyyy-MM-dd");
            }
            else if (eventCategoryType == "Today")
            {
                var startDate = new DateTime(now.Year, now.Month, now.Day);
                var endDate = startDate.AddDays(1);
                fromDate = startDate.AddDays(-1).ToString("yyyy-MM-dd");
                toDate = endDate.ToString("yyyy-MM-dd");
            }
            else if (eventCategoryType == "Week")
            {
                isWeeklyEvent = true;
                DateTime baseDate = new DateTime(now.Year, now.Month, now.Day);

                var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
                var thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1);
                fromDate = thisWeekStart.ToString("yyyy-MM-dd");
                toDate = thisWeekEnd.ToString("yyyy-MM-dd");
            }
            else if (eventCategoryType == "Month")
            {
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                fromDate = startDate.ToString("yyyy-MM-dd");
                toDate = endDate.ToString("yyyy-MM-dd");
            }
            else if (eventCategoryType == "FilterByDate")
            {
                var startDate = Convert.ToDateTime(eFromDate);
                var endDate = Convert.ToDateTime(eToDate);
                fromDate = startDate.AddDays(-1).ToString("yyyy-MM-dd");
                toDate = endDate.ToString("yyyy-MM-dd");
            }

            string parentId = "", teacherId = "", studentId = "", isStaff = "", jsonEvents = "", jsonMyCommunityEvents = "", jsonStudentEvents = "", jsonTodayOrWeeklyEvents = "", allCombinedEvents = "", studentNumber = "", jsonAssignments = "";
            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                parentId = SessionHelper.CurrentSession.UserName;
                isStaff = "0";
                studentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserName;
                studentNumber = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber;
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isTeacher = true;
                userId = SessionHelper.CurrentSession.Id;
                teacherId = SessionHelper.CurrentSession.UserName;
                isStaff = "1";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                userId = SessionHelper.CurrentSession.Id;
                isStaff = "0";
                studentNumber = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                studentId = SessionHelper.CurrentSession.UserName;
                parentId = SessionHelper.CurrentSession.ParentUsername;
            }

            if ((eventType == "undefined" || string.IsNullOrEmpty(eventType)) && string.IsNullOrEmpty(categoryIds))
            {
                jsonEvents = _myPlannerService.GetAllMonthlyEvents(SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id, fromDate, toDate, categoryIds);
                jsonMyCommunityEvents = "[]";//_calendarService.GetEvents(parentId, studentId, "1", fromDate, toDate, "en", isStaff, teacherId);
            }
            else if (eventType == "WholeSchool")
            {
                jsonMyCommunityEvents = "[]";//_calendarService.GetEvents(parentId, studentId, "1", fromDate, toDate, "en", isStaff, teacherId);
            }
            else if (!string.IsNullOrEmpty(categoryIds))
            {
                jsonEvents = _myPlannerService.GetAllMonthlyEvents(SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id, fromDate, toDate, categoryIds);
            }

            // commented the code as it is covered under get today/weekly timetable events
            // to get the student events data with DateRange
            //if (!string.IsNullOrEmpty(studentNumber) || eventType == "StudentTimeTable")
            //{
            //    if (eventType != "WholeSchool" && string.IsNullOrEmpty(categoryIds))
            //        jsonStudentEvents = _calendarService.GetStudentEvents(studentNumber, fromDate, toDate);
            //}

            if (string.IsNullOrEmpty(categoryIds))
            {
                jsonTodayOrWeeklyEvents = _myPlannerService.GetTodayOrWeeklyTimeTableEvents(userId, schoolId, fromWeekDay, toWeekDay, fromDate, toDate, isTeacher, isWeeklyEvent);
            }

            // commented the code as of now for assignment data
            //if (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent())
            //{
            //    jsonAssignments = _assignmentService.GetAssignmentsForMyPlanner(userId, fromDate, toDate, "");
            //}

            var finalJsonEvents = JsonConvert.SerializeObject(
                new[] { JsonConvert.DeserializeObject(jsonEvents), JsonConvert.DeserializeObject(jsonMyCommunityEvents), JsonConvert.DeserializeObject(jsonStudentEvents), JsonConvert.DeserializeObject(jsonTodayOrWeeklyEvents), JsonConvert.DeserializeObject(jsonAssignments) }
            );

            var jsonResult = new { defaultView = defaultEventView, defaultDate = DateTime.Parse(fromDate), events = finalJsonEvents };
            //return new Json(jsonResult, JsonRequestBehavior.AllowGet);
            return new JsonResult()
            {
                Data = jsonResult,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue
            };
        }

        public JsonResult GetDailyOrWeeklyEvents(bool isDashboardEvents, string eventCategoryIds, int? timeZoneOffset)
        {

            long userId = 0;
            bool isTeacher = false;
            string categoryIds = "";
            string fromDate = "", toDate = "", jsonWeeklyEvents = "";
            string defaultEventView = "dayGridMonth";
            DateTime baseDate = (DateTime)DateTime.UtcNow.ConvertUtcToLocalTime();

            string schoolWeekendDay = string.Empty;
            DateTime schoolWeekStartDate;
            DateTime now = (DateTime)DateTime.UtcNow.ConvertUtcToLocalTime();
            //DateTime baseDate = new DateTime(now.Year, now.Month, now.Day);

            var schoolWeekend = _AttendanceService.GetWeekEndBySchoolId(SessionHelper.CurrentSession.SchoolId).ToList();
            if (schoolWeekend.Count() > 0)
            {
                var item = schoolWeekend[schoolWeekend.Count() - 1];
                schoolWeekendDay = item.SchoolWeekend;
            }
            else
            {
                schoolWeekendDay = "Saturday";
            }
            string[] weekdaysArray = new string[7] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
            var startDayIndex = Array.IndexOf(weekdaysArray, schoolWeekendDay);

            var thisWeekStart = (baseDate.AddDays(-(int)baseDate.DayOfWeek));
            long schoolId = SessionHelper.CurrentSession.SchoolId;

            DateTime todayDate = (DateTime.UtcNow.ConvertUtcToLocalTime()).Value.Date;
            DateTime startOfWeek = todayDate.Date.AddDays(
                                    (int)CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek -
                                    (int)todayDate.Date.DayOfWeek);

            string result = string.Join("," + Environment.NewLine, Enumerable
              .Range(0, 7)
              .Select(i => startOfWeek
                 .AddDays(i)
                 .ToString("dd-MMM-yyyy")));

            result = result.Replace(System.Environment.NewLine, string.Empty);
            // below code to get the school week startDate and enddate
            string[] weekdayDatesArray = result.Split(',');
            if (startDayIndex != 6)
                schoolWeekStartDate = Convert.ToDateTime(weekdayDatesArray[startDayIndex + 1]);
            else
                schoolWeekStartDate = thisWeekStart;

            // comment the below 3 line code for new dashboard
            //var schoolWeekEndDate = schoolWeekStartDate.AddDays(6);
            //fromDate = schoolWeekStartDate.AddDays(-1).ToString("yyyy-MM-dd");
            //toDate = schoolWeekEndDate.ToString("yyyy-MM-dd");

            // uncomment the code for new dashboard
            var startDate = new DateTime(now.Year, now.Month, now.Day);
            var endDate = startDate.AddDays(1);
            fromDate = startDate.AddDays(-1).ToString("yyyy-MM-dd");
            toDate = endDate.ToString("yyyy-MM-dd");

            string schoolWeekDates = string.Join("," + Environment.NewLine, Enumerable
              .Range(0, 7)
              .Select(i => schoolWeekStartDate
                 .AddDays(i)
                 .ToString("dd-MMM-yyyy")));
            schoolWeekDates = schoolWeekDates.Replace(System.Environment.NewLine, string.Empty);
            schoolWeekDates = string.Concat(schoolWeekDates, ",", schoolWeekendDay);
            string weekDates = JsonConvert.SerializeObject(schoolWeekDates);

            string parentId = "", teacherId = "", studentId = "", isStaff = "", jsonMyPlannerEvents = "", jsonMyCommunityEvents = "", jsonStudentEvents = "", allCombinedEvents = "", studentNumber = "", jsonAssignments = "";
            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                parentId = SessionHelper.CurrentSession.UserName;
                isStaff = "0";
                studentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserName;
                studentNumber = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber;
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isTeacher = true;
                userId = SessionHelper.CurrentSession.Id;
                teacherId = SessionHelper.CurrentSession.UserName;
                isStaff = "1";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                userId = SessionHelper.CurrentSession.Id;
                isStaff = "0";
                studentNumber = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                studentId = SessionHelper.CurrentSession.UserName;
                parentId = SessionHelper.CurrentSession.ParentUsername;
            }

            var sortedCategoryIds = string.Empty;
            if (!string.IsNullOrEmpty(eventCategoryIds))
            {
                var splittedString = eventCategoryIds.Split(',');
                ArrayList myArraylist = new ArrayList();
                myArraylist.AddRange(splittedString);
                if (myArraylist.Contains("All Events"))
                {
                    myArraylist.RemoveAt(myArraylist.IndexOf("All Events"));
                }
                if (myArraylist.Contains("School Events"))
                {
                    myArraylist.RemoveAt(myArraylist.IndexOf("School Events"));
                }
                if (myArraylist.Contains("Student Timetable"))
                {
                    myArraylist.RemoveAt(myArraylist.IndexOf("Student Timetable"));
                }
                if (myArraylist.Contains("Assignments"))
                {
                    myArraylist.RemoveAt(myArraylist.IndexOf("Assignments"));
                }
                sortedCategoryIds = String.Join(",", myArraylist.ToArray());
            }

            // to get the daily or weekly timetable events
            if (string.IsNullOrEmpty(eventCategoryIds) || eventCategoryIds.Contains("All Events") || eventCategoryIds.Contains("Student Timetable"))
            {
                jsonWeeklyEvents = _myPlannerService.GetWeeklyTimeTableEvents(userId, schoolId, fromDate, toDate, isTeacher, isDashboardEvents);
            }
            if (string.IsNullOrEmpty(eventCategoryIds) || eventCategoryIds.Contains("All Events"))
            {
                jsonMyPlannerEvents = _myPlannerService.GetAllMonthlyEvents(SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id, fromDate, toDate, categoryIds);
            }
            if (string.IsNullOrEmpty(eventCategoryIds) || eventCategoryIds.Contains("All Events") || eventCategoryIds.Contains("School Events"))
            {
                jsonMyCommunityEvents = "[]";// _calendarService.GetEvents(parentId, studentId, "1", fromDate, toDate, "en", isStaff, teacherId);
            }

            if (!string.IsNullOrEmpty(sortedCategoryIds))
                jsonMyPlannerEvents = _myPlannerService.GetAllMonthlyEvents(SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id, fromDate, toDate, sortedCategoryIds);

            // Commented the student time table code as it covered in weekly/daily events
            //if (studentNumber != "" && (string.IsNullOrEmpty(eventCategoryIds) || eventCategoryIds.Contains("All Events") || eventCategoryIds.Contains("Student Timetable")))
            //{
            //    jsonStudentEvents = _calendarService.GetStudentEvents(studentNumber, fromDate, toDate);
            //}

            // commented the code as of now for assignment data
            //if (string.IsNullOrEmpty(eventCategoryIds) || eventCategoryIds.Contains("All Events") || eventCategoryIds.Contains("Assignments"))
            //{
            //    if (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent())
            //    {
            //        jsonAssignments = _assignmentService.GetAssignmentsForMyPlanner(userId, fromDate, toDate, "");
            //    }
            //}

            var finalJsonEvents = JsonConvert.SerializeObject(
               new[] { JsonConvert.DeserializeObject(weekDates), JsonConvert.DeserializeObject(jsonWeeklyEvents), JsonConvert.DeserializeObject(jsonMyPlannerEvents), JsonConvert.DeserializeObject(jsonMyCommunityEvents), JsonConvert.DeserializeObject(jsonStudentEvents), JsonConvert.DeserializeObject(jsonAssignments) }
           );

            var jsonResult = new { defaultView = defaultEventView, defaultDate = DateTime.Parse(fromDate), events = finalJsonEvents };
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTodaysSchedule()
        {
            long userId = 0;
            bool isTeacher = false;
            string fromDate = "", toDate = "";
            string parentId = "", teacherId = "", studentId = "", isStaff = "", jsonPlannerTimetableEvents = "", jsonMyCommunityEvents = "";
            string defaultEventView = "dayGridMonth";
            DateTime now = (DateTime)DateTime.UtcNow.ConvertUtcToLocalTime();
            long schoolId = SessionHelper.CurrentSession.SchoolId;

            var startDate = new DateTime(now.Year, now.Month, now.Day);
            fromDate = startDate.AddDays(-1).ToString("yyyy-MM-dd");
            toDate = startDate.ToString("yyyy-MM-dd");

            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                parentId = SessionHelper.CurrentSession.UserName;
                isStaff = "0";
                studentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserName;
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isTeacher = true;
                userId = SessionHelper.CurrentSession.Id;
                teacherId = SessionHelper.CurrentSession.UserName;
                isStaff = "1";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                userId = SessionHelper.CurrentSession.Id;
                isStaff = "0";
                studentId = SessionHelper.CurrentSession.UserName;
                parentId = SessionHelper.CurrentSession.ParentUsername;
            }

            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var plannerTimetable = _myPlannerService.GetPlannerTimetableData(userId, schoolId, fromDate, toDate, isTeacher);
            SynchronizationContext.SetSynchronizationContext(syncContext);

            plannerTimetable.Result.PlannerEventData = plannerTimetable.Result.PlannerEventData.Select(x =>
            {
                x.IsZakTokenRequired = (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ZoomZAKToken"]) && SessionHelper.CurrentSession.Id != x.CreatedBy ? true : false);
                x.EventUserEmail = SessionHelper.CurrentSession.Email;
                return x;
            });
            jsonPlannerTimetableEvents = JsonConvert.SerializeObject(plannerTimetable);


            //commented the school events api
            //jsonMyCommunityEvents = calendarService.GetEvents(parentId, studentId, "1", fromDate, toDate, "en", isStaff, teacherId);

            var finalJsonEvents = JsonConvert.SerializeObject(
              new[] { JsonConvert.DeserializeObject(jsonPlannerTimetableEvents), JsonConvert.DeserializeObject(jsonMyCommunityEvents) }
            );

            var jsonResult = new { defaultView = defaultEventView, defaultDate = DateTime.Parse(fromDate), events = finalJsonEvents };
            //return Json(jsonResult, JsonRequestBehavior.AllowGet);
            return new JsonResult()
            {
                Data = jsonResult,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue
            };
        }

        public JsonResult GetTimetableEvents(int pageNumber)
        {
            long userId = 0;
            bool isTeacher = false;
            string fromDate = "", toDate = "";
            string parentId = "", teacherId = "", studentId = "", isStaff = "", jsonTimetableEvents = "", jsonMyCommunityEvents = "";
            string defaultEventView = "dayGridMonth";
            DateTime now = (DateTime)DateTime.UtcNow.ConvertUtcToLocalTime();
            long schoolId = SessionHelper.CurrentSession.SchoolId;

            var startDate = new DateTime(now.Year, now.Month, now.Day);
            //fromDate = startDate.AddDays(-1).ToString("yyyy-MM-dd");
            fromDate = startDate.ToString("yyyy-MM-dd");
            toDate = startDate.ToString("yyyy-MM-dd");

            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                parentId = SessionHelper.CurrentSession.UserName;
                isStaff = "0";
                studentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserName;
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isTeacher = true;
                userId = SessionHelper.CurrentSession.Id;
                teacherId = SessionHelper.CurrentSession.UserName;
                isStaff = "1";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                userId = SessionHelper.CurrentSession.Id;
                isStaff = "0";
                studentId = SessionHelper.CurrentSession.UserName;
                parentId = SessionHelper.CurrentSession.ParentUsername;
            }

            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var timetableEvents = _myPlannerService.GetTimetableDataWithPaging(pageNumber, 4, userId, schoolId, fromDate, toDate, isTeacher).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);


            jsonTimetableEvents = JsonConvert.SerializeObject(timetableEvents);


            //commented the school events api
            //jsonMyCommunityEvents = calendarService.GetEvents(parentId, studentId, "1", fromDate, toDate, "en", isStaff, teacherId);

            var finalJsonEvents = JsonConvert.SerializeObject(
              new[] { JsonConvert.DeserializeObject(jsonTimetableEvents), JsonConvert.DeserializeObject(jsonMyCommunityEvents) }
            );

            var jsonResult = new { defaultView = defaultEventView, defaultDate = DateTime.Parse(fromDate), events = finalJsonEvents };
            //return Json(jsonResult, JsonRequestBehavior.AllowGet);
            return new JsonResult()
            {
                Data = jsonResult,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue
            };
        }

        public JsonResult GetPlannerEvents(int pageNumber, string eventType = "", string eventCategoryType = "", string eFromDate = "", string eToDate = "")
        {
            long userId = 0;
            bool isTeacher = false;
            string fromDate = "", toDate = "";

            DateTime now = (DateTime)DateTime.UtcNow.ConvertUtcToLocalTime();
            long schoolId = SessionHelper.CurrentSession.SchoolId;
            string parentId = "", teacherId = "", studentId = "", isStaff = "", jsonEventsData = "", studentNumber = "";

            #region categorytype filter
            if (eventCategoryType == "Today")
            {
                var startDate = new DateTime(now.Year, now.Month, now.Day);
                //var endDate = startDate;
                fromDate = startDate.AddDays(-1).ToString("yyyy-MM-dd");
                toDate = startDate.ToString("yyyy-MM-dd");
            }
            else if (eventCategoryType == "Week")
            {
                DateTime baseDate = new DateTime(now.Year, now.Month, now.Day);

                var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
                var thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1);
                fromDate = thisWeekStart.ToString("yyyy-MM-dd");
                toDate = thisWeekEnd.ToString("yyyy-MM-dd");
            }
            else if (eventCategoryType == "Month")
            {
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                fromDate = startDate.ToString("yyyy-MM-dd");
                toDate = endDate.ToString("yyyy-MM-dd");
            }
            else if (eventCategoryType == "FilterByDate")
            {
                var startDate = Convert.ToDateTime(eFromDate);
                var endDate = Convert.ToDateTime(eToDate);
                fromDate = startDate.AddDays(-1).ToString("yyyy-MM-dd");
                toDate = endDate.ToString("yyyy-MM-dd");

            }
            #endregion

            #region userType filter

            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                parentId = SessionHelper.CurrentSession.UserName;
                isStaff = "0";
                studentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserName;
                studentNumber = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber;
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                isTeacher = true;
                userId = SessionHelper.CurrentSession.Id;
                teacherId = SessionHelper.CurrentSession.UserName;
                isStaff = "1";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                userId = SessionHelper.CurrentSession.Id;
                isStaff = "0";
                studentNumber = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                studentId = SessionHelper.CurrentSession.UserName;
                parentId = SessionHelper.CurrentSession.ParentUsername;
            }
            #endregion


            if (eventType == "OnlineMeetings")
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var onlineMeetings = _myPlannerService.GetOnlineMeetingEvents(pageNumber, 25, userId, schoolId, fromDate, toDate, true).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);

                onlineMeetings = onlineMeetings.Select(x => { x.ShowRecordingLink = (!SessionHelper.CurrentSession.IsParent() && !string.IsNullOrEmpty(x.RecordingLink)); return x; }).ToList();
                jsonEventsData = JsonConvert.SerializeObject(onlineMeetings);
            }
            else if (eventType == "Timetable")
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var timetableEvents = _myPlannerService.GetSchoolTimetableEvents(pageNumber, 25, userId, schoolId, fromDate, toDate, isTeacher).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);
                jsonEventsData = JsonConvert.SerializeObject(timetableEvents);
            }
            else if (eventType == "SchoolEvents")
            {
                //jsonEventsData = "[]";//_calendarService.GetEvents(parentId, studentId, "1", fromDate, toDate, "en", isStaff, teacherId);
                jsonEventsData = _calendarService.GetEvents(parentId, studentId, "1", fromDate, toDate, "en", isStaff, teacherId);
            }
            else if (eventType == "OtherEvents")
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var otherEvents = _myPlannerService.GetOnlineMeetingEvents(pageNumber, 25, userId, schoolId, fromDate, toDate, false).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);
                jsonEventsData = JsonConvert.SerializeObject(otherEvents);
            }

            var jsonResult = new { type = eventType, events = jsonEventsData };

            return new JsonResult()
            {
                Data = jsonResult,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue
            };
        }

        public ActionResult LoadPlannerDetails(int id, int? timeOffset)
        {
            long userId = 0;
            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;
            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
            }
            else
            {
                userId = SessionHelper.CurrentSession.Id;
            }
            List<EventEdit> eventDeatils = _myPlannerService.GetEventById(id, userId).ToList();
            if (eventDeatils.Count == 0)
            {
                return RedirectToAction("NoPermission", "Error");
            }
            var model = new EventEdit();
            model = eventDeatils.FirstOrDefault();
            model.EventInternalUser = _myPlannerService.GetInternalEventUserByEventId(id).ToList();
            model.EventExternalUser = _myPlannerService.GetExternalEventUserByEventId(id).ToList();
            model.StartDate = (DateTime)model.StartDate.ConvertUtcToLocalTime();
            model.EndDate = (DateTime)model.EndDate.ConvertUtcToLocalTime();
            if (model.IsRecurringEvent)
            {
                if (model.RecurringStartDate.HasValue)
                    model.RecurringStartDate = model.RecurringStartDate.Value.ConvertUtcToLocalTime();
                if (model.RecurringEndDate.HasValue)
                    model.RecurringEndDate = model.RecurringEndDate.Value.ConvertUtcToLocalTime();
            }
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsUpdatePermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_MyPlanner.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            return PartialView("_PlannerDetails", model);
        }
        public ActionResult EventAttachmentViewer(string fileName)
        {
            try
            {
                fileName = EncryptDecryptHelper.DecodeBase64(fileName);
                //string resourceDir = Constants.ResourcesDir;
                //string content = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content";
                string path = Constants.ResourcesDir + "/Content" + "/PlannerResourceFiles/" + fileName;
                Phoenix.Models.File eventFile = new Phoenix.Models.File();
                eventFile.FilePath = path;
                eventFile.FileName = fileName;
                if (eventFile != null && !string.IsNullOrWhiteSpace(eventFile.FileName))
                {
                    var webClient = new WebClient();
                    DevExpressEditorSettings devExpressEditorSettings = new DevExpressEditorSettings(eventFile);
                    if (devExpressEditorSettings.FileExtension.IndexOf("html", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        byte[] FileBytes = webClient.DownloadData(devExpressEditorSettings.ServerPath);
                        return File(FileBytes, "text/html", fileName);
                    }
                    var extensions = new[] { "doc", "docx", "xlsx", "xls", "csv" };
                    if (devExpressEditorSettings.FileExtension.ToLower().Contains("csv"))
                    {
                        byte[] FileBytes = webClient.DownloadData(devExpressEditorSettings.ServerPath);
                        Response.BufferOutput = true;
                        Response.ContentType = "text/csv";


                        Response.BinaryWrite(FileBytes);
                        Response.Flush();
                        Response.End();
                        return new EmptyResult();
                    }
                    else if (!extensions.Any(devExpressEditorSettings.FileExtension.ToLower().Contains))
                    {
                        byte[] FileBytes = webClient.DownloadData(devExpressEditorSettings.ServerPath);
                        Response.BufferOutput = true;
                        Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;

                        return File(FileBytes, GetMimeType(devExpressEditorSettings.ServerPath));
                    }
                    return View(devExpressEditorSettings);
                }
            }
            catch
            {
                throw new FileNotFoundException();
            }
            return HttpNotFound();
        }

        public ActionResult DownloadFile(int eventId, string fileName)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(fileName))
                {


                    List<EventEdit> eventDeatils = _myPlannerService.GetEventById(eventId, SessionHelper.CurrentSession.Id).ToList();
                    if (eventDeatils.Count == 0)
                    {
                        return RedirectToAction("NoPermission", "Error");
                    }
                    var resourceFileName = eventDeatils.Select(x => x.ResourceFile).Single();

                    //string _filepath = Constants.ResourcesDir + "/Content" + "/PlannerResourceFiles/" + resourceFileName;
                    string _filepath = PhoenixConfiguration.Instance.ReadFilePath + resourceFileName;
                    Phoenix.Models.File eventFile = new Phoenix.Models.File();
                    eventFile.FilePath = _filepath;
                    eventFile.FileName = fileName;

                    if (eventFile.FileName != null && !string.IsNullOrWhiteSpace(eventFile.FilePath))
                    {
                        string path = PhoenixConfiguration.Instance.ReadFilePath + resourceFileName;

                        WebClient myWebClient = new WebClient();
                        byte[] myDataBuffer = myWebClient.DownloadData(path);
                        Response.BufferOutput = true;

                        return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, eventFile.FileName);

                    }
                }

            }
            catch
            {
                throw new FileNotFoundException();
            }
            return HttpNotFound();
        }

        public ActionResult DeleteEventFile(int eventId)
        {
            if (eventId > 0)
            {
                List<EventEdit> eventDeatils = _myPlannerService.GetEventById(eventId, SessionHelper.CurrentSession.Id).ToList();
                if (eventDeatils.Count == 0)
                {
                    return RedirectToAction("NoPermission", "Error");
                }
                var resourceFileName = eventDeatils.Select(x => x.ResourceFile).Single();

                //string _filepath = Constants.ResourcesDir + "/Content" + "/PlannerResourceFiles/" + resourceFileName;
                string _filepath = PhoenixConfiguration.Instance.ReadFilePath + resourceFileName;
                Phoenix.Models.File eventFile = new Phoenix.Models.File();
                eventFile.FilePath = _filepath;

                if (resourceFileName != null)
                {
                    var filePath = PhoenixConfiguration.Instance.WriteFilePath + resourceFileName;
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                var result = _eventService.DeleteEventFile(eventId);
                return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewEventFile(int eventId)
        {
            try
            {
                if (eventId > 0)
                {
                    var fileTypesList = _fileService.GetFileTypes();
                    var exceptDocExt = new[] { "pdf", "txt", "html", "htm", "csv", "ppt", "pptx" };

                    var docExt = fileTypesList.Where(r => r.DocumentType.IndexOf("document", StringComparison.OrdinalIgnoreCase) >= 0 && !exceptDocExt.Any(e => e == r.Extension.ToLower())).Select(r => r.Extension.ToLower()).ToList();

                    var videoExt = fileTypesList.Where(r => r.DocumentType.IndexOf("video", StringComparison.OrdinalIgnoreCase) >= 0).Select(r => r.Extension.ToLower()).ToList();

                    var audioExt = fileTypesList.Where(r => r.DocumentType.IndexOf("audio", StringComparison.OrdinalIgnoreCase) >= 0).Select(r => r.Extension.ToLower()).ToList();

                    var imgExt = fileTypesList.Where(r => r.DocumentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) >= 0 && !exceptDocExt.Any(e => e == r.Extension.ToLower())).Select(r => r.Extension.ToLower()).ToList();

                    ViewBag.DocExt = docExt;
                    ViewBag.VideoExt = videoExt;
                    ViewBag.AudioExt = audioExt;
                    ViewBag.ImageExt = imgExt;

                    List<EventEdit> eventDeatils = _myPlannerService.GetEventById(eventId, SessionHelper.CurrentSession.Id).ToList();
                    if (eventDeatils.Count == 0)
                    {
                        return RedirectToAction("NoPermission", "Error");
                    }
                    var resourceFilePath = eventDeatils.Select(x => x.ResourceFile).Single();
                    var fileName = eventDeatils.Select(x => x.FileName).Single();

                    //string _filepath = Constants.ResourcesDir + "/Content" + "/PlannerResourceFiles/" + resourceFileName;
                    string _filepath = PhoenixConfiguration.Instance.ReadFilePath + resourceFilePath;
                    Phoenix.Models.File eventFile = new Phoenix.Models.File();
                    eventFile.FilePath = resourceFilePath;
                    eventFile.FileName = fileName;

                    if (resourceFilePath != null && !string.IsNullOrWhiteSpace(eventFile.FilePath))
                    {
                        var webClient = new WebClient();
                        DevExpressEditorSettings devExpressEditorSettings = new DevExpressEditorSettings(eventFile);

                        if (devExpressEditorSettings.FileExtension.IndexOf("html", StringComparison.CurrentCultureIgnoreCase) >= 0)
                        {
                            byte[] FileBytes = webClient.DownloadData(devExpressEditorSettings.ServerPath);
                            return File(FileBytes, "text/html", eventFile.FileName);
                        }
                        else if (devExpressEditorSettings.FileExtension.IndexOf("csv", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            byte[] FileBytes = webClient.DownloadData(devExpressEditorSettings.ServerPath);
                            Response.BufferOutput = true;
                            Response.ContentType = "text/csv";


                            Response.BinaryWrite(FileBytes);
                            Response.Flush();
                            Response.End();
                            return new EmptyResult();
                        }
                        else if (videoExt.Any(devExpressEditorSettings.FileExtension.Replace(".", "").ToLower().Contains) || audioExt.Any(devExpressEditorSettings.FileExtension.Replace(".", "").ToLower().Contains))
                        {
                            return View(devExpressEditorSettings);
                        }
                        else if (imgExt.Any(devExpressEditorSettings.FileExtension.Replace(".", "").ToLower().Contains) || devExpressEditorSettings.FileExtension.IndexOf("pdf", StringComparison.OrdinalIgnoreCase) >= 0 || devExpressEditorSettings.FileExtension.IndexOf("txt", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            byte[] FileBytes = webClient.DownloadData(devExpressEditorSettings.ServerPath);
                            Response.BufferOutput = true;
                            Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;

                            return File(FileBytes, GetMimeType(devExpressEditorSettings.ServerPath));
                        }
                        else if (!docExt.Any(devExpressEditorSettings.FileExtension.Replace(".", "").ToLower().Contains))
                        {
                            byte[] FileBytes = webClient.DownloadData(devExpressEditorSettings.ServerPath);
                            Response.BufferOutput = true;
                            Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;

                            return File(FileBytes, GetMimeType(devExpressEditorSettings.ServerPath), eventFile.FileName);
                        }
                        else
                        {
                            return View(devExpressEditorSettings);
                        }
                    }
                }
            }
            catch
            {
                throw new FileNotFoundException();
            }
            return HttpNotFound();
        }

        //public void DownloadEventAttachment(string fileName)
        //{
        //    string resourceDir = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir;
        //    string content = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content";
        //    string path = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content" + "/PlannerResourceFiles/" + fileName;
        //    var webClient = new WebClient();
        //    byte[] FileBytes = webClient.DownloadData(path);
        //    Response.ContentType = GetMimeType(path);
        //    Response.BufferOutput = true;
        //    Response.AppendHeader("Content-Disposition", "Attachment; Filename=" + System.IO.Path.GetFileName(path) + "");
        //    Response.AddHeader("Last-Modified", DateTime.Now.ToLongDateString());
        //    Response.BinaryWrite(FileBytes);
        //    Response.Flush();
        //    Response.End();
        //}

        private string GetMimeType(string filePath)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(filePath).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }

        [AllowAnonymous]
        public ActionResult AcceptRequest(string ed, string ud)
        {
            int eventId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(ed));
            long userId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(ud));
            //int eventId = Convert.ToInt32(ed);
            //long userId = Convert.ToInt64(ud);
            var result = _myPlannerService.AcceptEventRequest(eventId, userId);
            return PartialView("/Areas/Planner/Views/MyPlanner/_EventAcceptSuccess.cshtml");
        }

        [HttpPost]
        public ActionResult AcceptEventRequest(string eventId)
        {
            int _eventId = Convert.ToInt32(EncryptDecryptHelper.DecodeBase64(eventId));
            long _userId = SessionHelper.CurrentSession.Id;
            var result = _myPlannerService.AcceptEventRequest(_eventId, _userId);

            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckIftheInvitationAccepted(int eventId)
        {

            bool isInvitationAccepted = false;
            long userId = SessionHelper.CurrentSession.Id;
            var result = _eventService.GeEventUserDetails(eventId, userId);
            if (result.IsRSVP == true || result.Id == 0 || result.OnlineMeetingType == (short)EventCategoryEnum.ZoomMeeting ||
                    result.OnlineMeetingType == (short)EventCategoryEnum.TeamsMeeting)
                isInvitationAccepted = true;

            return Json(new OperationDetails(isInvitationAccepted, "", null, eventId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckIftheTimetableEventExists(string title, string meetingPassword, DateTime startDate, string startTime)
        {
            bool isEventAvailable = false;
            long userId = SessionHelper.CurrentSession.Id;
            startDate = (DateTime)Convert.ToDateTime(startDate.ToString("yyyy-MM-dd") + " " + startTime).ConvertLocalToUTC();
            var result = _eventService.CheckTimetableEventExists(title, meetingPassword, startDate, startTime, userId);
            if (result > 0)
                isEventAvailable = true;

            return Json(new OperationDetails(isEventAvailable, "", null), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult AcceptExternalRequest(string ed, string ud)
        {
            int eventId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(ed));
            string emailId = Convert.ToString(EncryptDecryptHelper.DecryptUrl(ud));

            var result = _myPlannerService.AcceptEventRequestExternalUser(eventId, emailId);
            return PartialView("/Areas/Planner/Views/MyPlanner/_EventAcceptSuccess.cshtml");
        }

        [HttpPost]
        public async Task<ActionResult> CancelEvent(EventEdit model)
        {
            var IsOnlineMeeting = 0;
            var result = _eventService.CancelEvent(model.EventId, SessionHelper.CurrentSession.Id, model.OnlineMeetingId);

            if (result == 0)
            {
                return RedirectToAction("NoPermission", "Error");
            }

            if (model.OnlineMeetingId != null)
                IsOnlineMeeting = 1;

            // Commented the email sending codeas per the requirement
            //var eventInternalUser = _myPlannerService.GetInternalEventUserByEventId(model.EventId).ToList();
            //var eventExternalUser = _myPlannerService.GetExternalEventUserByEventId(model.EventId).ToList();

            //foreach (var item in eventInternalUser)
            //{
            //    SendEventCancellationEmail(item, model);
            //}
            //foreach (var item in eventExternalUser)
            //{
            //    SendEventCancellationEmail(item, model);
            //}

            if (model.OnlineMeetingType == (short)EventCategoryEnum.TeamsMeeting)
            {
                var teamsHelper = new MicrosoftTeamsHelper();
                await teamsHelper.DeleteEvent(model.OnlineMeetingId);
            }
            if (result == 1)
                return Json(new OperationDetails(true, "", null, IsOnlineMeeting), JsonRequestBehavior.AllowGet);
            else
                return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }

        public void SendEventCancellationEmail(EditModels.EventUser eventUser, EventEdit model)
        {
            var user = _userService.GetUserById(Convert.ToInt64(eventUser.Id));
            var Subject = model.Title;
            string EmailBody = String.Empty;
            var encryptEventId = EncryptDecryptHelper.EncryptUrl(model.EventId);
            var encryptUserId = EncryptDecryptHelper.EncryptUrl(eventUser.Id);
            var IsInternal = EncryptDecryptHelper.EncryptUrl("True");

            StreamReader reader = new StreamReader(Server.MapPath(Constants.CancelEventTemplate), Encoding.UTF8);
            EmailBody = reader.ReadToEnd();
            EmailBody = EmailBody.Replace("@@SchoolLogo", Constants.SchoolImageDir + SessionHelper.CurrentSession.SchoolImage);
            var Message = model.Description;
            ICommonService _commonService = new CommonService();
            //EmailBody = reader.ReadToEnd();
            EmailBody = EmailBody.Replace("@@StartDate", model.SelectedDate);
            EmailBody = EmailBody.Replace("@@StartTime", model.StartTime);
            EmailBody = EmailBody.Replace("@@Description", Message);

            EmailBody = EmailBody.Replace("@@title", Subject);
            string firstName = String.IsNullOrEmpty(SessionHelper.CurrentSession.FirstName) ? "" : SessionHelper.CurrentSession.FirstName;
            string lastName = String.IsNullOrEmpty(SessionHelper.CurrentSession.LastName) ? "" : SessionHelper.CurrentSession.LastName;
            EmailBody = EmailBody.Replace("@@username", firstName + " " + lastName);
            SendMailView sendEmailNotificationView = new SendMailView();
            sendEmailNotificationView.FROM_EMAIL = SessionHelper.CurrentSession.Email;
            sendEmailNotificationView.TO_EMAIL = user.Email;
            sendEmailNotificationView.LOG_TYPE = "CancelEvent";
            sendEmailNotificationView.SUBJECT = Subject;
            sendEmailNotificationView.MSG = EmailBody;
            sendEmailNotificationView.LOG_USERNAME = SessionHelper.CurrentSession.UserName;
            sendEmailNotificationView.STU_ID = "11111";
            sendEmailNotificationView.LOG_PASSWORD = "tt";
            sendEmailNotificationView.PORT_NUM = "123";
            sendEmailNotificationView.EmailHostNew = "false";
            var operationalDetails = _commonService.SendEmail(sendEmailNotificationView);
        }
    }
}