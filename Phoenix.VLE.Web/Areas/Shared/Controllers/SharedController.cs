﻿using Phoenix.Common.Helpers;
using Phoenix.Helpers;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.Enums;
using Phoenix.Common.Localization;
using Phoenix.Common;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.Common.Models;
using System.Net;
using System.Threading.Tasks;
using Phoenix.VLE.Web.ViewModels;
using System.Threading;
using Google.Apis.Auth.OAuth2.Mvc;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.VLE.Web.Utils;
using Microsoft.Identity.Client;
using System.Security.Claims;
using NLog.Internal;
using Phoenix.Common.Logger;

namespace Phoenix.VLE.Web.Areas.Shared.Controllers
{
    public class SharedController : BaseController
    {
        private readonly INotificationService _notificationService;
        public SharedController(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }
        #region Layout Page 
        private string GetUrl()
        {
            string url = Common.Helpers.CommonHelper.GetControllerActionUrl().Replace("/index", "");
            return url;
        }

        //[OutputCache(VaryByCustom = "User", Duration = 60)]
        public ActionResult GetBreadCrumbModuleStructure()
        {
            var model = ModuleHelper.GetPhoenixModuleStructure("up", GetUrl(), null, false, null, null);
            if (SessionHelper.CurrentSession.IsTeacher() && model.Any(x => x.ModuleName.Contains("My Vault")))
            {
                model.Where(x => x.ModuleName.Contains("My Vault")).ToList().ForEach(f => f.ModuleName = f.ModuleName.Replace("My", "Student"));
            }
            ViewBag.PageStructure = model;
            return PartialView("_BreadcrumbPartial");
        }


        public ActionResult GetAdminBreadCrumbModuleStructure()
        {
            ViewBag.PageStructure = ModuleHelper.GetPhoenixModuleStructure(StringEnum.GetStringValue(MainModuleCodes.AdminPanel), "up", GetUrl(), null);
            return PartialView("_BreadcrumbPartial");
        }

        [HttpGet]
        public ActionResult GetHSEMenuList()
        {
            return PartialView("_MenuListPartial", GetMenuList(MainModuleCodes.HSE, false));
        }



        public ActionResult GetVLEMenuList()
        {
            return PartialView("_MenuListPartial", GetMenuList(MainModuleCodes.Classroom, false));
        }


        public ActionResult GetAdminMenuList()
        {
            if (SessionHelper.CurrentSession.UserTypeId == 5)
            {
                SchoolInformation schoolInfo = (SchoolInformation)Session["SelectedSchoolForSuperAdmin"];
                ViewBag.SchoolImage = schoolInfo.SchoolImage;
            }

            return PartialView("_AdminMenuList", GetMenuList(MainModuleCodes.AdminPanel, false));
        }


        public ActionResult GetSystemLanguageList()
        {
            return PartialView("_LanguageListPartial", SystemLanguageHelper.SystemLanguages);
        }


        public ActionResult GetSIMSMenuList()
        {
            return PartialView("_MenuListPartial", GetMenuList(MainModuleCodes.SIMS, false));
        }


        public ActionResult GetStudentMenuList()
        {
            return PartialView("_MenuListPartial", GetMenuList(MainModuleCodes.Classroom, false));
        }


        public ActionResult GetTeacherMenuList()
        {
            return PartialView("_MenuListPartial", GetMenuList(MainModuleCodes.Classroom, false));
        }


        public ActionResult GetParentMenuList()
        {
            var menulist = GetMenuList(MainModuleCodes.Classroom, false);
            menulist = menulist.Where(x => x.ModuleName != "Locker" || x.ParentModuleId != 1).ToList();
            return PartialView("_MenuListPartial", menulist);
        }
        [HttpGet]
        private IEnumerable<ModuleStructure> GetMenuList(MainModuleCodes moduleCode, bool excludeParent = true)
        {
            IEnumerable<ModuleStructure> moduleStructureList = ModuleHelper.GetPhoenixModuleStructure("down", null, StringEnum.GetStringValue(moduleCode), excludeParent);
            string url = GetUrl();
            if (GetAllowedInnerPagesUrl().Contains(url.ToLower().Replace("/index", "")))
                url = GetRawUrl();
            else if (SessionHelper.CurrentSession.IsAdmin && url == "/schoolinfo/reports") //Added by Deepak Singh on 2/Nov/2020. To mark Dashboard menu active for admin
                url = GetRawUrl().ToLower();

            var isCurrentPage = false;

            foreach (var item in moduleStructureList)
            {
                if (url == item.ModuleUrl)
                {
                    item.IsCurrentPage = true;
                    isCurrentPage = true;
                    break;
                }
                else if(url == "/parent/" && item.ModuleUrl == "/home")
                {
                    item.IsCurrentPage = true;
                    isCurrentPage = true;
                    break;
                }
            }
            if (!isCurrentPage)
            {
                ModuleStructure moduleStructure = ModuleHelper.GetModuleTitle();
                foreach (var item in moduleStructureList)
                {
                    if (moduleStructure.ModuleId == item.ModuleId)
                    {
                        item.IsCurrentPage = true;
                        break;
                    }
                }
            }
            return moduleStructureList;
        }
        public string GetRawUrl()
        {
            string url = Request.RawUrl;
            if (!string.IsNullOrEmpty(Request.Url.Query))
            {
                url = url.Replace(Request.Url.Query, "");
            }
            if (url == "/" || url == "\\")
                url = Request.Url.AbsolutePath;
            return url;
        }
        #endregion
        #region Calender
        [HttpGet]
        public JsonResult GetCalendarTimeTable()
        {
            //CalendarService calendarService = new CalendarService();
            //calendarService.GetCalendarEvent(12300400127471, "22/jul/2019");
            return Json(JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult SetSession(string key, string value)
        {
            Session[key] = value;
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateCurrentSystemLanguage(int languageId)
        {
            LocalizationHelper.UpdateCurrentSystemLanguage(languageId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLanguageTerminology()
        {
            ITerminologyEditorService _terminologyEditorService = new TerminologyEditorService();
            var terminologyEditors = _terminologyEditorService.GetAllTerminologyEditor((long)SessionHelper.CurrentSession.SchoolId);
            return Json(terminologyEditors, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetLanguageTerminologyList(string url)
        {
            ITerminologyEditorService _terminologyEditorService = new TerminologyEditorService();
            string baseUrl = url;
            var terminologyEditors = CustomCacheManager.GetObjectFromCache(baseUrl, () => _terminologyEditorService.GetAllTerminologyEditor((long)SessionHelper.CurrentSession.SchoolId));
            return Json(terminologyEditors, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetUserProfilePartial(string loginType)
        {
            var loginSetting = Phoenix.VLE.Web.Helpers.CommonHelper.GetUserFeeling();
            loginSetting.LoginType = loginType;

            return PartialView("_ProfilePartial", loginSetting);
        }

        public ActionResult GetStudentsListPartial()
        {
            return PartialView("_StudentsProfilePartial");
        }

        public ActionResult UpdateAllNotification()
        {

            string studentNumber = string.Empty;
            string Key = string.Empty;
            string Identifier = string.Empty;
            

            if (SessionHelper.CurrentSession.IsParent())
            {
                Identifier = SessionHelper.CurrentSession.UserName;
                Key = "PARENT";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                Identifier = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                Key = "STUDENT";
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                Identifier = SessionHelper.CurrentSession.UserName;
                Key = "STAFF";
            }

            string Source = "CLASSROOM";
            if (TempData["notificationData"] == null) {

                var allNotifications = _notificationService.GetAllNotifications(Key, Identifier, Source, "");
                _notificationService.UpdateNotificationMarkAsRead(Key, Identifier, Source, 0, allNotifications.Where(x => x.IsRead == false).ToList());
                var strDrawerHTML = ControllerExtension.RenderPartialViewToString(this, "_NotificationsDrawer", new List<StudentNotification>());
                var strCount = ControllerExtension.RenderPartialViewToString(this, "_UserNotification", new List<StudentNotification>());
                return Json(new { Count= strCount, DrawerHTML= strDrawerHTML }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                var unreadNotifications = TempData["notificationData"] as List<StudentNotification>;
                _notificationService.UpdateNotificationMarkAsRead(Key, Identifier, Source, 0, unreadNotifications.Where(x => x.IsRead == false).ToList());
                TempData.Keep();
                var strDrawerHTML = ControllerExtension.RenderPartialViewToString(this, "_NotificationsDrawer", new List<StudentNotification>());
                var strCount = ControllerExtension.RenderPartialViewToString(this, "_UserNotification", new List<StudentNotification>());
                return Json(new { Count = strCount, DrawerHTML = strDrawerHTML }, JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult GetUserNotificationPartial()
        {
            if (TempData["notificationData"] == null)
            {
                string studentNumber = string.Empty;
                string Key = string.Empty;
                string Identifier = string.Empty;

                if (SessionHelper.CurrentSession.IsParent())
                {
                    Identifier = SessionHelper.CurrentSession.UserName;
                    Key = "PARENT";
                }
                else if (SessionHelper.CurrentSession.IsStudent())
                {
                    Identifier = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                    Key = "STUDENT";
                }
                else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                {
                    Identifier = SessionHelper.CurrentSession.UserName;
                    Key = "STAFF";
                }

                string Source = "CLASSROOM";
                string Type = "";

                var allNotifications = _notificationService.GetAllNotifications(Key, Identifier, Source, Type);

                TempData["notificationData"] = allNotifications;
                if (allNotifications == null)
                {
                    return PartialView("_UserNotification", new List<StudentNotification>());
                }
                else
                {
                    var unreadNotifications = allNotifications.Where(x => x.IsRead == false).ToList();
                    return PartialView("_UserNotification", unreadNotifications);
                }
            }
            else
            {
                //var allNotifications = TempData["notificationData"] as List<StudentNotification>;
                //var unreadNotifications = allNotifications.Where(x => x.IsRead == false).ToList();
                var unreadNotifications = TempData["notificationData"] as List<StudentNotification>;
                if (unreadNotifications == null)
                    return PartialView("_UserNotification", new List<StudentNotification>());
                else
                {
                    TempData.Keep("notificationData");
                    return PartialView("_UserNotification", unreadNotifications.Where(x => x.IsRead == false).ToList());
                }
            }
        }
        public ActionResult GetUserNotificationMobilePartial()
        {
            if (TempData["notificationData"] == null)
            {
                string studentNumber = string.Empty;
                string Key = string.Empty;
                string Identifier = string.Empty;

                if (SessionHelper.CurrentSession.IsParent())
                {
                    Identifier = SessionHelper.CurrentSession.UserName;
                    Key = "PARENT";
                }
                else if (SessionHelper.CurrentSession.IsStudent())
                {
                    Identifier = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                    Key = "STUDENT";
                }

                string Source = "CLASSROOM";
                string Type = "";

                var allNotifications = _notificationService.GetAllNotifications(Key, Identifier, Source, Type);
                if (allNotifications == null)
                {
                    return PartialView("_UserNotificationMobile", new List<StudentNotification>());
                }
                else
                {
                    var unreadNotifications = allNotifications.Where(x => x.IsRead == false).ToList();
                    return PartialView("_UserNotificationMobile", unreadNotifications);
                }
            }
            else
            {
                //var allNotifications = TempData["notificationData"] as List<StudentNotification>;
                //var unreadNotifications = allNotifications.Where(x => x.IsRead == false).ToList();
                var unreadNotifications = TempData["notificationData"] as List<StudentNotification>;
                if (unreadNotifications == null)
                    return PartialView("_UserNotificationMobile", new List<StudentNotification>());
                else
                {
                    TempData.Keep("notificationData");
                    return PartialView("_UserNotificationMobile", unreadNotifications.Where(x => x.IsRead == false).ToList());
                }
            }
        }
        [HttpPost]
        public ActionResult UpdateNotificationMarkAsRead(string notificationId,bool IsMarked)
        {

            bool result = false;
            string studentNumber = string.Empty;
            string Key = string.Empty, Identifier = string.Empty, Source = string.Empty;
            long _notificationId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(notificationId));
            

            if (SessionHelper.CurrentSession.IsParent())
            {
                studentNumber = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber;
                Key = "PARENT";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                studentNumber = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                Key = "STUDENT";
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                studentNumber = SessionHelper.CurrentSession.UserName;
                Key = "STAFF";
            }

            Identifier = studentNumber;
            Source = "CLASSROOM";

            string Type = "";
            result = _notificationService.UpdateNotificationMarkAsRead(Key, Identifier, Source, _notificationId, null, IsMarked) ;
            UpdateTempData(_notificationId);
            var op = new OperationDetails(result);
            op.Identifier = SessionHelper.CurrentSession.IsTeacher() ? "staff" : SessionHelper.CurrentSession.IsStudent() ? "student" : "parent";
            op.RelatedHtml = "";
            if (TempData["notificationData"] != null)
            {
                var model = TempData["notificationData"] as List<StudentNotification>;
                op.RelatedHtml = ControllerExtension.RenderPartialViewToString(this, "_NotificationsDrawer", model);
            }
            else {

                var allNotifications = _notificationService.GetAllNotifications(Key, Identifier, Source, Type);
                op.RelatedHtml = ControllerExtension.RenderPartialViewToString(this, "_NotificationsDrawer", allNotifications);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        private void UpdateTempData(long notificationId)
        {
            if (TempData["notificationData"] != null)
            {
                var model = TempData["notificationData"] as List<StudentNotification>;
                model.Where(x => x.NotificationID == notificationId.ToString()).Select(x => x.IsRead = true).ToList();
                TempData["notificationData"] = model;
            }
        }

        public ActionResult GetUserNotificationDrawer()
        {
            if (TempData["notificationData"] == null)
            {
                string studentNumber = string.Empty;
                string Key = string.Empty;
                string Identifier = string.Empty;

                if (SessionHelper.CurrentSession.IsParent())
                {
                    Identifier = SessionHelper.CurrentSession.UserName;
                    Key = "PARENT";
                }
                else if (SessionHelper.CurrentSession.IsStudent())
                {
                    Identifier = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                    Key = "STUDENT";
                }
                else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
                {
                    Identifier = Convert.ToString(SessionHelper.CurrentSession.UserName);
                    Key = "STAFF";
                }

                string Source = "CLASSROOM";
                string Type = "";

                var allNotifications = _notificationService.GetAllNotifications(Key, Identifier, Source, Type);
                var unreadNotifications = allNotifications.Where(x => x.IsRead == false).ToList();
                return PartialView("_NotificationsDrawer", unreadNotifications);
            }
            else
            {
                var model = TempData["notificationData"] as List<StudentNotification>;
                //TempData.Remove("notificationData");
                return PartialView("_NotificationsDrawer", model);
            }
        }
        public static IList<string> GetAllowedInnerPagesUrl()
        {
            IList<string> allowedUrlList = new List<string>();
            allowedUrlList.Add("/files/files/lockers");
            return allowedUrlList;
        }

        public ActionResult GetUserProfilePartialForMobile(string loginType)
        {
            var loginSetting = Phoenix.VLE.Web.Helpers.CommonHelper.GetUserFeeling();
            loginSetting.LoginType = loginType;
            return PartialView("_MobileProfilePartial", loginSetting);
        }

        public ActionResult GetDeploymentNotification()
        {
            var _schoolNotificationService = new SchoolNotificationService();
            var notifications = _schoolNotificationService.GetSchoolDeploymentNotificationList().FirstOrDefault(e => e.IsActive);
            if (notifications == null || string.IsNullOrEmpty(notifications.NotificationMessage))
                return Content("");
            return PartialView("_DeploymentPartial", notifications);
        }

        public ActionResult GetSchoolList()
        {
            var schoolService = new SchoolService();
            var schoolList = schoolService.GetSchoolList();
            if (Session["SelectedSchoolForSuperAdmin"] == null && schoolList.Any())
            {
                Session["SelectedSchoolForSuperAdmin"] = schoolList.FirstOrDefault();
            }

            return PartialView("_SchoolListDdl", schoolList);
        }
        public ActionResult GetAdminSchoolList()
        {
            var schoolService = new SchoolService();
            var schoolList = schoolService.GetAdminSchoolList();
            if (Session["SelectedSchoolForSuperAdmin"] == null && schoolList.Any())
            {
                Session["SelectedSchoolForSuperAdmin"] = schoolList.FirstOrDefault();
            }

            return PartialView("_SchoolListDdl", schoolList);
        }
        public ActionResult GetDataFilters()
        {
            ViewBag.SchoolAcademicYearList = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId), "ItemId", "ItemName");
            ViewBag.Stream = new SelectList(SelectListHelper.GetSelectListData(ListItems.Stream, SessionHelper.CurrentSession.SchoolId), "ItemId", "ItemName");

            return PartialView("_DataFilters");
        }

        public void DownloadFile(string filePath)
        {
            var webClient = new WebClient();
            byte[] FileBytes = webClient.DownloadData(filePath);
            Response.ContentType = GetMimeType(filePath);
            Response.BufferOutput = true;
            Response.AppendHeader("Content-Disposition", "Attachment; Filename=" + System.IO.Path.GetFileName(filePath) + "");
            Response.BinaryWrite(FileBytes);
            Response.Flush();
            Response.End();
        }
        private string GetMimeType(string filePath)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(filePath).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }
        public ActionResult SetUserCurrentLanguage(int langId)
        {
            var _commonService = new CommonService();
            var result = _commonService.SetUserCurrentLanguage(langId, SessionHelper.CurrentSession.Id);
            var visitorInfo = new VisitorInfo();
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();
            string IpDetails = IpAddress + ":" + HostIpAddress;
            if (result)
            {
                var _logInUserService = new LogInUserService();
                var user = _logInUserService.GetLoginUserByUserName(SessionHelper.CurrentSession.UserName, IpDetails);
                string firstName = string.IsNullOrEmpty(user.FirstName) ? "" : user.FirstName;
                string lastName = string.IsNullOrEmpty(user.LastName) ? "" : user.LastName;
                Session["SystemLangaugeId"] = langId;
                Session["FullName"] = firstName + " " + lastName;
                //add SystemLanguge Cookie
                SystemLanguageHelper.SetCurrentSystemLanguageCookie(langId.ToString());
                LocalizationHelper.CurrentSystemLanguage = null;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Cloud Storage methods

        public async Task<ActionResult> GetGoogleDriveFileList(ResourceFileTypes resourceTypeId, int? fileUploadModuleType)
        {
            IList<CloudFileListView> fileList = new List<CloudFileListView>();

            CancellationToken cancellationToken = new CancellationToken();
            var appFlow = new AppFlowMetadata();
            var result = await new CustomAuthorizationCodeMvcApp(this, appFlow).
            AuthorizeAsync(cancellationToken);
            Helpers.CommonHelper.AppendCookie("cloudfileloadertype", "cloudloadertype", "0");
            Helpers.CommonHelper.AppendCookie("gdiveredirecturi", "driveredirect", Request.UrlReferrer.AbsoluteUri);
            if (result.Credential != null)
            {
                fileList = await CloudFilesHelper.GetDriveFiles(result);
                ViewBag.ResourceFileTypeId = (short)resourceTypeId;
                int moduleType = 0;
                if (fileUploadModuleType.HasValue)
                {
                    moduleType = fileUploadModuleType.Value;
                }
                ViewBag.fileUploadModuleType = moduleType;
                var partialViewString = ControllerExtension.RenderPartialViewToString(this, "_CloudFileList", fileList);
                return Json(new { RedirectUrl = string.Empty, PartialView = partialViewString }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string redirectUri = result.RedirectUri;
                ILoggerClient logger = LoggerClient.Instance;
                logger.LogWarning("Google Redirect Uri-> " + result.RedirectUri);
                var uriBuilder = new UriBuilder(result.RedirectUri);
                var qs = HttpUtility.ParseQueryString(uriBuilder.Query);
                if (qs.Get("client_id") ==null)
                {
                    qs.Set("client_id", Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["GoogleDriveApiKey"]));
                    uriBuilder.Query = qs.ToString(); var newUri = uriBuilder.Uri;
                    redirectUri = newUri.AbsoluteUri;
                }

                return Json(new { RedirectUrl = redirectUri, PartialView = string.Empty }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> GetFolderFilesList(string folderId, ResourceFileTypes resourceFileTypeId)
        {
            IList<CloudFileListView> fileList = new List<CloudFileListView>();
            if (resourceFileTypeId == ResourceFileTypes.GoogleDrive)
            {
                CancellationToken cancellationToken = new CancellationToken();
                var result = await new CustomAuthorizationCodeMvcApp(this, new AppFlowMetadata()).
                AuthorizeAsync(cancellationToken);
                fileList = await CloudFilesHelper.GetFolderFiles(result, folderId);
            }
            else if (resourceFileTypeId == ResourceFileTypes.OneDrive)
            {
                fileList = await CloudFilesHelper.GetOneDriveFolderFiles(folderId);
            }
            return PartialView("_CLoudFolderFiles", fileList);
        }

        public async Task<ActionResult> GetOneDriveFiles(ResourceFileTypes resourceTypeId, int? fileUploadModuleType)
        {
            bool loadFiles = true;
            Helpers.CommonHelper.AppendCookie("gdiveredirecturi", "driveredirect", Request.UrlReferrer.AbsoluteUri);
            Helpers.CommonHelper.AppendCookie("cloudfileloadertype", "cloudloadertype", "0");
            if (SessionHelper.CurrentSession.MicrosoftToken == null || string.IsNullOrEmpty(SessionHelper.CurrentSession.MicrosoftToken.AccessToken))
            {
                // IConfidentialClientApplication app = MsalAppBuilder.BuildConfidentialClientApplication();
                // var scopes = System.Configuration.ConfigurationManager.AppSettings["ida:GraphScopes"].Split(' ');
                //var redirectUri = await OAuth2RequestManager.GenerateAuthorizationRequestUrl(scopes, app, this.HttpContext, Url);
                var redirectUri = CloudFilesHelper.GetOneDriveAuthUrl();
                ILoggerClient logger = LoggerClient.Instance;
                logger.LogWarning("One Redirect Uri-> " + redirectUri);
                return Json(new { RedirectUrl = redirectUri, PartialView = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            else if (SessionHelper.CurrentSession.MicrosoftToken.AccessDateTime.AddSeconds(SessionHelper.CurrentSession.MicrosoftToken.ExpiresIn) < DateTime.Now.AddMinutes(-3))
            {
                await CloudFilesHelper.RefreshOneDriveAccesToken(apiResource: "https://graph.microsoft.com/");
                loadFiles = true;
            }
            IList<CloudFileListView> fileList = new List<CloudFileListView>();
            if (loadFiles)
            {
                ViewBag.ResourceFileTypeId = (short)resourceTypeId;
                ViewBag.fileUploadModuleType = fileUploadModuleType.HasValue ? fileUploadModuleType.Value : 0;
                fileList = await CloudFilesHelper.GetOneDriveRootFiles();
            }

            var partialViewString = ControllerExtension.RenderPartialViewToString(this, "_CloudFileList", fileList);
            return Json(new { RedirectUri = string.Empty, PartialView = partialViewString }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region system file extension
        public PartialViewResult SystemFileExtension()
        {
            var commonService = new CommonService();
            var systemImageList = commonService.GetSystemImageList();
            return PartialView("_SystemImageExtension", systemImageList);
        }
        #endregion
    }
}