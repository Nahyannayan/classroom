﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DevExpress.Web;
using DevExpress.Web.ASPxHtmlEditor.Internal;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Auth.OAuth2.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PdfSharp;
using PdfSharp.Pdf;
using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Extensions;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using File = Phoenix.Models.File;
using Phoenix.Common.Models;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Logger;
using StudentList = Phoenix.VLE.Web.Models.EditModels.StudentList;

namespace Phoenix.VLE.Web.Areas.Files.Controllers
{
    public class FilesController : BaseController
    {
        private readonly IFolderService _folderService;
        private readonly IFileService _fileService;
        private List<FileManagementModule> _fileModuleList;
        private List<VLEFileType> _fileTypeList;
        IUserPermissionService _userPermissionService;
        ISchoolSpaceService _schoolSpaceService;
        IUserService _userService;
        ISchoolGroupService _schoolGroupService;
        IGroupUrlService _groupUrlService;
        IGroupQuizService _groupQuizService;
        private readonly IQuizService _quizService;
        private readonly IQuizQuestionsService _quizQuestionsService;
        private readonly IBookmarkService _bookmarkService;
        private IGradingTemplateService _gradingTemplateService;
        private IGradingTemplateItemService _gradingTemplateItemService;
        private IAssignmentService _assignmentService;
        private IStudentListService _studentListService;
        private IStudentService _studentService;
        private ILoggerClient _loggerClient;
        //Added by Shankar Kadam on 11/10/2020
        //To display assesment report in student valut section
        private readonly IAssessmentReport _IAssessmentReport;
        public FilesController(IFolderService folderService, IFileService fileService, IUserPermissionService userPermissionService, ISchoolSpaceService schoolSpaceService,
            IGradingTemplateItemService gradingTemplateItemService, IGradingTemplateService gradingTemplateService, IUserService userService, ISchoolGroupService schoolGroupService, IQuizService quizService, IGroupUrlService groupUrlService, IGroupQuizService groupQuizService,
            IQuizQuestionsService quizQuestionsService, IBookmarkService bookmarkService, IAssignmentService assignmentService, IStudentListService studentListService, IStudentService studentService, IAssessmentReport iassessmentReport)
        {
            _folderService = folderService;
            _fileService = fileService;
            _fileModuleList = _fileService.GetFileManagementModules().ToList();
            _fileTypeList = _fileService.GetFileTypes().ToList();
            _userPermissionService = userPermissionService;
            _schoolSpaceService = schoolSpaceService;
            _userService = userService;
            _gradingTemplateItemService = gradingTemplateItemService;
            _schoolGroupService = schoolGroupService;
            _quizService = quizService;
            _groupUrlService = groupUrlService;
            _groupQuizService = groupQuizService;
            _quizQuestionsService = quizQuestionsService;
            _bookmarkService = bookmarkService;
            _gradingTemplateService = gradingTemplateService;
            _assignmentService = assignmentService;
            _studentListService = studentListService;
            _studentService = studentService;
            _IAssessmentReport = iassessmentReport;
        }

        // GET: Files/MyFiles
        public ActionResult Index()
        {
            var module = _fileModuleList.FirstOrDefault(r => r.ModuleName.Contains("My Files"));
            //set permission
            ViewBag.IsCustomPermission = SetAccessPermissionForModule(module.ModuleId);
            ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            return View();
        }

        public ActionResult SeachMyFiles(int? folderId, int? sectionId, string sortBy = "NameAsc", int moduleId = 0)
        {
            var vm = new MyFilesViewModel();
            var defaultModuleId = moduleId > 0 ? moduleId : _fileModuleList.FirstOrDefault(r => r.ModuleName.Contains("My Files")).ModuleId;
            vm.ModuleId = defaultModuleId;
            int userId = (int)SessionHelper.CurrentSession.Id;
            IEnumerable<Folder> folderList;
            IEnumerable<File> fileList;
            ViewBag.ParentFolderId = folderId;
            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = Convert.ToInt32(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId);
            }
            if (folderId.HasValue)
            {
                vm.ParentFolderId = folderId.Value;
                folderList = _folderService.GetFoldersByParentFolderId(folderId.Value, defaultModuleId, true);
                fileList = _fileService.GetFilesByFolderId(folderId.Value, true);
                vm.FolderTree = _folderService.GetFolderTree(folderId.Value);
            }
            else if (sectionId.HasValue)
            {
                folderList = _folderService.GetFoldersBySectionId(sectionId.Value, defaultModuleId, null);
                fileList = _fileService.GetFilesBySectionId(sectionId.Value, defaultModuleId, true);
            }
            else
            {
                folderList = _folderService.GetFoldersByUserId(userId, defaultModuleId, true);
                fileList = _fileService.GetFilesByUserId(userId, defaultModuleId, true);
            }
            vm.Folders = folderList.Select(r => EntityMapper<Folder, FolderEdit>.Map(r));
            vm.Files = fileList.Select(r => EntityMapper<File, FileEdit>.Map(r));
            vm = GetSortedFilesAndFolders(vm, sortBy);
            //set permission
            ViewBag.IsCustomPermission = SetAccessPermissionForModule(defaultModuleId);
            return PartialView("_MyFiles", vm);
        }

        private MyFilesViewModel GetSortedFilesAndFolders(MyFilesViewModel model, string sortBy)
        {
            if (sortBy.Equals("NameAsc", StringComparison.OrdinalIgnoreCase))
            {
                model.Folders = model.Folders.OrderBy(e => e.FolderName);
                model.Files = model.Files.OrderBy(e => e.FileName);
            }
            else if (sortBy.Equals("NameDesc", StringComparison.OrdinalIgnoreCase))
            {
                model.Folders = model.Folders.OrderByDescending(e => e.FolderName);
                model.Files = model.Files.OrderByDescending(e => e.FileName);
            }
            else if (sortBy.Equals("DateAsc", StringComparison.OrdinalIgnoreCase))
            {
                model.Folders = model.Folders.OrderBy(e => e.UpdatedOn);
                model.Files = model.Files.OrderBy(e => e.UpdatedOn);
            }
            else if (sortBy.Equals("DateDesc", StringComparison.OrdinalIgnoreCase))
            {
                model.Folders = model.Folders.OrderByDescending(e => e.UpdatedOn);
                model.Files = model.Files.OrderByDescending(e => e.UpdatedOn);
            }
            else if (sortBy.Equals("Type", StringComparison.OrdinalIgnoreCase))
            {
                model.Folders = model.Folders.OrderBy(e => e.FolderName);
                model.Files = model.Files.OrderBy(e => e.FileTypeName);
            }
            else if (sortBy.Equals("items/size", StringComparison.OrdinalIgnoreCase))
            {
                model.Folders = model.Folders.OrderBy(e => e.TotalCount);
                model.Files = model.Files.OrderBy(e => e.FileSizeInMB);
            }
            return model;
        }

        public ActionResult DownloadFolderFiles(int folderId)
        {
            IEnumerable<File> fileList = new List<File>();
            fileList = _fileService.GetFilesByFolderId(folderId, true);
            List<string> filePathList = new List<string>();
            string downloadUrl = string.Empty;
            foreach (var file in fileList)
            {
                var filePath = PhoenixConfiguration.Instance.WriteFilePath + file.FilePath;
                if (System.IO.File.Exists(filePath))
                    filePathList.Add(filePath);
            }
            if (filePathList.Count > 0)
            {
                var directoryPath = PhoenixConfiguration.Instance.WriteFilePath + Constants.SchoolGroupZipFilePath;
                string fullFilePath = $"{directoryPath}{folderId.ToString()}.zip";
                Common.Helpers.CommonHelper.CreateDestinationFolder(directoryPath);
                if (System.IO.File.Exists(fullFilePath))
                    System.IO.File.Delete(fullFilePath);
                DownloadHelper.DownloadZipFile(filePathList, fullFilePath);
                downloadUrl = $"{PhoenixConfiguration.Instance.ReadFilePath}{Constants.SchoolGroupZipFilePath}{folderId.ToString()}.zip";
            }
            return Json(downloadUrl, JsonRequestBehavior.AllowGet);
        }
        public DateTime GetServerDateTime()
        {
            var serverDateTime = Convert.ToDateTime(DateTime.UtcNow.ConvertUtcToLocalTime());
            //var testDateTime = _fileService.GetServerDateTime();
            //var serverDateTime = DateTime.UtcNow;
            return serverDateTime;
        }

        // GET: Files/Groups
        public ActionResult Lockers(string lkId, string pfId, string vSSId)
        {
            var vm = new MyFilesViewModel();
            var module = _fileModuleList.FirstOrDefault(r => r.ModuleName.Contains("Lockers"));
            vm.ModuleId = module.ModuleId;
            vm.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();


            int userId = 0;
            if (SessionHelper.CurrentSession.IsStudent())
            {
                userId = (int)SessionHelper.CurrentSession.Id;
            }
            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = Convert.ToInt32(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId);
            }
            //vm.SectionId = !string.IsNullOrWhiteSpace(lkId) ? Helpers.CommonHelper.GetDecryptedId(lkId)) : userId;
            vSSId = string.IsNullOrWhiteSpace(lkId) ? EncryptDecryptHelper.DecryptUrl(vSSId) : EncryptDecryptHelper.DecryptUrl(lkId);
            vm.SectionId = userId == 0 ? Convert.ToInt32(vSSId) : userId;

            ViewBag.Student = _userService.GetUserById(vm.SectionId);

            IEnumerable<Folder> folderList;
            IEnumerable<File> fileList;
            IEnumerable<FolderTree> folderTrees;

            if (!string.IsNullOrWhiteSpace(pfId))
            {
                var parentFolderId = Helpers.CommonHelper.GetDecryptedId(pfId);
                vm.ParentFolderId = parentFolderId;
                folderList = _folderService.GetFoldersByParentFolderId(parentFolderId, module.ModuleId, null);
                fileList = _fileService.GetFilesByFolderId(parentFolderId, null);
                vm.FolderTree = _folderService.GetFolderTree(parentFolderId);
            }
            else
            {
                //Get by SchoolSpaceId
                folderList = _folderService.GetFoldersBySectionId(vm.SectionId, module.ModuleId, null);
                fileList = _fileService.GetFilesBySectionId(vm.SectionId, module.ModuleId, null);
            }

            vm.Folders = folderList.Select(r => EntityMapper<Folder, FolderEdit>.Map(r));
            vm.Files = fileList.Select(r => EntityMapper<File, FileEdit>.Map(r));
            //sort by name
            vm = GetSortedFilesAndFolders(vm, "NameAsc");
            //set permission
            ViewBag.IsCustomPermission = SetAccessPermissionForModule(module.ModuleId);

            //Get Acadmic Year
            string studentNumber = "";
            long studentId = 0;
            var studentinfo = Session["CurrentSelectedStudent"] == null ? new StudentDetail() : (StudentDetail)Session["CurrentSelectedStudent"];
            var StudentSchoolAcademicYearId = 0;
            var PhoenixSchoolAcademicYearId = "";
            StudentDetail obj = null;
            if (SessionHelper.CurrentSession.IsParent())
            {
                studentId = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentId;
                studentNumber = Convert.ToString(SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber);
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                studentNumber = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                StudentSchoolAcademicYearId = studentinfo.SchoolAcademicYearId;
                PhoenixSchoolAcademicYearId = studentinfo.PhoenixSchoolAcademicYearId.ToString();
                studentId = SessionHelper.CurrentSession.Id;
            }
            else if (SessionHelper.CurrentSession.IsAdmin || SessionHelper.CurrentSession.IsTeacher())
            {
                obj = _studentService.GetStudentByUserId(vm.SectionId);
                studentNumber = obj.StudentNumber;
                studentId = obj.StudentId;
                StudentSchoolAcademicYearId = obj.SchoolAcademicYearId;
                PhoenixSchoolAcademicYearId = obj.PhoenixSchoolAcademicYearId.ToString();
                //studentId = "13100100040538";
            }
            AssessmentModel Assessmentmodel = new AssessmentModel();
            var studList = SessionHelper.CurrentSession.FamilyStudentList;
            List<StudentList> studentlist = new List<StudentList>();
            foreach (var item in studList)
            {
                StudentList model = new StudentList();
                model.StudentFirstName = item.FirstName + " " + item.LastName;
                model.StudentNumber = item.StudentNumber;
                model.StudentLastName = item.LastName;
                model.StudentId = item.StudentId;
                model.SchooolId = item.SchoolId;
                model.schoolAcademicYearid = item.SchoolAcademicYearId;
                studentlist.Add(model);
            }
            Assessmentmodel.studentList = studentlist;  //......Student List Binding
            var StudInfo = Assessmentmodel.studentList.Where(x => x.StudentId == studentId).FirstOrDefault();
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var AcademicYearList = SelectListHelper.GetSelectListData(ListItems.Assessment, string.Format("{0},{1}", SessionHelper.CurrentSession.SchoolId, StudInfo == null ? StudentSchoolAcademicYearId : StudInfo.schoolAcademicYearid)).ToList();

            List<AssessmentdropModel> Dropmodel = new List<AssessmentdropModel>();
            foreach (var item1 in AcademicYearList)   // ....... DropDownlist Binding
            {
                AssessmentdropModel asm = new AssessmentdropModel();
                string AcdYear = Convert.ToDateTime(item1.ItemName).Year.ToString();
                int number = int.Parse(AcdYear);
                string number1 = Convert.ToString(number + 1);
                asm.ItemName = AcdYear + "-" + number1;
                asm.ACDID = item1.ItemId;
                Dropmodel.Add(asm);
            }
            string SchoolAcademicYearId = studList.Count() == 0 ? PhoenixSchoolAcademicYearId : studList.FirstOrDefault().PhoenixSchoolAcademicYearId.ToString();
            IEnumerable<PdfInfoModel> pdfInfoModel = _IAssessmentReport.GetPdfListReport(studentNumber, SchoolAcademicYearId);
            Assessmentmodel.pdfInfoModellist = pdfInfoModel.ToList();            //pdf file List
            List<SelectListItem> selectList1 = new List<SelectListItem>();
            selectList1.AddRange(Dropmodel.Select(x => new SelectListItem { Value = x.ACDID.ToString(), Text = x.ItemName }).ToList());
            Assessmentmodel.AcademicYear = SchoolAcademicYearId;
            Assessmentmodel.dropAcdemicYear = selectList1;
            ViewBag.Assessmentmodel = Assessmentmodel;
            ViewBag.StudentNumber = studentNumber;
            return View(vm);
        }

        // GET: Files/Groups
        public ActionResult Groups(string grId, string pfId)
        {
            var vm = new MyFilesViewModel();
            var module = _fileModuleList.FirstOrDefault(r => r.ModuleName.Contains("Groups"));
            vm.ModuleId = module.ModuleId;
            vm.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            var userId = (int)SessionHelper.CurrentSession.Id;
            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = Convert.ToInt32(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId);
            }

            vm.SectionId = !string.IsNullOrWhiteSpace(grId) ? Helpers.CommonHelper.GetDecryptedId(grId) : userId;
            var schoolGroup = _schoolGroupService.GetSchoolGroupByID(vm.SectionId);
            ViewBag.Group = schoolGroup;

            IEnumerable<Folder> folderList;
            IEnumerable<File> fileList;
            //IEnumerable<FolderTree> folderTrees;

            IEnumerable<GroupUrl> groupUrlList;
            IEnumerable<GroupQuiz> groupQuizList;
            IEnumerable<QuizView> groupFormList;
            IEnumerable<Assignment> lstAssignments;
            if (SessionHelper.CurrentSession.IsStudent())
            {
                vm.AssignmentStudents = _assignmentService.GetStudentSchoolGroupAssignments(vm.SectionId, userId);
            }
            if (SessionHelper.CurrentSession.IsTeacher())
            {
                vm.Assignments = _assignmentService.GetSchoolGroupAssignments(vm.SectionId);
            }
            if (SessionHelper.CurrentSession.IsParent())
            {
                vm.AssignmentStudents = _assignmentService.GetStudentSchoolGroupAssignments(vm.SectionId, userId);

            }
            if (vm.AssignmentStudents != null)
            {
                foreach (var item in vm.AssignmentStudents)
                {
                    item.objAssignment = _assignmentService.GetAssignmentById(item.AssignmentId);
                }
            }

            if (!string.IsNullOrWhiteSpace(pfId))
            {
                var parentFolderId = Helpers.CommonHelper.GetDecryptedId(pfId);
                vm.ParentFolderId = parentFolderId;
                folderList = _folderService.GetFoldersByParentFolderId(parentFolderId, module.ModuleId, null);
                fileList = _fileService.GetFilesByFolderId(parentFolderId, true);
                vm.FolderTree = _folderService.GetFolderTree(parentFolderId);
            }
            else
            {
                //Get by SchoolSpaceId
                folderList = _folderService.GetFoldersBySectionId(vm.SectionId, module.ModuleId, true);
                fileList = _fileService.GetFilesBySectionId(vm.SectionId, module.ModuleId, true);
                groupUrlList = _groupUrlService.GetGroupUrlListByGroupId(vm.SectionId);
                groupQuizList = _groupQuizService.GetGroupQuizListByGroupId(vm.SectionId);
                groupFormList = _quizService.GetAllQuizByUserAndSource(SessionHelper.CurrentSession.Id, vm.SectionId, "Group", true);
                vm.GroupUrl = groupUrlList;
                vm.GroupQuiz = groupQuizList;
                vm.GroupForm = groupFormList;

            }

            vm.Folders = folderList.Select(r => EntityMapper<Folder, FolderEdit>.Map(r));
            vm.Files = fileList.Select(r => EntityMapper<File, FileEdit>.Map(r));

            //ViewBag.IsCustomPermission = SetAccessPermissionForModule(module.ModuleId);

            var permission = _folderService.GetGroupPermission(Convert.ToInt32(SessionHelper.CurrentSession.Id), vm.SectionId);

            //For archived groups, implemented default permission to deny add/delete features
            if (schoolGroup.ArchivedBy.HasValue && schoolGroup.ArchivedOn.HasValue && DateTime.Compare(schoolGroup.ArchivedOn.Value.Date, DateTime.Today) <= 0)
            {
                ViewBag.IsCustomPermission = false;
                ViewBag.GroupPermission = 1;
            }
            else
            {
                ViewBag.IsCustomPermission = (permission >= 2);
                ViewBag.GroupPermission = permission;
            }
            vm.GroupStudentCount = _studentListService.GetStudentsInGroup(vm.SectionId).Count();
            bool isLastSeenupdated = _schoolGroupService.UpdateLastSeen(userId, vm.SectionId, SessionHelper.CurrentSession.IsParent());
            return View(vm);
        }

        #region Zip Selected File
        public ActionResult CreateSelectedFIlesZip(int schoolGroupId, string fileIds, string parentFolderId)
        {
            var op = new OperationDetails();
            IEnumerable<File> fileList;
            string[] arrFileIds = fileIds.Split(',');
            if (string.IsNullOrEmpty(parentFolderId))
                fileList = _fileService.GetFilesBySectionId(schoolGroupId, (int)FileManagementModuleTypes.Groups, true);
            else
                fileList = _fileService.GetFilesByFolderId(EncryptDecryptHelper.DecryptUrl(parentFolderId).ToInteger(), true);
            fileList = from f in fileList.AsEnumerable()
                       join ff in arrFileIds on f.FileId equals ff.ToInteger()
                       select f;
            List<string> filePathList = new List<string>();
            foreach (var file in fileList)
            {
                var filePath = PhoenixConfiguration.Instance.WriteFilePath + file.FilePath;
                if (System.IO.File.Exists(filePath))
                {
                    filePathList.Add(filePath);
                }
            }
            if (filePathList.Count() > 0)
            {
                try
                {
                    var directoryPath = PhoenixConfiguration.Instance.WriteFilePath + string.Format(Constants.SchoolGroupZipFilePath, ((int)FileManagementModuleTypes.Groups).ToString());
                    string fullFilePath = $"{directoryPath}{schoolGroupId.ToString()}.zip";
                    Common.Helpers.CommonHelper.CreateDestinationFolder(directoryPath);
                    if (System.IO.File.Exists(fullFilePath))
                        System.IO.File.Delete(fullFilePath);
                    DownloadHelper.DownloadZipFile(filePathList, fullFilePath);
                    op.Success = true;
                    op.InsertedRowId = schoolGroupId;
                    op.HeaderText = $"{PhoenixConfiguration.Instance.ReadFilePath}{string.Format(Constants.SchoolGroupZipFilePath, ((int)FileManagementModuleTypes.Groups).ToString())}{schoolGroupId.ToString()}.zip";
                    op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
                    op.Message = ResourceManager.GetString("SchoolGroup.SchoolGroup.ZipCreatedSuccessfully");
                }
                catch
                {
                    op.Success = false;
                    op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                    op.Message = LocalizationHelper.TechnicalErrorMessage;
                }
            }
            else
            {
                op.Success = false;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                op.Message = op.Message = ResourceManager.GetString("SchoolGroup.SchoolGroup.NoFilesToZip");
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult SchoolGroups(string grId, string startDate, string endDate, string pfId)
        {
            DateTime stDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(startDate));
            DateTime edDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(endDate));
            var vm = new MyFilesViewModel();
            var module = _fileModuleList.FirstOrDefault(r => r.ModuleName.Contains("Groups"));
            vm.ModuleId = module.ModuleId;
            vm.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            var userId = (int)SessionHelper.CurrentSession.Id;
            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = Convert.ToInt32(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId);
            }
            vm.SectionId = !string.IsNullOrWhiteSpace(grId) ? Convert.ToInt32(EncryptDecryptHelper.DecodeBase64(grId)) : userId;
            var schoolGroup = _schoolGroupService.GetSchoolGroupByID(vm.SectionId);
            ViewBag.Group = schoolGroup;

            IEnumerable<Folder> folderList;
            IEnumerable<File> fileList;
            //IEnumerable<FolderTree> folderTrees;

            IEnumerable<GroupUrl> groupUrlList;
            IEnumerable<GroupQuiz> groupQuizList;
            IEnumerable<QuizView> groupFormList;

            if (!string.IsNullOrWhiteSpace(pfId))
            {
                var parentFolderId = Helpers.CommonHelper.GetDecryptedId(pfId);
                vm.ParentFolderId = parentFolderId;
                folderList = _folderService.GetFoldersByParentFolderId(parentFolderId, module.ModuleId, null);
                fileList = _fileService.GetFilesByFolderId(parentFolderId, null);
                vm.FolderTree = _folderService.GetFolderTree(parentFolderId);
            }
            else
            {
                //Get by SchoolSpaceId
                folderList = _folderService.GetFoldersBySectionId(vm.SectionId, module.ModuleId, null);
                fileList = _fileService.GetFilesBySectionId(vm.SectionId, module.ModuleId, null);
                groupUrlList = _groupUrlService.GetGroupUrlListByGroupId(vm.SectionId);
                groupQuizList = _groupQuizService.GetGroupQuizListByGroupIdAndDateRange(vm.SectionId, stDate, edDate);
                groupFormList = _quizService.GetAllQuizByUserAndSource(SessionHelper.CurrentSession.Id, vm.SectionId, "Group", true);
                vm.GroupUrl = groupUrlList;
                vm.GroupQuiz = groupQuizList;
                vm.GroupForm = groupFormList;
            }

            vm.Folders = folderList.Select(r => EntityMapper<Folder, FolderEdit>.Map(r));
            vm.Files = fileList.Select(r => EntityMapper<File, FileEdit>.Map(r));

            //ViewBag.IsCustomPermission = SetAccessPermissionForModule(module.ModuleId);

            int permission = 0;
            if (SessionHelper.CurrentSession.IsAdmin || SessionHelper.CurrentSession.UserTypeId == 5)
            {
                permission = 3;
            }
            else
            {
                permission = _folderService.GetGroupPermission(Convert.ToInt32(SessionHelper.CurrentSession.Id), vm.SectionId);
            }

            //For archived groups, implemented default permission to deny add/delete features
            if (schoolGroup.ArchivedBy.HasValue && schoolGroup.ArchivedOn.HasValue && DateTime.Compare(schoolGroup.ArchivedOn.Value.Date, DateTime.Today) <= 0)
            {
                ViewBag.IsCustomPermission = false;
                ViewBag.GroupPermission = 1;
            }
            else
            {
                ViewBag.IsCustomPermission = (permission >= 2);
                ViewBag.GroupPermission = permission;
            }
            bool isLastSeenupdated = _schoolGroupService.UpdateLastSeen(userId, vm.SectionId, SessionHelper.CurrentSession.IsParent());
            return View(vm);
        }

        // GET: Files/SchoolSpace
        public ActionResult SchoolSpace(string spId, string pfId)
        {
            var vm = new MyFilesViewModel();
            var module = _fileModuleList.FirstOrDefault(r => r.ModuleName.Contains("School Spaces"));
            vm.ModuleId = module.ModuleId;
            vm.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            var userId = (int)SessionHelper.CurrentSession.Id;
            if (SessionHelper.CurrentSession.IsParent())
            {
                userId = Convert.ToInt32(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId);
            }
            vm.SectionId = !string.IsNullOrWhiteSpace(spId) ? Helpers.CommonHelper.GetDecryptedId(spId) : userId;
            ViewBag.SchoolSpace = _schoolSpaceService.GetSchoolSpaceById(vm.SectionId);

            IEnumerable<Folder> folderList;
            IEnumerable<File> fileList;
            //IEnumerable<FolderTree> folderTrees;

            if (!string.IsNullOrWhiteSpace(pfId))
            {
                var parentFolderId = Helpers.CommonHelper.GetDecryptedId(pfId);
                vm.ParentFolderId = parentFolderId;
                folderList = _folderService.GetFoldersByParentFolderId(parentFolderId, module.ModuleId, null);
                fileList = _fileService.GetFilesByFolderId(parentFolderId, null);
                vm.FolderTree = _folderService.GetFolderTree(parentFolderId);
            }
            else
            {
                //Get by SchoolSpaceId
                folderList = _folderService.GetFoldersBySectionId(vm.SectionId, module.ModuleId, null);
                fileList = _fileService.GetFilesBySectionId(vm.SectionId, module.ModuleId, null);
            }

            vm.Folders = folderList.Select(r => EntityMapper<Folder, FolderEdit>.Map(r));
            vm.Files = fileList.Select(r => EntityMapper<File, FileEdit>.Map(r));

            ViewBag.IsCustomPermission = SetAccessPermissionForModule(module.ModuleId);
            return View(vm);
        }


        public bool SetAccessPermissionForModule(int moduleId)
        {
            var result = false;
            var syncContext = SynchronizationContext.Current;

            switch (moduleId)
            {
                case 1: //My files
                    SynchronizationContext.SetSynchronizationContext(null);
                    result = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_MyFiles.ToString()).Result;
                    SynchronizationContext.SetSynchronizationContext(syncContext);
                    break;
                case 2: // Groups
                    SynchronizationContext.SetSynchronizationContext(null);
                    result = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ViewGroupFiles.ToString()).Result;
                    SynchronizationContext.SetSynchronizationContext(syncContext);
                    break;
                case 3: // School Space
                    SynchronizationContext.SetSynchronizationContext(null);
                    result = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ViewSchoolSpaceFiles.ToString()).Result;
                    SynchronizationContext.SetSynchronizationContext(syncContext);
                    break;
                default: // Lockers
                    SynchronizationContext.SetSynchronizationContext(null);
                    result = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ViewLockerFiles.ToString()).Result;
                    SynchronizationContext.SetSynchronizationContext(syncContext);
                    break;
            }

            return result;

        }

        public ActionResult InitAddEditFile(int fileId)
        {
            var model = new FileEdit();
            var file = _fileService.GetById(fileId);
            EntityMapper<File, FileEdit>.Map(file, model);
            model.FileName = model.FileName.Split('.')[0];
            Session["extension"] = System.IO.Path.GetExtension(file.FileName);
            model.SectId = EncryptDecryptHelper.EncryptUrl(Convert.ToString(model.SectionId));
            return PartialView("_AddEditFileName", model);
        }

        #region Folder Section

        public ActionResult InitAddEditFolderForm(string id, int moduleId, string parentFolderId, string sectionId)
        {
            var model = new FolderEdit();
            if (!string.IsNullOrWhiteSpace(id))
            {
                var folderId = id.ToInteger();
                var folder = _folderService.GetById(folderId);
                EntityMapper<Folder, FolderEdit>.Map(folder, model);
                model.SectId = Convert.ToString(model.SectionId);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = SessionHelper.CurrentSession.SchoolId;
                model.ModuleId = moduleId;
                model.ParentFdId = parentFolderId;
                model.SectId = sectionId;
            }


            return PartialView("_AddEditFolder", model);
        }

        public ActionResult GetFolderListView(int moduleId, string pfId, string sectionId, string sortBy)
        {
            IEnumerable<Folder> folderList;
            if (!string.IsNullOrWhiteSpace(pfId))
            {
                var parentFolderId = Helpers.CommonHelper.GetDecryptedId(pfId);
                folderList = _folderService.GetFoldersByParentFolderId(parentFolderId, moduleId, null);
            }
            else if (!string.IsNullOrWhiteSpace(sectionId))
            {
                var sectId = Helpers.CommonHelper.GetDecryptedId(sectionId);
                folderList = _folderService.GetFoldersBySectionId(sectId, moduleId, null);
            }
            else
            {
                folderList = _folderService.GetFoldersByUserId((int)SessionHelper.CurrentSession.Id, moduleId, null);
            }

            ViewBag.IsCustomPermission = SetAccessPermissionForModule(moduleId);

            var model = folderList.Select(r => EntityMapper<Folder, FolderEdit>.Map(r));

            if (!string.IsNullOrWhiteSpace(sortBy) && sortBy.Equals("Name-Asc", StringComparison.OrdinalIgnoreCase))
            {
                model = model.OrderBy(r => r.FolderName);
            }
            else if (!string.IsNullOrWhiteSpace(sortBy) && sortBy.Equals("Name-Desc", StringComparison.OrdinalIgnoreCase))
            {
                model = model.OrderByDescending(r => r.FolderName);
            }
            else if (!string.IsNullOrWhiteSpace(sortBy) && sortBy.Equals("Date-Asc", StringComparison.OrdinalIgnoreCase))
            {
                model = model.OrderBy(r => r.UpdatedOn);
            }

            if (model.Any() && model.Count() > 0)
            {
                return PartialView("~/Areas/Files/Views/Shared/_ListFolders.cshtml", model);
            }
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveFolderData(FolderEdit model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }

            var folder = new Folder();
            EntityMapper<FolderEdit, Folder>.Map(model, folder);
            folder.IsActive = true;
            folder.ParentFolderId = Helpers.CommonHelper.GetDecryptedId(model.ParentFdId);
            folder.SectionId = Helpers.CommonHelper.GetDecryptedId(model.SectId);
            folder.FolderName = folder.FolderName.Trim();
            int result = 0;
            if (model.FolderId > 0)
            {
                folder.UpdatedBy = SessionHelper.CurrentSession.Id;
                result = _folderService.Update(folder);
            }
            else
            {
                folder.CreatedBy = SessionHelper.CurrentSession.Id;
                result = _folderService.Insert(folder);
            }

            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.DuplicateFolderName), JsonRequestBehavior.AllowGet);
            }
            else
            {
                var op = new OperationDetails(result == 1 ? true : false);
                if (op.Success && model.FolderId == 0) op.Message = ResourceManager.GetString("Files.Folder.FolderUploadSuccess");
                return Json(op, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult DeleteFolderData(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                var folderId = Helpers.CommonHelper.GetDecryptedId(id);
                var result = _folderService.Delete(folderId, SessionHelper.CurrentSession.Id);
                var op = new OperationDetails(result);
                if (op.Success) op.Message = ResourceManager.GetString("Files.Folder.FolderDeleteSuccess");
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region File Section
        public ActionResult GetFileListView(int moduleId, string pfId, string sectionId, string sortBy, bool enableFileDownload = false)
        {
            IEnumerable<File> fileList;
            var sectId = 0;
            if (!string.IsNullOrWhiteSpace(pfId))
            {
                var parentFolderId = Convert.ToInt32(pfId);
                fileList = _fileService.GetFilesByFolderId(parentFolderId, true);
            }
            else if (!string.IsNullOrWhiteSpace(sectionId))
            {
                sectId = Convert.ToInt32(sectionId);
                fileList = _fileService.GetFilesBySectionId(sectId, moduleId, true);
            }
            else
            {
                fileList = _fileService.GetFilesByUserId((int)SessionHelper.CurrentSession.Id, moduleId, null);
            }
            if (moduleId == 2)
            {
                var permission = _folderService.GetGroupPermission(Convert.ToInt32(SessionHelper.CurrentSession.Id), sectId);
                ViewBag.IsCustomPermission = (permission >= 2);
                ViewBag.GroupPermission = permission;
            }
            else
            {
                ViewBag.IsCustomPermission = SetAccessPermissionForModule(moduleId);
            }

            var model = fileList.Select(r => EntityMapper<File, FileEdit>.Map(r));

            if (!string.IsNullOrWhiteSpace(sortBy) && sortBy.Equals("Name-Asc", StringComparison.OrdinalIgnoreCase))
            {
                model = model.OrderBy(r => r.FileName);
            }
            else if (!string.IsNullOrWhiteSpace(sortBy) && sortBy.Equals("Name-Desc", StringComparison.OrdinalIgnoreCase))
            {
                model = model.OrderByDescending(r => r.FileName);
            }
            else if (!string.IsNullOrWhiteSpace(sortBy) && sortBy.Equals("Date-Asc", StringComparison.OrdinalIgnoreCase))
            {
                model = model.OrderBy(r => r.UpdatedOn);
            }

            if (model.Any() && model.Count() > 0)
            {
                // Enable Download All file functionality
                enableFileDownload = Request.UrlReferrer.AbsoluteUri.ToLower().Contains("/files/groups");
                model = model.Select(x => { x.EnableAllFileDownloads = enableFileDownload; x.SectionId = sectId; return x; }).ToList();
                return PartialView("~/Areas/Files/Views/Shared/_ListFiles.cshtml", model);
            }
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveFileData(FileEdit model)
        {
            var file = new File();
            EntityMapper<FileEdit, File>.Map(model, file);
            string fileName = string.Empty;
            string extension = string.Empty;

            model.SchoolId = file.SchoolId = SessionHelper.CurrentSession.SchoolId;
            model.IsActive = file.IsActive = true;
            bool result = false;
            if (model.PostedFile != null)
            {
                SharePointFileView sharePointFileView = new SharePointFileView();
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    sharePointFileView = await azureHelper.UploadStudentFilesAsPerModuleAsync(FileModulesConstants.MyFiles, SessionHelper.CurrentSession.OldUserId.ToString(), model.PostedFile);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    sharePointFileView = SharePointHelper.UploadStudentFilesAsPerModule(ref cx, FileModulesConstants.MyFiles, SessionHelper.CurrentSession.OldUserId.ToString(), model.PostedFile);
                }
                extension = Path.GetExtension(sharePointFileView.ActualFileName).Replace(".", "");
                var extId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;
                file.ResourceFileTypeId = (short)ResourceFileTypes.SharePoint;
                file.FileName = sharePointFileView.ActualFileName;
                file.FilePath = sharePointFileView.ShareableLink;
                file.FileTypeId = extId;
                file.FolderId = Helpers.CommonHelper.GetDecryptedId(model.ParentFolderId);
                file.SectionId = Helpers.CommonHelper.GetDecryptedId(model.SectId);
                file.PhysicalFilePath = sharePointFileView.SharepointUploadedFileURL;
                file.FileSizeInMB = sharePointFileView.FileSizeInMb;
                if (model.FolderId > 0)
                {
                    file.UpdatedBy = SessionHelper.CurrentSession.Id;
                    result = _fileService.Update(file);
                }
                else
                {
                    file.CreatedBy = SessionHelper.CurrentSession.Id;
                    result = _fileService.Insert(file);
                }
                var op = new OperationDetails(result);
                if (result)
                    op.Message = ResourceManager.GetString("Files.File.FileUploadSuccess");
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            else if (model.PostedFileList != null)
            {
                var fileList = new List<File>();
                List<SharePointFileView> sharePointFileViews = new List<SharePointFileView>();
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    sharePointFileViews = await azureHelper.UploadMultipleFilesAsPerModuleAsync(FileModulesConstants.MyFiles, SessionHelper.CurrentSession.OldUserId.ToString(), model.PostedFileList.ToArray());
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    //var syncContext = SynchronizationContext.Current;
                    //SynchronizationContext.SetSynchronizationContext(null);
                    sharePointFileViews = SharePointHelper.UploadMultipleFilesAsPerModule(ref cx, FileModulesConstants.MyFiles, SessionHelper.CurrentSession.OldUserId.ToString(), model.PostedFileList.ToArray());
                    // SynchronizationContext.SetSynchronizationContext(syncContext);
                }

                if (sharePointFileViews.Count > 0)
                {
                    fileList = sharePointFileViews.Select(x => new File()
                    {
                        FileName = x.ActualFileName,
                        FileSizeInMB = x.FileSizeInMb,
                        FilePath = x.ShareableLink,
                        PhysicalFilePath = x.SharepointUploadedFileURL,
                        FileTypeId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(Path.GetExtension(x.ActualFileName).Replace(".", ""))).TypeId,
                        ResourceFileTypeId = (short)ResourceFileTypes.SharePoint,
                        FolderId = Helpers.CommonHelper.GetDecryptedId(model.ParentFolderId),
                        SectionId = Helpers.CommonHelper.GetDecryptedId(model.SectId),
                        ModuleId = model.ModuleId,
                        CreatedBy = SessionHelper.CurrentSession.Id,
                        UpdatedBy = SessionHelper.CurrentSession.Id
                    }).ToList();
                    var myFilesModel = new MyFilesModel();
                    myFilesModel.Files = fileList;
                    result = _fileService.FilesFolderBulkCreate(myFilesModel);
                }
                return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
            }

            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RenameFile(FileEdit model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            var ext = Session["extension"].ToString();
            var UpdatedBy = SessionHelper.CurrentSession.Id;
            model.FileName = model.FileName.Trim() + (string.IsNullOrEmpty(ext) ? string.Empty : ext);
            var result = _fileService.RenameFile(model.FileId, model.FileName, UpdatedBy);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteFileData(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                var fileId = Helpers.CommonHelper.GetDecryptedId(id);
                var file = _fileService.GetById(fileId);
                if (file != null)
                {
                    var filePath = PhoenixConfiguration.Instance.WriteFilePath + file.FilePath;
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }


                var result = _fileService.Delete(fileId, SessionHelper.CurrentSession.Id);
                var op = new OperationDetails(result);
                if (op.Success) op.Message = ResourceManager.GetString("Files.File.FileDeleteSuccess");
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CopyToClipboard(int id)
        {
            var file = _fileService.GetById(id);
            if (Session["clipBoardFiles"] == null)
            {
                List<File> files = new List<File>();
                files.Add(file);
                Session["clipBoardFiles"] = files;
            }
            else
            {
                List<File> files = (List<File>)Session["clipBoardFiles"];
                if (!files.Any(r => r.FileId == file.FileId))
                {
                    files.Add(file);
                    Session["clipBoardFiles"] = null;
                    Session["clipBoardFiles"] = files;
                }
            }
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetClipboard()
        {
            List<File> model = new List<File>();
            if (Session["clipBoardFiles"] != null)
            {
                var fileList = (List<File>)Session["clipBoardFiles"];
                model.AddRange(fileList);
            }
            //if (model.Any() && model.Count() > 0)
            return PartialView("_Clipboard", model);
            //else
            //   return null;
        }

        [HttpPost]
        public ActionResult AddFromClipboard(int id, int moduleId, string parentFolderId, string sectionId)
        {
            bool result = false;

            if (Session["clipBoardFiles"] != null)
            {
                var fileList = (List<File>)Session["clipBoardFiles"];
                var file = _fileService.GetById(id);
                var selectedFile = fileList.Where(r => r.FileId == id).FirstOrDefault();
                int prntFolderId = !string.IsNullOrWhiteSpace(parentFolderId) ? Convert.ToInt32(parentFolderId) : 0;
                int sectId = !string.IsNullOrWhiteSpace(sectionId) ? Convert.ToInt32(sectionId) : 0;
                if (file != null && (file.FolderId != prntFolderId  || file.FileName != selectedFile.FileName) || ((moduleId == 4 || moduleId==2) && sectId != 0 && sectId != file.SectionId))
                {

                    File cbFile = new File();
                    cbFile.FileName = selectedFile.FileName;
                    cbFile.ModuleId = moduleId;
                    cbFile.FolderId = prntFolderId;
                    cbFile.SectionId = sectId;
                    cbFile.FilePath = selectedFile.FilePath;
                    cbFile.FileTypeId = selectedFile.FileTypeId;
                    cbFile.FileSizeInMB = selectedFile.FileSizeInMB;
                    cbFile.ResourceFileTypeId = selectedFile.ResourceFileTypeId;
                    cbFile.GoogleDriveFileId = selectedFile.GoogleDriveFileId;
                    cbFile.SchoolId = SessionHelper.CurrentSession.SchoolId;
                    cbFile.IsActive = true;
                    cbFile.CreatedBy = SessionHelper.CurrentSession.Id;


                    result = _fileService.Insert(cbFile);
                    fileList = fileList.Where(x => x.FileId != id).ToList();
                    Session["clipBoardFiles"] = null;
                    Session["clipBoardFiles"] = fileList.Count > 0 ? fileList : null;
                }
                else
                {
                    return Json(new OperationDetails(result, ResourceManager.GetString("Files.File.Duplicate")), JsonRequestBehavior.AllowGet);
                }



                return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveFromClipboard(int id)
        {
            List<File> model = new List<File>();
            if (Session["clipBoardFiles"] != null)
            {
                var fileList = (List<File>)Session["clipBoardFiles"];
                var file = fileList.Where(r => r.FileId == id).FirstOrDefault();
                fileList.Remove(file);
                Session["clipBoardFiles"] = null;
                Session["clipBoardFiles"] = fileList.Any() && fileList.Count() > 0 ? fileList : null;

            }
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadFile(string id)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(id))
                {
                    var fileId = Helpers.CommonHelper.GetDecryptedId(id);
                    var file = _fileService.GetById(fileId);
                    if (file != null && !string.IsNullOrWhiteSpace(file.FilePath))
                    {
                        string path = PhoenixConfiguration.Instance.ReadFilePath + file.FilePath;

                        WebClient myWebClient = new WebClient();
                        byte[] myDataBuffer = myWebClient.DownloadData(path);
                        Response.BufferOutput = true;

                        return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, file.FileName);

                    }
                }

            }
            catch
            {
                throw new FileNotFoundException();
            }
            return HttpNotFound();
        }

        public ActionResult FileBulkCopy(List<long> ids)
        {
            if (ids != null)
            {
                string filesIds = string.Join(",", ids);
                var fileList = _fileService.GetFilesByIds(filesIds);
                if (Session["clipBoardFiles"] == null)
                {
                    Session["clipBoardFiles"] = fileList;
                }
                else
                {
                    List<File> selectedFiles = (List<File>)Session["clipBoardFiles"];
                    foreach (var file in fileList)
                    {
                        if (!selectedFiles.Any(r => r.FileId == file.FileId))
                        {
                            selectedFiles.Add(file);
                            Session["clipBoardFiles"] = selectedFiles;
                        }
                    }
                }
                return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FileFolderBulkDelete(List<long> filesIds, List<long> foldersIds)
        {
            if (filesIds != null || foldersIds != null)
            {
                var model = new MyFilesModel();
                var selectedFiles = new List<File>();
                var selectedFolders = new List<Folder>();
                if (filesIds != null)
                {
                    foreach (var id in filesIds)
                    {
                        var file = new File();
                        file.FileId = id;
                        selectedFiles.Add(file);
                    }
                }
                if (foldersIds != null)
                {
                    foreach (var id in foldersIds)
                    {
                        var folder = new Folder();
                        folder.FolderId = id;
                        selectedFolders.Add(folder);
                    }
                }
                model.Files = selectedFiles;
                model.Folders = selectedFolders;
                //delete file pending -not implemented in sharepoint
                //string filesIds = string.Join(",",ids);
                //var file = _fileService.GetById(fileId);
                //if (file != null)
                //{
                //    var filePath = PhoenixConfiguration.Instance.WriteFilePath + file.FilePath;
                //    if (System.IO.File.Exists(filePath))
                //    {
                //        System.IO.File.Delete(filePath);
                //    }
                //}
                var result = _fileService.FilesFolderBulkDelete(model);
                return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Form


        public ActionResult InitAddEditQuizForm(string id, string pfId, string moduleId)
        {
            var model = new QuizEdit();
            if (!string.IsNullOrWhiteSpace(id))
            {
                var formId = Helpers.CommonHelper.GetDecryptedId(id);
                model = _quizService.GetQuiz(formId);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = SessionHelper.CurrentSession.SchoolId;
                model.IsForm = true;
                model.QuestionPaginationRange = 1;
                model.FolderId = Helpers.CommonHelper.GetDecryptedId(pfId);
                model.ModuleId = Helpers.CommonHelper.GetDecryptedId(moduleId);
            }
            return PartialView("~/Areas/Forms/Views/Shared/_AddEditQuiz.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveQuizData(QuizEdit model)
        {
            model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            var result = _quizService.QuizCUD(model);
            var op = new OperationDetails(result);
            if (op.Success && model.QuizId == 0) {
                op.Message = model.IsForm ? ResourceManager.GetString("Files.Form.FormAddSuccess") : ResourceManager.GetString("Files.Quiz.QuizAddSuccess");
};
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> UploadFreeTextQuestionFiles(long resourceId, string resourceType, long questionId, long studentId)
        {
            var result = false;
            List<QuizQuestionAnswerFiles> lstQuizAnswerFiles = new List<QuizQuestionAnswerFiles>();
            string relativePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Get all files from Request object
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;

                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        SharePointFileView shv = new SharePointFileView();
                        if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                        {
                            AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                            await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                            shv = await azureHelper.UploadFilesAsPerModuleAsync(FileModulesConstants.QuizQuestionAnswer, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                        }
                        else
                        {
                            SharePointHelper sh = new SharePointHelper();
                            Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);

                            shv = SharePointHelper.UploadFilesAsPerModule(ref cx, FileModulesConstants.QuizQuestionAnswer, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                        }
                        var extension = System.IO.Path.GetExtension(file.FileName);
                        lstQuizAnswerFiles.Add(new QuizQuestionAnswerFiles
                        {
                            FileName = file.FileName,
                            Extension = extension.Replace(".", ""),
                            FilePath = shv.ShareableLink,
                            PhysicalPath = shv.SharepointUploadedFileURL,
                            ResourceId = resourceId,
                            ResourceType = resourceType,
                            QuizQuestionId = questionId,
                            StudentId = studentId
                        });
                    }
                }
                catch (Exception ex)
                {
                }
            }
            if (lstQuizAnswerFiles.Any())
            {
                result = _quizService.UploadQuestionAnswerFiles(lstQuizAnswerFiles);
            }
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetQuestionAnswerFileDetails(long resourceId, string resourceType, long questionId, long studentId)
        {
            var QuizQuestionfile = _quizService.GetQuizQuestionAnswerFiles(resourceId, resourceType, questionId, studentId);
            return PartialView("_QuestionAnswerFileList", QuizQuestionfile);
        }

        [HttpPost]
        public ActionResult DeleteQuestionAnswerFile(long resourceId, string resourceType, long questionId, long studentId, int fileId)
        {
            var result = _quizService.DeleteQuestionAnswerFile(fileId, SessionHelper.CurrentSession.Id);
            var QuizQuestionfile = _quizService.GetQuizQuestionAnswerFiles(resourceId, resourceType, questionId, studentId);
            return PartialView("_QuestionAnswerFileList", QuizQuestionfile);
        }

        [HttpPost]
        public ActionResult LogQuizTime(string id, string ResourceId, string ResourceType, bool IsStartQuiz)
        {
            int quizId = Helpers.CommonHelper.GetDecryptedId(id);
            int resourceId = Helpers.CommonHelper.GetDecryptedId(ResourceId);
            long userId = SessionHelper.CurrentSession.Id;
            var result = _quizService.LogQuizTime(quizId, resourceId, ResourceType, userId, IsStartQuiz);
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteQuizData(string id)
        {
            QuizEdit model = new QuizEdit();
            if (!string.IsNullOrWhiteSpace(id))
            {
                var formId = Helpers.CommonHelper.GetDecryptedId(id);
                model.QuizId = formId;
                model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            }
            var result = _quizService.QuizDelete(model);
            var op = new OperationDetails(result);
            if (op.Success) op.Message = ResourceManager.GetString("Files.Form.FormDeleteSuccess");
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetFormListView(string Id, string sectionType, string SectionId)
        {
            var module = _fileModuleList.FirstOrDefault(r => r.ModuleName.Contains("My Files"));
            ViewBag.IsCustomPermission = SetAccessPermissionForModule(module.ModuleId);
            IEnumerable<QuizView> model = new List<QuizView>();
            string View = String.Empty;
            if (string.IsNullOrWhiteSpace(sectionType))
            {
                if (!string.IsNullOrWhiteSpace(Id) && Id != "undefined")
                {
                    var userId = Helpers.CommonHelper.GetDecryptedId(Id);
                    model = _quizService.GetAllQuizByUser(userId, true);
                }
                View = "~/Areas/Forms/Views/Shared/_ListForms.cshtml";
            }
            if (sectionType == "Group")
            {
                if (!string.IsNullOrWhiteSpace(SectionId))
                {
                    //var sectId =Helpers.CommonHelper.GetDecryptedId(SectionId);
                    model = _quizService.GetAllQuizByUserAndSource(SessionHelper.CurrentSession.Id, Helpers.CommonHelper.GetDecryptedId(SectionId), "Group", true);
                    View = "~/Areas/Files/Views/Shared/_ListGroupForm.cshtml";
                }

            }
            return PartialView(View, model);
        }
        #endregion

        public ActionResult InitAddEditGroupUrl(int id, string grId, string pfId, string moduleId)
        {
            var model = new GroupUrlEdit();
            var grpId = Helpers.CommonHelper.GetDecryptedId(grId);
            if (id > 0)
            {
                var groupUrlModel = _groupUrlService.GetById(id);
                EntityMapper<GroupUrl, GroupUrlEdit>.Map(groupUrlModel, model);
            }
            else
            {
                model.IsAddMode = true;
                model.GroupId = grpId;
                model.FolderId = Helpers.CommonHelper.GetDecryptedId(pfId);
                model.ModuleId = Helpers.CommonHelper.GetDecryptedId(moduleId);
            }


            return PartialView("_AddEditGroupUrl", model);
        }
        public ActionResult InitAddEditGroupQuiz(int id, string grId, string pfId, string moduleId, string cid)
        {
            var model = new GroupQuizEdit();
            List<GradingTemplate> lstGradingTemplate = new List<GradingTemplate>();
            lstGradingTemplate = _gradingTemplateService.GetGradtingTemplates((int)SessionHelper.CurrentSession.SchoolId).ToList();
            var grpId = Helpers.CommonHelper.GetDecryptedId(grId);
            var courseId = Helpers.CommonHelper.GetDecryptedId(cid);
            lstGradingTemplate.Insert(0, new GradingTemplate { GradingTemplateId = 0, GradingTemplateTitle = ResourceManager.GetString("Shared.Labels.Select") });
            if (id > 0)
            {
            }
            else
            {
                model.IsAddMode = true;
                model.GroupId = grpId;
                model.FolderId = Helpers.CommonHelper.GetDecryptedId(pfId);
                model.ModuleId = Helpers.CommonHelper.GetDecryptedId(moduleId);
                model.CourseId.Add(Convert.ToString(courseId));
            }

            List<ListItem> lstCourse = new List<ListItem>();
            lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
            var courses = new SelectList(lstCourse, "ItemId", "ItemName");
            ViewBag.CourseList = courses;
            if (courseId == 0)
            {
                //ViewBag.QuizList = new SelectList(SelectListHelper.GetSelectListData(ListItems.QuizList, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)), "ItemId", "ItemName");
                var QuizList = _groupQuizService.GetQuizByCourse(String.Empty, (int)SessionHelper.CurrentSession.SchoolId);
                ViewBag.QuizList = new SelectList(QuizList.Select(i => new Phoenix.Common.Models.ListItem()
                {
                    ItemName = i.QuizName,
                    ItemId = i.QuizId.ToString()
                }).ToList(), "ItemId", "ItemName");
            }
            else
            {
                var QuizList = _groupQuizService.GetQuizByCourse(string.Join(",", courseId), (int)SessionHelper.CurrentSession.SchoolId);
                ViewBag.QuizList = new SelectList(QuizList.Select(i => new Phoenix.Common.Models.ListItem()
                {
                    ItemName = i.QuizName,
                    ItemId = i.QuizId.ToString()
                }).ToList(), "ItemId", "ItemName");
            }
            //ViewBag.QuizList = new SelectList(listQuiz, "ItemId", "ItemName");

            var temp = lstGradingTemplate;
            model.MaxSubmit = 1;
            model.QuestionPaginationRange = 1;
            model.QuizTime = 1;
            ViewBag.lstGradingTemplate = new SelectList(lstGradingTemplate, "GradingTemplateId", "GradingTemplateTitle", model.GradingTemplateId);
            return PartialView("_AddGroupQuiz", model);
        }
        [HttpPost]
        public JsonResult GetQuizByCourse(int[] id)
        {
            IEnumerable<Phoenix.Common.Models.ListItem> QuizSelectList = new List<Phoenix.Common.Models.ListItem>();
            if (id != null && id.Any())
            {
                int schoolId = (int)SessionHelper.CurrentSession.SchoolId;
                var QuizList = _groupQuizService.GetQuizByCourse(string.Join(",", id), schoolId);
                QuizSelectList = QuizList.Select(i => new Phoenix.Common.Models.ListItem()
                {
                    ItemName = i.QuizName,
                    ItemId = i.QuizId.ToString()
                });
            }
            else
            {
                var QuizList = _groupQuizService.GetQuizByCourse(String.Empty, (int)SessionHelper.CurrentSession.SchoolId);
                QuizSelectList = QuizList.Select(i => new Phoenix.Common.Models.ListItem()
                {
                    ItemName = i.QuizName,
                    ItemId = i.QuizId.ToString()
                });
            }
            return Json(QuizSelectList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveGroupQuiz(GroupQuizEdit model)
        {
            _loggerClient = LoggerClient.Instance;
            if (model.QuizId == 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            var groupQuiz = new GroupQuiz();
            //BELOW CODE IS ADD IN API
            if (model.IsSetTime)
            {
                //convert arabic dates
                model.StartDate = model.strStartDate.ToSystemReadableDate();
                model.StartTime = model.StartTime.ToSystemReadableTime();
                if (!String.IsNullOrEmpty(model.StartTime))
                {
                    //_loggerClient.LogWarning(model.StartDate.Value.ToString());
                    string iString = model.StartDate.Value.ToString("MM/dd/yyyy") + " " + model.StartTime;
                    //DateTime startDateTime = model.StartDate.Value.Add(TimeSpan.Parse(model.StartTime));
                    // _loggerClient.LogWarning(iString);
                    DateTime startDateTime = Convert.ToDateTime(iString);
                    //_loggerClient.LogWarning(startDateTime.ToString());
                    DateTime? testUTC = startDateTime.ConvertLocalToUTC();
                    //_loggerClient.LogWarning(testUTC.ToString());
                    model.StartDate = testUTC;
                    model.StartTime = Convert.ToDateTime(model.StartDate).ToString("h:mm tt");
                    //_loggerClient.LogWarning(model.StartTime);
                }
            }
            if (model.IsHideScore)
            {
                //convert arabic dates
                model.ShowScoreDate = model.strShowScoreDate.ToSystemReadableDate();
            }
            EntityMapper<GroupQuizEdit, GroupQuiz>.Map(model, groupQuiz);
            int result = 0;
            groupQuiz.CreatedBy = SessionHelper.CurrentSession.Id;
            groupQuiz.UpdatedOn = Convert.ToDateTime(DateTime.Now.ConvertLocalToUTC());
            result = _groupQuizService.Insert(groupQuiz);
            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.DuplicateFolderName), JsonRequestBehavior.AllowGet);
            }
            else
            {
                var op = new OperationDetails(result == 1 ? true : false); if (op.Success) op.Message = ResourceManager.GetString("Files.Quiz.QuizAddSuccess");
                return Json(op, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveGroupUrlData(GroupUrlEdit model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }

            var groupUrlModel = new GroupUrl();
            EntityMapper<GroupUrlEdit, GroupUrl>.Map(model, groupUrlModel);
            int result = 0;
            if (model.Id > 0)
            {
                groupUrlModel.UpdatedBy = SessionHelper.CurrentSession.Id;
                result = _groupUrlService.Update(groupUrlModel);
            }
            else
            {
                groupUrlModel.CreatedBy = SessionHelper.CurrentSession.Id;
                result = _groupUrlService.Insert(groupUrlModel);
            }

            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.DuplicateFolderName), JsonRequestBehavior.AllowGet);
            }
            else
            {
                var op = new OperationDetails(result == 1 ? true : false);
                if (op.Success && model.Id == 0) op.Message = ResourceManager.GetString("Files.GroupUrl.GroupUrlSuccess");
                return Json(op, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public ActionResult GetGroupUrlList(string grId)
        {
            var id = Helpers.CommonHelper.GetDecryptedId(grId);
            var model = _groupUrlService.GetGroupUrlListByGroupId(id);
            var module = _fileModuleList.FirstOrDefault(r => r.ModuleName.Contains("My Files"));
            ViewBag.IsCustomPermission = SetAccessPermissionForModule(module.ModuleId);
            if (model.Count() > 0)
                return PartialView("~/Areas/Files/Views/Shared/_ListGroupUrl.cshtml", model);
            else
                return Content("");

        }
        [HttpGet]
        public ActionResult GetGroupQuizList(string grId)
        {
            var id = Helpers.CommonHelper.GetDecryptedId(grId);
            var model = _groupQuizService.GetGroupQuizListByGroupId(id);
            var permission = _folderService.GetGroupPermission(Convert.ToInt32(SessionHelper.CurrentSession.Id), id);
            ViewBag.IsCustomPermission = (permission >= 2);
            ViewBag.GroupPermission = permission;
            if (model.Count() > 0)
                return PartialView("~/Areas/Files/Views/Shared/_ListGroupQuiz.cshtml", model);
            else
                return Content("");

        }
        [HttpGet]
        public ActionResult DeleteGroupUrl(int id)
        {

            var model = _groupUrlService.GetById(id);
            model.DeletedBy = SessionHelper.CurrentSession.Id;
            var result = _groupUrlService.Update(model);
            if (result > 0)
            {
                var op = new OperationDetails(true);
                op.Message = ResourceManager.GetString("Files.GroupUrl.GroupUrlDeleteMessage");
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult DeleteGroupQuiz(int id)
        {

            var model = _groupQuizService.GetById(id);
            model.DeletedBy = SessionHelper.CurrentSession.Id;
            model.UpdatedOn = Convert.ToDateTime(DateTime.Now.ConvertLocalToUTC());
            var result = _groupQuizService.Update(model);
            if (result > 0)
            {
                var op = new OperationDetails(true);
                op.Message = ResourceManager.GetString("Files.Quiz.QuizDeleteSuccess");
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteRecording(int id)
        {
            var result = _fileService.Delete(id, SessionHelper.CurrentSession.Id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Quiz(string id, string resourceId, string studentId)
        {
            //Quiz DATA

            int quizId = Helpers.CommonHelper.GetDecryptedId(id);
            int quizResourceId = resourceId != null ? Helpers.CommonHelper.GetDecryptedId(resourceId) : 0;
            int StudentId = studentId != null ? Helpers.CommonHelper.GetDecryptedId(studentId) : 0;
            var quizDetails = _quizService.GetQuizDetails(quizId, quizResourceId);
            quizDetails.lstDocuments = _quizService.GetFilesByQuizId(quizId);
            Session["QuizId"] = quizId;
            Session["ResourceId"] = quizResourceId;
            Session["StudentId"] = studentId;
            if (StudentId == 0)
            {
                StudentId = (int)SessionHelper.CurrentSession.Id;
            }
            if (SessionHelper.CurrentSession.IsParent())
            {
                StudentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
            }
            IEnumerable<QuizQuestionsEdit> questionList = new List<QuizQuestionsEdit>();
            QuizResult quizResponse = new QuizResult();

            questionList = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            if (questionList.Any())
            {
                foreach (var item in questionList)
                {
                    item.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(item.QuizQuestionId);
                }
            }
            quizResponse = _groupQuizService.GetQuizResultByUserId(quizId, StudentId, quizResourceId);
            if (!quizResponse.ResponseQuestionAnswer.Any() && SessionHelper.CurrentSession.IsStudent())
            {
                var logResult = _groupQuizService.GetLogResultQuestionAnswer(quizId, StudentId, quizResourceId, "Q");
                quizResponse.ResponseQuestionAnswer = logResult.ResponseQuestionAnswer;
            }
            quizResponse.StudentId = (int)SessionHelper.CurrentSession.Id;
            bool IsMarkByTeacher = quizResponse.QuizResponseByTeacherId != 0 ? true : false;
            questionList.Select(x => x.IsCompleteMarkByTeacher = IsMarkByTeacher);
            foreach (var question in questionList)
            {
                var questionDetails = _quizQuestionsService.GetQuizQuestionById(question.QuizQuestionId);
                question.Answers = questionDetails.Answers;
            }
            ViewBag.Quiz = quizDetails;
            ViewBag.QuizDetails = questionList;
            ViewBag.QuizResponse = quizResponse;
            ViewBag.ResourceId = quizResourceId;
            ViewBag.IsView = quizResourceId == 0 ? true : false;
            ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            return View(questionList);
        }

        public async Task<ActionResult> IsSafeQuiz(string quizid)
        {
            int quizId = Helpers.CommonHelper.GetDecryptedId(quizid);
            string data = await _quizService.IsSafeQuiz(quizId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public ActionResult LoadQuizQuestions(int pageIndex = 1, string searchString = "")
        {
            var pageName = "";
            var model = new Pagination<QuizQuestionsEdit>();
            pageName = "_QuizQuestions";
            int quizId = Convert.ToInt32(Session["QuizId"]);
            int quizResourceId = Convert.ToInt32(Session["ResourceId"]);
            var quizDetails = _quizService.GetQuiz(quizId);
            var paginationRange = quizDetails.QuestionPaginationRange == 0 ? 5 : quizDetails.QuestionPaginationRange;
            int StudentId = Helpers.CommonHelper.GetDecryptedId(Session["StudentId"].ToString());
            if (StudentId == 0)
            {
                StudentId = (int)SessionHelper.CurrentSession.Id;
            }
            if (SessionHelper.CurrentSession.IsParent())
            {
                StudentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
            }
            //var assignmentList = _assignmentService.GetAssignmentStudentDetails(pageIndex, 6, Helpers.CommonHelper.GetLoginUser().Id, searchString, assignmentType).ToList();
            //if (assignmentList.Any())
            List<QuizQuestionsEdit> questionList = new List<QuizQuestionsEdit>();
            QuizResult quizResponse = new QuizResult();

            questionList = _quizQuestionsService.GetQuizQuestionsPaginationByQuizId(quizId, pageIndex, paginationRange).ToList();
            if (quizDetails != null)
            {
                questionList.ForEach(x => { x.MaxSubmit = quizDetails.MaxSubmit; });
            }
            if (questionList.Any())
            {
                foreach (var item in questionList)
                {
                    item.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(item.QuizQuestionId);
                }
            }
            quizResponse = _groupQuizService.GetQuizResultByUserId(quizId, StudentId, quizResourceId);
            quizResponse.StudentId = (int)SessionHelper.CurrentSession.Id;
            bool IsMarkByTeacher = quizResponse.QuizResponseByTeacherId != 0 ? true : false;
            questionList.Select(x => x.IsCompleteMarkByTeacher = IsMarkByTeacher);
            foreach (var question in questionList)
            {
                var questionDetails = _quizQuestionsService.GetQuizQuestionById(question.QuizQuestionId);
                question.Answers = questionDetails.Answers;
            }
            if (questionList.Any())
                model = new Pagination<QuizQuestionsEdit>(pageIndex, paginationRange, questionList, questionList.FirstOrDefault().TotalCount);
            model.RecordCount = questionList.Count == 0 ? 0 : questionList[0].TotalCount;

            model.LoadPageRecordsUrl = "/Files/Files/LoadQuizQuestions?pageIndex={0}";
            model.SearchString = searchString;
            ViewBag.Quiz = quizDetails;
            ViewBag.QuizDetails = questionList;
            ViewBag.QuizResponse = quizResponse;
            ViewBag.ResourceId = quizResourceId;
            return PartialView(pageName, model);
        }


        public ActionResult Form(string id, string resourceId, string studentId)
        {
            //Quiz DATA

            int quizId = Helpers.CommonHelper.GetDecryptedId(id);
            int quizResourceId = Helpers.CommonHelper.GetDecryptedId(resourceId);
            int StudentId = Helpers.CommonHelper.GetDecryptedId(studentId);
            Session["QuizId"] = quizId;
            Session["ResourceId"] = quizResourceId;
            Session["StudentId"] = studentId;
            if (StudentId == 0)
            {
                StudentId = (int)SessionHelper.CurrentSession.Id;
            }
            IEnumerable<QuizQuestionsEdit> questionList = new List<QuizQuestionsEdit>();
            QuizResult quizResponse = new QuizResult();

            var quizDetails = _quizService.GetQuiz(quizId);
            var paginationRange = quizDetails.QuestionPaginationRange == 0 ? 5 : quizDetails.QuestionPaginationRange;
            questionList = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            quizResponse = _groupQuizService.GetQuizResultByUserId(quizId, StudentId, quizResourceId);
            quizResponse.StudentId = (int)SessionHelper.CurrentSession.Id;
            bool IsMarkByTeacher = quizResponse.QuizResponseByTeacherId != 0 ? true : false;
            questionList.Select(x => x.IsCompleteMarkByTeacher = IsMarkByTeacher);
            foreach (var question in questionList)
            {
                var questionDetails = _quizQuestionsService.GetQuizQuestionById(question.QuizQuestionId);
                question.Answers = questionDetails.Answers;
                question.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(question.QuizQuestionId);
                //question.lstDocuments = _quizService.GetFilesByQuizId(quizId);
            }
            ViewBag.Quiz = quizDetails;
            ViewBag.QuizDetails = questionList;
            ViewBag.QuizResponse = quizResponse;
            ViewBag.ResourceId = quizResourceId;
            return View(questionList);
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult SubmitQuiz(QuizResult model, List<QuizAnswersEdit> answersList)
        {
            model.QuizResponseByUserId = (int)SessionHelper.CurrentSession.Id;
            //foreach (var item in answersList)
            //{
            //    var questionDetails = _quizQuestionsService.GetQuizQuestionById(item.QuizQuestionId);
            //    if (questionDetails.QuestionTypeId == 7)//for sort quiz question answer set obtain marks
            //    {

            //        var QuizAnswers = _quizQuestionsService.GetQuizAnswerByQuizQuestionId(item.QuizQuestionId);
            //        int answerCount = QuizAnswers.Count();
            //        //var userAnswer= answersList.Where(x=>x.QuizAnswerId==)

            //    }
            //}

            if (SessionHelper.CurrentSession.IsTeacher())
            {
                model.QuizResponseByTeacherId = (int)SessionHelper.CurrentSession.Id;
            }
            var sourceModel = new List<QuizAnswersView>();
            foreach (var item in answersList)
            {
                var quizAnswersView = new QuizAnswersView();
                EntityMapper<QuizAnswersEdit, QuizAnswersView>.Map(item, quizAnswersView);
                sourceModel.Add(quizAnswersView);
            }

            //EntityMapper<List<QuizAnswersEdit>, List<QuizAnswersView>>.Map(answersList, sourceModel);
            model.ResponseQuestionAnswer = sourceModel;
            var result = _groupQuizService.InsertUpdateQuizAnswers(model);
            //var result = _quizQuestionsService.UpdateQuizAnswerData(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LogQuizQuestionAnswer(QuizResult model, List<QuizAnswersEdit> answersList)
        {
            if (answersList != null)
            {
                answersList = answersList.Where(x => x.ResourceType == null).ToList();
                answersList.ForEach(x => x.ResourceId = model.TaskId);
                answersList.ForEach(x => x.ResourceType = model.ResourceType);
                model.QuizResponseByUserId = (int)SessionHelper.CurrentSession.Id;
                var sourceModel = new List<QuizAnswersView>();
                foreach (var item in answersList)
                {
                    var quizAnswersView = new QuizAnswersView();
                    EntityMapper<QuizAnswersEdit, QuizAnswersView>.Map(item, quizAnswersView);
                    sourceModel.Add(quizAnswersView);
                }
                model.ResponseQuestionAnswer = sourceModel;
                var result = _groupQuizService.LogQuizQuestionAnswer(model);
            }
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateQuizGrade(int gradeId, int quizId, long studentId, int QuizResultId, int gradingTemplateId)
        {
            var result = _groupQuizService.UpdateQuizGrade(gradeId, gradingTemplateId, QuizResultId);
            var gradingItem = _gradingTemplateItemService.GetGradingTemplateItemById(gradeId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
            return Json(gradingItem, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult InsertQuizFeedback(QuizFeedbackEdit quizFeedback, List<HttpPostedFileBase> recordings)
        {
            if (recordings != null)
            {
                foreach (var item in recordings)
                {
                    var filesType = _fileService.GetFileTypes("Recording");
                    var fileModule = _fileService.GetFileManagementModules().ToList();
                    int i = 1;
                    File file = new File();
                    file.SchoolId = SessionHelper.CurrentSession.SchoolId;
                    file.ModuleId = fileModule.Where(x => x.ModuleName == "Feedback").Select(x => x.ModuleId).FirstOrDefault();
                    file.SectionId = quizFeedback.QuizResultId;
                    file.FileName = SessionHelper.CurrentSession.FirstName + "_" + SessionHelper.CurrentSession.LastName + "_" + i + Path.GetExtension(item.FileName);
                    file.FileTypeId = filesType.Select(x => x.TypeId).FirstOrDefault();
                    file.FilePath = UploadQuizFeedbackResource(item);
                    i++;
                    quizFeedback.Files.Add(file);
                }
            }
            quizFeedback.UserId = SessionHelper.CurrentSession.Id;
            var result = _groupQuizService.InsertQuizFeedback(quizFeedback);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        private string UploadQuizFeedbackResource(HttpPostedFileBase resourceFile)
        {
            string GUID = Guid.NewGuid().ToString();
            string filePath = String.Empty;
            string strFile = String.Format("{0}{1}", GUID, Path.GetExtension(resourceFile.FileName));
            string relativeFilePath = String.Empty;
            //string strFile = suggestionFile.FileName;
            string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
            relativeFilePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizFeedbackDir;
            //filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizFeedbackDir + "/User_" + SessionHelper.CurrentSession.Id;
            Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
            Common.Helpers.CommonHelper.CreateDestinationFolder(relativeFilePath);
            //Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
            filePath = Path.Combine(relativeFilePath, strFile);
            //relativeFilePath = Constants.PlannerDir + "/User_" + SessionHelper.CurrentSession.Id;
            string path = Constants.QuizFeedbackDir + "/" + strFile;
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            resourceFile.SaveAs(filePath);
            return path;
        }
        public ActionResult GroupStudentDetails(string id, string groupId, bool isForm, string selectedGroupId)
        {
            int groupQuizId = Helpers.CommonHelper.GetDecryptedId(id);
            int decrptGroupId = Helpers.CommonHelper.GetDecryptedId(groupId);
            var groupQuiz = new GroupQuiz();
            if (isForm)
            {
                groupQuiz = _groupQuizService.GetFormStudentDetailsByQuizId(groupQuizId, decrptGroupId);
                groupQuiz.IsForm = true;
            }
            else
            {
                groupQuiz = _groupQuizService.GetQuizStudentDetailsByQuizId(groupQuizId, decrptGroupId, Helpers.CommonHelper.GetDecryptedId(selectedGroupId));
                groupQuiz.IsForm = false;
                Session["questionWiseReport"] = groupQuiz.QuestionWiseReport;
                groupQuiz.GroupQuizId = groupQuizId;
            }
            return View(groupQuiz);
        }

        #region Download As PDF


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadReportToPDF")]
        public ActionResult DownloadReportDataAsPDF(FormCollection form)
        {
            string pdfhtml = "";
            string pdfTitle = "";
            string downloadFileName = "";
            string reportName = form["reportName"];
            ViewBag.ReportType = "PDF";
            var questionWiseReport = Session["questionWiseReport"] as List<QuestionWiseReport>;

            switch (reportName)
            {
                case "QuestionWiseReport":
                    {
                        pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_QuestionWiseReport", questionWiseReport);
                        pdfTitle = ResourceManager.GetString("Quiz.Report.QuestionWiseReport");
                        downloadFileName = "QuestionWiseReport.pdf";
                        break;
                    }
            }

            PdfDocument pdf = new PdfDocument();

            pdf = PdfGenerator.GeneratePdf(pdfhtml, PageSize.A4);
            pdf.Info.Title = pdfTitle;
            MemoryStream stream = new MemoryStream();
            pdf.Save(stream, false);
            byte[] file = stream.ToArray();
            stream.Write(file, 0, file.Length);
            stream.Position = 0;

            return File(stream, "application/pdf", downloadFileName);

        }

       
        public ActionResult DownloadReportCardAsPDF(string id, string resourceId, string studentId, string resourceType)
        {
            string pdfhtml = String.Empty;
            string pdfTitle = String.Empty;
            string downloadFileName = String.Empty;
            int quizId = Helpers.CommonHelper.GetDecryptedId(id);
            int quizResourceId = resourceId != null ? Helpers.CommonHelper.GetDecryptedId(resourceId) : 0;
            int StudentId = studentId != null ? Helpers.CommonHelper.GetDecryptedId(studentId) : 0;

            ViewBag.ReportType = "PDF";
            var reportCardData = _quizService.GetQuizReportCardByUserId(quizId, StudentId, quizResourceId, resourceType);

            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_ReportCard", reportCardData);
            pdfTitle = ResourceManager.GetString("Assignment.Assignment.ReportCard");
            downloadFileName = "ReportCard.pdf";

            PdfDocument pdf = new PdfDocument();

            pdf = PdfGenerator.GeneratePdf(pdfhtml, PageSize.A4);
            pdf.Info.Title = pdfTitle;
            MemoryStream stream = new MemoryStream();
            pdf.Save(stream, false);
            byte[] file = stream.ToArray();
            stream.Write(file, 0, file.Length);
            stream.Position = 0;

            return File(stream, "application/pdf", downloadFileName);

        }

        #endregion

        #region  Download Report Data As Excel

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadReportToExcel")]
        public ActionResult DownloadReportDataAsExcelFile(FormCollection form)
        {
            string path = string.Empty;
            string downloadFileName = "";
            byte[] bytes;
            string reportName = form["reportName"];
            var questionWiseReport = Session["questionWiseReport"] as List<QuestionWiseReport>;

            path = Server.MapPath("~/Content/Files/EmptyReportFile.xls");

            switch (reportName)
            {
                case "QuestionWiseReport":
                    {
                        bytes = _quizService.GenerateQuestionWiseReportXLSFile(path, questionWiseReport);
                        downloadFileName = "QuestionWiseReport.xls";
                        return File(bytes, "application/vnd.ms-excel", downloadFileName);
                    }
                default:
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadGroupQuizReportToExcel")]
        public ActionResult DownloadGroupQuizReportToExcel(int quizId, int groupQuizId)
        {
            string path = string.Empty;
            string downloadFileName = "";
            byte[] bytes;
            var quizReport = _quizService.GetGroupQuizReport(quizId, groupQuizId);

            path = Server.MapPath("~/Content/Files/EmptyReportFile.xls");

            bytes = _quizService.GenerateQuizReportXLSFile(path, quizReport);
            downloadFileName = "GroupQuizReport.xls";
            return File(bytes, "application/vnd.ms-excel", downloadFileName);
        }

        #endregion

        public ActionResult LoadGradingTemplateItems(int gradingtemplateId, long studentId, int quizId, int QuizResultId, int selectedGradeId, int gradingType)
        {
            List<GradingTemplateItem> lstGradingtemplate = _gradingTemplateItemService.GetGradingTemplateItems(gradingtemplateId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId).ToList();
            ViewBag.quizId = quizId;
            ViewBag.entityId = studentId;
            ViewBag.gradingType = gradingType;
            ViewBag.QuizResultId = QuizResultId;
            ViewBag.selectedGradeId = selectedGradeId;
            return PartialView("_LoadGradingTemplateItems", lstGradingtemplate);
        }
        public ActionResult LoadQuizFeedBack(long studentId, int quizId, int QuizResultId, int ResourceId)
        {
            var quizResponse = _groupQuizService.GetQuizResultByUserId(quizId, Convert.ToInt32(studentId), ResourceId);
            ViewBag.quizId = quizId;
            ViewBag.entityId = studentId;
            ViewBag.QuizResultId = QuizResultId;
            return PartialView("_LoadQuizFeedBack", quizResponse);
        }
        public ActionResult ResetMTPQuestion(int quizQuestionId)
        {
            var quizId = Helpers.CommonHelper.GetDecryptedId(Session["QuizId"].ToString());
            var quizResourceId = Helpers.CommonHelper.GetDecryptedId(Session["ResourceId"].ToString());
            var StudentId = Helpers.CommonHelper.GetDecryptedId(Session["StudentId"].ToString());
            IEnumerable<QuizQuestionsEdit> quizQuestionList = new List<QuizQuestionsEdit>();
            QuizResult quizResponse = new QuizResult();

            quizQuestionList = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            QuizQuestionsEdit questionList = quizQuestionList.Where(a => a.QuizQuestionId == quizQuestionId).FirstOrDefault();
            if (questionList != null)
            {
                questionList.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(questionList.QuizQuestionId);
            }
            quizResponse = _groupQuizService.GetQuizResultByUserId(quizId, StudentId, quizResourceId);
            quizResponse.StudentId = (int)SessionHelper.CurrentSession.Id;
            bool IsMarkByTeacher = quizResponse.QuizResponseByTeacherId != 0 ? true : false;
            questionList.IsCompleteMarkByTeacher = IsMarkByTeacher;
            var questionDetails = _quizQuestionsService.GetQuizQuestionById(questionList.QuizQuestionId);
            questionList.Answers = questionDetails.Answers;
            return PartialView("_DragDrop", questionList);
        }
        #region bookmark
        public ActionResult AddEditBookmark(string id)
        {
            var model = new BookmarkEdit();
            long bookmarkId = 0;
            List<Phoenix.Models.SchoolGroup> lstSchoolGroups = new List<Phoenix.Models.SchoolGroup>();
            if (!string.IsNullOrWhiteSpace(id))
                bookmarkId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(id));
            if (bookmarkId > 0)
            {
                lstSchoolGroups = _bookmarkService.GetBookmarkSchoolGroups(bookmarkId).ToList();
                model.SchoolGroupId = lstSchoolGroups.Select(m => m.SchoolGroupId.ToString()).ToList();
                var bookmark = _bookmarkService.GetBookmark(bookmarkId);
                EntityMapper<Bookmark, BookmarkEdit>.Map(bookmark, model);
            }
            else
            {
                model.SchoolGroupId = new List<string>();
            }
            List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
            lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            lstSchoolGroup = lstSchoolGroup.Where(m => m.IsContribute == true || m.IsManager == true).ToList();
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });
            var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName,
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
            }).ToList();
            ViewBag.lstSchoolGroup = groupedOptions;
            return PartialView("_AddEditBookmark", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveBookmark(BookmarkEdit model)
        {
            //if (ModelState.IsValid)
            //{

            //}
            model.UserId = (int)SessionHelper.CurrentSession.Id;
            model.BookmarkName = model.BookmarkName.Trim();
            var result = _bookmarkService.AddUpdateBookmarkData(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetBookmarks()
        {
            var module = _fileModuleList.FirstOrDefault(r => r.ModuleName.Contains("My Files"));
            ViewBag.IsCustomPermission = SetAccessPermissionForModule(module.ModuleId);
            long userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            List<Bookmark> lstBookmarks = new List<Bookmark>();
            lstBookmarks = _bookmarkService.GetBookmarks(userId).ToList();
            return PartialView("_ListBookmarks", lstBookmarks);
        }

        public ActionResult DeleteBookmark(string id)
        {
            long bookmarkId = 0;
            if (!string.IsNullOrWhiteSpace(id))
                bookmarkId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(id));
            var result = _bookmarkService.DeleteBookmarkData(bookmarkId);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region flickr image search

        public ActionResult ImageSearch()
        {
            return View();
        }

        public ActionResult LoadFlickrImages(string search, int currentPage = 1, int pageSize = 20)
        {
            FlickrManager flickr = new FlickrManager();
            MediaModal media = new MediaModal();
            media = flickr.GetPhotos(search, currentPage, pageSize);

            return PartialView("_FlickrImagePartial", media);
        }

        [HttpPost]
        public JsonResult SaveFile(string url, string imgName)
        {
            string fileName = string.Empty;
            string relativePath = string.Empty;
            string srcPath = string.Empty;
            string trgPath = string.Empty;
            string extension = string.Empty;
            var module = _fileModuleList.FirstOrDefault(r => r.ModuleName.Contains("My Files"));
            bool result = false;
            if (string.IsNullOrWhiteSpace(imgName) || string.IsNullOrWhiteSpace(url))
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            imgName = imgName.Replace(" ", "_");

            WebClient myWebClient = new WebClient();

            byte[] myDataBuffer = myWebClient.DownloadData(url);


            extension = url.Substring(url.LastIndexOf(".") + 1);
            fileName = Guid.NewGuid() + "_" + imgName + "." + extension;
            var extId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(extension.ToLower())).TypeId;

            string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
            string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.FileDir;
            string fileModule = PhoenixConfiguration.Instance.WriteFilePath + Constants.FileDir + "/Module_" + module.ModuleId;
            trgPath = PhoenixConfiguration.Instance.WriteFilePath + Constants.FileDir + "/Module_" + module.ModuleId + "/User_" + SessionHelper.CurrentSession.Id;

            Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
            Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);
            Common.Helpers.CommonHelper.CreateDestinationFolder(fileModule);
            Common.Helpers.CommonHelper.CreateDestinationFolder(trgPath);
            relativePath = Constants.FileDir + "/Module_" + module.ModuleId + "/User_" + SessionHelper.CurrentSession.Id + "/" + fileName;
            trgPath = Path.Combine(trgPath, fileName);

            System.IO.File.WriteAllBytes(trgPath, myDataBuffer);


            File flickrFile = new File();
            flickrFile.FileName = imgName + "." + extension;
            flickrFile.ModuleId = module.ModuleId;
            flickrFile.FolderId = 0;
            flickrFile.SectionId = 0;
            flickrFile.FilePath = relativePath;
            flickrFile.FileTypeId = extId;

            flickrFile.SchoolId = SessionHelper.CurrentSession.SchoolId;
            flickrFile.IsActive = true;
            flickrFile.CreatedBy = SessionHelper.CurrentSession.Id;


            result = _fileService.Insert(flickrFile);

            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);

        }

        #endregion
        #region Download PDF
        [AllowAnonymous]
        public ActionResult GenerateQuizResultPdf()
        {
            int quizId = Helpers.CommonHelper.GetDecryptedId(Session["QuizId"].ToString());
            int quizResourceId = Helpers.CommonHelper.GetDecryptedId(Session["ResourceId"].ToString());
            int StudentId = Session["StudentId"] != null ? Helpers.CommonHelper.GetDecryptedId(Session["StudentId"].ToString()) : 0;
            var quizDetails = _quizService.GetQuiz(quizId);
            quizDetails.lstDocuments = _quizService.GetFilesByQuizId(quizId);
            if (StudentId == 0)
            {
                StudentId = (int)SessionHelper.CurrentSession.Id;
            }
            if (SessionHelper.CurrentSession.IsParent())
            {
                StudentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
            }
            IEnumerable<QuizQuestionsEdit> questionList = new List<QuizQuestionsEdit>();
            QuizResult quizResponse = new QuizResult();

            questionList = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            if (questionList.Any())
            {
                foreach (var item in questionList)
                {
                    item.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(item.QuizQuestionId);
                }
            }
            quizResponse = _groupQuizService.GetQuizResultByUserId(quizId, StudentId, quizResourceId);
            quizResponse.StudentId = (int)SessionHelper.CurrentSession.Id;
            bool IsMarkByTeacher = quizResponse.QuizResponseByTeacherId != 0 ? true : false;
            questionList.Select(x => x.IsCompleteMarkByTeacher = IsMarkByTeacher);
            foreach (var question in questionList)
            {
                var questionDetails = _quizQuestionsService.GetQuizQuestionById(question.QuizQuestionId);
                question.Answers = questionDetails.Answers;
            }
            ViewBag.Quiz = quizDetails;
            ViewBag.QuizDetails = questionList;
            ViewBag.QuizResponse = quizResponse;
            ViewBag.ResourceId = quizResourceId;
            ViewBag.IsView = quizResourceId == 0 ? true : false;
            return View(questionList);
        }
        [AllowAnonymous]
        public ActionResult TaskReportPDF()
        {
            int quizId = Helpers.CommonHelper.GetDecryptedId(Session["QuizId"].ToString());
            int quizResourceId = Helpers.CommonHelper.GetDecryptedId(Session["ResourceId"].ToString());
            int StudentId = Session["StudentId"] != null ? Helpers.CommonHelper.GetDecryptedId(Session["StudentId"].ToString()) : 0;
            var quizDetails = _quizService.GetQuiz(quizId);
            quizDetails.lstDocuments = _quizService.GetFilesByQuizId(quizId);
            if (StudentId == 0)
            {
                StudentId = (int)SessionHelper.CurrentSession.Id;
            }
            if (SessionHelper.CurrentSession.IsParent())
            {
                StudentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
            }
            IEnumerable<QuizQuestionsEdit> questionList = new List<QuizQuestionsEdit>();
            QuizResult quizResponse = new QuizResult();

            questionList = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            if (questionList.Any())
            {
                foreach (var item in questionList)
                {
                    item.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(item.QuizQuestionId);
                }
            }
            quizResponse = _groupQuizService.GetQuizResultByUserId(quizId, StudentId, quizResourceId);
            quizResponse.StudentId = (int)SessionHelper.CurrentSession.Id;
            bool IsMarkByTeacher = quizResponse.QuizResponseByTeacherId != 0 ? true : false;
            questionList.Select(x => x.IsCompleteMarkByTeacher = IsMarkByTeacher);
            foreach (var question in questionList)
            {
                var questionDetails = _quizQuestionsService.GetQuizQuestionById(question.QuizQuestionId);
                question.Answers = questionDetails.Answers;
            }
            ViewBag.Quiz = quizDetails;
            ViewBag.QuizDetails = questionList;
            ViewBag.QuizResponse = quizResponse;
            ViewBag.ResourceId = quizResourceId;
            ViewBag.IsView = quizResourceId == 0 ? true : false;
            return new Rotativa.ViewAsPdf("_LoadQuiz", questionList);

            //Dictionary<string, string> cookieCollection = new Dictionary<string, string>();
            //foreach (var key in Request.Cookies.AllKeys)
            //{
            //    cookieCollection.Add(key, Request.Cookies.Get(key).Value);
            //}
            //return new Rotativa.ActionAsPdf("GenerateQuizResultPdf")
            //{
            //    FileName = "Result.pdf",
            //    Cookies = cookieCollection
            //};

            ////var viewToString = StringUtilities.RenderViewToString(ControllerContext, "~/Views/Home/Index.cshtml", viewModel, true);
            //var html = RenderToString(PartialView("_LoadQuiz"));
            //// return resulted pdf document
            //FileResult fileResult = new FileContentResult(PdfSharpConvert(html), "application/pdf");
            //fileResult.FileDownloadName = "Document.pdf";
            //return fileResult;



        }
        public string RenderToString(PartialViewResult partialView)
        {
            var httpContext = System.Web.HttpContext.Current;

            if (httpContext == null)
            {
                throw new NotSupportedException("An HTTP context is required to render the partial view to a string");
            }

            var controllerName = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();

            var controller = (ControllerBase)ControllerBuilder.Current.GetControllerFactory().CreateController(httpContext.Request.RequestContext, controllerName);

            var controllerContext = new ControllerContext(httpContext.Request.RequestContext, controller);

            var view = ViewEngines.Engines.FindPartialView(controllerContext, partialView.ViewName).View;

            var sb = new StringBuilder();

            using (var sw = new StringWriter(sb))
            {
                using (var tw = new HtmlTextWriter(sw))
                {
                    view.Render(new ViewContext(controllerContext, view, partialView.ViewData, partialView.TempData, tw), tw);
                }
            }

            return sb.ToString();
        }
        public Byte[] PdfSharpConvert(String html)
        {
            Byte[] res = null;
            using (MemoryStream ms = new MemoryStream())
            {
                var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.A4);
                pdf.Save(ms);
                res = ms.ToArray();
            }
            return res;
        }
        //public void TaskReportPDF()
        //{
        //    /////START FROM HERE
        //    int quizId = Convert.ToInt32(Session["QuizId"]);
        //    int quizResourceId= Convert.ToInt32(Session["ResourceId"]);
        //    int StudentId = Session["StudentId"] != null ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(Session["StudentId"].ToString())) : 0;
        //    var quizDetails = _quizService.GetQuiz(quizId);
        //    quizDetails.lstDocuments = _quizService.GetFilesByQuizId(quizId);
        //    if (StudentId == 0)
        //    {
        //        StudentId = (int)SessionHelper.CurrentSession.Id;
        //    }
        //    if (SessionHelper.CurrentSession.IsParent())
        //    {
        //        StudentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
        //    }
        //    IEnumerable<QuizQuestionsEdit> questionList = new List<QuizQuestionsEdit>();
        //    QuizResult quizResponse = new QuizResult();

        //    questionList = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
        //    if (questionList.Any())
        //    {
        //        foreach (var item in questionList)
        //        {
        //            item.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(item.QuizQuestionId);
        //        }
        //    }
        //    quizResponse = _groupQuizService.GetQuizResultByUserId(quizId, StudentId, quizResourceId);
        //    quizResponse.StudentId = (int)SessionHelper.CurrentSession.Id;
        //    bool IsMarkByTeacher = quizResponse.QuizResponseByTeacherId != 0 ? true : false;
        //    questionList.Select(x => x.IsCompleteMarkByTeacher = IsMarkByTeacher);
        //    foreach (var question in questionList)
        //    {
        //        var questionDetails = _quizQuestionsService.GetQuizQuestionById(question.QuizQuestionId);
        //        question.Answers = questionDetails.Answers;
        //    }
        //    ViewBag.Quiz = quizDetails;
        //    ViewBag.QuizDetails = questionList;
        //    ViewBag.QuizResponse = quizResponse;
        //    ViewBag.ResourceId = quizResourceId;
        //    ViewBag.IsView = quizResourceId == 0 ? true : false;

        //    ///END HERE
        //   // var taskReportdata = _taskreportService.GetTaskReport(taskParam).OrderBy(item => item.DateCreated).OrderBy(item => item.AuditName);

        //    var taskHtml = ControllerExtension.RenderPartialViewToString(this, "Quiz", questionList);

        //    var config = new PdfGenerateConfig();
        //    config.PageOrientation = PageOrientation.Landscape;
        //    config.PageSize = PageSize.A3;
        //    PdfDocument pdf = PdfGenerator.GeneratePdf(taskHtml, config);

        //    // Send PDF to browser
        //    MemoryStream stream = new MemoryStream();
        //    pdf.Save(stream, false);
        //    Response.Clear();
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-length", stream.Length.ToString());
        //    Response.BinaryWrite(stream.ToArray());
        //    Response.Flush();
        //    stream.Close();
        //    Response.End();
        //}

        #endregion

        #region Cloud Storage Files Save
        [HttpPost]
        public async Task<ActionResult> SaveCloudFiles(string fileIds, string sectionId, string parentFolderId, int? moduleId, short resourceFileTypeId)
        {
            var selectedFiles = JsonConvert.DeserializeObject<List<CloudFileListView>>(fileIds);

            IList<File> files = new List<File>();
            foreach (var item in selectedFiles)
            {
                var file = new File();
                if (string.IsNullOrEmpty(item.FileExtension))
                    item.FileExtension = Helpers.CommonHelper.GetFileExtensionByMimeType(item.MimeType);
                else if (item.FileExtension.Equals("onenote", StringComparison.OrdinalIgnoreCase))
                    item.FileExtension = "one";
                file.GoogleDriveFileId = item.Id;
                file.CreatedBy = SessionHelper.CurrentSession.Id;
                file.SectionId = !string.IsNullOrEmpty(sectionId) ? Helpers.CommonHelper.GetDecryptedId(sectionId) : 0;
                file.FolderId = !string.IsNullOrEmpty(parentFolderId) ? Helpers.CommonHelper.GetDecryptedId(parentFolderId) : 0;
                file.FileName = item.Name;
                file.FilePath = item.WebViewLink;
                file.FileTypeId = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(item.FileExtension.ToLower())).TypeId;
                file.ResourceFileTypeId = item.ResourceFileTypeId != 0 ? item.ResourceFileTypeId : resourceFileTypeId;
                file.ModuleId = moduleId.HasValue ? moduleId.Value : 0;
                files.Add(file);
            }

            bool result = _fileService.SaveCloudFiles(files);

            #region Resource File Share With users
            if (result)
            {
                moduleId = moduleId.HasValue ? moduleId.Value : 0;
                var module = _fileModuleList.FirstOrDefault(r => r.ModuleId == moduleId.Value);
                if (module.ModuleName.ToLower().Contains("locker"))
                {
                    var userInfo = _studentService.GetStudentByUserId(Convert.ToInt64(sectionId));
                    var student = _schoolGroupService.GetSchoolGroupUserEmailAddress(string.Empty, userInfo.StudentId.ToString()).FirstOrDefault();
                    if (student != null)
                    {
                        foreach (var file in files)
                        {
                            if (file.ResourceFileTypeId == (short)ResourceFileTypes.OneDrive)
                                await CloudFilesHelper.ShareOneDriveFile(file.GoogleDriveFileId, new List<string>() { student.MicorosoftMailAddress }, "read");
                            else if (file.ResourceFileTypeId == (short)ResourceFileTypes.OneNote)
                            {
                                await CloudFilesHelper.AssignNotebookToStudent(student.MicorosoftMailAddress, file.GoogleDriveFileId);
                            }
                            else if (file.ResourceFileTypeId == (short)ResourceFileTypes.GoogleDrive)
                            {
                                CancellationToken cancellationToken = new CancellationToken();
                                var authResult = new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).
                                   AuthorizeAsync(cancellationToken).GetAwaiter().GetResult();
                                CloudFilesHelper.AddGoogleDriveUserPermission(authResult, student.GoogleMailAddress, file.GoogleDriveFileId, "reader");
                            }
                        }
                    }

                }
                else if (module.ModuleName.ToLower().Contains("groups"))
                {
                    var studentsList = _schoolGroupService.GetSchoolGroupUserEmailAddress(string.Join(",", sectionId), string.Empty).ToList();
                    if (studentsList != null && studentsList.Count > 0)
                    {
                        foreach (var file in files)
                        {
                            if (file.ResourceFileTypeId == (short)ResourceFileTypes.OneDrive)
                                await CloudFilesHelper.ShareOneDriveFile(file.GoogleDriveFileId, studentsList.Select(e => e.MicorosoftMailAddress), "read");
                            else if (file.ResourceFileTypeId == (short)ResourceFileTypes.OneNote)
                            {
                                foreach (var std in studentsList)
                                    await CloudFilesHelper.AssignNotebookToStudent(std.MicorosoftMailAddress, file.GoogleDriveFileId);
                            }
                            else if (file.ResourceFileTypeId == (short)ResourceFileTypes.GoogleDrive)
                            {
                                CancellationToken cancellationToken = new CancellationToken();
                                var authResult = new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).
                                   AuthorizeAsync(cancellationToken).GetAwaiter().GetResult();
                                CloudFilesHelper.AddGoogleDriveUserPermission(authResult, string.Join(",", studentsList.Where(e => !string.IsNullOrEmpty(e.GoogleMailAddress)).Select(e => e.GoogleMailAddress)), file.GoogleDriveFileId, "reader");
                            }
                        }
                    }

                }
            }
            #endregion

            var op = new OperationDetails(result);
            if (op.Success) op.Message = ResourceManager.GetString("Files.File.FileUploadSuccess");
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DriveAuthCallback(AuthorizationCodeResponseUrl authorizationCode,
        CancellationToken taskCancellationToken)
        {
            if (string.IsNullOrEmpty(authorizationCode.Code)) return Redirect(SessionHelper.CurrentSession.GoogleDriveRedirectUri);
            //PlEASE REVERT THESE CHANGES AFTER TESTED ON LIVE 
            var returnUrl = Request.Url.ToString().Replace("http", "https");
            returnUrl = returnUrl.Substring(0, returnUrl.IndexOf("?"));
            var flowData = new AppFlowMetadata();
            var token = flowData.Flow.ExchangeCodeForTokenAsync(flowData.GetUserId(this), authorizationCode.Code, returnUrl,
            taskCancellationToken).GetAwaiter().GetResult();
            var oauthState = AuthWebUtility.ExtracRedirectFromState(flowData.Flow.DataStore, flowData.GetUserId(this),
            authorizationCode.State).GetAwaiter().GetResult();
            SetGoogleAuthCookies(token.AccessToken, ResourceFileTypes.GoogleDrive);
            return PartialView("_EmptyPartial");
            //return Redirect(SessionHelper.CurrentSession.GoogleDriveRedirectUri);
        }

        public void SetGoogleAuthCookies(string accessToken, ResourceFileTypes resourceFileType)
        {
            Helpers.CommonHelper.AppendCookie("googledriveaccess", "gdriveaccess", accessToken);
            Helpers.CommonHelper.AppendCookie("cloudfileloadertype", "cloudloadertype", ((short)resourceFileType).ToString());
        }

        public async Task<ActionResult> OnOneDriveAuthorization(string code)
        {
            MicrosoftResponseTokenView responseTokenView = await CloudFilesHelper.GetOneDriveAccessToken(code);
            if (string.IsNullOrEmpty(responseTokenView.AccessToken)) throw new Exception("Failed to get access token");
            Helpers.CommonHelper.AppendCookie("cloudfileloadertype", "cloudloadertype", ((short)ResourceFileTypes.OneDrive).ToString());
            return PartialView("_EmptyPartial");
            //return Redirect(SessionHelper.CurrentSession.GoogleDriveRedirectUri);
        }
        #endregion

        #region Course File Save
        [HttpPost]
        public async Task<ActionResult> SaveCourseCloudFiles(string fileIds, string moduleId, short resourceFileTypeId)
        {
            var selectedFiles = JsonConvert.DeserializeObject<List<CloudFileListView>>(fileIds);

            IList<Phoenix.Models.Entities.Attachment> files = new List<Phoenix.Models.Entities.Attachment>();
            foreach (var item in selectedFiles)
            {
                var file = new Phoenix.Models.Entities.Attachment();
                if (string.IsNullOrEmpty(item.FileExtension))
                    item.FileExtension = Helpers.CommonHelper.GetFileExtensionByMimeType(item.MimeType);
                else if (item.FileExtension.Equals("onenote", StringComparison.OrdinalIgnoreCase))
                    item.FileExtension = "one";
                file.GoogleDriveFileId = item.Id;
                file.FileName = item.Name;
                file.AttachmentPath = item.WebViewLink;
                var fileType = _fileTypeList.FirstOrDefault(r => r.Extension.Contains(item.FileExtension.ToLower()));
                if (fileType != null)
                    file.FileTypeId = fileType.TypeId;
                file.FileExtension = item.FileExtension;
                file.ResourceFileTypeId = item.ResourceFileTypeId != 0 ? item.ResourceFileTypeId : resourceFileTypeId;
                file.AttachmentKey = moduleId;
                file.AttachmentType = AttachmentType.Link;
                files.Add(file);
            }

            foreach (var file in files)
            {
                if (file.ResourceFileTypeId == (short)ResourceFileTypes.OneDrive)
                {
                    var response = await CloudFilesHelper.CreateOneDriveSharingLink(file.GoogleDriveFileId);
                    var data = (JObject)JsonConvert.DeserializeObject(response);

                    file.AttachmentPath = (string)data["link"]["webUrl"];
                }
                else if (file.ResourceFileTypeId == (short)ResourceFileTypes.GoogleDrive)
                {
                    CancellationToken cancellationToken = new CancellationToken();
                    var authResult = new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).
                       AuthorizeAsync(cancellationToken).GetAwaiter().GetResult();
                    CloudFilesHelper.AddGoogleDriveUserPermission(authResult, file.GoogleDriveFileId, "reader");
                }
            }
            var jsondata = JsonConvert.SerializeObject(files);
            return Json(new OperationDetails(true) { RelatedHtml = jsondata }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}