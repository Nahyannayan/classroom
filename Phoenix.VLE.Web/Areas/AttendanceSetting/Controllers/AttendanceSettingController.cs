﻿using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.Course.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.ViewModels;
using Phoenix.Models.Entities;
namespace Phoenix.VLE.Web.Areas.AttendanceSetting.Controllers
{
    public class AttendanceSettingController : BaseController
    {
        #region DI
        private IAttendanceService _AttendanceService;
        private IAttendanceSettingService _attendanceSettingService;
        private const string Add = "ADD";
        private const string Edit = "EDIT";
        private const string Delete = "DELETE";

        public AttendanceSettingController(IAttendanceService AttendanceService, IAttendanceSettingService attendanceSettingService)
        {
            _AttendanceService = AttendanceService;
            _attendanceSettingService = attendanceSettingService;
        }
        #endregion

        // GET: AttendanceSetting/AttendanceSetting
        public ActionResult Index()
        {
            return View();
        }

        #region Attendance Calendar       
        public ActionResult AttendanceCalendar()
        {
            ViewBag.AcademicYear = new SelectList(SelectListHelper.GetSelectListData(ListItems.AcademicYear, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            return View();
        }
        public ActionResult EditCalendarEvent(long SCH_ID = 0)
        {
            AttendanceCalendar attendanceCalendar = new AttendanceCalendar();
            var SchoolId = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            if (SCH_ID > 0)
            {
                var eventList = _attendanceSettingService.GetCalendarDetail(SchoolId, SCH_ID);
                if (eventList.Any())
                {
                    attendanceCalendar = eventList.FirstOrDefault();
                }
            }
            var academicYearDetail = _attendanceSettingService.GetAcademicYearDetail(SchoolId);
            attendanceCalendar.SchoolWeekEnd = new SchoolWeekEnd();// _attendanceSettingService.GetSchoolWeekEnd(BSU_ID);
            attendanceCalendar.AcademicYearDetail = academicYearDetail;
            return PartialView("_EditCalendarEvent", attendanceCalendar);
        }
        public ActionResult LoadCalendarListView()
        {
            var SchoolId = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            var eventList = _attendanceSettingService.GetCalendarDetail(SchoolId, 0, true);
            return PartialView("_LoadCalendarListView", eventList);
        }
        public ActionResult GetGradeAndSectionList()
        {
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            List<ComboTreeView> objJsonResult = new List<ComboTreeView>();
            var childList = _attendanceSettingService.GetGradeAndSectionList(SchoolId);
            var parentList = childList
                            .GroupBy(c => c.SchoolGradeId)
                            .Select(g => new { Qty = g.Count(), First = g.OrderBy(c => c.SchoolGradeId).First() })
                            .Select(p => new
                            {
                                SchoolGradeId = p.First.SchoolGradeId,
                                GradeDisplay = p.First.GradeDisplay,
                            });

            foreach (var parent in parentList)
            {
                List<ComboTreeView> objChildJsonResult = new List<ComboTreeView>();
                var Id = parentList.Where(e => e.SchoolGradeId == parent.SchoolGradeId).Select(e => e.SchoolGradeId).FirstOrDefault();
                if (Id > 0)
                {
                    var title = parentList.Where(e => e.SchoolGradeId == parent.SchoolGradeId).Select(e => e.GradeDisplay).FirstOrDefault();
                    var lstChild = childList.Where(e => e.SchoolGradeId == Id).ToList();
                    if (lstChild.Any())
                    {
                        foreach (var child in lstChild)
                        {
                            objChildJsonResult.Add(new ComboTreeView
                            {
                                id = Convert.ToString(child.SectionId) + "_" + Id,
                                text = child.SectionName
                            });
                        }
                    }
                    objJsonResult.Add(new ComboTreeView
                    {
                        id = Id,
                        text = title,
                        state = "closed",
                        children = objChildJsonResult
                    });
                }
            }
            return Json(objJsonResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveCalendarEvent(AttendanceCalendar attendanceCalendar)
        {
            attendanceCalendar.SchoolId = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            var DATAMODE = attendanceCalendar.SCH_ID > 0 ? Edit : Add;
            var result = _attendanceSettingService.SaveCalendarEvent(attendanceCalendar, DATAMODE);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteCalendarEvent(long SCH_ID)
        {
            AttendanceCalendar attendanceCalendar = new AttendanceCalendar();
            attendanceCalendar.SCH_ID = SCH_ID;
            var result = _attendanceSettingService.SaveCalendarEvent(attendanceCalendar, Delete);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        //public ActionResult GetCalendarEvent(long ACD_ID = 0)
        //{
        //    var IsCurrentAcademic = Common.Helpers.SessionHelper.CurrentSession.ACD_ID == ACD_ID ? true : false;
        //    var BSU_ID = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
        //    var eventList = _attendanceSettingService.GetCalendarDetail(BSU_ID, ACD_ID);
        //    var SchoolWeekEnd = _attendanceSettingService.GetSchoolWeekEnd(BSU_ID);
        //    var AcademicYearDetailList = _attendanceSettingService.GetAcademicYearDetail(ACD_ID);
        //    AcademicYearDetail academicYearDetail = new AcademicYearDetail();
        //    if (AcademicYearDetailList.Any())
        //    {
        //        academicYearDetail = AcademicYearDetailList.FirstOrDefault();
        //    }

        //    string CurrentDefaultDate = academicYearDetail.ACD_STARTDT.ToString("yyyy-MM-dd");
        //    if (IsCurrentAcademic)
        //    {
        //        if (academicYearDetail.ACD_STARTDT.Year == DateTime.Now.Year)
        //        {
        //            CurrentDefaultDate = new DateTime(academicYearDetail.ACD_STARTDT.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
        //        }
        //        else
        //        {
        //            CurrentDefaultDate = new DateTime(academicYearDetail.ACD_ENDDT.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
        //        }
        //    }
        //    return Json(new { eventList, SchoolWeekEnd, academicYearDetail, CurrentDefaultDate }, JsonRequestBehavior.AllowGet);
        //}

        #endregion Attendance Calendar   

        #region Parameter Setting
        public ActionResult ParameterSetting()
        {
            var SchoolId = SessionHelper.CurrentSession.SchoolId;
            var CLM_ID = SessionHelper.CurrentSession.CLM_ID;
            long ACD_ID = Phoenix.Common.Helpers.SessionHelper.CurrentSession.ACD_ID;

            return PartialView("_ParameterSetting");
        }
        public ActionResult LoadParameterGrid(long Att_TypeID = 0)
        {
            var SchoolId = SessionHelper.CurrentSession.SchoolId;
            var list = _attendanceSettingService.GetParameterSettingList(SchoolId).Where(x => x.ParameterTypeId == Att_TypeID).ToList();
            return PartialView("_LoadParameterGrid", list);

        }
        public ActionResult AddEditParameterSetting(long ParamaterId)
        {
            ParameterSetting parameterSetting = new ParameterSetting();
            var SchoolId = SessionHelper.CurrentSession.SchoolId;
            var CLM_ID = SessionHelper.CurrentSession.CLM_ID;
            List<ParameterMapping> ParameterT = Enum.GetValues(typeof(ParameterMapping)).Cast<ParameterMapping>().ToList();
            ViewBag.ParameterType = new SelectList(ParameterT);
            var list = _attendanceSettingService.GetParameterSettingList(SchoolId).Where(x => x.ParamaterId == ParamaterId).ToList();
            if (list.Any())
            {
                parameterSetting = list.FirstOrDefault();
            }
            return PartialView("_AddEditParameterSetting", parameterSetting);
        }
        public ActionResult SaveParameterSetting(ParameterSetting parameterSetting)
        {
            string DATAMODE = parameterSetting.ParamaterId > 0 ? "Edit" : "Add";
            parameterSetting.SchoolId = SessionHelper.CurrentSession.SchoolId;
            var result = _attendanceSettingService.SaveParameterSetting(parameterSetting, DATAMODE);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteParameterSetting(long ParamaterId)
        {
            ParameterSetting parameterSetting = new ParameterSetting();
            parameterSetting.SchoolId = SessionHelper.CurrentSession.SchoolId;
            parameterSetting.ParamaterId = ParamaterId;
            parameterSetting.ParameterDesc = string.Empty;
            parameterSetting.ParameterDisplayCode = string.Empty;
            parameterSetting.ParameterDisplayOrder = string.Empty;
            parameterSetting.ReportParameterDesc = string.Empty;
            var result = _attendanceSettingService.SaveParameterSetting(parameterSetting, "Delete");
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Leave approval permission
        public ActionResult LeaveApprovalPermission()
        {
            //var academicYears = _SIMSCommonService.GetAcademicYearList(SessionHelper.CurrentSession.SchoolId.ToString(), SessionHelper.CurrentSession.CLM_ID, null);
            //ViewBag.AcademicYear = new SelectList(academicYears, "ItemId", "ItemName", academicYears != null && academicYears.Any() ? academicYears.FirstOrDefault(x => x.Selected == true).ItemId : "");
            //ViewBag.StaffList = new SelectList(_attendanceSettingService.GetStaffDetails(SessionHelper.CurrentSession.SchoolId.ToString()), "EmployeeId", "EmployeeName");
            //var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            //long grd_access = (int)curr_role.GSA_ID;
            //int clm_id = SessionHelper.CurrentSession.CLM_ID;
            //string IsSuperUser = (curr_role.IsSuperUser ? "Y" : "N");

            //ViewBag.Grades = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.Grade, $"{SessionHelper.CurrentSession.ACD_ID},{SchoolDivisionHelper.GetCurrentDivision}"), "ItemId", "ItemName");
            return PartialView("LeaveApprovalPermission");

        }
        //public JsonResult SaveLeaveApprovalPermission(LeaveApprovalPermissionModel leave)
        //{
        //    leave.SchoolId = schoolid;
        //    leave.DivisionId = SchoolDivisionHelper.GetCurrentDivision;
        //    var result = _attendanceSettingService.LeaveApprovalPermissionCU(leave);
        //    return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        //}

        public PartialViewResult LoadLeaveApprovalGrid(long ACD_ID)
        {
            // var result = _attendanceSettingService.GetLeaveApprovalPermission(ACD_ID, SessionHelper.CurrentSession.SchoolId, SchoolDivisionHelper.GetCurrentDivision).OrderBy(x => x.GradeDisplayOrder);
            return PartialView("_LoadLeaveApprovalGrid", "");
        }
        #endregion

        #region Att type
        public ActionResult AttendanceType()
        {
            List<AttendanceType> attendanceTypeList = new List<AttendanceType>();
            var schoolid = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            attendanceTypeList = _attendanceSettingService.GetAttendanceType(schoolid).ToList();
            return PartialView("_AttendanceType", attendanceTypeList);
        }
        public ActionResult AddEditAttendanceType(long AttendanceConfigurationTypeID = 0)
        {
            AttendanceType attendanceType = new AttendanceType();
            var schoolid = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            ViewBag.AttendanceTypeSession = Enum.GetValues(typeof(AttendanceTypeSession)).Cast<AttendanceTypeSession>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            ViewBag.AttendanceSelectType = Enum.GetValues(typeof(AttendanceSelectType)).Cast<AttendanceSelectType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            var Grades = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolGrade, schoolid), "ItemId", "ItemName");
            ViewBag.GradeList = Grades;
            if (AttendanceConfigurationTypeID > 0)
            {
                var list = _attendanceSettingService.GetAttendanceType(schoolid).Where(m => m.AttendanceConfigurationTypeID == AttendanceConfigurationTypeID).ToList();
                if (list.Any())
                {
                    attendanceType = list.FirstOrDefault();
                    attendanceType.AttendacePeriodSettingList = _attendanceSettingService.GetAttendancePeriodList(attendanceType.GradeId).ToList();
                }
            }
            return PartialView("_AddEditAttendanceType", attendanceType);

        }
        public ActionResult GetAttendanceType()
        {
            List<AttendanceType> AttendanceType = new List<AttendanceType>();
            //long ACD_ID = Phoenix.Common.Helpers.SessionHelper.CurrentSession.ACD_ID;
            var schoolid = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            //var clm_id = Phoenix.Common.Helpers.SessionHelper.CurrentSession.CLM_ID;
            //ViewBag.AcademicYear = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AcademicYear, $"{schoolid},{clm_id},{null}"), "ItemId", "ItemName", ACD_ID);
            //ViewBag.GradeList = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.Grade, $"{ACD_ID},{SchoolDivisionHelper.GetCurrentDivision}"), "ItemId", "ItemName");
            ////ViewBag.AttendanceType = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AttendanceType, SessionHelper.CurrentSession.ACD_ID), "ItemId", "ItemName");
            AttendanceType = _attendanceSettingService.GetAttendanceType(schoolid).ToList();
            return PartialView("_GetAttendanceType", AttendanceType);
        }
        public ActionResult SaveAttendanceType(AttendanceType attendanceType)
        {
            var schoolid = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            attendanceType.SchoolId = schoolid;
            var result = _attendanceSettingService.SaveAttendanceType(attendanceType, attendanceType.AttendanceConfigurationTypeID > 0 ? "EDIT" : "ADD");
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult AttendancePeriodGrid(long gradeId = 0)
        {

            List<AttendacePeriodModel> objAttendancePeriod = new List<AttendacePeriodModel>();
            objAttendancePeriod = _attendanceSettingService.GetAttendancePeriodList(gradeId).ToList();
            return PartialView("_AttendancePeriodGrid", objAttendancePeriod);
        }
        public ActionResult GetAttendanceTypeListBYId(long AttendanceConfigurationTypeID = 0)
        {
            List<AttendanceType> objAttendancePeriod = new List<AttendanceType>();
            objAttendancePeriod = _attendanceSettingService.GetAttendanceTypeListBYId(AttendanceConfigurationTypeID).ToList();
            return PartialView("_AttendanceType", objAttendancePeriod);
        }
        #endregion
    }


}

