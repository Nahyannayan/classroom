﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.AttendanceSetting
{
    public class AttendanceSettingAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AttendanceSetting";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AttendanceSetting_default",
                "AttendanceSetting/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}