﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.TerminologyEditor
{
    public class TerminologyEditorAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "TerminologyEditor";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "TerminologyEditor_default",
                "TerminologyEditor/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}