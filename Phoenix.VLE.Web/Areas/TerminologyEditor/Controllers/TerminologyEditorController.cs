﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Phoenix.Common.Localization;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.Helpers.Extensions;

namespace Phoenix.VLE.Web.Areas.TerminologyEditor.Controllers
{
    [Authorize]
    public class TerminologyEditorController : BaseController
    {
        private ITerminologyEditorService _terminologyEditorService;
        public TerminologyEditorController()
        {
            _terminologyEditorService = new TerminologyEditorService();
        }
        //public TerminologyEditorController(ITerminologyEditorService terminologyEditorSerive)
        //{
        //    _terminologyEditorService = terminologyEditorSerive;
        //}

        // GET: TerminologyEditor/TerminologyEditor
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult LoadTerminologyEditorGrid()
        {
            var terminologyEditors = _terminologyEditorService.GetAllTerminologyEditor((long)SessionHelper.CurrentSession.SchoolId);
            //var actionButtonsHtmlTemplate = "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='terminologyEditors.editTerminologyEditorPopup($(this),{0})'><i class='fas fa-pencil-alt'></i></button><button type='button' class='table-action-icon-btn' onclick='terminologyEditors.deleteTerminologyEditorData($(this),{0})'><i class='far fa-trash-alt'></i></button></div>";

            var dataList = new object();
            dataList = new
            {
                aaData = (from item in terminologyEditors
                          select new
                          {
                              //Actions = string.Format(actionButtonsHtmlTemplate, item.Id),
                              Actions = HtmlHelperExtensions.LocalizedEditDeleteLinkButtons(item.Id , "TerminologyEditor", "terminologyEditors"),
                              item.OldTerm,
                              item.NewTerm,
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InitAddEditTerminologyEditorForm(int? id)
        {
            var model = new TerminologyEditorEdit();
            if (id.HasValue)
            {
                model = _terminologyEditorService.GetTerminologyEditor(id.Value, SessionHelper.CurrentSession.SchoolId);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = SessionHelper.CurrentSession.SchoolId;
            }
            return PartialView("_AddEditTerminologyEditor", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveTerminologyEditorData(TerminologyEditorEdit model)
        {
            bool check = false;

            check = _terminologyEditorService.CheckForTerminology(model.OldTerm,model.Id);

            if (check)
            {
                var operationalModel = new OperationDetails();
                operationalModel.Success = false;
                operationalModel.NotificationType = "error";
                operationalModel.Message = ResourceManager.GetString("TerminologyEditor.TerminologyEditor.IsExist");
                return Json(operationalModel, JsonRequestBehavior.AllowGet);
            }
            model.UpdatedBy = SessionHelper.CurrentSession.Id;
            var result = _terminologyEditorService.TerminologyEditorCUD(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteTerminologyEditorData(int id)
        {
            TerminologyEditorEdit model = new TerminologyEditorEdit();
            model.Id = id;
            var result = _terminologyEditorService.TerminologyEditorDelete(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
    }
}