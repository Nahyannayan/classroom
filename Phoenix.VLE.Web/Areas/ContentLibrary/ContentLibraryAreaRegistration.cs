﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ContentLibrary
{
    public class ContentLibraryAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ContentLibrary";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ContentLibrary_default",
                "ContentLibrary/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}