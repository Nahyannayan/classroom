﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ContentLibrary.Controllers
{
    public class ResourcesController : BaseController
    {
        private readonly IContentLibraryService _contentLibraryService;
        private readonly IUserPermissionService _userPermissionService;
        private IStudentListService _studentListService;
        ContentLibraryService contentLibraryService = new ContentLibraryService();
        public ResourcesController(IContentLibraryService contentLibraryService, IUserPermissionService userPermissionService, IStudentListService StudentListService)
        {
            _contentLibraryService = contentLibraryService;
            _studentListService = StudentListService;
            _userPermissionService = userPermissionService;
        }

        // GET: ContentLibrary/Resources
        public ActionResult Index()
        {
            IEnumerable<ContentView> content = new List<ContentView>();
            content = _contentLibraryService.GetContentProvider((int)SessionHelper.CurrentSession.SchoolId, "");
            var schoolLevels = _contentLibraryService.GetSchoolLevelsBySchoolId().ToList();
            ViewBag.schoolLevels = schoolLevels.Select(i => new Phoenix.Common.Models.ListItem
            {
                ItemId = i.DivisionId.ToString(),
                ItemName = i.DivisionName
            });
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ContentLibrary.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            if (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent())
            {
                return View("StudentContent", content);
            }
            return View(content);
        }
        [HttpGet]
        public ActionResult GetContentLibrary()
        {
            var model = _contentLibraryService.GetContentLibrary((int)SessionHelper.CurrentSession.SchoolId); //we can get ContentProvider and ContentLibrary
            ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName", SessionHelper.CurrentSession.SchoolId);
            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;
            return PartialView("_ListContentLibrary", model);
        }
        public ActionResult GetContentProvider(string sortBy, string divisionIds)
        {
            IEnumerable<ContentView> content = new List<ContentView>();
            content = _contentLibraryService.GetContentProvider((int)SessionHelper.CurrentSession.SchoolId, divisionIds, sortBy);
            return PartialView("_ContentProvider", content);
        }
        public ActionResult StudentContent()
        {
            IEnumerable<ContentView> content = new List<ContentView>();
            content = _contentLibraryService.GetContentProvider((int)SessionHelper.CurrentSession.SchoolId, "");
            return View(content);
        }
        public ActionResult LoadContentLibraryGrid()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var content = _contentLibraryService.GetContentLibrary((int)SessionHelper.CurrentSession.SchoolId);
            //var actionButtonsHtmlTemplate = string.Empty;
            //actionButtonsHtmlTemplate = CurrentPagePermission.CanEdit?"<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='contentLibrary.editContentLibraryPopup($(this),{0})'><i class='fas fa-pencil-alt'></i></button><button type='button' class='table-action-icon-btn' onclick='contentLibrary.deleteContentResourceData($(this),{0})'><i class='far fa-trash-alt'></i></button><button type='button' class='table-action-icon-btn' onclick='contentLibrary.downloadAttachment($(this),\"{1}\")'><i class='fa fa-download'></i></button></div>": "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='contentLibrary.downloadAttachment($(this),\"{1}\")'><i class='fa fa-download'></i></button></div>";
            //var studentActionButton = "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='contentLibrary.downloadAttachment($(this),\"{0}\")'><i class='fa fa-download'></i></button></div>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in content
                          select new
                          {
                              Actions = SessionHelper.CurrentSession.IsTeacher() && !item.IsAssign && !item.IsApproved ? String.Format(actionButtonsHtmlTemplate(item.ContentImage), item.ContentId, GetFilePath(item.ContentImage)) : String.Format(studentActionButton(item.ContentImage), GetFilePath(item.ContentImage)),
                              //ContentName = String.IsNullOrEmpty(item.RedirectUrl) ? item.ContentName : HtmlHelperExtensions.LocalizedLinkButtons($"<a  href='{item.RedirectUrl}' target='_blank' class='table-action-text-link' title='{item.ContentName}' '>{item.ContentName}</a>"),
                              item.ContentName,
                              item.Description,
                              SubjectName = GetSubjectNameById(item.ContentId.ToInteger()),
                              Status = String.Format("{0} - {1}", item.IsApproved ? ResourceManager.GetString("ContentLibrary.Resources.Approved") : ResourceManager.GetString("ContentLibrary.Resources.InProcess"), item.IsAssign ? ResourceManager.GetString("ContentLibrary.Resources.Assigned") : ResourceManager.GetString("ContentLibrary.Resources.NotAssigned")),
                              CreatedBy = item.CreatedByName
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PaginateContentLibraryGrid(int[] categoryIds, bool selectCategory = false, int pageIndex = 1, string searchString = "", string sortBy = "")
        {
            var userId = SessionHelper.CurrentSession.IsParent() ? SessionHelper.CurrentSession.CurrentSelectedStudent.UserId : SessionHelper.CurrentSession.Id;
            var model = new Pagination<ContentView>();
            if (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent())
            {
                var studentCourses = _studentListService.GetStudentCourses(userId);
                var studentCourse = studentCourses.Select(x => new SelectListItem
                {
                    Value = x.CourseId.ToString(),
                    Text = x.Title,
                }).ToList();
                ViewBag.CourseList = studentCourse;
            }
            else
            {
                List<ListItem> lstCourse = new List<ListItem>();
                lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
                var courses = lstCourse.Select(x => new SelectListItem
                {
                    Value = x.ItemId.ToString(),
                    Text = x.ItemName,
                }).ToList();
                ViewBag.CourseList = courses;
            }
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_ContentLibrary.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var oldSearchString = searchString;
            Session["SelectedCategories"] = !selectCategory ? Session["SelectedCategories"] : categoryIds != null ? categoryIds.ToList() : null;
            string categories = Session["SelectedCategories"] != null ? string.Join(",", (List<int>)Session["SelectedCategories"]) : String.Empty;
            //if (selectCategory)
            //{
            //    categories = String.Empty;
            //    Session["SelectedCategories"] = null;
            //}
            var ContentLibrary = SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent() ?
                _contentLibraryService.GetStudentContentLibrary(pageIndex, 9, searchString, sortBy, categories).ToList() :
                _contentLibraryService.GetPaginateContentLibrary(pageIndex, 9, searchString, sortBy, categories).ToList();
            if (ContentLibrary.Any())
                model = new Pagination<ContentView>(pageIndex, 9, ContentLibrary, ContentLibrary.FirstOrDefault().TotalCount);
            model.RecordCount = ContentLibrary.Count == 0 ? 0 : ContentLibrary[0].TotalCount;
            model.LoadPageRecordsUrl = "/ContentLibrary/Resources/PaginateContentLibraryGrid?pageIndex={0}";
            model.SearchString = oldSearchString;
            model.SortBy = sortBy;
            return PartialView("_ContentLibrary", model);
        }
        private string actionButtonsHtmlTemplate(string imageName)
        {
            return CurrentPagePermission.CanEdit ? String.IsNullOrEmpty(imageName) ? "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='contentLibrary.editContentLibraryPopup($(this),{0})'><i class='fas fa-pencil-alt'></i></button><button type='button' class='table-action-icon-btn' onclick='contentLibrary.deleteContentResourceData($(this),{0})'><i class='far fa-trash-alt'></i></button></div>" : "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='contentLibrary.editContentLibraryPopup($(this),{0})'><i class='fas fa-pencil-alt'></i></button><button type='button' class='table-action-icon-btn' onclick='contentLibrary.deleteContentResourceData($(this),{0})'><i class='far fa-trash-alt'></i></button><button type='button' class='table-action-icon-btn' onclick='contentLibrary.downloadAttachment($(this),\"{1}\")'><i class='fa fa-download'></i></button></div>" : String.IsNullOrEmpty(imageName) ? String.Empty : "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='contentLibrary.downloadAttachment($(this),\"{1}\")'><i class='fa fa-download'></i></button></div>";
        }
        private string studentActionButton(string imageName)
        {
            return String.IsNullOrEmpty(imageName) ? String.Empty : "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='contentLibrary.downloadAttachment($(this),\"{0}\")'><i class='fa fa-download'></i></button></div>";
        }
        public ActionResult LoadStudentContentLibraryGrid()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var content = _contentLibraryService.GetContentLibrary((int)SessionHelper.CurrentSession.SchoolId);
            content = content.Where(x => x.IsAssign == true);
            //var actionButtonsHtmlTemplate = string.Empty;
            //actionButtonsHtmlTemplate = "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='contentLibrary.downloadAttachment($(this),\"{0}\")'><i class='fa fa-download'></i></button></div>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in content
                          select new
                          {
                              Actions = String.Format(studentActionButton(item.ContentImage), GetFilePath(item.ContentImage)),
                              //ContentName = String.IsNullOrEmpty(item.RedirectUrl)? item.ContentName : HtmlHelperExtensions.LocalizedLinkButtons($"<a  href='{item.RedirectUrl}' target='_blank' class='table-action-text-link' title='{item.ContentName}' '>{item.ContentName}</a>"),
                              item.ContentName,
                              item.Description,
                              SubjectName = GetSubjectNameById(item.ContentId.ToInteger())
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        private string GetSubjectNameById(int ContentId)
        {
            string subject = String.Empty;
            var subjects = _contentLibraryService.getSubjectsByContentId(ContentId);
            foreach (var n in subjects)
            {
                subject = subject + n.SelectedSubjectName + " | ";
            }
            return String.IsNullOrEmpty(subject) ? "" : subject.Remove(subject.Length - 2);
        }
        public ActionResult InitAddContentResourceForm(int? id)
        {
            var model = new ContentEdit();
            if (id.HasValue)
            {
                var resource = _contentLibraryService.getContentResourceById(id.Value);
                var subjects = _contentLibraryService.getSubjectsByContentId(id.Value);
                resource.SubjectId = subjects.Select(m => m.SelectedSubjectId.ToString()).ToList();
                EntityMapper<Content, ContentEdit>.Map(resource, model);
            }
            else
            {
                model.IsAddMode = true;
            }
            //ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName", SessionHelper.CurrentSession.SchoolId);
            List<ListItem> lstCourse = new List<ListItem>();
            lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
            var courses = lstCourse.Select(x => new SelectListItem
            {
                Value = x.ItemId.ToString(),
                Text = x.ItemName,
            }).ToList();
            ViewBag.CourseList = courses;
            return PartialView("_AddEditContentResource", model);
        }

        [HttpPost]
        public async Task<ActionResult> InsertUpdateContentResource(ContentEdit model, HttpPostedFileBase resourceFile)
        {
            if (resourceFile != null)
            {
                SharePointFileView shv = new SharePointFileView();
                model.FileExtenstion = Path.GetExtension(resourceFile.FileName);
                model.IsSharepointFile = true;
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    shv = await azureHelper.UploadStudentFilesAsPerModuleAsync(FileModulesConstants.ContentLibrary, SessionHelper.CurrentSession.OldUserId.ToString(), resourceFile);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    shv = SharePointHelper.UploadStudentFilesAsPerModule(ref cx, FileModulesConstants.ContentLibrary, SessionHelper.CurrentSession.OldUserId.ToString(), resourceFile);
                }

                //model.ContentImage = UploadContentResource(resourceFile);
                model.ContentImage = shv.ShareableLink;
                model.PhysicalPath = shv.SharepointUploadedFileURL;
            }
            foreach (var i in model.SubjectIds)
            {
                var m = i.Split(',');
            }
            var result = _contentLibraryService.InsertUpdateContentResource(model);
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }

        private async Task<string> UploadContentResource(HttpPostedFileBase resourceFile)
        {
            string GUID = Guid.NewGuid().ToString();
            SharePointFileView shv = new SharePointFileView();
            if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
            {
                AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                shv = await azureHelper.UploadFilesAsPerModuleAsync(FileModulesConstants.ContentLibrary, SessionHelper.CurrentSession.OldUserId.ToString(), resourceFile);
            }
            else
            {
                SharePointHelper sh = new SharePointHelper();
                Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);

                shv = SharePointHelper.UploadFilesAsPerModule(ref cx, FileModulesConstants.ContentLibrary, SessionHelper.CurrentSession.OldUserId.ToString(), resourceFile);
            }
            
            return shv.ShareableLink;
        }

        [HttpPost]
        public ActionResult DeleteContentResourceData(int id)
        {
            var result = _contentLibraryService.DeleteContentResourceData(id);
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }

        public string GetFilePath(string fileName)
        {
            string resourceDir = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir;
            string content = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content";
            return PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content" + "/ContentResourceFiles/" + fileName;

        }
        public void DownloadAttachment(string fileName)
        {
            string resourceDir = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir;
            string content = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content";
            string path = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content" + "/ContentResourceFiles/" + fileName;
            var webClient = new WebClient();
            byte[] FileBytes = webClient.DownloadData(path);
            Response.ContentType = GetMimeType(path);
            Response.BufferOutput = true;
            Response.AppendHeader("Content-Disposition", "Attachment; Filename=" + System.IO.Path.GetFileName(path) + "");
            Response.AddHeader("Last-Modified", DateTime.Now.ToLongDateString());
            Response.BinaryWrite(FileBytes);
            Response.Flush();
            Response.End();
        }

        private string GetMimeType(string filePath)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(filePath).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }

    }
}