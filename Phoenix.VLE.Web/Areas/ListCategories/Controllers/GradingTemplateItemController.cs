﻿using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common;
using Phoenix.Common.Localization;

namespace Phoenix.VLE.Web.Areas.ListCategories.Controllers
{
    [Authorize]
    public class GradingTemplateItemController : BaseController
    {
        private IGradingTemplateItemService _gradingTemplateItemService;
        private IGradingTemplateService _gradingTemplateService;

        public GradingTemplateItemController(IGradingTemplateService gradingTemplateService, IGradingTemplateItemService gradingTemplateItemService)
        {
            _gradingTemplateService = gradingTemplateService;
            _gradingTemplateItemService = gradingTemplateItemService;
        }
        // GET: GradingTemplate/GradingTemplate
        public ActionResult Index()
        {
            var model = new GradingTemplate();
            if(Session["GradingTemplateId"] != null)
            {
                model = _gradingTemplateService.GetGradingTemplateById(Session["GradingTemplateId"].ToInteger());
                return View(model);
            }
            else
                return Redirect("~/ListCategories/GradingTemplate");            
        }

        public ActionResult LoadGradingTemplateItemGrid(int templateId)
        {
            var gradingTemplates = _gradingTemplateItemService.GetGradingTemplateItems(templateId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
            var dataList = new object();            
            dataList = new
            {// adding score from score to --syed | 13 aug 2020
                aaData = (from item in gradingTemplates.OrderBy(x=>x.SortOrder)
                          select new
                          {
                             
                              Actions = HtmlHelperExtensions.LocalizedEditDeleteLinkButtons(new HyperlinkAttributes() {PagePermission=CurrentPagePermission
                              }, item.GradingTemplateItemId, "GradingTemplateItem", "gradingTemplateItems"),
                              Percentage = item.Percentage + "%",
                              GradingColor = $"<span class='d-inline-block p-3' style='background: {item.GradingColor}'></span>",
                              item.GradingTemplateItemSymbol,
                              item.GradingItemDescription,
                              item.ShortLabel,
                              item.ScoreFrom,
                              item.ScoreTo,
                              item.CreatedOn,
                              item.CreatedBy,
                              item.IsActive
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitAddEditGradingTemplateItemForm(int? id, int templateId)
        {
            var model = new GradingTemplateItemEdit();
            if (id.HasValue)
            {
                var template = _gradingTemplateItemService.GetGradingTemplateItemById(id.Value, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
                EntityMapper<Phoenix.Models.GradingTemplateItem, GradingTemplateItemEdit>.Map(template, model);
            }
            else
            {
                model.IsAddMode = true;
                model.GradingTemplateId = templateId;
            }

            return PartialView("_AddEditGradingTemplateItem", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveGradingTemplateItemData(GradingTemplateItemEdit model)
        {
            if (!CurrentPagePermission.CanEdit)
            {

                return Json(new OperationDetails(false, LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            }
            model.ParseJsonToXml("GradingTemplateItemXml");
            if (model.ShortLabel.Trim() == "")
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.NoDataToSave), JsonRequestBehavior.AllowGet);
            }
            var result = _gradingTemplateItemService.UpdateGradingTemplateItemData(model);
            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "Grading Template Item")), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteGradingTemplateItemData(int id)
        {
            var result = _gradingTemplateItemService.DeleteGradingTemplateItemData(id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

    }
}