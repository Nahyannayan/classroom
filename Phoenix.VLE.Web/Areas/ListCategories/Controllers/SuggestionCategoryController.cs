﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.Localization;

namespace Phoenix.VLE.Web.Areas.ListCategories.Controllers
{
    [Authorize]
    public class SuggestionCategoryController : BaseController
    {
        private ISuggestionCategoryService _suggestionCategoriesService;

        public SuggestionCategoryController(ISuggestionCategoryService suggestionCategoriesService)
        {
            _suggestionCategoriesService = suggestionCategoriesService;
        }
        // GET: ListCategories/SuggestionCategory
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult InitAddEditSuggestionCategoryForm(int? id)
        {
            var model = new SuggestionCategoryEdit();
            if (id.HasValue)
            {
                var category = _suggestionCategoriesService.GetSuggestionCategoryById(id.Value);
                EntityMapper<SuggestionCategory, SuggestionCategoryEdit>.Map(category, model);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = Convert.ToInt32(SessionHelper.CurrentSession.SchoolId)               ;
            }
            return PartialView("_AddEditSuggestionCategory", model);
        }
        public ActionResult LoadSuggestionCategoriesGrid()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var suggestionCategories = _suggestionCategoriesService.GetSuggestionCategory((int)(int)SessionHelper.CurrentSession.SchoolId);
            var actionButtonsHtmlTemplate = string.Empty;
            if (CurrentPagePermission.CanEdit)
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='suggestionCategories.editCategoryPopup($(this),{0})' title='" + Common.Localization.ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='suggestionCategories.deleteCategoryData($(this),{0})' title='" + Common.Localization.ResourceManager.GetString("Shared.Labels.DeleteRecord") + "'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a></div>";
            else
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='suggestionCategories.editCategoryPopup($(this),{0})' title='" + Common.Localization.ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-view.svg'></a></div>";

            var dataList = new object();
            dataList = new
            {
                aaData = (from item in suggestionCategories
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.SuggestionCategoryId),
                              item.SuggestionCategoryName,
                              item.FormattedCreatedOn,
                              item.CreatedByName,
                              item.IsActive
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSuggestionCategoryData(SuggestionCategoryEdit model)
        {
            if (!CurrentPagePermission.CanEdit)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            }
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            model.ParseJsonToXml("SuggestionCategoryXml");

            var result = _suggestionCategoriesService.UpdateSuggestionCategoryData(model);
            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "Think Box Catagory")), JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSuggestionCategoryData(int id)
        {
            var result = _suggestionCategoriesService.DeleteSuggestionCategoryData(id);
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }

    }
}