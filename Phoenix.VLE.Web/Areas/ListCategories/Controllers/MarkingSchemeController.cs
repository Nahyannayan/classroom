﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ListCategories.Controllers
{
    [Authorize]
    public class MarkingSchemeController : BaseController
    {
        private IMarkingSchemeService _markingSchemeService;

        public MarkingSchemeController(IMarkingSchemeService markingSchemeService)
        {
            _markingSchemeService = markingSchemeService;
        }

        // GET: ListCategories/MarkingScheme
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult LoadMarkingSchemeGrid()
        {
            var markingSchemes = _markingSchemeService.GetMarkingSchemes((int)SessionHelper.CurrentSession.SchoolId);
            var actionButtonsHtmlTemplate = string.Empty;
            if (CurrentPagePermission.CanEdit)
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='markingSchemes.editMarkingSchemePopup($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='markingSchemes.deleteMarkingSchemePopup($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.DeleteRecord") + "'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a></div>";
            else
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='markingSchemes.editMarkingSchemePopup($(this),{0},true)' title='" + ResourceManager.GetString("Shared.Labels.view") + "'><img src='/Content/VLE/img/svg/tbl-view.svg'></a></div>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in markingSchemes
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.MarkingSchemeId),
                              //Actions = HtmlHelperExtensions.LocalizedEditDeleteLinkButtons(new HyperlinkAttributes()
                              //{
                              //    PagePermission = CurrentPagePermission
                              //}, item.MarkingSchemeId, "MarkingScheme", "markingSchemes"),
                              item.MarkingName,
                              ColorCode = $"<span class='d-inline-block p-3 rounded' style='background: {item.ColourCode}'></span>",
                              Description = item.MarkingDesc
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitAddEditMarkingSchemeForm(int? id)
        {
            var model = new MarkingSchemeEdit();
            if (id.HasValue)
            {
                var markingScheme = _markingSchemeService.GetMarkingScheme(id.Value);
                EntityMapper<MarkingScheme, MarkingSchemeEdit>.Map(markingScheme, model);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            }
            ViewBag.IsEditPerMission = CurrentPagePermission.CanEdit;
            return PartialView("_AddEditMarkingScheme", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveMarkingSchemeData(MarkingSchemeEdit model)
        {
            if (model.MarkingName.Trim() == "")
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.NoDataToSave), JsonRequestBehavior.AllowGet);
            }
            model.ParseJsonToXml("MarkingSchemeXml");
            var result = _markingSchemeService.UpdateMarkingScheme(model);

            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "Marking Scheme with same name/color code")), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult DeleteMarkingSchemeData(int id)
        {
            var result = _markingSchemeService.DeleteMarkingScheme(id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
    }
}