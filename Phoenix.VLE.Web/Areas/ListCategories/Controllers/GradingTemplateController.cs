﻿using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.Localization;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common;
using System.IO;

namespace Phoenix.VLE.Web.Areas.ListCategories.Controllers
{
    [Authorize]
    public class GradingTemplateController : BaseController
    {
        private IGradingTemplateService _gradingTemplateService;

        public GradingTemplateController(IGradingTemplateService gradingTemplateService)
        {
            _gradingTemplateService = gradingTemplateService;
        }
        // GET: GradingTemplate/GradingTemplate
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadGradingTemplateGrid()
        {
            var gradingTemplates = _gradingTemplateService.GetGradtingTemplates((int)SessionHelper.CurrentSession.SchoolId);
            var dataList = new object();
            var actionButtonsHtmlTemplate = string.Empty;
            if (CurrentPagePermission.CanEdit)
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='gradingTemplates.openGradingTemplateItem($(this),{0})' title='" + ResourceManager.GetString("ListCategories.GradingTemplates.OpenTemplateItem") + "'><img src='/Content/VLE/img/svg/tbl-view.svg'></a><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='gradingTemplates.editGradingTemplatePopup($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='gradingTemplates.deleteGradingTemplateData($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.DeleteRecord") + "'><img src='/Content/VLE/img/svg/tbl-delete.svg'></a></div>";
            else
                actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='gradingTemplates.openGradingTemplateItem($(this),{0})' title='" + ResourceManager.GetString("ListCategories.GradingTemplates.OpenTemplateItem") + "'><img src='/Content/VLE/img/svg/tbl-view.svg'></a></div>";
            dataList = new
            {
                aaData = (from item in gradingTemplates
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.GradingTemplateId),
                              //Actions = item.IsEditable && CurrentPagePermission.CanEdit ? string.Format(HtmlHelperExtensions.ActionButtonsHtmlTemplate,
                              //HtmlHelperExtensions.LocalizedLinkButton(new HyperlinkAttributes()
                              //{
                              //    InnerIconCssClass = "fas fa-external-link-alt",
                              //    OnClick = "gradingTemplates.openGradingTemplateItem(" + item.GradingTemplateId + ")",
                              //    TitleResourceKey = "ListCategories.GradingTemplates.OpenTemplateItem"
                              //}) + "" + HtmlHelperExtensions.LocalizedEditLinkButton(new HyperlinkAttributes()
                              //{
                              //    PagePermission = CurrentPagePermission,
                              //    OnClick = "gradingTemplates.editGradingTemplatePopup($(this)," + item.GradingTemplateId + ")",
                              //}) + "" + HtmlHelperExtensions.LocalizedDeleteLinkButton(new HyperlinkAttributes()
                              //{
                              //    OnClick = "gradingTemplates.deleteGradingTemplateData($(this)," + item.GradingTemplateId + ")",
                              //})) : string.Format(HtmlHelperExtensions.ActionButtonsHtmlTemplate,
                              //HtmlHelperExtensions.LocalizedLinkButton(new HyperlinkAttributes()
                              //{
                              //    InnerIconCssClass = "fas fa-external-link-alt",
                              //    OnClick = "gradingTemplates.openGradingTemplateItem(" + item.GradingTemplateId + ")",
                              //    TitleResourceKey = "ListCategories.GradingTemplates.OpenTemplateItem"
                              //}) +"" + HtmlHelperExtensions.LocalizedEditLinkButton(new HyperlinkAttributes()
                              //{
                              //    PagePermission = CurrentPagePermission,
                              //    OnClick = "gradingTemplates.editGradingTemplatePopup($(this)," + item.GradingTemplateId + ")",
                              //})),
                              item.GradingTemplateTitle,
                              item.GradingTemplateDesc,
                              Logo = !string.IsNullOrWhiteSpace(item.GradingTemplateLogo) ? $"<img src='{(item.IsEditable? PhoenixConfiguration.Instance.ReadFilePath :"") + Url.Content(item.GradingTemplateLogo)}' class='img-thumbnail' width='40' height='40' />" : item.GradingTemplateLogo,
                              item.CreatedOn,
                              item.CreatedBy,
                              item.IsActive
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitAddEditGradingTemplateForm(int? id)
        {
            var model = new GradingTemplateEdit();
            if (id.HasValue)
            {
                var template = _gradingTemplateService.GetGradingTemplateById(id.Value);
                EntityMapper<Phoenix.Models.GradingTemplate, GradingTemplateEdit>.Map(template, model);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            }

            return PartialView("_AddEditGradingTemplate", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveGradingTemplateData(GradingTemplateEdit model, HttpPostedFileBase GradingImage)
        {
            model.ParseJsonToXml("GradingTemplateXml");
            //var test= Request.Files[0];
            if (!CurrentPagePermission.CanEdit)
            {
               
                return Json(new OperationDetails(false, LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            }
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }

            if (GradingImage!=null)
            {
                //string strFile = model.GradingTemplateTitle+Path.GetExtension(GradingImage.FileName);
                //string gradingTemplate = PhoenixConfiguration.Instance.WriteFilePath  + "/Content" + "/VLE" + "/img" + "/GradingTemplate";
                //CommonHelper.CreateDestinationFolder(gradingTemplate);
                //gradingTemplate = Path.Combine(gradingTemplate, strFile);
                //GradingImage.SaveAs(gradingTemplate);
                string folderPath = "/Content" + "/VLE" + "/img" + "/GradingTemplate";
                string fileName = model.GradingTemplateTitle + Path.GetExtension(GradingImage.FileName);
                CommonHelper.SaveFile(GradingImage, folderPath, false, fileName);
                model.GradingTemplateLogo = Constants.ProfileAvatarPath+"GradingTemplate/" + fileName;                      
            }
           
            if (model.GradingTemplateTitle.Trim() == "")
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.NoDataToSave), JsonRequestBehavior.AllowGet);
            }
            var result = _gradingTemplateService.UpdateGradingTemplate(model);
            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "Grading Template")), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteGradingTemplateData(int id)
        {
            var result = _gradingTemplateService.DeleteGradingTemplate(id);
            if (result == -1)
            {
                return Json(new OperationDetails(false, ResourceManager.GetString("ListCategories.GradingTemplates.GradingDeleteMessage")), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new OperationDetails(result == 1? true:false), JsonRequestBehavior.AllowGet);
            }
        }



    }
}