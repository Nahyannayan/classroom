﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ListCategories.Controllers
{
    [Authorize]
    public class SubjectListController : BaseController
    {
        private ISubjectService _subjectService;

        public SubjectListController(ISubjectService subjectService)
        {
            _subjectService = subjectService;
        }

        // GET: ListCategories/SubjectList
        public ActionResult Index()
        {            
            return View();
        }

        public ActionResult LoadSubjectGrid()
        {
            var subjects = _subjectService.GetSubjects((int)SessionHelper.CurrentSession.SchoolId);            
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in subjects
                          select new
                          {
                              Actions = HtmlHelperExtensions.LocalizedEditDeleteLinkButtons(item.SubjectId, "Subject", "subjects"),
                              item.SubjectName,
                              item.SubjectCode,
                              SubjectColorCode = $"<span class='d-inline-block p-3' style='background: {item.SubjectColorCode}'></span>",
                              item.SubjectTypeName,
                              item.FormattedCreatedOn,
                              item.CreatedByName,
                              item.IsActive
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitAddEditSubjectForm(int? id)
        {
            var model = new SubjectEdit();
            if (id.HasValue)
            {
                var subject = _subjectService.GetSubjectById(id.Value);
                EntityMapper<Subject, SubjectEdit>.Map(subject, model);
            }
            else
            {
                model.IsAddMode = true;
                model.SchoolId = (int)SessionHelper.CurrentSession.SchoolId;
            }
               
            return PartialView("_AddEditSubject", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSubjectData(SubjectEdit model)
        {
            //model.ParseJsonToXml("SubjectName");
            var result = _subjectService.UpdateSubjectData(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSubjectData(int id)
        {
            var result = _subjectService.DeleteSubjectData(id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
    }
}