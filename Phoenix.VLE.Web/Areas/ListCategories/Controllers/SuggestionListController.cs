﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ListCategories.Controllers
{
    public class SuggestionListController : BaseController
    {
        private ISuggestionCategoryService _suggestionCategoriesService;
        private ISuggestionService _suggestionService;
        public SuggestionListController(ISuggestionCategoryService suggestionCategoriesService, ISuggestionService suggestionService)
        {
            _suggestionCategoriesService = suggestionCategoriesService;
            _suggestionService = suggestionService;
        }

        // GET: ListCategories/SuggestionList
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadSuggestionGridBySchool()
        {
            var user = Helpers.CommonHelper.GetLoginUser();
            var suggestions = _suggestionService.getStudentSuggestionBySchoolId();
            var actionButtonsHtmlTemplate = string.Empty;
            actionButtonsHtmlTemplate = "<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='suggestionList.downloadAttachment($(this),\"{0}\")'><i class='fa fa-download'></i></button></div>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in suggestions
                          select new
                          {
                              Actions = String.IsNullOrEmpty(item.FileName)?"": String.Format(actionButtonsHtmlTemplate, item.FileName),
                              item.Title,
                              item.Description,
                              item.Type,
                              item.UserName
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }
        public string GetFileName(string fileName)
        {
            return String.Format("<div class='tbl-actions'><button type='button' class='table-action-icon-btn' onclick='suggestionList.downloadAttachment($(this),{0})'><i class='far fa-trash-alt'></i></button></div>","'"+ fileName + "'");

        }
        public void DownloadAttachment(string fileName)
        {
            string resourceDir = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir;
            string content = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content";
            string path = PhoenixConfiguration.Instance.ReadFilePath + Constants.ResourcesDir + "/Content" + "/SuggestionFiles/" + fileName;
            var webClient = new WebClient();
            byte[] FileBytes = webClient.DownloadData(path);
            Response.ContentType = GetMimeType(path);
            Response.BufferOutput = true;
            Response.AppendHeader("Content-Disposition", "Attachment; Filename=" + System.IO.Path.GetFileName(path) + "");
            Response.AddHeader("Last-Modified", DateTime.Now.ToLongDateString());
            Response.BinaryWrite(FileBytes);
            Response.Flush();
            Response.End();
        }

        private string GetMimeType(string filePath)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(filePath).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }
    }
}