﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ListCategories
{
    public class ListCategoriesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ListCategories";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ListCategories_default",
                "ListCategories/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}