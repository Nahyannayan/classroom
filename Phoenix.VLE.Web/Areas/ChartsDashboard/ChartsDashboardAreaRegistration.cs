﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ChartsDashboard
{
    public class ChartsDashboardAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ChartsDashboard";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ChartsDashboard_default",
                "ChartsDashboard/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}