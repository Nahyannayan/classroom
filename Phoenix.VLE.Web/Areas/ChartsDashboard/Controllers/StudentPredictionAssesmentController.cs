﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ChartsDashboard.Controllers
{
    public class StudentPredictionAssesmentController : BaseController
    {
        private readonly IStudentProgressTrackerService _studentProgressTrackerService;
        private readonly IStudentAssAndPredAnalysisService _studentAssAndPredAnalysisService;
        public StudentPredictionAssesmentController(IStudentProgressTrackerService studentProgressTrackerService, IStudentAssAndPredAnalysisService studentAssAndPredAnalysisService)
        {
            _studentProgressTrackerService = studentProgressTrackerService;
            _studentAssAndPredAnalysisService = studentAssAndPredAnalysisService;
        }

        // GET: ChartsDashboard/StudentPredicationAssesment
        public ActionResult Index()
        {
            long bsuid = SessionHelper.CurrentSession.SchoolId;
            StudentAssAndPredChartDashboard studentAssAndPredChartDashboard = new StudentAssAndPredChartDashboard();

            //studentAssAndPredChartDashboard = AllCharts(bsuid, "", "", "", studentId);

            //Various filters
            StudentFilterRes studentFilterRes = _studentAssAndPredAnalysisService.GetStudentProgressFilter(bsuid);

            if (studentFilterRes != null)
            {
                studentAssAndPredChartDashboard.StudentFilterRes = studentFilterRes;
                studentAssAndPredChartDashboard.StudentAssAndPredFilter = new StudentAssAndPredFilter();
                studentAssAndPredChartDashboard.StudentAssAndPredFilter.AcademicYear = new SelectList(studentAssAndPredChartDashboard.StudentFilterRes.AcademicYear, "id", "name");
                studentAssAndPredChartDashboard.StudentAssAndPredFilter.Class = new SelectList(studentAssAndPredChartDashboard.StudentFilterRes.Class, "id", "name");
                studentAssAndPredChartDashboard.StudentAssAndPredFilter.Subject = new SelectList(studentAssAndPredChartDashboard.StudentFilterRes.Subject, "id", "name");
                studentAssAndPredChartDashboard.StudentAssAndPredFilter.Assessment = new SelectList(studentAssAndPredChartDashboard.StudentFilterRes.Assessment, "id", "name");
                studentAssAndPredChartDashboard.StudentAssAndPredFilter.Teacher = new SelectList(studentAssAndPredChartDashboard.StudentFilterRes.Teacher, "id", "name");
            }

            return View(studentAssAndPredChartDashboard);
        }

        [HttpPost]
        public ActionResult GetStudentDetail(string classId,string assessment, string academicYear)
        {
            long bsuid = SessionHelper.CurrentSession.SchoolId;
            List<FilStudent> filStudents = _studentAssAndPredAnalysisService.GetStudentFilters(bsuid, classId, assessment, academicYear);
            TempData["studentdetail"] = filStudents;
            return Json(filStudents);
        }


        [HttpPost]
        public ActionResult GetStudent(string studentId)
        {
            FilStudent filStudent = ((List<FilStudent>)TempData.Peek("studentdetail")).FirstOrDefault(x => x.Id == studentId);
            return Json(filStudent);
        }
        public ActionResult GetAllCharts(string classId, string assessment, string academicYear, string studentId)
        {
            long bsuid = SessionHelper.CurrentSession.SchoolId;
            StudentAssAndPredChartDashboard studentAssAndPredChartDashboard = new StudentAssAndPredChartDashboard();
            studentAssAndPredChartDashboard = AllCharts(bsuid, classId, assessment, academicYear, studentId);

            return PartialView("_FilteredPredictionCharts",studentAssAndPredChartDashboard);
        }


        private StudentAssAndPredChartDashboard AllCharts(long bsuid,string classId, string assessment, string academicYear, string studentId)
        {
            StudentAssAndPredChartDashboard studentAssAndPredChartDashboard = new StudentAssAndPredChartDashboard();
            try
            {
                 studentAssAndPredChartDashboard.FileStudents = _studentAssAndPredAnalysisService.GetStudentFilters(bsuid, classId, assessment, academicYear);
                 List<AssessmentDataByStudent> assessmentDataByStudent = _studentAssAndPredAnalysisService.GetAllAssessmentData(bsuid, classId, assessment, academicYear, studentId);
                if (assessmentDataByStudent != null)
                {
                    studentAssAndPredChartDashboard.AssessmentDataByStudent = assessmentDataByStudent;
                }

                FilStudent filStudent = studentAssAndPredChartDashboard.FileStudents.FirstOrDefault(x => x.Id == studentId);
                if (filStudent != null)
                {
                    studentAssAndPredChartDashboard.FileStudent = filStudent;
                }

                StudentChartRes studentChartRes = _studentAssAndPredAnalysisService.GetStudentAssessmentPredictionCharts(bsuid, classId, assessment, academicYear, studentId);

                if (studentChartRes != null)
                {
                    studentAssAndPredChartDashboard.StudentChartRes = studentChartRes;
                    //Avg Score by Class and Prediction (Column Chart)
                    #region Avg Score by Class and Prediction (Column Chart)
                    studentAssAndPredChartDashboard.AvgScoreClassPredColumnChartVMs = new List<StudentAssAndPredViewModel>();
                    List<StudentPercentageValue> percentageValuesass = new List<StudentPercentageValue>();

                    List<string> categoriesass = new List<string>();
                    List<AverageScoreAssAndPredByAcademicYear> averageAssessmentPredictionByYears = new List<AverageScoreAssAndPredByAcademicYear>();
                    
                    foreach (var item in studentAssAndPredChartDashboard.StudentChartRes.AvgAssPredByYear?.GroupBy(c => c.AcademicYear) ?? Enumerable.Empty<IGrouping<string, AverageScoreAssAndPredByAcademicYear>>())
                    {
                        categoriesass.Add(item.Key);

                        foreach (var item1 in studentAssAndPredChartDashboard.StudentChartRes.AvgAssPredByYear?.GroupBy(k => k.Category) ?? Enumerable.Empty<IGrouping<string, AverageScoreAssAndPredByAcademicYear>>())
                        {
                            bool flag = true;

                            if (studentAssAndPredChartDashboard.StudentChartRes.AvgAssPredByYear.Where(x => x.AcademicYear == item.Key).Select(x => x.Category).Contains(item1.Key))
                            {
                                flag = false;
                                averageAssessmentPredictionByYears.Add(new AverageScoreAssAndPredByAcademicYear { AvgPercentage = studentAssAndPredChartDashboard.StudentChartRes.AvgAssPredByYear.FirstOrDefault(g => g.Category == item1.Key && g.AcademicYear == item.Key).AvgPercentage, Category = item1.Key, AcademicYear = item.Key });
                                //j++;
                            }
                            if (flag)
                            {
                                averageAssessmentPredictionByYears.Add(new AverageScoreAssAndPredByAcademicYear { AvgPercentage = 0, Category = item1.Key, AcademicYear = item.Key });
                            }
                        }
                    }

                    foreach (var item in averageAssessmentPredictionByYears.GroupBy(c => c.Category))
                    {
                        percentageValuesass.Add(new StudentPercentageValue { name = item.Key, data = item.Select(x => x.AvgPercentage == 0 ? (double?)null : Math.Round((double)x.AvgPercentage, 2, MidpointRounding.AwayFromZero)).ToList() });
                    }

                    studentAssAndPredChartDashboard.AvgScoreClassPredColumnChartVMs.Add(new StudentAssAndPredViewModel { LabelNameValue = percentageValuesass, categories = categoriesass });
                    #endregion

                    //Assessment vs Prediction by Academic Year (Column Chart)
                    #region Assessment vs Prediction by Academic Year (Column Chart)
                    studentAssAndPredChartDashboard.AssVsPredColumnChartVMs = new List<StudentAssAndPredViewModel>();
                    List<StudentPercentageValue> percentageValues = new List<StudentPercentageValue>();

                    List<string> categories = new List<string>();
                    List<AverageScoreAssAndPredByAcademicYear> averageMarksAssessmentPredictionByYears = new List<AverageScoreAssAndPredByAcademicYear>();

                    foreach (var item in studentAssAndPredChartDashboard.StudentChartRes.AvgMarksByYearAndAss?.GroupBy(c => c.AcademicYear) ?? Enumerable.Empty<IGrouping<string, AverageScoreAssAndPredByAcademicYear>>())
                    {
                        categories.Add(item.Key);
                        foreach (var item1 in studentAssAndPredChartDashboard.StudentChartRes.AvgMarksByYearAndAss?.GroupBy(k => k.Assessment) ?? Enumerable.Empty<IGrouping<string, AverageScoreAssAndPredByAcademicYear>>())
                        {
                            bool flag1 = true;

                            if (studentAssAndPredChartDashboard.StudentChartRes.AvgMarksByYearAndAss.Where(x => x.AcademicYear == item.Key).Select(x => x.Assessment).Contains(item1.Key))
                            {
                                flag1 = false;
                                averageMarksAssessmentPredictionByYears.Add(new AverageScoreAssAndPredByAcademicYear { AvgPercentage = studentAssAndPredChartDashboard.StudentChartRes.AvgMarksByYearAndAss.FirstOrDefault(g => g.Assessment == item1.Key && g.AcademicYear == item.Key).AvgPercentage, Assessment = item1.Key, AcademicYear = item.Key });
                            }
                            if (flag1)
                            {
                                averageMarksAssessmentPredictionByYears.Add(new AverageScoreAssAndPredByAcademicYear { AvgPercentage = 0, Assessment = item1.Key, AcademicYear = item.Key });
                            }
                        }
                    }

                    foreach (var item in averageMarksAssessmentPredictionByYears.GroupBy(c => c.Assessment))
                    {
                        percentageValues.Add(new StudentPercentageValue { name = item.Key, data = item.Select(x => x.AvgPercentage == 0 ? (double?)null : Math.Round((double)x.AvgPercentage, 2, MidpointRounding.AwayFromZero)).ToList() });
                    }

                    studentAssAndPredChartDashboard.AssVsPredColumnChartVMs.Add(new StudentAssAndPredViewModel { LabelNameValue = percentageValues, categories = categories });
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return studentAssAndPredChartDashboard;
        }

        [HttpPost]
        public ActionResult GetAllAssessment(string classId, string assessment, string academicYear, string studentId)
        {
            long bsuid = SessionHelper.CurrentSession.SchoolId;
            List<AssessmentDataByStudent> assessmentDataByStudent = _studentAssAndPredAnalysisService.GetAllAssessmentData(bsuid, classId, assessment, academicYear, studentId);
            return Json(assessmentDataByStudent);
        }
    }
    
}