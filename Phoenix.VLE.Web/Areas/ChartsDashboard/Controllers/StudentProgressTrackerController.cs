﻿using DevExpress.XtraSpreadsheet.Layout;
using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.ChartsDashboard.Controllers
{

    public class StudentProgressTrackerController : BaseController
    {
        private readonly IStudentProgressTrackerService _studentProgressTrackerService;
        public StudentProgressTrackerController(IStudentProgressTrackerService studentProgressTrackerService)
        {
            _studentProgressTrackerService = studentProgressTrackerService;
        }


        // GET: ChartsDashboard/StudentProgressTracker
        public ActionResult Index()
        {
            //long bsuid = 131001
            long bsuid = SessionHelper.CurrentSession.SchoolId;
            string defaultClassid = "";

            StudentProgressTrackerChartsDashboard studentProgressTrackerChartsDashboard = new StudentProgressTrackerChartsDashboard();

            //Various filters
            FilterRes filterRes = _studentProgressTrackerService.GetStudentProgressFilter(bsuid);
            if (filterRes != null)
            {
                /* Default Class value to be selected is {10} */
                var _objClass = filterRes.Class.Where(x => x.id == "10").FirstOrDefault();
                defaultClassid = _objClass == null ? "" : _objClass.id;

                studentProgressTrackerChartsDashboard = GetCharts(bsuid, "", "", defaultClassid, "", "");

                studentProgressTrackerChartsDashboard.FilterRes = filterRes;

                studentProgressTrackerChartsDashboard.StudentProgressFilter = new StudentProgressFilter();
                studentProgressTrackerChartsDashboard.StudentProgressFilter.AcademicYear = new SelectList(studentProgressTrackerChartsDashboard.FilterRes.AcademicYear, "id", "name");
                studentProgressTrackerChartsDashboard.StudentProgressFilter.Class = new SelectList(studentProgressTrackerChartsDashboard.FilterRes.Class, "id", "name", defaultClassid);
                studentProgressTrackerChartsDashboard.StudentProgressFilter.Subject = new SelectList(studentProgressTrackerChartsDashboard.FilterRes.Subject, "id", "name");
                studentProgressTrackerChartsDashboard.StudentProgressFilter.Assessment = new SelectList(studentProgressTrackerChartsDashboard.FilterRes.Assessment, "id", "name");
                studentProgressTrackerChartsDashboard.StudentProgressFilter.Teacher = new SelectList(studentProgressTrackerChartsDashboard.FilterRes.Teacher, "id", "name");
            }

            return View("Index2", studentProgressTrackerChartsDashboard);
        }

        [HttpPost]
        public ActionResult ChartFilter(string academicYear, string subject, string classId, string assessment, string teacher)
        {
            long bsuid = SessionHelper.CurrentSession.SchoolId;

            if (academicYear != null && academicYear.Trim() == "")
                academicYear = null;
            if (subject != null && subject.Trim() == "")
                subject = null;
            if (classId != null && classId.Trim() == "")
                classId = null;
            if (assessment != null && assessment.Trim() == "")
                assessment = null;
            if (teacher != null && teacher.Trim() == "")
                teacher = null;
            StudentProgressTrackerChartsDashboard studentProgressTrackerChartsDashboard = new StudentProgressTrackerChartsDashboard();
            studentProgressTrackerChartsDashboard = GetCharts(bsuid, academicYear, subject, classId, assessment, teacher);

            return PartialView("_FilteredProgressCharts", studentProgressTrackerChartsDashboard);
        }

        private StudentProgressTrackerChartsDashboard GetCharts(long bsuid, string academicYear, string subject, string classId, string assessment, string teacher)
        {
            StudentProgressTrackerChartsDashboard studentProgressTrackerChartsDashboard = new StudentProgressTrackerChartsDashboard();
            try
            {
                ChartRes chartRes = _studentProgressTrackerService.GetChart(bsuid, academicYear, subject, classId, assessment, teacher);
                
                if (chartRes != null)
                {
                    studentProgressTrackerChartsDashboard.ChartRes = chartRes;

                    //Avg Score by Year (Column Chart)
                    #region Avg Score by Year (Column Chart)
                    studentProgressTrackerChartsDashboard.AvgScoreColumnChartVMs = new List<AvgScoreColumnChartViewModel>();
                    List<PercentageValue> percentageValuesass = new List<PercentageValue>();

                    List<string> categoriesass = new List<string>();
                    List<AverageAssessmentPredictionByYear> averageMarksAssessmentPredictionByYears = new List<AverageAssessmentPredictionByYear>();

                    foreach (var item in studentProgressTrackerChartsDashboard.ChartRes.AvgAssPredByYear.GroupBy(c => c.AcademicYear))
                    {
                        categoriesass.Add(item.Key);
                        
                            foreach (var item1 in studentProgressTrackerChartsDashboard.ChartRes.AvgAssPredByYear.GroupBy(k => k.Category))
                            {
                                bool flag = true;

                                if (studentProgressTrackerChartsDashboard.ChartRes.AvgAssPredByYear.Where(x => x.AcademicYear == item.Key).Select(s=>s.Category).Contains(item1.Key))
                                {
                                    flag = false;
                                averageMarksAssessmentPredictionByYears.Add(new AverageAssessmentPredictionByYear { AvgPercentage = studentProgressTrackerChartsDashboard.ChartRes.AvgAssPredByYear.FirstOrDefault(g => g.Category == item1.Key && g.AcademicYear == item.Key).AvgPercentage, Category = item1.Key, AcademicYear = item.Key });
                                    //j++;
                                }
                                if (flag)
                                {
                                averageMarksAssessmentPredictionByYears.Add(new AverageAssessmentPredictionByYear { AvgPercentage = 0, Category = item1.Key, AcademicYear = item.Key });
                                }
                            }
                        
                    }

                    foreach (var item in averageMarksAssessmentPredictionByYears.GroupBy(c => c.Category))
                    {
                        percentageValuesass.Add(new PercentageValue { name = item.Key, data = item.Select(x => x.AvgPercentage == 0 ? (double?)null : Math.Round((double)x.AvgPercentage, 2, MidpointRounding.AwayFromZero)).ToList() });
                    }

                    studentProgressTrackerChartsDashboard.AvgScoreColumnChartVMs.Add(new AvgScoreColumnChartViewModel { LabelNameValue = percentageValuesass, categories = categoriesass });
                    #endregion

                    //Student Count Prediction (Donut Chart)
                    #region Student Count Prediction (Donut Chart)
                    List<StudentCountDonutChartViewModel> studentCountVMs = new List<StudentCountDonutChartViewModel>();
                    foreach (var item in studentProgressTrackerChartsDashboard.ChartRes.PredBuck)
                    {
                        studentCountVMs.Add(new StudentCountDonutChartViewModel { category = item.PRange, value = item.StudentCount });
                    }
                    studentProgressTrackerChartsDashboard.StudentCountDonutChartVMs = studentCountVMs;
                    #endregion

                    //Assessment wise Avg Score by Year (Column Chart)
                    #region Assessment wise Avg Score by Year (Column Chart)
                    studentProgressTrackerChartsDashboard.AssWiseColumnChartVMs = new List<AvgScoreColumnChartViewModel>();
                    List<PercentageValue> percentageValues = new List<PercentageValue>();

                    List<string> categories = new List<string>();
                    List<AverageAssessmentPredictionByYear> assessmentPredictionByYears = new List<AverageAssessmentPredictionByYear>();

                    foreach (var item in studentProgressTrackerChartsDashboard.ChartRes.AvgMarksByYearAndAss.GroupBy(c => c.AcademicYear))
                    {
                        categories.Add(item.Key);
                        foreach (var item1 in studentProgressTrackerChartsDashboard.ChartRes.AvgMarksByYearAndAss.GroupBy(k => k.Assessment))
                        {
                            bool flag1 = true;

                            if (studentProgressTrackerChartsDashboard.ChartRes.AvgMarksByYearAndAss.Where(x=>x.AcademicYear==item.Key).Select(x=>x.Assessment).Contains(item1.Key))
                            {
                                flag1 = false;
                                assessmentPredictionByYears.Add(new AverageAssessmentPredictionByYear { AvgPercentage = studentProgressTrackerChartsDashboard.ChartRes.AvgMarksByYearAndAss.FirstOrDefault(g => g.Assessment == item1.Key && g.AcademicYear == item.Key).AvgPercentage, Assessment = item1.Key, AcademicYear = item.Key });
                            }
                            if (flag1)
                            {
                                assessmentPredictionByYears.Add(new AverageAssessmentPredictionByYear { AvgPercentage = 0, Assessment = item1.Key, AcademicYear = item.Key });
                            }
                        }
                    }

                    foreach (var item in assessmentPredictionByYears.GroupBy(c => c.Assessment))
                    {
                        percentageValues.Add(new PercentageValue { name = item.Key, data = item.Select(x => x.AvgPercentage == 0 ? (double?)null : Math.Round((double)x.AvgPercentage, 2, MidpointRounding.AwayFromZero)).ToList() });
                    }

                    studentProgressTrackerChartsDashboard.AssWiseColumnChartVMs.Add(new AvgScoreColumnChartViewModel { LabelNameValue = percentageValues, categories = categories });
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return studentProgressTrackerChartsDashboard;
        }
    }
}