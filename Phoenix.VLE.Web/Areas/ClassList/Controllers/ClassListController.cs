﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using SMS.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models.Entities;
using Phoenix.Common.Localization;
using SMS.Web.Areas.SMS.ViewModels;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Models;
using SMS.Web.Areas.SMS.Model;
using Phoenix.Common.Enums;

namespace Phoenix.VLE.Web.Areas.ClassList.Controllers
{
    [Authorize]
    public class ClassListController : BaseController
    {

        private IClassListService _ClassListService;
        private ISIMSCommonService _SIMSCommonService;
        private IAttendanceService _AttendanceService;

        public ClassListController(IClassListService ClassListService, ISIMSCommonService SIMSCommonService, IAttendanceService AttendanceService)
        {
            _ClassListService = ClassListService;
            _SIMSCommonService = SIMSCommonService;
            _AttendanceService = AttendanceService;
        }
        // GET: ClassList/ClassList
        public ActionResult Index()
        {
            //var CurrentUserId = 815837;
            var CurrentUserId = SessionHelper.CurrentSession.Id;
            ViewBag.UserTypeId = SessionHelper.CurrentSession.UserTypeId;
            ViewBag.StudentGroupList = new SelectList(SelectListHelper.GetSelectListData(ListItems.CourseGroupList, $"{null},{CurrentUserId}"), "ItemId", "ItemName");
            var list = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolGrade, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.GradeList = list;
            return View("ClassList");
        }
        public JsonResult FetchSectionListByGrade(long GradeId = 0)
        {
            SelectList list = new SelectList(string.Empty);
            if (GradeId > 0)
            {
                list = new SelectList(SelectListHelper.GetSelectListData(ListItems.Section, $"{GradeId}").ToList(), "ItemId", "ItemName");
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        // GET: SIMS/ClassList/GetClassList?username={sting}&tt_id={int}
        public ActionResult GetClassList(long GroupId = 0, long GradeId = 0, long SectionId = 0)
        {
            var SchoolId = SessionHelper.CurrentSession.SchoolId;
            var list = _ClassListService.GetClassList(SchoolId, GroupId, GradeId, SectionId);
            return View("ClassList", list);
        }
        public PartialViewResult GetStudentCard(long GroupId = 0, long GradeId = 0, long SectionId = 0, bool IsAllDdlEnabled = false)
        {
            var SchoolId = SessionHelper.CurrentSession.SchoolId;
            TempData["UserTypeId"] = SessionHelper.CurrentSession.UserTypeId;
            IEnumerable<ClassListModel> list = new List<ClassListModel>();
            if ((!IsAllDdlEnabled && (GroupId != 0 || (GradeId != 0 && SectionId != 0))) || (IsAllDdlEnabled && GroupId != 0 && GradeId != 0 && SectionId != 0))
            {
                list = _ClassListService.GetClassList(SchoolId, GroupId, GradeId, SectionId);
            }
            TempData["classList"] = list is null ? null : list.ToList();
            return PartialView("_StudentCardPartial", list);
        }
        // GET: SIMS/ClassList/GetStudentDetails?stu_id={sting}
        public ActionResult GetStudentDetails(string stu_id, string view = "", bool IsOpenBySearch = false)
        {
            var listOfDetails = (List<ClassListModel>)TempData.Peek("classList");
            var Student_Profile = new StudentProfileModel();
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            DateTime nowDate = (DateTime)DateTime.UtcNow.ConvertUtcToLocalTime();
            Student_Profile = _ClassListService.GetStudentProfileDetail(Convert.ToInt64(stu_id), SchoolId, nowDate);
            Student_Profile.IsOpenBySearch = IsOpenBySearch;
            if (Student_Profile.BasicDetailModel == null)
            {
                Student_Profile.BasicDetailModel = new BasicDetailModel();
            }
            if (Student_Profile.ParentDetailModel == null)
            {
                Student_Profile.ParentDetailModel = new ParentDetailModel();
            }
            if (Student_Profile.MedicalDetailModel == null)
            {
                Student_Profile.MedicalDetailModel = new MedicalDetailModel();
            }
            if (Student_Profile.SiblingDetailModel == null)
            {
                Student_Profile.SiblingDetailModel = new List<SiblingDetailModel>();
            }
            if (Student_Profile.AchievementsDetailModel == null)
            {
                Student_Profile.AchievementsDetailModel = new List<AchievementsDetailModel>();
            }
            if (Student_Profile.IncidentDetailModel == null)
            {
                Student_Profile.IncidentDetailModel = new List<BehaviorDetailModel>();
            }
            if (Student_Profile.AttendanceDetailModel == null)
            {
                Student_Profile.AttendanceDetailModel = new List<AttendanceDetailModel>();
            }
            if (Student_Profile.TransportDetailModel == null)
            {
                Student_Profile.TransportDetailModel = new TransportDetailModel();
            }
            if (Student_Profile.StudentDashboardModel == null)
            {
                Student_Profile.StudentDashboardModel = new StudentDashboardModel();
            }
            if (Student_Profile.AttendenceListModel == null)
            {
                Student_Profile.AttendenceListModel = new List<AttendenceListModel>();
            }
            if (Student_Profile.AssessmentDetailModel == null)
            {
                Student_Profile.AssessmentDetailModel = new List<AssessmentDetailModel>();
            }
            if (Student_Profile.BehaviourSubCategoryList == null)
            {
                Student_Profile.BehaviourSubCategoryList = new List<BehaviourSubCategoryModel>();
            }
            if (string.IsNullOrEmpty(Student_Profile.IncidentBehaviourPoint))
            {
                Student_Profile.IncidentBehaviourPoint = string.Empty;
            }
            if (Student_Profile.WeekDayList == null)
            {
                Student_Profile.WeekDayList = new List<WeekDayModel>();
            }

            var studentlstOfIds = listOfDetails != null && listOfDetails.Any() ? listOfDetails.OrderBy(e => e.StudentName).Select(e => Convert.ToInt32(e.StudentID)).ToList() : new List<int>();
            int CountOfIds = studentlstOfIds.Count();
            int max = studentlstOfIds.Any() ? studentlstOfIds.Max() : 0;
            int min = studentlstOfIds.Any() ? studentlstOfIds.Min() : 0;

            int currentElem = Convert.ToInt32(stu_id);
            var index = studentlstOfIds.IndexOf(currentElem);

            int preElem = 0;
            int nextElem = 0;
            int incrementalIndex = index + 1;
            if (index > -1)
            {
                preElem = index == 0 ? studentlstOfIds[index] : studentlstOfIds[index - 1];
                nextElem = incrementalIndex >= CountOfIds ? studentlstOfIds[index] : studentlstOfIds[index + 1];

            }
            Student_Profile.PrevStudentName = listOfDetails != null && listOfDetails.Where(e => Convert.ToInt32(e.StudentID) == preElem).Any() ? listOfDetails.Where(e => Convert.ToInt32(e.StudentID) == preElem).FirstOrDefault().StudentName : string.Empty;
            Student_Profile.NextStudentName = listOfDetails != null && listOfDetails.Where(e => Convert.ToInt32(e.StudentID) == preElem).Any() ? listOfDetails.Where(e => Convert.ToInt32(e.StudentID) == nextElem).FirstOrDefault().StudentName : string.Empty;

            ViewBag.PrevElement = preElem;
            ViewBag.NextElement = nextElem;

            ViewBag.PrevCss = index == 0 ? "d-none" : "";
            ViewBag.NextCss = incrementalIndex == CountOfIds ? "d-none" : "";
            ViewBag.isFullView = !string.IsNullOrEmpty(view);
            Student_Profile.IsFullView = !string.IsNullOrEmpty(view);
            return PartialView("_StudentListTab", Student_Profile);
        }
        public ActionResult StudentSiblingDetail(long StudentId = 0)
        {
            ViewBag.StudentId = StudentId;
            ViewBag.UserTypeId = SessionHelper.CurrentSession.UserTypeId;
            return View();
        }
        public ActionResult StudentGlobalSearch(long StudentId = 0)
        {
            ViewBag.StudentId = StudentId;
            ViewBag.UserTypeId = SessionHelper.CurrentSession.UserTypeId;
            return View();
        }
        public ActionResult GetStudentSiblingDetail(string stu_id, string view = "", bool IsOpenBySearch = false)
        {
            var Student_Profile = new StudentProfileModel();
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            DateTime nowDate = (DateTime)DateTime.UtcNow.ConvertUtcToLocalTime();
            Student_Profile = _ClassListService.GetStudentProfileDetail(Convert.ToInt64(stu_id), SchoolId, nowDate);
            Student_Profile.IsOpenBySearch = IsOpenBySearch;
            if (Student_Profile.BasicDetailModel == null)
            {
                Student_Profile.BasicDetailModel = new BasicDetailModel();
            }
            if (Student_Profile.ParentDetailModel == null)
            {
                Student_Profile.ParentDetailModel = new ParentDetailModel();
            }
            if (Student_Profile.MedicalDetailModel == null)
            {
                Student_Profile.MedicalDetailModel = new MedicalDetailModel();
            }
            if (Student_Profile.SiblingDetailModel == null)
            {
                Student_Profile.SiblingDetailModel = new List<SiblingDetailModel>();
            }
            if (Student_Profile.AchievementsDetailModel == null)
            {
                Student_Profile.AchievementsDetailModel = new List<AchievementsDetailModel>();
            }
            if (Student_Profile.IncidentDetailModel == null)
            {
                Student_Profile.IncidentDetailModel = new List<BehaviorDetailModel>();
            }
            if (Student_Profile.AttendanceDetailModel == null)
            {
                Student_Profile.AttendanceDetailModel = new List<AttendanceDetailModel>();
            }
            if (Student_Profile.TransportDetailModel == null)
            {
                Student_Profile.TransportDetailModel = new TransportDetailModel();
            }
            if (Student_Profile.StudentDashboardModel == null)
            {
                Student_Profile.StudentDashboardModel = new StudentDashboardModel();
            }
            if (Student_Profile.AttendenceListModel == null)
            {
                Student_Profile.AttendenceListModel = new List<AttendenceListModel>();
            }
            if (Student_Profile.AssessmentDetailModel == null)
            {
                Student_Profile.AssessmentDetailModel = new List<AssessmentDetailModel>();
            }
            if (Student_Profile.BehaviourSubCategoryList == null)
            {
                Student_Profile.BehaviourSubCategoryList = new List<BehaviourSubCategoryModel>();
            }
            if (string.IsNullOrEmpty(Student_Profile.IncidentBehaviourPoint))
            {
                Student_Profile.IncidentBehaviourPoint = string.Empty;
            }
            if (Student_Profile.WeekDayList == null)
            {
                Student_Profile.WeekDayList = new List<WeekDayModel>();
            }
            if (Student_Profile.BehaviourPointModel == null)
            {
                Student_Profile.BehaviourPointModel =new BehaviourPointModel();
            }

            ViewBag.PrevElement = 0;
            ViewBag.NextElement = 0;

            ViewBag.PrevCss = "d-none";
            ViewBag.NextCss = "d-none";
            ViewBag.isFullView = true;
            Student_Profile.IsFullView = true;
            return PartialView("_StudentListTab", Student_Profile);
        }
        public PartialViewResult CourseWiseSchoolGroup(long StudentId)
        {
            var changeGroupStudentModel = _ClassListService.GetCourseWiseSchoolGroups(StudentId);
            return PartialView("_CourseWiseSchoolGroup", changeGroupStudentModel);
        }
        public PartialViewResult GetWeeklyTimeTableDetail(long StudentId, int WeekCount = 0)
        {
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            DateTime nowDate = (DateTime)DateTime.UtcNow.ConvertUtcToLocalTime();
            var weeklyDayList = _ClassListService.GetWeeklyTimeTableDetail(StudentId, SchoolId, WeekCount, nowDate);
            return PartialView("_StudentTimeTableDashboard", weeklyDayList);
        }
        public PartialViewResult GetQuickContactDetail(long StudentId)
        {
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            var parentDetail = _ClassListService.GetQuickContactDetail(StudentId, SchoolId);
            return PartialView("_QuickContactDetail", parentDetail);
        }
        public PartialViewResult GetStudentFindMeDetail(long StudentId)
        {
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            DateTime nowDate = (DateTime)DateTime.UtcNow.ConvertUtcToLocalTime();
            var timeTable = _ClassListService.GetStudentFindMeDetail(StudentId, SchoolId, nowDate);
            return PartialView("_StudentFindMeDetail", timeTable);
        }
        public ActionResult ChangeCourseWiseStudentSchoolGroup(List<ChangeGroupCourseModel> ChangeGroupCourseList)
        {
            ChangeGroupStudentModel changeGroupStudentModel = new ChangeGroupStudentModel();
            changeGroupStudentModel.ChangeGroupCourseList = ChangeGroupCourseList;
            var result = _ClassListService.ChangeCourseWiseStudentSchoolGroup(changeGroupStudentModel);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult AttendanceProfileHistory(string id, string AttendanceDt = "", string AttendanceType = "")
        {
            AttendanceDt = AttendanceDt == string.Empty ? String.Format("{0:dd-MMM-yyyy}", DateTime.Now.Date) : AttendanceDt;
            AttendanceType = string.IsNullOrEmpty(AttendanceType) ? "W" : AttendanceType;
            var SchoolId = SessionHelper.CurrentSession.SchoolId;
            var Attendance_Profile = new AttendanceProfileModel();
            Attendance_Profile = _ClassListService.GetAttendanceProfileDetail(Convert.ToInt64(id), SchoolId, AttendanceDt, AttendanceType);
            Attendance_Profile.AttendanceType = AttendanceType;
            ViewBag.Stu_Acd_Year = Attendance_Profile.StudentDetail.StudentACDYEAR;
            ViewBag.HistoryDate = AttendanceDt;

            if (Attendance_Profile.AttendenceSessionCode != null)
            {
                if (Attendance_Profile.AttendenceSessionCode.Count() > 0)
                {
                    var attendanceSessionCode = Attendance_Profile.AttendenceSessionCode.FirstOrDefault();
                    Attendance_Profile.OverAllPercentage = attendanceSessionCode.OverallPrecent;
                    Attendance_Profile.AttendanceCount = attendanceSessionCode.AttendanceCount;
                    Attendance_Profile.TotalClassesHeld = attendanceSessionCode.TotalClassesHeld;
                }
            }
            return PartialView("_AttendanceProfileDetail", Attendance_Profile);
        }
        [HttpGet]
        public ActionResult AttendanceProfileDetail(string id, string AttendanceDt = "", string AttendanceType = "")
        {
            AttendanceDt = AttendanceDt == string.Empty ? String.Format("{0:dd-MMM-yyyy}", DateTime.Now.Date) : AttendanceDt;
            AttendanceType = string.IsNullOrEmpty(AttendanceType) ? "W" : AttendanceType;
            var SchoolId = SessionHelper.CurrentSession.SchoolId;
            var Attendance_Profile = new AttendanceProfileModel();
            Attendance_Profile = _ClassListService.GetAttendanceProfileDetail(Convert.ToInt64(id), SchoolId, AttendanceDt, AttendanceType);
            Attendance_Profile.AttendanceType = AttendanceType;
            ViewBag.Stu_Acd_Year = Attendance_Profile.StudentDetail.StudentACDYEAR;
            ViewBag.HistoryDate = AttendanceDt;
            if (Attendance_Profile.AttendenceSessionCode != null)
            {
                if (Attendance_Profile.AttendenceSessionCode.Count() > 0)
                {
                    var attendanceSessionCode = Attendance_Profile.AttendenceSessionCode.FirstOrDefault();
                    Attendance_Profile.OverAllPercentage = attendanceSessionCode.OverallPrecent;
                    Attendance_Profile.AttendanceCount = attendanceSessionCode.AttendanceCount;
                    Attendance_Profile.TotalClassesHeld = attendanceSessionCode.TotalClassesHeld;
                }
            }
            return PartialView("_AttendanceDetail", Attendance_Profile);
        }

        //[HttpGet]
        //public ActionResult StudentHistory(int id)
        //{

        //    var model = new MarkedAttendaceDetails();
        //    ViewBag.HistoryType = "W";
        //    string date = String.Format("{0:dd-MMM-yyyy}", DateTime.Now.Date);

        //    model.StudentDetail = _ClassListService.GetStudentDetails(id.ToString());
        //    ViewBag.Stu_Acd_Year = model.StudentDetail.Student_ACD_YEAR;

        //    model.AttendenceBySession = _AttendanceService.Get_AttendenceBySession(id.ToString(), Convert.ToDateTime(date)).ToList();
        //    model.AttendenceSessionCode = _AttendanceService.Get_AttendenceSessionCode(id.ToString(), Convert.ToDateTime(date)).ToList();
        //    model.AttendenceList = _ClassListService.GetAttendenceList(id.ToString(), Convert.ToDateTime(date)).ToList();

        //    ViewBag.HistoryDate = date;

        //    var AttendenceChartList = _AttendanceService.Get_AttendanceChartMain(id.ToString());
        //    var acd_descs = AttendenceChartList.Select(s => s.ACD_DESC).Distinct();

        //    int total = acd_descs.Count();
        //    foreach (string ACD in acd_descs)
        //    {
        //        if (total == 3)
        //        {
        //            model.AttendenceHistory3 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
        //        }
        //        else if (total == 2)
        //        {
        //            model.AttendenceHistory2 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
        //        }
        //        else if (total == 1)
        //        {
        //            model.AttendenceHistory1 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
        //        }
        //        total--;
        //    }
        //    var _classListDashboard = _ClassListService.GetStudentDashboardDetails(id.ToString());


        //    ViewBag.OverAllPercentage = _classListDashboard.Attencence;

        //    return View("AttendanceDetail", model);
        //}

        public PartialViewResult LoadStudentOnReportForm(long studentId)
        {
            var result = _ClassListService.GetStudentOnReportMasters(studentId, SessionHelper.CurrentSession.ACD_ID, SessionHelper.CurrentSession.SchoolId);
            ViewBag.AcademicYearId = SessionHelper.CurrentSession.ACD_ID;
            ViewBag.SchoolId = SessionHelper.CurrentSession.SchoolId;
            ViewBag.StudentId = studentId;
            return PartialView("_LoadStudentOnReportForm", result);
        }
        public ActionResult SaveStudentOnReport(StudentOnReportMasterModel studentOnReportMaster)
        {
            studentOnReportMaster.AcademicYearId = SessionHelper.CurrentSession.ACD_ID;
            studentOnReportMaster.SchoolId = SessionHelper.CurrentSession.SchoolId;
            studentOnReportMaster.CreatedBy = SessionHelper.CurrentSession.UserName;
            if (!string.IsNullOrEmpty(studentOnReportMaster.GroupId))
                studentOnReportMaster.GroupId = EncryptDecryptHelper.Decrypt(studentOnReportMaster.GroupId);
            var result = _ClassListService.StudentOnReportMasterCU(studentOnReportMaster);
            if (result > 0)
            {
                var newRecords = _ClassListService.GetStudentOnReportMasters(studentOnReportMaster.StudentId, SessionHelper.CurrentSession.ACD_ID, SessionHelper.CurrentSession.SchoolId);
                return PartialView("_LoadStudentOnReportGrid", newRecords);
            }
            else if (result == -10)
                return Json(new OperationDetails(false, ResourceManager.GetString("SIMS.Classlist.StudentAlreadyExistForReporting")), JsonRequestBehavior.AllowGet);
            else
                return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult LoadStudentOnReportDetailsForm(long studentId, long? studentOnReportMasterId, string groupId = null)
        {
            var parameters = new StudentOnReportParameterModel
            {
                StudentId = studentId,
                GroupId = groupId,
                AcademicYear = SessionHelper.CurrentSession.ACD_ID,
                SchoolId = SessionHelper.CurrentSession.SchoolId,
                StudentOnReportMasterId = studentOnReportMasterId
            };
            var result = _ClassListService.GetStudentOnReportDetails(parameters);
            return PartialView("_LoadStudentOnReportDetailsForm", result);
        }
        public ActionResult SaveStudentOnReportDetails(StudentOnReportModel studentOnReportDetail)
        {
            studentOnReportDetail.CreatedBy = SessionHelper.CurrentSession.UserName;
            if (!string.IsNullOrEmpty(studentOnReportDetail.GroupId))
                studentOnReportDetail.GroupId = EncryptDecryptHelper.Decrypt(studentOnReportDetail.GroupId);
            var result = _ClassListService.StudentOnReportDetailsCU(studentOnReportDetail);
            if (result > 0)
            {
                var parameters = new StudentOnReportParameterModel
                {
                    StudentId = studentOnReportDetail.Id,
                    AcademicYear = SessionHelper.CurrentSession.ACD_ID,
                    SchoolId = SessionHelper.CurrentSession.SchoolId,
                    StudentOnReportMasterId = studentOnReportDetail.Id
                };
                var newRecords = _ClassListService.GetStudentOnReportDetails(parameters);
                return PartialView("_LoadStudentOnReportDetailsGrid", newRecords);
            }
            else
                return Json(new OperationDetails(false), JsonRequestBehavior.AllowGet);
        }
        public ActionResult StudentDetailBySearch(string SearchString = "")
        {
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            long UserId = SessionHelper.CurrentSession.Id;
            IEnumerable<ClassListModel> classList = new List<ClassListModel>();
            var studentList = _ClassListService.GetStudentListBySearch(SchoolId, UserId, SearchString);
            if (studentList != null)
                classList = studentList;
            return PartialView("_StudentDetailBySearch", classList);
        }
        public ActionResult StudentDetailByGlobalSearch(string SearchString = "")
        {
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            long UserId = SessionHelper.CurrentSession.Id;
            IEnumerable<ClassListModel> classList = new List<ClassListModel>();
            var studentList = _ClassListService.GetStudentListBySearch(SchoolId, UserId, SearchString);
            if (studentList != null)
                classList = studentList;
            return PartialView("_StudentDetailByGlobalSearch", classList);
        }
    }
}