﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.CustomAttributes;
using Phoenix.Models;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Template.PlanScheme")]
    public class PlanSchemeDetailEdit
    {
        public Int64 PlanSchemeDetailId { get; set; }
        public Int64 TemplateId { get; set; }
        public string PlanSchemeName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public bool IsActive { get; set; }
        public bool CanApprovePlan { get; set; }
        public int Status { get; set; }
        public string StatusText { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime UpdatedOn { get; set; }
        public Int64 UpdatedBy { get; set; }
        public bool IsAddMode { get; set; }
        public List<SelectListItem> TemplateList { get; set; }
        public List<TemplateField> TemplateFields { get; set; }

        public RichTemplateEditorData RichTemplateEditorData { get; set; }

        //Added By Shankar
        public string GroupList { get; set; }
        public string UnitList { get; set; }
        public long SchoolId { get; set; }
        public bool IsRejected { get; set; }
        public string SelectedGrade { get; set; }

        public PlanSchemeDetailEdit()
        {
            TemplateList = new List<SelectListItem>();
            TemplateFields = new List<TemplateField>();
            RichTemplateEditorData = new RichTemplateEditorData();
        }
    }
}