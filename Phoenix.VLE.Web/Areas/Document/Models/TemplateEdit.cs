﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.CustomAttributes;
using Phoenix.Models;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Template.Template")]
    public class TemplateEdit
    {
        public Int64 TemplateId { get; set; }
        public Int64 SchoolId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TemplateType { get; set; }
        public string Period { get; set; }
        public int SelectedColumnId { get; set; }
        public bool IsActive { get; set; }
        public int Status { get; set; }
        public string StatusText { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime UpdatedOn { get; set; }
        public Int64 UpdatedBy { get; set; }
        public bool IsAddMode { get; set; }
        public bool IsApprovalRequired { get; set; }
        public string RejectedComment { get; set; }
        public bool IsRejected { get; set; }
        public bool IsPlanTemplateDeleteAble { get; set; }
        public List<TemplateField> TemplateFields { get; set; }
        public List<SelectListItem> TemplateTypeList { get; set; }
        public List<SelectListItem> PeriodList { get; set; }
        //public List<SelectListItem> CertifiedColumn { get; set; }

        public string FilePath { get; set; }
        public string FileName { get; set; }
        public RichTemplateEditorData RichTemplateEditorData { get; set; }
        public int AllPlaceholderExist { get; set; }
        public short TeacherApprovalStatus { get; set; }


        public TemplateEdit()
        {
            TemplateFields = new List<TemplateField>();
            TemplateTypeList = new List<SelectListItem>();
            PeriodList = new List<SelectListItem>();
            //  CertifiedColumn = new List<SelectListItem>();
            RichTemplateEditorData = new RichTemplateEditorData();

            TemplateTypeList.Add(new SelectListItem { Text = "Select Template Type", Value = "" });
            TemplateTypeList.AddRange(Enum.GetValues(typeof(PlanTemplateTypes)).Cast<PlanTemplateTypes>().Select(r => new SelectListItem { Text = StringEnum.GetStringValue(r), Value = r.ToString() }));

            PeriodList.Add(new SelectListItem { Text = "Select Period", Value = "" });
            PeriodList.AddRange(Enum.GetValues(typeof(PlanPeriod)).Cast<PlanPeriod>().Select(r => new SelectListItem { Text = r.ToString(), Value = r.ToString() }));
        }
    }
}