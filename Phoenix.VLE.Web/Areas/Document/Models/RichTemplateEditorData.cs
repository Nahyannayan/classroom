﻿
namespace Phoenix.VLE.Web.EditModels
{
    public class RichTemplateEditorData
    {
        public string FilePath { get; set; }
        public int Status { get; set; }
        public bool IsViewOnly { get; set; }
    }
}