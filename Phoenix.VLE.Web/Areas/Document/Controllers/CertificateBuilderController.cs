﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Document.Controllers
{
    public class CertificateBuilderController : BaseController
    {

        private readonly IStudentCertificateService _studentCertificateService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly ITemplateService _templateService;
        private readonly ISchoolGroupService _schoolGroupService;

        public CertificateBuilderController(IStudentCertificateService studentCertificateService, IUserPermissionService userPermissionService,
                ITemplateService templateService, ISchoolGroupService schoolGroupService)
        {
            _studentCertificateService = studentCertificateService;
            _userPermissionService = userPermissionService;
            _templateService = templateService;
            _schoolGroupService = schoolGroupService;
        }
        // GET: Document/CertificateBuilder
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadCertificateGrid()
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var isCustomPermissionEnabled = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_StudentCertificate.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            IEnumerable<Template> templateList = new List<Template>();
            templateList = _templateService.GetTemplatesBySchoolId((int)SessionHelper.CurrentSession.SchoolId, null, true, Enum.GetName(typeof(PlanTemplateTypes), PlanTemplateTypes.Certificate), null);
            var dataList = new
            {
                aaData = (from item in templateList
                          select new
                          {
                              item.Title,
                              item.Description,
                              item.TemplateType,
                              Status = ResourceManager.GetString("Template.Template." + Enum.GetName(typeof(TemplateStatus), item.Status).Replace(" ", "")),
                              Actions = GetTemplateActionLinks(isCustomPermissionEnabled, item)
                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        private string GetTemplateActionLinks(bool isPermissionEnabled, Template model)
        {
            bool isPermissionAssigned = SessionHelper.CurrentSession.IsAdmin && CurrentPagePermission.CanEdit;
            string actions = string.Empty;
            actions +=
                (isPermissionAssigned ? $"<a data-toggle='tooltip' class='mr-2' href='/document/CertificateBuilder/AddEditCertificateDetail?id={EncryptDecryptHelper.EncryptUrl(model.TemplateId.ToString())}'  title='" + ResourceManager.GetString("Shared.Buttons.Edit") + "'><img src='/Content/vle/img/svg/tbl-edit.svg'/></a>"
              + (isPermissionAssigned && model.IsActive ? $" <a data-toggle='tooltip' href='javascript:void(0)' class='mr-2' onclick='certificate.deleteTemplate($(this),{model.TemplateId}); ' title='" + ResourceManager.GetString("Shared.Buttons.Delete") + "'><img src='/Content/vle/img/svg/tbl-delete.svg' /></a>" : string.Empty)
              : string.Empty);
            return actions;
        }

        public ActionResult AddEditCertificateDetail(string id)
        {
            TemplateEdit templateEdit = new TemplateEdit();
            if (!string.IsNullOrWhiteSpace(id))
            {
                var templateId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
                var templateDetail = _templateService.GetTemplateDetail(templateId, (int)SessionHelper.CurrentSession.SchoolId, null, null, null, null, true);
                EntityMapper<Template, TemplateEdit>.Map(templateDetail, templateEdit);
            }
            else
                templateEdit.IsAddMode = true;
            return View(templateEdit);
        }
        public ActionResult GetTemplateFieldByTemplateId(int templateId)
        {
            var model = _templateService.GetTemplateFieldByTemplateId(templateId,0,0, true);
            return PartialView("_TemplateField", model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AddEditCertificateDetail(TemplateEdit templateEdit)
        {
            var err = new OperationDetails();
            Template template = new Template();
            if (!SessionHelper.CurrentSession.IsAdmin)
            {

                err.Message = LocalizationHelper.ActionNotPermittedMessage;
                err.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(new { data = err, EditUrl = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            EntityMapper<TemplateEdit, Template>.Map(templateEdit, template);
            var result = 0;
            template.SchoolId = SessionHelper.CurrentSession.SchoolId;
            template.CreatedBy = SessionHelper.CurrentSession.Id;
            template.IsActive = true;
            template.Status = (int)TemplateStatus.Pending;
            template.Period = "Daily";
            template.TemplateType = Enum.GetName(typeof(PlanTemplateTypes), PlanTemplateTypes.Certificate);
            if (templateEdit.IsAddMode)
            {
                result = _templateService.Insert(template);
            }
            else
            {
                template.UpdatedBy = SessionHelper.CurrentSession.Id;
                result = _templateService.Update(template);
            }
            var success = result > 0;
            var op = new OperationDetails(success);
            op.InsertedRowId = result;
            return Json(new { data = op, EditUrl = EncryptDecryptHelper.EncryptUrl(result) }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AssignCertificateFields(string id)
        {
            int templateId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            ViewBag.TemplateId = templateId;
            ViewBag.FieldsList = SelectListHelper.GetSelectListData(ListItems.CertificateColumns);
            Template template = _templateService.GetTemplateDetail(templateId, (int)SessionHelper.CurrentSession.SchoolId, null, null, null, null, true);
            return View(template);
        }

        [HttpPost]
        public ActionResult SaveCertificateAssignedColumns(string jsonString)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }

            List<TemplateField> templateFieldList = JsonConvert.DeserializeObject<List<TemplateField>>(jsonString);
            bool result = _templateService.SaveCertificateAssignedColumns(templateFieldList);
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CertificateBuilder(string id)
        {
            int templateId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            var templateDetail = _templateService.GetTemplateDetail(templateId, (int)SessionHelper.CurrentSession.SchoolId, StringEnum.GetStringValue(PlanTemplateTypes.Certificate), null, null, null, true);
            string baseUri = GetBaseUrl();
            templateDetail.JSONFileUrl = baseUri + "/pixie/";
            templateDetail.FilePath = PhoenixConfiguration.Instance.ReadFilePath + "/" + templateDetail.FilePath;
            return View(templateDetail);
        }

        private string GetBaseUrl()
        {
            var request = Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            var scheme = Request.IsLocal ? "http" : "https";

            if (appUrl != "/")
                appUrl = "/" + appUrl;

            var baseUrl = string.Format("{0}://{1}", scheme, request.Url.Authority);

            return baseUrl; //this will return complete url
        }
        [HttpPost]
        public ActionResult SaveCertificate(string fileName, string data, int templateId)
        {
            var permission = CurrentPagePermission.CanEdit;
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var templateDetail = _templateService.GetTemplateDetail(templateId, (int)SessionHelper.CurrentSession.SchoolId, null, null, null, null, true);
            var canvas = (JObject)JsonConvert.DeserializeObject(data);
            var canvasObjectList = canvas["canvas"]["objects"].Children<JObject>().Where(e => e["type"] != null && e["type"].ToString() == "i-text").ToList();
            List<string> certificateFieldsAdded = canvasObjectList.Select(e => e["text"].ToString()).ToList();
            var missedFields = templateDetail.TemplateFields.Select(e => e.Placeholder).Except(certificateFieldsAdded);
            if (certificateFieldsAdded.Count < templateDetail.TemplateFields.Count || missedFields.Count() > 0)
            {
                var missingFields = certificateFieldsAdded.Count == 0 ? string.Join(",", templateDetail.TemplateFields.Select(e => e.Placeholder)) :
                     string.Join(",", missedFields);
                op.Message = string.Format(ResourceManager.GetString("Template.TemplateField.CertificateMissingFieldMsg"), missingFields);
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var fileSavePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.TemplateCertificateDir + "/User_" + SessionHelper.CurrentSession.Id;
            Common.Helpers.CommonHelper.CreateDestinationFolder(fileSavePath);
            bool result = false;
            try
            {
                System.IO.File.WriteAllText(fileSavePath + "/" + fileName, data);
                var templateData = new TemplateEdit()
                {
                    TemplateId = templateId,
                    CreatedBy = SessionHelper.CurrentSession.Id,
                    FilePath = Constants.TemplateCertificateDir + "/User_" + SessionHelper.CurrentSession.Id + "/" + fileName,
                    FileName = fileName
                };
                result = _templateService.SaveTemplateImageData(templateData);
            }
            catch { }
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteCertificateTemplate(int templateId)
        {
            var op = new OperationDetails();
            if (!SessionHelper.CurrentSession.IsAdmin)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            bool result = _templateService.Delete(templateId);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCertificateColumns()
        {
            return Json(new SelectList(_templateService.GetCertificateColumns((int)TemplateColumnTypes.Certificate), "CertificateColumnId", "ColumnName"), JsonRequestBehavior.AllowGet);
        }
    }
}