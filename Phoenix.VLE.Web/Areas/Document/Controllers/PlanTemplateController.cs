﻿using Phoenix.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.VLE.Web.Services;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.Common.Enums;
using DevExpress.Web.Mvc;
using DevExpress.Web;
using DevExpress.XtraRichEdit;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using System.Text.RegularExpressions;
using Phoenix.Common.Localization;
using Phoenix.Models.Entities;
using System.Threading;

namespace Phoenix.VLE.Web.Areas.Document.Controllers
{
    public class PlanTemplateController : BaseController
    {
        private readonly ITemplateService _templateService;
        private readonly IUserPermissionService _userPermissionService;
        public PlanTemplateController(ITemplateService templateService, IUserPermissionService userPermissionService)
        {
            _templateService = templateService;
            _userPermissionService = userPermissionService;
        }

        // GET: Document/Template
        public ActionResult Index()
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_PlanTemplate.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            return View();
        }

        public ActionResult LoadTemplateGrid()
        {
            var templates = _templateService.GetTemplatesBySchoolId((int)SessionHelper.CurrentSession.SchoolId, null, true,PlanTemplateTypes.SOW.ToString(),null);
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var CustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_PlanTemplate.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var dataList = new
            {
                aaData = (from item in templates
                          select new
                          {
                              item.Title,
                              item.Description,
                              item.TemplateType,
                              item.Period,
                              Status = ResourceManager.GetString("Template.Template." + Enum.GetName(typeof(TemplateStatus), item.Status).Replace(" ", "")),
                              Actions = GetPlanTemplateGridActionLink(CustomPermission,item)

                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        private string GetPlanTemplateGridActionLink(bool isCustomPermission, Template item)
        {
            string actionLink = string.Empty;

            if (!item.IsPlanTemplateDeleteAble)
            {                
                actionLink = (isCustomPermission ?  $@"<a class='mr-2' data-toggle='tooltip' href='/document/plantemplate/addedittemplatedetail?id={EncryptDecryptHelper.EncryptUrl(item.TemplateId.ToString())}' title='{ResourceManager.GetString("Shared.Buttons.Edit")}'><img src='/Content/vle/img/svg/tbl-edit.svg'/></a>"
                    + $"<a class='mr-2' data-toggle='tooltip' href='javascript:void(0)' onclick='template.deleteTemplate($(this),{item.TemplateId});' title='{ResourceManager.GetString("Shared.Buttons.Delete")}'> <img src='/Content/vle/img/svg/tbl-delete.svg' /></a>": "");
            }
            else
            {
                actionLink = (isCustomPermission ? $@"<a class='mr-2' data-toggle='tooltip' href='/document/plantemplate/addedittemplatedetail?id={EncryptDecryptHelper.EncryptUrl(item.TemplateId.ToString())}' title='{ResourceManager.GetString("Shared.Buttons.Edit")}'><img src='/Content/vle/img/svg/tbl-edit.svg'/></a>" : "");
            }

            return actionLink;
        }

        public ActionResult AddEditTemplateDetail(string id)
        {
            TemplateEdit templateEdit = new TemplateEdit();
            Session["TemplateFields"] = null;
            if (!string.IsNullOrWhiteSpace(id))
            {
                var templateId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
                var templateDetail = _templateService.GetTemplateDetail(templateId, (int)SessionHelper.CurrentSession.SchoolId, null, null, null, null, true);
                EntityMapper<Template, TemplateEdit>.Map(templateDetail, templateEdit);
                Session["TemplateFields"] = templateEdit;
            }
            else
            { 
                templateEdit.IsAddMode = true;
            }

            //To remove certificate type from drop downlist in template
            templateEdit.TemplateTypeList.RemoveAt(3);
            ViewBag.CertifiedColumnList = _templateService.GetCertificateColumns((int)TemplateColumnTypes.LessonPlan);
            //var ColumnList = _templateService.GetCertificateColumns();
            ViewBag.CertifiedColumnList = new SelectList(_templateService.GetCertificateColumns((int)TemplateColumnTypes.LessonPlan), "CertificateColumnId", "ColumnName");
            return View(templateEdit);
        }

        public ActionResult GetCertificateColumns()
        {
            return Json(new SelectList(_templateService.GetCertificateColumns((int)TemplateColumnTypes.LessonPlan), "CertificateColumnId", "ColumnName"), JsonRequestBehavior.AllowGet);
        }
        [HttpPost,ValidateAntiForgeryToken]
        public ActionResult AddEditTemplateDetail(TemplateEdit templateEdit)
        {
            Template template = new Template();
            if(!ModelState.IsValid)
            {
                //return View(templateEdit);
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            EntityMapper<TemplateEdit, Template>.Map(templateEdit, template);
            var result = 0;
            template.SchoolId = SessionHelper.CurrentSession.SchoolId;
            template.CreatedBy = SessionHelper.CurrentSession.Id;
            template.IsActive = true;
            if (template.TemplateId  == 0)
            {
                template.Status = (int)TemplateStatus.New;
                result = _templateService.Insert(template);
                return Json(new OperationDetails() { Message = Phoenix.Common.Localization.LocalizationHelper.GetResourceText("Shared.Messages.Added"), Success = (result > 0), NotificationType = "success", InsertedRowId = result, Identifier = EncryptDecryptHelper.EncryptUrl(result) }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                template.UpdatedBy = SessionHelper.CurrentSession.Id;
                result = _templateService.Update(template);
                return Json(new OperationDetails() { Message = Phoenix.Common.Localization.LocalizationHelper.GetResourceText("Shared.Messages.Updated"), Success = (result > 0), NotificationType = "success", InsertedRowId = result, Identifier = EncryptDecryptHelper.EncryptUrl(result) }, JsonRequestBehavior.AllowGet);
            }
   
        }

        public ActionResult GetTemplateFieldByTemplateId(int templateId)
        {
            var model = _templateService.GetTemplateFieldByTemplateId(templateId,0,0, true);
            return PartialView("_TemplateField", model);
        }

        [HttpPost]
        public ActionResult DeleteTemplate(int id)
        {
            var result = _templateService.Delete(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CompleteTemplateProcess(int templateId)
        {
            var template = _templateService.GetById(templateId);
            template.Status = (int)TemplateStatus.Approved;
            template.UpdatedBy = SessionHelper.CurrentSession.Id;

            var result = _templateService.Update(template);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TemplateEditor(string id)
        {
            TemplateEdit templateEdit = new TemplateEdit();
            var templateId = !string.IsNullOrWhiteSpace(id) ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id)) : 0;
            if(templateId == 0)
            {
                return RedirectToAction("Index");
            }


            string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
            string templateContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.TemplateDir;
            string schoolTemplate = PhoenixConfiguration.Instance.WriteFilePath + Constants.TemplateDir + "/School_" + SessionHelper.CurrentSession.SchoolId;
            string schoolMasterFolder = PhoenixConfiguration.Instance.WriteFilePath + Constants.TemplateDir + "/School_" + SessionHelper.CurrentSession.SchoolId + "/Templates";
            CommonHelper.CreateDestinationFolder(resourceDir);
            CommonHelper.CreateDestinationFolder(templateContent);
            CommonHelper.CreateDestinationFolder(schoolTemplate);
            CommonHelper.CreateDestinationFolder(schoolMasterFolder);


            var templateDetail = _templateService.GetTemplateDetail(templateId, (int)SessionHelper.CurrentSession.SchoolId, null, null, null, null, true);

            if ((string.IsNullOrWhiteSpace(templateDetail.FileName) && string.IsNullOrWhiteSpace(templateDetail.FilePath)) || !System.IO.File.Exists(Path.Combine(schoolMasterFolder, templateDetail.FileName)))
            {
                templateDetail.FileName = templateDetail.Title.Replace(" ", "_") + ".docx";
                templateDetail.FilePath = Constants.TemplateDir + "/School_" + SessionHelper.CurrentSession.SchoolId + "/Templates/" + templateDetail.FileName;
                string filePath = PhoenixConfiguration.Instance.WriteFilePath + templateDetail.FilePath;
                byte[] docBytes = RichEditExtension.SaveCopy("DemoRichEdit", DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                //System.IO.File.WriteAllBytes(filePath, docBytes);
                using (MemoryStream m = new MemoryStream(docBytes, 0, docBytes.Length))
                {
                    m.Position = 0;
                    RichEditDocumentServer r = new RichEditDocumentServer();
                    r.LoadDocument(m, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                    r.SaveDocument(filePath, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
                }

                var result = _templateService.Update(templateDetail);
            }
            EntityMapper<Template, TemplateEdit>.Map(templateDetail, templateEdit);

            templateEdit.RichTemplateEditorData.FilePath = templateEdit.FilePath;
            string docText = null;
            templateEdit.AllPlaceholderExist = 1;

            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(PhoenixConfiguration.Instance.WriteFilePath + templateDetail.FilePath, true))
            {

                using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    docText = sr.ReadToEnd();
                }

                //using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.OpenOrCreate, FileAccess.ReadWrite)))
                //{
                //    sw.Write(docText);
                //}
            }

            foreach (var item in templateEdit.TemplateFields)
            {
                if (!docText.Contains(item.Placeholder))
                {
                    templateEdit.AllPlaceholderExist = 0;
                    break;
                }
            }

            return View(templateEdit);
        }

        public ActionResult CustomToolbarPartial(RichTemplateEditorData model)
        {
            return PartialView("TemplateCustomToolbar", model);
        }

    }
}