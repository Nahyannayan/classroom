﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Document.Controllers
{
    public class CertificateController : BaseController
    {

        private readonly IStudentCertificateService _studentCertificateService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly ITemplateService _templateService;
        private readonly ISchoolGroupService _schoolGroupService;

        public CertificateController(IStudentCertificateService studentCertificateService, IUserPermissionService userPermissionService,
                ITemplateService templateService, ISchoolGroupService schoolGroupService)
        {
            _studentCertificateService = studentCertificateService;
            _userPermissionService = userPermissionService;
            _templateService = templateService;
            _schoolGroupService = schoolGroupService;
        }
        // GET: Document/Certificate
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadCertificateGrid()
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var isCustomPermissionEnabled = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_StudentCertificate.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            IEnumerable<Template> templateList = new List<Template>();
            templateList = _templateService.GetTemplatesBySchoolId((int)SessionHelper.CurrentSession.SchoolId, Convert.ToInt32(SessionHelper.CurrentSession.Id), true, Enum.GetName(typeof(PlanTemplateTypes), PlanTemplateTypes.Certificate), null);
            var dataList = new
            {
                aaData = (from item in templateList
                          select new
                          {
                              item.Title,
                              item.Description,
                              item.TemplateType,
                              Status = ResourceManager.GetString("Template.Template." + Enum.GetName(typeof(TemplateStatus), item.TeacherApprovalStatus).Replace(" ", "")),
                              Actions = GetTemplateActionLinks(isCustomPermissionEnabled, item)
                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult LoadSchoolGroups(int pageIndex = 1, string searchString = "", bool isBeSpokeGroup = false)
        {
            var model = new Pagination<Phoenix.Models.SchoolGroup>();
            var oldSearchString = searchString;
            searchString = searchString.Replace(" /", "_");
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var teacherGroups = _schoolGroupService.GetSchoolGroups(pageIndex, 6, (isBeSpokeGroup ? 1 : 0), searchString).Result.ToList();
                SynchronizationContext.SetSynchronizationContext(syncContext);

                if (teacherGroups.Any())
                    model = new Pagination<Phoenix.Models.SchoolGroup>(pageIndex, 6, teacherGroups, teacherGroups.FirstOrDefault().TotalCount);
                model.RecordCount = teacherGroups.Count == 0 ? 0 : teacherGroups[0].TotalCount;
            }
            model.LoadPageRecordsUrl = "/Document/Certificate/LoadSchoolGroups?pageIndex={0}&isBeSpokeGroup=" + isBeSpokeGroup;
            model.SearchString = oldSearchString;
            return PartialView("_TeacherSchoolGroups", model);
        }
        private string GetTemplateActionLinks(bool isPermissionEnabled, Template model)
        {
            string actions = string.Empty;
            actions +=  (model.CanApproveTemplate & model.IsStudentAssigned && model.StudentAssignedBy != SessionHelper.CurrentSession.Id ? $"<a class='mr-2' href='javascript:void(0)' data-toggle='tooltip'  data-original-title='" + ResourceManager.GetString("Template.Template.UpdateTemplateStatus") + $"' onclick='certificate.initTemplateStatusForm(this, {model.TemplateId})'><img src='/Content/vle/img/svg/pencil-big.svg' /></a>" : string.Empty)
               + (isPermissionEnabled && (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin) ? $"<a class='mr-2' href='javascript:void(0)' data-toggle='tooltip'  data-original-title='" + ResourceManager.GetString("Template.Template.AssignStudents") + $"' onclick='certificate.assignStudents(this, {model.TemplateId})'><img src='/Content/vle/img/svg/tbl-settings.svg' /></a>" : string.Empty)
                + $"<a class='mr-2' href='javascript:void(0)' data-toggle='tooltip' data-original-title='{ ResourceManager.GetString("Template.Template.PreviewCertificateTemplate")} ' onclick=certificate.previewCertificate(this) data-id='{EncryptDecryptHelper.EncryptUrl(model.TemplateId)}'><img src='/Content/vle/img/svg/tbl-view.svg'/></button>";
            return actions;
        }

        public ActionResult InitTemplateUpdateStatusForm(int templateId)
        {
            ViewBag.TemplateStatus = new List<Common.Models.ListItem>() {
                new Common.Models.ListItem{ ItemId = Convert.ToInt16(TemplateStatus.Pending).ToString(), ItemName = ResourceManager.GetString("Template.Template.Pending")},
                new Common.Models.ListItem{ ItemId = Convert.ToInt16(TemplateStatus.Approved).ToString(), ItemName = ResourceManager.GetString("Template.Template.Approved")},
                new Common.Models.ListItem{ ItemId = Convert.ToInt16(TemplateStatus.Rejected).ToString(), ItemName = ResourceManager.GetString("Template.Template.Rejected")}
            };
            var editModel = new TemplateEdit();
            var template = _templateService.GetTemplateDetail(templateId, (int)SessionHelper.CurrentSession.SchoolId, null, null, SessionHelper.CurrentSession.Id.ToInteger(), null, true);
            EntityMapper<Template, TemplateEdit>.Map(template, editModel);
            return PartialView("_FormTemplateStatus", editModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveTemplateCertificateStatus(TemplateEdit model)
        {
            var op = new OperationDetails();
            var template = _templateService.GetTemplateDetail(model.TemplateId.ToInteger(), (int)SessionHelper.CurrentSession.SchoolId, StringEnum.GetStringValue(PlanTemplateTypes.Certificate), null, SessionHelper.CurrentSession.Id.ToInteger(), null, true);
            bool canApproveTemplate = template.CanApproveTemplate;
            if (!(canApproveTemplate && template.StudentAssignedBy != SessionHelper.CurrentSession.Id && template.IsStudentAssigned))
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            model.UpdatedBy = SessionHelper.CurrentSession.Id;
            bool result = _templateService.SaveTemplateCertificateStatus(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssignStudents(int templateId)
        {
            const int maxLength = 50;
            var schoolGroups = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            ViewBag.SchoolGroups = schoolGroups.Select(i => new Phoenix.Common.Models.ListItem
            {
                ItemId = i.SchoolGroupId.ToString(),
                ItemName = !string.IsNullOrEmpty(i.TeacherGivenName) ? (i.TeacherGivenName.Length > maxLength ? i.TeacherGivenName.Substring(0, maxLength) + "..." : i.TeacherGivenName) : (i.SchoolGroupDescription.Length > maxLength ? i.SchoolGroupDescription.Substring(0, maxLength) + "..." : i.SchoolGroupDescription)
            });
            ViewBag.TemplateId = templateId;
            return PartialView("_CertificateStudentMapping");
        }

        public ActionResult GetTeacherGroups(int pageIndex = 1, string searchString = "")
        {
            int pageSize = 9;
            if (pageIndex == 0) pageIndex = 1;
            int skipItemsCount = (pageIndex - 1) * 9;
            var schoolGroups = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            schoolGroups = schoolGroups.Where(e => e.SchoolGroupName.ToLower().Contains(searchString.ToLower())).ToList();
            var model = new Pagination<Phoenix.Models.SchoolGroup>();
            if (schoolGroups.Any())
                model = new Pagination<Phoenix.Models.SchoolGroup>(pageIndex, pageSize, schoolGroups.Skip(skipItemsCount).Take(9).ToList(), schoolGroups.Count);
            model.RecordCount = schoolGroups.Count == 0 ? 0 : schoolGroups[0].TotalCount;
            model.LoadPageRecordsUrl = "/Document/Certificate/GetTeacherGroups?pageIndex={0}&searchString=" + searchString;
            model.SearchString = searchString;
            return PartialView("_TeacherSchoolGroups", model);
        }

        public ActionResult GetSchoolGroupStudents(int templateId, string schoolGroupIds, int pageIndex = 1, string searchString = "")
        {
            var model = new Pagination<Student>();
            var studentList = _templateService.GetCertificateMappingStudents((int)SessionHelper.CurrentSession.Id, pageIndex, 16, templateId, schoolGroupIds, searchString);
            if (studentList.Any())
            {
                model = new Pagination<Student>(pageIndex, 16, studentList.ToList(), studentList.First().TotalCount);
            }
            model.LoadPageRecordsUrl = "/Document/Certificate/GetSchoolGroupStudents?pageIndex={0}&schoolGroupIds=" + schoolGroupIds + "&searchString=" + searchString
                    + "&templateId=" + templateId;
            model.SearchString = searchString;
            model.PageIndex = pageIndex;
            var template = _templateService.GetTemplateDetail(templateId, (int)SessionHelper.CurrentSession.SchoolId, null, null, SessionHelper.CurrentSession.Id.ToInteger(), null, true);
            ViewBag.CanUpdateStudents = !(template.IsStudentAssigned && (template.TeacherApprovalStatus == (short)TemplateStatus.Approved || template.TeacherApprovalStatus == (short)TemplateStatus.Rejected));
            return PartialView("_CertificateStudents", model);
        }

        [HttpPost]
        public ActionResult SaveTemplateStudentMapping(StudentCertificateMapping model)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            bool result = _templateService.SaveTemplateStudentMapping(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveCertificateApprovalTeacher(string teacherIds, int templateId)
        {
            var op = new OperationDetails();
            if (!CurrentPagePermission.CanEdit)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                return Json(op, JsonRequestBehavior.AllowGet);
            }

            var model = new CertificateTeacherMapping() { TeacherUserIds = teacherIds, TemplateId = templateId };
            bool result = _templateService.SaveCertificateApprovalTeacher(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        private string GetBaseUrl()
        {
            var request = Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            var scheme = Request.IsLocal ? "http" : "https";
            if (appUrl != "/")
                appUrl = "/" + appUrl;

            var baseUrl = string.Format("{0}://{1}",
       scheme, //request.Url.Scheme gives https or http
        request.Url.Authority //request.Url.Authority gives qawithexperts/com
         ); //appUrl = /questions/111/ok-this-is-url

            return baseUrl; //this will return complete url
        }

        public ActionResult PreviewCertificateTemplate(string id)
        {
            int templateId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            var template = _templateService.GetTemplateDetail(templateId, (int)SessionHelper.CurrentSession.SchoolId, StringEnum.GetStringValue(PlanTemplateTypes.Certificate), string.Empty, null, true, true);
            if(template != null)
            {
                template.FilePath = PhoenixConfiguration.Instance.ReadFilePath + "/" + template.FilePath;
                string baseUri = GetBaseUrl();
                template.JSONFileUrl = baseUri + "/pixie/";

                IEnumerable<TemplateFieldMapping> mappingList = new List<TemplateFieldMapping>();
                mappingList = _templateService.GetTemplatePreviewData(templateId, SessionHelper.CurrentSession.SchoolId);
                if (mappingList.Count() > 0)
                {                    
                    string certificateFilePath = PhoenixConfiguration.Instance.WriteFilePath + mappingList.FirstOrDefault().FileName;
                    
                    string previewFilePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.TemplateCertificatePreviewDir;
                    Common.Helpers.CommonHelper.CreateDestinationFolder(previewFilePath);
                    previewFilePath += "/User_" + SessionHelper.CurrentSession.Id;

                    Common.Helpers.CommonHelper.CreateDestinationFolder(previewFilePath);
                    string json = System.IO.File.Exists(certificateFilePath) ? System.IO.File.ReadAllText(certificateFilePath) : "";
                    foreach (var item in mappingList)
                    {
                        json = json.Replace(item.PlaceHolder, item.ColumnData);
                    }
                    template.FilePath = PhoenixConfiguration.Instance.ReadFilePath + Constants.TemplateCertificatePreviewDir + "/User_" + SessionHelper.CurrentSession.Id + "/" + template.FileName;

                    Common.Helpers.CommonHelper.DeleteFile(previewFilePath + "/" + template.FileName);
                    System.IO.File.WriteAllText(previewFilePath + "/" + template.FileName, json);
                }
            }            
            
            return View("_PreviewTemplateCertificate", template);
        }

        public ActionResult GenerateCertificateReport()
        {
            // var certificateList = _templateService.GetCertificateReportData();
            string filePath = Server.MapPath("~/Content/Files/CertificateReportFile.xls");
            byte[] bytes = _templateService.GenerateCertificateReportFile(filePath);
            return File(bytes, "application/vnd.ms-excel", $"CertificateReport_{DateTime.Now.Ticks}.xls");
        }
    }
}