﻿using Phoenix.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Phoenix.VLE.Web.Services;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.Common.Enums;
using DocumentFormat.OpenXml.Packaging;
using System.IO;
using System.Text.RegularExpressions;
using Phoenix.Common.Localization;
//using Phoenix.VLE.Web.Areas.Document.Models;
using System.Globalization;
using Phoenix.VLE.Web.ViewModels;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models.Entities;
using System.Web;
using System.Threading;
using WebHelper = Phoenix.VLE.Web.Helpers;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Areas.Document.Controllers
{
    public class PlanSchemeController : BaseController
    {
        private readonly IPlanSchemeDetailService _planSchemeDetailService;
        private readonly IExemplarWallServices _exemplarWallService;
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly ITemplateService _templateService;
        private readonly IUserService _userService;
        private readonly ISchoolService _schoolService;

        public PlanSchemeController(IPlanSchemeDetailService planSchemeDetailService,
            IExemplarWallServices exemplarWallService,
            IUserPermissionService userPermissionService,
            ISchoolGroupService schoolGroupService,
            ITemplateService templateService,
            IUserService userService,
            ISchoolService schoolService)
        {
            _exemplarWallService = exemplarWallService;
            _schoolService = schoolService;
            _schoolGroupService = schoolGroupService;
            _planSchemeDetailService = planSchemeDetailService;
            _userPermissionService = userPermissionService;
            _templateService = templateService;
            _userService = userService;
        }

        // GET: Document/PlanScheme
        public ActionResult Index()
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_LessonPlan.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            return View();
        }


        public ActionResult GetFilterPanelData()
        {
            var LessonPlanFilterModule = _planSchemeDetailService.GetLessonPlanFilterModule(SessionHelper.CurrentSession.SchoolId, SessionHelper.CurrentSession.Id);
            return PartialView("_FilterPanel", LessonPlanFilterModule);

        }

        #region Lesson Group
        public List<SelectListItem> GetSchoolGroupByUserId()
        {
            //var lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            var lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.IsTeacher()).OrderBy(x => x.SchoolGroupName);
            //var lstSchoolGroup = _planSchemeDetailService.GetUserGroups(SessionHelper.CurrentSession.Id);

            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = "Class Group",
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = "Home Tutor Group",
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = "Other Groups",
                Disabled = false
            });
            var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName.Trim().Length > 30 ? x.SchoolGroupName.Trim().Substring(0, 29) + "..." : x.SchoolGroupName.Trim(),
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
            }).ToList();
            return groupedOptions;
        }

        public ActionResult GetUnitStructure(string groupIds)
        {
            //groupIds = "279391";310877
            var UnitDetail = _planSchemeDetailService.GetUnitStructure(groupIds);
            var htmlString = ControllerExtension.RenderPartialViewToString(this, "_LessonGrouping", UnitDetail);
            var jsonResult = Json(new { Success = true, planHTML = htmlString }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        #endregion

        #region Share LessonPlan
        private void GetSharedPlanData(string planSchemeId)
        {
            var lstTeachers = _userService.GetTeachersListBySchoolId(SessionHelper.CurrentSession.SchoolId).ToList();

            PlanSchemeDetail planScheme = _planSchemeDetailService.GetPlanSchemeDetail(Convert.ToInt32(planSchemeId)
                , null, null, null, null, (int)SessionHelper.CurrentSession.Id, null).FirstOrDefault();
            if (planScheme.SelectedTeacherIds != null)
            {
                var SelectedTeacherIds = planScheme.SelectedTeacherIds.Split(',');
                List<Phoenix.Models.User> SelectedTeachers = new List<Phoenix.Models.User>();
                foreach (string id in SelectedTeacherIds)
                {
                    if (!string.IsNullOrEmpty(id) && id != ",")
                    {
                        long TeacherId = Convert.ToInt64(id);
                        if (SelectedTeachers.Where(x => x.Id == TeacherId).Count() == 0)
                        {
                            SelectedTeachers.Add(lstTeachers.Where(x => x.Id == TeacherId).FirstOrDefault());
                            lstTeachers.Remove(lstTeachers.Where(x => x.Id == TeacherId).FirstOrDefault());
                        }
                    }
                }
                lstTeachers.Remove(lstTeachers.Where(x => x.Id == SessionHelper.CurrentSession.Id).FirstOrDefault());
                ViewBag.lstTeachers = lstTeachers;
                ViewBag.lstSelectedTeachers = SelectedTeachers;
                ViewBag.SelectedTeacherIds = planScheme.SelectedTeacherIds;
                ViewBag.SharedCount = SelectedTeachers.Count();
            }
            else
            {
                ViewBag.SelectedTeacherIds = "";
                ViewBag.lstTeachers = lstTeachers;
                ViewBag.lstSelectedTeachers = new List<Phoenix.Models.User>();
                ViewBag.SharedCount = 0;
            }

        }

        public ActionResult LoadSharedPlanView(string planSchemeId)
        {
            GetSharedPlanData(planSchemeId);
            return PartialView("_ShareLessonPlan");
        }

        public ActionResult DeleteSharedLessonLink(string PlanSchemeId, string TeacherId)
        {
            PlanSchemeDetail planSchemeDetail = new PlanSchemeDetail();
            planSchemeDetail.PlanSchemeDetailId = Convert.ToInt64(PlanSchemeId);
            planSchemeDetail.SelectedTeacherIds = TeacherId;
            long planSchemeid = _planSchemeDetailService.UpdateSharedLessonPlanDetial(planSchemeDetail, SessionHelper.CurrentSession.Id, "delete");
            GetSharedPlanData(PlanSchemeId);
            var htmlString = ControllerExtension.RenderPartialViewToString(this, "_ShareLessonPlan");
            return Json(new { Success = planSchemeid > 0, planHTML = htmlString }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveSharedPlanView(string PlanSchemeId, string SelectedTeacherList)
        {
            PlanSchemeDetail planSchemeDetail = new PlanSchemeDetail();
            planSchemeDetail.PlanSchemeDetailId = Convert.ToInt64(PlanSchemeId);
            planSchemeDetail.SelectedTeacherIds = SelectedTeacherList;
            long planSchemeid = _planSchemeDetailService.UpdateSharedLessonPlanDetial(planSchemeDetail, (int)SessionHelper.CurrentSession.Id, "update");
            GetSharedPlanData(PlanSchemeId);
            var htmlString = ControllerExtension.RenderPartialViewToString(this, "_ShareLessonPlan");
            return Json(new { Success = planSchemeid > 0, planHTML = htmlString }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult LoadPlanGrid(string selectedSearchParameterId, string selectedGroupList, long PlanSchemeId, int pageIndex = 1, string searchString = "", string sortBy = "",
            bool? SharedWithMe = null, bool? CreatedByMe = null, string MISGroupId = null, string OtherGroupId = null,
            string CourseId = null, string GradeId = null, bool? IsSearchByName = null, bool? IsSortBySelected = null)
        {
            var PageSize = 8;
            var model = new Pagination<PlanSchemeViewModel>();
            var isCustomPermissionEnabled = true;
            //_userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_StudentCertificate.ToString());

            List<PlanSchemeDetail> planSchemeList = _planSchemeDetailService.GetPlanSchemeBySchoolId(
                (int)SessionHelper.CurrentSession.SchoolId, (int)SessionHelper.CurrentSession.Id, null, true, SharedWithMe, CreatedByMe, MISGroupId, OtherGroupId, CourseId, GradeId, pageIndex, PageSize, searchString, sortBy).ToList();

            var PlanSchemeEdits = ReturnPlanEditObject(planSchemeList, isCustomPermissionEnabled);

            PlanSchemeEdits = UpdateIsSharedWithMeProperty(PlanSchemeEdits);

            if (!string.IsNullOrEmpty(selectedGroupList) && selectedGroupList != null)
            {
                List<Phoenix.Models.Entities.LessonPlanFilterModule> selectedParameterList = new List<LessonPlanFilterModule>();
                if (selectedGroupList != null && selectedSearchParameterId != null)
                {
                    var filterCollectionText = selectedGroupList.Split(',');
                    var filterCollectionId = selectedSearchParameterId.Split(',');
                    for (int i = 0; i < filterCollectionText.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(filterCollectionText[i]))
                            selectedParameterList.Add(new LessonPlanFilterModule
                            {
                                Group = filterCollectionText[i],
                                Count = 0,
                                GroupId = Convert.ToInt64(filterCollectionId[i]),
                                Type = ""
                            });
                    }
                    Session["SelectedParameterList"] = selectedParameterList;
                }
            }
            else
            {
                Session["SelectedParameterList"] = null;
            }

            if (planSchemeList.Any())
                model = new Pagination<PlanSchemeViewModel>(pageIndex, PageSize, PlanSchemeEdits.ToList(), planSchemeList.FirstOrDefault().TotalCount);
            model.RecordCount = planSchemeList.Count() == 0 ? 0 : planSchemeList.Count();
            model.LoadPageRecordsUrl = "/Document/PlanScheme/LoadPlanGrid?selectedGroupList=" + selectedGroupList + "&PlanSchemeId=" + PlanSchemeId + "&pageIndex={0}" + "&searchString=" + searchString +
                "&sortBy=" + sortBy + "&SharedWithMe=" + SharedWithMe + "&CreatedByMe=" + CreatedByMe + "&MISGroupId=" + MISGroupId + "&OtherGroupId=" + OtherGroupId +
                "&CourseId=" + CourseId + "&GradeId=" + GradeId;
            model.SearchString = searchString;
            model.PageIndex = pageIndex;
            ViewBag.isCustomPermissionEnabled = isCustomPermissionEnabled;

            if (string.IsNullOrEmpty(searchString) && string.IsNullOrEmpty(sortBy) &&
                SharedWithMe == null && CreatedByMe == null && string.IsNullOrEmpty(CourseId) && string.IsNullOrEmpty(GradeId)
                && string.IsNullOrEmpty(MISGroupId) && string.IsNullOrEmpty(OtherGroupId))
            {
                return PartialView("_ListPlanScheme", model);
            }
            else if (pageIndex >= 1)
            {
                if (pageIndex == 1 && Convert.ToBoolean(IsSearchByName) && !string.IsNullOrEmpty(searchString))
                    return Json(new { Success = true, planHTML = RefreshSearchPanel(model) }, JsonRequestBehavior.AllowGet);
                else if (pageIndex == 1 && Convert.ToBoolean(IsSortBySelected) && !string.IsNullOrEmpty(sortBy))
                    return Json(new { Success = true, planHTML = RefreshSearchPanel(model) }, JsonRequestBehavior.AllowGet);
                return PartialView("_ListPlanScheme", model);
            }
            return PartialView("_ListPlanScheme", model);
        }

        private List<PlanSchemeViewModel> UpdateIsSharedWithMeProperty(List<PlanSchemeViewModel> planSchemeEdits)
        {
            foreach (var plan in planSchemeEdits)
            {
                if (plan.SharedLessonPlanTeacherList != null)
                {
                    foreach (var user in plan.SharedLessonPlanTeacherList)
                    {
                        if (user.Id == SessionHelper.CurrentSession.Id)
                        {
                            plan.IsSharedWithMe = true;
                        }
                    }
                }
            }
            return planSchemeEdits;
        }

        public List<PlanSchemeViewModel> ReturnPlanEditObject(List<PlanSchemeDetail> planSchemeList, bool isCustomPermissionEnabled)
        {
            //string PlanSchemeDetailIds = String.Join(",", planSchemeList.Select(x => x.PlanSchemeDetailId));
            //var SharedLessonPlanTeacherList= _planSchemeDetailService.GetSharedLessonPlanTeacherList(PlanSchemeDetailIds).ToList();

            List<PlanSchemeViewModel> PlanSchemeEdits = new List<PlanSchemeViewModel>();
            planSchemeList.ForEach(x =>
            {
                PlanSchemeEdits.Add(new PlanSchemeViewModel()
                {
                    SchemeId = x.PlanSchemeDetailId,
                    SchemeName = x.PlanSchemeName,
                    CreateDate = x.CreatedOn.Day.ToString() + "-" + x.CreatedOn.ToString("MMM", CultureInfo.InvariantCulture) + "-" + x.CreatedOn.Year.ToString(),
                    PlanTemplateName = x.TemplateName,
                    StringStatus = ResourceManager.GetString("Template.Template." + Enum.GetName(typeof(TemplateStatus), x.Status).Replace(" ", "")),
                    IntStatus = x.Status,
                    PreviewActionsUrl = GetPlanSchemeGridActionLink(isCustomPermissionEnabled, x, "Preview"),
                    EditActionUrl = GetPlanSchemeGridActionLink(isCustomPermissionEnabled, x, "Edit"),
                    CreatedByName = x.CreatedByName,
                    CreatedById = x.CreatedBy,
                    CanApprovePlan = x.CanApprovePlan,
                    SharedLessonPlanTeacherList = x.SharedLessonPlanTeacherList,
                    IsAdminApprovalRequired = x.IsAdminApprovalRequired
                });
            });
            return PlanSchemeEdits;
        }

        public string RefreshSearchPanel(Pagination<PlanSchemeViewModel> model)
        {
            var htmlString = ControllerExtension.RenderPartialViewToString(this, "_ListPlanScheme", model);
            return htmlString;
        }

        public ActionResult LoadPlanGrid1()
        {
            var planSchemeList = _planSchemeDetailService.GetPlanSchemeBySchoolId((int)SessionHelper.CurrentSession.SchoolId, (int)SessionHelper.CurrentSession.Id, null, true);
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var CustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_LessonPlan.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var dataList = new
            {
                aaData = (from item in planSchemeList
                          select new
                          {
                              item.PlanSchemeName,
                              Status = ResourceManager.GetString("Template.Template." + Enum.GetName(typeof(TemplateStatus), item.Status).Replace(" ", "")),
                              Actions = GetPlanSchemeGridActionLink(CustomPermission, item)

                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        private string GetPlanSchemeGridActionLink(bool isCustomPermission, PlanSchemeDetail item)
        {

            if (item.Status == (int)TemplateStatus.New && item.CreatedBy == SessionHelper.CurrentSession.Id)
            {
                //Edit, Delete and AssignForApproval
                return "<div class='tbl-actions'>" + (isCustomPermission ? $"<a href='/document/planscheme/planschemeeditor?id={EncryptDecryptHelper.EncryptUrl(item.PlanSchemeDetailId.ToString())}' class='table-action-icon-btn'  title='{ ResourceManager.GetString("Shared.Buttons.Edit")} '><i class='fas fa-pencil-alt btnEdit'></i></a> <button type='button' class='table-action-icon-btn' onclick='planDetail.deletePlan($(this),{item.PlanSchemeDetailId}); ' title='{ ResourceManager.GetString("Shared.Buttons.Delete")}'><i class='far fa-trash-alt btnDelete'></i></button> <button type class='table-action-icon-btn'  title='{ ResourceManager.GetString("Template.PlanScheme.AssignForApproval")}' onclick='planDetail.assignForApproval($(this), {item.PlanSchemeDetailId})'><i class='fas fa-users btnDelete'></i></button>" : "");
            }
            else if (!(item.Status == (int)TemplateStatus.New) && item.CanApprovePlan && item.CreatedBy != SessionHelper.CurrentSession.Id)
            {
                //Preview, UpdateStatus
                return "<div class='tbl-actions'>" + (isCustomPermission ? $"<a href='/document/planscheme/planschemeeditor?id={EncryptDecryptHelper.EncryptUrl(item.PlanSchemeDetailId.ToString())}' class='table-action-icon-btn'  title='{ResourceManager.GetString("Shared.Buttons.View")}'><i class='fas fa-eye btnEdit'></i></a>  <button type class='table-action-icon-btn'  title='" + ResourceManager.GetString("Shared.Buttons.View") + $"' onclick='planDetail.initPlanSchemeStatus($(this), {item.PlanSchemeDetailId})'><i class='fas fa-check btnDelete'></i></button>" : "");
            }
            else
            {
                //Preview
                return "<div class='tbl-actions'>" + (isCustomPermission ? $"<a href='/document/planscheme/planschemeeditor?id={EncryptDecryptHelper.EncryptUrl(item.PlanSchemeDetailId.ToString())}' class='table-action-icon-btn'  title='{ResourceManager.GetString("Shared.Buttons.View")}'><i class='fas fa-eye btnEdit'></i></a>" : "");
            }
        }

        //Start --> New Code
        private string GetPlanSchemeGridActionLink(bool isCustomPermission, PlanSchemeDetail item, string actionType)
        {
            //if (actionType == "Edit" && item.Status == (int)TemplateStatus.New && item.CreatedBy == SessionHelper.CurrentSession.Id)
            if (actionType == "Edit")
                return "";
            else if (actionType == "Edit" && item.CanApprovePlan)
                return "";
            //else if (actionType == "Delete" && item.Status == (int)TemplateStatus.New && item.CreatedBy == SessionHelper.CurrentSession.Id)
            else if (actionType == "Delete")
                return isCustomPermission ? "/document/planscheme/DeletePlan?id=" + EncryptDecryptHelper.EncryptUrl(item.PlanSchemeDetailId.ToString()) : "";
            else if (actionType == "Preview")
                return isCustomPermission ? "/document/planscheme/planschemeeditor?id=" + EncryptDecryptHelper.EncryptUrl(item.PlanSchemeDetailId.ToString()) + "&IsviewOnly=" + EncryptDecryptHelper.EncryptUrl(1) : "";
            else if (actionType == "SharePlaneScheme")
                return "";
            else if (actionType == "SendForApproval" && !item.CanApprovePlan)
                return isCustomPermission ? "/document/planscheme/AssignPlanSchemeForApproval?id=" + EncryptDecryptHelper.EncryptUrl(item.PlanSchemeDetailId.ToString()) : "";
            else if (actionType == "SendForApproval" && item.CanApprovePlan)
                return "";
            else
                return "";

        }
        //End --> New Code
        public ActionResult InitPlanSchemeStatusForm(int planSchemeId)
        {
            ViewBag.TemplateStatus = new List<Common.Models.ListItem>() {
                ///new Common.Models.ListItem{ ItemId = Convert.ToInt16(TemplateStatus.Pending).ToString(), ItemName = StringEnum.GetStringValue(TemplateStatus.Pending)},
                new Common.Models.ListItem{ ItemId = Convert.ToInt16(TemplateStatus.Approved).ToString(), ItemName = StringEnum.GetStringValue(TemplateStatus.Approved)},
                new Common.Models.ListItem{ ItemId = Convert.ToInt16(TemplateStatus.Rejected).ToString(), ItemName = StringEnum.GetStringValue(TemplateStatus.Rejected)}
            };
            var editModel = new PlanSchemeDetailEdit();
            var planScheme = _planSchemeDetailService.GetById(planSchemeId);
            EntityMapper<PlanSchemeDetail, PlanSchemeDetailEdit>.Map(planScheme, editModel);
            return PartialView("_PlanSchemeStatus", editModel);
        }

        public ActionResult SavePlanSchemeStatus(PlanSchemeDetailEdit model)
        {
            if (_schoolService.GetSchoolById(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId)).IsAdminApprovalForLessonPlan != true)
                model.Status = 1;
            PlanSchemeDetail planScheme = new PlanSchemeDetail();
            EntityMapper<PlanSchemeDetailEdit, PlanSchemeDetail>.Map(model, planScheme);
            planScheme.UpdatedBy = SessionHelper.CurrentSession.Id;
            var result = _planSchemeDetailService.UpdatePlanSchemeStatus(planScheme);

            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }

        //New to Pending 
        public ActionResult AssignPlanSchemeForApproval(int planSchemeDetailId)
        {
            PlanSchemeDetail planScheme = new PlanSchemeDetail();
            planScheme.PlanSchemeDetailId = planSchemeDetailId;
            planScheme.Status = (int)TemplateStatus.Pending;
            planScheme.UpdatedBy = SessionHelper.CurrentSession.Id;
            var result = _planSchemeDetailService.UpdatePlanSchemeStatus(planScheme);

            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }

        //Pending to Approve
        public ActionResult ChangePlanApproveStatus(int planSchemeDetailId)
        {
            PlanSchemeDetail planScheme = new PlanSchemeDetail();
            planScheme.PlanSchemeDetailId = planSchemeDetailId;
            planScheme.Status = (int)TemplateStatus.Approved;
            planScheme.UpdatedBy = SessionHelper.CurrentSession.Id;
            var result = _planSchemeDetailService.UpdatePlanSchemeStatus(planScheme);

            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddPlanSchemeDetail(string id)
        {
            var model = new PlanSchemeDetailEdit();
            var templateList = _templateService.GetTemplatesBySchoolId((int)SessionHelper.CurrentSession.SchoolId, null, true, PlanTemplateTypes.SOW.ToString(), (int)TemplateStatus.Approved);
            if (!string.IsNullOrWhiteSpace(id))
            {
                PlanSchemeDetail planScheme = _planSchemeDetailService.GetPlanSchemeDetail(Convert.ToInt32(id), null, null, null, null, (int)SessionHelper.CurrentSession.Id, null).FirstOrDefault();
                EntityMapper<PlanSchemeDetail, PlanSchemeDetailEdit>.Map(planScheme, model);
                Session["PlanSchemeId"] = id;
            }
            model.TemplateList.Add(new SelectListItem { Text = ResourceManager.GetString("Template.PlanScheme.SelectPlanTemplate"), Value = "", Selected = true });
            model.TemplateList.AddRange(templateList.Select(r => new SelectListItem { Text = r.Title, Value = r.TemplateId.ToString() }).OrderBy(r => r.Text));
            ViewBag.AssignedGroup = GetSchoolGroupByUserId();
            return View(model);
        }


        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPlanSchemeDetail(PlanSchemeDetailEdit planSchemeDetailEdit)
        {
            PlanSchemeDetail planSchemeDetail = new PlanSchemeDetail();
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            //planSchemeDetailEdit= UpdateGradeValue(planSchemeDetailEdit);
            //Pass SchoolId when Groups are selected
            if (planSchemeDetailEdit.GroupList != null && planSchemeDetailEdit.GroupList.Contains("null")) planSchemeDetailEdit.GroupList = null;
            if (planSchemeDetailEdit.GroupList == null)
                planSchemeDetailEdit.SchoolId = 0;
            else if (planSchemeDetailEdit.GroupList != null && planSchemeDetailEdit.GroupList.Length > 0)
                planSchemeDetailEdit.SchoolId = SessionHelper.CurrentSession.SchoolId;
            //Start Create file & filepath for plan scheme and update in model
            string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
            string templateContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.TemplateDir;
            string schoolTemplate = PhoenixConfiguration.Instance.WriteFilePath + Constants.TemplateDir + "/School_" + SessionHelper.CurrentSession.SchoolId;
            string planScheme = PhoenixConfiguration.Instance.WriteFilePath + Constants.TemplateDir + "/School_" + SessionHelper.CurrentSession.SchoolId + "/PlanSchemes/User_" + SessionHelper.CurrentSession.Id.ToString();

            CommonHelper.CreateDestinationFolder(resourceDir);
            CommonHelper.CreateDestinationFolder(templateContent);
            CommonHelper.CreateDestinationFolder(schoolTemplate);
            CommonHelper.CreateDestinationFolder(planScheme);

            planSchemeDetailEdit.FileName = planSchemeDetailEdit.PlanSchemeName.Replace(" ", "_") + ".docx";
            planSchemeDetailEdit.FilePath = Constants.TemplateDir + "/School_" + SessionHelper.CurrentSession.SchoolId + "/" + "/PlanSchemes/User_" + SessionHelper.CurrentSession.Id.ToString() + "/" + planSchemeDetailEdit.FileName;
            string filePath = PhoenixConfiguration.Instance.WriteFilePath + planSchemeDetailEdit.FilePath;
            //End 

            //Create replica of template document in plan scheme folder
            var template = _templateService.GetById((int)planSchemeDetailEdit.TemplateId);
            byte[] docBytes = System.IO.File.ReadAllBytes(PhoenixConfiguration.Instance.WriteFilePath + template.FilePath);
            System.IO.File.WriteAllBytes(filePath, docBytes);




            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(filePath, true))
            {
                string docText = null;
                using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    docText = sr.ReadToEnd();
                }
                foreach (var item in planSchemeDetailEdit.TemplateFields)
                {
                    Regex regexText = new Regex(item.Field);
                    docText = regexText.Replace(docText, string.IsNullOrEmpty(item.Placeholder) ? " " : item.Placeholder);
                }
                using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                {
                    sw.Write(docText);
                }
            }

            HttpPostedFileViewModel postedfile = new HttpPostedFileViewModel();
            FileInfo fileinfo = new FileInfo(filePath);

            postedfile.ContentLength = Convert.ToInt32(fileinfo.Length / 1000);
            postedfile.FileName = fileinfo.FullName;
            postedfile.ContentType = "docx";
            postedfile.InputStream = System.IO.File.OpenRead(filePath);
            SharePointFileView shv = new SharePointFileView();
            string SharePTUploadedFilePath = string.Empty;
            string ShareableLink = string.Empty;
            if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
            {
                WebHelper.AzureBlobStorageHelper azureHelper = new WebHelper.AzureBlobStorageHelper();
                await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                shv = await azureHelper.UploadFileSlicePerSliceAsync(FileModulesConstants.LessonPlanDocument, SessionHelper.CurrentSession.OldUserId.ToString(), postedfile,
               postedfile.ContentLength / (1024 * 1024) == 0 ? 5 : postedfile.ContentLength / (1024 * 1024));
            }
            else
            {
                WebHelper.SharePointHelper sh = new WebHelper.SharePointHelper();
                Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                shv = WebHelper.SharePointHelper.UploadFileSlicePerSlice(ref cx,
                    FileModulesConstants.LessonPlanDocument, SessionHelper.CurrentSession.OldUserId.ToString(), postedfile,
                    postedfile.ContentLength / (1024 * 1024) == 0 ? 5 : postedfile.ContentLength / (1024 * 1024));
            }
            SharePTUploadedFilePath = shv.SharepointUploadedFileURL;
            ShareableLink = shv.ShareableLink;

            EntityMapper<PlanSchemeDetailEdit, PlanSchemeDetail>.Map(planSchemeDetailEdit, planSchemeDetail);
            planSchemeDetail.IsActive = true;
            planSchemeDetail.Status = (int)TemplateStatus.New;
            planSchemeDetail.CreatedBy = (int)SessionHelper.CurrentSession.Id;
            planSchemeDetail.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            if (_schoolService.GetSchoolById(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId)).IsAdminApprovalForLessonPlan != true)
                planSchemeDetail.Status = 1;
            else
                planSchemeDetail.Status = 0;

            planSchemeDetail.SharePointUploadFilePath = SharePTUploadedFilePath;
            planSchemeDetail.ShareableLink = ShareableLink;
            var result = _planSchemeDetailService.Insert(planSchemeDetail);

            if (result > 0)
            {
                List<PlanSchemeField> planSchemeField = new List<PlanSchemeField>();
                foreach (var field in planSchemeDetailEdit.TemplateFields)
                {
                    planSchemeField.Add(new PlanSchemeField
                    {
                        FieldId = Convert.ToString(field.Field),
                        FieldValue = field.Placeholder,
                        PlanSchemeId = Convert.ToInt64(result)
                    });
                }
                _planSchemeDetailService.SavePlanSchemeFields(planSchemeField);
            }
            planSchemeDetailEdit.PlanSchemeDetailId = result;

            return Json(new OperationDetails() { Message = Phoenix.Common.Localization.LocalizationHelper.GetResourceText("Shared.Messages.Added"), Success = (result > 0), NotificationType = "success", InsertedRowId = result, Identifier = EncryptDecryptHelper.EncryptUrl(result.ToString()) }, JsonRequestBehavior.AllowGet);
        }

        private PlanSchemeDetailEdit UpdateGradeValue(PlanSchemeDetailEdit planSchemeDetailEdit)
        {
            if (string.IsNullOrEmpty(planSchemeDetailEdit.SelectedGrade)) return planSchemeDetailEdit;
            foreach (var fields in planSchemeDetailEdit.TemplateFields)
            {
                if ((planSchemeDetailEdit.SelectedGrade.Split(':')[0]).Trim() == fields.Field)
                {
                    fields.Placeholder = planSchemeDetailEdit.SelectedGrade.Split(':')[1];
                    break;
                }
            }
            return planSchemeDetailEdit;
        }

        [HttpPost]
        public ActionResult DeletePlan(int id)
        {

            var result = _planSchemeDetailService.Delete(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTemplateFieldByTemplateId(int templateId, int groupid)
        {
            var model = _templateService.GetTemplateFieldByTemplateId(templateId, Convert.ToInt32(SessionHelper.CurrentSession.Id), groupid, true);
            if (Session["PlanSchemeId"] != null && Convert.ToInt64(Session["PlanSchemeId"]) > 0)
            {
                List<PlanSchemeField> planSchemeField = _planSchemeDetailService.GetPlanSchemeFields(Convert.ToInt64(Session["PlanSchemeId"])).ToList();
                if (planSchemeField != null && planSchemeField.Count > 0)
                {
                    foreach (var item in planSchemeField)
                    {
                        foreach (var item1 in model)
                        {
                            if (item1.TemplateFieldId == Convert.ToInt64(item.FieldId))
                            {
                                item1.FieldValue = item.FieldValue;
                                break;
                            }
                        }
                    }

                }
                Session["PlanSchemeId"] = null;
            }
            //ViewBag.GetTimeTableList = GetTimeTableList(0, "06/04/2020");
            // if (groupid != 0) ViewBag.GetGradeList = GetGradeList(groupid);
            return PartialView("_TemplateFieldData", model);
        }

        public ActionResult GetTimeTableList(string UserId, string SelectDate)
        {
            UserId = SessionHelper.CurrentSession.Id.ToString();
            return Json(_templateService.GetTimeTableList(Convert.ToInt64(UserId), SelectDate).ToList(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetGradeList(string Groupid)
        {
            if (Groupid == "0") { return Json("", JsonRequestBehavior.AllowGet); }
            if (Groupid == "null") { return Json("", JsonRequestBehavior.AllowGet); }
            if (string.IsNullOrEmpty(Groupid)) { return Json("", JsonRequestBehavior.AllowGet); }
            if (!string.IsNullOrEmpty(Groupid) && Groupid.Contains(",")) { Groupid = Groupid.Substring(0, (Groupid.Length - 1)); }
            var result = _templateService.GetGradeList(Convert.ToInt64(Groupid), SessionHelper.CurrentSession.SchoolId).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PlanSchemeEditor(PlanSchemeDetailEdit planSchemeDetail, string id, string IsViewOnly = "0")
        {
            int ViewOnly = 0;
            if (IsViewOnly != "0")
                ViewOnly = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(IsViewOnly));
            if (planSchemeDetail.PlanSchemeDetailId == 0 && !string.IsNullOrWhiteSpace(id))
            {
                var psId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
                var psDetail = _planSchemeDetailService.GetPlanSchemeDetail(psId, null, null, null, null, (int)SessionHelper.CurrentSession.Id, null).FirstOrDefault();
                EntityMapper<PlanSchemeDetail, PlanSchemeDetailEdit>.Map(psDetail, planSchemeDetail);
            }

            planSchemeDetail.RichTemplateEditorData.FilePath = planSchemeDetail.FilePath;
            planSchemeDetail.RichTemplateEditorData.Status = planSchemeDetail.Status;
            planSchemeDetail.RichTemplateEditorData.IsViewOnly = Convert.ToBoolean(ViewOnly) ? true : false;
            return View(planSchemeDetail);
        }

        public ActionResult CustomToolbarPartial(RichTemplateEditorData model)
        {
            return PartialView("PlanCustomToolbar", model);
        }

        public ActionResult ApproveLessonPlan()
        {

            return View();
        }

        public ActionResult GetApproveLessonPlan()
        {
            var pendingPlanSchemeList = _planSchemeDetailService.GetPendingForApprovalLessonPlan(SessionHelper.CurrentSession.SchoolId);
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var CustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_PlanTemplate.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var dataList = new
            {
                aaData = (from item in pendingPlanSchemeList
                          select new
                          {
                              item.PlanSchemeName,
                              Status = ResourceManager.GetString("Template.Template." + Enum.GetName(typeof(TemplateStatus), item.Status).Replace(" ", "")),
                              Actions = GetPlanTemplateGridActionLink(CustomPermission, item)

                          }).ToArray()
            };

            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        private string GetPlanTemplateGridActionLink(bool isCustomPermission, PlanSchemeDetail item)
        {
            if (!item.CanApprovePlan)
            {
                return "<div class='tbl-actions d-flex align-items-center justify-content-center'>" + (isCustomPermission ? $"<button class='table-action-icon-btn border-0 bg-transparent viewLessonPlan' data-PlanDetailId={item.PlanSchemeDetailId} title='{ResourceManager.GetString("Shared.Buttons.View")}' ><i class='fas fa-eye btnEdit'></i></button>" +
                    $"<div class='custom-control custom-checkbox'> <input type='checkbox' id={item.PlanSchemeDetailId} class='approveLessonPlan custom-control-input' data-PlanDetailId={item.PlanSchemeDetailId}  class='approveLessonPlan' > <label class='custom-control-label mt-2' for={item.PlanSchemeDetailId}></label>" +
                    $"</div> <a class='btnRejectPlan ml-4 mb-2' data-planSchemeId={item.PlanSchemeDetailId}> <img  src='/Content/vle/img/svg/tbl-delete.svg' ></a></div> " : "");
            }
            else
            {
                return "<div class='tbl-actions d-flex align-items-center justify-content-center'>" + (isCustomPermission ? $"<button class='table-action-icon-btnborder-0 bg-transparent viewLessonPlan' data-PlanDetailId={item.PlanSchemeDetailId} title='{ResourceManager.GetString("Shared.Buttons.View")}'  ><i class='fas fa-eye btnEdit'></i></button>" : "");
            }
        }

        public ActionResult GetApproveStatusDetail(string Status, string Operation)
        {
            var result =
                 _exemplarWallService.GetApprovalStatusDetails(
                 SessionHelper.CurrentSession.SchoolId,
                 Operation, "LessonPlan", Status == "true" ? true : false
                 );
            return Json(new { Status = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RejectLessonPlan(string planSchemeDetailId)
        {
            PlanSchemeDetailEdit _planschemedetailEdit = new PlanSchemeDetailEdit();
            _planschemedetailEdit.PlanSchemeDetailId = Convert.ToInt64(planSchemeDetailId);
            _planschemedetailEdit.IsRejected = true;
            PlanSchemeDetail planScheme = new PlanSchemeDetail();
            EntityMapper<PlanSchemeDetailEdit, PlanSchemeDetail>.Map(_planschemedetailEdit, planScheme);
            planScheme.UpdatedBy = SessionHelper.CurrentSession.Id;
            var result = _planSchemeDetailService.Insert(planScheme);

            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }

    }
}