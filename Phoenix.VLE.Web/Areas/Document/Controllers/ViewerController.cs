﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Document.Controllers
{
    public class ViewerController : Controller
    {
        private readonly IFileService _fileService;
        private readonly IObservationService _observationService;
        private readonly IAssignmentService _assignmentService;
        private readonly IQuizQuestionsService _quizQuestionsService;
        private readonly IQuizService _quizService;
        private readonly IBlogService _blogService;
        private readonly IMyPlannerService _myPlannerService;
        private readonly IAttachmentService _attachmentService;
        private readonly ITemplateService _templateService;
        private readonly IPlanSchemeDetailService _planSchemeDetailService;
        public ViewerController(IFileService fileService
            , IObservationService observationService
            , IAssignmentService assignmentService
            , IQuizQuestionsService quizQuestionsService
            , IQuizService quizService
            , IBlogService blogService
            ,IMyPlannerService myPlannerService
            ,IAttachmentService attachmentService
            ,ITemplateService templateService
            ,IPlanSchemeDetailService planSchemeDetailService)
        {
            _fileService = fileService;
            _observationService = observationService;
            _assignmentService = assignmentService;
            _quizQuestionsService = quizQuestionsService;
            _quizService = quizService;
            _blogService = blogService;
            _myPlannerService = myPlannerService;
            _attachmentService = attachmentService;
            _templateService = templateService;
            _planSchemeDetailService = planSchemeDetailService;
        }
        // GET: Document/Viewer
        public ActionResult Index(string id, string module)
        {
            try
            {
                var fileTypesList = _fileService.GetFileTypes();
                var exceptDocExt = new[] { "pdf", "txt", "html", "htm", "csv", "ppt", "pptx" };

                var docExt = fileTypesList.Where(r => r.DocumentType.IndexOf("document", StringComparison.OrdinalIgnoreCase) >= 0 && !exceptDocExt.Any(e => e == r.Extension.ToLower())).Select(r => r.Extension.ToLower()).ToList();

                var videoExt = fileTypesList.Where(r => r.DocumentType.IndexOf("video", StringComparison.OrdinalIgnoreCase) >= 0).Select(r => r.Extension.ToLower()).ToList();

                var audioExt = fileTypesList.Where(r => r.DocumentType.IndexOf("audio", StringComparison.OrdinalIgnoreCase) >= 0).Select(r => r.Extension.ToLower()).ToList();

                var imgExt = fileTypesList.Where(r => r.DocumentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) >= 0 && !exceptDocExt.Any(e => e == r.Extension.ToLower())).Select(r => r.Extension.ToLower()).ToList();

                ViewBag.DocExt = docExt;
                ViewBag.VideoExt = videoExt;
                ViewBag.AudioExt = audioExt;
                ViewBag.ImageExt = imgExt;

                if (!string.IsNullOrWhiteSpace(id))
                {
                    var file = new Phoenix.Models.File();
                    
                    if (module == "file")
                    {
                        var IdValue = module.IndexOf("edit", StringComparison.CurrentCultureIgnoreCase) >= 0 ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));

                        file = _fileService.GetById(IdValue);
                    }
                    else if (module == "observation")
                    {
                        var IdValue = module.IndexOf("edit", StringComparison.CurrentCultureIgnoreCase) >= 0 ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));

                        file = _observationService.GetObservationFilebyObservationFileId(IdValue);
                    }
                    else if (module == "editObservation")
                    {
                        var fileName = EncryptDecryptHelper.DecryptUrl(id);
                        List<ObservationFile> lstfiles = Session["ObservationFiles"] as List<ObservationFile>;
                        var sessionFile = lstfiles.FirstOrDefault(e => e.FileName.ToUpper().Trim() == fileName.ToUpper().Trim());
                        file.FileName = sessionFile.FileName;
                        file.FilePath = sessionFile.UploadedFileName;
                    }
                    else if (module == "assignment")
                    {
                        var IdValue = module.IndexOf("edit", StringComparison.CurrentCultureIgnoreCase) >= 0 ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));

                        file = _assignmentService.GetAssignmentFilebyAssignmentFileId(IdValue);
                    }
                    else if (module == "editAssignment")
                    {
                        var fileName = EncryptDecryptHelper.DecryptUrl(id);
                        List<AssignmentFile> lstfiles = Session["AssignmentFiles"] as List<AssignmentFile>;
                        var sessionFile = lstfiles.FirstOrDefault(e => e.FileName.ToUpper().Trim() == fileName.ToUpper().Trim());
                        file.FileName = sessionFile.FileName;
                        file.FilePath = sessionFile.UploadedFileName;
                    }
                    else if (module == "addeditQuizQuestion")
                    {
                        var quizQuestionFileName = Convert.ToString(EncryptDecryptHelper.DecryptUrl(id));
                        List<QuizQuestionFiles> lstfiles = Session["QuizQuestionFiles"] as List<QuizQuestionFiles>;
                        var sessionFile = lstfiles.FirstOrDefault(e => e.FileName.ToUpper().Trim() == quizQuestionFileName.ToUpper().Trim());
                        file.FileName = sessionFile.FileName;
                        file.FilePath = sessionFile.UploadedFileName;
                    }
                    else if (module == "editQuizQuestion")
                    {
                        var quizQuestionFileId = Convert.ToString(EncryptDecryptHelper.DecryptUrl(id));
                        List<QuizQuestionFiles> lstfiles = Session["QuizQuestionFiles"] as List<QuizQuestionFiles>;
                        var sessionFile = lstfiles.FirstOrDefault(e => e.QuizQuestionFileId == Convert.ToInt32(quizQuestionFileId));
                        file.FileName = sessionFile.FileName;
                        file.FilePath = sessionFile.UploadedFileName;
                    }
                    else if (module == "editStudentQuizQuestion")
                    {
                        var fileId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
                        var QuizQuestionfile = _quizQuestionsService.GetFilesByQuizQuestionFileId(fileId);
                        file.FileName = QuizQuestionfile.FileName;
                        file.FilePath = QuizQuestionfile.UploadedFileName.Contains("/Resources") ? QuizQuestionfile.UploadedFileName : SessionHelper.CurrentSession.IsParent() ? QuizQuestionfile.FilePath : QuizQuestionfile.PhysicalPath;
                    }
                    else if (module == "editQuizFile")
                    {
                        var fileQuizName = EncryptDecryptHelper.DecryptUrl(id);
                        List<QuizFile> lstfiles = Session["QuizFiles"] as List<QuizFile>;
                        var sessionFile = lstfiles.FirstOrDefault(e => e.FileName.ToUpper().Trim() == fileQuizName.ToUpper().Trim());
                        file.FileName = sessionFile.FileName;
                        file.FilePath = sessionFile.UploadedFileName;
                    }
                    else if (module == "studentQuiz")
                    {
                        var fileId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
                        var QuizQuestionfile = _quizService.GetFileByQuizFileId(fileId);
                        file.FileName = QuizQuestionfile.FileName;
                        file.FilePath = QuizQuestionfile.UploadedFileName;
                    }
                    else if (module == "stdassignment")
                    {
                        var IdValue = module.IndexOf("edit", StringComparison.CurrentCultureIgnoreCase) >= 0 ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));

                        file = _assignmentService.GetStudentAssignmentFilebyStudentAsgFileId(IdValue);
                    }
                    else if (module == "taskfile")
                    {
                        var IdValue = module.IndexOf("edit", StringComparison.CurrentCultureIgnoreCase) >= 0 ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));

                        file = _assignmentService.GetTaskFile(IdValue);
                    }
                    else if (module == "studenttaskfile")
                    {
                        var IdValue = module.IndexOf("edit", StringComparison.CurrentCultureIgnoreCase) >= 0 ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));

                        file = _assignmentService.GetStudentTaskFile(IdValue);
                    }
                    else if (module.Equals("editCertificates", StringComparison.OrdinalIgnoreCase) || module.Equals("editAcademicFiles", StringComparison.OrdinalIgnoreCase))
                    {
                        var fileName = Convert.ToString(EncryptDecryptHelper.DecryptUrl(id));
                        file.FileName = Path.GetFileName(fileName);
                        file.FilePath = fileName;
                    }



                    else if (module == "blog")
                    {
                        var IdValue = module.IndexOf("edit", StringComparison.CurrentCultureIgnoreCase) >= 0 ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));

                        var blog = _blogService.Get(IdValue);
                        if (blog != null)
                        {
                            //file.FileName = blog.BlogImage.Substring(blog.BlogImage.LastIndexOf('/') + 1);
                            file.FileName = string.Concat(blog.Title.Replace(" ", "_"), blog.BlogImage.Substring(blog.BlogImage.LastIndexOf('.')));
                            file.FilePath = blog.BlogImage;
                        }
                    }

                    else if (module == "myPlanner")
                    {
                        var IdValue = module.IndexOf("edit", StringComparison.CurrentCultureIgnoreCase) >= 0 ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));

                        var eventDeatils = _myPlannerService.GetEventById(IdValue).ToList();
                        if (eventDeatils != null)
                        {
                            file.FileName = eventDeatils.Select(x => x.FileName).Single();
                            file.FilePath = eventDeatils.Select(x => x.ResourceFile).Single();
                        }
                    }

                    else if (module.Equals("coursefiles", StringComparison.OrdinalIgnoreCase))
                    {
                        var fileId = Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(id));
                        var attachfile = _attachmentService.GetAttachment(attachmentId: fileId);
                        if (attachfile != null)
                        {
                            var filename = Path.GetFileName(attachfile.AttachmentPath);
                            file.FileName = filename.Split('_')[1];
                            file.FilePath = attachfile.AttachmentPath;
                        }
                    }
                    //Added by shankar on 07/17/2020 to preview selected template format
                    else if (module.Equals("template", StringComparison.OrdinalIgnoreCase))
                    {
                        var TemplateId = Convert.ToInt32(id);
                        var attachfile = _templateService.GetTemplateDetail(TemplateId, null, "", "", null, null, null);
                        if (attachfile != null)
                        {
                            file.FileName = attachfile.FileName;
                            file.FilePath = attachfile.FilePath;
                        }
                    }
                    else if (module.Equals("lessonplan", StringComparison.OrdinalIgnoreCase))
                    {
                        var LessonPlanId = Convert.ToInt32(id);
                        var attachfile = _planSchemeDetailService.GetById(LessonPlanId);
                        if (attachfile != null)
                        {
                            file.FileName = attachfile.FileName;
                            file.FilePath = attachfile.FilePath;
                        }
                    }

                    if (file != null && !string.IsNullOrWhiteSpace(file.FilePath))
                    {
                        var webClient = new WebClient();
                        var redirectPathURL = PhoenixConfiguration.Instance.ReadFilePath + file.FilePath;
                        DevExpressEditorSettings devExpressEditorSettings = new DevExpressEditorSettings(file);

                        //if (devExpressEditorSettings.FileExtension.IndexOf("html", StringComparison.CurrentCultureIgnoreCase) >= 0)
                        //{
                        //    byte[] FileBytes = webClient.DownloadData(devExpressEditorSettings.ServerPath);
                        //    return File(FileBytes, "text/html", file.FileName);
                        //}
                        //else if (devExpressEditorSettings.FileExtension.IndexOf("csv", StringComparison.OrdinalIgnoreCase) >= 0)
                        //{
                        //    byte[] FileBytes = webClient.DownloadData(devExpressEditorSettings.ServerPath);
                        //    Response.BufferOutput = true;
                        //    Response.ContentType = "text/csv";


                        //    Response.BinaryWrite(FileBytes);
                        //    Response.Flush();
                        //    Response.End();
                        //    return new EmptyResult();
                        //}

                        //else if (imgExt.Any(devExpressEditorSettings.FileExtension.Replace(".", "").ToLower().Contains) || devExpressEditorSettings.FileExtension.IndexOf("pdf", StringComparison.OrdinalIgnoreCase) >= 0 || devExpressEditorSettings.FileExtension.IndexOf("txt", StringComparison.OrdinalIgnoreCase) >= 0)
                        //{
                        //    byte[] FileBytes = webClient.DownloadData(devExpressEditorSettings.ServerPath);
                        //    Response.BufferOutput = true;
                        //    Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;

                        //    return File(FileBytes, GetMimeType(devExpressEditorSettings.ServerPath));
                        //}
                        //else if (!docExt.Any(devExpressEditorSettings.FileExtension.Replace(".", "").ToLower().Contains))
                        //{
                        //    byte[] FileBytes = webClient.DownloadData(devExpressEditorSettings.ServerPath);
                        //    Response.BufferOutput = true;
                        //    Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;

                        //    return File(FileBytes, GetMimeType(devExpressEditorSettings.ServerPath), file.FileName);
                        //}

                        if (devExpressEditorSettings.FileExtension.IndexOf("xls", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            return View(devExpressEditorSettings);
                        }
                        else if (devExpressEditorSettings.FileExtension.IndexOf("doc", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            return View(devExpressEditorSettings);
                        }
                        //else if(videoExt.Any(devExpressEditorSettings.FileExtension.Replace(".", "").ToLower().Contains) || audioExt.Any(devExpressEditorSettings.FileExtension.Replace(".", "").ToLower().Contains))
                        //{
                        //    return View(devExpressEditorSettings);
                        //}
                        else
                        {
                            return Redirect(redirectPathURL);
                        }

                    }
                }
            }
            catch
            {
                throw new FileNotFoundException();
            }
            return HttpNotFound();
        }

        private string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }

    }
}