﻿using OfficeOpenXml;
using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Phoenix.VLE.Web.Areas.Document.Controllers
{
    [Authorize]
    public class ExcelUploadController : Controller
    {
        private ITemplateUploadService _TemplateService;

        public ExcelUploadController(ITemplateUploadService templateService)
        {
            _TemplateService = templateService;
        }
        // GET: Document/ExcelUpload


        public ActionResult Index()
        {
            TemplateUploadModel Model = new TemplateUploadModel();
            Model.Template_M = new List<Template_M>();
            Template_M _obj = new Template_M();

            _obj = new Template_M() { TPT_NAME = "Student", TEMPLATE_CODE = "T1" };
            Model.Template_M.Add(_obj);
            _obj = new Template_M() { TPT_NAME = "Staff", TEMPLATE_CODE = "T2" };
            Model.Template_M.Add(_obj);
            _obj = new Template_M() { TPT_NAME = "Grade Wise Subject Master", TEMPLATE_CODE = "T3" };
            Model.Template_M.Add(_obj);
            _obj = new Template_M() { TPT_NAME = "Student Wise Subject Mapping", TEMPLATE_CODE = "T4" };
            Model.Template_M.Add(_obj);
            if (TempData["ErrorMsg"] != null)
            {
                ViewBag.ErrorClass = "alert alert-danger";
                ViewBag.ErrorMsg = TempData["ErrorMsg"];
                Model.TEMPLATE_CODE =Convert.ToString(TempData["TemplateCode"]);
            }


            return View("Index", Model);
        }

        [HttpPost]
        public ActionResult ImportTemplate(TemplateUploadModel model)
        {
            if (!ModelState.IsValid)
                return View("Index", model);

         
            Template_M _obj = new Template_M();
            model.Template_M = new List<Template_M>();
            _obj = new Template_M() { TPT_NAME = "Student", TEMPLATE_CODE = "T1" };
            model.Template_M.Add(_obj);
            _obj = new Template_M() { TPT_NAME = "Staff", TEMPLATE_CODE = "T2" };
            model.Template_M.Add(_obj);
            _obj = new Template_M() { TPT_NAME = "Grade Wise Subject Master", TEMPLATE_CODE = "T3" };
            model.Template_M.Add(_obj);
            _obj = new Template_M() { TPT_NAME = "Student Wise Subject Mapping", TEMPLATE_CODE = "T4" };
            model.Template_M.Add(_obj);

            try
            {
               
                if (model.TemplateFile != null)
                {

                    OperationDetails operationDetails = new OperationDetails();
                    operationDetails.Success = true;
                    operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);


                    ExcelPackage package = new ExcelPackage(model.TemplateFile.InputStream);

                    DataTable excelDataTable = new DataTable();
                    try
                    {
                        ExcelWorksheet ws = null;
                        if (model.TEMPLATE_CODE == "T1")
                            ws = package.Workbook.Worksheets["Student Data"];
                        else if (model.TEMPLATE_CODE == "T2")
                            ws = package.Workbook.Worksheets["Staff Data"];
                        else if (model.TEMPLATE_CODE == "T3")
                            ws = package.Workbook.Worksheets["Grade Wise Subject Master"];
                        else if (model.TEMPLATE_CODE == "T4")
                            ws = package.Workbook.Worksheets["Student Wise Subject"];

                        excelDataTable = GetWorksheetAsDataTable(ws);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.ErrorMsg = "Invalid Format Result";
                        return  View("Index", model);
                    }

                    bool isFirstEmptyRow = false;

                    //Delete first row if it contains all NULL values
                    if (excelDataTable.Rows.Count == 1)
                    {
                        DataRow firstRow = excelDataTable.Rows[0];

                        if (firstRow[0] == DBNull.Value
                                 && firstRow[1] == DBNull.Value
                                 && firstRow[2] == DBNull.Value
                                 && firstRow[3] == DBNull.Value
                                 && firstRow[4] == DBNull.Value)
                        {
                            isFirstEmptyRow = true;
                        }

                        if (isFirstEmptyRow)
                        {
                            excelDataTable.Rows.RemoveAt(0);
                        }
                    }

                    TempData["TemplateCode"] = model.TEMPLATE_CODE;
                   // Check if empty file or only headers
                    if (excelDataTable.Rows.Count == 0)
                    {

                       TempData["ErrorMsg"] = "File is empty";
                        return RedirectToAction("Index", "ExcelUpload");
                    }

                    //Validate headers
                    bool isValidFileFormat = true;
                    isValidFileFormat = IsHeaderValid(excelDataTable, model.TEMPLATE_CODE);

                    if (!isValidFileFormat)
                    {

                        TempData["ErrorMsg"] = "Invalid Headers";
                        return RedirectToAction("Index", "ExcelUpload");
                    }


                    var result = false;
                    long UserId = SessionHelper.CurrentSession.Id;
                    if (model.TEMPLATE_CODE == "T1")
                    {
                        List<StudentUploadTemplate> StudentList = GetStudentList(excelDataTable);
                        IEnumerable<StudentUploadTemplate> _StudentList = _TemplateService.StudentTemplateBulkImport(StudentList, UserId);
                        TempData["_StudentList"] = _StudentList;
                        bool IsSuccess = _StudentList.Any(cus => cus.Return_Status == "FAILED");
                        return RedirectToAction("StudentTemplateUploadResult", "ExcelUpload", new { TemplateCode = "T1", ErrorMsg = "", IsSuccess= !IsSuccess });
                    }
                    else
                        if (model.TEMPLATE_CODE == "T2")
                    {
                        List<StaffUploadTemplate> StaffList = GetStaffList(excelDataTable);
                        IEnumerable<StaffUploadTemplate> _StaffList = _TemplateService.StaffTemplateBulkImport(StaffList, UserId);
                        TempData["StaffList"] = _StaffList;
                        
                        bool IsSuccess = _StaffList.Any(cus => cus.Return_Status == "FAILED");
                        IsSuccess = _StaffList.Count() == 0 ? true : IsSuccess;
                        return RedirectToAction("StaffTemplateUploadResult", "ExcelUpload", new { TemplateCode = "T2", ErrorMsg = "", IsSuccess = !IsSuccess });
                    }
                    else
                        if (model.TEMPLATE_CODE == "T3")
                    {
                        List<GRADE_WISE_SUBJECT_M> GradeWiseSubjectList = GetGradeWiseSubjectList(excelDataTable);
                        IEnumerable<GRADE_WISE_SUBJECT_M> _GradeWiseSubjectList = _TemplateService.GradeWiseSubjectTemplateBulkImport(GradeWiseSubjectList, UserId);
                        TempData["SubjectList"] = _GradeWiseSubjectList;
                        bool IsSuccess = _GradeWiseSubjectList.Any(cus => cus.Return_Status == "FAILED");
                        return RedirectToAction("SubjectTemplateUploadResult", "ExcelUpload", new { TemplateCode = "T3", ErrorMsg = "", IsSuccess = !IsSuccess });
                    }
                    if (model.TEMPLATE_CODE == "T4")
                    {
                        List<STUDENT_WISE_SUBJECT_MAPPING> StudentWiseSubjectList = GetStudentWiseSubject(excelDataTable);
                        IEnumerable<STUDENT_WISE_SUBJECT_MAPPING> _StudentWiseSubjectList = _TemplateService.StudentWiseSubjectTemplateBulkImport(StudentWiseSubjectList, UserId);
                        TempData["SubjectList"] = _StudentWiseSubjectList;
                        bool IsSuccess = _StudentWiseSubjectList.Any(cus => cus.Return_Status == "FAILED");
                        return RedirectToAction("StudentSubjectTemplateUploadResult", "ExcelUpload", new { TemplateCode = "T4", ErrorMsg = "", IsSuccess = !IsSuccess });
                    }
                }
                else
                {
                    TempData["ErrorMsg"] = "Please select a template file";
                    return RedirectToAction("Index", "ExcelUpload");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("TemplateUploadResult", "ExcelUpload", new { list = new List<object>(), TemplateCode = "T0", ErrorMsg = ex.Message,IsSuccess=false });
            }
            return null;
        }

        [HttpPost]
        public ActionResult ImportTemplate_New(TemplateUploadModel model)
        {
            if (!ModelState.IsValid)
                return View("Index", model);


            Template_M _obj = new Template_M();
            model.Template_M = new List<Template_M>();
            _obj = new Template_M() { TPT_NAME = "Student", TEMPLATE_CODE = "T1" };
            model.Template_M.Add(_obj);
            _obj = new Template_M() { TPT_NAME = "Staff", TEMPLATE_CODE = "T2" };
            model.Template_M.Add(_obj);
            _obj = new Template_M() { TPT_NAME = "Grade Wise Subject Master", TEMPLATE_CODE = "T3" };
            model.Template_M.Add(_obj);
            _obj = new Template_M() { TPT_NAME = "Student Wise Subject Mapping", TEMPLATE_CODE = "T4" };
            model.Template_M.Add(_obj);

            try
            {

                if (model.TemplateFile != null)
                {

                    OperationDetails operationDetails = new OperationDetails();
                    operationDetails.Success = true;
                    operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                    DataTable excelDataTable = new DataTable();
                    FileInfo file = new FileInfo(model.TemplateFile.FileName);
                    using (var package = new ExcelPackage(file))
                    {
                        //var worksheet = package.Workbook.Worksheets.Add(package.Workbook.Worksheets[0].Name);

                        //var stream = new MemoryStream(package.GetAsByteArray());

                        try
                        {
                            ExcelWorksheet ws = null;
                            if (model.TEMPLATE_CODE == "T1")
                            {
                                ws = package.Workbook.Worksheets["Student Data"];
                                ws = package.Workbook.Worksheets["Sheet1"];
                            }

                            else if (model.TEMPLATE_CODE == "T2")
                                ws = package.Workbook.Worksheets["Staff Data"];
                            else if (model.TEMPLATE_CODE == "T3")
                                ws = package.Workbook.Worksheets["Grade Wise Subject Master"];
                            else if (model.TEMPLATE_CODE == "T4")
                                ws = package.Workbook.Worksheets["Student Wise Subject"];

                            excelDataTable = GetWorksheetAsDataTable(ws);
                        }
                        catch (Exception ex)
                        {
                            ViewBag.ErrorMsg = "Invalid Format Result";
                            return View("Index", model);
                        }

                    }


                    bool isFirstEmptyRow = false;

                    //Delete first row if it contains all NULL values
                    if (excelDataTable.Rows.Count == 1)
                    {
                        DataRow firstRow = excelDataTable.Rows[0];

                        if (firstRow[0] == DBNull.Value
                                 && firstRow[1] == DBNull.Value
                                 && firstRow[2] == DBNull.Value
                                 && firstRow[3] == DBNull.Value
                                 && firstRow[4] == DBNull.Value)
                        {
                            isFirstEmptyRow = true;
                        }

                        if (isFirstEmptyRow)
                        {
                            excelDataTable.Rows.RemoveAt(0);
                        }
                    }

                    TempData["TemplateCode"] = model.TEMPLATE_CODE;
                    // Check if empty file or only headers
                    if (excelDataTable.Rows.Count == 0)
                    {

                        TempData["ErrorMsg"] = "File is empty";
                        return RedirectToAction("Index", "ExcelUpload");
                    }

                    //Validate headers
                    bool isValidFileFormat = true;
                    isValidFileFormat = IsHeaderValid(excelDataTable, model.TEMPLATE_CODE);

                    if (!isValidFileFormat)
                    {

                        TempData["ErrorMsg"] = "Invalid Headers";
                        return RedirectToAction("Index", "ExcelUpload");
                    }


                    var result = false;
                    long UserId = SessionHelper.CurrentSession.Id;
                    if (model.TEMPLATE_CODE == "T1")
                    {
                        List<StudentUploadTemplate> StudentList = GetStudentList(excelDataTable);
                        IEnumerable<StudentUploadTemplate> _StudentList = _TemplateService.StudentTemplateBulkImport(StudentList, UserId);
                        TempData["_StudentList"] = _StudentList;
                        bool IsSuccess = _StudentList.Any(cus => cus.Return_Status == "FAILED");
                        return RedirectToAction("StudentTemplateUploadResult", "ExcelUpload", new { TemplateCode = "T1", ErrorMsg = "", IsSuccess = !IsSuccess });
                    }
                    else
                        if (model.TEMPLATE_CODE == "T2")
                    {
                        List<StaffUploadTemplate> StaffList = GetStaffList(excelDataTable);
                        IEnumerable<StaffUploadTemplate> _StaffList = _TemplateService.StaffTemplateBulkImport(StaffList, UserId);
                        TempData["StaffList"] = _StaffList;

                        bool IsSuccess = _StaffList.Any(cus => cus.Return_Status == "FAILED");
                        IsSuccess = _StaffList.Count() == 0 ? true : IsSuccess;
                        return RedirectToAction("StaffTemplateUploadResult", "ExcelUpload", new { TemplateCode = "T2", ErrorMsg = "", IsSuccess = !IsSuccess });
                    }
                    else
                        if (model.TEMPLATE_CODE == "T3")
                    {
                        List<GRADE_WISE_SUBJECT_M> GradeWiseSubjectList = GetGradeWiseSubjectList(excelDataTable);
                        IEnumerable<GRADE_WISE_SUBJECT_M> _GradeWiseSubjectList = _TemplateService.GradeWiseSubjectTemplateBulkImport(GradeWiseSubjectList, UserId);
                        TempData["SubjectList"] = _GradeWiseSubjectList;
                        bool IsSuccess = _GradeWiseSubjectList.Any(cus => cus.Return_Status == "FAILED");
                        return RedirectToAction("SubjectTemplateUploadResult", "ExcelUpload", new { TemplateCode = "T3", ErrorMsg = "", IsSuccess = !IsSuccess });
                    }
                    if (model.TEMPLATE_CODE == "T4")
                    {
                        List<STUDENT_WISE_SUBJECT_MAPPING> StudentWiseSubjectList = GetStudentWiseSubject(excelDataTable);
                        IEnumerable<STUDENT_WISE_SUBJECT_MAPPING> _StudentWiseSubjectList = _TemplateService.StudentWiseSubjectTemplateBulkImport(StudentWiseSubjectList, UserId);
                        TempData["SubjectList"] = _StudentWiseSubjectList;
                        bool IsSuccess = _StudentWiseSubjectList.Any(cus => cus.Return_Status == "FAILED");
                        return RedirectToAction("StudentSubjectTemplateUploadResult", "ExcelUpload", new { TemplateCode = "T4", ErrorMsg = "", IsSuccess = !IsSuccess });
                    }
                }
                else
                {
                    TempData["ErrorMsg"] = "Please select a template file";
                    return RedirectToAction("Index", "ExcelUpload");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("TemplateUploadResult", "ExcelUpload", new { list = new List<object>(), TemplateCode = "T0", ErrorMsg = ex.Message, IsSuccess = false });
            }
            return null;
        }

        private JsonResult InvalidFormatResult()
        {
            //result Object for Invalid file format
            OperationDetails operationDetails = new OperationDetails();
            operationDetails.Success = true;
            operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
            operationDetails.Message = ResourceManager.GetString("Shared.Messages.fileInvalidFormatMessage");
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }
        private bool IsHeaderValid(DataTable excelDatatTable, string TemplateCode = "")
        {
            bool isHeaderValid = true;



            if (TemplateCode == "T1")
            {
                if ((excelDatatTable.Columns[0].Caption != "School Name") ||
                   (excelDatatTable.Columns[1].Caption != "FEE ID/Admission number") ||
                   (excelDatatTable.Columns[2].Caption != "Siblin Ref") ||
                   (excelDatatTable.Columns[3].Caption != "Student First Name") ||
                   (excelDatatTable.Columns[4].Caption != "Student Middle Name") ||
                   (excelDatatTable.Columns[5].Caption != "Student Last Name") ||
                   (excelDatatTable.Columns[6].Caption != "Student Gender") ||
                   (excelDatatTable.Columns[7].Caption != "Nationality(Passport)") ||
                   (excelDatatTable.Columns[8].Caption != "Date of Birth") ||
                   (excelDatatTable.Columns[9].Caption != "Academic Year") ||
                   (excelDatatTable.Columns[10].Caption != "Curriculum") ||
                   (excelDatatTable.Columns[11].Caption != "Grade") ||
                   (excelDatatTable.Columns[12].Caption != "Grade Description") ||
                   (excelDatatTable.Columns[13].Caption != "Section") ||
                   (excelDatatTable.Columns[14].Caption != "Stream") ||
                   (excelDatatTable.Columns[15].Caption != "Primary Contact") ||
                   (excelDatatTable.Columns[16].Caption != "Primary Contact First Name") ||
                   (excelDatatTable.Columns[17].Caption != "Primary Contact Middle Name") ||
                   (excelDatatTable.Columns[18].Caption != "Primary Contact Last Name") ||
                   (excelDatatTable.Columns[19].Caption != "Primary Contact Mobile") ||
                   (excelDatatTable.Columns[20].Caption != "Primary Contact Email")

                   )
                {
                    isHeaderValid = false;
                }
            }
            else if (TemplateCode == "T2")
            {

                if ((excelDatatTable.Columns[0].Caption != "School Name") ||
                   (excelDatatTable.Columns[1].Caption != "Staff No") ||
                   (excelDatatTable.Columns[2].Caption != "Staff First Name") ||
                   (excelDatatTable.Columns[3].Caption != "Staff Middle Name") ||
                   (excelDatatTable.Columns[4].Caption != "Staff Last Name") ||
                   (excelDatatTable.Columns[5].Caption != "Staff Gender") ||
                   (excelDatatTable.Columns[6].Caption != "Staff DOB") ||
                   (excelDatatTable.Columns[7].Caption != "Staff Email") ||
                   (excelDatatTable.Columns[8].Caption != "Staff Mobile") ||
                   (excelDatatTable.Columns[9].Caption != "Staff Role")
                   )
                {
                    isHeaderValid = false;
                }
            }
            else if (TemplateCode == "T3")
            {

                if ((excelDatatTable.Columns[0].Caption != "School Name") ||
                   (excelDatatTable.Columns[1].Caption != "Academic Year") ||
                   (excelDatatTable.Columns[2].Caption != "Curriculum") ||
                   (excelDatatTable.Columns[3].Caption != "Grade") ||
                   (excelDatatTable.Columns[4].Caption != "Stream") ||
                   (excelDatatTable.Columns[5].Caption != "Subject ID") ||
                   (excelDatatTable.Columns[6].Caption != "Optional Subject") ||
                   (excelDatatTable.Columns[7].Caption != "Subject Teacher") ||
                   (excelDatatTable.Columns[8].Caption != "Parent Subject")
                   )
                {
                    isHeaderValid = false;
                }
            }
            else if (TemplateCode == "T4")
            {

                if ((excelDatatTable.Columns[0].Caption != "School Name") ||
                   (excelDatatTable.Columns[1].Caption != "FEE ID/Admission number") ||
                   (excelDatatTable.Columns[2].Caption != "Subject ID")

                   )
                {
                    isHeaderValid = false;
                }
            }



            return isHeaderValid;
        }

        private List<StudentUploadTemplate> GetStudentList(DataTable excelDataTable)
        {
            int rowIndex = 0;
            List<StudentUploadTemplate> StudentList = new List<StudentUploadTemplate>();

            foreach (DataRow dr in excelDataTable.Rows)
            {
                //increment row counter - used to show line number in error
                rowIndex++;
                StudentUploadTemplate Student = new StudentUploadTemplate();

                Student.TSTU_SCHOOL_NAME = dr["School Name"].ToString();
                Student.TSTU_FEEID_OR_ADMISSION_NUMBER = dr["FEE ID/Admission number"].ToString();
                Student.TSTU_SIBLIN_REF = dr["Siblin Ref"].ToString();
                Student.TSTU_FIRSTNAME = dr["Student First Name"].ToString();
                Student.TSTU_MIDNAME = dr["Student Middle Name"].ToString();
                Student.TSTU_LASTNAME = dr["Student Last Name"].ToString();
                Student.TSTU_GENDER = dr["Student Gender"].ToString();
                Student.TSTU_NATIONALITY = dr["Nationality(Passport)"].ToString();
                Student.TSTU_DOB = (dr["Date of Birth"].ToString());
                Student.TSTU_ACY_DESCR = dr["Academic Year"].ToString();
                Student.TSTU_CLM_DESCR = dr["Curriculum"].ToString();
                Student.TSTU_GRD_ID = dr["Grade"].ToString();
                Student.TSTU_GRD_DESCR = dr["Grade Description"].ToString();
                Student.TSTU_SCT_DESCR = dr["Section"].ToString();
                Student.TSTU_STREAM_DESCR = dr["Stream"].ToString();
                Student.TSTU_PRIMARYCONTACT = dr["Primary Contact"].ToString();
                Student.TSTU_PRIMARYCONTACT_FNAME = dr["Primary Contact First Name"].ToString();
                Student.TSTU_PRIMARYCONTACT_SNAME = dr["Primary Contact Middle Name"].ToString();
                Student.TSTU_PRIMARYCONTACT_LNAME = dr["Primary Contact Last Name"].ToString();
                Student.TSTU_PRIMARYCONTACT_MOBILE = dr["Primary Contact Mobile"].ToString();
                Student.TSTU_PRIMARYCONTACT_EMAIL = dr["Primary Contact Email"].ToString();
                StudentList.Add(Student);
            }
            return StudentList;
        }

        private List<StaffUploadTemplate> GetStaffList(DataTable excelDataTable)
        {


            int rowIndex = 0;
            List<StaffUploadTemplate> StaffUploadTemplateList = new List<StaffUploadTemplate>();

            foreach (DataRow dr in excelDataTable.Rows)
            {
                //increment row counter - used to show line number in error
                rowIndex++;
                StaffUploadTemplate StaffUploadTemplate = new StaffUploadTemplate();
                StaffUploadTemplate.TSTF_SCHOOL_NAME = dr["School Name"].ToString();
                StaffUploadTemplate.TSTF_NO = dr["Staff No"].ToString();
                StaffUploadTemplate.TSTF_FIRSTNAME = dr["Staff First Name"].ToString();
                StaffUploadTemplate.TSTF_MIDNAME = dr["Staff Middle Name"].ToString();
                StaffUploadTemplate.TSTF_LASTNAME = dr["Staff Last Name"].ToString();
                StaffUploadTemplate.TSTF_GENDER = dr["Staff Gender"].ToString();
                StaffUploadTemplate.TSTF_DOB = dr["Staff DOB"].ToString();
                StaffUploadTemplate.TSTF_EMAIL = dr["Staff Email"].ToString();
                StaffUploadTemplate.TSTF_MOBILE = dr["Staff Mobile"].ToString();
                StaffUploadTemplate.TSTF_ROLE = dr["Staff Role"].ToString();

                StaffUploadTemplateList.Add(StaffUploadTemplate);
            }
            return StaffUploadTemplateList;
        }

        private List<GRADE_WISE_SUBJECT_M> GetGradeWiseSubjectList(DataTable excelDataTable)
        {



            int rowIndex = 0;
            List<GRADE_WISE_SUBJECT_M> GradeWiseSubjectList = new List<GRADE_WISE_SUBJECT_M>();

            foreach (DataRow dr in excelDataTable.Rows)
            {
                //increment row counter - used to show line number in error
                rowIndex++;
                GRADE_WISE_SUBJECT_M GradeWiseSubject = new GRADE_WISE_SUBJECT_M();
                GradeWiseSubject.TGSM_SCHOOL_NAME = dr["School Name"].ToString();
                GradeWiseSubject.TGSM_ACY_DESCR = dr["Academic Year"].ToString();
                GradeWiseSubject.TGSM_CLM_DESCR = dr["Curriculum"].ToString();
                GradeWiseSubject.TGSM_GRD_DESCR = dr["Grade"].ToString();
                GradeWiseSubject.TGSM_STM_DESCR = dr["Stream"].ToString();
                GradeWiseSubject.TGSM_SBJ_DESCR = dr["Subject ID"].ToString();
                GradeWiseSubject.TGSM_OPTIONAL_SBJ = dr["Optional Subject"].ToString();
                GradeWiseSubject.TGSM_SBJ_TEACHER = dr["Subject Teacher"].ToString();
                GradeWiseSubject.TGSM_PARENT_SBJ = dr["Parent Subject"].ToString();


                GradeWiseSubjectList.Add(GradeWiseSubject);
            }
            return GradeWiseSubjectList;
        }

        private List<STUDENT_WISE_SUBJECT_MAPPING> GetStudentWiseSubject(DataTable excelDataTable)
        {

            int rowIndex = 0;
            List<STUDENT_WISE_SUBJECT_MAPPING> GetStudentWiseSubjectList = new List<STUDENT_WISE_SUBJECT_MAPPING>();

            foreach (DataRow dr in excelDataTable.Rows)
            {
                //increment row counter - used to show line number in error
                rowIndex++;
                STUDENT_WISE_SUBJECT_MAPPING GetStudentWiseSubject = new STUDENT_WISE_SUBJECT_MAPPING();
                GetStudentWiseSubject.TSSM_SCHOOL_NAME = dr["School Name"].ToString();
                GetStudentWiseSubject.TSSM_FEEID_OR_ADMISSION_NUMBER = dr["FEE ID/Admission number"].ToString();
                GetStudentWiseSubject.TSSM_SBJ_ID = dr["Subject ID"].ToString();
                GetStudentWiseSubjectList.Add(GetStudentWiseSubject);
            }
            return GetStudentWiseSubjectList;
        }


        /// <summary>
        /// Assumption: Worksheet is in table format with no weird padding or blank column headers.
        /// 
        /// Assertion: Duplicate column names will be aliased by appending a sequence number (eg. Column, Column1, Column2)
        /// </summary>
        /// <param name="worksheet"></param>
        /// <returns></returns>
        public static DataTable GetWorksheetAsDataTable(ExcelWorksheet worksheet)
        {
            var dt = new DataTable(worksheet.Name);
            dt.Columns.AddRange(GetDataColumns(worksheet).ToArray());
            var headerOffset = 1; //have to skip header row
            var width = dt.Columns.Count;
            var depth = GetTableDepth(worksheet, headerOffset);
            for (var i = 1; i <= depth; i++)
            {
                var row = dt.NewRow();
                for (var j = 1; j <= width; j++)
                {
                    var currentValue = worksheet.Cells[i + headerOffset, j].Value;

                    //have to decrement b/c excel is 1 based and datatable is 0 based.
                    row[j - 1] = currentValue == null ? null : currentValue.ToString();
                }

                dt.Rows.Add(row);
            }

            return dt;
        }

        /// <summary>
        /// Assumption: There are no null or empty cells in the first column
        /// </summary>
        /// <param name="worksheet"></param>
        /// <returns></returns>
        private static int GetTableDepth(ExcelWorksheet worksheet, int headerOffset)
        {
            var i = 1;
            var j = 1;
            var cellValue = worksheet.Cells[i + headerOffset, j].Value;
            while (cellValue != null)
            {
                i++;
                cellValue = worksheet.Cells[i + headerOffset, j].Value;
            }

            return i - 1; //subtract one because we're going from rownumber (1 based) to depth (0 based)
        }

        private static IEnumerable<DataColumn> GetDataColumns(ExcelWorksheet worksheet)
        {
            return GatherColumnNames(worksheet).Select(x => new DataColumn(x));
        }

        private static IEnumerable<string> GatherColumnNames(ExcelWorksheet worksheet)
        {
            var columns = new List<string>();

            var i = 1;
            var j = 1;
            var columnName = worksheet.Cells[i, j].Value;
            while (columnName != null)
            {
                columns.Add(GetUniqueColumnName(columns, columnName.ToString()));
                j++;
                columnName = worksheet.Cells[i, j].Value;
            }

            return columns;
        }

        private static string GetUniqueColumnName(IEnumerable<string> columnNames, string columnName)
        {
            var colName = columnName;
            var i = 1;
            while (columnNames.Contains(colName))
            {
                colName = columnName + i.ToString();
                i++;
            }

            return colName;
        }

        public ActionResult StudentTemplateUploadResult(string TemplateCode, string ErrorMsg,bool IsSuccess)
        {
            ViewBag.TemplateCode = "T1";
            ViewBag.StudentList = (List<StudentUploadTemplate>)TempData["_StudentList"];
            ViewBag.IsSuccess = IsSuccess == true? "Success":"Failed";
            return View("TemplateUploadResult");
        }
        public ActionResult StaffTemplateUploadResult(string TemplateCode, string ErrorMsg, bool IsSuccess)
        {
            ViewBag.TemplateCode = "T2";
            ViewBag.StaffList = (List<StaffUploadTemplate>)TempData["StaffList"];
            ViewBag.IsSuccess = IsSuccess == true ? "Success" : "Failed";
            return View("TemplateUploadResult");
        }
        public ActionResult SubjectTemplateUploadResult( string TemplateCode, string ErrorMsg, bool IsSuccess)
        {

            ViewBag.TemplateCode = "T3";
            ViewBag.SubjectList = (List<GRADE_WISE_SUBJECT_M>)TempData["SubjectList"];
            ViewBag.IsSuccess = IsSuccess == true ? "Success" : "Failed";
            return View("TemplateUploadResult");
        }
        public ActionResult StudentSubjectTemplateUploadResult( string TemplateCode, string ErrorMsg, bool IsSuccess)
        {
            ViewBag.TemplateCode = "T4";
            ViewBag.SubjectList = (List<STUDENT_WISE_SUBJECT_MAPPING>)TempData["SubjectList"];
            ViewBag.IsSuccess = IsSuccess == true ? "Success" : "Failed";
            return View("TemplateUploadResult");
        }

        public ActionResult TemplateUploadResult(string TemplateCode, string ErrorMsg)
        {
            ViewBag.TemplateCode = "T0";
            ViewBag.ErrorMsg = ErrorMsg;
          
            return View("TemplateUploadResult");
        }
    }
}