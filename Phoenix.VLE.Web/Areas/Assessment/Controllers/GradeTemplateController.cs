﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Phoenix.VLE.Web.Areas.Assessment.Controllers
{
    public class GradeTemplateController : Controller
    {
        // GET: Assessment/GradeTemplate/
        private IAssessmentConfigService _AssessmentConfigService;
        public GradeTemplateController(IAssessmentConfigService assessmentConfigService)
        {
            _AssessmentConfigService = assessmentConfigService;

        }
        public ActionResult Index()
        {
            List<GradeTemplate> obj = new List<GradeTemplate>();
            var Result = _AssessmentConfigService.GetAssessmentConfigMasterList(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId)).ToList();
            return View(Result);
        }
        public ActionResult SaveGradeTemplate(List<GradeTemplate> gradingList,string Description, long? GradeTemplateMasterId,string MasterIds)
        {
            var result = _AssessmentConfigService.AddEditAssessmentConfig(gradingList, Description, MasterIds ,Convert.ToInt32(GradeTemplateMasterId), SessionHelper.CurrentSession.SchoolId);
            //return View();
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAssessmentConfigList(int MasterId)
        {
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;
            var Result = _AssessmentConfigService.GetAssessmentConfigList(Convert.ToInt32(itemId)).Where(x => x.GradeTemplateMasterId == MasterId).ToList();
            return Json(Result, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetAssessmentLoad()
        {
            var Result = _AssessmentConfigService.GetAssessmentConfigMasterList(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId)).ToList();
            return PartialView("_GradeTemplate", Result);

        }
    }
}