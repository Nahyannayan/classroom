﻿using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Assessment.Controllers
{
    public class GradingSlabController : Controller
    {
        private IAssessmentConfigService _AssessmentConfigService;
        public GradingSlabController(IAssessmentConfigService assessmentConfigService)
        {
            _AssessmentConfigService = assessmentConfigService;

        }
        // GET: Assessment/GradingSlab
        public ActionResult Index()
        {

            var Result = _AssessmentConfigService.GetGradeSlabMasterList(SessionHelper.CurrentSession.SchoolId).ToList();
            return View(Result);
        }
        public ActionResult SaveGradingSlab(List<GradeSlab> obj,string GradeSlabMasterDesc, long? GradeSlabMasterId, string MasterIds)
        {

          
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;

            var result = _AssessmentConfigService.AddEditGradeSlab(obj, GradeSlabMasterDesc, MasterIds, Convert.ToInt32(GradeSlabMasterId), Convert.ToInt32(itemId));
            //return View();
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GradeSlabListList(int MasterId)
        {
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;
            var Result = _AssessmentConfigService.GetGradeSlabList(Convert.ToInt32(itemId)).Where(x => x.GradeSlabMasterId == MasterId).ToList();
            //return PartialView("_GradeSlab", Result);
            return Json(Result, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GradeSlabListDetails()
        {

            var Result = _AssessmentConfigService.GetGradeSlabMasterList(SessionHelper.CurrentSession.SchoolId).ToList();
            return PartialView("_GradingMasterSlab", Result);
        }
    }
}