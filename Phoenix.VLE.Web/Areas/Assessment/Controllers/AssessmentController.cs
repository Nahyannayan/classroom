﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Areas.Assessment.Controllers
{
    [Authorize]
    public class AssessmentController : BaseController
    {
        private IAssessmentService _AssessmentService;
        private ISIMSCommonService _SIMSCommonService;
        //private readonly IAssessmentSettingService assessmentSettingService;
        private readonly IClassListService _classListService;
        private const string Add = "ADD";
        private const string Edit = "EDIT";
        private const string Delete = "DELETE";

        public AssessmentController(IAssessmentService AssessmentService, ISIMSCommonService SIMSCommonService, IClassListService classListService)
        {
            _AssessmentService = AssessmentService;
            _SIMSCommonService = SIMSCommonService;
            //this.assessmentSettingService = assessmentSettingService;
            _classListService = classListService;
        }
        // GET: SMS/Assessment
        public ActionResult Index()
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = SessionHelper.CurrentSession.SchoolId;//Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var clm_id = SessionHelper.CurrentSession.CLM_ID;  //new to get this value from some generic class .
            var current_acd = 1;
            var academicYears = _SIMSCommonService.GetAcademicYearList(schoolid.ToString(), clm_id, null);
            ViewBag.AcademicYear = new SelectList(academicYears, "ItemId", "ItemName", academicYears.Count() > 0 ? academicYears.FirstOrDefault(x => x.Selected == true).ItemId : "");
            var Categories = _AssessmentService.GetAssessmentCategories(schoolid, "").ToList();

            if (Categories.Count > 0)
            {
                List<SelectListItem> listOfCategories = Categories.ConvertAll(a =>
                {
                    return new SelectListItem()
                    {
                        Text = a.CAT_DESC,
                        Value = a.CAT_ID.ToString(),
                        Selected = false
                    };
                });
            }
            ViewBag.AssessmentCategories = Categories;
            var strtt_id = "";
            if (Session["SIMSTimeTableSchedule"] != null)
            {
                strtt_id = Convert.ToString(Session["SIMSTimeTableSchedule"]);
                if (!string.IsNullOrWhiteSpace(strtt_id) && strtt_id != "0")
                {
                    ViewBag.strtt_id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
                }
            }
            //if (strtt_id != "0")
            //{
            //    return RedirectToAction("GetClassList", "Assessment", new { strtt_id = strtt_id });
            //}
            //else
            return View("AssessmentGradeEntry");
            // return View("AssessmentGradeEntry");
        }
        [HttpGet]
        public JsonResult FetchReportHeaders(int id)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var ReportHeader = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.ReportHeader, string.Format("{0},{1}", schoolid, id)), "ItemId", "ItemName");
            return Json(ReportHeader, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetGradesAccess(Int32 acd_id, Int32 rsm_id)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, acd_id, Convert.ToInt32(schoolid), grd_access, rsm_id);
            var gradesList = new SelectList(list, "GRD_ID", "GRM_DISPLAY");
            return Json(gradesList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetSubjectsByGrade(Int32 acd_id, string grd_id)
        {
            var list = GetSubjectByGrade(acd_id, grd_id);
            var subjectList = new SelectList(list, "SBG_ID", "SBG_DESCR");
            return Json(subjectList, JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        //public JsonResult GetSubjectGroupBySubject(string grd_id, Int32 sbg_id)
        //{
        //    var Userdetail = Helpers.CommonHelper.GetLoginUser();
        //    var schoolid = Userdetail.SchoolId;
        //    var username = Userdetail.UserName;
        //    var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
        //    var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
        //    var list = _SIMSCommonService.GetSubjectGroupBySubject(username, isSuperUser, Convert.ToInt32(schoolid), grd_id, sbg_id);
        //    var subjectgroupList = new SelectList(list, "SGR_ID", "SGR_DESCR");
        //    return Json(subjectgroupList, JsonRequestBehavior.AllowGet);
        //}
        [HttpGet]
        public JsonResult FetchSubjectCategory(int sbg_id)
        {
            var SubjectCategory = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.SubjectCategory, string.Format("{0}", sbg_id)), "ItemId", "ItemName");
            return Json(SubjectCategory, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchReportSchedule(int rsm_id)
        {
            //var SubjectCategory = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.ReportSchedule, string.Format("{0}", rsm_id)), "ItemId", "ItemName");
            //return Json(SubjectCategory, JsonRequestBehavior.AllowGet);
            //added by syed 20JAN2020
            var ReportSchedule = new SelectList(_AssessmentService.GetReportSchedule(rsm_id), "ItemId", "ItemName");
            return Json(ReportSchedule, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchTerms(int acd_id)
        {
            var terms = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.Terms, string.Format("{0}", acd_id)), "ItemId", "ItemName");
            return Json(terms, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchAssessmentActivity()
        {
            //var Userdetail = Helpers.CommonHelper.GetLoginUser();
            //var schoolid = Userdetail.SchoolId;
            var activity = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AssessmentActivity, string.Format("{0}", SessionHelper.CurrentSession.SchoolId)), "ItemId", "ItemName");
            return Json(activity, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetStudentList(string GRD_ID, Int32 ACD_ID, Int32 SGR_ID, Int32 SCT_ID)
        {
            var list = _AssessmentService.GetStudentList(GRD_ID, ACD_ID, SGR_ID, SCT_ID);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetReportHeaders(string GRD_ID, Int32 ACD_ID, Int32 SBG_ID, Int32 RPF_ID, Int32 RSM_ID, string prv = "")
        {
            List<ReportHeader> objhdr = new List<ReportHeader>();
            if (!string.IsNullOrWhiteSpace(prv))
            {
                var ids = prv.Replace(",", "|");
                objhdr = _AssessmentService.GetReportHeaders(GRD_ID, ACD_ID, SBG_ID, RPF_ID, RSM_ID, ids).ToList();
            }
            var list = _AssessmentService.GetReportHeaders(GRD_ID, ACD_ID, SBG_ID, RPF_ID, RSM_ID, "");
            objhdr.AddRange(list);
            return Json(objhdr, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetReportHeadersDropdowns(Int32 RSM_ID, Int32 SBG_ID, Int32 RSD_ID)
        {
            var list = _AssessmentService.GetReportHeadersDropdowns(RSM_ID, SBG_ID, RSD_ID);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetAssessmentData(Int32 ACD_ID, Int32 SBG_ID, Int32 RPF_ID, Int32 RSM_ID, string prv = "")
        {
            List<AssessmentData> objhdr = new List<AssessmentData>();
            if (!string.IsNullOrWhiteSpace(prv))
            {
                var ids = prv.Replace(",", "|");
                objhdr = _AssessmentService.GetAssessmentData(ACD_ID, SBG_ID, RPF_ID, RSM_ID, ids).ToList();
            }
            var list = _AssessmentService.GetAssessmentData(ACD_ID, SBG_ID, RPF_ID, RSM_ID, "");
            objhdr.AddRange(list);
            var jsonResult = Json(objhdr, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;

        }
        [HttpPost]
        [ValidateInput(false)]
        
        public ActionResult InsertAssessmentData(string student_xml, int bEdit)
        {
            var currentUser = Helpers.CommonHelper.GetLoginUser();
            var username = currentUser.UserName;
            var result = _AssessmentService.InsertAssessmentData(student_xml, username, bEdit);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHeaderBySubjectCategory(long id)
        {
            List<GetHeaderBySubjectCategory> objAssessmentComment = new List<GetHeaderBySubjectCategory>();
            objAssessmentComment = _AssessmentService.GetHeaderBySubjectCategory(id).ToList();
            return Json(objAssessmentComment, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCommentByCategoryId(int CAT_ID = 0, long STU_ID = 0)
        {
            List<AssessmentComments> objAssessmentComment = new List<AssessmentComments>();
            objAssessmentComment = _AssessmentService.GetAssessmentComments(CAT_ID, STU_ID).ToList();
            if (objAssessmentComment.Count > 0)
            {
                return PartialView("_AssessmentCommentGrid", objAssessmentComment);
            }
            return null;
        }
        public ActionResult getAssessmentCategory(string CAT_GRD_ID = "")
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var Categories = _AssessmentService.GetAssessmentCategories(schoolid, "").ToList();
            if (Categories.Count > 0)
            {
                List<SelectListItem> listOfCategories = Categories.ConvertAll(a =>
                {
                    return new SelectListItem()
                    {
                        Text = a.CAT_DESC,
                        Value = a.CAT_ID.ToString(),
                        Selected = false
                    };
                });

                ViewBag.AssessmentCategories = Categories;
                return Json(listOfCategories, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        [HttpGet]
        public JsonResult GetSectionAccess(long ACD_ID, string GRD_ID)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _AssessmentService.GetSectionAccess(username, isSuperUser, ACD_ID, Convert.ToInt32(schoolid), grd_access, GRD_ID);
            var gradesList = new SelectList(list, "SCT_ID", "SCT_DESCR");
            return Json(gradesList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetReportHeaderOptional(string AOD_IDs)
        {
            //var list = _AssessmentService.GetReportHeaderOptional(AOD_IDs);
            var ids = AOD_IDs.Replace(",", "|");
            var list = _AssessmentService.GetReportHeaderOptional(ids);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetAssessmentDataOptional(long ACD_ID, long RPF_ID, long RSM_ID, long SBG_ID, long SGR_ID, string GRD_ID, long SCT_ID, string AOD_IDs)
        {
            var AODIDS = AOD_IDs.Replace(",", "|");
            var list = _AssessmentService.GetAssessmentDataOptional(ACD_ID, RPF_ID, RSM_ID, SBG_ID, SGR_ID, GRD_ID, SCT_ID, AODIDS);
            // var list = _AssessmentService.GetAssessmentDataOptional(0, 0, 0, 0, 0, "0", 0, "1|2|3|4|5|6");          
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult IsReportPublish(long ACD_ID = 0, long RPF_ID = 0, long RSM_ID = 0, string GRD_ID = "", long SCT_ID = 0, long TRM_ID = 0)
        {
            var IsReportPublish = _AssessmentService.IsReportPublish(RSM_ID, RPF_ID, ACD_ID, GRD_ID, SCT_ID, TRM_ID);
            return Json(IsReportPublish, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAssessmentOptionalLists(long ACD_ID = 0, string GRD_ID = "")
        {
            var BSU_ID = SessionHelper.CurrentSession.SchoolId;
            var jsonPreviousResult = _AssessmentService.GetAssessmentPreviousSchedule(ACD_ID, GRD_ID).ToList();
            var jsonOptionResult = _AssessmentService.GetAssessmentOptionList(BSU_ID, ACD_ID).ToList();
            var result = new { PreviousResult = jsonPreviousResult, OptionResult = jsonOptionResult };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        // GET: SMS/Assessment/MarkEntry
        #region MARKENTRY
        public ActionResult MarkEntry()
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = SessionHelper.CurrentSession.SchoolId;//Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var clm_id = SessionHelper.CurrentSession.CLM_ID;  //new to get this value from some generic class .
            var academicYears = _SIMSCommonService.GetAcademicYearList(schoolid.ToString(), clm_id, null);
            ViewBag.AcademicYear = new SelectList(academicYears, "ItemId", "ItemName", academicYears.FirstOrDefault(x => x.Selected == true).ItemId);
            var strtt_id = "";
            if (Session["SIMSTimeTableSchedule"] != null)
            {
                strtt_id = Convert.ToString(Session["SIMSTimeTableSchedule"]);
                if (!string.IsNullOrWhiteSpace(strtt_id))
                {
                    if (!strtt_id.Equals("0"))
                    {

                        ViewBag.strtt_id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
                    }
                }
            }
            //var list = _AssessmentService.GetAssessmentActivityList(1273, 0, "01", 1, 1, 1, 1, 1, "charan", "N");
            return View("MarkEntry");
        }
        public ActionResult ViewListMArkEntry(long ACD_ID = 0, long CAM_ID = 0, string GRD_ID = "", long STM_ID = 0, long TRM_ID = 0, long SGR_ID = 0, long SBG_ID = 0, int GRADE_ACCESS = 0)
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            List<MarkEntry> objListMarkEntry = new List<MarkEntry>();
            objListMarkEntry = _AssessmentService.GetAssessmentActivityList(ACD_ID, CAM_ID, GRD_ID, STM_ID, TRM_ID, SGR_ID, SBG_ID, Convert.ToInt32(curr_role.GSA_ID), username, isSuperUser).ToList();
            return PartialView("_AssessmentViewMarkList", objListMarkEntry);
        }
        public ActionResult GetMarkEntryAOL(bool IsAOLEXAM, long CAS_ID = 0, bool Isbwithoutskill = false, long SlabId = 0, string Type = "", double minMarks = 0.0, double maxMarks = 0.0)
        {
            List<MarkEntryAOLData> objListOfmarkEntryAOLData = new List<MarkEntryAOLData>();
            List<MarkEntryAOLData> objlistofmarkentryAOI = new List<MarkEntryAOLData>();
            List<SkillSET> objskillset = new List<SkillSET>();
            ViewBag.CAS_ID = CAS_ID;
            if (IsAOLEXAM)
            {
                //LIST OF MARKENTRYAOI AS PER CAD_ID 
                objlistofmarkentryAOI = _AssessmentService.GetMarkEntryAOLData(CAS_ID).ToList();
                //-----------ADDED THE LIST OF STUDENT FOR AOI----------------//
                var markAOLGroup = objlistofmarkentryAOI.GroupBy(e => new
                {
                    e.STU_NAME,
                    e.STU_NO,
                    e.STA_ID,
                    e.STU_ID,
                    e.MARKS,
                    e.STA_GRADE,
                    e.IS_ENABLED,
                    e.WITHOUTSKILLS_GRADE,
                    e.WITHOUTSKILLS_MAXMARKS,
                    e.CHAPTER,
                    e.FEEDBACK,
                    e.WOT,
                    e.TARGET,
                    e.bATTENDED,
                    e.STU_STATUS,
                    e.WITHOUTSKILLS_MARKS,

                })
                 .Select(f => new
                 {
                     f.Key.STU_NAME,
                     f.Key.STU_NO,
                     f.Key.STA_ID,
                     f.Key.STU_ID,
                     f.Key.MARKS,
                     f.Key.STA_GRADE,
                     f.Key.IS_ENABLED,
                     f.Key.WITHOUTSKILLS_GRADE,
                     f.Key.WITHOUTSKILLS_MAXMARKS,
                     f.Key.CHAPTER,
                     f.Key.FEEDBACK,
                     f.Key.WOT,
                     f.Key.TARGET,
                     f.Key.bATTENDED,
                     f.Key.STU_STATUS,
                     f.Key.WITHOUTSKILLS_MARKS,
                 });

                foreach (var i in markAOLGroup)
                {
                    objListOfmarkEntryAOLData.Add(new MarkEntryAOLData
                    {
                        bATTENDED = i.bATTENDED,
                        CHAPTER = i.CHAPTER,
                        FEEDBACK = i.FEEDBACK,
                        IS_ENABLED = i.IS_ENABLED,
                        MARKS = i.MARKS,
                        STA_GRADE = i.STA_GRADE,
                        STA_ID = i.STA_ID,
                        STU_ID = i.STU_ID,
                        STU_NAME = i.STU_NAME,
                        STU_NO = i.STU_NO,
                        STU_STATUS = i.STU_STATUS,
                        TARGET = i.TARGET,
                        WITHOUTSKILLS_GRADE = i.WITHOUTSKILLS_GRADE,
                        WITHOUTSKILLS_MARKS = i.WITHOUTSKILLS_MARKS,
                        WITHOUTSKILLS_MAXMARKS = i.WITHOUTSKILLS_MAXMARKS,
                        WOT = i.WOT,
                    });
                }
                //-------EXTRACTED SKILL_NAME SKILL_MAXMARKS-----------------//
                var SkillsMarkAOLGroup = objlistofmarkentryAOI.GroupBy(e => new { e.SKILL_NAME }).Select(f => new { f.Key.SKILL_NAME, SKILL_MAX_MARK = f.Sum(x => x.SKILL_MAX_MARK) });
                foreach (var set in SkillsMarkAOLGroup)
                {
                    var SkillMarks = Convert.ToDouble(set.SKILL_MAX_MARK / objListOfmarkEntryAOLData.Count());
                    objskillset.Add(new SkillSET
                    {
                        SKILL_MAX_MARK = SkillMarks,// set.SKILL_MAX_MARK/ objListOfmarkEntryAOLData.Count(),
                        SKILL_NAME = set.SKILL_NAME,
                    });
                }

                ViewBag.SKillSET = objskillset;
                ViewBag.lstmarkofAOI = objlistofmarkentryAOI;
                ViewBag.Isbwithoutskill = Isbwithoutskill;
                // ViewBag.CAS_ID = CAS_ID;
                return PartialView("_AssessmentMarkEntryAOL", objListOfmarkEntryAOLData);
            }
            else
            {
                List<MarkEntryData> objMarkEntryData = new List<MarkEntryData>();
                objMarkEntryData = _AssessmentService.GetMarkEntryData(CAS_ID, minMarks, maxMarks).ToList();
                ViewBag.SlabId = SlabId;
                ViewBag.EntryType = Type;
                return PartialView("_AssessmentMarkEntryData", objMarkEntryData);
            }
        }
        public ActionResult GetMarkEntryData(bool IsAOLEXAM, long CAS_ID = 0, bool Isbwithoutskill = false, long SlabId = 0, string Type = "")
        {
            ViewBag.CAS_ID = CAS_ID;
            List<MarkEntryData> objMarkEntryData = new List<MarkEntryData>();
            objMarkEntryData = _AssessmentService.GetMarkEntryData(1276616, 25, 8.25).ToList();
            ViewBag.SlabId = SlabId;
            ViewBag.EntryType = Type;
            return PartialView("_AssessmentMarkEntryData", objMarkEntryData);
        }

        
        public ActionResult SaveMarkEntryData(List<MarkEntryData> objListofMarkEntryData, long slabId = 0, string entryType = "", long CAS_ID = 0)
        {
            if (objListofMarkEntryData.Count > 0)
            {
                var IsActionPerformed = _AssessmentService.InsertMarkEntryData(objListofMarkEntryData, slabId, entryType, CAS_ID);
                if (IsActionPerformed)
                {
                    return Json(new OperationDetails(IsActionPerformed), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new OperationDetails(IsActionPerformed), JsonRequestBehavior.AllowGet);
                }
            }
            return null;
        }

        public ActionResult MarkAttendanceDetails(bool IsAOLEXAM, long CAS_ID = 0, bool Isbwithoutskill = false, long SlabId = 0, string Type = "", double minMarks = 0.0, double maxMarks = 0.0, string CAS_DESC = "")
        {
            List<MarkEntryAOLData> objListOfmarkEntryAOLData = new List<MarkEntryAOLData>();
            List<MarkEntryAOLData> objlistofmarkentryAOI = new List<MarkEntryAOLData>();
            List<MarkAttendance> objMarkAttendance = new List<MarkAttendance>();
            ViewBag.CAS_ID = CAS_ID;
            ViewBag.CAS_DESC = CAS_DESC;
            if (IsAOLEXAM)
            {
                //LIST OF MARKENTRYAOI AS PER CAD_ID 
                objlistofmarkentryAOI = _AssessmentService.GetMarkEntryAOLData(CAS_ID).ToList();
                objMarkAttendance = objlistofmarkentryAOI.Select(e => new MarkAttendance
                {
                    bATTENDED = e.bATTENDED,
                    CAS_ID = CAS_ID,
                    SlabId = SlabId,
                    STA_ID = e.STA_ID,
                    STU_ID = e.STU_ID,
                    STU_NAME = e.STU_NAME,
                    STU_NO = e.STU_NO

                }).DistinctBy(x => x.STU_ID).ToList();
                // ViewBag.CAS_ID = CAS_ID;
                return PartialView("_MarkAttendance", objMarkAttendance);
            }
            else
            {
                List<MarkEntryData> objMarkEntryData = new List<MarkEntryData>();
                objMarkEntryData = _AssessmentService.GetMarkEntryData(CAS_ID, minMarks, maxMarks).ToList();
                objMarkAttendance = objMarkEntryData.Select(e => new MarkAttendance
                {
                    bATTENDED = e.bATTENDED,
                    CAS_ID = CAS_ID,
                    SlabId = SlabId,
                    STA_ID = e.STA_ID,
                    STU_ID = e.STU_ID,
                    STU_NAME = e.STU_NAME,
                    STU_NO = e.STU_NO

                }).DistinctBy(x => x.STU_ID).ToList();
                return PartialView("_MarkAttendance", objMarkAttendance);
            }
        }


        
        public ActionResult SaveMarkAttendance(List<MarkAttendance> markAttendance, long CAS_ID = 0)
        {

            var result = _AssessmentService.UpdateMarkAttendance(markAttendance, CAS_ID);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult SaveMarkEntryList(List<MarkEntryAOLData> markEntryAOLData, bool Isbwithoutskill = false, long CAS_ID = 0)
        {
            var currentUser = Helpers.CommonHelper.GetLoginUser();
            var username = currentUser.UserName;
            var isActionPerformed = _AssessmentService.InsertMarkEntryAOLData(markEntryAOLData, username, Isbwithoutskill, CAS_ID);
            return Json(new OperationDetails(isActionPerformed), JsonRequestBehavior.AllowGet);

        }




        #endregion

        #region Grade Book
        public ActionResult GradeBook()
        {
            int ACD_ID = Phoenix.Common.Helpers.SessionHelper.CurrentSession.ACD_ID;
            var schoolid = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            var clm_id = Phoenix.Common.Helpers.SessionHelper.CurrentSession.CLM_ID;
            var username = Phoenix.Common.Helpers.SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var Categories = _AssessmentService.GetAssessmentCategories(SessionHelper.CurrentSession.SchoolId, "").ToList();
            ViewBag.AssessmentCategories = Categories.Select(x => new SelectListItem { Text = x.CAT_DESC, Value = x.CAT_ID.ToString() });
            ViewBag.AcademicYear = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AcademicYear, $"{schoolid},{clm_id},{null}"), "ItemId", "ItemName", ACD_ID);
            var strtt_id = "";
            if (Session["SIMSTimeTableSchedule"] != null)
            {
                strtt_id = Convert.ToString(Session["SIMSTimeTableSchedule"]);
                if (!string.IsNullOrWhiteSpace(strtt_id))
                {
                    if (!strtt_id.Equals("0"))
                    {
                        ViewBag.strtt_id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
                    }
                }
            }
            return View();
        }
        public ActionResult LoadGradBookTopFilter(int ACD_ID = 0, int RSM_ID = 0, string GRD_ID = "", int SBG_ID = 0, string DropdownId = "")
        {
            List<object> ddlList = new List<object>();
            var schoolid = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            var username = Phoenix.Common.Helpers.SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            //var GradeList = _SIMSCommonService.GetGradesAccess(username, isSuperUser, ACD_ID, Convert.ToInt32(schoolid), Convert.ToInt32(curr_role.GSA_ID)).Select(x => new ListItem { Value = x.GRD_ID.ToString(), Text = x.GRM_DISPLAY }).ToList();                        
            if (!string.IsNullOrEmpty(DropdownId))
            {
                if (DropdownId.ToLower() == "academicyear")
                {
                    var TermList = SelectSIMSListHelper.GetSelectListData(ListItems.Terms, string.Format("{0}", ACD_ID)).Select(x => new ListItem { Value = x.ItemId.ToString(), Text = x.ItemName }).ToList();
                    var ReportHeaderList = SelectSIMSListHelper.GetSelectListData(ListItems.ReportHeader, string.Format("{0},{1}", schoolid, ACD_ID)).Select(x => new ListItem { Value = x.ItemId.ToString(), Text = x.ItemName }).ToList();
                    ddlList.Add(TermList);
                    ddlList.Add(ReportHeaderList);
                }
                else if (DropdownId.ToLower() == "reportheader")
                {
                    var GradeList = _SIMSCommonService.GetGradesAccess(username, isSuperUser, ACD_ID, Convert.ToInt32(schoolid), Convert.ToInt32(curr_role.GSA_ID), RSM_ID).Select(x => new ListItem { Value = x.GRD_ID.ToString(), Text = x.GRM_DISPLAY }).ToList();
                    var ReportScheduleList = SelectSIMSListHelper.GetSelectListData(ListItems.ReportSchedule, string.Format("{0}", RSM_ID)).Select(x => new ListItem { Value = x.ItemId.ToString(), Text = x.ItemName }).ToList();
                    ddlList.Add(GradeList);
                    ddlList.Add(ReportScheduleList);
                }
                else if (DropdownId.ToLower() == "grade")
                {
                    var SubjectList = _SIMSCommonService.GetSubjectsByGrade(ACD_ID, GRD_ID, username, isSuperUser).Select(x => new ListItem { Value = x.SBG_ID.ToString(), Text = x.SBG_DESCR }).ToList();
                    ddlList.Add(SubjectList);
                }
                //else if (DropdownId.ToLower() == "subject")
                //{
                //    var SubjectGroupList = _SIMSCommonService.GetSubjectGroupBySubject(username, isSuperUser, Convert.ToInt32(schoolid), GRD_ID, SBG_ID).Select(x => new ListItem { Value = x.SGR_ID.ToString(), Text = x.SGR_DESCR }).ToList();
                //    ddlList.Add(SubjectGroupList);
                //}
            }
            return Json(ddlList, JsonRequestBehavior.AllowGet);
        }

        #region Grade Book Setup
        public ActionResult LoadGradeBook(GradeBookSetup gradeBookSetup)
        {
            gradeBookSetup.GBM_BSU_ID = SessionHelper.CurrentSession.SchoolId;
            var GradeBookList = _AssessmentService.GetGradeBookSetupList(gradeBookSetup);
            return PartialView("_LoadGradeBook", GradeBookList);
        }
        public ActionResult BindGradeBookSetup(GradeBookSetup gradeBookSetup)
        {
            gradeBookSetup.GBM_ENTRY_MODE = "M";
            var schoolid = Phoenix.Common.Helpers.SessionHelper.CurrentSession.SchoolId;
            ViewBag.ReportHeaderList = new SelectList(_AssessmentService.GetReportHeaderByRSMID(gradeBookSetup.GBM_RSM_ID), "RSD_ID", "Header_Description");
            ViewBag.MarksTypeList = new SelectList(AssessmentConfiguration.MarksTypeList, "Value", "Text");
            ViewBag.EntryModeList = new SelectList(GradeBookSetup.EntryModeList, "Value", "Text");
            gradeBookSetup.GBM_DUE_DATE = DateTime.Today;
            gradeBookSetup.GBM_PUBLISH_DATE = DateTime.Today;
            if (gradeBookSetup.GBM_ID > 0)
            {
                gradeBookSetup.GBM_BSU_ID = schoolid;
                var GradeBookList = _AssessmentService.GetGradeBookSetupList(gradeBookSetup);
                if (GradeBookList.Where(x => x.GBM_ID == gradeBookSetup.GBM_ID).Any())
                {
                    gradeBookSetup = GradeBookList.Where(x => x.GBM_ID == gradeBookSetup.GBM_ID).FirstOrDefault();
                }
            }
            ViewBag.GradeScaleList = new SelectList(_AssessmentService.GetGradeScaleList(gradeBookSetup.GBM_BSU_ID, gradeBookSetup.GBM_ACD_ID, gradeBookSetup.GBM_TEACHER_ID), "GSM_ID", "GSM_DESCRIPTION");
            return PartialView("_BindGradeBookSetup", gradeBookSetup);
        }
        
        public ActionResult SaveGradeBookSetup(GradeBookSetup gradeBookSetup)
        {
            var DATAMODE = gradeBookSetup.GBM_ID > 0 ? Edit : Add;
            gradeBookSetup.GBM_BSU_ID = SessionHelper.CurrentSession.SchoolId;
            gradeBookSetup.GBM_TEACHER_ID = SessionHelper.CurrentSession.Id;
            gradeBookSetup.GBM_CREATED_BY = SessionHelper.CurrentSession.UserName;
            gradeBookSetup.GBM_PUBLISH_DATE = gradeBookSetup.GBM_bON_PORTAL ? gradeBookSetup.GBM_PUBLISH_DATE : DateTime.Today;
            var result = _AssessmentService.SaveGradeBookSetup(gradeBookSetup, DATAMODE);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteGradeBookSetup(long GBM_ID)
        {
            GradeBookSetup gradeBookSetup = new GradeBookSetup();
            gradeBookSetup.GBM_ID = GBM_ID;
            gradeBookSetup.GBM_DUE_DATE = DateTime.Today;
            gradeBookSetup.GBM_PUBLISH_DATE = DateTime.Today;
            gradeBookSetup.GBM_CREATED_BY = string.Empty;
            gradeBookSetup.GBM_DESCR = string.Empty;
            gradeBookSetup.GBM_ENTRY_MODE = string.Empty;
            gradeBookSetup.GBM_GRD_ID = string.Empty;
            gradeBookSetup.GBM_MARK_TYPE = string.Empty;
            gradeBookSetup.GBM_SHORT_CODE = string.Empty;
            var result = _AssessmentService.SaveGradeBookSetup(gradeBookSetup, Delete);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadGradeBookScale()
        {
            var BSU_ID = SessionHelper.CurrentSession.SchoolId;
            var ACD_ID = SessionHelper.CurrentSession.ACD_ID;
            var TEACHER_ID = SessionHelper.CurrentSession.Id;
            List<GradeBookGradeScale> GradeBookGradeScaleList = new List<GradeBookGradeScale>();
            GradeBookGradeScaleList = _AssessmentService.GetGradeScaleList(BSU_ID, ACD_ID, TEACHER_ID).ToList();
            return PartialView("_LoadGradeBookScale", GradeBookGradeScaleList);
        }
        
        public ActionResult SaveGradeScaleAndDetail(GradeBookGradeScale gradeBookGradeScale)
        {
            var DATAMODE = gradeBookGradeScale.GSM_ID > 0 ? Edit : Add;
            gradeBookGradeScale.GSM_BSU_ID = SessionHelper.CurrentSession.SchoolId;
            gradeBookGradeScale.GSM_ACD_ID = SessionHelper.CurrentSession.ACD_ID;
            gradeBookGradeScale.GSM_TEACHER_ID = SessionHelper.CurrentSession.Id;
            var result = _AssessmentService.SaveGradeScaleAndDetail(gradeBookGradeScale, DATAMODE);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetGradeScaleDetailList(long GSM_ID)
        {
            List<GradeBookGradeScaleDetail> GradeBookGradeScaleDetailList = new List<GradeBookGradeScaleDetail>();
            GradeBookGradeScaleDetailList = _AssessmentService.GetGradeScaleDetailList(GSM_ID).ToList();
            return Json(GradeBookGradeScaleDetailList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BindGradeScaleDropdownList()
        {
            var BSU_ID = SessionHelper.CurrentSession.SchoolId;
            var ACD_ID = SessionHelper.CurrentSession.ACD_ID;
            var TEACHER_ID = SessionHelper.CurrentSession.Id;
            var GradeScaleList = _AssessmentService.GetGradeScaleList(BSU_ID, ACD_ID, TEACHER_ID).Select(x => new ListItem { Value = x.GSM_ID.ToString(), Text = x.GSM_DESCRIPTION }).ToList();
            return Json(GradeScaleList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteGradeScaleAndDetail(long GSM_ID)
        {
            GradeBookGradeScale gradeBookGradeScale = new GradeBookGradeScale();
            gradeBookGradeScale.GSM_ID = GSM_ID;
            gradeBookGradeScale.GSM_BSU_ID = SessionHelper.CurrentSession.SchoolId;
            gradeBookGradeScale.GSM_ACD_ID = SessionHelper.CurrentSession.ACD_ID;
            gradeBookGradeScale.GSM_TEACHER_ID = SessionHelper.CurrentSession.Id;
            gradeBookGradeScale.GSM_DESCRIPTION = string.Empty;
            var result = _AssessmentService.SaveGradeScaleAndDetail(gradeBookGradeScale, Delete);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult BindProcessingRuleSetup(GradeBookSetup gradeBookSetup)
        {
            ProcessingRuleSetup processingRuleSetup = new ProcessingRuleSetup();
            processingRuleSetup.PRS_RSD_ID = gradeBookSetup.GBM_RSD_ID;
            gradeBookSetup.GBM_BSU_ID = SessionHelper.CurrentSession.SchoolId;
            ViewBag.AssessmentList = new SelectList(_AssessmentService.GetGradeBookSetupList(gradeBookSetup).ToList(), "GBM_ID", "GBM_DESCR");
            ViewBag.CalculationTypeList = new SelectList(ProcessingRuleSetup.CalculationTypeList, "Value", "Text");
            var ProcessingRuleSetupList = _AssessmentService.GetProcessingRuleSetupList(gradeBookSetup.GBM_RSD_ID).ToList();
            if (ProcessingRuleSetupList.Any())
            {
                processingRuleSetup = ProcessingRuleSetupList.FirstOrDefault();
                if (!string.IsNullOrEmpty(processingRuleSetup.PRS_GBM_IDS))
                    processingRuleSetup.selectedPRS_GBM_IDS = processingRuleSetup.PRS_GBM_IDS.Split('|');
            }
            return PartialView("_BindProcessingRuleSetup", processingRuleSetup);
        }
        
        public ActionResult SaveProcessingRuleSetup(ProcessingRuleSetup processingRuleSetup)
        {
            var DATAMODE = processingRuleSetup.PRS_ID > 0 ? Edit : Add;
            processingRuleSetup.PRS_MODIFIED_BY = SessionHelper.CurrentSession.UserName;
            var result = _AssessmentService.SaveProcessingRuleSetup(processingRuleSetup, DATAMODE);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Grade Book Entry 

        //public JsonResult GetDefaultListById(long RDM_ID)
        //{
        //    var result = assessmentSettingService.GetDefaultListById(RDM_ID).Select(x => new ListItem { Text = x.RDD_DESCR, Value = x.RDD_ID.ToString() }).OrderBy(x => x.Text);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult GradeBookCUD(GradeBookEntryListModel gradeBookEntry)
        {
            gradeBookEntry.GradeBookDetails.ToList().ForEach(x =>
            {
                x.GBD_MODIFIED_BY = SessionHelper.CurrentSession.Id.ToString();
                x.GBD_MODIFIED_ON = DateTime.Now;
            });
            gradeBookEntry.Username = SessionHelper.CurrentSession.UserName;
            var result = _AssessmentService.GradeBookCUD(gradeBookEntry);
            return Json(new OperationDetails(Convert.ToBoolean(result)));
        }

        #region Grade Book Entry Partial Views
        public PartialViewResult GetGradeBookEntryForm(GradeBookSetup gradeBook)
        {
            gradeBook.GBM_BSU_ID = SessionHelper.CurrentSession.SchoolId;
            GradeBookEntryListModel entryListModel = new GradeBookEntryListModel();
            var headerObj = new List<ReportHeader>();
            var assessmentDataObject = new List<AssessmentData>();

            entryListModel.StudentLists = _AssessmentService.GetStudentList(gradeBook.GBM_GRD_ID, (int)gradeBook.GBM_ACD_ID, (int)gradeBook.GBM_SGR_ID, gradeBook.GBM_SCT_ID);
            entryListModel.HeaderOptionals = _AssessmentService.GetReportHeaderOptional(gradeBook.GBM_AOD_IDS);
            entryListModel.AssessmentDataOptionals = _AssessmentService.GetAssessmentDataOptional(gradeBook.GBM_ACD_ID, gradeBook.GBM_RPF_ID, gradeBook.GBM_RSM_ID, gradeBook.GBM_SBG_ID, gradeBook.GBM_SGR_ID, gradeBook.GBM_GRD_ID, gradeBook.GBM_SCT_ID, gradeBook.GBM_AOD_IDS);

            if (!string.IsNullOrWhiteSpace(gradeBook.GBM_PRV_SCHEDULE))
            {
                headerObj = _AssessmentService.GetReportHeaders(gradeBook.GBM_GRD_ID, (int)gradeBook.GBM_ACD_ID, (int)gradeBook.GBM_SBG_ID, (int)gradeBook.GBM_RPF_ID, (int)gradeBook.GBM_RSM_ID, gradeBook.GBM_PRV_SCHEDULE, ReportHeaderType.GradeBook).ToList();
                assessmentDataObject = _AssessmentService.GetAssessmentData((int)gradeBook.GBM_ACD_ID, (int)gradeBook.GBM_SBG_ID, (int)gradeBook.GBM_RPF_ID, (int)gradeBook.GBM_RSM_ID, gradeBook.GBM_PRV_SCHEDULE).ToList();
            }

            var assessmentDatalist = _AssessmentService.GetAssessmentData((int)gradeBook.GBM_ACD_ID, (int)gradeBook.GBM_SBG_ID, (int)gradeBook.GBM_RPF_ID, (int)gradeBook.GBM_RSM_ID, "");
            assessmentDataObject.AddRange(assessmentDatalist);

            var headerList = _AssessmentService.GetReportHeaders(gradeBook.GBM_GRD_ID, (int)gradeBook.GBM_ACD_ID, (int)gradeBook.GBM_SBG_ID, (int)gradeBook.GBM_RPF_ID, (int)gradeBook.GBM_RSM_ID, "", ReportHeaderType.GradeBook);
            headerObj.AddRange(headerList);

            entryListModel.GradeBookSetups = _AssessmentService.GetGradeBookSetupList(gradeBook);
            var gradeBookDetails = new List<GradeBookDetail>();
            foreach (var headerObject in headerObj)
            {
                if (!headerObject.IS_PREV)
                {
                    gradeBookDetails.AddRange(_AssessmentService.GetGradebookDetail(Convert.ToInt64(headerObject.RSD_ID)));
                }
            }
            entryListModel.GradeBookDetails = gradeBookDetails;

            entryListModel.ReportHeaders = headerObj;
            entryListModel.AssessmentDatas = assessmentDataObject;
            ViewBag.SelectedOptions = gradeBook.SelectedOptions;
            return PartialView("_LoadGradeBookEntry", entryListModel);
        }
        #endregion

        #endregion

        #endregion


        #region Report Writing

        public ActionResult GetReportWriting(int acdId, string gradeId, int rpfId, string studentId, int rsmId, long sectionId)
        {
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(SessionHelper.CurrentSession.SchoolId.ToString(), SessionHelper.CurrentSession.UserName);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var result = _AssessmentService.GetSubjectsForReportWriting(acdId, studentId.ToLong(), isSuperUser, SessionHelper.CurrentSession.Id).ToList();
            //var result2 = GetSubjectByGrade(acdId, gradeId);
            var savedRecords = _AssessmentService.GetSavedRecordsOfReportWriting(rpfId, studentId.ToLong());
            var subjects = result.Any() ? string.Join("|", result.Select(x => x.SBG_ID.ToString())) : string.Empty;
            var data = _AssessmentService.GetReportHeaders(gradeId, acdId, 0, rpfId, rsmId, subjects, ReportHeaderType.ReportWriting);
            foreach (var item in result)
            {
                item.Header = new List<ReportHeader>();
                if (data.Any(x => x.SBG_ID == item.SBG_ID))
                    item.Header.AddRange(data.Where(x => x.SBG_ID == item.SBG_ID));
                item.Header.AddRange(data.Where(x => x.SBG_ID == 0));
                item.Header.RemoveAll(x => x.RSD_RESULT.ToLower() == "d" && string.IsNullOrEmpty(x.RSP_DESCR));
            }
            result.RemoveAll(x => x.Header.Count <= 0);
            var studentDetails = _classListService.GetStudentDetails(studentId);
            return PartialView("_ReportWriting", new ReportWriting
            {
                Subjects = result,
                StudentDetails = studentDetails,
                StudentReportWritings = savedRecords,
                AcdId = acdId,
                GradeId = gradeId,
                RpfId = rpfId,
                RsmId = rsmId,
                SectionId = sectionId,
                StudentId = studentId
            });
        }

        public JsonResult ReportWritingCU(List<AssessmentData> assessmentDatas)
        {
            assessmentDatas.RemoveAll(x => string.IsNullOrEmpty(x.COMMENTS) && string.IsNullOrEmpty(x.GRADING) && string.IsNullOrEmpty(x.MARK));
            var result = _AssessmentService.ReportWritingCU(assessmentDatas);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region private Methods

        public IEnumerable<Phoenix.VLE.Web.Models.Subjects> GetSubjectByGrade(int academicYearId, string gradeId)
        {
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(SessionHelper.CurrentSession.SchoolId.ToString(), SessionHelper.CurrentSession.UserName);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var list = _SIMSCommonService.GetSubjectsByGrade(academicYearId, gradeId, SessionHelper.CurrentSession.UserName, isSuperUser);
            return list;
        }

        #endregion
    }
}