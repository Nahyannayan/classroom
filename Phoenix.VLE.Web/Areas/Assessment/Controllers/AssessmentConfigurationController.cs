﻿using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Assessment.Controllers
{
    public class AssessmentConfigurationController : Controller
    {
        private ISIMSCommonService _SIMSCommonService;
        private IAssessmentConfigurationService assessmentConfigurationService;
        private IAssessmentConfigService _AssessmentConfigService;

        private const string Add = "ADD";
        private const string Edit = "EDIT";
        private const string Delete = "DELETE";

        public AssessmentConfigurationController(ISIMSCommonService SIMSCommonService, IAssessmentConfigurationService _assessmentConfigurationService, IAssessmentConfigService assessmentConfigService)
        {
            _SIMSCommonService = SIMSCommonService;
            assessmentConfigurationService = _assessmentConfigurationService;
            _AssessmentConfigService = assessmentConfigService;
        }

        // GET: Assessment/AssessmentConfiguration
        public ActionResult Index()
        {
            return View();
        }

        #region Assessment Configuration Setting
        public ActionResult AssesmentConfiguration()
        {
            ViewBag.Curriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            return View();
        }

        public PartialViewResult ReportCardGrid(int pageIndex = 1, string searchString = "")
        {
            var model = new Pagination<AssessmentConfigSetting>();
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            var list = assessmentConfigurationService.GetAssessmentConfigPagination(SchoolId, pageIndex, 12, searchString);
            if (list.Any())
                model = new Pagination<AssessmentConfigSetting>(pageIndex, 12, list.ToList(), list.FirstOrDefault().TotalCount);
            model.RecordCount = list.Count() == 0 ? 0 : list.FirstOrDefault().TotalCount;
            model.LoadPageRecordsUrl = "/Assessment/AssessmentConfiguration/ReportCardGrid?pageIndex={0}";
            model.SearchString = searchString;
            return PartialView("_ReportCardGrid", model);
        }

        public JsonResult GetGradeTemplateMasterList(long CurriculumId)
        {
            var list = SelectListHelper.GetSelectListData(ListItems.GradeTemplateMaster, string.Format("{0},{1}", SessionHelper.CurrentSession.SchoolId, CurriculumId));
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAssessmentColumnGrid(long AssessmentMasterId = 0)
        {
            var list = assessmentConfigurationService.GetAssessmentColumnDetail(AssessmentMasterId);
            return PartialView("_AssessmentColumnGrid", list);
        }

        public PartialViewResult ReportCardForm(long AssessmentMasterId = 0)
        {
            AssessmentConfigSetting assessmentConfig = new AssessmentConfigSetting();
            ViewBag.GradeList = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolGrade, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.CurriculumList = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            ViewBag.ControlTypeList = new SelectList(Enum.GetValues(typeof(Phoenix.Common.Enums.ControlType)).Cast<Phoenix.Common.Enums.ControlType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            long SchoolId = SessionHelper.CurrentSession.SchoolId;
            if (AssessmentMasterId > 0)
            {
                var list = assessmentConfigurationService.GetAssessmentConfigDetail(AssessmentMasterId, SchoolId);
                if (list.Any())
                    assessmentConfig = list.FirstOrDefault();
            }
            return PartialView("_ReportCardForm", assessmentConfig);
        }

        public ActionResult FinalSaveAssessmentConfig(AssessmentConfigSetting assessmentConfigSetting)
        {
            assessmentConfigSetting.SchoolId = SessionHelper.CurrentSession.SchoolId;
            assessmentConfigSetting.UserId = SessionHelper.CurrentSession.ParentId;
            assessmentConfigSetting.DATAMODE = assessmentConfigSetting.AssessmentMasterId > 0 ? Edit : Add;
            var result = assessmentConfigurationService.SaveAssessmentConfigDetail(assessmentConfigSetting);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCourseListByGrade(string SchoolGradeIds)
        {
            var list = new SelectList(assessmentConfigurationService.GetCourseListByGrade(SchoolGradeIds).ToList(), "CourseId", "Title");
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Grade Template
        public ActionResult GradeTemplate()
        {
            List<GradeTemplate> obj = new List<GradeTemplate>();
            var Result = _AssessmentConfigService.GetAssessmentConfigMasterList(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId)).ToList();
            return View("~/Areas/Assessment/Views/GradeTemplate/Index.cshtml", Result);
        }
        public ActionResult SaveGradeTemplate(List<GradeTemplate> gradingList, string Description, long? GradeTemplateMasterId, string MasterIds)
        {
            var result = _AssessmentConfigService.AddEditAssessmentConfig(gradingList, Description, MasterIds, Convert.ToInt32(GradeTemplateMasterId), SessionHelper.CurrentSession.SchoolId);
            //return View();
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAssessmentConfigList(int MasterId)
        {
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;
            var Result = _AssessmentConfigService.GetAssessmentConfigList(Convert.ToInt32(itemId)).Where(x => x.GradeTemplateMasterId == MasterId).ToList();
            return Json(Result, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetAssessmentLoad()
        {
            var Result = _AssessmentConfigService.GetAssessmentConfigMasterList(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId)).ToList();
            return PartialView("~/Areas/Assessment/Views/GradeTemplate/_GradeTemplate.cshtml", Result);

        }
        #endregion
        #region GradingSlab
        public ActionResult GradingSlab()
        {

            var Result = _AssessmentConfigService.GetGradeSlabMasterList(SessionHelper.CurrentSession.SchoolId).ToList();
            return View("~/Areas/Assessment/Views/GradingSlab/Index.cshtml", Result);
        }
        public ActionResult SaveGradingSlab(List<GradeSlab> obj, string GradeSlabMasterDesc, long? GradeSlabMasterId, string MasterIds)
        {


            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;

            var result = _AssessmentConfigService.AddEditGradeSlab(obj, GradeSlabMasterDesc, MasterIds, Convert.ToInt32(GradeSlabMasterId), Convert.ToInt32(itemId));
            //return View();
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GradeSlabListList(int MasterId)
        {
            var currAcdId = SelectListHelper.GetSelectListData(ListItems.SchoolAcademicYear, SessionHelper.CurrentSession.SchoolId);
            var itemId = currAcdId.Count() > 0 ? currAcdId.Select(e => e.ItemId).FirstOrDefault() : null;
            var Result = _AssessmentConfigService.GetGradeSlabList(Convert.ToInt32(itemId)).Where(x => x.GradeSlabMasterId == MasterId).ToList();
            //return PartialView("_GradeSlab", Result);
            return Json(Result, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GradeSlabListDetails()
        {

            var Result = _AssessmentConfigService.GetGradeSlabMasterList(SessionHelper.CurrentSession.SchoolId).ToList();
            return PartialView("~/Areas/Assessment/Views/GradingSlab/_GradingMasterSlab.cshtml", Result);
        }
        #endregion
    }
}