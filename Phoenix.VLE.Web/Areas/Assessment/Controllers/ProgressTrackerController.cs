﻿using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.Controllers;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Phoenix.VLE.Web.Extensions;
using System.Net;
using System.Globalization;
using Phoenix.Common.ViewModels;
using System.IO.Compression;
using System.Web.Script.Serialization;
//using DocumentFormat.OpenXml.Packaging;
using System.Text.RegularExpressions;
using DevExpress.XtraRichEdit;
using System.Threading;
using Phoenix.VLE.Web.Models;
using Phoenix.Models.Entities;

namespace Phoenix.VLE.Web.Areas.Assessment.Controllers
{
    public class ProgressTrackerController : Controller
    {
        private ISIMSCommonService _SIMSCommonService;
        private readonly IProgressTrackerService _iProgressTrackerService;
        private IAssessmentService _assessmentService;
        private IAttendanceService _attendanceService;
        private IStudentListService _studentListService;
        private IAttachmentService _attachmentService;
        private IGradingTemplateItemService _iGradingTemplateItemService;
        public ProgressTrackerController(IProgressTrackerService iProgressTrackerService, ISIMSCommonService SIMSCommonService, IAssessmentService assessmentService, IStudentListService studentListService, IAttachmentService attachmentService, IGradingTemplateItemService iGradingTemplateItemService)
        {
            _iProgressTrackerService = iProgressTrackerService;
            _SIMSCommonService = SIMSCommonService;
            _assessmentService = assessmentService;
            _studentListService = studentListService;
            _attachmentService = attachmentService;
            _iGradingTemplateItemService = iGradingTemplateItemService;
        }
        // GET: Assessment/ProgressTracker
        public ActionResult Index1()
        {
            //var schoolid = SessionHelper.CurrentSession.SchoolId;//Userdetail.SchoolId;
            //var username = SessionHelper.CurrentSession.UserName;
            //var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            //var grd_access = (int)curr_role.GSA_ID;
            //var clm_id = SessionHelper.CurrentSession.CLM_ID;  //new to get this value from some generic class .
            //                                                   // var current_acd = 1;
            //var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            ////var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, curr_role.ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ////ViewBag.Grades = list;
            //// ViewBag.AgeBand = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AgeBand, null), "ItemId", "ItemName");
            //var academicYears = _SIMSCommonService.GetAcademicYearList(schoolid.ToString(), clm_id, null);
            //ViewBag.AcademicYear = new SelectList(academicYears, "ItemId", "ItemName", academicYears.FirstOrDefault(x => x.Selected == true).ItemId);
            //var strtt_id = "";
            //if (Session["SIMSTimeTableSchedule"] != null)
            //{
            //    strtt_id = Convert.ToString(Session["SIMSTimeTableSchedule"]);
            //    if (!string.IsNullOrWhiteSpace(strtt_id))
            //    {
            //        ViewBag.strtt_id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
            //    }
            //}
            return View();
        }



        public ActionResult CourseGroupByCourseId(long? courseId)
        {
            var courseGroupByCourseId = SelectListHelper.GetSelectListData(ListItems.CourseGroupList, courseId.Value);
            return Json(courseGroupByCourseId, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Progresstrack(string STEPS, int ACD_ID = 0, int SGR_ID = 0, int SCT_ID = 0, long SBG_ID = 0, string TOPIC_ID = "", long AGE_BAND_ID = 0, long TSM_ID = 0, string GRD_ID = "")
        {
            ProgressTracker objProgressTracker = new ProgressTracker();
            List<TopicDetails> objTopics = new List<TopicDetails>();
            List<ObjectiveDetails> objObjectiveDetails = new List<ObjectiveDetails>();
            string lstSteps = string.Empty;
            var schoolid = SessionHelper.CurrentSession.SchoolId;

            if (STEPS != null)
            {
                lstSteps = string.Join("|", STEPS);

            }

            objProgressTracker.objStudentList = _iProgressTrackerService.GetStudentList(GRD_ID, ACD_ID, SGR_ID, SCT_ID).ToList();

            objProgressTracker.objProgressTrackerHeader = _iProgressTrackerService.GET_PROGRESS_TRACKER_HEADERS(SBG_ID, TOPIC_ID, AGE_BAND_ID, lstSteps, TSM_ID).ToList();

            objProgressTracker.objProgressTrackerData = _iProgressTrackerService.GET_PROGRESS_TRACKER_DATA(SBG_ID, TOPIC_ID, TSM_ID, SGR_ID).ToList();
            //separated unique topics
            var topics = objProgressTracker.objProgressTrackerHeader.GroupBy(e => new { e.TOPIC }).Select(f => new Topics { topics = f.Key.TOPIC }).ToList();
            objProgressTracker.objTopics.AddRange(topics);

            //to get distinct ids
            var TopicIds = objProgressTracker.objProgressTrackerHeader.Select(e => e.TOPIC_ID).Distinct().ToList();

            //grouping done to obtain topic details
            var grpresult = objProgressTracker.objProgressTrackerHeader.GroupBy(test => test.TOPIC_ID)
                   .Select(grp => grp.First())
                   .ToList();
            var lqTopicDetails = grpresult.Select(e => new TopicDetails
            {
                TopicId = e.TOPIC_ID,
                Topics = e.TOPIC,
                subTopics = e.SUB_TOPIC,
                subTopicColSpan = e.SUBTOPIC_COLSPAN,
                Steps = e.STEPS,
                STEPS_WIDTH = e.OBJ_WIDTH.ToString()


            }).ToList();

            //separated the object ids for the steps and object details
            var objIds = objProgressTracker.objProgressTrackerHeader.Where(e => TopicIds.Contains(e.TOPIC_ID)).Select(e => e.OBJ_ID).Distinct().ToList();
            var objStepsTopicDetails = objProgressTracker.objProgressTrackerHeader.Where(e => objIds.Contains(e.OBJ_ID))
                                      .Select(e => new StepsTopicDetails
                                      {
                                          Steps = e.STEPS,
                                          StepsWidth = e.OBJ_WIDTH,
                                          subTopics = e.SUB_TOPIC,
                                          TopicId = e.TOPIC_ID

                                      }).Distinct().ToList();
            objProgressTracker.objStepsTopicDetails = objStepsTopicDetails;
            var objObjectiveDetailsList = objProgressTracker.objProgressTrackerHeader.Distinct().Where(e => objIds.Contains(e.OBJ_ID))
                                     .Select(e => new ObjectiveDetails
                                     {
                                         objId = e.OBJ_ID,
                                         objWidth = e.OBJ_WIDTH,
                                         objDate = e.OBJ_ENDDT,
                                         objDescription = e.OBJ_DESC,
                                         TopicId = e.TOPIC_ID,
                                         Steps = e.STEPS,
                                         subTopics = e.SUB_TOPIC,
                                         ObjectiveCode = e.OBJ_CODE

                                     }).Distinct().ToList();

            objProgressTracker.objTopicDetails = lqTopicDetails;
            objProgressTracker.objObjectiveDetails = objObjectiveDetailsList;
            //objProgressTracker.objProgressDropdown = _progressTrackerService.BindProgressTrackerDropdown(SessionHelper.CurrentSession.SchoolId, GRD_ID).ToList();
            objProgressTracker.objProgressDropdown = _iProgressTrackerService.BindProgressTrackerDropdown(SessionHelper.CurrentSession.SchoolId, GRD_ID).Select(e => new ProgressTrackerDropdown
            {
                CODE = e.CODE.Replace(" ", ""),
                COLOR_CODE = e.COLOR_CODE,
                DESCRIPTION = e.DESCRIPTION,
                IsShowCodeAsHeader = e.IsShowCodeAsHeader,
                IS_DROPDOWN = e.IS_DROPDOWN,
                ORDER_SEQUENCE = e.ORDER_SEQUENCE
            }).Distinct().ToList();
            objProgressTracker.objPTSettingMaster = _iProgressTrackerService.BindProgressTrackerMasterSetting(ACD_ID, schoolid, GRD_ID);
            return PartialView("~/Areas/Assessment/Views/ProgressTracker/_ProgressTrackerPartial.cshtml", objProgressTracker);
            //return PartialView("_ProgressTrcker", objProgressTracker);
        }




        #region Progress Setup

        public ActionResult ProgressSetup()
        {


            return View("ProgressSetup");
        }
        public ActionResult ProgressSetupGrid()
        {
            var schoolCurriculum = new SelectList(SelectListHelper.GetSelectListData(ListItems.SchoolCurriculum, $"{SessionHelper.CurrentSession.SchoolId}"), "ItemId", "ItemName");
            var result = _iProgressTrackerService.GetProgressSetup(SessionHelper.CurrentSession.SchoolId);
            var dataList = new object();
            List<long> progressSetupIdList = result.Select(e=>e.Id).ToList();
  
            dataList = new
            {
                aaData = (from item in result
                          select new
                          {

                              CourseName = item.CourseName,
                              GradeDisplay = item.GradeDisplay,
                              GradeTemplate = item.GradeTemplate,
                              GradeSlab = item.GradeSlab,
                              ShowCodeAsHeader = item.ShowCodeAsHeader ? "<a href='javascript:void(0)' class='text-center'><i class='far fa-check-square fa-2x text-success'></i></a>" : "<a href='javascript:void(0)' class='text-center'><i class='far fa-window-close fa-2x text-danger'></i></a>",
                              ShowAsDropDown = item.ShowAsDropDown ? "<a href='javascript:void(0)' class='text-center'><i class='far fa-check-square fa-2x text-success'></i></a>" : "<a href='javascript:void(0)' class='text-center'><i class='far fa-window-close fa-2x text-danger'></i></a>",
                              Action = "<a class='table-action-text-link clsEditSetup' data-toggle='tooltip' title='Edit' data-PSId=" + item.Id + "><i class='fas fa-pencil-alt'></i></a>" +
                              "&nbsp;&nbsp;" +
                              "<a class='table-action-text-link clsDeleteSetup' data-toggle='tooltip' title='Delete' data-PSId=" + item.Id + "><i class='fa fa-trash-alt'></i></a>"
                          }).ToArray()



            };
            var gridResponse = new { dataList  = dataList , result = result, schoolCurriculum= schoolCurriculum };
            var dtjsResult = Json(gridResponse, JsonRequestBehavior.AllowGet);
            dtjsResult.MaxJsonLength = int.MaxValue;

            return dtjsResult;
        }

        public ActionResult AddEditProgressSetup(long Id = 0)
        {
            ProgressTrackerSetup objProgressSetup = new ProgressTrackerSetup();
            if (Id > 0)
            {
                objProgressSetup = _iProgressTrackerService.GetProgressSetupDetailsById(Id);
                objProgressSetup.ProgressSetupRules.ForEach(x =>
                {
                    x.DisplayStartDate = x.StartDate.ToString("dd-MMM-yyyy");
                    x.DisplayEndDate = x.EndDate.ToString("dd-MMM-yyyy");

                });
            }
            return PartialView("AddEditProgressSetup", objProgressSetup);
        }

        public ActionResult GetGradesByCourseId(string courseIds)
        {
            var gradeList = _iProgressTrackerService.GetCourseGradeDisplay(SessionHelper.CurrentSession.SchoolId, courseIds);
            return Json(gradeList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetProgressSetupList(long curriculumId=0)
        {
            var courseList = new SelectList(_SIMSCommonService.GetTeacherCourse(SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId, curriculumId), "COR_ID", "COR_DESCR");
            var gradingTemplate = SelectListHelper.GetSelectListData(ListItems.GradeTemplateMaster, SessionHelper.CurrentSession.SchoolId);
            var gradingSlab = SelectListHelper.GetSelectListData(ListItems.GradeSlab, SessionHelper.CurrentSession.SchoolId);

            var jsonResult = new { courseList = courseList, gradingTemplateList = gradingTemplate, gradingSlabList = gradingSlab };

            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveProgressSetup(ProgressTrackerSetup objProgressSetup)
        {
            objProgressSetup.SchoolId = SessionHelper.CurrentSession.SchoolId;
            var result = _iProgressTrackerService.AddEditProgressSetUP(SessionHelper.CurrentSession.Id, objProgressSetup);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGradingItemTemplate(long gradingId)
        {

            var result = _iGradingTemplateItemService.GetGradingTemplateItems(Convert.ToInt32(gradingId), 1);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ValidateProgressSetupCourseGrade(string courseIds,string gradeIds)
        {
            var result = _iProgressTrackerService.ValidateProgressSetupCourseGrade(courseIds, gradeIds);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region ProgressTracker

        public ActionResult Index()
        {

            return View();
        }
        public ActionResult GetCourseList()
        {
            var coursesListByTeacherId = new SelectList(_SIMSCommonService.GetTeacherCourse(SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId), "COR_ID", "COR_DESCR");
            return Json(coursesListByTeacherId, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCourseGroupByCourse(long? courseId)
        {
            var list = _SIMSCommonService.GetCourseGroupByCourse(courseId.Value, SessionHelper.CurrentSession.Id);
            var subjectgroupList = new SelectList(list, "GroupId", "GroupName");

            return Json(subjectgroupList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetTopicsByCourseId(long? courseId)
        {
            var courseTopicList = _iProgressTrackerService.GetTopicsByCourseId(courseId.Value);
            return Json(courseTopicList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DisplayTracker(long? courseId, long? courseGroupId, string unitIds)
        {
            AssessmentProgressTracker objAssessmentProgressTracker = new AssessmentProgressTracker();
            objAssessmentProgressTracker.StudentList = _iProgressTrackerService.StudentProgressTracker(courseGroupId.Value).OrderBy(e => e.StudentName).ToList();
            objAssessmentProgressTracker.GradingTemplateItemList = _iProgressTrackerService.GetProgressLessonGrading(courseId.Value, courseGroupId.Value).ToList();
            objAssessmentProgressTracker.LessonList = _iProgressTrackerService.GetLessonObjectivesByUnitIds(unitIds).OrderBy(e => e.MainTopic).ToList();
            return PartialView("_ProgressTracker", objAssessmentProgressTracker);
        }
        [HttpPost]
        public ActionResult StudentProgressMapper(long groupId, List<AssessmentStudent> assessmentStudents)
        {
            var result = _iProgressTrackerService.StudentProgressMapper(SessionHelper.CurrentSession.Id, groupId, assessmentStudents);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);


        }

        public ActionResult GetStudentProgressAttachments(long lessonId = 0, long studentId = 0)
        {
            ProgressTrackerEvidence objProgressTrackerEvidence = new ProgressTrackerEvidence();
            objProgressTrackerEvidence.StudentId = studentId;
            objProgressTrackerEvidence.LessonId = lessonId;
            ViewBag.DataModule = $"PT_{lessonId}_{studentId}";
            objProgressTrackerEvidence.AttachmentList = _iProgressTrackerService.GetProgressTrackerEvidence(studentId, $"PT_{lessonId}_{studentId}").ToList();
            return PartialView("_GetStudentProgressAttachments", objProgressTrackerEvidence);
        }

        [HttpPost]
        public ActionResult SaveStudentProgressAttachment(ProgressTrackerEvidence objProgressTrackerEvidence)
        {
            Attachments obbjAttachment = new Attachments();
            string storagePath = Constants.ProgressTracker + "/School_" + SessionHelper.CurrentSession.SchoolId + "/User_" + SessionHelper.CurrentSession.Id;
            string attachmentKey = $"PT_{objProgressTrackerEvidence.LessonId}_{objProgressTrackerEvidence.StudentId}";
            objProgressTrackerEvidence.AttachmentList = StorageHelper.AddGoogleDrivePermission(this, objProgressTrackerEvidence.CloudFileList);
            if (!string.IsNullOrWhiteSpace(objProgressTrackerEvidence.CloudFileList))
            {
                objProgressTrackerEvidence.AttachmentList.ForEach(x => { x.AttachedToId = objProgressTrackerEvidence.StudentId; });
            }
            objProgressTrackerEvidence.AttachmentList.AddRange(StorageHelper.SaveAllFileAttachment(Request.Files.GetMultiple("AttachmentList"), attachmentKey, storagePath, objProgressTrackerEvidence.StudentId));
            objProgressTrackerEvidence.AttachmentList.AddRange(StorageHelper.SaveFileAttachment(objProgressTrackerEvidence.StudentProgressEvidence, attachmentKey, storagePath, objProgressTrackerEvidence.StudentId));
            var result = _iProgressTrackerService.SaveProgressTrackerEvidence(objProgressTrackerEvidence.AttachmentList);
            var jsonResponse = new
            {
                IsSuccess = new OperationDetails(result),
                evidenceCount = _iProgressTrackerService.GetProgressTrackerEvidence(objProgressTrackerEvidence.StudentId, attachmentKey).ToList().Count()
            };
            return Json(jsonResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAssignmentsForStudentLesson(long lessonId, long studentId)
        {
            List<AssignmentObjectiveGrading> objAssignmentObjectiveGrading = new List<AssignmentObjectiveGrading>();
            objAssignmentObjectiveGrading = _iProgressTrackerService.GetObjectiveAssignmentGrading(lessonId, studentId).ToList();
            return PartialView("_StudentAssignmentGradeForLesson", objAssignmentObjectiveGrading);
        }

        #endregion
    }

}