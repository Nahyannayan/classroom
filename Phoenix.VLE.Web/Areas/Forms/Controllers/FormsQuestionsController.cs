﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Forms.Controllers
{
    [Authorize]
    public class FormsQuestionsController : BaseController
    {

        private readonly IQuizQuestionsService _quizQuestionsService;
        private readonly IQuizService _quizService;
        private readonly IUserPermissionService _userPermissionService;
        public FormsQuestionsController(IQuizQuestionsService quizQuestionsService, IQuizService quizService, IUserPermissionService userPermissionService)
        {
            _quizQuestionsService = quizQuestionsService;
            _quizService = quizService;
            _userPermissionService = userPermissionService;
        }
        // GET: Forms/FormsQuestions
        public ActionResult Index()
        {
            int quizId = 0;
            if (Session["SelectedQuizId"] != null)
            {
                quizId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(Session["SelectedQuizId"].ToString()));
            }
            Session["quizId"] = quizId;
            var questions = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            return View(questions);
        }
        public ActionResult AddQuizQuestion()
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var QuestionTypes = SelectListHelper.GetSelectListData(ListItems.QuestionType);
            QuestionTypes = QuestionTypes.Where(x => x.ItemId != "6" && x.ItemId != "8").ToList();
            ViewBag.QuestionType = QuestionTypes;
            var model = new QuizQuestionsEdit();
            model.QuizId = (int)Session["quizId"];
            model.IsAddMode = true;
            var quizModel = _quizService.GetQuiz(model.QuizId);
            model.QuizName = quizModel.QuizName;
            Session["QuizQuestionFiles"] = new List<QuizQuestionFiles>();
            return View("AddQuizQuestion", model);
        }
        //[HttpPost]
        //public ActionResult SaveQuizQuestion(QuizQuestionsEdit model, List<Answers> answersList)
        //{
        //    model.Answers = answersList;
        //    model.CreatedBy = (int)SessionHelper.CurrentSession.Id;
        //    model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
        //    List<Objective> lstObjectives = new List<Objective>();
        //    var result = _quizQuestionsService.QuizQuestionsCUD(model, lstObjectives);
        //    TempData["IsQuizQuestion"] = result;
        //    return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        //}

        [HttpPost, ValidateInput(false)]
        public ActionResult SaveQuizQuestion(QuizQuestionsEdit model)
        {
            model.QuestionText = model.QuestionText.Replace("\n", "</br>");
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var isCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            if (!isCustomPermission)
            {
                return Json(new OperationDetails(false, LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrWhiteSpace(model.QuestionText))
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            model.CreatedBy = (int)SessionHelper.CurrentSession.Id;
            model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            List<Objective> lstObjectives = new List<Objective>();
            model.lstDocuments = Session["QuizQuestionFiles"] as List<QuizQuestionFiles>;
            model.lstObjectives = lstObjectives;
            var result = _quizQuestionsService.QuizQuestionsCUD(model, lstObjectives);
            TempData["IsQuizQuestion"] = result;
            Session["QuizQuestionFiles"] = new List<QuizQuestionFiles>();
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetuploadedFileDetails(int? id, bool isTask)
        {

            List<QuizQuestionFiles> lstfiles = Session["QuizQuestionFiles"] as List<QuizQuestionFiles>;
            return PartialView("_QuizQuestionFileList", lstfiles);
        }
        [HttpPost]
        public async Task<ActionResult> UploadQuizQuestionFiles()
        {
            List<QuizQuestionFiles> fileNames = new List<QuizQuestionFiles>();
            string relativePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                
                try
                {
                   
                    // Get all files from Request object
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;

                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        SharePointFileView shv = new SharePointFileView();
                        if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                        {
                            AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                            await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                            shv = await azureHelper.UploadFilesAsPerModuleAsync(FileModulesConstants.QuizQuestion, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                        }
                        else
                        {
                            SharePointHelper sh = new SharePointHelper();
                            Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                            shv = SharePointHelper.UploadFilesAsPerModule(ref cx, FileModulesConstants.QuizQuestion, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                        }
                        var extension = System.IO.Path.GetExtension(file.FileName);
                        fileNames.Add(new QuizQuestionFiles
                        {
                            FileName = file.FileName,
                            FileExtension = extension.Replace(".", ""),
                            UploadedFileName = shv.ShareableLink,
                            PhysicalPath = shv.SharepointUploadedFileURL
                        });
                    }
                }
                catch (Exception ex)
                {
                }
                if (Session["QuizQuestionFiles"] == null)
                {
                    Session["QuizQuestionFiles"] = fileNames;
                }
                else
                {
                    List<QuizQuestionFiles> lstExistingFiles = new List<QuizQuestionFiles>();
                    lstExistingFiles = Session["QuizQuestionFiles"] as List<QuizQuestionFiles>;
                    lstExistingFiles.AddRange(fileNames);
                    Session["QuizQuestionFiles"] = lstExistingFiles;
                }
            }
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditQuizQuestion(int id)
        {
            int quiQuestionId = id;
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var question = _quizQuestionsService.GetQuizQuestionById(quiQuestionId);
            question.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(quiQuestionId);
            var QuestionTypes = SelectListHelper.GetSelectListData(ListItems.QuestionType);
            QuestionTypes = QuestionTypes.Where(x => x.ItemId != "6" && x.ItemId != "8").ToList();
            ViewBag.QuestionType = QuestionTypes;
            question.QuestionText = question.QuestionText.Replace("</br>", "\n");
            question.QuizId = (int)Session["quizId"];
            ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName");
            List<Subject> lstSubjects = new List<Subject>();
            lstSubjects = _quizQuestionsService.GetQuestionSubjects(quiQuestionId).ToList();
            question.SubjectId = lstSubjects.Select(m => m.SubjectId.ToString()).ToList();
            question.lstObjective = _quizQuestionsService.GetQuestionObjectives(quiQuestionId).ToList();
            Session["lstObjective"] = question.lstObjective;
            Session["QuizQuestionFiles"] = question.lstDocuments;
            return View("EditQuizQuestion", question);
        }
        public ActionResult AddCorrectAnswer(int id)
        {
            var answerList = _quizQuestionsService.GetQuizAnswerByQuizQuestionId(id);
            return PartialView("_AddCorrectAnswer", answerList);
        }
        [HttpPost]
        public ActionResult UploadMTPFiles()
        {
            string SavedFileName = String.Empty;
            List<QuizQuestionFiles> fileNames = new List<QuizQuestionFiles>();
            string relativePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Get all files from Request object
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;

                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        var extension = System.IO.Path.GetExtension(file.FileName);


                        //content for file upload
                        string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                        string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizMTPQuestion;
                        //string fileModule = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFilesDir;
                        string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizMTPQuestion;

                        Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                        Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);

                        Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                        fname = Guid.NewGuid() + "_" + fname;
                        SavedFileName = fname;
                        relativePath = Constants.QuizQuestion + "/" + fname;
                        fname = Path.Combine(filePath, fname);
                        file.SaveAs(fname);
                        fileNames.Add(new QuizQuestionFiles
                        {
                            FileName = file.FileName,
                            FileExtension = extension.Replace(".", ""),
                            UploadedFileName = relativePath
                        });
                    }
                }
                catch (Exception ex)
                {
                }
                if (Session["QuizMTPQuestionFiles"] == null)
                {
                    Session["QuizMTPQuestionFiles"] = fileNames;
                }
                else
                {
                    List<QuizQuestionFiles> lstExistingFiles = new List<QuizQuestionFiles>();
                    lstExistingFiles = Session["QuizMTPQuestionFiles"] as List<QuizQuestionFiles>;
                    lstExistingFiles.AddRange(fileNames);
                    Session["QuizMTPQuestionFiles"] = lstExistingFiles;
                }
            }
            return Json(SavedFileName, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddCorrectAnswer(int id, int response, int questionType, int questionId)
        {
            var model = new QuizAnswersEdit();
            model.QuizAnswerId = id;
            model.IsCorrectAnswer = Convert.ToBoolean(response);
            model.QuestionTypeId = questionType;
            model.QuizQuestionId = questionId;
            var result = _quizQuestionsService.UpdateQuizCorrectAnswerByQuizAnswerId(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteQuizQuestionData(int id)
        {
            var model = new QuizQuestionsEdit();
            model.QuizQuestionId = id;
            model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            var result = _quizQuestionsService.QuizQuestionsDelete(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadImportFormQuestionPopup()
        {
            return PartialView("_LoadImportFormQuestionPopup");
        }
    }
}