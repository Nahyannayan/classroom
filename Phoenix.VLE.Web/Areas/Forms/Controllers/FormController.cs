﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Forms.Controllers
{
    public class FormController : BaseController
    {
            private readonly IQuizService _quizService;
            private readonly IUserPermissionService _userPermissionService;
            public FormController(IQuizService quizService, IUserPermissionService userPermissionService)
            {
                _quizService = quizService;
                _userPermissionService = userPermissionService;
            }
            public ActionResult Index()
            {
                 var syncContext = SynchronizationContext.Current;
                 SynchronizationContext.SetSynchronizationContext(null);
                 ViewBag.isCanEditPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_MyFiles.ToString()).Result;
                 SynchronizationContext.SetSynchronizationContext(syncContext);
                return View();
            }
            public ActionResult LoadQuizGrid()
            {
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var canEdit = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_MyFiles.ToString()).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);

                var quiz = _quizService.GetAllQuiz();
                var filterByTeacher = quiz.Where(x => x.UpdatedBy == SessionHelper.CurrentSession.Id);
                var dataList = new object();
              
                string QuestionsBtn = canEdit? "<button type='button' class='table-action-icon-btn' onclick='quiz.openQuizQuestion({0});' title='"+ ResourceManager.GetString("Files.Forms.ViewQuiz") + "' ><i class='fas fa-eye'></i></button>":"";
                string editBtn = canEdit ? "<button type='button' class='table-action-icon-btn' onclick='quiz.editQuizPopup($(this),{0});' title='" + ResourceManager.GetString("Files.Forms.EditQuiz") + "'><i class='fas fa-pencil-alt btnEdit'></i></button>" : "";
                string deleteBtn = canEdit ? "<button type='button' class='table-action-icon-btn' onclick='quiz.deleteQuizData($(this),{0}); ' title='" + ResourceManager.GetString("Files.Forms.DeleteQuiz") + "'><i class='far fa-trash-alt btnDelete'></i></button>" : "";
                dataList = new
                {
                    aaData = (from item in filterByTeacher
                              select new
                              {

                                  Actions = "<div class='tbl-actions'>" + string.Format(editBtn, item.QuizId) + string.Format(QuestionsBtn, item.QuizId) + string.Format(deleteBtn, item.QuizId) + "</div>",
                                  item.QuizId,
                                  item.QuizName,
                                  item.PassMarks,
                                  IsActive = "<div class='custom-control custom-checkbox mb-2 permission-checkbox' ><input type='checkbox' id='isActive_" + item.QuizId + "' class='custom-control-input' " + (item.IsActive ? "checked" : "") + " onclick='quiz.changeStatus(" + item.QuizId + ",\"" + item.QuizName + "\",\"" + item.Description + "\"," + item.PassMarks + "," + (item.IsActive ? "0" : "1") + ")'><label class='custom-control-label  w-100' for='isActive_" + item.QuizId + "'></lable></div>"
                              }).ToArray()
                };

                return Json(dataList, JsonRequestBehavior.AllowGet);
            }

            public ActionResult InitAddEditQuizForm(int? id)
            {
                var model = new QuizEdit();
                if (id.HasValue)
                {
                    model = _quizService.GetQuiz(id.Value);
                }
                else
                {
                    model.IsAddMode = true;
                    model.SchoolId = SessionHelper.CurrentSession.SchoolId;
                }
                return PartialView("_AddEditQuiz", model);
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult SaveQuizData(QuizEdit model)
            {
                model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
                var result = _quizService.QuizCUD(model);
                return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
            }

            [HttpPost]
            public ActionResult DeleteQuizData(int id)
            {
                QuizEdit model = new QuizEdit();
                model.QuizId = id;
                model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
                var result = _quizService.QuizDelete(model);
                return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
            }
    }
}