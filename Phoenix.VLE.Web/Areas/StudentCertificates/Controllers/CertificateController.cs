﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.StudentCertificates.Controllers
{
    public class CertificateController : BaseController
    {
        private readonly IStudentCertificateService _studentCertificateService;
        private readonly IUserPermissionService _userPermissionService;

        public CertificateController(IStudentCertificateService studentCertificateService, IUserPermissionService userPermissionService)
        {
            _studentCertificateService = studentCertificateService;
            _userPermissionService = userPermissionService;
        }
        // GET: StudentCertificates/Certificate
        public ActionResult Index()
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_StudentCertificate.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            long userId = (long)SessionHelper.CurrentSession.Id;

            return View();
        }

        public ActionResult LoadQuizGrid()
        {
            int isTeacher = 0;
            if (SessionHelper.CurrentSession.IsTeacher())
            {
                isTeacher = 1;
            }
            if (SessionHelper.CurrentSession.IsAdmin)
            {
                var quiz = _studentCertificateService.GetStudentCertificates(SessionHelper.CurrentSession.Id, isTeacher);
                var dataList = new object();

                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var CustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_StudentCertificate.ToString()).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);

                //string QuestionsBtn = "<button type='button' class='table-action-icon-btn' onclick='quiz.openQuizQuestion({0});' title='View Quiz' ><i class='fas fa-eye'></i></button>";

                string editBtn = CustomPermission ? "<button type='button' class='table-action-icon-btn' onclick='quiz.editQuizPopup($(this),{0});' title='Edit Quiz'><i class='fas fa-pencil-alt btnEdit'></i></button>" : "";
                //string deleteBtn = CustomPermission ? "<button type='button' class='table-action-icon-btn' onclick='quiz.deleteQuizData($(this),{0}); ' title='Delete Quiz'><i class='far fa-trash-alt btnDelete'></i></button>" : "";
                dataList = new
                {
                    aaData = (from item in quiz
                              select new
                              {
                                  //QuizImage = !string.IsNullOrEmpty(item.QuizImagePath) ? "<img src = '" + PhoenixConfiguration.Instance.ReadFilePath + item.QuizImagePath + "' class='img-thumbnail' style = 'height:40px;width:40px' />" : "",
                                  Actions = "<div class='tbl-actions'>" + string.Format(editBtn, item.CertificateId) + "</div>",
                                  item.CertificateId,
                                  item.CertificateName,
                                  item.CertificateDescription,

                              }).ToArray()
                };

                return Json(dataList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var quiz = _studentCertificateService.GetStudentCertificates(SessionHelper.CurrentSession.Id, isTeacher);
                var dataList = new object();
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var CustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_StudentCertificate.ToString()).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);

                string QuestionsBtn = "<button type='button' class='table-action-icon-btn' onclick='quiz.openQuizQuestion({0});' title='View Quiz' ><i class='fas fa-eye'></i></button>";

                string editBtn = CustomPermission ? "<button type='button' class='table-action-icon-btn' onclick='quiz.editQuizPopup($(this),{0});' title='Edit Quiz'><i class='fas fa-pencil-alt btnEdit'></i></button>" : "";
                string deleteBtn = CustomPermission ? "<button type='button' class='table-action-icon-btn' onclick='quiz.deleteQuizData($(this),{0}); ' title='Delete Quiz'><i class='far fa-trash-alt btnDelete'></i></button>" : "";
                dataList = new
                {
                    aaData = (from item in quiz
                              select new
                              {
                                  //QuizImage = !string.IsNullOrEmpty(item.QuizImagePath) ? "<img src = '" + PhoenixConfiguration.Instance.ReadFilePath + item.QuizImagePath + "' class='img-thumbnail' style = 'height:40px;width:40px' />" : "",
                                  Actions = "<div class='tbl-actions'>" + string.Format(editBtn, item.CertificateId) + string.Format(QuestionsBtn, item.CertificateId) + string.Format(deleteBtn, item.CertificateId) + "</div>",
                                  item.CertificateId,
                                  item.CertificateName,
                                  item.CertificateDescription,

                              }).ToArray()
                };

                return Json(dataList, JsonRequestBehavior.AllowGet);
            }


        }
        public ActionResult InitAddEditCertificate(int? id)
        {
            var model = new CertificateEdit();
            if (id.HasValue)
            {
                model = _studentCertificateService.GetStudentCertificatesById(id.Value);
            }
            else
            {
                model.IsAddMode = true;
            }
            return PartialView("_AddEditCerificate", model);
        }
        public ActionResult AddEditCertificate1()
        {
            int? id = 0;
            var model = new CertificateEdit();
            TempData["File"] = "certificate.json";
            if (id.HasValue)
            {
                model = _studentCertificateService.GetStudentCertificatesById(id.Value);
            }
            else
            {
                model.IsAddMode = true;

            }
            return PartialView("AddEditCertificate", model);
        }
        public ActionResult AddEditCertificate()
        {
            int? id = 0;
            var model = new CertificateEdit();
            TempData["File"] = "certificate.json";
            if (id.HasValue)
            {
                model = _studentCertificateService.GetStudentCertificatesById(id.Value);
                model.CertificateName = "_student-name_";
                model.CertificateDescription = "_teacher-name_";
                model.count = 2;
                model.FieldValue = "_student-name_,_teacher-name_";
            }
            else
            {
                model.IsAddMode = true;
            }
            string pageName = String.Empty;
            pageName = "_AddEditCertificatePartial";
            return View(pageName, model);
        }

        public ActionResult Save(string name, string data)
        {

            var model = new CertificateEdit();
            TempData["File"] = "certificate.json";
            var studentId = Convert.ToInt32(Session["StudentID"]);
            var data1 = (JObject)JsonConvert.DeserializeObject(data);
            var str = data1["canvas"]["objects"][1];
            var name1 = str["text"].Value<string>();
            var count = data1["canvas"]["objects"].Count();
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    var value = data1["canvas"]["objects"][i]["text"];
                }
            }

            string path = Server.MapPath("~/pixie/");
            // Write that JSON to txt file,  
            System.IO.File.WriteAllText(path + name, data);
            string pageName = "_AddEditCertificatePartial";
            return View(pageName, model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCertificateData(CertificateEdit model)
        {
            // model.ParseJsonToXml("SafetyCategoryName");
            var result = 0;
            model.SchoolId = SessionHelper.CurrentSession.SchoolId;
            if (!CurrentPagePermission.CanEdit)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            }
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            if (SessionHelper.CurrentSession.IsTeacher() && model.CertificateId > 0)
            {
                model.FileName = "certificate.json";
                model.Status = "UnApproved";
                //model.IsApproved = true;
                result = _studentCertificateService.InsertCertificate(model);
            }
            if (SessionHelper.CurrentSession.IsAdmin)
            {
                model.Status = "Approved";
                //model.IsApproved = true;
                result = _studentCertificateService.InsertCertificate(model);
            }
            if (result < 0)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ItemExistsMessage.Replace("{0}", "Safety Catagory")), JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(result == 1 ? true : false), JsonRequestBehavior.AllowGet);
        }

        #region Student Certificates

        private IList<Certificate> GetStudentCertificates(long userId, int isTeacher)
        {
            IList<Certificate> studentCertificatesList = new List<Certificate>();
            studentCertificatesList = _studentCertificateService.GetStudentCertificates(userId, isTeacher);
            return studentCertificatesList;
        }
        #endregion
    }
}