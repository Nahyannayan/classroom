﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.StudentCertificates
{
    public class StudentCertificatesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "StudentCertificates";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "StudentCertificates_default",
                "StudentCertificates/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}