﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.Survey.Controllers
{
    public class SurveyController : BaseController
    {
        #region DI
        private ISurveyService surveyService;

        public SurveyController(ISurveyService surveyService)
        {
            this.surveyService = surveyService;
        }
        #endregion

        // GET: Survey/Survey
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddEditSurvey(string id = null)
        {
            if (id.IsNotNullOrEmpty())
            {
                var surveyId = EncryptDecryptHelper.DecryptUrl(id).ToLong();
            }
            return View();
        }

    }
}