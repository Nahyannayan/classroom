﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Areas.SMS.Model
{
    public class Assessment
    {
        public string Student_No { get; set; }
        public string Student_Name { get; set; }
        public string TDATE { get; set; }
        public string Status { get; set; }
        public string Student_Image_url { get; set; }
        public string AllowEdit { get; set; }
        public string ALG_ID { get; set; }
        public string STU_ID { get; set; }
        public string Student_XML { get; set; }
        public string current_ALG_ID { get; set; }
        public string APD_ID { get; set; }
        public string STATUS_DESCR { get; set; }
        public string Remarks { get; set; }
    }
    public class StudentList
    {
        public string STU_NO { get; set; }
        public string STU_ID { get; set; }
        public string STU_NAME { get; set; }
        public string Grade { get; set; }
        public string Section { get; set; }
        public string STU_PHOTO { get; set; }

    }
    public class ReportHeader
    {
        public string RSM_ID { get; set; }
        public string CSSCLASS { get; set; }
        public string RSD_RESULT { get; set; }
        public string RSD_SUB_DESC { get; set; }
        public string RSD_HEADER { get; set; }
        public string DISPLAYORDER { get; set; }
        public string RSD_ID { get; set; }
        public string RSP_DESCR { get; set; }
        public bool IS_PREV { get; set; }
    }
    public class ReportHeaderDropDown
    {
        public string RSP_ID { get; set; }
        public string RSP_DESCR { get; set; }
        public string RSP_DISPLAYORDER { get; set; }

    }
    public class AssessmentData
    {
        public string RST_ID { get; set; }
        public string RSD_ID { get; set; }
        public string RPF_ID { get; set; }
        public string RSD_RESULT { get; set; }
        public string SBG_ID { get; set; }
        public string RSD_HEADER { get; set; }
        public string MARK { get; set; }
        public string COMMENTS { get; set; }
        public string GRADING { get; set; }
        public string RRM_ID { get; set; }
        public string RSM_ID { get; set; }
        public string STU_ID { get; set; }


    }


    public class MarkEntry
    {
        public long CAS_ID { get; set; }
        public long SLAB_ID { get; set; }
        public long SGR_ID { get; set; }
        public long SBG_ID { get; set; }
        public DateTime ? CAS_DATE { get; set; }
        public DateTime ? CAS_TIME { get; set; }
        public string CAS_DESC { get; set; }
        public string  SBG_DESCR { get; set; }
        public string  SGR_DESCR { get; set; }
        public string MARKS { get; set; }
        public string ATT { get; set; }
        public string bMARKS { get; set; }
        public string  CAS_TYPE_LEVEL { get; set; }
        public int CAS_GSM_SLAB_ID { get; set; }
        public double CAS_MIN_MARK { get; set; }
        public double CAS_MAX_MARK { get; set; }
        public string SBG_ENTRYTYPE { get; set; }
        public bool CAS_bHasAOLEXAM { get; set; }
        public bool CAS_bWITHOUTSKILLS { get; set; }
        public bool CAS_bSKILLS { get; set; }
        public int GRD_ID { get; set; }





    }
    public class AssessmentOptionalList
    {
        public string AOM_ID { get; set; }
        public string AOM_DESCR { get; set; }
        public string AOM_TYPE { get; set; }
        public int AOD_ID { get; set; }
        public int AOD_AOM_ID { get; set; }
        public int AOD_BSU_ID { get; set; }
        public int AOD_ACD_ID { get; set; }



    }


    public class AssessmentPreviousSchedule
    {
        public long RSM_ID { get; set; }
        public string RSM_DESCR { get; set; }
        public int RPF_ID { get; set; }
        public string RPF_DESCR { get; set; }
        public string DISPLAY_TEXT { get; set; }

    }

    //MARK ENTRY

    public class MarkEntryData
    {
        public long   STA_ID { get; set; }
        public long   STU_ID { get; set; }
        public long   STU_NO { get; set; }
        public string STU_NAME { get; set; }
        public double MARKS { get; set; }
        public string MARK_GRADE { get; set; }
        public double MIN_MARK { get; set; }
        public double MAX_MARK { get; set; }
        public string IS_ENABLED { get; set; }
        public string STU_STATUS { get; set; }
    }

    public class MarkEntryAOLData
    {
        public long STA_ID { get; set; }
        public long STU_ID { get; set; }
        public long STU_NO { get; set; }
        public string STU_NAME { get; set; }
        public string SKILL_NAME { get; set; }
        public double SKILL_MARK { get; set; }
        public string SKILL_GRADE { get; set; }
        public double SKILL_MAX_MARK { get; set; }
        public double MARKS { get; set; }
        public string STA_GRADE { get; set; }
        public string IS_ENABLED { get; set; }
        public string WITHOUTSKILLS_MARKS { get; set; }
        public string WITHOUTSKILLS_GRADE { get; set; }
        public string WITHOUTSKILLS_MAXMARKS { get; set; }
        public string CHAPTER { get; set; }
        public string FEEDBACK { get; set; }
        public string WOT { get; set; }
        public string TARGET { get; set; }
        public string bATTENDED { get; set; }
        public string STU_STATUS { get; set; }
        public int SKILL_ORDER { get; set; }
    }

    public class SkillSET
    {
        public string SKILL_NAME { get; set; }
        public double SKILL_MARK { get; set; }
        public string SKILL_GRADE { get; set; }
        public double SKILL_MAX_MARK { get; set; }
        public long STU_NO { get; set; }

        public string STU_NAME { get; set; }


      

    }

    public class AssessmentComments
    {
        public long CMT_ID { get; set; }
        public string CMT_COMMENTS { get; set; }
    }
    public class AssessmentCategory
    {
        public long CAT_ID { get; set; }

        public string CAT_DESC { get; set; }


    }
    public class SectionAccess
    {
        public long SCT_ID { get; set; }

        public string SCT_DESCR { get; set; }


    }
    public class HeaderOptional
    {
        public long AOM_ID { get; set; }

        public string AOM_DESCR { get; set; }
        public string AOM_TYPE { get; set; }
        public long AOD_ID { get; set; }
        public long AOD_BSU_ID { get; set; }
        public long AOD_ACD_ID { get; set; }


    }
    public class AssessmentDataOptional
    {
        public long AOM_ID { get; set; }
        public long STU_ID { get; set; }
        public string OPTIONAL_VALUE { get; set; }

    }

}
