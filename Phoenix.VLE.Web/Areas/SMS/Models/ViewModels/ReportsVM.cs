﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phoenix.VLE.Web.Areas.SMS.Model;
namespace Phoenix.VLE.Web.Areas.SMS.Models.ViewModels
{
    public class ReportsVM
    {
        public List<ReportDesigner> ReportDesigner { get; set; }
        public List<ReportBinder> ReportBinder { get; set; }

        public List<ReportFilterControls> ReportFilterControls { get; set; }
    }

    public class PopulatereportFilter
    {
        public string FILTER_CODE { get; set; }

        public string Filter_Value { get; set; }

        public string DataType { get; set; }
    }

   


}