﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phoenix.Models.Entities;
using SMS.Web.Areas.SMS.Model;

namespace SMS.Web.Areas.SMS.ViewModels
{

    public class BehaviourVM
    {

        public BehaviourVM()
        {
            classList = new List<ClassList>();
            behaviour = new List<BehaviourModel>();
        }
        public IEnumerable<ClassList> classList { get; set; }
        public IEnumerable<BehaviourModel> behaviour { get; set; }
        public BehaviourDetails behaviourDetails { get; set; }
    }

    public class StudentBehaviourVM
    {
        public StudentBehaviourVM()
        {
            BehaviourList = new List<StudentBehaviour>();

        }
        public int? BehaviourId { get; set; }

        public string BehaviourType { get; set; }

        public string BehaviourLogo { get; set; }

        public string Behaviourpoint { get; set; }

        public List<StudentBehaviour> BehaviourList { get; set; }

        public long? StudentId { get; set; }

        public bool IsSelectMode { get; set; }
        public int IsActive { get; set; }
        public int CategoryId { get; set; }

        public string BehaviourComment { get; set; }

        public string UploadedFilePath { get; set; }

        public string FileName { get; set; }
    }

    public class StudentBehaviourMerit : Attachments
    {
        public StudentBehaviourMerit()
        {
            IncidentDate = DateTime.Now;
        }
        public long AttachmentId { get; set; }
        public long MeritId { get; set; }

        public string MeritType { get; set; }

        public string MeritRemarks { get; set; }

        public int MeritCategoryId { get; set; }

        public int MeritSubCategoryId { get; set; }

        public string MeritUpload { get; set; }

        public int MeritUploaded { get; set; }

        public long StudentId { get; set; }

        public string[] StudentIds { get; set; }

        public string[] MeritDemertIds { get; set; }

        public string GradeId { get; set; }
        public bool IsExistFile { get; set; }
        public long UserId { get; set; }
        public DateTime? IncidentDate { get; set; }
        public string strIncidentDate { get; set; }
        public long GroupId { get; set; }
    }


    public class StudentMeritList
    {

        public int TimeTableID { get; set; }
        public string Student_Name { get; set; }
        public string Student_No { get; set; }
        public string Student_ID { get; set; }
        public string Student_Image_url { get; set; }
        public string Student_Grade { get; set; }
        public string Student_Section { get; set; }
        public string Student_Flag_Status { get; set; }
        public int Behaviourpoint { get; set; }
        public int PositivePoint { get; set; }
        public int NegativePoint { get; set; }
        public long MeritId { get; set; }
        public DateTime? IncidentDate { get; set; }


    }

    public class SubCategories
    {

        public long MeritId { get; set; }
        public long StudentId { get; set; }
        public long CategoryId { get; set; }
        public int CategoryScore { get; set; }
        public string CategoryName { get; set; }
        public string ImagePath { get; set; }
        public string UploadedPath { get; set; }
        public int MeritUploaded { get; set; }
        public long LevelMapping_ID { get; set; }


    }


    public class MeritDemerit
    {
        public MeritDemerit()
        {
            objListOfCategories = new List<CategoryDetails>();
            objMeritDemerit = new StudentBehaviourMerit();
        }
        public StudentBehaviourMerit objMeritDemerit { get; set; }

        public List<CategoryDetails> objListOfCategories { get; set; }

    }
    public class CategoryDetails
    {

        public int CategoryId { get; set; }
        public int SubcategoryId { get; set; }
        public long LevelMappingId { get; set; }
    }
}
