﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.VLE.Web.Areas.SMS.Model
{
    public class Behaviour
    {
        public Behaviour() { }
        public Behaviour(int _behaviourId)
        {
            Behaviour_ID = _behaviourId;
        }
        public int Behaviour_ID { get; set; }
        public string Student_ID { get; set; }
        public string Student_Name { get; set; }
        public string Type { get; set; }
        public string Comments { get; set; }
        public string Recorded_by { get; set; }
        public string Recorded_Date { get; set; }

    }
    public class BehaviourDetails
    {
        public BehaviourDetails() { }
        public BehaviourDetails(int _behaviourId)
        {
            Behaviour_ID = _behaviourId;
        }
        public int Behaviour_ID { get; set; }

        public int Category_ID { get; set; }
        public int SubCategory_ID { get; set; }
        public int Points { get; set; }
        public int Activity_ID { get; set; }
        public int Location_ID { get; set; }
        public DateTime? Incident_Date { get; set; }
        public string Incident_Time { get; set; }
        public string Period { get; set; }
        public string Comments { get; set; }
        public DateTime? Recorded_Date { get; set; }
        public int Status_ID { get; set; }
        public string Recorded_By { get; set; }
        public string DocPath { get; set; }
        public int OtherStaff_ID { get; set; }
        public DateTime ? Followup_Date { get; set; }
        public string ReferredTo { get; set; }
        public string FollowupComments { get; set; }
        public string FileName { get; set; }
        public string otherStudentEnvolved { get; set; }
        public string WitnessStudentIds { get; set; }
        public string WitnessStaffIds { get; set; }
        public string WitnessStatement { get; set; }


    }

    public class StudentBehaviour
    {

        public int BehaviourId { get; set; }
        public long StudentId { get; set; }

        public string BehaviourType { get; set; }

        public string BehaviourLogo { get; set; }

        public int Behaviourpoint { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int IsActive { get; set; }

        public int CategoryId { get; set; }
        public string BehaviourComment { get; set; }

        public string UploadedFilePath { get; set; }

        public string FileName { get; set; }

        public int FileCount { get; set; }
    }


    public class StudentBehaviourFiles
    {
        public int FileId { get; set; }
        public long StudentId { get; set; }

        public int BehaviourId { get; set; }

        public string FileName { get; set; }
        public string UploadedFilePath { get; set; }

    }
}
