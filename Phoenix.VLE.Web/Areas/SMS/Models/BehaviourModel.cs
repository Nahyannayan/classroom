﻿using Phoenix.VLE.Web.Areas.SMS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SMS.Web.Areas.SMS.Model
{
    public class BehaviourModel
    {
        public BehaviourModel() { }
        public BehaviourModel(int _behaviourId)
        {
            Behaviour_ID = _behaviourId;
        }
        public int Behaviour_ID { get; set; }
        public string Student_ID { get; set; }
        public string Student_Name { get; set; }
        public string Type { get; set; }
        public string Comments { get; set; }
        public string Recorded_by { get; set; }
        public string Recorded_Date { get; set; }

    }
    public class BehaviourDetails
    {
        public BehaviourDetails() { }
        public BehaviourDetails(int _behaviourId)
        {
            Behaviour_ID = _behaviourId;
        }
        public int Behaviour_ID { get; set; }

        public int Category_ID { get; set; }
        public int SubCategory_ID { get; set; }
        public int Points { get; set; }
        public int Activity_ID { get; set; }
        public int Location_ID { get; set; }
        public DateTime? Incident_Date { get; set; }
        public string Incident_Time { get; set; }
        public string Period { get; set; }
        public string Comments { get; set; }
        public DateTime? Recorded_Date { get; set; }
        public int Status_ID { get; set; }
        public string Recorded_By { get; set; }
        public string DocPath { get; set; }
        public int OtherStaff_ID { get; set; }
        public DateTime ? Followup_Date { get; set; }
        public string ReferredTo { get; set; }
        public string FollowupComments { get; set; }
        public string FileName { get; set; }
        public string otherStudentEnvolved { get; set; }
        public string WitnessStudentIds { get; set; }
        public string WitnessStaffIds { get; set; }
        public string WitnessStatement { get; set; }


    }

    public class StudentBehaviour
    {

        public int BehaviourId { get; set; }
        public long StudentId { get; set; }

        public string BehaviourType { get; set; }

        public string BehaviourLogo { get; set; }

        public int Behaviourpoint { get; set; }
        public int PositivePoint { get; set; }
        public int NegativePoint { get; set; }
       
        public DateTime? ModifiedOn { get; set; }

        public int IsActive { get; set; }

        public int CategoryId { get; set; }
        public string BehaviourComment { get; set; }

        public string UploadedFilePath { get; set; }

        public string FileName { get; set; }

        public int FileCount { get; set; }
        public long SchoolId { get; set; }
    }


    public class StudentBehaviourFiles
    {
        public int FileId { get; set; }
        public long StudentId { get; set; }

        public int BehaviourId { get; set; }

        public string FileName { get; set; }
        public string UploadedFilePath { get; set; }

    }
    public class MeritDemeritUpload
    {
        public string IncidentType { get; set; }

        public string FileName { get; set; }
        public string UploadedFilePath { get; set; }

    }

    public class ChartModel
    {
        public string GRADE { get; set; }
        public string IncidentType { get; set; }
        public int IncidentCount { get; set; }
    }
    
    public class BehaviourAction
    {
        public long ActionId { get; set; }
        public bool ParentCalled { get; set; }
        public DateTime? ParentCalledDate { get; set; }
        public string ParentCalledComment { get; set; }
        public bool ParentInterviewed { get; set; }
        public DateTime? ParentInterviewedDate { get; set; }
        public string ParentInterviewedComment { get; set; }
        public bool NotesInStudentPlanner { get; set; }
        public DateTime? NotesInStudentPlannerDate { get; set; }
        public bool BreakDetentionGiven { get; set; }
        public DateTime? BreakDetentionGivenDate { get; set; }
        public bool AfterSchoolDetentionGiven { get; set; }
        public DateTime? AfterSchoolDetentionGivenDate { get; set; }
        public bool Suspension { get; set; }
        public DateTime? SuspensionDate { get; set; }
        public bool ReferredToStudentsCounsellor { get; set; }
        public DateTime? ReferredToStudentsCounsellorDate { get; set; }
        public BasicDetails StudentDetails { get; set; }
        public long IncidentId { get; set; }
        public long StudentId { get; set; }
        public long CategoryId { get; set; }
        public string DataMode { get; set; }
        public DateTime Entry_Date { get; set; }
        public double Score { get; set; }
    }
    public class BehaviourActionFollowup
    {
        public long ActionDetailsId { get; set; }
        public long IncidentId { get; set; }
        public long ActionId { get; set; }
        public DateTime? ActionDate { get; set; }
        public string Action_Followup_Remarks { get; set; }
        public long Action_FollowupBy_Id { get; set; }
        public long Action_FollowupBy_Designation_Id { get; set; }
        public string Action_FollowupBy_EmpNo { get; set; }
        public string Action_FollowupBy_EmpName { get; set; }
        public string Action_FollowupBy_Designation { get; set; }
        public string Action_FollowupBy_DesignationGroup { get; set; }
        public long Action_CurrentUser_DesignationId { get; set; }
        public string DataMode { get; set; }
        public long StudentInvolvedId { get; set; }
       
    }
    public class FollowUpStaff
    {
        public long EMPLOYEE_ID { get; set; }
        public string EMPLOYEE_NAME { get; set; }
    }
    public class FollowUpDesignation
    {
        public long DESIGNATION_ID { get; set; }
        public string DESIGNATION_DESCRIPTION { get; set; }
    }
}