﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Areas.SMS.Model
{
    public class ProgressTracker
    {
        public ProgressTracker()
        {
            objProgressTrackerData = new List<ProgressTrackerData>();
            objProgressTrackerHeader = new List<ProgressTrackerHeader>();
            objStudentList = new List<StudentList>();
            objTopics = new List<Topics>();
            objTopicDetails = new List<TopicDetails>();
            objObjectiveDetails = new List<ObjectiveDetails>();
            objStepsTopicDetails = new List<StepsTopicDetails>();
            objMergeStudentData = new List<MergeStudentData>();
            objProgressDropdown = new List<ProgressTrackerDropdown>();
        }

        public List<ProgressTrackerData> objProgressTrackerData { get; set; }
        public List<ProgressTrackerHeader> objProgressTrackerHeader { get; set; }
        public List<StudentList> objStudentList { get; set; }

        public List<Topics> objTopics { get; set; }
        public List<TopicDetails> objTopicDetails { get; set; }
        public List<ObjectiveDetails> objObjectiveDetails { get; set; }
        public List<StepsTopicDetails> objStepsTopicDetails { get; set; }

        public List<MergeStudentData> objMergeStudentData { get; set; }

        public List<ProgressTrackerDropdown> objProgressDropdown { get; set; }
    }
    public class BindSteps
    {
        public string SYC_STEP { get; set; }

    }

    public class TopicTree
    {
        public long SYD_ID { get; set; }
        public long SYD_PARENT_ID { get; set; }
        public string SYD_DESCR { get; set; }


    }

    public class SubTopicTree
    {
        public long id { get; set; }
        public string title { get; set; }


    }
    public class SubTerms
    {
        public int ID { get; set; }
        public string DESCRIPTION { get; set; }
        public bool LOCK_STATUS { get; set; }
        public int TERM_ID { get; set; }
        public int DISPLAY_ORDER { get; set; }
    }

    public class ProgressTrackerData
    {
        public long STU_ID { get; set; }
        public int OBJ_ID { get; set; }
        public string OBJ_VALUE { get; set; }
        public string TSM_ID { get; set; }

        public int FileCount { get; set; }
    }
    public class ProgressTrackerHeader
    {
        public int OBJ_ID { get; set; }
        public string OBJ_DESC { get; set; }
        public string OBJ_ENDDT { get; set; }
        public string TOPIC_ID { get; set; }
        public string TOPIC { get; set; }
        public string SUB_TOPIC { get; set; }
        public string STEPS { get; set; }
        public float OBJ_WIDTH { get; set; }
        public int SUBTOPIC_COLSPAN { get; set; }

        public float STEPS_WIDTH { get; set; }

    }
    public class TopicDetails
    {
        public string TopicId { get; set; }
        public string Topics { get; set; }
        public string subTopics { get; set; }
        public int subTopicColSpan { get; set; }
        public string Steps { get; set; }
        public string STEPS_WIDTH { get; set; }
    }

    public class ObjectiveDetails
    {
        public long objId { get; set; }
        public string TopicId { get; set; }

        public string objDate { get; set; }
        public double objWidth { get; set; }
        public string objDescription { get; set; }
        public string Steps { get; set; }
        public string subTopics { get; set; }
    }

    public class Topics
    {

        public string topics { get; set; }


    }
    public class StepsTopicDetails
    {
        public string TopicId { get; set; }
        public string subTopics { get; set; }
        public string Steps { get; set; }

        public double StepsWidth { get; set; }

    }
    public class MergeStudentData
    {
        public string StudentName { get; set; }
        public string StudentId { get; set; }
        public string StudentNumber { get; set; }
        public string StudentPhoto { get; set; }
        public int OBJ_ID { get; set; }
        public string OBJ_Value { get; set; }
        public string TSMID { get; set; }
    }


    public class ProgressTrackerDropdown
    {
        public string CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string COLOR_CODE { get; set; }
        public int ORDER_SEQUENCE { get; set; }

        public bool IS_DROPDOWN { get; set; }
    }
}