﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Areas.SMS.Model
{
    public class Reports
    {
        public int DEV_ID { get; set; }

        public string DEV_REPORT_NAME { get; set; }

        public Byte[] DEV_LAYOUT { get; set; }
        public int DEV_REPORT_FILTER_ID { get; set; }
    }


    public class ReportDesigner
    {
        public int DEV_ID { get; set; }
        public string DEV_REPORT_NAME { get; set; }

        public int RDSR_ID { get; set; }

        public string RDD_TITLE { get; set; }

        public string RDD_DB_NAME { get; set; }

        public string RDD_PROC_NAME { get; set; }

        public string RDSR_FILTER_TYPES { get; set; }
    }

    public class ReportBinder
    {

        public string Code { get; set; }
        public string Name { get; set; }

    }

    public class ReportFilterControls
    {
        public long RDF_ID { get; set; }
        public string RDF_FILTER_CODE { get; set; }
        public string RDF_FILTER_NAME { get; set; }
        public string RDF_FILTER_TYPE { get; set; }
        public int RDF_ORDER { get; set; }
    }

    public class ReportTypes
    {
        public int Id { get; set; }

        public string ReportType { get; set; }

        public string ReportCode { get; set; }



    }

    public class ReportSetup
    {
        public int RSM_ID { get; set; }
        public string RSM_DESCR { get; set; }
        public long RSM_BSU_ID { get; set; }
        public int RSM_ACD_ID { get; set; }
        public int RSM_DEV_ID { get; set; }
    }


}
