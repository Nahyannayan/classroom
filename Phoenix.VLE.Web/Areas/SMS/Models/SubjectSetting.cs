﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Areas.SMS.Models
{
    #region Made By Dhanaji
    [ResourceMappingRoot(Path = "SIMS.SubjectMaster")]
    public class SubjectMaster
    {
        public long SubjectId { get; set; }
        public string Subject_Description { get; set; }
        public bool IsLanguage { get; set; }
        public string Subject_ShortCode { get; set; }
        public string Subject_BoardCode { get; set; }
        public long CurriculumId { get; set; }
        public string Curriculum_Description { get; set; }
        public long SyllabusId { get; set; }
    }

    //[ResourceMappingRoot(Path = "SIMS.SubjectGroup")]
    public class SubjectGroup
    {
        [Required(ErrorMessage = "Mandatory field.")]
        public long ACD_ID { get; set; }
        public int GroupId { get; set; }
        [Required(ErrorMessage = "Mandatory field.")]
        public int SubjectGradeId { get; set; }
        [Required(ErrorMessage = "Mandatory field.")]
        public string GradeId { get; set; }
        [Required(ErrorMessage = "Mandatory field.")]
        public int ShiftId { get; set; }
        [Required(ErrorMessage = "Mandatory field.")]
        public int StreamId { get; set; }
        [Required(ErrorMessage = "Mandatory field.")]
        public string Group_Description { get; set; }
        public string Grade_Display { get; set; }
        public string Shift_Description { get; set; }
        public string Stream_Description { get; set; }
        public string SubjectGrade_Description { get; set; }
        public string SubjectGrade_Parent { get; set; }
        public string SubjectGroup_UNTIS { get; set; }
        public string StudentListId { get; set; }
        public string SelectedStudentListId { get; set; }
        public string SelectedStudentListSSD { get; set; }
        [AllowHtml]
        public string SelectedStudentListXML { get; set; }
        [AllowHtml]
        public string RemovedStudentListXML { get; set; }
        public IEnumerable<SubjectGroupStudent> SubjectGroupStudentList { get; set; }
        public IEnumerable<SubjectGroupStudent> SubjectGroupSelectedStudentList { get; set; }
        public SubjectGroupTeacher SubjectGroupTeacher { get; set; }
        public string Username { get; set; }
        public long SchoolId { get; set; }
        public int SectionId { get; set; }
    }
    public class SubjectGroupTeacherDdl
    {
        public int EMP_ID { get; set; }
        public string EMP_NAME { get; set; }
    }
    public class SubjectGroupTeacher
    {
        public long? TeacherId { get; set; }
        public int? GroupTeacherId { get; set; }
        public string TeacherName { get; set; }
        public string GroupTeacherSchedule { get; set; }
        public string GroupTeacherRoom { get; set; }
        public DateTime? FromDate { get; set; }
    }
    public class SubjectGroupStudent
    {
        public long StudentId { get; set; }
        public long StudentGroupId { get; set; }
        public string StudentNo { get; set; }
        public string Student_Name { get; set; }
        public string Section { get; set; }
        public long SectionId { get; set; }
        public string Option_Description { get; set; }
        public string Religion { get; set; }
    }
    #endregion
    public class SubjectByGradeParent
    {

        public string GradeId { get; set; }

        public int StreamId { get; set; }

        public string Grade_Display { get; set; }

        public string Stream_Description { get; set; }

        public int Grade_Order { get; set; }

    }
    public class SubjectByGradeChild
    {

        public int SubjectGradeId { get; set; }

        public int SubjectId { get; set; }

        public string SubjectGrade_Description { get; set; }

        public string SubjectGrade_ShortCode { get; set; }

        public int SubjectGrade_DepartmentId { get; set; }

        public string Subject_Department_Description { get; set; }

        public bool b_IsOptional { get; set; }

        public string OptionIds { get; set; }

        public string Option_Description { get; set; }

        public int Subject_ParentId { get; set; }

        public bool b_DisplayOnReportCard { get; set; }

        public bool b_DisplayMarks { get; set; }

        public string Subject_Description { get; set; }

        public int Display_Order { get; set; }

        public bool b_TC_Display { get; set; }

        public string SubjectGrade_ParentShortCode { get; set; }

        public bool b_IsCore { get; set; }

        public bool b_AutoGroup { get; set; }

        public bool b_Major { get; set; }

        public string MarkType { get; set; }

        public double Credits { get; set; }

        public double WT { get; set; }

        public string LENG { get; set; }

        public string HRS { get; set; }

        public string GPA { get; set; }

        public int StreamId { get; set; }
        public string GradeId { get; set; }
        public double MinMark { get; set; }
        public bool b_Retest { get; set; }
        public bool b_BrownbookDisplay { get; set; }
        public bool b_Btech { get; set; }
    }

    public class ParentSubject : SubjectByGradeChild
    {
        public int ParentId { get; set; }
        public List<ParentSubject> childs { get; set; }
    }

    public class ComboTree
    {
        public ComboTree()
        {
            this.subs = new List<ComboTree>();
        }
        public int id { get; set; }
        public string title { get; set; }
        public List<ComboTree> subs { get; set; }
    }

    public class FromStream
    {
        public int STM_ID { get; set; }
        public string STM_DESCR { get; set; }
    }
    [ResourceMappingRoot(Path = "SIMS.SubjectByGradeEntryForm")]
    public class SubjectByGradeEntry
    {
        /// <summary>
        /// to check if child data is present and depending on that disable/enable copy feature
        /// </summary>
        public bool IsChildGradeIdPresent { get; set; }
        public int FromGradeId { get; set; }
        public int FromStreamId { get; set; }
        public string ShortCode { get; set; } = null;
        public int SubjectId { get; set; }
        public int DepartmentId { get; set; }
        public string Description { get; set; }
        public bool SubjectType { get; set; }
        public string ParentSubject { get; set; }
        public string ParentSubjectDescription { get; set; }
        public string OptionName { get; set; }
        public string OptionNamesJoin { get; set; }
        [AllowHtml]
        public string OptionNamesXML { get; set; }
        public double MinimumMarks { get; set; }
        public bool b_DisplaySubjectInTC { get; set; }
        public bool b_DisplaySubjectInReportCard { get; set; }
        public bool b_DisplayInExcel { get; set; }
        public bool b_BtecSubject { get; set; }
        public bool b_Retest { get; set; }
        public bool b_MarkAsOptional { get; set; }
        public bool b_DisplayMarksInReportCard { get; set; }
        public bool b_EnterMarks { get; set; }
        public bool AutoGroup { get; set; }
        public int SubjectGradeId { get; set; }
        public string AcademicYear { get; set; }
        public string Grade { get; set; }
        public string Stream { get; set; }
        public long ACD_ID { get; set; }
        public long BSU_ID { get; set; }
        public string GRD_ID { get; set; }
        public int STM_ID { get; set; }
        public int DisplayOrder { get; set; }
        public bool b_IsCor { get; set; }
        public double Credits { get; set; }
        public double WT { get; set; }
        public string LENG { get; set; }
        public string HRS { get; set; }
        public string GPA { get; set; }
        public SelectList FromGrade { get; set; }
        public SelectList FromStream { get; set; }
        public SelectList SubjectMasters { get; set; }
        public SelectList Department { get; set; }
        public SelectList ParentSubjects { get; set; }
        public SelectList OptionNames { get; set; }
        public List<SubjectByGradeChild> SubjectByGradeChildrens { get; set; }
    }
    public class ShiftModel
    {
        public int SHF_ID { get; set; }
        public string SHF_DESCR { get; set; }
    }

}