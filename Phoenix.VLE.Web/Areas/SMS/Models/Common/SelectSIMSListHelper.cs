﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

using Phoenix.Common.Models;
using Phoenix.Common.Localization;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Areas.SMS.Model.Common
{
    public static class SelectSIMSListHelper
    {        

        #region GetSelectListData

        public static IList<ListItem> GetSelectListData(ListItems listCode)
        {
            return GetSelectListData(listCode, "", "").ToList();
        }

        public static IList<ListItem> GetSelectListData(ListItems listCode, object whereConditionParamValues)
        {
            return GetSelectListData(listCode, "", whereConditionParamValues).ToList();
        }

        public static IList<ListItem> GetSelectListData(ListItems listCode, string whereCondition, object whereConditionParamValues)
        {
            ISIMSCommonService service = new SIMSCommonService();
            return service.GetSelectListItems(StringEnum.GetStringValue(listCode), whereCondition, whereConditionParamValues).ToList();
        }

        #endregion

        #region Custom Select List Methods
        public static IList<SubjectListItem> GetSubjectListByUserId(int userId)
        {
            ISelectListService service = new SelectListService();
            return service.GetSubjectsByUserId(userId).ToList();
        }
        #endregion

        #region Miscellaneous

        #endregion
    }
}
