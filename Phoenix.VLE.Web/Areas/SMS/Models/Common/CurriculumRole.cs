﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Areas.SMS.Model.Common
{
    public class CurriculumRole
    {
        public int Id { get; set; }
        public int BSU_CLM_ID { get; set; }
        public bool IsSuperUser { get; set; }
        public int ACD_ID { get; set; }
        public long GSA_ID { get; set; }
    }
    public class Curriculum
    {
        public Int64 SchoolId { get; set; }
        public int ACD_ID { get; set; }
        public int CLM_ID { get; set; }
        public string CLM_DESCR { get; set; }
    }
    public class CourseDetails
    {
        public int COR_ID { get; set; }
        public string COR_DESCR { get; set; }
    }
    public class GradeTemplateMaster
    {
        public int  GTM_ID { get; set; }
        public string GTM_DESCR { get; set; }
    }


}