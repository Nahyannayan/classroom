﻿using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Services;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Xml.Serialization;
using System.Xml.Linq;
using Phoenix.VLE.Web.Areas.SMS.Models;
using System.Web.UI.WebControls;
using Phoenix.Common.Localization;

namespace Phoenix.VLE.Web.Areas.SMS.Controllers
{
    public class SubjectSettingController : BaseController
    {
        private ISubjectSettingService _SubjectSettingService;
        private ISIMSCommonService _SIMSCommonService;
        private const string Add = "ADD";
        private const string Edit = "EDIT";
        private const string Delete = "DELETE";

        public SubjectSettingController(ISubjectSettingService SubjectSettingService, ISIMSCommonService SIMSCommonService)
        {
            _SubjectSettingService = SubjectSettingService;
            _SIMSCommonService = SIMSCommonService;
        }
        #region Made By Dhanaji

        #region SubjectMaster

        // GET: SMS/SubjectSetting
        public ActionResult Index()
        {
            return View("~/Areas/SMS/Views/SubjectSetting/SubjectMaster.cshtml");
        }

        public ActionResult GetSubjectMasterList()
        {
            int clm_id = Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID;
            var subjectMasterList = _SubjectSettingService.GetSubjectMasterList(clm_id);

            string Actions = "<a class='table-action-text-link trigger-rightDrawer' onclick='subjectmaster.loadSubjectMasterModel(SubjectId);'>VIEW</a>";
            string LanguageActionCheck = "<a href='javascript:void(0)' class='text-center'><i class='far fa-check-square fa-2x text-success'></i></a>";
            string LanguageActionUncheck = "<a href='javascript:void(0)' class='text-center'><i class='far fa-window-close fa-2x text-danger'></a>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in subjectMasterList
                          select new
                          {
                              Curriculum_Description = item.Curriculum_Description,
                              Subject_Description = item.Subject_Description,
                              Subject_ShortCode = item.Subject_ShortCode,
                              IsLanguage = item.IsLanguage ? LanguageActionCheck : LanguageActionUncheck,
                              Subject_BoardCode = item.Subject_BoardCode,
                              Actions = Actions.Replace("SubjectId", item.SubjectId.ToString())
                          }).ToArray()
            };
            var jsonResult = Json(dataList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult SaveSubjectMaster(SubjectMaster subjectMaster)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                result = _SubjectSettingService.SubjectMasterCUD(subjectMaster, subjectMaster.SubjectId > 0 ? "EDIT" : "ADD");
            }
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult AddEditSubjectMaster(long SubjectId)
        {
            int clm_id = Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID;
            ViewBag.CurriculumList = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.Curriculum, clm_id), "ItemId", "ItemName");
            SubjectMaster subjectMaster = new SubjectMaster();
            if (SubjectId != 0)
            {
                var subjectMasterList = _SubjectSettingService.GetSubjectMasterList(clm_id);
                if (subjectMasterList.Where(m => m.SubjectId == SubjectId).Any())
                {
                    subjectMaster = subjectMasterList.Where(m => m.SubjectId == SubjectId).FirstOrDefault();
                }
            }
            return PartialView("~/Areas/SMS/Views/SubjectSetting/_AddEditSubjectMaster.cshtml", subjectMaster);
        }

        #endregion

        #region Subject Group
        public ActionResult SubjectGroupList()
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var clm_id = Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID;
            ViewBag.AcademicYear = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AcademicYear, string.Format("{0},{1},{2}", schoolid, clm_id, null)), "ItemId", "ItemName");
            return View("~/Areas/SMS/Views/SubjectSetting/SubjectGroupList.cshtml");
        }
        public ActionResult GetSubjectGroupList(long ACD_ID = 0)
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var Username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), Username);
            var IsSuperUser = (curr_role.IsSuperUser ? "Y" : "N");

            var subjectGroupList = _SubjectSettingService.GetSubjectGroupList(ACD_ID, IsSuperUser, Username);

            string Actions = "<a class='table-action-text-link' onclick=' subjectGroup.openSubjectGroupModel(GroupId);'>VIEW</a>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in subjectGroupList
                          select new
                          {
                              Group_Description = item.Group_Description,
                              SubjectGrade_Description = item.SubjectGrade_Description,
                              SubjectGrade_Parent = item.SubjectGrade_Parent,
                              Grade_Display = item.Grade_Display,
                              Shift_Description = item.Shift_Description,
                              Stream_Description = item.Stream_Description,
                              Actions = Actions.Replace("GroupId", item.GroupId.ToString())
                          }).ToArray()
            };
            var jsonResult = Json(dataList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult AddEditSubjectGroup(long SGR_ID = 0, long ACD_ID = 0)
        {
            SubjectGroupTeacher _SubjectGroupTeacher = new SubjectGroupTeacher();
            SubjectGroup _SubjectGroup = new SubjectGroup();
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var clm_id = Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID;
            var Username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), Username);
            var IsSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var subjectGroupList = _SubjectSettingService.GetSubjectGroupList(ACD_ID, IsSuperUser, Username);
            if (subjectGroupList.Where(m => m.GroupId == SGR_ID).Any())
            {
                _SubjectGroup = subjectGroupList.Where(m => m.GroupId == SGR_ID).FirstOrDefault();
            }
            _SubjectGroup.ACD_ID = ACD_ID;

            ViewBag.AcademicYear = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AcademicYear, string.Format("{0},{1},{2}", schoolid, clm_id, null)), "ItemId", "ItemName");
            ViewBag.Grade_filter = new SelectList(_SubjectSettingService.GetGradeForSubjectCopy(ACD_ID), "GradeId", "Grade_Display");
            ViewBag.Stream_Filter = new SelectList(_SubjectSettingService.GetStreamForSubjectCopy(ACD_ID, _SubjectGroup.GradeId), "STM_ID", "STM_DESCR");
            ViewBag.Subject_Filter = new SelectList(_SIMSCommonService.GetSubjectsByGrade(Convert.ToInt32(ACD_ID), _SubjectGroup.GradeId), "SBG_ID", "SBG_DESCR");
            ViewBag.Shift_Filter = new SelectList(_SubjectSettingService.GetShiftListById(ACD_ID, _SubjectGroup.GradeId), "SHF_ID", "SHF_DESCR");

            return View("~/Areas/SMS/Views/SubjectSetting/_AddEditSubjectGroup.cshtml", _SubjectGroup);
        }

        public ActionResult GetGradesByACD_ID(long ACD_ID = 0)
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var clm_id = Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID;
            var filter = _SubjectSettingService.GetGradeForSubjectCopy(ACD_ID).Select(x => new ListItem { Value = x.GradeId.ToString(), Text = x.Grade_Display }).ToList();

            return Json(filter, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BindFilterById(long ACD_ID = 0, string GRD_ID = "")
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var clm_id = Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID;
            var Stream_Filter = _SubjectSettingService.GetStreamForSubjectCopy(ACD_ID, GRD_ID).Select(x => new ListItem { Value = x.STM_ID.ToString(), Text = x.STM_DESCR }).ToList();
            var Subject_Filter = _SIMSCommonService.GetSubjectsByGrade(Convert.ToInt32(ACD_ID), GRD_ID).Select(x => new ListItem { Value = x.SBG_ID.ToString(), Text = x.SBG_DESCR }).ToList();
            var Shift_Filter = _SubjectSettingService.GetShiftListById(ACD_ID, GRD_ID).Select(x => new ListItem { Value = x.SHF_ID.ToString(), Text = x.SHF_DESCR }).ToList();
            return Json(new
            {
                Stream_Filter = Stream_Filter,
                Subject_Filter = Subject_Filter,
                Shift_Filter = Shift_Filter
            }, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult AssignTeacher()
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var Username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), Username);
            var IsSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            ViewBag.SubjectGroupTeacher = new SelectList(_SubjectSettingService.GetSubjectGroupTeachers(schoolid, IsSuperUser, Username), "EMP_ID", "EMP_NAME");
            return PartialView("~/Areas/SMS/Views/SubjectSetting/_AssignTeacher.cshtml");
        }
        public ActionResult LoadSubjectGroupTeachersList(long SGR_ID = 0)
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var Username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), Username);
            var IsSuperUser = (curr_role.IsSuperUser ? "Y" : "N");

            var subjectGroupTeacherList = _SubjectSettingService.GetSubjectGroupTeachersGrid(SGR_ID, IsSuperUser, Username);

            string Actions = "<a href='#' onclick='subjectGroup.editAssignedTeacher(GroupId,TeacherId,this)' class='table-action-icon-btn' data-toggle='tooltip' data-title='Edit' data-placement='left' data-original-title='' title=''><small><i class='fas fa-pencil-alt fa-2x'></i></small></a>" +
                             " <a href='#' onclick='subjectGroup.unAssignTeacher(GroupId,TeacherId)' class='table-action-icon-btn' data-toggle='tooltip' data-title='Delete' data-placement='left' data-original-title='' title=''><small><i class='fas fa-trash-alt fa-2x'></i></small></a>";
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in subjectGroupTeacherList
                          select new
                          {
                              TeacherId = item.TeacherId,
                              TeacherName = item.TeacherName,
                              FromDate = item.FromDate.Value.ToString("dd/MM/yyyy"),
                              GroupTeacherSchedule = item.GroupTeacherSchedule,
                              GroupTeacherRoom = item.GroupTeacherRoom,
                              Actions = Actions.Replace("TeacherId", item.TeacherId.ToString())
                                        .Replace("GroupId", item.GroupTeacherId.ToString())
                          }).ToArray()
            };
            var jsonResult = Json(dataList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        public ActionResult SaveUpdateSubjectGroup(SubjectGroup subjectGroup)
        {
            var result = false;
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var Username = SessionHelper.CurrentSession.UserName;
            subjectGroup.Username = Username;
            subjectGroup.SchoolId = schoolid;
            result = _SubjectSettingService.SaveUpdateSubjectGroup(subjectGroup, subjectGroup.GroupId > 0 ? Edit : Add);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult UnAssignGroupTeacher(int GroupId, long TeacherId)
        {
            var result = false;
            SubjectGroup _SubjectGroup = new SubjectGroup();
            SubjectGroupTeacher _SubjectGroupTeacher = new SubjectGroupTeacher();
            _SubjectGroupTeacher.FromDate = DateTime.Now;
            _SubjectGroupTeacher.TeacherId = TeacherId;
            _SubjectGroup.GroupId = GroupId;
            _SubjectGroupTeacher.GroupTeacherSchedule = string.Empty;
            _SubjectGroupTeacher.GroupTeacherRoom = string.Empty;
            _SubjectGroup.SubjectGroupTeacher = _SubjectGroupTeacher;
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var Username = SessionHelper.CurrentSession.UserName;
            result = _SubjectSettingService.SaveUpdateGroupTeacher(_SubjectGroup, Username, Delete);

            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult LoadAllocateStudentList(long ACD_ID = 0, string GRD_ID = "0", int SHF_ID = 0, int STM_ID = 0, long SBG_ID = 0, long SGR_ID = 0)
        {
            SubjectGroup _SubjectGroup = new SubjectGroup();
            _SubjectGroup.SubjectGroupStudentList = _SubjectSettingService.GetSubjectGroupStudentList(ACD_ID, GRD_ID, SHF_ID, STM_ID, SBG_ID, 0);
            if (SGR_ID > 0)
            {
                _SubjectGroup.SubjectGroupSelectedStudentList = _SubjectSettingService.GetSubjectGroupStudentList(ACD_ID, GRD_ID, SHF_ID, STM_ID, SBG_ID, SGR_ID);
                _SubjectGroup.SelectedStudentListId = _SubjectGroup.SubjectGroupSelectedStudentList.Count() > 0 ? string.Join(",", _SubjectGroup.SubjectGroupSelectedStudentList.Select(r => r.StudentId).ToList()) + "," : string.Empty;
                _SubjectGroup.SelectedStudentListSSD = _SubjectGroup.SubjectGroupSelectedStudentList.Count() > 0 ? string.Join(",", _SubjectGroup.SubjectGroupSelectedStudentList.Select(r => r.StudentGroupId).ToList()) + "," : string.Empty;
                if (_SubjectGroup.SubjectGroupStudentList.Where(r => _SubjectGroup.SubjectGroupSelectedStudentList.Any(a => a.StudentId != r.StudentId)).Any())
                {
                    _SubjectGroup.SubjectGroupStudentList = _SubjectGroup.SubjectGroupStudentList.Where(r => _SubjectGroup.SubjectGroupSelectedStudentList.Any(a => a.StudentId != r.StudentId));
                }
            }

            return PartialView("~/Areas/SMS/Views/SubjectSetting/_LoadAllocateStudentList.cshtml", _SubjectGroup);
        }
        #endregion 

        #endregion

        #region Subject By Grade

        public ActionResult SubjectByGrade()
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;//Userdetail.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var grd_access = (int)curr_role.GSA_ID;
            var clm_id = Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID;  //new to get this value from some generic class .
                                                                              // var current_acd = 1;
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            //var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, curr_role.ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            //ViewBag.Grades = list;
            // ViewBag.AgeBand = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AgeBand, null), "ItemId", "ItemName");
            ViewBag.AcademicYear = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AcademicYear, string.Format("{0},{1},{2}", schoolid, clm_id, null)), "ItemId", "ItemName");


            return View("~/Areas/SMS/Views/SubjectSetting/SubjectByGrade.cshtml");

        }
        /// <summary>
        /// To show list of Subject grade parent list
        /// </summary>
        /// <param name="ACD_ID">Academic year id</param>
        /// <returns></returns>
        public ActionResult SubjectByGradeParent(long ACD_ID = 0)
        {
            List<SubjectByGradeParent> objSubjectByGrade = new List<SubjectByGradeParent>();
            objSubjectByGrade = _SubjectSettingService.BindGradesForSubject(ACD_ID).ToList();

            ViewBag.ACD_ID = ACD_ID;
            return PartialView("~/Areas/SMS/Views/SubjectSetting/_SubjectByGradeParent.cshtml", objSubjectByGrade);

        }
        /// <summary>
        /// Populate the list of subject
        /// </summary>
        /// <param name="Acd_Id">Academic year id</param>
        /// <param name="Grade_Id">Grade Id</param>
        /// <param name="STM_Id">Stream Id</param>
        /// <param name="isParentView">to identify the call is from parent of child form</param>
        /// <returns></returns>
        public ActionResult SubjectByGradeChild(int Acd_Id = 0, string Grade_Id = "", int STM_Id = 0, bool isParentView = false)
        {

            List<SubjectByGradeChild> objSubjectByGradeChild = new List<SubjectByGradeChild>();
            objSubjectByGradeChild = _SubjectSettingService.BindSubjectsByGrade(Acd_Id, Grade_Id, STM_Id).ToList();
            ViewBag.GradeId = Grade_Id;
            ViewBag.StreamId = STM_Id;
            ViewBag.isParentView = isParentView;
            return PartialView("~/Areas/SMS/Views/SubjectSetting/_SubjectByGradeChild.cshtml", objSubjectByGradeChild);

        }
        #region Subject By Grade Setup
        /// <summary>
        /// populate the forms select field from data and if in edit mode populate the form with the records available to edit
        /// </summary>
        /// <param name="grdId">Grade Id</param>
        /// <param name="acdId">Academic year id</param>
        /// <param name="streamId">stream id</param>
        /// <param name="subjectGradeId">if null return blank form to add the records if not null the form is in edit mode</param>
        /// <returns></returns>
        public PartialViewResult SubjectByGradeEntry(string grdId, long acdId, int streamId, int? subjectGradeId = null)
        {
            var subjectDataList = _SubjectSettingService.BindSubjectsByGrade(acdId, grdId, streamId);
            SubjectByGradeEntry entry = new SubjectByGradeEntry();
            #region populateList
            entry.FromGrade = new SelectList(_SubjectSettingService.GetGradeForSubjectCopy(acdId), "GradeId", "Grade_Display");
            entry.FromStream = new SelectList(_SubjectSettingService.GetStreamForSubjectCopy(acdId, grdId), "STM_ID", "STM_DESCR");
            var subjectMasterList = _SubjectSettingService.GetSubjectMastersByCurriculum(Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID);
            subjectMasterList.ToList().ForEach(x => x.Subject_Description = $"{x.Subject_Description}|{x.Subject_ShortCode}");
            entry.SubjectMasters = new SelectList(subjectMasterList, "SubjectId", "Subject_Description");
            entry.ParentSubjects = new SelectList(_SubjectSettingService.GetParentSubjects(acdId, streamId, grdId), "SubjectGradeId", "SubjectGrade_Description");
            entry.Department = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.Department, null), "ItemId", "ItemName");
            entry.OptionNames = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.SubjectOption, null, SessionHelper.CurrentSession.SchoolId), "ItemId", "ItemName");
            var list = _SubjectSettingService.BindGradesForSubject(acdId).FirstOrDefault(x => x.GradeId == grdId && x.StreamId == streamId);
            entry.Grade = list.Grade_Display;
            entry.Stream = list.Stream_Description;
            #endregion
            entry.IsChildGradeIdPresent = subjectDataList.Any();
            entry.GRD_ID = grdId;
            entry.ACD_ID = acdId;
            entry.STM_ID = streamId;
            entry.BSU_ID = SessionHelper.CurrentSession.SchoolId;
            if (subjectGradeId != null)
            {
                var subjectData = _SubjectSettingService.BindSubjectsByGrade(acdId, grdId, streamId, (int)subjectGradeId).FirstOrDefault();
                entry.Grade = list.Grade_Display;
                entry.Stream = list.Stream_Description;
                entry.SubjectId = subjectData.SubjectId;
                entry.SubjectGradeId = subjectData.SubjectGradeId;
                entry.DepartmentId = subjectData.SubjectGrade_DepartmentId;
                entry.Description = subjectData.SubjectGrade_Description;
                entry.ShortCode = subjectData.SubjectGrade_ShortCode;
                entry.OptionName = subjectData.OptionIds;
                entry.b_MarkAsOptional = subjectData.b_IsOptional;
                entry.ParentSubject = subjectData.Subject_ParentId.ToString();
                entry.b_DisplaySubjectInReportCard = subjectData.b_DisplayOnReportCard;
                entry.b_DisplayMarksInReportCard = subjectData.b_DisplayMarks;
                entry.b_DisplaySubjectInTC = subjectData.b_TC_Display;
                entry.SubjectType = subjectData.b_Major;
                entry.b_EnterMarks = subjectData.MarkType == "Mark";
                entry.b_BtecSubject = subjectData.b_Btech;
                entry.b_DisplayInExcel = subjectData.b_BrownbookDisplay;
                entry.b_Retest = subjectData.b_Retest;
                entry.MinimumMarks = subjectData.MinMark;
                entry.SubjectGradeId = subjectData.SubjectGradeId;
                entry.AutoGroup = subjectData.b_AutoGroup;
            }
            return PartialView("~/Areas/SMS/Views/SubjectSetting/_SubjectByGradeSetup.cshtml", entry);
        }
        /// <summary>
        /// Populate stream dropdown with the grade and academic id
        /// </summary>
        /// <param name="ACD_ID">Academic year id<param>
        /// <param name="GRD_ID">Grade Id</param>
        /// <returns></returns>
        public ActionResult GetStreamFromGrade(long ACD_ID, string GRD_ID)
        {
            var result = _SubjectSettingService.GetStreamForSubjectCopy(ACD_ID, GRD_ID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Copy Subject details form the selected grade and stream
        /// </summary>
        /// <param name="ACD_ID">Academic year id</param>
        /// <param name="From_GRD_ID">Copy From Grade Id</param>
        /// <param name="From_STM_ID">Copy From Stream Id</param>
        /// <param name="To_GRD_ID">Paste To Grade Id</param>
        /// <param name="To_STM_ID">Paste To Stream Id</param>
        /// <returns></returns>
        public ActionResult CopySubjectByGrade(long ACD_ID, string From_GRD_ID, int From_STM_ID, string To_GRD_ID, int To_STM_ID)
        {
            List<SubjectByGradeEntry> SubjectByGradeEntryList = new List<SubjectByGradeEntry>();
            var subjectDataList = _SubjectSettingService.BindSubjectsByGrade(ACD_ID, From_GRD_ID, From_STM_ID);
            var list = _SubjectSettingService.BindGradesForSubject(ACD_ID).FirstOrDefault(x => x.GradeId == From_GRD_ID && x.StreamId == From_STM_ID);
            foreach (var subjectData in subjectDataList)
            {
                SubjectByGradeEntry entry = new SubjectByGradeEntry();
                entry.Grade = list.Grade_Display;
                entry.Stream = list.Stream_Description;
                entry.SubjectId = subjectData.SubjectId;
                entry.SubjectGradeId = subjectData.SubjectGradeId;
                entry.DepartmentId = subjectData.SubjectGrade_DepartmentId;
                entry.Description = subjectData.SubjectGrade_Description;
                entry.ShortCode = subjectData.SubjectGrade_ShortCode;
                entry.OptionName = subjectData.OptionIds;
                entry.b_MarkAsOptional = subjectData.b_IsOptional;
                entry.ParentSubject = subjectData.Subject_ParentId.ToString();
                entry.b_DisplaySubjectInReportCard = subjectData.b_DisplayOnReportCard;
                entry.b_DisplayMarksInReportCard = subjectData.b_DisplayMarks;
                entry.b_DisplaySubjectInTC = subjectData.b_TC_Display;
                entry.SubjectType = subjectData.b_Major;
                entry.b_EnterMarks = subjectData.MarkType == "Mark";
                entry.GRD_ID = To_GRD_ID;
                entry.ACD_ID = ACD_ID;
                entry.STM_ID = To_STM_ID;
                entry.BSU_ID = SessionHelper.CurrentSession.SchoolId;
                entry.b_BtecSubject = subjectData.b_Btech;
                entry.b_DisplayInExcel = subjectData.b_BrownbookDisplay;
                entry.b_Retest = subjectData.b_Retest;
                entry.MinimumMarks = subjectData.MinMark;
                entry.SubjectGradeId = subjectData.SubjectGradeId;
                entry.b_IsCor = subjectData.b_IsCore;
                entry.DisplayOrder = subjectData.Display_Order;
                entry.AutoGroup = subjectData.b_AutoGroup;
                entry.Credits = subjectData.Credits;
                entry.WT = subjectData.WT;
                entry.LENG = subjectData.LENG;
                entry.HRS = subjectData.HRS;
                entry.GPA = subjectData.GPA;
                SubjectByGradeEntryList.Add(entry);
            }
            var response = _SubjectSettingService.SubjectGrade(SubjectByGradeEntryList);
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubjectByGradeEntry(SubjectByGradeEntry subjectByGradeEntry)
        {
            var subjectData = _SubjectSettingService.BindSubjectsByGrade(subjectByGradeEntry.ACD_ID, subjectByGradeEntry.GRD_ID, subjectByGradeEntry.STM_ID, subjectByGradeEntry.SubjectGradeId).FirstOrDefault();
            subjectByGradeEntry.b_IsCor = subjectData.b_IsCore;
            subjectByGradeEntry.DisplayOrder = subjectData.Display_Order;
            subjectByGradeEntry.Credits = subjectData.Credits;
            subjectByGradeEntry.WT = subjectData.WT;
            subjectByGradeEntry.LENG = subjectData.LENG;
            subjectByGradeEntry.HRS = subjectData.HRS;
            subjectByGradeEntry.GPA = subjectData.GPA;
            var response = _SubjectSettingService.SubjectGrade(new List<SubjectByGradeEntry> { subjectByGradeEntry });
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }
        public ActionResult SubjectByGradeDelete(int subjectGradeId)
        {
            var response = _SubjectSettingService.SubjectGradeDelete(subjectGradeId);
            if (response.Contains("Groups Exists!!!!"))
                return Json(new OperationDetails(false, response), JsonRequestBehavior.AllowGet);
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddOptionName(int schoolId, string OptionName)
        {
            var isSuccess = _SubjectSettingService.AddOptionName(schoolId, OptionName);
            if (isSuccess)
            {
                var options = SelectSIMSListHelper.GetSelectListData(ListItems.SubjectOption, null, SessionHelper.CurrentSession.SchoolId);
                return Json(options, JsonRequestBehavior.AllowGet);
            }
            return Json(isSuccess, JsonRequestBehavior.AllowGet);
        }
        public ActionResult loadOptionNames(int schoolId)
        {
            var options = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.SubjectOption, "BSU_ID", SessionHelper.CurrentSession.SchoolId));
            return Json(options, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public JsonResult BindParentOptions(long acdId = 0, int streamId = 0, string grdId = "")
        {
            List<ComboTree> objJsonResult = new List<ComboTree>();
            var parentSubjectList = _SubjectSettingService.GetParentSubjects(acdId, streamId, grdId).ToList();
            var listsubtopic = parentSubjectList.Where(e => e.ParentId > 0).ToList();
            var listParentTopic = parentSubjectList.Where(e => e.ParentId == 0).ToList();
            foreach (var topicList in parentSubjectList)
            {
                List<ComboTree> strSubTopic = new List<ComboTree>();
                var Id = listParentTopic.Where(e => e.SubjectGradeId == topicList.SubjectGradeId).Select(e => e.SubjectGradeId).FirstOrDefault();
                if (Id > 0)
                {
                    var title = listParentTopic.Where(e => e.SubjectGradeId == topicList.SubjectGradeId).Select(e => e.SubjectGrade_Description).FirstOrDefault();
                    var lstsubtopics = listsubtopic.Where(e => e.ParentId == Id).ToList();
                    if (lstsubtopics.Count > 0)
                    {
                        foreach (var subtopic in lstsubtopics)
                        {
                            strSubTopic.Add(new ComboTree
                            {
                                id = subtopic.SubjectGradeId,

                                title = subtopic.SubjectGrade_Description
                            });
                        }
                    }
                    objJsonResult.Add(new ComboTree
                    {
                        id = Id,
                        title = title,
                        subs = strSubTopic
                    });
                }
            }
            return Json(objJsonResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChangeGroup(long sgrId = 399041, long acdId = 1417)
        {
            SubjectGroup subjectGroup = new SubjectGroup();
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var clm_id = Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID;
            var Username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), Username);
            var IsSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var subjectGroupList =
                _SubjectSettingService.GetSubjectGroupList(acdId, IsSuperUser, Username);
            subjectGroup.ACD_ID = acdId;
            ViewBag.AcademicYear = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AcademicYear,
                $"{schoolid},{clm_id},{null}"), "ItemId", "ItemName");
            ViewBag.SubjectGroupTeacher = new SelectList(_SubjectSettingService.GetSubjectGroupTeachers(schoolid, IsSuperUser, Username), "EMP_ID", "EMP_NAME");
            return View("~/Areas/SMS/Views/SubjectSetting/TeacherAssingment.cshtml", subjectGroup);
        }

        #endregion
    }
}