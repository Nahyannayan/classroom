﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Areas.SMS.Models.ViewModels;
using System.Data;
using System.Dynamic;
using Newtonsoft.Json.Converters;

namespace Phoenix.VLE.Web.Areas.SMS.Controllers
{
    public class ReportsController : Controller
    {
        // GET: SMS/Reports

        ReportService _reportsService = new ReportService();

        public ActionResult Index()
        {
            List<SelectListItem> lisofReportType = _reportsService.GetReportTypes().ToList().ConvertAll(a =>
            {
                return new SelectListItem()
                {
                    Text = a.ReportType.ToString(),
                    Value = a.ReportCode.ToString(),
                    Selected = false
                };
            });

            ViewBag.lisofReportType = lisofReportType;

            return View("~/Areas/SMS/Views/Reports/ReportIndex.cshtml");
        }

        public ActionResult LoadReportsByReportType(string ReportType = "")
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            List<ReportDesigner> objRpt = new List<ReportDesigner>();
            objRpt = _reportsService.LoadDesignedReports(schoolid.ToString(), ReportType).ToList();

            List<SelectListItem> lisofReportType = objRpt.ConvertAll(a =>
            {
                return new SelectListItem()
                {
                    Text = a.DEV_REPORT_NAME.ToString(),
                    Value = a.DEV_ID.ToString(),
                    Selected = false
                };
            });
            return Json(lisofReportType, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadReportFormControls(string ReportType = "", string Dev_Id = "")
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            List<ReportBinder> objReportBinder = new List<ReportBinder>();
            var schoolid = Userdetail.SchoolId;
            ReportDesigner objRpt = new ReportDesigner();
            objRpt = _reportsService.LoadDesignedReports(schoolid.ToString(), ReportType).Where(e => e.DEV_ID == Convert.ToInt32(Dev_Id)).FirstOrDefault();
            string RDSRFilter = objRpt.RDSR_FILTER_TYPES.Replace("||", ",");
            string[] arrFilterType = RDSRFilter.Split(',');
            string Filtercode = arrFilterType[0] != "BSU_ID" ? "BSU_ID:" + schoolid + "|" : arrFilterType[0] + ":" + schoolid + "|";

            objReportBinder = _reportsService.BindReportFilters(objRpt.RDSR_ID, arrFilterType[0], Filtercode).ToList();
            List<SelectListItem> lisofReportType = objReportBinder.ConvertAll(a =>
            {
                return new SelectListItem()
                {
                    Text = a.Name.ToString(),
                    Value = a.Code.ToString(),
                    Selected = false
                };
            });
            ViewBag.ReportFormControl = _reportsService.GetReportFiltersType().ToList();
            ViewBag.DropDownValue = lisofReportType;
            ViewBag.RDSR_ID = objRpt.RDSR_ID;
            ViewBag.Dev_Id = objRpt.DEV_ID;
            return PartialView("~/Areas/SMS/Views/Reports/_ReportFormControls.cshtml", objRpt);
        }


        public ActionResult PopulateReportFormControls(long RDSR_ID, string RDF_FILTER_CODE, List<PopulatereportFilter> objPopulateDropdownlist)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            List<ReportBinder> objReportBinder = new List<ReportBinder>();
            string filtervalues = objPopulateDropdownlist[0].FILTER_CODE != "BSU_ID" ? "BSU_ID:" + schoolid + "|" : string.Empty; //Convert.ToString(objPopulateDropdownlist[0].FILTER_CODE +":"+ objPopulateDropdownlist[0].Filter_Value+"|"): "BSU_ID:"+ schoolid +"|";
            foreach (var item in objPopulateDropdownlist)
            {

                filtervalues += Convert.ToString(item.FILTER_CODE + ":" + item.Filter_Value + "|");
            }

            objReportBinder = _reportsService.BindReportFilters(RDSR_ID, RDF_FILTER_CODE, filtervalues).ToList();

            return Json(objReportBinder, JsonRequestBehavior.AllowGet);


        }
        public ActionResult PopulateMultiselectReportFormControls(long RDSR_ID, string RDF_FILTER_CODE, string currentFilterCode, List<PopulatereportFilter> objPopulateDropdownlist)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            List<ReportBinder> objReportBinder = new List<ReportBinder>();
            string filtervalues = objPopulateDropdownlist[0].FILTER_CODE != "BSU_ID" ? "BSU_ID:" + schoolid + "|" : string.Empty; //Convert.ToString(objPopulateDropdownlist[0].FILTER_CODE +":"+ objPopulateDropdownlist[0].Filter_Value+"|"): "BSU_ID:"+ schoolid +"|";
            foreach (var item in objPopulateDropdownlist)
            {
                if (currentFilterCode == item.FILTER_CODE)
                {
                    item.Filter_Value = item.Filter_Value.Replace(',', '#');
                }
                filtervalues += Convert.ToString(item.FILTER_CODE + ":" + item.Filter_Value + "|");
            }

            objReportBinder = _reportsService.BindReportFilters(RDSR_ID, RDF_FILTER_CODE, filtervalues).ToList();

            return Json(objReportBinder, JsonRequestBehavior.AllowGet);


        }

        public ActionResult ViewReport(List<PopulatereportFilter> objlstcontroldetails)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            
            var schoolid = Userdetail.SchoolId;
            var details = _reportsService.GetDatasetForSp("oasis..GETREPORTDESIGNER_MARK_DETAILS", "BSU_ID:131001|ACD_ID:1417|GRD_ID:09#10|SCT_ID:81453#81495|RSM_ID:3830|RPF_ID:8709|");
            var values = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(details);
            List<string> headers = new List<string>();
            DataTable dt = new DataTable();
            var lisofdetails = values[0];


            foreach (string column in values[0].Keys)
            {
                dt.Columns.Add(column);
            }

            foreach (Dictionary<string, string> dictionary in values)
            {
                DataRow dataRow = dt.NewRow();

                foreach (string column in dictionary.Keys)
                {
                    dataRow[column] = dictionary[column];
                }

                dt.Rows.Add(dataRow);
            }
            var objRpt = _reportsService.LoadDesignedReports(schoolid.ToString(), "C0").Where(e => e.DEV_ID == 25).FirstOrDefault();
            return null;
        }







    }
}