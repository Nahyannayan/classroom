﻿using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using System.Net;
using System.IO.Compression;
using System.Text;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using SMS.Web.Services;
using SMS.Web.Areas.SMS.ViewModels;

namespace Phoenix.VLE.Web.Areas.SMS.Controllers
{
    [Authorize]
    public class BehaviourController : BaseController
    {

        private IBehaviourService _BehaviourService;
        private ISIMSCommonService _SIMSCommonService;

        public BehaviourController(IBehaviourService BehaviourService, ISIMSCommonService SIMSCommonService)
        {
            _BehaviourService = BehaviourService;
            _SIMSCommonService = SIMSCommonService;
        }


        // GET: SIMS/Behaviour
        public ActionResult Index()
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            return View("~/Areas/SMS/Views/Behaviour/Behaviour.cshtml");
        }
        [HttpGet]
        public JsonResult FetchSubCategory(int id)
        {
            //var Userdetail = Helpers.CommonHelper.GetLoginUser();
            //var schoolid = Userdetail.SchoolId;
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var SubCategory = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourSubCategory, string.Format("{0},{1}", id, schoolid)), "ItemId", "ItemName");
            return Json(SubCategory, JsonRequestBehavior.AllowGet);
        }
        public ViewResult LoadBehaviour(int tt_id = 0, string grade = null, string section = null, string stu_id = null)
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");
            var BehaviourVM = new BehaviourVM();
            BehaviourVM.classList = _BehaviourService.GetStudentList(username, tt_id, grade, section);
            BehaviourVM.behaviour = _BehaviourService.LoadBehaviourByStudentId(stu_id);
            return View("~/Areas/SMS/Views/Behaviour/Behaviour.cshtml", BehaviourVM);
        }
        public ActionResult GetStudentList(int tt_id = 0, string grade = null, string section = null, string stu_id = null, int IstudentBehaviour = 0)
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            var BehaviourVM = new BehaviourVM();
            
            if (IstudentBehaviour == 1)
            {
                BehaviourVM.classList = _BehaviourService.GetBehaviourClassList(username, tt_id, grade, section);
                ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");

                var result = new { Result = ConvertViewToString("~/Areas/SMS/Views/Behaviour/_StudentBehaviourList.cshtml", BehaviourVM) };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                BehaviourVM.classList = _BehaviourService.GetStudentList(username, tt_id, grade, section);
                BehaviourVM.behaviour = _BehaviourService.LoadBehaviourByStudentId(stu_id);
                return PartialView("~/Areas/SMS/Views/Behaviour/_BehaviourBodyPartial.cshtml", BehaviourVM);

            }

        }
        public ActionResult LoadBehaviourHistoryGrid(string stu_id)
        {

            var behaviourHistory = _BehaviourService.LoadBehaviourByStudentId(stu_id);
            var dataList = new object();
            string editbutton = "<a   onclick='" +
                "editBehaviour($(this),{0})'><i class='fa fa-pen'></i></a>";

            dataList = new
            {
                aaData = (from item in behaviourHistory
                          select new
                          {

                              item.Student_Name,
                              item.Type,
                              item.Comments,
                              item.Recorded_by,
                              item.Recorded_Date,
                              editbutton = string.Format(editbutton, item.Behaviour_ID),
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult EditBehaviourForm(int? id)
        {

            var schoolid = SessionHelper.CurrentSession.SchoolId;
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");

            //List<SelectListItem> priorityli = new List<SelectListItem>();
            //priorityli.Add(new SelectListItem { Text = "High", Value = "High" });
            //priorityli.Add(new SelectListItem { Text = "Medium", Value = "Medium" });
            //priorityli.Add(new SelectListItem { Text = "Low", Value = "Low" });
            //ViewBag.PriorityEdit = priorityli;
            var model = new BehaviourVM();
            if (id.HasValue)
            {
                var behaviourDetails = _BehaviourService.GetBehaviourById(id.Value);
                //var _tmp=  model.behaviourDetails.FirstOrDefault();
                //_tmp.Recorded_By
                ////EntityMapper<BehaviourDetails, BehaviourVM>.Map(category, model);
                //model.behaviourDetails.FirstOrDefault().Recorded_By = category.UserId.ToString();
                //var duedate = Convert.ToDateTime(category.EndDate).ToString("dd/MM/yyyy");
                //model.EndDate = duedate;
            }
            
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            ViewBag.IncidentGrades = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.Grade, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID), "ItemId", "ItemName");
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null).Where(e=>e.ItemName == "Negative Behavior"), "ItemId", "ItemName");

            return PartialView("~/Areas/SMS/Views/Behaviour/_AddEditBehaviourPartial.cshtml", model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SaveBehaviourDetail(BehaviourVM BehaviourVM, string listOfStudentIds, string[] ddlOtherStudentList,string[] ddlwitnessStudentList,string[] ddlwitnessstaffstring,string txtWitnessComment)
        {
            string StudentIds = string.Empty;
            string otherStudentIds = string.Empty;
            string WitnessStudentIds = string.Empty;
            string otherwithnessstaff = string.Empty;
            if (ddlOtherStudentList != null)
            {
                otherStudentIds = string.Join(",", ddlOtherStudentList);
            }
            if (ddlwitnessStudentList != null)
            {
                WitnessStudentIds = string.Join(",", ddlwitnessStudentList);
            }
            if (ddlwitnessstaffstring != null)
            {
                otherwithnessstaff = string.Join(",", ddlwitnessstaffstring);
            }
            var behaviourDetails = BehaviourVM.behaviourDetails;
          //  var Userdetail = Phoenix.VLE.Web.Helpers.CommonHelper.GetLoginUser();
            int Userid = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var result = false;
            string relativePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {

                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;
                        var extension = System.IO.Path.GetExtension(file.FileName);

                        //content for file upload
                        string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                        string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSBehaviour;
                        //string fileModule = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFilesDir;
                        string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSBehaviour + "/User_" + SessionHelper.CurrentSession.Id;

                        Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                        Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);

                        Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                        fname = Guid.NewGuid() + "_" + fname;
                        relativePath = Constants.AssignmentFilesDir + "/User_" + SessionHelper.CurrentSession.Id + "/" + fname;
                        fname = Path.Combine(filePath, fname);
                        file.SaveAs(fname);
                        behaviourDetails.DocPath = relativePath;
                        behaviourDetails.FileName = file.FileName;
                    }



                }
                catch (Exception e)
                {



                }




            }
            behaviourDetails.otherStudentEnvolved = otherStudentIds;
            behaviourDetails.WitnessStaffIds = otherwithnessstaff;
            behaviourDetails.WitnessStudentIds = WitnessStudentIds;
            behaviourDetails.WitnessStatement = txtWitnessComment;
            behaviourDetails.Recorded_Date = DateTime.Now;
            behaviourDetails.Followup_Date = DateTime.Now;
            result = _BehaviourService.InsertBehaviourDetails(behaviourDetails, schoolid.ToString());


            //behaviourDetails.Recorded_Date = DateTime.Now;
            //behaviourDetails.Incident_Date = DateTime.Now;
            //behaviourDetails.Followup_Date = DateTime.Now;



            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetStudentByGrade(int grade = 0, int section = 0)
        {
            var currentUser = Helpers.CommonHelper.GetLoginUser();
            var username = currentUser.UserName;

            var studentDetails = _BehaviourService.GetStudentList(username, 0, grade.ToString(), section.ToString()).ToList();
            List<SelectListItem> studentSelectList = studentDetails.ConvertAll(a =>
            {
                return new SelectListItem()
                {
                    Text = a.Student_Name.ToString(),
                    Value = a.Student_ID,
                    Selected = false
                };
            });
            return Json(studentSelectList, JsonRequestBehavior.AllowGet);
        }


        //Added studentbehaviour region for add edit behaviour of particular  student 
        #region StudentBehaviour

        public ActionResult StudentBehaviour()
        {
            BehaviourVM objBehaviourVM = new BehaviourVM();
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");
            return View("~/Areas/SMS/Views/Behaviour/StudentBehaviourIndex.cshtml", objBehaviourVM);
            // return View("~/Areas/SMS/Views/Behaviour/StudentBehaviourList.cshtml");
        }

        public ActionResult SelectedBehaviour(int studentId = 0)
        {
            StudentBehaviourVM objStudentBehavior = new StudentBehaviourVM();
            objStudentBehavior.BehaviourList = _BehaviourService.GetStudentBehaviorByStudentId(studentId).ToList();
            objStudentBehavior.IsSelectMode = true;
            var result = new { Result = ConvertViewToString("_StudentBehaviourModelList", objStudentBehavior) };

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AddEditStudentBehaviour(long StudentId = 0, int BehaviourId = 0, string txtAreaBehaviourComment = "")
        {
            var a = Request.Files.Count;
            var fileName = Request.Files[0].FileName;
            string relativePath = string.Empty;
            StringBuilder sb = new StringBuilder();
            string docpath = string.Empty;
            string FileName = string.Empty;
            List<StudentBehaviourFiles> objFiles = new List<StudentBehaviourFiles>();
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(files[i].FileName))
                        {


                            HttpPostedFileBase file = files[i];
                            string fname = file.FileName;
                            var extension = System.IO.Path.GetExtension(file.FileName);

                            //content for file upload
                            string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                            string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSBehaviour;
                            //string fileModule = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFilesDir;
                            string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSBehaviour + "/User_" + SessionHelper.CurrentSession.Id;

                            Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                            Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);

                            Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                            fname = Guid.NewGuid() + "_" + fname;
                            relativePath = Constants.SIMSBehaviour + "/User_" + SessionHelper.CurrentSession.Id + "/" + fname;
                            fname = Path.Combine(filePath, fname);
                            file.SaveAs(fname);
                            docpath = relativePath;
                            sb.Append(relativePath);
                            FileName = file.FileName;
                            objFiles.Add(new StudentBehaviourFiles { StudentId = StudentId, BehaviourId = BehaviourId, FileName = FileName, UploadedFilePath = docpath });
                        }
                    }



                }
                catch (Exception e) { }
            }
            var isActionperformed = _BehaviourService.InsertUpdateStudentBehavior(objFiles,StudentId, BehaviourId, txtAreaBehaviourComment);
            // var result = _BehaviourService.InsertBulkStudentBehaviour(studentId.ToString(), behaviourId);
            StudentBehaviourVM objStudentBehavior = new StudentBehaviourVM();
            if (isActionperformed)
            {

                return Json(new OperationDetails(isActionperformed), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new OperationDetails(isActionperformed), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult StudentBehaviourList(int categoryId = 0)
        {
            categoryId = categoryId == 0 ? 1 : categoryId;
            StudentBehaviourVM obj = new StudentBehaviourVM();
            obj.BehaviourList = _BehaviourService.GetListOfStudentBehaviour().Where(e => e.IsActive == 1 && e.CategoryId == categoryId).ToList();
            var ItemName = SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null).Where(e => e.ItemId == categoryId.ToString()).Select(e => e.ItemName).FirstOrDefault();
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");
            ViewBag.ItemName = ItemName;
            obj.CategoryId = categoryId;
            return PartialView("_StudentBehaviourModelList", obj);
        }

        public ActionResult GetSelectedBehaviour(int behaviourId = 0)
        {
            return View();
        }

        [HttpPost]
        public ActionResult BulkInsert(string[] lstofstudentId, int BehaviourId = 0, string txtAreaBehaviourComment = "")
        {
            string relativePath = string.Empty;
            string docpath = string.Empty;
            string FileName = string.Empty;
            List<StudentBehaviourFiles> objFiles = new List<StudentBehaviourFiles>();
            string Id = "";
            if (lstofstudentId != null)
            {
                Id = string.Join(",", lstofstudentId);
               
            }
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {

                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;
                        var extension = System.IO.Path.GetExtension(file.FileName);

                        //content for file upload
                        string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                        string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSBehaviour;
                        //string fileModule = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFilesDir;
                        string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSBehaviour + "/User_" + SessionHelper.CurrentSession.Id;

                        Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                        Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);

                        Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                        fname = Guid.NewGuid() + "_" + fname;
                        relativePath = Constants.SIMSBehaviour + "/User_" + SessionHelper.CurrentSession.Id + "/" + fname;
                        fname = Path.Combine(filePath, fname);
                        file.SaveAs(fname);
                        docpath = relativePath;
                        FileName = file.FileName;

                        List<long> TagIds = Id.Split(',').Select(long.Parse).ToList();
                        foreach (var s in TagIds)
                        {
                            objFiles.Add(new StudentBehaviourFiles { StudentId = s, BehaviourId = BehaviourId, FileName = FileName, UploadedFilePath = docpath });

                        }
                    }



                }
                catch (Exception e) { }
            }
            var isActionperformed = _BehaviourService.InsertBulkStudentBehaviour(objFiles, Id, BehaviourId, txtAreaBehaviourComment);
            if (isActionperformed)
            {
                return Json(new OperationDetails(isActionperformed), JsonRequestBehavior.AllowGet);
            }

            return null;
        }
        private string ConvertViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter writer = new StringWriter())
            {
                ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }


        public ActionResult EditStudentBehaviourType(int behaviourId = 0)
        {
            StudentBehaviourVM objStudentBehaviour = new StudentBehaviourVM();
            if (behaviourId != 0)
            {
                objStudentBehaviour.BehaviourList = _BehaviourService.GetListOfStudentBehaviour().Where(e => e.BehaviourId == behaviourId).ToList();
                objStudentBehaviour.IsSelectMode = true;
            }
            else
            {
                objStudentBehaviour.BehaviourList = _BehaviourService.GetListOfStudentBehaviour().Where(e => e.IsActive == 0).ToList();


            }
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");
            return PartialView("_StudentBehaviourTypeAddEdit", objStudentBehaviour);
        }

        public ActionResult EditStudentTypeById(int behaviourId = 0, string behaviourType = "", int behaviourPoint = 0, int categoryId = 0)
        {
            if (behaviourId > 0)
            {
                var IsActionPerformed = _BehaviourService.UpdateBehaviourTypes(behaviourId, behaviourType, behaviourPoint, categoryId);

                return Json(new OperationDetails(IsActionPerformed), JsonRequestBehavior.AllowGet);

            }

            return null;


        }


        public ActionResult OnselectLoadBehaviourLogo(int behaviourId = 0)
        {
            if (behaviourId > 0)
            {
                var BehaviourLogo = _BehaviourService.GetListOfStudentBehaviour().Where(e => e.IsActive == 0 && e.BehaviourId == behaviourId).FirstOrDefault();
                return Json(BehaviourLogo.BehaviourLogo, JsonRequestBehavior.AllowGet);
            }
            return null;

        }


        public ViewResult LoadStudentBehaviour(string strtt_id = "", string grade = null, string section = null, string stu_id = null)
        {
            int tt_id = 0;
            if (!string.IsNullOrWhiteSpace(strtt_id))
            {
                tt_id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
            }
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            ViewBag.Category = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.BehaviourMainCategory, null), "ItemId", "ItemName");
            var BehaviourVM = new BehaviourVM();
            BehaviourVM.classList = _BehaviourService.GetBehaviourClassList(username, tt_id, grade, section);
            BehaviourVM.behaviour = _BehaviourService.LoadBehaviourByStudentId(stu_id);
            ViewBag.tt_Id = tt_id;
            return View("~/Areas/SMS/Views/Behaviour/StudentBehaviourIndex.cshtml", BehaviourVM);
        }


        public FileResult DownloadAttachment(int studentId = 0)
        {
            if (studentId != 0)
            {
                var FileDetails = _BehaviourService.GetStudentBehaviorByStudentId(studentId).ToList();

                if (FileDetails != null && !string.IsNullOrWhiteSpace(FileDetails[0].FileName))
                {
                    string path = PhoenixConfiguration.Instance.ReadFilePath + FileDetails[0].UploadedFilePath;
                    WebClient myWebClient = new WebClient();
                    byte[] myDataBuffer = myWebClient.DownloadData(path);
                    return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, FileDetails[0].FileName);
                }



            }

            return null;


        }

        public ActionResult Download(long studentId =0)
        {
           
            var FileDetails = _BehaviourService.GetFileDetailsByStudentId(studentId).ToList();
            //var Filecount = FileDetails.ToList();
            using (var memoryStream = new MemoryStream())
            {
                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    for (int i = 0; i < FileDetails.Count(); i++)
                    {
                        
ziparchive.CreateEntryFromFile(PhoenixConfiguration.Instance.ReadFilePath+ FileDetails[i].UploadedFilePath, FileDetails[i].FileName);
                    }
                }
                return File(memoryStream.ToArray(), "application/zip", "Attachments.zip");
            }
        }



        public ActionResult DeleteStudentBehaviour(long studentId =0,int BehaviourId = 0)
        {
            StudentBehaviourVM objStudentBehavior = new StudentBehaviourVM();
           
            var IsActionPerformed = _BehaviourService.DeleteStudentBehaviourMapping(studentId, BehaviourId);
            if (IsActionPerformed)
            {
                objStudentBehavior.BehaviourList = _BehaviourService.GetStudentBehaviorByStudentId(studentId).ToList();
                objStudentBehavior.IsSelectMode = true;
                var result = new { Result = ConvertViewToString("_StudentBehaviourModelList", objStudentBehavior), OperationMessage= new OperationDetails(IsActionPerformed) };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objStudentBehavior.BehaviourList = _BehaviourService.GetStudentBehaviorByStudentId(studentId).ToList();
                var result = new { Result = ConvertViewToString("_StudentBehaviourModelList", objStudentBehavior), OperationMessage = new OperationDetails(IsActionPerformed) };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
           
        }
        #endregion




    }
}