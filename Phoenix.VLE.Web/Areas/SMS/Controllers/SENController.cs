﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using Phoenix.VLE.Web.Areas.SMS.Model;
using System.Text;
using System.IO.Compression;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;

namespace Phoenix.VLE.Web.Areas.SMS.Controllers
{
    [Authorize]
    public class SENController : BaseController
    {

        private ISENService _SENService;
        private ISIMSCommonService _SIMSCommonService;
        private IClassListService _ClassListService;
        #region SEN PROFILE       

        public SENController(ISENService SENService, ISIMSCommonService ISIMSCommonService, IClassListService ClassListService)
        {
            _SENService = SENService;
            _SIMSCommonService = ISIMSCommonService;
            _ClassListService = ClassListService;
        }

        // GET: SMS/Assessment
        public ActionResult Index()
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            return View("~/Areas/SMS/Views/SEN/SenStudentInclusion.cshtml");
        }

        public ActionResult GetSenStudentList(int tt_id = 0, string grade = null, string section = null, string stu_id = null)
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;

            var senModel = new SEN();
            senModel.studentInclusionAll = _SENService.Get_studentInclusionAll(schoolid.ToString(), Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID.ToString(), grade, section);
            senModel.studentInclusionList = _SENService.Get_studentInclusionList(schoolid.ToString(), Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID.ToString(), grade, section);

            return PartialView("~/Areas/SMS/Views/SEN/_SenInclusionList.cshtml", senModel);

        }
        [HttpPost]
        public ActionResult BulkInsert(string[] lstofstudentId)
        {
            if (lstofstudentId != null)
            {
                StringBuilder stuIds = new StringBuilder();

                foreach (string stuId in lstofstudentId)
                {
                    stuIds.Append(stuId + "|");
                }
                var isActionperformed = _SENService.InsertBulkSEN(stuIds.ToString());
                if (isActionperformed)
                {
                    return Json(new OperationDetails(isActionperformed), JsonRequestBehavior.AllowGet);
                }
            }
            else {
                return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
            }
            return null;
        }


        [HttpGet]
        public ActionResult updateSenStudent(string stuId="")
        {
            
            var isActionperformed = _SENService.updateSenStudent(stuId);
            if (isActionperformed)
            {
                return Json(new OperationDetails(isActionperformed), JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        #endregion

        #region KHDA


      
        public ActionResult KHDA(string stuId)
        {
            var Userdetail = Phoenix.VLE.Web.Helpers.CommonHelper.GetLoginUser();
            ViewBag.UserName = Userdetail.UserName.ToString();

            var model = new KHDA();
            model.STudentDetails = _ClassListService.GetStudentDetails(stuId);
            model.SEN_KHDA_MASTER_LIST = _SENService.Get_SEN_KHDA_MASTER();
            model.KHDA_STUDENT = _SENService.Get_KHDA_STUDENT(stuId);
            model.SEN_KHDA_TRANS_LIST = _SENService.Get_SEN_KHDA_TRANS_LIST(stuId);
            return View("~/Areas/SMS/Views/SEN/SenStudentKHDA.cshtml", model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SaveSENKHDA(KHDA_STUDENT model, List<SEN_KHDA_TRANS> modalList)
        {
            var result = false;

            if (Session["uGUID"] == null)
            {
                Session["uGUID"] = string.Empty;
            }
            result = _SENService.SaveSENKHDA(model, modalList,Convert.ToString(Session["uGUID"]));
            if (result)
            {
                Session["uGUID"] = null;
            }
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult SaveKHDAFiles(int SKT_ID=0,string TABLE_ID = "",int ROW_NUM = 0,int SKP_ID=0)
        {

            string relativePath = string.Empty;
            string docpath = string.Empty;
            string FileName = string.Empty;           
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {

                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;
                        if (fname != "" && fname != null)
                        {
                        
                            var extension = System.IO.Path.GetExtension(file.FileName);
                            //content for file upload
                            if (Session["uGUID"] == null)
                            {
                                Session["uGUID"] = Guid.NewGuid().ToString();
                            }
                         
                            string filePath = filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSKHDA_TEMP + "/" + Session["uGUID"] + "/" + TABLE_ID + "/" + ROW_NUM;
                            Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);

                            //relativePath = Constants.SIMSBehaviour + "/User_" + SessionHelper.CurrentSession.Id + "/" + fname;
                            fname = Path.Combine(filePath, fname);
                            file.SaveAs(fname);
                            // docpath = relativePath;
                            FileName = file.FileName;
                        }
                    } 
                }
                catch (Exception e) {
                }
            }

            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
            
        }



        public ActionResult DownloadSaveKHDAFiles(int SKT_ID = 0, string TABLE_ID = "", int ROW_NUM = 0, int SKP_ID = 0)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    string filePath = string.Empty;
                    if (Session["uGUID"] != null)
                    {
                        filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSKHDA_TEMP + "/" + Session["uGUID"] + "/" + TABLE_ID + "/" + ROW_NUM;
                        if (Directory.Exists(filePath) == false)
                        {
                            filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSKHDA_FINAL + "/" + SKP_ID + "/"+ SKT_ID; 
                        }
                    }
                    else
                    {
                        filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSKHDA_FINAL + "/" + SKP_ID + "/" + SKT_ID;
                    }

                    if (Directory.Exists(filePath))
                    {
                        if (Directory.EnumerateFiles(filePath).Any())
                        {
                            string[] files = Directory.GetFiles(filePath);

                            foreach (string file in files)
                            {
                                ziparchive.CreateEntryFromFile(file, Path.GetFileName(file));
                            }

                           // Array.ForEach(Directory.GetFiles(folderPath), System.IO.File.Delete);
                        }
                        //System.IO.Directory.Delete(folderPath);
                    }


                    //for (int i = 0; i < FileDetails.Count(); i++)
                    //{
                    //    ziparchive.CreateEntryFromFile(PhoenixConfiguration.Instance.ReadFilePath + FileDetails[i].UploadedFilePath, FileDetails[i].FileName);
                    //}
                }
                return File(memoryStream.ToArray(), "application/zip", "Attachments.zip");
            }
        }
        #endregion
    }
}