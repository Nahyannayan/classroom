﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;

namespace Phoenix.Web.Areas.SIMS.Controllers
{
    [Authorize]
    public class TimetableController : Controller
    {
        
        private ICalendarService _TimeTableService;


        public TimetableController(ICalendarService timeTableService)
        {
            _TimeTableService = timeTableService;
        }


        // GET: SIMS/Timetable
        public ActionResult Index()
        {
            Session["ACD_ID"] = 5;
            ViewBag.Grades = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.Grade,Session["ACD_ID"]), "ItemId", "ItemName");
            return View("~/Views/SMS/Index.cshtml");
        }
    }
}