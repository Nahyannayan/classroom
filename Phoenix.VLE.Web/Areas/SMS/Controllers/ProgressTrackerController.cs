﻿using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Services;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Xml.Serialization;
using System.Xml.Linq;

namespace Phoenix.VLE.Web.Areas.SMS.Controllers
{
    public class ProgressTrackerController : BaseController
    {
        private ISIMSCommonService _SIMSCommonService;
        private IProgressTrackerService _progressTrackerService;
        private IAssessmentService _assessmentService;

        public ProgressTrackerController(ISIMSCommonService SIMSCommonService, IProgressTrackerService progressTrackerService, IAssessmentService assessmentService)
        {
            _progressTrackerService = progressTrackerService;
            _SIMSCommonService = SIMSCommonService;
            _assessmentService = assessmentService;

        }
        // GET: SMS/ProgressTracker
        public ActionResult Index()
        {

            var schoolid = SessionHelper.CurrentSession.SchoolId;//Userdetail.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var grd_access = (int)curr_role.GSA_ID;
            var clm_id = Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID;  //new to get this value from some generic class .
                                                                              // var current_acd = 1;
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            //var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, curr_role.ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            //ViewBag.Grades = list;
            // ViewBag.AgeBand = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AgeBand, null), "ItemId", "ItemName");
            ViewBag.AcademicYear = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AcademicYear, string.Format("{0},{1},{2}", schoolid, clm_id, null)), "ItemId", "ItemName");

            return View("~/Areas/SMS/Views/ProgressTracker/Index.cshtml");
        }



        public ActionResult GetGradesAccess(Int32 acd_id, Int32 rsm_id)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, acd_id, Convert.ToInt32(schoolid), grd_access, rsm_id);
            var gradesList = new SelectList(list, "GRD_ID", "GRD_DESCR");
            return Json(gradesList, JsonRequestBehavior.AllowGet);
        }

        public class JsonTopicList
        {
            public long id { get; set; }

            public string title { get; set; }

            public List<SubTopicTree> subs { get; set; }

        }


        public ActionResult BindTopics(long TRM_ID = 0, long SBG_ID = 0)
        {

            List<JsonTopicList> objJsonResult = new List<JsonTopicList>();
            var listtopicTrees = _progressTrackerService.BindTopicTree(TRM_ID, SBG_ID).ToList();
            var listsubtopic = listtopicTrees.Where(e => e.SYD_PARENT_ID > 0).ToList();
            var listParentTopic = listtopicTrees.Where(e => e.SYD_PARENT_ID == 0).ToList();
            var jsonSerialiser = new JavaScriptSerializer();
            //var json = jsonSerialiser.Serialize(ListtopicTrees);
            foreach (var topicList in listtopicTrees)
            {
                List<SubTopicTree> strSubTopic = new List<SubTopicTree>();
                var Id = listParentTopic.Where(e => e.SYD_ID == topicList.SYD_ID).Select(e => e.SYD_ID).FirstOrDefault();
                if (Id > 0)
                {
                    var title = listParentTopic.Where(e => e.SYD_ID == topicList.SYD_ID).Select(e => e.SYD_DESCR).FirstOrDefault();
                    var lstsubtopics = listsubtopic.Where(e => e.SYD_PARENT_ID == Id).ToList();
                    if (lstsubtopics.Count > 0)
                    {
                        foreach (var subtopic in lstsubtopics)
                        {
                            strSubTopic.Add(new SubTopicTree { id = subtopic.SYD_ID, title = subtopic.SYD_DESCR });

                        }
                    }
                    objJsonResult.Add(new JsonTopicList
                    {
                        id = Id,
                        title = title,
                        subs = strSubTopic
                    });


                }

            }

            return Json(objJsonResult, JsonRequestBehavior.AllowGet);

        }


        public ActionResult BindSubTerms(long ACD_ID = 0)
        {
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            List<SubTerms> objBindSubterms = new List<SubTerms>();
            objBindSubterms = _progressTrackerService.BindSubTerm(schoolId, ACD_ID).ToList();

            return Json(objBindSubterms, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetSubjectsByGrade(Int32 acd_id, string grd_id)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var list = _SIMSCommonService.GetSubjectsByGrade(acd_id, grd_id);
            var subjectList = new SelectList(list, "SBG_ID", "SBG_DESCR");
            return Json(subjectList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSubjectGroupBySubject(string grd_id, Int32 sbg_id)
        {

            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var list = _SIMSCommonService.GetSubjectGroupBySubject(username, isSuperUser, Convert.ToInt32(schoolid), grd_id, sbg_id);
            var subjectgroupList = new SelectList(list, "SGR_ID", "SGR_DESCR");
            return Json(subjectgroupList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BindAgeBandOnGrade(long ACD_ID = 0, string Grd_id = "")
        {
            List<SelectListItem> listOfband = new List<SelectListItem>();
            var hasBand = _progressTrackerService.HAS_AgeBand(SessionHelper.CurrentSession.SchoolId, ACD_ID, Grd_id);
            if (hasBand)
            {
                listOfband = SelectSIMSListHelper.GetSelectListData(ListItems.AgeBand, null).ToList().ConvertAll(a =>
                {
                    return new SelectListItem()
                    {
                        Text = a.ItemName,
                        Value = a.ItemId.ToString(),
                        Selected = false
                    };
                });
            }


            return Json(listOfband, JsonRequestBehavior.AllowGet);


        }

        public ActionResult BindStepsOntopic(string Topicname, long Acd_Id = 0, string GRD_Id = "", long Sbg_Id = 0)
        {
            var schoolId = SessionHelper.CurrentSession.SchoolId;
            string Syd_Id = string.Empty;
            List<BindSteps> objBindSteps = new List<BindSteps>();
            var HasSteps = _progressTrackerService.HAS_STEPS(schoolId, Acd_Id, GRD_Id);

            if (HasSteps)
            {

                objBindSteps = _progressTrackerService.BindSteps(Acd_Id, GRD_Id, Sbg_Id, Topicname).ToList();
            }

            return Json(objBindSteps, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Progresstrack(string STEPS, int ACD_ID = 0, int SGR_ID = 0, int SCT_ID = 0, long SBG_ID = 0, string TOPIC_ID = "", long AGE_BAND_ID = 0, long TSM_ID = 0, string GRD_ID = "")
        {
            ProgressTracker objProgressTracker = new ProgressTracker();
            List<TopicDetails> objTopics = new List<TopicDetails>();
            List<ObjectiveDetails> objObjectiveDetails = new List<ObjectiveDetails>();
            string lstSteps = string.Empty;

            if (STEPS != null)
            {
                lstSteps = string.Join("|", STEPS);

            }

            objProgressTracker.objStudentList = _assessmentService.GetStudentList(GRD_ID, ACD_ID, SGR_ID, SCT_ID).ToList();

            objProgressTracker.objProgressTrackerHeader = _progressTrackerService.GET_PROGRESS_TRACKER_HEADERS(SBG_ID, TOPIC_ID, AGE_BAND_ID, lstSteps, TSM_ID).ToList();

            objProgressTracker.objProgressTrackerData = _progressTrackerService.GET_PROGRESS_TRACKER_DATA(SBG_ID, TOPIC_ID, TSM_ID, SGR_ID).ToList();
           //separated unique topics
            var topics = objProgressTracker.objProgressTrackerHeader.GroupBy(e => new { e.TOPIC }).Select(f => new Topics { topics=f.Key.TOPIC }).ToList();
            objProgressTracker.objTopics.AddRange(topics);

            //to get distinct ids
            var TopicIds = objProgressTracker.objProgressTrackerHeader.Select(e => e.TOPIC_ID).Distinct().ToList();

            //grouping done to obtain topic details
            var grpresult = objProgressTracker.objProgressTrackerHeader.GroupBy(test => test.TOPIC_ID)
                   .Select(grp => grp.First())
                   .ToList();
            var lqTopicDetails = grpresult.Select(e => new TopicDetails
            {
                TopicId = e.TOPIC_ID,
                Topics = e.TOPIC,
                subTopics = e.SUB_TOPIC,
                subTopicColSpan = e.SUBTOPIC_COLSPAN,
                Steps = e.STEPS,
                STEPS_WIDTH = e.STEPS_WIDTH.ToString()


            }).ToList();

            //separated the object ids for the steps and object details
            var objIds = objProgressTracker.objProgressTrackerHeader.Where(e => TopicIds.Contains(e.TOPIC_ID)).Select(e => e.OBJ_ID).Distinct().ToList();
            var objStepsTopicDetails = objProgressTracker.objProgressTrackerHeader.Where(e => objIds.Contains(e.OBJ_ID))
                                      .Select(e => new StepsTopicDetails
                                      {
                                          Steps = e.STEPS,
                                          StepsWidth = e.STEPS_WIDTH,
                                          subTopics = e.SUB_TOPIC,
                                          TopicId = e.TOPIC_ID

                                      }).Distinct().ToList();
            objProgressTracker.objStepsTopicDetails = objStepsTopicDetails;
            var objObjectiveDetailsList = objProgressTracker.objProgressTrackerHeader.Distinct().Where(e => objIds.Contains(e.OBJ_ID))
                                     .Select(e => new ObjectiveDetails
                                     {
                                         objId = e.OBJ_ID,
                                         objWidth = e.OBJ_WIDTH,
                                         objDate = e.OBJ_ENDDT,
                                         objDescription = e.OBJ_DESC,
                                         TopicId = e.TOPIC_ID,
                                         Steps = e.STEPS,
                                         subTopics = e.SUB_TOPIC

                                     }).Distinct().ToList();

            objProgressTracker.objTopicDetails = lqTopicDetails;
            objProgressTracker.objObjectiveDetails = objObjectiveDetailsList;
            objProgressTracker.objProgressDropdown = _progressTrackerService.BindProgressTrackerDropdown(SessionHelper.CurrentSession.SchoolId, GRD_ID).ToList();

            return PartialView("~/Areas/SMS/Views/ProgressTracker/_ProgressTrackerPartial.cshtml", objProgressTracker);
        }


        public ActionResult ProgressTrackerReport()
        {



            return View("~/Areas/SMS/Views/ProgressTracker/ProgressTrackerReport.cshtml");
        }

        public class StudentObjectData
        {

            public long StudentId { get; set; }
            public int ObjectiveId { get; set; }

            public string data { get; set; }

            public string SYD_ID { get; set; }

            public string UploadPath { get; set; }

            public long TermId { get; set; }

            public int ACD_ID { get; set; }
            public string GRD_ID { get; set; }
            public int SBG_ID { get; set; }


        }
        public class StudentEvidence
        {

            public long StudentId { get; set; }
            public int ObjectiveId { get; set; }

            public string FileName { get; set; }

            public string FileSize { get; set; }

            public string FileType { get; set; }


        }

        [HttpPost]
        public ActionResult SaveStudentProgressData(string[] objListofStudentObjectData, string[] objStudentEvidenceList)
        {

            List<StudentObjectData> objtrackerList = new List<StudentObjectData>();
            List<StudentEvidence> objEvidence = new List<StudentEvidence>();
            List<StudentEvidence> jointoobjEvidence = new List<StudentEvidence>();
            objtrackerList = JsonConvert.DeserializeObject<List<StudentObjectData>>(objListofStudentObjectData[1]);
            objEvidence = JsonConvert.DeserializeObject<List<StudentEvidence>>(objStudentEvidenceList[1]);
            string relativePath = string.Empty;
            string docpath = string.Empty;
            string FileName = string.Empty;
            if (Request.Files.Count > 0)
            {
               
                HttpFileCollectionBase files = Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];
                    string fname = file.FileName;
                    var contentLength = file.ContentLength;
                    var extension = System.IO.Path.GetExtension(file.FileName);

                    if (contentLength != 0)
                    {
                        //content for file upload
                        string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                        string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSProgressTracker;

                        foreach (var item in objEvidence)
                        {

                            //string fileModule = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFilesDir;
                            string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.SIMSProgressTracker + "/" + SessionHelper.CurrentSession.SchoolId + "/" + item.StudentId;

                            Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                            Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);

                            Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                            fname = Guid.NewGuid() + "_" + fname;
                            relativePath = Constants.SIMSProgressTracker + "/" + SessionHelper.CurrentSession.SchoolId + "/" + item.StudentId + "/" + fname;
                            fname = Path.Combine(filePath, fname);
                            file.SaveAs(fname);
                            docpath = relativePath;
                            FileName = file.FileName;

                            var tracker = objtrackerList.Where(e => e.StudentId == item.StudentId && e.ObjectiveId == item.ObjectiveId).Select(x => { x.UploadPath = docpath; return x; }).FirstOrDefault();
                            var index = objtrackerList.IndexOf(tracker);
                            if (index != -1)
                            {
                                objtrackerList[index] = tracker;
                            }

                        }
                    }

                }
            }


            var StudentProgressXml = new XElement("IDS",
            from p in objtrackerList
            select new XElement("ID",
                new XElement("STU_ID", p.StudentId),
                new XElement("SYC_ID", p.ObjectiveId),
                new XElement("STC_STATUS", p.data),
                new XElement("SYD_ID", string.IsNullOrEmpty(p.SYD_ID) ? "" : p.SYD_ID.ToString()),
                new XElement("STC_EVIDENCE_PATH", string.IsNullOrEmpty(p.UploadPath) ? "" : p.UploadPath.ToString()),
                new XElement("TSM_ID", p.TermId)
                ));
            DateTime STC_UPDATEDATE = DateTime.Now; 

            string STC_USER = SessionHelper.CurrentSession.UserName;
            int syd_Id = objtrackerList[0].SYD_ID == "" ? 0 : Convert.ToInt32(objtrackerList[0].SYD_ID);
            var result = _progressTrackerService.SaveProgressTrackerData(objtrackerList[0].ACD_ID, objtrackerList[0].GRD_ID, objtrackerList[0].SBG_ID, StudentProgressXml, STC_UPDATEDATE, STC_USER, syd_Id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }


    }
}