﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Areas.SMS.Controllers
{
    [Authorize]
    public class ClassListController : BaseController
    {

        private IClassListService _ClassListService;
        private ISIMSCommonService _SIMSCommonService;
        private IAttendanceService _AttendanceService;
        private IBehaviourService _behaviourService;
        public ClassListController(IClassListService ClassListService, ISIMSCommonService SIMSCommonService, IAttendanceService AttendanceService,IBehaviourService behaviourService)
        {
            _ClassListService = ClassListService;
            _SIMSCommonService = SIMSCommonService;
            _AttendanceService = AttendanceService;
            _behaviourService = behaviourService;
        }


        // GET: SIMS/ClassList
        public ActionResult Index()
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            return View("~/Areas/SMS/Views/ClassList/ClassList.cshtml");
        }
        // GET: SIMS/ClassList/GetClassList?username={sting}&tt_id={int}
        public ActionResult GetClassList(string strtt_id="",string grade=null,string section=null)
        {
            int tt_id = 0;
            if (!string.IsNullOrWhiteSpace(strtt_id))
            {
                tt_id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
            }

           
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var gradelist = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = gradelist;
            var list=_ClassListService.GetClassList(username, tt_id, grade, section);
            return View("~/Areas/SMS/Views/ClassList/ClassList.cshtml", list);
        }
        public PartialViewResult GetStudentCard(int tt_id = 0, string grade = null, string section = null)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var username = Userdetail.UserName;
            var list = _ClassListService.GetClassList(username, tt_id, grade, section);
            return PartialView("~/Areas/SMS/Views/Classlist/_StudentCardPartial.cshtml", list);
        }
        // GET: SIMS/ClassList/GetStudentDetails?stu_id={sting}
        public ActionResult GetStudentDetails(string stu_id)
        {
            var Student_Profile = new StudentProfile();
            Student_Profile.BasicDetails= _ClassListService.GetStudentDetails(stu_id);
            //Student_Profile.ParentDetails = _ClassListService.GetProfileParentDetails(stu_id);

            Student_Profile.SiblingDetails  = _ClassListService.GetSiblingDetails(stu_id);

            Student_Profile.TransportDetails = _ClassListService.GetTransportDetails(stu_id);

            Student_Profile.MedicalDetails = _ClassListService.GetMedicalDetails(stu_id);

            Student_Profile.StudentDashboardDetails = _ClassListService.GetStudentDashboardDetails(stu_id);
            Student_Profile.ActivitiesDetails  = _ClassListService.GetActivitiesDetails(stu_id);
             
            //Student_Profile.AttendanceChartMain.AttendanceChart  = _ClassListService.GetAttendanceChart(stu_id);
            //Student_Profile.AttendanceChartMain.ACD_YEAR_DESC = Student_Profile.BasicDetails.Student_ACD_YEAR;

            Student_Profile.AssessmentDetails = _ClassListService.GetAssessmentDetails(stu_id);

            Student_Profile.StudentBehaviourProfile = _behaviourService.LoadBehaviourByStudentId(stu_id);
            //if (Session["HistoryDate"] == null)
            //{
            //    Session["HistoryDate"] = DateTime.Now;
            //}
            //Student_Profile.AttendenceList = _ClassListService.GetAttendenceList(stu_id, Convert.ToDateTime(Session["HistoryDate"])).ToList(); 
            return View("~/Areas/SMS/Views/ClassList/StudentProfile.cshtml", Student_Profile);
        }

        [HttpGet]
        public ActionResult StudentHistory(int id)
        {

            var model = new MarkedAttendaceDetails();
            ViewBag.HistoryType = "W";
            string date = String.Format("{0:dd-MMM-yyyy}", DateTime.Now.Date);

            //if (Session["HistoryDate"] == null) {
            //    Session["HistoryDate"] = String.Format("{0:dd-MMM-yyyy}", DateTime.Now.Date);
            //}
            //if (Session["HistoryType"] == null)
            //{
            //    Session["HistoryType"] = "W";
            //}
            model.StudentDetail = _ClassListService.GetStudentDetails(id.ToString());
            ViewBag.Stu_Acd_Year = model.StudentDetail.Student_ACD_YEAR;
            //model.ATTENDENCE_ANALYSIS_LST = _AttendanceService.Get_ATTENDENCE_ANALYSIS(id.ToString()).ToList();

            model.AttendenceBySession = _AttendanceService.Get_AttendenceBySession(id.ToString(), Convert.ToDateTime(date)).ToList();
            model.AttendenceSessionCode = _AttendanceService.Get_AttendenceSessionCode(id.ToString(), Convert.ToDateTime(date)).ToList();
            model.AttendenceList = _ClassListService.GetAttendenceList(id.ToString(), Convert.ToDateTime(date)).ToList();

            ViewBag.HistoryDate = date;

            var AttendenceChartList = _AttendanceService.Get_AttendanceChartMain(id.ToString());
            var acd_descs = AttendenceChartList.Select(s => s.ACD_DESC).Distinct();

            int total = acd_descs.Count();
            foreach (string ACD in acd_descs)
            {
                if (total == 3)
                {
                    model.AttendenceHistory3 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
                }
                else if (total == 2)
                {
                    model.AttendenceHistory2 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
                }
                else if (total == 1)
                {
                    model.AttendenceHistory1 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
                }
                total--;
            }
            var _classListDashboard = _ClassListService.GetStudentDashboardDetails(id.ToString());


            ViewBag.OverAllPercentage = _classListDashboard.Attencence;

            return View("~/Areas/SMS/Views/Attendance/AttendanceDetail.cshtml", model);
        }

    }
}
