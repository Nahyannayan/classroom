﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;

namespace Phoenix.VLE.Web.Areas.SMS.Controllers
{
    [Authorize]
    public class AssessmentController : BaseController
    {

        private IAssessmentService _AssessmentService;
        private ISIMSCommonService _SIMSCommonService;


        public AssessmentController(IAssessmentService AssessmentService, ISIMSCommonService SIMSCommonService)
        {
            _AssessmentService = AssessmentService;
            _SIMSCommonService = SIMSCommonService;
        }


        // GET: SMS/Assessment
        public ActionResult Index()
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = SessionHelper.CurrentSession.SchoolId;//Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var clm_id = Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID;  //new to get this value from some generic class .
            var current_acd = 1;
            ViewBag.AcademicYear = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AcademicYear, string.Format("{0},{1},{2}", schoolid, clm_id, DBNull.Value)), "ItemId", "ItemName");
            var Categories = _AssessmentService.GetAssessmentCategories(schoolid, "").ToList();


            if (Categories.Count > 0)
            {
                List<SelectListItem> listOfCategories = Categories.ConvertAll(a =>
                {
                    return new SelectListItem()
                    {
                        Text = a.CAT_DESC,
                        Value = a.CAT_ID.ToString(),
                        Selected = false
                    };
                });
            }
                ViewBag.AssessmentCategories = Categories;
                return View("~/Areas/SMS/Views/Assessment/AssessmentGradeEntry.cshtml");
        }
        [HttpGet]
        public JsonResult FetchReportHeaders(int id)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var ReportHeader = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.ReportHeader, string.Format("{0},{1}", schoolid, id)), "ItemId", "ItemName");
            return Json(ReportHeader, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetGradesAccess(Int32 acd_id, Int32 rsm_id)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username); 
            var isSuperUser = (curr_role.IsSuperUser?"Y":"N"); 
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, acd_id, Convert.ToInt32(schoolid), grd_access, rsm_id);
            var gradesList = new SelectList(list, "GRD_ID", "GRD_DESCR");
            return Json(gradesList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetSubjectsByGrade(Int32 acd_id, string grd_id)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var list = _SIMSCommonService.GetSubjectsByGrade(acd_id, grd_id);
            var subjectList = new SelectList(list, "SBG_ID", "SBG_DESCR");
            return Json(subjectList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetSubjectGroupBySubject(string grd_id, Int32 sbg_id)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var list = _SIMSCommonService.GetSubjectGroupBySubject(username, isSuperUser, Convert.ToInt32(schoolid), grd_id, sbg_id);
            var subjectgroupList = new SelectList(list, "SGR_ID", "SGR_DESCR");
            return Json(subjectgroupList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchSubjectCategory(int sbg_id)
        {
            var SubjectCategory = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.SubjectCategory, string.Format("{0}", sbg_id)), "ItemId", "ItemName");
            return Json(SubjectCategory, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchReportSchedule(int rsm_id)
        {
            var SubjectCategory = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.ReportSchedule, string.Format("{0}", rsm_id)), "ItemId", "ItemName");
            return Json(SubjectCategory, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchTerms(int acd_id)
        {
            var terms = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.Terms, string.Format("{0}", acd_id)), "ItemId", "ItemName");
            return Json(terms, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchAssessmentActivity()
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var activity = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AssessmentActivity, string.Format("{0}", schoolid)), "ItemId", "ItemName");
            return Json(activity, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetStudentList(string GRD_ID, Int32 ACD_ID, Int32 SGR_ID, Int32 SCT_ID)
        {
            var list = _AssessmentService.GetStudentList(GRD_ID, ACD_ID, SGR_ID, SCT_ID);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetReportHeaders(string GRD_ID, Int32 ACD_ID, Int32 SBG_ID, Int32 RPF_ID, Int32 RSM_ID, string prv="")
        {
            List<ReportHeader> objhdr = new List<ReportHeader>();
            if (!string.IsNullOrWhiteSpace(prv))
            {
                var ids = prv.Replace(",", "|");
                objhdr = _AssessmentService.GetReportHeaders(GRD_ID, ACD_ID, SBG_ID, RPF_ID, RSM_ID, ids).ToList();


            }

            var list = _AssessmentService.GetReportHeaders(GRD_ID, ACD_ID, SBG_ID, RPF_ID, RSM_ID, "");
            objhdr.AddRange(list);

            return Json(objhdr, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetReportHeadersDropdowns(Int32 RSM_ID, Int32 SBG_ID, Int32 RSD_ID)
        {
            var list = _AssessmentService.GetReportHeadersDropdowns(RSM_ID, SBG_ID, RSD_ID);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetAssessmentData(Int32 ACD_ID, Int32 SBG_ID, Int32 RPF_ID, Int32 RSM_ID, string prv = "")
        {
            List<AssessmentData> objhdr = new List<AssessmentData>();
            if (!string.IsNullOrWhiteSpace(prv))
            {
                var ids = prv.Replace(",", "|");
                objhdr = _AssessmentService.GetAssessmentData(ACD_ID, SBG_ID, RPF_ID, RSM_ID, ids).ToList();


            }

            var list = _AssessmentService.GetAssessmentData(ACD_ID, SBG_ID, RPF_ID, RSM_ID, "");
            objhdr.AddRange(list);

           
            return Json(objhdr, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertAssessmentData(string student_xml, int bEdit)
        {
            var currentUser = Helpers.CommonHelper.GetLoginUser();
            var username = currentUser.UserName;
            var result = _AssessmentService.InsertAssessmentData(student_xml, username, bEdit);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCommentByCategoryId(int CAT_ID = 0,long STU_ID = 0)
        {
            List<AssessmentComments> objAssessmentComment = new List<AssessmentComments>();
            objAssessmentComment = _AssessmentService.GetAssessmentComments(CAT_ID, STU_ID).ToList();
            if(objAssessmentComment.Count > 0)
            {

                return PartialView("_AssessmentCommentGrid", objAssessmentComment);

            }
            return null;
        }

        public ActionResult getAssessmentCategory(string CAT_GRD_ID="")
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var Categories = _AssessmentService.GetAssessmentCategories(schoolid, "").ToList();


            if(Categories.Count > 0)
            {
                List<SelectListItem> listOfCategories = Categories.ConvertAll(a =>
                {
                    return new SelectListItem()
                    {
                        Text = a.CAT_DESC,
                        Value = a.CAT_ID.ToString(),
                        Selected = false
                    };
                });

                ViewBag.AssessmentCategories = Categories;
                return Json(listOfCategories, JsonRequestBehavior.AllowGet);

            }


            return null;


        }
        [HttpGet]
        public JsonResult GetSectionAccess(long ACD_ID,string GRD_ID)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _AssessmentService.GetSectionAccess(username, isSuperUser, ACD_ID, Convert.ToInt32(schoolid), grd_access, GRD_ID);
            var gradesList = new SelectList(list, "SCT_ID", "SCT_DESCR");
            return Json(gradesList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetReportHeaderOptional(string AOD_IDs)
        {
            //var list = _AssessmentService.GetReportHeaderOptional(AOD_IDs);
            var ids = AOD_IDs.Replace(",", "|");
            var list = _AssessmentService.GetReportHeaderOptional(ids);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetAssessmentDataOptional(long ACD_ID, long RPF_ID, long RSM_ID, long SBG_ID, long SGR_ID, string GRD_ID, long SCT_ID, string AOD_IDs)
        {
            var AODIDS = AOD_IDs.Replace(",", "|");
            var list = _AssessmentService.GetAssessmentDataOptional(ACD_ID, RPF_ID, RSM_ID, SBG_ID, SGR_ID, GRD_ID, SCT_ID, AODIDS);
           // var list = _AssessmentService.GetAssessmentDataOptional(0, 0, 0, 0, 0, "0", 0, "1|2|3|4|5|6");
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetAssessmentOptionalLists(long ACD_ID=0, string GRD_ID="")
        {
            var BSU_ID = SessionHelper.CurrentSession.SchoolId;
            var jsonPreviousResult = _AssessmentService.GetAssessmentPreviousSchedule(ACD_ID, GRD_ID).ToList();
            var jsonOptionResult = _AssessmentService.GetAssessmentOptionList(BSU_ID, ACD_ID).ToList();
            var result = new { PreviousResult = jsonPreviousResult, OptionResult= jsonOptionResult };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
       



        // GET: SMS/Assessment/MarkEntry
        #region MARKENTRY

        public ActionResult MarkEntry()
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = SessionHelper.CurrentSession.SchoolId;//Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var clm_id = Helpers.CommonHelper.GetCurrentCurriculum().CLM_ID;  //new to get this value from some generic class .
            ViewBag.AcademicYear = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AcademicYear, string.Format("{0},{1},{2}", schoolid, clm_id, DBNull.Value)), "ItemId", "ItemName");
            //var list = _AssessmentService.GetAssessmentActivityList(1273, 0, "01", 1, 1, 1, 1, 1, "charan", "N");
            return View("~/Areas/SMS/Views/Assessment/MarkEntry.cshtml");
        }
        
        public ActionResult ViewListMArkEntry(long ACD_ID = 0, long CAM_ID = 0, string GRD_ID = "", long STM_ID = 0, long TRM_ID = 0, long SGR_ID = 0, long SBG_ID = 0, int GRADE_ACCESS = 0)
        {

            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");

            List<MarkEntry> objListMarkEntry = new List<MarkEntry>();
            objListMarkEntry = _AssessmentService.GetAssessmentActivityList(ACD_ID, CAM_ID, GRD_ID, STM_ID, TRM_ID, SGR_ID, SBG_ID, Convert.ToInt32(curr_role.GSA_ID), username, isSuperUser).ToList();
            return PartialView("_AssessmentViewMarkList", objListMarkEntry);
        }
        
        public ActionResult GetMarkEntryAOL(bool IsAOLEXAM, long CAS_ID = 0, bool Isbwithoutskill = false, long SlabId = 0, string Type = "",double minMarks=0.0,double maxMarks=0.0)
        {
            List<MarkEntryAOLData> objListOfmarkEntryAOLData = new List<MarkEntryAOLData>();
            List<MarkEntryAOLData> objlistofmarkentryAOI = new List<MarkEntryAOLData>();
            List<SkillSET> objskillset = new List<SkillSET>();
            ViewBag.CAS_ID = CAS_ID;
            if (IsAOLEXAM)
            {
                //LIST OF MARKENTRYAOI AS PER CAD_ID 
                objlistofmarkentryAOI = _AssessmentService.GetMarkEntryAOLData(CAS_ID).ToList();



                //-----------ADDED THE LIST OF STUDENT FOR AOI----------------//
                var markAOLGroup = objlistofmarkentryAOI.GroupBy(e => new
                {
                    e.STU_NAME,
                    e.STU_NO,
                    e.STA_ID,
                    e.STU_ID,
                    e.MARKS,
                    e.STA_GRADE,
                    e.IS_ENABLED,
                    e.WITHOUTSKILLS_GRADE,
                    e.WITHOUTSKILLS_MAXMARKS,
                    e.CHAPTER,
                    e.FEEDBACK,
                    e.WOT,
                    e.TARGET,
                    e.bATTENDED,
                    e.STU_STATUS,
                    e.WITHOUTSKILLS_MARKS,

                })
                 .Select(f => new
                 {
                     f.Key.STU_NAME,
                     f.Key.STU_NO,
                     f.Key.STA_ID,
                     f.Key.STU_ID,
                     f.Key.MARKS,
                     f.Key.STA_GRADE,
                     f.Key.IS_ENABLED,
                     f.Key.WITHOUTSKILLS_GRADE,
                     f.Key.WITHOUTSKILLS_MAXMARKS,
                     f.Key.CHAPTER,
                     f.Key.FEEDBACK,
                     f.Key.WOT,
                     f.Key.TARGET,
                     f.Key.bATTENDED,
                     f.Key.STU_STATUS,
                     f.Key.WITHOUTSKILLS_MARKS,


                 });

                foreach (var i in markAOLGroup)
                {

                    objListOfmarkEntryAOLData.Add(new MarkEntryAOLData
                    {
                        bATTENDED = i.bATTENDED,
                        CHAPTER = i.CHAPTER,
                        FEEDBACK = i.FEEDBACK,
                        IS_ENABLED = i.IS_ENABLED,
                        MARKS = i.MARKS,
                        STA_GRADE = i.STA_GRADE,
                        STA_ID = i.STA_ID,
                        STU_ID = i.STU_ID,
                        STU_NAME = i.STU_NAME,
                        STU_NO = i.STU_NO,
                        STU_STATUS = i.STU_STATUS,
                        TARGET = i.TARGET,
                        WITHOUTSKILLS_GRADE = i.WITHOUTSKILLS_GRADE,
                        WITHOUTSKILLS_MARKS = i.WITHOUTSKILLS_MARKS,
                        WITHOUTSKILLS_MAXMARKS = i.WITHOUTSKILLS_MAXMARKS,
                        WOT = i.WOT,



                    });

                }

                //-------EXTRACTED SKILL_NAME SKILL_MAXMARKS-----------------//
                var SkillsMarkAOLGroup = objlistofmarkentryAOI.GroupBy(e => new { e.SKILL_NAME }).Select(f => new { f.Key.SKILL_NAME, SKILL_MAX_MARK = f.Sum(x => x.SKILL_MAX_MARK) });
                foreach (var set in SkillsMarkAOLGroup)
                {
                    var SkillMarks = Convert.ToDouble(set.SKILL_MAX_MARK / objListOfmarkEntryAOLData.Count());
                    objskillset.Add(new SkillSET
                    {
                        SKILL_MAX_MARK = SkillMarks,// set.SKILL_MAX_MARK/ objListOfmarkEntryAOLData.Count(),
                        SKILL_NAME = set.SKILL_NAME,

                    });
                }

                ViewBag.SKillSET = objskillset;
                ViewBag.lstmarkofAOI = objlistofmarkentryAOI;
                ViewBag.Isbwithoutskill = Isbwithoutskill;
                // ViewBag.CAS_ID = CAS_ID;
                return PartialView("_AssessmentMarkEntryAOL", objListOfmarkEntryAOLData);
            }
            else
            {
                List<MarkEntryData> objMarkEntryData = new List<MarkEntryData>();
                objMarkEntryData = _AssessmentService.GetMarkEntryData(CAS_ID, minMarks,maxMarks).ToList();
                ViewBag.SlabId = SlabId;
                ViewBag.EntryType = Type;
                return PartialView("_AssessmentMarkEntryData", objMarkEntryData);

            }


        }
        
        public ActionResult GetMarkEntryData(bool IsAOLEXAM, long CAS_ID = 0, bool Isbwithoutskill = false, long SlabId = 0, string Type = "")
        {
            
                ViewBag.CAS_ID = CAS_ID;
                List<MarkEntryData> objMarkEntryData = new List<MarkEntryData>();
                objMarkEntryData = _AssessmentService.GetMarkEntryData(1276616, 25, 8.25).ToList();
                ViewBag.SlabId = SlabId;
                ViewBag.EntryType = Type;
                return PartialView("_AssessmentMarkEntryData", objMarkEntryData);

            


        }
        public ActionResult SaveMarkEntryData(List<MarkEntryData> objListofMarkEntryData, long slabId = 0, string entryType = "", long CAS_ID = 0)
        {
            if (objListofMarkEntryData.Count > 0)
            {

                var IsActionPerformed = _AssessmentService.InsertMarkEntryData(objListofMarkEntryData, slabId, entryType, CAS_ID);
                if (IsActionPerformed)
                {
                    return Json(new OperationDetails(IsActionPerformed), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new OperationDetails(IsActionPerformed), JsonRequestBehavior.AllowGet);
                }
            }

            return null;

        }






        #endregion
    }
}