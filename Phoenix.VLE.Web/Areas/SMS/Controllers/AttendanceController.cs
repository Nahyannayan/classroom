﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Newtonsoft.Json;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Areas.SMS.Controllers
{
    [Authorize]
    public class AttendanceController : BaseController
    {

        private IAttendanceService _AttendanceService;
        private IClassListService _ClassListService;
        private ISIMSCommonService _SIMSCommonService;


        public AttendanceController(IAttendanceService AttendanceService, IClassListService ClassListService, ISIMSCommonService SIMSCommonService)
        {
            _AttendanceService = AttendanceService;
            _ClassListService = ClassListService;
            _SIMSCommonService = SIMSCommonService;
        }


        // GET: SIMS/Attendance
        public ActionResult Index(string strtt_id = "", string entrydate = null, string grade = null, string section = null)
        {
            int tt_id = 0;
            if (!string.IsNullOrWhiteSpace(strtt_id))
            {
                tt_id = Convert.ToInt32(EncryptDecryptHelper.Decrypt(strtt_id));
            }

            if (!string.IsNullOrWhiteSpace(entrydate))
            {
                entrydate = EncryptDecryptHelper.Decrypt(entrydate);
            }


            ViewBag.TTM_Id = tt_id;
            ViewBag.EntryDate = entrydate;
            ViewBag.GRD_Id = grade;
            ViewBag.SCT_Id = section;
            if (tt_id != 0)
            {
                ViewBag.IsFromTT = "1";
            }
            else
            {
                ViewBag.IsFromTT = "0";
            }
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
            ViewBag.AttendanceType = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.AttendanceType, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID), "ItemId", "ItemName");
            return View("~/Areas/SMS/Views/Attendance/StudentList.cshtml");
        }

        [HttpGet]
        public JsonResult GetStudentList(int tt_id = 0, string entrydate = null, string grade = null, string section = null)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var list = _AttendanceService.GetAttendanceByIdAndDate(Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, tt_id, username, entrydate, grade, section);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertAttendanceDetails(string student_xml, string entry_date, int alg_id, int ttm_id = 0, int sct_id = 0, string GRD_ID = "", long SHF_ID = 0, long STM_ID = 0)
        {
            var Userdetail = Helpers.CommonHelper.GetLoginUser();
            var schoolid = Userdetail.SchoolId;
            var username = Userdetail.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var result = _AttendanceService.InsertAttendanceDetails(student_xml, entry_date, username, alg_id, ttm_id, Convert.ToString(sct_id), GRD_ID, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, schoolid, SHF_ID, STM_ID);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult StudentHistory(int id)
        {
            var model = new MarkedAttendaceDetails();
            ViewBag.HistoryType = "W";
            string date = String.Format("{0:dd-MMM-yyyy}", DateTime.Now.Date);

            //if (Session["HistoryDate"] == null) {
            //    Session["HistoryDate"] = String.Format("{0:dd-MMM-yyyy}", DateTime.Now.Date);
            //}
            //if (Session["HistoryType"] == null)
            //{
            //    Session["HistoryType"] = "W";
            //}
            model.StudentDetail = _ClassListService.GetStudentDetails(id.ToString());
            ViewBag.Stu_Acd_Year = model.StudentDetail.Student_ACD_YEAR;
            model.ATTENDENCE_ANALYSIS_LST = _AttendanceService.Get_ATTENDENCE_ANALYSIS(id.ToString()).ToList();
            model.StudentDetail.AnalysisList = _AttendanceService.Get_ATTENDENCE_ANALYSIS(id.ToString()).ToList();
            model.AttendenceBySession = _AttendanceService.Get_AttendenceBySession(id.ToString(), Convert.ToDateTime(date)).ToList();
            model.AttendenceSessionCode = _AttendanceService.Get_AttendenceSessionCode(id.ToString(), Convert.ToDateTime(date)).ToList();
            model.AttendenceList = _ClassListService.GetAttendenceList(id.ToString(), Convert.ToDateTime(date)).ToList();
            model.StudentDetail.AttendenceBySession = _AttendanceService.Get_AttendenceBySession(id.ToString(), Convert.ToDateTime(date)).ToList();
            model.StudentDetail.AttendenceList = _ClassListService.GetAttendenceList(id.ToString(), Convert.ToDateTime(date)).ToList();

            ViewBag.HistoryDate = date;

            var AttendenceChartList = _AttendanceService.Get_AttendanceChartMain(id.ToString());
            var acd_descs = AttendenceChartList.Select(s => s.ACD_DESC).Distinct();

            int total = acd_descs.Count();
            foreach (string ACD in acd_descs)
            {
                if (total == 3)
                {
                    model.AttendenceHistory3 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
                }
                else if (total == 2)
                {
                    model.AttendenceHistory2 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
                }
                else if (total == 1)
                {
                    model.AttendenceHistory1 = AttendenceChartList.Where(s => s.ACD_DESC.Equals(ACD)).ToList();
                }
                total--;
            }
            var _classListDashboard = _ClassListService.GetStudentDashboardDetails(id.ToString());


            ViewBag.OverAllPercentage = _classListDashboard.Attencence;

            return View("~/Areas/SMS/Views/Attendance/AttendanceDetail.cshtml", model);
        }

        //[HttpGet]
        //public string SetStudentHistoryParam(string date, string type)
        //{
        //    Session["HistoryDate"] = date;
        //    Session["HistoryType"] = type;
        //    return "Success";
        //}


        [HttpGet]
        public ActionResult GetAttendenceBySession(string selectedDate, string STU_ID)
        {
            var model = _AttendanceService.Get_AttendenceBySession(STU_ID, Convert.ToDateTime(selectedDate)).ToList();
            return View("~/Areas/SMS/Views/Attendance/Partial/AttendenceBySession.cshtml", model);
        }

        [HttpGet]
        public ActionResult GetAttendenceSessionCode(string selectedDate, string STU_ID)
        {
            var model = _AttendanceService.Get_AttendenceSessionCode(STU_ID, Convert.ToDateTime(selectedDate)).ToList();
            return View("~/Areas/SMS/Views/Attendance/Partial/AttendenceSessionCode.cshtml", model);
        }

        [HttpGet]
        public ActionResult GetAttendenceListMonthly(string selectedDate, string STU_ID)
        {
            var model = _ClassListService.GetAttendenceList(STU_ID, Convert.ToDateTime(selectedDate)).ToList();
            return View("~/Areas/SMS/Views/Attendance/Partial/AttendenceList.cshtml", model);
        }

        [HttpGet]
        public ActionResult GetAttendenceListWeekly(string selectedDate, string STU_ID)
        {
            var model = _ClassListService.GetAttendenceList(STU_ID, Convert.ToDateTime(selectedDate)).ToList();
            return View("~/Areas/SMS/Views/Attendance/Partial/AttendenceListWeekly.cshtml", model);
        }


        [HttpGet]
        public ActionResult GetAttendenceChartMonthly(string selectedDate, string STU_ID)
        {
            var MonthtlyChartModel = new AttendanceChartMain();
            MonthtlyChartModel.AttendanceChart = _ClassListService.GetAttendanceChart(STU_ID);

            var studentDetails = _ClassListService.GetStudentDetails(STU_ID);
            MonthtlyChartModel.ACD_YEAR_DESC = studentDetails.Student_ACD_YEAR;// Student_Profile.BasicDetails.Student_ACD_YEAR; 

            return View("~/Areas/SMS/Views/Classlist/_StudentAttendenceChart.cshtml", MonthtlyChartModel);
        }

    }
}