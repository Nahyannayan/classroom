﻿using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.Controllers;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Phoenix.VLE.Web.Extensions;
using System.Net;
using System.Globalization;
using Phoenix.Common.ViewModels;
using System.IO.Compression;
using System.Web.Script.Serialization;
//using DocumentFormat.OpenXml.Packaging;
using System.Text.RegularExpressions;
using DevExpress.XtraRichEdit;
using System.Threading;
using Newtonsoft.Json;
using CommonWebHelper = Phoenix.VLE.Web.Helpers.CommonHelper;
using Google.Apis.Auth.OAuth2.Mvc;
using Phoenix.Common.Logger;
using DevExpress.Web.Mvc;
using System.Configuration;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharp;

namespace Phoenix.VLE.Web.Areas.Assignments.Controllers
{
    [Authorize]
    public class AssignmentController : BaseController
    {
        private readonly IAssignmentService _assignmentService;
        private IStudentListService _studentListService;
        private ISchoolService _schoolService;
        private IGradingTemplateService _gradingTemplateService;
        private IGradingTemplateItemService _gradingTemplateItemService;
        //private IDocumentService _documentService;
        private readonly IFileService _fileService;
        private List<VLEFileType> _fileTypeList;
        private ISchoolGroupService _schoolGroupService;
        private ISubjectService _subjectService;
        private readonly IQuizQuestionsService _quizQuestionsService;
        private readonly IQuizService _quizService;
        private readonly IStudentService _studentService;
        private readonly IMarkingSchemeService _markingSchemeService;
        private readonly ICommonService _commonService;
        private readonly IUserService _userService;
        private readonly IAsyncLessonService _asyncLessonService;


        // GET: Assignments/Assignment
        public AssignmentController(IAssignmentService AssignmentService, IStudentListService StudentListService, ISchoolService schoolService,
            IGradingTemplateService gradingTemplateService, IGradingTemplateItemService gradingTemplateItemService, IFileService fileService,
            ISchoolGroupService schoolGroupService, ISubjectService subjectService, IQuizQuestionsService quizQuestionsService, IQuizService quizService,
            IStudentService studentService, IMarkingSchemeService markingSchemeService, ICommonService commonService, IUserService userService,
            IAsyncLessonService asyncLessonService)
        {
            _assignmentService = AssignmentService;
            _studentListService = StudentListService;
            _schoolService = schoolService;
            _gradingTemplateService = gradingTemplateService;

            _gradingTemplateItemService = gradingTemplateItemService;
            _fileService = fileService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
            _schoolGroupService = schoolGroupService;
            _subjectService = subjectService;
            _quizQuestionsService = quizQuestionsService;
            _quizService = quizService;
            _studentService = studentService;
            _markingSchemeService = markingSchemeService;
            _commonService = commonService;
            _userService = userService;
            _asyncLessonService = asyncLessonService;
        }

        public ActionResult Index(int pageIndex = 1, string assignmentType = "")
        {
            string pageName = String.Empty;
            List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
            if (SessionHelper.CurrentSession.IsParent() || SessionHelper.CurrentSession.IsStudent())
            {
                var filterstudentId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;//Session["CurrentSelectedStudent"] as StudentDetail;
                lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(filterstudentId, true).ToList();
            }
            else
            {
                lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            }
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            var model = new AssignmentEdit();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });
            int SchoolGroupId = 0;
            var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName,
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                Selected = x.SchoolGroupId == SchoolGroupId ? true : false
            }).ToList();
            ViewBag.lstSchoolGroup = groupedOptions;
            if (SessionHelper.CurrentSession.IsParent() || SessionHelper.CurrentSession.IsStudent())
            {
                var studentId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                pageName = "_StudentAssignment";
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                pageName = "_AssignmentDetailsByPaging";
            }
            Session["AssignmentType"] = assignmentType;
            return View(pageName);
        }

        public ActionResult AddEditAssignment(string id, string courseId, string unitId, bool isCloned = false, string selectedGroupId = "", string teacherCreatedUnits = "", string asylsId = "")
        {
            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;
            var oldUsersID = SessionHelper.CurrentSession.OldUserId;
            ViewBag.oldUsersID = oldUsersID;
            var SchoolCode = SessionHelper.CurrentSession.SchoolCode;
            ViewBag.SchoolCode = SchoolCode;
            if (!CurrentPagePermission.CanEdit && !(SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin))
            {
                return RedirectToAction("NoPermission", "Error");
            }
            int? assignmentId = !string.IsNullOrWhiteSpace(id) ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id)) : 0;
            List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
            lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            var model = new AssignmentEdit();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });
            List<GradingTemplate> lstGradingtemplate = _gradingTemplateService.GetGradtingTemplates((int)SessionHelper.CurrentSession.SchoolId).ToList();
            ViewBag.lstGradingtemplate = lstGradingtemplate;
            ViewBag.AssignmentCategoryList = new SelectList(SelectListHelper.GetSelectListData(ListItems.AssignmentCategory, string.Format("{0}", SessionHelper.CurrentSession.SchoolId)), "ItemId", "ItemName");
            if (assignmentId > 0)
            {
                var assignment = _assignmentService.GetAssignmentById(assignmentId.Value);
                ViewBag.archivedDate = assignment.ArchiveDate;
                OperationDetails isAssignmentPermissions = new OperationDetails();
                isAssignmentPermissions = _assignmentService.GetAssignmentPermissions(assignmentId.Value, SessionHelper.CurrentSession.Id);
                if (assignment.CreatedById == SessionHelper.CurrentSession.Id || SessionHelper.CurrentSession.IsAdmin || isAssignmentPermissions.Success == true)
                {
                    EntityMapper<Assignment, AssignmentEdit>.Map(assignment, model);
                    List<AssignmentTask> lstAssignmentTasks = _assignmentService.GetAssignmentTasks(assignmentId.Value).ToList().Where(m => m.IsDeleted == false).OrderBy(m => m.SortOrder).ToList();
                    int k = 1;
                    model.lstAssignmentTasks = new List<AssignmentTask>();
                    foreach (var item in lstAssignmentTasks)
                    {
                        item.SortOrder = k;
                        item.lstTaskFiles = _assignmentService.GetTaskFilesByTaskId(item.TaskId).ToList();
                        //item.RandomId = DateTime.Now.Ticks.ToString();
                        item.RandomId = Guid.NewGuid().ToString();
                        model.lstAssignmentTasks.Add(item);
                        k++;
                    }
                    ViewBag.lstAssignmentTask = JsonConvert.SerializeObject(model.lstAssignmentTasks);
                    model.DueTimeString = assignment.DueDate == null ? "" : assignment.DueDate.Value.ToString("h:mm tt");
                    model.lstDocuments = _assignmentService.GetAssignmentFiles(assignmentId.Value).ToList();
                    if (model.lstDocuments != null && model.lstDocuments.Count > 0)
                    {
                        model.lstDocuments.ForEach(e =>
                        {
                            e.IsCloudFile = e.ResourceFileTypeId == (short)ResourceFileTypes.GoogleDrive || e.ResourceFileTypeId == (short)ResourceFileTypes.OneDrive || e.ResourceFileTypeId == (short)ResourceFileTypes.OneNote;
                            e.UploadedFileName = string.IsNullOrEmpty(e.UploadedFileName) ? e.ShareableLinkFileURL : e.UploadedFileName;
                        });
                    }
                    ViewBag.lstAssignmentFiles = JsonConvert.SerializeObject(model.lstDocuments);
                    model.lstObjective = _subjectService.GetAssignmentObjective(assignmentId.Value).ToList();
                    model.lstCourseUnit = _subjectService.GetAssignmentUnit(assignmentId.Value).ToList();
                    Session["lstObjective"] = model.lstObjective;
                    var gd = _gradingTemplateService.GetGradingTemplateById(model.GradingTemplateId);
                    model.GradingTemplate = gd.GradingTemplateTitle;
                    model.GradingTemplateLogo = gd.GradingTemplateLogo;
                    List<Phoenix.Models.SchoolGroup> lstSchoolGroups = new List<Phoenix.Models.SchoolGroup>();
                    lstSchoolGroups = _assignmentService.GetSelectedSchoolGroupsByAssignmentID(assignmentId.Value).ToList();
                    List<Phoenix.Models.Course> lstCourses = new List<Phoenix.Models.Course>();
                    lstCourses = _assignmentService.GetSelectegCoursesByAssignmentId(assignmentId.Value).ToList();
                    model.SchoolGroupId = lstSchoolGroups.Select(m => m.SchoolGroupId.ToString()).ToList();
                    model.CourseId = lstCourses.Select(m => m.CourseId.ToString()).ToList();
                    var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
                    {
                        Value = x.SchoolGroupId.ToString(),
                        Text = x.SchoolGroupName,
                        Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                        Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
                    }).ToList();
                    ViewBag.lstSchoolGroup = groupedOptions;
                    var StudentList = _studentListService.GetStudentsInSelectedGroups(string.Join(",", model.SchoolGroupId));
                    var StudentSelectList = StudentList.Select(i => new ListItem()
                    {
                        ItemName = i.FirstName + " " + i.LastName + " (" + i.UserName + ")",
                        ItemId = i.StudentId.ToString()
                    }).OrderBy(m => m.ItemName);
                    ViewBag.StudentList = new SelectList(StudentSelectList.OrderBy(m => m.ItemName), "ItemId", "ItemName");
                    List<AssignmentStudent> lstAsgStudents = new List<AssignmentStudent>();
                    int groupId = 0;
                    lstAsgStudents = _assignmentService.GetStudentsByAssignmentId(assignmentId.Value, groupId).ToList();
                    model.StudentId = lstAsgStudents.Select(m => m.StudentId.ToString()).ToList();
                    if (model.lstDocuments != null)
                    {
                        foreach (var item in model.lstDocuments)
                        {
                            item.FileExtension = item.FileName.Split('.').Length > 1 ? item.FileName.Split('.')[1] : string.Empty;
                        }
                    }
                    Session["TaskList"] = model.lstAssignmentTasks;
                    if (model.IsPeerMarkingEnable)
                    {
                        List<AssignmentPeerReview> lstPeerReview = new List<AssignmentPeerReview>();
                        lstPeerReview = _assignmentService.GetPeerMappingDetails(assignmentId.Value).ToList();
                        Session["PeerMappingDetails"] = lstPeerReview;
                    }
                    List<ListItem> lstCourse = new List<ListItem>();
                    lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
                    var courses = lstCourse.Select(x => new SelectListItem
                    {
                        Value = x.ItemId.ToString(),
                        Text = x.ItemName,
                    }).ToList();
                    ViewBag.CourseList = courses;
                }
                else
                {
                    return RedirectToAction("NoPermission", "Error");
                }
            }
            else
            {
                int SchoolGroupId = !string.IsNullOrWhiteSpace(selectedGroupId) ? Convert.ToInt32(CommonWebHelper.GetDecryptedId(selectedGroupId)) : 0;

                List<SelectListItem> lstStudentList = new List<SelectListItem>();
                if (SchoolGroupId > 0)
                {
                    var StudentList = _studentListService.GetStudentsInSelectedGroups(SchoolGroupId.ToString());
                    var StudentSelectList = StudentList.Select(i => new ListItem()
                    {
                        ItemName = i.FirstName + " " + i.LastName + " (" + i.UserName + ")",
                        ItemId = i.StudentId.ToString()
                    }).OrderBy(m => m.ItemName);
                    ViewBag.StudentList = new SelectList(StudentSelectList.OrderBy(m => m.ItemName), "ItemId", "ItemName");


                    //Deepak Singh for asyncLesson on 3/August/2020
                    var asyncLessonId = CommonWebHelper.GetDecryptedId(asylsId);
                    var asynLesson = new AsyncLesson();
                    if (asyncLessonId > 0)
                    {
                        asynLesson = _asyncLessonService.GetAsyncLessonById(asyncLessonId, true);
                        if (!string.IsNullOrEmpty(asynLesson.Students))
                            model.StudentId = StudentList.Where(r => asynLesson.Students.Contains(r.Id.ToString())).Select(r => r.StudentId.ToString()).ToList();
                    }
                }
                else
                {
                    ViewBag.StudentList = lstStudentList;
                }

                List<ListItem> lstCourse = new List<ListItem>();
                lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
                var courses = lstCourse.Select(x => new SelectListItem
                {
                    Value = x.ItemId.ToString(),
                    Text = x.ItemName,
                }).ToList();
                ViewBag.CourseList = courses;
                int selectedcourseId = !string.IsNullOrWhiteSpace(courseId) ? Convert.ToInt32(CommonWebHelper.GetDecryptedId(courseId)) : 0;
                if (selectedcourseId > 0)
                {
                    List<string> lstcoursesId = new List<string>();
                    lstcoursesId.Add(selectedcourseId.ToString());
                    model.CourseId = lstcoursesId;
                }
                int selectedUnitId = !string.IsNullOrWhiteSpace(unitId) ? Convert.ToInt32(CommonWebHelper.GetDecryptedId(unitId)) : 0;
                if (selectedUnitId > 0)
                {
                    var selectedLesson = _subjectService.GetLessonDetailsById(selectedUnitId);
                    model.lstObjective = selectedLesson.Any() ? selectedLesson.ToList() : new List<Objective>();
                    Session["lstCourseLesson"] = model.lstObjective;
                    model.lstCourseUnit = new List<CourseUnit>();
                    model.lstCourseUnit.Add(new CourseUnit
                    {
                        CourseUnitId = selectedUnitId,
                    });
                    Session["lstCourseUnit"] = model.lstCourseUnit;
                }
                int selectedTeacherUnits = !string.IsNullOrWhiteSpace(unitId) ? Convert.ToInt32(CommonWebHelper.GetDecryptedId(unitId)) : 0;
                if (selectedTeacherUnits > 0)
                {
                    model.lstGroupTopicUnit = new List<GroupUnit>();
                    model.lstGroupTopicUnit.Add(new GroupUnit
                    {
                        GroupCourseTopicId = selectedTeacherUnits,
                    });
                    Session["lstGroupUnit"] = model.lstGroupTopicUnit;
                }
                var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
                {
                    Value = x.SchoolGroupId.ToString(),
                    Text = x.SchoolGroupName.Length > 50 ? x.SchoolGroupName.Substring(0, 50) : x.SchoolGroupName,  // Added by Mohd Ameeq on 16-Nov for DDl Issue in IPad
                    Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                    Selected = x.SchoolGroupId == SchoolGroupId ? true : false
                }).ToList();
                ViewBag.lstSchoolGroup = groupedOptions;
                model.lstAssignmentTasks = new List<AssignmentTask>();
                model.IsAddMode = true;
                ViewBag.lstAssignmentFiles = "";
                ViewBag.lstAssignmentTask = "";
            }
            if (isCloned)
            {
                model.IsAddMode = model.IsCloned = true;
                var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
                {
                    Value = x.SchoolGroupId.ToString(),
                    Text = x.SchoolGroupName,
                    Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2]))
                }).ToList();
                ViewBag.lstSchoolGroup = groupedOptions;
                model.AssignmentId = 0;
                model.SchoolGroupIds = null;

                model.StudentIds = null;
                model.StudentId = null;
                model.lstObjective = null;//for remove selected Tagged Curriculums.
                Session["lstObjective"] = null;//for remove selected Tagged Curriculums.
            }

            return View(model);
        }

        public JsonResult GetStudentsinGroup(int[] id)
        {
            IEnumerable<ListItem> StudentSelectList = new List<ListItem>();
            if (id != null && id.Any())
            {
                int systemlangaugeid = LocalizationHelper.CurrentSystemLanguageId;
                var StudentList = _studentListService.GetStudentsInSelectedGroups(string.Join(",", id));
                StudentSelectList = StudentList.Select(i => new ListItem()
                {
                    ItemName = i.FirstName + " " + i.LastName + " (" + i.UserName + ")",
                    ItemId = i.StudentId.ToString()
                });
            }
            return Json(StudentSelectList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveAssignmentBasicDetails(AssignmentEdit model, string mappingDetails, string mappingUnitDetails, string mappingGroupTopicIds, string lstAssignmentFiles, string lstAssignmentTask, string lstTaskOrdering, string asyncLessonId)
        {
            //convert arabic dates
            model.StartDate = model.strStartDate.ToSystemReadableDate();
            model.DueDate = model.strDueDate.ToSystemReadableDate();
            if (model.ArchiveDate != null)
                model.ArchiveDate = model.strArchiveDate.ToSystemReadableDate();
            model.DueTimeString = model.DueTimeString.ToSystemReadableTime();
            OperationDetails op = new OperationDetails();
            List<AssignmentFile> fileNames = new List<AssignmentFile>();
            if (lstAssignmentFiles != "")
            {
                fileNames = JsonConvert.DeserializeObject<List<AssignmentFile>>(lstAssignmentFiles);
            }

            if (!CurrentPagePermission.CanEdit)
            {
                return RedirectToAction("NoPermission", "Error");
            }
            ICommonService _commonService = new CommonService();

            List<AssignmentTask> lstTasks = new List<AssignmentTask>();
            model.lstAssignmentTasks = new List<AssignmentTask>();
            if (lstAssignmentTask != "")
            {
                lstTasks = JsonConvert.DeserializeObject<List<AssignmentTask>>(lstAssignmentTask);
            }
            if (model.IsAssignmentMarksEnable == false)
            {
                model.AssignmentMarks = 0;
                foreach (var item in lstTasks)
                {
                    item.TaskMarks = 0;
                }
            }
            List<Objective> lstObjectives = new List<Objective>();
            if (!string.IsNullOrEmpty(mappingDetails))
            {
                lstObjectives = JsonConvert.DeserializeObject<List<Objective>>(mappingDetails);
            }
            List<CourseUnit> lstCourseUnit = new List<CourseUnit>();
            if (!string.IsNullOrEmpty(mappingUnitDetails))
            {
                lstCourseUnit = JsonConvert.DeserializeObject<List<CourseUnit>>(mappingUnitDetails);
            }
            List<GroupUnit> lstGroupTopicUnit = new List<GroupUnit>();
            if (!string.IsNullOrEmpty(mappingGroupTopicIds))
            {
                lstGroupTopicUnit = JsonConvert.DeserializeObject<List<GroupUnit>>(mappingGroupTopicIds);
            }
            List<int> lstSortOrder = new List<int>();

            if (model.IsCloned)
            {
                foreach (var item in lstTasks)
                {
                    item.AssignmentId = item.TaskId = 0;
                }
            }

            if (!string.IsNullOrEmpty(lstTaskOrdering))
            {
                try
                {
                    if (lstTaskOrdering != "")
                    {
                        lstSortOrder = Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(lstTaskOrdering);
                    }
                }
                catch (Exception ex)
                {
                    ILoggerClient _loggerClient = LoggerClient.Instance;
                    _loggerClient.LogWarning("Assignment exception");
                    _loggerClient.LogException(ex);
                }

                if (lstSortOrder.Count == 0)
                {
                    model.lstAssignmentTasks = lstTasks;
                }
                else
                {
                    int i = 1;
                    if (lstTasks.Count > 0)
                    {
                        foreach (var item in lstSortOrder)
                        {
                            AssignmentTask task = lstTasks.Where(m => m.SortOrder == item).FirstOrDefault();
                            // task.lstTaskFiles = (List<TaskFile>)Session["SaveTaskFiles"];
                            model.lstAssignmentTasks.Add(new AssignmentTask
                            {
                                AssignmentId = task.AssignmentId,
                                TaskId = task.TaskId,
                                TaskTitle = task.TaskTitle,
                                TaskDescription = task.TaskDescription,
                                SortOrder = i,
                                IsActive = task.IsActive,
                                TaskGradingTemplateId = task.TaskGradingTemplateId,
                                lstTaskFiles = task.lstTaskFiles,
                                IsDeleted = task.IsDeleted,
                                QuizId = task.QuizId,
                                IsSetTime = task.IsSetTime,
                                IsRandomQuestion = task.IsRandomQuestion,
                                QuizTime = task.QuizTime,
                                StartDate = task.StartDate,
                                StartTime = task.StartTime,
                                MaxSubmit = task.MaxSubmit,
                                CourseIds = task.CourseIds,
                                Courses = task.Courses,
                                QuestionPaginationRange = task.QuestionPaginationRange,
                                IsHideScore = task.IsHideScore,
                                ShowScoreDate = task.ShowScoreDate,
                                TaskMarks = task.TaskMarks
                            }
                                );
                            i = i + 1;
                        }
                    }
                    model.lstAssignmentTasks.AddRange(lstTasks.Where(m => m.IsDeleted == true));
                }
            }
            else
            {
                model.lstAssignmentTasks = lstTasks;
            }

            model.CreatedById = (int)SessionHelper.CurrentSession.Id;
            model.DueTime = model.DueTimeString != null ? DateTime.ParseExact(model.DueTimeString, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay : new TimeSpan();
            model.lstDocuments = fileNames.ToList();
            var lstStudents = _studentListService.GetStudentDetailsByIds(string.Join(",", model.StudentIds));
            if (!String.IsNullOrEmpty(model.AssignmentDesc))
                model.AssignmentDesc = model.AssignmentDesc.SanitizeHTML();
            op = _assignmentService.UpdateAssignmentData(model, lstObjectives, lstCourseUnit, lstGroupTopicUnit);
            //Added by Deepak Singh on 31 July for async lesson - asylsId
            if (!string.IsNullOrWhiteSpace(asyncLessonId))
            {
                var rsList = new List<AsyncLessonResourcesActivities>();
                var rsModel = new AsyncLessonResourcesActivities();
                rsModel.AsyncLessonId = Helpers.CommonHelper.GetDecryptedId(asyncLessonId);
                rsModel.ModuleId = (int)AsyncLessonModule.Assignment;
                rsModel.SectionId = op.InsertedRowId;
                rsModel.FolderId = 0;
                rsModel.StepNo = 1; //Automatically updated in sp
                rsModel.Name = model.AssignmentTitle;
                rsModel.Path = "";
                rsModel.FileTypeId = 0;
                rsModel.Status = 1;
                rsModel.IsActive = true;
                rsModel.CreatedBy = SessionHelper.CurrentSession.Id;

                rsList.Add(rsModel);

                _asyncLessonService.InsertResourceActivity(rsList);

            }


            List<AssignmentPeerReview> lstPeerReview = new List<AssignmentPeerReview>();
            if (Session["PeerMappingDetails"] != null)
            {
                lstPeerReview = Session["PeerMappingDetails"] as List<AssignmentPeerReview>;
                Session["PeerMappingDetails"] = null;
            }
            try
            {
                if (op.Success)
                {
                    var studentList = _schoolGroupService.GetSchoolGroupUserEmailAddress(string.Join(",", model.SchoolGroupIds), string.Join(",", model.StudentIds)).ToList();
                    Session["TaskList"] = null;
                    if (studentList.Count > 0)
                    {
                        var assignmentfiles = fileNames.Where(e => e.IsCloudFile);
                        List<AssignmentStudentSharedFiles> copiedFiles = new List<AssignmentStudentSharedFiles>();
                        CancellationToken cancellationToken = new CancellationToken();
                        var authResult = new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).
                           AuthorizeAsync(cancellationToken).GetAwaiter().GetResult();
                        foreach (var file in assignmentfiles)
                        {
                            if (file.ResourceFileTypeId == (short)ResourceFileTypes.OneDrive)
                            {
                                if (!model.CreateSeperateFileCopy)
                                {
                                    await CloudFilesHelper.ShareOneDriveFile(file.CloudFileId, studentList.Select(e => e.MicorosoftMailAddress), permission: "write");
                                    studentList.ForEach(x =>
                                    {
                                        copiedFiles.Add(new AssignmentStudentSharedFiles
                                        {
                                            StudentId = x.StudentId,
                                            AssignmentId = op.InsertedRowId,
                                            FilePath = file.UploadedFileName,
                                            ResourceFileTypeId = (short)file.ResourceFileTypeId,
                                            FileName = file.FileName,
                                            IsCollaboratedFile = true
                                        });
                                    });
                                }

                                else
                                {
                                    OneDriveFilesView assignmentFolderDetails = await CloudFilesHelper.CreateOneDriveFolder(model.AssignmentTitle.Trim() + "_" + DateTime.Now.ToString("dd-MMM-yyyy"), file.ParentFolderId);
                                    var assignmentFilesNames = new Dictionary<string, string>();
                                    foreach (var std in studentList.DistinctBy(e => e.MicorosoftMailAddress))
                                    {
                                        var fileName = $"{std.StudentNumber}_{std.StudentName.Replace(" ", "")}_{Path.GetFileNameWithoutExtension(file.FileName)}{Path.GetExtension(file.FileName)}";
                                        await CloudFilesHelper.CopyOneDriveFile(file.CloudFileId, fileName, file.DriveId, assignmentFolderDetails.Id);
                                        assignmentFilesNames.Add(std.MicorosoftMailAddress, fileName);
                                    }
                                    IList<CloudFileListView> newFiles = await CloudFilesHelper.GetOneDriveFolderFiles(assignmentFolderDetails.Id);
                                    foreach (var item in newFiles)
                                    {
                                        var studentMail = assignmentFilesNames.Where(e => e.Value == item.Name)?.FirstOrDefault().Key;
                                        if (!string.IsNullOrEmpty(studentMail))
                                        {
                                            copiedFiles.Add(new AssignmentStudentSharedFiles
                                            {
                                                StudentId = studentList.Where(x => x.MicorosoftMailAddress == studentMail).FirstOrDefault().StudentId,
                                                AssignmentId = op.InsertedRowId,
                                                FilePath = item.WebViewLink,
                                                ResourceFileTypeId = (short)file.ResourceFileTypeId,
                                                FileName = item.Name,
                                                IsCollaboratedFile = true
                                            });
                                        }
                                    }
                                    await CloudFilesHelper.ShareAllFilesToStudents(assignmentFolderDetails.Id, assignmentFilesNames, permission: "write");
                                }
                            }
                            else if (file.ResourceFileTypeId == (short)ResourceFileTypes.OneNote)
                            {
                                foreach (var std in studentList)
                                {
                                    await CloudFilesHelper.AssignNotebookToStudent(std.MicorosoftMailAddress, file.CloudFileId);
                                    copiedFiles.Add(new AssignmentStudentSharedFiles
                                    {
                                        StudentId = std.StudentId,
                                        AssignmentId = op.InsertedRowId,
                                        FilePath = file.UploadedFileName,
                                        ResourceFileTypeId = (short)file.ResourceFileTypeId,
                                        FileName = file.FileName,
                                        IsCollaboratedFile = true
                                    });
                                }
                            }

                            else if (file.ResourceFileTypeId == (short)ResourceFileTypes.GoogleDrive)
                            {
                                foreach (var student in studentList)
                                {
                                    if (model.CreateSeperateFileCopy)
                                    {
                                        string[] folders = new string[] { "assignments", model.AssignmentTitle.Trim() + "_" + DateTime.Now.ToString("dd-MMM-yyyy") };
                                        Google.Apis.Drive.v3.Data.File copiedFile = await CloudFilesHelper.CopyFile(authResult, file.CloudFileId, $"{student.StudentNumber}_{student.StudentName.Replace(" ", "")}_{Path.GetFileNameWithoutExtension(file.FileName)}", folders);
                                        if (copiedFile != null)
                                            CloudFilesHelper.AddGoogleDriveUserPermission(authResult, student.GoogleMailAddress, copiedFile.Id, filePermission: "writer");

                                        copiedFiles.Add(new AssignmentStudentSharedFiles
                                        {
                                            StudentId = student.StudentId,
                                            AssignmentId = op.InsertedRowId,
                                            FilePath = (file.UploadedFileName.Replace(file.CloudFileId, copiedFile.Id)),
                                            ResourceFileTypeId = (short)file.ResourceFileTypeId,
                                            FileName = file.FileName,
                                            IsCollaboratedFile = true
                                        });
                                    }
                                    else
                                    {
                                        CloudFilesHelper.AddGoogleDriveUserPermission(authResult, string.Join(",", studentList.Select(e => e.GoogleMailAddress)), file.CloudFileId, filePermission: "writer");
                                        copiedFiles.Add(new AssignmentStudentSharedFiles
                                        {
                                            StudentId = student.StudentId,
                                            AssignmentId = op.InsertedRowId,
                                            FilePath = (file.UploadedFileName),
                                            ResourceFileTypeId = (short)ResourceFileTypes.GoogleDrive,
                                            FileName = file.FileName,
                                            IsCollaboratedFile = false
                                        });
                                    }

                                }
                            }
                        }
                        bool fileSuccessResult = _assignmentService.UploadStudentSharedCopyFiles(copiedFiles);
                    }
                }
            }
            catch (Exception ex)
            {
                ILoggerClient _loggerClient = LoggerClient.Instance;
                _loggerClient.LogWarning("Assignment exception");
                _loggerClient.LogException(ex);
            }
            if (op.Success && model.IsPeerMarkingEnable)
            {
                foreach (var item in lstPeerReview)
                {
                    item.AssignmentId = op.InsertedRowId;
                }
                var peerResult = _assignmentService.AddUpdatePeermarking(lstPeerReview);
            }
            string studentInternalIds = "";
            foreach (var item in lstStudents)
            {
                studentInternalIds = studentInternalIds + (studentInternalIds == "" ? "" : "|") + item.StudentInternalId;
            }
            if (op.Success && model.IsPublished)
            {
                try
                {
                    SendEmailNotificationView sendEmailNotificationView = new SendEmailNotificationView();
                    sendEmailNotificationView.FromMail = Constants.NoReplyMail;
                    sendEmailNotificationView.LogType = "AssignmentNotification";
                    if (model.AssignmentId > 0)
                    {
                        sendEmailNotificationView.Subject = Constants.AssignmentResubmitNotification;
                    }
                    else
                    {
                        sendEmailNotificationView.Subject = Constants.AssignmentNotification;
                    }
                    string EmailBody = "";
                    StreamReader reader = new StreamReader(Server.MapPath(Constants.AssignmentNotificationTemplate), Encoding.UTF8);
                    EmailBody = reader.ReadToEnd();
                    EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
                    //EmailBody = EmailBody.Replace("@@StudentName", SessionHelper.CurrentSession.FullName);
                    EmailBody = EmailBody.Replace("@@TeacherName", SessionHelper.CurrentSession.FullName);
                    EmailBody = EmailBody.Replace("@@AssignmentName", model.AssignmentTitle);
                    sendEmailNotificationView.StudentId = studentInternalIds;
                    sendEmailNotificationView.Message = EmailBody;
                    sendEmailNotificationView.Username = SessionHelper.CurrentSession.UserName;
                    if (model.IsInstantMailToParent && model.IsInstantNotificationMailToStudent)
                    {
                        sendEmailNotificationView.NotifyTo = "BOTH";
                        bool isSuccess = _commonService.SendEmailNotifications(sendEmailNotificationView);
                    }
                    else if (!model.IsInstantMailToParent && model.IsInstantNotificationMailToStudent)
                    {
                        sendEmailNotificationView.NotifyTo = "STUDENT";
                        bool isSuccess = _commonService.SendEmailNotifications(sendEmailNotificationView);
                    }
                    else if (model.IsInstantMailToParent && !model.IsInstantNotificationMailToStudent)
                    {
                        sendEmailNotificationView.NotifyTo = "PARENT";
                        bool isSuccess = _commonService.SendEmailNotifications(sendEmailNotificationView);
                    }
                    // var operationalDetails = EmailHelper.SendEmail(toEmailAddrees, Constants.AssignmentNotification, EmailBody);

                }
                catch (Exception ex)
                {
                    ILoggerClient _loggerClient = LoggerClient.Instance;
                    _loggerClient.LogWarning("Assignment exception");
                    _loggerClient.LogException(ex);
                }
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveAssignmentDetails(AssignmentEdit model, string lstTaskOrdering)
        {

            if (!CurrentPagePermission.CanEdit)
            {
                return RedirectToAction("NoPermission", "Error");
            }
            ICommonService _commonService = new CommonService();
            // model.ArchiveDate = model.ArchiveDate.HasValue ? model.ArchiveDate : DateTime.Now;
            List<AssignmentTask> lstTask;
            model.lstAssignmentTasks = new List<AssignmentTask>();
            List<Objective> lstObjectives = new List<Objective>();
            List<CourseUnit> lstCourseUnit = new List<CourseUnit>();
            List<GroupUnit> lstGroupTopicUnit = new List<GroupUnit>();
            List<int> lstSortOrder;
            if (Session["TaskList"] == null)
            {
                lstTask = new List<AssignmentTask>();
            }
            else
            {
                lstTask = (List<AssignmentTask>)Session["TaskList"];

            }
            if (model.IsCloned)
            {
                foreach (var item in lstTask)
                {
                    item.AssignmentId = item.TaskId = 0;
                }
            }
            if (Session["lstObjective"] != null)
            {
                lstObjectives = Session["lstObjective"] as List<Objective>;
                Session["lstObjective"] = null;
            }
            // No used of this method
            if (Session["lstCourseUnit"] != null)
            {
                lstCourseUnit = Session["lstCourseUnit"] as List<CourseUnit>;
                Session["lstCourseUnit"] = null;
            }
            if (Session["lstGroupTopicUnit"] != null)
            {
                lstGroupTopicUnit = Session["lstGroupTopicUnit"] as List<GroupUnit>;
                Session["lstGroupTopicUnit"] = null;
            }

            if (!string.IsNullOrEmpty(lstTaskOrdering))
            {
                lstSortOrder = Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(lstTaskOrdering);
                if (lstSortOrder.Count == 0)
                {
                    model.lstAssignmentTasks = lstTask;
                }
                else
                {
                    int i = 1;
                    foreach (var item in lstSortOrder)
                    {
                        AssignmentTask task = lstTask.Where(m => m.SortOrder == item).FirstOrDefault();
                        // task.lstTaskFiles = (List<TaskFile>)Session["SaveTaskFiles"];
                        model.lstAssignmentTasks.Add(new AssignmentTask
                        {
                            AssignmentId = task.AssignmentId,
                            TaskId = task.TaskId,
                            TaskTitle = task.TaskTitle,
                            TaskDescription = task.TaskDescription,
                            SortOrder = i,
                            IsActive = task.IsActive,
                            TaskGradingTemplateId = task.TaskGradingTemplateId,
                            lstTaskFiles = task.lstTaskFiles,
                            IsDeleted = task.IsDeleted,
                            QuizId = task.QuizId,
                            IsSetTime = task.IsSetTime,
                            IsRandomQuestion = task.IsRandomQuestion,
                            QuizTime = task.QuizTime,
                            StartDate = task.StartDate,
                            StartTime = task.StartTime,
                            MaxSubmit = task.MaxSubmit,
                            QuestionPaginationRange = task.QuestionPaginationRange
                        }
                            );
                        i = i + 1;
                    }
                    model.lstAssignmentTasks.AddRange(lstTask.Where(m => m.IsDeleted == true));
                }

            }
            else
            {
                model.lstAssignmentTasks = lstTask;
            }
            model.CreatedById = (int)SessionHelper.CurrentSession.Id;
            model.DueTime = model.DueTimeString != null ? DateTime.ParseExact(model.DueTimeString, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay : new TimeSpan();
            model.lstDocuments = Session["AssignmentFiles"] as List<AssignmentFile>;
            var lstStudents = _studentListService.GetStudentDetailsByIds(string.Join(",", model.StudentIds));
            var result = _assignmentService.UpdateAssignmentData(model, lstObjectives, lstCourseUnit, lstGroupTopicUnit);
            List<AssignmentPeerReview> lstPeerReview = new List<AssignmentPeerReview>();
            if (Session["PeerMappingDetails"] != null)
            {
                lstPeerReview = Session["PeerMappingDetails"] as List<AssignmentPeerReview>;
                Session["PeerMappingDetails"] = null;
            }
            if (result.Success && model.IsPeerMarkingEnable)
            {
                foreach (var item in lstPeerReview)
                {
                    item.AssignmentId = result.InsertedRowId;
                }
                var peerResult = _assignmentService.AddUpdatePeermarking(lstPeerReview);
            }
            string studentInternalIds = "";
            foreach (var item in lstStudents)
            {
                studentInternalIds = studentInternalIds + (studentInternalIds == "" ? "" : "|") + item.StudentInternalId;
            }
            if (result.Success)
            {
                Session["TaskList"] = null;
                foreach (var file in model.lstDocuments.Where(e => e.IsCloudFile))
                    await CloudFilesHelper.AssignNotebookToStudent("rohit.neosoft@gemseducation.com", file.CloudFileId);
            }

            if (result.Success && model.IsInstantMessage && model.IsPublished)
            {
                string TemplatePath = "";
                SendEmailNotificationView sendEmailNotificationView = new SendEmailNotificationView();
                sendEmailNotificationView.FromMail = Constants.NoReplyMail;
                sendEmailNotificationView.LogType = "AssignmentNotification";
                if (model.AssignmentId > 0)
                {
                    TemplatePath = Constants.AssignmentResubmitNotificationTemplate;
                    sendEmailNotificationView.Subject = Constants.AssignmentResubmitNotification;

                }
                else
                {
                    TemplatePath = Constants.AssignmentNotificationTemplate;
                    sendEmailNotificationView.Subject = Constants.AssignmentNotification;

                }
                string EmailBody = "";
                StreamReader reader = new StreamReader(Server.MapPath(Constants.AssignmentNotificationTemplate), Encoding.UTF8);
                EmailBody = reader.ReadToEnd();
                EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
                //EmailBody = EmailBody.Replace("@@StudentName", SessionHelper.CurrentSession.FullName);
                EmailBody = EmailBody.Replace("@@TeacherName", SessionHelper.CurrentSession.FullName);
                EmailBody = EmailBody.Replace("@@AssignmentName", model.AssignmentTitle);
                sendEmailNotificationView.StudentId = studentInternalIds;
                sendEmailNotificationView.Message = EmailBody;
                sendEmailNotificationView.Username = SessionHelper.CurrentSession.UserName;
                // var operationalDetails = EmailHelper.SendEmail(toEmailAddrees, Constants.AssignmentNotification, EmailBody);
                bool isSuccess = _commonService.SendEmailNotifications(sendEmailNotificationView);
            }
            //if (result.Success && model.IsResubmit)
            //{
            //    string EmailBody = "";
            //    StreamReader reader = new StreamReader(Server.MapPath(Constants.AssignmentResubmitNotificationTemplate), Encoding.UTF8);
            //    EmailBody = reader.ReadToEnd();
            //    EmailBody = EmailBody.Replace("@@SchoolLogo", "/Uploads/SchoolImages/" + SessionHelper.CurrentSession.SchoolImage);
            //    //EmailBody = EmailBody.Replace("@@StudentName", SessionHelper.CurrentSession.FullName);
            //    EmailBody = EmailBody.Replace("@@TeacherName", SessionHelper.CurrentSession.FullName);
            //    EmailBody = EmailBody.Replace("@@AssignmentName", model.AssignmentTitle);
            //    SendEmailNotificationView sendEmailNotificationView = new SendEmailNotificationView();
            //    sendEmailNotificationView.FromMail = Constants.NoReplyMail;
            //    sendEmailNotificationView.LogType = "AssignmentNotification";
            //    sendEmailNotificationView.StudentId = studentInternalIds;
            //    sendEmailNotificationView.Subject = Constants.AssignmentResubmitNotification;
            //    sendEmailNotificationView.Message = EmailBody;
            //    sendEmailNotificationView.Username = SessionHelper.CurrentSession.UserName;
            //    // var operationalDetails = EmailHelper.SendEmail(toEmailAddrees, Constants.AssignmentNotification, EmailBody);
            //    bool isSuccess = _commonService.SendEmailNotifications(sendEmailNotificationView);
            //}

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult InitAddEditAassignmentForm(string id, bool isCloned = false)
        {
            if (!CurrentPagePermission.CanEdit && !(SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin))
            {
                return RedirectToAction("NoPermission", "Error");
            }
            int? assignmentId = !string.IsNullOrWhiteSpace(id) ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id)) : 0;
            var model = new AssignmentEdit();
            Session["AssignmentFiles"] = null;
            Session["lstObjective"] = null;
            Session["lstSelectedMyFiles"] = null;
            Session["AssignmentFiles"] = null;
            Session["PeerMappingDetails"] = null;
            ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName");
            // ViewBag.CourseList=new SelectList(SelectListHelper.GetSelectListData((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName");
            ViewBag.CourseList = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct();

            List<Phoenix.Models.SchoolGroup> lstSchoolGroup = new List<Phoenix.Models.SchoolGroup>();
            lstSchoolGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, true).ToList();
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });

            ViewBag.InstantMessageUserTypeList = new List<SelectListItem>()
            {
                new SelectListItem {Text=ResourceManager.GetString("Shared.Labels.Both"), Value="0"},
                new SelectListItem { Text =ResourceManager.GetString("Shared.Labels.Parents"), Value= ((short)UserTypes.Parent).ToString()},
                new SelectListItem{Text =ResourceManager.GetString("Shared.Labels.Students"), Value =((short)UserTypes.Student).ToString()}
            };

            ViewBag.GradingTemplateList = new SelectList(SelectListHelper.GetSelectListData(ListItems.GradingTemplate, SessionHelper.CurrentSession.SchoolId), "ItemId", "ItemName");
            long schoolId = Helpers.CommonHelper.GetLoginUser().SchoolId;
            if (schoolId <= 0)
            {
                schoolId = (int)SessionHelper.CurrentSession.SchoolId;
            }
            List<SchoolInformation> schoolList = _schoolService.GetSchoolList().ToList();
            List<SelectListItem> lstSchoolList = new SelectList(schoolList, "SchoolId", "SchoolName").ToList();
            ViewBag.SchoolList = lstSchoolList;
            ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();

            if (assignmentId > 0)
            {
                var assignment = _assignmentService.GetAssignmentById(assignmentId.Value);

                ViewBag.archivedDate = assignment.ArchiveDate;
                if (assignment.CreatedById == SessionHelper.CurrentSession.Id || SessionHelper.CurrentSession.IsAdmin)
                {
                    EntityMapper<Assignment, AssignmentEdit>.Map(assignment, model);
                    List<AssignmentTask> lstAssignmentTasks = _assignmentService.GetAssignmentTasks(assignmentId.Value).ToList().Where(m => m.IsDeleted == false).OrderBy(m => m.SortOrder).ToList();
                    int k = 1;
                    model.lstAssignmentTasks = new List<AssignmentTask>();
                    foreach (var item in lstAssignmentTasks)
                    {
                        item.SortOrder = k;
                        item.lstTaskFiles = _assignmentService.GetTaskFilesByTaskId(item.TaskId).ToList();
                        model.lstAssignmentTasks.Add(item);
                        k++;
                    }
                    model.DueTimeString = assignment.DueDate == null ? "" : assignment.DueDate.Value.ToString("h:mm tt");
                    model.lstDocuments = _assignmentService.GetAssignmentFiles(assignmentId.Value).ToList();
                    Session["AssignmentFiles"] = model.lstDocuments;
                    model.lstObjective = _subjectService.GetAssignmentObjective(assignmentId.Value).ToList();
                    Session["lstObjective"] = model.lstObjective;
                    model.lstCourseUnit = _subjectService.GetAssignmentUnit(assignmentId.Value).ToList();
                    Session["lstCourseUnit"] = model.lstCourseUnit;
                    var gd = _gradingTemplateService.GetGradingTemplateById(model.GradingTemplateId);
                    model.GradingTemplate = gd.GradingTemplateTitle;
                    model.GradingTemplateLogo = gd.GradingTemplateLogo;
                    List<Phoenix.Models.SchoolGroup> lstSchoolGroups = new List<Phoenix.Models.SchoolGroup>();
                    lstSchoolGroups = _assignmentService.GetSelectedSchoolGroupsByAssignmentID(assignmentId.Value).ToList();
                    model.SchoolGroupId = lstSchoolGroups.Select(m => m.SchoolGroupId.ToString()).ToList();

                    var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
                    {
                        Value = x.SchoolGroupId.ToString(),
                        Text = x.SchoolGroupName,
                        Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
                        Selected = model.SchoolGroupId.Contains(x.SchoolGroupId.ToString()) ? true : false
                    }).ToList();
                    ViewBag.lstSchoolGroup = groupedOptions;
                    var StudentList = _studentListService.GetStudentsInSelectedGroups(string.Join(",", model.SchoolGroupId));
                    var StudentSelectList = StudentList.Select(i => new ListItem()
                    {
                        ItemName = i.FirstName + " " + i.LastName + " (" + i.UserName + ")",
                        ItemId = i.StudentId.ToString()
                    }).OrderBy(m => m.ItemName);
                    ViewBag.StudentList = new SelectList(StudentSelectList.OrderBy(m => m.ItemName), "ItemId", "ItemName");
                    List<AssignmentStudent> lstAsgStudents = new List<AssignmentStudent>();
                    int groupId = 0;
                    lstAsgStudents = _assignmentService.GetStudentsByAssignmentId(assignmentId.Value, groupId).ToList();
                    model.StudentId = lstAsgStudents.Select(m => m.StudentId.ToString()).ToList();
                    if (model.lstDocuments != null)
                    {
                        foreach (var item in model.lstDocuments)
                        {
                            item.FileExtension = item.FileName.Split('.')[1];
                        }
                    }
                    Session["TaskList"] = model.lstAssignmentTasks;
                    if (model.IsPeerMarkingEnable)
                    {
                        List<AssignmentPeerReview> lstPeerReview = new List<AssignmentPeerReview>();
                        lstPeerReview = _assignmentService.GetPeerMappingDetails(assignmentId.Value).ToList();
                        Session["PeerMappingDetails"] = lstPeerReview;
                    }
                }
                else
                {
                    return RedirectToAction("NoPermission", "Error");
                }
            }
            else
            {
                List<SelectListItem> lstStudentList = new List<SelectListItem>();
                ViewBag.StudentList = lstStudentList;
                var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
                {
                    Value = x.SchoolGroupId.ToString(),
                    Text = x.SchoolGroupName,
                    Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2]))
                }).ToList();
                ViewBag.lstSchoolGroup = groupedOptions;
                model.IsAddMode = true;
                model.TeacherId = PhoenixConfiguration.Instance.AdminUserId;
                model.lstAssignmentTasks = new List<AssignmentTask>();
                //model.StartDate = DateTime.Now;
                Session["TaskList"] = null;
                model.IsPeerMarkingEnable = false;
            }
            if (isCloned)
            {
                model.IsAddMode = model.IsCloned = true;
                var groupedOptions = lstSchoolGroup.Select(x => new SelectListItem
                {
                    Value = x.SchoolGroupId.ToString(),
                    Text = x.SchoolGroupName,
                    Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2]))
                }).ToList();
                ViewBag.lstSchoolGroup = groupedOptions;
                model.AssignmentId = 0;
                model.SchoolGroupIds = null;

                model.StudentIds = null;
                model.StudentId = null;
                model.lstObjective = null;//for remove selected Tagged Curriculums.
                Session["lstObjective"] = null;//for remove selected Tagged Curriculums.
            }
            return View("_AddEditAssignment", model);
        }

        public ActionResult LoadAssignment(AssignmentFilter loadAssignment)
        {
            var pageName = string.Empty;
            var model = new Pagination<AssignmentStudentDetails>();
            var assignmentList = new List<AssignmentStudentDetails>();
            if (SessionHelper.CurrentSession.IsParent() || SessionHelper.CurrentSession.IsStudent())
            {
                var studentId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                pageName = "_AssignmentGroup";
                if (loadAssignment.filterVal != null)
                    assignmentList = _assignmentService.GeStudentFilterSchoolGroupsById(loadAssignment.page, loadAssignment.size, Convert.ToInt64(studentId), loadAssignment.searchString, loadAssignment.assignmentType, loadAssignment.sortBy, loadAssignment.filterVal).ToList();
                else
                    assignmentList = _assignmentService.GetAssignmentByStudentId(loadAssignment.page, loadAssignment.size, Convert.ToInt64(studentId), loadAssignment.searchString, loadAssignment.assignmentType, loadAssignment.sortBy).ToList();
                if (assignmentList.Any())
                    model = new Pagination<AssignmentStudentDetails>(loadAssignment.page, loadAssignment.size, assignmentList, assignmentList.FirstOrDefault().TotalCount);
                model.RecordCount = assignmentList.Count == 0 ? 0 : assignmentList[0].TotalCount;
                bool NoRecords = false;
                if ((loadAssignment.page == 1 && assignmentList.Count == 0) || (!string.IsNullOrEmpty(loadAssignment.searchString) && assignmentList.Count == 0))
                {
                    NoRecords = true;
                }
                ViewBag.Norecords = NoRecords;
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                pageName = "_TeacherAssignmentGroup";
                if (loadAssignment.filterVal != null)
                    assignmentList = _assignmentService.GetFilterSchoolGroupsById(Helpers.CommonHelper.GetLoginUser().Id, loadAssignment).ToList();
                else
                    assignmentList = _assignmentService.GetAssignmentStudentDetails(Helpers.CommonHelper.GetLoginUser().Id, loadAssignment).ToList();
                //if (assignmentList.Any())
                //    model = new Pagination<AssignmentStudentDetails>(page, 6, assignmentList, assignmentList.geFirstOrDefault().TotalCount);
                //model.RecordCount = assignmentList.Count == 0 ? 0 : assignmentList[0].TotalCount;
                bool NoRecords = false;
                if ((loadAssignment.page == 1 && assignmentList.Count == 0) || (!string.IsNullOrEmpty(loadAssignment.searchString) && assignmentList.Count == 0))
                {
                    NoRecords = true;
                }
                ViewBag.Norecords = NoRecords;
            }
            //model.LoadPageRecordsUrl = "/Assignments/Assignment/LoadAssignment?pageIndex={0}";
            //model.SearchString = searchString;
            //model.CreatedByMeOnly = CreatedByMeOnly;
            var result = new Object();
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, pageName, assignmentList);
            var contentList = new string[] { htmlContent };
            result = new
            {
                isContentFinished = assignmentList.Count < loadAssignment.size ? true : false,
                content = contentList
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult LoadAdminAssignment(AssignmentFilter loadAssignment)
        {
            var pageName = string.Empty;
            var model = new Pagination<AssignmentStudentDetails>();
            var assignmentList = new List<AssignmentStudentDetails>();
            if (SessionHelper.CurrentSession.IsAdmin)
            {
                pageName = "_TeacherAssignmentGroup";
                loadAssignment.schoolId = SessionHelper.CurrentSession.SchoolId;
                assignmentList = _assignmentService.GetAdminAssignmentStudentDetails(loadAssignment).ToList();
                bool NoRecords = false;
                if ((loadAssignment.page == 1 && assignmentList.Count == 0) || (!string.IsNullOrEmpty(loadAssignment.searchString) && assignmentList.Count == 0))
                {
                    NoRecords = true;
                }
                ViewBag.Norecords = NoRecords;
            }
            var result = new Object();
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, pageName, assignmentList);
            var contentList = new string[] { htmlContent };
            result = new
            {
                isContentFinished = assignmentList.Count < loadAssignment.size ? true : false,
                content = contentList
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentGroupAssignments(string startDate, string endDate, string studentId)
        {
            long schoolStudentId;
            DateTime stDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(startDate));
            DateTime edDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(endDate));
            // SessionHelper.CurrentSession.
            schoolStudentId = EncryptDecryptHelper.DecodeBase64(studentId).ToInteger();
            schoolStudentId = Convert.ToInt64(schoolStudentId);
            Session["studentId"] = schoolStudentId;

            Session["stdate"] = stDate;
            Session["edDate"] = edDate;

            return View();
        }

        public ActionResult LoadStudentGroupAssignments(int pageIndex = 1, string searchString = "", string startDate = "", string endDate = "", string studentId = "")
        {
            DateTime stDate, edDate;
            long schoolStudentId = 0;
            long teacherId = SessionHelper.CurrentSession.Id;

            if (!string.IsNullOrEmpty(startDate))
            {
                stDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(startDate));
                edDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(endDate));
                schoolStudentId = EncryptDecryptHelper.DecodeBase64(studentId).ToInteger();
            }
            else
            {
                stDate = DateTime.Parse(Session["stdate"].ToString());
                edDate = DateTime.Parse(Session["edDate"].ToString());
                schoolStudentId = (Int64)Session["studentId"];
            }

            var pageName = "";
            var model = new Pagination<AssignmentStudentDetails>();
            string assignmentType = "";

            pageName = "_StudentAssignmentGroup";
            var assignmentList = _assignmentService.GetAssignmentByStudentIdAndTeacherIdWithDateRange(pageIndex, 6, schoolStudentId, teacherId, stDate, edDate, searchString, assignmentType).ToList();
            if (assignmentList.Any())
                model = new Pagination<AssignmentStudentDetails>(pageIndex, 6, assignmentList, assignmentList.FirstOrDefault().TotalCount);
            model.RecordCount = assignmentList.Count == 0 ? 0 : assignmentList[0].TotalCount;

            model.LoadPageRecordsUrl = "/Assignments/Assignment/LoadStudentGroupAssignments?pageIndex={0}";
            model.SearchString = searchString;
            return PartialView(pageName, model);
        }

        [HttpPost]
        public ActionResult SearchAssignment(string searchString)
        {
            var pageName = "";
            int pageIndex = 1;
            AssignmentFilter loadAssignment = new AssignmentFilter();
            var model = new Pagination<AssignmentStudentDetails>();
            if (SessionHelper.CurrentSession.IsParent() || SessionHelper.CurrentSession.IsStudent())
            {
                var studentId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                //pageName = "~/Areas/Assignments/Views/Assignment/_StudentAssignment.cshtml";
                pageName = "_AssignmentGroup";
                var assignmentList = _assignmentService.GetAssignmentByStudentId(pageIndex, 6, Convert.ToInt64(studentId), searchString).ToList();
                if (assignmentList.Any())
                    model = new Pagination<AssignmentStudentDetails>(pageIndex, 6, assignmentList, assignmentList.FirstOrDefault().TotalCount);
                model.RecordCount = assignmentList.Count == 0 ? 0 : assignmentList[0].TotalCount;
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                pageName = "_TeacherAssignmentGroup";
                //var assignmentList = _assignmentService.GetAssignmentStudentDetails(pageIndex, 6, Helpers.CommonHelper.GetLoginUser().Id, searchString).ToList();
                var assignmentList = _assignmentService.GetAssignmentStudentDetails(Helpers.CommonHelper.GetLoginUser().Id, loadAssignment).ToList();
                //assignmentList = assignmentList.Where(x => x.AssignmentTitle == "test 1 22 august").ToList();
                if (assignmentList.Any())
                    model = new Pagination<AssignmentStudentDetails>(pageIndex, 6, assignmentList, assignmentList.FirstOrDefault().TotalCount);
                model.RecordCount = assignmentList.Count == 0 ? 0 : assignmentList[0].TotalCount;
            }

            return PartialView(pageName, model);
        }

        private string ConvertViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter writer = new StringWriter())
            {
                ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }

        public ActionResult LoadSelectedStudentsIngroup(string[] grpids, string[] stu)
        {
            string gid = string.Empty;
            string sId = string.Empty;
            string oldgrp = string.Empty;
            IEnumerable<ListItem> StudentSelectList = new List<ListItem>();
            if (grpids != null)
            {
                gid = string.Join(",", grpids);
                var StudentList = _studentListService.GetStudentsInSelectedGroups(string.Join(",", grpids));
                StudentSelectList = StudentList.Select(i => new ListItem()
                {
                    ItemName = i.FirstName + " " + i.LastName + " (" + i.UserName + ")",
                    ItemId = i.StudentId.ToString()
                });

                if (stu != null)
                {

                    var oldStudentList = _studentListService.GetStudentsInSelectedGroups(string.Join(",", gid));
                    foreach (var sid in stu)
                    {
                        StudentSelectList = oldStudentList.Where(e => e.StudentId == int.Parse(sid)).Select(i => new ListItem()
                        {
                            ItemName = i.FirstName + " " + i.LastName + " (" + i.UserName + ")",
                            ItemId = i.StudentId.ToString()
                        });

                    }


                }


            }
            //if (stu != null)
            //{
            //    sId = string.Join(",", stu);
            //}
            //if (oldGrpIds != null)
            //{
            //    oldgrp = string.Join(",", oldGrpIds);
            //}
            return Json(StudentSelectList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubjectSchoolGroups(int? id)
        {
            var subjectId = id.HasValue ? id.Value : 0;
            int systemlangaugeid = LocalizationHelper.CurrentSystemLanguageId;
            long schoolId = SessionHelper.CurrentSession.SchoolId;
            var schoolGroups = _schoolGroupService.GetSubjectSchoolGroups(subjectId, schoolId);

            var schoolGroupList = schoolGroups.Select(i => new ListItem()
            {
                ItemName = i.SchoolGroupDescription.Replace("_", "/"),
                ItemId = i.SchoolGroupId.ToString()
            });
            return Json(schoolGroupList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteAssignment(int assignmentId)
        {
            OperationDetails op = new OperationDetails();
            var assignment = _assignmentService.GetAssignmentById(assignmentId);
            assignment.DeletedBy = (int)SessionHelper.CurrentSession.Id;
            op = _assignmentService.DeleteAssignmentData(assignment);
            return Json(op, JsonRequestBehavior.AllowGet);
        }


        //public JsonResult GetSchoolGroupSubject(int schoolGroupId)
        //{
        //    int subjectId = 0;
        //    var schoolGroup = _schoolGroupService.GetSchoolGroupByID(schoolGroupId);           
        //    if (schoolGroup.SubjectId != null)
        //    {
        //        subjectId = schoolGroup.SubjectId.Value;
        //    }
        //    return Json(subjectId, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult GetTopicSubTopicStructure(int subjectId)
        {
            var TopicList = _subjectService.GetTopicSubTopicStructure(subjectId).ToList();
            List<TopicTreeItem> treeList = new List<TopicTreeItem>();
            TopicList.ForEach(l => treeList.Add(new TopicTreeItem() { id = l.SubSyllabusId, parentId = l.ParentId, name = l.SubSyllabusDescription, isLesson = l.isLesson }));
            return Json(treeList);
        }

        public JsonResult GetUnitStructure(string courseIds)
        {
            var TopicList = _subjectService.GetUnitStructure(courseIds).ToList();
            List<TopicTreeItem> treeList = new List<TopicTreeItem>();
            TopicList.ForEach(l => treeList.Add(new TopicTreeItem() { id = l.SubSyllabusId, parentId = l.ParentId, name = l.SubSyllabusDescription, isLesson = l.isLesson }));
            return Json(treeList);
        }

        public ActionResult GetUnitStructureView(string courseIds, string groupIds, int? assignmentId)
        {
            var TopicList = _subjectService.GetUnitStructure(courseIds).ToList();
            var GroupTopicIdList = _subjectService.GetSelectedUnitByGroupId(groupIds);
            if (assignmentId.HasValue)
            {
                List<CourseUnit> editCourseUnitId = new List<CourseUnit>();
                List<Objective> editLessonIds = new List<Objective>();
                List<GroupUnit> lstGroupUnits = new List<GroupUnit>();
                var lstObjective = _subjectService.GetAssignmentObjective(assignmentId.Value).ToList();
                var lstCourseUnitid = _subjectService.GetAssignmentUnit(assignmentId.Value).ToList();
                var lstCourseTopicId = _subjectService.GetAssignmentCourseTopicUnit(assignmentId.Value).ToList();
                if (Session["lstCourseUnit"] != null)
                {
                    editCourseUnitId = Session["lstCourseUnit"] as List<CourseUnit>;
                    Session["lstCourseUnit"] = null;
                }
                if (Session["lstCourseLesson"] != null)
                {
                    editLessonIds = Session["lstCourseLesson"] as List<Objective>;
                    Session["lstCourseLesson"] = null;
                }
                foreach (var itemGroupUnit in GroupTopicIdList)
                {
                    if (lstCourseTopicId.Count > 0)
                    {
                        if (lstCourseTopicId.Where(m => m.GroupCourseTopicId == Convert.ToInt32(itemGroupUnit.GroupCourseTopicId)).Any())
                        {
                            itemGroupUnit.IsActive = true;
                        }
                    }
                    if (Session["lstGroupUnit"] != null)
                    {
                        List<GroupUnit> lstSessionGroupUnits = Session["lstGroupUnit"] as List<GroupUnit>;
                        if (lstSessionGroupUnits.Where(m => m.GroupCourseTopicId == Convert.ToInt32(itemGroupUnit.GroupCourseTopicId)).Any())
                        {
                            itemGroupUnit.IsActive = true;
                        }
                        Session["lstGroupUnit"] = null;
                    }
                    lstGroupUnits.Add(itemGroupUnit);
                }
                ViewBag.lstGroupUnits = lstGroupUnits;
                foreach (var item in TopicList)
                {
                    if (lstObjective.Count > 0)
                    {
                        if (lstObjective.Where(m => m.ObjectiveId == Convert.ToInt32(item.SubSyllabusId)).Any())
                        {
                            item.IsSelected = true;
                        }
                    }
                    if (lstCourseUnitid.Count > 0)
                    {
                        if (lstCourseUnitid.Where(m => m.CourseUnitId == Convert.ToInt32(item.SubSyllabusId)).Any())
                        {
                            item.IsSelected = true;
                        }
                    }
                    if (editCourseUnitId.Count > 0)
                    {
                        if (editCourseUnitId.Where(m => m.CourseUnitId == Convert.ToInt32(item.SubSyllabusId)).Any())
                        {
                            item.IsSelected = true;
                        }
                    }
                    if (editLessonIds.Count > 0)
                    {
                        if (editLessonIds.Where(m => m.ObjectiveId == Convert.ToInt32(item.SubSyllabusId)).Any())
                        {
                            item.IsSelected = true;
                        }
                    }
                }
            }

            return PartialView("_UnitStructureView", TopicList);
        }

        /// <summary>
        ///Author : Mahesh Chikhale
        ///Created Date : 24-June-2019
        ///Description : To open modal popup for insert/update Assignment task
        /// </summary>
        /// <param name="id">TaskId</param>
        ///  <param name="gradingTemplateId">gradingTemplateId</param>
        /// <returns></returns>
        public ActionResult InitAddEditTaskDetailsForm(string tasksDetails, string RandomId, int? gradingTemplateId, bool isAddMode, decimal AssignmentMarks)
        {
            var modal = new AssignmentTaskEdit();
            GroupQuizService _groupQuizService = new GroupQuizService();
            List<GradingTemplate> lstGradingTemplate = new List<GradingTemplate>();
            lstGradingTemplate = _gradingTemplateService.GetGradtingTemplates((int)SessionHelper.CurrentSession.SchoolId).ToList();
            List<AssignmentTask> lstTasks = new List<AssignmentTask>();
            AssignmentTask task = new AssignmentTask();
            if (gradingTemplateId.HasValue && isAddMode)
            {
                modal.TaskGradingTemplateId = gradingTemplateId.Value;
            }
            if (tasksDetails != "")
            {
                decimal sumOfTaskMarks = 0;
                lstTasks = JsonConvert.DeserializeObject<List<AssignmentTask>>(tasksDetails);
                foreach (var item in lstTasks)
                {
                    if (AssignmentMarks == 0)
                    {
                        item.TaskMarks = 0;
                    }
                    sumOfTaskMarks += item.TaskMarks;
                }
                AssignmentMarks = AssignmentMarks - sumOfTaskMarks;
            }
            if (!isAddMode)
            {
                foreach (var item in lstTasks)
                {
                    if (item.RandomId == RandomId)
                    {
                        task = item;
                        EntityMapper<AssignmentTask, AssignmentTaskEdit>.Map(task, modal);
                        modal.IsAddMode = false;
                        modal.lstTaskFile = item.lstTaskFiles;
                        if (item.Courses != null)
                        {
                            modal.CourseId = item.Courses.Split(',').ToList();
                        }
                        break;
                    }
                }
            }
            else
            {
                //modal.RandomId = DateTime.Now.Ticks.ToString();
                modal.RandomId = Guid.NewGuid().ToString();
                modal.MaxSubmit = 1;
                modal.QuestionPaginationRange = 1;
                modal.QuizTime = 1;
                modal.TaskMarks = AssignmentMarks != 0 ? AssignmentMarks : 0;
            }

            ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            ViewBag.lstGradingTemplate = new SelectList(lstGradingTemplate, "GradingTemplateId", "GradingTemplateTitle", modal.TaskGradingTemplateId);
            //ViewBag.QuizList = new SelectList(SelectListHelper.GetSelectListData(ListItems.QuizList, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)), "ItemId", "ItemName");
            var QuizList = _groupQuizService.GetQuizByCourse(String.Empty, (int)SessionHelper.CurrentSession.SchoolId);
            ViewBag.QuizList = new SelectList(QuizList.Select(i => new Phoenix.Common.Models.ListItem()
            {
                ItemName = i.QuizName,
                ItemId = i.QuizId.ToString()
            }).ToList(), "ItemId", "ItemName");
            //ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName");
            List<ListItem> lstCourse = new List<ListItem>();
            lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
            var courses = lstCourse.Select(x => new SelectListItem
            {
                Value = x.ItemId.ToString(),
                Text = x.ItemName,
            }).ToList();
            ViewBag.CourseList = courses; /*new SelectList(lstCourse, "Value", "Text", 56609);*/
            ViewBag.filedetails = modal.lstTaskFile == null ? "" : JsonConvert.SerializeObject(modal.lstTaskFile);
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_AddEditAssignmentTask", modal);
            return Json(htmlContent, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveAssignmentTask(AssignmentTaskEdit model, string taskFilesDetails, string taskDetails)
        {
            //convert arabic dates
            model.ShowScoreDate = model.strShowScoreDate.ToSystemReadableDate();
            if (model.IsSetTime)
            {
                model.StartDate = model.strStartDate.ToSystemReadableDate();
                model.StartTime = model.StartTime.ToSystemReadableTime();
                if (model.StartDate != null && (!String.IsNullOrEmpty(model.StartTime)))
                {
                    //convert arabic dates
                    model.StartDate = model.strStartDate.ToSystemReadableDate();
                    model.ShowScoreDate = model.strShowScoreDate.ToSystemReadableDate();
                    model.StartTime = model.StartTime.ToSystemReadableTime();
                    string iString = model.StartDate.Value.ToString("MM/dd/yyyy") + " " + model.StartTime;
                    DateTime startDateTime = Convert.ToDateTime(iString);
                    DateTime? testUTC = startDateTime.ConvertLocalToUTC();
                    //model.StartDate = startDateTime;
                    //model.StartTime = testUTC.Value.ToString("h:mm tt");
                    model.StartDate = testUTC;
                    model.StartTime = Convert.ToDateTime(model.StartDate).ToString("h:mm tt");
                }
            }
            int maxSortOrder = 0;
            var sourceModel = new AssignmentTask();
            EntityMapper<AssignmentTaskEdit, AssignmentTask>.Map(model, sourceModel);
            sourceModel.lstTaskFiles = new List<TaskFile>();
            if (taskFilesDetails != null)
            {
                sourceModel.lstTaskFiles = JsonConvert.DeserializeObject<List<TaskFile>>(taskFilesDetails);
            }
            var assignEdit = new AssignmentEdit();
            List<AssignmentTask> lstTasks;
            if (string.IsNullOrEmpty(taskDetails))
            {
                lstTasks = new List<AssignmentTask>();
                maxSortOrder = 1;
                sourceModel.SortOrder = 1;
                lstTasks.Add(sourceModel);
            }
            else
            {
                lstTasks = JsonConvert.DeserializeObject<List<AssignmentTask>>(taskDetails);
                if (lstTasks.Count > 0)
                {
                    maxSortOrder = lstTasks.Select(m => m.SortOrder).Max();
                    sourceModel.SortOrder = maxSortOrder + 1;
                    lstTasks = lstTasks.Where(m => m.RandomId != sourceModel.RandomId).ToList();
                }
                lstTasks.Add(sourceModel);
            }
            assignEdit.lstAssignmentTasks = lstTasks;
            //var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_TaskList", lstTasks);
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_TaskList", assignEdit);
            var contentList = htmlContent;// new string[] { htmlContent };
            var result = new Object();
            result = new
            {
                content = contentList,
                lstAssignmentTasks = lstTasks
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteTask(string randomId, string taskDetails)
        {
            List<AssignmentTask> lstTasks = new List<AssignmentTask>();
            List<AssignmentTask> lstUpdatedTask = new List<AssignmentTask>();
            var assignEdit = new AssignmentEdit();
            if (!string.IsNullOrEmpty(taskDetails))
            {
                lstTasks = JsonConvert.DeserializeObject<List<AssignmentTask>>(taskDetails);
            }
            foreach (var item in lstTasks)
            {
                if (item.RandomId == randomId)
                {
                    item.IsDeleted = true;
                }
            }

            lstUpdatedTask = lstTasks;
            assignEdit.lstAssignmentTasks = lstUpdatedTask;
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_TaskList", assignEdit);
            var contentList = htmlContent;// new string[] { htmlContent };
            var result = new Object();
            result = new
            {
                content = contentList,
                lstAssignmentTasks = lstUpdatedTask
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadGradingTemplate()
        {
            List<GradingTemplate> lstGradingtemplate = _gradingTemplateService.GetGradtingTemplates((int)SessionHelper.CurrentSession.SchoolId).ToList();
            return PartialView("_LoadGradingTemplate", lstGradingtemplate);
        }

        public ActionResult GetAssignmentStudentDetailsByAssgnmtId(string id, string GroupId = "")
        {
            var assignmentId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            var gId = 0;
            OperationDetails isAssignmentPermissions = new OperationDetails();
            if (GroupId != "")
            {
                gId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(GroupId));
            }
            Assignment objAssignment = _assignmentService.GetAssignmentById(assignmentId);
            isAssignmentPermissions = _assignmentService.GetAssignmentPermissions(assignmentId, SessionHelper.CurrentSession.Id);
            if ((objAssignment.CreatedById == SessionHelper.CurrentSession.Id) || SessionHelper.CurrentSession.IsAdmin || isAssignmentPermissions.Success == true)
            {
                objAssignment.lstAssignmentStudent = _assignmentService.GetStudentsByAssignmentId(assignmentId, gId).ToList();
                objAssignment.lstAssignmentFiles = _assignmentService.GetAssignmentFiles(assignmentId).ToList();
                foreach (var item in objAssignment.lstAssignmentFiles)
                {
                    item.CreatedOn = (DateTime)ConvertExtensions.ConvertUtcToLocalTime(item.CreatedOn);
                }
                Session["AssignmentFiles"] = objAssignment.lstAssignmentFiles;
                foreach (var item in objAssignment.lstAssignmentStudent)
                {
                    item.lstStudentAsgFiles = _assignmentService.GetStudentAssignmentFiles(item.StudentId, assignmentId).ToList();
                    if (item.CompletedOn != DateTime.MinValue)
                        item.CompletedOn = (DateTime)ConvertExtensions.ConvertUtcToLocalTime(item.CompletedOn);
                }
                List<AssignmentPeerReview> lstAssignmentPeers = new List<AssignmentPeerReview>();
                lstAssignmentPeers = _assignmentService.GetPeerMappingDetails(assignmentId).ToList();
                ViewBag.lstPeersAssignments = lstAssignmentPeers;
                objAssignment.IsQuizExist = _assignmentService.GetAssignmentTaskQuizByAssignmentId(assignmentId).Any();
                return View("AssignmentStudentDetailsPage", objAssignment);
            }
            else
            {
                return RedirectToAction("NoPermission", "Error");
            }

        }


        public ActionResult GetAllStudents(int assignmentId, int assignmentStudentId)
        {
            Assignment objAssignment = new Assignment();
            var assignmentStudentList = new List<Phoenix.Models.AssignmentStudent>();

            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                objAssignment.lstAssignmentStudent = _assignmentService.GetAssignmentCompletedStudentsByAssignmentId(assignmentId).Where(a => a.IsCompletedByStudent == true).ToList();
            }

            assignmentStudentList = objAssignment.lstAssignmentStudent;

            if (objAssignment.lstAssignmentStudent.Count() > 0)
            {
                ViewBag.assignmentStudentList = assignmentStudentList.Select(r => new SelectListItem { Text = r.StudentName, Value = EncryptDecryptHelper.EncryptUrl(r.AssignmentStudentId.ToString()) }).ToList();
                ViewBag.assignmentStudentId = assignmentStudentId;
            }
            else
            {
                ViewBag.assignmentStudentList = new List<SelectListItem>() { new SelectListItem { Text = "Select", Value = "0" } };
            }


            return PartialView("_StudentList", objAssignment);
        }

        [HttpPost]
        public async Task<ActionResult> UploadAssignmentFiles(HttpPostedFileBase[] postedFile, string lstAssignmentFiles)
        {
            List<AssignmentFile> fileNames = new List<AssignmentFile>();
            SharePointFileView shv = new SharePointFileView();
            if (lstAssignmentFiles != "")
            {
                fileNames = JsonConvert.DeserializeObject<List<AssignmentFile>>(lstAssignmentFiles);
            }
            try
            {
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    for (int i = 0; i < postedFile.Count(); i++)
                    {
                        HttpPostedFileBase file = postedFile[i];
                        shv = await azureHelper.UploadAssignmentFilesAsync(FileModulesConstants.AssignmentFile, SessionHelper.CurrentSession.OldUserId.ToString(), file, 5);
                        AddFileNameInList(fileNames, file, shv);
                    }
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    for (int i = 0; i < postedFile.Count(); i++)
                    {
                        HttpPostedFileBase file = postedFile[i];
                        var syncContext = SynchronizationContext.Current;
                        SynchronizationContext.SetSynchronizationContext(null);
                        shv = SharePointHelper.UploadAssignmentFiles(ref cx, FileModulesConstants.AssignmentFile, SessionHelper.CurrentSession.OldUserId.ToString(), file, 5);
                        SynchronizationContext.SetSynchronizationContext(syncContext);
                        AddFileNameInList(fileNames, file, shv);
                    }
                }
            }
            catch (Exception ex)
            {
                ILoggerClient _loggerClient = LoggerClient.Instance;
                _loggerClient.LogWarning("Assignment exception");
                _loggerClient.LogException(ex);
            }
            var result = new Object();
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_AssignmentFileList", fileNames);
            var contentList = new string[] { htmlContent };
            result = new
            {
                content = contentList,
                filenames = fileNames
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private static void AddFileNameInList(List<AssignmentFile> fileNames, HttpPostedFileBase file, SharePointFileView shv)
        {
            var extension = System.IO.Path.GetExtension(file.FileName);
            fileNames.Add(new AssignmentFile
            {
                FileName = file.FileName,
                FileExtension = extension.Replace(".", ""),
                UploadedFileName = shv.SharepointUploadedFileURL,
                ShareableLinkFileURL = shv.ShareableLink,
                ResourceFileTypeId = (short)ResourceFileTypes.SharePoint
            });
        }

        [HttpPost]
        public ActionResult GetUploadedFiles(List<AssignmentFile> uploadedFileDetails, string lstAssignmentFiles)
        {
            List<AssignmentFile> fileNames = new List<AssignmentFile>();
            string SharepointDomain = ConfigurationManager.AppSettings["SharepointDomain"];
            if (lstAssignmentFiles != "")
            {
                fileNames = JsonConvert.DeserializeObject<List<AssignmentFile>>(lstAssignmentFiles);
            }
            try
            {
                foreach (var file in uploadedFileDetails)
                {
                    var extension = System.IO.Path.GetExtension(file.FileName);
                    fileNames.Add(new AssignmentFile
                    {
                        FileName = file.FileName,
                        FileExtension = extension.Replace(".", ""),
                        UploadedFileName = SharepointDomain + file.UploadedFileName,
                        ShareableLinkFileURL = file.ShareableLinkFileURL,
                        ResourceFileTypeId = (short)ResourceFileTypes.SharePoint,
                        UploadedOn = DateTime.Now
                    });
                }
            }
            catch (Exception ex)
            {
                ILoggerClient _loggerClient = LoggerClient.Instance;
                _loggerClient.LogWarning("Assignment exception");
                _loggerClient.LogException(ex);
            }
            var result = new Object();
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_AssignmentFileList", fileNames);
            var contentList = new string[] { htmlContent };
            result = new
            {
                content = contentList,
                filenames = fileNames
            };


            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<ActionResult> UploadTaskFiles(HttpPostedFileBase[] dropAreaTask, string taskFilesDetails)
        {
            List<TaskFile> fileNames = new List<TaskFile>();
            if (taskFilesDetails != "" && taskFilesDetails != "null")
            {
                fileNames = JsonConvert.DeserializeObject<List<TaskFile>>(taskFilesDetails);
            }
            try
            {
                for (int i = 0; i < dropAreaTask.Count(); i++)
                {
                    HttpPostedFileBase file = dropAreaTask[i];
                    SharePointFileView shv = new SharePointFileView();
                    long oldUserId = SessionHelper.CurrentSession.OldUserId;
                    if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                    {
                        AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                        await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                        shv = await azureHelper.UploadAssignmentFilesAsync(FileModulesConstants.AssignmentTaskFile, oldUserId.ToString(), file);
                    }
                    else
                    {
                        SharePointHelper sh = new SharePointHelper();
                        Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                        shv = SharePointHelper.UploadAssignmentFiles(ref cx, FileModulesConstants.AssignmentTaskFile, oldUserId.ToString(), file);
                    }
                    var extension = System.IO.Path.GetExtension(file.FileName);
                    fileNames.Add(new TaskFile
                    {
                        FileName = file.FileName,
                        FileExtension = extension.Replace(".", ""),
                        UploadedFileName = shv.SharepointUploadedFileURL,
                        CreateOn = DateTime.Now,
                        IsSharePointFile = true,
                        ShareableLinkFileURL = shv.ShareableLink,
                        ResourceFileTypeId = (short)ResourceFileTypes.SharePoint
                    });
                }
            }
            catch (Exception ex)
            {
                ILoggerClient _loggerClient = LoggerClient.Instance;
                _loggerClient.LogWarning("UploadTaskFiles exception");
                _loggerClient.LogException(ex);
            }

            var result = new Object();
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_TaskFileList", fileNames.DistinctBy(x => x.UploadedFileName).ToList());
            var contentList = new string[] { htmlContent };
            result = new
            {
                content = contentList,
                filenames = fileNames
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }



        public ActionResult LoadGradingTemplateItems(int gradingtemplateId, int studAsgId, int gradingType)
        {
            var studentAssignment = _assignmentService.GetStudentAsgHomeworkDetailsById(studAsgId);
            List<GradingTemplateItem> lstGradingtemplate = _gradingTemplateItemService.GetGradingTemplateItems(gradingtemplateId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId).ToList();
            ViewBag.selectedGradeId = studentAssignment.GradingTemplateItemId;
            ViewBag.entityId = studAsgId;
            ViewBag.gradingType = gradingType;
            return PartialView("_LoadGradingTemplateItems", lstGradingtemplate);
        }

        public ActionResult UpdateStudentAssignmentGrade(int gradeId, int studentAssignmentId)
        {
            OperationDetails op = new OperationDetails();
            int GradedBy = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            op.Success = _assignmentService.AssignGradeToStudentAssignment(studentAssignmentId, gradeId, GradedBy);
            var gradingItem = _gradingTemplateItemService.GetGradingTemplateItemById(gradeId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);


            var studentAssignmentDetails = _assignmentService.GetStudentAssignmentDetailsById(studentAssignmentId);
            var student = _studentListService.GetStudentDetailsByStudentId(studentAssignmentDetails.StudentId);

            if (op.Success == true)
            {
                var sendEmailNotificationView = _assignmentService.GenerateEmailTemplateForUpdateAssignmentGrade(studentAssignmentDetails.AssignmentTitle, student.StudentInternalId);
                bool isSuccess = _commonService.SendEmailNotifications(sendEmailNotificationView);
            }

            return Json(gradingItem, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetuploadedFileDetails()
        {
            List<AssignmentFile> lstfiles = Session["AssignmentFiles"] as List<AssignmentFile>;
            //List<AssignmentFile> lstSavedFiles = _assignmentService.GetAssignmentFiles(id).ToList();
            //foreach (var item in lstSavedFiles)
            //{
            //    item.FileExtension = item.FileName.Split('.')[1];
            //}
            //  lstfiles.AddRange(lstSavedFiles);
            return PartialView("_AssignmentFileList", lstfiles);
        }

        public ActionResult DeleteFilesFromSession(string lstAssignmentFiles, int fileId = 0, int AssignmentId = 0, string fileName = "", string UploadedFilePath = "")
        {
            string relativepath = string.Empty;
            List<AssignmentFile> fileNames = new List<AssignmentFile>();
            if (lstAssignmentFiles != "")
            {
                fileNames = JsonConvert.DeserializeObject<List<AssignmentFile>>(lstAssignmentFiles);
            }
            if (fileId == 0)
            {
                var file = fileNames.FirstOrDefault(e => e.FileName.ToUpper().Trim() == fileName.ToUpper().Trim());
                if (file != null)
                {
                    fileNames.Remove(file);
                    if (System.IO.File.Exists(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath))
                    {
                        System.IO.File.Delete(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath);
                    }
                }
            }
            else
            {
                long UserId = SessionHelper.CurrentSession.Id;
                var isActionPerformed = _assignmentService.DeleteUploadedFiles(fileId, AssignmentId, UserId);
                if (isActionPerformed)
                {
                    var file = fileNames.FirstOrDefault(e => e.AssignmentFileId == fileId);
                    fileNames.Remove(file);
                }
            }
            // return PartialView("_AssignmentFileList", lstfiles);
            var result = new Object();
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_AssignmentFileList", fileNames);
            var contentList = new string[] { htmlContent };
            result = new
            {
                content = contentList,
                filenames = fileNames
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteStudentAssignmentFile(int fileId = 0, int assignmentId = 0, int studentId = 0)
        {
            long UserId = SessionHelper.CurrentSession.Id;
            var isActionPerformed = _assignmentService.DeleteStudentAssignmentFile(fileId, UserId, false);
            return Json(new OperationDetails(isActionPerformed), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteStudentAssignmentTaskFile(int fileId = 0, int assignmentId = 0, int studentId = 0)
        {
            long UserId = SessionHelper.CurrentSession.Id;
            var isActionPerformed = _assignmentService.DeleteStudentAssignmentFile(fileId, UserId, true);
            return Json(new OperationDetails(isActionPerformed), JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentHomeworkAddEdit(string id, int IsSeenByStudent = 1)
        {
            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;
            var oldUsersID = SessionHelper.CurrentSession.OldUserId;
            ViewBag.oldUsersID = oldUsersID;
            var SchoolCode = SessionHelper.CurrentSession.SchoolCode;
            ViewBag.SchoolCode = SchoolCode;
            var assignmentStudentId = string.IsNullOrEmpty(id) ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            if (assignmentStudentId > 0)
            {
                AssignmentStudent objAssgnmentStudent = new AssignmentStudent();

                objAssgnmentStudent = _assignmentService.GetStudentAsgHomeworkDetailsById(assignmentStudentId, IsSeenByStudent);
                Assignment asg = new Assignment();
                asg = _assignmentService.GetAssignmentById(objAssgnmentStudent.AssignmentId);
                if (asg.DeletedBy != 0)
                {
                    return View("DeletedAssignment");
                }
                objAssgnmentStudent.objAssignment = asg;
                int studentId = 0;
                if (SessionHelper.CurrentSession.IsParent())
                {
                    studentId = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentId;
                }
                else
                {
                    studentId = _studentService.GetStudentByUserId(SessionHelper.CurrentSession.Id).StudentId;
                }

                if (objAssgnmentStudent.StudentId != studentId)
                {
                    return RedirectToAction("NoPermission", "Error");
                }
                List<AssignmentPeerReview> lstAssignmentPeers = new List<AssignmentPeerReview>();
                lstAssignmentPeers = _assignmentService.GetPeerAssignmentDetails(objAssgnmentStudent.AssignmentId, SessionHelper.CurrentSession.Id).ToList();
                ViewBag.lstPeersAssignments = lstAssignmentPeers;

                Session["StudentAsgUploadedFiles"] = "";
                objAssgnmentStudent.objAssignment.lstAssignmentTasks = _assignmentService.GetAssignmentTasks(objAssgnmentStudent.AssignmentId).ToList();
                foreach (var item in objAssgnmentStudent.objAssignment.lstAssignmentTasks)
                {
                    item.lstTaskFiles = _assignmentService.GetTaskFilesByTaskId(item.TaskId).ToList();
                }
                objAssgnmentStudent.objAssignment.lstAssignmentFiles = _assignmentService.GetAssignmentFiles(objAssgnmentStudent.AssignmentId).ToList();
                //model.lstDocuments = _assignmentService.GetAssignmentFiles(assignmentId.Value).ToList();
                if (objAssgnmentStudent.objAssignment.lstAssignmentFiles != null && objAssgnmentStudent.objAssignment.lstAssignmentFiles.Count > 0)
                {
                    objAssgnmentStudent.objAssignment.lstAssignmentFiles.ForEach(e =>
                    {
                        e.IsCloudFile = e.ResourceFileTypeId == (short)ResourceFileTypes.GoogleDrive || e.ResourceFileTypeId == (short)ResourceFileTypes.OneDrive || e.ResourceFileTypeId == (short)ResourceFileTypes.OneNote;
                        e.UploadedFileName = string.IsNullOrEmpty(e.UploadedFileName) ? e.ShareableLinkFileURL : e.UploadedFileName;
                    });
                }
                ViewBag.lstStudentAssignmentFiles = JsonConvert.SerializeObject(objAssgnmentStudent.objAssignment.lstAssignmentFiles);

                if (objAssgnmentStudent.objAssignment.lstAssignmentFiles != null)
                {
                    foreach (var item in objAssgnmentStudent.objAssignment.lstAssignmentFiles)
                    {
                        item.FileExtension = item.FileName.Split('.').Length > 1 ? item.FileName.Split('.')[1] : string.Empty;
                        item.CreatedOn = (DateTime)ConvertExtensions.ConvertUtcToLocalTime(item.CreatedOn);
                    }
                }
                List<StudentTaskFile> lstStudentTaskFile = new List<StudentTaskFile>();
                objAssgnmentStudent.lstStudentAsgFiles = _assignmentService.GetStudentAssignmentFiles(objAssgnmentStudent.StudentId, objAssgnmentStudent.AssignmentId).ToList();
                foreach (var task in objAssgnmentStudent.objAssignment.lstAssignmentTasks.ToList())
                {
                    foreach (var taskdetails in _assignmentService.GetStudentTaskFiles(objAssgnmentStudent.StudentId, task.TaskId).ToList())
                    {
                        lstStudentTaskFile.Add(taskdetails);

                    }
                    task.objStudentTask = _assignmentService.GetStudentTaskDetails(task.TaskId, objAssgnmentStudent.StudentId);
                    task.objStudentTask.objQuizResponse = new QuizResponse();
                    if (task.QuizId.HasValue)
                    {
                        task.objStudentTask.objQuizResponse = _quizQuestionsService.GetQuizResponseByUserId(task.QuizId.Value, (int)SessionHelper.CurrentSession.Id, objAssgnmentStudent.StudentId, task.TaskId);
                        var quizEdit = new QuizEdit();
                        quizEdit = _quizService.GetQuiz(task.QuizId.Value);
                        task.objStudentTask.objQuizView = new QuizView();
                        EntityMapper<QuizEdit, QuizView>.Map(quizEdit, task.objStudentTask.objQuizView);
                    }
                }
                ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
                ViewBag.StudentTaskFiles = lstStudentTaskFile;
                ViewBag.IsCompletedByStudent = objAssgnmentStudent.IsCompletedByStudent;
                AssignmentPeerReview objPeerReview = new AssignmentPeerReview();
                if (objAssgnmentStudent.objAssignment.IsPeerMarkingEnable)
                {
                    objPeerReview = _assignmentService.GetPeerAssignmentDetailByAssignmentStudentId(assignmentStudentId);
                }
                ViewBag.PeerDetails = objPeerReview;
                Session["IsCompletedMarkByStudent"] = objAssgnmentStudent.IsCompletedByStudent;
                Session["DownloadReviewVisible"] = objAssgnmentStudent.objAssignment.IsPeerMarkingEnable && objPeerReview.IsReviewCompleted;
                return View(objAssgnmentStudent);
            }
            else
            {
                return Redirect("~/Home");
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> UploadStudentAssignmentFiles(int assignmentId, int studentId)
        {
            List<StudentAssignmentFile> fileNames = new List<StudentAssignmentFile>();
            Session["StudentAsgUploadedFiles"] = null;
            OperationDetails op = new OperationDetails();
            string relativePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Get all files from Request object
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        SharePointFileView shv = new SharePointFileView();
                        if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                        {
                            AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                            await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                            shv = await azureHelper.UploadAssignmentFilesAsync(FileModulesConstants.StudentAssignmentFile, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                        }
                        else
                        {
                            SharePointHelper sh = new SharePointHelper();
                            Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                            var syncContext = SynchronizationContext.Current;
                            SynchronizationContext.SetSynchronizationContext(null);
                            shv = SharePointHelper.UploadAssignmentFiles(ref cx, FileModulesConstants.StudentAssignmentFile, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                            SynchronizationContext.SetSynchronizationContext(syncContext);
                        }
                        var extension = System.IO.Path.GetExtension(file.FileName);
                        fileNames.Add(new StudentAssignmentFile
                        {
                            FileName = file.FileName,
                            FileExtension = extension,
                            AssignmentId = assignmentId,
                            UploadedFileName = shv.UploadedFileName,
                            PathtoDownload = shv.ShareableLink,
                            SharepointUploadedFileURL = shv.SharepointUploadedFileURL,
                            UploadedOn = DateTime.Now,
                            ResourceFileTypeId = (int)ResourceFileTypes.SharePoint
                        });
                    }
                }
                catch (Exception ex)
                {
                }
                Session["StudentAsgUploadedFiles"] = fileNames;
            }
            if (fileNames.Count > 0)
            {
                var visitorInfo = new VisitorInfo();
                string IpAddress = visitorInfo.GetIpAddress();
                string HostIpAddress = visitorInfo.GetClientIPAddress();
                string IpDetails = IpAddress + ":" + HostIpAddress;
                op.Success = _assignmentService.UploadStudentAssignmentFile(fileNames, studentId, assignmentId, IpDetails);
                if (op.Success)
                {
                    op.Message = ResourceManager.GetString("Shared.Messages.FileUploaded");
                }
                else
                {
                    op.Message = ResourceManager.GetString("Shared.Messages.ErrorUploadingFiles");
                }
            }
            return Json(op, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetUploadedStudentFile(int assignmentId, int studentId)
        {
            List<StudentAssignmentFile> lstfiles = _assignmentService.GetStudentAssignmentFiles(studentId, assignmentId).ToList();

            return PartialView("_StudentAssignmentFiles", lstfiles);
        }

        [HttpPost]
        public ActionResult UploadStudentFiles(string lstStudentFiles, int assignmentId, int studentId)
        {
            List<StudentAssignmentFile> lstStudTasks = JsonConvert.DeserializeObject<List<StudentAssignmentFile>>(lstStudentFiles);
            List<StudentAssignmentFile> lstfiles = _assignmentService.GetStudentAssignmentFiles(studentId, assignmentId).ToList();
            string SharepointDomain = ConfigurationManager.AppSettings["SharepointDomain"];
            var visitorInfo = new VisitorInfo();
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();
            string IpDetails = IpAddress + ":" + HostIpAddress;
            OperationDetails op = new OperationDetails();
            foreach (var file in lstStudTasks)
            {
                var extension = System.IO.Path.GetExtension(file.FileName);
                file.FileExtension = extension;
                file.AssignmentId = assignmentId;
                file.PathtoDownload = SharepointDomain + file.PathtoDownload;
                file.ResourceFileTypeId = (int)ResourceFileTypes.SharePoint;
                file.UploadedOn = DateTime.Now;
                file.ResourceFileTypeId = (int)ResourceFileTypes.SharePoint;
            }
            op.Success = _assignmentService.UploadStudentAssignmentFile(lstStudTasks, studentId, assignmentId, IpDetails);

            foreach (var file in lstStudTasks)
            {
                var extension = System.IO.Path.GetExtension(file.FileName);
                lstfiles.Add(new StudentAssignmentFile
                {
                    FileName = file.FileName,
                    FileExtension = extension,
                    AssignmentId = assignmentId,
                    UploadedFileName = SharepointDomain + file.UploadedFileName,
                    PathtoDownload = SharepointDomain + file.PathtoDownload,
                    SharepointUploadedFileURL = file.SharepointUploadedFileURL,
                    UploadedOn = DateTime.Now,
                    ResourceFileTypeId = (int)ResourceFileTypes.SharePoint
                });
            }
            return PartialView("_StudentAssignmentFiles", lstfiles);
        }

        public JsonResult MarkAsComplete(int AssignmentStudentId)
        {
            bool isTeacher = (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin);
            int CompletedBy = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            bool bFlag = _assignmentService.MarkAsComplete(AssignmentStudentId, isTeacher, CompletedBy);

            var studentAssignmentDetails = _assignmentService.GetStudentAssignmentDetailsById(AssignmentStudentId);

            var student = _studentListService.GetStudentDetailsByStudentId(studentAssignmentDetails.StudentId);
            if (bFlag == true && isTeacher == true)
            {
                var sendEmailNotificationView = _assignmentService.GenerateEmailTemplateForAssignmentMarkAsComplete(studentAssignmentDetails.AssignmentTitle, student.StudentInternalId);
                bool isSuccess = _commonService.SendEmailNotifications(sendEmailNotificationView);
            }

            return Json(bFlag, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SubmitAssignmentMarks(int AssignmentStudentId, decimal submitMarks, int gradingTemplateId)
        {
            int CompletedBy = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            var gradeItem = _assignmentService.SubmitAssignmentMarks(AssignmentStudentId, submitMarks, CompletedBy, gradingTemplateId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
            return Json(gradeItem, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TeacherStudentAssignment(string id)
        {
            var assignmentStuddentId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            AssignmentStudent objAssgnmentStudent = new AssignmentStudent();
            OperationDetails isAssignmentPermissions = new OperationDetails();
            int CompletedBy = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            objAssgnmentStudent = _assignmentService.GetStudentAsgHomeworkDetailsById(assignmentStuddentId);
            objAssgnmentStudent.objAssignment = _assignmentService.GetAssignmentById(objAssgnmentStudent.AssignmentId);
            isAssignmentPermissions = _assignmentService.GetAssignmentPermissions(objAssgnmentStudent.AssignmentId, SessionHelper.CurrentSession.Id);
            if (objAssgnmentStudent.objAssignment.GradingTemplateId != 0)
                objAssgnmentStudent.objGradingTemplateItem = _assignmentService.SubmitAssignmentMarks(assignmentStuddentId, objAssgnmentStudent.AssignmentSumbitMarks, CompletedBy, objAssgnmentStudent.objAssignment.GradingTemplateId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
            if ((objAssgnmentStudent.objAssignment.CreatedById == SessionHelper.CurrentSession.Id) || SessionHelper.CurrentSession.IsAdmin || isAssignmentPermissions.Success == true)
            {
                Session["StudentAsgUploadedFiles"] = "";
                objAssgnmentStudent.objAssignment.lstAssignmentTasks = _assignmentService.GetAssignmentTasks(objAssgnmentStudent.AssignmentId).ToList();
                objAssgnmentStudent.objAssignment.lstAssignmentFiles = _assignmentService.GetAssignmentFiles(objAssgnmentStudent.AssignmentId).ToList();

                if (objAssgnmentStudent.objAssignment.lstAssignmentTasks != null)
                {
                    foreach (var item in objAssgnmentStudent.objAssignment.lstAssignmentTasks)
                    {
                        item.objStudentTask = _assignmentService.GetStudentTaskDetails(item.TaskId, objAssgnmentStudent.StudentId);
                    }
                }
                if (objAssgnmentStudent.objAssignment.lstAssignmentFiles != null)
                {
                    foreach (var item in objAssgnmentStudent.objAssignment.lstAssignmentFiles)
                    {
                        item.CreatedOn = (DateTime)ConvertExtensions.ConvertUtcToLocalTime(item.CreatedOn);
                    }
                }
                List<StudentAssignmentFile> lstfiles = _assignmentService.GetStudentAssignmentFiles(objAssgnmentStudent.StudentId, objAssgnmentStudent.AssignmentId).ToList();
                if (lstfiles.Count > 0)
                {
                    foreach (var item in lstfiles)
                    {
                        item.UploadedOn = (DateTime)ConvertExtensions.ConvertUtcToLocalTime(item.UploadedOn);
                    }
                }
                objAssgnmentStudent.lstStudentAsgFiles = lstfiles;
                objAssgnmentStudent.lstStudentObjective = _assignmentService.GetStudentAssignmentObjectives(assignmentStuddentId).ToList();
                ViewBag.FileTypes = _fileTypeList;
                return View(objAssgnmentStudent);
            }
            else
            {
                return RedirectToAction("NoPermission", "Error");
            }

        }
        [HttpPost]
        public ActionResult UploadAssignmentTaskFeedbackFiles()
        {

            List<AssignmentFeedbackFiles> fileNames = new List<AssignmentFeedbackFiles>();
            string relativePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Get all files from Request object
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;

                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        var extension = System.IO.Path.GetExtension(file.FileName);


                        //content for file upload
                        string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                        string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFeedbackDir;
                        string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFeedbackDir;

                        Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                        Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);

                        Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                        fname = Guid.NewGuid() + "_" + fname;
                        relativePath = Constants.AssignmentFeedbackDir + "/" + fname;
                        fname = Path.Combine(filePath, fname);
                        file.SaveAs(fname);
                        fileNames.Add(new AssignmentFeedbackFiles
                        {
                            FileName = file.FileName,
                            FileExtension = extension.Replace(".", ""),
                            UploadedFileName = relativePath
                        });
                    }
                }
                catch (Exception ex)
                {
                }
                if (Session["FeedbackFiles"] == null)
                {
                    Session["FeedbackFiles"] = fileNames;
                }
                else
                {
                    List<AssignmentFeedbackFiles> lstExistingFiles = new List<AssignmentFeedbackFiles>();
                    lstExistingFiles = Session["FeedbackFiles"] as List<AssignmentFeedbackFiles>;
                    lstExistingFiles.AddRange(fileNames);
                    Session["FeedbackFiles"] = lstExistingFiles;
                }
            }
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAssignmentFeedbackFileDetails()
        {

            List<AssignmentFeedbackFiles> lstfiles = Session["FeedbackFiles"] as List<AssignmentFeedbackFiles>;
            return PartialView("_AssignmentTaskFeedbackFileList", lstfiles);
        }
        [HttpPost]
        public ActionResult DeleteAssignmentTaskFile(string id, string quizQuestionId, string fileName, string UploadedFilePath)
        {
            string relativepath = string.Empty;
            List<AssignmentFeedbackFiles> lstfiles = new List<AssignmentFeedbackFiles>();
            if (id == "0")
            {
                lstfiles = Session["FeedbackFiles"] as List<AssignmentFeedbackFiles>;
                var file = lstfiles.FirstOrDefault(e => e.FileName.ToUpper().Trim() == fileName.ToUpper().Trim());
                if (file != null)
                {
                    lstfiles.Remove(file);
                    if (System.IO.File.Exists(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath))
                    {
                        System.IO.File.Delete(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath);
                    }
                    Session["FeedbackFiles"] = lstfiles;
                }
            }
            return PartialView("_AssignmentTaskFeedbackFileList", lstfiles);
        }
        public ActionResult DownloadFile(int studentId = 0, int assignmentId = 0, int FId = 0, string Type = "", int taskId = 0, string fileName = "")
        {
            //var studId = string.IsNullOrEmpty(studentId) ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(studentId));
            //var taskID = string.IsNullOrEmpty(taskId) ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(taskId));
            //var assignmentID = string.IsNullOrEmpty(assignmentId) ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(assignmentId));
            //var fID = string.IsNullOrEmpty(FId) ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(FId));
            switch (Type)
            {
                case "Student Assignment":
                    List<StudentAssignmentFile> lstfiles = _assignmentService.GetStudentAssignmentFiles(studentId, assignmentId).ToList();
                    if (FId != 0)
                    {
                        var FileDetails = lstfiles.Where(e => e.StudentAsgFileId == FId).FirstOrDefault();

                        if (FileDetails != null && !string.IsNullOrWhiteSpace(FileDetails.FileName))
                        {
                            string path = PhoenixConfiguration.Instance.ReadFilePath + FileDetails.UploadedFileName;
                            //WebClient myWebClient = new WebClient();
                            //byte[] myDataBuffer = myWebClient.DownloadData(path);
                            //Response.BufferOutput = true;
                            //return new RedirectResult(path,false);
                            return Redirect(path);
                            //return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, FileDetails.FileName);
                        }
                    }

                    //return null;
                    break;

                case "Student TaskFile":

                    var FileList = _assignmentService.GetStudentTaskFiles(studentId, taskId).ToList();
                    if (FileList.Count > 0)
                    {
                        var filedetails = FileList.Where(e => e.StudentTaskFileId == FId).FirstOrDefault();
                        if (filedetails != null && !string.IsNullOrWhiteSpace(filedetails.FileName))
                        {
                            string path = PhoenixConfiguration.Instance.ReadFilePath + filedetails.UploadedFileName;
                            //WebClient myWebClient = new WebClient();
                            //byte[] myDataBuffer = myWebClient.DownloadData(path);
                            //Response.BufferOutput = true;
                            return Redirect(path);
                            // return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, filedetails.FileName);
                        }
                    }

                    break;
                case "Student ReviewedFile":
                    DocumentReviewdetails objDoc = _assignmentService.GetDocumentFileByStudAsgFileId(FId);
                    if (objDoc != null)
                    {
                        string path = PhoenixConfiguration.Instance.ReadFilePath + objDoc.ReviewedFileName;
                        //WebClient myWebClient = new WebClient();
                        //byte[] myDataBuffer = myWebClient.DownloadData(path);
                        //Response.BufferOutput = true;
                        return Redirect(path);
                        //return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet,Path.GetFileName(objDoc.ReviewedFileName));
                    }
                    break;
                case "Student ReviewedTaskFile":
                    TaskDocumentReviewdetails objStudentTaskDoc = _assignmentService.GetTaskDocumentFileByStudTaskFileId(FId);
                    if (objStudentTaskDoc != null)
                    {
                        string path = PhoenixConfiguration.Instance.ReadFilePath + objStudentTaskDoc.ReviewedFileName;
                        //WebClient myWebClient = new WebClient();
                        //byte[] myDataBuffer = myWebClient.DownloadData(path);
                        //Response.BufferOutput = true;
                        //return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(objStudentTaskDoc.ReviewedFileName));
                        return Redirect(path);
                    }
                    break;
                case "Teacher Assignment":
                    var teacherAssignment = _assignmentService.GetAssignmentFiles(assignmentId).ToList();
                    var Filedetails = teacherAssignment.Where(e => e.AssignmentFileId == FId).FirstOrDefault();

                    if (Filedetails != null && !string.IsNullOrWhiteSpace(Filedetails.FileName))
                    {

                        string path = PhoenixConfiguration.Instance.ReadFilePath + Filedetails.UploadedFileName;
                        //WebClient myWebClient = new WebClient();
                        //byte[] myDataBuffer = myWebClient.DownloadData(path);
                        //Response.BufferOutput = true;
                        return Redirect(path);
                        //return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, Filedetails.FileName);
                    }
                    else
                    {
                        List<AssignmentFile> lstAssignmentFiles = new List<AssignmentFile>();
                        lstAssignmentFiles = Session["AssignmentFiles"] as List<AssignmentFile>;
                        foreach (var item in lstAssignmentFiles)
                        {
                            if (item.UploadedFileName == fileName)
                            {
                                string path = PhoenixConfiguration.Instance.ReadFilePath + item.UploadedFileName;
                                //WebClient myWebClient = new WebClient();
                                //byte[] myDataBuffer = myWebClient.DownloadData(path);
                                //Response.BufferOutput = true;

                                //return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, item.FileName);
                                return Redirect(path);
                            }
                        }
                    }

                    break;

                case "Teacher TaskFile":

                    var teachertasklist = _assignmentService.GetTaskFilesByTaskId(taskId).ToList();
                    var fdetails = teachertasklist.Where(e => e.TaskFileId == FId).FirstOrDefault();
                    if (fdetails != null && !string.IsNullOrWhiteSpace(fdetails.FileName))
                    {
                        string path = PhoenixConfiguration.Instance.ReadFilePath + fdetails.UploadedFileName;
                        //WebClient myWebClient = new WebClient();
                        //byte[] myDataBuffer = myWebClient.DownloadData(path);
                        //Response.BufferOutput = true;

                        //return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, fdetails.FileName);
                        return Redirect(path);
                    }


                    break;

                default:
                    return null;

            }
            //List<StudentAssignmentFile> lstfiles = _assignmentService.GetStudentAssignmentFiles(studentId, assignmentId).ToList();
            //if (FId != 0)
            //{
            //    var FileDetails = lstfiles.Where(e => e.StudentAsgFileId == FId).FirstOrDefault();

            //    if (FileDetails != null && !string.IsNullOrWhiteSpace(FileDetails.FileName))
            //    {
            //        var path = PhoenixConfiguration.Instance.ReadFilePath + FileDetails.UploadedFileName;
            //        byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            //        return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, FileDetails.FileName);
            //    }
            //}

            //return null;

            return null;
        }
        public ActionResult GetGradingTemplateById(int id)
        {
            var gradingTemplate = _gradingTemplateService.GetGradingTemplateById(id);
            return Json(gradingTemplate, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MarkAsIncompleteAssignment(int studentId, int assignmentId)
        {
            OperationDetails op = new OperationDetails();
            bool isReSubmitAssignnment = false;
            op.Success = _assignmentService.MarkAsIncompleteAssignment(studentId, assignmentId, isReSubmitAssignnment);
            if (op.Success)
            {
                var lstStudents = _studentListService.GetStudentDetailsByIds(studentId.ToString());
                var assignment = _assignmentService.GetAssignmentById(assignmentId);
                //string toEmailAddrees = "";
                //foreach (var item in lstStudents)
                //{
                //    toEmailAddrees = toEmailAddrees + (toEmailAddrees == "" ? "" : ";") + item.Email;
                //}
                ////  toEmailAddrees = toEmailAddrees.Remove(toEmailAddrees.Length - 1);
                //string EmailBody = "";

                //StreamReader reader = new StreamReader(Server.MapPath(Constants.AssignmentNotificationTemplate), Encoding.UTF8);

                //EmailBody = reader.ReadToEnd();
                //EmailBody = EmailBody.Replace("@@studentname", SessionHelper.CurrentSession.FullName);
                //EmailBody = EmailBody.Replace("@@message", assignment.AssignmentDesc);

                //var operationalDetails = EmailHelper.SendEmail(toEmailAddrees, Constants.AssignmentNotification, EmailBody);
            }

            return Json(op, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///Author : Mahesh Chikhale
        ///Created Date : 29-July-2019
        ///Description : To open modal popup for curriculm
        /// </summary>
        /// <param name="subjectId">subjectId</param>       
        /// <returns></returns>
        public ActionResult loadTopicSubtopic(int subjectId)
        {
            ViewBag.SubjectId = subjectId;
            List<SelectListItem> lessonsList = new List<SelectListItem>();
            ViewBag.Lessons = lessonsList;
            List<Objective> lstselectedObjective = Session["lstObjective"] as List<Objective>;
            var TopicList = _subjectService.GetTopicSubTopicStructure(subjectId).ToList();
            List<TopicTreeItem> treeList = new List<TopicTreeItem>();
            TopicList.ForEach(l => treeList.Add(new TopicTreeItem() { id = l.SubSyllabusId, parentId = l.ParentId, name = l.SubSyllabusDescription, isLesson = l.isLesson }));
            ViewBag.TopicList = TopicList;
            return PartialView("_TopicSubtopicStructure", lstselectedObjective);
        }

        public ActionResult GetSubtopicObjectives(string id, string courseIds, bool isLesson)
        {

            List<Objective> lstObjective = new List<Objective>();
            bool isMainSyllabus = false;
            if (id.Contains('x'))
            {
                isMainSyllabus = true;
            }
            int topicId = Convert.ToInt32(id.Replace("x", ""));
            lstObjective = _subjectService.GetSubtopicObjectivesWithMultipleSubjects(topicId, isMainSyllabus, courseIds, isLesson).ToList();
            List<Objective> lstSelectedObjective = new List<Objective>();
            if (Session["lstObjective"] == null)
            {
                Session["lstObjective"] = lstObjective;
                lstSelectedObjective = lstObjective;
            }
            else
            {
                lstSelectedObjective = Session["lstObjective"] as List<Objective>;
                List<Objective> tempList = new List<Objective>();
                foreach (var item in lstObjective)
                {
                    Objective obj = lstSelectedObjective.Where(m => m.ObjectiveId == item.ObjectiveId).FirstOrDefault();
                    if (obj == null)
                    {
                        lstSelectedObjective.Add(item);
                    }
                }
                // lstSelectedObjective.AddRange(lstObjective);
                Session["lstObjective"] = lstSelectedObjective;

            }
            return PartialView("_LoadObjectives", lstSelectedObjective);
        }

        public ActionResult UpdateObjectives(List<Objective> mappingDetails)
        {
            var operationDetails = new OperationDetails();
            List<Objective> lstObjectives = new List<Objective>();
            if (Session["lstObjective"] == null)
            {
                Session["lstObjective"] = mappingDetails;
            }
            else
            {
                Objective obj = mappingDetails.FirstOrDefault();
                lstObjectives = Session["lstObjective"] as List<Objective>;
                lstObjectives = lstObjectives.Where(m => m.ObjectiveId != obj.ObjectiveId).ToList();
                //lstObjectives.AddRange(mappingDetails);
                Session["lstObjective"] = lstObjectives;
            }
            operationDetails.Success = false;
            operationDetails.Message = "";
            return PartialView("_LoadObjectives", lstObjectives);
        }

        public ActionResult DisselectAllObjectives()
        {
            var operationDetails = new OperationDetails();
            var lstObjectives = new List<Objective>();
            Session["lstObjective"] = lstObjectives;
            operationDetails.Success = false;
            operationDetails.Message = "";
            return PartialView("_LoadObjectives", lstObjectives);
        }
        public ActionResult DeleteObjective(int objectiveId)
        {
            var operationDetails = new OperationDetails();
            List<Objective> lstobjectives = Session["lstObjective"] as List<Objective>;
            lstobjectives = lstobjectives.Where(m => m.ObjectiveId != objectiveId).ToList();
            Session["lstObjective"] = lstobjectives;
            operationDetails.Success = true; ;
            return PartialView("_ShowSelectedObjective", lstobjectives);
        }
        #region StudentAssignmentTaskDetails

        public ActionResult GetAssignmentTaskDetails(string id, string taskId, string studentId)
        {
            var quizDetails = new QuizEdit();
            var studId = string.IsNullOrEmpty(studentId) ? (int)SessionHelper.CurrentSession.Id : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(studentId));
            var taskID = string.IsNullOrEmpty(taskId) ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(taskId));
            var Assignmttask = _assignmentService.GetAssignmentTaskbyTaskId(taskID);
            var studentTaskDetails = _assignmentService.GetStudentTaskDetails(taskID, studId);

            Assignmttask.lstTaskFiles = _assignmentService.GetTaskFilesByTaskId(taskID).ToList();
            studentTaskDetails.lstStudentTaskFile = _assignmentService.GetStudentTaskFiles(studId, taskID).ToList();

            ViewBag.StudentTaskDetails = studentTaskDetails;

            //TimeStamp
            if (Assignmttask.lstTaskFiles != null)
            {
                foreach (var item in Assignmttask.lstTaskFiles)
                {
                    item.CreateOn = (DateTime)ConvertExtensions.ConvertUtcToLocalTime(item.CreateOn);
                }
            }

            if (studentTaskDetails.lstStudentTaskFile.Count > 0)
            {
                foreach (var item in studentTaskDetails.lstStudentTaskFile)
                {
                    item.CreateOn = (DateTime)ConvertExtensions.ConvertUtcToLocalTime(item.CreateOn);
                }
            }

            //Quiz DATA
            IEnumerable<QuizQuestionsEdit> questionList = new List<QuizQuestionsEdit>();
            QuizResponse quizResponse = new QuizResponse();
            if (Assignmttask.QuizId.HasValue)
            {
                questionList = _quizQuestionsService.GetQuizQuestionsByQuizId(Assignmttask.QuizId.Value);
                if (questionList.Any())
                {
                    foreach (var item in questionList)
                    {
                        item.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(item.QuizQuestionId);
                    }
                }
                var quizFeedback = _assignmentService.GetStudentTaskDetailById(taskID, studId, studentTaskDetails.StudentAssignmentId, Convert.ToInt64(studentTaskDetails.StudentTaskId));
                quizResponse = _quizQuestionsService.GetQuizResponseByUserId(Assignmttask.QuizId.Value, (int)SessionHelper.CurrentSession.Id, studId, taskID);
                quizResponse.StudentId = studId;
                if (quizFeedback != null)
                {
                    quizResponse.Feedback = quizFeedback.Feedback;
                    quizResponse.QuizFeedbacks = quizFeedback.QuizFeedbacks;
                    quizResponse.FeedbackFrom = quizFeedback.FeedbackFrom;
                }
                quizDetails = _quizService.GetTaskQuizDetail(Assignmttask.QuizId.Value, studentTaskDetails.TaskId);
                quizDetails.lstDocuments = _quizService.GetFilesByQuizId(Assignmttask.QuizId.Value);
            }
            foreach (var question in questionList)
            {
                var questionDetails = _quizQuestionsService.GetQuizQuestionById(question.QuizQuestionId);
                question.Answers = questionDetails.Answers;
            }
            ViewBag.Quiz = quizDetails;
            ViewBag.QuizDetails = questionList;
            ViewBag.QuizResponse = quizResponse;
            ViewBag.studentId = studentId;
            ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            return View("_AssignmentTaskDetails", Assignmttask);
        }

        public ActionResult GetStudentTaskDetails(string taskId, string studentId)
        {
            var taskID = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(taskId));
            var studentID = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(studentId));
            var studentTaskDetails = _assignmentService.GetStudentTaskDetails(taskID, studentID);
            studentTaskDetails.lstStudentTaskFile = _assignmentService.GetStudentTaskFiles(studentID, taskID).ToList();

            ViewBag.SharepointToken = SharepointTokenHelper.Instance.Token;
            int selectedStudentId = 0;
            if (SessionHelper.CurrentSession.IsParent())
            {
                selectedStudentId = SessionHelper.CurrentSession.CurrentSelectedStudent.StudentId;
            }
            else
            {
                selectedStudentId = _studentService.GetStudentByUserId(SessionHelper.CurrentSession.Id).StudentId;
            }
            if (studentTaskDetails.lstStudentTaskFile.Count > 0)
            {
                foreach (var item in studentTaskDetails.lstStudentTaskFile)
                {
                    item.CreateOn = (DateTime)ConvertExtensions.ConvertUtcToLocalTime(item.CreateOn);
                }
            }

            if (studentID != selectedStudentId)
            {
                return RedirectToAction("NoPermission", "Error");
            }
            var assignmentTask = _assignmentService.GetAssignmentTaskbyTaskId(taskID);
            assignmentTask.lstTaskFiles = _assignmentService.GetTaskFilesByTaskId(taskID).ToList();
            ViewBag.assignmentTask = assignmentTask;
            //Quiz DATA
            IEnumerable<QuizQuestionsEdit> questionList = new List<QuizQuestionsEdit>();
            QuizResponse quizResponse = new QuizResponse();
            QuizEdit quizDetails = new QuizEdit();
            if (assignmentTask.QuizId.HasValue)
            {
                questionList = _quizQuestionsService.GetQuizQuestionsByQuizId(assignmentTask.QuizId.Value);
                if (questionList.Any())
                {
                    foreach (var item in questionList)
                    {
                        item.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(item.QuizQuestionId);
                    }
                }
                quizDetails = _quizService.GetTaskQuizDetail(assignmentTask.QuizId.Value, studentTaskDetails.TaskId);
                quizDetails.lstDocuments = _quizService.GetFilesByQuizId(assignmentTask.QuizId.Value);
                quizResponse = _quizQuestionsService.GetQuizResponseByUserId(assignmentTask.QuizId.Value, (int)SessionHelper.CurrentSession.Id, studentID, taskID);
                if (!quizResponse.ResponseQuestionAnswer.Any() && SessionHelper.CurrentSession.IsStudent())
                {
                    GroupQuizService _groupQuizService = new GroupQuizService();
                    var logResult = _groupQuizService.GetLogResultQuestionAnswer(assignmentTask.QuizId.Value, (int)SessionHelper.CurrentSession.Id, taskID, "A");
                    quizResponse.ResponseQuestionAnswer = logResult.ResponseQuestionAnswer;
                }
                quizResponse.StudentId = studentID;
                var quizFeedback = _assignmentService.GetStudentTaskDetailById(taskID, studentID, studentTaskDetails.StudentAssignmentId, Convert.ToInt64(studentTaskDetails.StudentTaskId));
                if (quizFeedback != null)
                {
                    quizResponse.Feedback = quizFeedback.Feedback;
                    quizResponse.QuizFeedbacks = quizFeedback.QuizFeedbacks;
                    quizResponse.FeedbackFrom = quizFeedback.FeedbackFrom;
                }
            }
            foreach (var question in questionList)
            {
                var questionDetails = _quizQuestionsService.GetQuizQuestionById(question.QuizQuestionId);
                question.Answers = questionDetails.Answers;
            }
            ViewBag.Quiz = quizDetails;
            ViewBag.QuizDetails = questionList;
            ViewBag.QuizResponse = quizResponse;
            ViewBag.studentId = studentId;
            ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            var assignment = _assignmentService.GetAssignmentById(assignmentTask.AssignmentId);
            TaskPeerReview objPeertaskreview = new TaskPeerReview();
            List<TaskDocumentReviewdetails> lstTaskReviewDocs = new List<TaskDocumentReviewdetails>();
            if (assignment.IsPeerMarkingEnable)
            {
                objPeertaskreview = _assignmentService.GetPeerTaskDetail(studentID, taskID);
                lstTaskReviewDocs = _assignmentService.GetTaskReviewedDocumentFiles(objPeertaskreview.TaskPeerReviewId).ToList();
            }
            ViewBag.IsTaskReviewCompleted = objPeertaskreview.IsTaskPeerReviewCompleted;
            ViewBag.lstTaskReviewDocs = lstTaskReviewDocs;

            return View("_StudentTaskDetails", studentTaskDetails);
        }
        public ActionResult ResetMTPQuestion(int quizId, int quizQuestionId)
        {
            IEnumerable<QuizQuestionsEdit> questionList = new List<QuizQuestionsEdit>();
            var questions = new QuizQuestionsEdit();
            questionList = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            questions = questionList.Where(a => a.QuizQuestionId == quizQuestionId).FirstOrDefault();
            if (questions != null)
            {
                questions.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(quizQuestionId);
            }
            var questionDetails = _quizQuestionsService.GetQuizQuestionById(quizQuestionId);
            questions.Answers = questionDetails.Answers;
            return PartialView("_DragDrop", questions);
        }
        [HttpGet]
        public JsonResult MarkAsComlpeteTask(int studentTaskId)
        {
            OperationDetails op = new OperationDetails();
            int CompletedBy = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            if (SessionHelper.CurrentSession.IsTeacher())
            {
                op.Success = _assignmentService.MarkAsCompleteTask(studentTaskId, true, CompletedBy);
            }
            else
            {
                op.Success = _assignmentService.MarkAsCompleteTask(studentTaskId, false, CompletedBy);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SubmitTaskMarks(int studentTaskId, int StudentAssignmentId, decimal submitMarks, int gradingTemplateId)
        {
            GradingTemplateItem grade = new GradingTemplateItem();
            int CompletedBy = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            if (SessionHelper.CurrentSession.IsTeacher())
            {
                grade = _assignmentService.SubmitTaskMarks(studentTaskId, StudentAssignmentId, submitMarks, CompletedBy, gradingTemplateId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
            }
            return Json(grade, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<ActionResult> UploadStudentTaskFiles(HttpPostedFileBase[] postedFile, string taskId, int studentId = 0)
        {
            var tskId = !string.IsNullOrWhiteSpace(taskId) ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(taskId)) : 0;
            List<StudentTaskFile> fileNames = new List<StudentTaskFile>();
            string relativePath = string.Empty;
            //Session["StudentAsgUploadedFiles"] = null;
            OperationDetails op = new OperationDetails();
            SharePointHelper sh = new SharePointHelper();
            AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
            if (Request.Files.Count > 0)
            {
                try
                {
                    for (int i = 0; i < postedFile.Count(); i++)
                    {
                        HttpPostedFileBase file = postedFile[i];
                        SharePointFileView shv = new SharePointFileView();
                        if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                        {
                            await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                            shv = await azureHelper.UploadAssignmentFilesAsync(FileModulesConstants.StudentTaskFile, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                        }
                        else
                        {
                            Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                            shv = SharePointHelper.UploadAssignmentFiles(ref cx, FileModulesConstants.StudentTaskFile, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                        }
                        var extension = System.IO.Path.GetExtension(file.FileName);
                        fileNames.Add(new StudentTaskFile
                        {
                            FileName = file.FileName,
                            TaskId = tskId,
                            UploadedFileName = shv.ShareableLink,
                            CreateOn = DateTime.Now,
                            SharepointUploadedFileURL = shv.SharepointUploadedFileURL,
                            ResourceFileTypeId = (short)ResourceFileTypes.SharePoint
                        });
                    }
                }
                catch (Exception ex)
                {

                }
            }
            if (fileNames.Count > 0)
            {
                var visitorInfo = new VisitorInfo();
                string IpAddress = visitorInfo.GetIpAddress();
                string HostIpAddress = visitorInfo.GetClientIPAddress();
                string IpDetails = IpAddress + ":" + HostIpAddress;
                op.Success = _assignmentService.UploadStudentTaskFiles(fileNames, studentId, tskId, IpDetails);
                if (op.Success)
                {
                    op.Message = ResourceManager.GetString("Assignment.AssignmentMessages.StudentTaskFileSuccess");
                    op.CssClass = "success";
                }
                else
                {
                    op.Message = ResourceManager.GetString("Shared.Messages.TechnicalError");
                    op.CssClass = "error";
                }
            }
            return Json(op, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetuploadedTaskFileDetails(string taskId, int studentId = 0)
        {
            var tskId = !string.IsNullOrWhiteSpace(taskId) ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(taskId)) : 0;

            List<StudentTaskFile> lstStudentTaskFile = new List<StudentTaskFile>();
            lstStudentTaskFile = _assignmentService.GetStudentTaskFiles(studentId, tskId).ToList();
            if (lstStudentTaskFile.Count > 0)
            {
                foreach (var item in lstStudentTaskFile)
                {
                    item.CreateOn = (DateTime)ConvertExtensions.ConvertUtcToLocalTime(item.CreateOn);
                }
            }
            return PartialView("_StudentTaskFileList", lstStudentTaskFile);
        }

        public ActionResult LoadTaskGradingTemplateItems(int gradingtemplateId, int taskId, int studentId, int gradingType)
        {
            var studentTask = _assignmentService.GetStudentTaskDetails(taskId, studentId);
            List<GradingTemplateItem> lstGradingtemplate = _gradingTemplateItemService.GetGradingTemplateItems(gradingtemplateId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId).ToList();
            ViewBag.selectedGradeId = studentTask.GradingTemplateItemId;
            ViewBag.entityId = studentTask.StudentTaskId;
            ViewBag.gradingType = gradingType;
            return PartialView("_LoadGradingTemplateItems", lstGradingtemplate);
        }

        public ActionResult LoadObjectiveGradingTemplateItems(int gradingtemplateId, int selectedGradingTemplateItemId, int studentObjectiveId, int gradingType)
        {

            List<GradingTemplateItem> lstGradingtemplate = _gradingTemplateItemService.GetGradingTemplateItems(gradingtemplateId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId).ToList();
            ViewBag.selectedGradeId = selectedGradingTemplateItemId;
            ViewBag.entityId = studentObjectiveId;
            ViewBag.gradingType = gradingType;
            return PartialView("_LoadGradingTemplateItems", lstGradingtemplate);
        }

        public ActionResult LoadQuizFeedbackPopup(int taskId, int studentId, int AssignmentStudentId, int StudentTaskId)
        {
            var assignmentTask = _assignmentService.GetStudentTaskDetailById(taskId, studentId, AssignmentStudentId, Convert.ToInt64(StudentTaskId));
            return PartialView("_LoadQuizFeedbackPopup", assignmentTask);
        }

        public ActionResult LoadObjectiveFeedbackPopup(int AssignmentStudentId, int StudentobjectiveId)
        {
            var lstStudentObjectives = _assignmentService.GetStudentAssignmentObjectives(AssignmentStudentId);
            AssignmentStudentObjective obj = lstStudentObjectives.Where(m => m.StudentObjectiveMarkId == StudentobjectiveId).FirstOrDefault();
            obj.ObjectiveFeedbacks = _assignmentService.GetStudentAssignmentObjectiveAudiofeedback(StudentobjectiveId).ToList();
            return PartialView("_LoadStudentObjectiveFeedbackPopup", obj);
        }


        [HttpPost]
        public ActionResult DeleteRecording(int id)
        {
            var result = _fileService.Delete(id, SessionHelper.CurrentSession.Id);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult InsertTaskFeedback(AssignmentTask assignmentTask, List<HttpPostedFileBase> recordings)
        {
            if (recordings != null)
            {
                foreach (var item in recordings)
                {
                    var filesType = _fileService.GetFileTypes("Recording");
                    var fileModule = _fileService.GetFileManagementModules().ToList();
                    int i = 1;
                    Phoenix.Models.File file = new Phoenix.Models.File();
                    file.SchoolId = SessionHelper.CurrentSession.SchoolId;
                    file.ModuleId = fileModule.Where(x => x.ModuleName == "Task Feedback").Select(x => x.ModuleId).FirstOrDefault();
                    file.SectionId = assignmentTask.StudentTaskId;
                    file.FileName = SessionHelper.CurrentSession.FirstName + "_" + SessionHelper.CurrentSession.LastName + "_" + i + Path.GetExtension(item.FileName);
                    file.FileTypeId = filesType.Select(x => x.TypeId).FirstOrDefault();
                    file.FilePath = UploadQuizFeedbackResource(item);
                    i++;
                    assignmentTask.Files.Add(file);
                }
            }
            assignmentTask.UserId = SessionHelper.CurrentSession.Id;
            var result = _assignmentService.InsertTaskFeedback(assignmentTask);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        private string UploadQuizFeedbackResource(HttpPostedFileBase resourceFile)
        {
            string GUID = Guid.NewGuid().ToString();
            string filePath = String.Empty;
            string strFile = String.Format("{0}{1}", GUID, Path.GetExtension(resourceFile.FileName));
            string relativeFilePath = String.Empty;
            //string strFile = suggestionFile.FileName;
            string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
            relativeFilePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.TaskFeedbackDir;
            //filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizFeedbackDir + "/User_" + SessionHelper.CurrentSession.Id;
            Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
            Common.Helpers.CommonHelper.CreateDestinationFolder(relativeFilePath);
            //Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
            filePath = Path.Combine(relativeFilePath, strFile);
            //relativeFilePath = Constants.PlannerDir + "/User_" + SessionHelper.CurrentSession.Id;
            string path = Constants.TaskFeedbackDir + "/" + strFile;
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            resourceFile.SaveAs(filePath);
            return path;
        }
        public ActionResult AssignGradeToStudentTask(int studentTaskId, int gradeId)
        {
            GradingTemplateItem objGrade = _gradingTemplateItemService.GetGradingTemplateItemById(gradeId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
            OperationDetails op = new OperationDetails();
            int GradedBy = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            op.Success = _assignmentService.AssignGradeToStudentTask(studentTaskId, gradeId, GradedBy);
            return Json(objGrade, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AssignGradeToStudentObjective(int studentObjectiveId, int gradeId)
        {
            GradingTemplateItem objGrade = _gradingTemplateItemService.GetGradingTemplateItemById(gradeId, LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
            OperationDetails op = new OperationDetails();
            op.Success = _assignmentService.AssignGradeToStudentObjective(studentObjectiveId, gradeId);
            return Json(objGrade, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadSelectedMyFiles()
        {
            List<AssignmentFile> lstMyFilesAsAsgFiles = new List<AssignmentFile>();
            if (Session["AssignmentFiles"] != null)
            {
                lstMyFilesAsAsgFiles = Session["AssignmentFiles"] as List<AssignmentFile>;
                Session["lstSelectedMyFiles"] = lstMyFilesAsAsgFiles;
            }

            return PartialView("_LoadMyFiles", lstMyFilesAsAsgFiles);
        }
        public ActionResult LoadMyFiles()
        {
            List<MyFilesTreeItem> myFiles = _assignmentService.GetMyFiles(SessionHelper.CurrentSession.Id).ToList();
            List<FileTreeItem> treeList = new List<FileTreeItem>();
            myFiles.ForEach(l => treeList.Add(new FileTreeItem() { id = l.FileId, parentId = l.ParentFolderId, name = l.FileName, isFolder = l.isFolder }));
            return Json(treeList);
        }

        public ActionResult GetSelectedFile(long id, bool isFolder)
        {
            List<AssignmentFile> lstMyFilesAsAsgFiles = new List<AssignmentFile>();
            lstMyFilesAsAsgFiles = _assignmentService.GetAssignmentMyFiles(id, isFolder).ToList();
            List<AssignmentFile> lstSelectedMyFilesAsAsgFiles = new List<AssignmentFile>();
            if (Session["lstSelectedMyFiles"] == null)
            {
                Session["lstSelectedMyFiles"] = lstMyFilesAsAsgFiles;
                lstSelectedMyFilesAsAsgFiles.AddRange(lstMyFilesAsAsgFiles);
            }
            else
            {
                lstSelectedMyFilesAsAsgFiles = Session["lstSelectedMyFiles"] as List<AssignmentFile>;
                lstSelectedMyFilesAsAsgFiles.AddRange(lstMyFilesAsAsgFiles);
                Session["lstSelectedMyFiles"] = lstSelectedMyFilesAsAsgFiles;
            }
            return PartialView("_LoadSelectedMyFile", lstSelectedMyFilesAsAsgFiles);
        }

        public ActionResult UpdateSelectedFiles(List<AssignmentFile> mappingDetails)
        {
            var operationDetails = new OperationDetails();
            List<AssignmentFile> lstFiles = new List<AssignmentFile>();
            if (Session["lstSelectedMyFiles"] == null)
            {
                Session["lstSelectedMyFiles"] = mappingDetails;
            }
            else
            {
                AssignmentFile obj = mappingDetails.FirstOrDefault();
                lstFiles = Session["lstSelectedMyFiles"] as List<AssignmentFile>;
                lstFiles = lstFiles.Where(m => m.AssignmentFileId != obj.AssignmentFileId).ToList();
                Session["lstSelectedMyFiles"] = lstFiles;
            }
            operationDetails.Success = false;
            operationDetails.Message = "";
            return PartialView("_LoadSelectedMyFile", lstFiles);
        }

        public ActionResult SaveSelectedFiles(List<AssignmentFile> mappingDetails)
        {
            var operationDetails = new OperationDetails();
            List<AssignmentFile> lstSelectedFiles = new List<AssignmentFile>();
            if (Session["AssignmentFiles"] == null)
            {
                Session["AssignmentFiles"] = mappingDetails;
                lstSelectedFiles = mappingDetails;
            }
            else
            {
                lstSelectedFiles = mappingDetails;
            }
            Session["lstSelectedMyFiles"] = lstSelectedFiles;
            return PartialView("_AssignmentFileList", lstSelectedFiles);
        }
        #endregion

        #region Quiz

        [HttpPost, ValidateInput(false)]
        public ActionResult SaveQuizData(QuizResponse model, List<QuizAnswersEdit> answersList)
        {
            OperationDetails op = new OperationDetails();
            int CompletedBy = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            if (SessionHelper.CurrentSession.IsStudent())
            {
                op.Success = _assignmentService.MarkAsCompleteTask(model.StudentTaskId, false, CompletedBy);
            }
            model.QuizResponseByUserId = (int)SessionHelper.CurrentSession.Id;
            answersList = answersList == null ? new List<QuizAnswersEdit>() : answersList;
            model.TaskId = !string.IsNullOrWhiteSpace(model.TskId) ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(model.TskId)) : 0;

            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                model.QuizResponseByTeacherId = (int)SessionHelper.CurrentSession.Id;
            }
            var sourceModel = new List<QuizAnswersView>();
            foreach (var item in answersList)
            {
                var quizAnswersView = new QuizAnswersView();
                EntityMapper<QuizAnswersEdit, QuizAnswersView>.Map(item, quizAnswersView);
                sourceModel.Add(quizAnswersView);
            }
            model.ResponseQuestionAnswer = sourceModel;

            var result = _quizQuestionsService.UpdateQuizAnswerData(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveAssignmentComment(AssignmentComment model, List<HttpPostedFileBase> recordings)
        {
            model.ByTeacher = SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin;
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            OperationDetails op = new OperationDetails();
            model.lstDocuments = Session["FeedbackFiles"] != null ? Session["FeedbackFiles"] as List<AssignmentFeedbackFiles> : new List<AssignmentFeedbackFiles>();
            if (model.ByTeacher)
            {
                int i = 1;
                if (recordings != null)
                {
                    foreach (var item in recordings)
                    {

                        var assignmentFeedbackFiles = new AssignmentFeedbackFiles();
                        assignmentFeedbackFiles.AssignmentStudentId = model.AssignmentStudentId;
                        assignmentFeedbackFiles.FileName = SessionHelper.CurrentSession.FirstName + "_" + SessionHelper.CurrentSession.LastName + "_" + i + Path.GetExtension(item.FileName);
                        assignmentFeedbackFiles.CreatedBy = (int)SessionHelper.CurrentSession.Id;
                        assignmentFeedbackFiles.UploadedFileName = UploadAssignmentFeedbackResource(item);
                        model.lstDocuments.Add(assignmentFeedbackFiles);
                        i++;
                    }
                }
            }
            op.Success = _assignmentService.InsertAssignmentComment(model);
            Session["FeedbackFiles"] = new List<AssignmentFeedbackFiles>();
            var student = _studentListService.GetStudentDetailsByStudentId(model.StudentId);
            if (op.Success == true && model.ByTeacher == true)
            {
                var sendEmailNotificationView = _assignmentService.GenerateEmailTemplateForAssignmentComment(model.AssignmentTitle, model.Comment, student.StudentInternalId);
                bool isSuccess = _commonService.SendEmailNotifications(sendEmailNotificationView);
            }

            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAssignmentComment(int assignmentCommentId)
        {
            OperationDetails op = new OperationDetails();
            AssignmentComment objAssignmentComment = new AssignmentComment();
            objAssignmentComment.DeletedBy = (int)SessionHelper.CurrentSession.Id;
            objAssignmentComment.AssignmentCommentId = assignmentCommentId;
            op = _assignmentService.DeleteAssignmentComment(objAssignmentComment);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public JsonResult editAssignmentComment(int assignmentCommentId, int assignmentStudentId)
        {
            var list = _assignmentService.GetAssignmentComments(assignmentStudentId).Where(a => a.AssignmentCommentId == assignmentCommentId).Select(s => s.Comment).FirstOrDefault();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        private string UploadAssignmentFeedbackResource(HttpPostedFileBase resourceFile)
        {
            string GUID = Guid.NewGuid().ToString();
            string filePath = String.Empty;
            string strFile = String.Format("{0}{1}", GUID, Path.GetExtension(resourceFile.FileName));
            string relativeFilePath = String.Empty;

            string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
            relativeFilePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFeedbackDir;

            Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
            Common.Helpers.CommonHelper.CreateDestinationFolder(relativeFilePath);
            filePath = Path.Combine(relativeFilePath, strFile);

            string path = Constants.AssignmentFeedbackDir + "/" + strFile;
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            resourceFile.SaveAs(filePath);

            return path;
        }
        public ActionResult LoadAssignmentComments(int assignmentStudentId)
        {
            var list = _assignmentService.GetAssignmentComments(assignmentStudentId);

            List<AssignmentComment> assignmentComments = new List<AssignmentComment>();
            List<AssignmentFeedbackFiles> feedbackFiles = new List<AssignmentFeedbackFiles>();
            foreach (var item in list)
            {
                var assignmentComment = new AssignmentComment();
                feedbackFiles = new List<AssignmentFeedbackFiles>();

                feedbackFiles = _assignmentService.GetAssignmentFeedbackFiles(assignmentStudentId, item.AssignmentCommentId).ToList();
                assignmentComment.AssignmentCommentId = item.AssignmentCommentId;
                assignmentComment.AssignmentStudentId = item.AssignmentStudentId;
                assignmentComment.ByTeacher = item.ByTeacher;
                assignmentComment.Comment = item.Comment;
                assignmentComment.IsCommentSeenByStudent = item.IsCommentSeenByStudent;
                assignmentComment.CreatedOn = (DateTime)ConvertExtensions.ConvertUtcToLocalTime(item.CreatedOn);
                assignmentComment.StudentImage = item.StudentImage;
                assignmentComment.StudentName = item.StudentName;
                assignmentComment.TeacherAvatar = item.TeacherAvatar;
                assignmentComment.TeacherImage = item.TeacherImage;
                assignmentComment.TeacherName = item.TeacherName;
                assignmentComment.lstDocuments = feedbackFiles;

                assignmentComments.Add(assignmentComment);
            }
            return PartialView("_AssignmentCommentList", assignmentComments);
        }
        #endregion

        public async Task<ActionResult> DownloadStudentAttachment(int AssignmentId, string strStudentList)
        {
            List<AssignmentStudentDetails> studentList = new List<AssignmentStudentDetails>();
            const string fileFullUrl = "https://contoso-my.sharepoint.com/personal/jdoe_contoso_onmicrosoft_com/documents/sample.docx";
            //GraphServiceClient graphClient = new GraphServiceClient(authProvider);

            //var sharedItemId = UrlToSharingToken(fileFullUrl);
            //var requestUrl = $"{graphClient.BaseUrl}/shares/{sharedItemId}/driveitem/content";
            //var message = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            //await graphClient.AuthenticationProvider.AuthenticateRequestAsync(message);
            //var response = await graphClient.HttpProvider.SendAsync(message);
            //var bytesContent = await response.Content.ReadAsByteArrayAsync();

            //System.IO.File.WriteAllBytes("sample.docx", bytesContent); //save into local file

            if (!string.IsNullOrEmpty(strStudentList))
            {
                studentList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AssignmentStudentDetails>>(strStudentList);
            }

            using (var memoryStream = new MemoryStream())
            {
                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    foreach (var student in studentList)
                    {
                        var lstStudentAsgFiles = _assignmentService.GetStudentAssignmentFiles(student.StudentId, AssignmentId).ToList();
                        var sharepointfiles = lstStudentAsgFiles.Where(m => m.UploadedFileName.Contains("sharepoint") && m.ResourceFileTypeId == 3).ToList();
                        var otherFiles = lstStudentAsgFiles.Where(m => !m.UploadedFileName.Contains("sharepoint")).ToList();
                        foreach (var file in sharepointfiles)
                        {
                            byte[] f = null;
                            if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                            {
                                AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                                await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                                f = await azureHelper.DownloadFilesAsync(file.SharepointUploadedFileURL);
                            }
                            else
                            {
                                SharePointHelper sh = new SharePointHelper();
                                var cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                                f = sh.DownloadFilesFromSharePoint(ref cx, file.SharepointUploadedFileURL);
                            }
                            Stream spStream = new MemoryStream(f);
                            ZipArchiveEntry readmeEntry = ziparchive.CreateEntry(AssignmentId + "/" + student.StudentName.Replace(" ", "_") + "/Assignment/" + file.FileName);
                            using (var stream = readmeEntry.Open())
                            {
                                spStream.CopyTo(stream);
                            }
                        }

                        foreach (var file in otherFiles)
                        {
                            try
                            {
                                if ((file.UploadedFileName != null))
                                {
                                    ZipArchiveEntry readmeEntry = ziparchive.CreateEntry(AssignmentId + "/" + student.StudentName.Replace(" ", "_") + "/Assignment/" + file.FileName);
                                    using (var stream = readmeEntry.Open())
                                    using (var client = new WebClient())
                                    {
                                        using (var readfile = client.OpenRead(file.UploadedFileName))
                                        {
                                            readfile.CopyTo(stream);
                                        }

                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                continue;
                            }

                        }
                        var lstAssignmentTasks = _assignmentService.GetAssignmentTasks(AssignmentId).ToList();

                        foreach (var task in lstAssignmentTasks)
                        {
                            var lstStudentTaskfile = _assignmentService.GetStudentTaskFiles(student.StudentId, task.TaskId).ToList();
                            var tasksharepointfiles = lstStudentTaskfile.Where(m => m.UploadedFileName.Contains("sharepoint") && m.ResourceFileTypeId == 3).ToList();
                            var taskotherFiles = lstStudentTaskfile.Where(m => !m.UploadedFileName.Contains("sharepoint")).ToList();
                            foreach (var file in tasksharepointfiles)
                            {
                                byte[] f = null;
                                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                                {
                                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                                    f = await azureHelper.DownloadFilesAsync(file.SharepointUploadedFileURL);
                                }
                                else
                                {
                                    SharePointHelper sh = new SharePointHelper();
                                    var cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                                    f = sh.DownloadFilesFromSharePoint(ref cx, file.SharepointUploadedFileURL);
                                }
                                Stream spStream = new MemoryStream(f);
                                ZipArchiveEntry readmeEntry = ziparchive.CreateEntry(AssignmentId + "/" + student.StudentName.Replace(" ", "_") + "/Task/" + task.TaskTitle + "/" + file.FileName);
                                using (var stream = readmeEntry.Open())
                                {
                                    spStream.CopyTo(stream);
                                }
                            }

                            foreach (var taskFile in taskotherFiles)
                            {
                                try
                                {
                                    if (taskFile.UploadedFileName != null)
                                    {
                                        ZipArchiveEntry readmeEntry = ziparchive.CreateEntry(AssignmentId + "/" + student.StudentName.Replace(" ", "_") + "/Task/" + task.TaskTitle + "/" + taskFile.FileName);
                                        using (var stream = readmeEntry.Open())
                                        using (var client = new WebClient())
                                        {
                                            using (var readfile = client.OpenRead(taskFile.UploadedFileName))
                                            {
                                                readfile.CopyTo(stream);
                                            }
                                        }
                                        //ziparchive.CreateEntryFromFile(PhoenixConfiguration.Instance.ReadFilePath + taskFile.UploadedFileName, AssignmentId + "/" + student.StudentName.Replace(" ", "_") + "/Task/" + task.TaskTitle + "/" + taskFile.FileName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    continue;
                                }
                            }
                        }

                    }
                }
                return File(memoryStream.ToArray(), "application/zip", AssignmentId + ".zip");
            }
        }


        public ActionResult DeleteUploadedfiles(int FileId = 0, int AssignmentFileId = 0)
        {
            long UserId = SessionHelper.CurrentSession.Id;
            var isActionPerformed = _assignmentService.DeleteUploadedFiles(FileId, AssignmentFileId, UserId);
            return null;
        }

        #region Archived Assignment
        //public ActionResult ArchieveAssignments()
        //{
        //    Assignment objAssignment = new Assignment();
        //    int teacherID = (int)SessionHelper.CurrentSession.Id;
        //    objAssignment = _assignmentService.GetAssignments(teacherID);
        //    return View("ArchieveAssignments", objAssignment);
        //}

        public ActionResult ArchiveAssignmentsList()
        {
            Assignment objAssignment = new Assignment();
            int teacherID = (int)SessionHelper.CurrentSession.Id;
            objAssignment = _assignmentService.GetAssignments(teacherID);
            return PartialView("_ArchiveAssignmentsList", objAssignment);
        }

        public ActionResult GetActiveAssignments()
        {
            int teacherID = (int)SessionHelper.CurrentSession.Id;
            var lstActiveAssignments = _assignmentService.GetAssignments(teacherID);
            return PartialView("_GetActiveAssignments", lstActiveAssignments.lstActiveAssignments);
        }

        public ActionResult GetArchiveAssignments()
        {
            int teacherID = (int)SessionHelper.CurrentSession.Id;
            var lstArchiveAssignments = _assignmentService.GetAssignments(teacherID);
            return PartialView("_GetArchiveAssignments", lstArchiveAssignments.lstArchiveAssignments);
        }

        [HttpPost]
        public ActionResult UpdateActiveAssignment(string assignmentsToArchive)
        {
            string[] str = assignmentsToArchive.Split(',');
            foreach (var assignmentId in str)
            {
                if (assignmentId != "")
                {
                    var result = _assignmentService.UpdateActiveAssignmets(Convert.ToInt64(assignmentId));
                }
            }
            return null;

        }

        public ActionResult ArchivedIndex(int pageIndex = 1, string assignmentType = "")
        {
            string pageName = String.Empty;
            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                pageName = "_ArchivedAssignmentDetailsByPaging";
            }
            Session["AssignmentType"] = assignmentType;
            return View(pageName);
        }

        public ActionResult LoadArchivedAssignment(AssignmentFilter loadAssignment)
        {
            var pageName = "";
            var model = new Pagination<AssignmentStudentDetails>();
            var assignmentList = new List<AssignmentStudentDetails>();
            loadAssignment.assignmentType = Convert.ToString(Session["AssignmentType"]);

            if (SessionHelper.CurrentSession.IsParent() || SessionHelper.CurrentSession.IsStudent())
            {
                var studentId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                pageName = "_StudentArchivedAssignmentGroup";
                if (loadAssignment.filterVal != null)
                    assignmentList = _assignmentService.GetArchivedStudentFilterSchoolGroupsById(loadAssignment.page, loadAssignment.size, Convert.ToInt64(studentId), loadAssignment.searchString, loadAssignment.assignmentType, loadAssignment.sortBy, loadAssignment.filterVal).ToList();
                else
                    assignmentList = _assignmentService.GetArchivedAssignmentByStudentId(loadAssignment.page, loadAssignment.size, Convert.ToInt64(studentId), loadAssignment.searchString, loadAssignment.assignmentType, loadAssignment.sortBy).ToList();
                if (assignmentList.Any())
                    model = new Pagination<AssignmentStudentDetails>(loadAssignment.page, loadAssignment.size, assignmentList, assignmentList.FirstOrDefault().TotalCount);
                model.RecordCount = assignmentList.Count == 0 ? 0 : assignmentList[0].TotalCount;
                bool NoRecords = false;
                if ((loadAssignment.page == 1 && assignmentList.Count == 0) || (!string.IsNullOrEmpty(loadAssignment.searchString) && assignmentList.Count == 0))
                {
                    NoRecords = true;
                }
                ViewBag.Norecords = NoRecords;
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                pageName = "_TeacherArchivedAssignmentGroup";
                if (loadAssignment.filterVal != null)
                    assignmentList = _assignmentService.GetArchivedFilterSchoolGroupsById(Helpers.CommonHelper.GetLoginUser().Id, loadAssignment).ToList();
                else
                    assignmentList = _assignmentService.GetArchivedAssignmentStudentDetails(Helpers.CommonHelper.GetLoginUser().Id, loadAssignment).ToList();
                //if (assignmentList.Any())
                //    model = new Pagination<AssignmentStudentDetails>(page, size, assignmentList, assignmentList.FirstOrDefault().TotalCount);
                //model.RecordCount = assignmentList.Count == 0 ? 0 : assignmentList[0].TotalCount;
                //    ViewBag.AcademicYearList = new SelectList(SelectListHelper.GetAcademicYearByUserId((int)SessionHelper.CurrentSession.Id), "AcademicYearId", "AcademicYear");
                bool NoRecords = false;
                if ((loadAssignment.page == 1 && assignmentList.Count == 0) || (!string.IsNullOrEmpty(loadAssignment.searchString) && assignmentList.Count == 0))
                {
                    NoRecords = true;
                }
                ViewBag.Norecords = NoRecords;
            }
            //model.LoadPageRecordsUrl = "/Assignments/Assignment/LoadArchivedAssignment?pageIndex={0}";
            //model.SearchString = searchString;
            //return PartialView(pageName, model);
            var result = new Object();
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, pageName, assignmentList);
            var contentList = new string[] { htmlContent };
            result = new
            {
                isContentFinished = assignmentList.Count < loadAssignment.size ? true : false,
                content = contentList
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadAdminArchivedAssignment(AssignmentFilter loadAssignment)
        {
            var pageName = "";
            var model = new Pagination<AssignmentStudentDetails>();
            var assignmentList = new List<AssignmentStudentDetails>();
            loadAssignment.assignmentType = Convert.ToString(Session["AssignmentType"]);

            if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                pageName = "_TeacherArchivedAssignmentGroup";
                loadAssignment.schoolId = SessionHelper.CurrentSession.SchoolId;
                assignmentList = _assignmentService.GetAdminArchivedAssignmentStudentDetails(loadAssignment).ToList();
                bool NoRecords = false;
                if ((loadAssignment.page == 1 && assignmentList.Count == 0) || (!string.IsNullOrEmpty(loadAssignment.searchString) && assignmentList.Count == 0))
                {
                    NoRecords = true;
                }
                ViewBag.Norecords = NoRecords;
            }
            var result = new Object();
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, pageName, assignmentList);
            var contentList = new string[] { htmlContent };
            result = new
            {
                isContentFinished = assignmentList.Count < loadAssignment.size ? true : false,
                content = contentList
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion Archived Assignment
        public ActionResult GroupAssignments(string startDate, string endDate, string groupId, string teacherId)
        {
            int schoolGroupId;
            DateTime stDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(startDate));
            DateTime edDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(endDate));

            schoolGroupId = EncryptDecryptHelper.DecodeBase64(groupId).ToInteger();
            Session["groupId"] = schoolGroupId;

            Session["startdate"] = stDate;
            Session["endDate"] = edDate;

            return View();
        }
        public ActionResult LoadGroupAssignments(int pageIndex = 1, string searchString = "", string startDate = "", string endDate = "", string groupId = "")
        {
            string usrType = SessionHelper.CurrentSession.IsAdmin ? "Admin" : "Teacher";

            DateTime stDate, edDate;
            int schoolGroupId = 0;
            if (!string.IsNullOrEmpty(startDate))
            {
                stDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(startDate));
                edDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(endDate));
                schoolGroupId = EncryptDecryptHelper.DecodeBase64(groupId).ToInteger();
            }
            else
            {
                stDate = DateTime.Parse(Session["startdate"].ToString());
                edDate = DateTime.Parse(Session["endDate"].ToString());
                schoolGroupId = (int)Session["groupId"];
            }
            var pageName = "_GroupAssignmentList";
            var model = new Pagination<AssignmentStudentDetails>();
            List<AssignmentStudentDetails> assignmentList;

            assignmentList = _assignmentService.GetAssignmentListForReport(pageIndex, 6, searchString, SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId, schoolGroupId, stDate, edDate, usrType).ToList();

            if (assignmentList.Any())
                model = new Pagination<AssignmentStudentDetails>(pageIndex, 6, assignmentList, assignmentList.FirstOrDefault().TotalCount);
            model.RecordCount = assignmentList.Count == 0 ? 0 : assignmentList[0].TotalCount;
            model.LoadPageRecordsUrl = "/Assignments/Assignment/LoadGroupAssignments?pageIndex={0}";
            model.SearchString = searchString;
            return PartialView(pageName, model);
        }

        public ActionResult TeacherGroupAssignments(string startDate, string endDate, string teacherId)
        {
            long schoolTeacherId;
            DateTime stDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(startDate));
            DateTime edDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(endDate));

            schoolTeacherId = EncryptDecryptHelper.DecodeBase64(teacherId).ToInteger();
            schoolTeacherId = Convert.ToInt64(schoolTeacherId);

            Session["teacherId"] = schoolTeacherId;
            Session["startdate"] = stDate;
            Session["endDate"] = edDate;

            return View();
        }

        public ActionResult LoadTeacherGroupAssignments(int pageIndex = 1, string searchString = "", string startDate = "", string endDate = "", string groupId = "")
        {
            string usrType = SessionHelper.CurrentSession.IsAdmin ? "Admin" : "Teacher";

            DateTime stDate, edDate;
            long schoolTeacherId = 0;
            if (!string.IsNullOrEmpty(startDate))
            {
                stDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(startDate));
                edDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(endDate));
                schoolTeacherId = (Int64)Session["teacherId"];
            }
            else
            {
                stDate = DateTime.Parse(Session["startdate"].ToString());
                edDate = DateTime.Parse(Session["endDate"].ToString());
                schoolTeacherId = (Int64)Session["teacherId"];
            }
            var pageName = "_GroupAssignmentList";
            var model = new Pagination<AssignmentStudentDetails>();
            List<AssignmentStudentDetails> assignmentList = new List<AssignmentStudentDetails>();

            if (schoolTeacherId > 0)
            {
                assignmentList = _assignmentService.GetAssignmentListForReport(pageIndex, 6, searchString, schoolTeacherId, SessionHelper.CurrentSession.SchoolId, 0, stDate, edDate, usrType).ToList();
            }

            if (assignmentList.Any())
                model = new Pagination<AssignmentStudentDetails>(pageIndex, 6, assignmentList, assignmentList.FirstOrDefault().TotalCount);
            model.RecordCount = assignmentList.Count == 0 ? 0 : assignmentList[0].TotalCount;
            model.LoadPageRecordsUrl = "/Assignments/Assignment/LoadTeacherGroupAssignments?pageIndex={0}";
            model.SearchString = searchString;
            return PartialView(pageName, model);
        }

        #region PeerManager
        public ActionResult AddEditPeerManagerSetting(int? assignmentId, string id)
        {
            List<AssignmentPeerReview> lstPeerreview = new List<AssignmentPeerReview>();

            var StudentList = _studentListService.GetStudentDetailsByIds(id);
            List<SelectListItem> PeersList = new List<SelectListItem>();
            foreach (var item in StudentList)
            {
                PeersList.Add(new SelectListItem
                {
                    Text = item.FirstName + ' ' + item.LastName,
                    Value = item.Id.ToString()
                });
            }
            PeersList.Add(new SelectListItem
            {
                Text = SessionHelper.CurrentSession.FirstName + ' ' + SessionHelper.CurrentSession.LastName,
                Value = SessionHelper.CurrentSession.Id.ToString()
            });
            ViewBag.PeerSelectList = new SelectList(PeersList, "Value", "Text");
            List<AssignmentPeerReview> lstAssignmentPeerReview = new List<AssignmentPeerReview>();
            if (assignmentId.Value != 0)
            {
                if (Session["PeerMappingDetails"] == null)
                {
                    lstAssignmentPeerReview = _assignmentService.GetPeerMappingDetails(assignmentId.Value).ToList();
                    Session["PeerMappingDetails"] = lstAssignmentPeerReview;
                }
                else
                {
                    lstAssignmentPeerReview = (List<AssignmentPeerReview>)Session["PeerMappingDetails"];
                }
            }
            return PartialView("_PeerReviewManager", lstAssignmentPeerReview);
        }
        [HttpPost]
        public ActionResult UpdatePeerDetails(List<AssignmentPeerReview> peerData, string studentIds)
        {
            var StudentList = _studentListService.GetStudentDetailsByIds(studentIds);
            foreach (var item in peerData)
            {
                if (StudentList.Any(m => m.Id == item.UserId))
                {
                    var student = StudentList.Where(m => m.Id == item.UserId).FirstOrDefault();
                    item.StudentName = student.FirstName + ' ' + student.LastName;
                    if (StudentList.Any(m => m.Id == item.ReviewerId))
                    {
                        var reviewer = StudentList.Where(m => m.Id == item.ReviewerId).FirstOrDefault();
                        item.ReviewerName = reviewer.FirstName + ' ' + reviewer.LastName;
                    }
                    else if (item.ReviewerId == SessionHelper.CurrentSession.Id)
                    {
                        item.ReviewerName = SessionHelper.CurrentSession.FirstName + " " + SessionHelper.CurrentSession.LastName;
                    }
                }

            }
            Session["PeerMappingDetails"] = peerData;
            OperationDetails op = new OperationDetails();
            op.Success = true;
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutomaticallyAssignPeers(int? assignmentId, string id)
        {
            List<AssignmentPeerReview> lstAssignmentPeerReview = new List<AssignmentPeerReview>();
            var StudentList = _studentListService.GetStudentDetailsByIds(id);
            if (Session["PeerMappingDetails"] != null)
            {
                lstAssignmentPeerReview = (List<AssignmentPeerReview>)Session["PeerMappingDetails"];
            }
            if (lstAssignmentPeerReview.Count == 0)
            {
                foreach (var item in StudentList)
                {
                    lstAssignmentPeerReview.Add(new AssignmentPeerReview
                    {
                        AssignmentId = assignmentId.HasValue == true ? assignmentId.Value : 0,
                        StudentId = item.StudentId,
                        UserId = item.Id,
                        StudentName = item.FirstName + ' ' + item.LastName
                    });
                }
                lstAssignmentPeerReview.Shuffle();
                int studentCount = lstAssignmentPeerReview.Count();
                int i = 0;
                foreach (var item in lstAssignmentPeerReview)
                {
                    if (i != (studentCount - 1))
                    {
                        item.ReviewerId = lstAssignmentPeerReview[i + 1].UserId;
                        item.ReviewerName = lstAssignmentPeerReview[i + 1].StudentName;
                    }
                    else
                    {
                        item.ReviewerId = lstAssignmentPeerReview[0].UserId;
                        item.ReviewerName = lstAssignmentPeerReview[0].StudentName;
                        if (item.ReviewerId == item.UserId)
                        {
                            item.ReviewerId = SessionHelper.CurrentSession.Id;
                            item.ReviewerName = SessionHelper.CurrentSession.FirstName + ' ' + SessionHelper.CurrentSession.LastName;
                        }
                    }
                    i++;
                }
            }
            return PartialView("_PeerList", lstAssignmentPeerReview);
        }

        public async Task<ActionResult> StudentHomeworkReview(string id, string encPeerReviewId, string redirectionId)
        {
            var assignmentStuddentId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            var peerReviewId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(encPeerReviewId));
            AssignmentStudent objAssgnmentStudent = new AssignmentStudent();
            objAssgnmentStudent = _assignmentService.GetStudentAsgHomeworkDetailsById(assignmentStuddentId);
            objAssgnmentStudent.objAssignment = _assignmentService.GetAssignmentById(objAssgnmentStudent.AssignmentId);
            Session["StudentAsgUploadedFiles"] = "";
            objAssgnmentStudent.objAssignment.lstAssignmentTasks = _assignmentService.GetAssignmentTasks(objAssgnmentStudent.AssignmentId).ToList();
            objAssgnmentStudent.objAssignment.lstAssignmentFiles = _assignmentService.GetAssignmentFiles(objAssgnmentStudent.AssignmentId).ToList();
            if (objAssgnmentStudent.objAssignment.lstAssignmentFiles != null)
            {
                foreach (var item in objAssgnmentStudent.objAssignment.lstAssignmentFiles)
                {
                    item.FileExtension = item.FileName.Split('.')[1];
                }
            }
            if (objAssgnmentStudent.objAssignment.lstAssignmentTasks != null)
            {
                foreach (var item in objAssgnmentStudent.objAssignment.lstAssignmentTasks)
                {
                    item.objStudentTask = _assignmentService.GetStudentTaskDetails(item.TaskId, objAssgnmentStudent.StudentId);
                }
            }
            List<StudentAssignmentFile> lstfiles = _assignmentService.GetStudentAssignmentFiles(objAssgnmentStudent.StudentId, objAssgnmentStudent.AssignmentId).ToList();
            List<DocumentReviewdetails> existingReviewedFiles = _assignmentService.GetReviewedDocumentFiles(peerReviewId).ToList();

            AssignmentPeerReview objAssignmentPeerReview = new AssignmentPeerReview();
            objAssignmentPeerReview = _assignmentService.GetPeerAssignmentDetail(peerReviewId);
            ViewBag.AssignmentPeerReview = objAssignmentPeerReview;

            List<DocumentReviewdetails> lstDocumentReviewdetails = new List<DocumentReviewdetails>();
            foreach (var item in lstfiles)
            {
                if (!existingReviewedFiles.Any(m => m.StudentAsgFileId == item.StudentAsgFileId))
                {
                    lstDocumentReviewdetails.Add(new DocumentReviewdetails()
                    {
                        ReviewedFileId = 0,
                        PeerReviewId = peerReviewId,
                        StudentAsgFileId = item.StudentAsgFileId,
                        ReviewedFileName = await UploadReviewedFiles(item.SharepointUploadedFileURL, item.FileName)
                    });
                }
            }
            OperationDetails opUpdateDoc = new OperationDetails();
            if (lstDocumentReviewdetails.Count() > 0)
            {
                opUpdateDoc = _assignmentService.AddUpdateDocumentReviewDetails(lstDocumentReviewdetails);
            }
            lstDocumentReviewdetails.AddRange(existingReviewedFiles);
            ViewBag.StudentFile = lstfiles;
            ViewBag.FileTypes = _fileTypeList;
            ViewBag.lstReviewedDocuments = lstDocumentReviewdetails;
            ViewBag.redirectionId = Uri.EscapeDataString(redirectionId);
            return View(objAssgnmentStudent);
        }

        public async Task<ActionResult> PeersUploadedDocument(string id)
        {
            var IdValue = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            var file = _assignmentService.GetDocumentFileByStudAsgFileId(IdValue);
            var fileExtension = Path.GetExtension(file.ReviewedFileName).ToLower();
            if (fileExtension == ".doc" || fileExtension == ".docx" || fileExtension == ".rtf" || fileExtension == ".odt" || fileExtension == ".txt")
            {
                DevExpress.XtraRichEdit.DocumentFormat DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.Doc;
                switch (fileExtension)
                {
                    case ".doc":
                        DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.Doc;
                        break;
                    case ".docx":
                        DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.OpenXml;
                        break;
                    case ".rtf":
                        DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.Rtf;
                        break;
                    case ".txt":
                        DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.PlainText;
                        break;
                    case ".odt":
                        DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.OpenDocument;
                        break;
                }
                List<MarkingScheme> lstmarkingSchemes = _markingSchemeService.GetMarkingSchemes(SessionHelper.CurrentSession.SchoolId).ToList();
                RichEditDocumentServer wordProcessor = new RichEditDocumentServer();
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    Models.AzureBlobStorage.BlobFileInfo BlobFileInfo = await azureHelper.DownloadFilesAsStreamAsync(AzureBlobStorageHelper.GetBlobUrlFromQueryString(file.ReviewedFileName));
                    wordProcessor.LoadDocument(BlobFileInfo.Content, DocumentFormat);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    var cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    Microsoft.SharePoint.Client.FileInformation fileInfo = sh.DownloadFilesFromSharePointAsStream(ref cx, file.ReviewedFileName);
                    wordProcessor.LoadDocument(fileInfo.Stream, DocumentFormat);
                }
                string text = wordProcessor.WordMLText;
                foreach (var item in lstmarkingSchemes)
                {
                    item.CalculatedWeight = CalculateMarksAsPerMarking(text, item);
                }


                ViewBag.MarkingSchemes = lstmarkingSchemes;
                return View(file);
            }
            else
            {
                var studentAsgFile = _assignmentService.GetStudentAssignmentFilebyStudentAsgFileId(file.StudentAsgFileId);
                return Redirect(studentAsgFile.FilePath);
            }

        }

        public ActionResult SpreadsheetPartial()
        {
            //var webClient = new WebClient();
            //ViewBag.ByteData = System.IO.File.ReadAllBytes("http://mtskheta.gov.ge/public/img/1530793528.xlsx");
            return PartialView("_SpreadsheetPartial");
        }

        public ActionResult CustomToolbarPartial(DocumentReviewdetails objDocument, string documentDetails)
        {
            var model = new DocumentReviewdetails();
            if (documentDetails == null)
            {
                model = objDocument;
            }
            else
            {
                DocumentReviewdetails document = JsonConvert.DeserializeObject<DocumentReviewdetails>(documentDetails);
                model = document;
            }
            return PartialView("CustomToolbarPartial", model);
        }

        public ActionResult CustomToolbar()
        {
            return View("CustomToolbar");
        }

        [HttpPost]
        public ActionResult CalculateMarks(string filePath, long peerReviewId, long FileId, long ReviewedFileId, bool isTask, string textData)
        {
            OperationDetails op = new OperationDetails();
            List<MarkingScheme> lstmarkingSchemes = _markingSchemeService.GetMarkingSchemes(SessionHelper.CurrentSession.SchoolId).ToList();
            decimal calculatedMarks = 0;
            int mistakesCount = 0;
            foreach (var item in lstmarkingSchemes)
            {
                Regex reg = new Regex(@"\[" + item.MarkingName + "\\]");
                foreach (Match match in reg.Matches(textData))
                {
                    calculatedMarks = calculatedMarks + item.Weight;
                    mistakesCount = mistakesCount + 1;
                }
            }

            //List<DocumentReviewdetails> lstDocumentReviewdetails = new List<DocumentReviewdetails>();
            //List<TaskDocumentReviewdetails> lstTaskDocumentReviewdetails = new List<TaskDocumentReviewdetails>();
            //OperationDetails opUpdateDoc = new OperationDetails();
            //if (!isTask)
            //{
            //    lstDocumentReviewdetails.Add(new DocumentReviewdetails()
            //    {
            //        ReviewedFileId = ReviewedFileId,
            //        PeerReviewId = peerReviewId,
            //        StudentAsgFileId = FileId,
            //        ReviewedFileName = filePath,
            //        CalculatedMarks = calculatedMarks,
            //        MistakesCount = mistakesCount
            //    });

            //    if (lstDocumentReviewdetails.Count() > 0)
            //    {
            //        opUpdateDoc = _assignmentService.AddUpdateDocumentReviewDetails(lstDocumentReviewdetails);
            //    }
            //}
            //else
            //{
            //    lstTaskDocumentReviewdetails.Add(new TaskDocumentReviewdetails()
            //    {
            //        ReviewTaskFileId = ReviewedFileId,
            //        TaskPeerReviewId = peerReviewId,
            //        StudentTaskFileId = FileId,
            //        ReviewedFileName = filePath,
            //        CalculatedMarks = calculatedMarks,
            //        MistakesCount = mistakesCount
            //    });

            //    if (lstTaskDocumentReviewdetails.Count() > 0)
            //    {
            //        opUpdateDoc = _assignmentService.AddUpdateTaskDocumentReviewDetails(lstTaskDocumentReviewdetails);
            //    }
            //}
            return Json(calculatedMarks, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> SaveUpdatedmarks(string filePath, long peerReviewId, long FileId, long ReviewedFileId, bool isTask, string textData, string filename)
        {
            OperationDetails op = new OperationDetails();
            string uploadedFileURl = filePath;
            if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
            {
                AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                byte[] bytes = System.IO.File.ReadAllBytes(PhoenixConfiguration.Instance.WriteFilePath + (isTask ? Constants.StudentAsgReviewTaskFilesDir : Constants.StudentAsgReviewUploadedFilesDir) + "/School_" + SessionHelper.CurrentSession.SchoolId + "/User_" + SessionHelper.CurrentSession.Id + "/" + filename);
                uploadedFileURl = await azureHelper.ReplaceFileAsync(bytes, AzureBlobStorageHelper.GetBlobUrlFromQueryString(filePath));
            }
            List<MarkingScheme> lstmarkingSchemes = _markingSchemeService.GetMarkingSchemes(SessionHelper.CurrentSession.SchoolId).ToList();
            decimal calculatedMarks = 0;
            int mistakesCount = 0;
            foreach (var item in lstmarkingSchemes)
            {
                Regex reg = new Regex(@"\[" + item.MarkingName + "\\]");
                foreach (Match match in reg.Matches(textData))
                {
                    calculatedMarks = calculatedMarks + item.Weight;
                    mistakesCount = mistakesCount + 1;
                }
            }

            List<DocumentReviewdetails> lstDocumentReviewdetails = new List<DocumentReviewdetails>();
            List<TaskDocumentReviewdetails> lstTaskDocumentReviewdetails = new List<TaskDocumentReviewdetails>();
            OperationDetails opUpdateDoc = new OperationDetails();
            if (!isTask)
            {
                lstDocumentReviewdetails.Add(new DocumentReviewdetails()
                {
                    ReviewedFileId = ReviewedFileId,
                    PeerReviewId = peerReviewId,
                    StudentAsgFileId = FileId,
                    ReviewedFileName = uploadedFileURl,
                    CalculatedMarks = calculatedMarks,
                    MistakesCount = mistakesCount
                });

                if (lstDocumentReviewdetails.Count() > 0)
                {
                    opUpdateDoc = _assignmentService.AddUpdateDocumentReviewDetails(lstDocumentReviewdetails);
                }
            }
            else
            {
                lstTaskDocumentReviewdetails.Add(new TaskDocumentReviewdetails()
                {
                    ReviewTaskFileId = ReviewedFileId,
                    TaskPeerReviewId = peerReviewId,
                    StudentTaskFileId = FileId,
                    ReviewedFileName = uploadedFileURl,
                    CalculatedMarks = calculatedMarks,
                    MistakesCount = mistakesCount
                });

                if (lstTaskDocumentReviewdetails.Count() > 0)
                {
                    opUpdateDoc = _assignmentService.AddUpdateTaskDocumentReviewDetails(lstTaskDocumentReviewdetails);
                }
            }
            return Json(calculatedMarks, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckInconsistencyInReview(string filePath, List<MarkingScheme> markingData, string textData)
        {
            List<MarkingScheme> lstmarkingSchemes = _markingSchemeService.GetMarkingSchemes(SessionHelper.CurrentSession.SchoolId).ToList();
            bool IsConsistant = true;
            foreach (var item in lstmarkingSchemes)
            {
                decimal marksPerMarking = 0;
                Regex reg = new Regex(@"\[" + item.MarkingName + "\\]");
                foreach (Match match in reg.Matches(textData))
                {
                    marksPerMarking = marksPerMarking + item.Weight;
                }
                if (marksPerMarking != markingData.Where(i => i.MarkingSchemeId == item.MarkingSchemeId).FirstOrDefault().CalculatedWeight)
                {
                    IsConsistant = false;
                }
            }
            return Json(IsConsistant, JsonRequestBehavior.AllowGet);
        }

        public decimal CalculateMarksAsPerMarking(string text, MarkingScheme markingPattern)
        {
            decimal calculatedMarks = 0;
            Regex reg = new Regex(@"\[" + markingPattern.MarkingName + "\\]");
            foreach (Match match in reg.Matches(text))
            {
                Console.WriteLine(match.Value);
                calculatedMarks = calculatedMarks + markingPattern.Weight;
            }
            return calculatedMarks;
        }
        public async Task<string> UploadReviewedFiles(string studentAsgFilePath, string actualFileName)
        {
            if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
            {
                AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                string OldUserID = SessionHelper.CurrentSession.OldUserId.ToString();
                return await azureHelper.CopyToOtherLocationAsync(studentAsgFilePath, FileModulesConstants.StudentAssignmentFileReview, OldUserID);
            }
            else
            {
                SharePointHelper sh = new SharePointHelper();
                var cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                string OldUserID = SessionHelper.CurrentSession.OldUserId.ToString();
                return SharePointHelper.CopyToOtherLocation(ref cx, studentAsgFilePath, FileModulesConstants.StudentAssignmentFileReview, OldUserID);
            }
        }

        public JsonResult MarkAsCompleteReview(long peerReviewId)
        {
            bool bFlag = _assignmentService.MarkAsCompleteReview(peerReviewId, SessionHelper.CurrentSession.Id);
            return Json(bFlag, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetReviewStudentTaskDetails(string taskId, string studentId)
        {
            var taskID = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(taskId));
            var studentID = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(studentId));
            var studentTaskDetails = _assignmentService.GetStudentTaskDetails(taskID, studentID);
            var PeerTaskDetail = _assignmentService.GetPeerTaskDetail(studentID, taskID);
            studentTaskDetails.lstStudentTaskFile = _assignmentService.GetStudentTaskFiles(studentID, taskID).ToList();
            List<TaskDocumentReviewdetails> existingReviewedFiles = _assignmentService.GetTaskReviewedDocumentFiles(PeerTaskDetail.TaskPeerReviewId).ToList();
            //var studentAssignmentReviewStatus = _assignmentService.GetPeerAssignmentDetailByAssignmentStudentId(studentTaskDetails.StudentAssignmentId);
            //ViewBag.studentAssignmentReviewStatus = studentAssignmentReviewStatus.IsReviewCompleted;
            ViewBag.TaskPeerReview = PeerTaskDetail;
            List<TaskDocumentReviewdetails> lstDocumentReviewdetails = new List<TaskDocumentReviewdetails>();
            foreach (var item in studentTaskDetails.lstStudentTaskFile)
            {
                if (!existingReviewedFiles.Any(m => m.StudentTaskFileId == item.StudentTaskFileId))
                {
                    lstDocumentReviewdetails.Add(new TaskDocumentReviewdetails()
                    {
                        ReviewTaskFileId = 0,
                        TaskPeerReviewId = PeerTaskDetail.TaskPeerReviewId,
                        StudentTaskFileId = item.StudentTaskFileId,
                        ReviewedFileName = await UploadReviewedFiles(item.SharepointUploadedFileURL, item.FileName)
                    });
                }
            }
            OperationDetails opUpdateDoc = new OperationDetails();
            if (lstDocumentReviewdetails.Count() > 0)
            {
                opUpdateDoc = _assignmentService.AddUpdateTaskDocumentReviewDetails(lstDocumentReviewdetails);
            }
            lstDocumentReviewdetails.AddRange(existingReviewedFiles);
            ViewBag.FileTypes = _fileTypeList;
            var assignmentTask = _assignmentService.GetAssignmentTaskbyTaskId(taskID);
            assignmentTask.lstTaskFiles = _assignmentService.GetTaskFilesByTaskId(taskID).ToList();
            ViewBag.assignmentTask = assignmentTask;
            ViewBag.studentId = studentId;
            return View("_ReviewStudentTaskDetails", studentTaskDetails);
        }

        public async Task<ActionResult> PeersTaskUploadedDocument(string id)
        {
            var IdValue = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            var file = _assignmentService.GetTaskDocumentFileByStudTaskFileId(IdValue);
            var fileExtension = Path.GetExtension(file.ReviewedFileName).ToLower();
            if (fileExtension == ".doc" || fileExtension == ".docx" || fileExtension == ".rtf" || fileExtension == ".odt" || fileExtension == ".txt")
            {
                DevExpress.XtraRichEdit.DocumentFormat DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.Doc;
                switch (fileExtension)
                {
                    case ".doc":
                        DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.Doc;
                        break;
                    case ".docx":
                        DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.OpenXml;
                        break;
                    case ".rtf":
                        DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.Rtf;
                        break;
                    case ".txt":
                        DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.PlainText;
                        break;
                    case ".odt":
                        DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.OpenDocument;
                        break;
                }
                List<MarkingScheme> lstmarkingSchemes = _markingSchemeService.GetMarkingSchemes(SessionHelper.CurrentSession.SchoolId).ToList();
                RichEditDocumentServer wordProcessor = new RichEditDocumentServer();
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    Models.AzureBlobStorage.BlobFileInfo fileInfo = await azureHelper.DownloadFilesAsStreamAsync(AzureBlobStorageHelper.GetBlobUrlFromQueryString(file.ReviewedFileName));
                    wordProcessor.LoadDocument(fileInfo.Content, DocumentFormat);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    var cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    Microsoft.SharePoint.Client.FileInformation fileInfo = sh.DownloadFilesFromSharePointAsStream(ref cx, file.ReviewedFileName);
                    wordProcessor.LoadDocument(fileInfo.Stream, DocumentFormat);
                }
                string text = wordProcessor.Text;
                foreach (var item in lstmarkingSchemes)
                {
                    item.CalculatedWeight = CalculateMarksAsPerMarking(text, item);
                }
                ViewBag.MarkingSchemes = lstmarkingSchemes;
                return View(file);
            }
            else
            {
                var filedetails = _assignmentService.GetStudentTaskFile(file.StudentTaskFileId);
                return Redirect(filedetails.FilePath);
            }
        }

        public ActionResult CustomTaskReviewToolbarPartial(TaskDocumentReviewdetails objDocument, string documentDetails)
        {
            TaskDocumentReviewdetails model = new TaskDocumentReviewdetails();
            if (documentDetails == null)
            {
                model = objDocument;
            }
            else
            {
                TaskDocumentReviewdetails document = JsonConvert.DeserializeObject<TaskDocumentReviewdetails>(documentDetails);
                model = document;
            }
            return PartialView("CustomTaskReviewToolbarPartial", model);
        }
        public ActionResult CustomTaskReviewToolbar()
        {
            return View("CustomTaskReviewToolbar");
        }

        public JsonResult MarkAsCompleteTaskReview(long peerReviewId)
        {
            bool bFlag = _assignmentService.MarkAsCompleteTaskReview(peerReviewId, SessionHelper.CurrentSession.Id);
            return Json(bFlag, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CloudFileUpload
        [HttpPost]
        public ActionResult SaveCloudFiles(string fileIds, string lstAssignmentFiles)
        {
            var selectedFiles = JsonConvert.DeserializeObject<List<CloudFileListView>>(fileIds);
            List<AssignmentFile> fileNames = new List<AssignmentFile>();
            foreach (var item in selectedFiles)
            {
                item.FileExtension = string.IsNullOrEmpty(item.FileExtension) ? Helpers.CommonHelper.GetFileExtensionByMimeType(item.MimeType) : item.FileExtension;
                fileNames.Add(new AssignmentFile
                {
                    CloudFileId = item.Id,
                    FileName = item.Name,
                    IsCloudFile = true,
                    FileExtension = item.FileExtension,
                    UploadedFileName = item.WebViewLink,
                    DriveId = item.DriveId,
                    ParentFolderId = item.ParentFolderId,
                    ResourceFileTypeId = item.ResourceFileTypeId
                });
            }

            List<AssignmentFile> lstExistingFiles = new List<AssignmentFile>();
            if (!string.IsNullOrEmpty(lstAssignmentFiles))
            {
                lstExistingFiles = JsonConvert.DeserializeObject<List<AssignmentFile>>(lstAssignmentFiles);
            }
            lstExistingFiles.AddRange(fileNames);
            var result = new Object();
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_AssignmentFileList", lstExistingFiles);
            var contentList = new string[] { htmlContent };
            result = new
            {
                content = contentList,
                filenames = lstExistingFiles
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveTaskCloudFiles(string fileIds, string taskFilesDetails)
        {


            List<TaskFile> fileNames = new List<TaskFile>();
            if (taskFilesDetails != "")
            {
                fileNames = JsonConvert.DeserializeObject<List<TaskFile>>(taskFilesDetails);
            }
            var selectedFiles = JsonConvert.DeserializeObject<List<CloudFileListView>>(fileIds);
            foreach (var item in selectedFiles)
            {
                item.FileExtension = Helpers.CommonHelper.GetFileExtensionByMimeType(item.MimeType);
                fileNames.Add(new TaskFile
                {
                    CloudFileId = item.Id,
                    FileName = item.Name,
                    IsCloudFile = true,
                    FileExtension = item.FileExtension,
                    UploadedFileName = item.WebViewLink,
                    ShareableLinkFileURL = item.WebViewLink,
                    DriveId = item.DriveId,
                    ParentFolderId = item.ParentFolderId,
                    ResourceFileTypeId = item.ResourceFileTypeId
                });
            }
            var result = new Object();
            var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_TaskFileList", fileNames.DistinctBy(x => x.UploadedFileName).ToList());
            var contentList = new string[] { htmlContent };
            result = new
            {
                content = contentList,
                filenames = fileNames
            };
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Student CloudFileUpload


        [HttpPost]
        public async Task<ActionResult> SaveStudentCloudFiles(string fileIds, int studentId, int assignmentId, string createdByOneDriveId, string createdByGmailId)
        {
            var selectedFiles = JsonConvert.DeserializeObject<List<CloudFileListView>>(fileIds);
            List<StudentAssignmentFile> fileNames = new List<StudentAssignmentFile>();
            foreach (var item in selectedFiles)
            {
                item.FileExtension = string.IsNullOrEmpty(item.FileExtension) ? Helpers.CommonHelper.GetFileExtensionByMimeType(item.MimeType) : item.FileExtension;
                fileNames.Add(new StudentAssignmentFile
                {
                    CloudFileId = item.Id,
                    FileName = item.Name,
                    IsCloudFile = true,
                    FileExtension = item.FileExtension,
                    PathtoDownload = item.WebViewLink,
                    //SharepointUploadedFileURL = item.WebContentLink == "" ? item.WebViewLink : item.WebContentLink,
                    SharepointUploadedFileURL = item.WebViewLink,
                    DriveId = item.DriveId,
                    ParentFolderId = item.ParentFolderId,
                    ResourceFileTypeId = item.ResourceFileTypeId,
                    UploadedOn = DateTime.Now,
                    AssignmentId = assignmentId,
                    StudentId = studentId
                });
                if (item.ResourceFileTypeId == (short)ResourceFileTypes.OneDrive)
                {
                    await CloudFilesHelper.ShareOneDriveFileToTeacher(item.Id, createdByOneDriveId, permission: "write");
                }
                if (item.ResourceFileTypeId == (short)ResourceFileTypes.GoogleDrive && !string.IsNullOrEmpty(createdByGmailId))
                {
                    CancellationToken cancellationToken = new CancellationToken();
                    var authResult = new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).
                         AuthorizeAsync(cancellationToken).GetAwaiter().GetResult();
                    CloudFilesHelper.AddGoogleDriveTeacherPermission(authResult, createdByGmailId, item.Id, filePermission: "writer");
                }

            }

            var result = new Object();
            OperationDetails op = new OperationDetails();
            if (fileNames.Count > 0)
            {
                var visitorInfo = new VisitorInfo();
                string IpAddress = visitorInfo.GetIpAddress();
                string HostIpAddress = visitorInfo.GetClientIPAddress();
                string IpDetails = IpAddress + ":" + HostIpAddress;
                op.Success = _assignmentService.UploadStudentAssignmentFile(fileNames, studentId, assignmentId, IpDetails);
                if (op.Success)
                {
                    op.Message = ResourceManager.GetString("Shared.Messages.FileUploaded");
                }
                else
                {
                    op.Message = ResourceManager.GetString("Shared.Messages.ErrorUploadingFiles");
                }
            }
            return Json(op, JsonRequestBehavior.AllowGet);
            //var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_AssignmentFileList", lstExistingFiles);
            //var contentList = new string[] { htmlContent };
            //result = new
            //{
            //    content = contentList,
            //    filenames = lstExistingFiles
            //};
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> SaveStudentTaskCloudFiles(string fileIds, int studentId, string taskId, string createdByOneDriveId, string createdByGmailId)
        {
            int convertTaskId = !string.IsNullOrWhiteSpace(taskId) ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(taskId)) : 0;
            List<StudentTaskFile> fileNames = new List<StudentTaskFile>();
            OperationDetails op = new OperationDetails();
            //if (taskFilesDetails != "")
            //{
            //    fileNames = JsonConvert.DeserializeObject<List<TaskFile>>(taskFilesDetails);
            //}
            var selectedFiles = JsonConvert.DeserializeObject<List<CloudFileListView>>(fileIds);
            foreach (var item in selectedFiles)
            {
                item.FileExtension = Helpers.CommonHelper.GetFileExtensionByMimeType(item.MimeType);
                fileNames.Add(new StudentTaskFile
                {
                    CloudFileId = item.Id,
                    FileName = item.Name,
                    IsCloudFile = true,
                    FileExtension = item.FileExtension,
                    UploadedFileName = item.WebViewLink,
                    DriveId = item.DriveId,
                    ParentFolderId = item.ParentFolderId,
                    ResourceFileTypeId = item.ResourceFileTypeId,
                    CreateOn = DateTime.Now,
                    //SharepointUploadedFileURL = item.WebContentLink == "" ? item.WebViewLink : item.WebContentLink,
                    SharepointUploadedFileURL = item.WebViewLink,
                    TaskId = convertTaskId
                });
                if (item.ResourceFileTypeId == (short)ResourceFileTypes.OneDrive)
                {
                    await CloudFilesHelper.ShareOneDriveFileToTeacher(item.Id, createdByOneDriveId, permission: "write");
                }
                if (item.ResourceFileTypeId == (short)ResourceFileTypes.GoogleDrive && !string.IsNullOrEmpty(createdByGmailId))
                {
                    CancellationToken cancellationToken = new CancellationToken();
                    var authResult = new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).
                         AuthorizeAsync(cancellationToken).GetAwaiter().GetResult();
                    CloudFilesHelper.AddGoogleDriveTeacherPermission(authResult, createdByGmailId, item.Id, filePermission: "writer");
                }
            }

            if (fileNames.Count > 0)
            {
                var visitorInfo = new VisitorInfo();
                string IpAddress = visitorInfo.GetIpAddress();
                string HostIpAddress = visitorInfo.GetClientIPAddress();
                string IpDetails = IpAddress + ":" + HostIpAddress;
                op.Success = _assignmentService.UploadStudentTaskFiles(fileNames, studentId, convertTaskId, IpDetails);
                if (op.Success)
                {
                    op.Message = ResourceManager.GetString("Assignment.AssignmentMessages.StudentTaskFileSuccess");
                    op.CssClass = "success";
                }
                else
                {
                    op.Message = ResourceManager.GetString("Shared.Messages.TechnicalError");
                    op.CssClass = "error";
                }
            }
            return Json(op, JsonRequestBehavior.AllowGet);
            //var result = new Object();
            //var htmlContent = ControllerExtension.RenderPartialViewToString(this, "_TaskFileList", fileNames.DistinctBy(x => x.CloudFileId).ToList());
            //var contentList = new string[] { htmlContent };
            //result = new
            //{
            //    content = contentList,
            //    filenames = fileNames
            //};
            //return Json(result, JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region  Get TeacherList To Share Assignment
        public ActionResult GetTeacherListToShareAssignment(int AssignmentId)
        {
            var users = _userService.GetUserBySchool(SessionHelper.CurrentSession.SchoolId, 3);
            ViewBag.AssignmentId = AssignmentId;
            return PartialView("_MultiSelectTeacherList", users);
        }

        [HttpPost]
        public ActionResult PostTeachersToShare(string selectedTeachers, int AssignmentId)
        {
            OperationDetails op = new OperationDetails();
            int[] teacherIds = JsonConvert.DeserializeObject<int[]>(selectedTeachers);
            string teachers = string.Join(",", teacherIds);
            op = _assignmentService.ShareAssignment(AssignmentId, teachers);
            return Json(op, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult InsertStudentObjectiveFeedback(AssignmentStudentObjective studentObjective, List<HttpPostedFileBase> recordings)
        {
            if (recordings != null)
            {
                foreach (var item in recordings)
                {
                    var filesType = _fileService.GetFileTypes("Recording");
                    var fileModule = _fileService.GetFileManagementModules().ToList();
                    int i = 1;
                    Phoenix.Models.File file = new Phoenix.Models.File();
                    file.SchoolId = SessionHelper.CurrentSession.SchoolId;
                    file.ModuleId = fileModule.Where(x => x.ModuleName == "ObjectiveFeedback").Select(x => x.ModuleId).FirstOrDefault();
                    file.SectionId = studentObjective.StudentObjectiveMarkId;
                    file.FileName = SessionHelper.CurrentSession.FirstName + "_" + SessionHelper.CurrentSession.LastName + "_" + i + Path.GetExtension(item.FileName);
                    file.FileTypeId = filesType.Select(x => x.TypeId).FirstOrDefault();
                    file.FilePath = UploadQuizFeedbackResource(item);
                    i++;
                    studentObjective.Files.Add(file);
                }
            }

            var result = _assignmentService.InsertStudentObjectiveFeedback(studentObjective);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadReportToExcel")]
        public ActionResult DownloadReportDataAsExcelFile(FormCollection form)
        {
            var reportData = new AssignmentReport();
            string path = string.Empty;
            string downloadFileName = "";
            byte[] bytes;
            List<Phoenix.Models.SchoolGroup> lstSchoolGroups = new List<Phoenix.Models.SchoolGroup>();
            string reportName = "AssignmentReport";
            int assignmentId = Convert.ToInt32(Session["AssignmentIdForXls"]);
            Assignment objAssignment = _assignmentService.GetExcelReportDataAssignmentById(assignmentId);
            var Tasks = _assignmentService.GetAssignmentTasks(assignmentId).ToList();
            OperationDetails isAssignmentPermissions = new OperationDetails();
            isAssignmentPermissions = _assignmentService.GetAssignmentPermissions(assignmentId, SessionHelper.CurrentSession.Id);
            if ((objAssignment.CreatedById == SessionHelper.CurrentSession.Id) || SessionHelper.CurrentSession.IsAdmin || isAssignmentPermissions.Success == true)
            {
                //objAssignment.lstAssignmentStudent = _assignmentService.GetExcelReportDataStudentsByAssignmentId(assignmentId);
                reportData = _assignmentService.GetExcelReportDataStudentsByAssignmentId(assignmentId);
                lstSchoolGroups = _assignmentService.GetSelectedSchoolGroupsByAssignmentID(assignmentId).ToList();
            }

            path = Server.MapPath("~/Content/Files/EmptyReportFile.xls");

            switch (reportName)
            {
                case "AssignmentReport":
                    {
                        bytes = _assignmentService.GenerateAssignmentReportXLSFile(path, objAssignment, lstSchoolGroups, Tasks, reportData);
                        downloadFileName = "AssignmentReport.xls";
                        return File(bytes, "application/vnd.ms-excel", downloadFileName);
                    }
                default:
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public JsonResult ReSubmitAssignment(int studentId, int assignmentId)
        {
            OperationDetails op = new OperationDetails();
            bool isReSubmitAssignnment = true;
            op.Success = _assignmentService.MarkAsIncompleteAssignment(studentId, assignmentId, isReSubmitAssignnment);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AssignmentUnArchive(int assignmentId)
        {
            OperationDetails isAssignmentUnArchive = new OperationDetails();
            isAssignmentUnArchive = _assignmentService.AssignmentUnArchive(assignmentId);
            return Json(isAssignmentUnArchive.Success, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MarkSeenStudentAssignmentComments(AssignmentComment objAssignmentComment)
        {
            OperationDetails op = new OperationDetails();
            objAssignmentComment.IsCommentSeenByStudent = true;
            if (objAssignmentComment.CommentIds != null)
            {
                if (SessionHelper.CurrentSession.IsTeacher())
                {
                    objAssignmentComment.ByTeacher = true;
                }
                else if (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent())
                {
                    objAssignmentComment.ByTeacher = false;
                }
                op = _assignmentService.SeenStudentAssignmentComment(objAssignmentComment);
            }
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        public ActionResult assignmentCategory()
        {
            return View();
        }
        public ActionResult LoadAssignmentCategoryGrid()
        {
            if (!(SessionHelper.CurrentSession.IsAdmin))
            {
                return RedirectToAction("NoPermission", "Error");
            }
            string actionButtonsHtmlTemplate = "<div class='tbl-actions'><a class='mr-3' data-toggle='tooltip' data-placement='bottom' onclick='AssignmentCategoryConfiguration.addAssignmentCategoryPopup($(this),{0})' title='" + ResourceManager.GetString("Shared.Labels.EditRecord") + "'><img src='/Content/VLE/img/svg/tbl-edit.svg'></a></div>";
            string isActiveHtmlChecked = "<a class='mr-1' onclick='AssignmentCategoryConfiguration.PushAssignmentCategory({0})' title ='check'><input id='chk_{0}' style='position: inherit;' type='checkbox' checked/></a>";
            string isActiveHtmlUnChecked = "<a class='mr-1' onclick='AssignmentCategoryConfiguration.PushAssignmentCategory({0})' title ='check'><input id='chk_{0}' style='position: inherit;' type='checkbox'/></a>";
            var assignmentCategory = _assignmentService.GetAssignmentCategoryBySchoolId(SessionHelper.CurrentSession.SchoolId);
            var dataList = new object();
            dataList = new
            {
                aaData = (from item in assignmentCategory
                          select new
                          {
                              Actions = string.Format(actionButtonsHtmlTemplate, item.AssigntmentCatgoryId),
                              item.CategoryTitle,
                              item.CategoryDescription,
                              //IsActive = item.IsActive ? ResourceManager.GetString("Shared.Labels.Active") : ResourceManager.GetString("Shared.Labels.DeActive")
                              IsActive = item.IsActive ? string.Format(isActiveHtmlChecked, item.AssigntmentCatgoryId) : string.Format(isActiveHtmlUnChecked, item.AssigntmentCatgoryId)
                          }).ToArray()
            };
            return Json(dataList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddEditAssignmentCategory(int AssignmentCategoryId, bool IsActive)
        {
            OperationDetails op = new OperationDetails();
            op = _assignmentService.AddEditAssignmentCategory(AssignmentCategoryId, IsActive, SessionHelper.CurrentSession.SchoolId);
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddAssignmentCategoryMaster(AssignmentCategory model)
        {
            OperationDetails op = new OperationDetails();
            model.SchoolId = SessionHelper.CurrentSession.SchoolId;
            op = _assignmentService.AddAssignmentCategoryMaster(model.CategoryTitle, model.CategoryDescription, model.AssigntmentCatgoryId, model.IsActive, model.SchoolId);
            return Json(new OperationDetails(op.Success), JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitAssignmentCategoryMaster(int? id)
        {
            var model = new AssignmentCategory();
            if (id.HasValue)
            {
                var categoryDetails = _assignmentService.GetAssignmentCategoryMasterById(id.Value);
                EntityMapper<AssignmentCategoryDetails, AssignmentCategory>.Map(categoryDetails, model);
            }
            return PartialView("_AddAssignmentCategoryMaster", model);
        }

        public ActionResult TaskReport(string id)
        {
            var assignmentId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            var assignmentTasks = _assignmentService.GetAssignmentTaskQuizByAssignmentId(assignmentId);
            ViewBag.TaskList = new SelectList(assignmentTasks.Select(i => new Phoenix.Common.Models.ListItem()
            {
                ItemName = i.TaskTitle + "( " + ResourceManager.GetString("Assignment.Assignment.Task") + " )" + i.QuizName + "( " + ResourceManager.GetString("Assignment.Assignment.Quiz") + " )",
                ItemId = i.TaskId.ToString()
            }).ToList(), "ItemId", "ItemName");
            return View();
        }

        public ActionResult QuizReport(string taskId)
        {
            var assignmentReportQuiz = _assignmentService.GetAssignmentQuizDetailsByTaskId(Convert.ToInt32(taskId));
            Session["questionWiseReport"] = assignmentReportQuiz.QuestionWiseReport;
            return PartialView("_TaskReportPartial", assignmentReportQuiz);
        }


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadTaskReportToExcel")]
        public ActionResult DownloadTaskReportDataAsExcelFile(FormCollection form)
        {
            string path = string.Empty;
            string downloadFileName = "";
            byte[] bytes;
            string reportName = form["reportName"];
            var questionWiseReport = Session["questionWiseReport"] as List<QuestionWiseReport>;

            path = Server.MapPath("~/Content/Files/EmptyReportFile.xls");

            switch (reportName)
            {
                case "QuestionWiseReport":
                    {
                        bytes = _quizService.GenerateQuestionWiseReportXLSFile(path, questionWiseReport);
                        downloadFileName = "QuestionWiseReport.xls";
                        return File(bytes, "application/vnd.ms-excel", downloadFileName);
                    }
                default:
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadTaskReportToPDF")]
        public ActionResult DownloadTaskReportDataAsPDF(FormCollection form)
        {
            string pdfhtml = "";
            string pdfTitle = "";
            string downloadFileName = "";
            string reportName = form["reportName"];
            ViewBag.ReportType = "PDF";
            var questionWiseReport = Session["questionWiseReport"] as List<QuestionWiseReport>;

            switch (reportName)
            {
                case "QuestionWiseReport":
                    {
                        pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_QuestionWiseReport", questionWiseReport);
                        pdfTitle = ResourceManager.GetString("Quiz.Report.QuestionWiseReport");
                        downloadFileName = "QuestionWiseReport.pdf";
                        break;
                    }
            }

            PdfDocument pdf = new PdfDocument();

            pdf = PdfGenerator.GeneratePdf(pdfhtml, PageSize.A4);
            pdf.Info.Title = pdfTitle;
            MemoryStream stream = new MemoryStream();
            pdf.Save(stream, false);
            byte[] file = stream.ToArray();
            stream.Write(file, 0, file.Length);
            stream.Position = 0;

            return File(stream, "application/pdf", downloadFileName);

        }





        public ActionResult InitSaveAssignmentMarks(int assignmentStudentId, decimal? submitMarks, decimal AssignmentTotakMarks)
        {
            var model = new AssignmentStudent();
            model = _assignmentService.GetAssignmentSubmitMarksByAssignmentStudentId(assignmentStudentId);
            model.AssignmentTotakMarks = AssignmentTotakMarks;
            model.AssignmentStudentId = assignmentStudentId;
            return PartialView("_SaveAssignmentSubmitMarks", model);
        }

        public ActionResult InitSaveTaskMarks(int studentTaskId, int assignmentStudentId, decimal? submitMarks, decimal TaskTotalMarks)
        {
            var model = new StudentTask();
            model = _assignmentService.GetTaskSubmitMarksByTaskId(studentTaskId);
            model.StudentTaskId = studentTaskId;
            model.TaskTotalMarks = TaskTotalMarks;
            model.StudentAssignmentId = assignmentStudentId;
            return PartialView("_SaveTaskSubmitMarks", model);
        }

        /// <summary>
        /// Created By - Rohit Patil
        /// Created Date - 11 Jan 2021
        /// Description - retreive achived assignments - actions only for admins.
        /// </summary>
        /// <param name="assignmentIds"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UndoAssignmentArchive(string assignmentIds)
        {
            var op = new OperationDetails();
            if (!SessionHelper.CurrentSession.IsAdmin)
            {
                op.Message = LocalizationHelper.ActionNotPermittedMessage;
                op.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                op.Success = false;
                return Json(op, JsonRequestBehavior.AllowGet);
            }
            var model = new ArchiveAssignment();
            model.AssignmentIds = assignmentIds;
            model.UpdatedBy = SessionHelper.CurrentSession.Id;
            var result = _assignmentService.UndoAssignmentsArchive(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
    }
}



