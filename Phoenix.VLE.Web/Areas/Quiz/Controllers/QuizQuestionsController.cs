﻿using OfficeOpenXml;
using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Web.Helpers;
using Phoenix.VLE.Web.ViewModels;
using System.Text.RegularExpressions;
using System.Threading;

namespace Phoenix.VLE.Web.Areas.Quiz.Controllers
{
    [Authorize]
    public class QuizQuestionsController : BaseController
    {
        private readonly IQuizQuestionsService _quizQuestionsService;
        private readonly IQuizService _quizService;
        private ISubjectService _subjectService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly IFileService _fileService;
        private List<VLEFileType> _fileTypeList;
        public QuizQuestionsController(IQuizQuestionsService quizQuestionsService, IFileService fileService,
                                    ISubjectService subjectService, IQuizService quizService, IUserPermissionService userPermissionService)
        {
            _quizQuestionsService = quizQuestionsService;
            _quizService = quizService;
            _fileService = fileService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
            _subjectService = subjectService;
            _userPermissionService = userPermissionService;
        }
        // GET: Quiz/QuizQuestions
        public ActionResult Index()
        {
            int quizId = Session["SelectedQuizId"].ToInteger();
            Session["quizId"] = quizId;
            var questions = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            Session["quizQuestionDetails"] = _quizService.GetQuiz(quizId);
            return View(questions);
        }
        public ActionResult PoolQuestions(string quiz)
        {
            var model = new QuizQuestionsEdit();
            Session["lstObjective"] = new List<Objective>();
            model.QuizId = Convert.ToInt32(EncryptDecryptHelper.DecodeBase64(quiz));
            var listOfQuestions = _quizQuestionsService.GetAllQuizQuestions().ToList();
            var existingQuestions = _quizQuestionsService.GetQuizQuestionsByQuizId(model.QuizId);
            model.UnAssignedQuestionList.AddRange(listOfQuestions.Where(r => !existingQuestions.Any(a => r.QuizQuestionId == a.QuizQuestionId)).Select(r => new ListItem
            {
                ItemId = $"{r.QuizQuestionId}",
                ItemName = r.QuestionText.ToString()
            }).OrderBy(r => r.ItemName));
            model.AssignedQuestionList.AddRange(existingQuestions.Select(r => new ListItem
            {
                ItemId = $"{r.QuizQuestionId}",
                ItemName = r.QuestionText.ToString()
            }).OrderBy(r => r.ItemName));
            List<ListItem> lstCourse = new List<ListItem>();
            lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
            var courses = lstCourse.Select(x => new SelectListItem
            {
                Value = x.ItemId.ToString(),
                Text = x.ItemName,
            }).ToList();
            ViewBag.CourseList = courses;
            return View("PoolQuestions", model);
        }
        public ActionResult AddQuizQuestion()
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);
            ViewBag.QuestionType = SelectListHelper.GetSelectListData(ListItems.QuestionType);
            var model = new QuizQuestionsEdit();
            model.QuizId = (int)Session["quizId"];
            model.IsAddMode = true;
            model.IsRequired = true;
            model.Marks = 1;
            var quizModel = _quizService.GetQuiz(model.QuizId);
            model.QuizName = quizModel.QuizName;
            model.QuizImagePath = quizModel.QuizImagePath;
            ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            //ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName");
            Session["QuizQuestionFiles"] = new List<QuizQuestionFiles>();
            Session["lstObjective"] = new List<Objective>();
            List<ListItem> lstCourse = new List<ListItem>();
            lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
            var courses = lstCourse.Select(x => new SelectListItem
            {
                Value = x.ItemId.ToString(),
                Text = x.ItemName,
            }).ToList();
            ViewBag.CourseList = courses;
            return View("AddQuizQuestion", model);
        }
        //public ActionResult SaveQuizQuestion(QuizQuestionsEdit model, HttpPostedFileBase QuizImage)
        [HttpPost, ValidateInput(false)]
        public ActionResult SaveQuizQuestion(QuizQuestionsEdit model, List<HttpPostedFileBase> recordings)
        {
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            var isCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            if (!isCustomPermission)
            {
                return Json(new OperationDetails(false, LocalizationHelper.ActionNotPermittedMessage), JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrWhiteSpace(model.QuestionText))
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (model.QuestionText.Contains("table"))
                {
                    model.QuestionText = model.QuestionText.Replace("\n", " ");
                    model.QuestionText = model.QuestionText.Replace("&nbsp;", " ");
                }
                else
                {
                    model.QuestionText = model.QuestionText.Replace("\n", "</br>");
                    model.QuestionText = model.QuestionText.Replace("&nbsp;", " ");
                    //model.QuestionText = HttpUtility.HtmlDecode(model.QuestionText);
                    //string tagsreplaced = Regex.Replace(decode, "<.*?>", " ");
                    //model.QuestionText = tagsreplaced.Normalize();
                }
            }
            model.CreatedBy = (int)SessionHelper.CurrentSession.Id;
            model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            List<Objective> lstObjectives = new List<Objective>();
            if (Session["lstObjective"] != null)
            {
                lstObjectives = Session["lstObjective"] as List<Objective>;
                Session["lstObjective"] = null;
            }
            model.lstDocuments = Session["QuizQuestionFiles"] as List<QuizQuestionFiles>;
            int i = 1;
            if (recordings != null)
            {
                foreach (var item in recordings)
                {

                    var quizQuestionFiles = new QuizQuestionFiles();
                    quizQuestionFiles.QuizQuestionId = model.QuizQuestionId;
                    quizQuestionFiles.FileName = SessionHelper.CurrentSession.FirstName + "_" + SessionHelper.CurrentSession.LastName + "_" + i + Path.GetExtension(item.FileName);
                    quizQuestionFiles.CreatedBy = (int)SessionHelper.CurrentSession.Id;
                    quizQuestionFiles.UploadedFileName = quizQuestionFiles.FileName;
                    quizQuestionFiles.FilePath = UploadQuizFeedbackResource(item);
                    quizQuestionFiles.PhysicalPath = quizQuestionFiles.FilePath;
                    model.lstDocuments.Add(quizQuestionFiles);
                    i++;
                }
            }
            model.lstObjectives = lstObjectives;
            var result = _quizQuestionsService.QuizQuestionsCUD(model, lstObjectives);
            TempData["IsQuizQuestion"] = result;
            Session["QuizQuestionFiles"] = new List<QuizQuestionFiles>();
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        private string UploadQuizFeedbackResource(HttpPostedFileBase resourceFile)
        {
            string GUID = Guid.NewGuid().ToString();
            string filePath = String.Empty;
            string strFile = String.Format("{0}{1}", GUID, Path.GetExtension(resourceFile.FileName));
            //string relativeFilePath = String.Empty;
            //string strFile = suggestionFile.FileName;
            string relativeFilePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizQuestion;
            //relativeFilePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizFeedbackDir;
            //filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizFeedbackDir + "/User_" + SessionHelper.CurrentSession.Id;
            //Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
            Common.Helpers.CommonHelper.CreateDestinationFolder(relativeFilePath);
            //Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
            filePath = Path.Combine(relativeFilePath, strFile);
            //relativeFilePath = Constants.PlannerDir + "/User_" + SessionHelper.CurrentSession.Id;
            string path = Constants.QuizQuestion + "/" + strFile;
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            resourceFile.SaveAs(filePath);
            return path;
        }
        [HttpPost]
        public ActionResult DeleteQuizQuestionFile(string id, string quizQuestionId, string fileName, string UploadedFilePath)
        {
            string relativepath = string.Empty;
            List<QuizQuestionFiles> lstfiles = new List<QuizQuestionFiles>();
            if (id == "0")
            {
                lstfiles = Session["QuizQuestionFiles"] as List<QuizQuestionFiles>;
                var file = lstfiles.FirstOrDefault(e => e.FileName.ToUpper().Trim() == fileName.ToUpper().Trim());
                if (file != null)
                {
                    lstfiles.Remove(file);
                    if (System.IO.File.Exists(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath))
                    {
                        System.IO.File.Delete(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath);
                    }
                    Session["QuizQuestionFiles"] = lstfiles;
                }
            }
            else
            {
                if (Session["QuizQuestionFiles"] != null)
                {
                    lstfiles = Session["QuizQuestionFiles"] as List<QuizQuestionFiles>;
                    var file = lstfiles.FirstOrDefault(e => e.QuizQuestionFileId == Convert.ToInt32(id));
                    if (file != null)
                    {
                        lstfiles.Remove(file);
                        if (System.IO.File.Exists(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath))
                        {
                            System.IO.File.Delete(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath);
                        }
                        Session["QuizQuestionFiles"] = lstfiles;
                    }
                }
                var result = _quizQuestionsService.DeleteQuizQuestionFile(Convert.ToInt32(id));
                lstfiles = _quizQuestionsService.GetFilesByQuizQuestionId(Convert.ToInt32(quizQuestionId));
            }

            return PartialView("_QuizQuestionFileList", lstfiles);
        }
        public ActionResult GetuploadedFileDetails(int? id, bool isTask)
        {

            List<QuizQuestionFiles> lstfiles = Session["QuizQuestionFiles"] as List<QuizQuestionFiles>;
            //if (id!=0)
            //{
            //    List<QuizQuestionFiles> lst = _quizQuestionsService.GetFilesByQuizQuestionId((int)id);
            //    lstfiles.AddRange(lst);
            //}
            return PartialView("_QuizQuestionFileList", lstfiles);
        }

        //Commented for share point changes by vinayak 

        //[HttpPost]
        //public ActionResult UploadQuizQuestionFiles()
        //{
        //    List<QuizQuestionFiles> fileNames = new List<QuizQuestionFiles>();
        //    if (Request.Files.Count > 0)
        //    {
        //        try
        //        {
        //            HttpFileCollectionBase files = Request.Files;
        //            for (int i = 0; i < files.Count; i++)
        //            {
        //                HttpPostedFileBase file = files[i];
        //                string fname = file.FileName;

        //                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
        //                {
        //                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
        //                    fname = testfiles[testfiles.Length - 1];
        //                }
        //                else
        //                {
        //                    fname = file.FileName;
        //                }
        //                var extension = System.IO.Path.GetExtension(file.FileName);
        //                SharePointHelper sh = new SharePointHelper();
        //                Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
        //                SharePointFileView shv = new SharePointFileView();
        //                shv = SharePointHelper.UploadFilesAsPerModule(ref cx, FileModulesConstants.QuizQuestion, SessionHelper.CurrentSession.OldUserId.ToString(), file);

        //                fileNames.Add(new QuizQuestionFiles
        //                {
        //                    FileName = file.FileName,
        //                    FileExtension = extension.Replace(".", ""),
        //                    UploadedFileName = file.FileName,
        //                    FilePath = shv.ShareableLink,
        //                    PhysicalPath = shv.SharepointUploadedFileURL
        //                });
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //        if (Session["QuizQuestionFiles"] == null)
        //        {
        //            Session["QuizQuestionFiles"] = fileNames;
        //        }
        //        else
        //        {
        //            List<QuizQuestionFiles> lstExistingFiles = new List<QuizQuestionFiles>();
        //            lstExistingFiles = Session["QuizQuestionFiles"] as List<QuizQuestionFiles>;
        //            lstExistingFiles.AddRange(fileNames);
        //            Session["QuizQuestionFiles"] = lstExistingFiles;
        //        }
        //    }
        //    return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult UploadQuizQuestionFiles()
        {

            List<QuizQuestionFiles> fileNames = new List<QuizQuestionFiles>();
            string relativePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Get all files from Request object
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;

                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        var extension = System.IO.Path.GetExtension(file.FileName);


                        //content for file upload
                        string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                        string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizQuestion;
                        //string fileModule = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFilesDir;
                        string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizQuestion;

                        Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                        Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);

                        Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                        fname = Guid.NewGuid() + "_" + fname;
                        relativePath = Constants.QuizQuestion + "/" + fname;
                        fname = Path.Combine(filePath, fname);
                        file.SaveAs(fname);
                        fileNames.Add(new QuizQuestionFiles
                        {
                            FileName = file.FileName,
                            FileExtension = extension.Replace(".", ""),
                            UploadedFileName = relativePath,
                            FilePath= relativePath,
                            PhysicalPath= relativePath
                        });
                    }
                }
                catch (Exception ex)
                {
                }
                if (Session["QuizQuestionFiles"] == null)
                {
                    Session["QuizQuestionFiles"] = fileNames;
                }
                else
                {
                    List<QuizQuestionFiles> lstExistingFiles = new List<QuizQuestionFiles>();
                    lstExistingFiles = Session["QuizQuestionFiles"] as List<QuizQuestionFiles>;
                    lstExistingFiles.AddRange(fileNames);
                    Session["QuizQuestionFiles"] = lstExistingFiles;
                }
            }
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult UploadMTPFiles()
        {
            string SavedFileName = String.Empty;
            List<QuizQuestionFiles> fileNames = new List<QuizQuestionFiles>();
            string relativePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Get all files from Request object
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;

                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        var extension = System.IO.Path.GetExtension(file.FileName);


                        //content for file upload
                        string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
                        string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizMTPQuestion;
                        //string fileModule = PhoenixConfiguration.Instance.WriteFilePath + Constants.AssignmentFilesDir;
                        string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizMTPQuestion;

                        Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                        Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);

                        Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
                        fname = Guid.NewGuid() + "_" + fname;
                        SavedFileName = fname;
                        relativePath = Constants.QuizQuestion + "/" + fname;
                        fname = Path.Combine(filePath, fname);
                        file.SaveAs(fname);
                        fileNames.Add(new QuizQuestionFiles
                        {
                            FileName = file.FileName,
                            FileExtension = extension.Replace(".", ""),
                            UploadedFileName = relativePath
                        });
                    }
                }
                catch (Exception ex)
                {
                }
                if (Session["QuizMTPQuestionFiles"] == null)
                {
                    Session["QuizMTPQuestionFiles"] = fileNames;
                }
                else
                {
                    List<QuizQuestionFiles> lstExistingFiles = new List<QuizQuestionFiles>();
                    lstExistingFiles = Session["QuizMTPQuestionFiles"] as List<QuizQuestionFiles>;
                    lstExistingFiles.AddRange(fileNames);
                    Session["QuizMTPQuestionFiles"] = lstExistingFiles;
                }
            }
            return Json(SavedFileName, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditQuizQuestion(string id)
        {
            int quiQuestionId = Convert.ToInt32(EncryptDecryptHelper.DecodeBase64(id));
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            var question = _quizQuestionsService.GetQuizQuestionById(quiQuestionId);
            question.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(quiQuestionId);
            ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            ViewBag.QuestionType = SelectListHelper.GetSelectListData(ListItems.QuestionType);
            question.QuestionText = question.QuestionText.Replace("</br>", "\n");
            question.QuizId = (int)Session["quizId"];
            //ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName");

            List<ListItem> lstCourse = new List<ListItem>();
            lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
            var courses = lstCourse.Select(x => new SelectListItem
            {
                Value = x.ItemId.ToString(),
                Text = x.ItemName,
            }).ToList();
            ViewBag.CourseList = courses;
            question.CourseId = question.lstCourse.Select(m => m.CourseId.ToString()).ToList();
            question.lstObjective = _quizQuestionsService.GetQuestionObjectives(quiQuestionId).ToList();
            Session["lstObjective"] = question.lstObjective;
            Session["QuizQuestionFiles"] = question.lstDocuments;
            return View("EditQuizQuestion", question);
        }
        public ActionResult AddCorrectAnswer(int id)
        {
            var answerList = _quizQuestionsService.GetQuizAnswerByQuizQuestionId(id);
            return PartialView("_AddCorrectAnswer", answerList);
        }
        [HttpPost]
        public ActionResult AddCorrectAnswer(int id, int response, int questionType, int questionId)
        {
            var model = new QuizAnswersEdit();
            model.QuizAnswerId = id;
            model.IsCorrectAnswer = Convert.ToBoolean(response);
            model.QuestionTypeId = questionType;
            model.QuizQuestionId = questionId;
            var result = _quizQuestionsService.UpdateQuizCorrectAnswerByQuizAnswerId(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteQuizQuestionData(int id)
        {
            var model = new QuizQuestionsEdit();
            model.QuizQuestionId = id;
            model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            var result = _quizQuestionsService.QuizQuestionsDelete(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteQuizQuestion(int QuizQuestionId, int QuizId)
        {
            var result = _quizQuestionsService.DeleteQuizQuestion(QuizQuestionId, QuizId);
            return Json(new OperationDetails(result > 0), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SortQuestionByOrder(QuizQuestionsEdit quizQuestion)
        {
            quizQuestion.SortedQuizQuestionIds = quizQuestion.SortedQuestionIds.Split(',').ToList();
            var result = _quizQuestionsService.SortQuestionByOrder(quizQuestion);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        public ActionResult loadTopicSubtopic(string[] subjectIds)
        {
            ViewBag.SubjectIds = string.Join(",", subjectIds);
            List<SelectListItem> lessonsList = new List<SelectListItem>();
            ViewBag.Lessons = lessonsList;
            List<Objective> lstselectedObjective = Session["lstObjective"] as List<Objective>;
            return PartialView("_TopicSubtopicStructure", lstselectedObjective);
        }
        public ActionResult filterQuestionWithSubjectAndCurriculum(string[] courseIds, int quizId)
        {
            List<ListItem> lstFilteredQuestions = new List<ListItem>();
            var CourseIds = string.Join(",", courseIds);
            List<Objective> lstselectedObjective = Session["lstObjective"] != null ? Session["lstObjective"] as List<Objective> : new List<Objective>();
            //if (lstselectedObjective.Any())
            //{
            string lstObjectives = lstselectedObjective.Any() ? string.Join(",", lstselectedObjective.Select(c => c.ObjectiveId.ToString()).ToArray<string>()) : String.Empty;
            var listOfQuestions = _quizQuestionsService.GetFilteredQuizQuestions(CourseIds, lstObjectives).ToList();
            var existingQuestions = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            lstFilteredQuestions.AddRange(listOfQuestions.Where(r => !existingQuestions.Any(a => r.QuizQuestionId == a.QuizQuestionId)).Select(r => new ListItem
            {
                ItemId = $"{r.QuizQuestionId}",
                ItemName = r.QuestionText.ToString()
            }).OrderBy(r => r.ItemName));
            //}
            return PartialView("_GetUnAssignedQuestions", lstFilteredQuestions);
        }
        public ActionResult ClearUnAssignedQuestions(int quizId)
        {
            //model.QuizId = Convert.ToInt32(EncryptDecryptHelper.DecodeBase64(quiz));
            var UnAssignedQuestionList = new List<ListItem>();
            var listOfQuestions = _quizQuestionsService.GetAllQuizQuestions().ToList();
            var existingQuestions = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            UnAssignedQuestionList.AddRange(listOfQuestions.Where(r => !existingQuestions.Any(a => r.QuizQuestionId == a.QuizQuestionId)).Select(r => new ListItem
            {
                ItemId = $"{r.QuizQuestionId}",
                ItemName = r.QuestionText.ToString()
            }).OrderBy(r => r.ItemName));
            return PartialView("_GetUnAssignedQuestions", UnAssignedQuestionList);
        }
        public ActionResult UpdateObjectives(List<Objective> mappingDetails)
        {
            var operationDetails = new OperationDetails();
            List<Objective> lstObjectives = new List<Objective>();
            if (Session["lstObjective"] == null)
            {
                Session["lstObjective"] = mappingDetails;
            }
            else
            {
                Objective obj = mappingDetails.FirstOrDefault();
                lstObjectives = Session["lstObjective"] as List<Objective>;
                lstObjectives = lstObjectives.Where(m => m.ObjectiveId != obj.ObjectiveId).ToList();
                Session["lstObjective"] = lstObjectives;
            }
            operationDetails.Success = false;
            operationDetails.Message = "";
            return PartialView("_LoadObjectives", lstObjectives);
        }

        public ActionResult DisselectAllObjectives()
        {
            var operationDetails = new OperationDetails();
            var lstObjectives = new List<Objective>();
            Session["lstObjective"] = lstObjectives;
            operationDetails.Success = false;
            operationDetails.Message = "";
            return PartialView("_LoadObjectives", lstObjectives);
        }

        public JsonResult GetTopicSubTopicStructure(string subjectIds)
        {
            var TopicList = _subjectService.GetTopicSubTopicStructureByMultipleSubjects(subjectIds).ToList();
            List<TopicTreeItem> treeList = new List<TopicTreeItem>();
            TopicList.ForEach(l => treeList.Add(new TopicTreeItem() { id = l.SubSyllabusId, parentId = l.ParentId, name = l.SubSyllabusDescription, isLesson = l.isLesson, SubjectId = l.SubjectId }));
            return Json(treeList);
        }

        public ActionResult GetSubtopicObjectives(string id, string subjectIds, bool isLesson)
        {

            List<Objective> lstObjective = new List<Objective>();
            bool isMainSyllabus = false;
            if (id.Contains('x'))
            {
                isMainSyllabus = true;
            }
            int topicId = Convert.ToInt32(id.Replace("x", ""));
            lstObjective = _subjectService.GetSubtopicObjectivesWithMultipleSubjects(topicId, isMainSyllabus, subjectIds, isLesson).ToList();
            List<Objective> lstSelectedObjective = new List<Objective>();
            if (Session["lstObjective"] == null)
            {
                Session["lstObjective"] = lstObjective;
                lstSelectedObjective = lstObjective;
            }
            else
            {
                lstSelectedObjective = Session["lstObjective"] as List<Objective>;
                List<Objective> tempList = new List<Objective>();
                foreach (var item in lstObjective)
                {
                    Objective obj = lstSelectedObjective.Where(m => m.ObjectiveId == item.ObjectiveId).FirstOrDefault();
                    if (obj == null)
                    {
                        lstSelectedObjective.Add(item);
                    }
                }
                // lstSelectedObjective.AddRange(lstObjective);
                Session["lstObjective"] = lstSelectedObjective;

            }
            return PartialView("_LoadObjectives", lstSelectedObjective);
        }

        public ActionResult SaveObjectives(List<Objective> mappingDetails)
        {
            mappingDetails = mappingDetails == null ? new List<Objective>() : mappingDetails;
            Session["lstObjective"] = mappingDetails;
            return PartialView("_ShowSelectedObjective", mappingDetails);
        }
        public ActionResult FilterObjectives(List<Objective> mappingDetails, string[] subjectIds)
        {
            if (mappingDetails == null)
            {
                mappingDetails = new List<Objective>();
            }
            if (subjectIds != null)
            {
                List<Objective> lstobjectives = Session["lstObjective"] as List<Objective>;
                foreach (var item in subjectIds)
                {
                    var objectives = lstobjectives.Where(x => x.SubjectId == Convert.ToInt32(item)).ToList();
                    if (objectives.Any())
                        mappingDetails.AddRange(objectives);
                }
            }
            Session["lstObjective"] = mappingDetails;
            return PartialView("_ShowSelectedObjective", mappingDetails);
        }
        public ActionResult DeleteObjective(int objectiveId)
        {
            var operationDetails = new OperationDetails();
            List<Objective> lstobjectives = Session["lstObjective"] as List<Objective>;
            lstobjectives = lstobjectives.Where(m => m.ObjectiveId != objectiveId).ToList();
            Session["lstObjective"] = lstobjectives;
            operationDetails.Success = true; ;
            return PartialView("_ShowSelectedObjective", lstobjectives);
        }
        public ActionResult GetUnitStructureView(string courseIds)
        {
            var lstselectedObjective = new List<Objective>();
            var TopicList = _subjectService.GetUnitStructure(courseIds).ToList();
            if (Session["lstObjective"] != null)
            {
                lstselectedObjective = Session["lstObjective"] as List<Objective>;
                TopicList.ForEach(x => { x.IsSelected = lstselectedObjective.Any(d => d.ObjectiveId.ToString() == x.SubSyllabusId); });
            }
            return PartialView("_UnitStructureView", TopicList);
        }
        [HttpPost]
        public ActionResult InsertUpdatePoolQuestions(QuizQuestionsEdit model)
        {
            var result = _quizQuestionsService.InsertUpdatePoolQuestions(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadImportQuizPopup()
        {
            return PartialView("_ImportQuizPopup");
        }
      
        [HttpPost]
        public ActionResult DeleteMTPResource(int ResourceId)
        {
            var result = _quizQuestionsService.DeleteMTPResource(ResourceId);
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ImportQuizQuestions()
        {
            try
            {
                OperationDetails operationDetails = new OperationDetails();
                var importQuiz = new ImportQuiz();
                bool IsValid = true;
                int tempQuestionTypeId = 0;
                bool RequiredAnswer = true;
                const string Remarks = "Remarks";
                //var quizQuestionsList = new List<QuizQuestionsEdit>();
                //var quizAnswers = new List<Answers>();
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase UploadQuizFile = files[i];


                        operationDetails.Success = true;
                        operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                        if (UploadQuizFile != null)
                        {
                            ExcelPackage package = new ExcelPackage(UploadQuizFile.InputStream);
                            DataTable excelDataTable = new DataTable();
                            try
                            {
                                excelDataTable = package.ToDataTable();
                            }
                            catch (Exception ex)
                            {
                                operationDetails.Success = true;
                                operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                                operationDetails.Message = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Messages.fileInvalidFormatMessage");
                                return Json(operationDetails, JsonRequestBehavior.AllowGet);
                            }
                            bool isFirstEmptyRow = false;
                            if (excelDataTable.Columns.Count != 7)
                            {
                                operationDetails.Success = true;
                                operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                                operationDetails.Message = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Messages.fileInvalidFormatMessage");
                                return Json(operationDetails, JsonRequestBehavior.AllowGet);
                            }
                            //Delete first row if it contains all NULL values
                            if (excelDataTable.Rows.Count == 1)
                            {
                                DataRow firstRow = excelDataTable.Rows[0];

                                if (firstRow[0] == DBNull.Value
                                        && firstRow[1] == DBNull.Value
                                        && firstRow[2] == DBNull.Value
                                        && firstRow[3] == DBNull.Value
                                        && firstRow[4] == DBNull.Value)
                                {
                                    isFirstEmptyRow = true;
                                }

                                if (isFirstEmptyRow)
                                {
                                    excelDataTable.Rows.RemoveAt(0);
                                }
                            }

                            //Check if empty file or only headers
                            if (excelDataTable.Rows.Count == 0)
                            {
                                operationDetails.Message = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Messages.NoData");
                                return Json(operationDetails, JsonRequestBehavior.AllowGet);
                            }
                            int rowIndex = 0;
                            int tempQuizQuestionId = 0;
                            //remove remarks column if exist
                            DataColumnCollection columns = excelDataTable.Columns;
                            if (columns.Contains(Remarks))
                            {
                                excelDataTable.Columns.Remove(Remarks);
                            }

                            excelDataTable.Columns.Add(Remarks);
                            //int IndexNo = 1;
                            //foreach (DataRow dr in excelDataTable.Rows)
                            //{
                            //    var quizQuestion = new QuizQuestionsEdit();
                            //    if (dr.ItemArray[0].ToString() != String.Empty)
                            //    {
                            //        if (dr.ItemArray[0].ToString().Trim() != IndexNo.ToString())
                            //        {
                            //            IsValid = false;
                            //            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.InvalidQuestionNo");
                            //        }
                            //        quizQuestion.QuizQuestionId = rowIndex + 1;
                            //        quizQuestion.QuestionText = dr.ItemArray[2].ToString().Replace("\n", "</br>");
                            //        if (quizQuestion.QuestionText == String.Empty)
                            //        {
                            //            IsValid = false;
                            //            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.EmptyQuizQuestion");
                            //        }
                            //        string questionType = dr.ItemArray[1].ToString() != String.Empty ? dr.ItemArray[1].ToString().ToLower().Replace(" ", "") : String.Empty;
                            //        if (questionType != String.Empty)
                            //        {
                            //            quizQuestion.QuestionTypeId = _quizQuestionsService.GetIdByQuestionType(questionType);
                            //            tempQuestionTypeId = quizQuestion.QuestionTypeId;
                            //        }
                            //        if (questionType == String.Empty)
                            //        {
                            //            IsValid = false;
                            //            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.EmptyQuestionType");
                            //        }
                            //        if (dr.ItemArray[4].ToString()== String.Empty)
                            //        {
                            //            IsValid = false;
                            //            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.IsRequiredFailed");
                            //        }
                            //        else if(dr.ItemArray[4].ToString().ToLower().Trim() != "yes" && dr.ItemArray[4].ToString().ToLower().Trim() != "no")
                            //        {
                            //            IsValid = false;
                            //            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.InvalidIsRequiredFormat");
                            //        }
                            //        if (dr.ItemArray[4].ToString().ToLower().Trim() == "yes")
                            //        {
                            //            quizQuestion.IsRequired = true;
                            //        } else if (dr.ItemArray[4].ToString().ToLower().Trim() == "no")
                            //        {
                            //            quizQuestion.IsRequired = false;
                            //        }
                            //        quizQuestion.SortOrder = 1;
                            //        quizQuestion.Marks = dr.ItemArray[3].ToString() != String.Empty ? Convert.ToInt32(dr.ItemArray[3]) : 0;
                            //        if (dr.ItemArray[3].ToString() == String.Empty)
                            //        {
                            //            IsValid = false;
                            //            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.EmptyQuizMark");
                            //        }
                            //        quizQuestion.CreatedBy = (int)SessionHelper.CurrentSession.Id;
                            //        tempQuizQuestionId = quizQuestion.QuizQuestionId;
                            //        importQuiz.quizQuestionsList.Add(quizQuestion);
                            //        rowIndex++;
                            //        IndexNo= IndexNo+1;
                            //    }
                            //    else
                            //    {
                            //        if (dr.ItemArray[5].ToString() == String.Empty)
                            //        {
                            //            IsValid = false;
                            //            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.AnswerRequired");
                            //        }
                            //        if (dr.ItemArray[6].ToString() == String.Empty)
                            //        {
                            //            IsValid = false;
                            //            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.IsCorrectRequired");
                            //        }
                            //        else if(dr.ItemArray[6].ToString().ToLower().Trim() != "yes" && dr.ItemArray[6].ToString().ToLower().Trim() != "no") 
                            //        {
                            //            IsValid = false;
                            //            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.InvalidIsCorrectFormat");
                            //        }
                            //        if (tempQuestionTypeId == 1)
                            //        {
                            //            if (dr.ItemArray[5].ToString() == String.Empty)
                            //            {
                            //                IsValid = false;
                            //                dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.RequiredAnswerSingleChoice");
                            //            }
                            //        }
                            //        if (IsValid)
                            //        {
                            //            var Answer = new Answers();
                            //            Answer.QuizQuestionId = tempQuizQuestionId;
                            //            Answer.AnswerText = dr.ItemArray[5].ToString();
                            //            if (dr.ItemArray[6].ToString().ToLower().Trim() == "yes")
                            //            {
                            //                Answer.IsCorrectAnswer = true;
                            //            }
                            //            else if (dr.ItemArray[6].ToString().ToLower().Trim() == "no")
                            //            {
                            //                Answer.IsCorrectAnswer = false;
                            //            }
                            //            Answer.IsCorrectAnswer = tempQuestionTypeId == 6 ? true : Answer.IsCorrectAnswer;
                            //            RequiredAnswer = Answer.IsCorrectAnswer ? true : false;
                            //            Answer.SortOrder = 1;
                            //            importQuiz.quizAnswersList.Add(Answer);
                            //        }
                            //    }
                            //}
                            int IndexNo = 1;
                            int tempQuestionNo = 0;
                            int tempQuestionId = 0;
                            int _rowNumber = 1;
                            string tempQuestionText = String.Empty;
                            string tempMarks = String.Empty;
                            string tempIsRequired = String.Empty;
                            string tempQuestionType = String.Empty;
                            bool _checkCorrectAnswer = false;
                            bool _isSingleChoice = false;
                            if (excelDataTable.Rows.Count > 1)
                            {
                                bool isValidColumn = true;
                                DataRow firstRow = excelDataTable.Rows[0];

                                if (firstRow[0].ToString().ToLower().Replace(" ", String.Empty) != "questionno"
                                        || firstRow[1].ToString().ToLower().Replace(" ", String.Empty) != "questiontype"
                                        || firstRow[2].ToString().ToLower().Replace(" ", String.Empty) != "questiontext"
                                        || firstRow[3].ToString().ToLower().Replace(" ", String.Empty) != "marks"
                                        || firstRow[4].ToString().ToLower().Replace(" ", String.Empty) != "isrequired"
                                        || firstRow[5].ToString().ToLower().Replace(" ", String.Empty) != "answers"
                                        || firstRow[6].ToString().ToLower().Replace(" ", String.Empty) != "iscorrectanswer")
                                {
                                    isValidColumn = false;
                                    //IsValid = false;
                                    //firstRow[Remarks] += "Please check column.";
                                    //excelDataTable.Rows.RemoveAt(0);
                                    operationDetails.Success = true;
                                    operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                                    operationDetails.Message = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Messages.columnInvalidFormatMessage");
                                    return Json(operationDetails, JsonRequestBehavior.AllowGet);
                                }

                                if (isValidColumn)
                                {
                                    excelDataTable.Rows.RemoveAt(0);
                                }

                            }
                            foreach (DataRow dr in excelDataTable.Rows)
                            {
                                var quizQuestion = new QuizQuestionsEdit();
                                quizQuestion.QuizQuestionId = rowIndex + 1;
                                bool isEmptyRow = false;
                                DataRow nextRow = excelDataTable.Rows.Count > rowIndex + 1 ? excelDataTable.Rows[rowIndex + 1] : dr;

                                //if (dr.ItemArray[0].ToString().Trim() != IndexNo.ToString())
                                //{
                                //    IsValid = false;
                                //    dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.InvalidQuestionNo");
                                //}else 
                                if (dr[0].ToString().Trim() == String.Empty
                                        && dr[1].ToString().Trim() == String.Empty
                                        && dr[2].ToString().Trim() == String.Empty
                                        && dr[3].ToString().Trim() == String.Empty
                                        && dr[4].ToString().Trim() == String.Empty
                                        && dr[5].ToString().Trim() == String.Empty
                                        && dr[6].ToString().Trim() == String.Empty)
                                {
                                    isEmptyRow = true;
                                }

                                if (!isEmptyRow)
                                {
                                    int n, m;
                                    //int QuestionNumber = dr.ItemArray[0].ToString().Trim() == String.Empty || dr.ItemArray[0].ToString().Trim() != IndexNo.ToString() ? 0 : Convert.ToInt32(dr.ItemArray[0].ToString());
                                    int QuestionNumber;

                                    if (int.TryParse(dr.ItemArray[0].ToString(), out m))
                                    {
                                        QuestionNumber = dr.ItemArray[0].ToString().Trim() == String.Empty ? 0 : Convert.ToInt32(dr.ItemArray[0].ToString());
                                    }
                                    else
                                    {
                                        QuestionNumber = 0;
                                    }
                                    if (tempQuestionNo != QuestionNumber || tempQuestionType != dr.ItemArray[1].ToString().Trim()
                                        || tempQuestionText != dr.ItemArray[2].ToString().Trim() || tempMarks != dr.ItemArray[3].ToString().Trim()
                                        || tempIsRequired != dr.ItemArray[4].ToString().ToLower().Trim())
                                    {
                                        if (dr.ItemArray[0].ToString() == String.Empty)
                                        {
                                            IsValid = false;
                                            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.QuestionNoRequired");
                                        }
                                        else if (!int.TryParse(dr.ItemArray[0].ToString(), out n))
                                        {
                                            IsValid = false;
                                            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.InvalidQuestionNo");
                                        }
                                        else
                                        {
                                            tempQuestionNo = Convert.ToInt32(dr.ItemArray[0].ToString());
                                            _checkCorrectAnswer = false;
                                        }

                                        string questionType = dr.ItemArray[1].ToString() != String.Empty ? dr.ItemArray[1].ToString().ToLower().Replace(" ", "") : String.Empty;
                                        tempQuestionType = dr.ItemArray[1].ToString().Trim();
                                        if (questionType == String.Empty)
                                        {
                                            IsValid = false;
                                            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.EmptyQuestionType");
                                        }
                                        else if (questionType != String.Empty)
                                        {
                                            quizQuestion.QuestionTypeId = _quizQuestionsService.GetIdByQuestionType(questionType);
                                            tempQuestionTypeId = quizQuestion.QuestionTypeId;

                                        }

                                        if (dr.ItemArray[2].ToString() == String.Empty)
                                        {
                                            IsValid = false;
                                            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.EmptyQuizQuestion");
                                        }
                                        else
                                        {
                                            quizQuestion.QuestionText = dr.ItemArray[2].ToString().Replace("\n", "</br>");
                                            tempQuestionText = dr.ItemArray[2].ToString().Trim();
                                        }
                                        quizQuestion.SortOrder = 1;
                                        if (dr.ItemArray[3].ToString() == String.Empty)
                                        {
                                            IsValid = false;
                                            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.EmptyQuizMark");
                                        }
                                        else if (dr.ItemArray[3].ToString() != String.Empty)
                                        {
                                            quizQuestion.Marks = dr.ItemArray[3].ToString() != String.Empty ? Convert.ToInt32(dr.ItemArray[3]) : 0;
                                            tempMarks = dr.ItemArray[3].ToString().Trim();
                                        }
                                        if (dr.ItemArray[4].ToString() == String.Empty)
                                        {
                                            IsValid = false;
                                            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.IsRequiredFailed");
                                        }
                                        else if (dr.ItemArray[4].ToString().ToLower().Trim() != "yes" && dr.ItemArray[4].ToString().ToLower().Trim() != "no")
                                        {
                                            IsValid = false;
                                            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.InvalidIsRequiredFormat");
                                        }
                                        if (dr.ItemArray[4].ToString().ToLower().Trim() == "yes")
                                        {
                                            quizQuestion.IsRequired = true;
                                            tempIsRequired = dr.ItemArray[4].ToString().ToLower().Trim();

                                        }
                                        else if (dr.ItemArray[4].ToString().ToLower().Trim() == "no")
                                        {
                                            quizQuestion.IsRequired = false;
                                            tempIsRequired = dr.ItemArray[4].ToString().ToLower().Trim();
                                        }
                                        quizQuestion.CreatedBy = (int)SessionHelper.CurrentSession.Id;
                                        tempQuizQuestionId = quizQuestion.QuizQuestionId;
                                        importQuiz.quizQuestionsList.Add(quizQuestion);
                                        rowIndex++; //increse once question is chnaged
                                        IndexNo = IndexNo + 1; //increase once question is changed

                                        //}
                                        //else
                                        //{
                                        //answer section put if condition above for check old question and in else put below code
                                        if (quizQuestion.QuestionTypeId != 4)
                                        {
                                            if (dr.ItemArray[5].ToString() == String.Empty)
                                            {
                                                IsValid = false;
                                                dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.AnswerRequired");
                                            }
                                            if (dr.ItemArray[6].ToString() == String.Empty)
                                            {
                                                IsValid = false;
                                                dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.IsCorrectRequired");
                                            }
                                            else if (dr.ItemArray[6].ToString().ToLower().Trim() != "yes" && dr.ItemArray[6].ToString().ToLower().Trim() != "no")
                                            {
                                                IsValid = false;
                                                dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.InvalidIsCorrectFormat");
                                            }
                                            else if (dr.ItemArray[6].ToString().ToLower().Trim() == "yes")
                                            {
                                                if (quizQuestion.QuestionTypeId == 1)
                                                {
                                                    if (_checkCorrectAnswer)
                                                    {
                                                        IsValid = false;
                                                        dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.IsCorrectYes");
                                                    }
                                                }
                                                _checkCorrectAnswer = true;
                                            }
                                            else if (dr.ItemArray[6].ToString().ToLower().Trim() == "no" && nextRow.ItemArray[2].ToString().ToLower().Trim() != dr.ItemArray[2].ToString().ToLower().Trim())
                                            {
                                                _isSingleChoice = true;
                                            }
                                            else if (excelDataTable.Rows.Count == _rowNumber)
                                            {
                                                if (!_checkCorrectAnswer)
                                                {
                                                    IsValid = false;
                                                    dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.IsCorrectMessage");
                                                }
                                            }
                                            else if (nextRow.ItemArray[0].ToString() != String.Empty && nextRow.ItemArray[1].ToString() != String.Empty
                                                     && nextRow.ItemArray[2].ToString() != String.Empty)
                                            {
                                                if (nextRow.ItemArray[0].ToString().ToLower().Trim() != dr.ItemArray[0].ToString().ToLower().Trim() && nextRow.ItemArray[1].ToString().ToLower().Trim() != dr.ItemArray[1].ToString().ToLower().Trim()
                                                     && nextRow.ItemArray[2].ToString().ToLower().Trim() != dr.ItemArray[2].ToString().ToLower().Trim())
                                                {
                                                    if (!_checkCorrectAnswer && !_isSingleChoice)
                                                    {
                                                        IsValid = false;
                                                        dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.IsCorrectMessage");
                                                    }
                                                }
                                            }
                                            if (tempQuestionTypeId == 1)
                                            {
                                                if (dr.ItemArray[5].ToString() == String.Empty)
                                                {
                                                    IsValid = false;
                                                    dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.RequiredAnswerSingleChoice");
                                                }
                                            }
                                            if (IsValid)
                                            {
                                                var Answer = new Answers();
                                                Answer.QuizQuestionId = tempQuizQuestionId;
                                                Answer.AnswerText = dr.ItemArray[5].ToString();
                                                if (dr.ItemArray[6].ToString().ToLower().Trim() == "yes")
                                                {
                                                    Answer.IsCorrectAnswer = true;
                                                }
                                                else if (dr.ItemArray[6].ToString().ToLower().Trim() == "no")
                                                {
                                                    Answer.IsCorrectAnswer = false;
                                                }
                                                Answer.IsCorrectAnswer = tempQuestionTypeId == 6 ? true : Answer.IsCorrectAnswer;
                                                RequiredAnswer = Answer.IsCorrectAnswer ? true : false;
                                                Answer.SortOrder = 1;
                                                importQuiz.quizAnswersList.Add(Answer);
                                            }
                                        }
                                        else
                                        {
                                            if (IsValid)
                                            {
                                                var Answer = new Answers();
                                                Answer.QuizQuestionId = tempQuizQuestionId;
                                                Answer.AnswerText = "freetext";
                                                Answer.IsCorrectAnswer = true;
                                                Answer.SortOrder = 1;
                                                importQuiz.quizAnswersList.Add(Answer);
                                            }

                                        }

                                    }
                                    else
                                    {
                                        if (dr.ItemArray[5].ToString() == String.Empty)
                                        {
                                            IsValid = false;
                                            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.AnswerRequired");
                                        }
                                        if (dr.ItemArray[6].ToString() == String.Empty)
                                        {
                                            IsValid = false;
                                            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.IsCorrectRequired");
                                        }
                                        else if (dr.ItemArray[6].ToString().ToLower().Trim() != "yes" && dr.ItemArray[6].ToString().ToLower().Trim() != "no")
                                        {
                                            IsValid = false;
                                            dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.InvalidIsCorrectFormat");
                                        }
                                        else if (dr.ItemArray[6].ToString().ToLower().Trim() == "yes")
                                        {
                                            if (tempQuestionTypeId == 1)
                                            {
                                                if (_checkCorrectAnswer)
                                                {
                                                    IsValid = false;
                                                    dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.IsCorrectYes");
                                                }
                                            }
                                            _checkCorrectAnswer = true;
                                        }
                                        else if (excelDataTable.Rows.Count == _rowNumber)
                                        {
                                            if (!_checkCorrectAnswer)
                                            {
                                                IsValid = false;
                                                dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.IsCorrectMessage");
                                            }
                                        }
                                        else if (nextRow.ItemArray[0].ToString() != String.Empty && nextRow.ItemArray[1].ToString() != String.Empty
                                                 && nextRow.ItemArray[2].ToString() != String.Empty)
                                        {
                                            if (nextRow.ItemArray[0].ToString().ToLower().Trim() != dr.ItemArray[0].ToString().ToLower().Trim() && nextRow.ItemArray[1].ToString().ToLower().Trim() != dr.ItemArray[1].ToString().ToLower().Trim()
                                                 && nextRow.ItemArray[2].ToString().ToLower().Trim() != dr.ItemArray[2].ToString().ToLower().Trim())
                                            {
                                                if (!_checkCorrectAnswer)
                                                {
                                                    IsValid = false;
                                                    dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.IsCorrectMessage");
                                                }
                                            }
                                        }
                                        if (tempQuestionTypeId == 1)
                                        {
                                            if (dr.ItemArray[5].ToString() == String.Empty)
                                            {
                                                IsValid = false;
                                                dr[Remarks] += ResourceManager.GetString("Quiz.QuizQuestion.RequiredAnswerSingleChoice");
                                            }
                                        }
                                        if (IsValid)
                                        {
                                            var Answer = new Answers();
                                            Answer.QuizQuestionId = tempQuizQuestionId;
                                            Answer.AnswerText = dr.ItemArray[5].ToString();
                                            if (dr.ItemArray[6].ToString().ToLower().Trim() == "yes")
                                            {
                                                Answer.IsCorrectAnswer = true;
                                            }
                                            else if (dr.ItemArray[6].ToString().ToLower().Trim() == "no")
                                            {
                                                Answer.IsCorrectAnswer = false;
                                            }
                                            Answer.IsCorrectAnswer = tempQuestionTypeId == 6 ? true : Answer.IsCorrectAnswer;
                                            RequiredAnswer = Answer.IsCorrectAnswer ? true : false;
                                            Answer.SortOrder = 1;
                                            importQuiz.quizAnswersList.Add(Answer);
                                        }
                                    }


                                }
                                _rowNumber++;

                            }
                            //Call services for insert data into database
                            bool result = false;
                            if (IsValid)
                            {
                                importQuiz.QuizId = Convert.ToInt32(Session["quizId"]);
                                importQuiz.CreatedBy = Convert.ToInt32(SessionHelper.CurrentSession.Id);
                                result = _quizQuestionsService.ImportQuizQuestions(importQuiz);
                                TempData["IsQuizQuestion"] = result;
                                Session["QuizQuestionFiles"] = new List<QuizQuestionFiles>();
                                if (result)
                                {
                                    operationDetails.Success = true;
                                    operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
                                    operationDetails.Message = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Labels.UploadSuccess");
                                    return Json(operationDetails, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    operationDetails.Success = false;
                                    operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
                                    operationDetails.Message = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Messages.fileInvalidFormatMessage");
                                    return Json(operationDetails, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                using (ExcelPackage pck = new ExcelPackage())
                                {
                                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
                                    ws.Cells["A1"].LoadFromDataTable(excelDataTable, true);
                                    //change cell color depending on the text input from stored proc?
                                    //if (dtdata.Rows[4].ToString() == "Annual Leave")
                                    for (var j = 0; j < excelDataTable.Rows.Count; j++)
                                    {
                                        if (excelDataTable.Rows[j][Remarks].ToString().Trim() != string.Empty)
                                        {
                                            ws.Row(j + 2).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                            ws.Row(j + 2).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);
                                        }
                                    }
                                    pck.Save();
                                    var ms = new System.IO.MemoryStream();
                                    pck.SaveAs(ms);
                                    Session["ImportFile"] = ms.ToArray();
                                }

                                operationDetails.Success = false;
                                operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                                operationDetails.Message = Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.ValidationFailed");
                                return Json(operationDetails, JsonRequestBehavior.AllowGet);
                            }

                        }
                    }

                }

                operationDetails.Success = true;
                operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
                operationDetails.Message = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Labels.UploadSuccess");
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                OperationDetails operationDetails = new OperationDetails();
                operationDetails.Success = true;
                operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
                operationDetails.Message = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Messages.fileInvalidFormatMessage");
                return Json(operationDetails, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DownloadImportFile()
        {
            var bytes = (byte[])Session["ImportFile"];
            var fileName = "ErrorLog.xlsx";
            return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }
        private JsonResult InvalidFormatResult()
        {
            //result Object for Invalid file format
            OperationDetails operationDetails = new OperationDetails();
            operationDetails.Success = false;
            operationDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Error);
            operationDetails.Message = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Messages.fileInvalidFormatMessage");
            return Json(operationDetails, JsonRequestBehavior.AllowGet);
        }

    }
}