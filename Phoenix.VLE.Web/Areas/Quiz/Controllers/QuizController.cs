﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using Phoenix.Web.Helpers;
using Phoenix.VLE.Web.Helpers;
using jQuery.DataTables.Mvc;
using Phoenix.Common.Queries;
using Phoenix.Common.Models;
using CommonWebHelper = Phoenix.VLE.Web.Helpers.CommonHelper;
using System.Net;
using System.Threading;
using Newtonsoft.Json;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharp;

namespace Phoenix.VLE.Web.Areas.Quiz.Controllers
{

    public class QuizController : BaseController
    {
        private readonly IQuizService _quizService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly IQuizQuestionsService _quizQuestionsService;
        IGroupQuizService _groupQuizService;
        private ISubjectService _subjectService;
        private readonly IFileService _fileService;
        private List<VLEFileType> _fileTypeList;
        private readonly IUserService _userService;


        public QuizController(IQuizService quizService, IFileService fileService, ISubjectService subjectService, IUserPermissionService userPermissionService, IGroupQuizService groupQuizService, IQuizQuestionsService quizQuestionsService, IUserService userService)
        {
            _quizService = quizService;
            _fileService = fileService;
            _fileTypeList = _fileService.GetFileTypes().ToList();
            _userPermissionService = userPermissionService;
            _quizQuestionsService = quizQuestionsService;
            _subjectService = subjectService;
            _groupQuizService = groupQuizService;
            _userService = userService;
        }
        // GET: Quiz/Quiz
        public ActionResult Index()
        {
            if (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent())
            {
                return View("myquiz");
            }
            var syncContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(null);
            ViewBag.IsCustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
            SynchronizationContext.SetSynchronizationContext(syncContext);

            return View();
        }
        public ActionResult LoadStudentQuizGrid()
        {
            var dataList = new object();
            if (!SessionHelper.CurrentSession.IsStudent())
            {
                var quiz = _quizService.GetAllQuizByUser(SessionHelper.CurrentSession.Id, false);
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var CustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);
                //string QuestionsBtn = "<button type='button' class='table-action-icon-btn' onclick='quiz.openQuizQuestion({0});' data-toggle='tooltip' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.ViewQuizQuestions") + "' ><i class='fas fa-file-signature'></i></button>";
                string QuestionsBtn = "<a  onclick='quiz.openQuizQuestion({0});'><img src='~/Content/VLE/img/svg/pencil.svg' class='svg'></a>";
                string editBtn = CustomPermission ? "<button type='button' class='table-action-icon-btn' onclick='quiz.editQuizPopup($(this),{0},{1});' data-toggle='tooltip' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.EditQuiz") + "'><i class='fas fa-pencil-alt btnEdit'></i></button>" : "";
                string ReviewBtn = "<button type='button' class='table-action-icon-btn' onclick=\"quiz.reviewQuiz('{0}')\" data-toggle='tooltip' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.ReviewQuiz") + "' ><i class='fas fa-eye'></i></button>";
                string DownloadQuizAsPDF = "<button type='button' class='table-action-icon-btn' onclick=\"quiz.downloadQuizAsPDF('{0}')\" data-toggle='tooltip' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.Quiz.DownloadQuizPDF") + "' ><i class='fas fa-clipboard'></i></button>";
                string deleteBtn = CustomPermission ? "<button type='button' class='table-action-icon-btn' onclick='quiz.deleteQuizData($(this),{0},{1}); ' data-toggle='tooltip' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.DeleteQuiz") + "'><i class='far fa-trash-alt btnDelete'></i></button>" : "";
                string downloadBtn = CustomPermission ? "<button type='button' class='table-action-icon-btn' onclick='quiz.downloadQuizData($(this),{0}); ' data-toggle='tooltip' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.DownloadQuizQuestion") + "'><i class='fas fa-download fa-1x btnDownload'></i></button>" : "";
                dataList = new
                {
                    aaData = (from item in quiz
                              select new
                              {
                                  //QuizImage = !string.IsNullOrEmpty(item.QuizImagePath) ? "<img src = '" + PhoenixConfiguration.Instance.ReadFilePath + item.QuizImagePath + "' class='img-thumbnail' style = 'height:40px;width:40px' />" : "",
                                  Actions = "<div class='tbl-actions'>" + string.Format(editBtn, item.QuizId, item.TaskCount) + string.Format(ReviewBtn, EncryptDecryptHelper.EncryptUrl(item.QuizId.ToString())) + string.Format(QuestionsBtn, item.QuizId) + string.Format(deleteBtn, item.QuizId, item.TaskCount) + string.Format(downloadBtn, item.QuizId) + "</div>",
                                  item.QuizId,
                                  item.QuizName,
                                  item.PassMarks,
                                  IsActive = CurrentPagePermission.CanEdit ? "<div class='custom-control custom-checkbox mb-2 permission-checkbox' ><input type='checkbox' id='isActive_" + item.QuizId + "' class='custom-control-input' " + (item.IsActive ? "checked" : "") + " onclick='quiz.changeStatus(" + item.QuizId + ",\"" + item.QuizName + "\",\"" + item.Description + "\"," + item.PassMarks + "," + (item.IsActive ? "0" : "1") + "," + item.MaxSubmit + ",\"" + item.QuizImagePath + "\")'><label class='custom-control-label' for='isActive_" + item.QuizId + "'></lable></div>" :
                                  "<div class='disabled custom-control custom-checkbox mb-2 permission-checkbox' ><input type = 'checkbox' id = 'isActive_" + item.QuizId + "' class='custom-control-input' " + (item.IsActive ? "checked" : "") + " '><label class='custom-control-label' for='isActive_" + item.QuizId + "'></lable></div>"
                              }).ToArray()
                };
            }
            else
            {
                var quizMark = String.Empty;
                var studentId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                var quiz = _quizService.GetAllQuizByStudentId(studentId);
                var strHr = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Messages.Hours");
                var strMin = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Messages.Minutes");
                foreach (var item in quiz)
                {
                    TimeSpan spWorkMin = TimeSpan.FromMinutes(item.QuizTime);
                    string Time = string.Format(strHr, (int)spWorkMin.TotalHours, spWorkMin.Minutes);
                    item.QuizTiming = item.QuizTime < 60 ? string.Format(strMin, item.QuizTime.ToString()) : Time;
                    if (item.IsHideScore)
                    {
                        if (Convert.ToDateTime(Convert.ToDateTime(item.ShowScoreDate).ConvertUtcToLocalTime()).Date > DateTime.Now.Date)
                        {
                            quizMark = "-";
                        }
                    }
                    else
                    {
                        quizMark = item.QuizMarks.ToString();
                    }
                }
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var CustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);
                dataList = new
                {
                    aaData = (from item in quiz
                              select new
                              {
                                  item.ResourceId,
                                  item.QuizId,
                                  IsSolvedQuiz = Phoenix.VLE.Web.Helpers.CommonHelper.CheckSolveQuizByStudent(item.QuizId, item.ResourceId),
                                  QuizMarks = quizMark,
                                  TotalMarks = item.QuizMarks.ToString() + "/" + item.TotalMarks.ToString(),
                                  QuizName = item.IsSetTime && !Phoenix.VLE.Web.Helpers.CommonHelper.CheckSolveQuizByStudent(item.QuizId, item.ResourceId) ? "<a href ='javascript:void(0)' id = 'btnStartGroupQuiz' data-studid='" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(0)) + "' data-quizresourceid ='" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(item.GroupQuizId)) + "' data-id = '" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(item.QuizId)) + "' data-quizName = '" + item.QuizName + "' data-startTime = '" + item.QuizTiming + "'> " + item.QuizName + " </ a >" : "<a href ='javascript:void(0)' id = 'btnSubmitWithouTimeQuiz' data-studid='" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(0)) + "' data-quizresourceid ='" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(item.ResourceId)) + "' data-id = '" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(item.QuizId)) + "' data-quizName = '" + item.QuizName + "' data-startTime = '" + item.QuizTiming + "'> " + item.QuizName + " </ a >",
                                  item.GroupName,
                                  item.IsSetTime,
                                  item.QuizTime,
                                  item.CreatedByName
                              }).ToArray()
                };
            }
            //return Json(dataList, JsonRequestBehavior.AllowGet);
            var jsonResult = Json(dataList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult LoadQuizGrid(JQueryDataTablesModel jQueryDataTablesModel)
        {
            IQueryConstraints<QuizView> constraints = new QueryConstraints<QuizView>(jQueryDataTablesModel);
            var dataList = new object();
            if (!SessionHelper.CurrentSession.IsStudent())
            {
                var quiz = _quizService.GetPaginateQuizByUser(SessionHelper.CurrentSession.Id, false, (QueryConstraints<QuizView>)constraints);
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var CustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);
                //string QuestionsBtn = "<button type='button' class='table-action-icon-btn' onclick='quiz.openQuizQuestion({0});' data-toggle='tooltip' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.ViewQuizQuestions") + "' ><i class='fas fa-file-signature'></i></button>";
                string QuestionsBtn = "<a class='mr-3' onclick='quiz.openQuizQuestion({0});' data-toggle='tooltip' data-placement='bottom' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.ViewQuizQuestions") + "'><img src='/Content/VLE/img/svg/tbl-view-quiz.svg'></a>";
                //string editBtn = CustomPermission ? "<button type='button' class='table-action-icon-btn' onclick='quiz.editQuizPopup($(this),{0},{1});' data-toggle='tooltip' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.EditQuiz") + "'><i class='fas fa-pencil-alt btnEdit'></i></button>" : "";
                string editBtn = CustomPermission ? "<a class='mr-3' onclick='quiz.editQuizPopup($(this),\"{0}\",{1});' data-toggle='tooltip' data-placement='bottom' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.EditQuiz") + "'><i class='flaticon-pencil file-actions'></i></a>" : "";
                //string ReviewBtn = "<button type='button' class='table-action-icon-btn' onclick=\"quiz.reviewQuiz('{0}')\" data-toggle='tooltip' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.ReviewQuiz") + "' ><i class='fas fa-eye'></i></button>";
                string ReviewBtn = "<a class='mr-3' id='viewQuiz' data-quizid=\"{0}\" data-isquestionexist=\"{1}\" data-toggle='tooltip' data-placement='bottom' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.ReviewQuiz") + "' ><i class='flaticon-view file-actions'></i></a>";
                string DownloadQuizAsPDF = "<a class='mr-3' id='downloadPDF' data-quizid=\"{0}\" data-isquestionexist=\"{1}\" data-toggle='tooltip' data-placement='bottom' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.Quiz.DownloadQuizPDF") + "' ><i class='far fa-file-pdf file-actions'></i></a>";
                //=string deleteBtn = CustomPermission ? "<button type='button' class='table-action-icon-btn' onclick='quiz.deleteQuizData($(this),{0},{1}); ' data-toggle='tooltip' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.DeleteQuiz") + "'><i class='far fa-trash-alt btnDelete'></i></button>" : "";
                string deleteBtn = CustomPermission ? "<a class='mr-3' onclick='quiz.deleteQuizData($(this),{0},{1});' data-toggle='tooltip' data-placement='bottom' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.DeleteQuiz") + "' ><i class='flaticon-delete file-actions'></i></a>" : "";
                //string downloadBtn = CustomPermission ? "<button type='button' class='table-action-icon-btn' onclick='quiz.downloadQuizData($(this),{0}); ' data-toggle='tooltip' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.DownloadQuizQuestion") + "'><i class='fas fa-download fa-1x btnDownload'></i></button>" : "";
                string downloadBtn = CustomPermission ? "<a class='mr-3' onclick='quiz.downloadQuizData($(this),{0});' data-toggle='tooltip' data-placement='bottom' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.QuizQuestion.DownloadQuizQuestion") + "' ><i class='flaticon-download file-actions'></i></a>" : "";

                string downloadReport = CustomPermission ? "<a class='mr-3' onclick='quiz.downloadQuizReport($(this),{0});' data-toggle='tooltip' data-placement='bottom' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.Quiz.DownloadQuizReport") + "' ><i class='flaticon-download file-actions'></i></a>" : "";

                string cloneQuizIcon = CustomPermission ? "<a class='mr-3' onclick='quiz.cloneQuizData($(this),{0});' data-toggle='tooltip' data-placement='bottom' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.Quiz.CloneQuiz") + "' ><i class='flaticon-copy file-actions'></i></a>" : "";
                string shareQuizIcon = CustomPermission ? "<a class='mr-3' onclick='quiz.shareQuizWithOthers($(this),{0});' data-toggle='tooltip' data-placement='bottom' title='" + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.Quiz.ShareQuiz") + "' ><i class='fas fa-share-alt mr-2' style='color:#636a6e'></i></a>" : "";

                dataList = new
                {
                    aaData = (from item in quiz.Items
                              select new
                              {
                                  //editBtn= item.StartDate == null && item.StartTime == null ? editBtn : (Convert.ToDateTime(item.StartDate).Date + Convert.ToDateTime(item.StartTime).TimeOfDay) > DateTime.Now ? editBtn : "",
                                  //deleteBtn = item.StartDate == null && item.StartTime == null ? deleteBtn : (Convert.ToDateTime(item.StartDate).Date + Convert.ToDateTime(item.StartTime).TimeOfDay) > DateTime.Now ? deleteBtn : "",
                                  //Actions = "<div class='tbl-actions'>" + string.Format(editBtn, item.QuizId, item.TaskCount) + string.Format(ReviewBtn, EncryptDecryptHelper.EncryptUrl(item.QuizId.ToString())) + string.Format(QuestionsBtn, item.QuizId) + string.Format(deleteBtn, item.QuizId, item.TaskCount) + string.Format(downloadBtn, item.QuizId) + "</div>",
                                  Actions = GenerateQuizActions(item, item.StartDate, item.StartTime, editBtn, ReviewBtn, DownloadQuizAsPDF, QuestionsBtn, deleteBtn, downloadBtn, downloadReport, cloneQuizIcon, shareQuizIcon, item.CreatedBy, SessionHelper.CurrentSession.Id),
                                  item.QuizId,
                                  QuizName = item.SharedBy == 0 ? item.QuizName : item.CreatedBy == SessionHelper.CurrentSession.Id ? item.QuizName : item.QuizName + " - " + Phoenix.Common.Localization.ResourceManager.GetString("Quiz.Quiz.SharedBy") + " " + item.SharedByName,
                                  item.PassMarks,
                                  //StartDate = item.StartDate == null && item.StartTime == null ? "NA" : (Convert.ToDateTime(item.StartDate).Date + Convert.ToDateTime(item.StartTime).TimeOfDay) > DateTime.Now? "Greater":"Smaller",
                                  //StartDate = item.StartDate == null && item.StartTime == null ? Phoenix.Common.Localization.ResourceManager.GetString("Quiz.Quiz.NA") : (Convert.ToDateTime(item.StartDate).Date + Convert.ToDateTime(item.StartTime).TimeOfDay).ToString("dd-MMM-yyyy  hh:mm tt"),
                                  IsStarted = item.IsStarted ? Phoenix.Common.Localization.ResourceManager.GetString("Quiz.Quiz.QuizStarted") : Phoenix.Common.Localization.ResourceManager.GetString("Quiz.Quiz.QuizNotStarted"),
                                  IsActive = CurrentPagePermission.CanEdit ? "<div class='custom-control custom-checkbox permission-checkbox'><input type='checkbox' id='isActive_" + item.QuizId + "' class='custom-control-input' " + (item.IsActive ? "checked" : "") + " onclick='quiz.changeStatus(" + item.QuizId + ",\"" + item.QuizName + "\",\"" + item.Description + "\"," + item.PassMarks + "," + (item.IsActive ? "0" : "1") + "," + item.MaxSubmit + ",\"" + item.QuizImagePath + "\")'><label class='custom-control-label mt-2' for='isActive_" + item.QuizId + "'></lable></div>" :
                                  "<div class='disabled custom-control custom-checkbox mb-2 permission-checkbox' ><input type = 'checkbox' id = 'isActive_" + item.QuizId + "' class='custom-control-input' " + (item.IsActive ? "checked" : "") + " '><label class='custom-control-label' for='isActive_" + item.QuizId + "'></lable></div>"
                              }).ToArray(),
                    iTotalRecords = quiz.TotalCount,
                    iTotalDisplayRecords = quiz.FilteredCount,
                    sEcho = jQueryDataTablesModel.sEcho
                };
            }
            else
            {
                var studentId = SessionHelper.CurrentSession.IsStudent() ? SessionHelper.CurrentSession.Id : SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
                var quiz = _quizService.GetAllQuizByStudentId(studentId);
                var strHr = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Messages.Hours");
                var strMin = Phoenix.Common.Localization.ResourceManager.GetString("Shared.Messages.Minutes");
                foreach (var item in quiz)
                {
                    TimeSpan spWorkMin = TimeSpan.FromMinutes(item.QuizTime);
                    string Time = string.Format(strHr, (int)spWorkMin.TotalHours, spWorkMin.Minutes);
                    item.QuizTiming = item.QuizTime < 60 ? string.Format(strMin, item.QuizTime.ToString()) : Time;
                }
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var CustomPermission = _userPermissionService.IsCustomPermissionAssigned(Convert.ToInt32(SessionHelper.CurrentSession.Id), PermissionCodes.U_Quiz.ToString()).Result;
                SynchronizationContext.SetSynchronizationContext(syncContext);

                dataList = new
                {
                    aaData = (from item in quiz
                              select new
                              {
                                  item.ResourceId,
                                  item.QuizId,
                                  IsSolvedQuiz = Phoenix.VLE.Web.Helpers.CommonHelper.CheckSolveQuizByStudent(item.QuizId, item.ResourceId),
                                  QuizMarks = item.QuizMarks.ToString(),
                                  TotalMarks = item.QuizMarks.ToString() + "/" + item.TotalMarks.ToString(),
                                  QuizName = item.IsSetTime && !Phoenix.VLE.Web.Helpers.CommonHelper.CheckSolveQuizByStudent(item.QuizId, item.ResourceId) ? "<a href ='javascript:void(0)' id = 'btnStartGroupQuiz' data-studid='" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(0)) + "' data-quizresourceid ='" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(item.GroupQuizId)) + "' data-id = '" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(item.QuizId)) + "' data-quizName = '" + item.QuizName + "' data-startTime = '" + item.QuizTiming + "'> " + item.QuizName + " </ a >" : "<a href ='javascript:void(0)' id = 'btnSubmitWithouTimeQuiz' data-studid='" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(0)) + "' data-quizresourceid ='" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(item.ResourceId)) + "' data-id = '" + @EncryptDecryptHelper.EncryptUrl(Convert.ToString(item.QuizId)) + "' data-quizName = '" + item.QuizName + "' data-startTime = '" + item.QuizTiming + "'> " + item.QuizName + " </ a >",
                                  item.GroupName,
                                  item.IsSetTime,
                                  item.QuizTime,
                                  item.CreatedByName
                              }).ToArray()
                };
            }
            var jsonResult = Json(dataList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        private string GenerateQuizActions(QuizView item, DateTime? startDate, string startTime, string editBtn, string reviewBtn, string DownloadQuizAsPDF, string questionsBtn, string deleteBtn, string downloadBtn, string downloadReport, string cloneQuizIcon, string shareQuizIcon, long CreatedBy, long LoggedInUserId)
        {
            //editBtn = item.StartDate == null && item.StartTime == null ? editBtn : (Convert.ToDateTime(item.StartDate).Date + Convert.ToDateTime(item.StartTime).TimeOfDay) > DateTime.Now ? editBtn : "";
            editBtn = item.IsStarted ? "" : CreatedBy != LoggedInUserId ? "" : editBtn;
            //deleteBtn = item.StartDate == null && item.StartTime == null ? deleteBtn : (Convert.ToDateTime(item.StartDate).Date + Convert.ToDateTime(item.StartTime).TimeOfDay) > DateTime.Now ? deleteBtn : "";
            deleteBtn = item.IsStarted ? "" : CreatedBy != LoggedInUserId ? "" : deleteBtn;
            //questionsBtn = item.StartDate == null && item.StartTime == null ? questionsBtn : (Convert.ToDateTime(item.StartDate).Date + Convert.ToDateTime(item.StartTime).TimeOfDay) > DateTime.Now ? questionsBtn : "";
            questionsBtn = item.IsStarted ? "" : CreatedBy != LoggedInUserId ? "" : questionsBtn;
            downloadReport = item.IsStarted ? downloadReport : "";
            //DownloadQuizAsPDF = item.IsQuestionsExist ? DownloadQuizAsPDF : "";
            //reviewBtn = item.IsQuestionsExist ? reviewBtn : "";
            //cloneQuizIcon = item.IsStarted ? cloneQuizIcon : "";
            //shareQuizIcon = item.IsStarted ? shareQuizIcon : "";
            return "<div class='tbl-actions'>" + string.Format(editBtn, EncryptDecryptHelper.EncryptUrl(item.QuizId.ToString()), item.TaskCount) + string.Format(reviewBtn, EncryptDecryptHelper.EncryptUrl(item.QuizId.ToString()), item.IsQuestionsExist) + string.Format(DownloadQuizAsPDF, EncryptDecryptHelper.EncryptUrl(item.QuizId.ToString()), item.IsQuestionsExist) + string.Format(questionsBtn, item.QuizId) + string.Format(deleteBtn, item.QuizId, item.TaskCount) + string.Format(downloadBtn, item.QuizId) + string.Format(downloadReport, item.QuizId) + string.Format(cloneQuizIcon, item.QuizId) + string.Format(shareQuizIcon, item.QuizId) + "</div>";
        }
        public ActionResult GetUnitStructureView(string courseIds)
        {
            var lstselectedObjective = new List<Objective>();
            var TopicList = _subjectService.GetUnitStructure(courseIds).ToList();
            if (Session["lstObjective"] != null)
            {
                lstselectedObjective = Session["lstObjective"] as List<Objective>;
                TopicList.ForEach(x => { x.IsSelected = lstselectedObjective.Any(d => d.ObjectiveId.ToString() == x.SubSyllabusId); });
            }
            return PartialView("_UnitStructureView", TopicList);
        }
        public ActionResult SaveObjectives(List<Objective> mappingDetails)
        {
            mappingDetails = mappingDetails == null ? new List<Objective>() : mappingDetails;
            Session["lstObjective"] = mappingDetails;
            return PartialView("_ShowSelectedObjective", mappingDetails);
        }
        public ActionResult DeleteObjective(int objectiveId)
        {
            var operationDetails = new OperationDetails();
            List<Objective> lstobjectives = Session["lstObjective"] as List<Objective>;
            lstobjectives = lstobjectives.Where(m => m.ObjectiveId != objectiveId).ToList();
            Session["lstObjective"] = lstobjectives;
            operationDetails.Success = true; ;
            return PartialView("_ShowSelectedObjective", lstobjectives);
        }
        public ActionResult InitAddEditQuizForm(string id, string courseId = "", string unitId = "")
        {
            var model = new QuizEdit();
            int quizId = String.IsNullOrEmpty(id) ? 0 : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            if (quizId != 0)
            {
                model = _quizService.GetQuiz(quizId);
                model.lstDocuments = _quizService.GetFilesByQuizId(quizId);
                Session["QuizFiles"] = model.lstDocuments;
                //ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName");
                //List<Subject> lstSubjects = new List<Subject>();
                //lstSubjects = _quizService.GetQuizSubjectsGrade(quizId).ToList();
                List<ListItem> lstCourse = new List<ListItem>();
                lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
                var courses = lstCourse.Select(x => new SelectListItem
                {
                    Value = x.ItemId.ToString(),
                    Text = x.ItemName,
                }).ToList();
                ViewBag.CourseList = courses;
                model.CourseId = model.lstCourse.Select(m => m.CourseId.ToString()).ToList();
                model.lstObjectives = _quizService.GetQuizObjectives(quizId).ToList();
                Session["lstObjective"] = model.lstObjectives;

                //model.SubjectId = lstSubjects.Select(m => m.SubjectId.ToString()).ToList();
            }
            else
            {
                Session["QuizFiles"] = new List<QuizFile>();
                Session["lstObjective"] = null;
                model.IsAddMode = true;
                model.SchoolId = SessionHelper.CurrentSession.SchoolId;
                model.MaxSubmit = 1;
                model.QuestionPaginationRange = 1;
                model.QuizTime = 1;
                //ViewBag.SubjectList = new SelectList(SelectListHelper.GetSubjectListByUserId((int)SessionHelper.CurrentSession.Id), "ItemId", "ItemName");
                List<ListItem> lstCourse = new List<ListItem>();
                lstCourse = SelectListHelper.GetSelectListData(ListItems.TeacherCourse, string.Format("{0},{1}", SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.SchoolId)).Distinct().ToList();
                var courses = lstCourse.Select(x => new SelectListItem
                {
                    Value = x.ItemId.ToString(),
                    Text = x.ItemName,
                }).ToList();
                if (!String.IsNullOrEmpty(courseId))
                {
                    int selectedcourseId = !string.IsNullOrWhiteSpace(courseId) ? Convert.ToInt32(CommonWebHelper.GetDecryptedId(courseId)) : 0;
                    model.CourseId.Add(selectedcourseId.ToString());
                }
                if (!String.IsNullOrEmpty(unitId))
                {
                    int selectedunitId = !string.IsNullOrWhiteSpace(unitId) ? Convert.ToInt32(CommonWebHelper.GetDecryptedId(unitId)) : 0;
                    var selectedLesson = _subjectService.GetLessonDetailsById(selectedunitId);
                    model.lstObjectives = selectedLesson.Any() ? selectedLesson.ToList() : new List<Objective>();
                    Session["lstObjective"] = model.lstObjectives;
                }
                ViewBag.CourseList = courses;
            }
            ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
            //return PartialView("_AddEditQuiz", model);
            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SaveQuizData(QuizEdit model, List<HttpPostedFileBase> recordings)
        {
            //if (!ModelState.IsValid)
            //{
            //    return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            //}
            model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            //if (QuizImage != null)
            //{
            //    string GUID = Guid.NewGuid().ToString();
            //    string strFile = String.Format("{0}_{1}{2}", QuizImage.FileName.Substring(0, QuizImage.FileName.IndexOf(".")), GUID, Path.GetExtension(QuizImage.FileName));
            //    string quizImage = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizQuestion;
            //    Common.Helpers.CommonHelper.CreateDestinationFolder(quizImage);
            //    quizImage = Path.Combine(quizImage, strFile);
            //    QuizImage.SaveAs(quizImage);
            //    model.QuizImagePath = Constants.QuizQuestion + strFile;
            //}
            List<Objective> lstObjectives = new List<Objective>();
            if (Session["lstObjective"] != null)
            {
                lstObjectives = Session["lstObjective"] as List<Objective>;
                Session["lstObjective"] = null;
            }
            model.lstDocuments = Session["QuizFiles"] as List<QuizFile>;
            model.lstObjectives = lstObjectives;
            int i = 1;
            if (recordings != null)
            {
                foreach (var item in recordings)
                {

                    var quizFiles = new QuizFile();
                    quizFiles.QuizId = model.QuizId;
                    quizFiles.FileName = SessionHelper.CurrentSession.FirstName + "_" + SessionHelper.CurrentSession.LastName + "_" + i + Path.GetExtension(item.FileName);
                    quizFiles.CreatedBy = (int)SessionHelper.CurrentSession.Id;
                    quizFiles.UploadedFileName = quizFiles.FileName;
                    quizFiles.FilePath = UploadQuizFeedbackResource(item);
                    quizFiles.PhysicalPath = quizFiles.FilePath;
                    model.lstDocuments.Add(quizFiles);
                    i++;
                }
            }
            if (!String.IsNullOrEmpty(model.Description))
                model.Description = model.Description.SanitizeHTML();
            var result = _quizService.QuizCUD(model);
            Session["QuizFiles"] = new List<QuizFile>();
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        private string UploadQuizFeedbackResource(HttpPostedFileBase resourceFile)
        {
            string GUID = Guid.NewGuid().ToString();
            string filePath = String.Empty;
            string strFile = String.Format("{0}{1}", GUID, Path.GetExtension(resourceFile.FileName));
            //string relativeFilePath = String.Empty;
            //string strFile = suggestionFile.FileName;
            string relativeFilePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizFilesDir;
            //relativeFilePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizFeedbackDir;
            //filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizFeedbackDir + "/User_" + SessionHelper.CurrentSession.Id;
            //Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
            Common.Helpers.CommonHelper.CreateDestinationFolder(relativeFilePath);
            //Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
            filePath = Path.Combine(relativeFilePath, strFile);
            //relativeFilePath = Constants.PlannerDir + "/User_" + SessionHelper.CurrentSession.Id;
            string path = Constants.QuizFilesDir + "/" + strFile;
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            resourceFile.SaveAs(filePath);
            return path;
        }
        [HttpPost]
        public ActionResult ActiveDeactiveQuiz(int QuizId)
        {
            QuizEdit model = new QuizEdit();
            model.QuizId = QuizId;
            model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            var result = _quizService.ActiveDeactiveQuiz(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteQuizData(int id)
        {
            QuizEdit model = new QuizEdit();
            model.QuizId = id;
            model.UpdatedBy = (int)SessionHelper.CurrentSession.Id;
            var result = _quizService.QuizDelete(model);
            return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        }

        //Comment out sharepoint quiz files changes by vinayak as discuss with charan

        //[HttpPost]
        //public ActionResult UploadQuizFiles(int quizId)
        //{
        //    List<QuizFile> fileNames = new List<QuizFile>();
        //    if (Request.Files.Count > 0)
        //    {
        //        try
        //        {
        //            HttpFileCollectionBase files = Request.Files;
        //            for (int i = 0; i < files.Count; i++)
        //            {
        //                HttpPostedFileBase file = files[i];
        //                string fname = file.FileName;

        //                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
        //                {
        //                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
        //                    fname = testfiles[testfiles.Length - 1];
        //                }
        //                else
        //                {
        //                    fname = file.FileName;
        //                }

        //                SharePointHelper sh = new SharePointHelper();
        //                Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
        //                SharePointFileView shv = new SharePointFileView();
        //                shv = SharePointHelper.UploadFilesAsPerModule(ref cx, FileModulesConstants.Quiz, SessionHelper.CurrentSession.OldUserId.ToString(), file);
        //                var extension = System.IO.Path.GetExtension(file.FileName);
        //                fileNames.Add(new QuizFile
        //                {
        //                    FileName = file.FileName,
        //                    FileExtension = extension.Replace(".", ""),
        //                    UploadedFileName = file.FileName,
        //                    FilePath = shv.ShareableLink,
        //                    PhysicalPath = shv.SharepointUploadedFileURL
        //                });
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //        if (Session["QuizFiles"] != null)
        //        {
        //            List<QuizFile> lstQuizFiles = Session["QuizFiles"] as List<QuizFile>;
        //            lstQuizFiles.AddRange(fileNames);
        //        }
        //        else
        //        {
        //            Session["QuizFiles"] = fileNames;
        //        }


        //    }
        //    return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult UploadQuizFiles(int quizId)
        {
            List<QuizFile> fileNames = new List<QuizFile>();
            string relativePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Get all files from Request object
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname = file.FileName;

                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }


                        var extension = System.IO.Path.GetExtension(file.FileName);
                        string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizFilesDir;
                        string fileContent = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizFilesDir;
                        string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.QuizFilesDir;

                        Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
                        Common.Helpers.CommonHelper.CreateDestinationFolder(fileContent);
                        Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);

                        fname = Guid.NewGuid() + "_" + fname;
                        relativePath = Constants.QuizFilesDir + fname;
                        fname = Path.Combine(filePath, fname);
                        file.SaveAs(fname);
                        fileNames.Add(new QuizFile
                        {
                            FileName = file.FileName,
                            FileExtension = extension.Replace(".", ""),
                            UploadedFileName = relativePath,
                            FilePath = relativePath,
                            PhysicalPath = relativePath
                        });
                    }
                }
                catch (Exception ex)
                {
                }
                if (Session["QuizFiles"] != null)
                {
                    List<QuizFile> lstQuizFiles = Session["QuizFiles"] as List<QuizFile>;
                    lstQuizFiles.AddRange(fileNames);
                }
                else
                {
                    Session["QuizFiles"] = fileNames;
                }


            }
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DownloadReportDataAsExcelFile(int quizId)
        {
            string path = string.Empty;
            string downloadFileName = "";
            byte[] bytes;
            var quizReport = _quizService.GetQuizReport(quizId, SessionHelper.CurrentSession.Id);

            path = Server.MapPath("~/Content/Files/EmptyReportFile.xls");

            bytes = _quizService.GenerateQuizReportXLSFile(path, quizReport);
            downloadFileName = "QuizReport.xls";
            return File(bytes, "application/vnd.ms-excel", downloadFileName);
        }

        [HttpPost]
        public ActionResult DeleteQuizFile(string id, string quizId, string fileName, string UploadedFilePath)
        {
            string relativepath = string.Empty;
            List<QuizFile> lstfiles = new List<QuizFile>();
            if (id == "0")
            {
                lstfiles = Session["QuizFiles"] as List<QuizFile>;
                var file = lstfiles.FirstOrDefault(e => e.FileName.ToUpper().Trim() == fileName.ToUpper().Trim());
                if (file != null)
                {
                    lstfiles.Remove(file);
                    if (System.IO.File.Exists(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath))
                    {
                        System.IO.File.Delete(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath);
                    }
                    Session["QuizFiles"] = lstfiles;
                }
            }
            else
            {
                if (Session["QuizFiles"] != null)
                {
                    lstfiles = Session["QuizFiles"] as List<QuizFile>;
                    var file = lstfiles.FirstOrDefault(e => e.QuizFileId == Convert.ToInt32(id));
                    if (file != null)
                    {
                        lstfiles.Remove(file);
                        if (System.IO.File.Exists(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath))
                        {
                            System.IO.File.Delete(PhoenixConfiguration.Instance.WriteFilePath + UploadedFilePath);
                        }
                        Session["QuizFiles"] = lstfiles;
                    }
                }
                var result = _quizService.DeleteQuizQuestionFile(Convert.ToInt32(id));
                lstfiles = _quizService.GetFilesByQuizId(Convert.ToInt32(quizId));
            }

            return PartialView("_QuizFileList", lstfiles);
        }

        [HttpPost]
        public ActionResult DeleteQuizRecordingFile(string id)
        {
            string isQuestionsAvailable = "False";
            var result = _quizService.DeleteQuizQuestionFile(Convert.ToInt32(id));
            if (result > 0)
                isQuestionsAvailable = "True";
            return Json(new OperationDetails(isQuestionsAvailable), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CloneQuizData(int quizId)
        {
            string isQuizCloned = "False";
            var result = _quizService.CloneQuizData(quizId);
            if (result > 0)
                isQuizCloned = "True";
            return Json(new OperationDetails(isQuizCloned), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTeacherListToShareQuiz(int quizId)
        {
            var users = _userService.GetUserBySchool(SessionHelper.CurrentSession.SchoolId, 3);
            ViewBag.QuizId = quizId;
            return PartialView("_ShareQuizPartial", users);
        }

        [HttpPost]
        public ActionResult PostShareQuizWithTeachers(string selectedTeachers, int QuizId)
        {
            string isQuizShared = "False";
            int[] teacherIds = JsonConvert.DeserializeObject<int[]>(selectedTeachers);
            string teachers = string.Join(",", teacherIds);
            var result = _quizService.ShareQuizData(QuizId, teachers, SessionHelper.CurrentSession.Id);
            if (result > 0)
                isQuizShared = "True";
            return Json(new OperationDetails(isQuizShared), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewQuiz(string id, string resourceId, string studentId)
        {
            //Quiz DATA

            int quizId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            int quizResourceId = resourceId != null ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(resourceId)) : 0;
            int StudentId = studentId != null ? Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(studentId)) : 0;
            var quizDetails = _quizService.GetQuiz(quizId);
            quizDetails.lstDocuments = _quizService.GetFilesByQuizId(quizId);
            Session["QuizId"] = quizId;
            Session["ResourceId"] = quizResourceId;
            Session["StudentId"] = studentId;
            if (StudentId == 0)
            {
                StudentId = (int)SessionHelper.CurrentSession.Id;
            }
            if (SessionHelper.CurrentSession.IsParent())
            {
                StudentId = SessionHelper.CurrentSession.CurrentSelectedStudent.UserId;
            }
            IEnumerable<QuizQuestionsEdit> questionList = new List<QuizQuestionsEdit>();
            QuizResult quizResponse = new QuizResult();

            questionList = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            if (questionList.Any())
            {
                foreach (var item in questionList)
                {
                    item.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(item.QuizQuestionId);
                }
            }
            quizResponse = _groupQuizService.GetQuizResultByUserId(quizId, StudentId, quizResourceId);
            quizResponse.StudentId = (int)SessionHelper.CurrentSession.Id;
            bool IsMarkByTeacher = quizResponse.QuizResponseByTeacherId != 0 ? true : false;
            questionList.Select(x => x.IsCompleteMarkByTeacher = IsMarkByTeacher);
            foreach (var question in questionList)
            {
                var questionDetails = _quizQuestionsService.GetQuizQuestionById(question.QuizQuestionId);
                question.Answers = questionDetails.Answers;
            }
            ViewBag.Quiz = quizDetails;
            ViewBag.QuizDetails = questionList;
            ViewBag.QuizResponse = quizResponse;
            ViewBag.ResourceId = quizResourceId;
            ViewBag.IsView = quizResourceId == 0 ? true : false;
            return View(questionList);
        }

        public ActionResult DownloadQuizAsPDF(string id, int resourceId = 0, string resourceType = "")
        {
            string pdfhtml = String.Empty;
            string pdfTitle = String.Empty;
            string downloadFileName = String.Empty;
            var resourseDetails = new QuizEdit();
            int quizId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            var quizDetails = _quizService.GetQuiz(quizId);
            quizDetails.lstDocuments = _quizService.GetFilesByQuizId(quizId);
            var questionList = _quizQuestionsService.GetQuizQuestionsByQuizId(quizId);
            if (questionList.Any())
            {
                foreach (var item in questionList)
                {
                    item.lstDocuments = _quizQuestionsService.GetFilesByQuizQuestionId(item.QuizQuestionId);
                }
            }
            if (resourceId > 0)
            {
                resourseDetails = _quizService.GetQuizResourseDetail(resourceId, resourceType);
                resourseDetails.ResourseType = resourceType == "G" ? "Group" : "Assignment";
            }
            foreach (var question in questionList)
            {
                var questionDetails = _quizQuestionsService.GetQuizQuestionById(question.QuizQuestionId);
                question.Answers = questionDetails.Answers;
            }

            ViewBag.ReportType = "PDF";
            ViewBag.Quiz = quizDetails;
            ViewBag.QuizDetails = questionList;
            ViewBag.ResourseDetails = resourseDetails;

            pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_QuizPdf", questionList);
            pdfTitle = Phoenix.Common.Localization.ResourceManager.GetString("Assignment.Assignment.ReportCard");
            downloadFileName = "Quiz.pdf";

            PdfDocument pdf = new PdfDocument();

            pdf = PdfGenerator.GeneratePdf(pdfhtml, PageSize.A4);
            pdf.Info.Title = pdfTitle;
            MemoryStream stream = new MemoryStream();
            pdf.Save(stream, false);
            byte[] file = stream.ToArray();
            stream.Write(file, 0, file.Length);
            stream.Position = 0;

            return File(stream, "application/pdf", downloadFileName);

        }

        [HttpPost]
        public ActionResult GetQuizQuestionsCount(int quizId)
        {
            string isQuestionsAvailable = "False";
            long userId = SessionHelper.CurrentSession.Id;
            int result = _quizService.GetQuizQuestionsCount(userId, quizId);
            if (result > 0)
                isQuestionsAvailable = "True";
            return Json(new OperationDetails(isQuestionsAvailable), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadQuizDetails(int quizId)
        {
            string path = string.Empty;
            string fileDownloadName = "Quiz_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".csv";
            byte[] bytes;
            long userId = SessionHelper.CurrentSession.Id;

            path = Server.MapPath("~/Content/Files/EmptyQuizQuestionFile.csv");
            bytes = _quizService.GenerateQuizQuestionsCSVFile(path, userId, quizId);

            var utf_result = System.Text.Encoding.UTF8.GetPreamble().Concat(bytes).ToArray();

            return File(utf_result, "application/csv;charset=utf-8", fileDownloadName);
        }
        public ActionResult GetuploadedFileDetails()
        {
            List<QuizFile> lstfiles = Session["QuizFiles"] as List<QuizFile>;
            return PartialView("_QuizFileList", lstfiles);
        }


    }
}