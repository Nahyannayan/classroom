﻿using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Phoenix.VLE.Web.Models
{
    public class ProgressTracker
    {
        public ProgressTracker()
        {
            objProgressTrackerData = new List<ProgressTrackerData>();
            objProgressTrackerHeader = new List<ProgressTrackerHeader>();
            objStudentList = new List<StudentList>();
            objTopics = new List<Topics>();
            objTopicDetails = new List<TopicDetails>();
            objObjectiveDetails = new List<ObjectiveDetails>();
            objStepsTopicDetails = new List<StepsTopicDetails>();
            objMergeStudentData = new List<MergeStudentData>();
            objProgressDropdown = new List<ProgressTrackerDropdown>();
            objPTSettingMaster = new ProgressTrackerSettingMaster();
        }

        public List<ProgressTrackerData> objProgressTrackerData { get; set; }
        public List<ProgressTrackerHeader> objProgressTrackerHeader { get; set; }
        public List<StudentList> objStudentList { get; set; }

        public List<Topics> objTopics { get; set; }
        public List<TopicDetails> objTopicDetails { get; set; }
        public List<ObjectiveDetails> objObjectiveDetails { get; set; }
        public List<StepsTopicDetails> objStepsTopicDetails { get; set; }

        public List<MergeStudentData> objMergeStudentData { get; set; }

        public List<ProgressTrackerDropdown> objProgressDropdown { get; set; }
        public ProgressTrackerSettingMaster objPTSettingMaster { get; set; }
    }
    public class BindSteps
    {
        public string SYC_STEP { get; set; }

    }

    public class TopicTree
    {
        public long SYD_ID { get; set; }
        public long SYD_PARENT_ID { get; set; }
        public string SYD_DESCR { get; set; }


    }

    public class SubTopicTree
    {
        public long id { get; set; }
        public string title { get; set; }


    }
    public class SubTerms
    {
        public int ID { get; set; }
        public string DESCRIPTION { get; set; }
        public bool LOCK_STATUS { get; set; }
        public int TERM_ID { get; set; }
        public string TERM_DESCRIPTION { get; set; }
        public int DISPLAY_ORDER { get; set; }
    }

    public class ProgressTrackerData
    {
        public long STU_ID { get; set; }
        public int OBJ_ID { get; set; }
        public string OBJ_VALUE { get; set; }
        public string TSM_ID { get; set; }

        public string FileCount { get; set; }
    }
    public class ProgressTrackerHeader
    {
        public int OBJ_ID { get; set; }
        public string OBJ_CODE { get; set; }
        public string OBJ_DESC { get; set; }
        public string OBJ_ENDDT { get; set; }
        public string TOPIC_ID { get; set; }
        public string TOPIC { get; set; }
        public string SUB_TOPIC { get; set; }
        public string STEPS { get; set; }
        public float OBJ_WIDTH { get; set; }
        public int SUBTOPIC_COLSPAN { get; set; }

        public float STEPS_WIDTH { get; set; }

    }
    public class TopicDetails
    {
        public string TopicId { get; set; }
        public string Topics { get; set; }
        public string subTopics { get; set; }
        public int subTopicColSpan { get; set; }
        public string Steps { get; set; }
        public string STEPS_WIDTH { get; set; }
    }

    public class ObjectiveDetails
    {
        public long objId { get; set; }
        public string TopicId { get; set; }
        public string ObjectiveCode { get; set; }
        public string objDate { get; set; }
        public double objWidth { get; set; }
        public string objDescription { get; set; }
        public string Steps { get; set; }
        public string subTopics { get; set; }
    }

    public class Topics
    {

        public string topics { get; set; }


    }
    public class StepsTopicDetails
    {
        public string TopicId { get; set; }
        public string subTopics { get; set; }
        public string Steps { get; set; }

        public double StepsWidth { get; set; }

    }
    public class MergeStudentData
    {
        public string StudentName { get; set; }
        public string StudentId { get; set; }
        public string StudentNumber { get; set; }
        public string StudentPhoto { get; set; }
        public int OBJ_ID { get; set; }
        public string OBJ_Value { get; set; }
        public string TSMID { get; set; }
    }


    public class ProgressTrackerDropdown
    {

        public string CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string COLOR_CODE { get; set; }
        public int ORDER_SEQUENCE { get; set; }
        public bool IS_DROPDOWN { get; set; }
        public bool IsShowCodeAsHeader { get; set; }
    }

    public class PivotGrid
    {
        public string StudentName { get; set; }

        public string Grade { get; set; }

        public string Section { get; set; }

        public string Term { get; set; }

        public long StudentNumber { get; set; }
        public string Subjects { get; set; }
        public double AttainmentPercentage { get; set; }

        public string AttainmentDescription { get; set; }


    }

    public class ProgressAssessment
    {
        public long id { get; set; }
        public string Grade { get; set; }
        public string Code { get; set; }

        public string Description { get; set; }

        public int DisplayOrder { get; set; }
        public string Color { get; set; }

        public string Value { get; set; }

    }

    public class ProgressExpectation
    {
        public string Grade { get; set; }
        public string Code { get; set; }

        public string Description { get; set; }

        public int DisplayOrder { get; set; }
        public string Color { get; set; }

        public string Value { get; set; }

    }



    public class ProgressTrackerSettingMaster
    {

        public long DAM_ID { get; set; }
        public string DAM_DESCR { get; set; }
        public string DAM_BSU_ID { get; set; }
        public string DAM_GRD_IDS { get; set; }
        public string DAM_ACD_ID { get; set; }
        public bool DAM_ShowCodeAsHeader { get; set; }
        public bool DAM_ShowAsDropdown { get; set; }
    }

    public class ProgressTrackerSettingDetails
    {

        public int DescriptorId { get; set; }
        public int DescriptorMasterId { get; set; }
        public string Descriptor_Descriptor { get; set; }
        public string Descriptor_Color_Code { get; set; }
        public int Descriptor_Order { get; set; }

        public string Descriptor_Code { get; set; }
        public string Descriptor_Value { get; set; }
        public string Descriptor_Type { get; set; }
        public string GradeIds { get; set; }

        public string DataMode { get; set; }
    }


    public class PTExpectationDetails
    {

        public int Id { get; set; }
        public string Description { get; set; }
        public string ColorCode { get; set; }
        public double FromRange { get; set; }
        public double ToRange { get; set; }

        public int SubjectId { get; set; }
        public string Subject { get; set; }

        public int Index { get; set; }

    }


    public class PTExpectationSaver
    {

        public PTExpectationSaver()
        {
            objExpectation = new List<PTExpectationDetails>();
            objDeleteExpectation = new List<PTExpectationDetails>();
        }


        public List<PTExpectationDetails> objExpectation { get; set; }
        public List<PTExpectationDetails> objDeleteExpectation { get; set; }
    }

    public class PTSubjectMaster
    {
        public int SBM_ID { get; set; }

        public string SBM_DESCR { get; set; }


    }
    public class ProgressSetUP
    {
        public int Id { get; set; }
        public string CourseId { get; set; }
        public int GradeTemplateMasterId { get; set; }
        public bool ShowCodeHeader { get; set; }
        public bool ShowDropdown { get; set; }
        public string CourseName { get; set; }

        public string GradeTemplateMasterDesc { get; set; }

        public string CreateBY { get; set; }

        public string TransMode { get; set; }

        public string SelectCourse_Id { get; set; }


        ///-----for Dropdowm------
        public int COR_ID { get; set; }
        public int GTM_ID { get; set; }
        ///-----for Dropdowm------
    }


    public class ProgressTrackerSetup
    {
        public ProgressTrackerSetup()
        {
            ProgressSetupRules = new List<ProgressSetupRules>();
        }
        public long Id { get; set; }
        public long SchoolId { get; set; }
        public long CourseId { get; set; }
        public string CourseName { get; set; }

        public long SchoolGradeId { get; set; }
        public string GradeDisplay { get; set; }
        public long GradeTemplateId { get; set; }
        public string GradeTemplate { get; set; }
        public long GradeSlabId { get; set; }
        public string GradeSlab { get; set; }

        public bool ShowCodeAsHeader { get; set; }
        public bool ShowAsDropDown { get; set; }

        public string CourseIds { get; set; }
        
        public string GradeIds { get; set; }

        public List<ProgressSetupRules> ProgressSetupRules { get; set; }
    }

    public class ProgressSetupRules
    {
        public long ProgressRuleId { get; set; }
        public long GradingTemplateId { get; set; }
        public string GradingTemplate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long Steps { get; set; }

        public string DisplayStartDate { get; set; }
        public string DisplayEndDate { get; set; }
        public bool IsDeleted { get; set; }

    }
    public class CourseGradeDisplay
    {

        public long SchoolGradeId { get; set; }
        public string GradeDisplay { get; set; }
    }

    public class AssessmentProgressTracker
    {
        public AssessmentProgressTracker()
        {
            StudentList = new List<AssessmentStudent>();
            GradingTemplateItemList = new List<GradingTemplateItem>();
            LessonList = new List<AssessmetLessons>();
        }
        public List<AssessmentStudent> StudentList { get; set; }
        public List<GradingTemplateItem> GradingTemplateItemList { get; set; }
        //public List<LessonCourse> LessonList { get; set; }

        public List<AssessmetLessons> LessonList { get; set; }
    }

    public class AssessmentStudent
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public string StudentImageUrl { get; set; }
        public string StudentName { get; set; }
        public long StudentNo { get; set; }
        public string GradeDisplay { get; set; }
        public long LessonId { get; set; }
        public long GradingTemplateId { get; set; }
        public long AssignmentGradingTemplateItemId { get; set; }
        public int AttachmentCount { get; set; }


    }

    public class AssessmetLessons
    {
        public long ParentId { get; set; }
        public long SubSyllabusId { get; set; }
        public long LessonId { get; set; }
        public long StandardBankId { get; set; }
        public string StandardCode { get; set; }
        public string StandardDescription { get; set; }
        public string MainTopic { get; set; }
        public string SubTopic { get; set; }
    }

    public class ProgressTrackerEvidence: Attachments
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public long LessonId{ get; set; }
        public HttpPostedFileBase StudentProgressEvidence { get; set; }

    }

    public class AssignmentObjectiveGrading
    {
        public long AssignmentId { get; set; }
        public string AssignmentDesc { get; set; }
        public DateTime DueDate { get; set; }
        public string DisplayDueDateLable { get; set; }
        public string GradingColor { get; set; }
        public string ShortLabel { get; set; }
        public string GradingItemDescription { get; set; }
        public long GradingTemplateItemId { get; set; }
        public double ScoreFrom { get; set; }
        public double ScoreTo { get; set; }

    }
}