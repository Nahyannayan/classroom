﻿using FlickrNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Models
{
    public class MediaModal
    {
        public int Count { get; set; }
        public IEnumerable<Photo> PhotoList { get; set; }
    }
}
