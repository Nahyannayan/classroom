﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Models
{
    #region Assessment Configuration
    public class AssessmentConfiguration
    {
        public int Index { get; set; }
        public long RSD_ID { get; set; }
        public long AcademicYearId { get; set; }
        public string Header_Description { get; set; }
        public long Grade { get; set; }
        public long Term { get; set; }
        public string ReportHeader { get; set; }
        public string RSD_DisplayType { get; set; }
        public string RSD_CssClass { get; set; }
        public string RSD_ResultType { get; set; }
        public bool IsDirectEntry { get; set; }
        public bool IsAllSubject { get; set; }
        public bool IsRuleRequired { get; set; }
        public bool IsPerformanceIndicator { get; set; }
        public bool IsLocked { get; set; }
        public string MarkType { get; set; }
        public double? MinMarks { get; set; }
        public double? MaxMarks { get; set; }
        public string SBG_ID { get; set; }
        public int? RDM_ID { get; set; }
        public string DataMode { get; set; }
        public static IEnumerable<SelectListItem> DisplayTypeList
        {
            get
            {
                var list = Enum.GetNames(typeof(DisplayType)).Select(x => new SelectListItem { Text = x, Value = x });
                return list;
            }
        }
        public static IEnumerable<SelectListItem> TextBoxSizeList
        {
            get
            {
                var list = Enum.GetNames(typeof(TextBoxSize)).Select(x => new SelectListItem { Text = x, Value = x });
                return list;
            }
        }
        public static IEnumerable<SelectListItem> ResultTypeList
        {
            get
            {
                var list = Enum.GetNames(typeof(ResultType)).Select(x => new SelectListItem { Text = x, Value = x.Substring(0, 1) });
                return list;
            }
        }
        public static IEnumerable<SelectListItem> MarksTypeList
        {
            get
            {
                var list = Enum.GetNames(typeof(MarksType)).Select(x => new SelectListItem { Text = x, Value = Enum.Parse(typeof(MarksType), x).ToString().Substring(0, 3) });
                return list;
            }
        }
        public enum DisplayType
        {
            COMMENT,
            GRADE,
            MARK
        }
        public enum TextBoxSize
        {
            TEXTBOXSMALL,
            TEXTBOXMEDIUM,
            TEXTBOXMULTI
        }
        public enum ResultType
        {
            Comment, Dropdown, Marks
        }
        public enum MarksType
        {
            Integer, UnsignedInteger, Decimal
        }
    }
    public class ReportSchedule
    {
        public int Index { get; set; }
        public int RPF_ID { get; set; }
        public string RPF_Description { get; set; }
        public string DataMode { get; set; }
    }
    public class AssessmentGrade
    {
        public long ReportGradeId { get; set; }
        public string GradeId { get; set; }
        public string Grade_Description { get; set; }
        public string DataMode { get; set; }
    }
    //public class AssessmentConfig : SubjectSpecificCriteriaData
    //{
    //    public AssessmentConfig()
    //    {
    //        AssessmentConfigurations = new List<AssessmentConfiguration>();
    //        AssessmentGrades = new List<AssessmentGrade>();
    //        ReportSchedules = new List<ReportSchedule>();
    //    }
    //    public long SchoolId { get; set; }
    //    public long ACD_ID { get; set; }
    //    public long RSM_ID { get; set; }
    //    public string Header { get; set; }
    //    public string DataMode { get; set; }
    //    public IEnumerable<AssessmentConfiguration> AssessmentConfigurations { get; set; }
    //    public IEnumerable<AssessmentGrade> AssessmentGrades { get; set; }
    //    public IEnumerable<ReportSchedule> ReportSchedules { get; set; }
    //    public Boolean IsSummative { get; set; }
    //}
    public class AssessmentSettings
    {
        public int ReportSM_Id { get; set; }
        public string ReportSM_Description { get; set; }
        public bool IsFinalReport { get; set; }
        public bool IsAOLProcessing { get; set; }
        public string LinkedReportName { get; set; }
        public Boolean IsSummative { get; set; }
    }
    #endregion

    #region Other Configuration Setting
    public class AbsenteeSettingModel
    {
        public long AbsenteeId { get; set; }
        public long BSU_ID { get; set; }
        public long AbsenteeACD_ID { get; set; }
        public string Absent_Description { get; set; }
        public string ApprovedLeave_Description { get; set; }
    }
    public class ReportDesignName
    {
        public long DEV_ID { get; set; }
        public string DEV_REPORT_NAME { get; set; }
    }
    public class ReportDesignLink
    {
        public long RSM_ID { get; set; }
        public long RSM_BSU_ID { get; set; }
        public long RSM_ACD_ID { get; set; }
        public long RSM_DEV_ID { get; set; }
        public string ReportName { get; set; }
        public string DesignName { get; set; }
    }
    public class RuleDeletionModel
    {
        public long RSM_ID { get; set; }
        public string RSM_Description { get; set; }
        public long ReportScheduleId { get; set; }
        public string ReportSchedule_Description { get; set; }
        public long ReportSetupD_Id { get; set; }
        public string ReportSetupD_Description { get; set; }
        public long SubjectGradeID { get; set; }
        public string SubjectGrade_Description { get; set; }
        public string GRD_ID { get; set; }
        public string GradeDisplay { get; set; }
        public long TermID { get; set; }
        public string Term_Description { get; set; }
        public string RuleType { get; set; }
        public string Rule_Description { get; set; }
        public long RuleId { get; set; }
    }
    public class RuleSetupModel
    {
        public RuleSetupModel()
        {
            AssesmentWithType = new List<AssesmentWithType>();
        }
        public string RSM_Description { get; set; }
        public string ReportSchedule_Description { get; set; }
        public string ReportSetupD_Description { get; set; }
        public long SBG_ID { get; set; }
        public string SubjectGrade_Description { get; set; }
        public string GRD_ID { get; set; }
        public string GradeDisplay { get; set; }
        public string Term_Description { get; set; }
        public string Rule_Description { get; set; }
        public long RRM_ID { set; get; }
        public long RSM_ID { set; get; }
        public string SBG_IDs { set; get; }
        public long ACD_ID { set; get; }
        public string GRD_IDs { set; get; }
        public string RRM_DESCR { set; get; }
        public long RPF_ID { set; get; }
        public long RSD_ID { set; get; }
        public long TRM_ID { set; get; }
        public string RRM_TYPE { set; get; }
        public long SLAB_ID { set; get; }
        public decimal PASSMARK { set; get; }
        public decimal MAXMARK { set; get; }
        public string ENTRYTYPE { set; get; }
        public decimal GRADE_WEIGTAGE { set; get; }
        public string DATAMODE { get; set; }
        public static IEnumerable<SelectListItem> CalculationTypeList
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem { Text = "SUM", Value = "SUM" });
                list.Add(new SelectListItem { Text = "AVERAGE", Value = "AVG" });
                list.Add(new SelectListItem { Text = "BEST OF", Value = "BEST OF" });
                return list;
            }
        }
        public static IEnumerable<SelectListItem> ExamLevelList
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem { Text = "Normal", Value = "Normal" });
                list.Add(new SelectListItem { Text = "Core", Value = "Core" });
                list.Add(new SelectListItem { Text = "Extended", Value = "Extended" });
                return list;
            }
        }
        public List<AssesmentWithType> AssesmentWithType { get; set; }
    }
    public class AssesmentWithType
    {
        public long CAD_ID { get; set; }
        public string CAD_DESC { get; set; }
        public long CAM_ID { get; set; }
        public string CAM_DESC { get; set; }
        public string CAD_IDs { get; set; }
        public long RSS_ID { get; set; }
        public decimal WEIGHTAGE { get; set; }
        public string OPERATION { get; set; }
        public string OPRT_DISPLAY { get; set; }
    }
    public class PublishedReportModel
    {
        public string Grade_Display { get; set; }
        public string Section { get; set; }
        public long SectionId { get; set; }
        public string GRD_ID { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsPublished { get; set; }
        public bool ISReleased { get; set; }
        public DateTime? ReleasedDate { get; set; }
    }
    public class UnpublishedReportData
    {
        public UnpublishedReportData()
        {
            UnpublishedReportList = new List<PublishedReportModel>();
        }
        public long ACD_ID { get; set; }
        public long TRM_ID { get; set; }
        public long RSM_ID { get; set; }
        public long RPF_ID { get; set; }
        public List<PublishedReportModel> UnpublishedReportList { get; set; }
    }
    public class SubjectSpecificCriteria
    {
        public long RSD_ID { get; set; }
        public long TempRSD_ID { get; set; }
        public string RSD_HEADER { get; set; }
        public string RSD_SUB_DESCRIPTION { get; set; }
        public long RDM_ID { get; set; }
        public int RSD_ORDER { get; set; }
        public long SBT_ID { get; set; }
        public string SBT_SHORTNAME { get; set; }
        public string SBT_DESCRIPTION { get; set; }
        public string RSD_SKILLS { get; set; }
        public long RSD_CATEGORY_ID { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class SubjectSpecificCriteriaData
    {
        public SubjectSpecificCriteriaData()
        {
            SubjectSpecificCriteriaList = new List<SubjectSpecificCriteria>();
        }
        public long RSM_ID { get; set; }
        public long SBG_ID { get; set; }
        public List<SubjectSpecificCriteria> SubjectSpecificCriteriaList { get; set; }
    }

    //public class GetDefaultListById
    //{
    //    public long RDD_ORDER { get; set; }
    //    public long RDD_RDM_ID { get; set; }
    //    public long RDD_ID { get; set; }
    //    public string RDD_DESCR { get; set; }
    //    public int Index { get; set; }
    //    private string _DataMode = "EDIT";
    //    public string DataMode { get => _DataMode; set => _DataMode = value; }
    //    public long BSU_ID { get; set; }

    //    public List<Phoenix.Common.Models.ListItem> BankListData { get; set; }


    //}

    public class AssessmentDescriptor
    {
        public AssessmentDescriptor()
        {

            DataMode = "EDIT";
        }
        public string RDD_DESCR { get; set; }
        public long RDD_ORDER { get; set; }
        public long RDD_RDM_ID { get; set; }
        public long RDD_ID { get; set; }

        public int Index { get; set; }
        public string DataMode { get; set; }

    }
    public class SubjectCategoryModel
    {
        public long CategoryId { get; set; }
        public string Category_Description { get; set; }
        public int Category_Order { get; set; }
        public string Category_ShortName { get; set; }
        public int Index { get; set; }
        private string _DataMode = "EDIT";
        public string DataMode { get => _DataMode; set => _DataMode = value; }
        public long BSU_ID { get; set; }
        public long ACD_ID { get; set; }
        public string GRD_ID { get; set; }
        public long SBG_ID { get; set; }
        public List<SubjectCategoryModel> SubjectCategory_Data { get; set; }
    }
    public class DefaultValues
    {
        public int Index { get; set; }
        public long RSM_ID { get; set; }
        public long RSD_ID { get; set; }
        public long RSP_RSM_ID { get; set; }
        public long RSP_RSD_ID { get; set; }
        public string RSP_TEXT { get; set; }
        public string RSD_HEADER { get; set; }
        public string GRD_IDS { get; set; }
        public string SBG_IDS { get; set; }
        public long RDM_ID { get; set; }
    }
    public class CourseContent
    {
        public long RCP_ID { get; set; }
        public long RCP_RSM_ID { get; set; }
        public long RCP_RPF_ID { get; set; }
        public long RCP_SBG_ID { get; set; }
        public long RCP_RSD_ID { get; set; }
        public string RCP_DESCR { get; set; }
        public long RCP_ACD_ID { get; set; }
        public string RCP_GRD_ID { get; set; }
        public string Content_Description { get; set; }
        public string DataMode { get; set; }
    }
    public class CourseContentParameter
    {
        public long RSM_ID { get; set; }
        public long RSD_ID { get; set; }
        public long RPF_ID { get; set; }
        public long SBG_ID { get; set; }
    }
    #endregion

    #region Reflection Setup
    public class ReflectionSetupModel
    {
        public long RFL_ID { get; set; }
        public long ACD_ID { get; set; }
        public long BSU_ID { get; set; }
        public long TRM_ID { get; set; }
        public string GRD_ID { get; set; }
        public long SBG_ID { get; set; }
        public string SubjectDescription { get; set; }
        public string OTC_IDS { get; set; }
        [AllowHtml]
        public string RFL_DECRIPTION { get; set; }
        public List<OutComeModel> OutComeModelList { get; set; }
    }
    public class OutComeModel
    {
        public long OTC_ID { get; set; }
        public long ACD_ID { get; set; }
        public long BSU_ID { get; set; }
        public long TRM_ID { get; set; }
        public string GRD_ID { get; set; }
        public long SBG_ID { get; set; }
        public string OTC_DESCRIPTION { get; set; }
    }
    #endregion

    #region Course Selection Setup
    public class CourseSelectionPrerequisite
    {
        public long PrerequisiteId { get; set; }
        public long PrerequisiteAcdId { get; set; }
        public string PrerequisiteGradeId { get; set; }
        public long PrerequisiteSubjectId { get; set; }
        public long PrerequisiteFutureSubjectId { get; set; }
        public int PrerequisiteMinimumPercent { get; set; }
        public string PrerequisiteSubjectName { get; set; }
        public string PrerequisiteAcademicYearDesc { get; set; }
        public string PrerequisiteGrade { get; set; }
        public bool IsActive { get; set; } = true;
    }

    public class CouseSelectionValidationType
    {
        public int CourseSelectionValidationId { get; set; }
        public string CourseSelectionValidationDesc { get; set; }
        public string CourseSelectionValidationType { get; set; }
    }

    public class CouserSelectionSubjectCriteria : CourseSelectionSubjects
    {
        public CouserSelectionSubjectCriteria()
        {
            SubjectList = new List<TreeItemView>();
        }
        public long CSM_ID { get; set; }
        public int Index { get; set; }
        public long BucketId { get; set; }
        public int ddlSubjectCriteria { get; set; }
        public string BucketName { get; set; }
        public int DisplayOrder { get; set; }
        public double? MinMarks { get; set; }
        public double? MaxMarks { get; set; }
        public int GroupId { get; set; }
        public List<TreeItemView> SubjectList { get; set; }
    }

    public class CourseSelectionSubjects
    {
        public CourseSelectionSubjects()
        {
            SubjectIds = new List<CourseSelectionSubjects>();
        }
        public long CSD_ID { get; set; }
        public long SubjectId { get; set; }
        public long SubjectGradeId { get; set; }
        public string SubjectGradeDescription { get; set; }
        public string ValidationName { get; set; }
        public string SubjectGradeShortCode { get; set; }
        public long ParentId { get; set; }
        public bool IsPrequisite { get; set; }
        public bool IsActive { get; set; } = true;
        public List<CourseSelectionSubjects> SubjectIds { get; set; }
    }

    public class CourseSelectionDetailsCUD
    {
        public CourseSelectionDetailsCUD()
        {
            CouserSelections = new List<CouserSelectionSubjectCriteria>();
        }

        public long CSM_ID { get; set; }
        public long CSM_ACD_ID { get; set; }
        public long CSM_BSU_ID { get; set; }
        public string CSM_GRD_ID { get; set; }
        public long CSM_STM_ID { get; set; }
        public List<CouserSelectionSubjectCriteria> CouserSelections { get; set; }
    }
    public class CourseSelectionDetailsCUDResponse
    {
        public long CSD_ID { get; set; }
        public long CSM_ID { get; set; }
        public int GROUPID { get; set; }
        public long SBG_ID { get; set; }
    }

    public class TreeItemView
    {
        public TreeItemView()
        {
            children = new List<TreeItemView>();
        }
        public dynamic id { get; set; }
        public string text { get; set; }
        public bool IsParent { get; set; }
        public dynamic ParentId { get; set; }
        public string iconCls { get; set; }
        public string state { get; set; }
        public bool _checked { get; set; }
        public List<TreeItemView> children { get; set; }
    }
    public class CourseSelectedStudent
    {
        public long CSD_ID { get; set; }
        public long StudentId { get; set; }
        public long SubjectId { get; set; }
        public long SchoolId { get; set; }
        public long AcademicYearId { get; set; }
        public long NextAcademicYearId { get; set; }
    }
    #endregion
}