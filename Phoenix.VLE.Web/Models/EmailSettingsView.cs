﻿using Phoenix.Common.CustomAttributes;
using System;

namespace Phoenix.VLE.Web.Models
{
    public class EmailSettingsView
    {
        public string EmailCode { get; set; }
        public string AuthorizedName { get; set; }

        public string SenderEmail { get; set; }

        public string SenderPassword { get; set; }

        public string SenderName { get; set; }

        public DateTime? Date { get; set; }
        public bool IncludeTerms { get; set; }
        public string AcceptanceLetter { get; set; }
        public string AcceptanceTerms { get; set; }
        public string EmailText { get; set; }

        public string SMTPClient { get; set; }

        public string PortNo { get; set; }

        public bool IsSSLEnabled { get; set; }
    }
}
