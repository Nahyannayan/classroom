﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class TeamsMeetingEdit
    {
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingName")]
        public string MeetingName { get; set; }
        public int SchoolGroupId { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingStartDate")]
        public string MeetingDate { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingStartTime")]
        public string MeetingStartTime { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingEndTime")]
        public string MeetingEndTime { get; set; }
        public string SelectedUserIds { get; set; }
    }
}