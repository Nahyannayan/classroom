﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models.EditModels
{
    public class StudentTcDetails
    {
        public string TCtype { get; set; }
        public DateTime AppliedDate { get; set; }
        public DateTime LastAttendanceDate { get; set; }
        public string Status { get; set; }
    }

    public class StudentTcDetailsRoot: JasonResult
    {
        public List<StudentTcDetails> data { get; set; }
    }
}