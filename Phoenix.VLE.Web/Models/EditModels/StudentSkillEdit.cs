﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class StudentSkillEdit
    {
        public int SkillId { get; set; }
        public bool IsActive { get; set; }
        public bool IsApproved { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Skills.SkillName")]
        public string SkillName { get; set; }
        public long UserId { get; set; }
        public int StudentId { get; set; }
        public long CreatedUserId { get; set; }
        public TransactionModes Mode { get; set; }
    }
}