﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Models;


namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "School.ContactInfo")]
    public class ContactInfoEdit
    {
        public int ContactInfoId { get; set; }

        public string SchoolName { get; set; }

        public int SchoolId { get; set; }
        public string SchoolEmail { get; set; }

        public string PrincipalName { get; set; }

        public string PrimaryContactFirstName { get; set; }
        public string PrimaryContactLastName { get; set; }
        public string Address1 { get; set; }

        public string Address2 { get; set; }
        public string Town { get; set; }
        public int CountyId { get; set; }

        public int CountryId { get; set; }

        public string Telephone { get; set; }

        public string PostCode { get; set; }

        public string WebsiteUrl { get; set; }
       

        public List<ListItem> CountryList { get; set; }
        public ContactInfoEdit()
        {
            CountryList = new List<ListItem>();
        }
    }
}