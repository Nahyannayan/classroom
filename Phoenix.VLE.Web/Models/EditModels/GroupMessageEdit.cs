﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "SchoolGroup.GroupMessage")]
    public class GroupMessageEdit
    {
        public int GroupMessageId { get; set; }
        public int ParentGroupId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string SelectedStudentId { get; set; }
        public string NotifyTo { get; set; }
        public string Attachment { get; set; }
        public List<SelectListItem> GroupList { get; set; }
        public string SelectedGroups { get; set; }
        public bool SendToAll { get; set; }

        public List<HttpPostedFileBase> PostedFiles { get; set; }

        public GroupMessageEdit()
        {
            PostedFiles = new List<HttpPostedFileBase>();
            GroupList = new List<SelectListItem>();
        }
    }
}