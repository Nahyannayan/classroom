﻿using Phoenix.Common.CustomAttributes;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "School.SchoolSpace")]
    public class SchoolSpaceEdit
    {
        public int SpaceId { get; set; }
        public int SchoolId { get; set; }
        public string SpaceName { get; set; }
        public string SpaceNameXml { get; set; }
        public string Description { get; set; }
        public bool UseWebsite { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public bool IsAddMode { get; set; }
        public string Category { get; set; }
        public string IconCssClass { get; set; }
        public int CategoryId { get; set; }
        public List<SelectListItem> UseWebsiteOptionList { get; set; }
        public int FileCount { get; set; }
        public int FolderCount { get; set; }
        public SchoolSpaceEdit()
        {
            UseWebsiteOptionList = new List<SelectListItem>();
            UseWebsiteOptionList.Add(new SelectListItem { Text = "No. Disable website access", Value = "false" });
            UseWebsiteOptionList.Add(new SelectListItem { Text = "Yes. Launch this group as a website", Value = "true" });
        }
    }
}