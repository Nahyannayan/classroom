﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models.EditModels
{
    public class PdfUrlModel
    {
        public string id { get; set; }
        public string url { get; set; }
        public string androidURL { get; set; }
        public string isBlockReportView { get; set; }
        public string alertMsg { get; set; }
        
    }
}