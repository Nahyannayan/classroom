﻿using System;
using Phoenix.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
   public class TcRequestmodel
    {

        public string parentloginId { get; set; }
        public string ParentLoginName { get; set; }
        public string ImgUrl { get; set; }
        public string schoolName { get; set; }
        public string Grade { get; set; }
        public string primary_contact { get; set; }
        public string MobNo { get; set; }
        public string Email { get; set; }
        public string Complete_the_exit_interview { get; set; }
        public IEnumerable<SelectListItem> DropdownListExitInterview { get; set; }
        public string TranferdSchool { get; set; }
        public IEnumerable<SelectListItem> DropdownListTranferdSchool { get; set; }
        public string ReasonForTransfer { get; set; }
        public IEnumerable<SelectListItem> DropdownReasonForTransfer { get; set; }
        public DateTime LastDayOfAttendance { get; set; }
        public string AppreciateFeedback { get; set; }

        public string schoolmet { get; set; }
        public string schoolleadership { get; set; }
        public string schoolfacilities { get; set; }
        public string schoolfees { get; set; }
        public string schoolexperience { get; set; }
        public string SchoolRecommend { get; set; }
    }
}
