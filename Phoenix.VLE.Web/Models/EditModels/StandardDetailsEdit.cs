﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Course.StandardDetails")]
    public class StandardDetailsEdit
    {
        public StandardDetailsEdit()
        {

        }

        public StandardDetailsEdit(Int64 ID)
        {
            StandardDetailID = ID;
        }

        public Int64 StandardDetailID { get; set; }
        public Int64 StandardID { get; set; }
       
        public int Order { get; set; }
        public int Weight { get; set; }
        public int Points { get; set; }
        public bool IsAddMode { get; set; }
        public string StandardGroupName { get; set; }
       

        public string Description { get; set; }
        public int MainSyllabusId { get; set; }
        public int ParentID { get; set; }

        public int GroupID { get; set; }

        public int StepID { get; set; }

    }
    //public class MainSyllabusIdList
    //{
    //    public Int64  Id { get; set; }
    //    public string Name { get; set; }
    //}
}