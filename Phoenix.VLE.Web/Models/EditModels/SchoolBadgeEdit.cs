﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "SchoolBadge.Badge")]
    public class SchoolBadgeEdit
    {
        public SchoolBadgeEdit()
        {

        }

        public SchoolBadgeEdit(int id)
        {
            SchoolBadgeId = id;
        }

        public Int64 SchoolBadgeId { get; set; }
       // [Remote("IsBadgeAvailable", "SchoolBadge", ErrorMessage = "Badge with same name already exists", HttpMethod = "POST",AdditionalFields = "SchoolBadgeId")]
        public string Title { get; set; }
        public string Description { get; set; }
        public string ActualImageName { get; set; }
        public string UploadedImageName { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DisplayOrder { get; set; }
        public string SchoolId { get; set; }
        public bool IsAddMode { get; set; }
        public HttpPostedFileBase BadgeFile { get; set; }
    }
}
