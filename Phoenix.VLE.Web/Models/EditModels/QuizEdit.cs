﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Quiz.Quiz")]
    public class QuizEdit
    {
        public QuizEdit()
        {
            lstDocuments = new List<QuizFile>();
            SubjectIds = new List<string>();
            SubjectId = new List<string>();
            lstObjectives = new List<Objective>();
            lstCourse = new List<Course>();
            CourseId = new List<string>();
        }
        public int QuizId { get; set; }
        public string QuizName { get; set; }
        [AllowHtml]
        public string Description{ get; set; }
        public int PassMarks { get; set; }
        public long SchoolId { get; set; }
        public bool IsActive { get; set; }
        public bool IsRandomQuestion { get; set; }
        public bool IsSetTime { get; set; }
        public int UpdatedBy { get; set; }
        public bool IsAddMode { get; set; }
        public bool IsForm { get; set; }
        public int MaxSubmit { get; set; }
        public int QuestionPaginationRange { get; set; }
        public int QuizTime { get; set; }
        public string QuizImagePath { get; set; }
        public int SectionId { get; set; }
        public string SectionType { get; set; }
        public List<QuizFile> lstDocuments { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string SelectedDate { get; set; }
        public string Subjects { get; set; }
        public List<string> SubjectIds { get; set; }
        public List<string> SubjectId { get; set; }
        public int ModuleId { get; set; }
        public long FolderId { get; set; }
        public int GroupId { get; set; }
        public string Courses { get; set; }
        public string CourseIds { get; set; }
        public List<string> CourseId { get; set; }
        public List<Course> lstCourse { get; set; }
        public List<Objective> lstObjectives { get; set; }
        public bool IsHideScore { get; set; }
        public DateTime? ShowScoreDate { get; set; }
        public bool IsLogAnswer { get; set; }
        public long GroupQuizId { get; set; }
        public string ResourceName { get; set; }
        public string ResourceCourseName { get; set; }
        public string ResourseType { get; set; }
    }
}