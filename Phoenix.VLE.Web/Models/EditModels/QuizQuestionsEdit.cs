﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Quiz.QuizQuestion")]
    public class QuizQuestionsEdit
    {
        public QuizQuestionsEdit()
        {
            lstDocuments = new List<QuizQuestionFiles>();
            SortedQuizQuestionIds = new List<string>();
            lstObjective = new List<Objective>();
            lstObjectives = new List<Objective>();
            Answers = new List<Answers>();
            UnAssignedQuestionList = new List<Common.Models.ListItem>();
            AssignedQuestionList = new List<Common.Models.ListItem>();
        }
        public int QuizQuestionId { get; set; }
        public int QuestionTypeId { get; set; }
        public int QuizId { get; set; }
        public string SelectedQuestionId { get; set; }
        public string DeselectedQuestionId { get; set; }
        public List<Common.Models.ListItem> UnAssignedQuestionList { get; set; }
        public List<Common.Models.ListItem> AssignedQuestionList { get; set; }
        public string QuizName { get; set; }
        [AllowHtml]
        public string QuestionText { get; set; }
        public string Subjects { get; set; }
        public List<string> SubjectIds { get; set; }
        public List<string> SubjectId { get; set; }
        public List<Objective> lstObjective { get; set; }
        public string QuestionTypeName { get; set; }
        public bool IsVisible { get; set; }
        public bool IsRequired { get; set; }
        public bool IsAddMode { get; set; }
        public int SortOrder { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public List<Answers> Answers { get; set; }
        public int Marks { get; set; }
        public int MaxSubmit { get; set; }
        public bool IsCompleteMarkByTeacher { get; set; }
        public string QuestionImagePath { get; set; }
        public string QuizImagePath { get; set; }
        public string SortedQuestionIds { get; set; }
        public List<QuizQuestionFiles> lstDocuments { get; set; }
        public List<Objective> lstObjectives { get; set; }
        public List<string> SortedQuizQuestionIds { get; set; }
        public int TotalCount { get; set; }
        public bool IsSetTime { get; set; }
        public int QuizTime { get; set; }
        public int RowNum { get; set; }
        public string Courses { get; set; }
        public List<string> CourseIds { get; set; }
        public List<string> CourseId { get; set; }
        public List<Course> lstCourse { get; set; }

    }
    public class AnswersEdit
    {
        public int QuizAnswerId { get; set; }
        public string AnswerText { get; set; }
        public int SortOrder { get; set; }
    }
    public class QuizAnswersEdit
    {
        public int QuizId { get; set; }
        public int ResourceId { get; set; }
        public int QuizResponseId { get; set; }
        public string ResourceType { get; set; }
        public int QuizAnswerId { get; set; }
        public int QuestionTypeId { get; set; }
        public int QuizQuestionId { get; set; }
        [AllowHtml]
        public string QuizAnswerText { get; set; }
        public string QuestionText { get; set; }
        public bool IsCorrectAnswer { get; set; }
        public int SortOrder { get; set; }
        public int CreatedBy { get; set; }
        public int Marks { get; set; }
        public string GradingColor { get; set; }
        public string ShortLabel { get; set; }
        public string GradingItemDescription { get; set; }
        public string GradingTemplateItemSymbol { get; set; }
        public string Percentage { get; set; }
        public int QuizMarks { get; set; }
        public int QuizStatus { get; set; }
        public decimal ObtainedMarks { get; set; }

    }
    public class ImportQuiz
    {
        public ImportQuiz()
        {
            quizQuestionsList = new List<QuizQuestionsEdit>();
            quizAnswersList = new List<Answers>();
        }
        public List<QuizQuestionsEdit> quizQuestionsList { get; set; }
        public List<Answers> quizAnswersList { get; set; }
        public int QuizId { get; set; }
        public int CreatedBy { get; set; }

    }
    public class QuizFeedbackEdit
    {
        public QuizFeedbackEdit()
        {
            Files = new List<File>();
        }
        [AllowHtml]
        public string feedback { get; set; }
        public int quizId { get; set; }
        public long studentId { get; set; }
        public int QuizResultId { get; set; }
        public List<File> Files { get; set; }
        public long UserId { get; set; }
    }

}