﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models.EditModels
{
    [ResourceMappingRoot(Path = "School.RSSFeed")]
    public class RSSFeedEdit
    {

        public int Id { get; set; }
        public string RSSTitle { get; set; }
        public string RSSLink { get; set; }
        public string RSSDescription { get; set; }
    }
}