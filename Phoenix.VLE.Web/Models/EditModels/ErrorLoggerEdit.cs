﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Shared.ErrorLogger")]
    public class ErrorLoggerEdit
    {
        public long ErrorLogId { get; set; }
        public string ErrorMessage { get; set; }
        public long UserId { get; set; }
        public string ModuleUrl { get; set; }
        public string UserIpAddress { get; set; }
        public string WebServerIpAddress { get; set; }
        public DateTime ReportingDate { get; set; }
        public string User { get; set; }
        public List<TaskFile> lstTaskFiles { get; set; }
        public List<ErrorFiles> lstErrorFiles { get; set; }
        public int ReportIssueId { get; set; }
        public string ReportIssueTitle { get; set; }
        public DateTime ErrorDateTime { get; set; }
        public string ErrorFrequency { get; set; }
        public string ModeOfContact { get; set; }
        public string ContactDetail { get; set; }

    }
}