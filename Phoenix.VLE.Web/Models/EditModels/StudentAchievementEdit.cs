﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class StudentAchievementEdit
    {
        public long UserId { get; set; }
        public long AchievementId { get; set; }

        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Common.Description")]
        public string Description { get; set; }

        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Common.Title")]
        public string Title { get; set; }

        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Achievements.EventDate")]
        public string EventDate { get; set; }

        public long StudentId { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Goals.TeacherName")]
        public long TeacherId { get; set; }
        public bool ShowOnPortfolio { get; set; }
        public int TotalCount { get; set; }
        public long CreatedBy { get; set; }
        public IEnumerable<HttpPostedFileBase> Certificates { get; set; }
        public List<AchievementFiles> lstAchievementFiles { get; set; }
    }

    
}