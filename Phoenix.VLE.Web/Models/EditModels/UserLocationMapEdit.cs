﻿using System;
using Phoenix.Common.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Phoenix.VLE.Web.EditModels
{
    public class UserLocationMapEdit
    {
        public long UserId { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public List<ListItem> SelectedBUList { get; set; }
    }
    
}