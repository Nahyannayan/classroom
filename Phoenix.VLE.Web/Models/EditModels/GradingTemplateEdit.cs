﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "ListCategories.GradingTemplates")]
    public class GradingTemplateEdit
    {
        public int GradingTemplateId { get; set; }
        public int SchoolId { get; set; }
        public string GradingTemplateTitle { get; set; }
        public string GradingTemplateDesc { get; set; }
        public bool AllowAdditionalGrade { get; set; }
        public string GradingTemplateLogo { get; set; }
        public bool IsActive { get; set; }
        public bool IsAddMode { get; set; }
        public bool IsEditable { get; set; }
        public string GradingTemplateXml { get; set; }

    }
}