﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "SuggestionBox.Suggestion")]
    public class SuggestionEdit
    {
        public SuggestionEdit()
        { }
        public int SuggestionId { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int TypeId { get; set; }
        public string FileName { get; set; }
        public string PhysicalPath { get; set; }
        public string Remark { get; set; }
        public bool IsAddMode { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }

    }
}