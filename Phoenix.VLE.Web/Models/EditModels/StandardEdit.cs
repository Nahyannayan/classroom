﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Course.Standard")]
    public class StandardEdit
    {
        public StandardEdit()
        {

        }

        public StandardEdit(Int64 ID)
        {
            StandardID = ID;
        }

        public Int64 StandardID { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public Int64 ParentID { get; set; }
        public Int64 AgeGroupID { get; set; }
        public Int64 StepsID { get; set; }
        public bool IsAddMode { get; set; }
        public string ParentGroupName { get; set; }

        public Int64 Courseid { get; set; }
        public Int64 Termid  { get; set; }
    }
}