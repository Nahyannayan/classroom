﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Models
{
    [ResourceMappingRoot(Path = "SecurityManagement.AbuseDelegate")]
    public class AbuseDelegateEdit
    {

        public Int64 AbuseDelegateId { get; set; }
        public Int64 DelegateId { get; set; }
        public Int64 SchoolId { get; set; }
        public List<SelectListItem> StaffList { get; set; }
        public List<SelectListItem> DelegateList { get; set; }
        public string SelectedDelegateId { get; set; }
        public List<SelectListItem> SelectedDelegateList { get; set; }
        public Int64 AbuseContactInfoId { get; set; }
        public string ContactMessage { get; set; }
        public string ContactNumber { get; set; }
        public string ContactEmail { get; set; }
        public Int64 CensorNoticeRecieverId { get; set; }

        public Int64 CreatedBy { get; set; }
        public Int64 UpdatedBy { get; set; }

        public bool IsActive { get; set; }
        public bool EnableCallingPolice { get; set; }
        public string PolicePhoneNumber { get; set; }
        public bool IsSOSEnabledInMobile { get; set; }
        public bool IsReportAbuseEnabledInMobile { get; set; }

        public AbuseDelegateEdit()
        {
            StaffList = new List<SelectListItem>();
            DelegateList = new List<SelectListItem>();
            SelectedDelegateList = new List<SelectListItem>();
        }
    }
}