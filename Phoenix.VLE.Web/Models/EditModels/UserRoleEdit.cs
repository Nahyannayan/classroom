﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Users.UserRole")]
    public class UserRoleEdit
    { 
        public int UserRoleId { get; set; }
        public string UserRoleName { get; set; }

        public string UserRoleXml { get; set; }
        public bool IsEditable { get; set; }
        public bool IsActive { get; set; }
        public bool IsAddMode { get; set; }
    }
}