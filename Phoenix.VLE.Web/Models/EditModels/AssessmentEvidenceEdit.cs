﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phoenix.Models.Entities;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Course.AssessmentEvidence")]
    public class AssessmentEvidenceEdit : Attachments
    {
        public AssessmentEvidenceEdit()
        {
        }
        public AssessmentEvidenceEdit(Int64 ID)
        {
            AssesmentEvidenceID = ID;
        }

        public long AssesmentEvidenceID { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public int AssessmentTypeId { get; set; }
        public bool IsAddMode { get; set; }
        public long CourseId { get; set; }
        public long UserId { get; set; }
        public long UnitId { get; set; }
        public string AttachmentKey { get; set; }

    }
}