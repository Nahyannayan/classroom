﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Models.EditModels
{
    public class AssessmentModel
    {
        
        public string AcademicYear { get; set; }
        public IEnumerable<SelectListItem> dropAcdemicYear { get; set; }
        public List<PdfInfoModel>pdfInfoModellist { get; set; }
        public List<StudentList> studentList { get; set; }
    }
    public class StudentList
    {
        public string StudentFirstName { get; set; }
        public string StudentLastName { get; set; }
        public int SchooolId { get; set; }
        public string StudentNumber { get; set; }
        public int StudentId { get; set; }
        public int schoolAcademicYearid { get; set; }
    }
}