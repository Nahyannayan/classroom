﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Goal.SchoolGoalEdit")]
    public class SchoolGoalEdit
    {
        public SchoolGoalEdit()
        {
        }
        public int GoalId { get; set; }
        public int SchoolId { get; set; }
        public string GoalName { get; set; }
        public string GoalDescription { get; set; }
        public string ExpectedDate { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public String CreatedByName { get; set; }
        public bool IsAddMode { get; set; }
        public bool IsApproved { get; set; }
        public string ApprovedBy { get; set; }

    }
}