﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models
{
    public class GradeBookEdit
    {
        public GradeBookEdit()
        {
            CourseIds = new List<long>();
            SchoolGradeIds = new List<long>();
            AssessmentTypeIds = new List<int>();
            AssignmentQuizSetups = new List<AssignmentQuizSetup>();
            Standardizeds = new List<StandardizedExaminationSetup>();
            InternalAssessments = new List<InternalAssessment>();
        }
        public long GradeBookId { get; set; }
        public string GradeBookName { get; set; }
        public long SchoolId { get; set; }
        public List<long> CourseIds { get; set; }
        public List<long> SchoolGradeIds { get; set; }
        public List<int> AssessmentTypeIds { get; set; }
        public long Createdby { get; set; }
        public TranModes Modes { get; set; }
        public int TotalCount { get; set; }
        public bool IsCopyGradebook { get; set; }
        public string ExternalExaminationIds { get; set; }
        public string SubExternalExaminationIds { get; set; }
        public List<AssignmentQuizSetup> AssignmentQuizSetups { get; set; }
        public List<StandardizedExaminationSetup> Standardizeds { get; set; }
        public List<InternalAssessment> InternalAssessments { get; set; }
    }
    [ResourceMappingRoot(Path = "Gradebook.Formula")]
    public class GradebookFormulaEdit
    {
        public long FormulaId { get; set; }
        public string Formula { get; set; }
        public string FormulaDescription { get; set; }
        public long SchoolId { get; set; }
        public string Assessments { get; set; }
        public IDictionary<string,string> Buttons { get; set; }
        public List<FormulaFormButton> GetFormulaFormButtons()
        {
            var list = new List<FormulaFormButton>();
            for (int i = 0; i <= 9; i++)
            {
                var iStr = i.ToString();
                list.Add(new FormulaFormButton { Text = iStr, Value = iStr, BtnClass = "btn-success" });
            }
            list.Add(new FormulaFormButton { Text = "Clear", Value = "C", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "Delete", Value = "D", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = ",", Value = ",", BtnClass = "btn-success" });

            list.Add(new FormulaFormButton { Text = "+", Value = "+", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "-", Value = "-", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "*", Value = "*", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "/", Value = "/", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "MOD", Value = "MOD", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "^", Value = "^", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "SQRT", Value = "SQRT", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "PI", Value = "PI", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "E", Value = "E", BtnClass = "btn-success" });

            list.Add(new FormulaFormButton { Text = "MAX", Value = "MAX", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "MIN", Value = "MIN", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "SUM", Value = "SUM", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "AVG", Value = "AVERAGE", BtnClass = "btn-success" });
            //list.Add(new FormulaFormButton { Text = "ABS", Value = "ABS", BtnClass = "btn-success" });
            //list.Add(new FormulaFormButton { Text = "ROUND", Value = "ROUND", BtnClass = "btn-success" });
            //list.Add(new FormulaFormButton { Text = "CEIL", Value = "CEIL", BtnClass = "btn-success" });
            //list.Add(new FormulaFormButton { Text = "FLOOR", Value = "FLOOR", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "<", Value = "<", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = ">", Value = ">", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = ">=", Value = ">=", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "<=", Value = "<=", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "<>", Value = "<>", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = "(", Value = "(", BtnClass = "btn-success" });
            list.Add(new FormulaFormButton { Text = ")", Value = ")", BtnClass = "btn-success" });
            return list;
        }
    }
    public class FormulaFormButton
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public string BtnClass { get; set; }
    }
    [ResourceMappingRoot(Path = "Gradebook.TeacherGradeBook")]
    public class TeacherInternalAssessmentEdit
    {
        public long InternalAssessmentId { get; set; }
        public string RowName { get; set; }
        public int RowType { get; set; }
        public int? FromMark { get; set; }
        public int? ToMark { get; set; }
        public int? GradingTemplateId { get; set; }
        public int? CommentLength { get; set; }
        public long GroupId { get; set; }
        public long ParentInternalAssessmentId { get; set; }
        public long CreatedBy { get; set; }
        public int ColumnCount { get; set; }
        public List<long> StudentIds { get; set; }
        public int Index { get; set; }
    }
    public class InternalAssessmentRowModel
    {
        public InternalAssessmentRowModel()
        {
            GradingTemplateItems = new List<GradeBookGradingTemplate>();
            StudentIds = new List<long>();
            InternalAssessmentScores = new List<InternalAssessmentScore>();
            GradebookRuleSetups = new GradebookRuleSetup();
        }
        public List<GradeBookGradingTemplate> GradingTemplateItems { get; set; }
        public TeacherInternalAssessmentEdit NewRowEdit { get; set; }
        public IEnumerable<long> StudentIds { get; set; }
        public IEnumerable<InternalAssessmentScore> InternalAssessmentScores { get; set; }
        public InternalAssessment InternalAssessment { get; set; }
        public GradebookRuleSetup GradebookRuleSetups { get; set; }
    }    
}