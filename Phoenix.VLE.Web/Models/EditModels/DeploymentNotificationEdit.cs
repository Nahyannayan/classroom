﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class DeploymentNotificationEdit
    {
        public int DeploymentNotificationId { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "Notifications.DeploymentNotification.NotificationMessage")]
        public string NotificationMessage { get; set; }
        public bool IsActive { get; set; }
    }
}