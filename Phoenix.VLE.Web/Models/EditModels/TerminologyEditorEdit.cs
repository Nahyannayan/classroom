﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models
{
    [ResourceMappingRoot(Path = "TerminologyEditor.TerminologyEditor")]
    public class TerminologyEditorEdit
    {
        public int Id { get; set; }
        public long UserId { get; set; }
        public long SchoolId { get; set; }
        public string OldTerm { get; set; }
        public string NewTerm { get; set; }
        public long CreatedBy { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsAddMode { get; set; }
    }
}