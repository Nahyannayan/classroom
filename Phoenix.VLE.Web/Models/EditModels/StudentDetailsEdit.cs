﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models.EditModels
{
    public class StudentDetailsEdit
    {
        public string Name { get; set; }
        public string StudentNumber { get; set; }
        public string ID { get; set; }
        public string Grade { get; set; }
        public string NameAsNationalId { get; set; }
        public string gender { get; set; }
        public string Religion { get; set; }
        public string DOB { get; set; }
        public string CountryOfBirth { get; set; }
        public string Nationality { get; set; }
        public string Nationality2 { get; set; }
        public string PhotoPath { get; set; }
        public string School { get; set; }
        public string PrimaryContact { get; set; }
        public string MobNo { get; set; }
        public string EmailID { get; set; }
        public bool IsTCRequest { get; set; }

    }
}