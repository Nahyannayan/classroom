﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class SkillEndorsementEdit
    {
        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Endorsement.TeacherName")]
        public long TeacherId { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Endorsement.StudentSkill")]
        public string[] SkillIds { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Skills.SkillDescription")]
        public string Message { get; set; }
        public long StudentUserId { get; set; }
        public short? Rating { get; set; }
        public char Mode { get; set; }
        public int SkillEndorsedId { get; set; }
        public int SkillId { get; set; }
        public long CreatedBy { get; set; }
        public bool RequestEndorsement { get; set; }
        public bool ShowOnDashboard { get; set; }
    }
}