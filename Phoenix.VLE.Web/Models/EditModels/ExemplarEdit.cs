﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Exemplar.ContentResource")]
    public class ExemplarEdit
    {
        public ExemplarEdit()
        {
            GroupsList = new List<SchoolGroup>();
            StudentList = new List<Student>();//deependra
            SchoolLevelList = new List<SchoolLevel>();//deependra
            SchoolDepartmentList = new List<SchoolDepartment>();//deependra
            SchoolCourseList = new List<SchoolCourse>();//deependra
            AllowedFileExtension = new List<string>();
        }

        public List<string> AllowedFileExtension { get; set; }
        public List<SchoolGroup> GroupsList { get; set; }
        public List<Student> StudentList { get; set; }
        public List<SchoolLevel> SchoolLevelList { get; set; }
        public List<SchoolDepartment> SchoolDepartmentList { get; set; }
        public List<SchoolCourse> SchoolCourseList { get; set; }
        public HttpFileCollectionBase PostedImage { get; set; }
        public long? ExemplarWallId { get; set; }
        public long? SchoolId { get; set; }
        
        public string PostTitle { get; set; }
        public string PostDescription { get; set; }
        public string TaggedGroup { get; set; }
        public string TaggedStudent { get; set; }
        public string WinnerStudent { get; set; }
        public bool? IsDepartmentWall { get; set; }
        public bool? IsCourseWall { get; set; }
        public string SchoolLevelId { get; set; }
        public string ReferenceLink { get; set; }
        public string EmbededVideoLink { get; set; }
        public string AdditionalDocPath { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<DateTime> UpdatedOn { get; set; }
        public Nullable<long> DeletedBy { get; set; }
        public Nullable<DateTime> DeletedOn { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<long> UserId { get; set; }

        public int SortOrder { get; set; }

        public int IsPublish { get; set; }

        public int BlogTypeId { get; set; }

        public string DeleteReason { get; set; }
        public bool IsApprove { get; set; }
        public long? CourseId { get; set; }

        public bool IsAddMode { get; set; }
        public string ParentSharableLink { get; set; }
        public string FileNames { get; set; }
    }
}