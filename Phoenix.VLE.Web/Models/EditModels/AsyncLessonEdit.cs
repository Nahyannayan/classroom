﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "SchoolGroup.AsyncLesson")]
    public class AsyncLessonEdit
    {
        public long AsyncLessonId { get; set; }
        public string LessonTitle { get; set; }
        public string Description { get; set; }
        public long CourseId { get; set; }
        public string CourseName { get; set; }
        public long PlanSchemeId { get; set; }
        public string PlanSchemeName { get; set; }
        public string Students { get; set; }
        public long UnitId { get; set; }
        public string UnitName { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }
        public long FolderId { get; set; }
        public long SectionId { get; set; }
        public long ModuleId { get; set; }
        public bool IsActive { get; set; }
        public bool IsPublish { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsAddMode { get; set; }

        public List<HttpPostedFileBase> PostedDocuments { get; set; }

        public List<SelectListItem> PlanSchemeList { get; set; }
        public List<SelectListItem> UnitList { get; set; }
        public List<SelectListItem> StudentList { get; set; }
        public List<SelectListItem> CourseList { get; set; }

        public AsyncLessonEdit()
        {
            PlanSchemeList = new List<SelectListItem>();
            UnitList = new List<SelectListItem>();
            StudentList = new List<SelectListItem>();
            CourseList = new List<SelectListItem>();
        }
    }

    public class AsyncLessonResourcesActivitiesEdit
    {
        public long ResourceActivityId { get; set; }
        public long AsyncLessonId { get; set; }
        public int ModuleId { get; set; }
        public long SectionId { get; set; }
        public long FolderId { get; set; }
        public int StepNo { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public int FileTypeId { get; set; }
        public string Icon { get; set; }
        public int Status { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime UpdatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public decimal FileSizeInMB { get; set; }
        public string GoogleDriveFileId { get; set; }
        public int ResourceFileTypeId { get; set; }
        public string PhysicalPath { get; set; }

        public List<HttpPostedFileBase> PostedDocuments { get; set; }
    }
}