﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "MarkingPolicy.MarkingPolicy")]
    public class MarkingPolicyEdit
    {
        public MarkingPolicyEdit()
        {

        }

        public MarkingPolicyEdit(int id)
        {
            MarkingPolicyId = id;
        }

        public Int64 MarkingPolicyId { get; set; }
        public string Symbol { get; set; }
        public string Description { get; set; }
        public Decimal Weight { get; set; }
        public string ColorCode { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public string SchoolId { get; set; }
        public bool IsAddMode { get; set; }
    }
}
