﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "StudentInformation.Certificates")]
    public class CertificateEdit
    {
        public int CertificateId { get; set; }
        public string CertificateName { get; set; }
        public string CertificateDescription { get; set; }
        public string Status { get; set; }
        public int CreatedBy { get; set; }
        public bool IsAddMode { get; set; }
        public long SchoolId { get; set; }
        public string FileName { get; set; }
        public int count { get; set; }
        public string FieldValue { get; set; }
    }
}