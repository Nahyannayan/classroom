﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "School.Theme")]
    public class SchoolThemeEdit
    {
        public int ThemeId { get; set; }
        public int SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string ThemeName { get; set; }
        public string ThemeCssFileName { get; set; }
        public string ThemePreviewImage { get; set; }
        public bool IsAddMode { get; set; }
        public int SchoolGradeId { get; set; }
        public string[] SchoolGradeIdList { get; set; }
    }
}