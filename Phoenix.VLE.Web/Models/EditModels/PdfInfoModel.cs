﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models.EditModels
{
    public class PdfInfoModel
    {
       public string rpF_ID { get; set;}
        public string rpF_DESCR { get; set;}
        public string rpP_RELEASEDATE { get; set;}
        public int isnew { get; set; }
        public string ACD_ID { get; set; }
        public  string StudentNumber { get; set; }
        public bool ShowOnDashboard { get; set; }
    }
}