﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class BBBMeetingEdit
    {
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingName")]
        public string MeetingName { get; set; }
        public int SchoolGroupId { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingDurationInMinutes")]
        public short Duration { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingStartDate")]
        public string StartDate { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingStartTime")]
        public string StartTime { get; set; }

        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingPassword")]
        public string MeetingPassword { get; set; }

    }
}