﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models.EditModels
{
    public class HifzTrackerFileEdit
    {       
        public int HifzTrackerId { get; set; }
        public string StudentID { get; set; }
        public string VerseNo { get; set; }
        public bool IsCompleted { get; set; }
        public string File { get; set; }

    }
    public class ResultSaveHifz : CommonResponse
    {
        public dynamic data { get; set; }
    }
}