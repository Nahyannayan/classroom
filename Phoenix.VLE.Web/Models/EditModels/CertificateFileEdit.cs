﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class CertificateFileEdit
    {
        public HttpPostedFileBase Certificates { get; set; }
        public long UserId { get; set; }
        public long CertificateUserId { get; set; }
        public short CertificateTypeId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int FileTypeId { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Common.Title")]
        public string CertificateDescription { get; set; }
        public int CertificateId { get; set; }
        public string PhysicalFilePath { get; set; }
    }
}