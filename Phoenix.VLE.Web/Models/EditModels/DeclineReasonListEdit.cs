﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class DeclineReasonListEdit
    {
		public DeclineReasonListEdit(int _schoolGradeId)
		{
			SchoolGradeId = _schoolGradeId;
		}
		public int SchoolId { get; set; }
		public int DeclineReasonId { get; set; }
		public int SchoolGradeId { get; set; }
		public bool IsAddMode { get; set; }
		public string DeclineReasonName { get; set; }
		public string DeclineReasonDescription { get; set; }
		public bool IsApproved { get; set; }
		public bool IsActive { get; set; }
		public string CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public int DeletedBy { get; set; }
		public DateTime DeletedOn { get; set; }
		public string ApprovedBy { get; set; }
		public DateTime ApprovedOn { get; set; }
		public List<ListItem> SchoolGradeList { get; set; }
		public DeclineReasonListEdit()
		{
			SchoolGradeList = new List<ListItem>();
		}
		public string DeclineReasonSetXml { get; set; }
	}
}