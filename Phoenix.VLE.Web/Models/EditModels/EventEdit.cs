﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Planner.MyPlanner")]
    public class EventEdit
    {
        public EventEdit()
        {
            TeacherList = new List<SelectListItem>();
            SelectedTeacherList = new List<SelectListItem>();
            StudentList = new List<SelectListItem>();
            SelectedStudentList = new List<SelectListItem>();
            UnAssignedMemberList = new List<Common.Models.ListItem>();
            AssignedMemberList = new List<Common.Models.ListItem>();
            EventExternalUser = new List<EventUser>();
            EventInternalUser = new List<EventUser>();
        }
        public int EventId { get; set; }
        public long SchoolId { get; set; }
        public long UserId { get; set; }
        public string SelectedDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int DurationId { get; set; }
        public int EventCategoryId { get; set; }
        public string Title { get; set; }
        public string Venue { get; set; }
        public string Description { get; set; }
        public string EventTypeId { get; set; }
        public string ResourceFile { get; set; }
        //public HttpPostedFileBase ResourceFileUploade { get; set; }
        public string EventPriority { get; set; }
        public string Extension { get; set; }
        public string Icon { get; set; }
        public string FileName { get; set; }
        public bool IsTeacherVisible { get; set; }
        public int EventRepeatTypeId { get; set; }
        public string EventRepeatTimes { get; set; }
        public bool IsCopyMessage { get; set; }
        public string ColorCode { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByUserName { get; set; }
        public DateTime UpdatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public string Duration { get; set; }
        public string EventCategory { get; set; }
        public bool IsAddMode { get; set; }
        public string SelectedTeacherId { get; set; }
        public string DeselectedTeacherId { get; set; }
        public string SelectedStudentId { get; set; }
        public string DeselectedStudentId { get; set; }
        public string SelectedPlannerMemberId { get; set; }
        public string DeselectedPlannerMemberId { get; set; }
        public string ExternalEmails { get; set; }
        public IEnumerable<Common.Models.ListItem> UnAssignedMemberList { get; set; }
        public List<Common.Models.ListItem> AssignedMemberList { get; set; }
        public List<SelectListItem> TeacherList { get; set; }
        public List<SelectListItem> SelectedTeacherList { get; set; }
        public List<SelectListItem> StudentList { get; set; }
        public List<SelectListItem> SelectedStudentList { get; set; }
        public List<EventUser> EventExternalUser { get; set; }
        public List<EventUser> EventInternalUser { get; set; }
        public string OnlineMeetingId { get; set; }
        public short MeetingDuration { get; set; }
        public string MeetingUrl { get; set; }
        public long MeetingCreatedBy { get; set; }
        public short OnlineMeetingType { get; set; }
        public string EventPassword { get; set; }
        public int DateTimeOffset { get; set; }
        public string PhysicalFilePath { get; set; }
        public string RecordingLink { get; set; }
        public MSTeamsReponse TeamMeeting { get; set; }
        public short OldMeetingType { get; set; }
        public bool IsRecurringEvent { get; set; }
        public string RecurringEventDays { get; set; }
        public DateTime? RecurringStartDate { get; set; }
        public DateTime? RecurringEndDate { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }
    }
    public class EventUser
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
        public bool IsRSVP { get; set; }
        public short OnlineMeetingType { get; set; }
        public string UserTypeName { get; set; }
    }

}
