﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models.EditModels
{
    public class HifzTrackerEdit
    {
        public int HifzTrackerId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public bool IsCertificateEnabled { get; set; }
        public int HifzTransactionId { get; set; }
        public int HifzRelationId { get; set; }
        public string VersesName { get; set; }
        public string UploadedFileName { get; set; }
        public bool IsApproved { get; set; }
        public string DownloadURL { get; set; }
        public string StudentId { get; set; }
    }

    public class HifzTrackerRoot : JasonResult
    {
        public List<HifzTrackerEdit> data { get; set; }
    }
}