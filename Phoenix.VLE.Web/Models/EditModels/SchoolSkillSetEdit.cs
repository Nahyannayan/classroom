﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Skill.SchoolSkillSetEdit")]
    public class SchoolSkillSetEdit
    {
       
        public SchoolSkillSetEdit(int _schoolGradeId)
        {
            SchoolGradeId = _schoolGradeId;
        }
        public int SkillId { get; set; }
        public int SchoolId { get; set; }
        public int SchoolGradeId { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "Skill.SchoolSkillSet.SkillName")]
        public string SkillName { get; set; }
        public string SkillDescription { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public String CreatedByName { get; set; }
        public bool IsAddMode { get; set; }
        public bool IsApproved { get; set; }
        public string ApprovedBy { get; set; }

        public List<ListItem> SchoolGradeList { get; set; }
        public SchoolSkillSetEdit()
        {
            SchoolGradeList = new List<ListItem>();
        }
        public string SkillSetXml { get; set; }
    }
}