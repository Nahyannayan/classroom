﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Phoenix.Models.Entities;
using Phoenix.Models;

namespace Phoenix.VLE.Web.EditModels
{
    //[ResourceMappingRoot(Path = "School.Blog")]
    public class BlogEdit : Attachments
    {

        public long BlogId { get; set; }
        public long SchoolId { get; set; }
        public int BlogTypeId { get; set; }
        public string BlogType { get; set; }
        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public int EnableCommentById { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public string EmbedUrl { get; set; }
        public string BlogImage { get; set; }
        public int SortOrder { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        //public Nullable<System.DateTime> RegistrationDate { get; set; }
        public DateTime CloseDiscussionDate { get; set; }
        public List<string> AllowedFileExtension { get; set; }
        public List<string> AllowedImageExtension { get; set; }
        public HttpPostedFileBase PostedImage { get; set; }
        public List<HttpPostedFileBase> PostedDocuments { get; set; }
        public List<BlogCloudFile> ListCloudFile { get; set; }
        public bool IsPublish { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UserProfileImage { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool ModerationRequired { get; set; }
        public List<SelectListItem> CategoryList { get; set; }
        public List<SelectListItem> GroupList { get; set; }
        public List<SelectListItem> BlogTypeList { get; set; }
        public List<SelectListItem> EnableCommentForList { get; set; }
        public bool IsAddMode { get; set; }
        public string SelectedStudentIDs { get; set; }
        public int CommentApprovedById { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? FromDate { get; set; }
        public string ScheduledTime { get; set; }
        public List<SelectListItem> SelectedStudentList { get; set; }
        public int UserTypeId { get; set; }
        public bool IsVisibleToParent { get; set; }
        public string strToDate { get; set; }
        public string strFromDate { get; set; }


        public BlogEdit()
        {
            CategoryList = new List<SelectListItem>();
            GroupList = new List<SelectListItem>();
            BlogTypeList = new List<SelectListItem>();
            EnableCommentForList = new List<SelectListItem>();
            PostedDocuments = new List<HttpPostedFileBase>();
            SelectedStudentList = new List<SelectListItem>();
            ListCloudFile = new List<BlogCloudFile>();
        }
    }
}