﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models.EditModels
{
    [ResourceMappingRoot(Path = "Quiz.GroupQuiz")]
    public class GroupQuizEdit
    {
        public int GroupId { get; set; }
        public int QuizId { get; set; }
        public int GradingTemplateId { get; set; }
        public string GradingTemplateTitle { get; set; }
        public bool IsAddMode { get; set; }
        public string Subjects { get; set; }
        public List<string> SubjectIds { get; set; }
        public List<string> SubjectId { get; set; }
        public List<string> CourseIds { get; set; }
        public List<string> CourseId { get; set; }
        public int ModuleId { get; set; }
        public long FolderId { get; set; }
        public bool IsSetTime { get; set; }
        public bool IsHideScore { get; set; }
        public bool IsRandomQuestion { get; set; }
        public int QuizTime { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartTime { get; set; }
        public DateTime? ShowScoreDate { get; set; }
        public int MaxSubmit { get; set; }
        public int QuestionPaginationRange { get; set; }
        public string strStartDate { get; set; }
        public GroupQuizEdit()
        {
            CourseId = new List<string>();
            CourseIds = new List<string>();
        }
        public string strShowScoreDate { get; set; }
    }
}