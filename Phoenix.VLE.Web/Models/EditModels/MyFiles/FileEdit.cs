﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Files.File")]
    public class FileEdit
    {
        public long FileId { get; set; }
        public long SchoolId { get; set; }
        public string Extension { get; set; }
        public int ModuleId { get; set; }
        public long SectionId { get; set; }
        public string SectId { get; set; }
        public long FolderId { get; set; }
        public string ParentFolderId { get; set; }
        public string FolderName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string PhysicalFilePath { get; set; }
        public List<HttpPostedFileBase> PostedFileList { get; set; }
        public HttpPostedFileBase PostedFile { get; set; }
        public int FileTypeId { get; set; }
        public string FileTypeName { get; set; }
        public string Icon { get; set; }
        public bool PinToHome { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsAddMode { get; set; }
        public bool EnableAllFileDownloads { get; set; }
        public double FileSizeInMB { get; set; }
        public short ResourceFileTypeId { get; set; }
        public string GoogleDriveFileId { get; set; }
        public string FlatIcon { get; set; }
    }
}