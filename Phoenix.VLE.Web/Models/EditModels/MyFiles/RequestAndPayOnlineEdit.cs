﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models.EditModels.MyFiles
{
    public class RequestAndPayOnlineEdit
    {
        public int Id { get; set; }
        public string EventName { get; set; }
        public int FeeTypeId { get; set; }
        public double EventCost { get; set; }
        public double PaymentProcessingCharge { get; set; }
        public int PaymentTypeID { get; set; }
        public bool OnlinePaymentAllowed { get; set; }
        public string EnrolledSiblingStudentIDs { get; set; }
        public string ApdId { get; set; }
        public string FeeCollType { get; set; }
        public string StudentNumber { get; set; }
        public string AlD_ID { get; set; }
        public int Couponcount { get; set; }        
        public string Radioselect { get; set; }
        public int EnrollmentRequestType { get; set; }
        public string Comment { get; set; }
        public bool IsTermsExist { get; set; }
    }
}