﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Files.Bookmark")]
    public class BookmarkEdit
    {
        public long BookmarkId { get; set; }
        
       // [RegularExpression(@"^\S*$", ErrorMessage = "No white space allowed")]
        public string BookmarkName { get; set; }
        public string BookmarkDescription { get; set; }
        public string URL { get; set; }
        public List<SchoolGroup> lstSchoolGroup { get; set; }
        public List<string> SchoolGroupIds { get; set; }
        public List<string> SchoolGroupId { get; set; }
        public string ThumbnailImage { get; set; }
        public bool IsAddMode { get; set; }
        public int UserId { get; set; }
    }
}