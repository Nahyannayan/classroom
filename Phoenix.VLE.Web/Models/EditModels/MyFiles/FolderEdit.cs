﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Files.Folder")]
    public class FolderEdit
    {
       
        public long FolderId { get; set; }
        public long SchoolId { get; set; }
        public int ModuleId { get; set; }
        public long SectionId { get; set; }
        public string SectId { get; set; }
        public string FolderName { get; set; }
        public long ParentFolderId { get; set; }
        public string ParentFdId { get; set; }
        public string ParentFolderName { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedByName { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedByName { get; set; }
        public bool IsAddMode { get; set; }
        public int TotalCount { get; set; }
    }
}