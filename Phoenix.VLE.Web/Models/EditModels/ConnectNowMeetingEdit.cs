﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class ConnectNowMeetingEdit
    {
        public int SchoolGroupId { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingName")]
        public string MeetingName { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingStartDate")]
        public string MeetingStartDate { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingEndDate")]
        public string MeetingEndDate { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingUrl")]
        public string MeetingUrl { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingStartTime")]

        public string MeetingStartTime { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingEndTime")]

        public string MeetingEndTime { get; set; }
        public string FolderId { get; set; }
        public long CreatedBy { get; set; }

    }
}