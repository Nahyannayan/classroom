﻿using Phoenix.Common.CustomAttributes;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "School.BlogComment")]
    public class BlogCommentEdit
    {
        public long CommentId { get; set; }
        public long BlogId { get; set; }

        [AllowHtml]
        public string Comment { get; set; }
        public bool IsPublish { get; set; }
        public long PublishBy { get; set; }
        public string CreatedByName { get; set; }
        public long CreatedBy { get; set; }
        public List<HttpPostedFileBase> PostedDocuments { get; set; }
        public List<HttpPostedFileBase> PostedImages { get; set; }
        public long UpdatedBy { get; set; }
    }
}