﻿
namespace Phoenix.VLE.Web.EditModels
{
    public class CustomPermissionEdit
    {
        public short UserRoleId { get; set; }

        public short PermissionId { get; set; }
        public short ModuleId { get; set; }
        public bool GrantPermission { get; set; }
        public short ParentModuleId { get; set; }
    }
}
