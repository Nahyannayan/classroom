﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Observation.Observation")]
    public class ObservationEdit
    {
        public ObservationEdit()
        {
            lstStudent = new List<ObservationStudent>();
            lstObjective = new List<Objective>();
        }
        public int ObservationId { get; set; }
        public string ObservationTitle { get; set; }
        public string ObservationTitleXml { get; set; }
        [AllowHtml]
        public string ObservationDesc { get; set; }
        public string ObservationDescXml { get; set; }
       
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        public bool IsPublished { get; set; }
        public bool IsAddMode { get; set; }     
        //public List<string> SchoolGroupId { get; set; }
        //public List<string> SchoolGroupIds { get; set; }
        public List<string> StudentId { get; set; }
        public List<string> StudentIds { get; set; }
        public int SchoolId { get; set; }       
        public long CreatedById { get; set; }
        public bool IsGradingEnable { get; set; }
        public int GradingTemplateId { get; set; }
        public List<ObservationFile> lstDocuments { get; set; }
        public string GradingTemplate { get; set; }
        public string GradingTemplateLogo { get; set; }     
        public bool IsResubmit { get; set; }
        public List<Objective> lstObjective { get; set; }
        public List<string> SchoolGroupId { get; set; }
        public List<string> SchoolGroupIds { get; set; }
        //public List<string> SubjectId { get; set; }
        //public List<string> SubjectIds { get; set; }
        public bool IsArchived { get; set; }
        public List<string> CourseId { get; set; }
        public List<string> CourseIds { get; set; }
        public List<ObservationStudent> lstStudent  { get; set; }
        public string strStartDate { get; set; }
    }
}