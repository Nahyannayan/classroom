﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class StudentGoalEdit
    {
        public long UserId { get; set; }
        public long GoalId { get; set; }

        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Common.Description")]
        public string GoalDescription { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Common.Title")]
        public string GoalName { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Goals.ExpectedDate")]
        public string ExpectedDate { get; set; }
        public long StudentId { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Goals.TeacherName")]
        public long TeacherId { get; set; }
        public bool ShowOnDashboard { get; set; }
        public int TotalCount { get; set; }
        public long CreatedUserId { get; set; }
    }
}