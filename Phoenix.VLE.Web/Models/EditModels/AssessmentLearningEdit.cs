﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phoenix.Models.Entities;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Course.AssessmentLearning")]
    public class AssessmentLearningEdit : Attachments
    {
        public AssessmentLearningEdit()
        {
        }
        public AssessmentLearningEdit(Int64 ID)
        {
            AssesmentLearningID = ID;
        }

        public long AssesmentLearningID { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public int DeliveryMode { get; set; }
        public bool IsAddMode { get; set; }
        public long CourseId { get; set; }
        public long UserId { get; set; }
        public long UnitId { get; set; }
        public string AttachmentKey { get; set; }

    }
}