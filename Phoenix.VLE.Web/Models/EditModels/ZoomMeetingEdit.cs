﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class ZoomMeetingEdit
    {
        public int SchoolGroupId { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingName")]
        public string MeetingName { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.ZoomEmail")]
        public string ZoomEmail { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingPassword")]
        public string MeetingPassword { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingStartDate")]
        public DateTime MeetingDate { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.MeetingStartTime")]
        public string MeetingTime { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.ZoomMeetingDuration")]
        public string MeetingDuration { get; set; }

        public int TimeZoneOffset { get; set; }
        public string FormattedMeetingDateTime { get; set; }
        public string ContactName { get; set; }
        public long CreatedBy { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "SchoolGroup.SchoolGroup.strMeetingStartDate")]
        public string strMeetingDate { get; set; }
    }
}