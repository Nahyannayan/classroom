﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "ContentLibrary.ContentResource")]
    public class ContentEdit
    {
        public ContentEdit()
        {
             SubjectId = new List<string>();
             SubjectIds = new List<string>();
        }
        public long ContentId { get; set; }
        public string ContentName { get; set; }
        public string Description { get; set; }
        public int ContentType { get; set; }
        public string RedirectUrl { get; set; }
        public string ContentImage { get; set; }
        public string FileExtenstion { get; set; }
        public bool IsSharepointFile { get; set; }
        public List<string> SubjectId { get; set; }
        public List<string> SubjectIds { get; set; }
        public int SelectedSubjectId { get; set; }
        public string SelectedSubjectName { get; set; }
        public string Subject { get; set; }
        public bool IsApproved { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public bool IsAddMode { get; set; }
        public string PhysicalPath { get; set; }
    }
}