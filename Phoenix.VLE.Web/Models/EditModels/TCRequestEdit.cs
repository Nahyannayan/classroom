﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models.EditModels
{
    public class TCRequestEdit
    {
        public string StudentID { get; set; }
        public string LastAttDate { get; set; }
        public string LeaveDate { get; set; }
        public int TotalDays { get; set; }
        public int TotalPresentDays { get; set; }
        public int TransferReasonID { get; set; }
        public long TransferTypeID { get; set; }
        public Boolean ISGemsTC { get; set; }
        public long TransferBSUID { get; set; }
        public string TransferFeedback { get; set; }
        public string TransferZone { get; set; }
        public Boolean IsOnlineTransfer { get; set; }
        public List<TCAnswer> AnswerList { get; set; }
    }
}