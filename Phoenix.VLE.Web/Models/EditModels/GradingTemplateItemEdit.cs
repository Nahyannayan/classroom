﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "ListCategories.GradingTemplateItem")]
    public class GradingTemplateItemEdit
    {
        public int GradingTemplateItemId { get; set; }
        public int GradingTemplateId { get; set; }
        public string GradingColor { get; set; }
        public string ShortLabel { get; set; }
        public string ShortLabelXml { get; set; }
        public string GradingItemDescription { get; set; }
        public string GradingTemplateItemSymbol { get; set; }
        public int Percentage { get; set; }
        public int SortOrder { get; set; }
        public bool IsActive { get; set; }
        public bool IsAddMode { get; set; }
        // adding score from score to --syed | 13 aug 2020
        public decimal ScoreFrom { get; set; }
        public decimal ScoreTo { get; set; }
        public string GradingTemplateItemXml { get; set; }
    }
}