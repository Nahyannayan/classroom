﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "SchoolBanner.Banner")]
    public class SchoolBannerEdit
    {
        public SchoolBannerEdit()
        {

        }

        public SchoolBannerEdit(int id)
        {
            SchoolBannerId = id;
        }

        public Int64 SchoolBannerId { get; set; }
        public string BannerName { get; set; }
        public string ActualImageName { get; set; }
        public string UploadedImageName { get; set; }
        public DateTime? BannerPublishDate { get; set; }
        public DateTime BannerEndDate { get; set; }
        public string ClickURL { get; set; }        
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DisplayOrder { get; set; }
        public List<string> SchoolId { get; set; }
        public List<string> SchoolIds { get; set; }
        public List<string> UserTypeId { get; set; }
        public List<string> UserTypeIds { get; set; }
        public bool IsAddMode { get; set; }
        public HttpPostedFileBase bannerFile { get; set; }
        public string SchoolBannerXml { get; set; }
    }
}
