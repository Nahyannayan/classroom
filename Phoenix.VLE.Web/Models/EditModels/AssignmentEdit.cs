﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "Assignment.Assignment")]
    public class AssignmentEdit
    {
        public int AssignmentId { get; set; }
        public string AssignmentTitle { get; set; }
        public string AssignmentTitleXml { get; set; }
        [AllowHtml]
        public string AssignmentDesc { get; set; }
        public string AssignmentDescXml { get; set; }
        public int SubmissionType { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DueDate { get; set; }
        public TimeSpan? DueTime { get; set; }
        public string DueTimeString { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ArchiveDate { get; set; }
        public bool IsPublished { get; set; }
        public bool IsTaskOrderRequired { get; set; }
        public bool IsAddMode { get; set; }
        public bool isChangeReverted { get; set; }
        public bool IsCloned { get; set; }
        public long TeacherId { get; set; }
        public string SubjectName { get; set; }
        public List<string> SchoolGroupId { get; set; }
        public List<string> SchoolGroupIds { get; set; }
        public List<string> CourseId { get; set; }
        public List<string> CourseIds { get; set; }
        public List<string> StudentId { get; set; }
        public List<string> StudentIds { get; set; }
        public int SchoolId { get; set; }
        public bool IsInstantMessage { get; set; }
        public short InstantMessageUserTypeId { get; set; }
        public List<AssignmentTask> lstAssignmentTasks { get; set; }
        public int CreatedById { get; set; }
        public bool IsGradingEnable { get; set; }
        public int GradingTemplateId { get; set; }
        public List<AssignmentFile> lstDocuments { get; set; }
        public bool SendEmail { get; set; }
        public string GradingTemplate { get; set; }
        public string GradingTemplateLogo { get; set; }
        public int TopicId { get; set; }
        public int SubTopicId { get; set; }
        public List<string> ObjectiveId { get; set; }
        public List<string> ObjectiveIds { get; set; }
        public bool IsResubmit { get; set; }
        public List<Objective> lstObjective { get; set; }
        public List<CourseUnit> lstCourseUnit { get; set; }
        public List<GroupUnit> lstGroupTopicUnit { get; set; }
        public bool IsPeerMarkingEnable { get; set; }
        public bool IsConsiderForFinalGrade { get; set; }
        public bool IsAssignmentMarksEnable { get; set; }
        public int? PeerMarkingType { get; set; }
        public bool CreateSeperateFileCopy { get; set; }
        public bool IsInstantMailToParent { get; set; }
        public bool IsNotificationToParent { get; set; }
        public bool IsInstantNotificationMailToStudent { get; set; }
        public bool IsConsiderForGrading { get; set; }
        public int AssignmentCategoryId { get; set; }
        public bool EnablePlagarism { get; set; }
        public string strStartDate { get; set; }
        public string strDueDate { get; set; }
        public string strArchiveDate { get; set; }
        public decimal AssignmentMarks { get; set; }
    }

    [ResourceMappingRoot(Path = "Assignment.AssignmentTask")]
    public class AssignmentTaskEdit
    {
        public AssignmentTaskEdit()
        {
            SubjectIds = new List<string>();
            SubjectId = new List<string>();
            CourseIds = new List<string>();
            CourseId = new List<string>();
        }
        public int AssignmentId { get; set; }
        public int TaskId { get; set; }
        public string TaskTitle { get; set; }
        public string TaskTitleXml { get; set; }
        public string TaskDescription { get; set; }
        public int SortOrder { get; set; }
        public bool IsActive { get; set; }
        public bool IsAddMode { get; set; }
        public int? TaskGradingTemplateId { get; set; }
        public List<TaskFile> lstTaskFile { get; set; }
        public bool IsTaskEdited { get; set; }
        public int? QuizId { get; set; }
        public string Subjects { get; set; }
        public List<string> SubjectIds { get; set; }
        public List<string> CourseIds { get; set; }
        public List<string> SubjectId { get; set; }
        public List<string> CourseId { get; set; }
        public bool IsSetTime { get; set; }
        public bool IsRandomQuestion { get; set; }
        public int QuizTime { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartTime { get; set; }
        public int MaxSubmit { get; set; }
        public int QuestionPaginationRange { get; set; }
        public string RandomId { get; set; }
        public bool IsHideScore { get; set; }
        public DateTime? ShowScoreDate { get; set; }
        public string strStartDate { get; set; }
        public string strShowScoreDate { get; set; }
        public decimal TaskMarks { get; set; }
    }


    public class AssignmentStudentEdit
    {

        public int AssignmentId { get; set; }
        public int AssignmentStudentId { get; set; }
        public int StudentId { get; set; }

    }

}