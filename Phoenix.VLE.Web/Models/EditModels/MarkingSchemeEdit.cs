﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "ListCategories.MarkingScheme")]
    public class MarkingSchemeEdit
    {
        public int MarkingSchemeId { get; set; }
        public int SchoolId { get; set; }
        public string ColourCode { get; set; }
        public string MarkingName { get; set; }
        public string MarkingDesc { get; set; }
        public bool IsActive { get; set; }
        public bool IsAddMode { get; set; }
        public decimal Weight { get; set; }
        public string MarkingSchemeXml { get; set; }
    }
}