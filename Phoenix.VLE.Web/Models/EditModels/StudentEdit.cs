﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    public class StudentEdit
    {

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Standard { get; set; }
        public int StudentId { get; set; }
        public string StudentNumber { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string StudentImage { get; set; }
        public List<string> SchoolGroupId { get; set; }
        public string StudentInternalId { get; set; }
        public int TotalCount { get; set; }
        public string SchoolName { get; set; }
        [LocalizedDynamicValidators(ResourceKeyPath = "StudentInformation.Student.Description")]
        public string Description { get; set; }
    }
}