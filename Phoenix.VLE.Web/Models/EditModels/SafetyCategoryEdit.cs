﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Helpers;
using Phoenix.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "SecurityManagement.SafetyCategories")]
    public class SafetyCategoryEdit 
    {
        public SafetyCategoryEdit()
        {
            
            
        }
        public int SafetyCategoryId { get; set; }
        public string SafetyCategoryName { get; set; }
        public string SafetyCategoryXml { get; set; }

        public bool IsActive { get; set; }
        public int SchoolId { get; set; }
        public bool IsAddMode { get; set; }
    }
}