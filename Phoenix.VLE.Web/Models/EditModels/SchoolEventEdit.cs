﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models
{
    [ResourceMappingRoot(Path = "Events.SchoolEvent")]
    public class SchoolEventEdit
    {
        public int EventId { get; set; }
        public long SchoolId { get; set; }
        public long UserId { get; set; }
        public DateTime StartDate { get; set; }
        public string StartTime { get; set; }
        public int DurationId { get; set; }
        public int EventCategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string EventTypeId { get; set; }
        public string ResourceFile { get; set; }
        public HttpPostedFileBase ResourceFileUploade { get; set; }
        public string EventPriority { get; set; }
        public bool IsTeacherVisible { get; set; }
        public int EventRepeatTypeId { get; set; }
        public string EventRepeatTimes { get; set; }
        public bool IsCopyMessage { get; set; }

        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public string Duration { get; set; }
        public string EventCategory { get; set; }
        public string EventCategoryColorCode { get; set; }
    }
}