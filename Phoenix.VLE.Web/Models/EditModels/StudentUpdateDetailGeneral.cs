﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Localization;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Models
{
    //new general model
    public class StudentUpdateDetailGeneral
    {
        public GetStudentDetail Geetstudentdetails { get; set; }
        public PassportVISADetails PassportVisaDetails { get; set; }
        public LanguageDetails Languagedetails { get; set; }
        public ContactInfoDetails ContactinfoDetails { get; set; }
        public ParentUpdateDetail ParentupdateDetail { get; set; }
        public ParentFeeSponsor ParentfeeSponsor { get; set; }
        public FatherDetails FatherDetails { get; set; }
        public MotherDetails Motherdetails { get; set; }
        public GuardianDetails GuardianDetails { get; set; }
        public HealthDetails Healthdetails { get; set; }
        public GeneralConsent Generalconsent { get; set; }
        public HistoryOfIllnessInfectious Infection { get; set; }
        public List<HistoryOfIllnessInfectious> HistoryOfIllnessInfectious { get; set; }
        public List<HistoryOfIllnessNonInfectious> HistoryOfIllnessNonInfectious { get; set; }
        public string Chekboxjson { get; set; }
        public string Infectious { get; set; }
        public string Noninfectious { get; set; }
        public FamilyHistory Familyhistory { get; set; }
        public string MultipleOtherLanguage { get; set; }
        public int STU_ACD_ID { get; set; }
        public string Stu_BSU_Id { get; set; }
        public string StuId { get; set; }
        public string STU_FullName { get; set; }
        public string STU_GENDER { get; set; }
        public string PrefferedContact { get; set; }
        public bool IsChangedGeneralConsent { get; set; }
        public bool IsChangedGeneral { get; set; }
        public bool IsChangedHealth { get; set; }
        public bool IsChangedPersonalDetails { get; set; }
    }
    public class FamilyHistory
    {
        public bool Diabetes { get; set; }
        public bool Hypertension { get; set; }
        public bool stroke { get; set; }
        public bool Tuberculosis { get; set; }
        public bool other { get; set; }
        public string otherdetail { get; set; }

    }
    public class GetStudentDetail
    {
        public string Name { get; set; }
        public string StudentImagePath { get; set; }
        public string StudentNumber { get; set; }
        public string ID { get; set; }
        public string Grade { get; set; }
        public string NameAsNationalId { get; set; }
        public string gender { get; set; }
        public string Religion { get; set; }
        public string DOB { get; set; }
        public string CountryOfBirth { get; set; }
        public string Joiningdate { get; set; }
        public string PalceOfBirth { get; set; }
        public string Nationality { get; set; }
        public string Nationality2 { get; set; }
        public string stU_EmiratesID { get; set; }
        public DateTime stU_EM_ID_EXP_DATE { get; set; }
        public string premisesID { get; set; }
        public string olU_ID { get; set; }
        public string stu_phone { get; set; }
        public string stu_email { get; set; }
        public string stu_prefcontact { get; set; }
    }
    public class PassportVISADetails
    {
        public string FullNameInPassport { get; set; }
        public string PassportNumber { get; set; }
        public string PassportIssuePlace { get; set; }
        public IEnumerable<SelectListItem> PassportIssuePlaceDropdownliast { get; set; }
        public string PassportIssueDate { get; set; }
        public string PassportExpiryDate { get; set; }
        public string VisaNumber { get; set; }
        public string VisaIssueDate { get; set; }
        public string VisaExpiryDate { get; set; }
        public string VisatIssuePlace { get; set; }
        public string IssuAuthority { get; set; }
    }
    public class LanguageDetails
    {
        public IEnumerable<String> Otherlang { get; set; }
        public string FirstLanguage { get; set; }
        public IEnumerable<SelectListItem> FristLanguageDropDownList { get; set; }
        public string OtherLanguage { get; set; }
        public IEnumerable<SelectListItem> OtherLanguageDropDownList { get; set; }

    }
    public class ContactInfoDetails
    {
        public string EmrgencyContact { get; set; }
        public string ECountry { get; set; }
        public string EArea { get; set; }
        public string Studentemail { get; set; }
        public string MobileNumber { get; set; }
        public string MCountry { get; set; }
        public string MArea { get; set; }

    }

    public class ParentUpdateDetail
    {
        public ParentFeeSponsor parentFeeSponsor { get; set; }
        public FatherDetails Fatherdetails { get; set; }
        public MotherDetails Motherdeatils { get; set; }
        public GuardianDetails Guardiandetails { get; set; }

    }
    public class ParentFeeSponsor
    {
        public string FeeSponsor { get; set; }
        public IEnumerable<SelectListItem> FeeSponsorDropDownList { get; set; }
        public string MediaNetwork { get; set; }
        public IEnumerable<SelectListItem> MediaNetworkDropDownList { get; set; }
        public string Feecompanytext { get; set; }
        public string PrimaryContact { get; set; }
        public string PreferredContact { get; set; }
        public IEnumerable<SelectListItem> PreferredContactDropDownList { get; set; }
    }
    public class FatherDetails
    {
        public string FatherName { get; set; }
        public string FirstName { get; set; }
        public string MiddalName { get; set; }
        public string LastName { get; set; }
        public string CountryOfResidencyId { get; set; }
        public string CountryOfResidency { get; set; }
        public IEnumerable<SelectListItem> CountryOfResidencyDropDownList { get; set; }
        public string ResidentialCity { get; set; }
        public IEnumerable<SelectListItem> ResidentialCityDropDownList { get; set; }
        public string Area { get; set; }
        public IEnumerable<SelectListItem> AreaDropDownList { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string ApartNo { get; set; }
        public string CityEmirate { get; set; }
        public IEnumerable<SelectListItem> CityEmirateDropDownList { get; set; }
        public string PoBox { get; set; }
        // [LocalizedPhoneAttribute]
        //[DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{5})[-. ]?([0-9]{5})$", ErrorMessage = "Not a valid Mobile number")]   
        //[RegularExpression(@"^([0|\+[0-9]{4})?([0-9]{5})([0-9]{5})$", ErrorMessage = "Not a valid Mobile number")]
        public string MobileNo { get; set; }
        public string EmailAddress { get; set; }
        public string Occupation { get; set; }
        public string Company { get; set; }
        public string CompanyId { get; set; }
        public string OtherComapnyText { get; set; }
        public IEnumerable<SelectListItem> CompanyDropDownList { get; set; }

    }
    public class MotherDetails
    {
        public string MotherName { get; set; }
        public string FirstName { get; set; }
        public string MiddalName { get; set; }
        public string LastName { get; set; }
        public string CountryOfResidency { get; set; }
        public string CountryOfResidencyId { get; set; }
        public IEnumerable<SelectListItem> CountryOfResidencyDropDownList { get; set; }
        public string ResidentialCity { get; set; }
        public IEnumerable<SelectListItem> ResidentialCityDropDownList { get; set; }
        public string Area { get; set; }
        public IEnumerable<SelectListItem> AreaDropDownList { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string ApartNo { get; set; }
        public string CityEmirate { get; set; }
        public IEnumerable<SelectListItem> CityEmirateDropDownList { get; set; }
        public string PoBox { get; set; }
        //[LocalizedPhoneAttribute]
        public string MobileNo { get; set; }
        public string EmailAddress { get; set; }
        public string Occupation { get; set; }
        public string Company { get; set; }
        public string CompanyId { get; set; }
        public string MOtherComapnyText { get; set; }
        public IEnumerable<SelectListItem> CompanyDropDownList { get; set; }

    }
    public class GuardianDetails
    {
        public string MotherName { get; set; }
        public string FirstName { get; set; }
        public string MiddalName { get; set; }
        public string LastName { get; set; }
        public string CountryOfResidency { get; set; }
        public string CountryOfResidencyId { get; set; }
        public IEnumerable<SelectListItem> CountryOfResidencyDropDownList { get; set; }
        public string ResidentialCity { get; set; }
        public IEnumerable<SelectListItem> ResidentialCityDropDownList { get; set; }
        public string Area { get; set; }
        public IEnumerable<SelectListItem> AreaDropDownList { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string ApartNo { get; set; }
        public string CityEmirate { get; set; }
        public IEnumerable<SelectListItem> CityEmirateDropDownList { get; set; }
        public string PoBox { get; set; }
        //[LocalizedPhoneAttribute]
        public string MobileNo { get; set; }
        public string EmailAddress { get; set; }
        public string Occupation { get; set; }
        public string Company { get; set; }
        public string CompanyId { get; set; }
        public string GOtherComapnyText { get; set; }
        public IEnumerable<SelectListItem> CompanyDropDownList { get; set; }
    }
    public class HealthDetails
    {
        public string InsuranceNumber { get; set; }
        public string BloodGroup { get; set; }
        public IEnumerable<SelectListItem> BloodGroupDownList { get; set; }
        public string healthissueTextArea { get; set; }
        public bool Myknowledge { get; set; }
        public bool Allergies { get; set; }
        public string Allergiestxt { get; set; }
        public bool SpecialMedication { get; set; }
        public string SpecialMedicationtxt { get; set; }
        public bool EducationRestrictions { get; set; }
        public string EducationRestrictionstxt { get; set; }
        public bool VisualDisability { get; set; }
        public string VisualDisabilitytxt { get; set; }
        public bool SpecialEducation { get; set; }
        public string SpecialEducationtxt { get; set; }
        public string healthissueTextArea1 { get; set; }
        public bool HealthIssue { get; set; }
        public string otherinfecioustxt { get; set; }
    }
    public class HistoryOfIllnessInfectious
    {
        public int diS_ID { get; set; }
        public bool diS_IS_INFECTIOUS { get; set; }
        public string diS_DESC { get; set; }
        public bool diS_ACTIVE { get; set; }
        public int isYChecked { get; set; }
        public int isNChecked { get; set; }
        public bool IsChecked { get; set; }

        //public string RheumaticFeverTetArea { get; set; }
        //public string FamilyHistory { get; set; }
    }

    public class HistoryOfIllnessNonInfectious
    {
        public int diS_ID { get; set; }
        public bool diS_IS_INFECTIOUS { get; set; }
        public string diS_DESC { get; set; }
        public bool diS_ACTIVE { get; set; }
        public int isYChecked { get; set; }
        public int isNChecked { get; set; }
        public bool IsChecked { get; set; }

    }
    public class GeneralConsent
    {
        public bool InternalMaterials { get; set; }
        public bool ExternalMaterial { get; set; }
        public bool InformationDirectory { get; set; }
        public bool Permission { get; set; }

    }
    public class StudentupdateMaster
    {
        public string descr { get; set; }
        public int id { get; set; }
    }

    public class StudentupdateMasterRoot
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public object responseCode { get; set; }
        public object message { get; set; }
        public List<StudentupdateMaster> data { get; set; }
    }
}




