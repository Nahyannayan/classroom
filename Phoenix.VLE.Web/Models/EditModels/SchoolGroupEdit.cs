﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "SchoolGroup.SchoolGroup")]
    public class SchoolGroupEdit
    {
        public int SchoolGroupId { get; set; }
        public string SchoolGroupName { get; set; }
        public string SchoolGroupNameXml { get; set; }
        public string SchoolGroupDescription { get; set; }
        public int ? SubjectId { get; set; }
        public bool IsActive { get; set; }
        public bool IsAddMode { get; set; }
        public long SchoolId { get; set; }
        public string TeacherGivenName{ get; set; }
        public string Description { get; set; }
        public string GroupImage { get; set; }
        public HttpPostedFileBase PostedImage { get; set; }

        public bool AutoSyncGroupMember { get; set; }
        public bool IsBespokeGroup { get; set; }

        public int ? DDLSchoolGroupId { get; set; }
        public string ClassGroupIdsToHide { get; set; }
    }
}
