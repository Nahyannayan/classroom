﻿using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Models.EditModels
{

    public class RequestEnroll
    {
        public int aldId { get; set; }
        public string academicYear { get; set; }
        public string eventName { get; set; }
        public string eventDate { get; set; }
        public string requestBy { get; set; }
        public string grades { get; set; }
        public string studentNo { get; set; }
        public string studentName { get; set; } 
        public bool isScheduleAvailable { get; set; }
        public List<StudentEnrollList> siblings { get; set; }
        public List<object> schedules { get; set; }
        public int maximumEligibleEventCount { get; set; }
        public bool allowComments { get; set; }
    }

    public class StudentEnrollList
    {
        public string studentNo { get; set; }
        public string studentName { get; set; }

    }
    public class RequestEnrollRoot
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public object responseCode { get; set; }
        public object message { get; set; }
        public List<RequestEnroll> data { get; set; }

    }
    public class JasonResultFileter
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public object responseCode { get; set; }
        public object message { get; set; }

    }

    public class EnrolllActivityRoot
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public object responseCode { get; set; }
        public object message { get; set; }
        public List<EnrolllActivityModel> data { get; set; }
    }

    public class EnrolllActivityModel
    {
        public int aldId { get; set; }
        public string groupName { get; set; }
        public int groupId { get; set; }
        public int groupMaxLimit { get; set; }
        public string activityIconPath { get; set; }
        public string regMode { get; set; }
        public string eventName { get; set; }
        public string isApprovalRequired { get; set; }
        public string eventDescription { get; set; }
        public string eventDate { get; set; }
        public int feeTypeId { get; set; }
        public string accountId { get; set; }
        public string eventId { get; set; }
        public string taxCode { get; set; }
        public int maximumCount { get; set; }
        public bool allowMultiple { get; set; }
        public int availableSeats { get; set; }
        public double eventCost { get; set; }
        public string applicableGrades { get; set; }
        public int bsuId { get; set; }
        public string enrolledInfo { get; set; }
        public string eventRequestStartDate { get; set; }
        public string eventRequestEndDate { get; set; }
        public string eventLocation { get; set; }
        public string studentId { get; set; }
        public string paymentStatus { get; set; }
        public string enrollmentStatus { get; set; }
        public string eventStartDate { get; set; }
        public string eventEndDate { get; set; }
        public string eventRequestedOn { get; set; }
        public string Activityrating { get; set; }
        public int feedbackRating { get; set; }
        public string feedbackComments { get; set; }
        public bool showUnsubscribe { get; set; }
        public double paymentProcessingCharge { get; set; }
        public int couponCount { get; set; }
        public int paymentTypeID { get; set; }
        public bool onlinePaymentAllowed { get; set; }
        public string EnrolledSiblingNames { get; set; }
        public string EnrolledSiblingStudentIDs { get; set; }
        public string apdId { get; set; }
        public string feeCollType { get; set; }
        public bool showPayOnline { get; set; }
        public List<StudentEnrollList> StuEnrollListt { get; set; }
        public bool isTermsExist{ get; set; }
        public string termAndConditions { get; set; }
        public string ReceiptURL { get; set; }
    }
    public class EnrollActivitiesDashaboard
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int groupMaxLimit { get; set; }
        public string StudentNumber { get; set; }

        public List<EnrolllActivityModel> Activity { get; set; }
    }

    public class AllActivities
    {
        public AllActivities()
        {
            Avaliable = new List<EnrollActivitiesDashaboard>();
            Upcoming = new List<EnrollActivitiesDashaboard>();
            Enrolled = new List<EnrolllActivityModel>();
            InProcess= new List<EnrolllActivityModel>();
        }
        public List<EnrollActivitiesDashaboard> Avaliable { get; set; }
        public List<EnrollActivitiesDashaboard> Upcoming { get; set; }
        public List<EnrolllActivityModel> Enrolled { get; set; }
        public List<EnrolllActivityModel> InProcess { get; set; }

    }

    public class ActiveFeedback
    {
        public int FeedbackId { get; set; }
        public int ActivityId { get; set; }
        public int RatingValue { get; set; }
        public string Comments { get; set; }
        public string ParentUserId { get; set; }

    }

}
