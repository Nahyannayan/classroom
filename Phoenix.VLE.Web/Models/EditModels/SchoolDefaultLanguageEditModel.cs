﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models.EditModels
{

    [ResourceMappingRoot(Path = "SchoolDefaultLanguage.Language")]
    public class SchoolDefaultLanguageEditModel
    {
        public SchoolDefaultLanguageEditModel()
        {

        }

        public SchoolDefaultLanguageEditModel(int id)
        {
            SchoolSettingId = id;
        }
        public Int64 SchoolSettingId { get; set; }
        public IEnumerable<LanguageModel> Languages { get; set; }
        public string SchoolID { get; set; }
        public string SchoolName { get; set; }
        public Int32 LanguageID { get; set; }
        public String LanguageName { get; set; }
        public bool IsAddMode { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }
        public Int16 TransMode { get; set; }
    }

    public class LanguageModel
    {
        public Int64 SystemLanguageID { get; set; }
        public string SystemLanguageName { get; set; }
        public string ShortCode { get; set; }
        public string CultureString { get; set; }
    }
}