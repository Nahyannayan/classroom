﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "ListCategories.SuggestionCategory")]
    public class SuggestionCategoryEdit
    {
        public SuggestionCategoryEdit()
        {

        }
        public int SuggestionCategoryId { get; set; }
        public string SuggestionCategoryName { get; set; }
        public string SuggestionCategoryXml { get; set; }
        public int SchoolId { get; set; }
        public bool IsActive { get; set; }
        public bool IsAddMode { get; set; }
    }
}