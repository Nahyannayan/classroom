﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "SecurityManagement.ThinkBox")]
    public class ThinkBoxEdit
    {
        public ThinkBoxEdit()
        {
            StaffList = new List<SelectListItem>();
            UsersList= new List<SelectListItem>();
            SelectedUsersList = new List<SelectListItem>();
        }
        public List<SelectListItem> StaffList { get; set; }
        public List<SelectListItem> UsersList { get; set; }

        public List<SelectListItem> SelectedUsersList { get; set; }
        public string SelectedUsersId { get; set; }
        public string DeselectedUsersId { get; set; }
        public int SchoolId { get; set; }
        public long UserId { get; set; }
        public long CreatedBy { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

    }
}