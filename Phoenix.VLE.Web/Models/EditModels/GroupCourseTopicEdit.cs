﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Models
{
    [ResourceMappingRoot(Path = "Files.GroupCourseTopics")]
    public class GroupCourseTopicEdit
    {
        public long GroupCourseTopicId { get; set; }
        public long GroupId { get; set; }
        public long GroupIdValue { get; set; }
        public long TopicId { get; set; }
        public string TopicName { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }
        public bool IsSubTopic { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime UpdatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public bool IsAddMode { get; set; }

        public List<SelectListItem> TopicList { get; set; }
        public GroupCourseTopicEdit()
        {
            TopicList = new List<SelectListItem>();
        }
    }
}