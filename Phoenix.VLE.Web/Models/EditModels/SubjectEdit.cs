﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.EditModels
{
    [ResourceMappingRoot(Path = "ListCategories.Subject")]
    public class SubjectEdit
    {
        public int SubjectId { get; set; }
        public int SubjectTypeId { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public string SubjectColorCode { get; set; }
        public string SubjectNameXml { get; set; }
        public bool IsActive { get; set; }
        public int SchoolId { get; set; }
        public bool IsAddMode { get; set; }
    }
}