﻿using Phoenix.Common.CustomAttributes;
using Phoenix.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Phoenix.VLE.Web.EditModels
{
   // public class Meeting
   // {  
   //   public int MeetingId { get; set; }
	  //public string MeetingTitle { get; set; }
	  //public string MeetingDetails { get; set; }
	  //public string MeetingDescription { get; set;  }
	  //public int MeetingCategoryId { get; set; }
   //   public DateTime StartDate { get; set; }
   //   public DateTime EndDate { get; set; }
	  //public TimeSpan MeetingSlotInTime { get; set; }
	  //public TimeSpan BreakTimeBetweenSlots { get; set; }
	  //public string Grades_YearGroups { get; set; }
	  //public bool IsAutoAllocatedTimeSlots { get; set; }
	  //public bool IsRecurringMeetin { get; set; }

   // }
	//public class MeetingSlots
	//{
	//	public int Id { get; set; }
	//	public int MeetingId { get; set; }
	//	public TimeSpan StartTime { get; set; }
	//	public TimeSpan EndTime { get; set; }
	//	public int ParentId { get; set; }
	//	public int TeacherId { get; set; }
	//	public bool IsTeacherAccepted { get; set; }
	//	public int DeclinedReasonId { get; set; }
	//	public bool IsParentEmailSent { get; set; }
	//	public string IsParentPresent { get; set; }
		
	//}
	//public class TeacherLunchBreakSlot
	//{
	//	public int Id { get; set; }
	//	public int TeacherId { get; set; }
	//	public int MeetingId { get; set; }
	//	public TimeSpan LunchStartTime { get; set; }
	//	public TimeSpan LunchEndTime { get; set; }
	//}
	//public class DeclineReasonList
	//{
	//	public int DeclineReasonId { get; set; }
	//	public string DeclineReasonName { get; set; }
	//	public string DeclineReasonDescription { get; set; }
	//	public bool IsApproved { get; set; }
	//	public bool IsActive { get; set; }
	//	public int CreatedBy { get; set; }
	//	public DateTime CreatedOn { get; set; }
	//	public int DeletedBy { get; set; }
	//	public DateTime DeletedOn { get; set; }
	//	public int ApprovedBy { get; set; }
	//	public DateTime ApprovedOn { get; set; }
	//}
	
 //   public class DaysMapping
	//{
	//	public int Id { get; set; }
	//	public int MeetingId { get; set; }
	//	public int Day { get; set; }

	//}
	public class CategoryListEdit
	{
		public CategoryListEdit(int _schoolGradeId)
		{
			SchoolGradeId = _schoolGradeId;
		}
		public int SchoolId { get; set; }
		public int CategoryId { get; set; }
		public int SchoolGradeId { get; set; }
		public bool IsAddMode { get; set; }
		public string CategoryName { get; set; }
		public string CategoryDescription { get; set; }
		public bool IsApproved { get; set; }
		public bool IsActive { get; set; }
		public string CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public int DeletedBy { get; set; }
		public DateTime DeletedOn { get; set; }
		public string ApprovedBy { get; set; }
		public DateTime ApprovedOn { get; set; }
		public List<ListItem> SchoolGradeList { get; set; }
		public CategoryListEdit()
		{
			SchoolGradeList = new List<ListItem>();
		}
		public string CategorySetXml { get; set; }
	}
	
}