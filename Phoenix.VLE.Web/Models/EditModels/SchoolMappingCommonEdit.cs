﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Phoenix.VLE.Web.EditModels
{
    public class SchoolMappingCommonEdit
    {
        public int RegistrationId { get; set; }
        public int CountryId { get; set; }
        public int CityId { get; set; }

        public long BusinessUnitId { get; set; }
        public List<ListItem> SelectedBUId { get; set; }
    }
}