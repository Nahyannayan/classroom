﻿using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models
{
    public class AzureBlobStorage
    {
        public class BlobFileInfo
        {
            public BlobFileInfo(Stream content, string contentType,string fileName)
            {
                Content = content;
                ContentType = contentType;
                FileName = fileName;
            }

            public Stream Content { get; }
            public string FileName { get;}
            public string ContentType { get; }
        }
    }
    public static class FileExtensions
    {
        private static readonly FileExtensionContentTypeProvider Provider = new FileExtensionContentTypeProvider();

        public static string GetContentType(this string fileName)
        {
            if (!Provider.TryGetContentType(fileName, out var contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }
    }

}