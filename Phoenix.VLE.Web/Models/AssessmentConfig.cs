﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models
{

    public class AssessmentConfig
    {
        public AssessmentConfig()
        {
            this.ConfigList = new List<GradeTemplate>();
            this.ConfigGradeSlabList = new List<GradeSlab>();
        }
        public List<GradeTemplate> ConfigList { get; set; }
        public List<GradeSlab> ConfigGradeSlabList { get; set; }
    }

    public class GradeTemplate
    {
        public int GradeTemplateDetailId { get; set; }
        public string GradeTemplateMasterdesc { get; set; }
        public int GradeTemplateMasterId { get; set; }
        public string TemPlateName { get; set; }
        public string ShortCode { get; set; }
        public string Description { get; set; }
        public string ColorCode { get; set; }
        public string Value { get; set; }
        public string GradeOrder { get; set; }
        // public string Order { get; set; }
        public int AcademicYearId { get; set; }


    }
    public class GradeSlab
    {
        public int GradeSlabId { get; set; }
        public string GradeSlabName { get; set; }
        public int GradeSlabMasterId { get; set; }
        public string GradeSlabMasterDesc { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
        public int FromMark { get; set; }
        public int ToMark { get; set; }
        public string Colour { get; set; }
        public int AcademicYearId { get; set; }
    }
    public class GradeSlabMaster
    {
        public int GradeSlabMasterId { get; set; }
        public string GradeSlabMasterDesc { get; set; }

    }
}