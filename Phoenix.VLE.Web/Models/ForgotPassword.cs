﻿using NLog.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models
{
    public class ForgotPassword
    {
    }
    public class ValidateUserNameResponse
    {
        public string userName { get; set; }
        public bool isValidUser { get; set; }
        public string errorCode { get; set; }
        public string message { get; set; }
    }
    public class ChangePasswordResponse
    {
        public string UserName { get; set; }
        public bool bPasswordChanged { get; set; }
        public string ErrorCode { get; set; }
        public string Message { get; set; }
        public string DeviceId { get; set; }
        public string Source { get; set; }
    }
    public class ForgotPasswordStudentDetail
    {
        public string StudentId { get; set; }
        public string DateOfBirth { get; set; }
    }
    public class PostForgetUsernameResponse
    {
        public string cmd { get; set; }
        public string success { get; set; }
        public string responseCode { get; set; }
        public string errorCode { get; set; }
        public string message { get; set; }
        public object data { get; set; }
        public int transactionId { get; set; }
    }
    public class ChangePasswordRequest
    {
        public string UserName { get; set; }
        public string NewPassword { get; set; }
        public string Source { get { return "app"; } }
        public string DeviceId { get { return "gemsapp"; } }
    }
}