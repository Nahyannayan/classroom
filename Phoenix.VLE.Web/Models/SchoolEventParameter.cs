﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models
{
    public class SchoolEventParameter
    {
        public string ParentID { get; set; }
        public string StudentID { get; set; }
        public string IsDateRange { get; set; }
        public string FromDate { get; set; }
        public string Source { get; set; }
        public string ToDate { get; set; }
        public string Language { get; set; }
        public string IsStaff { get; set; }
        public string StaffID { get; set; }
    }
}