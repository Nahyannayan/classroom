﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Models
{
    public class AssessmentConfigSetting
    {
        public AssessmentConfigSetting()
        {
            AssessmentColumnList = new List<AssessmentColumn>();
        }
        public long AssessmentMasterId { get; set; }
        public int AcademicYearId { get; set; }
        public string ASM_Description { get; set; }
        public string SchoolGradeIds { get; set; }
        public string SchoolGradeDisplay { get; set; }
        public long UserId { get; set; }
        public long SchoolId { get; set; }
        public int ASM_Order { get; set; }
        public bool ASM_ActiveStatus { get; set; }
        public string DATAMODE { get; set; }
        public int TotalCount { get; set; }
        public IEnumerable<AssessmentColumn> AssessmentColumnList { get; set; }

    }
    public class AssessmentColumn
    {
        public long ASC_Id { get; set; }
        public long CourseId { get; set; }
        public string CourseName { get; set; }
        public string ASC_Description { get; set; }
        public int ControlType { get; set; }
        public long GradeTemplateMasterId { get; set; }
        public decimal FromMark { get; set; }
        public decimal ToMark { get; set; }
        public int ASC_Order { get; set; }
        public bool ASC_ActiveStatus { get; set; }
        public string AssessComment { get; set; }
    }
}