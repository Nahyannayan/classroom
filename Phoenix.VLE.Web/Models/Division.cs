﻿using Phoenix.Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Models
{
    [ResourceMappingRoot(Path = "School.Division")]
    public class DivisionDetails
    {
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public string GradeList { get; set; }

        public string DisplayGradeList { get; set; }
        public long CREATED_BY { get; set; }
        public long BSU_ID { set; get; }
        public string DataMode { get; set; }
        public int CurriculumId { get; set; }
    }
}