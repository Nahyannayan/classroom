﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Certificategeneration.pixie
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }
        private SqlConnection con;
        // GET: Demo
        public ActionResult StudentIndex(int? StudentID)
        {
            DataTable dt = new DataTable();
            connection();

            con.Open();
            SqlCommand cmd = new SqlCommand("Select * from Certificate where StudentId=" + StudentID, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                var imagename = Convert.ToString(dt.Rows[0]["FileName"]);
                TempData["File"] = imagename;
            }
            else
            {
                TempData["File"] = "certificate.json";
            }
            TempData["Id"] = StudentID;
            string resultInHtml = "";

            //using (StreamReader rdr = new StreamReader(Server.MapPath("../pixie/index.html")))
            //{
            //    resultInHtml = rdr.ReadToEnd();
            //    //resultInHtml.Replace("###", "../pixie/styles.min.css");
            //}

            // return Content(resultInHtml, "text/html");
            return View();
            // return new FilePathResult("C:/Users/Neosoft/source/repos/Certificategeneration/Certificategeneration/pixie/index.html", "text/html");
        }
        public ActionResult LoadStudent()
        {
            //GetAllStudents();

            return View("LoadStudent", GetAllStudents());
        }
        public DataTable GetAllStudents()
        {
            DataTable dt = new DataTable();
            connection();

            con.Open();
            SqlCommand cmd = new SqlCommand("Select * from StudentInfo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public ActionResult Save(string name, string data)
        {
            var data1 = (JObject)JsonConvert.DeserializeObject(data);
            var str = data1["canvas"]["objects"][1];
            var name1 = str["text"].Value<string>();
            string path = Server.MapPath("~/pixie/");
            // Write that JSON to txt file,  
            System.IO.File.WriteAllText(path + name, data);
            //string newstr = "";
            //if (name.Contains(".json"))
            //     newstr = name.Replace(".json", "");
            //else
            //string timeZone = data1["text"].Value<string>();

            connection();
            SqlCommand com = new SqlCommand("SaveCertificate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@StudentName", name1);
            com.Parameters.AddWithValue("@FileName", name);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                var result = true;
            }
            else
            {
                var result = false;
            }
            return View("Index");
        }
        private void connection()
        {
            string constr = ConfigurationManager.AppSettings["CnnectionString"].ToString(); //ConfigurationManager.ConnectionStrings["CnnectionString"].ToString();
            con = new SqlConnection(constr);

        }
        public void GetHtml()
        {
            var encoding = new System.Text.UTF8Encoding();
            var htm = System.IO.File.ReadAllText(Server.MapPath("/Solution/Html/") + "index.html", encoding);
            byte[] data = encoding.GetBytes(htm);
            Response.OutputStream.Write(data, 0, data.Length);
            Response.OutputStream.Flush();
        }
        //To Add Employee details    
      
    }
}