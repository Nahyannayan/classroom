﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;

namespace Phoenix.VLE.Web.Helpers
{
    public static class SchoolDivisionHelper
    {
        #region private members
        private const string cookieName = "SchoolDivision";
        private const string cookieKey = "SchoolDivision";
        private static IEnumerable<DivisionDetails> divisionDetails;
        private static long schoolId;
        #endregion

        #region public members
        public static IEnumerable<DivisionDetails> DivisionDetails
        {
            get
            {
                if (divisionDetails == null || schoolId != SessionHelper.CurrentSession.SchoolId)
                {
                    schoolId = SessionHelper.CurrentSession.SchoolId;
                    IDivisionService divisionService = new DivisionService();
                    divisionDetails = divisionService.GetDivisionDetails(SessionHelper.CurrentSession.SchoolId,1);
                }
                return divisionDetails;
            }
        }
        public static int GetCurrentDivision
        {
            get
            {
                var cookieValue = CommonHelper.GetCookieValue(cookieName, cookieKey);
                if (string.IsNullOrEmpty(cookieValue))
                    return 0;
                else
                    return Convert.ToInt32(cookieValue);
            }
        }
        public static void SetCurrentDivision(string value)
        {
            CommonHelper.AppendCookie(cookieName, cookieKey, value, DateTime.Now.AddDays(7));
        }
        public static void SetFirstDivision()
        {
            var list = DivisionDetails.FirstOrDefault();
            SetCurrentDivision(list != null ? list.DivisionId.ToString() : "");
        }
        #endregion
    }
}