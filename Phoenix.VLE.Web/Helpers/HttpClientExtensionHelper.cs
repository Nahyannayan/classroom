﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Text;
using Phoenix.Common.Helpers;

namespace Phoenix.VLE.Web
{
    public static class HttpClientExtensionHelper
    {
        public static Task<HttpResponseMessage> GetAsyncExt(this HttpClient client, string requestUri,string token="")
        {
            token = string.IsNullOrEmpty(token) ? SessionHelper.CurrentSession.Token : token;
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, requestUri);
            httpRequestMessage.Headers.Authorization =new AuthenticationHeaderValue("Bearer", token);
            return client.SendAsync(httpRequestMessage);
        }
        public static Task<HttpResponseMessage> PostAsJsonAsyncExt<T>(this HttpClient client, string requestUri, T value, string token = "")
        {
            token = string.IsNullOrEmpty(token) ? SessionHelper.CurrentSession.Token : token;
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(value);
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, requestUri) { Content = new StringContent(serializedResult, Encoding.UTF8, "application/json") };
            httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            return client.SendAsync(httpRequestMessage);
        }
        public static Task<HttpResponseMessage> PutAsJsonAsyncExt<T>(this HttpClient client, string requestUri, T value, string token = "")
        {
            token = string.IsNullOrEmpty(token) ? SessionHelper.CurrentSession.Token : token;
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(value);
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Put, requestUri) { Content = new StringContent(serializedResult, Encoding.UTF8, "application/json") };
            httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            return client.SendAsync(httpRequestMessage);
        }
        public static Task<HttpResponseMessage> DeleteAsyncExt(this HttpClient client, string requestUri, string token = "")
        {
            token = string.IsNullOrEmpty(token) ? SessionHelper.CurrentSession.Token : token;
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Delete, requestUri);
            httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            return client.SendAsync(httpRequestMessage);
        }
    }
}