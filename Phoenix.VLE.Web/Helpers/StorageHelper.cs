﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure;
using Microsoft.Azure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;
using Phoenix.Common.Helpers;
using Phoenix.Models.Entities;
using Newtonsoft.Json;
using Phoenix.Common.Enums;
using System.Threading;
using Google.Apis.Auth.OAuth2.Mvc;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Helpers
{
    public class StorageHelper
    {
        public bool UploadBlob(Stream stream, string fileName, string containerName, string subContainerName, out string uploadedPath)
        {
            bool bFlag = false;
            uploadedPath = "";
            try
            {
                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["AzureStorageConnectionString"]);

                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve reference to a previously created container.
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                // Retrieve reference to a blob named "myblob".
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(subContainerName + fileName);

                // Create or overwrite the "myblob" blob with contents from a local file.
                //using (var fileStream = System.IO.File.OpenRead(path))
                //{
                stream.Position = 0;
                blockBlob.UploadFromStream(stream);
                //}
                bFlag = true;
                uploadedPath = blockBlob.Uri.ToString();
            }
            catch (Exception ex) { var errmsg = ex.Message; }

            return bFlag;
        }

        public Stream DownloadBlob(string fileName, string containerName, string subContainerName)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["AzureStorageConnectionString"]);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            // Retrieve reference to a blob named "photo1.jpg".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(subContainerName + fileName);

            return blockBlob.OpenRead();
        }

        public static List<Attachment> SaveAllFileAttachment(IList<HttpPostedFileBase> files, string key, string filePath, long attachedFileId)
        {
            List<Attachment> attachments = new List<Attachment>();
            string fileRelativePath = string.Empty;
            if (files.Any())
            {
                try
                {
                    foreach (var file in files)
                    {
                        if (file.ContentLength > 0 && file.FileName != string.Empty)
                        {
                            string fname = file.FileName;
                            fname = file.FileName;
                            var extension = System.IO.Path.GetExtension(file.FileName);

                            //content for file upload
                            string path = PhoenixConfiguration.Instance.WriteFilePath + filePath;

                            Common.Helpers.CommonHelper.CreateDestinationFolder(path);
                            var fileName = Guid.NewGuid() + "_" + fname;
                            fname = fileName;
                            fileRelativePath = filePath + "/" + fname;
                            fname = Path.Combine(path, fname);
                            file.SaveAs(fname);
                            attachments.Add(new Attachment { 
                                AttachmentKey = key, 
                                AttachmentType = AttachmentType.File, 
                                AttachmentPath = fileRelativePath, 
                                AttachedToId = attachedFileId,
                                FileExtension = extension,
                                FileName = fileName,
                                Mode = TranModes.Insert });
                        }
                    }                    
                }
                catch (Exception ex)
                {
                    return attachments;
                }

            }
            return attachments;
        }
        public static List<Attachment> SaveFileAttachment(HttpPostedFileBase httpPostedFileBase, string key, string filePath, long attachedFileId)
        {
            List<Attachment> attachments = new List<Attachment>();
            string fileRelativePath = string.Empty;
            if (httpPostedFileBase != null)
            {
                try
                {
                    HttpPostedFileBase file = httpPostedFileBase;
                    if (file.ContentLength > 0 && file.FileName != string.Empty)
                    {
                        string fname = file.FileName;
                        fname = file.FileName;
                        var extension = System.IO.Path.GetExtension(file.FileName);

                        //content for file upload
                        string path = PhoenixConfiguration.Instance.WriteFilePath + filePath;

                        Common.Helpers.CommonHelper.CreateDestinationFolder(path);
                        var fileName = Guid.NewGuid() + "_" + fname;
                        fname = fileName;
                        fileRelativePath = filePath + "/" + fname;
                        fname = Path.Combine(path, fname);
                        file.SaveAs(fname);
                        attachments.Add(new Attachment { 
                            AttachmentKey = key, 
                            AttachmentType = AttachmentType.File, 
                            AttachmentPath = fileRelativePath, 
                            AttachedToId = attachedFileId, 
                            FileExtension = extension,
                            FileName =fileName,
                            Mode = TranModes.Insert });
                    }
                }
                catch (Exception ex)
                {
                    return attachments;
                }

            }
            return attachments;
        }
        public static List<Attachment> AddGoogleDrivePermission(Controller controller, string attachmentJson)
        {
            var attachments = new List<Phoenix.Models.Entities.Attachment>();
            if (!string.IsNullOrEmpty(attachmentJson))
            {
                attachments = JsonConvert.DeserializeObject<List<Phoenix.Models.Entities.Attachment>>(attachmentJson);
                attachments.ForEach(x =>
                {
                    x.AttachmentType = Phoenix.Models.Entities.AttachmentType.Link;
                    x.Mode = Phoenix.Models.Entities.TranModes.Insert;
                });
            }
            return attachments;
        }
    }
}