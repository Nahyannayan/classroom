﻿using System;
using System.Linq;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Common.Extensions;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.Common.Models;
using Phoenix.Common.Enums;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.Models;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Phoenix.Common.Localization;
using System.Configuration;
using System.Text.RegularExpressions;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;

namespace Phoenix.VLE.Web.Helpers
{
    public static class CommonHelper
    {
        /// <summary>
        /// CreatedBy: Deepak Singh
        /// CreatedOn: 05/May/2019
        /// Get login user details from session
        /// </summary>
        /// <returns></returns>
        public static UserPrincipal GetLoginUser()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return (HttpContext.Current.User as UserPrincipal);
            }
            return null;
        }

        public static EmailSettingsView GetEmailSettings()
        {
            ICommonService _commonService = new CommonService();
            //string query = "SELECT TOP 1 AuthorizedName, SenderName, SenderEmail, SenderPassword, SMTPClient, PortNo, IsSSLEnabled FROM Admin.EmailSettings";
            //DataHelper _dataHelper = new DataHelper();
            //return _dataHelper.CopyDataReaderToObject<EmailSettingsView>(_dataHelper.GetSqlDataReader(query));
            return EntityMapper<Phoenix.Common.ViewModels.EmailSettingsView, EmailSettingsView>.Map(_commonService.GetEmailSettings());

        }

        public static void AppendCookie(string cookieName, string key, string value)
        {
            HttpCookie getCookie = HttpContext.Current.Request.Cookies.Get(cookieName);
            if (getCookie != null)
            {
                RemoveCookie(cookieName);
            }
            HttpCookie cookie = new HttpCookie(cookieName);
            //if (!cookieName.Equals("TimeZone", StringComparison.OrdinalIgnoreCase) || !cookieName.Equals("ShowNotification", StringComparison.OrdinalIgnoreCase)
            //        || cookieName.Equals("NotificationExist", StringComparison.OrdinalIgnoreCase))
            cookie.Secure = !HttpContext.Current.Request.IsLocal;
            cookie.Values.Add(key, value);
            //cookie.Expires = DateTime.Now.AddMinutes(Convert.ToDouble(ConfigurationManager.AppSettings["SessionTimeOut"])); // === Need to update as per web.config method
            //cookie.Expires = DateTime.Now.AddYears(1);
            HttpContext.Current.Response.AppendCookie(cookie);
        }
        public static void AppendCookie(string cookieName, string key, string value, DateTime? expiryDate = null)
        {
            HttpCookie getCookie = HttpContext.Current.Request.Cookies.Get(cookieName);
            if (getCookie != null)
            {
                RemoveCookie(cookieName);
            }
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie.Secure = !HttpContext.Current.Request.IsLocal;
            if (cookie.Values.AllKeys.Any(x => x.Contains(key)))
                cookie.Values.Set(key, EncryptDecryptHelper.Encrypt(value));
            else
                cookie.Values.Add(key, EncryptDecryptHelper.Encrypt(value));
            cookie.Expires = expiryDate.HasValue ? expiryDate.Value : DateTime.Now.AddDays(2); // === Need to update as per web.config method
            HttpContext.Current.Response.AppendCookie(cookie);
        }

        public static string GetCookieValue(string cookieName, string key)
        {
            HttpCookie getCookie = HttpContext.Current.Request.Cookies.Get(cookieName);
            return getCookie != null ? getCookie[key].ToString() : string.Empty;
        }

        public static void RemoveCookie(string cookieName)
        {
            HttpCookie rmCookie = new HttpCookie(cookieName, "");
            rmCookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(rmCookie);
        }

        public static string GetFileIcon(string filePath)
        {
            IFileService _fileService = new FileService();
            List<VLEFileType> _fileTypeList = _fileService.GetFileTypes().ToList();
            VLEFileType fileType = new VLEFileType();
            fileType = _fileTypeList.FirstOrDefault(r => r.Extension.ToLower() == (Path.GetExtension(filePath).ToLower().Replace(".", "")));
            if (fileType.Icon == "")
            {
                fileType = new VLEFileType();
                fileType.Icon = "fas fa-file-alt ic-file";
            }
            return fileType.Icon;
        }
        public static string GetSubjectNameById(int ContentId)
        {
            string subject = String.Empty;
            IContentLibraryService _contentLibraryService = new ContentLibraryService();
            var subjects = _contentLibraryService.getSubjectsByContentId(ContentId);
            foreach (var n in subjects)
            {
                subject = subject + n.SelectedSubjectName + " | ";
            }
            return String.IsNullOrEmpty(subject) ? "" : subject.Remove(subject.Length - 2);
        }

        public static bool CheckSolveQuizByStudent(int quizId, int resourceId)
        {
            if (SessionHelper.CurrentSession.IsStudent() || SessionHelper.CurrentSession.IsParent())
            {
                IGroupQuizService _groupQuizService = new GroupQuizService();
                int StudentId = SessionHelper.CurrentSession.IsStudent() ? Convert.ToInt32(SessionHelper.CurrentSession.Id) : Convert.ToInt32(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId);
                var quizresult = _groupQuizService.GetQuizResultByUserId(quizId, StudentId, resourceId);
                if (quizresult.QuizId != 0)
                { return true; }
                else { return false; }
            }
            else
            { return true; }
        }

        #region User Profile
        public static UserProfileView GetUserFeeling()
        {
            var userProfile = new UserProfileView();
            var _userService = new UserService();
            userProfile.LoginType = StringEnum.GetStringValue(MainModuleCodes.Classroom);
            var userFeelingsList = _userService.GetUserFeelings(LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
            var userDetails = _userService.GetUserById(SessionHelper.CurrentSession.Id);
            UserFeelingView userFeeling = new UserFeelingView();
            var UserFeel = userFeelingsList.Where(e => e.FeelId == userDetails.FeelId).FirstOrDefault();
            userProfile.UserFeeling.FeelId = userDetails.FeelId;
            userProfile.UserFeeling.FeelingType = UserFeel == null ? "" : UserFeel.FeelingType;
            userProfile.UserFeeling.Logo = UserFeel == null ? "" : UserFeel.Logo;
            userProfile.UserFeeling.UserId = userDetails.Id;
            userProfile.UserFeeling.UserTypeId = userDetails.UserTypeId;
            userProfile.UserFeeling.CurrentStatus = userDetails.CureentStatus == null ? "" : userDetails.CureentStatus;
            userProfile.AvatarLogo = String.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["UpdatedAvatarName"])) ? userDetails.UserAvatar : Convert.ToString(HttpContext.Current.Session["UpdatedAvatarName"]);
            userProfile.ProfilePhoto = String.IsNullOrEmpty(userDetails.ProfilePhoto) ? null : userDetails.ProfilePhoto;
            userProfile.IsProfilePicMigrated = userDetails.IsProfilePicMigrated;
            return userProfile;
        }
        public static List<QuizFile> CheckFileNameExtension(this List<QuizFile> QuizFiles)
        {
            if (QuizFiles.Any())
            {
                foreach (var item in QuizFiles)
                {
                    if (String.IsNullOrEmpty(item.FileName))
                    {
                        string fileExtension = Path.GetExtension(item.UploadedFileName);
                        item.FileName = "QuizFile" + fileExtension;
                    }
                }
            }
            return QuizFiles;
        }
        public static List<QuizQuestionFiles> CheckQuestionFileNameExtension(this List<QuizQuestionFiles> QuizFiles)
        {
            if (QuizFiles.Any())
            {
                foreach (var item in QuizFiles)
                {
                    if (String.IsNullOrEmpty(item.FileName))
                    {
                        string fileExtension = Path.GetExtension(item.UploadedFileName);
                        item.FileName = "QuizQuestionFile" + fileExtension;
                    }
                }
            }
            return QuizFiles;
        }
        #endregion

        #region SIMS
        public static Curriculum GetCurrentCurriculum()
        {
            if (HttpContext.Current.Session["SIMSCurrentCurriculum"] != null)
            {
                if (HttpContext.Current.Session["SIMSCurrentCurriculum"] is Curriculum)
                {
                    if ((HttpContext.Current.Session["SIMSCurrentCurriculum"] as Curriculum).SchoolId != SessionHelper.CurrentSession.SchoolId)
                    {
                        ISIMSCommonService service = new SIMSCommonService();
                        var curriculum = service.GetCurriculum(SessionHelper.CurrentSession.SchoolId).FirstOrDefault();
                        curriculum.SchoolId = SessionHelper.CurrentSession.SchoolId;
                        HttpContext.Current.Session["SIMSCurrentCurriculum"] = curriculum;
                    }
                }
            }
            else
            {
                ISIMSCommonService service = new SIMSCommonService();
                var curriculum = service.GetCurriculum(SessionHelper.CurrentSession.SchoolId).FirstOrDefault();
                curriculum.SchoolId = SessionHelper.CurrentSession.SchoolId;
                HttpContext.Current.Session["SIMSCurrentCurriculum"] = curriculum;
            }
            return HttpContext.Current.Session["SIMSCurrentCurriculum"] as Curriculum;
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
        #endregion

        public static string Hash(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("X2"));
                }

                return sb.ToString();
            }
        }

        #region File
        public static string GetHumanReadableFileSize(double size)
        {
            string[] sizes = { "B", "KB", "MB", "GB", "TB" };
            int order = 0;

            while (size > 1024 && order < sizes.Length)
            {
                order++;
                size /= 1024;
            }
            return string.Format("{0:0.##} {1}", size, sizes[order]);
        }
        #endregion

        public static int GetDecryptedId(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                string decryptedId = string.Empty;
                var isInteger = int.TryParse(id, out _);
                if (isInteger)
                {
                    return Convert.ToInt32(id);
                }
                else if (id.IsBase64String())
                {
                    decryptedId = EncryptDecryptHelper.DecodeBase64(id);
                }

                return !string.IsNullOrWhiteSpace(decryptedId) && int.TryParse(decryptedId, out _) ? Convert.ToInt32(decryptedId) : Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
            }
            return 0;
        }

        public static bool IsBase64String(this string s)
        {
            s = s.Trim();
            return (s.Length % 4 == 0) && Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);

        }

        public static long GetDecryptedIdFromEncryptedString(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                string decryptedId = string.Empty;
                var isInteger = Int64.TryParse(id, out _);
                if (isInteger)
                {
                    return -1;
                }
                else if (id.IsBase64String())
                {
                    decryptedId = EncryptDecryptHelper.DecodeBase64(id);
                }

                return !string.IsNullOrWhiteSpace(decryptedId) && Int64.TryParse(decryptedId, out _) ? Convert.ToInt64(decryptedId) : Convert.ToInt64(EncryptDecryptHelper.DecryptUrl(id));
            }
            return 0;
        }

        public static string GetFileExtensionByMimeType(string mimeType)
        {
            switch (mimeType.ToLower())
            {
                case "text/csv": return "csv";
                case "application/pdf": return "pdf";
                case "image/bmp": return "bmp";
                case "application/msword": return "doc";
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document": return "docx";
                case "image/gif": return "gif";
                case "text/html": return "html";
                case "image/jpeg": return "jpeg";
                case "application/json": return "json";
                case "audio/mpeg": return "mp3";
                case "video/mpeg": return "mpeg";
                case "application/vnd.oasis.opendocument.presentation": return "odp";
                case "application/vnd.oasis.opendocument.spreadsheet": return "ods";
                case "application/vnd.oasis.opendocument.text": return "odt";
                case "audio/ogg": return "oga";
                case "video/ogg": return "ogv";
                case "image/png": return "png";
                case "application/vnd.ms-powerpoint": return "ppt";
                case "application/vnd.openxmlformats-officedocument.presentationml.presentation": return "pptx";
                case "application/vnd.rar": return "rar";
                case "image/svg+xml": return "svg";
                case "application/vnd.ms-excel": return "xls";
                case "application/vnd.google-apps.spreadsheet": return "xls";
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": return "xlsx";
                case " application/zip": return "zip";
                case "application/x-tar": return "tar";
                case "application/octet-stream": return "html";
                case "application/x-javascript": return "js";
                case "onenote": return "one";
                case "application/vnd.google-apps.document": return "docs";
                default: return string.Empty;
            }
        }
        public static string GetBaseUrlWithoutScheme()
        {
            return HttpContext.Current.Request.Url.Authority +
            HttpContext.Current.Request.ApplicationPath.TrimEnd('/');
        }
        public static string GetPhoenixBaseURI()
        {
            if (typeof(SessionHelper).GetType().GetProperty("CurrentSession") != null)
                return SessionHelper.CurrentSession.IsGEMSBU ? ConfigurationManager.AppSettings["PhoenixBaseUriGEMS"] : ConfigurationManager.AppSettings["PhoenixBaseUriNonGEMS"];
            else return ConfigurationManager.AppSettings["PhoenixBaseUriGEMS"];
        }
        public static string ToRelativeTime(DateTime dt)
        {
            string result = string.Empty;
            var timeSpan = DateTime.UtcNow.Subtract(dt);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = timeSpan.Seconds > 0 ? string.Format(ResourceManager.GetString("Shared.Labels.SecondsAgo"), timeSpan.Seconds)
                    : ResourceManager.GetString("Shared.Labels.OneSecondAgo");
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format(ResourceManager.GetString("Shared.Labels.MinutesAgo"), timeSpan.Minutes) :
                    ResourceManager.GetString("Shared.Labels.AMinuteAgo");
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format(ResourceManager.GetString("Shared.Labels.HoursAgo"), timeSpan.Hours) :
                    ResourceManager.GetString("Shared.Labels.AnHourAgo");
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ?
                    String.Format(ResourceManager.GetString("Shared.Labels.DaysAgo"), timeSpan.Days) :
                    ResourceManager.GetString("Shared.Labels.Yesterday");
            }
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format(ResourceManager.GetString("Shared.Labels.MonthsAgo"), timeSpan.Days / 30) :
                    ResourceManager.GetString("Shared.Labels.OneMonthAgo");
            }
            else
            {
                result = timeSpan.Days > 365 ?
                    String.Format(ResourceManager.GetString("Shared.Labels.YearsAgo"), timeSpan.Days / 365) :
                    ResourceManager.GetString("Shared.Labels.OneYearAgo");
            }

            return result;

        }

        public static DateTime? ConvertLocalToUTC(DateTime? dt, int timezoneOffset)
        {
            if (dt == null)
                return null;

            DateTime newDate = dt.Value + new TimeSpan(timezoneOffset / 60, timezoneOffset % 60, 0);
            return newDate;
        }

        public static DateTime? ConvertUtcToLocalTime(DateTime? dt, int timezoneOffset)
        {
            if (dt == null)
                return null;

            DateTime newDate = dt.Value - new TimeSpan(timezoneOffset / 60, timezoneOffset % 60, 0);
            return newDate;
        }


        #region Convert Excel To FileStream
        public static ISheet GetFileStream(this string fullFilePath)
        {
            var fileExtension = Path.GetExtension(fullFilePath);
            string sheetName;
            ISheet sheet = null;
            switch (fileExtension)
            {
                case ".xlsx":
                    using (var fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read))
                    {
                        var wb = new XSSFWorkbook(fs);
                        sheetName = wb.GetSheetAt(0).SheetName;
                        sheet = (XSSFSheet)wb.GetSheet(sheetName);
                    }
                    break;
                case ".xls":
                    using (var fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read))
                    {
                        var wb = new HSSFWorkbook(fs);
                        sheetName = wb.GetSheetAt(0).SheetName;
                        sheet = (HSSFSheet)wb.GetSheet(sheetName);
                    }
                    break;
            }
            return sheet;
        }
        #endregion

        #region Get Tree List
        public delegate void ParentSetter<T>(T child, T parent);
        public delegate void ChildSetter<T>(T child, T parent);

        public static IEnumerable<T> AsHierarchy<T, TID>(
          this IEnumerable<T> elements,
          Func<T, TID> idSelector,
          Func<T, TID> parentIdSelector,
          ParentSetter<T> parentSetter,
          ChildSetter<T> childAdder,
          TID rootId)
        {
            Dictionary<TID, T> lookUp = elements.ToDictionary(e => idSelector(e));

            foreach (T element in lookUp.Values)
            {
                TID parentId = parentIdSelector(element);
                if (!lookUp.TryGetValue(parentId, out T parent))
                {
                    if (parentId.ToString().Equals(rootId.ToString()))
                    {
                        yield return element;
                        continue;
                    }
                    else
                        throw new InvalidOperationException($"Parent not found for: {element}");
                }

                parentSetter(element, parent);
                childAdder(element, parent);
            }
        }
        #endregion

        public static string GetEmbeddedVideoLink(string inputYouTubeLink)
        {
            if (!string.IsNullOrEmpty(inputYouTubeLink))
            {
                Regex youTubeURLRegex = new Regex(@"^(https?\:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/.+$");
                if (youTubeURLRegex.IsMatch(inputYouTubeLink))
                {
                    const string pattern = @"(?:https?:\/\/)?(?:www\.)?(?:(?:(?:youtube.com\/watch\?[^?]*v=|youtu.be\/)([\w\-]+))(?:[^\s?]+)?)";
                    const string replacement = "<iframe title='YouTube video player' width='480' height='390' src='https://www.youtube.com/embed/$1' frameborder='0' allowfullscreen='1'></iframe>";

                    var rgx = new Regex(pattern);
                    inputYouTubeLink= rgx.Replace(inputYouTubeLink, replacement);
                }
            }
            return inputYouTubeLink;
        }


    }
}