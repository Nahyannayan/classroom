﻿using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Helpers
{
    public static class EmailHelper
    {
        public static OperationDetails SendEmail(this Controller controller, string toEmailAddress, string subject)
        {
            return SendEmail(controller, toEmailAddress, subject, EmailTemplates.Default);
        }

        public static OperationDetails SendEmail<T>(this Controller controller, T model, string partialViewName, string toEmailAddress, string subject)
        {
            var body = GetEmailTemplate<T>(controller, model, partialViewName);
            return SendEmail(toEmailAddress, subject, body);
        }

        public static OperationDetails SendEmail(this Controller controller, string toEmailAddress, string subject, EmailTemplates template)
        {
            var body = GetEmailTemplate(controller, template);
            return SendEmail(toEmailAddress, subject, body);
        }

        public static OperationDetails SendEmail(string toEmailAddress, string subject, string body)
        {
            var operationalDetails = new OperationDetails();
            SendEmail emailSender = new SendEmail(true);
            emailSender.Subject = subject;
            emailSender.Message = body;
            try
            {
                emailSender.To = toEmailAddress;
                emailSender.Send();
                operationalDetails.Success = true;
                operationalDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Success);
                operationalDetails.Message = LocalizationHelper.UpdateSuccessMessage;
            }
            catch (Exception ex)
            {
                operationalDetails.Success = true;
                operationalDetails.NotificationType = StringEnum.GetStringValue(NotificationTypeConstants.Warning);
                operationalDetails.Message = LocalizationHelper.TechnicalErrorMessage;
            }
            return operationalDetails;

        }

        public static void SendAsyncEmail(this Controller controller, string toEmailAddress, string subject, EmailTemplates template)
        {
            try
            {
                var body = GetEmailTemplate(controller, template);
                Task.Run(() => Email(subject, body, toEmailAddress));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }


        public static void SendAsyncEmail<T>(this Controller controller, T model, string partialViewName, string toEmailAddress, string subject)
        {
            try
            {
                var body = GetEmailTemplate<T>(controller, model, partialViewName);
                Task.Run(() => Email(subject, body, toEmailAddress));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private static void Email(string Subject, string Message, string to)
        {
            SendEmail emailSender = new SendEmail(true);
            emailSender.Subject = Subject;
            emailSender.Message = Message;
            emailSender.To = to;
            emailSender.Send();
            // Loop in here
        }

        public static string GetEmailTemplate(this Controller controller, EmailTemplates template)
        {
            return ControllerExtension.RenderPartialViewToString(controller, StringEnum.GetStringValue(template));
        }

        public static string GetEmailTemplate<T>(this Controller controller, T model, string partialViewName)
        {
            return ControllerExtension.RenderPartialViewToString(controller, partialViewName, model);
        }
    }
}