﻿using DevExpress.DataProcessing;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static Google.Apis.Auth.OAuth2.Web.AuthorizationCodeWebApp;

namespace Phoenix.VLE.Web.Helpers
{
    public static class CloudFilesHelper
    {
        public static string _oneDriveApiEndPoint;
        public static string _oneDriveTokenEndpoint;
        public static string _oneDriveClientId;
        public static string _oneDriveClientSecret;
        public static string _oneDriveRedirectUri;
        public static string _oneDriveScopes;
        public static readonly JsonSerializerSettings jsonSettings =
       new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };

        static CloudFilesHelper()
        {
            _oneDriveApiEndPoint = ConfigurationManager.AppSettings["OneDriveApiEndPoint"].ToString();
            _oneDriveTokenEndpoint = ConfigurationManager.AppSettings["OneDriveAuthorizationTokenEndpoint"].ToString();
            _oneDriveClientId = Convert.ToString(ConfigurationManager.AppSettings["ida:ClientId"]);
            _oneDriveClientSecret = Convert.ToString(ConfigurationManager.AppSettings["ida:ClientSecret"]);
            _oneDriveRedirectUri = Convert.ToString(ConfigurationManager.AppSettings["ida:RedirectUri"]);
            _oneDriveScopes = Convert.ToString(ConfigurationManager.AppSettings["ida:GraphScopes"]);
        }

        #region Google Drive
        public async static Task<IList<CloudFileListView>> GetDriveFiles(AuthResult result)
        {
            FilesResource.ListRequest FileListRequest = GetDriveFileRequest(result);
            FileListRequest.Q = "'root' in parents";
            FileListRequest.PageSize = 1000;
            IList<CloudFileListView> fileList = new List<CloudFileListView>();
            var list = await FileListRequest.ExecuteAsync();
            fileList = list.Files.Select(file => new CloudFileListView
            {
                Id = file.Id,
                Name = file.Name,
                Size = file.Size,
                Version = file.Version,
                CreatedTime = file.CreatedTime,
                Parents = file.Parents,
                MimeType = file.MimeType,
                IsFolder = file.MimeType.Equals("application/vnd.google-apps.folder"),
                WebContentLink = file.WebContentLink,
                WebViewLink = file.WebViewLink,
                ThumbnailLink = file.ThumbnailLink,
                FileExtension = file.FileExtension,
                IconLink = file.IconLink,
                ResourceFileTypeId = (short)ResourceFileTypes.GoogleDrive
            }).ToList();
            return fileList;
        }

        private static Permission InsertPermission(DriveService service, string fileId, string email,
       string type, string role)
        {
            Permission newPermission = new Permission();

            if (!string.IsNullOrEmpty(email))
                newPermission.EmailAddress = email;
            newPermission.Type = type;
            newPermission.Role = role;
            try
            {
                var permission = service.Permissions.Create(newPermission, fileId);
                permission.SendNotificationEmail = false;
                return permission.Execute();
            }
            catch (Exception e) { }
            return null;
        }

        public static void AddGoogleDriveUserPermission(AuthResult result, string emailIds, string fileId, string filePermission)
        {
            if (string.IsNullOrEmpty(emailIds))
                emailIds = "";
            string[] gmailAccounts = emailIds.Split(',');
            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = result.Credential,
                ApplicationName = "Gems Education"
            });

            foreach (var mailId in gmailAccounts)
                InsertPermission(service, fileId, mailId, "user", filePermission);
        }

        public static void AddGoogleDriveTeacherPermission(AuthResult result, string teacherGmailId, string fileId, string filePermission)
        {
            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = result.Credential,
                ApplicationName = "Gems Education"
            });

            InsertPermission(service, fileId, teacherGmailId, "user", filePermission);
        }

        public static void AddGoogleDriveUserPermission(AuthResult result, string fileId, string filePermission)
        {
            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = result.Credential,
                ApplicationName = "Gems Education"
            });
            InsertPermission(service, fileId, null, "anyone", filePermission);
        }

        public static Permission ShareGoogleDriveFileWithDomain(AuthResult result, string fileId, string filePermission, string domain)
        {
            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = result.Credential,
                ApplicationName = "Gems Education"
            });
            Permission newPermission = new Permission();
            newPermission.Type = "domain";
            newPermission.Role = filePermission;
            newPermission.Domain = domain;
            try
            {
                var permission = service.Permissions.Create(newPermission, fileId);
                permission.SendNotificationEmail = false;
                return permission.Execute();
            }
            catch (Exception e) { }
            return null;
        }

        public static async Task<Google.Apis.Drive.v3.Data.File> CopyFile(AuthResult result, string originFileId, string copyTitle, string[] parentFolder)
        {

            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = result.Credential,
                ApplicationName = "Gems Education"
            });

            var parentFolderId = string.Empty;
            foreach (var folderName in parentFolder)
            {
                var folderDetails = await CheckAndCreateGoogleDriveFolder(result, folderName, parentFolderId);
                parentFolderId = folderDetails.Id;
            }
            Google.Apis.Drive.v3.Data.File copiedFile = new Google.Apis.Drive.v3.Data.File();
            copiedFile.Name = copyTitle;
            copiedFile.Parents = new List<string>
            {
                parentFolderId
            };
            try
            {
                return service.Files.Copy(copiedFile, originFileId).Execute();
            }
            catch (IOException e) { }
            return null;
        }

        public static async Task<Google.Apis.Drive.v3.Data.File> CheckAndCreateGoogleDriveFolder(AuthResult authResult, string folderName, string parentFolderId = "")
        {
            FilesResource.ListRequest FileListRequest = GetDriveFileRequest(authResult);
            FileListRequest.Q = $"mimeType = 'application/vnd.google-apps.folder' and trashed = false and name = '{folderName}'" + (!string.IsNullOrEmpty(parentFolderId) ? $" and '{parentFolderId}' in parents" : "");
            var list = await FileListRequest.ExecuteAsync();

            if (list.Files.Count == 0)
            {
                Google.Apis.Drive.v3.Data.File body = new Google.Apis.Drive.v3.Data.File();
                body.Name = folderName;
                body.MimeType = "application/vnd.google-apps.folder";
                if (!string.IsNullOrEmpty(parentFolderId))
                {
                    body.Parents = new List<string>
                {
                    parentFolderId
                };
                }

                var service = new DriveService(new BaseClientService.Initializer
                {
                    HttpClientInitializer = authResult.Credential,
                    ApplicationName = "Gems Education"
                });
                return service.Files.Create(body).Execute();
            }

            return list.Files.FirstOrDefault();
        }


        public static FilesResource.ListRequest GetDriveFileRequest(AuthResult result)
        {
            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = result.Credential,
                ApplicationName = "Gems Education"
            });
            FilesResource.ListRequest FileListRequest = service.Files.List();
            FileListRequest.Fields = "nextPageToken, files(*)";
            FileListRequest.PageSize = 1000;
            return FileListRequest;
        }
        public async static Task<IList<CloudFileListView>> GetFolderFiles(AuthResult result, string folderId)
        {
            FilesResource.ListRequest FileListRequest = GetDriveFileRequest(result);
            FileListRequest.Q = $"'{folderId}' in parents";
            FileListRequest.PageSize = 1000;
            IList<CloudFileListView> fileList = new List<CloudFileListView>();
            var list = await FileListRequest.ExecuteAsync();
            fileList = list.Files.Select(file => new CloudFileListView
            {
                Id = file.Id,
                Name = file.Name,
                Size = file.Size,
                Version = file.Version,
                CreatedTime = file.CreatedTime,
                Parents = file.Parents,
                MimeType = file.MimeType,
                IsFolder = file.MimeType.Equals("application/vnd.google-apps.folder"),
                WebContentLink = file.WebContentLink,
                WebViewLink = file.WebViewLink,
                ThumbnailLink = file.ThumbnailLink,
                FileExtension = file.FileExtension,
                IconLink = file.IconLink,
                ResourceFileTypeId = (short)ResourceFileTypes.GoogleDrive
            }).ToList();
            return fileList;
        }
        #endregion

        #region One Drive
        public static async Task<MicrosoftResponseTokenView> GetOneDriveAccessToken(string code)
        {
            var content = new FormUrlEncodedContent(new[]
               {
                   new KeyValuePair<string, string>("redirect_uri", _oneDriveRedirectUri),
                   new KeyValuePair<string, string>("client_id", _oneDriveClientId),
                    new KeyValuePair<string, string>("client_secret", _oneDriveClientSecret),
                    new KeyValuePair<string, string>("code", code),
                    new KeyValuePair<string, string>("grant_type", "authorization_code"),
                     new KeyValuePair<string, string>("resource", "https://graph.microsoft.com/"),
                });
            try
            {
                var responseString = await GetOneDriveApiResponse(_oneDriveTokenEndpoint, HttpMethod.Post, content, false);
                var tokenView = JsonConvert.DeserializeObject<MicrosoftResponseTokenView>(responseString);
                tokenView.TokenResourceType = "graph";
                SaveOneDriveAccessToken(tokenView);
                return tokenView;
            }
            catch
            {
                return new MicrosoftResponseTokenView(); ;
            }


        }

        public static void SaveOneDriveAccessToken(MicrosoftResponseTokenView tokenView)
        {
            tokenView.AccessDateTime = DateTime.Now;
            CommonHelper.AppendCookie("odat", "odatk", tokenView.AccessToken);
            CommonHelper.AppendCookie("odty", "odtokentype", EncryptDecryptHelper.Encrypt(tokenView.TokenType));
            CommonHelper.AppendCookie("odrt", "odrft", tokenView.RefreshToken);
            CommonHelper.AppendCookie("odei", "odtexipry", tokenView.ExpiresIn.ToString());
            CommonHelper.AppendCookie("odtdt", "oddt", tokenView.AccessDateTime.ToString());
            CommonHelper.AppendCookie("odtrt", "odtrtype", tokenView.TokenResourceType);
            HttpContext.Current.Session["microsofttoken"] = tokenView;
        }

        public async static Task<IList<CloudFileListView>> GetOneDriveRootFiles()
        {
            var credentials = await RefreshOneDriveAccesToken(apiResource: "https://graph.microsoft.com/");
            IList<CloudFileListView> cloudFiles = new List<CloudFileListView>();
            try
            {
                var responseString = await GetOneDriveApiResponse($"{_oneDriveApiEndPoint}/root/children", HttpMethod.Get, null, true, $"{credentials.TokenType} {credentials.AccessToken}");
                var fileFolders = JsonConvert.DeserializeObject<OneDriveFilesView>(responseString);
                cloudFiles = fileFolders.Value.Select(x => new CloudFileListView
                {
                    Name = x.Name,
                    IsFolder = x.Folder != null,
                    MimeType = x.PackageType != null ? x.PackageType.Type : (x.Folder == null ? x.File.MimeType : string.Empty),
                    WebViewLink = x.WebUrl.AbsoluteUri,
                    Id = x.PackageType != null ? "1-" + x.ETag.Split('{', '}')[1] : x.Id,
                    FolderFilesCount = x.PackageType != null ? 0 : (x.Folder != null ? x.Folder.ChildCount : 0),
                    DownloadUrl = x.PackageType != null ? x.WebUrl.AbsoluteUri : (x.Folder != null ? string.Empty : x.MicrosoftGraphDownloadUrl.AbsoluteUri),
                    FileExtension = x.PackageType != null ? x.PackageType.Type : (x.Folder != null ? string.Empty : Path.GetExtension(x.Name).Substring(1)),
                    DriveId = x.ParentInfo?.DriveId,
                    ParentFolderId = x.ParentInfo?.Id,
                    ResourceFileTypeId = x.PackageType != null ? (short)ResourceFileTypes.OneNote : (short)ResourceFileTypes.OneDrive
                }).ToList();
                return cloudFiles;
            }
            catch (Exception ex)
            {
                return cloudFiles;
            }
        }

        public static async Task<IList<CloudFileListView>> GetOneDriveFolderFiles(string folderId)
        {
            IList<CloudFileListView> cloudFiles = new List<CloudFileListView>();
            try
            {
                var responseString = await GetOneDriveApiResponse($"{_oneDriveApiEndPoint}/items/{folderId}/children", HttpMethod.Get, null, true);
                var fileFolders = JsonConvert.DeserializeObject<OneDriveFilesView>(responseString);
                cloudFiles = fileFolders.Value.Select(x => new CloudFileListView
                {
                    Name = x.Name,
                    IsFolder = x.Folder != null,
                    MimeType = x.PackageType != null ? x.PackageType.Type : (x.Folder == null ? x.File.MimeType : string.Empty),
                    WebViewLink = x.WebUrl.AbsoluteUri,
                    FileExtension = x.PackageType != null ? x.PackageType.Type : (x.Folder != null ? string.Empty : (Path.GetExtension(x.Name).Length != 0 ? Path.GetExtension(x.Name).Substring(1) : x.Name)),
                    Id = x.PackageType != null ? "1-" + x.CTag.Split('{', '}')[1] : x.Id,
                    DownloadUrl = x.PackageType != null ? x.WebUrl.AbsoluteUri : (x.Folder != null ? string.Empty : x.MicrosoftGraphDownloadUrl.AbsoluteUri),
                    DriveId = x.ParentInfo?.DriveId,
                    ParentFolderId = x.ParentInfo?.Id,
                    ResourceFileTypeId = x.PackageType != null ? (short)ResourceFileTypes.OneNote : (short)ResourceFileTypes.OneDrive
                }).ToList();
                return cloudFiles;
            }
            catch (Exception ex)
            {
                return cloudFiles;
            }
        }

        public static async Task<string> CopyOneDriveFile(string fileId, string fileName, string driveId, string parentFolderId)
        {
            var palyload = new
            {
                parentReference = new
                {
                    driveId = driveId,
                    id = parentFolderId
                },
                name = fileName
            };

            return await CallApiMethods(HttpMethod.Post, $"{_oneDriveApiEndPoint}/items/{fileId}/copy", JsonConvert.SerializeObject(palyload), SessionHelper.CurrentSession.MicrosoftToken.AccessToken);
        }



        public static async Task<OneDriveFilesView> CreateOneDriveFolder(string folderName, string parentFolderId)
        {
            var rootFolderName = "assignments";
            var payloads = new
            {
                name = "{0}",
                folder = new { }
            };
            var serializedPayload = JsonConvert.SerializeObject(payloads);
            string response = await CallApiMethods(HttpMethod.Post, $"{_oneDriveApiEndPoint}//items/{parentFolderId}/children", serializedPayload.Replace("{0}", rootFolderName), SessionHelper.CurrentSession.MicrosoftToken.AccessToken);
            if (string.IsNullOrEmpty(response))
                response = await CallApiMethods(HttpMethod.Get, $"{_oneDriveApiEndPoint}/root:/{rootFolderName}", null, SessionHelper.CurrentSession.MicrosoftToken.AccessToken);
            OneDriveFilesView folderDetails = JsonConvert.DeserializeObject<OneDriveFilesView>(response);
            string assignmentFolderDetails = await CallApiMethods(HttpMethod.Post, $"{_oneDriveApiEndPoint}//items/{folderDetails.Id}/children", serializedPayload.Replace("{0}", folderName), SessionHelper.CurrentSession.MicrosoftToken.AccessToken);
            return JsonConvert.DeserializeObject<OneDriveFilesView>(assignmentFolderDetails);
        }


        public static string GetOneDriveAuthUrl()
        {

            return $"https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id={_oneDriveClientId}&scope={_oneDriveScopes}&response_type=code&redirect_uri={_oneDriveRedirectUri}&prompt=select_account";
        }

        public static async Task<MicrosoftResponseTokenView> RefreshOneDriveAccesToken(string apiResource)
        {
            var content = new FormUrlEncodedContent(new[]
             {
                      new KeyValuePair<string, string>("client_id", _oneDriveClientId),
                    new KeyValuePair<string, string>("client_secret", _oneDriveClientSecret),
                    new KeyValuePair<string, string>("redirect_uri", _oneDriveRedirectUri),
                    new KeyValuePair<string, string>("refresh_token", SessionHelper.CurrentSession.MicrosoftToken.RefreshToken),
                    new KeyValuePair<string, string>("resource", apiResource),
                     new KeyValuePair<string, string>("grant_type", "refresh_token")
                });
            var responseString = await GetOneDriveApiResponse($"{_oneDriveTokenEndpoint}", HttpMethod.Post, content, false);
            var tokenView = JsonConvert.DeserializeObject<MicrosoftResponseTokenView>(responseString);
            tokenView.TokenResourceType = apiResource.Contains("graph") ? "graph" : "onenote";
            SaveOneDriveAccessToken(tokenView);
            return tokenView;
        }

        public static async Task<string> GetOneDriveApiResponse(string uri, HttpMethod method, FormUrlEncodedContent content = null, bool appendAuthHeader = false, string tokenValue = "")
        {
            using (var client = new HttpClient())
            {
                if (appendAuthHeader)
                    client.DefaultRequestHeaders.Add("Authorization", (!string.IsNullOrEmpty(tokenValue) ? tokenValue : $"{SessionHelper.CurrentSession.MicrosoftToken.TokenType} {SessionHelper.CurrentSession.MicrosoftToken.AccessToken}"));
                var result = method == HttpMethod.Get ? await client.GetAsync($"{uri}") : await client.PostAsync(uri, content);
                var responseString = await result.Content.ReadAsStringAsync();
                return responseString;
            }
        }

        public static async Task<string> CallApiMethods(HttpMethod method, string uri, object body = null, string accessToken = "")
        {
            string bodyString;

            if (body is string)
                bodyString = body as string;
            else
                bodyString = JsonConvert.SerializeObject(body, jsonSettings);
            var apiUrl = uri;
            var request = new HttpRequestMessage(method, apiUrl);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            if (method != HttpMethod.Get && method != HttpMethod.Delete)
                request.Content = new StringContent(bodyString, Encoding.UTF8, "application/json");
            var httpClient = new HttpClient();
            HttpResponseMessage response = await httpClient.SendAsync(request);
            string responseBody = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                return string.Empty;
            }
            return responseBody;
        }

        public static async Task<string> ShareOneDriveFile(string fileId, IEnumerable<string> studentMails, string permission)
        {
            var credentials = !SessionHelper.CurrentSession.MicrosoftToken.TokenResourceType.Contains("graph") ? await RefreshOneDriveAccesToken(apiResource: "https://graph.microsoft.com/") : SessionHelper.CurrentSession.MicrosoftToken;
            List<dynamic> permissionStudents = new List<dynamic>();
            studentMails.ForEach(e =>
            {
                if (!string.IsNullOrEmpty(e))
                    permissionStudents.Add(new { email = e });
            });

            var payloads = new
            {
                requireSignIn = true,
                sendInvitation = false,
                roles = new string[] { permission },
                recipients = permissionStudents,
                message = "Here's the file that we're collaborating on."
            };

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", $"{credentials.TokenType} {credentials.AccessToken}");
                var result = await client.PostAsJsonAsync($"{_oneDriveApiEndPoint}/items/{fileId}/invite", payloads);
                var responseString = await result.Content.ReadAsStringAsync();
                return responseString;
            }
        }
        public static async Task<string> ShareOneDriveFileToTeacher(string fileId, string teacherEmail, string permission)
        {
            var credentials = !SessionHelper.CurrentSession.MicrosoftToken.TokenResourceType.Contains("graph") ? await RefreshOneDriveAccesToken(apiResource: "https://graph.microsoft.com/") : SessionHelper.CurrentSession.MicrosoftToken;
            List<dynamic> permissionStudents = new List<dynamic>();
            permissionStudents.Add(new { email = teacherEmail });

            var payloads = new
            {
                requireSignIn = true,
                sendInvitation = false,
                roles = new string[] { permission },
                recipients = permissionStudents,
                message = "Here's the file that we're collaborating on."
            };

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", $"{credentials.TokenType} {credentials.AccessToken}");
                var result = await client.PostAsJsonAsync($"{_oneDriveApiEndPoint}/items/{fileId}/invite", payloads);
                var responseString = await result.Content.ReadAsStringAsync();
                return responseString;
            }
        }
        public static async Task<string> CreateOneDriveSharingLink(string fileId, string scope = "anonymous")
        {
            var credentials = !SessionHelper.CurrentSession.MicrosoftToken.TokenResourceType.Contains("graph") ? await RefreshOneDriveAccesToken(apiResource: "https://graph.microsoft.com/") : SessionHelper.CurrentSession.MicrosoftToken;
            var payloads = new
            {
                type = "view",
                scope = scope
            };


            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", $"{credentials.TokenType} {credentials.AccessToken}");
                var result = await client.PostAsJsonAsync($"{_oneDriveApiEndPoint}/items/{fileId}/createLink", payloads);
                var responseString = await result.Content.ReadAsStringAsync();
                return responseString;
            }
        }
        #endregion

        #region One Note
        public static async Task<string> AssignNotebookToStudent(string studentEmail, string fileId)
        {
            var credentials = SessionHelper.CurrentSession.MicrosoftToken.TokenResourceType.Contains("graph") ? await RefreshOneDriveAccesToken(apiResource: "https://onenote.com/") : SessionHelper.CurrentSession.MicrosoftToken;
            var payload = new
            {
                id = studentEmail,
                principalType = "Person"
            };
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", $"{credentials.TokenType} {credentials.AccessToken}");
                client.DefaultRequestHeaders.Add("accept", "application/json");
                var result = await client.PostAsJsonAsync($"https://www.onenote.com/api/v1.0/me/notes/classNotebooks/{fileId}/students", payload);
                var responseString = await result.Content.ReadAsStringAsync();
                return responseString;
            }
        }

        public static async Task<string> ShareAllFilesToStudents(string folderId, Dictionary<string, string> assignmentFilesNames, string permission)
        {
            IList<CloudFileListView> filesList = await GetOneDriveFolderFiles(folderId);
            foreach (var keyValue in assignmentFilesNames)
            {
                var file = filesList.Where(x => x.Name.Equals(keyValue.Value, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                await ShareOneDriveFile(file.Id, new List<string>() { keyValue.Key }, permission);
            }
            return string.Empty;
        }
        #endregion
    }
    public class AppFlowMetadata : FlowMetadata
    {
        public override string AuthCallback => "/Files/Files/DriveAuthCallback";
        public static string filePath => Path.Combine(HttpContext.Current.Server.MapPath("~"), "Content");
        public static IAuthorizationCodeFlow flow =
            new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
            {
                ClientSecrets = new ClientSecrets
                {
                    ClientId = ConfigurationManager.AppSettings["GoogleDriveApiKey"].ToString(),
                    ClientSecret = ConfigurationManager.AppSettings["GoogleDriveSecretKey"].ToString()
                },
                Scopes = new[] { DriveService.Scope.Drive, DriveService.Scope.DriveReadonly },

                DataStore = new FileDataStore(filePath, true)
            });

        public override string GetUserId(Controller controller)
        {
            var user = controller.Session["googleuser"];
            if (user == null)
            {
                user = Guid.NewGuid();
                controller.Session["googleuser"] = user;
            }
            return user.ToString();
        }

        public override IAuthorizationCodeFlow Flow
        {
            get { return flow; }
        }
    }
}