﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Helpers
{
    public static class FunctionalConfig
    {
        public static bool IsAzureBlobStorage
        {
            get
            {
#if ELITE
                return true;
#else
        return false;
#endif
            }
        }
        public static bool IsForgotPasswordForParent
        {
            get
            {
#if ELITE
                return true;
#else
        return false;
#endif
            }
        }
        public static bool IsSharePoint
        {
            get
            {
#if GEMS
                return true;
#else
                return false;
#endif
            }
        }
    }
}