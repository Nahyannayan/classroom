﻿using Phoenix.Common.Helpers;

namespace Phoenix.VLE.Web.Helpers
{
    public enum ListItems
    {
        [StringValue("countries")]
        Countries,
        [StringValue("Assessment")]
        Assessment,
        [StringValue("city")]
        City,
        [StringValue("incidentsection")]
        IncidentSection,
        [StringValue("incidentcountry")]
        IncidentCountry,
        [StringValue("incidentcity")]
        IncidentCity,
        [StringValue("incidentbusinessunit")]
        IncidentBusinessUnit,
        [StringValue("incidentseverity")]
        IncidentSeverity,
        [StringValue("incidententitytype")]
        IncidentEntityType,
        [StringValue("IncidentCategory")]
        IncidentCategory,
        [StringValue("TaskUser")]
        TaskUser,
        [StringValue("locationfilter")]
        LocationFilter,
        [StringValue("Users")]
        Users,
        [StringValue("IncidentInvestigationUsers")]
        IncidentInvestigationUsers,
        [StringValue("Subject")]
        SubjectName,
        [StringValue("ModuleStructure")]
        ModuleList,
        [StringValue("Grade")]
        Grade,
        [StringValue("Section")]
        Section,
        [StringValue("UserTypes")]
        UserTypes,
        [StringValue("DocumentBU")]
        DocumentBU,
        [StringValue("SchoolGroup")]
        SchoolGroup,
        [StringValue("Student")]
        Student,
        [StringValue("BuisnessUnitByCountryId")]
        BuisnessUnitByCountryId,
        [StringValue("SafetyAuditQuestions")]
        SafetyAuditQuestions,
        [StringValue("SafetyAuditControlProfile")]
        SafetyAuditControlProfile,
        [StringValue("SafetyAuditFrequency")]
        SafetyAuditFrequency,
        [StringValue("UserByRole")]
        UserByRole,
        [StringValue("TaskSource")]
        TaskSource,
        [StringValue("AuditList")]
        AuditList,
        [StringValue("SafetyAuditSeverity")]
        SafetyAuditSeverity,
        [StringValue("SafetyAuditScoreCalcMaster")]
        SafetyAuditScoreCalcMaster,
        [StringValue("TaskAssignto")]
        TaskAssignto,
        [StringValue("BehaviourMainCategory")]
        BehaviourMainCategory,
        [StringValue("BehaviourSubCategory")]
        BehaviourSubCategory,
        [StringValue("SchoolSpaceCategory")]
        SchoolSpaceCategory,
        [StringValue("GradingTemplate")]
        GradingTemplate,
        [StringValue("MainSyllabus")]
        MainSyllabus,
        [StringValue("SubSyllabus")]
        SubSyllabus,
        [StringValue("Lessons")]
        Lessons,
        [StringValue("QuestionType")]
        QuestionType,
        [StringValue("AttendanceType")]
        AttendanceType,
        [StringValue("AttendanceConfig")]
        AttendanceConfig,
        [StringValue("QuizList")]
        QuizList,
        [StringValue("AcademicYear")]
        AcademicYear,
        [StringValue("ReportHeader")]
        ReportHeader,
        [StringValue("SubjectCategory")]
        SubjectCategory,
        [StringValue("ReportSchedule")]
        ReportSchedule,
        [StringValue("Terms")]
        Terms,
        [StringValue("AssessmentActivity")]
        AssessmentActivity,
        [StringValue("SchoolAcademicYear")]
        SchoolAcademicYear,
        [StringValue("Stream")]
        Stream,
        [StringValue("AgeBand")]
        AgeBand,
        [StringValue("Curriculum")]
        Curriculum,
        [StringValue("Department")]
        Department,
        [StringValue("SubjectOption")]
        SubjectOption,
        [StringValue("SchoolGrade")]
        SchoolGrade,
        [StringValue("StudentTheme")]
        StudentTheme,
        [StringValue("AcademicYearList")]
        AcademicYearList,
        [StringValue("CertificateTypes")]
        CertificateTypes,
        [StringValue("StudentGroupTeacher")]
        StudentGroupTeacher,
        [StringValue("CertificateColumns")]
        CertificateColumns,
        [StringValue("Course")]
        Course,
        [StringValue("SchoolCurriculum")]
        SchoolCurriculum,
        [StringValue("Standard")]
        Standard,
        [StringValue("CoursesList")]
        CoursesList,
        [StringValue("CourseGroupList")]
        CourseGroupList,
        [StringValue("TeacherCourse")]
        TeacherCourse,

        [StringValue("GradeTemplateMaster")]
        GradeTemplateMaster,
        [StringValue("CourseGradeDisplay")]
        CourseGradeDisplay,
        [StringValue("GradeSlab")]
        GradeSlab,
        [StringValue("AssignmentCategory")]
        AssignmentCategory,

        [StringValue("StandardizedAssessment")]
        StandardizedAssessment,
        [StringValue("ExternalExamination")]
        ExternalExamination,

        [StringValue("ExternalSubExamination")]
        ExternalSubExamination,

        [StringValue("StudentFeesAcademicYears")]
        StudentFeesAcademicYears,

        [StringValue("IsTransportFeePaymentEnabled")]
        IsTransportFeePaymentEnabled,
        [StringValue("LiveSessionDuration")]
        LiveSessionDuration,
        [StringValue("ReportIssueTypes")]
        ReportIssueTypes,
        [StringValue("ReportDetail")]
        ReportDetail,
        [StringValue("ReportModule")]
        ReportModule,
        [StringValue("UnitGroupName")]
        UnitGroupName,
        [StringValue("AttendanceParameterShortCut")]
        AttendanceParameterShortCut,
        [StringValue("SchoolDivision")]
        SchoolDivision,
        [StringValue("SchoolDepartment")]
        SchoolDepartment,
        [StringValue("AssessmentType")]
        AssessmentType,
        [StringValue("SubAssessmentType")]
        SubAssessmentType,
        [StringValue("DepartmentCourse")]
        DepartmentCourse
    }
}
