﻿using Phoenix.Common.Enums;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Areas.SMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Phoenix.VLE.Web.Helpers
{
    public static class API
    {
        public static class Student
        {

            public static string GetAllStudents(string baseUri) => $"{baseUri}/getstudents";
            public static string GetStudent(string baseUri, int id) => $"{baseUri}/getstudent/{id}";
            public static string AddStudent(string baseUri) => $"{baseUri}/addstudent";
            public static string UpdateStudent(string baseUri, int id) => $"{baseUri}/updatestudent/{id}";
            public static string DeleteStudent(string baseUri, int id) => $"{baseUri}/deletestudent/{id}";
            public static string GetStudentByUserId(string baseUri, long id) => $"{baseUri}/getStudentByUserId?id={id}";
            public static string GetStudentByFamily(string baseUri, long id) => $"{baseUri}/getStudentByFamily?id={id}";
            public static string GetStudentDashboard(string baseUri, long id) => $"{baseUri}/GetStudentDashboard?id={id}";
            public static string GetStudentProfileDetails(string baseUri, long userId) => $"{baseUri}/GetStudentProfileDetails?userId={userId}";
            public static string SaveStudentProfileImages(string baseUri) => $"{baseUri}/SaveStudentProfileImages";
            public static string UpdateStudentPortfolioSectionDetails(string baseUri) => $"{baseUri}/UpdateStudentPortfolioSectionDetails";
            public static string CheckIfTheResourceDeleted(string baseUri, int sourceId, string notificationType) => $"{baseUri}/checkIfTheResourceDeleted?sourceId={sourceId}&notificationType={notificationType}";
        }
        public static class Parent
        {
            public static string GetStudentDetailsById(string baseUri, long id) => $"{baseUri}/getstudentdetails?id={id}";
            public static string AssignAccessPermission(string baseUri, string StudentIdList) => $"{baseUri}/AssignAccessPermission?StudentIdList={StudentIdList}";

        }

        public static class StudentInformation
        {
            public static string GetStudentListBySchoolId(string baseUri, int schoolId) => $"{baseUri}/GetStudentListBySchoolId/{schoolId}"; public static string GetAllStudentCertificates(string baseUri, long id) => $"{baseUri}/GetAllStudentCertificates/{id}";
            public static string UpdateDeleteUserCertificate(string baseUri, char mode) => $"{baseUri}/UpdateDeleteUserCertificate?mode={mode}";
            public static string GetStudentSkillsData(string baseUri, long id) => $"{baseUri}/GetStudentSkillsData?id={id}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string InsertAcademicFile(string baseUri, long userId) => $"{baseUri}/InsertAcademicFile?id={userId}";
            public static string UpdateAssignmentStatus(string baseUri) => $"{baseUri}/UpdateAssignmentStatus";
            public static string UpdateAllAssignmentStatus(string baseUri) => $"{baseUri}/UpdateAllAssignmentStatus";
            public static string GetStudentsInGroup(string baseUri, int groupId) => $"{baseUri}/GetStudentsInGroup/{groupId}";
            public static string GetStudentsNotInGroup(string baseUri, int schoolId, int groupId) => $"{baseUri}/GetStudentsNotInGroup/{schoolId}/{groupId}";
            public static string InsertCertificateFile(string baseUri) => $"{baseUri}/InsertCertificateFile";
            public static string GetStudentsInSelectedGroups(string baseUri) => $"{baseUri}/GetStudentsInSelectedGroups";
            public static string GetStudentCourses(string baseUri) => $"{baseUri}/GetStudentCourses";
            public static string UpdateAcademicFileDashboardStatus(string baseUri) => $"{baseUri}/UpdateAcademicFileDashboardStatus/";
            public static string GetStudentDetailsByIds(string baseUri) => $"{baseUri}/GetStudentDetailsByIds";
            public static string GetStudentDetailsByStudentId(string baseUri, int studentId) => $"{baseUri}/GetStudentDetailsByStudentId?studentId={studentId}";
            public static string InsertStudentSkill(string baseUri) => $"{baseUri}/InsertStudentSkill/";
            public static string UpdateAssessmentReportStatus(string baseUri) => $"{baseUri}/UpdateAssessmentReportStatus";
            public static string UpdateStudentSkillsData(string baseUri) => $"{baseUri}/UpdateStudentSkillsData/";
            public static string GetStudentPortfolioInformation(string baseUri, long userId) => $"{baseUri}/GetStudentPortfolioInformation?userId={userId}";
            public static string DeleteAcademicAssignment(string baseUri) => $"{baseUri}/DeleteAcademicAssignment";
            public static string DeleteAcheivementFile(string baseUri) => $"{baseUri}/DeleteAcheivementFile";
            public static string GetStudentAcademicData(string baseUri, long id) => $"{baseUri}/GetStudentAcademicData/{id}";
            public static string GetStudentAcademicDetailById(string baseUri, int assignmentId, long userId) => $"{baseUri}/GetStudentAcademicDetailById?assignmentId={assignmentId}&userId={userId}";
            public static string GetAllStudentGoals(string baseUri, long userId, int pageSize, int pageIndex, string searchString, long currentUserId) => $"{baseUri}/GetAllStudentGoals?id={userId}&pageIndex={pageIndex}&pageSize={pageSize}&searchString={searchString}&currentUserId={currentUserId}";

            public static string GetStudentAcademicAssignments(string baseUri, long id, string searchString) => $"{baseUri}/GetStudentAcademicAssignments?userId={id}&searchString={searchString}";

            public static string InsertAchievementFile(string baseUri) => $"{baseUri}/InsertAchievementFile";
            public static string GetStudentachevementById(string baseUri, long acheivementId) => $"{baseUri}/GetStudentAcheivementById?acheivementId={acheivementId}";
            public static string UpdateDashboardGoalsStatus(string baseUri) => $"{baseUri}/UpdateDashboardGoalsStatus";
            public static string UpdateAllDashboardGoalsStatus(string baseUri) => $"{baseUri}/UpdateAllDashboardGoalsStatus";
            public static string UpdateAcheivementApprovalStatus(string baseUri) => $"{baseUri}/UpdateAcheivementApprovalStatus";
            public static string InsertStudentGoals(string baseUri) => $"{baseUri}/InsertStudentGoals";
            public static string UpdateStudentSkillEndorsementDetails(string baseUri) => $"{baseUri}/UpdateStudentSkillEndorsementDetails";
            public static string DeleteAcheivement(string baseUri) => $"{baseUri}/DeleteAcheivement";
            public static string GetAllStudentEndorsedSkills(string baseUri, long userId, int pageIndex, int pageSize, string searchString, long currentUserId) => $"{baseUri}/GetAllStudentEndorsedSkills?userId={userId}&pageSize={pageSize}&pageIndex={pageIndex}&searchString={searchString}&currentUserId={currentUserId}";
            public static string UpdateEndorseSkillStatusRating(string baseUri) => $"{baseUri}/UpdateEndorseSkillStatusRating";
            public static string UpdateAllEndorsedSkillStatus(string baseUri) => $"{baseUri}/UpdateAllEndorsedSkillStatus";
            public static string InsertStudentAchievement(string baseUri) => $"{baseUri}/InsertAchievement";
            public static string DeleteEndorsedSkill(string baseUri) => $"{baseUri}/DeleteEndorsedSkill";

            public static string GetStudentAchievements(string baseUri, long userId, int pageIndex, int pageSize, string searchString, short type) => $"{baseUri}/GetStudentAchievements?userId={userId}&pageIndex={pageIndex}&pageSize={pageSize}&searchString={searchString}&type={type}";
            public static string UpdateStudentEndorsementStatus(string baseUri) => $"{baseUri}/UpdateStudentEndorsementStatus";

            public static string GetStudentsWithStaffInGroup(string baseUri, int groupId) => $"{baseUri}/GetStudentsWithStaffInGroup/{groupId}";

            public static string GetStudentEndorsedSkillById(string baseUri, int skillEndorsementId) => $"{baseUri}/GetStudentEndorsedSkillById?skillEndorsementId={skillEndorsementId}";
            public static string GetStudentAchievementsWithFiles(string baseUri, long userId, int pageIndex, int pageSize, string searchString, long currentUserId) => $"{baseUri}/GetStudentAchievementsWithFiles?userId={userId}&pageIndex={pageIndex}&pageSize={pageSize}&searchString={searchString}&currentUserId={currentUserId}";
            public static string UpdateAchievementsPortfolioStatus(string baseUri) => $"{baseUri}/UpdateAchievementsPortfolioStatus";
            public static string UpdateAcheivementDashboardStatus(string baseUri) => $"{baseUri}/UpdateAcheivementDashboardStatus";
            public static string DeleteAcademicFile(string baseUri) => $"{baseUri}/DeleteAcademicFile";
            public static string SaveStudentDescription(string baseUri) => $"{baseUri}/SaveStudentDescription";
            public static string SaveStudentAboutMe(string baseUri) => $"{baseUri}/SaveStudentAboutMe";

            public static string GetStudentCertificates(string baseUri, long userId, int isTeacher) => $"{baseUri}/GetStudentCertificates?userId={userId}&isTeacher={isTeacher}";
            public static string GetStudentCertificateById(string baseUri, long Id) => $"{baseUri}/GetStudentCertificateByID?Id={Id}";
            public static string InsertCertificate(string baseUri) => $"{baseUri}/InsertCertificate";
            public static string UpdateCertificate(string baseUri) => $"{baseUri}/UpdateCertificate";

            public static string GetStudentByGradeIds(string baseUri, string gradeIds) => $"{baseUri}/GetStudentByGradeIds?gradeIds={gradeIds}";
            public static string GetStudentByGradeSection(string baseUri, long gradeId, long sectionId) =>
                $"{baseUri}/GetStudentByGradeSection?gradeId={gradeId}&sectionId={sectionId}";

            public static string GetParentsByStudents(string baseUri, string studentIds) => $"{baseUri}/GetParentsByStudents?studentIds={studentIds}";
            public static string ApproveStudentGoal(string baseUri) => $"{baseUri}/ApproveStudentGoal";
            public static string GetStudentProfileSections(string baseUri, long Id) => $"{baseUri}/GetStudentsProfileSections?studentid={Id}";
            public static string GetStudentDetailsByIdsPaginate(string baseUri, int page, int size) => $"{baseUri}/GetStudentDetailsByIdsPaginate?page={page}&size={size}";

            public static string GetStudentActivePoints(string baseUri, long id) => $"{baseUri}/GetTotalPointsByUserId?UserId={id}";

            public static string GetLeaderBoard(string baseUri, long? schoolId, long? studentId, int? range, int? gradeId, DateTime? date) => $"{baseUri}/GetLeadBoard?schoolId={schoolId}&studentId={studentId}&range={range}&gradeId={gradeId}&date={date}";
        }

        #region Common
        public static class SelectList
        {
            public static string GetSelectListItems(string baseUri, string listCode, string whereCondition, object whereConditionParamValues) => $"{baseUri}/getselectlistitems?listCode={listCode}&whereCondition={whereCondition}&whereConditionParamValues={whereConditionParamValues}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string GetSubjectListItems(string baseUri, int languageId, int userId) => $"{baseUri}/getsubjectlistitems?languageId={languageId}&userId={userId}";
            public static string GetAcademicYearListItem(string baseUri, int userId) => $"{baseUri}/GetAcademicYearByUserId?userId={userId}";
            public static string GetAssignmentCategoryMasterList(string baseUri) => $"{baseUri}/GetAssignmentCategoryMasterList";
            public static string GetAssignmentCategoryMappingListBySchoolId(string baseUri, long SchoolId) => $"{baseUri}/GetAssignmentCategoryMappingListBySchoolId?SchoolId={SchoolId}";
        }

        public static class ModuleStructure
        {
            public static string GetPhoenixModuleStructure(string baseUri, int systemLanguageId,
            long userId, string applicationCode, string traverseDirection, string moduleUrl, string moduleCode, bool excludeParent,
            string excludeModuleCodes, bool? showInMenu)
                => $"{baseUri}/getphoenixmodulestructure?systemLanguageId={systemLanguageId}&userId={userId}&applicationCode={applicationCode}&traverseDirection={traverseDirection}&moduleUrl={moduleUrl}&moduleCode={moduleCode}&excludeParent={excludeParent}&excludeModuleCodes={excludeModuleCodes}&showInMenu={showInMenu}";
        }

        public static class Common
        {
            public static string GetEmailSettings(string baseUri) => $"{baseUri}/getEmailSettings";
            public static string GetSchoolCurrentLanguage(string baseUri, int schoolId) => $"{baseUri}/getSchoolCurrentLanguage?schoolId={schoolId}";
            public static string SetSchoolCurrentLanguage(string baseUri, int languageId, int schoolId) => $"{baseUri}/setSchoolCurrentLanguage?languageId={languageId}&schoolId={schoolId}";
            public static string SetUserCurrentLanguage(string baseUri, int languageId, long userId) => $"{baseUri}/setUserCurrentLanguage?languageId={languageId}&userId={userId}";
            public static string GetUserCurrentLanguage(string baseUri, int languageId) => $"{baseUri}/getUserCurrentLanguage?languageId={languageId}";
            public static string OperationAuditCU(string baseUri) => $"{baseUri}/OperationAuditCU";
            public static string GetSystemImageList(string baseUri) => $"{baseUri}/GetSystemImageList";
            public static string GetServerDateTime(string baseUri) => $"{baseUri}/GetServerDateTime";
        }

        #endregion

        #region Security Management

        public static class SafetyCategories
        {
            public static string GetSafetyCategories(string baseUri, int languageId, int schoolId) => $"{baseUri}/getsafetycategories?languageId={languageId}&schoolId={schoolId}";
            public static string GetSafetyCategoryById(string baseUri, int id) => $"{baseUri}/getsafetycategorybyid?id={id}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string InsertSafetyCategory(string baseUri) => $"{baseUri}/insertsafetycategory";
            public static string UpdateSafetyCategory(string baseUri) => $"{baseUri}/updatesafetycategory";
            public static string DeleteSafetyCategory(string baseUri, int id) => $"{baseUri}/deletesafetycategory/{id}";
        }

        public static class CensorFilter
        {
            public static string GetCensorUsers(string baseUri, int schoolId) => $"{baseUri}/getcensorusers?schoolId={schoolId}";
            public static string GetBannedWords(string baseUri, int schoolId) => $"{baseUri}/getbannedwords?schoolId={schoolId}";
            public static string InsertBannedWord(string baseUri) => $"{baseUri}/insertbannedword";
            public static string UpdateBannedWord(string baseUri) => $"{baseUri}/updatebannedword";
            public static string DeleteBannedWord(string baseUri) => $"{baseUri}/deletebannedword";
        }

        public static class AbuseDelegate
        {
            public static string GetAllAbuseDelegates(string baseUri) => $"{baseUri}/getAbuseDelegates";
            public static string GetAllAbuseDelegateBySchoolId(string baseUri, long id) => $"{baseUri}/getAbuseDelegatesbyschoolid/?schoolId={id}";
            public static string GetAbuseDelegateById(string baseUri, long id) => $"{baseUri}/getAbuseDelegatebyid/?id={id}";
            public static string AddAbuseDelegate(string baseUri) => $"{baseUri}/addAbuseDelegate";
            public static string UpdateAbuseDelegate(string baseUri) => $"{baseUri}/updateAbuseDelegate";
            public static string DeleteAbuseDelegate(string baseUri, long id) => $"{baseUri}/deleteAbuseDelegate/?id={id}";

            public static string GetAllAbuseContactInfo(string baseUri) => $"{baseUri}/getAbuseContactInfos";
            public static string GetAllAbuseContactInfoBySchoolId(string baseUri, long id) => $"{baseUri}/getAbuseContactInfobyschoolid/?schoolId={id}";
            public static string GetAbuseContactInfoById(string baseUri, long id) => $"{baseUri}/getAbuseContactInfobyid/?id={id}";
            public static string AddAbuseContactInfo(string baseUri) => $"{baseUri}/addAbuseContactInfo";
            public static string UpdateAbuseContactInfo(string baseUri) => $"{baseUri}/updateAbuseContactInfo";
            public static string DeleteAbuseContactInfo(string baseUri, long id) => $"{baseUri}/deleteAbuseContactInfo/?id={id}";

            public static string GetAllAbuseReportLedger(string baseUri) => $"{baseUri}/getAbuseReportLedgers";
            public static string GetAllAbuseReportLedgerBySafetyCategoryId(string baseUri, long id) => $"{baseUri}/getAbuseReportLedgerbysafetycategoryid/?safetyCategoryId={id}";
            public static string GetAllAbuseReportLedgerByAbuseReportLedgerId(string baseUri, long id) => $"{baseUri}/getAbuseReportLedgerbyabusereportledgerid/?id={id}";
            public static string AddAbuseReportLedger(string baseUri) => $"{baseUri}/addAbuseReportLedger";
            public static string UpdateAbuseReportLedger(string baseUri) => $"{baseUri}/updateAbuseReportLedger";
            public static string DeleteAbuseReportLedger(string baseUri, long id) => $"{baseUri}/deleteAbuseReportLedger?id={id}";
            public static string GetAbuseReportByUserId(string baseUri, long id) => $"{baseUri}/getAbuseReportListByUser/?id={id}";
            public static string SaveMobileSettings(string baseUri) => $"{baseUri}/SaveMobileSettings";
        }
        #endregion

        #region List & Categories

        public static class SuggestionCategory
        {
            public static string GetSuggestionCategory(string baseUri, int languageId, int SchoolId) => $"{baseUri}/getsuggestioncategories?languageId={languageId}&schoolId={SchoolId}";
            public static string GetSuggestionCategoryById(string baseUri, int id) => $"{baseUri}/getsuggestioncategorybyid?Id={id}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string InsertSuggestionCategory(string baseUri) => $"{baseUri}/insertsuggestioncategory";
            public static string UpdateSuggestionCategory(string baseUri) => $"{baseUri}/UpdateSuggestionCategory";
            public static string DeleteSuggestionCategory(string baseUri, int id) => $"{baseUri}/DeleteSuggestionCategory/{id}";
        }

        public static class Suggestion
        {
            //public static string GetSuggestionCategory(string baseUri, int languageId, int SchoolId) => $"{baseUri}/getsuggestioncategories?languageId={languageId}&schoolId={SchoolId}";
            public static string getThinkBoxUsersBySchoolId(string baseUri, long id) => $"{baseUri}/getThinkBoxUsersBySchoolId/?Id={id}";
            public static string getStudentSuggestionByUserId(string baseUri, int id) => $"{baseUri}/getStudentSuggestionsByUserId?Id={id}";
            public static string getStudentSuggestionBySchoolId(string baseUri, int id) => $"{baseUri}/getStudentSuggestionsBySchoolId?Id={id}";
            public static string PaginateStudentSuggestionByUserId(string baseUri, int userId, int pageNumber, int pageSize, string SearchString = "", string sortBy = "") => $"{baseUri}/paginateStudentSuggestionByUserId?userId={userId}&pageNumber={pageNumber}&pageSize={pageSize}&SearchString={SearchString}&sortBy={sortBy}";
            public static string PaginateStudentSuggestionBySchoolId(string baseUri, int schoolId, int pageNumber, int pageSize, string SearchString = "", string sortBy = "") => $"{baseUri}/paginateStudentSuggestionBySchoolId?schoolId={schoolId}&pageNumber={pageNumber}&pageSize={pageSize}&SearchString={SearchString}&sortBy={sortBy}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string getStudentSuggestionById(string baseUri, int id) => $"{baseUri}/getStudentSuggestionById?Id={id}";
            public static string GetStudentSuggestionByIdAndUserId(string baseUri, int id, long userId) => $"{baseUri}/getStudentSuggestionByIdAndUserId?Id={id}&userId={userId}";
            public static string DeleteUserFromThinkBox(string baseUri, long userId, long schoolId) => $"{baseUri}/deleteUserFromThinkBox/?userId={userId}&schoolId={schoolId}";
            public static string HasThinkBoxUserPermission(string baseUri, long userId) => $"{baseUri}/hasThinkBoxUserPermission/?userId={userId}";

            public static string InsertSuggestion(string baseUri) => $"{baseUri}/insertStudentSuggestion";
            public static string AddUpdateThinkBoxUser(string baseUri) => $"{baseUri}/addupdatethinkboxuser";
            public static string UpdateSuggestion(string baseUri) => $"{baseUri}/updateSudentSuggestion";
            public static string DeleteSuggestionData(string baseUri, int id, long userId) => $"{baseUri}/deleteSudentSuggestion?Id={id}&userId={userId}";

            //public static string DeleteSuggestionCategory(string baseUri, int id) => $"{baseUri}/DeleteSuggestionCategory/{id}";
        }

        public static class Subject
        {
            public static string GetSubjects(string baseUri, int languageId, int schoolId) => $"{baseUri}/getsubjects?languageId={languageId}&schoolId={schoolId}";
            public static string GetSubjectById(string baseUri, int id) => $"{baseUri}/getsubjectbyid/{id}";
            public static string GetLessonsByTopicId(string baseUri, int id) => $"{baseUri}/getLessonsByTopicId/{id}";
            public static string GetSelectedUnitByGroupId(string baseUri, string id) => $"{baseUri}/getSelectedUnitByGroupId?groupIds={id}";
            public static string InsertSubject(string baseUri) => $"{baseUri}/insertsubject";
            public static string UpdateSubject(string baseUri) => $"{baseUri}/updatesubject";
            public static string DeleteSubject(string baseUri, int id) => $"{baseUri}/deletesubject/{id}";
            public static string GetTopicSubTopicStructure(string baseUri, int subjectId) => $"{baseUri}/GetTopicSubTopicStructure?subjectId={subjectId}";
            public static string GetUnitStructure(string baseUri, string courseIds) => $"{baseUri}/GetUnitStructure?courseIds={courseIds}";
            public static string GetTopicSubTopicStructureByMultipleSubjectIds(string baseUri, string subjectIds) => $"{baseUri}/GetTopicSubTopicStructureByMultipleSubjectIds?subjectIds={subjectIds}";
            public static string GetSubTopicObjective(string baseUri, int id, bool isMainSyllabus, int subjectId, bool isLesson) => $"{baseUri}/GetSubtopicObjectives?id={id}&isMainSyllabus={isMainSyllabus}&subjectId={subjectId}&isLesson={isLesson}";
            public static string GetSubtopicObjectivesWithMultipleSubjects(string baseUri, int id, bool isMainSyllabus, string subjectIds, bool isLesson) => $"{baseUri}/GetSubtopicObjectivesWithMultipleSubjects?id={id}&isMainSyllabus={isMainSyllabus}&subjectIds={subjectIds}&isLesson={isLesson}";
            public static string GetAssignmentObjective(string baseUri, int assignmentId) => $"{baseUri}/GetAssignmentObjective?assignmentId={assignmentId}";
            public static string GetAssignmentUnit(string baseUri, int assignmentId) => $"{baseUri}/GetAssignmentUnit?assignmentId={assignmentId}";
            public static string GetAssignmentCourseTopicUnit(string baseUri, int assignmentId) => $"{baseUri}/GetAssignmentCourseTopicUnit?assignmentId={assignmentId}";
        }

        public static class SchoolGroup
        {
            public static string GetSchoolGroups(string baseUri, int schoolId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string SearchString = "") => $"{baseUri}/getschoolgroups?userId={schoolId}&pageNumber={pageNumber}&pageSize={pageSize}&IsBeSpokeGroup={IsBeSpokeGroup}&SearchString={SearchString}";
            public static string GetOtherschoolgroups(string baseUri, int schoolId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string SearchString = "") => $"{baseUri}/getschoolgroups?userId={schoolId}&pageNumber={pageNumber}&pageSize={pageSize}&IsBeSpokeGroup={IsBeSpokeGroup}&SearchString={SearchString}";
            public static string GetSchoolGroupsForAdminSpace(string baseUri) => $"{baseUri}/getSchoolGroupsForAdminSpace";
            public static string GetCoursesBySchoolId(string baseUri, long schoolId) => $"{baseUri}/getCoursesBySchoolId?schoolId={schoolId}";
            public static string GetAllTeamsMeetings(string baseUri, int schoolGroupId) => $"{baseUri}/GetAllTeamsMeetings?schoolGroupId={schoolGroupId}";
            public static string GetSchoolGroupsByStudentId(string baseUri, long studentId) => $"{baseUri}/getSchoolGroupsByStudentId?studentId={studentId}";
            public static string GetStudentGroups(string baseUri, long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string SearchString = "") => $"{baseUri}/getStudentGroups?studentId={studentId}&PageNumber={PageNumber}&PageSize={PageSize}&IsBespokeGroup={IsBespokeGroup}&SearchString={SearchString}";
            public static string GetOtherschoolStudentGroups(string baseUri, long studentId, int PageNumber, int PageSize, int IsBespokeGroup = 0, string SearchString = "") => $"{baseUri}/getStudentGroups?studentId={studentId}&PageNumber={PageNumber}&PageSize={PageSize}&IsBespokeGroup={IsBespokeGroup}&SearchString={SearchString}";
            public static string UpdateZoomMeetingInfo(string baseUri) => $"{baseUri}/UpdateZoomMeetingInfo";
            public static string GetSchoolGroupById(string baseUri, int id) => $"{baseUri}/getschoolgroupbyid/{id}";
            public static string GetAllSchoolLevel(string baseUri, long schoolId) => $"{baseUri}/GetAllSchoolLevel/{schoolId}";
            public static string GetAllCourses(string baseUri, long schoolId, long schoolLevelId, long departmentlId) => $"{baseUri}/GetAllSchoolCoursesBySchoolLevelAndSchoolIdAndDepartmentId?schoolId={schoolId}&schoolLevelId={schoolLevelId}&departmentlId={departmentlId}";


            public static string GetAllZoomMeetings(string baseUri, int schoolGroupId, string meetingId, long userId) => $"{baseUri}/GetAllZoomMeetings?schoolGroupId={schoolGroupId}&meetingId={meetingId}&userId={userId}";
            public static string UpdateZoomMeetingParticipants(string baseUri) => $"{baseUri}/UpdateZoomMeetingParticipants";
            public static string InsertGroup(string baseUri) => $"{baseUri}/InsertSchoolGroup";
            public static string UpdateSchoolGroup(string baseUri) => $"{baseUri}/updateschoolgroup";
            public static string UpdateTeacherGivenName(string baseUri) => $"{baseUri}/updateteachergivenname";
            public static string InsertGroupMessage(string baseUri) => $"{baseUri}/InsertGroupMessage";
            public static string SaveGroupAdobeMeeting(string baseUri) => $"{baseUri}/SaveGroupAdobeMeeting";
            public static string DeleteSchoolGroup(string baseUri, int id, long UserId = 0) => $"{baseUri}/deleteschoolgroup?id={id}&UserId={UserId}";
            public static string GetSubjectSchoolGroup(string baseUri, int subjectId, long schoolId) => $"{baseUri}/getsubjectschoolgroup?subjectId={subjectId}&schoolId={schoolId}";
            public static string GetAllAdobeMeetings(string baseUri, int schoolGroupId) => $"{baseUri}/GetAllAdobeMeetings?schoolGroupId={schoolGroupId}";

            public static string GetAssignedMembers(string baseUri, short systemLanguageId, int schoolGroupId) => $"{baseUri}/getAssignedMembers?systemLanguageId={systemLanguageId}&schoolGroupId={schoolGroupId}";

            public static string GetSchoolGroupsHavingBlogs(string baseUri, long id) => $"{baseUri}/GetSchoolGroupsHavingBlogs/{id}";
            public static string GetGroupMessageListByGroup(string baseUri, int groupId, long id) => $"{baseUri}/getGroupMessageListByGroup?groupId={groupId}&id={id}";
            public static string GetAllGroupBBBMeetings(string baseUri, int schoolGroupId) => $"{baseUri}/GetAllGroupBBBMeetings?schoolGroupId={schoolGroupId}";
            public static string GetGroupsBySchoolId(string baseUri, long schoolId) => $"{baseUri}/getSchoolGroupsBySchoolId/{schoolId}";
            public static string GetGroupsByUserId(string baseUri, long userId, bool isTeacher) => $"{baseUri}/getSchoolGroupsByUserId/{userId}/{isTeacher}";
            public static string GetUnassignedMembers(string baseUri, short systemLanguageId, int schoolId, long userId, int? userTypeId, int schoolGroupId) => $"{baseUri}/getUnassignedMembers?schoolId={schoolId}&userId={userId}&userTypeId={userTypeId}&schoolGroupId={schoolGroupId}";
            public static string GetUnassignedMembersWithSelectedGroups(string baseUri, short systemLanguageId, int schoolId, long userId, int? userTypeId, int schoolGroupId, string selectedGroupIds) => $"{baseUri}/GetUnassignedMembersWithSelectedGroup?schoolId={schoolId}&userId={userId}&userTypeId={userTypeId}&schoolGroupId={schoolGroupId}&selectedSchoolGroupIds={selectedGroupIds}";
            public static string AddUpdateMembersToGroup(string baseUri) => $"{baseUri}/AddUpdateGroupMember";
            public static string UpdateLastSeenDate(string baseUri, long userId, int schoolGroupId, bool isParent) => $"{baseUri}/UpdateLastSeen?userId={userId}&schoolGroupId={schoolGroupId}&isParent={isParent}";
            public static string UpdateArchiveSchoolGroup(string baseUri, long schoolGroupId, long teacherId) => $"{baseUri}/UpdateArchiveSchoolGroup?schoolGroupIds={schoolGroupId}&teacherId={teacherId}";
            public static string GetGroupBBBMeetingInfo(string baseUri, string meetingId) => $"{baseUri}/GetAllGroupBBBMeetings?meetingId={meetingId}";
            public static string GetArchivedGroupsByUserId(string baseUri, long teacherId, bool isTeacher) => $"{baseUri}/GetArchivedSchoolGroupsByUserId?teacherId={teacherId}&isTeacher={isTeacher}";
            public static string GetActiveGroupsByUserId(string baseUri, long teacherId, bool isTeacher) => $"{baseUri}/GetActiveSchoolGroupsByUserId?teacherId={teacherId}&isTeacher={isTeacher}";
            public static string GetGroupRecordedSessions(string baseUri, int groupId, int pageIndex, int pageSize, string searchText, string fromDate, string toDate) => $"{baseUri}/GetGroupRecordedSessions?groupId={groupId}&pageIndex={pageIndex}&pageSize={pageSize}&searchText={searchText}&fromDate={fromDate}&toDate={toDate}";

            public static string GetArchivedSchoolGroups(string baseUri, int userId, int pageNumber, int pageSize, int IsBeSpokeGroup = 0, string SearchString = "") => $"{baseUri}/GetArchivedSchoolGroups?userId={userId}&pageNumber={pageNumber}&pageSize={pageSize}&IsBeSpokeGroup={IsBeSpokeGroup}&SearchString={SearchString}";
            public static string GetAllLiveSessions(string baseUri, long userId, int pageIndex, int pageSize, string searchText, string type, string groupId)
                => $"{baseUri}/GetAllLiveSessions?userId={userId}&pageIndex={pageIndex}&pageSize={pageSize}&searchText={searchText}&type={type}&groupId={groupId}";

            public static string GetCurrentGroupMeeting(string baseUri, long userId) => $"{baseUri}/GetCurrentGroupMeeting?userId={userId}";
            public static string UpdateGroupMeeting(string baseUri) => $"{baseUri}/UpdateGroupMeeting";
            public static string RemoveGroupMeeting(string baseUri) => $"{baseUri}/RemoveGroupMeeting";
            public static string UpdateTeamsMeetingInfo(string baseUri) => $"{baseUri}/UpdateTeamsMeetingInfo";
            public static string GenerateZoomMeetingJoinURL(string baseUri) => $"{baseUri}/GenerateZoomMeetingJoinURL";
            public static string UpdateGroupWebExMeeting(string baseUri) => $"{baseUri}/UpdateGroupWebExMeeting";
            public static string GetAllGroupWebExMeetings(string baseUri, int schoolGroupId) => $"{baseUri}/GetAllGroupWebExMeetings?schoolGroupId={schoolGroupId}";
            public static string GetSchoolGroupWithPagination(string baseUri, long id, short pageSize, int pageIndex) => $"{baseUri}/GetSchoolGroupWithPagination?userId={id}&pageSize={pageSize}&pageNumber={pageIndex}";
            public static string GetSchoolGroupUserEmailAddress(string baseUri, string schoolGroupIds, string studentIds) => $"{baseUri}/GetSchoolGroupUserEmailAddress?schoolGroupIds={schoolGroupIds}&studentIds={studentIds}";
            public static string CreateSynchounousLesson(string baseUri) => $"{baseUri}/CreateSynchounousLesson";
            public static string DeleteLiveSession(string baseUri) => $"{baseUri}/DeleteLiveSession";
            public static string DeleteTeamsSession(string baseUri) => $"{baseUri}/DeleteTeamsSession";

            public static string GetUsersMailingDetails(string baseUri, string userIds) => $"{baseUri}/GetUsersMailingDetails?userIds={userIds}";

            public static string InsertGroupPrimaryTeacher(string baseUri) => $"{baseUri}/insertgroupprimaryteacher";
            public static string DeleteGroupPrimaryTeacher(string baseUri, long id) => $"{baseUri}/deletegroupprimaryteacher/{id}";
            public static string UpdateSchoolGroupsToHide(string baseUri) => $"{baseUri}/updateschoolgrouptohide";
            public static string CheckStudentGroupsAvailable(string baseUri, long studentId, int IsBespokeGroup) => $"{baseUri}/checkStudentGroupsAvailable?studentId={studentId}&IsBespokeGroup={IsBespokeGroup}";

            public static string GetDisabledGroupList(string baseUri, long BlogId, long SchoolId) => $"{baseUri}/GetDisabledGroupList?BlogId={BlogId}&SchoolId={SchoolId}";
            public static string UpdateBlogWithNewUpdate(string baseUri, long UpdateBlogId, long UpdatFromBlogId, bool IsApproved, bool IsRejected) => $"{baseUri}/UpdateBlogWithNewUpdate?UpdateBlogId={UpdateBlogId}&UpdatFromBlogId={UpdatFromBlogId}&IsApproved={IsApproved}&IsRejected={IsRejected}";


        }

        public static class AsyncLesson
        {
            public static string InsertAsyncLesson(string baseUri) => $"{baseUri}/InsertAsyncLesson";
            public static string UpdateAsyncLesson(string baseUri) => $"{baseUri}/UpdateAsyncLesson";
            public static string DeleteAsyncLesson(string baseUri, long id, long userId) => $"{baseUri}/deleteasynclesson/{id}/{userId}";
            public static string GetAsyncLessons(string baseUri, long? asyncLessonId, long? sectionId, long? moduleId, long? folderId, bool? isActive, int? IsIncludeResources, int? IsIncludeStudents) => $"{baseUri}/getasynclesson?asyncLessonId={asyncLessonId}&sectionId={sectionId}&moduleId={moduleId}&folderId={folderId}&isActive={isActive}&IsIncludeResources={IsIncludeResources}&IsIncludeStudents={IsIncludeStudents}";
            public static string GetAsyncLessonById(string baseUri, long asyncLessonId, bool? isActive) => $"{baseUri}/getasynclessonbyid?asyncLessonId={asyncLessonId}&isActive={isActive}";

            public static string InsertResourceActivity(string baseUri) => $"{baseUri}/InsertResourceActivity";
            public static string UpdateResourceActivity(string baseUri) => $"{baseUri}/UpdateResourceActivity";
            public static string DeleteResouceActivity(string baseUri, long id, long userId) => $"{baseUri}/deleteresourceactivity/{id}/{userId}";
            public static string GetResourcesActivities(string baseUri, long asyncLessonId, long userId, bool? isActive) => $"{baseUri}/getresourcesactivities?asyncLessonId={asyncLessonId}&userId={userId}&isActive={isActive}";
            public static string UpdateResouceActivityStatus(string baseUri) => $"{baseUri}/updateresourceactivitystatus";

            public static string UpdateStudentMapping(string baseUri) => $"{baseUri}/Updatestudentmapping";
            public static string GetAsyncLessonStudentMappingByAsyncLessonId(string baseUri, long asyncLessonId, long? userId, bool? isActive) => $"{baseUri}/getstudentmappingbyasynclessonId?asyncLessonId={asyncLessonId}&userId={userId}&isActive={isActive}";

            public static string InsertComment(string baseUri) => $"{baseUri}/InsertComment";
            public static string UpdateComment(string baseUri) => $"{baseUri}/UpdateComment";
            public static string DeleteComment(string baseUri, long id, long userId) => $"{baseUri}/deletecomment/{id}/{userId}";
            public static string GetAsyncLessonComments(string baseUri, long? asyncLessonCommentId, long? resourceActivityId, long studentId, long asyncLessonId) => $"{baseUri}/getasynclessoncomments?asyncLessonCommentId={asyncLessonCommentId}&resourceActivityId={resourceActivityId}&studentId={studentId}&asyncLessonId={asyncLessonId}";
        }


        public static class GradingTemplate
        {
            public static string GetGradingTemplates(string baseUri, int languageId, int schoolId) => $"{baseUri}/GetGradingTemplates?languageId={languageId}&schoolId={schoolId}";
            public static string GetGradingTemplateById(string baseUri, int id) => $"{baseUri}/GetGradingTemplateById?id={id}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string InsertGradingTemplate(string baseUri) => $"{baseUri}/InsertGradingTemplate";
            public static string UpdateGradingTemplate(string baseUri) => $"{baseUri}/UpdateGradingTemplate";
            public static string DeleteGradingTemplate(string baseUri, int id) => $"{baseUri}/DeleteGradingTemplate/{id}";
        }
        public static class MyPlanner
        {
            public static string GetEventCategories(string baseUri, long schoolId) => $"{baseUri}/getEventCategory?schoolId={schoolId}";
            public static string GetEventTypes(string baseUri) => $"{baseUri}/getEventType";
            public static string GetEventDuration(string baseUri) => $"{baseUri}/getEventDuration";
            public static string InsertPlanner(string baseUri) => $"{baseUri}/EventInsert";
            public static string UpdatePlanner(string baseUri) => $"{baseUri}/EventUpdate";
            public static string getEventByUserId(string baseUri, long id, DateTime fromDate, DateTime toDate, int? categoryId) => $"{baseUri}/getEventByUserId?Id={id}&fromDate={fromDate}&toDate={toDate}&categoryId={categoryId}";
            public static string getAllEventByUserId(string baseUri, long id, DateTime fromDate, DateTime toDate, string eventCategoryIds) => $"{baseUri}/getAllEventByUserId?Id={id}&fromDate={fromDate}&toDate={toDate}&eventCategoryIds={eventCategoryIds}";
            public static string getWeeklyTimeTableEvents(string baseUri, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher, bool isDashboardEvents) => $"{baseUri}/getWeeklyTimeTableEvents?userId={userId}&schoolId={schoolId}&fromDate={fromDate}&toDate={toDate}&isTeacher={isTeacher}&isDashboardEvents={isDashboardEvents}";
            public static string getTodayOrWeeklyTimeTableEvents(string baseUri, long userId, long schoolId, int fromWeekDay, int toWeekDay, DateTime fromDate, DateTime toDate, bool isTeacher, bool isWeeklyEvent) => $"{baseUri}/getTodayOrWeeklyTimeTableEvents?userId={userId}&schoolId={schoolId}&fromWeekDay={fromWeekDay}&toWeekDay={toWeekDay}&fromDate={fromDate}&toDate={toDate}&isTeacher={isTeacher}&isWeeklyEvent={isWeeklyEvent}";
            public static string getPlannerTimetableData(string baseUri, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher) => $"{baseUri}/getPlannerTimetableData?userId={userId}&schoolId={schoolId}&fromDate={fromDate}&toDate={toDate}&isTeacher={isTeacher}";
            public static string getPlannerTimetableDataWithPaging(string baseUri, int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher) => $"{baseUri}/getPlannerTimetableDataWithPaging?pageNumber={pageNumber}&pageSize={pageSize}&userId={userId}&schoolId={schoolId}&fromDate={fromDate}&toDate={toDate}&isTeacher={isTeacher}";
            public static string getTimetableDataWithPaging(string baseUri, int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher) => $"{baseUri}/getTimetableDataWithPaging?pageNumber={pageNumber}&pageSize={pageSize}&userId={userId}&schoolId={schoolId}&fromDate={fromDate}&toDate={toDate}&isTeacher={isTeacher}";
            public static string getOnlineMeetingEvents(string baseUri, int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isOnlineMeetingEvents) => $"{baseUri}/getOnlineMeetingEvents?pageNumber={pageNumber}&pageSize={pageSize}&userId={userId}&schoolId={schoolId}&fromDate={fromDate}&toDate={toDate}&isOnlineMeetingEvents={isOnlineMeetingEvents}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string getSchoolTimetableEvents(string baseUri, int pageNumber, int pageSize, long userId, long schoolId, DateTime fromDate, DateTime toDate, bool isTeacher) => $"{baseUri}/getSchoolTimetableEvents?pageNumber={pageNumber}&pageSize={pageSize}&userId={userId}&schoolId={schoolId}&fromDate={fromDate}&toDate={toDate}&isTeacher={isTeacher}";
            public static string getTimetableEventsReport(string baseUri) => $"{baseUri}/getTimetableEventsReport";
            public static string getLiveSessionsReport(string baseUri) => $"{baseUri}/getLiveSessionsReport";
            public static string acceptEventRequest(string baseUri, int eventId, long userId) => $"{baseUri}/acceptEventRequest?eventId={eventId}&userId={userId}";
            public static string acceptEventRequestExternalUser(string baseUri, int eventId, string emailId) => $"{baseUri}/acceptEventRequestExternalUser?eventId={eventId}&emailId={emailId}";
            public static string getEventById(string baseUri, long id, long? userId) => $"{baseUri}/getEvent?id={id}&userId={userId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";

            public static string getInternalEventUserByEventId(string baseUri, int eventId) => $"{baseUri}/getInternalEventUserByEventId?eventId={eventId}";
            public static string getExternalEventUserByEventId(string baseUri, int eventId) => $"{baseUri}/getExternalEventUserByEventId?eventId={eventId}";


        }
        public static class GradingTemplateItem
        {
            public static string GetGradingTemplateItems(string baseUri, int templateId, int SystemLanguageId) => $"{baseUri}/GetGradingTemplateItems?id={templateId}&SystemLanguageId={SystemLanguageId}";
            public static string GetGradingTemplateItemById(string baseUri, int id, int SystemLanguageId) => $"{baseUri}/GetGradingTemplateItemById?id={id}&SystemLanguageId={SystemLanguageId}";
            public static string InsertGradingTemplateItem(string baseUri) => $"{baseUri}/InsertGradingTemplateItem";
            public static string UpdateGradingTemplateItem(string baseUri) => $"{baseUri}/UpdateGradingTemplateItem";
            public static string DeleteGradingTemplateItem(string baseUri, int id) => $"{baseUri}/DeleteGradingTemplateItem/{id}";
        }

        public static class MarkingScheme
        {
            public static string GetMarkingSchemes(string baseUri, long schoolId) => $"{baseUri}/GetMarkingSchemas?schoolId={schoolId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string GetMarkingSchemeById(string baseUri, int id) => $"{baseUri}/GetMarkingSchemeById?id={id}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string InsertMarkingScheme(string baseUri) => $"{baseUri}/InsertMarkingScheme";
            public static string UpdateMarkingScheme(string baseUri) => $"{baseUri}/UpdateMarkingScheme";
            public static string DeleteMarkingScheme(string baseUri, int id) => $"{baseUri}/DeleteMarkingScheme/{id}";
        }


        #endregion

        #region Skill

        public static class SchoolSkillSet
        {
            public static string GetSchoolSkillSet(string baseUri, int languageId, int SchoolGradeId) => $"{baseUri}/getschoolskillset?languageId={languageId}&SchoolGradeId={SchoolGradeId}";
            public static string GetSchoolSkillSetById(string baseUri, int id) => $"{baseUri}/getschoolskillsetById?id={id}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string InsertSchoolSkillSet(string baseUri) => $"{baseUri}/insertSchoolSkillSet";
            public static string UpdateSchoolSkillSet(string baseUri) => $"{baseUri}/updateSchoolSkillSet";
            public static string DeleteSchoolSkillSet(string baseUri, int id) => $"{baseUri}/deleteSchoolSkillSet/{id}";
        }
        //PTM CategorySetFile
        public static class PTMCateorySet
        {
            //PTM CategorySetFile
            public static string GetCategorySet(string baseUri, int languageId, int SchoolGradeId) => $"{baseUri}/getptmcategory?languageId={languageId}&SchoolGradeId={SchoolGradeId}";
            public static string GetCategorySetById(string baseUri, int id) => $"{baseUri}/getptmcategoryById?id={id}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string InsertCategorySet(string baseUri) => $"{baseUri}/insertptmcategory";
            public static string UpdateCategorySet(string baseUri) => $"{baseUri}/updateptmcategory";
            public static string DeleteCategorySet(string baseUri, int id) => $"{baseUri}/deleteptmcategory/{id}";
        }
        //PTM CategorySetFile
        public static class PTMDeclineReasonSet
        {
            //PTM CategorySetFile
            public static string GetDeclineReasonSet(string baseUri, int languageId, int SchoolGradeId) => $"{baseUri}/getptmdeclinereason?languageId={languageId}&SchoolGradeId={SchoolGradeId}";
            public static string GetDeclineReasonSetById(string baseUri, int id) => $"{baseUri}/getptmdeclinereasonById?id={id}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string InsertDeclineReasonSet(string baseUri) => $"{baseUri}/insertptmdeclinereason";
            public static string UpdateDeclineReasonSet(string baseUri) => $"{baseUri}/updateptmdeclinereason";
            public static string DeleteDeclineReasonSet(string baseUri, int id) => $"{baseUri}/deleteptmdeclinereason/{id}";
        }
        #endregion

        #region Goal

        public static class SchoolGoal
        {
            public static string GetSchoolGoal(string baseUri, int languageId, int UserId, int IsTeacher) => $"{baseUri}/getschoolgoals?languageId={languageId}&UserId={UserId}&IsTeacher={IsTeacher}";
            public static string GetSchoolGoalById(string baseUri, int id) => $"{baseUri}/getschoolGoalById/{id}";
            public static string InsertSchoolGoal(string baseUri) => $"{baseUri}/insertSchoolGoal";
            public static string UpdateSchoolGoal(string baseUri) => $"{baseUri}/updateSchoolGoal";
            public static string DeleteSchoolGoal(string baseUri, int id) => $"{baseUri}/deleteSchoolGoal/{id}";
            public static string DeleteStudentGoal(string baseUri) => $"{baseUri}/DeleteStudentGoal";
            public static string GetStudentGoalById(string baseUri, long goalId) => $"{baseUri}/GetStudentGoalById?goalId={goalId}";
        }

        #endregion

        #region School Section

        public static class School
        {
            public static string GetSchoolList(string baseUri) => $"{baseUri}/getschoollist";
            public static string GetAdminSchoolList(string baseUri) => $"{baseUri}/getadminschoollist";
            public static string GetSchoolById(string baseUri, int id) => $"{baseUri}/getschoolbyid/{id}";
            public static string GetTeacherDashboard(string baseUri, int id) => $"{baseUri}/GetTeacherDashboard/{id}";
            public static string GetStudentForTeacher(string baseUri, long id, string ids = "") => $"{baseUri}/GetStudentForTeacher?id={id}&ids={ids}";
            public static string GetStudentForTeacherBySchoolGroup(string baseUri, int teacherId, int pageNumber, int pageSize, string schoolGroupIds, string searchString, string sortBy) => $"{baseUri}/GetStudentForTeacherBySchoolGroup?schoolGroupIds={schoolGroupIds}&pageNumber={pageNumber}&pageSize={pageSize}&searchString={searchString}&id={teacherId}&sortBy={sortBy}";
            public static string GetModuleFileDetails(string baseUri, int id, string filetype) => $"{baseUri}/GetModuleFileDetails?id={id}&filetype={filetype}";
            public static string GetReportDashboard(string baseUri, long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId) =>
                $"{baseUri}/GetReportDashboard?schoolId={schoolId}&startDate={startDate.ToString("dd MMM yyyy")}&endDate={endDate.ToString("dd MMM yyyy")}&userId={userId}&groupId={groupId}";
            public static string GetAssignmentReportData(string baseUri, long? schoolId, DateTime startDate, DateTime endDate, long? userId, long? groupId) =>
                $"{baseUri}/GetAssignmentReportData?schoolId={schoolId}&startDate={startDate.ToString("dd MMM yyyy")}&endDate={endDate.ToString("dd MMM yyyy")}&userId={userId}&groupId={groupId}";
            public static string InsertSchoolDefaultLanguage(string baseUri) => $"{baseUri}/InsertSchoolDefaultLanguage";
            public static string UpdateSchoolDefaultLangauge(string baseUri) => $"{baseUri}/UpdateSchoolDefaultLangauge";
            public static string GetAllLanguages(string baseUri) => $"{baseUri}/GetAllLanguages";
            public static string GetAllSchoolDefaultLanguage(string baseUri, long userId) => $"{baseUri}/GetAllSchoolDefaultLanguage?userId={userId}";
            public static string DeleteSchoolDefaultLanguageSettings(string baseUri) => $"{baseUri}/DeleteSchoolDefaultLanguageSettings";
            public static string GetChildQuizReportData(string baseUri, long? schoolId, DateTime startDate, DateTime endDate, long? userId) =>
                $"{baseUri}/getChildQuizReportData?schoolId={schoolId}&startDate={startDate.ToString("dd MMM yyyy")}&endDate={endDate.ToString("dd MMM yyyy")}&userId={userId}";
            public static string GetSchoolTeachersBySchoolId(string baseUri, long schoolId) => $"{baseUri}/getSchoolTeachersBySchoolId?schoolId={schoolId}";



            public static string GetQuizReportDataWithDateRange(string baseUri, long? userId, long? schoolId, DateTime startDate, DateTime endDate) =>
                $"{baseUri}/getQuizReportDataWithDateRange?userId={userId}&schoolId={schoolId}&startDate={startDate.ToString("dd MMM yyyy")}&endDate={endDate.ToString("dd MMM yyyy")}";
            public static string GetSchoolGroupAssignmentReport(string baseUri, long? schoolId, DateTime startDate, DateTime endDate, long? userId) =>
               $"{baseUri}/getSchoolGroupAssignmentReport?schoolId={schoolId}&startDate={startDate.ToString("dd MMM yyyy")}&endDate={endDate.ToString("dd MMM yyyy")}&userId={userId}";
            public static string GetGroupChatterReport(string baseUri, long? schoolId, DateTime startDate, DateTime endDate, long? userId) =>
               $"{baseUri}/getGroupChatterReport?schoolId={schoolId}&startDate={startDate.ToString("dd MMM yyyy")}&endDate={endDate.ToString("dd MMM yyyy")}&userId={userId}";
            public static string GetTeacherAssignmentReport(string baseUri, long? schoolId, DateTime startDate, DateTime endDate, long? userId) =>
               $"{baseUri}/getTeacherAssignmentReport?schoolId={schoolId}&startDate={startDate.ToString("dd MMM yyyy")}&endDate={endDate.ToString("dd MMM yyyy")}&userId={userId}";
            public static string GetStudentAssignmentReport(string baseUri, long? schoolId, DateTime startDate, DateTime endDate, long? userId) =>
               $"{baseUri}/getStudentAssignmentReport?schoolId={schoolId}&startDate={startDate.ToString("dd MMM yyyy")}&endDate={endDate.ToString("dd MMM yyyy")}&userId={userId}";
            public static string GetSchoolAbuseReport(string baseUri, long? schoolId, DateTime startDate, DateTime endDate, long? userId) =>
               $"{baseUri}/getSchoolAbuseReport?schoolId={schoolId}&startDate={startDate.ToString("dd MMM yyyy")}&endDate={endDate.ToString("dd MMM yyyy")}&userId={userId}";
            public static string GetReportDetails(string baseUri, DateTime startDate, DateTime endDate) =>
               $"{baseUri}/GetReportDetails?startDate={startDate}&endDate={endDate}";

            public static string GetAllBusinesUnitSchools(string baseUri, int pageIndex, int pageSize, string searchString) => $"{baseUri}/GetAllBusinesUnitSchools?pageIndex={pageIndex}&pageSize={pageSize}&searchString={searchString}";
            public static string UpdateSchoolClassroomStatus(string baseUri) => $"{baseUri}/UpdateSchoolClassroomStatus";

            public static string GetSchoolLoginReport(string baseUri, long userId, long schoolId, string startDate, string endDate) =>
                $"{baseUri}/GetSchoolLoginReport?userId={userId}&schoolId={schoolId}&startDate={startDate}&endDate={endDate}";

            public static string GetUserLiveSessionInfo(string baseUri) => $"{baseUri}/GenerateUserLiveSessionInfo";
            public static string GetSchoolSessionStatus(string baseUri, long schoolId) => $"{baseUri}/GetSchoolSessionStatus?schoolId={schoolId}";
            public static string SaveSchoolSessionStatus(string baseUri) => $"{baseUri}/SaveSchoolSessionStatus";
            #region FOR DEPARTMENT
            public static string DepartmentCourseCU(string baseUri) => $"{baseUri}/DepartmentCourseCU";

            public static string GetDepartmentCourse(string baseUri, long schoolId, long curriculumId, long? departmentId) =>
                $"{baseUri}/GetDepartmentCourse?schoolId={schoolId}&curriculumId={curriculumId}&departmentId={departmentId}";
            public static string CanDeleteDepartment(string baseUri, long departmentId) => $"{baseUri}/CanDeleteDepartment?departmentId={departmentId}";
            public static string GetStudentVaultReportData(string baseUri, long id, long schoolId) => $"{baseUri}/GetStudentVaultReportData?schoolId={schoolId}&teacherId={id}";
            #endregion

        }

        public static class ContactInfo
        {
            public static string GetContactInfos(string baseUri) => $"{baseUri}/getcontactinfos";
            public static string GetContactInfoById(string baseUri, int id) => $"{baseUri}/getcontactinfobyid/?id={id}";
            public static string GetContactInfoBySchoolId(string baseUri, int schoolId) => $"{baseUri}/getcontactinfobyschoolid/?schoolId={schoolId}";
            public static string AddContactInfo(string baseUri) => $"{baseUri}/addcontactinfo";
            public static string UpdateContactInfo(string baseUri) => $"{baseUri}/updatecontactinfo";
            public static string DeleteContactInfo(string baseUri, int id) => $"{baseUri}/deletecontactinfo/{id}";
        }

        public static class SchoolSpace
        {
            public static string GetSchoolSpaces(string baseUri) => $"{baseUri}/getschoolspaces";
            public static string GetSchoolSpaceById(string baseUri, int id) => $"{baseUri}/getschoolspacebyid/?id={id}";
            public static string GetSchoolSpaceBySchoolId(string baseUri, int schoolId) => $"{baseUri}/getschoolspacesbyschoolid/?schoolId={schoolId}";
            public static string AddSchoolSpace(string baseUri) => $"{baseUri}/addschoolspace";
            public static string UpdateSchoolSpace(string baseUri) => $"{baseUri}/updateschoolspace";
            public static string DeleteSchoolSpace(string baseUri, int id) => $"{baseUri}/deleteschoolspace/{id}";
        }


        public static class ExemplarWall
        {
            public static string AddExemplarWallModel(string baseUri) => $"{baseUri}/saveexemplarwall";
            public static string UpdateExemplarWallModel(string baseUri) => $"{baseUri}/UpdateExemplarWall";

            public static string ShareExemplarPost(string baseUri) => $"{baseUri}/ShareExemplarWall";
            public static string GetExemplarWallById(string baseUri, long id) => $"{baseUri}/getexemplardetailbyid/{id}";
            public static string DeleteExemplar(string baseUri) => $"{baseUri}/DeleteExemplarWall";

            public static string GetAllExemplarWallDetail(string baseUri, int schoolId) =>
               $"{baseUri}/GetExemplarWallDetailBySchoolId?schoolId={schoolId}";

            public static string GetBlogByGroupNBlogTypeId(string baseUri, int SchoolLevel, int Department, int CourseId, int SchoolId, int groupId, int blogTypeId,
                int? isPublish, Int64 UserId, Int64 CreatedBy, string SearchText, bool MyExemplarWork)
                => $"{baseUri}/getblogsbygroupnblogtypeid?SchoolLevel={SchoolLevel}" +
                $"&Department={Department}&CourseId={CourseId}&SchoolId={SchoolId}&groupId={groupId}&blogTypeId={blogTypeId}&isPublish={isPublish}" +
                $"&UserId={UserId}&CreatedBy={CreatedBy}&SearchText={SearchText}&MyExemplarWork={MyExemplarWork}";

            public static string GetExemplarWallList(string baseUri, int SchoolLevel, int Department, int CourseId, int SchoolId, int groupId, int blogTypeId,
                int? isPublish, Int64 UserId, Int64 CreatedBy, string SearchText, bool MyExemplarWork, int PageNumber, int PageSize)
                => $"{baseUri}/GetExemplarWallList?SchoolLevel={SchoolLevel}" +
                $"&Department={Department}&CourseId={CourseId}&SchoolId={SchoolId}&groupId={groupId}&blogTypeId={blogTypeId}&isPublish={isPublish}" +
                $"&UserId={UserId}&CreatedBy={CreatedBy}&SearchText={SearchText}&MyExemplarWork={MyExemplarWork}&PageNumber={PageNumber}" +
                $"&PageSize={PageSize}";

            public static string GetSchoolLevelBySchoolId(string baseUri, long schoolId)
                => $"{baseUri}/GetSchoolLevelBySchoolId?schoolId={schoolId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";

            public static string GetSchoolDepartment(string baseUri, long schoolId)
     => $"{baseUri}/GetSchoolDepartmentList?schoolId={schoolId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";

            public static string GetSchoolGroupBasedOnSchoolLevel(string baseUri, long LevelId, long UserId, long CourseId, long SchoolId)
               => $"{baseUri}/GetSchoolGroupBasedOnSchoolLevel?LevelId={LevelId}&UserId={UserId}&CourseId={CourseId}&SchoolId={SchoolId}";

            public static string GetPendingForApprovalExemplarPost(string baseUri, long schoolId)
                => $"{baseUri}/GetPendingForApprovalExemplarPost?schoolId={schoolId}";
            public static string UpdateExemplarPostStatusFromPendingToApprove(string baseUri, long ExemplarPostId)
               => $"{baseUri}/UpdateExemplarPostStatusFromPendingToApprove?ExemplarPostId={ExemplarPostId}";

            public static string GetApprovalStatusDetails(string baseUri, long SchoolId, string Opearion, string FlagType, bool Value)
               => $"{baseUri}/GetApprovalStatusDetails?SchoolId={SchoolId}&Opearion={Opearion}&FlagType={FlagType}&Value={Value}";

            public static string GetGroupsByUserId(string baseUri, long userId, bool isTeacher) => $"{baseUri}/getSchoolGroupsByUserId/{userId}/{isTeacher}";

            public static string GetCourseByDepartment(string baseUri, long DepartmentId)
                => $"{baseUri}/GetCourseByDepartment?DepartmentId={DepartmentId}";

        }
        public static class Blog
        {
            public static string GetAllBlogs(string baseUri) => $"{baseUri}/getblogs";
            public static string GetBlogById(string baseUri, int id) => $"{baseUri}/getblogbyid/{id}";
            public static string GetTeacherBlogs(string baseUri, int schoolId, int pageNumber, int pageSize) => $"{baseUri}/GetTeacherBlogs?schoolId={schoolId}&pageNumber={pageNumber}&pageSize={pageSize}";
            public static string GetBlogsBySchoolId(string baseUri, int schoolId, bool? isPublish) => $"{baseUri}/getblogsbyschoolid/{schoolId}/{isPublish}";
            public static string GetBlogsByCategoryId(string baseUri, int schoolId, int categoryId, bool? isPublish) => $"{baseUri}/getblogsbycategoryid/{schoolId}/{categoryId}/{isPublish}";
            public static string AddBlog(string baseUri) => $"{baseUri}/addblog";
            public static string MapGroupAndStudent(string baseUri) => $"{baseUri}/AddSchoolGroupAndStudentMapping";
            public static string DeleteExemplar(string baseUri) => $"{baseUri}/deleteExemplar";
            public static string UpdateBlog(string baseUri) => $"{baseUri}/updateblog";
            public static string DeleteBlog(string baseUri, int id, long userId) => $"{baseUri}/deleteblog/{id}/{userId}";
            public static string GetBlogCategories(string baseUri, long? schoolId) => $"{baseUri}/getblogcategories?schoolId={schoolId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string PublishBlogById(string baseUri, int blogId, bool isPublish, int publishBy) => $"{baseUri}/publishblogbyid/{blogId}/{isPublish}/{publishBy}";

            public static string GetBlogByGroupNBlogTypeId(string baseUri, int groupId, int blogTypeId, bool? isPublish) => $"{baseUri}/getblogsbygroupnblogtypeid/{groupId}/{blogTypeId}/{isPublish}";
            public static string GetBlogByGroupId(string baseUri, int groupId, int schoolId, int? userId, bool? isPublish) => $"{baseUri}/getblogsbygroupid?groupid={groupId}&schoolid={schoolId}&userid={userId}&ispublish={isPublish}";
            public static string GetBlogByBlogTypeId(string baseUri, int blogTypeId, bool? isPublish) => $"{baseUri}/getblogsbyblogtypeid/{blogTypeId}/{isPublish}";

            public static string GetBlogLikes(string baseUri, int? blogId, int? likedBy, bool? isLike, int? blogLikeId) => $"{baseUri}/getbloglikes/{blogId}/{likedBy}";
            public static string AddBlogLike(string baseUri) => $"{baseUri}/addbloglike";
            public static string UpdateBlogLike(string baseUri) => $"{baseUri}/updatebloglike";
            public static string DeleteBlogLike(string baseUri, int id) => $"{baseUri}/deletebloglike/{id}";
            public static string GetBlogTypes(string baseUri, long? schoolId, short languageId) => $"{baseUri}/getblogtypes?schoolId={schoolId}&languageId={languageId}";
            public static string UpdateWallSortOrder(string baseUri) => $"{baseUri}/UpdateWallSortOrder";
            public static string GetChatterWithComment(string baseUri, int? blogId, int schoolId, int? categoryId, int groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId, int? publishBy, bool? isCommentPublish) => $"{baseUri}/getChatterWithComments?blogId={blogId}&schoolid={schoolId}&categroyId={categoryId}&groupid={groupId}&userid={userId}&isTeacher={isTeacher}&ispublish={isPublish}&commentId={commentId}&publishBy={publishBy}&isCommentPublish={isCommentPublish}";
            public static string GetUserExemplerWallData(string baseUri, long userId, int pageIndex, int pageSize) => $"{baseUri}/GetUserExemplerWallData?userId={userId}&pageIndex={pageIndex}&pageSize={pageSize}";

            public static string ShareBlogWithGroups(string baseUri) => $"{baseUri}/shareblogs";
            public static string GetWallWithComment(string baseUri, int? blogId, int schoolId, int? categoryId, int? groupId, int? userId, bool isTeacher, bool? isPublish, int? commentId, int? publishBy, bool? isCommentPublish, int? LogedInUserId, int? PageNum) => $"{baseUri}/getmywalldata?blogId={blogId}&schoolid={schoolId}&categroyId={categoryId}&groupid={groupId}&userid={userId}&isTeacher={isTeacher}&ispublish={isPublish}&commentId={commentId}&publishBy={publishBy}&isCommentPublish={isCommentPublish}&logedInUserId={LogedInUserId}&pageNum={PageNum}";
            public static string GetSearchInBlog(string baseUri, long schoolId, string searchString, long userId, bool? isTeacher) => $"{baseUri}/GetSearchOnMyWall?schoolId={schoolId}&searchString={searchString}&userId={userId}&isTeacher={isTeacher}";
            public static string GetGroupBySearchInBlog(string baseUri, long schoolId, string searchString, long userId, bool? isTeacher, long groupId) => $"{baseUri}/GetSearchBlogsOnMyGroup?schoolId={schoolId}&searchString={searchString}&userId={userId}&isTeacher={isTeacher}&groupId={groupId}";

            public static string GetWallWithCommentOnSearch(string baseUri, long? blogId, long schoolId, int? categoryId, int? blogTypeId, long? groupId, long? userId, bool isTeacher, bool? isPublish, int? commentId, int? publishBy, bool? isCommentPublish, int? LogedInUserId, int? pageNum) => $"{baseUri}/getmywalldataonsearch?blogId={blogId}&schoolid={schoolId}&categroyId={categoryId}&blogtypeid={blogTypeId}&groupid={groupId}&userid={userId}&isTeacher={isTeacher}&ispublish={isPublish}&commentId={commentId}&publishBy={publishBy}&isCommentPublish={isCommentPublish}&logedInUserId={LogedInUserId}&pageNum={pageNum}";

            public static string GetStudentChatterBlogs(string baseUri, long schoolId, long userId) => $"{baseUri}/getStudentChatter?schoolId={schoolId}&userId={userId}";

            public static string GetTeacherChatterBlogs(string baseUri, long schoolId, long userId) => $"{baseUri}/GetTeacherChatter?schoolId={schoolId}&userId={userId}";
            public static string ViewPostCommentUserList(string baseUri, long BlogId, long CommentSectioId, bool IsCommentDetailDisplay, long UserId) => $"{baseUri}/GetPostAndCommentLikeUserList?BlogId={BlogId}&CommentSectioId={CommentSectioId}&UserId={UserId}&IsCommentDetailDisplay={IsCommentDetailDisplay}";

        }
        public static class Chat
        {
            public static string GetChatHistoryByUserId(string baseUri, int schoolId, int userId, int otheruser, int groupId, int pageNum) => $"{baseUri}/getchathistorybyuserid/{schoolId}/{userId}/{otheruser}/{groupId}/{pageNum}";
            public static string GetRecentChatUserList(string baseUri, int schoolId, int userId) => $"{baseUri}/getrecentChatuserlist/{schoolId}/{userId}";

            public static string GetRecentUserActivity(string baseUri, int schoolId, int userId, bool enableChat, long? groupId = 0) => $"{baseUri}/getrecentuseractivity/{schoolId}/{userId}/{enableChat}?{nameof(groupId)}={groupId}";

            public static string GetSearchInChat(string baseUri, int schoolId, int userId, int userTypeId, string searchText) => $"{baseUri}/getsearchinchat?schoolId={schoolId}&userId={userId}&userTypeId={userTypeId}&text={searchText}";
            public static string GetMyContactsList(string baseUri, int schoolId, int userId) => $"{baseUri}/getmycontactslist/{schoolId}/{userId}";

            public static string GetLatestGroup(string baseUri) => $"{baseUri}/getlatestinviteGroup";
            public static string GetGroupMemberList(string baseUri, int groupId, int schoolId) => $"{baseUri}/getgroupchatusers?groupid={groupId}&schoolid={schoolId}";
            public static string UpdateInviteChatByUserId(string baseUri) => $"{baseUri}/inviteuserforchat";
            public static string UpdateInviteChatGroupName(string baseUri) => $"{baseUri}/updateinvitegroupname";
            public static string GetGroupName(string baseUri, int groupId) => $"{baseUri}/getinviteGroupName?groupid={groupId}";
            public static string DeleteChatConversation(string baseUri, long schoolId, long groupId, long fromUser, long toUser) => $"{baseUri}/deletechatconversation?schoolId={schoolId}&groupId={groupId}&fromUser={fromUser}&toUser={toUser}";

        }
        public static class ChatterPermission
        {
            public static string GetChatPermmissionByUser(string baseUri, int schoolId, int userId) => $"{baseUri}/getUserPermissions/{schoolId}/{userId}";

        }

        public static class Chatter
        {
            public static string UpdateChatterPermission(string baseUri) => $"{baseUri}/UpdateChatterPermission";
            public static string GetUsersInGroup(string baseUri, int groupId) => $"{baseUri}/GetUsersInGroup/{groupId}";

        }


        public static class BlogComment
        {
            public static string GetAllBlogComments(string baseUri) => $"{baseUri}/getblogcomments";
            public static string GetBlogCommentById(string baseUri, int id) => $"{baseUri}/getblogcommentbyid/{id}";
            public static string GetBlogCommentsByBlogId(string baseUri, int blogId, bool? isPublish) => $"{baseUri}/getblogcommentsbyblogid/{blogId}/{isPublish}";
            public static string GetBlogCommentsByPublishBy(string baseUri, int publishById, bool? isPublish) => $"{baseUri}/getblogcommentbypublishby/{publishById}/{isPublish}";
            public static string AddBlogComment(string baseUri) => $"{baseUri}/addblogcomment";
            public static string UpdateBlogComment(string baseUri) => $"{baseUri}/updateblogcomment";
            public static string DeleteBlogComment(string baseUri, int id, long userId) => $"{baseUri}/deleteblogcomment/{id}/{userId}";
            public static string UpdateDiscloseDate(string baseUri, int id) => $"{baseUri}/UpdateDiscloseDate/{id}";
            public static string GetBlogCommentLikes(string baseUri, int? commentId, int? blogCommentLikeId, int? likedBy, bool? isLike) => $"{baseUri}/getblogcommentlikes/{commentId}/{likedBy}";
            public static string AddBlogCommentLike(string baseUri) => $"{baseUri}/addblogcommentlike";
            public static string UpdateBlogCommentLike(string baseUri) => $"{baseUri}/updateblogcommentlike";
            public static string DeleteBlogCommentLike(string baseUri, int id) => $"{baseUri}/deleteblogcommentlike/{id}";
        }

        #endregion

        #region Master Section
        public static class Country
        {
            public static string GetCountries(string baseUri) => $"{baseUri}/getcountries";
            public static string GetCountryById(string baseUri, int id) => $"{baseUri}/getcountrybyid/{id}";
        }
        #endregion

        #region Incident Management
        public static class IncidentCategories
        {
            public static string GetIncidentCategories(string baseUri, string roles) => $"{baseUri}/getincidentcategories?roles={roles}";
            public static string GetIncidentCategoryById(string baseUri, int id) => $"{baseUri}/getincidentcategorybyid/{id}";
            public static string InsertIncidentCategory(string baseUri) => $"{baseUri}/insertincidentcategory";
            public static string UpdateIncidentCategory(string baseUri) => $"{baseUri}/updateincidentcategory";
            public static string DeleteIncidentCategory(string baseUri, int id) => $"{baseUri}/deleteincidentcategory/{id}";
            public static string GetCategoryRoles(string baseUri, int id) => $"{baseUri}/getcategoryroles/{id}";
            public static string SetCategoryRoles(string baseUri) => $"{baseUri}/setcategoryroles";
            public static string IsDuplicateCategory(string baseUri) => $"{baseUri}/isduplicatecategory";
        }

        public static class IncidentSubCategories
        {
            public static string GetIncidentSubCategories(string baseUri, int id) => $"{baseUri}/getsubcategories?id={id}";
            public static string GetIncidentSubCategoryById(string baseUri, int id) => $"{baseUri}/getsubcategotybyid/{id}";
            public static string InsertIncidentSubCategory(string baseUri) => $"{baseUri}/insertincidentsubcategory";
            public static string UpdateIncidentSubCategory(string baseUri) => $"{baseUri}/updateincidentsubcategory";
            public static string DeleteIncidentSubCategory(string baseUri, int id) => $"{baseUri}/deleteincidentsubcategory/{id}";
            public static string IsDuplicateSubCategoryInOneCategory(string baseUri) => $"{baseUri}/isduplicatesubcategoryinonecategory";
        }

        public static class IncidentCountry
        {
            public static string GetIncidentCounries(string baseUri, int userId) => $"{baseUri}/getincidentcountries?userId={userId}";
            public static string GetIncidentCountryById(string baseUri, int id) => $"{baseUri}/getincidentcountriesbyid/{id}";
        }

        public static class IncidentCity
        {
            public static string GetIncidentCities(string baseUri, int userId, int countryId) => $"{baseUri}/getincidentcities?userId={userId}&coutryId={countryId}";
            public static string GetIncidentCityById(string baseUri, int id) => $"{baseUri}/getincidentcitybyid/{id}";
        }

        public static class IncidentBusinessUnit
        {
            public static string GetIncidentBusinessUnits(string baseUri, int cityId, int userId) => $"{baseUri}/getincidentbusinessunits?cityId={cityId}&userId={userId}";
            public static string GetIncidentBusinessUnitById(string baseUri, int id) => $"{baseUri}/getincidentbusinessunitbyid/{id}";
        }

        public static class IncidentSection
        {
            public static string GetIncidentSections(string baseUri) => $"{baseUri}/getincidentsections";
            public static string GetIncidentSectionById(string baseUri, int id) => $"{baseUri}/getincidentsectionbyid/{id}";
            public static string InsertIncidentSection(string baseUri) => $"{baseUri}/insertincidentsection";
            public static string UpdateIncidentSection(string baseUri) => $"{baseUri}/updateincidentsection";
            public static string DeleteIncidentSection(string baseUri, int id) => $"{baseUri}/deleteincidentsection/{id}";
            public static string IsDuplicateSection(string baseUri) => $"{baseUri}/isduplicatesection";
        }

        public static class IncidentRegistration
        {
            public static string GetIncidentRegistrations(string baseUri, int userId) => $"{baseUri}/getincidentRegistrations?userId={userId}";
            public static string GetIncidentRegistrationByIds(string baseUri, int userId, string incidentIds) => $"{baseUri}/getincidentRegistrationByIds?userId={userId}&ids={incidentIds}";
            public static string GetIncidentRegistrationById(string baseUri, int id) => $"{baseUri}/incidentregistrationbyid/{id}";
            public static string InsertIncidentRegistration(string baseUri) => $"{baseUri}/insertincidentregistration";
            public static string UpdateIncidentRegistration(string baseUri) => $"{baseUri}/updateincidentregistration";
            public static string DeleteIncidentRegistration(string baseUri, int id) => $"{baseUri}/deleteincidentregistration/{id}";
            public static string GetIncidentUserLocations(string baseUri, int userId) => $"{baseUri}/getuserlocationfilter?userId={userId}";
        }

        public static class IncidentInvestigation
        {
            //Investigation
            public static string GetAllInvestigation(string baseUri) => $"{baseUri}/getinvestigation";
            public static string GetAllInvestigationByIncident(string baseUri, long id) => $"{baseUri}/getinvestigationbyincidentid/{id}";
            public static string GetInvestigationById(string baseUri, int id) => $"{baseUri}/getinvestigationbyid/{id}";
            public static string InsertIncidentInvestigation(string baseUri) => $"{baseUri}/insertinvestigation";
            public static string UpdateIncidentInvestigation(string baseUri) => $"{baseUri}/updateinvestigation";
            public static string UpdateInvestigationAttachment(string baseUri) => $"{baseUri}/updateinvestigationattachments";
            //public static string DeleteIncidentRegistration(string baseUri, int id) => $"{baseUri}/deleteincidentregistration/{id}";
            //Investigator
            public static string GetInvestigatorById(string baseUri, long id) => $"{baseUri}/getinvestigatorbyid/{id}";
            public static string InsertIncidentInvestigator(string baseUri) => $"{baseUri}/insertinvestigator";
            public static string UpdateIncidentInvestigator(string baseUri) => $"{baseUri}/updateinvestigator";
            public static string DeleteIncidentInvestigator(string baseUri, int id, long userId) => $"{baseUri}/deleteinvestigator?id={id}&userId={userId}";

            //CauseOfIncident
            public static string GetCauseOfIncidentById(string baseUri, int id) => $"{baseUri}/getcauseofincidentbyid/{id}";
            public static string InsertCauseOfIncident(string baseUri) => $"{baseUri}/insertcauseofincident";
            public static string UpdateCauseOfIncident(string baseUri) => $"{baseUri}/updatecauseofincident";
        }

        public static class IncidentEntity
        {
            public static string GetEntities(string baseUri, int incidentId) => $"{baseUri}/getentities?incidentId={incidentId}";
            public static string GetEntityById(string baseUri, int id) => $"{baseUri}/getentitybyid/{id}";
            public static string InsertEntity(string baseUri) => $"{baseUri}/insertentity";
            public static string UpdateEntity(string baseUri) => $"{baseUri}/updateentity";
            public static string DeleteEntity(string baseUri, int id) => $"{baseUri}/deleteentity/{id}";
        }

        public static class EntityInjury
        {
            public static string GetEntityInjury(string baseUri, int entityId, int entityInjuryId) => $"{baseUri}/getentityinjury?entityId={entityId}&entityInjuryId={entityInjuryId}";
            public static string InsertEntityInjury(string baseUri) => $"{baseUri}/insertentityinjury";
            public static string UpdateEntityInjury(string baseUri) => $"{baseUri}/updateentityinjury";
            public static string DeleteEntityInjury(string baseUri, int id) => $"{baseUri}/deleteentityinjury/{id}";
        }

        public static class EntityTreatment
        {
            public static string GetEntityTreatment(string baseUri, int entityId, int treatmentId) => $"{baseUri}/getentitytreatment?entityId={entityId}&treatmentId={treatmentId}";
            public static string InsertEntityTreatment(string baseUri) => $"{baseUri}/insertentitytreatment";
            public static string UpdateEntityTreatment(string baseUri) => $"{baseUri}/updateentitytreatment";
            public static string DeleteEntityTreatment(string baseUri, int id) => $"{baseUri}/deleteentitytreatment/{id}";
        }

        public static class IncidentProperty
        {
            public static string GetProperties(string baseUri, int incidentId) => $"{baseUri}/getincidentproperties?id={incidentId}";
            public static string InsertProperty(string baseUri) => $"{baseUri}/insertincidentproperty";
            public static string UpdateProperty(string baseUri) => $"{baseUri}/updateincidentproperty";
            public static string DeleteProperty(string baseUri, int id) => $"{baseUri}/deleteincidentcategory/{id}";
        }

        public static class IncidentVehicle
        {
            public static string GetVehicles(string baseUri, int incidentId) => $"{baseUri}/getincidentvehicle?id={incidentId}";
            public static string InsertVehicle(string baseUri) => $"{baseUri}/insertincidentvehicle";
            public static string UpdateVehicle(string baseUri) => $"{baseUri}/updateincidentvehicle";
            public static string DeleteVehicle(string baseUri, int id) => $"{baseUri}/deleteincidentvehicle/{id}";
        }

        public static class IncidentAssailant
        {
            public static string GetAssailants(string baseUri, int incidentId) => $"{baseUri}/getincidentassailant?incidentId={incidentId}";

            public static string GetAssailantById(string baseUri, int id) => $"{baseUri}/getassailantbyid/{id}";
            public static string InsertAssailant(string baseUri) => $"{baseUri}/insertincidentassailant";
            public static string UpdateAssailant(string baseUri) => $"{baseUri}/updateincidentassailant";
            public static string DeleteAssailant(string baseUri, int id) => $"{baseUri}/deleteincidentassailant/{id}";
        }

        public static class IncidentWitness
        {
            public static string GetWitnesses(string baseUri, int incidentId) => $"{baseUri}/getincidentwitnesses?incidentId={incidentId}";

            public static string GetWitnessById(string baseUri, int id) => $"{baseUri}/getincidentwitnessbyid/{id}";

            public static string InsertWitness(string baseUri) => $"{baseUri}/insertincidentwitness";
            public static string UpdateWitness(string baseUri) => $"{baseUri}/updateincidentwitness";
            public static string DeleteWitness(string baseUri, int id) => $"{baseUri}/deleteincidentwitness/{id}";
        }

        public static class IncidentAgecy
        {
            public static string GetAgencies(string baseUri, int incidentId) => $"{baseUri}/getincidentagencies?incidentId={incidentId}";

            public static string GetAgencyById(string baseUri, int id) => $"{baseUri}/getincidentagencybyId/{id}";

            public static string InsertAgency(string baseUri) => $"{baseUri}/insertincidentagency";
            public static string UpdateAgency(string baseUri) => $"{baseUri}/updateincidentagency";
            public static string DeleteAgency(string baseUri, int id) => $"{baseUri}/deleteincidentagency/{id}";
        }

        public static class AgencyType
        {
            public static string GetAgencyTypes(string baseUri) => $"{baseUri}/getagencytypes";

            public static string GetAgencyTypeById(string baseUri, int id) => $"{baseUri}/agencytypebyid/{id}";

            public static string InsertAgencyType(string baseUri) => $"{baseUri}/insertagencytype";
            public static string UpdateAgencyType(string baseUri) => $"{baseUri}/updateagencytype";
            public static string DeleteAgencyType(string baseUri, int id) => $"{baseUri}/deleteagencytype/{id}";
        }

        public static class IncidentClosureCongiguration
        {
            public static string GetClosureConfigurations(string baseUri) => $"{baseUri}/getincidentclosureconfigs";

            public static string GetClosureConfigurationById(string baseUri, int id) => $"{baseUri}/getincidentclosureconfigbyid/{id}";

            public static string InsertClosureConfiguration(string baseUri) => $"{baseUri}/insertrincidentclosureconfig";
            public static string UpdateClosureConfiguration(string baseUri) => $"{baseUri}/updateincidentclosureconfig";
            public static string DeleteClosureConfiguration(string baseUri, int id) => $"{baseUri}/deleteincidentclosureconfig/{id}";
        }

        #endregion

        #region Reminder
        public static class ReminderRegistration
        {
            public static string GetReminderRegistrations(string baseUri) => $"{baseUri}/getreminderregistrations";
            public static string GetReminderRegistrationById(string baseUri, int id) => $"{baseUri}/getreminderregistrationbyid/{id}";
            public static string InsertReminderRegistration(string baseUri) => $"{baseUri}/insertreminderregistration";
            public static string UpdateReminderRegistration(string baseUri) => $"{baseUri}/updatereminderregistration";
            public static string DeleteReminderRegistration(string baseUri, int id) => $"{baseUri}/deletereminderregistration/{id}";
        }

        public static class CustomReminder
        {
            public static string GetReminderByProfile(string baseUri) => $"{baseUri}/getcustomreminderbyprofile";
            public static string GetReminderById(string baseUri, int id) => $"{baseUri}/getcunstomreminderbyid/{id}";
            public static string InsertDefaultReminder(string baseUri) => $"{baseUri}/insertdefaultreminder";
            public static string InsertReminder(string baseUri) => $"{baseUri}/insertcustomreminder";
            public static string UpdateReminder(string baseUri) => $"{baseUri}/updatecustomreminder";
            public static string DeleteReminder(string baseUri, int id) => $"{baseUri}/deletecustomreminder/{id}";
        }
        #endregion

        #region Notification
        public static class NotificatonProfile
        {
            public static string GetNotificatonProfiles(string baseUri) => $"{baseUri}/getnotificationprofiles";
            public static string GetNotificatonProfileById(string baseUri, int id) => $"{baseUri}/getnotificationprofilebyid/{id}";
            public static string InsertNotificatonProfile(string baseUri) => $"{baseUri}/insertnotificationprofile";
            public static string UpdateNotificatonProfile(string baseUri) => $"{baseUri}/updatenotificationprofile";
            public static string DeleteNotificatonProfile(string baseUri, int id) => $"{baseUri}/deletenotificationprofile/{id}";
        }

        public static class IncidentMatrix
        {
            public static string GetIncidentMatrix(string baseUri, int notificationProfileId) => $"{baseUri}/getincidentmatrix?notificationProfileId={notificationProfileId}";
            public static string GetIncidentMatrixById(string baseUri, int id) => $"{baseUri}/incidentmatrixbyid/{id}";
            public static string InsertIncidentMatrix(string baseUri) => $"{baseUri}/insertincidentmatrix";
            public static string UpdateIncidentMatrix(string baseUri) => $"{baseUri}/updateincidentmatrix";
            public static string DeleteIncidentMatrix(string baseUri, int id) => $"{baseUri}/deleteincidentmatrix/{id}";
            public static string DeleteAllIncidentMatrix(string baseUri, int id) => $"{baseUri}/deleteallincidentmatrixbyprofileId/{id}";
            public static string InsertMultipleIncidentMatrix(string baseUri) => $"{baseUri}/insertmultipleincidentmatrix";
        }

        public static class AuditMatrix
        {
            public static string GetMatrix(string baseUri, int notificationProfileId) => $"{baseUri}/getauditmatrix?notificationProfileId={notificationProfileId}";
            public static string InsertMultipleMatrix(string baseUri) => $"{baseUri}/insertmultipleauditmatrix";
        }
        #endregion

        #region Task Details

        public static class TaskDetails
        {
            public static string GetTaskDetails(string baseUri, int taskid) => $"{baseUri}/gettask";
            public static string GetTaskDetailById(string baseUri, int id) => $"{baseUri}/task/{id}";
            public static string InsertTask(string baseUri) => $"{baseUri}/task";
            public static string UpdateTask(string baseUri) => $"{baseUri}/updatetaskdetail";
            public static string DeleteTask(string baseUri, int id) => $"{baseUri}/{id}";

            public static string CompleteTask(string baseUri, int id) => $"{baseUri}/completetask/{id}";
            public static string GetTaskDetailsByUser(string baseUri, int userid) => $"{baseUri}/gettaskbyUser/{userid}";
            public static string UpdateTaskAttachment(string baseUri) => $"{baseUri}/updatetaskattachment";
            public static string GetTaskByFilter(string baseUri) => $"{baseUri}/gettaskbyfilter";
        }

        #endregion

        #region Task Log

        public static class TaskLog
        {

            public static string GetTaskLogAssignById(string baseUri, int id) => $"{baseUri}/assigntask/{id}";
            public static string GetTaskLogs(string baseUri, int taskid) => $"{baseUri}/gettasklog";

            public static string checkTaskLog(string baseUri) => $"{baseUri}/checktasklog";
            public static string GetTaskLogByUser(string baseUri, int taskid, int Userid) => $"{baseUri}/gettasklogbyUser/{taskid}/{Userid}";
            public static string GetTaskLogById(string baseUri, int id) => $"{baseUri}/tasklog/{id}";

            public static string InsertTaskLog(string baseUri) => $"{baseUri}/createtasklog";
            public static string UpdateTaskLog(string baseUri) => $"{baseUri}/updatetasklog";
            public static string DeleteTaskLog(string baseUri, int id) => $"{baseUri}/{id}";
        }

        #endregion

        #region Task Report

        public static class TaskReport
        {

            public static string GetTaskReport(string baseUri) => $"{baseUri}/gettaskreport";
            public static string GetTaskStatusReport(string baseUri) => $"{baseUri}/gettaskstatusreport";

        }

        #endregion

        #region Themes
        public static class Themes
        {
            public static string GetAllSchoolThemes(string baseUri, int? schoolId, short languageId) => $"{baseUri}/getallschoolthemesbyschoolid?schoolId={schoolId}&languageId={languageId}";
            public static string InsertSchoolTheme(string baseUri) => $"{baseUri}/insertschoolthemes";
            public static string UpdateSchoolTheme(string baseUri) => $"{baseUri}/updateschoolthemes";
            public static string GetSchoolThemeById(string baseUri, int themeId) => $"{baseUri}/getschoolthemebyid?themeId={themeId}";
            public static string GetAllSchoolThemes(string baseUri, int schoolId, int? curriculumId, short languageId) => $"{baseUri}/GetAllSchoolThemesByCurriculum?schoolId={schoolId}&curriculumId={curriculumId}&languageId={languageId}";
            public static string GetStudentGradeTheme(string baseUri, long userId) => $"{baseUri}/GetStudentGradeTheme?userId={userId}";
        }
        #endregion Themes

        #region Users
        public static class UserRole
        {
            public static string GetUserRoles(string baseUri) => $"{baseUri}/getuserroles";
            public static string GetAllUserRoleMappingData(string baseUri, int userid) => $"{baseUri}/GetAllUserRoleMappingData?userid={userid}";
            public static string GetUserRoleById(string baseUri, int id) => $"{baseUri}/getuserrolebyid?id={id}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string InsertUserRole(string baseUri) => $"{baseUri}/insertuserrole";
            public static string UpdateUserRole(string baseUri) => $"{baseUri}/updateuserrole";
            public static string DeleteUserRole(string baseUri, int id) => $"{baseUri}/deleteuserrole/{id}";
            public static string GetModuleList(string baseUri, int systemlanguageid, string modulecode) => $"{baseUri}/GetModuleList?systemlanguageid={systemlanguageid}&modulecode={modulecode}";
            public static string GetModuleStructureList(string baseUri, int systemlanguageid, int? moduleid, string modulecode) => $"{baseUri}/GetModuleStructureList?systemlanguageid={systemlanguageid}&moduleid={moduleid}&modulecode={modulecode}";
            public static string GetAllPermissionData(string baseUri, int userroleid, int userid, int moduleid, bool loadcustomepermission, int schoolId) => $"{baseUri}/GetAllPermissionData?userroleid={userroleid}&userid={userid}&moduleid={moduleid}&loadcustomepermission={loadcustomepermission}&schoolId={schoolId}";
            public static string UpdatePermissionTypeDataCUD(string baseUri) => $"{baseUri}/UpdatePermissionTypeDataCUD";
            public static string CheckUserRoleMapping(string baseUri, long? userId, short? roleId) => $"{baseUri}/CheckUserRoleMapping?userid={userId}&roleid={roleId}";
            public static string InsertUserRoleMappingData(string baseUri, long userId, short roleId) => $"{baseUri}/InsertUserRoleMappingData?userid={userId}&roleid={roleId}";
            public static string DeleteUserRoleMappingData(string baseUri, long userId, short roleId) => $"{baseUri}/DeleteUserRoleMappingData?userid={userId}&roleid={roleId}";
            public static string GetUserRolesBySchoolId(string baseUri, int schoolId) => $"{baseUri}/getUserRolesbyschoolid?schoolId={schoolId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string GetUsersForRole(string baseUri, int roleId, int schoolId) => $"{baseUri}/getusersbyrole/{roleId}/{schoolId}";
        }

        public static class LogInUser
        {
            public static string GetLoginUserByUserName(string baseUri, string userName, string ipDetails) => $"{baseUri}/getloginuserbyusername/?userName={userName}&ipDetails={ipDetails}";
            public static string GetUserList(string baseUri, int schoolId) => $"{baseUri}/getuserlist?schoolId={schoolId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string GetLoginUserByUserNamePassword(string baseUri, string userName, string password, string ipDetails) => $"{baseUri}/getloginuserbyusernamepassword/?userName={userName}&password={password}&ipDetails={ipDetails}";
        }

        public static class TemplateLoginUser
        {
            public static string IsValidUser(string baseUri, string userName, string Password) => $"{baseUri}/IsValidUser?userName={userName}&Password={Password}";
            public static string ResetPassword(string baseUri, string userName, string Password) => $"{baseUri}/ResetPassword?userName={userName}&Password={Password}";



        }
        public static class TemplateUpload
        {
            public static string StudentTemplateBulkImport(string baseUri, long UserId) => $"{baseUri}/studenttemplatebulkimport?UserId={UserId}";
            public static string StaffTemplateBulkImport(string baseUri, long UserId) => $"{baseUri}/StaffTemplateBulkImport?UserId={UserId}";
            public static string GradeWiseSubjectTemplateBulkImport(string baseUri, long UserId) => $"{baseUri}/GradeWiseSubjectTemplateBulkImport?UserId={UserId}";
            public static string StudentWiseSubjectTemplateBulkImport(string baseUri, long UserId) => $"{baseUri}/StudentWiseSubjectTemplateBulkImport?UserId={UserId}";
        }

        public static class Users
        {
            public static string GetAllUsers(string baseUri) => $"{baseUri}/getallusers/";
            public static string GetUserFeelings(string baseUri, int SystemLanguageId) => $"{baseUri}/getalluserfeelings?SystemLanguageId={SystemLanguageId}";
            public static string GetProfileAvatars(string baseUri) => $"{baseUri}/getprofileavatars/";
            public static string UpdateUserFeeling(string baseUri) => $"{baseUri}/updateuserfeeling";
            public static string UpdateUserProfile(string baseUri) => $"{baseUri}/updateuserprofile";
            public static string GetUsersBySchoolAndType(string baseUri, long? schoolId, int userTypeId) => $"{baseUri}/getUsersBySchoolAndType?id={schoolId}&userTypeId={userTypeId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string GetUserBySchool(string baseUri, long? schoolId, int userTypeId) => $"{baseUri}/getusersbyschool?id={schoolId}&userTypeId={userTypeId}";
            public static string GetUserNotifications(string baseUri, int userTypeId, long? userId, long? loginUserId) => $"{baseUri}/getusernotifications?userTypeId={userTypeId}&userId={userId}&loginUserId={loginUserId}";
            public static string GetAllNotifications(string baseUri, int userTypeId, long? userId, long? loginUserId) => $"{baseUri}/getallnotifications?userTypeId={userTypeId}&userId={userId}&loginUserId={loginUserId}";

            public static string PushNotificationLogs(string baseUri, string notificationType, int sourceId, long userId) => $"{baseUri}/pushnotificationlogs?notificationType={notificationType}&sourceId={sourceId}&userId={userId}";

            public static string GetUserById(string baseUri, long UserId) => $"{baseUri}/getuserbyid?id={UserId}";
            public static string SearchUserByName(string baseUri, string name, int typeId = 0, long schoolId = 0) => $"{baseUri}/searchuserbyname?name={name}&typeId={typeId}&schoolId={schoolId}";
            public static string GetUsersByRolesLocatonAllowed(string baseUri, string roles) => $"{baseUri}/getusersbyrolelocation?roles={roles}";
            public static string SendErrorLog(string baseUri) => $"{baseUri}/saveErrorLogger";
            public static string GetDBLogDetails(string baseUri) => $"{baseUri}/GetDBLogdetails";
            public static string GetUserLog(string baseUri, long? schoolId, DateTime startDate, DateTime endDate, string loginType) => $"{baseUri}/GetUserLog?schoolId={schoolId}&startDate={startDate}&endDate={endDate}&loginType={loginType}";
            // public static string GetErrorLogs(string baseUri) => $"{baseUri}/GetErrorLogs";
            public static string GetErrorLogs(string baseUri, long? schoolId, DateTime startDate, DateTime endDate, string loginType) => $"{baseUri}/GetErrorLogs?schoolId={schoolId}&startDate={startDate}&endDate={endDate}&loginType={loginType}";

            public static string GetErrorLogFiles(string baseUri, long ErrorLogId) => $"{baseUri}/GetErrorLogfiles?ErrorLogId={ErrorLogId}";
            public static string GetTeachersListBySchoolId(string baseUri, long schoolId) => $"{baseUri}/GetTeachersListBySchoolId?schoolId={schoolId}";
            public static string InsertLogDetails(string baseUri) => $"{baseUri}/InsertLogDetails";
            public static string SaveUserFeedback(string baseUri) => $"{baseUri}/SaveUserFeedback";
        }

        public static class UserPermission
        {
            public static string CheckUserPermission(string baseUri, long userId, string moduleUrl, int userTypeId)
                => $"{baseUri}/checkuserpermission?userId={userId}&moduleUrl={moduleUrl}&userTypeId={userTypeId}";
            public static string IsCustomPermissionAssigned(string baseUri, int userId, string permissionCodes)
                => $"{baseUri}/IsCustomPermissionAssigned?userId={userId}&permissionCodes={permissionCodes}";

            public static string GetUserPermission(string baseUri, long userId, string moduleUrl, string moduleCode, int userTypeId)
               => $"{baseUri}/getuserpermission?userId={userId}&moduleUrl={moduleUrl}&moduleCode={moduleCode}&userTypeId={userTypeId}";

            public static string AddMenuItem(string baseUri) => $"{baseUri}/addmenuitem";
        }

        public static class UserLocationMap
        {
            public static string GetUserLocationMapsByUserId(string baseUri, int id) => $"{baseUri}/getmapsbyuserid/{id}";
            public static string InsertUserLocationMaps(string baseUri) => $"{baseUri}/insertuserlocationmaps";


        }
        #endregion


        #region Assignment
        public static class Assignment
        {
            public static string GetAssignments(string baseUri, int teacherId) => $"{baseUri}/GetAssignments?teacherId={teacherId}";
            public static string GetStudentAssignmentByUserId(string baseUri, int pagenumber, int pagesize, long userId, string SearchString) => $"{baseUri}/GetStudentAssignmentByUserId?pageNumber={pagenumber}&pageSize={pagesize}&userId={userId}&searchString={SearchString}";
            public static string GetAssignmentByStudentId(string baseUri, int pagenumber, int pagesize, long studentId, string SearchString, string assignmentType, string sortBy) => $"{baseUri}/GetAssignmentByStudentId?PageNumber={pagenumber}&PageSize={pagesize}&studentId={studentId}&SearchString={SearchString}&assignmentType={assignmentType}&sortBy={sortBy}";
            public static string GetArchivedAssignmentByStudentId(string baseUri, int pagenumber, int pagesize, long studentId, string SearchString, string assignmentType, string sortBy) => $"{baseUri}/GetArchivedAssignmentByStudentId?PageNumber={pagenumber}&PageSize={pagesize}&studentId={studentId}&SearchString={SearchString}&assignmentType={assignmentType}&sortBy={sortBy}";
            public static string GeStudentFilterSchoolGroupsById(string baseUri, int pagenumber, int pagesize, long studentId, string SearchString, string assignmentType, string sortBy) => $"{baseUri}/GeStudentFilterSchoolGroupsById?PageNumber={pagenumber}&PageSize={pagesize}&studentId={studentId}&SearchString={SearchString}&assignmentType={assignmentType}&sortBy={sortBy}";
            public static string GetArchivedStudentFilterSchoolGroupsById(string baseUri, int pagenumber, int pagesize, long studentId, string SearchString, string assignmentType, string sortBy) => $"{baseUri}/GetArchivedStudentFilterSchoolGroupsById?PageNumber={pagenumber}&PageSize={pagesize}&studentId={studentId}&SearchString={SearchString}&assignmentType={assignmentType}&sortBy={sortBy}";
            public static string GetAssignmentByStudentIdWithoutPagination(string baseUri, long studentId, string assignmentType) => $"{baseUri}/GetAssignmentByStudentIdWithoutPagination?studentId={studentId}&assignmentType={assignmentType}";
            public static string GetAssignmentByStudentIdWithDateRange(string baseUri, int pagenumber, int pagesize, long studentId, DateTime startDate, DateTime endDate, string SearchString, string assignmentType) => $"{baseUri}/getAssignmentByStudentIdWithDateRange?PageNumber={pagenumber}&PageSize={pagesize}&studentId={studentId}&startDate={startDate}&endDate={endDate}&SearchString={SearchString}&assignmentType={assignmentType}";
            public static string GetAssignmentByStudentIdAndTeacherIdWithDateRange(string baseUri, int pagenumber, int pagesize, long studentId, long teacherId, DateTime startDate, DateTime endDate, string SearchString, string assignmentType) => $"{baseUri}/getAssignmentByStudentIdAndTeacherIdWithDateRange?PageNumber={pagenumber}&PageSize={pagesize}&studentId={studentId}&teacherId={teacherId}&startDate={startDate}&endDate={endDate}&SearchString={SearchString}&assignmentType={assignmentType}";
            public static string GetAssignmentTaskQuizByAssignmentId(string baseUri, int assignmentId) => $"{baseUri}/GetAssignmentTaskQuizByAssignmentId?assignmentId={assignmentId}";
            public static string GetAssignmentsForMyPlanner(string baseUri, long studentId, DateTime startDate, DateTime endDate, string assignmentType) => $"{baseUri}/getAssignmentsForMyPlanner?studentId={studentId}&startDate={startDate}&endDate={endDate}&assignmentType={assignmentType}";
            public static string GetAssignmentListForReport(string baseUri, int pageNumber, int pageSize, string searchString, long userId, long schoolId, int schoolGroupId, DateTime startDate, DateTime endDate, string userType) => $"{baseUri}/GetAssignmentListForReport?pageNumber={pageNumber}&pageSize={pageSize}&searchString={searchString}&userId={userId}&schoolId={schoolId}&schoolGroupId={schoolGroupId}&startDate={startDate}&endDate={endDate}&userType={userType}";
            public static string GetAssignmentById(string baseUri, int id) => $"{baseUri}/GetAssignmentById/{id}";
            public static string GetAssignmentSubmitMarksByAssignmentStudentId(string baseUri, int assignmentStudentId) => $"{baseUri}/GetAssignmentSubmitMarksByAssignmentStudentId/{assignmentStudentId}";
            public static string GetTaskSubmitMarksByTaskId(string baseUri, int taskId) => $"{baseUri}/GetTaskSubmitMarksByTaskId/{taskId}";
            public static string GetAssignmentCategoryMasterById(string baseUri, int id) => $"{baseUri}/GetAssignmentCategoryMasterById/{id}";
            public static string GetAssignmentCategoryBySchoolId(string baseUri, long SchoolId) => $"{baseUri}/GetAssignmentCategoryBySchoolId?SchoolId={SchoolId}";
            public static string GetExcelReportDataAssignmentById(string baseUri, int id) => $"{baseUri}/GetExcelReportDataAssignmentById/{id}";
            public static string InsertAssignment(string baseUri) => $"{baseUri}/InsertAssignment";
            public static string SaveAssignmentDetails(string baseUri) => $"{baseUri}/SaveAssignmentDetails";
            public static string UpdateAssignment(string baseUri) => $"{baseUri}/UpdateAssignment";
            public static string DeleteAssignment(string baseUri) => $"{baseUri}/DeleteAssignment";
            public static string DeleteAssignmentComment(string baseUri) => $"{baseUri}/DeleteAssignmentComment";
            public static string GetAssignmentStudent(string baseUri) => $"{baseUri}/GetAssignmentStudent";
            public static string GetAssignmentStudentDetails(string baseUri, long TeacherId) => $"{baseUri}/GetAssignmentStudentDetails?TeacherId={TeacherId}";
            public static string UndoAssignmentsArchive(string baseUri) => $"{baseUri}/UndoAssignmentsArchive";
            public static string GetAdminAssignmentStudentDetails(string baseUri) => $"{baseUri}/GetAdminAssignmentStudentDetails";
            public static string GetFilterSchoolGroupsById(string baseUri, long TeacherId) => $"{baseUri}/GetFilterSchoolGroupsById?TeacherId={TeacherId}";
            public static string GetArchivedFilterSchoolGroupsById(string baseUri, long TeacherId) => $"{baseUri}/GetArchivedFilterSchoolGroupsById?TeacherId={TeacherId}";
            public static string GetAssignmentStudentDetailsById(string baseUri, int Id) => $"{baseUri}/GetAssignmentStudentDetailsById/{Id}";
            public static string GetTasksByAssignmentId(string baseUri, int Id) => $"{baseUri}/GetTasksByAssignmentId/{Id}";
            public static string AssignGradeToStudentAssignment(string baseUri, int studentAssignmentId, int gradingId, int GradedBy) => $"{baseUri}/AssignGradeToStudentAssignment?studentAssignmentId=" + studentAssignmentId + "&gradingTemplateItemId=" + gradingId + "&GradedBy=" + GradedBy;
            public static string GetAssignmentPermissions(string baseUri, int assignmentId, long userId) => $"{baseUri}/GetAssignmentPermissions?assignmentId=" + assignmentId + "&userId=" + userId;
            public static string AssignmentUnArchive(string baseUri, int assignmentId) => $"{baseUri}/AssignmentUnArchive?assignmentId=" + assignmentId;
            public static string AddEditAssignmentCategory(string baseUri, int AssignmentCategoryId, bool IsActive, long SchoolId) => $"{baseUri}/AddEditAssignmentCategory?AssignmentCategoryId=" + AssignmentCategoryId + "&IsActive=" + IsActive + "&SchoolId=" + SchoolId;
            public static string AddAssignmentCategoryMaster(string baseUri, string AssignmentCategoryTitle, string AssignmentCategoryDesc, int CategoryId, bool IsActive, long SchoolId) => $"{baseUri}/AddAssignmentCategoryMaster?AssignmentCategoryTitle=" + AssignmentCategoryTitle + "&AssignmentCategoryDesc=" + AssignmentCategoryDesc + "&CategoryId=" + CategoryId + "&IsActive=" + IsActive + "&SchoolId=" + SchoolId;
            public static string SeenStudentAssignmentComment(string baseUri) => $"{baseUri}/SeenStudentAssignmentComment";
            public static string getAssignmentFilesByAssignmentId(string baseUri, int Id) => $"{baseUri}/GetAssignmentFilesByAssignmentId?assignmentId={Id}";
            public static string SaveAssignmentFiles(string baseUri) => $"{baseUri}/SaveAssignmentFiles";
            public static string getFilesByTaskId(string baseUri, int Id) => $"{baseUri}/GetTaskFilesByTaskId?taskId={Id}";
            public static string GetStudentAsgHomeworkDetailsById(string baseUri, int Id, int IsSeenByStudent = 0) => $"{baseUri}/GetStudentAsgHomeworkDetailsById?assignmentStudentId={Id}&IsSeenByStudent={IsSeenByStudent}";
            public static string GetStudentAssignmentDetailsById(string baseUri, int Id) => $"{baseUri}/GetStudentAssignmentDetailsById?assignmentStudentId={Id}";
            public static string GetAssignmentTaskbyTaskId(string baseUri, int Id) => $"{baseUri}/GetAssignmentTaskbyTaskId?taskId={Id}";
            public static string GetSchoolGroupsByAssignmentId(string baseUri, int Id) => $"{baseUri}/GetSelectegGroupsByAssignmentId?assignmentId={Id}";
            public static string GetSelectegCoursesByAssignmentId(string baseUri, int Id) => $"{baseUri}/GetSelectegCoursesByAssignmentId?assignmentId={Id}";
            public static string GetStudentsByAssignmentId(string baseUri, int Id, int groupId) => $"{baseUri}/GetStudentsByAssignmentId?assignmentId={Id}&GroupId={groupId}";
            public static string GetAssignmentCompletedStudentsByAssignmentId(string baseUri, int Id) => $"{baseUri}/GetAssignmentCompletedStudentsByAssignmentId?assignmentId={Id}";
            public static string GetExcelReportDataStudentsByAssignmentId(string baseUri, int Id) => $"{baseUri}/GetExcelReportDataStudentsByAssignmentId?assignmentId={Id}";
            public static string MarkAsComplete(string baseUri, int assignmentStudentId, bool isTeacher, int CompletedBy) => $"{baseUri}/MarkAsComplete?assignmentStudentId={assignmentStudentId}&isTeacher={isTeacher}&CompletedBy={CompletedBy}";
            public static string SubmitAssignmentMarks(string baseUri, int assignmentStudentId, decimal submitMarks, int CompletedBy, int gradingTemplateId, int SystemLanguageId) => $"{baseUri}/SubmitAssignmentMarks?assignmentStudentId={assignmentStudentId}&submitMarks={submitMarks}&CompletedBy={CompletedBy}&gradingTemplateId={gradingTemplateId}&SystemLanguageId={SystemLanguageId}";
            public static string UploadStudentAssignmeentFiles(string baseUri, int studentId, int assignmentId, string IpDetails) => $"{baseUri}/UploadStudentAssignmentFiles?studentId={studentId}&assignmentId={assignmentId}&IpDetails={IpDetails}";
            public static string GetStudentAssignmentFiles(string baseUri, int studentId, int assignmentId) => $"{baseUri}/GetStudentAssignmentFiles?studentId={studentId}&assignmentId={assignmentId}";
            public static string GetSavedAssignmentFiles(string baseUri, int assignmentId, long userId) => $"{baseUri}/GetSavedAssignmentFilesByAssignmentId?assignmentId={assignmentId}&userId={userId}";
            public static string GetAssignmentFeedbackFiles(string baseUri, int assignmentStudentId, int assignmentCommentId) => $"{baseUri}/GetAssignmentFeedbackFiles?assignmentStudentId={assignmentStudentId}&assignmentCommentId={assignmentCommentId}";
            public static string MarkAsIncomplete(string baseUri, int studentId, int assignmentId, bool isReSubmitAssignnment) => $"{baseUri}/MarkAsIncomplete?studentId={studentId}&assignmentId={assignmentId}&isReSubmitAssignnment={isReSubmitAssignnment}";

            public static string UploadStudentTaskFiles(string baseUri, int studentId, int taskId, string IpDetails) => $"{baseUri}/UploadStudentTaskFiles?studentId={studentId}&taskId={taskId}&IpDetails={IpDetails}";
            public static string GetStudentTaskFiles(string baseUri, int studentId, int taskid, int UserType = 0) => $"{baseUri}/GetStudentTaskFiles?studentId={studentId}&taskId={taskid}&UserType={UserType}";
            public static string GetStudentTaskdetails(string baseUri, int taskId, int studentId) => $"{baseUri}/GetStudentTaskdetails?taskId={taskId}&studentId={studentId}";
            public static string GetStudentTaskDetailById(string baseUri, int taskId, int studentId, int AssignmentStudentId, long StudentTaskId) => $"{baseUri}/GetStudentTaskDetailById?taskId={taskId}&studentId={studentId}&assignmentStudentId={AssignmentStudentId}&studentTaskId={StudentTaskId}";
            public static string MarkAsCompleteTask(string baseUri, long studentTaskId, bool isByteacher, int CompletedBy) => $"{baseUri}/MarkAsCompleteTask?studentTaskId={studentTaskId}&isByTeacher={isByteacher}&CompletedBy={CompletedBy}";
            public static string SubmitTaskMarks(string baseUri, long studentTaskId, int StudentAssignmentId, decimal submitMarks, int CompletedBy, int gradingTemplateId, int languageId) => $"{baseUri}/SubmitTaskMarks?studentTaskId={studentTaskId}&StudentAssignmentId={StudentAssignmentId}&submitMarks={submitMarks}&CompletedBy={CompletedBy}&gradingTemplateId={gradingTemplateId}&languageId={languageId}";
            public static string AssignGradeToStudentTask(string baseUri, int studentTaskId, int gradingTemplateItemId, int GradedBy) => $"{baseUri}/AssignGradeToStudentTask?studentTaskId={studentTaskId}&gradingTemplateItemId={gradingTemplateItemId}&GradedBy={GradedBy}";
            public static string AssignGradeToStudentObjective(string baseUri, int studentObjectiveId, int gradingTemplateItemId) => $"{baseUri}/AssignGradeToStudentObjective?studentObjectiveId={studentObjectiveId}&gradingTemplateItemId={gradingTemplateItemId}";
            public static string InsertAssignmentComment(string baseUri) => $"{baseUri}/InsertAssignmentComment";
            public static string GetAssignmentComments(string baseUri, int assignmentStudentId) => $"{baseUri}/GetAssignmentComments?assignmentStudentId={assignmentStudentId}";
            public static string GetMyFiles(string baseUri, long userId) => $"{baseUri}/GetMyFiles?userId={userId}";
            public static string InsertTaskFeedback(string baseUri) => $"{baseUri}/InsertTaskFeedback";
            public static string InsertStudentObjectiveFeedback(string baseUri) => $"{baseUri}/InsertStudentObjectiveFeedback";
            public static string GetStudentAssignmentObjectiveAudiofeedback(string baseUri, int StudentObjectiveMarkId) => $"{baseUri}/GetStudentAssignmentObjectiveAudiofeedback?StudentObjectiveMarkId=" + StudentObjectiveMarkId;
            public static string GetAssignmentMyFiles(string baseUri, long fileId, bool isFolder) => $"{baseUri}/GetAssignmentMyFile?fileId={fileId}&isFolder={isFolder}";
            public static string GetAssignmentFilebyAssignmentFileId(string baseUri, long id) => $"{baseUri}/GetAssignmentFilebyAssignmentFileId?id={id}";
            public static string GetStudentTaskFile(string baseUri, long id) => $"{baseUri}/GetStudentTaskFile?id={id}";
            public static string GetTaskFile(string baseUri, long id) => $"{baseUri}/GetTaskFile?id={id}";
            public static string GetStudentAssignmentFilebyStudentAsgFileId(string baseUri, long id) => $"{baseUri}/GetStudentAssignmentFilebyStudentAsgFileId?id={id}";
            public static string DeleteUploadedFiles(string baseUri, int FileId, int AssignmentFileId, long UserId) => $"{baseUri}/DeleteUploadedFiles?FileId={FileId}&AssignmentFileId={AssignmentFileId}&UserId={UserId}";
            public static string DeleteStudentAssignmentFile(string baseUri, int FileId, long UserId, bool IsTaskFile) => $"{baseUri}/DeleteStudentAssignmentFile?FileId={FileId}&UserId={UserId}&IsTaskFile={IsTaskFile}";
            public static string UpdateActiveAssignment(string baseUri, long assignmentId) => $"{baseUri}/UpdateActiveAssignment?assignmentId={assignmentId}";
            public static string GetArchivedAssignmentStudentDetails(string baseUri, long TeacherId) => $"{baseUri}/GetArchivedAssignmentStudentDetails?TeacherId={TeacherId}";
            public static string GetAdminArchivedAssignmentStudentDetails(string baseUri) => $"{baseUri}/GetAdminArchivedAssignmentStudentDetails";
            public static string AddUpdatePeermarking(string baseUri) => $"{baseUri}/AddUpdatePeermarking";
            public static string GetPeerAssignmentDetails(string baseUri, long assignmentId, long userId) => $"{baseUri}/GetPeerAssignmentDetails?assignmentId={assignmentId}&userId={userId}";
            public static string GetPeerMappingDetails(string baseUri, long assignmentId) => $"{baseUri}/GetPeerMappingDetails?assignmentId={assignmentId}";
            public static string AddUpdateDocumentReviewDetails(string baseUri, long userId) => $"{baseUri}/AddUpdateDocumentReviewDetails?userId={userId}";
            public static string GetReviewedDocumentFiles(string baseUri, long peerReviewId) => $"{baseUri}/GetReviewedDocumentFiles?peerReviewId={peerReviewId}";
            public static string GetDocumentFileByStudAsgFileId(string baseUri, long stdAsgFileId) => $"{baseUri}/GetDocumentFileByStudAsgFileId?stdAsgFileId={stdAsgFileId}";
            public static string MarkAsCompleteReview(string baseUri, long peerReviewId, long reviewedBy) => $"{baseUri}/MarkAsCompleteReview?peerReviewId={peerReviewId}&reviewedBy={reviewedBy}";
            public static string GetPeerAssignmentDetail(string baseUri, long peerReviewId) => $"{baseUri}/GetPeerAssignmentDetail?peerReviewId={peerReviewId}";
            public static string GetPeerAssignmentDetailByAssignmentStudentId(string baseUri, long assignmentStudentId) => $"{baseUri}/GetPeerAssignmentDetailByAssignmentStudentId?assinmentStudentId={assignmentStudentId}";
            public static string AddUpdateTaskDocumentReviewDetails(string baseUri, long userId) => $"{baseUri}/AddUpdateTaskDocumentReviewDetails?userId={userId}";
            public static string GetPeerTaskDetail(string baseUri, long studentId, long taskId) => $"{baseUri}/GetPeerTaskDetail?studentId={studentId}&taskId={taskId}";
            public static string GetPeerTaskDetails(string baseUri, long assignmentId, long studentId) => $"{baseUri}/GetPeerTaskDetails?assignmentId={assignmentId}&studentId={studentId}";
            public static string GetTaskDocumentFileByStudTaskFileId(string baseUri, long studentTaskFileId) => $"{baseUri}/GetTaskDocumentFileByStudTaskFileId?stdTaskFileId={studentTaskFileId}";
            public static string GetTaskReviewedDocumentFiles(string baseUri, long taskPeerReviewId) => $"{baseUri}/GetTaskReviewedDocumentFiles?taskPeerReviewId={taskPeerReviewId}";
            public static string MarkAsCompleteTaskReview(string baseUri, long peerReviewId, long reviewedBy) => $"{baseUri}/MarkAsCompleteTaskReview?peerReviewId={peerReviewId}&reviewedBy={reviewedBy}";
            public static string GetSchoolGroupAssignment(string baseUri, long schoolGroupId) => $"{baseUri}/GetGroupAssignments?SchoolGroupId={schoolGroupId}";
            public static string GetStudentGroupAssignments(string baseUri, long schoolGroupId, long UserId) => $"{baseUri}/GetStudentGroupAssignments?SchoolGroupId={schoolGroupId}&UserId={UserId}";
            public static string GetDashboardAssignmentOverview(string baseUri, long userId) => $"{baseUri}/GetDashboardAssignmentOverview?userId={userId}";
            public static string GetStudentAssignmentObjectives(string baseUri, long assignmentStudentId) => $"{baseUri}/GetStudentAssignmentObjectives?assignmentStudentId={assignmentStudentId}";
            public static string UploadStudentSharedCopyFiles(string baseUri) => $"{baseUri}/UploadStudentSharedCopyFiles";
            public static string ShareAssignment(string baseUri, int assignmentId, string teacherIds) => $"{baseUri}/ShareAssignment?assignmentId={assignmentId}&teacherIds={teacherIds}";
            public static string GetQuizQuestionsByTaskId(string baseUri, int id) => $"{baseUri}/GetQuizQuestionsByTaskId?taskId={id}";
            public static string GetAssignmentQuizDetailsByTaskId(string baseUri, int taskId) => $"{baseUri}/GetAssignmentQuizDetailsByTaskId?taskId={taskId}";
        }
        #endregion

        #region Observation
        public static class Observation
        {
            public static string InsertObservation(string baseUri) => $"{baseUri}/InsertObservation";
            public static string UpdateObservation(string baseUri) => $"{baseUri}/UpdateObservation";
            public static string DeleteObservation(string baseUri) => $"{baseUri}/DeleteObservation";
            public static string GetObservationById(string baseUri, int id, long userId) => $"{baseUri}/GetObservationById/{id}/{userId}";
            public static string getObservationFilesById(string baseUri, int Id) => $"{baseUri}/GetObservationFilesById?observationId={Id}";
            public static string GetObservationStudentById(string baseUri, int Id) => $"{baseUri}/GetObservationStudent?observationId={Id}";
            public static string GetObservationObjectives(string baseUri, int observationId) => $"{baseUri}/GetObservationObjectives?observationId={observationId}";
            public static string GetSchoolGroupsByObservationId(string baseUri, int Id) => $"{baseUri}/GetSelectegGroupsByObservationId?observationId={Id}";
            public static string GetObservationStudent(string baseUri, int Id) => $"{baseUri}/GetObservationStudent?observationId={Id}";
            public static string GetObservationMyFiles(string baseUri, long fileId, bool isFolder) => $"{baseUri}/GetObservationMyFile?fileId={fileId}&isFolder={isFolder}";
            public static string GetObservationTeacherPaging(string baseUri, int pageNumber, int pageSize, long teacherId, string searchString, string schoolGroupIds, string sortBy) => $"{baseUri}/GetObservationTeacherPaging?PageNumber={pageNumber}&PageSize={pageSize}&TeacherId={teacherId}&searchString={searchString}&schoolGroupIds={schoolGroupIds}&sortBy={sortBy}";
            public static string GetObservationStudentPaging(string baseUri, int PageNumber, int PageSize, long studentId, string searchString, string sortBy) => $"{baseUri}/GetObservationStudentPaging?PageNumber={PageNumber}&PageSize={PageSize}&studentId={studentId}&searchString={searchString}&sortBy={sortBy}";
            public static string DeleteObservationFile(string baseUri, int fileId, int observationId, long userId) => $"{baseUri}/DeleteObservationFile?FileId={fileId}&ObservationId={observationId}&UserId={userId}";
            public static string GetObservationSubjects(string baseUri, int Id) => $"{baseUri}/GetObservationSubjects?observationId={Id}";
            public static string GetObservationFilebyObservationFileId(string baseUri, long Id) => $"{baseUri}/GetObservationFilebyObservationFileId?id={Id}";
            public static string GetArchivedObservationTeacherPaging(string baseUri, int pageNumber, int pageSize, long teacherId, string searchString) => $"{baseUri}/GetArchivedObservationTeacherPaging?PageNumber={pageNumber}&PageSize={pageSize}&TeacherId={teacherId}&searchString={searchString}";
            public static string GetObservations(string baseUri, int teacherId) => $"{baseUri}/GetObservationsByUserId?teacherId={teacherId}";
            public static string UpdateActiveObservation(string baseUri, long observationId) => $"{baseUri}/UpdateActiveObservation?observationId={observationId}";
            public static string GetSchoolCoursesByObservation(string baseUri, int Id) => $"{baseUri}/GetSchoolCoursesByObservation?observationId={Id}";
        }

        #endregion

        #region Document Management
        public static class Document
        {
            public static string GetDocumentById(string baseUri, int id) => $"{baseUri}/getdocumentbyid/{id}";
            public static string InsertDocument(string baseUri) => $"{baseUri}/insertdocument";
            public static string GetDocumentListByIds(string baseUri, string ids) => $"{baseUri}/getdocumentlistbyids?Ids={ids}";
            public static string GetDocumentListByCatId(string baseUri, int id) => $"{baseUri}/getdocumentlistbycatid?id={id}";
            public static string DeleteDocument(string baseUri, int id) => $"{baseUri}/deletedocument/{id}";
        }
        public static class Catagory
        {
            public static string GetCatagoryByBU(string baseUri, int parentId, string bUIds, long userId) => $"{baseUri}/getcatagorybybu?parentId={parentId}&bUIds={bUIds}&userId={userId}";
            public static string InsertCatagory(string baseUri) => $"{baseUri}/insertcatagory";
            public static string DeleteCatagory(string baseUri, int id) => $"{baseUri}/deletecatagory/{id}";
        }
        public static class CatagoryBUMap
        {
            public static string GetCatagoryBUMapListByCatId(string baseUri, int id) => $"{baseUri}/getcatagorybumapbycatid/{id}";
            public static string InsertCatagoryBUMap(string baseUri) => $"{baseUri}/insertcatagorybumap";
        }
        #endregion

        #region SafetyAudit

        public static class Audit
        {

            public static string GetAllAudits(string baseUri) => $"{baseUri}/getaudit";
            public static string GetAuditById(string baseUri, int id) => $"{baseUri}/getaudit/{id}";
            public static string InsertAudit(string baseUri) => $"{baseUri}/insertaudit";
            public static string UpdateAudit(string baseUri) => $"{baseUri}/updateaudit";
            public static string DeleteAudit(string baseUri, int id) => $"{baseUri}/deleteaudit/{id}";
            public static string GetAuditSeverityMapByAudit(string baseUri, long id) => $"{baseUri}/getauditseveritymapbyaudit/{id}";

        }
        public static class AuditAmmend
        {

            public static string GetAuditAmmendByAudit(string baseUri, long id) => $"{baseUri}/getauditammendbyaudit/{id}";
            public static string GetAuditAmmendById(string baseUri, int id) => $"{baseUri}/auditammend/{id}";
            public static string UpdateAuditAmmendData(string baseUri) => $"{baseUri}/insertauditammend";
            public static string DeleteAuditAmmend(string baseUri, int id) => $"{baseUri}/deleteauditammend/{id}";
            public static string ApplyAuditAmmendData(string baseUri) => $"{baseUri}/applyauditammend";

        }
        public static class AuditQuestionAnswer
        {

            public static string GetAuditQuestionAnswerByAuditSection(string baseUri, long id) => $"{baseUri}/getauditquestionanswerbyauditsection/{id}";
            public static string GetAuditQuestionAnswerById(string baseUri, int id) => $"{baseUri}/auditquestionanswer/{id}";
            public static string UpdateAuditQuestionAnswerData(string baseUri) => $"{baseUri}/insertauditquestionanswer";
            public static string DeleteAuditQuestionAnswerAmmend(string baseUri, int id) => $"{baseUri}/deleteauditquestionanswer/{id}";
            public static string AuditQuestionAnswerBulkImport(string baseUri) => $"{baseUri}/auditquestionanswerbulkimport";
            public static string IsDuplicateQuestion(string baseUri) => $"{baseUri}/isduplicatequestion";

        }
        public static class AuditLocationMap
        {

            public static string GetAuditLocationMapById(string baseUri, int id) => $"{baseUri}/auditlocationmap/{id}";

            public static string GetAuditLocationMapBySchedule(string baseUri, long id) => $"{baseUri}/getauditlocationmapbyschedule/{id}";
            public static string UpdateAuditLocationMapData(string baseUri) => $"{baseUri}/insertauditlocationmap";

        }
        public static class Question
        {

            public static string GetAllQuestions(string baseUri) => $"{baseUri}/getquestion";
            public static string GetQuestionById(string baseUri, int id) => $"{baseUri}/question/{id}";
            public static string InsertQuestion(string baseUri) => $"{baseUri}/insertquestion";
            public static string UpdateQuestion(string baseUri) => $"{baseUri}/updatequestion";
            public static string DeleteQuestion(string baseUri, int id) => $"{baseUri}/deletequestion/{id}";
            public static string IsDuplicateQuestion(string baseUri) => $"{baseUri}/isduplicatequestion";
            public static string GetQuestionIdByNameAndAuditAmmendID(string baseUri) => $"{baseUri}/getquestionidbynameandauditammendid";

        }
        public static class AuditSection
        {

            public static string GetAllAuditSection(string baseUri) => $"{baseUri}/getauditsection";
            public static string GetAuditSectionById(string baseUri, int id) => $"{baseUri}/auditsection/{id}";
            public static string InsertAuditSection(string baseUri) => $"{baseUri}/insertauditsection";
            public static string DeleteAuditSection(string baseUri, int id) => $"{baseUri}/deleteauditsection/{id}";
            public static string GetAuditSectionByAuditAmmend(string baseUri, long id) => $"{baseUri}/getauditsectionbyauditammend/{id}";
            public static string GetAuditSectionIdByNameAndAuditAmmendID(string baseUri) => $"{baseUri}/getauditsectionidbynameandauditammendid";

        }
        public static class AuditSchedule
        {

            public static string GetAuditSchdeuleByAudit(string baseUri, long id) => $"{baseUri}/getauditschedulebyaudit/{id}";
            public static string GetAuditSchdeuleById(string baseUri, int id) => $"{baseUri}/auditschedule/{id}";
            public static string UpdateAuditSchdeuleData(string baseUri) => $"{baseUri}/insertauditschedule";
            public static string DeleteAuditSchdeule(string baseUri, int id) => $"{baseUri}/deleteauditschedule/{id}";
            public static string GetAuditScheduleDetailsBySchedule(string baseUri, int id) => $"{baseUri}/getauditscheduledetailsbyschedule/{id}";
            public static string IsDuplicateAuditSchedule(string baseUri) => $"{baseUri}/isduplicateauditschedule";


        }
        public static class Inspection
        {
            public static string GetInspectionById(string baseUri, int id) => $"{baseUri}/inspection/{id}";
            public static string GetInspectionByBU(string baseUri, string ids, bool IsAdHoc) => $"{baseUri}/getinspectionbybu?Ids={ids}&IsAdHoc={IsAdHoc}";
            public static string UpdateInspectionAssigneeData(string baseUri) => $"{baseUri}/insertinspectionassignee";
            public static string MarkInspectionAsComplete(string baseUri) => $"{baseUri}/markinspectionascomplete";
            public static string GetBusinessUnitsByUser(string baseUri, long userId) => $"{baseUri}/getbusinessunitsbyuser?userId={userId}";
            public static string InspectionAddDocuments(string baseUri) => $"{baseUri}/insertinspectiondocument";
            public static string UpdateAdHocAuditData(string baseUri) => $"{baseUri}/insertauditadhoc";
            public static string GetAdHocBySchedule(string baseUri, int id) => $"{baseUri}/inspectionadhoc/{id}";



        }
        public static class ControlDetails
        {
            public static string GetControlDetailsByControlProfile(string baseUri, int id) => $"{baseUri}/getcontroldetailsbycontrolprofile/{id}";

        }

        public static class ControlProfile
        {
            public static string GetControlProfileIdByName(string baseUri) => $"{baseUri}/getcontrolprofileidbyname";

        }
        public static class InspectionValues
        {
            public static string GetInspectionValuesByInspection(string baseUri, int id) => $"{baseUri}/getinspectionvaluesbyinspection/{id}";
            public static string UpdateInspectionValuesData(string baseUri) => $"{baseUri}/insertinspectionvalues";
            public static string AddTaskData(string baseUri) => $"{baseUri}/addtaskdata";
            public static string AddNoteDate(string baseUri) => $"{baseUri}/addnotedata";
        }

        #endregion

        #region SIMS

        public static class TimeTable
        {
            public static string GetGrades(string baseUri) => $"{baseUri}/getGrades";
            public static string GetGradesByUser(string baseUri, string username) => $"{baseUri}/getGradesByUser?username={username}";
            public static string GetSectionsByGrade(string baseUri, int gradeId) => $"{baseUri}/GetSectionsByGrade?gradeId={gradeId}";
            public static string GetCurrentMonth(string baseUri) => $"{baseUri}/GetCurrentMonth";
            public static string GetTimeTablesByUserandDate(string baseUri, string username, DateTime dateTime, string type, string grade = null, string section = null) =>
                $"{baseUri}/GetTimeTablesByUserandDate?username={username}&dateTime={dateTime}&type={type}&grade={grade}&section={section}";
        }

        public static class Attendance
        {
            public static string GetAttendanceByIdAndDate(string baseUri, long acd_id, int ttm_id, string username, string entrydate, string grade = null, string section = null, string AttendanceType = "") =>
               $"{baseUri}/GetAttendanceByIdAndDate?acd_id={acd_id}&ttm_id={ttm_id}&username={username}&entrydate={entrydate}&grade={grade}&section={section}&AttendanceType={AttendanceType}";
            public static string InsertAttendanceDetails(string baseUri, string entry_date, string username, int alg_id, int ttm_id = 0, string sct_id = "", string GRD_ID = "", long ACD_ID = 0, long BSU_ID = 0, long SHF_ID = 0, long STM_ID = 0, string AttendanceType = "") =>
               $"{baseUri}/InsertAttendanceDetails?entry_date={entry_date}&username={username}&alg_id={alg_id}&ttm_id={ttm_id}&sct_id={sct_id}&GRD_ID={GRD_ID}&ACD_ID={ACD_ID}&BSU_ID={BSU_ID}&SHF_ID={SHF_ID}&STM_ID={STM_ID}&AttendanceType={AttendanceType}";
            public static string GetRoomAttendanceHeader(string baseUri, long SGR_ID, DateTime ENTRY_DATE) =>
       $"{baseUri}/GetRoomAttendanceHeader?SGR_ID={SGR_ID}&ENTRY_DATE={ENTRY_DATE}";

            public static string GetRoomAttendanceDetails(string baseUri, long Coursegroupid, string entryDate) =>
               $"{baseUri}/GetRoomAttendanceDetails?Coursegroupid={Coursegroupid}&entryDate={entryDate}";
            public static string InsertUpdateRoomAttendance(string baseUri, string SchoolId, string UserId, long acd_id, int isDailyWeekly, long schoolGroupId, long teacherId, DateTime entryDate) =>
                $"{baseUri}/InsertUpdateRoomAttendance?SchoolId={SchoolId}&UserId={UserId}&acd_id={acd_id}&isDailyWeekly={isDailyWeekly}&schoolGroupId={schoolGroupId}&teacherId={teacherId}&entryDate={entryDate}";
            public static string GetRoomAttendanceRemarksList(string baseUri, string entryDate, string schoolId, string UserId, int GroupId) =>
               $"{baseUri}/GetRoomAttendanceRemarksList?entryDate={entryDate}&schoolId={schoolId}&UserId={UserId}&GroupId={GroupId}";
            public static string GetAttendanceTypeByEntryDate(string baseUri, int acdId, string schoolId, DateTime AttendanceDt, string GrdId)
                 => $"{baseUri}/GetAttendanceTypeByEntryDate?acdId={acdId}&schoolId={schoolId}&AttendanceDt={AttendanceDt}&GrdId={GrdId}";
            public static string GetGradeSectionAccesses(string baseUri, long schoolId, long academicYear, string userName, string IsSuperUser, int gradeAccess, string gradeId) =>
              $"{baseUri}/GetGradeSectionAccesses?schoolId={schoolId}&academicYear={academicYear}&userName={userName}&IsSuperUser={IsSuperUser}&gradeAccess={gradeAccess}&gradeId={gradeId}";

            public static string GetAttendanceByIdAndDate(string baseUri, long acd_id, int ttm_id, string username, string entrydate, string grade = null, string section = null) =>
                $"{baseUri}/GetAttendanceByIdAndDate?acd_id={acd_id}&ttm_id={ttm_id}&username={username}&entrydate={entrydate}&grade={grade}&section={section}";

            public static string InsertAttendanceDetails(string baseUri, string entry_date, string username, int alg_id, int ttm_id = 0, int sct_id = 0, string GRD_ID = "", long ACD_ID = 0, long BSU_ID = 0, long SHF_ID = 0, long STM_ID = 0) =>
                $"{baseUri}/InsertAttendanceDetails?entry_date={entry_date}&username={username}&alg_id={alg_id}&ttm_id={ttm_id}&sct_id={sct_id}&GRD_ID={GRD_ID}&ACD_ID={ACD_ID}&BSU_ID={BSU_ID}&SHF_ID={SHF_ID}&STM_ID={STM_ID}";

            public static string Get_ATTENDENCE_ANALYSIS(string baseUri, string STU_ID) =>
                $"{baseUri}/Get_ATTENDENCE_ANALYSIS?STU_ID={STU_ID}";


            public static string Get_AttendenceBySession(string baseUri, string STU_ID, DateTime EndDate) =>
               $"{baseUri}/Get_AttendenceBySession?STU_ID={STU_ID}&EndDate={EndDate}";

            public static string Get_AttendenceSessionCode(string baseUri, string STU_ID, DateTime EndDate) =>
              $"{baseUri}/Get_AttendenceSessionCode?STU_ID={STU_ID}&EndDate={EndDate}";
            public static string Get_AttendanceChartMain(string baseUri, string STU_ID) =>
              $"{baseUri}/Get_AttendanceChartMain?STU_ID={STU_ID}";

            public static string GetGradeSectionByUserId(string baseUri, long Id, long currentAcademicYearId) =>
                $"{baseUri}/GetGradeSectionByUserId?Id={Id}&currentAcademicYearId={currentAcademicYearId}";

            public static string GetDailyAttendanceDetails(string baseUri, long gradeId, long sectionId, long sessionTypeId, DateTime asOnDate, long schoolId) =>
                 $"{baseUri}/GetDailyAttendanceDetails?gradeId={gradeId}&sectionId={sectionId}&sessionTypeId={sessionTypeId}&asOnDate={asOnDate}&schoolId={schoolId}";
            public static string SaveDailyAttendance(string baseUri, long userId, int IsMobile) =>
                $"{baseUri}/SaveDailyAttendance?userId={userId}&IsMobile={IsMobile}";

            public static string GetClassAttendanceHeader(string baseUri, long schoolId, long schoolGroupId, DateTime entryDate) =>
                $"{baseUri}/GetClassAttendanceHeader?schoolId={schoolId}&schoolGroupId={schoolGroupId}&entryDate={entryDate}";

            public static string GetClassAttendanceDetails(string baseUri, long schoolGroupId, DateTime entryDate) =>
                $"{baseUri}/GetClassAttendanceDetails?schoolGroupId={schoolGroupId}&entryDate={entryDate}";

            public static string GetWeekEndBySchoolId(string baseUri, long schoolId, long academicyearId, long schoolgradeId, long courseId, DateTime date) =>
                $"{baseUri}/GetWeekEndBySchoolId?schoolId={schoolId}&academicyearId={academicyearId}&schoolgradeId={schoolgradeId}&courseId={courseId}&date={date}&languageId={LocalizationHelper.CurrentSystemLanguage.SystemLanguageId}";

            public static string GetGradeSessionByGradeId(string baseUri, long schoolGradeId) =>
                $"{baseUri}/GetGradeSessionByGradeId?schoolGradeId={schoolGradeId}";


            public static string GetCurriculumByAuthorizedStaffId(string baseUri, long staffId) =>
                $"{baseUri}/GetCurriculumByAuthorizedStaffId?staffId={staffId}";

            public static string GetGroupsfromTimeTable(string baseUri, long teacherId, DateTime courseDate) =>
                $"{baseUri}/GetGroupsfromTimeTable?teacherId={teacherId}&courseDate={courseDate}";


            public static string GetPreviousClassAttendanceHeader(string baseUri, long schoolId, long schoolGroupId, DateTime entryDate) =>
                $"{baseUri}/GetPreviousClassAttendanceHeader?schoolId={schoolId}&schoolGroupId={schoolGroupId}&entryDate={entryDate}";

            public static string GetPreviousClassAttendanceDetails(string baseUri, long schoolGroupId, DateTime entryDate) =>
                $"{baseUri}/GetPreviousClassAttendanceDetails?schoolGroupId={schoolGroupId}&entryDate={entryDate}";

            public static string GetParameterMappingByAcademicId(string baseUri, long academicYearId) =>
                $"{baseUri}/GetParameterMappingByAcademicId?academicYearId={academicYearId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";

            public static string GetRequiredParamBySchoolUserId(string baseUri, long schoolId, long id, long academicYearId) =>
                $"{baseUri}/GetRequiredParamBySchoolUserId?schoolId={schoolId}&id={id}&academicYearId={academicYearId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";

            public static string GetClassAttendanceHeaderNDetails(string baseUri, long schoolId, long schoolGroupId, DateTime entryDate) =>
                $"{baseUri}/GetClassAttendanceHeaderNDetails?schoolId={schoolId}&schoolGroupId={schoolGroupId}&entryDate={entryDate}";
        }
        public static class ClassList
        {
            public static string GetClassList(string baseUri, long SchoolId, long GroupId, long GradeId, long SectionId)
                => $"{baseUri}/GetClassList?SchoolId={SchoolId}&GroupId={GroupId}&GradeId={GradeId}&SectionId={SectionId}";
            public static string GetStudentDetails(string baseUri, string stu_id) =>
                $"{baseUri}/GetStudentDetails?stu_id={stu_id}";
            public static string GetStudentProfileDetail(string baseUri, long stu_id, long SchoolId, DateTime nowDate) =>
               $"{baseUri}/GetStudentProfileDetail?stu_id={stu_id}&SchoolId={SchoolId}&nowDate={nowDate}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string GetQuickContactDetail(string baseUri, long StudentId, long SchoolId) =>
                 $"{baseUri}/GetQuickContactDetail?StudentId={StudentId}&SchoolId={SchoolId}";
            public static string GetStudentFindMeDetail(string baseUri, long StudentId, long SchoolId, DateTime nowDate) =>
                 $"{baseUri}/GetStudentFindMeDetail?StudentId={StudentId}&SchoolId={SchoolId}&nowDate={nowDate}";
            public static string GetWeeklyTimeTableDetail(string baseUri, long StudentId, long SchoolId, int WeekCount, DateTime nowDate) =>
             $"{baseUri}/GetWeeklyTimeTableDetail?StudentId={StudentId}&SchoolId={SchoolId}&WeekCount={WeekCount}&nowDate={nowDate}";

            public static string GetAttendanceProfileDetail(string baseUri, long StudentId, long SchoolId, string AttendanceDt, string AttendanceType) =>
               $"{baseUri}/GetAttendanceProfileDetail?StudentId={StudentId}&SchoolId={SchoolId}&AttendanceDt={AttendanceDt}&AttendanceType={AttendanceType}";

            public static string GetStudentDashboardDetails(string baseUri, string stu_id) =>
             $"{baseUri}/GetStudentDashboardDetails?stu_id={stu_id}";
            public static string GetAttendanceChart(string baseUri, string stu_id) =>
             $"{baseUri}/GetAttendanceChart?stu_id={stu_id}";
            public static string GetAttendenceList(string baseUri, string stu_id, DateTime EndDate) =>
           $"{baseUri}/GetAttendenceList?stu_id={stu_id}&EndDate={EndDate}";
            public static string StudentOnReportMasterCU(string baseUri) => $"{baseUri}/StudentOnReportMasterCU";
            public static string GetStudentOnReportMasters(string baseUri, long studentId, long academicYearId, long schoolId) =>
           $"{baseUri}/GetStudentOnReportMasters?studentId={studentId}&academicYearId={academicYearId}&schoolId={schoolId}";
            public static string StudentOnReportDetailsCU(string baseUri) =>
           $"{baseUri}/StudentOnReportDetailsCU";
            public static string GetStudentOnReportDetails(string baseUri) =>
           $"{baseUri}/GetStudentOnReportDetails";
            public static string GetCourseWiseSchoolGroups(string baseUri, long StudentId) =>
            $"{baseUri}/GetCourseWiseSchoolGroups?StudentId={StudentId}";
            public static string ChangeCourseWiseStudentSchoolGroup(string baseUri) =>
                 $"{baseUri}/ChangeCourseWiseStudentSchoolGroup";
            public static string GetStudentListBySearch(string baseUri, long SchoolId, long UserId, string SearchString) =>
                   $"{baseUri}/GetStudentListBySearch?SchoolId={SchoolId}&UserId={UserId}&SearchString={SearchString}";
        }

        public static class Behaviour
        {
            public static string GetStudentList(string baseUri, string username, int tt_id = 0, string grade = null, string section = null) =>
                    $"{baseUri}/GetStudentList?username={username}&tt_id={tt_id}&grade={grade}&section={section}";
            public static string LoadBehaviourByStudentId(string baseUri, string stu_id) =>
                    $"{baseUri}/LoadBehaviourByStudentId?stu_id={stu_id}";
            public static string GetBehaviourById(string baseUri, int id) => $"{baseUri}/GetBehaviourById?id={id}";

            public static string InsertBehaviourDetails(string baseUri, string bsu_id, string mode) => $"{baseUri}/InsertBehaviourDetails?bsu_id={bsu_id}&mode={mode}";

            public static string GetListOfStudentBehaviour(string baseUri) => $"{baseUri}/GetListOfStudentBehaviour";

            public static string InsertUpdateStudentBehavior(string baseUri, long studentId = 0, int behaviorId = 0, string behaviourComment = "") => $"{baseUri}/InsertUpdateStudentBehavior?studentId={studentId}&behaviourId={behaviorId}&behaviourComment={behaviourComment}";
            public static string GetStudentBehaviorByStudentId(string baseUri, long studentId = 0) => $"{baseUri}/GetStudentBehaviorByStudentId?studentId={studentId}";

            public static string InsertBulkStudentBehaviour(string baseUri, string bulkStudentids = "", int behaviourId = 0, string behaviourComment = "") => $"{baseUri}/InsertBulkStudentBehaviour?bulkStudentIds={bulkStudentids}&behaviourId={behaviourId}&behaviourComment={behaviourComment}";
            //UpdateBehaviourTypes
            public static string UpdateBehaviourTypes(string baseUri, int behaviourId = 0, string behaviourType = "", int behaviourPoint = 0, int categoryId = 0) => $"{baseUri}/UpdateBehaviourTypes?behaviourId={behaviourId}&behaviourType={behaviourType}&behaviourPoint={behaviourPoint}&categoryId={categoryId}";

            //GetFileDetailsByStudentId
            public static string GetFileDetailsByStudentId(string baseUri, long studentId = 0) => $"{baseUri}/GetFileDetailsByStudentId?studentId={studentId}";

            public static string GetBehaviourClassList(string baseUri, string username, int tt_id = 0, string grade = null, string section = null, long GroupId = 0, bool IsFilterByGroup = false) =>
                  $"{baseUri}/GetBehaviourClassList?username={username}&tt_id={tt_id}&grade={grade}&section={section}&GroupId={GroupId}&IsFilterByGroup={IsFilterByGroup}";

            public static string GetBehaviourStudentListByGroupId(string baseUri, long groupId) =>
                $"{baseUri}/GetBehaviourStudentListByGroupId?groupId={groupId}";
            public static string GetCourseGroupList(string baseUri, long TeacherId, long SchoolId) =>
             $"{baseUri}/GetCourseGroupList?TeacherId={TeacherId}&SchoolId={SchoolId}";
            public static string DeleteStudentBehaviourMapping(string baseUri, long studentId = 0, int behaviourId = 0) =>
                $"{baseUri}/DeleteStudentBehaviourMapping?studentId={studentId}&behaviourId={behaviourId}";

            public static string GetIncidentList(string baseUri, long schoolId, long academicYearId, int month, long incidentId) =>
                $"{baseUri}/GetIncidentList?schoolId={schoolId}&academicYearId={academicYearId}&month={month}&incidentId={incidentId}";
            public static string GetIncidentStudentList(string baseUri, long IncidentId) =>
                $"{baseUri}/GetIncidentStudentList?IncidentId={IncidentId}";
            public static string GetIncidentChart(string baseUri, long schoolId, long academicYearId, int month, bool isCategory) =>
                $"{baseUri}/GetIncidentChart?schoolId={schoolId}&academicYearId={academicYearId}&month={month}&isCategory={isCategory}";
            public static string GetIncidentStaffLists(string baseUri, long schoolId) =>
                $"{baseUri}/GetIncidentStaffLists?schoolId={schoolId}";
            public static string GetIncidentEntryCUD(string baseUri) =>
                $"{baseUri}/GetIncidentEntryCUD";
            public static string GetSubCategoriesByCategoryId(string baseUri, long categoryId, string BSU_ID, string GRD_ID, long GroupId) =>
                $"{baseUri}/GetSubCategoriesByCategoryId?categoryId={categoryId}&BSU_ID={BSU_ID}&GRD_ID={GRD_ID}&GroupId={GroupId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";

            public static string GetMeritDetails(string baseUri, long meritId) =>
                $"{baseUri}/GetMeritDetails?meritId={meritId}";
            public static string GetMeritCategoryByStudent(string baseUri, long schoolId, long studentId) =>
                $"{baseUri}/GetMeritCategoryByStudent?schoolId={schoolId}&studentId={studentId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";

            public static string InsertMeritDemerit(string baseUri, string schoolId, int academicId, DateTime? incidentDate) =>
                $"{baseUri}/InsertMeritDemerit?schoolId={schoolId}&academicId={academicId}&incidentDate={incidentDate}";

            #region Student Point Category
            public static string GetStudentPointCategory(string baseUri, long schoolId, long academicYearId, int scheduleType) =>
                $"{baseUri}/GetStudentPointCategory?schoolId={schoolId}&academicYearId={academicYearId}&scheduleType={scheduleType}";
            public static string GetStudentPointCategory(string baseUri, string sectionId, int? scheduleType, long? CertificateScheduleId, string date, long? studentId) =>
                $"{baseUri}/GetStudentPointCategorySchedule?sectionId={sectionId}&scheduleType={scheduleType}&CertificateScheduleId={CertificateScheduleId}&date={date}&studentId={studentId}";
            public static string CertificateProcessLogCU(string baseUri) =>
                $"{baseUri}/CertificateProcessLogCU";
            #endregion

            #region
            public static string GetStudentOnReportMDetail(string baseUri, long studentId) =>
          $"{baseUri}/GetStudentOnReportMDetail?studentId={studentId}";
            public static string StudentOnReportMCU(string baseUri) =>
         $"{baseUri}/StudentOnReportMCU";

            public static string GetStudentOnReportDetails(string baseUri) =>
         $"{baseUri}/GetStudentOnReportDetails";
            public static string StudentOnReportDetailsCU(string baseUri) =>
          $"{baseUri}/StudentOnReportDetailsCU";
            #endregion
        }
        public static class SIMSCommon
        {
            public static string GetGradesAccess(string baseUri, string username, string isSuperUser, Int32 acd_id, Int32 bsu_id, int grd_access, Int32 rsm_id) =>
                $"{baseUri}/GetGradesAccess?username={username}&isSuperUser={isSuperUser}&acd_id={acd_id}&bsu_id={bsu_id}&grd_access={grd_access}&rsm_id={rsm_id}";
            public static string GetSubjectsByGrade(string baseUri, Int32 acd_id, string grd_id) =>
             $"{baseUri}/GetSubjectsByGrade?acd_id={acd_id}&grd_id={grd_id}";

            public static string GetSubjectsByGrade(string baseUri, Int32 acd_id, string grd_id, string username = "", string IsSuperUser = "") =>
            $"{baseUri}/GetSubjectsByGrade?acd_id={acd_id}&grd_id={grd_id}&username={username}&IsSuperUser={IsSuperUser}";
            public static string GetCourseGroupByCourse(string baseUri, long courseId, long userId) =>
            $"{baseUri}/GetCourseGroupByCourse?courseId={courseId}&userId={userId}";

            public static string GetTeacherCourse(string baseUri, long id, long Schoolid, long curriculumId) => $"{baseUri}/GetTeacherCourse?id={id}&Schoolid={Schoolid}&curriculumId={curriculumId}";
            public static string GradeTemplateMaster(string baseUri, long acd_id) => $"{baseUri}/GradeTemplateMaster?acd_id={acd_id}";

            public static string GetUserCurriculumRole(string baseUri, string BSU_ID = "", string UserName = "") =>
            $"{baseUri}/GetUserCurriculumRole?BSU_ID={BSU_ID}&UserName={UserName}";
            public static string GetCurriculum(string baseUri, long schoolId, int? CLM_ID) => $"{baseUri}/GetCurriculum?schoolId={schoolId}&CLM_ID={CLM_ID}";

            public static string GetSelectListItems(string baseUri, string listCode, string whereCondition, object whereConditionParamValues) => $"{baseUri}/getselectlistitems?listCode={listCode}&whereCondition={whereCondition}&whereConditionParamValues={whereConditionParamValues}";

        }
        public static class Assessment
        {
            public static string GetStudentList(string baseUri, string GRD_ID, Int32 ACD_ID, Int32 SGR_ID, Int32 SCT_ID) =>
                $"{baseUri}/GetStudentList?GRD_ID={GRD_ID}&ACD_ID={ACD_ID}&SGR_ID={SGR_ID}&SCT_ID={SCT_ID}";
            public static string GetReportHeaders(string baseUri, string GRD_ID, Int32 ACD_ID, Int32 SBG_ID, Int32 RPF_ID, Int32 RSM_ID, string prv, int isGradeBook) =>
             $"{baseUri}/GetReportHeaders?GRD_ID={GRD_ID}&ACD_ID={ACD_ID}&SBG_ID={SBG_ID}&RPF_ID={RPF_ID}&RSM_ID={RSM_ID}&prv={prv}&isGradeBook={isGradeBook}";
            public static string GetReportHeadersDropdowns(string baseUri, Int32 RSM_ID, Int32 SBG_ID, Int32 RSD_ID) =>
             $"{baseUri}/GetReportHeadersDropdowns?RSM_ID={RSM_ID}&SBG_ID={SBG_ID}&RSD_ID={RSD_ID}";
            public static string GetAssessmentData(string baseUri, Int32 ACD_ID, Int32 SBG_ID, Int32 RPF_ID, Int32 RSM_ID, string prv) =>
             $"{baseUri}/GetAssessmentData?ACD_ID={ACD_ID}&SBG_ID={SBG_ID}&RPF_ID={RPF_ID}&RSM_ID={RSM_ID}&prv={prv}";
            public static string InsertAssessmentData(string baseUri, string username, int bEdit) =>
               $"{baseUri}/InsertAssessmentData?username={username}&bEdit={bEdit}";

            public static string GetAssessmentActivityList(string baseUri, long ACD_ID = 0, long CAM_ID = 0, string GRD_ID = "", long STM_ID = 0, long TRM_ID = 0, long SGR_ID = 0, long SBG_ID = 0, int GRADE_ACCESS = 0, string Username = "", string SuperUser = "") =>
                 $"{baseUri}/GetAssessmentActivityList?ACD_ID={ACD_ID}&CAM_ID={CAM_ID}&GRD_ID={GRD_ID}&STM_ID={STM_ID}&TRM_ID={TRM_ID}&SGR_ID={SGR_ID}&SBG_ID={SBG_ID}&GRADE_ACCESS={GRADE_ACCESS}&Username={Username}&SuperUser={SuperUser}";


            public static string GetMarkEntryAOLData(string baseUri, long CAS_ID) =>
                 $"{baseUri}/GetMarkEntryAOLData?CAS_ID={CAS_ID}";

            public static string InsertMarkEntryAOLData(string baseUri, string Username = "", bool bWithoutSkill = false, long CAS_ID = 0) =>
                  $"{baseUri}/InsertMarkEntryAOLData?Username={Username}&bWithoutSkill={bWithoutSkill}&CAS_ID={CAS_ID}";

            public static string GetMarkEntryData(string baseUri, long CAS_ID, double MIN_MARK, double MAX_MARK) =>
                $"{baseUri}/GetMarkEntryData?CAS_ID={CAS_ID}&MIN_MARK={MIN_MARK}&MAX_MARK={MAX_MARK}";

            public static string InsertMarkEntryData(string baseUri, long SlabId, string entryType, long CAS_ID) =>
                $"{baseUri}/InsertMarkEntryData?SlabId={SlabId}&entryType={entryType}&CAS_ID={CAS_ID}";

            public static string UpdateMarkAttendance(string baseUri, long CAS_ID) =>
                $"{baseUri}/UpdateMarkAttendanceEntry?CAS_ID={CAS_ID}";
            public static string GetAssessmentComments(string baseUri, int CAT_ID, long STU_ID) =>
                $"{baseUri}/GetAssessmentComments?CAT_ID={CAT_ID}&STU_ID={STU_ID}";

            public static string GetHeaderBySubjectCategory(string baseUri, long SGRP_ID) =>
                $"{baseUri}/GetHeaderBySubjectCategory?SGRP_ID={SGRP_ID}";

            public static string GetAssessmentCategories(string baseUri, long CAT_BSU_ID, string CAT_GRD_ID) =>
                $"{baseUri}/GetAssessmentCategories?CAT_BSU_ID={CAT_BSU_ID}&CAT_GRD_ID={CAT_GRD_ID}";
            public static string GetSectionAccess(string baseUri, string USERNAME, string IsSuperUser, long ACD_ID, long BSU_ID, int GRD_ACCESS, string GRD_ID) =>
                $"{baseUri}/GetSectionAccess?USERNAME={USERNAME}&IsSuperUser={IsSuperUser}&ACD_ID={ACD_ID}&BSU_ID={BSU_ID}&GRD_ACCESS={GRD_ACCESS}&GRD_ID={GRD_ID}";
            public static string GetReportHeaderOptional(string baseUri, string AOD_IDs) =>
                $"{baseUri}/GetReportHeaderOptional?AOD_IDs={AOD_IDs}";
            public static string GetAssessmentDataOptional(string baseUri, long ACD_ID, long RPF_ID, long RSM_ID, long SBG_ID, long SGR_ID, string GRD_ID, long SCT_ID, string AOD_IDs) =>
               $"{baseUri}/GetAssessmentDataOptional?ACD_ID={ACD_ID}&RPF_ID={RPF_ID}&RSM_ID={RSM_ID}&SBG_ID={SBG_ID}&SGR_ID={SGR_ID}&GRD_ID={GRD_ID}&SCT_ID={SCT_ID}&AOD_IDs={AOD_IDs}";

            public static string GetAssessmentPreviousSchedule(string baseUri, long ACD_ID, string GRD_ID) =>
               $"{baseUri}/GetAssessmentPreviousSchedule?ACD_ID={ACD_ID}&GRD_ID={GRD_ID}";
            public static string GetAssessmentOptionList(string baseUri, long BSU_ID, long ACD_ID) =>
              $"{baseUri}/GetAssessmentOptionList?BSU_ID={BSU_ID}&ACD_ID={ACD_ID}";
            public static string GetGradeScaleList(string baseUri, long BSU_ID, long ACD_ID, long TEACHER_ID) =>
            $"{baseUri}/GetGradeScaleList?BSU_ID={BSU_ID}&ACD_ID={ACD_ID}&TEACHER_ID={TEACHER_ID}";
            public static string GetGradeScaleDetailList(string baseUri, long GSM_ID) =>
           $"{baseUri}/GetGradeScaleDetailList?GSM_ID={GSM_ID}";
            public static string SaveGradeScaleAndDetail(string baseUri, string DATAMODE) => $"{baseUri}/SaveGradeScaleAndDetail?DATAMODE={DATAMODE}";
            public static string IsReportPublish(string baseUri, long RPP_RSM_ID, long RPP_RPF_ID, long RPP_ACD_ID, string RPP_GRD_ID, long RPP_SCT_ID, long RPP_TRM_ID)
                => $"{baseUri}/IsReportPublish?RPP_RSM_ID={RPP_RSM_ID}&RPP_RPF_ID={RPP_RPF_ID}&RPP_ACD_ID={RPP_ACD_ID}&RPP_GRD_ID={RPP_GRD_ID}&RPP_SCT_ID={RPP_SCT_ID}&RPP_TRM_ID={RPP_TRM_ID}";
            public static string SaveGradeBookSetup(string baseUri, string DATAMODE) => $"{baseUri}/SaveGradeBookSetup?DATAMODE={DATAMODE}";
            public static string GetGradeBookSetupList(string baseUri) => $"{baseUri}/GetGradeBookSetupList";
            public static string GetReportHeaderByRSMID(string baseUri, long RSM_ID) => $"{baseUri}/GetReportHeaderByRSMID?RSM_ID={RSM_ID}";
            public static string GetReportHeaderDDLByRSMID(string baseUri, long RSM_ID) => $"{baseUri}/GetReportHeaderDDLByRSMID?RSM_ID={RSM_ID}";
            public static string SaveProcessingRuleSetup(string baseUri, string DATAMODE) => $"{baseUri}/SaveProcessingRuleSetup?DATAMODE={DATAMODE}";
            public static string GetProcessingRuleSetupList(string baseUri, long PRS_RSD_ID) => $"{baseUri}/GetProcessingRuleSetupList?PRS_RSD_ID={PRS_RSD_ID}";
            public static string GetGradebookDetail(string baseUri, long RSD_ID) => $"{baseUri}/GetGradebookDetail?RSD_ID={RSD_ID}";
            public static string GradeBookCUD(string baseUri) => $"{baseUri}/GradeBookCUD";

            public static string GetSubjectsForReportWriting(string baseUri, long acdId, long studentId, string IsSuperUser, long employeeId) =>
                $"{baseUri}/GetSubjectsForReportWriting?acdId={acdId}&studentId={studentId}&IsSuperUser={IsSuperUser}&employeeId={employeeId}";
            public static string GetSavedRecordsOfReportWriting(string baseUri, long rpfId, long studentId) =>
                $"{baseUri}/GetSavedRecordsOfReportWriting?rpfId={rpfId}&studentId={studentId}";
            public static string ReportWritingCU(string baseUri) =>
                $"{baseUri}/ReportWritingCU";
        }

        public static class SEN
        {
            public static string Get_studentInclusionList(string baseUri, string BSU_ID, string ACD_ID, string GRD_ID, string SCT_ID) =>
                $"{baseUri}/Get_studentInclusionList?BSU_ID={BSU_ID}&ACD_ID={ACD_ID}&GRD_ID={GRD_ID}&SCT_ID={SCT_ID}";
            public static string Get_studentInclusionAll(string baseUri, string BSU_ID, string ACD_ID, string GRD_ID, string SCT_ID) =>
               $"{baseUri}/Get_studentInclusionAll?BSU_ID={BSU_ID}&ACD_ID={ACD_ID}&GRD_ID={GRD_ID}&SCT_ID={SCT_ID}";
            public static string InsertBulkSEN(string baseUri) =>
              $"{baseUri}/InsertBulkSEN";

            public static string updateSenStudent(string baseUri, string stuId) =>
              $"{baseUri}/updateSenStudent?stuId={stuId}";

            public static string Get_SEN_KHDA_MASTER(string baseUri) =>
              $"{baseUri}/Get_SEN_KHDA_MASTER";

            public static string Get_KHDA_STUDENT(string baseUri, string stuId) =>
              $"{baseUri}/Get_KHDA_STUDENT?stuId={stuId}";

            public static string Get_SEN_KHDA_TRANS_LIST(string baseUri, string stuId) =>
              $"{baseUri}/Get_SEN_KHDA_TRANS_LIST?stuId={stuId}";
            public static string SaveSENKHDA(string baseUri, string uGUID, string filePath) =>
              $"{baseUri}/SaveSENKHDA?uGUID={uGUID}&filePath={filePath}";
        }


        public static class SIMSReport
        {


            public static string BindReportFilters(string baseUri, long RDSR_ID, string RDF_FILTER_CODE) =>
               $"{baseUri}/BindReportFilters?RDSR_ID={RDSR_ID}&RDF_FILTER_CODE={RDF_FILTER_CODE}";

            public static string GetReportFiltersType(string baseUri) =>
                $"{baseUri}/GetReportFiltersType";

            public static string GetReportLayoutById(string baseUri, string Dev_Id) =>
                $"{baseUri}/GetReportLayoutById?Dev_Id={Dev_Id}";

            public static string LoadDesignedReports(string baseUri, string BSU_ID, string ModuleId) =>
                $"{baseUri}/LoadDesignedReports?BSU_ID={BSU_ID}&ModuleId={ModuleId}";

            public static string GetReportTypes(string baseUri) =>
           $"{baseUri}/GetReportTypes";

            public static string GetDatasetForSp(string baseUri, string sp_Name) =>
         $"{baseUri}/GetDatasetForSp?sp_Name={sp_Name}";

            public static string GetDevIdFromRSM_ID(string baseUri, int RSM_ID) =>
          $"{baseUri}/GetDevIdFromRSM_ID?RSM_ID={RSM_ID}";

            public static string GetReportTopHeaders(string baseUri, string IMG_BSU_ID, string IMG_TYPE) =>
         $"{baseUri}/GetReportTopHeaders?IMG_BSU_ID={IMG_BSU_ID}&IMG_TYPE={IMG_TYPE}";

            public static string BindCrystalReportFilters(string baseUri, long RDSR_ID, string RDF_FILTER_CODE) =>
              $"{baseUri}/BindCrystalReportFilters?RDSR_ID={RDSR_ID}&RDF_FILTER_CODE={RDF_FILTER_CODE}";
        }

        public static class ProgressTracker
        {

            public static string GetTopicsByCourseId(string baseUri, long Id) =>
                  $"{baseUri}/GetTopicsByCourseId?Id={Id}";
            public static string GetStudentList(string baseUri, string GRD_ID, Int32 ACD_ID, Int32 SGR_ID, Int32 SCT_ID) =>
               $"{baseUri}/GetStudentList?GRD_ID={GRD_ID}&ACD_ID={ACD_ID}&SGR_ID={SGR_ID}&SCT_ID={SCT_ID}";
            public static string GET_PROGRESS_TRACKER_HEADERS(string baseUri, long SBG_ID, string TOPIC_ID, long AGE_BAND_ID, string STEPS, long TSM_ID) =>
              $"{baseUri}/GET_PROGRESS_TRACKER_HEADERS?SBG_ID={SBG_ID}&TOPIC_ID={TOPIC_ID}&AGE_BAND_ID={AGE_BAND_ID}&STEPS={STEPS}&TSM_ID={TSM_ID}";
            public static string GET_PROGRESS_TRACKER_DATA(string baseUri, long SBG_ID, string TOPIC_ID, long TSM_ID, long SGR_ID) =>
             $"{baseUri}/GET_PROGRESS_TRACKER_DATA?SBG_ID={SBG_ID}&TOPIC_ID={TOPIC_ID}&TSM_ID={TSM_ID}&SGR_ID={SGR_ID}";
            public static string BindProgressTrackerDropdown(string baseUri, long BSU_ID, string GRD_ID) =>
                $"{baseUri}/BindProgressTrackerDropdown?BSU_ID={BSU_ID}&GRD_ID={GRD_ID}";
            public static string BindProgressTrackerMasterSetting(string baseUri, long ACD_ID, long BSU_ID, string GRD_ID) =>
             $"{baseUri}/BindProgressTrackerMasterSetting?ACD_ID={ACD_ID}&BSU_ID={BSU_ID}&GRD_ID={GRD_ID}";

            public static string AddEditProgressSetUP(string baseUri, long teacherId) => $"{baseUri}/AddEditProgressSetUP?teacherId={teacherId}";

            public static string GetProgressSetup(string baseUri, long schoolId) => $"{baseUri}/GetProgressSetup?schoolId={schoolId}";

            public static string GetCourseGradeDisplay(string baseUri, long schoolId, string courseIds) =>
                $"{baseUri}/GetCourseGradeDisplay?schoolId={schoolId}&courseIds={courseIds}";

            public static string GetProgressSetupDetailsById(string baseUri, long Id) =>
                $"{baseUri}/GetProgressSetupDetailsById?Id={Id}";

            public static string GetProgressLessonGrading(string baseUri, long courseId, long schoolGroupId) =>
                $"{baseUri}/GetProgressLessonGrading?courseId={courseId}&schoolGroupId={schoolGroupId}";

            public static string StudentProgressTracker(string baseUri, long schoolGroupId) =>
                $"{baseUri}/StudentProgressTracker?schoolGroupId={schoolGroupId}";

            public static string GetLessonObjectivesByUnitIds(string baseUri, string subSyllabusIds) =>
                $"{baseUri}/GetLessonObjectivesByUnitIds?subSyllabusIds={subSyllabusIds}";

            public static string StudentProgressMapper(string baseUri, long userId, long groudId) =>
                $"{baseUri}/StudentProgressMapper?userId={userId}&groudId={groudId}";

            public static string SaveProgressTrackerEvidence(string baseUri) =>
                $"{baseUri}/SaveProgressTrackerEvidence";

            public static string GetProgressTrackerEvidence(string baseUri, long id, string key) =>
                $"{baseUri}/GetProgressTrackerEvidence?id={id}&key={key}";

            public static string GetObjectiveAssignmentGrading(string baseUri, long lessonId, long studentId) =>
                $"{baseUri}/GetObjectiveAssignmentGrading?lessonId={lessonId}&studentId={studentId}";

            public static string ValidateProgressSetupCourseGrade(string baseUri, string courseIds, string gradeIds) =>
                $"{baseUri}/ValidateProgressSetupCourseGrade?courseIds={courseIds}&gradeIds={gradeIds}";



        }
        public static class SubjectSetting
        {
            public static string GetStudentList(string baseUri, int CLM_ID) => $"{baseUri}/GetSubjectMasterList?CLM_ID={CLM_ID}";
            public static string BindGradesForSubject(string baseUri, long ACD_ID) => $"{baseUri}/BindGradesForSubject?ACD_ID={ACD_ID}";
            public static string BindSubjectsByGrade(string baseUri, long ACD_ID, string GRD_ID, int STM_ID, int SBG_ID) => $"{baseUri}/BindSubjectsByGrade?ACD_ID={ACD_ID}&GRD_ID={GRD_ID}&STM_ID={STM_ID}&SBG_ID={SBG_ID}";
            public static string SubjectMasterCUD(string baseUri, string mode) => $"{baseUri}/SubjectMasterCUD?mode={mode}";
            public static string GetSubjectMastersByCurriculum(string baseUri, long curriculumId) =>
                $"{baseUri}/GetSubjectMastersByCurriculum?curriculumId={curriculumId}";
            public static string GetParentSubjects(string baseUri, long ACD_ID, int STM_ID, string GRD_ID) =>
                $"{baseUri}/GetParentSubjects?ACD_ID={ACD_ID}&STM_ID={STM_ID}&GRD_ID={GRD_ID}";
            public static string GetGradeForSubjectCopy(string baseUri, long ACD_ID) =>
                $"{baseUri}/GetGradeForSubjectCopy?ACD_ID={ACD_ID}";
            public static string GetStreamForSubjectCopy(string baseUri, long ACD_ID, string GRD_ID) =>
                $"{baseUri}/GetStreamForSubjectCopy?ACD_ID={ACD_ID}&GRD_ID={GRD_ID}";
            public static string GetSubjectGroupList(string baseUri, long ACD_ID, string IsSuperUser, string Username) => $"{baseUri}/GetSubjectGroupList?ACD_ID={ACD_ID}&IsSuperUser={IsSuperUser}&Username={Username}";
            public static string GetShiftListById(string baseUri, long ACD_ID, string GRD_ID) => $"{baseUri}/GetShiftListById?ACD_ID={ACD_ID}&GRD_ID={GRD_ID}";
            public static string GetSubjectGroupTeachers(string baseUri, long BSU_ID, string IsSuperUser, string Username) => $"{baseUri}/GetSubjectGroupTeachers?BSU_ID={BSU_ID}&IsSuperUser={IsSuperUser}&Username={Username}";
            public static string GetSubjectGroupTeachersGrid(string baseUri, long SGR_ID, string IsSuperUser, string Username) => $"{baseUri}/GetSubjectGroupTeachersGrid?SGR_ID={SGR_ID}&IsSuperUser={IsSuperUser}&Username={Username}";
            public static string SaveUpdateGroupTeacher(string baseUri, string UserName, string mode) => $"{baseUri}/SaveUpdateGroupTeacher?UserName={UserName}&mode={mode}";
            public static string SaveUpdateSubjectGroup(string baseUri, string mode) => $"{baseUri}/SaveUpdateSubjectGroup?mode={mode}";
            public static string GetSubjectGroupStudentList(string baseUri, long ACD_ID, string GRD_ID, int SHF_ID, int STM_ID, long SBG_ID, long SGR_ID) =>
                $"{baseUri}/GetSubjectGroupStudentList?ACD_ID={ACD_ID}&GRD_ID={GRD_ID}&SHF_ID={SHF_ID}&STM_ID={STM_ID}&SBG_ID={SBG_ID}&SGR_ID={SGR_ID}";
            public static string AddOptionName(string baseUri, long schoolId, string optionsName) => $"{baseUri}/AddOptionName?schoolId={schoolId}&OptionName={optionsName}";
            public static string SubjectGrade(string baseUri) => $"{baseUri}/SubjectGrade/";
            public static string SubjectGradeDelete(string baseUri, int subjectGradeId) => $"{baseUri}/SubjectGradeD?subjectGradeId={subjectGradeId}";
        }

        #endregion

        #region RSSFeed

        public static class RssFeed
        {
            public static string GetRSSFeedList(string baseUri) => $"{baseUri}/GetRSSFeedList";
            public static string AddUpdateRssFeed(string baseUri) => $"{baseUri}/AddUpdateRSSFeed";
            public static string GetRSSFeedId(string baseUri, int Id) => $"{baseUri}/GetRSSFeedById/{Id}";

        }
        #endregion
        #region GroupUrl

        public static class GroupUrl
        {
            public static string GetGroupUrlList(string baseUri) => $"{baseUri}/GetGroupUrlList";
            public static string AddGroupUrl(string baseUri) => $"{baseUri}/AddGroupUrl";
            public static string UpdateGroupUrl(string baseUri) => $"{baseUri}/UpdateGroupUrl";
            public static string GetGroupUrlById(string baseUri, int Id) => $"{baseUri}/GetGroupUrlById?id={Id}";
            public static string GetGroupUrlListByGroupId(string baseUri, int groupId) => $"{baseUri}/GetGroupUrlListByGroupId?groupId={groupId}";


        }
        #endregion
        #region GroupQuiz

        public static class GroupQuiz
        {
            public static string AddGroupQuiz(string baseUri) => $"{baseUri}/addgroupquiz";
            public static string GetGroupQuizListByGroupId(string baseUri, int groupId) => $"{baseUri}/GetGroupQuizListByGroupId?groupId={groupId}";
            public static string GetGroupQuizListByGroupIdAndDateRange(string baseUri, int groupId, DateTime startDate, DateTime endDate) => $"{baseUri}/GetGroupQuizListByGroupIdAndDateRange?groupId={groupId}&startDate={startDate}&endDate={endDate}";
            public static string GetQuizByCourse(string baseUri, string courseIds, int schoolId) => $"{baseUri}/GetQuizByCourse?courseIds={courseIds}&schoolId={schoolId}";
            public static string GetGroupQuizById(string baseUri, int Id) => $"{baseUri}/GetGroupQuizById?id={Id}";
            public static string UpdateGroupQuiz(string baseUri) => $"{baseUri}/UpdateGroupQuiz";
            public static string InsertUpdateQuizAnswers(string baseUri) => $"{baseUri}/insertupdatequizanswers";
            public static string LogQuizQuestionAnswer(string baseUri) => $"{baseUri}/logquizquestionanswer";

            public static string UpdateQuizGrade(string baseUri, int gradeId, int gradingTemplateId, int quizResultId) => $"{baseUri}/updateQuizGrade?gradeId={gradeId}&gradingTemplateId={gradingTemplateId}&quizResultId={quizResultId}";
            public static string InsertQuizFeedback(string baseUri) => $"{baseUri}/InsertQuizFeedback";
            public static string GetQuizResultByUserId(string baseUri, int id, int userId, int resourceId) => $"{baseUri}/GetQuizResultByUserId?quizId={id}&userId={userId}&resourceId={resourceId}";
            public static string GetQuizReportCardByUserId(string baseUri, int id, int userId, int resourceId, string resourceType) => $"{baseUri}/GetQuizReportCardByUserId?quizId={id}&userId={userId}&resourceId={resourceId}&resourceType={resourceType}";
            public static string GetLogResultQuestionAnswer(string baseUri, int id, int userId, int resourceId, string resourceType) => $"{baseUri}/GetLogResultQuestionAnswer?quizId={id}&userId={userId}&resourceId={resourceId}&resourceType={resourceType}";
            public static string GetQuizStudentDetailsByQuizId(string baseUri, int groupQuizId, int groupId, int selectedGroupId) => $"{baseUri}/GetQuizStudentDetailsByQuizId?groupQuizId={groupQuizId}&groupId={groupId}&selectedGroupId={selectedGroupId}";
            public static string GetFormStudentDetailsByQuizId(string baseUri, int quizId, int groupId) => $"{baseUri}/GetFormStudentDetailsByQuizId?QuizId={quizId}&groupId={groupId}";

        }
        #endregion

        #region Reports
        public static class IncidentReport
        {
            public static string GetInjuredBodyPartReport(string baseUri, string incidentIds) => $"{baseUri}/getinjuredbodypartreport?incidentIds={incidentIds}";
            public static string GetReportComparisonData(string baseUri) => $"{baseUri}/getreportcomparisiondata";
        }
        #endregion
        #region SchoolBanner
        public static class SchoolBanner
        {
            public static string InsertSchoolBanner(string baseUri) => $"{baseUri}/InsertSchoolBanner";
            public static string UpdateSchoolBanner(string baseUri) => $"{baseUri}/UpdateSchoolBanner";
            public static string DeleteSchoolBanner(string baseUri) => $"{baseUri}/DeleteSchoolBanner";
            public static string GetSchoolBannersbyPage(string baseUri, int pagenumber, int pagesize, string SearchString, int userId, int schoolId) => $"{baseUri}/GetSchoolBannersByPage";
            public static string GetSchoolBanners(string baseUri, long userId, int schoolId) => $"{baseUri}/GetSchoolBanners?userId={userId}&schoolId={schoolId}";
            public static string GetBannerSchoolIds(string baseUri, long bannerId) => $"{baseUri}/GetBannerSchoolIds?SchoolBannerId={bannerId}";
            public static string GetSchoolBannerDetails(string baseUri, long bannerId, long userId) => $"{baseUri}/GetSchoolBannerDetails?schoolBannerId={bannerId}&userId={userId}";
            public static string GetTopOrderForDisplayBanner(string baseUri, long userId) => $"{baseUri}/GetTopOrderForDisplayBanner?userId={userId}";
            public static string GetUserBanners(string baseUri, long userId) => $"{baseUri}/GetUserBanners?userId={userId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
        }
        #endregion


        #region SchoolBadge
        public static class SchoolBadge
        {
            public static string InsertSchoolBadge(string baseUri) => $"{baseUri}/InsertSchoolBadge";
            public static string UpdateSchoolBadge(string baseUri) => $"{baseUri}/UpdateSchoolBadge";
            public static string DeleteSchoolBadge(string baseUri) => $"{baseUri}/DeleteSchoolBadge";
            public static string GetSchoolBadgesbyPage(string baseUri, int pagenumber, int pagesize, string SearchString, int userId, int schoolId) => $"{baseUri}/GetSchoolBadgesByPage";
            public static string GetSchoolBadges(string baseUri, long userId, int schoolId) => $"{baseUri}/GetSchoolBadges?userId={userId}&schoolId={schoolId}";
            public static string GetBadgeSchoolIds(string baseUri, long BadgeId) => $"{baseUri}/GetBadgeSchoolIds?SchoolBadgeId={BadgeId}";
            public static string GetSchoolBadgeDetails(string baseUri, long BadgeId, long userId) => $"{baseUri}/GetSchoolBadgeDetails?schoolBadgeId={BadgeId}&userId={userId}";
            public static string GetTopOrderForDisplayBadge(string baseUri, long userId) => $"{baseUri}/GetTopOrderForDisplayBadge?userId={userId}";
            public static string GetUserBadges(string baseUri, long userId) => $"{baseUri}/GetUserBadges?userId={userId}";
            public static string SaveStudentBadges(string baseUri) => $"{baseUri}/SaveStudentBadges";
            public static string GetBadgesBySchoolId(string baseUri, int schoolId) => $"{baseUri}/GetBadgesBySchoolId?schoolId={schoolId}";
            public static string SaveGroupWiseStudentBadges(string baseUri) => $"{baseUri}/SaveGroupWiseStudentBadges";
        }
        #endregion

        #region MarkingPolicy
        public static class MarkingPolicy
        {
            public static string InsertMarkingPolicy(string baseUri) => $"{baseUri}/InsertMarkingPolicy";
            public static string UpdateMarkingPolicy(string baseUri) => $"{baseUri}/UpdateMarkingPolicy";
            public static string DeleteMarkingPolicy(string baseUri) => $"{baseUri}/DeleteMarkingPolicy";
            public static string GetMarkingPolicysbyPage(string baseUri, int pagenumber, int pagesize, string SearchString, int userId, int schoolId) => $"{baseUri}/GetMarkingPolicysByPage";
            public static string GetMarkingPolicys(string baseUri, long userId, int schoolId) => $"{baseUri}/GetMarkingPolicys?userId={userId}&schoolId={schoolId}";
            public static string GetMarkingPolicySchoolIds(string baseUri, long MarkingPolicyId) => $"{baseUri}/GetMarkingPolicySchoolIds?MarkingPolicyId={MarkingPolicyId}";
            public static string GetMarkingPolicyDetails(string baseUri, long MarkingPolicyId, long userId) => $"{baseUri}/GetMarkingPolicyDetails?MarkingPolicyId={MarkingPolicyId}&userId={userId}";
            public static string GetUserMarkingPolicys(string baseUri, long userId) => $"{baseUri}/GetUserMarkingPolicys?userId={userId}";
        }
        #endregion
        public static class Duration
        {
            public static string GetDuration(string baseUri, int Id) => $"{baseUri}/getDuration?id={Id}";

        }
        public static class EventCategory
        {
            public static string GetEventCategory(string baseUri, int Id) => $"{baseUri}/getEventCategory?id={Id}";
            public static string GetAllEventCategoryBySchool(string baseUri, long schoolId) => $"{baseUri}/getAllEventCategoryBySchool?schoolId={schoolId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string GetEventCategoriesBySchoolId(string baseUri, long schoolId) => $"{baseUri}/getEventCategoriesBySchoolId?schoolId={schoolId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string GetEventCategoryById(string baseUri, int id) => $"{baseUri}/geteventcategorybyid?Id={id}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string InsertEventCategory(string baseUri) => $"{baseUri}/inserteventcategory";
            public static string UpdateEventCategory(string baseUri) => $"{baseUri}/UpdateEventCategory";
            public static string DeleteEventCategory(string baseUri, int id) => $"{baseUri}/DeleteEventCategory/{id}";
            public static string CheckEventCategoryAvailable(string baseUri, string categoryName, int id, long schoolId) => $"{baseUri}/CheckEventCategoryAvailable?categoryName={categoryName}&id={id}&schoolId={schoolId}";

        }
        public static class Event
        {
            public static string EventInsert(string baseUri) => $"{baseUri}/EventInsert";
            public static string EventUpdate(string baseUri) => $"{baseUri}/EventUpdate";
            public static string GetEvent(string baseUri, int id) => $"{baseUri}/getEvent?id={id}";
            public static string CancelEvent(string baseUri, int eventId, string onlineMeetingId, long userId) => $"{baseUri}/cancelEvent?eventId={eventId}&onlineMeetingId={onlineMeetingId}&userId={userId}";
            public static string GetEventUserDetails(string baseUri, int eventId, long userId) => $"{baseUri}/getEventUserDetails?eventId={eventId}&userId={userId}";
            public static string DeleteEventFile(string baseUri, int eventId) => $"{baseUri}/DeleteEventFile?eventId={eventId}";
            public static string CheckTimetableEventExists(string baseUri, string title, string meetingPassword, DateTime startDate, string startTime, long userId) => $"{baseUri}/checkTimetableEventExists?title={title}&meetingPassword={meetingPassword}&startDate={startDate}&startTime={startTime}&userId={userId}";

        }
        public static class TerminologyEditor
        {
            public static string TerminologyEditorInsert(string baseUri) => $"{baseUri}/TerminologyEditorInsert";
            public static string TerminologyEditorUpdate(string baseUri) => $"{baseUri}/TerminologyEditorUpdate";
            public static string TerminologyEditorDelete(string baseUri) => $"{baseUri}/TerminologyEditorDelete";
            public static string GetTerminologyEditor(string baseUri, int? id, long SchoolId) => $"{baseUri}/getTerminologyEditor?id={id}&schoolId={SchoolId}";
            public static string GetAllTerminologyEditor(string baseUri, long SchoolId) => $"{baseUri}/getAllTerminologyEditor?schoolId={SchoolId}";
            public static string CheckForTerminology(string baseUri, string term, int id) => $"{baseUri}/checkForTerminology?term={term}&id={id}";
        }
        public static class ContentLibrary
        {
            public static string GetPaginateContentLibrary(string baseUri, int schoolId, int pageNumber, int pageSize, string SearchString = "", string sortBy = "", string categoryIds = "") => $"{baseUri}/getPaginateContentLibrary?schoolId={schoolId}&pageNumber={pageNumber}&pageSize={pageSize}&SearchString={SearchString}&SortBy={sortBy}&CategoryIds={categoryIds}";
            public static string GetStudentContentLibrary(string baseUri, int schoolId, int pageNumber, int pageSize, string SearchString = "", string sortBy = "", string categoryIds = "") => $"{baseUri}/getStudentContentLibrary?schoolId={schoolId}&pageNumber={pageNumber}&pageSize={pageSize}&SearchString={SearchString}&SortBy={sortBy}&CategoryIds={categoryIds}";
            public static string GetContentLibrary(string baseUri, long SchoolId) => $"{baseUri}/getContentLibrary?schoolId={SchoolId}";
            public static string GetSchoolLevelsBySchoolId(string baseUri, long schoolId) => $"{baseUri}/getSchoolLevelsBySchoolId?schoolId={schoolId}";
            public static string GetContentProvider(string baseUri, long SchoolId, string divisionIds, string sortBy = "") => $"{baseUri}/getContentProvider?schoolId={SchoolId}&divisionIds={divisionIds}&sortBy={sortBy}";
            public static string InsertContentResource(string baseUri) => $"{baseUri}/insertContentResource";
            public static string UpdateContentResource(string baseUri) => $"{baseUri}/updateContentResource";
            public static string getContentResourceById(string baseUri, int id) => $"{baseUri}/getContentResourceById?Id={id}";
            public static string getSubjectsByContentId(string baseUri, int id) => $"{baseUri}/getSubjectsByContentId?Id={id}";
            public static string DeleteContentResourceData(string baseUri, int id, long userId) => $"{baseUri}/deleteContentResource?Id={id}&userId={userId}";
            public static string ChangeContentLibraryStatus(string baseUri, int contentId, bool status, long schoolId) => $"{baseUri}/changeContentLibraryStatus?resourceId={contentId}&status={status}&schoolId={schoolId}";
            public static string AssignContentResourceToSchool(string baseUri, int contentId, bool status, long schoolId) => $"{baseUri}/assignContentResourceToSchool?resourceId={contentId}&status={status}&schoolId={schoolId}";



        }
        public static class Quiz
        {
            public static string LogQuizTime(string baseUri, int id, int resourceId, string resourceType, long userId, bool IsStartQuiz) => $"{baseUri}/LogQuizTime?Id={id}&resourceId={resourceId}&resourceType={resourceType}&userId={userId}&isStartQuiz={IsStartQuiz}";
            public static string GetAllQuiz(string baseUri, long SchoolId) => $"{baseUri}/getallquiz?schoolId={SchoolId}";
            public static string GetQuiz(string baseUri, int quizId, long SchoolId) => $"{baseUri}/getquiz?quizId={quizId}&schoolId={SchoolId}";
            public static string GetQuizResourseDetail(string baseUri, int resourseId, string resourseType) => $"{baseUri}/GetQuizResourseDetail?resourseId={resourseId}&resourseType={resourseType}";
            public static string GetTaskQuizDetail(string baseUri, int quizId, long taskId, long SchoolId) => $"{baseUri}/getTaskQuizDetail?quizId={quizId}&taskId={taskId}&schoolId={SchoolId}";
            public static string GetQuizDetails(string baseUri, int quizId, int quizResourceId, long SchoolId) => $"{baseUri}/getquizdetails?quizId={quizId}&quizResourceId={quizResourceId}&schoolId={SchoolId}";
            public static string GetFilesByQuizId(string baseUri, int id) => $"{baseUri}/GetFilesByQuizId?quizId={id}";
            public static string GetFileByQuizFileId(string baseUri, int id) => $"{baseUri}/GetFileByQuizFileId?quizFileId={id}";
            public static string GetQuizQuestionAnswerFiles(string baseUri, long resourceId, string resourceType, long questionId, long studentId) => $"{baseUri}/GetQuizQuestionAnswerFiles?resourceId={resourceId}&resourceType={resourceType}&questionId={questionId}&studentId={studentId}";
            public static string QuizInsert(string baseUri) => $"{baseUri}/quizInsert";
            public static string QuizUpdate(string baseUri) => $"{baseUri}/quizUpdate";
            public static string QuizDelete(string baseUri) => $"{baseUri}/quizDelete";
            public static string UploadQuestionAnswerFiles(string baseUri) => $"{baseUri}/UploadQuestionAnswerFiles";
            public static string GetQuizObjectives(string baseUri, int quizId) => $"{baseUri}/GetQuizObjectives?quizId={quizId}";
            public static string GetQuizSubjectsGrade(string baseUri, int Id) => $"{baseUri}/GetQuizSubjectsGrade?quizId={Id}";
            public static string GetQuizReport(string baseUri, int Id, long userId) => $"{baseUri}/GetQuizReport?quizId={Id}&userId={userId}";
            public static string GetGroupQuizReport(string baseUri, int quizId, int groupQuizId) => $"{baseUri}/GetGroupQuizReport?quizId={quizId}&groupQuizId={groupQuizId}";
            public static string ActiveDeactiveQuiz(string baseUri) => $"{baseUri}/ActiveDeactiveQuiz";
            public static string DeleteQuizFile(string baseUri, int id) => $"{baseUri}/DeleteQuizFile?Id={id}";
            public static string DeleteQuestionAnswerFile(string baseUri, int id, long userId) => $"{baseUri}/DeleteQuestionAnswerFile?Id={id}&userId={userId}";
            public static string CloneQuizData(string baseUri, int quizId) => $"{baseUri}/cloneQuizData?quizId={quizId}";
            public static string ShareQuizData(string baseUri, int quizId, string teacherIds, long SharedBy) => $"{baseUri}/shareQuizData?quizId={quizId}&teacherIds={teacherIds}&SharedBy={SharedBy}";

            public static string GetAllQuizByUser(string baseUri, long id, bool isForm) => $"{baseUri}/getallquizbyuser?id={id}&isForm={isForm}";
            public static string GetPaginateQuizByUser(string baseUri) => $"{baseUri}/getpaginatequizbyuser";

            public static string GetAllQuizByUserAndSource(string baseUri, long id, long sourceId, string sourceType, bool isForm) => $"{baseUri}/getallquizbyuserandsource?id={id}&sourceId={sourceId}&sourceType={sourceType}&isForm={isForm}";
            public static string GetAllQuizByStudentId(string baseUri, long studentId) => $"{baseUri}/GetAllQuizByStudentId?studentId={studentId}";

            public static string GetQuizDetailsByQuizIdAndUserId(string baseUri, long userId, int quizId) => $"{baseUri}/GetQuizDetailsByQuizIdAndUserId?userId={userId}&quizId={quizId}";
            public static string GetAnswersByQuestionId(string baseUri, long userId, int questionId) => $"{baseUri}/GetAnswersByQuestionId?userId={userId}&questionId={questionId}";
            public static string GetQuizQuestionsCount(string baseUri, long userId, int quizId) => $"{baseUri}/GetQuizQuestionsCount?userId={userId}&quizId={quizId}";
            public static string GetSafeQuiz(string baseUri, int quizId) => $"{baseUri}/IsSafeQuiz?QuizId={quizId}";


        }
        public static class QuizQuestions
        {
            public static string GetQuizQuestionById(string baseUri, int id) => $"{baseUri}/getQuizQuestionsById?questionId={id}";
            public static string GetAllQuizQuestions(string baseUri) => $"{baseUri}/GetAllQuizQuestions";
            public static string GetFilteredQuizQuestions(string baseUri, string courseIds, string lstObjectives) => $"{baseUri}/GetFilteredQuizQuestions?courseIds={courseIds}&lstObjectives={lstObjectives}";
            public static string GetQuizQuestionsByQuizId(string baseUri, int id, bool IsTeacher) => $"{baseUri}/GetQuizQuestionsByQuizId?quizId={id}&IsTeacher={IsTeacher}&languageId={LocalizationHelper.CurrentSystemLanguageId}";

            public static string GetQuizQuestionsPaginationByQuizId(string baseUri, int id, int PageNumber, int PageSize) => $"{baseUri}/GetQuizQuestionsPaginationByQuizId?quizId={id}&PageNumber={PageNumber}&PageSize={PageSize}";
            public static string GetFilesByQuizQuestionsId(string baseUri, int id) => $"{baseUri}/GetFilesByQuizQuestionsId?quizQuestionId={id}";
            public static string GetFileByQuizQuestionFileId(string baseUri, int id) => $"{baseUri}/GetFileByQuizQuestionFileId?quizQuestionFileId={id}";
            public static string DeleteQuizQuestionFile(string baseUri, int id) => $"{baseUri}/DeleteQuizQuestionFile?Id={id}";
            public static string DeleteMTPResource(string baseUri, int id) => $"{baseUri}/DeleteMTPResource?Id={id}";
            public static string QuizQuestionsUpdate(string baseUri) => $"{baseUri}/QuizQuestionsUpdate";
            public static string QuizQuestionsInsert(string baseUri) => $"{baseUri}/QuizQuestionsInsert";
            public static string GetIdByQuestionType(string baseUri, string questionType) => $"{baseUri}/GetIdByQuestionType?questionType={questionType}";
            public static string ImportQuizQuestion(string baseUri) => $"{baseUri}/ImportQuizQuestion";
            public static string InsertUpdatePoolQuestions(string baseUri) => $"{baseUri}/InsertUpdatePoolQuestions";
            public static string UpdateQuizCorrectAnswerByQuizAnswerId(string baseUri) => $"{baseUri}/updateQuizCorrectAnswerByQuizAnswerId";
            public static string GetQuizAnswerByQuizQuestionId(string baseUri, int id) => $"{baseUri}/GetQuizAnswerByQuizQuestionId?quizQuestionId={id}";
            public static string QuizQuestionsDelete(string baseUri) => $"{baseUri}/QuizQuestionsDelete";
            public static string DeleteQuizQuestion(string baseUri, int QuizQuestionId, int QuizId) => $"{baseUri}/DeleteQuizQuestion?quizQuestionId={QuizQuestionId}&quizId={QuizId}";
            public static string GetQuestionObjectives(string baseUri, int questionId) => $"{baseUri}/GetQuestionObjectives?questionId={questionId}";
            public static string GetQuestionSubjects(string baseUri, int Id) => $"{baseUri}/GetQuestionSubjects?questionId={Id}";
            public static string GetQuestionCourses(string baseUri, int Id) => $"{baseUri}/GetQuestionCourses?questionId={Id}";
            public static string UpdateQuizAnswerData(string baseUri) => $"{baseUri}/insertquizanswer";
            public static string GetQuizResponseByUserId(string baseUri, int id, int userId, int studentId, int taskId) => $"{baseUri}/GetQuizResponseByUserId?quizId={id}&userId={userId}&studentId={studentId}&taskId={taskId}";
            public static string SortQuestionByOrder(string baseUri) => $"{baseUri}/SortQuestionByOrder";


        }

        #region My Files
        public static class Files
        {
            public static string GetFileTypes(string baseUri, string DocumentType) => $"{baseUri}/getfiletypes?DocumentType={DocumentType}";
            public static string GetStudentGroupsFiles(string baseUri, long userId) => $"{baseUri}/getStudentGroupsFiles?userId={userId}";
            public static string GetFilemanagementModules(string baseUri) => $"{baseUri}/getfilemanagementmodule";
            public static string GetFiles(string baseUri) => $"{baseUri}/getfiles";
            public static string GetFileById(string baseUri, int id) => $"{baseUri}/getfilebyid/{id}";
            public static string GetFilesByFolderId(string baseUri, int folderId, bool? isActive) => $"{baseUri}/getfilesbyfolderid/{folderId}/{isActive}";
            public static string GetFilesByModuleId(string baseUri, int schoolId, int moduleId, bool? isActive) => $"{baseUri}/getfilesbymoduleid/{schoolId}/{moduleId}/{isActive}";
            public static string GetFilesBySectionId(string baseUri, int sectionId, int moduleId, bool? isActive) => $"{baseUri}/getfilesbysectionid/{sectionId}/{moduleId}/{isActive}";
            public static string GetFilesBySchoolId(string baseUri, int schoolId, bool? isActive) => $"{baseUri}/getfilesbyschoolid/{schoolId}/{isActive}";
            public static string GetFilesByUserId(string baseUri, int userId, int moduleId, bool? isActive) => $"{baseUri}/getfilesbyuserid/{userId}/{moduleId}/{isActive}";
            public static string AddFile(string baseUri) => $"{baseUri}/addfile";
            public static string UpdateFile(string baseUri) => $"{baseUri}/updatefile";
            public static string DeleteFile(string baseUri, int id, long userId) => $"{baseUri}/deletefile/{id}/{userId}";
            public static string RenameFile(string baseUri, long fileId, string fileName, long updatedBy) => $"{baseUri}/FileRename?fileId={fileId}&fileName={fileName}&updatedBy={updatedBy}";
            public static string GetAllGroupFiles(string baseUri, int id) => $"{baseUri}/GetAllGroupFiles?schoolGroupId={id}";
            public static string ClearGroupFiles(string baseUri) => $"{baseUri}/ClearGroupFiles";
            public static string SaveCloudFiles(string baseUri) => $"{baseUri}/SaveCloudFiles";
            public static string FilesFolderBulkDelete(string baseUri) => $"{baseUri}/FilesFolderBulkDelete";
            public static string GetFilesByIds(string baseUri, string ids) => $"{baseUri}/getfilesbyids?Ids={ids}";
            public static string FilesFolderBulkCreate(string baseUri) => $"{baseUri}/FilesFolderBulkCreate";
            public static string GetGroupFileExplorer(string baseUri, int moduleId, long sectionId, long folderId, long studentUserId, bool isActive) => $"{baseUri}/GetGroupFileExplorer?moduleId={moduleId}&sectionId={sectionId}&folderId={folderId}&studentUserId={studentUserId}&isActive={isActive}";

        }

        public static class Folders
        {
            public static string GetFolders(string baseUri) => $"{baseUri}/getfolders";
            public static string GetFolderById(string baseUri, int id) => $"{baseUri}/getfolderbyid/{id}";
            public static string GetFolderTree(string baseUri, int folderId) => $"{baseUri}/getfoldertree/{folderId}";
            public static string GetFoldersByParentFolderId(string baseUri, int parentfolderId, int moduleId, bool? isActive) => $"{baseUri}/getfoldersbyparentfolderid/{parentfolderId}/{moduleId}/{isActive}";
            public static string GetFoldersByModuleId(string baseUri, int schoolId, int moduleId, bool? isActive) => $"{baseUri}/getfoldersbymoduleId/{schoolId}/{moduleId}/{isActive}";
            public static string GetFoldersBySectionId(string baseUri, int sectionId, int moduleId, bool? isActive) => $"{baseUri}/getfoldersbysectionId/{sectionId}/{moduleId}/{isActive}";
            public static string GetFoldersBySchoolId(string baseUri, int schoolId, bool? isActive) => $"{baseUri}/getfoldersbyschoolid/{schoolId}/{isActive}";
            public static string GetFoldersByUserId(string baseUri, int userId, int moduleId, bool? isActive) => $"{baseUri}/getfoldersbyuserid/{userId}/{moduleId}/{isActive}";
            public static string AddFolder(string baseUri) => $"{baseUri}/addfolder";
            public static string UpdateFolder(string baseUri) => $"{baseUri}/updatefolder";
            public static string DeleteFolder(string baseUri, long id, long userId) => $"{baseUri}/deletefolder/{id}/{userId}";
            public static string GetGroupPermission(string baseUri, int userId, int groupId) => $"{baseUri}/getgrouppermission/{userId}/{groupId}";
        }

        public static class GroupCourseTopic
        {
            public static string GetAllGroupCourseTopics(string baseUri) => $"{baseUri}/getallgroupcoursetopics";
            public static string GetGroupCourseTopicById(string baseUri, long id) => $"{baseUri}/getgroupcoursetopicbyid/{id}";
            public static string GetGroupCourseTopicByGroupId(string baseUri, long groupId, bool? isActive) => $"{baseUri}/getgroupcoursetopicbygroupid/{groupId}/{isActive}";
            public static string GetGroupCourseTopics(string baseUri, long? groupCourseTopicId, long? groupId, long? topicId, long? userId, bool? isSubTopic, bool? isActive) => $"{baseUri}/getgroupcoursetopics?groupCourseTopicId={groupCourseTopicId}&groupId={groupId}&topicId={topicId}&userId={userId}&isSubTopic={isSubTopic}&isActive={isActive}";

            public static string AddGroupCourseTopic(string baseUri) => $"{baseUri}/addgroupcoursetopic";
            public static string UpdateGroupCourseTopic(string baseUri) => $"{baseUri}/updategroupcoursetopic";
            public static string DeleteGroupCourseTopic(string baseUri, int id, long userId) => $"{baseUri}/deleteGroupCourseTopic/{id}/{userId}";
            public static string UpdateUnitSortOrder(string baseUri) => $"{baseUri}/updateunitsortorder";
        }
        #endregion

        #region Bookmark
        public static class Bookmarks
        {
            public static string GetBookmark(string baseUri, long id) => $"{baseUri}/GetBookmark?bookmarkId={id}";
            public static string AddBookmark(string baseUri) => $"{baseUri}/AddBookmark";
            public static string UpdateBookmark(string baseUri) => $"{baseUri}/UpdateBookmark";
            public static string GetBookmarks(string baseUri, long userId) => $"{baseUri}/GetBookmarks?userId={userId}";
            public static string GetBookmarkSchoolGroups(string baseUri, long bookmarkId) => $"{baseUri}/getBookmarkSchoolGroups?bookmarkId={bookmarkId}";
            public static string DeleteBookmark(string baseUri, long bookmarkId) => $"{baseUri}/DeleteBookmark?bookmarkId={bookmarkId}";
        }
        #endregion

        #region Plan Template
        public static class Template
        {
            public static string GetTemplates(string baseUri) => $"{baseUri}/gettemplates";
            public static string GetTemplatesBySchoolid(string baseUri, int schoolId, int? userId, bool isActive, string templateType, int? status) =>
                $"{baseUri}/gettemplatesbyschoolid?schoolid={schoolId}&userId={userId}&isActive={isActive}&templateType={templateType}&status={status}";
            public static string GetTemplateById(string baseUri, int id) => $"{baseUri}/gettemplatebyid/{id}";
            public static string GetTemplateDetail(string baseUri, int? templateId, int? schoolId, string templateType, string period, int? userId, bool? isActive, bool? includeTemplateField) => $"{baseUri}/gettemplatedetail?templateId={templateId}&schoolId={schoolId}&templateType={templateType}&period={period}&userId={userId}&isActive={isActive}&includeTemplateField={includeTemplateField}";
            public static string GetTemplateFieldById(string baseUri, int id) => $"{baseUri}/gettemplatefieldbyid/{id}";
            public static string GetTemplateFieldByTemplateId(string baseUri, int id, bool isActive, long UserId, int groupid) => $"{baseUri}/gettemplatefieldbytemplateid/{id}/{isActive}/{UserId}/{groupid}";
            public static string AddTemplate(string baseUri) => $"{baseUri}/addtemplate";
            public static string UpdateTemplate(string baseUri) => $"{baseUri}/updatetemplate";
            public static string DeleteTemplate(string baseUri, int id, int deletedBy) => $"{baseUri}/deletetemplate/{id}/{deletedBy}";
            public static string SaveTemplateImageData(string baseUri) => $"{baseUri}/SaveTemplateImageData";
            public static string GetCertificateMappingStudents(string baseUri, int id, int pageIndex, int pageSize, int templateId, string schoolGroupIds, string searchString) => $"{baseUri}/GetCertificateMappingStudents?userId={id}&pageIndex={pageIndex}&pageSize={pageSize}&templateId={templateId}&schoolGroupIds={schoolGroupIds}&searchString={searchString}";
            public static string SaveTemplateStudentMapping(string baseUri) => $"{baseUri}/SaveTemplateStudentMapping";
            public static string SaveCertificateAssignedColumns(string baseUri) => $"{baseUri}/SaveCertificateAssignedColumns";
            public static string GetStudentAssignedCertificates(string baseUri, long userId, short templateType) => $"{baseUri}/GetStudentAssignedCertificates?userId={userId}&templateType={templateType}";
            public static string GetTemplateFieldData(string baseUri, int templateId, long userId) => $"{baseUri}/GetTemplateFieldData?templateId={templateId}&userId={userId}";
            public static string SaveTemplateCertificateStatus(string baseUri) => $"{baseUri}/SaveTemplateCertificateStatus";
            public static string GetSchoolTemplateApproverDelegates(string baseUri, long schoolId, short templateTypeId, long? departmentId) =>
                            $"{baseUri}/GetSchoolTemplateApproverDelegates?schoolId={schoolId}&templateType={templateTypeId}&departmentId={departmentId}";
            public static string SaveCertificateApprovalTeacher(string baseUri) => $"{baseUri}/SaveCertificateApprovalTeacher";
            public static string GetTemplatePreviewData(string baseUri, int templateId, long schoolId) =>
                            $"{baseUri}/GetTemplatePreviewData?templateId={templateId}&schoolId={schoolId}";
            public static string GetCertificateColumns(string baseUri, int certificateColumnType) => $"{baseUri}/GetCertificateColumns?certificateColumnType={certificateColumnType}";

            public static string GetTimeTableList(string baseUri, long UserId, string SelectDate) =>
                            $"{baseUri}/GetTimeTableList?UserId={UserId}&SelectDate={SelectDate}";
            public static string GetGradeList(string baseUri, long Groupid, long SchoolId) =>
                            $"{baseUri}/GetGradeList?Groupid={Groupid}&SchoolId={SchoolId}";
            public static string GetTemplateFieldData(string baseUri) =>
                $"{baseUri}/GetTemplateFieldDataForMultipleUsers";

            public static string GetCertificateReportData(string baseUri, long schoolId) => $"{baseUri}/GetCertificateReportData?schoolId={schoolId}";
        }

        public static class PlanSchemeDetail
        {
            public static string GetAllPlanSchemeDetail(string baseUri) => $"{baseUri}/getallplanschemedetail";
            public static string GetPlanSchemeDetailBySchoolId(string baseUri, int schoolId, int? userId, int? status, bool? isActive,
            bool? SharedWithMe = null, bool? CreatedByMe = null,
            string MISGroupId = null, string OtherGroupId = null, string CourseId = null, string GradeId = null,
            int? pageIndex = null, int? PageSize = null, string searchString = "", string sortBy = "") =>
                $"{baseUri}/getplanschemedetailbyschoolid?schoolid={schoolId}&userId={userId}&status={status}&isActive={isActive}" +
                $"&SharedWithMe={SharedWithMe}&CreatedByMe={CreatedByMe}&MISGroupId={MISGroupId}&OtherGroupId={OtherGroupId}&CourseId={CourseId}&GradeId={GradeId}" +
                $"&pageIndex={pageIndex}&PageSize={PageSize}&searchString={searchString}&sortBy={sortBy}";
            public static string GetPlanSchemeDetailById(string baseUri, int id) => $"{baseUri}/getplanschemedetailbyid/{id}";

            public static string GetPlanSchemeDetail(string baseUri, int? planSchemeDetailId, int? templateId, int? schoolId, string templateType, int? status, int? userId, bool? isActive) => $"{baseUri}/getplanschemedetail?planSchemeDetailId={planSchemeDetailId}&templateId={templateId}&schoolId={schoolId}&templateType={templateType}&status={status}&userId={userId}&isActive={isActive}";

            public static string AddPlanSchemeDetail(string baseUri) => $"{baseUri}/addplanschemedetail";

            public static string SavePlanSchemeFields(string baseUri) => $"{baseUri}/SavePlanSchemeFields";
            public static string UpdatePlanSchemeDetail(string baseUri) => $"{baseUri}/updateplanschemedetail";
            public static string UpdatePlanSchemeStatus(string baseUri) => $"{baseUri}/updateplanschemestatus";
            public static string DeletePlanSchemeDetail(string baseUri, int id, int deletedBy) => $"{baseUri}/deleteplanschemedetail/{id}/{deletedBy}";
            public static string UpdateSharedLessonPlanDetial(string baseUri, long PlanSchemeId, string SelectedTeacherList, long UserId, string Operation) =>
                $"{baseUri}/UpdateSharedLessonPlanDetial?PlanSchemeId={PlanSchemeId}&SelectedTeacherList={SelectedTeacherList}&UserId={UserId}&Operation={Operation}";

            public static string GetSharedLessonPlanTeacherList(string baseUri, string PlanSchemeId) =>
                $"{baseUri}/GetSharedLessonPlanTeacherList?PlanSchemeId={PlanSchemeId}";

            public static string GetUnitStructure(string baseUri, string groupIds) =>
                $"{baseUri}/GetUnitStructure?groupIds={groupIds}";
            public static string GetLessonPlanFilterModule(string baseUri, long SchoolId, long TeacherId) =>
                $"{baseUri}/GetLessonPlanFilterModule?SchoolId={SchoolId}&TeacherId={TeacherId}";

            public static string GetPendingForApprovalLessonPlan(string baseUri, long SchoolId) =>
                $"{baseUri}/GetPendingForApprovalLessonPlan?SchoolId={SchoolId}";

            public static string GetPlanSchemeFields(string baseUri, long PlanSchemeId) => $"{baseUri}/GetPlanSchemeFields?planSchemeId={PlanSchemeId}";
            public static string GetUserGroups(string baseUri, long Userid) => $"{baseUri}/GetUserGroups?Userid={Userid}";
            public static string GetPlanSchemesByUnitId(string baseUri, long unitId, int? status) => $"{baseUri}/getplanschemesbyunitid?unitId={unitId}&status={status}";
        }
        #endregion

        #region School Notification
        public static class SchoolNotification
        {
            public static string GetSchoolDeploymentNotificationList(string baseUri) => $"{baseUri}/GetSchoolDeploymentNotificationList";
            public static string SaveDeploymentNotificationData(string baseUri) => $"{baseUri}/SaveDeploymentNotificationData";
        }
        #endregion

        #region Course
        public static class Course
        {
            public static string CourseCU(string baseUri)
            => $"{baseUri}/CourseCU";
            public static string IsCourseTitleUnique(string baseUri, string title) =>
                $"{baseUri}/IsCourseTitleUnique?title={title}";
            public static string GetCourses(string baseUri, long academicYearId, long? schoolId, int PageSize, int PageNumber, string SearchString) =>
                $"{baseUri}/GetCourses?curriculumId={academicYearId}&schoolId={schoolId}&PageSize={PageSize}&PageNumber={PageNumber}&SearchString={SearchString}";
            public static string GetCourseDetails(string baseUri, long courseId) =>
                $"{baseUri}/GetCourseDetails?courseId={courseId}";
            public static string CourseMappingCUD(string baseUri)
            => $"{baseUri}/CourseMappingCUD";
            public static string GetCourseMappings(string baseUri, long schoolId, long courseId, int pageNum = 1, int pageSize = 6, string searchString = "")
                => $"{baseUri}/GetCourseMappings?schoolId={schoolId}&courseId={courseId}&pageNum={pageNum}&pageSize={pageSize}&searchString={searchString}";
            public static string GetCourseMappingDetail(string baseUri, long groupId)
               => $"{baseUri}/GetCourseMappingDetail?groupId={groupId}";
            public static string GetGradeListBySchoolId(string baseUri, string SchoolIds, long TeacherId)
              => $"{baseUri}/GetGradeListBySchoolId?SchoolIds={SchoolIds}&TeacherId={TeacherId}";
            public static string SaveCrossSchoolPermission(string baseUri)
           => $"{baseUri}/SaveCrossSchoolPermission";
            public static string GetCrossSchoolPermissionList(string baseUri, long TeacherId, long SchoolId, long TCS_ID)
           => $"{baseUri}/GetCrossSchoolPermissionList?TeacherId={TeacherId}&SchoolId={SchoolId}&TCS_ID={TCS_ID}";
            public static string GetSchoolListByTeacherId(string baseUri) => $"{baseUri}/GetSchoolListByTeacherId";
            public static string GetSchoolTeachersBySchoolId(string baseUri) => $"{baseUri}/GeteGradeListPassByschoolIds";
            public static string IsGroupNameUnique(string baseUri, string title) =>
                $"{baseUri}/IsGroupNameUnique?title={title}";


            #region Unit

            public static string AddEditUnitMaster(string baseUri, TransactionModes mode, string createdBy) =>
            $"{baseUri}/AddEditUnitMaster?mode={mode}&createdBy={createdBy}";
            public static string GetUnitMasterDetails(string baseUri) =>
            $"{baseUri}/GetUnitMasterDetails";
            public static string GetUnitMasterDetailsByCourseId(string baseUri, long courseId) =>
            $"{baseUri}/GetUnitMasterDetailsByCourseId?courseId={courseId}";
            public static string GetUnitMasterDetailsByUnitId(string baseUri, long Id) =>
            $"{baseUri}/GetUnitMasterDetailsByUnitId?Id={Id}";
            public static string GetUnitDetailsType(string baseUri, long unitId) =>
            $"{baseUri}/GetUnitDetailsType?unitId={unitId}";

            public static string AddEditUnitDetailsType(string baseUri, long schoolId, string standardIds, string assessmentEvidenceIds, string assessmentLearningIds, string lessonIdsToDelete, long CreatedBy) =>
                $"{baseUri}/AddEditUnitDetailsType?schoolId={schoolId}&standardIds={standardIds}&assessmentEvidenceIds={assessmentEvidenceIds}&assessmentLearningIds={assessmentLearningIds}&lessonIdsToDelete={lessonIdsToDelete}&CreatedBy={CreatedBy}";

            public static string GetUnitTopicList(string baseUri, long MainSyllabusId) =>
                $"{baseUri}/GetUnitTopicList?MainSyllabusId={MainSyllabusId}";

            public static string GetUnitTopicStandardDetails(string baseUri, long UnitMasterId, long GroupId, long UnitId, long StandardBankId, int PageNumber, string SearchString, long SchoolId)
                => $"{baseUri}/GetUnitTopicStandardDetails?UnitMasterId={UnitMasterId}&GroupId={GroupId}&UnitId={UnitId}&StandardBankId={StandardBankId}&PageNumber={PageNumber}&SearchString={SearchString}&SchoolId={SchoolId}";

            public static string GetUnitWeekList(string baseUri, long SchoolId) =>
           $"{baseUri}/GetUnitWeekList?SchoolId={SchoolId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";

            public static string GetUnitCalendarByCourseId(string baseUri, long SchoolId, long courseId) =>
         $"{baseUri}/GetUnitCalendarByCourseId?SchoolId={SchoolId}&courseId={courseId}";
            public static string GetStandardDetailsById(string baseUri, long unitId) =>
                $"{baseUri}/GetStandardDetailsById?unitId={unitId}";

            public static string DeleteUnitStandardDetailsById(string baseUri, long Id)
                => $"{baseUri}/DeleteUnitStandardDetailsById?scmId={Id}";

            public static string GetUnitMasterByGroupId(string baseUri, long groupId) => $"{baseUri}/GetUnitMasterByGroupId?groupId={groupId}";

            public static string GetUnitsByGroup(string baseUri, long Id) =>
           $"{baseUri}/GetUnitsByGroup?Id={Id}";

            public static string GetSubUnitsById(string baseUri, long Id) =>
          $"{baseUri}/GetSubUnitsById?Id={Id}";
            public static string GetAttachmentDetailByUnitId(string baseUri, long UnitId) => $"{baseUri}/GetAttachmentDetailByUnitId?UnitId={UnitId}";
            #endregion

            #region Curriculum Map
            public static string GetCourseUnitMasterByCourseId(string baseUri, long schoolId, long courseId)
              => $"{baseUri}/GetCourseUnitMasterByCourseId?schoolId={schoolId}&courseId={courseId}";
            public static string GetUnitDetailsByCourseId(string baseUri, long courseId) =>
                $"{baseUri}/GetUnitDetailsByCourseId?courseId={courseId}";
            #endregion

            #region Course Catelog
            public static string GetCourseDetailsAndUnitList(string baseUri, long courseId, long teacherId) =>
                $"{baseUri}/GetCourseDetailsAndUnitList?courseId={courseId}&teacherId={teacherId}";
            #endregion

        }
        #endregion

        #region Standard
        public static class Standard
        {
            public static string GetStandardMasterDetailsById(string baseUri, Int64? StandardId) => $"{baseUri}/GetStandardMasterDetailsById?StandardId={StandardId}";
            public static string SaveUpdateStandardMasterDetails(string baseUri) => $"{baseUri}/SaveUpdateStandardMasterDetails";
            public static string GetStandardMasterList(string baseUri, int Acd_Id) => $"{baseUri}/GetStandardMasterList?Acd_Id={Acd_Id}";
            public static string GetParentList(string baseUri, Int64? SId) => $"{baseUri}/GetParentList?SId={SId}";

            public static string MainSyllabusIdList(string baseUri) => $"{baseUri}/MainSyllabusIdList";


            public static string DeleteStandardMaster(string baseUri) => $"{baseUri}/DeleteStandardMaster";

            public static string GetStandardGroupName(string baseUri, long UnitId, long SchoolId) => $"{baseUri}/GetStandardGroupName?UnitId={UnitId}&SchoolId={SchoolId}";

            public static string GetSchoolTerm(string baseUri, int acdid) => $"{baseUri}/GetSchoolTerm?acdid={acdid}";
            public static string BulkStandardUpload(string baseUri) => $"{baseUri}/BulkStandardUpload";

            public static string BulkStandardBankUpload(string baseUri) => $"{baseUri}/BulkStandardBankUpload";
            public static string GetStandardBankList(string baseUri, long SchoolId, long SB_Id) => $"{baseUri}/GetStandardBankList?SchoolId={SchoolId}&SB_Id={SB_Id}";
            public static string AddEditStandardBank(string baseUri, string TransMode, string EditType) => $"{baseUri}/AddEditStandardBank?TransMode={TransMode}&EditType={EditType}";
        }
        #endregion
        #region AssessmentEvidence
        public static class AssessmentEvidence
        {
            public static string SaveUpdateAssessmentEvidenceDetails(string baseUri, long UserId) => $"{baseUri}/SaveUpdateAssessmentEvidenceDetails?UserId={UserId}";
            public static string GetAssessmentEvidenceList(string baseUri, long UnitId) => $"{baseUri}/GetAssessmentEvidenceList?UnitId={UnitId}";

            public static string GetAssessmentEvidenceDetailsById(string baseUri, long AssessmentId) => $"{baseUri}/GetAssessmentEvidenceDetailsById?AssessmentId={AssessmentId}";
        }
        #endregion

        #region AssessmentLearning
        public static class AssessmentLearning
        {
            public static string SaveUpdateAssessmentLearningDetails(string baseUri, long UserId) => $"{baseUri}/SaveUpdateAssessmentLearningDetails?UserId={UserId}";
            public static string GetAssessmentLearningList(string baseUri, long UnitId) => $"{baseUri}/GetAssessmentLearningList?UnitId={UnitId}";

            public static string GetAssessmentLearningDetailsById(string baseUri, long AssessmentId) => $"{baseUri}/GetAssessmentLearningDetailsById?AssessmentId={AssessmentId}";
        }
        #endregion

        #region Lesson
        public static class Lesson
        {
            public static string GetUnitDetails(string baseUri) => $"{baseUri}/GetUnitDetails";
            public static string GetStandardDetails(string baseUri) => $"{baseUri}/GetStandardDetails";
            public static string AddEditLesson(string baseUri) => $"{baseUri}/AddEditLesson";
            public static string GetLessonDetails(string baseUri) => $"{baseUri}/GetLessonDetails";
            public static string GetTopicLessonDetail(string baseUri, long CourseId) => $"{baseUri}/GetTopicLessonDetail?CourseId={CourseId}";
            public static string GetUnitBasedonCourse(string baseUri, long CourseId) => $"{baseUri}/GetUnitBasedonCourse?CourseId={CourseId}";

            public static string SchoolUnitDetailsTypeCD(string baseUri) => $"{baseUri}/SchoolUnitDetailsTypeCD";
            public static string GetSchoolUnitDetailTypes(string baseUri, long SchoolId, int divisionId) => $"{baseUri}/GetSchoolUnitDetailTypes?SchoolId={SchoolId}&divisionId={divisionId}";
            public static string GetSchoolUnitDetailType(string baseUri) => $"{baseUri}/GetSchoolUnitDetailType";
            public static string SaveSchoolUnitDetailType(string baseUri) => $"{baseUri}/SaveSchoolUnitDetailType";

        }
        #endregion

        #region Attachment
        public static class AttachmentAPI
        {
            public static string DeleteAttachment(string baseUri, long id) => $"{baseUri}/DeleteAttachment?id={id}";
            public static string GetAttachment(string baseUri, long masterId = 0, string masterKey = "", long? attachmentId = null) =>
                $"{baseUri}/GetAttachment?masterId={masterId}&masterKey={masterKey}&attachmentId={attachmentId}";
        }
        #endregion

        #region StandardDetails
        public static class StandardDetails
        {
            public static string GetStandardDetailsById(string baseUri, Int64? StandardDetailsId) => $"{baseUri}/GetStandardDetailsById?StandardDetailsId={StandardDetailsId}";
            public static string SaveUpdateStandardDetails(string baseUri) => $"{baseUri}/SaveUpdateStandardDetails";
            public static string GetStandardDetailsList(string baseUri) => $"{baseUri}/GetStandardDetailsList";
            public static string DeleteStandardDetails(string baseUri) => $"{baseUri}/DeleteStandardDetails";
        }
        #endregion

        #region Behaviour
        public static class Incident
        {
            public static string GetIncidentList(string baseUri, long schoolId, long curriculumId, int month, bool isFA) =>
                $"{baseUri}/GetIncidentList?schoolId={schoolId}&curriculumId={curriculumId}&month={month}&isFA={isFA}";
            public static string GetIncident(string baseUri, long IncidentId) =>
                $"{baseUri}/GetIncident?IncidentId={IncidentId}";
            public static string GetIncidentChart(string baseUri, long schoolId, long academicYearId, int month, bool isCategory) =>
                $"{baseUri}/GetIncidentChart?schoolId={schoolId}&academicYearId={academicYearId}&month={month}&isCategory={isCategory}";
            public static string GetStudentByIncidentId(string baseUri, long incidentId) =>
                $"{baseUri}/GetStudentByIncidentId?incidentId={incidentId}";
            public static string GetIncidentEntryCUD(string baseUri) =>
                $"{baseUri}/GetIncidentEntryCUD";
            public static string GetSubCategoriesByCategoryId(string baseUri, long categoryId, string BSU_ID, string GRD_ID) =>
                $"{baseUri}/GetSubCategoriesByCategoryId?categoryId={categoryId}&BSU_ID={BSU_ID}&GRD_ID={GRD_ID}";

            public static string GetMeritDetails(string baseUri, int acdId, string schoolId, long studentId) =>
                $"{baseUri}/GetMeritDetails?acdId={acdId}&schoolId={schoolId}&studentId={studentId}";
            public static string GetMeritCategoryByStudent(string baseUri, int acdId, string schoolId, long studentId) =>
                $"{baseUri}/GetMeritCategoryByStudent?acdId={acdId}&schoolId={schoolId}&studentId={studentId}";

            public static string InsertMeritDemerit(string baseUri, string schoolId, int academicId) =>
                $"{baseUri}/InsertMeritDemerit?schoolId={schoolId}&academicId={academicId}";

            #region IncidentAction
            public static string GetBehaviourAction(string baseUri, long incidentId, long studentId) =>
                $"{baseUri}/GetBehaviourAction?incidentId={incidentId}&studentId={studentId}";
            public static string GetBehaviourActionFollowups(string baseUri, long incidentId, long actionId) =>
                $"{baseUri}/GetBehaviourActionFollowups?incidentId={incidentId}&actionId={actionId}";
            public static string GetFollowUpDesignations(string baseUri, long schoolId, long incidentId, long UserId) =>
                $"{baseUri}/GetFollowUpDesignations?schoolId={schoolId}&incidentId={incidentId}&UserId={UserId}";
            public static string GetFollowUpStaffs(string baseUri, long schoolId, long designationId) =>
                $"{baseUri}/GetFollowUpStaffs?schoolId={schoolId}&designationId={designationId}";
            public static string ActionCUD(string baseUri) =>
                $"{baseUri}/ActionCUD";
            public static string ActionFollowUpCUD(string baseUri) =>
                $"{baseUri}/ActionFollowUpCUD";
            public static string GetSchoolTeachersBySchoolId(string baseUri, long schoolId) =>
                $"{baseUri}/GetSchoolTeachersBySchoolId?SchoolId={schoolId}";
            #endregion
        }
        #endregion
        public static string GetGradeDetails(string baseUri) => $"{baseUri}/GetGradeDetails";

        public static class AssessmentConfig
        {
            public static string AddEditAssessmentConfig(string baseUri, string Description, string MasterId, long GTM_Id, long SchoolId) => $"{baseUri}/AddEditAssessmentConfig?Description={Description}&MasterId={MasterId}&GTM_Id={GTM_Id}&SchoolId={SchoolId}";

            public static string AddEditGradeSlab(string baseUri, string Description, string MasterIds, long GradeSlabMasterId, long Acd_Id) => $"{baseUri}/AddEditGradeSlab?Description={Description}&MasterIds={MasterIds}&GradeSlabMasterId={GradeSlabMasterId}&Acd_Id={Acd_Id}";


            public static string GetAssessmentConfigList(string baseUri, long Acd_Id) => $"{baseUri}/GetAssessmentConfigList?Acd_Id={Acd_Id}";

            public static string GetAssessmentConfigMasterList(string baseUri, long SchoolId) => $"{baseUri}/GetAssessmentConfigMasterList?SchoolId={SchoolId}";




            public static string GetGradeSlabList(string baseUri, long Acd_Id) => $"{baseUri}/GetGradeSlabList?Acd_Id={Acd_Id}";
            public static string GetGradeSlabMasterList(string baseUri, long SchoolId) => $"{baseUri}/GetGradeSlabMasterList?SchoolId={SchoolId}";
        }

        #region Assessment Configuration
        public static class AssessmentConfiguration
        {
            public static string GetCourseListByGrade(string baseUri, string SchoolGradeIds) =>
                 $"{baseUri}/GetCourseListByGrade?SchoolGradeIds={SchoolGradeIds}";
            public static string GetAssessmentConfigDetail(string baseUri, long AssessmentMasterId, long schoolId) =>
                 $"{baseUri}/GetAssessmentConfigDetail?AssessmentMasterId={AssessmentMasterId}&schoolId={schoolId}";
            public static string GetAssessmentConfigPagination(string baseUri, long schoolId, int pageNum, int pageSize, string searchString) =>
                $"{baseUri}/GetAssessmentConfigPagination?schoolId={schoolId}&pageNum={pageNum}&pageSize={pageSize}&searchString={searchString}";
            public static string GetAssessmentColumnDetail(string baseUri, long AssessmentMasterId) =>
              $"{baseUri}/GetAssessmentColumnDetail?AssessmentMasterId={AssessmentMasterId}";
            public static string SaveAssessmentConfigDetail(string baseUri) =>
              $"{baseUri}/SaveAssessmentConfigDetail";
        }
        #endregion Assessment Configuration

        #region Course Catalogue
        public class CourseCatalogue
        {
            public static string GetCatalogueInfoUsingGroupId(string baseUri, long groupId) =>
                 $"{baseUri}/GetCatalogueInfoUsingGroupId?groupId={groupId}";
            public static string GetCourseCatalogueInformationByTeacher(string baseUri, long memberId, bool isStudent) =>
                $"{baseUri}/GetCourseCatalogueInformationByTeacher?memberId={memberId}&isStudent={isStudent}";
            public static string GetCatalogueUnits(string baseUri, long courseId, long memberId, bool isStudent) =>
                $"{baseUri}/GetCatalogueUnits?courseId={courseId}&memberId={memberId}&isStudent={isStudent}";
        }
        #endregion

        #region Behaviour Setup
        public static class BehaviourSetup
        {
            public static string GetSubCategoryList(string baseUri, long MainCategoryId, long SchoolId) =>
                $"{baseUri}/GetSubCategoryList?MainCategoryId={MainCategoryId}&SchoolId={SchoolId}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string GetDesignations(string baseUri, long schoolId) => $"{baseUri}/GetDesignations?schoolId={schoolId}";
            public static string GetDesignationsRoutings(string baseUri, long schoolId, long? designationFrom) => $"{baseUri}/GetDesignationsRoutings?schoolId={schoolId}&designationFrom={designationFrom}";
            public static string DesignationBySchoolCUD(string baseUri) => $"{baseUri}/DesignationBySchoolCUD";

            public static string SaveSubCategory(string baseUri, string DATAMODE) => $"{baseUri}/SaveSubCategory?DATAMODE={DATAMODE}";

            #region Certificate Schedule
            public static string GetCertificateSchedulings(string baseUri, long? CertificateSchedulingId, long? curriculumId, long? schoolId, int? scheduleType = null) =>
                $"{baseUri}/GetCertificateSchedulings?CertificateSchedulingId={CertificateSchedulingId}&academicYear={curriculumId}&schoolId={schoolId}&scheduleType={scheduleType}&languageId={LocalizationHelper.CurrentSystemLanguageId}";
            public static string CertificateSchedulingCUD(string baseUri) => $"{baseUri}/CertificateSchedulingCUD";
            #endregion
        }
        #endregion

        #region Division
        public static class Division
        {
            public static string SaveDivisionDetails(string baseUri, string DATAMODE) => $"{baseUri}/SaveDivisionDetails?DATAMODE={DATAMODE}";
            public static string GetDivisionDetails(string baseUri, long BSU_ID, int CurriculumId) => $"{baseUri}/GetDivisionDetails?BSU_ID={BSU_ID}&CurriculumId={CurriculumId}";
            public static string GetReportingTermDetail(string baseUri, long ReportingTermId, long SchoolId) => $"{baseUri}/GetReportingTermDetail?ReportingTermId={ReportingTermId}&SchoolId={SchoolId}";
            public static string GetReportingSubTermDetail(string baseUri, long ReportingTermId, long ReportingSubTermId) => $"{baseUri}/GetReportingSubTermDetail?ReportingTermId={ReportingTermId}&ReportingSubTermId={ReportingSubTermId}";
            public static string SaveReportingTermDetail(string baseUri) => $"{baseUri}/SaveReportingTermDetail";
            public static string SaveReportingSubTermDetail(string baseUri) => $"{baseUri}/SaveReportingSubTermDetail";
            public static string LockUnlockTerm(string baseUri, long ReportingTermId, bool IsLock) => $"{baseUri}/LockUnlockTerm?ReportingTermId={ReportingTermId}&IsLock={IsLock}";
            public static string LockUnlockSubTerm(string baseUri, long ReportingSubTermId, bool IsLock) => $"{baseUri}/LockUnlockSubTerm?ReportingSubTermId={ReportingSubTermId}&IsLock={IsLock}";
        }
        #endregion

        #region Attendance Setting
        public static class AttendanceSetting
        {
            public static string GetParameterSettingList(string baseUri, long BSU_ID) => $"{baseUri}/GetParameterSettingList?BSU_ID={BSU_ID}";
            public static string SaveParameterSetting(string baseUri, string DATAMODE) => $"{baseUri}/SaveParameterSetting?DATAMODE={DATAMODE}";

            public static string LeaveApprovalPermissionCU(string baseUri) => $"{baseUri}/LeaveApprovalPermissionCU";
            public static string AddUpdatePermission(string baseUri, string schoolId) => $"{baseUri}/AddUpdatePermission?schoolId={schoolId}";
            public static string GetLeaveApprovalPermission(string baseUri, long ACD_ID, long schoolId, int divisionId) => $"{baseUri}/GetLeaveApprovalPermission?ACD_ID={ACD_ID}&schoolId={schoolId}&divisionId={divisionId}";
            public static string GetAttendanceType(string baseUri, long BSU_ID) => $"{baseUri}/GetAttendanceType?BSU_ID={BSU_ID}";
            public static string SaveAttendanceType(string baseUri, string DATAMODE) => $"{baseUri}/SaveAttendanceType?DATAMODE={DATAMODE}";

            public static string GetAttendancePeriodList(string baseUri, long gradeId) =>
           $"{baseUri}/GetAttendancePeriod?gradeId={gradeId}";
            public static string GetAttendanceTypeListBYId(string baseUri, long AttendanceConfigurationTypeID) =>
           $"{baseUri}/GetAttendancePeriod?AttendanceConfigurationTypeID={AttendanceConfigurationTypeID}";


            public static string AddUpdateAttendancePeriod(string baseUri, long schoolId, long academicId, string gradeId, long CreatedBy) =>
                 $"{baseUri}/AddUpdateAttendancePeriod?schoolId={schoolId}&academicId={academicId}&gradeId={gradeId}&CreatedBy={CreatedBy}";
            #region Attendance Calendar
            public static string GetGradeAndSectionList(string baseUri, long SchoolId) => $"{baseUri}/GetGradeAndSectionList?SchoolId={SchoolId}";
            public static string GetSchoolWeekEnd(string baseUri, long BSU_ID) => $"{baseUri}/GetSchoolWeekEnd?BSU_ID={BSU_ID}";
            public static string SaveCalendarEvent(string baseUri, string DATAMODE) => $"{baseUri}/SaveCalendarEvent?DATAMODE={DATAMODE}";
            public static string GetCalendarDetail(string baseUri, long SchoolId, long SCH_ID, bool IsListView) => $"{baseUri}/GetCalendarDetail?SchoolId={SchoolId}&SCH_ID={SCH_ID}&IsListView={IsListView}";
            public static string GetAcademicYearDetail(string baseUri, long SchoolId) => $"{baseUri}/GetAcademicYearDetail?SchoolId={SchoolId}";
            #endregion Attendance Calendar
        }
        #endregion Attendance Setting

        #region Grade Book
        public static class GradeBook
        {
            public static string GetTeacherGradeBook(string baseUrl, long SchoolGroupId, long teacherId) => $"{baseUrl}/GetTeacherGradeBook?SchoolGroupId={SchoolGroupId}&teacherId={teacherId}";
            public static string CreateNewRow(string baseUrl) => $"{baseUrl}/TeacherGradebookCreateNewRow";
            public static string InternalAssessmentCU(string baseUrl) => $"{baseUrl}/InternalAssessmentCU";
            public static string GetAssignmentQuizScore(string baseUrl, long SchoolGroupId, bool isAssignment) => $"{baseUrl}/GetAssignmentQuizScore?SchoolGroupId={SchoolGroupId}&isAssignment={isAssignment}";
            public static string GetInternalAssessments(string baseUrl, long SchoolGroupId, long teacherId) => $"{baseUrl}/GetInternalAssessments?SchoolGroupId={SchoolGroupId}&teacherId={teacherId}";
            public static string GetStandardExaminations(string baseUrl, long SchoolGroupId, int assessmentTypeId) => $"{baseUrl}/GetStandardExaminations?SchoolGroupId={SchoolGroupId}&assessmentTypeId={assessmentTypeId}";
            public static string GetProgressTracker(string baseUrl, long schoolGroupId, DateTime? startDate, DateTime? endDate) => $"{baseUrl}/GetProgressTracker?schoolGroupId={schoolGroupId}";
            public static string GetSubInternalAssessment(string baseUrl, long internalAssessmentId) =>
                $"{baseUrl}/GetSubInternalAssessment?internalAssessmentId={internalAssessmentId}";
            public static string GetInternalAssessmentScoreByAssessmentId(string baseUrl, long internalAssessmentId) =>
                $"{baseUrl}/GetInternalAssessmentScoreByAssessmentId?internalAssessmentId={internalAssessmentId}";
        }
        #endregion Grade Book

        #region Grade Book Setup
        public static class GradeBookSetup
        {
            public static string SaveGradeBookForm(string baseUrl) => $"{baseUrl}/SaveGradeBookForm";
            public static string DeleteGradeBookDetail(string baseUrl) => $"{baseUrl}/DeleteGradeBookDetail";
            public static string GetGradeBookDetail(string baseUrl, long gradeBookId) => $"{baseUrl}/GetGradeBookDetail?gradeBookId={gradeBookId}";
            public static string GetGradeBookDetailPagination(string baseUri, int curriculumId, long schoolId, int pageNum, int pageSize, string searchString, string GradeIds, string CourseIds, string sortBy) =>
              $"{baseUri}/GetGradeBookDetailPagination?curriculumId={curriculumId}&schoolId={schoolId}&pageNum={pageNum}&pageSize={pageSize}&searchString={searchString}&GradeIds={GradeIds}&CourseIds={CourseIds}&sortBy={sortBy}";
            public static string GetGradeAndCourseList(string baseUri, int curriculumId, long schoolId) => $"{baseUri}/GetGradeAndCourseList?curriculumId={curriculumId}&schoolId={schoolId}";
            public static string GradebookFormulaCU(string baseUri) => $"{baseUri}/GradebookFormulaCU";
            public static string GetGradebookFormulas(string baseUri, long schoolId) => $"{baseUri}/GetGradebookFormulas?schoolId={schoolId}";
            public static string GetGradebookFormulaDetailById(string baseUri, long FormulaId) => $"{baseUri}/GetGradebookFormulaDetailById?FormulaId={FormulaId}";
            public static string GetGradeListExceptGradebookGrade(string baseUri, long schoolId, long GradebookId) => $"{baseUri}/GetGradeListExceptGradebookGrade?schoolId={schoolId}&GradebookId={GradebookId}";
            public static string GetGradebookRuleSetups(string baseUri, string assessmentIds) => $"{baseUri}/GetGradebookRuleSetups?assessmentIds={assessmentIds}";
            public static string ProcessingRuleSetupCU(string baseUri) => $"{baseUri}/ProcessingRuleSetupCU";
            public static string DeleteRuleProcessingSetup(string baseUri, long ruleSetupId) => $"{baseUri}/DeleteRuleProcessingSetup?ruleSetupId={ruleSetupId}";
            public static string GetGradingTemplatesByIds(string baseUri, string ids) => $"{baseUri}/GetGradingTemplatesByIds?ids={ids}";
            public static string ValidateGradeAndCourse(string baseUri, string courseIds, string gradeIds) => $"{baseUri}/ValidateGradeAndCourse?courseIds={courseIds}&gradeIds={gradeIds}";

        }
        #endregion Grade Book Setup

        #region SchoolReports
        public static class SchoolReports
        {
            public static string DeleteSchoolReport(string baseUri, int id, long schoolId) =>
                $"{baseUri}/DeleteSchoolReport?id={id}&schoolId={schoolId}";
            public static string SchoolReportCU(string baseUri)
                => $"{baseUri}/SchoolReportCU";
            public static string GetReportLists(string baseUri, long schoolId)
                => $"{baseUri}/GetReportLists?schoolId={schoolId}";
            public static string GetReportDetail(string baseUri, long reportDetailId, bool isCommonReport, long schoolId, bool isPreview)
                => $"{baseUri}/GetReportDetail?reportDetailId={reportDetailId}&isCommonReport={isCommonReport}&schoolId={schoolId}&isPreview={isPreview}";
            public static string GetReportFilter(string baseUri)
                => $"{baseUri}/GetReportFilter";
            public static string GetReportData(string baseUri)
                => $"{baseUri}/GetReportData";
        }
        #endregion

        #region Charts Dashboard
        public static class ChartsDashboard
        {
            public static string GetStudentProgress(string baseUri, long bsuid) => $"{baseUri}/GetFilters?bsUid={bsuid}";
            public static string GetChartAndPercentage(string baseUri, long bsuid, string academicYear, string subject, string classId, string assessment, string teacher) => $"{baseUri}/GetStudentProgressTrackerCharts?bsUid={bsuid}&academicYear={academicYear}&subject={subject}&classId={classId}&assessment={assessment}&teacher={teacher}";
            public static string GetAllStudents(string baseUri, long bsuid, string classId, string assessment, string academicYear) => $"{baseUri}/GetStudentFilters?bsUid={bsuid}&classId={classId}&assessment={assessment}&academicYear={academicYear}";
            public static string GetStudentAssessmentPrediction(string baseUri, long bsuid, string classId, string assessment, string academicYear, string studentId) => $"{baseUri}/GetStudentAssessmentPredictionCharts?bsUid={bsuid}&classId={classId}&assessment={assessment}&academicYear={academicYear}&studentId={studentId}";
            public static string GetAllAssessment(string baseUri, long bsuid, string classId, string assessment, string academicYear, string studentId) => $"{baseUri}/GetAllAssessmentData?bsUid={bsuid}&classId={classId}&assessment={assessment}&academicYear={academicYear}&studentId={studentId}";
            #endregion
        }
        public static class Setting
        {
            public static string GetEnableDisableNotificationListForParent(string baseUri, long parentId) => $"{baseUri}/GetEnableDisableNotificationListForParent?parentId={parentId}";

            public static string SaveNotificationSettingForParent(string baseUri, long parentId) => $"{baseUri}/SaveNotificationSettingForParent?parentId={parentId}";
        }
        public static class ForgotPasswordUrl
        {
            public static string GetPostForgetUserNameURI(string baseUri) => $"{baseUri}api/user/PostForgetUserName";

            public static string GetValidateUserNameURI(string baseUri) => $"{baseUri}api/user/GetValidateUserName";

            public static string PasswordChangeURI(string baseUri) => $"{baseUri}api/password/forgot";
        }
        public static class Survey
        {
        }


        #region
        public static class MigrationJob
        {
            public static string GetMigrationDetailList(string baseUri, long SchoolId) => $"{baseUri}/GetMigrationDetailList?SchoolId={SchoolId}";
            //public static string GetMigrationDetailList(string baseUri, long SchoolId) => $"{baseUri}/GetMigrationDetailList?SchoolId={SchoolId}";

             public static string GetSyncDetailbyId(string baseUri) => $"{baseUri}/GetSyncDetailbyId";

            //public static string GetSyncDetailbyId(string baseUri, long id, long SchoolId) => $"{baseUri}/GetSyncDetailbyId?id={id}&SchoolId={SchoolId}";
            public static string GetMigrationDetailCountList(string baseUri) => $"{baseUri}/GetMigrationDetailCount";
        }

        #endregion
    }
}

