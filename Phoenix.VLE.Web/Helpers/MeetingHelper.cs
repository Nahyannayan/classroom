﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System.Globalization;
using System.Xml;

namespace Phoenix.VLE.Web.Helpers
{
    public static class MeetingHelper
    {
        public static HttpClient httpClient;
        private static string _moderatorPassword;
        private static string _baseURL;
        private static string _meetingSecret;
        private static string _clientBaseUrl;

        static MeetingHelper()
        {
            _meetingSecret = ConfigurationManager.AppSettings["MeetingSecret"].ToString();
            _moderatorPassword = ConfigurationManager.AppSettings["ModeratorPassword"].ToString();
            _baseURL = ConfigurationManager.AppSettings["MetingBaseURL"].ToString();
            _clientBaseUrl = ConfigurationManager.AppSettings["ClientBaseURL"].ToString();
            httpClient = new HttpClient();
        }
        public static MeetingResponse CreateNewMeeting(string meetingName, short meetingDuration)
        {
            var newMeetingId = DateTime.Now.Ticks.ToString();
            string createAPIRequest = "clientURL=" + HttpUtility.UrlEncode(_clientBaseUrl)
           + "&allowStartStopRecording=false&autoStartRecording=false&meetingID=" + newMeetingId + "&name=" + HttpUtility.UrlEncode(meetingName) +
           "&record=false&moderatorPW=" + _moderatorPassword + "&logoutURL=" + HttpUtility.UrlEncode(HttpContext.Current.Request.UrlReferrer.AbsoluteUri)
           + "&duration=" + meetingDuration;
            string apiCall = "create" + createAPIRequest + _meetingSecret;
            var checksum = CommonHelper.Hash(apiCall).ToLower();
            string moderatorURL = _baseURL + "create" + "?" + createAPIRequest + "&checksum=" + checksum;
            return GetMeetingAPIResponse(moderatorURL);
        }

        public static string GetMeetingURL(long meetingCreatorId, string meetingId, string attendeePassword)
        {
            bool isCreatedByMe = SessionHelper.CurrentSession.Id == meetingCreatorId;
            bool isRunning = isCreatedByMe || IsMeetingRunning(meetingId);
            if (isRunning)
            {
                string joinApIRequest = string.Format("clientURL=" + HttpUtility.UrlEncode(_clientBaseUrl) +
                        "&fullName={0}&meetingID={1}&redirect=true&password={2}", HttpUtility.UrlEncode(SessionHelper.CurrentSession.FirstName), meetingId, isCreatedByMe ? _moderatorPassword : attendeePassword);
                string apiCall = "join" + joinApIRequest + _meetingSecret;//generate api call i.e. name of api + request + shared secret
                string checkSum = CommonHelper.Hash(apiCall).ToLower();
                return string.Format(_baseURL + "{0}?" + joinApIRequest + "&checksum=" + checkSum, "join");
            }
            return string.Empty;
        }

        public static bool IsMeetingRunning(string meetingId)
        {
            string meetingURL = $"meetingID={meetingId}";
            string apiCall = "isMeetingRunning" + meetingURL + _meetingSecret;
            string checkSum = CommonHelper.Hash(apiCall).ToLower();
            MeetingResponse meetingResponse = GetMeetingAPIResponse($"{_baseURL}isMeetingRunning?{meetingURL}&checksum={checkSum}");
            return meetingResponse.Returncode.Equals("success", StringComparison.OrdinalIgnoreCase) && meetingResponse.IsRunning;
        }

        public static MeetingResponse GetMeetingInfo(string meetingId)
        {
            string meetingURL = $"meetingID={meetingId}&password={_moderatorPassword}";
            string apiCall = "getMeetingInfo" + meetingURL + _meetingSecret;
            string checkSum = CommonHelper.Hash(apiCall).ToLower();
            MeetingResponse meetingResponse = GetMeetingAPIResponse($"{_baseURL}getMeetingInfo?{meetingURL}&checksum={checkSum}");
            return meetingResponse;
        }

        public static MeetingResponse GetMeetingAPIResponse(string url)
        {
            using (WebClient webClient = new WebClient())
            {
                var data = webClient.DownloadString(url);
                XmlSerializer x = new XmlSerializer(typeof(MeetingResponse));
                MeetingResponse response = (MeetingResponse)x.Deserialize(new StringReader(data));
                return response;
            }
        }

        private static DateTime GetDateTimeFromString(string date, string time)
        {
            TimeSpan timeSpan = DateTime.ParseExact(time, "hh:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
            return Convert.ToDateTime(date).Add(timeSpan);
        }

        public static WebExMeeting CreateWebExMeeting(BBBMeetingEdit meetingEdit)
        {
            string _webExMeetingId = ConfigurationManager.AppSettings["WebExId"];
            string _webExPassword = ConfigurationManager.AppSettings["WebExPassword"];
            string _webExSiteName = ConfigurationManager.AppSettings["WebExSiteName"];
            TimeSpan timeSpan = DateTime.ParseExact(meetingEdit.StartTime, "hh:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
            DateTime meetingDate = Convert.ToDateTime(meetingEdit.StartDate).Add(timeSpan);
            string meetingXML = @"<?xml version='1.0' encoding='UTF-8'?>
<serv:message xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
    <header>
        <securityContext>
            <siteName>##SiteName</siteName>
            <webExID>##WebExId</webExID>
            <password>##WebExPassword</password>   
        </securityContext>
    </header>
    <body>
        <bodyContent
            xsi:type='java:com.webex.service.binding.meeting.CreateMeeting'>
            <accessControl>
                <meetingPassword>##MeetingPassword</meetingPassword>
            </accessControl>
            <metaData>
                <confName>##MeetingName</confName>
                <agenda>##MeetingName</agenda>
            </metaData>
            <enableOptions>
                <chat>true</chat>
                <poll>true</poll>
                <audioVideo>true</audioVideo>
                <supportE2E>TRUE</supportE2E>
                <autoRecord>TRUE</autoRecord>
            </enableOptions>
            <schedule>
                <startDate>##StartDate</startDate>
                <openTime>900</openTime>
                <duration>##Duration</duration>
                </schedule>
          
        </bodyContent>
    </body>
</serv:message>";

            meetingXML = meetingXML.Replace("##SiteName", _webExSiteName);
            meetingXML = meetingXML.Replace("##WebExId", _webExMeetingId);
            meetingXML = meetingXML.Replace("##WebExPassword", _webExPassword);
            meetingXML = meetingXML.Replace("##MeetingPassword", meetingEdit.MeetingPassword);
            meetingXML = meetingXML.Replace("##MeetingName", meetingEdit.MeetingName);
            meetingXML = meetingXML.Replace("##Duration", meetingEdit.Duration.ToString());
            meetingXML = meetingXML.Replace("##StartDate", meetingDate.ToString("MM/dd/yyyy HH:mm:ss"));

            string strXMLServer = $"https://{_webExSiteName}.webex.com/WBXService/XMLService";
            WebRequest request = WebRequest.Create(strXMLServer);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] byteArray = Encoding.UTF8.GetBytes(meetingXML);
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();
            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            XmlSerializer x = new XmlSerializer(typeof(WebExMeeting));
            WebExMeeting meetingInfo = (WebExMeeting)x.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"meet:createMeetingResponse\"", "")));
            return meetingInfo;
        }

        public static string GetWebExMeetingUrl(string meetingKey)
        {
            string _webExMeetingId = ConfigurationManager.AppSettings["WebExId"];
            string _webExPassword = ConfigurationManager.AppSettings["WebExPassword"];
            string _webExSiteName = ConfigurationManager.AppSettings["WebExSiteName"];
            string meetingXML = $@"<?xml version='1.0' encoding='ISO-8859-1'?>
<serv:message
    xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
    xmlns:serv='http://www.webex.com/schemas/2002/06/service'>
    <header>
        <securityContext>
            <siteName>{_webExSiteName}</siteName>
            <webExID>{_webExMeetingId}</webExID>
            <password>{_webExPassword}</password>
        </securityContext>
    </header>
    <body>
        <bodyContent xsi:type='java:com.webex.service.binding.meeting.GetMeeting'>
            <meetingKey>{meetingKey}</meetingKey>
        </bodyContent>
    </body>
</serv:message>";
            try
            {
                string strXMLServer = $"https://{_webExSiteName}.webex.com/WBXService/XMLService";
                WebRequest request = WebRequest.Create(strXMLServer);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                byte[] byteArray = Encoding.UTF8.GetBytes(meetingXML);
                request.ContentLength = byteArray.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse response = request.GetResponse();
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                XmlDocument xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(responseFromServer);
                var meetingInfo = xmlDOc.GetElementsByTagName("meet:meetingLink");
                return meetingInfo[0].InnerText;
            }
            catch
            {
                return string.Empty;
            }
        }

    }

}