﻿using System;
using System.Runtime.Caching;

namespace Phoenix.VLE.Web.Helpers
{
    public static class CustomCacheManager
    {
        private static MemoryCache _cache = MemoryCache.Default;

        public static T GetObjectFromCache<T>(string cacheItemName, Func<T> objectSettingFunction)
        {
            return GetObjectFromCache(cacheItemName, 20, objectSettingFunction);
        }

        public static T GetObjectFromCache<T>(string cacheItemName, int cacheTimeInMinutes, Func<T> objectSettingFunction)
        {
            var cachedObject = (T)_cache[cacheItemName];
            if (cachedObject == null)
            {
                CacheItemPolicy policy = new CacheItemPolicy();
                policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(cacheTimeInMinutes);
                cachedObject = objectSettingFunction();
                _cache.Set(cacheItemName, cachedObject, policy);
            }
            return cachedObject;
        }
    }
}
