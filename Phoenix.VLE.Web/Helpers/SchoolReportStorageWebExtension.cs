﻿using DevExpress.XtraReports.UI;
using Phoenix.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Xml;

namespace Phoenix.VLE.Web.Helpers
{
    public class SchoolReportStorageWebExtension :
        DevExpress.XtraReports.Web.Extensions.ReportStorageWebExtension
    {
        readonly string reportDirectory;
        const string FileExtension = ".repx";
        public SchoolReportStorageWebExtension()
        {
            var reportDirectory = PhoenixConfiguration.Instance.WriteFilePath ;
            Common.Helpers.CommonHelper.CreateDestinationFolder(reportDirectory);
            this.reportDirectory = reportDirectory;
        }
        public override bool CanSetData(string url)
        {
            return true;
        }
        public override bool IsValidUrl(string url)
        {
            return true;
        }
        public override byte[] GetData(string url)
        {
            // Gets the report layout data stored in a Report Storage by the specified URL. 
            try
            {
                if (Directory.EnumerateFiles(reportDirectory).Select(Path.GetFileNameWithoutExtension).Contains(url))
                {
                    return File.ReadAllBytes(Path.Combine(reportDirectory, url + FileExtension));
                }
                //if (PredefinedReports.ReportsFactory.Reports.ContainsKey(url))
                //{
                //    using (MemoryStream ms = new MemoryStream())
                //    {
                //        PredefinedReports.ReportsFactory.Reports[url]().SaveLayoutToXml(ms);
                //        return ms.ToArray();
                //    }
                //}
                throw new FaultException(new FaultReason(string.Format("Could not find report '{0}'.", url)), new FaultCode("Server"), "GetData");
            }
            catch (Exception)
            {
                throw new FaultException(new FaultReason(string.Format("Could not find report '{0}'.", url)), new FaultCode("Server"), "GetData");
            }
        }
        public override Dictionary<string, string> GetUrls()
        {
            // Returns a dictionary that contains report URLs and display names. 
            // This method is called when running the Report Designer, 
            // before the Open Report and Save Report dialogs and after a new report is saved to a storage.

            return Directory.GetFiles(reportDirectory, "*" + FileExtension)
                                     .Select(Path.GetFileNameWithoutExtension)
                                     //.Concat(PredefinedReports.ReportsFactory.Reports.Select(x => x.Key))
                                     .ToDictionary<string, string>(x => x);
        }
        public override void SetData(XtraReport report, string url)
        {
            // Saves a report to the Report Storage with the specified URL. 
            var relativePath = reportDirectory + Constants.SchoolReportsDir ;
            var path = Path.GetDirectoryName(relativePath + url + FileExtension);
            Common.Helpers.CommonHelper.CreateDestinationFolder(path);
            report.SaveLayoutToXml(Path.Combine(relativePath, url + FileExtension));
        }
        public override string SetNewData(XtraReport report, string defaultUrl)
        {
            // Stores the report with a new URL. . 
            // Validate and correct the specified URL in the SetNewData method
            // and return the resulting URL that is used to save a report in the storage.
            SetData(report, defaultUrl);
            return defaultUrl;
        }
    }
    public class XPCollectionSerializer : DevExpress.XtraReports.Native.IDataSerializer
    {
        public const string Name = "XPCollectionSerializer";

        public bool CanSerialize(object data, object extensionProvider)
        {
            return (data is DataSet);
        }
        public string Serialize(object data, object extensionProvider)
        {
            if (data is DataSet)
            {
                DataSet tbl = data as DataSet;
                StringBuilder sb = new StringBuilder();
                XmlWriter writer = XmlWriter.Create(sb);
                tbl.WriteXml(writer, XmlWriteMode.WriteSchema);
                return sb.ToString();
            }
            return string.Empty;
        }
        public bool CanDeserialize(string value, string typeName, object extensionProvider)
        {
            return typeName == "System.Data.DataSet";
        }
        public object Deserialize(string value, string typeName, object extensionProvider)
        {
            DataSet tbl = new DataSet();
            using (XmlReader reader = XmlReader.Create(new StringReader(value)))
            {
                tbl.ReadXml(reader);
            }
            return tbl;
        }
    }
}