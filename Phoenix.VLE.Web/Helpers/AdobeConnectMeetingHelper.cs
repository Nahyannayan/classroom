﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Task = System.Threading.Tasks.Task;

namespace Phoenix.VLE.Web.Helpers
{
    public static class AdobeConnectMeetingHelper
    {
        private static string _meetingBaseUri;
        private static string _userLogin;
        private static string _userPassword;
        private static string _authSessionToken;
        private static string _principalId;
        static AdobeConnectMeetingHelper()
        {
            _meetingBaseUri = ConfigurationManager.AppSettings["ConnectNowUrlBase"].ToString();
            _userPassword = ConfigurationManager.AppSettings["AdobeConnectPassword"].ToString();
            _userLogin = ConfigurationManager.AppSettings["AdobeConnectLogin"].ToString();
        }

        private static async Task<HttpResponseMessage> CallAdobeApi(string urlSegment, bool isResponseCookieEnabled = false)
        {
            Uri address = new Uri(_meetingBaseUri + urlSegment);
            CookieContainer cookies = new CookieContainer();
            HttpClientHandler handler = new HttpClientHandler();
            handler.CookieContainer = cookies;
            HttpClient client = new HttpClient(handler);
            HttpResponseMessage response = client.GetAsync(address).Result;
            if (isResponseCookieEnabled)
            {
                IEnumerable<Cookie> responseCookies = cookies.GetCookies(address).Cast<Cookie>();
                foreach (Cookie cookie in responseCookies)
                    _authSessionToken = cookie.Value;
            }
            return response;
        }

        public static async Task<HttpResponseMessage> LogIntoAdobeConnect()
        {
            return await CallAdobeApi($"api/xml?action=login&login={_userLogin}&password={_userPassword}", true);
        }

        public static async Task<HttpResponseMessage> MeetingFolderDetails()
        {
            return await CallAdobeApi($"api/xml?action=sco-shortcuts&session={_authSessionToken}");
        }

        private static XmlElement GetXmlElement(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc.DocumentElement;
        }

        public static async Task<HttpResponseMessage> GetPrincipalList()
        {
            return await CallAdobeApi($"api/xml?action=principal-list&session={_authSessionToken}");
        }
        public static async Task<AdobeConnectMeeting> CreateConnectNowMeeting(ConnectNowMeetingEdit model)
        {
            bool callSuccess = true;
            try
            {
                HttpResponseMessage httpResponseMessage = await LogIntoAdobeConnect();
                HttpResponseMessage responseMessage = await MeetingFolderDetails();
                HttpResponseMessage principalList = await GetPrincipalList();
                XmlNode principalNode = GetXmlElement(principalList.Content.ReadAsStringAsync().Result).SelectSingleNode("/results/principal-list/principal[login='Meeting Hosts']");
                _principalId = principalNode.Attributes["principal-id"].Value;
                if ((httpResponseMessage.StatusCode != HttpStatusCode.OK) || (httpResponseMessage.StatusCode != HttpStatusCode.OK))
                    callSuccess = false;
                if (callSuccess)
                {
                    string folderDetails = responseMessage.Content.ReadAsStringAsync().Result;
                    XmlElement xmlDoc = GetXmlElement(folderDetails);
                    XmlNodeList nodes = xmlDoc.SelectNodes("//sco[@type='my-meetings']");
                    string folderId = nodes[0].Attributes["sco-id"].Value;
                    HttpResponseMessage meetingResponseMessage = await CallAdobeApi($"api/xml?action=sco-update&type=meeting&name={model.MeetingName}&folder-id={folderId}&date-begin={GetMeetingFormatedDateTime(model.MeetingStartDate, model.MeetingStartTime)}&date-end={GetMeetingFormatedDateTime(model.MeetingEndDate, model.MeetingEndTime)}&session={_authSessionToken}" + (string.IsNullOrEmpty(model.MeetingUrl) ? "" : $"&url-path=publicmeeting&session={_authSessionToken}"));
                    if (meetingResponseMessage.IsSuccessStatusCode)
                    {

                        var responseString = meetingResponseMessage.Content.ReadAsStringAsync().Result;
                        XmlSerializer x = new XmlSerializer(typeof(Results));
                        Results meetingInfo = (Results)x.Deserialize(new StringReader(responseString));
                        await MakeMeetingPublic(scoId: meetingInfo.Sco.Scoid);
                        await AddMeetingHost(princialId: _principalId, scoId: meetingInfo.Sco.Scoid); // Success but not adding any host
                        return new AdobeConnectMeeting()
                        {
                            MeetingId = meetingInfo.Sco.Scoid,
                            FolderId = meetingInfo.Sco.Folderid,
                            CreatedDateTime = meetingInfo.Sco.Datecreated,
                            StartDateTime = meetingInfo.Sco.Datebegin,
                            EndDateTime = meetingInfo.Sco.Dateend,
                            MeetingName = meetingInfo.Sco.Name,
                            MeetingUrlSegment = meetingInfo.Sco.Urlpath
                        };
                    }
                }
                else return new AdobeConnectMeeting();
            }
            catch (Exception ex)
            {
                return new AdobeConnectMeeting();
            }
            return new AdobeConnectMeeting();
        }

        public static string GetMeetingFormatedDateTime(string date, string time)
        {
            TimeSpan timeSpan = DateTime.ParseExact(time, "hh:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
            return Convert.ToDateTime(date).Add(timeSpan)
                       .ToString("yyyy-MM-ddTHH:mm:ss");
        }

        public static async Task<HttpResponseMessage> AddMeetingHost(string princialId, string scoId)
        {
            return await CallAdobeApi($"api/xml?action=permissions-update&principal-id={princialId}&acl-id={scoId}&permission-id=host&session={_authSessionToken}");
        }

        public static async Task<HttpResponseMessage> MakeMeetingPublic(string scoId)
        {
            return await CallAdobeApi($"api/xml?action=permissions-update&acl-id={scoId}&principal-id=public-access&permission-id=view-hidden&session={_authSessionToken}");
        }
    }
}