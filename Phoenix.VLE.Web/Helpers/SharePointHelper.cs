﻿
using Microsoft.SharePoint.Client;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using File = Phoenix.Models.File;

namespace Phoenix.VLE.Web.Helpers
{
    public class SharePointHelper
    {
        public string SHAREPOINT_URL { get; set; }
        public string SHAREPOINT_USER { get; set; }
        public string SHAREPOINT_PASSWORD { get; set; }
        public bool IsSharepointEnabled { get; set; }


        public ClientContext LoginToSharepoint(string schoolCode)
        {
            SHAREPOINT_URL = ConfigurationManager.AppSettings["SharepointURL"].ToString();
            SHAREPOINT_USER = ConfigurationManager.AppSettings["SharepointUser"].ToString();
            SHAREPOINT_PASSWORD = ConfigurationManager.AppSettings["SharepointPassword"].ToString();
            SHAREPOINT_URL = SHAREPOINT_URL.Replace("SchoolCode", schoolCode);
            ClientContext cxt = new ClientContext(SHAREPOINT_URL);
            System.Security.SecureString s = new System.Security.SecureString();
            foreach (char c in SHAREPOINT_PASSWORD.ToCharArray())
                s.AppendChar(c);
            cxt.Credentials = new Microsoft.SharePoint.Client.SharePointOnlineCredentials(SHAREPOINT_USER, s);
            Microsoft.SharePoint.Client.Web web = cxt.Web;
            cxt.Load(web);
            cxt.ExecuteQuery();
            string webtitle = web.Title;
            return cxt;
        }
        public static bool CreateFolder(ref ClientContext cxt, ref List list, string FolderNAme)
        {
            bool CreateFolder = false;
            list.EnableFolderCreation = true;
            list.Update();
            cxt.ExecuteQuery();
            ListItemCreationInformation info = new ListItemCreationInformation();
            info.UnderlyingObjectType = FileSystemObjectType.Folder;
            info.LeafName = FolderNAme.Trim(); // Trim for spaces.Just extra check
            ListItem newItem = list.AddItem(info);
            newItem["Title"] = FolderNAme;
            newItem.Update();
            cxt.ExecuteQuery();
            CreateFolder = true;
            return CreateFolder;
        }

        public static bool CreateLibrary(ref ClientContext cxt, string LibraryName)
        {
            bool CreateLibrary = false;
            ListCreationInformation lci = new ListCreationInformation();
            lci.Description = LibraryName;
            lci.Title = LibraryName;
            lci.TemplateType = System.Convert.ToInt32(ListTemplateType.DocumentLibrary);
            List newLib = cxt.Web.Lists.Add(lci);
            cxt.Load(newLib);
            cxt.ExecuteQuery();
            CreateLibrary = true;
            return CreateLibrary;
        }

        public static bool IsLibraryExists(ref ClientContext clientContext, string listTitle)
        {
            bool IsLibraryExists = false;
            var web = clientContext.Web;
            ListCollection lists = web.Lists;
            clientContext.Load(lists);
            clientContext.ExecuteQuery();
            foreach (List L in lists)
            {
                if (L.Title.Equals(listTitle, StringComparison.CurrentCultureIgnoreCase))
                {
                    IsLibraryExists = true;
                    break;
                }
            }
            return IsLibraryExists;
        }

        public static bool CheckFileExists(ref ClientContext ctx, string FilePath)
        {
            bool CheckFileExists = false;
            try
            {
                var web = ctx.Web;
                ctx.Load(web);
                string url = new Uri(FilePath).AbsolutePath;
                Microsoft.SharePoint.Client.File f = web.GetFileByServerRelativeUrl(url);
                ctx.Load(f);
                ctx.ExecuteQuery();
                if (f.Exists)
                    CheckFileExists = true;
            }
            catch (Exception ex)
            {

            }
            return CheckFileExists;
        }

        public static SharePointFileView UploadFilesAsPerModule(ref ClientContext clientContext, FileModulesConstants ModuleName, string oldUserId, HttpPostedFileBase file)
        {
            SharePointFileView shv = new SharePointFileView();
            shv = UploadFileSlicePerSlice(ref clientContext, ModuleName, oldUserId, file, 5);
            return shv;

        }
        public static List<FileEdit> UploadFilesListAsPerModule(ref ClientContext clientContext, FileModulesConstants ModuleName, string oldUserId, List<FileEdit> fileList)
        {
            bool LibraryExists = false;
            bool IsFolderExists = false;
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);

            if (!IsLibraryExists(ref clientContext, ModuleNametoUpload))
                LibraryExists = CreateLibrary(ref clientContext, ModuleNametoUpload); // Creates library if not exists
            else
                LibraryExists = true;
            if (LibraryExists)
            {
                List List = clientContext.Web.Lists.GetByTitle(ModuleNametoUpload);
                clientContext.Load(List);
                clientContext.Load(List.RootFolder);
                clientContext.Load(List.RootFolder.Folders);
                clientContext.ExecuteQuery();
                FolderCollection folders = List.RootFolder.Folders;
                foreach (Folder fldr in folders)
                {
                    if (fldr.Name == oldUserId)
                    {
                        IsFolderExists = true;
                        break;
                    }
                }
                if (!IsFolderExists)
                {
                    if (CreateFolder(ref clientContext, ref List, oldUserId))
                        IsFolderExists = true;
                }
                if (IsFolderExists)
                {
                    foreach (var file in fileList)
                    {
                        using (var fs = file.PostedFile.InputStream)
                        {
                            string DestinationFileName = "";
                            string ServerDomain = "";
                            ServerDomain = new Uri(clientContext.Url).GetLeftPart(UriPartial.Authority);
                            DestinationFileName = file.FileName;
                            var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, DestinationFileName);
                            while (CheckFileExists(ref clientContext, fileUrl))
                            {
                                var random = new Random(DateTime.Now.Millisecond);
                                int randomNumber = random.Next(1, 500000);
                                DestinationFileName = randomNumber + "_" + DestinationFileName;
                                fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, DestinationFileName);
                            }
                            if ((clientContext.HasPendingRequest))
                                clientContext.ExecuteQuery();
                            Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, List.RootFolder.ServerRelativeUrl + "/" + oldUserId + "/" + DestinationFileName, fs, false);
                            file.PhysicalFilePath = fileUrl;
                            file.FilePath = fileUrl;// clientContext.Web.CreateAnonymousLinkForDocument(fileUrl, ExternalSharingDocumentOption.View);
                        }
                    }

                }
            }
            return fileList;

        }

        /// <summary>
        ///  Added by Arvind for camera base64 format string image 
        /// </summary>
        /// <param name="clientContext"></param>
        /// <param name="LibraryName"></param>
        /// <param name="ModuleName"></param>
        /// <param name="fileList"></param>
        /// <param name="relativePath"></param>
        /// <returns></returns>
        public static List<FileEdit> UploadFilesListAsPerModuleCamera(ref ClientContext clientContext, FileModulesConstants ModuleName, string oldUserId, List<FileEdit> fileList, string relativePath)
        {
            bool LibraryExists = false;
            bool IsFolderExists = false;
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);

            if (!IsLibraryExists(ref clientContext, ModuleNametoUpload))
                LibraryExists = CreateLibrary(ref clientContext, ModuleNametoUpload); // Creates library if not exists
            else
                LibraryExists = true;
            if (LibraryExists)
            {
                List List = clientContext.Web.Lists.GetByTitle(ModuleNametoUpload);
                clientContext.Load(List);
                clientContext.Load(List.RootFolder);
                clientContext.Load(List.RootFolder.Folders);
                clientContext.ExecuteQuery();
                FolderCollection folders = List.RootFolder.Folders;
                foreach (Folder fldr in folders)
                {
                    if (fldr.Name == oldUserId)
                    {
                        IsFolderExists = true;
                        break;
                    }
                }
                if (!IsFolderExists)
                {
                    if (CreateFolder(ref clientContext, ref List, oldUserId))
                        IsFolderExists = true;
                }
                if (IsFolderExists)
                {
                    foreach (var file in fileList)
                    {
                        using (var fs = new FileStream(HttpContext.Current.Server.MapPath(relativePath), FileMode.Open, FileAccess.Read))
                        {
                            string DestinationFileName = "";
                            string ServerDomain = "";
                            ServerDomain = new Uri(clientContext.Url).GetLeftPart(UriPartial.Authority);
                            DestinationFileName = file.FileName;
                            var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, DestinationFileName);
                            while (CheckFileExists(ref clientContext, fileUrl))
                            {
                                var random = new Random(DateTime.Now.Millisecond);
                                int randomNumber = random.Next(1, 500000);
                                DestinationFileName = randomNumber + "_" + DestinationFileName;
                                fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, DestinationFileName);
                            }
                            if ((clientContext.HasPendingRequest))
                                clientContext.ExecuteQuery();
                            Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, List.RootFolder.ServerRelativeUrl + "/" + oldUserId + "/" + DestinationFileName, fs, false);
                            file.PhysicalFilePath = fileUrl;
                            file.FilePath = fileUrl; //clientContext.Web.CreateAnonymousLinkForDocument(fileUrl, ExternalSharingDocumentOption.View);
                        }
                    }

                }
            }
            return fileList;

        }

        public static List<SharePointFileView> UploadMultipleFilesAsPerModule(ref ClientContext clientContext, FileModulesConstants ModuleName, string oldUserId, HttpPostedFileBase[] files)
        {
            bool UploadFile = false;
            bool LibraryExists = false;
            bool IsFolderExists = false;
            string ShareableLink = "";
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            List<SharePointFileView> lstSharepointFiles = new List<SharePointFileView>();
            if (!IsLibraryExists(ref clientContext, ModuleNametoUpload))
                LibraryExists = CreateLibrary(ref clientContext, ModuleNametoUpload); // Creates library if not exists
            else
                LibraryExists = true;
            if (LibraryExists)
            {
                List List = clientContext.Web.Lists.GetByTitle(ModuleNametoUpload);
                clientContext.Load(List);
                clientContext.Load(List.RootFolder);
                clientContext.Load(List.RootFolder.Folders);
                clientContext.ExecuteQuery();
                FolderCollection folders = List.RootFolder.Folders;
                foreach (Folder fldr in folders)
                {
                    if (fldr.Name == oldUserId)
                    {
                        IsFolderExists = true;
                        break;
                    }
                }
                if (!IsFolderExists)
                {
                    if (CreateFolder(ref clientContext, ref List, oldUserId))
                        IsFolderExists = true;
                }
                if (IsFolderExists)
                {
                    string ServerDomain = "";
                    ServerDomain = new Uri(clientContext.Url).GetLeftPart(UriPartial.Authority);
                    Folder folder = clientContext.Web.GetFolderByServerRelativeUrl(ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId);
                    foreach (var file in files)
                    {
                        using (var fs = file.InputStream)
                        {
                            SharePointFileView shv = new SharePointFileView();
                            shv.ActualFileName = file.FileName;
                            shv.UploadedFileName = file.FileName;
                            var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, shv.UploadedFileName);
                            while (CheckFileExists(ref clientContext, fileUrl))
                            {
                                shv.UploadedFileName = Guid.NewGuid() + "_" + shv.UploadedFileName;
                                fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, shv.UploadedFileName);
                            }
                            //added to tweak inputsteam
                            var inputFile = file;
                            double fileSizeInMb = (file.ContentLength / 1024f) / 1024f;
                            byte[] fileData = null;
                            using (var binaryReader = new BinaryReader(fs))
                            {
                                fileData = binaryReader.ReadBytes(file.ContentLength);
                            }

                            shv.FileSizeInMb = fileSizeInMb;
                            Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, List.RootFolder.ServerRelativeUrl + "/" + oldUserId + "/" + shv.UploadedFileName, new MemoryStream(fileData), true);
                            clientContext.ExecuteQuery();
                            shv.ShareableLink = fileUrl;// clientContext.Web.CreateAnonymousLinkForDocument(fileUrl, ExternalSharingDocumentOption.View);
                            shv.SharepointUploadedFileURL = fileUrl;
                            lstSharepointFiles.Add(shv);
                        }
                    }


                }
            }
            return lstSharepointFiles;
        }

        public static SharePointFileView UploadFileSlicePerSlice(ref ClientContext ctx, FileModulesConstants ModuleName, string oldUserId, HttpPostedFileBase files, int fileChunkSizeInMB = 3)
        {
            ILoggerClient _loggerClient = LoggerClient.Instance;
            Microsoft.SharePoint.Client.File uploadFile = null;
            bool LibraryExists = false;
            bool IsFolderExists = false;
            Guid uploadId = Guid.NewGuid();
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            string uniqueFileName = Path.GetFileName(files.FileName);
            SharePointFileView shv = new SharePointFileView();
            if (!IsLibraryExists(ref ctx, ModuleNametoUpload))
            {
                LibraryExists = CreateLibrary(ref ctx, ModuleNametoUpload);
            }
            else
            {
                LibraryExists = true;
            }
            if (LibraryExists)
            {
                List List = ctx.Web.Lists.GetByTitle(ModuleNametoUpload);
                ctx.Load(List);
                ctx.Load(List.RootFolder);
                ctx.Load(List.RootFolder.Folders);
                ctx.ExecuteQuery();
                FolderCollection folders = List.RootFolder.Folders;
                foreach (Folder fldr in folders)
                {
                    if (fldr.Name == oldUserId)
                    {
                        IsFolderExists = true;
                        break;
                    }
                }
                if (!IsFolderExists)
                {
                    if (CreateFolder(ref ctx, ref List, oldUserId))
                        IsFolderExists = true;
                }
                if (IsFolderExists)
                {
                    var targetFolder = ctx.Web.GetFolderByServerRelativeUrl(List.RootFolder.ServerRelativeUrl + "/" + oldUserId);
                    int blockSize = fileChunkSizeInMB * 1024 * 1024;
                    ctx.ExecuteQuery();
                    long fileSize = files.InputStream.Length;
                    string ServerDomain = "";
                    ServerDomain = new Uri(ctx.Url).GetLeftPart(UriPartial.Authority);
                    var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, uniqueFileName);
                    while (CheckFileExists(ref ctx, fileUrl))
                    {
                        uniqueFileName = Guid.NewGuid() + "_" + uniqueFileName;
                        fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, uniqueFileName);
                    }

                    if (fileSize <= blockSize)
                    {
                        FileCreationInformation fileInfo = new FileCreationInformation();
                        fileInfo.ContentStream = files.InputStream;
                        fileInfo.Url = uniqueFileName;
                        fileInfo.Overwrite = true;
                        uploadFile = targetFolder.Files.Add(fileInfo);
                        ctx.Load(uploadFile);
                        ctx.ExecuteQuery();
                    }
                    else
                    {
                        ClientResult<long> bytesUploaded = null;
                        FileStream fs = null;
                        try
                        {
                            fs = files.InputStream as FileStream;
                            using (BinaryReader br = new BinaryReader(files.InputStream))
                            {
                                byte[] buffer = new byte[blockSize];
                                long fileoffset = 0;
                                long totalBytesRead = 0;
                                int bytesRead;
                                bool first = true;
                                while ((bytesRead = br.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    totalBytesRead = totalBytesRead + bytesRead;
                                    if (first)
                                    {
                                        using (MemoryStream contentStream = new MemoryStream())
                                        {
                                            FileCreationInformation fileInfo = new FileCreationInformation();
                                            fileInfo.ContentStream = contentStream;
                                            fileInfo.Url = uniqueFileName;
                                            fileInfo.Overwrite = true;
                                            uploadFile = targetFolder.Files.Add(fileInfo);
                                            using (MemoryStream s = new MemoryStream(buffer))
                                            {
                                                bytesUploaded = uploadFile.StartUpload(uploadId, s);
                                                ctx.ExecuteQuery();
                                                fileoffset = bytesUploaded.Value;
                                            }
                                            first = false;
                                        }
                                    }
                                    else
                                    {
                                        uploadFile = ctx.Web.GetFileByServerRelativeUrl(List.RootFolder.ServerRelativeUrl + '/' + oldUserId + System.IO.Path.AltDirectorySeparatorChar + uniqueFileName);
                                        if (totalBytesRead == fileSize)
                                        {
                                            using (MemoryStream s = new MemoryStream(buffer, 0, bytesRead))
                                            {
                                                uploadFile = uploadFile.FinishUpload(uploadId, fileoffset, s);
                                                ctx.ExecuteQuery();
                                            }
                                        }
                                        else
                                        {
                                            using (MemoryStream s = new MemoryStream(buffer))
                                            {
                                                bytesUploaded = uploadFile.ContinueUpload(uploadId, fileoffset, s);
                                                ctx.ExecuteQuery();
                                                fileoffset = bytesUploaded.Value;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _loggerClient.LogWarning("Sharepoint Exception");
                            _loggerClient.LogException(ex);
                            _loggerClient.LogException(ex.InnerException);

                        }
                        finally
                        {
                            if (fs != null)
                            {
                                fs.Dispose();
                            }
                        }
                    }
                    ServerDomain = new Uri(ctx.Url).GetLeftPart(UriPartial.Authority);
                    shv.ShareableLink = fileUrl;// ctx.Web.CreateAnonymousLinkForDocument((fileUrl), ExternalSharingDocumentOption.View);
                    shv.SharepointUploadedFileURL = fileUrl;
                }

            }

            return shv;
        }

        public byte[] DownloadFilesFromSharePoint(ref ClientContext ctx, string FileName)
        {
            var web = ctx.Web;
            ctx.Load(web);
            string url = new Uri(FileName).AbsolutePath;
            Microsoft.SharePoint.Client.File f = web.GetFileByServerRelativeUrl(url);
            ctx.Load(f);
            ctx.ExecuteQuery();
            FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(ctx, f.ServerRelativeUrl);
            byte[] imageArray = null;
            using (System.IO.MemoryStream mStream = new System.IO.MemoryStream())
            {
                if (fileInfo.Stream != null)
                {
                    fileInfo.Stream.CopyTo(mStream);
                    imageArray = mStream.ToArray();
                }
            }
            return imageArray;
        }

        public FileInformation DownloadFilesFromSharePointAsStream(ref ClientContext ctx, string FileName)
        {
            var web = ctx.Web;
            ctx.Load(web);
            string url = new Uri(FileName).AbsolutePath;
            Microsoft.SharePoint.Client.File f = web.GetFileByServerRelativeUrl(url);
            ctx.Load(f);
            ctx.ExecuteQuery();
            FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(ctx, f.ServerRelativeUrl);

            return fileInfo;
        }

        public static string ReplaceFileInSharepoint(ref ClientContext clientContext, byte[] fileData, string uploadedFileUrl)
        {

            var web = clientContext.Web;
            string ShareableLink = "";
            string url = new Uri(uploadedFileUrl).AbsolutePath;
            string FileName = Path.GetFileName(uploadedFileUrl);
            MemoryStream memStream = new MemoryStream();
            memStream.Write(fileData, 0, fileData.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Microsoft.SharePoint.Client.File f = web.GetFileByServerRelativeUrl(url);
            clientContext.Load(f);
            clientContext.ExecuteQuery();
            Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, f.ServerRelativeUrl, memStream, true);
            clientContext.ExecuteQuery();
            // ShareableLink = clientContext.Web.CreateAnonymousLinkForDocument(uploadedFileUrl, ExternalSharingDocumentOption.View);
            return ShareableLink;
        }

        public static string CopyToOtherLocation(ref ClientContext ctx, string TargetFileName, FileModulesConstants ModuleName, string OldUserId)
        {
            bool IsFolderExists = false;
            string ShareableLink = "";
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            var web = ctx.Web;
            ctx.Load(web);
            string url = new Uri(TargetFileName).AbsolutePath;
            Microsoft.SharePoint.Client.File f = web.GetFileByServerRelativeUrl(url);
            ctx.Load(f);
            ctx.ExecuteQuery();
            List List = ctx.Web.Lists.GetByTitle(ModuleNametoUpload);
            ctx.Load(List);
            ctx.Load(List.RootFolder);
            ctx.ExecuteQuery();
            IsFolderExists = FolderExists(ctx, List.RootFolder.ServerRelativeUrl + "/" + OldUserId);

            if (!IsFolderExists)
            {
                if (CreateFolder(ref ctx, ref List, OldUserId))
                    IsFolderExists = true;
            }
            if (IsFolderExists)
            {

                string DestinationFileName = "";
                string ServerDomain = "";
                ServerDomain = new Uri(ctx.Url).GetLeftPart(UriPartial.Authority);
                DestinationFileName = f.Name;
                var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + OldUserId, DestinationFileName);
                while (CheckFileExists(ref ctx, fileUrl))
                {
                    DestinationFileName = new Guid() + "_" + DestinationFileName;
                    fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + OldUserId, DestinationFileName);
                }

                f.CopyTo(fileUrl, true);
                ctx.ExecuteQuery();
                ShareableLink = fileUrl;

            }

            return ShareableLink;
        }

        public static SharePointFileView UploadStudentFilesAsPerModule(ref ClientContext clientContext, FileModulesConstants ModuleName, string oldUserId, HttpPostedFileBase file)
        {
            bool LibraryExists = false;
            bool IsFolderExists = false;
            string ShareableLink = "";
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            SharePointFileView sharePointFile = new SharePointFileView();
            if (!IsLibraryExists(ref clientContext, ModuleNametoUpload))
                LibraryExists = CreateLibrary(ref clientContext, ModuleNametoUpload); // Creates library if not exists
            else
                LibraryExists = true;
            if (LibraryExists)
            {
                List List = clientContext.Web.Lists.GetByTitle(ModuleNametoUpload);
                clientContext.Load(List);
                clientContext.Load(List.RootFolder);
                clientContext.Load(List.RootFolder.Folders);
                clientContext.ExecuteQuery();
                FolderCollection folders = List.RootFolder.Folders;
                foreach (Folder fldr in folders)
                {
                    if (fldr.Name == oldUserId)
                    {
                        IsFolderExists = true;
                        break;
                    }
                }
                if (!IsFolderExists)
                {
                    if (CreateFolder(ref clientContext, ref List, oldUserId))
                        IsFolderExists = true;
                }
                if (IsFolderExists)
                {
                    using (var fs = file.InputStream)
                    {
                        string DestinationFileName = "";
                        string ServerDomain = "";
                        ServerDomain = new Uri(clientContext.Url).GetLeftPart(UriPartial.Authority);
                        DestinationFileName = file.FileName;
                        var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, DestinationFileName);
                        while (CheckFileExists(ref clientContext, fileUrl))
                        {
                            DestinationFileName = Guid.NewGuid() + "_" + DestinationFileName;
                            fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, DestinationFileName);
                        }
                        //if ((clientContext.HasPendingRequest))
                        //    clientContext.ExecuteQuery();
                        //Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, List.RootFolder.ServerRelativeUrl + "/" + ModuleName + "/" + DestinationFileName, fs, false);

                        //added to tweak inputsteam
                        var inputFile = file;
                        double fileSizeInMb = (file.ContentLength / 1024f) / 1024f;
                        byte[] fileData = null;
                        using (var binaryReader = new BinaryReader(fs))
                        {
                            fileData = binaryReader.ReadBytes(file.ContentLength);
                        }

                        Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, List.RootFolder.ServerRelativeUrl + "/" + oldUserId + "/" + DestinationFileName, new MemoryStream(fileData), true);
                        clientContext.ExecuteQuery();
                        // ShareableLink = clientContext.Web.CreateAnonymousLinkForDocument(fileUrl, ExternalSharingDocumentOption.View);
                        sharePointFile.ActualFileName = file.FileName;
                        sharePointFile.ShareableLink = fileUrl;
                        sharePointFile.SharepointUploadedFileURL = fileUrl;
                        sharePointFile.UploadedFileName = DestinationFileName;
                        sharePointFile.FileSizeInMb = fileSizeInMb;
                        //SharingResult result = clientContext.Web.ShareDocument(fileUrl, "mahesh.neosoft@gemseducation.com", ExternalSharingDocumentOption.View, true, "Doc shared programmatically");
                    }
                }
            }
            return sharePointFile;
        }

        public static string UploadfileToAzure(ref ClientContext clientContext, string LibraryName, FileModulesConstants ModuleName, HttpPostedFileBase file)
        {
            ILoggerClient _loggerClient = LoggerClient.Instance;
            _loggerClient.LogWarning("upload to azure");
            //bool UploadFile = false;
            //bool LibraryExists = false;
            //bool IsFolderExists = false;
            string ShareableLink = "";
            //string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);

            //string fileName = string.Empty;
            //string relativePath = string.Empty;
            //string extension = string.Empty;

            //fileName = file.FileName.Replace(" ", "_");
            //extension = Path.GetExtension(file.FileName).Replace(".", "");

            //string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
            //string groupImageDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.GroupImageDir;
            //string groupSchoolDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.GroupImageDir + "/School_" + SessionHelper.CurrentSession.SchoolId;
            //string filePath = PhoenixConfiguration.Instance.WriteFilePath + Constants.GroupImageDir + "/School_" + SessionHelper.CurrentSession.SchoolId + "/User_" + SessionHelper.CurrentSession.Id;

            //Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
            //Common.Helpers.CommonHelper.CreateDestinationFolder(groupImageDir);
            //Common.Helpers.CommonHelper.CreateDestinationFolder(groupSchoolDir);
            //Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
            //string updatedFileName = Guid.NewGuid() + "_" + fileName;
            //fileName = updatedFileName;
            //relativePath = Constants.GroupImageDir + "/School_" + SessionHelper.CurrentSession.SchoolId + "/User_" + SessionHelper.CurrentSession.Id + "/" + fileName;
            //fileName = Path.Combine(filePath, fileName);
            //file.SaveAs(fileName);
            //ShareableLink = PhoenixConfiguration.Instance.ReadFilePath + "/" + Constants.GroupImageDir + "/School_" + SessionHelper.CurrentSession.SchoolId + "/User_" + SessionHelper.CurrentSession.Id + "/" + updatedFileName;
            return ShareableLink;

        }

        public static SharePointFileView UploadFileSlicePerSlice(ref ClientContext ctx, FileModulesConstants ModuleName, string oldUserId, HttpPostedFileViewModel file, int fileChunkSizeInMB = 3)
        {
            ILoggerClient _loggerClient = LoggerClient.Instance;
            Microsoft.SharePoint.Client.File uploadFile = null;
            bool LibraryExists = false;
            bool IsFolderExists = false;
            Guid uploadId = Guid.NewGuid();
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            string uniqueFileName = Path.GetFileName(file.FileName);
            SharePointFileView shv = new SharePointFileView();
            if (!IsLibraryExists(ref ctx, ModuleNametoUpload))
            {
                LibraryExists = CreateLibrary(ref ctx, ModuleNametoUpload);
            }
            else
            {
                LibraryExists = true;
            }
            if (LibraryExists)
            {
                List List = ctx.Web.Lists.GetByTitle(ModuleNametoUpload);
                ctx.Load(List);
                ctx.Load(List.RootFolder);
                ctx.Load(List.RootFolder.Folders);
                ctx.ExecuteQuery();
                FolderCollection folders = List.RootFolder.Folders;
                foreach (Folder fldr in folders)
                {
                    if (fldr.Name == oldUserId)
                    {
                        IsFolderExists = true;
                        break;
                    }
                }
                if (!IsFolderExists)
                {
                    if (CreateFolder(ref ctx, ref List, oldUserId))
                        IsFolderExists = true;
                }
                if (IsFolderExists)
                {
                    var targetFolder = ctx.Web.GetFolderByServerRelativeUrl(List.RootFolder.ServerRelativeUrl + "/" + oldUserId);
                    int blockSize = fileChunkSizeInMB * 1024 * 1024;
                    ctx.ExecuteQuery();
                    long fileSize = file.InputStream.Length;
                    string ServerDomain = "";
                    ServerDomain = new Uri(ctx.Url).GetLeftPart(UriPartial.Authority);
                    var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, uniqueFileName);
                    while (CheckFileExists(ref ctx, fileUrl))
                    {
                        uniqueFileName = Guid.NewGuid() + "_" + uniqueFileName;
                        fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, uniqueFileName);
                    }

                    if (fileSize <= blockSize)
                    {
                        FileCreationInformation fileInfo = new FileCreationInformation();
                        fileInfo.ContentStream = file.InputStream;
                        fileInfo.Url = uniqueFileName;
                        fileInfo.Overwrite = true;
                        uploadFile = targetFolder.Files.Add(fileInfo);
                        ctx.Load(uploadFile);
                        ctx.ExecuteQuery();
                    }
                    else
                    {
                        ClientResult<long> bytesUploaded = null;
                        FileStream fs = null;
                        try
                        {
                            fs = file.InputStream as FileStream;
                            using (BinaryReader br = new BinaryReader(file.InputStream))
                            {
                                byte[] buffer = new byte[blockSize];
                                long fileoffset = 0;
                                long totalBytesRead = 0;
                                int bytesRead;
                                bool first = true;
                                while ((bytesRead = br.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    totalBytesRead = totalBytesRead + bytesRead;
                                    if (first)
                                    {
                                        using (MemoryStream contentStream = new MemoryStream())
                                        {
                                            FileCreationInformation fileInfo = new FileCreationInformation();
                                            fileInfo.ContentStream = contentStream;
                                            fileInfo.Url = uniqueFileName;
                                            fileInfo.Overwrite = true;
                                            uploadFile = targetFolder.Files.Add(fileInfo);
                                            using (MemoryStream s = new MemoryStream(buffer))
                                            {
                                                bytesUploaded = uploadFile.StartUpload(uploadId, s);
                                                ctx.ExecuteQuery();
                                                fileoffset = bytesUploaded.Value;
                                            }
                                            first = false;
                                        }
                                    }
                                    else
                                    {
                                        uploadFile = ctx.Web.GetFileByServerRelativeUrl(List.RootFolder.ServerRelativeUrl + '/' + oldUserId + System.IO.Path.AltDirectorySeparatorChar + uniqueFileName);
                                        if (totalBytesRead == fileSize)
                                        {
                                            using (MemoryStream s = new MemoryStream(buffer, 0, bytesRead))
                                            {
                                                uploadFile = uploadFile.FinishUpload(uploadId, fileoffset, s);
                                                ctx.ExecuteQuery();
                                            }
                                        }
                                        else
                                        {
                                            using (MemoryStream s = new MemoryStream(buffer))
                                            {
                                                bytesUploaded = uploadFile.ContinueUpload(uploadId, fileoffset, s);
                                                ctx.ExecuteQuery();
                                                fileoffset = bytesUploaded.Value;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _loggerClient.LogWarning("Sharepoint Exception");
                            _loggerClient.LogException(ex);
                            _loggerClient.LogException(ex.InnerException);

                        }
                        finally
                        {
                            if (fs != null)
                            {
                                fs.Dispose();
                            }
                        }
                    }
                    ServerDomain = new Uri(ctx.Url).GetLeftPart(UriPartial.Authority);
                    //shv.ShareableLink = ctx.Web.CreateAnonymousLinkForDocument((fileUrl), ExternalSharingDocumentOption.View);
                    shv.ShareableLink = fileUrl;
                    shv.SharepointUploadedFileURL = fileUrl;
                }

            }
            return shv;
        }

        public static SharePointFileView UploadFileSlicePerSlice(ref ClientContext ctx, FileModulesConstants ModuleName, HttpPostedFileViewModel file, int fileChunkSizeInMB = 3)
        {
            SharePointFileView sharePointFile = new SharePointFileView();
            ILoggerClient _loggerClient = LoggerClient.Instance;
            // File object 
            Microsoft.SharePoint.Client.File uploadFile = null;
            _loggerClient.LogWarning("1");
            bool LibraryExists = false;
            Guid uploadId = Guid.NewGuid();
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            _loggerClient.LogWarning("1");
            // Get the name of the file
            string uniqueFileName = Path.GetFileName(file.FileName);
            _loggerClient.LogWarning("2");
            // Ensure that target library exists, create if is missing
            if (!IsLibraryExists(ref ctx, ModuleNametoUpload))
            {
                _loggerClient.LogWarning("3");
                LibraryExists = CreateLibrary(ref ctx, ModuleNametoUpload);
            }
            else
            {
                LibraryExists = true;
            }
            // Get to folder to upload into 
            List docs = ctx.Web.Lists.GetByTitle(ModuleNametoUpload);
            _loggerClient.LogWarning("4");
            ctx.Load(docs, l => l.RootFolder);
            // Get the information about the folder that will hold the file
            ctx.Load(docs.RootFolder, f => f.ServerRelativeUrl);
            _loggerClient.LogWarning("6");
            ctx.ExecuteQuery();

            _loggerClient.LogWarning("7");
            // Calculate block size in bytes
            int blockSize = fileChunkSizeInMB * 1024 * 1024;
            // Get the information about the folder that will hold the file
            ctx.Load(docs.RootFolder, f => f.ServerRelativeUrl);
            ctx.ExecuteQuery();


            _loggerClient.LogWarning("8");
            // Get the size of the file
            long fileSize = file.InputStream.Length;
            if (fileSize <= blockSize)
            {
                // Use regular approach
                FileCreationInformation fileInfo = new FileCreationInformation();
                fileInfo.ContentStream = file.InputStream;
                fileInfo.Url = uniqueFileName;
                fileInfo.Overwrite = true;
                uploadFile = docs.RootFolder.Files.Add(fileInfo);
                ctx.Load(uploadFile);
                ctx.ExecuteQuery();
                _loggerClient.LogWarning("9");
                // return the file object for the uploaded file

            }
            else
            {
                // Use large file upload approach
                ClientResult<long> bytesUploaded = null;
                _loggerClient.LogWarning("10");
                FileStream fs = null;
                try
                {
                    _loggerClient.LogWarning("11");
                    fs = file.InputStream as FileStream;
                    _loggerClient.LogWarning("12");
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        _loggerClient.LogWarning("13");
                        byte[] buffer = new byte[blockSize];
                        _loggerClient.LogWarning("14");
                        long fileoffset = 0;
                        long totalBytesRead = 0;
                        int bytesRead;
                        bool first = true;
                        // Read data from filesystem in blocks 
                        _loggerClient.LogWarning("15");
                        while ((bytesRead = br.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            _loggerClient.LogWarning("16");
                            totalBytesRead = totalBytesRead + bytesRead;
                            if (first)
                            {
                                using (MemoryStream contentStream = new MemoryStream())
                                {
                                    _loggerClient.LogWarning("17");
                                    // Add an empty file.
                                    FileCreationInformation fileInfo = new FileCreationInformation();
                                    fileInfo.ContentStream = contentStream;
                                    fileInfo.Url = uniqueFileName;
                                    fileInfo.Overwrite = true;
                                    uploadFile = docs.RootFolder.Files.Add(fileInfo);
                                    _loggerClient.LogWarning("18");
                                    // Start upload by uploading the first slice. 
                                    using (MemoryStream s = new MemoryStream(buffer))
                                    {
                                        _loggerClient.LogWarning("19");
                                        // Call the start upload method on the first slice
                                        bytesUploaded = uploadFile.StartUpload(uploadId, s);
                                        _loggerClient.LogWarning("20");
                                        ctx.ExecuteQuery();
                                        _loggerClient.LogWarning("21");
                                        // fileoffset is the pointer where the next slice will be added
                                        fileoffset = bytesUploaded.Value;
                                        _loggerClient.LogWarning("22");
                                    }
                                    // we can only start the upload once
                                    first = false;
                                }
                            }
                            else
                            {
                                _loggerClient.LogWarning("23");
                                // Get a reference to our file
                                uploadFile = ctx.Web.GetFileByServerRelativeUrl(docs.RootFolder.ServerRelativeUrl + System.IO.Path.AltDirectorySeparatorChar + uniqueFileName);
                                _loggerClient.LogWarning("24");
                                if (totalBytesRead == fileSize)
                                {
                                    // We've reached the end of the file
                                    using (MemoryStream s = new MemoryStream(buffer, 0, bytesRead))
                                    {
                                        _loggerClient.LogWarning("25");
                                        // End sliced upload by calling FinishUpload
                                        uploadFile = uploadFile.FinishUpload(uploadId, fileoffset, s);
                                        ctx.ExecuteQuery();
                                        _loggerClient.LogWarning("26");
                                        // return the file object for the uploaded file
                                    }
                                }
                                else
                                {
                                    _loggerClient.LogWarning("27");
                                    using (MemoryStream s = new MemoryStream(buffer))
                                    {
                                        // Continue sliced upload
                                        bytesUploaded = uploadFile.ContinueUpload(uploadId, fileoffset, s);
                                        _loggerClient.LogWarning("28");
                                        ctx.ExecuteQuery();
                                        _loggerClient.LogWarning("29");
                                        // update fileoffset for the next slice
                                        fileoffset = bytesUploaded.Value;
                                        _loggerClient.LogWarning("30");
                                    }
                                }
                            }
                        } // while ((bytesRead = br.Read(buffer, 0, buffer.Length)) > 0)
                    }
                }
                catch (Exception ex)
                {
                    _loggerClient.LogWarning("Sharepoint Exception");
                    _loggerClient.LogException(ex);
                    _loggerClient.LogException(ex.InnerException);

                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Dispose();
                    }
                }
            }
            string DestinationFileName = "";
            string ServerDomain = "";
            ServerDomain = new Uri(ctx.Url).GetLeftPart(UriPartial.Authority);
            DestinationFileName = uniqueFileName;
            var fileUrl = string.Format("{0}{1}", ServerDomain + docs.RootFolder.ServerRelativeUrl + System.IO.Path.AltDirectorySeparatorChar, DestinationFileName);
            //var v = uploadFile.ListItemAllFields["Thumbnail"];
            sharePointFile.ActualFileName = file.FileName;
            sharePointFile.ShareableLink = ctx.Web.CreateAnonymousLinkForDocument((fileUrl), ExternalSharingDocumentOption.View);
            sharePointFile.SharepointUploadedFileURL = fileUrl;
            sharePointFile.UploadedFileName = DestinationFileName;
            //return ctx.Web.CreateAnonymousLinkForDocument((fileUrl), ExternalSharingDocumentOption.View);
            return sharePointFile;
        }

        public static SharePointFileView UploadAssignmentFiles(ref ClientContext ctx, FileModulesConstants ModuleName, string oldUserId, HttpPostedFileBase files, int fileChunkSizeInMB = 3)
        {
            ILoggerClient _loggerClient = LoggerClient.Instance;
            Microsoft.SharePoint.Client.File uploadFile = null;
            bool LibraryExists = false;
            bool IsFolderExists = false;
            Guid uploadId = Guid.NewGuid();
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            string uploadedfileName = Path.GetFileName(files.FileName);
            string uniqueFileName = uploadedfileName;
            SharePointFileView shv = new SharePointFileView();
            if (!IsLibraryExists(ref ctx, ModuleNametoUpload))
            {
                LibraryExists = CreateLibrary(ref ctx, ModuleNametoUpload);
            }
            else
            {
                LibraryExists = true;
            }
            if (LibraryExists)
            {
                List List = ctx.Web.Lists.GetByTitle(ModuleNametoUpload);
                ctx.Load(List);
                ctx.Load(List.RootFolder);
                //  ctx.Load(List.RootFolder.Folders);
                ctx.ExecuteQuery();
                // FolderCollection folders = List.RootFolder.Folders;
                //foreach (Folder fldr in folders)
                //{
                //    if (fldr.Name == oldUserId)
                //    {
                //        IsFolderExists = true;
                //        break;
                //    }
                //}
                IsFolderExists = FolderExists(ctx, List.RootFolder.ServerRelativeUrl + "/" + oldUserId);

                if (!IsFolderExists)
                {
                    if (CreateFolder(ref ctx, ref List, oldUserId))
                        IsFolderExists = true;
                }
                if (IsFolderExists)
                {
                    var targetFolder = ctx.Web.GetFolderByServerRelativeUrl(List.RootFolder.ServerRelativeUrl + "/" + oldUserId);
                    int blockSize = fileChunkSizeInMB * 1024 * 1024;
                    ctx.ExecuteQuery();
                    long fileSize = files.InputStream.Length;
                    string ServerDomain = "";
                    ServerDomain = new Uri(ctx.Url).GetLeftPart(UriPartial.Authority);
                    uploadedfileName = SessionHelper.CurrentSession.FirstName + "_" + SessionHelper.CurrentSession.LastName + "_" + uploadedfileName;
                    var fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, uploadedfileName);
                    while (CheckFileExists(ref ctx, fileUrl))
                    {
                        uploadedfileName = uniqueFileName;
                        string fullName = SessionHelper.CurrentSession.FirstName + "_" + SessionHelper.CurrentSession.LastName;
                        uploadedfileName = Guid.NewGuid() + "_" + fullName.Replace("/","") + "_" + uploadedfileName;
                        fileUrl = string.Format("{0}/{1}", ServerDomain + List.RootFolder.ServerRelativeUrl + "/" + oldUserId, uploadedfileName);
                    }

                    if (fileSize <= blockSize)
                    {
                        FileCreationInformation fileInfo = new FileCreationInformation();
                        fileInfo.ContentStream = files.InputStream;
                        fileInfo.Url = uploadedfileName;
                        fileInfo.Overwrite = true;
                        uploadFile = targetFolder.Files.Add(fileInfo);
                        ctx.Load(uploadFile);
                        ctx.ExecuteQuery();
                    }
                    else
                    {
                        ClientResult<long> bytesUploaded = null;
                        FileStream fs = null;
                        try
                        {
                            fs = files.InputStream as FileStream;
                            using (BinaryReader br = new BinaryReader(files.InputStream))
                            {
                                byte[] buffer = new byte[blockSize];
                                long fileoffset = 0;
                                long totalBytesRead = 0;
                                int bytesRead;
                                bool first = true;
                                while ((bytesRead = br.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    totalBytesRead = totalBytesRead + bytesRead;
                                    if (first)
                                    {
                                        using (MemoryStream contentStream = new MemoryStream())
                                        {
                                            FileCreationInformation fileInfo = new FileCreationInformation();
                                            fileInfo.ContentStream = contentStream;
                                            fileInfo.Url = uploadedfileName;
                                            fileInfo.Overwrite = true;
                                            uploadFile = targetFolder.Files.Add(fileInfo);
                                            using (MemoryStream s = new MemoryStream(buffer))
                                            {
                                                bytesUploaded = uploadFile.StartUpload(uploadId, s);
                                                ctx.ExecuteQuery();
                                                fileoffset = bytesUploaded.Value;
                                            }
                                            first = false;
                                        }
                                    }
                                    else
                                    {
                                        uploadFile = ctx.Web.GetFileByServerRelativeUrl(List.RootFolder.ServerRelativeUrl + '/' + oldUserId + System.IO.Path.AltDirectorySeparatorChar + uploadedfileName);
                                        if (totalBytesRead == fileSize)
                                        {
                                            using (MemoryStream s = new MemoryStream(buffer, 0, bytesRead))
                                            {
                                                uploadFile = uploadFile.FinishUpload(uploadId, fileoffset, s);
                                                ctx.ExecuteQuery();
                                            }
                                        }
                                        else
                                        {
                                            using (MemoryStream s = new MemoryStream(buffer))
                                            {
                                                bytesUploaded = uploadFile.ContinueUpload(uploadId, fileoffset, s);
                                                ctx.ExecuteQuery();
                                                fileoffset = bytesUploaded.Value;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _loggerClient.LogWarning("Sharepoint Exception");
                            _loggerClient.LogException(ex);
                            _loggerClient.LogException(ex.InnerException);

                        }
                        finally
                        {
                            if (fs != null)
                            {
                                fs.Dispose();
                            }
                        }
                    }
                    ServerDomain = new Uri(ctx.Url).GetLeftPart(UriPartial.Authority);
                    shv.ShareableLink = fileUrl; //ctx.Web.CreateAnonymousLinkForDocument((fileUrl), ExternalSharingDocumentOption.View);
                    shv.SharepointUploadedFileURL = fileUrl;
                }

            }

            return shv;
        }

        private static bool FolderExists(ClientContext context, string url)
        {
            var folder = context.Web.GetFolderByServerRelativeUrl(url);
            context.Load(folder, f => f.Exists);
            try
            {
                context.ExecuteQuery();
                if (folder.Exists)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}