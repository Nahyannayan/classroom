﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;

namespace Phoenix.VLE.Web.Helpers
{
    public static class AccessPermissionHelper
    {
        public static void HandleAcquireRequestState(HttpContext context)
        {
            if (context != null)
            {
                bool isAjaxCall = string.Equals("XMLHttpRequest", context.Request.Headers["x-requested-with"], StringComparison.OrdinalIgnoreCase);
                //bool isAjaxCall= new HttpRequestWrapper(context.Request).IsAjaxRequest()
                var queryStringSID = Convert.ToString(context.Request.QueryString["sid"]);
                //SessionHelper.ReCreateSessionToStayLoggedIn(queryStringSID);

                if (!IsUrlAllowedWithoutLogin(context.Request.Url.AbsolutePath))
                {
                    string controllerUrl = Common.Helpers.CommonHelper.GetControllerUrl();
                    string permissionUrl = Common.Helpers.CommonHelper.GetControllerActionUrl().Replace("/index", "");
                    IUserPermissionService userPermissionService = new UserPermissionService();

                    // login required to access this page
                    //ILoggerClient _loggerClient = LoggerClient.Instance;

                    if (!SessionHelper.IsSessionActive())
                    {
                        SessionHelper.LogOffUser();

                        if (isAjaxCall)
                        {
                            context.Response.ContentType = "application/json";
                            context.Response.StatusCode = 401;
                            return;
                        }
                        else
                        {
                            SessionHelper.RedirectToLoginPage(context.Request.Url);
                        }

                    }

                    else if (!isAjaxCall)
                    {
                        // its not an Ajax Call, user opened the page either through Menu or through Browser Address Bar
                        // user already logged in, check whether he/she got access to this page.                   

                        if (context.User.Identity.IsAuthenticated && !IsUrlAllowedWithoutPermission(context.Request.Url.AbsolutePath))
                        {
                            var user = SessionHelper.CurrentSession;
                            // Check if current user is allowed to access this page?
                            var syncContext = SynchronizationContext.Current;
                            SynchronizationContext.SetSynchronizationContext(null);
                            var isPermissionAssigned = userPermissionService.IsPermissionAssigned(user.Id, permissionUrl, user.UserTypeId).Result;
                            SynchronizationContext.SetSynchronizationContext(syncContext);

                            if (!isPermissionAssigned)
                            {
                                context.Response.Redirect(Common.Helpers.PhoenixConfiguration.Instance.NoPermissionPageUrl);
                                return;
                            }

                        }
                        else
                        {
                            return;
                        }
                    }
                    bool renewPermission = true;
                    string permissionKey = Common.Helpers.CommonHelper.GetPermissionKey();

                    // If its a non-ajax call (browser refresh or new page opening) then permission has to be refreshed.
                    // If its an ajax call then get permission from Session.
                    if (isAjaxCall)
                    {
                        if (HttpContext.Current.Session != null)
                        {

                            if (HttpContext.Current.Session[permissionKey] != null)
                            {
                                renewPermission = false;
                            }

                        }
                        else
                        {
                            renewPermission = false;
                        }
                    }
                    if (renewPermission)
                    {
                        var user = SessionHelper.CurrentSession;
                        var syncContext = SynchronizationContext.Current;
                        SynchronizationContext.SetSynchronizationContext(null);
                        PagePermission CurrentPagePermission = userPermissionService.GetUserPermissions(user.Id, controllerUrl, null, user.UserTypeId).Result;
                        SynchronizationContext.SetSynchronizationContext(syncContext);

                        if (HttpContext.Current.Session != null)
                        {
                            HttpContext.Current.Session[permissionKey] = CurrentPagePermission;
                        }

                    }
                }
            }
        }


        #region Urls allowed without login and permission


        /// <summary>
        /// The list of allowed urls, neither login nor permission is required.
        /// </summary>
        /// <returns></returns>
        public static IList<string> GetAllowedUrlsWithoutLogin()
        {
            IList<string> allowedUrlList = new List<string>();
            allowedUrlList.Add("/");
            allowedUrlList.Add("/elmah.axd");
            allowedUrlList.Add("/elmah.axd/stylesheet");
            allowedUrlList.Add("/elmah.axd/detail");
            allowedUrlList.Add("/account/enterclassroom");
            allowedUrlList.Add("/account/signin");
            allowedUrlList.Add("/account/adlogin");
            allowedUrlList.Add("/account/mssignout");
            allowedUrlList.Add("/account/authcallback");
            allowedUrlList.Add("/account/signout");
            allowedUrlList.Add("/account/samlconsume");
            allowedUrlList.Add("/content/vle/img/smileys");
            allowedUrlList.Add("/document/viewer");
            allowedUrlList.Add("/schoolinfo/blog/uploadeditorimage");
            allowedUrlList.Add("/planner/myplanner/acceptrequest");//for accept request of planner from mail
            allowedUrlList.Add("/planner/myplanner/acceptexternalrequest");
            allowedUrlList.Add("/account/enter");
            allowedUrlList.Add("/account/forgotpassword");
            allowedUrlList.Add("/account/resetpassword/{id}");
            allowedUrlList.Add("/account/changelanguage");
            allowedUrlList.Add("/schoolinfo/contactinfo/senddemorequestmail");

            allowedUrlList.Add("/bundles/toplayoutjs");
            allowedUrlList.Add("/bundles/calendarbundlejs-teacher");
            allowedUrlList.Add("/bundles/bottomlayoutjs");
            allowedUrlList.Add("/bundles/datapagelibrariesjs");
            allowedUrlList.Add("/bundles/calendarbundlejs-student");
            allowedUrlList.Add("/bundles/toplayoutstudentjs");
            allowedUrlList.Add("/bundles/loginjs");

            allowedUrlList.Add("/content/bootstrapcore");
            allowedUrlList.Add("/content/materialcss");
            allowedUrlList.Add("/content/datatablecss");
            allowedUrlList.Add("/content/main-teacher");
            allowedUrlList.Add("/content/main-parent");
            allowedUrlList.Add("/content/bootstrapcorestudent");
            allowedUrlList.Add("/content/datatablecssstudent");
            allowedUrlList.Add("/content/main-student");
            allowedUrlList.Add("/content/skin-1");
            allowedUrlList.Add("/content/calendar");
            allowedUrlList.Add("/content/skin-default");
            allowedUrlList.Add("/content/calendar");
            allowedUrlList.Add("/content/VLE/css/bootstrap.min.css");
            allowedUrlList.Add("/content/VLE/css/bootstrap-rtl.css");
            allowedUrlList.Add("/account/login");
            allowedUrlList.Add("/error/nomapping");
            allowedUrlList.Add("/home/GetTermsAndConditionHtmlPage");
            allowedUrlList.Add("/home/GetPrivacyHtmlPage");
            allowedUrlList.Add("/home/GetEliteTermsAndConditionHtmlPage");
            allowedUrlList.Add("/home/GetElitePrivacyHtmlPage");
            allowedUrlList.Add("/parent/initSetting");
            //allowedUrlList.Add("/schoolinfo/reports");
            allowedUrlList.Add("/behaviour/behaviour/certificate");
            return allowedUrlList;
        }
        public static IList<string> GetForgotPasswordAllowedUrlsWithoutLogin()
        {
            IList<string> allowedUrlList = new List<string>();
            allowedUrlList.Add("/account/postforgetusername");
            allowedUrlList.Add("/account/passwordchange");
            allowedUrlList.Add("/account/getvalidateusername");
            return allowedUrlList;
        }
        /// <summary>
        /// Checks if the given url is in the list of allowed urls
        /// </summary>
        /// <returns></returns>
        public static bool IsUrlAllowedWithoutLogin(string url)
        {
            if (url.Substring(url.Length - 1, 1) == "/" || url.Substring(url.Length - 1, 1) == "\\")
            {
                url = url.Substring(0, url.Length - 1);
            }
            return GetAllowedUrlsWithoutLogin().Contains(url.ToLower().Replace("/index", ""));
        }

        #endregion

        #region Urls allowed with login and without any permission

        /// <summary>
        /// The list of allowed urls, login required, but permission isn't required.
        /// </summary>
        /// <returns></returns>
        public static IList<string> GetAllowedUrlsWithoutPermission()
        {
            IList<string> allowedUrlList = new List<string>();
            allowedUrlList.Add("");
            allowedUrlList.Add("/");
            //allowedUrlList.Add("/home");
            allowedUrlList.Add("/error/index");
            allowedUrlList.Add("/error/nopermission");
            allowedUrlList.Add("/error/notfound");
            allowedUrlList.Add("/account/authcallback");
            allowedUrlList.Add("/error/httperror500");
            allowedUrlList.Add("/error/httperror404");
            allowedUrlList.Add("/shared/shared");
            allowedUrlList.Add("/content/vle/img/smileys");
            allowedUrlList.Add("/Content/vle/img/");
            allowedUrlList.Add("/Resources/content/profilephotos/teachers/");
            allowedUrlList.Add("/shared/shared/downloadfile");
            allowedUrlList.Add("/error/nomapping");
            allowedUrlList.Add("/dxxrdv.axd");
            allowedUrlList.Add("/reportviewer/reportviewer.aspx");
            allowedUrlList.Add("/home/gettermsandconditionhtmlpage");
            allowedUrlList.Add("/home/getprivacyhtmlpage");
            allowedUrlList.Add("/home/getelitetermsandconditionhtmlpage");
            allowedUrlList.Add("/home/geteliteprivacyhtmlpage");
            allowedUrlList.Add("/files/files/driveauthcallback");
            allowedUrlList.Add("/files/files/ononedriveauthorization");
            allowedUrlList.Add("/filedownlod/downloadblobfile");            
            return allowedUrlList;
        }

        /// <summary>
        /// Checks if the given url is in the list of allowed urls
        /// </summary>
        /// <returns></returns>
        public static bool IsUrlAllowedWithoutPermission(string url)
        {
            return GetAllowedUrlsWithoutPermission().Contains(url.ToLower().Replace("/index", ""));
        }

        #endregion
    }
}