﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using HtmlToOpenXml;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace Phoenix.Web.Helpers
{
    public static class EPPlusHelper
    {
        public static DataTable ToDataTable(this ExcelPackage package)
        {
            ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
            DataTable table = new DataTable();
            foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
            {
                table.Columns.Add(firstRowCell.Text);
            }

            for (var rowNumber = 1; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
            {
                var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                var newRow = table.NewRow();
                foreach (var cell in row)
                {
                    newRow[cell.Start.Column - 1] = string.IsNullOrEmpty(cell.Text) ? string.Empty : cell.Text;
                }
                for (int i = 0; i < newRow.ItemArray.Length; i++)
                {
                    if (newRow.ItemArray[i] == DBNull.Value)
                    {
                        newRow.SetField<string>(i, string.Empty);
                    }
                }
                table.Rows.Add(newRow);
            }
            return table;
        }

        public static byte[] HtmlToWord(String html)
        {
            //const string filename = "test.docx";
            //if (File.Exists(filename)) File.Delete(filename);

            using (MemoryStream generatedDocument = new MemoryStream())
            {
                using (WordprocessingDocument package = WordprocessingDocument.Create(
                       generatedDocument, WordprocessingDocumentType.Document))
                {
              //      PreventDocumentEdition(package);
                    MainDocumentPart mainPart = package.MainDocumentPart;
                    if (mainPart == null)
                    {
                        mainPart = package.AddMainDocumentPart();
                        new Document(new Body()).Save(mainPart);
                    }

                    HtmlConverter converter = new HtmlConverter(mainPart);
                    Body body = mainPart.Document.Body;
                    converter.BaseImageUrl = new Uri(Phoenix.Common.Helpers.PhoenixConfiguration.Instance.ReadFilePath);


                    var paragraphs = converter.Parse(html);
                    for (int i = 0; i < paragraphs.Count; i++)
                    {
                        body.Append(paragraphs[i]);
                    }

                    mainPart.Document.Save();
                }

                return generatedDocument.ToArray();
            }
        }

        public static void PreventDocumentEdition(WordprocessingDocument document)
        {
            document.ExtendedFilePropertiesPart.Properties.DocumentSecurity.Text = "8";
            document.ExtendedFilePropertiesPart.Properties.Save();

            Settings settings = document.MainDocumentPart.DocumentSettingsPart.Settings;
            settings.Append(
                 new DocumentProtection()
                 {
                     Edit = DocumentProtectionValues.ReadOnly,
                     Enforcement = new OnOffValue(true)
                 });

            settings.Save();
        }

    }
}