﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

using Phoenix.Common.Models;
using Phoenix.Common.Localization;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.Models;

namespace Phoenix.VLE.Web.Helpers
{
    public static class SelectListHelper
    {

        #region GetSelectListData

        public static IList<ListItem> GetSelectListData(ListItems listCode)
        {
            return GetSelectListData(listCode, "", "").ToList();
        }

        public static IList<ListItem> GetSelectListData(ListItems listCode, object whereConditionParamValues)
        {
            return GetSelectListData(listCode, "", whereConditionParamValues).ToList();
        }

        public static IList<ListItem> GetSelectListData(ListItems listCode, string whereCondition, object whereConditionParamValues)
        {
            ISelectListService service = new SelectListService();
            return service.GetSelectListItems(StringEnum.GetStringValue(listCode), whereCondition, whereConditionParamValues).ToList();
        }

        #endregion

        #region Custom Select List Methods
        public static IList<SubjectListItem> GetSubjectListByUserId(int userId)
        {
            ISelectListService service = new SelectListService();
            return service.GetSubjectsByUserId(userId).ToList();
        }
        public static IList<AcademicYearList> GetAcademicYearByUserId(int userId)
        {
            ISelectListService service = new SelectListService();
            return service.GetAcademicYearByUserId(userId).ToList();
        }

        public static IList<ListItem> GetAssignmentCategoryMasterList()
        {
            ISelectListService service = new SelectListService();
            return service.GetAssignmentCategoryMasterList().ToList();
        }

        public static List<AssignmentCategory> GetAssignmentCategoryMappingListBySchoolId(long SchoolId)
        {
            ISelectListService service = new SelectListService();
            return service.GetAssignmentCategoryMappingListBySchoolId(SchoolId).ToList();
        }
        #endregion

        #region Miscellaneous

        #endregion
    }
}
