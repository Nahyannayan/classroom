﻿using FlickrNet;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web
{
    public class FlickrManager
    {
        public string ApiKey = ConfigurationManager.AppSettings["FlickrApiKey"];
        public string SharedSecret = ConfigurationManager.AppSettings["FlickrSharedSecret"];
        
        public MediaModal GetPhotos(string photoStr, int currentPage, int perPage)
        {
            var photoList = new Flickr(ApiKey, SharedSecret).PhotosSearch(new PhotoSearchOptions()
            {
                Extras = PhotoSearchExtras.LargeUrl | PhotoSearchExtras.Tags | PhotoSearchExtras.OriginalFormat,
                Tags = photoStr,
                PerPage = perPage,
                Page = currentPage,
                SafeSearch = SafetyLevel.Restricted,
                ContentType = ContentTypeSearch.PhotosAndScreenshots
            });
            return new MediaModal
            {
                Count = photoList.Total,
                PhotoList = photoList.Select(x => x)
            };
        }

        public static void SaveImage(string Url, ref string filename)
        {
            filename = HttpContext.Current.Server.MapPath("~/Content/" + filename);
            WebClient client = new WebClient();
            client.DownloadFile(new Uri(Url), filename);

            File.Move(filename, Path.ChangeExtension(filename, ".jpg"));
        }
    }
}
