﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Specialized;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.VLE.Web.Helpers
{
    public class AzureBlobStorageHelper
    {
        private static string SchoolCode;

        public string BlobUrl(string FileName) => $"{BaseURL()}/{RootContainerName().ToLower()}/{FileName}";
        public static string GetBlobUrlFromQueryString(string FileName) => string.Format("{0}", FileName.ToLower().Contains("downloadblobfile") ? FileName.Split(new string[] { "FileName=" }, StringSplitOptions.None)[1] : FileName);
        public string BaseURL() => ConfigurationManager.AppSettings["AzureStorageBlobUrl"].ToString();
        public string BaseURL(string FileName) => $"GetBlobSasUri({containerClient}, {FileName})";
        private static string AccountName() => ConfigurationManager.AppSettings["AzureStorageAccountName"].ToString();
        private static string AccountSecret() => ConfigurationManager.AppSettings["AzureStorageAccountSecret"].ToString();
        private static string RootContainerName() => $"{SchoolCode.ToLower()}";
        //private Uri GetBlobUri(string fileName) => new Uri(GetBlobSasUri(containerClient, fileName));

        //private Uri GetBlobUri(string fileName) => new Uri($"{BaseURL()}/{RootContainerName().ToLower()}/{fileName}");

        public static string GetBlobUri(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return fileName;
            if (fileName.ToLower().Contains(".sharepoint.com"))
                return fileName;
            if (fileName.ToLower().Contains("/resources"))
                return fileName;
            var newFileName = fileName != null ? (fileName.ToLower().Contains("downloadblobfile") ? fileName : $"/FileDownlod/DownloadBlobFile?FileName={fileName}") : "";
            return newFileName;
        }
        private BlobServiceClient blobServiceClient;
        private BlobContainerClient containerClient;
        private ILoggerClient _loggerClient = LoggerClient.Instance;
        #region LoginBlobStoarage GetContainerName Set AccessPolicy

        public async Task Connect(string schoolCode)
        {
            SchoolCode = schoolCode;
            blobServiceClient = new BlobServiceClient(ConfigurationManager.AppSettings["AzureBlobStorageConnectionString"].ToString());
            containerClient = blobServiceClient.GetBlobContainerClient(RootContainerName().ToLower());
            if (!await containerClient.ExistsAsync())
            {
                await containerClient.CreateAsync();
                await containerClient.SetAccessPolicyAsync(Azure.Storage.Blobs.Models.PublicAccessType.Blob);
            }
        }

        #endregion LoginBlobStoarage GetContainerName Set AccessPolicy

        #region Check File Name Exit in Blob

        public bool CheckFileExists(string FilePath)
        {
            return containerClient.GetBlobClient(FilePath).Exists();
        }

        #endregion Check File Name Exit in Blob

        #region Upload Single File On Blob As Per Module

        public Task<SharePointFileView> UploadFilesAsPerModuleAsync(FileModulesConstants ModuleName, string oldUserId, HttpPostedFileBase file)
        {
            //SharePointFileView shv = new SharePointFileView();
            return UploadFileSlicePerSliceAsync(ModuleName, oldUserId, file, 5);
            //return shv;
        }

        #endregion Upload Single File On Blob As Per Module

        #region UploadFilesList On BlobStorage

        public async Task<List<FileEdit>> UploadFilesListAsPerModuleAsync(FileModulesConstants ModuleName, string oldUserId, List<FileEdit> fileList)
        {
            Stream fs = null;
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            string FileNamePrefix = ModuleNametoUpload + "/" + oldUserId;
            try
            {
                foreach (var file in fileList)
                {
                    using (fs = file.PostedFile.InputStream)
                    {
                        string DestinationFileName = "";
                        DestinationFileName = file.FileName.ToLower();
                        var fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), DestinationFileName.ToLower());
                        while (CheckFileExists(fileUrl))
                        {
                            var random = new Random(DateTime.Now.Millisecond);
                            int randomNumber = random.Next(1, 500000);
                            DestinationFileName = randomNumber + "_" + DestinationFileName;
                            fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), DestinationFileName.ToLower());
                        }
                        var ActualBlobUri = BlobUrl(fileUrl);
                        var blobClient = containerClient.GetBlobClient(fileUrl);
                        var info = await blobClient.UploadAsync(file.PostedFile.InputStream, new Azure.Storage.Blobs.Models.BlobHttpHeaders { ContentType = DestinationFileName.GetContentType() });
                        file.PhysicalFilePath = ActualBlobUri;
                        file.FilePath = ActualBlobUri;
                    }
                }
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Azure Blob Storage Exception");
                _loggerClient.LogException(ex);
                _loggerClient.LogException(ex.InnerException);
            }
            finally
            {
                if (fs != null)
                {
                    fs.Dispose();
                }
            }

            return fileList;
        }

        #endregion UploadFilesList On BlobStorage

        #region UploadFilesList On BlobStorage As Per Module

        public async Task<List<FileEdit>> UploadFilesListAsPerModuleCameraAsync(FileModulesConstants ModuleName, string oldUserId, List<FileEdit> fileList, string relativePath)
        {
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            string FileNamePrefix = ModuleNametoUpload + "/" + oldUserId;

            foreach (var file in fileList)
            {
                using (var fs = new FileStream(HttpContext.Current.Server.MapPath(relativePath), FileMode.Open, FileAccess.Read))
                {
                    string DestinationFileName = "";
                    DestinationFileName = file.FileName;
                    var fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), DestinationFileName.ToLower());
                    while (CheckFileExists(fileUrl))
                    {
                        var random = new Random(DateTime.Now.Millisecond);
                        int randomNumber = random.Next(1, 500000);
                        DestinationFileName = randomNumber + "_" + DestinationFileName;
                        fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), DestinationFileName.ToLower());
                    }
                    var ActualBlobUri = BlobUrl(fileUrl);
                    var blobClient = containerClient.GetBlobClient(fileUrl);
                    var info = await blobClient.UploadAsync(fs, new Azure.Storage.Blobs.Models.BlobHttpHeaders { ContentType = DestinationFileName.GetContentType() });
                    file.PhysicalFilePath = ActualBlobUri;
                    file.FilePath = ActualBlobUri;
                }
            }
            return fileList;
        }

        #endregion UploadFilesList On BlobStorage As Per Module

        #region UploadMultipleFiles On BlobStorage As Per Module

        public async Task<List<SharePointFileView>> UploadMultipleFilesAsPerModuleAsync(FileModulesConstants ModuleName, string oldUserId, HttpPostedFileBase[] files)
        {
            string ShareableLink = string.Empty;
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            string FileNamePrefix = ModuleNametoUpload + "/" + oldUserId;
            List<SharePointFileView> lstSharepointFiles = new List<SharePointFileView>();
            foreach (var file in files)
            {
                using (var fs = file.InputStream)
                {
                    try
                    {
                        SharePointFileView shv = new SharePointFileView();
                        shv.ActualFileName = file.FileName;
                        shv.UploadedFileName = file.FileName;
                        var fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), shv.UploadedFileName.ToLower());
                        while (CheckFileExists(fileUrl))
                        {
                            shv.UploadedFileName = Guid.NewGuid() + "_" + shv.UploadedFileName;
                            fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), shv.UploadedFileName.ToLower());
                        }
                        var inputFile = file;
                        double fileSizeInMb = (file.ContentLength / 1024f) / 1024f;
                        var ActualBlobUri = BlobUrl(fileUrl);
                        var blobClient = containerClient.GetBlobClient(fileUrl);
                        var info = await blobClient.UploadAsync(file.InputStream, new Azure.Storage.Blobs.Models.BlobHttpHeaders { ContentType = shv.UploadedFileName.GetContentType() });

                        ShareableLink = ActualBlobUri;
                        shv.ShareableLink = ShareableLink;
                        shv.SharepointUploadedFileURL = ShareableLink;
                        lstSharepointFiles.Add(shv);
                    }
                    catch (Exception ex)
                    {
                        _loggerClient.LogWarning("Azure Blob Storage Exception");
                        _loggerClient.LogException(ex);
                        _loggerClient.LogException(ex.InnerException);
                    }
                    finally
                    {
                        if (fs != null)
                        {
                            fs.Dispose();
                        }
                    }
                }
            }
            return lstSharepointFiles;
        }

        #endregion UploadMultipleFiles On BlobStorage As Per Module

        #region UploadFileSlicePerSlice On BlobStorage

        public async Task<SharePointFileView> UploadFileSlicePerSliceAsync(FileModulesConstants ModuleName, string oldUserId, HttpPostedFileBase files, int fileChunkSizeInMB = 3)
        {
            Guid uploadId = Guid.NewGuid();
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            string uniqueFileName = Path.GetFileName(files.FileName);
            SharePointFileView sharePointFile = new SharePointFileView();
            int blockSize = fileChunkSizeInMB * 1024 * 1024;
            long fileSize = files.InputStream.Length;
            string FileNamePrefix = ModuleNametoUpload + "/" + oldUserId;
            var fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), uniqueFileName.ToLower());
            while (CheckFileExists(fileUrl))
            {
                uniqueFileName = Guid.NewGuid() + "_" + uniqueFileName;
                fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), uniqueFileName.ToLower());
            }
            try
            {
                var ActualBlobUri = BlobUrl(fileUrl);
                var blobClient = containerClient.GetBlobClient(fileUrl);
                var blobInfo = await blobClient.UploadAsync(files.InputStream, new Azure.Storage.Blobs.Models.BlobHttpHeaders { ContentType = uniqueFileName.GetContentType() });

                double fileSizeInMb = (files.ContentLength / 1024f) / 1024f;

                var ShareableLink = ActualBlobUri;
                sharePointFile.ActualFileName = files.FileName;
                sharePointFile.ShareableLink = ShareableLink;
                sharePointFile.SharepointUploadedFileURL = ShareableLink;
                sharePointFile.UploadedFileName = uniqueFileName;
                sharePointFile.FileSizeInMb = fileSizeInMb;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Azure Blob Storage Exception");
                _loggerClient.LogException(ex);
                _loggerClient.LogException(ex.InnerException);
            }
            finally
            {
                if (files.InputStream != null)
                {
                    files.InputStream.Dispose();
                }
            }
            return sharePointFile;
        }

        #endregion UploadFileSlicePerSlice On BlobStorage

        #region DownloadFiles From BlobStorage

        public async Task<byte[]> DownloadFilesAsync(string FileName)
        {
            //FileName = FileName.Replace($"{BaseURL()}/{RootContainerName().ToLower()}/", "");

            var blobClient = new BlobClient(new Uri(FileName));
            var blobDownloadInfo = await blobClient.DownloadAsync();
            var blobData = new AzureBlobStorage.BlobFileInfo(blobDownloadInfo.Value.Content, blobDownloadInfo.Value.ContentType, blobClient.Name);
            byte[] imageArray = null;
            using (MemoryStream mStream = new MemoryStream())
            {
                if (blobData.Content != null)
                {
                    blobData.Content.CopyTo(mStream);
                    imageArray = mStream.ToArray();
                }
            }
            return imageArray;
        }

        #endregion DownloadFiles From BlobStorage

        #region Download Files Stream From BlobStorage

        public async Task<AzureBlobStorage.BlobFileInfo> DownloadFilesAsStreamAsync(string FileName)
        {
            //FileName = FileName.Replace($"{BaseURL()}/{RootContainerName().ToLower()}/", "");
            var blobClient = new BlobClient(new Uri(FileName));
            var blobDownloadInfo = await blobClient.DownloadAsync();
            var blobData = new AzureBlobStorage.BlobFileInfo
(blobDownloadInfo.Value.Content, blobDownloadInfo.Value.ContentType, blobClient.Name);
            return blobData;
        }

        #endregion Download Files Stream From BlobStorage

        #region ReplaceFile In BlobStoarge

        public async Task<string> ReplaceFileAsync(byte[] fileData, string uploadedFileUrl)
        {
            var blobClient = containerClient.GetBlobClient(uploadedFileUrl);
            //var blobClient = new BlobClient(new Uri(uploadedFileUrl));
            using (var memoryStream = new MemoryStream(fileData))
            {
                var info = await blobClient.UploadAsync(memoryStream, new Azure.Storage.Blobs.Models.BlobHttpHeaders { ContentType = uploadedFileUrl.GetContentType() });
            }
            return BlobUrl(uploadedFileUrl);
        }

        #endregion ReplaceFile In BlobStoarge

        #region Copy To OtherLocation In BlobStorage

        public async Task<string> CopyToOtherLocationAsync(string TargetFileName, FileModulesConstants ModuleName, string OldUserId)
        {
            string ShareableLink = string.Empty;
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            var fileName = TargetFileName.Split(new string[] { "FileName=" }, StringSplitOptions.None)[1];
            var containerClient = blobServiceClient.GetBlobContainerClient(RootContainerName().ToLower());
            var blobClient = containerClient.GetBlobClient(fileName);
            var blobDownloadInfo = await blobClient.DownloadAsync();
            var FileInfo = new AzureBlobStorage.BlobFileInfo(blobDownloadInfo.Value.Content, blobDownloadInfo.Value.ContentType, blobClient.Name);
            var newFileLocation = ModuleNametoUpload + "/" + OldUserId;
            var newFileName = newFileLocation + "/" + fileName.Split('/')[2];
            containerClient = blobServiceClient.GetBlobContainerClient(RootContainerName().ToLower());
            var newBlobClient = containerClient.GetBlobClient(newFileName);
            var returnData = newBlobClient.Upload(FileInfo.Content, new Azure.Storage.Blobs.Models.BlobHttpHeaders { ContentType = newFileName.GetContentType() });
            ShareableLink = BlobUrl(newFileName);
            return ShareableLink;
        }

        #endregion Copy To OtherLocation In BlobStorage

        #region UploadStudentFiles in Blob Storage

        public async Task<SharePointFileView> UploadStudentFilesAsPerModuleAsync(FileModulesConstants ModuleName, string oldUserId, HttpPostedFileBase file)
        {
            string ShareableLink = string.Empty;
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            SharePointFileView sharePointFile = new SharePointFileView();
            string FileNamePrefix = ModuleNametoUpload + "/" + oldUserId;
            using (var fs = file.InputStream)
            {
                try
                {
                    string DestinationFileName = "";
                    DestinationFileName = file.FileName;
                    var fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), DestinationFileName.ToLower());
                    while (CheckFileExists(fileUrl))
                    {
                        DestinationFileName = Guid.NewGuid() + "_" + DestinationFileName;
                        fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), DestinationFileName.ToLower());
                    }
                    var ActualBlobUri = BlobUrl(fileUrl);
                    var blobClient = containerClient.GetBlobClient(fileUrl);
                    var info = await blobClient.UploadAsync(file.InputStream, new Azure.Storage.Blobs.Models.BlobHttpHeaders { ContentType = DestinationFileName.GetContentType() });

                    double fileSizeInMb = (file.ContentLength / 1024f) / 1024f;
                    ShareableLink = ActualBlobUri;
                    sharePointFile.ActualFileName = file.FileName;
                    sharePointFile.ShareableLink = ShareableLink;
                    sharePointFile.SharepointUploadedFileURL = ShareableLink;
                    sharePointFile.UploadedFileName = DestinationFileName;
                    sharePointFile.FileSizeInMb = fileSizeInMb;
                }
                catch (Exception ex)
                {
                    _loggerClient.LogWarning("Azure Blob Storage Exception");
                    _loggerClient.LogException(ex);
                    _loggerClient.LogException(ex.InnerException);
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Dispose();
                    }
                }
            }
            return sharePointFile;
        }

        #endregion UploadStudentFiles in Blob Storage

        #region UploadFileSlicePerSlice In BlobStorage

        public async Task<SharePointFileView> UploadFileSlicePerSliceAsync(FileModulesConstants ModuleName, string oldUserId, HttpPostedFileViewModel file, int fileChunkSizeInMB = 3)
        {
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            string uniqueFileName = Path.GetFileName(file.FileName);
            SharePointFileView shv = new SharePointFileView();
            string FileNamePrefix = ModuleNametoUpload + "/" + oldUserId;
            int blockSize = fileChunkSizeInMB * 1024 * 1024;
            long fileSize = file.InputStream.Length;
            var fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), uniqueFileName.ToLower());
            while (CheckFileExists(fileUrl))
            {
                uniqueFileName = Guid.NewGuid() + "_" + uniqueFileName;
                fileUrl = string.Format("{0}/{1}", FileNamePrefix.ToLower(), uniqueFileName.ToLower());
            }
            try
            {
                var ActualBlobUri = BlobUrl(fileUrl);
                var blobClient = containerClient.GetBlobClient(fileUrl);
                var info = await blobClient.UploadAsync(file.InputStream, new Azure.Storage.Blobs.Models.BlobHttpHeaders { ContentType = uniqueFileName.GetContentType() });

                shv.ShareableLink = ActualBlobUri;
                shv.SharepointUploadedFileURL = ActualBlobUri;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Sharepoint Exception");
                _loggerClient.LogException(ex);
                _loggerClient.LogException(ex.InnerException);
            }
            finally
            {
                if (file.InputStream != null)
                {
                    file.InputStream.Dispose();
                }
            }
            return shv;
        }

        #endregion UploadFileSlicePerSlice In BlobStorage

        #region UploadAssignmentFiles In BlobStorage

        public async Task<SharePointFileView> UploadAssignmentFilesAsync(FileModulesConstants ModuleName, string oldUserId, HttpPostedFileBase files, int fileChunkSizeInMB = 3)
        {
            Guid uploadId = Guid.NewGuid();
            string ModuleNametoUpload = StringEnum.GetStringValue(ModuleName);
            string FileNamePrefix = ModuleNametoUpload + "/" + oldUserId;
            string uniqueFileName = Path.GetFileName(files.FileName);
            SharePointFileView shv = new SharePointFileView();
            int blockSize = fileChunkSizeInMB * 1024 * 1024;
            long fileSize = files.InputStream.Length;
            var fileUrl = string.Format("{0}/{1}", FileNamePrefix, uniqueFileName);
            while (CheckFileExists(fileUrl))
            {
                uniqueFileName = Guid.NewGuid() + "_" + uniqueFileName;
                fileUrl = string.Format("{0}/{1}", FileNamePrefix, uniqueFileName);
            }
            try
            {
                var ActualBlobUri = BlobUrl(fileUrl);
                var blobClient = containerClient.GetBlobClient(fileUrl);
                var info = await blobClient.UploadAsync(files.InputStream, new Azure.Storage.Blobs.Models.BlobHttpHeaders { ContentType = uniqueFileName.GetContentType() });

                shv.ShareableLink = ActualBlobUri;
                shv.SharepointUploadedFileURL = ActualBlobUri;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Azure Blob Storage Exception");
                _loggerClient.LogException(ex);
                _loggerClient.LogException(ex.InnerException);
            }
            finally
            {
                if (files.InputStream != null)
                {
                    files.InputStream.Dispose();
                }
            }
            return shv;
        }

        #endregion UploadAssignmentFiles In BlobStorage

        #region Get SAS Token with URI

        private string GetBlobSasUri(BlobContainerClient container, string blobName)
        {
            Azure.Storage.StorageSharedKeyCredential key = new Azure.Storage.StorageSharedKeyCredential(AccountName(), AccountSecret());
            // Create a SAS token that's valid for one hour.
            Azure.Storage.Sas.BlobSasBuilder sasBuilder = new Azure.Storage.Sas.BlobSasBuilder()
            {
                BlobContainerName = container.Name,
                BlobName = blobName,
                Resource = "b",
            };
            sasBuilder.StartsOn = DateTimeOffset.UtcNow;
            sasBuilder.ExpiresOn = DateTimeOffset.MaxValue;
            //sasBuilder.IPRange = new Azure.Storage.Sas.SasIPRange(new System.Net.IPAddress(Encoding.ASCII.GetBytes("0.0.0.0")), new System.Net.IPAddress(Encoding.ASCII.GetBytes("255.255.255.255")));
            sasBuilder.SetPermissions(Azure.Storage.Sas.BlobContainerSasPermissions.Read);
            // Use the key to get the SAS token.
            string sasToken = sasBuilder.ToSasQueryParameters(key).ToString();
            return container.GetBlockBlobClient(blobName).Uri + "?" + sasToken;
        }

        #endregion Get SAS Token with URI
    }
}