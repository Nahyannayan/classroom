﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.VLE.Web.Helpers
{
    public class MicrosoftTeamsHelper
    {
        public HttpClient httpClient = new HttpClient();
        string graphEndPoint = ConfigurationManager.AppSettings["ida:GraphRootUri"];
        public static readonly JsonSerializerSettings jsonSettings =
         new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
        public class HttpResponse
        {
            public string Body;
            public HttpResponseHeaders Headers;
        }

        public async Task<HttpResponse> CallGraphWithHeaders(HttpMethod method, string uri, object body = null, int retries = 0, int retryDelay = 30)
        {
            string bodyString;
            if (body == null)
                bodyString = null;
            else if (body is string)
                bodyString = body as string;
            else
                bodyString = JsonConvert.SerializeObject(body, jsonSettings);

            var tokenView = SessionHelper.CurrentSession.MicrosoftToken;// HttpContext.Current.Session["microsofttoken"] as MicrosoftResponseTokenView;
            var request = new HttpRequestMessage(method, graphEndPoint + uri);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.Headers.Authorization = new AuthenticationHeaderValue($"{tokenView.TokenType}", $"{tokenView.AccessToken}");

            if (method != HttpMethod.Get && method != HttpMethod.Delete)
                request.Content = new StringContent(bodyString, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await httpClient.SendAsync(request);
            string responseBody = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                if (retries > 0)
                {
                    Thread.Sleep(retryDelay * 1000);
                    return await CallGraphWithHeaders(method, uri, body, retries: retries - 1, retryDelay: retryDelay);
                }
            }
            return new HttpResponse() { Body = responseBody, Headers = response.Headers };
        }

        public async Task<HttpResponse> CreateMeeting(string payload)
        {
            return await CallGraphWithHeaders(HttpMethod.Post, "/me/onlineMeetings/", payload);
        }

        public async Task<HttpResponse> CreateEvent(string payload)
        {
            return await CallGraphWithHeaders(HttpMethod.Post, "/me/events", payload);
        }

        public async Task<HttpResponse> DeleteEvent(string id)
        {
            return await CallGraphWithHeaders(HttpMethod.Delete, $"/me/events/{id}");
        }

    }
}