﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Phoenix.Models;
using Phoenix.Common.Localization;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.Helpers
{
    public static class ModuleHelper
    {
        private static IModuleStructureService _moduleStructureService;
        

        public static IEnumerable<ModuleStructure> GetPhoenixModuleStructure(string traverseDirection, string moduleCode)
        {
            return GetPhoenixModuleStructure(traverseDirection, null, moduleCode, false, "", true);
        }

        public static IEnumerable<ModuleStructure> GetPhoenixModuleStructure(string traverseDirection, string moduleUrl, string moduleCode)
        {
            return GetPhoenixModuleStructure(traverseDirection, moduleUrl, moduleCode, false, "", true);
        }

        public static IEnumerable<ModuleStructure> GetPhoenixModuleStructure(string traverseDirection, string moduleUrl, string moduleCode, bool excludeParent)
        {
            return GetPhoenixModuleStructure(traverseDirection, moduleUrl, moduleCode, excludeParent, "", true);
        }

        public static IEnumerable<ModuleStructure> GetPhoenixModuleStructure(
            string traverseDirection, string moduleUrl, string moduleCode, bool excludeParent, string excludeModuleCodes, bool? showInMenu)
        {
            _moduleStructureService = new ModuleStructureService();
            var userId = SessionHelper.CurrentSession.Id;
            
            return _moduleStructureService.GetPhoenixModuleStructure(LocalizationHelper.CurrentSystemLanguage.SystemLanguageId,userId, "", traverseDirection, moduleUrl,
                moduleCode, excludeParent, excludeModuleCodes, showInMenu);
        }

        public static IEnumerable<ModuleStructure> GetPhoenixModuleStructure(string applicationCode, string traverseDirection, string moduleUrl, string moduleCode)
        {
            return GetPhoenixModuleStructure(applicationCode, traverseDirection, moduleUrl, moduleCode, false, "", true);
        }
        public static IEnumerable<ModuleStructure> GetPhoenixModuleStructure(string applicationCode,
           string traverseDirection, string moduleUrl, string moduleCode, bool excludeParent, string excludeModuleCodes, bool? showInMenu)
        {
            _moduleStructureService = new ModuleStructureService();
            var userId = SessionHelper.CurrentSession.Id;
            return _moduleStructureService.GetPhoenixModuleStructure(LocalizationHelper.CurrentSystemLanguage.SystemLanguageId, userId, applicationCode, traverseDirection, moduleUrl,
                moduleCode, excludeParent, excludeModuleCodes, showInMenu);
        }

        public static ModuleStructure GetModuleTitle()
        {
            var model = new ModuleStructure();
            string url = Common.Helpers.CommonHelper.GetControllerActionUrl().Replace("/index", "");
            var moduleStructures=GetPhoenixModuleStructure("up", url, null, false, null, null);
            if(moduleStructures.Any())
            {
                model = moduleStructures.FirstOrDefault(x => x.ParentModuleId == 1);
                if(model == null)
                    model= moduleStructures.FirstOrDefault(x => string.Equals(x.ModuleUrl, url, StringComparison.CurrentCultureIgnoreCase));
            }
            return model;
        }
        //public static IEnumerable<ModuleStructure> GetChildModuleStructure(string moduleUrl, string moduleCode)
        //{
        //    using (var context = new PhoenixDbContext())
        //    {
        //        int? userId = null;
        //        ICampusSession sessionObj = System.Web.HttpContext.Current.Session["iCampusSession"] as ICampusSession;
        //        if (sessionObj != null)
        //            userId = sessionObj.UserId;

        //        return context.GetChildModuleStructure(userId, "Phoenix", LocalizationHelper.CurrentSystemLanguageId, moduleUrl, moduleCode).ToList()
        //            .Select(c => new ModuleStructure()
        //            {
        //                ModuleId = c.ModuleId,
        //                ModuleName = c.ModuleName,
        //                ModuleCode = c.ModuleCode,
        //                ModuleIconCssClass = c.ModuleIconCssClass,
        //                ModuleUrl = c.ModuleUrl,
        //                ParentModuleId = c.ParentModuleId,
        //                LevelNumber = (short)c.LevelNumber,
        //                Sequence = (short)c.Sequence
        //            });
        //    }
        //}
        //public static short GetModuleId(string moduleUrl)
        //{
        //    IPhoenixModuleStructureRead moduleService = new PhoenixModuleStructureService();
        //    ModuleStructure module = moduleService.GetModuleByUrl(moduleUrl);
        //    short moduleId = GetReportModuleId(Convert.ToInt16(module.ParentModuleId));
        //    return moduleId;
        //}
        //public static string GetModuleTitle(string moduleUrl)
        //{
        //    using (IPhoenixModuleStructureRead moduleStructureService = new PhoenixModuleStructureService())
        //    {
        //        ModuleStructure module = moduleStructureService.GetItem(m => m.ModuleUrl.Equals(moduleUrl, System.StringComparison.OrdinalIgnoreCase));
        //        module = !string.IsNullOrWhiteSpace(module.ModuleUrl) ? module : moduleStructureService.GetItem(m => m.AbsoluteModuleUrl.Equals(moduleUrl, System.StringComparison.OrdinalIgnoreCase));
        //        string moduleName = string.Empty;

        //        if (module != null)
        //        {
        //            try
        //            {
        //                moduleName = Convert.ToString(module.GetType().GetProperty("ModuleName_" + LocalizationHelper.CurrentSystemLanguageId).GetValue(module));
        //            }
        //            catch (System.Exception ex)
        //            {
        //                moduleName = module.ModuleName_1;
        //            }
        //        }

        //        return moduleName;
        //    }
        //}
        //public static string GetModuleUrlByModuleCode(string moduleCode)
        //{
        //    using (IPhoenixModuleStructureRead moduleStructureService = new PhoenixModuleStructureService())
        //    {
        //        ModuleStructure module = moduleStructureService.GetItem(m => m.ModuleCode.Equals(moduleCode, System.StringComparison.OrdinalIgnoreCase));
        //        string moduleUrl = string.Empty;

        //        if (module != null)
        //        {
        //            moduleUrl = module.ModuleUrl;
        //        }

        //        return moduleUrl;
        //    }
        //}
        //public static object GetChildModuleUrl(short ParentItemId)
        //{
        //    short languageId = LocalizationHelper.CurrentSystemLanguageId;

        //    string query = "select moduleurl from admin.ActivePhoenixModuleStructure where ModuleID =" +
        //                "(select top 1 ModuleID from school.CurriculumStructure where ParentItemId = @ParentItemId)";

        //    DataParameter selectParameter = new DataParameter("@ParentItemId", System.Data.SqlDbType.SmallInt, ParentItemId);

        //    DataHelper dataHelper = new DataHelper();
        //    return dataHelper.GetScalarValue(query, CommandType.Text, selectParameter);
        //}
        //public static short GetReportModuleId(short moduleId)
        //{
        //    using (IPhoenixModuleStructureRead moduleStructureService = new PhoenixModuleStructureService())
        //    {
        //        short result = 0;

        //        if (moduleId != 0)
        //        {
        //            try
        //            {
        //                string query = "select ReportModuleId from Admin.ModuleReportModuleMapping Where ModuleId = @ModuleId";
        //                DataParameter selectParameter = new DataParameter("@ModuleId", System.Data.SqlDbType.SmallInt, moduleId);

        //                DataHelper dataHelper = new DataHelper();
        //                result = Convert.ToInt16(dataHelper.GetScalarValue(query, CommandType.Text, selectParameter));
        //            }
        //            catch (System.Exception ex)
        //            {
        //                moduleId = 0;
        //            }
        //        }

        //        return result;
        //    }
        //}

        //public static string GetParentModuleUrl(string absoluteModuleUrl)
        //{
        //    string parentModuleUrl = string.Empty;
        //    string actualUrl = "/" + absoluteModuleUrl.Split('/')[1];
        //    string query = "SELECT ModuleUrl FROM [Admin].[ActivePhoenixModuleStructure] WHERE (lower(AbsoluteModuleUrl))  = lower(dbo.FormatModuleUrl(@AbsoluteModuleUrl))";
        //    DataParameter selectParameter = new DataParameter("@AbsoluteModuleUrl", System.Data.SqlDbType.VarChar, actualUrl);
        //    DataHelper dataHelper = new DataHelper();
        //    string moduleUrl = (string)dataHelper.GetScalarValue(query, CommandType.Text, selectParameter);
        //    if (!string.IsNullOrEmpty(moduleUrl))
        //        parentModuleUrl = "/" + moduleUrl.Split('/')[1];
        //    else
        //        return actualUrl;

        //    string browserUrlQuery = "SELECT ModuleUrl FROM [Admin].[ActivePhoenixModuleStructure] WHERE (lower(AbsoluteModuleUrl))  = lower(dbo.FormatModuleUrl(@AbsoluteModuleUrl))";
        //    DataParameter param = new DataParameter("@AbsoluteModuleUrl", System.Data.SqlDbType.VarChar, absoluteModuleUrl);
        //    string browserUrl = (string)dataHelper.GetScalarValue(browserUrlQuery, CommandType.Text, param);
        //    if (string.IsNullOrEmpty(browserUrl))
        //        return actualUrl;
        //    else
        //    {
        //        browserUrl = "/" + browserUrl.Split('/')[1];
        //        if (parentModuleUrl == browserUrl)
        //            return parentModuleUrl;
        //    }

        //    return actualUrl;
        //}
    }
}
