﻿using System;
using System.Data;
using System.Web;
using System.Net.Mail;
using System.Collections.Generic;

//Custome Namespaces
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.DataAccess;
///Header Information
///Created By : Renees P O
///Created Date : 13-04-2010
///Usage/Comments : Class that provide methods to extend RadGrid 
///Properties : None
///Methods :
namespace Phoenix.VLE.Web.Helpers
{
    /// <summary>
    /// Summary description for SendEmail
    /// </summary>
    public class SendEmail
    {       

        private SmtpClient _smtp;
        public string SmtpHost
        {
            get;
            set;
        }

        public int SmtpPort
        {
            get;
            set;
        }

        public bool EnableSSL
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public string FromAddress
        {
            get;
            set;
        }

        public string DisplayName
        {
            get;
            set;
        }

        public string To
        {
            get;
            set;
        }

        public IList<string> ToList;
        public string CC
        {
            get;
            set;
        }

        public string BCC { get; set; }

        public string Subject
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public bool IsBodyHtml
        {
            get;
            set;
        }

        public string ProfileName
        {
            get;
            set;
        }

        public SendEmail()
        {
            // Set default values
            this.IsBodyHtml = true;
            this.ProfileName = "Database Mail";
            this.EnableSSL = true;
            _smtp = new SmtpClient();
            ToList = new List<string>();
        }

        public SendEmail(bool initDefaultConfig)
        {          
            _smtp = new SmtpClient();
            ToList = new List<string>();

            if (initDefaultConfig)
            {
                var emailSettings = CommonHelper.GetEmailSettings();
                this.FromAddress = emailSettings.SenderEmail;
                this.UserName = emailSettings.SenderEmail;
                this.Password = emailSettings.SenderPassword;
                this.DisplayName = emailSettings.SenderName;
                this.SmtpPort = Convert.ToInt32(emailSettings.PortNo);
                this.SmtpHost = emailSettings.SMTPClient;
                this.EnableSSL = emailSettings.IsSSLEnabled;
                this.IsBodyHtml = true;
            }
        }

        public void Send()
        {
            // Create the MailMessage instance
            MailMessage message = new MailMessage();           
            message.From = new MailAddress(this.FromAddress, this.DisplayName);

            if (ToList.Count > 0)
            {
                foreach (var email in ToList)
                {
                    message.To.Add(email);
                }
            }
            else
                message.To.Add(To);

            if (!string.IsNullOrEmpty(this.CC))
                message.CC.Add(this.CC);
            if (!string.IsNullOrEmpty(this.BCC))
                message.Bcc.Add(this.BCC);

            message.Subject = this.Subject;
            message.Body = this.Message;
            message.IsBodyHtml = this.IsBodyHtml;
            message.Priority = MailPriority.Normal;

            //Send email (will use the Web.config settings)
            _smtp.Credentials = new System.Net.NetworkCredential(this.UserName, this.Password);
            _smtp.Port = this.SmtpPort; //587; 
            _smtp.Host = this.SmtpHost; //"smtp.gmail.com";
            _smtp.EnableSsl = this.EnableSSL;
            _smtp.Send(message);
        }

        public int EnqueueNewEmailMessage()
        {
            DataHelper dataHelper = new DataHelper();
            IList<DataParameter> parameters = new List<DataParameter>()
            {
                 new DataParameter("@EmailFrom",SqlDbType.NVarChar, FromAddress),
                 new DataParameter("@EmailTo",SqlDbType.NVarChar, To),
                 new DataParameter("@CC",SqlDbType.NVarChar, CC),
                 new DataParameter("@BCC",SqlDbType.NVarChar, BCC),
                 new DataParameter("@EmailSubject",SqlDbType.NVarChar, Subject),
                 new DataParameter("@EmailMessage",SqlDbType.NVarChar, Message),
                 new DataParameter("@ExportStatus",SqlDbType.Char, 'P'),
                 new DataParameter("@RequestUId",SqlDbType.UniqueIdentifier, DBNull.Value)
            };
            return dataHelper.ExecuteNonQuery("[Admin].[EnqueueEmailQueue]", CommandType.StoredProcedure, parameters).ToInteger();
        }

        public int SendDatabaseMail()
        {
            DataHelper _dataHelper = new DataHelper();

            // Create select parameter list
            List<DataParameter> _selectDataParameters = new List<DataParameter>();
            _selectDataParameters.Add(new DataParameter("@ProfileName", SqlDbType.VarChar, this.ProfileName));
            _selectDataParameters.Add(new DataParameter("@Subject", SqlDbType.VarChar, this.Subject));
            _selectDataParameters.Add(new DataParameter("@Body", SqlDbType.VarChar, this.Message));
            _selectDataParameters.Add(new DataParameter("@Recipients", SqlDbType.VarChar, this.To));
            _selectDataParameters.Add(new DataParameter("@BodyFormat", SqlDbType.VarChar, this.IsBodyHtml ? "HTML" : "TEXT"));

            // Execute SPROC and return the DataSet object
            return _dataHelper.ExecuteNonQuery("Admin.SendMail", CommandType.StoredProcedure, _selectDataParameters);
        }
    }
}