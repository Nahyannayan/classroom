﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Phoenix.Common.Helpers;

namespace Phoenix.VLE.Web
{
    public static class Constants
    {
        public static readonly string PhoenixAPIUrl = PhoenixConfiguration.Instance.PhoenixApiUrl;
        public static readonly string HSEAPIUrl = PhoenixConfiguration.Instance.HseApiUrl;
        public static readonly string SIMSApiUrl = PhoenixConfiguration.Instance.SIMSApiUrl;

        //public const string IdentityAPIUrl = @"http://localhost:5501";
        //public const string PhoenixUrl = @"http://localhost:6455/";
        //public const string RedirectUri = @"http://localhost:5550/signin-oidc";
        //public const string PostLogoutRedirectUri = @"http://localhost:5550/signout-callback-oidc";
        //added to resolve error
        // public const string SIMSApiUrl = @"";


        //public const string ClientId = "phoenixapp";
        //public const string ClientSecret = "secret";
        //public const string AuthorizeEndpoint = PhoenixUrl + "/connect/authorize";
        //public const string LogoutEndpoint = PhoenixUrl + "/connect/endsession";
        //public const string TokenEndpoint = PhoenixUrl + "/connect/token";
        //public const string UserInfoEndpoint = PhoenixUrl + "/connect/userinfo";
        //public const string IdentityTokenValidationEndpoint = PhoenixUrl + "/connect/identitytokenvalidation";
        //public const string TokenRevocationEndpoint = PhoenixUrl + "/connect/revocation";

        //Shared
        public const string Shared = "/Shared/";
        //for report abuse mail template and other constant
        public const string SchoolImageDir = "/Uploads/SchoolImages/";
        public const string ReportAbuseSOSEmailTemplate = "/EmailTemplate/ReportAbuseSOSEmailBody.html";
        public const string ReportAbuseEmailTemplate = "/EmailTemplate/ReportAbuseEmailBody.html";
        public const string RequestDemoEmailTemplate = "/EmailTemplate/RequestDemo.html";
        public const string AssignmentNotificationTemplate = "/EmailTemplate/AssignmentNotification.html";
        public const string AssignmentCommentNotificationTemplate = "/EmailTemplate/AssignmentCommentNotification.html";
        public const string ChatterNotificationTemplate = "/EmailTemplate/ChatterNotification.html";
        public const string AssignmentGradeNotificationTemplate = "/EmailTemplate/AssignmentGradeNotification.html";
        public const string AssignmentMarkAsCompleteNotificationTemplate = "/EmailTemplate/AssignmentMarkAsCompletedNotification.html";
        public const string ThinkBoxAlertTemplate = "/EmailTemplate/ThinkBoxAlert.html";
        public const string EventAlertTemplate = "/EmailTemplate/EventAlert.html";
        public const string CancelEventTemplate = "/EmailTemplate/EventCancelAlert.html";
        public const string GroupMessageTemplate = "/EmailTemplate/GroupMessage.html";
        public const string AssignmentResubmitNotificationTemplate = "/EmailTemplate/AssignmentResubmitNotification.html";
        public const string ObservationNotificationTemplate = "/EmailTemplate/ObservationNotification.html";
        public const string ReportAbuseEmailSubject = "Report Abuse";
        public const string AssignmentNotification = "Assignment Notification";
        public const string AssignmentCommentNotification = "Assignment Comment Notification";
        public const string ChatterNotification = "Chatter Notification";
        public const string AssignmentGradeNotification = "Assignment Graded Notification";
        public const string AssignmentMarkAsCompleted = "Your assignment has been marked as \"completed\"";
        public const string AssignmentCompletionMessage = "Submitted assignment has been marked as completed by teacher.";
        public const string AssignmentWorkSubmission = "Assignment Work Submission Notification";
        public const string ObservationNotification = "Observation Notification";
        public const string AssignmentResubmitNotification = "Assignment Resubmit Notification";
        public const string SchoolEventUploads = "/Uploads/SchoolEventUploads/";
        public const string NoReplyMail = "no_reply@gemseducation.com";
        public const string IssueReportEmailTemplate = "/EmailTemplate/IssueReportEmailTemplate.html";
        public const string BehaviorCertificateEmailBody = "/EmailTemplate/BehaviorCertificateEmailBody.html";

        public const string ResourcesDir = "/Resources";
        public const string BlogDir = "/Resources/BlogContent";
        public const string ChatDir = "/Resources/ChatContent";
        public const string FileDir = "/Resources/FileContent";
        public const string PlannerDir = "/Resources/PlannerContent";
        public const string QuizFeedbackDir = "/Resources/QuizFeedback";
        public const string TaskFeedbackDir = "/Resources/TaskFeedback";
        public const string AssignmentFeedbackDir = "/Resources/AssignmentFeedback";

        public const string GroupImageDir = "/Resources/GroupImage";
        //excel connection strings
        public const string Excel03ConString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties = 'Excel 8.0;HDR={1}'";
        public const string Excel07ConString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties = 'Excel 12.0;HDR={1}'";
        public const string SchoolImagesPath = "/Uploads/SchoolImages/";
        public static readonly string StudentImagesPath = ConfigurationManager.AppSettings["StudentImagesPath"].ToString();

        //user profile strings
        public const string ProfileAvatarPath = "/Content/VLE/img/";
        public const string ParentProfilePath = "/Resources/Content/ProfilePhotos/Parents/";
        public const string TeacherProfilePath = "/Resources/Content/ProfilePhotos/Teachers/";
        public const string StudentProfilePath = "/Resources/Content/ProfilePhotos/Students/";
        //Assignment Files constants
        public const string AssignmentFilesDir = "/Resources/AssignmentFiles";
        public const string AssignmentTaskFilesDir = "/Resources/AssignmentTaskFiles";

        //Student Uploaded Review Files constants
        public const string StudentAsgReviewUploadedFilesDir = "/Resources/StudentAsgReviewedFiles";
        public const string StudentAsgReviewTaskFilesDir = "/Resources/StudentTaskReviwedFiles";

        //Observation Files constants
        public const string ObservationFilesDir = "/Resources/ObservationFiles";
        public const string ObservationTaskFilesDir = "/Resources/ObservationTaskFiles";

        //SIMS Upload File details
        public const string StudentBehaviour = "/Resources/StudentBehaviour";
        public const string SIMSProgressTracker = "/Resources/ProgressTracker";
        public const string SIMSKHDA_FINAL = "/KHDA/Final";
        public const string SIMSKHDA_TEMP = "/KHDA/TEMP";
        public const string DefaultVideoImage = "/Content/img/video-thumb.jpg";

        //Error Screen Shots
        public const string ErrorScreenShotDir = "/Resources/ErrorScreens";
        public const string ErrorScreenShots = "/Resources/ErrorScreenShots";

        //Quiz File
        public const string QuizFilesDir = "/Resources/QuizFiles/";
        public const string QuizQuestion = "/Resources/QuizQuestion/";
        public const string QuizMTPQuestion = "/Resources/QuizMTPQuestion/";
        public const string ImportFile = "/ImportFormat.xlsx";


        //Banner Files Path details
        public const string BannerFilesDir = "/Resources/BannerFiles";

        //Banner Files Path details
        public const string BadgeFilesDir = "/Resources/BannerFiles";

        // Certificates File Path Details
        public const string CertificateFilesDir = "/Resources/Certificates";

        public const string AcademicFilesDir = "/Resources/Academics";
        public const string TemplateCertificateDir = "/Resources/TemplateCertificates";
        public const string TemplateCertificatePreviewDir = "/Resources/TemplateCertificates/Preview";
        public const string BehaviorCertificateDir = "/Resources/BehaviorCertificates";
        //Template File
        public const string TemplateDir = "/Resources/Templates";

        public const string ForgotPasswordEmailTemplate = "/EmailTemplate/ForgotPasswordEmailTemplate.html";

        public const string SchoolGroupZipFilePath = "/Resources/FileContent/Module_{0}/";

        public const string CourseFilesDir = "/Resources/CourseFiles";
        public const string UnitDetailsDir = "/Resources/UnitDetails";
        public const string BehaviourSubCategory = "/Resources/StudentBehaviour/BehaviourSubCategory";
        public const string ProgressTracker = "/Resources/ProgressTrackerFiles";
        public static readonly string PhoenixActiveKidsApiUrl = PhoenixConfiguration.Instance.PhoenixActiveKidsApiUrl;

        //payment redirection
        //public const string PCPayGatewayRefId;
        //public const string PCPayGateWayReturnTo;
        //public const string PCPayGateWaySource;
        public const string PCPaymentGatewayRedirection = "/parentcorner/fee/PaymentRedirectionProcess";

        //Enroll Activity
        public const string PaymentStatusCompleted = "COMPLETED";

        //School Report
        public const string SchoolReportsDir = "/Resources/SchoolReports/";
        public const string SchoolReportsCommonDir = "Common";
        public static bool IsElite
        {
            get
            {
#if ELITE
                return true;
#else
        return false;
#endif
            }
        }
        public static bool IsNonGems
        {
            get
            {
#if NONGEMS
                return true;
#else
                return false;
#endif
            }
        }
        public static bool IsGems
        {
            get
            {
#if GEMS
                return true;
#else
                return false;
#endif
            }
        }
        public static bool IsWellFormedUriString(string uri)
        {
            Uri uriResult;
            return Uri.TryCreate(uri, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }
    }

}