﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;

namespace Phoenix.VLE.Web.Helpers
{
    public class VisitorInfo
    {
        private ILoggerClient _loggerClient;
        public VisitorInfo()
        {
            _loggerClient = LoggerClient.Instance;
        }
        public string GetIpAddress()
        {
            var IpAddress = "";
            try
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        IpAddress = ip.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            return IpAddress;
        }

        public string GetClientIPAddress()
        {

            string actualAddress = "";
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string sIPAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(sIPAddress))
                {
                    actualAddress = context.Request.ServerVariables["REMOTE_ADDR"];
                }
                else
                {
                    string[] ipArray = sIPAddress.Split(new Char[] { ',' });
                    actualAddress = ipArray[0];
                }

            }
            catch (Exception ex)
            {
                _loggerClient.LogException(ex);
            }
            return actualAddress;
        }
    }
}