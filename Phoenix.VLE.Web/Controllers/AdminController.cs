﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PdfSharp;
using PdfSharp.Pdf;
using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.VLE.Web.Extensions;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace Phoenix.VLE.Web.Controllers
{
    [Authorize]
    public class AdminController : BaseController
    {
        private ISchoolService _schoolService;
        private IUserService _userService;

        public AdminController(ISchoolService schoolService,IUserService userService)
        {
            _schoolService = schoolService;
            _userService = userService;
        }
        // GET: Admin
        public ActionResult Index()
        {
            if (!CurrentPagePermission.CanView && SessionHelper.CurrentSession.IsAdmin)
            {
                return RedirectToAction("NoPermission", "Error");
            }
            Session["LoginType"] = StringEnum.GetStringValue(MainModuleCodes.AdminPanel) ;
            Request.RequestContext.HttpContext.Server.TransferRequest("/SchoolInfo/Reports/index");
            return Content("success");
        }

        [HttpPost]
        public ActionResult ChangeSchool(int schoolId)
        {
            var schoolInfo = _schoolService.GetSchoolById(schoolId);
            Session["SelectedSchoolForSuperAdmin"] = schoolInfo;
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Users(string loginType,string startDate,string endDate)
        {
            var lgType = EncryptDecryptHelper.DecodeBase64(loginType);
            DateTime stDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(startDate));
            DateTime edDate = DateTime.Parse(EncryptDecryptHelper.DecodeBase64(endDate));

            var users = _userService.GetUserLog(Convert.ToInt32(SessionHelper.CurrentSession.SchoolId), stDate, edDate, lgType);

            if(users != null)
                Session["usersLoginReport"] = users.ToList();

            return View(users);
        }

        #region Download As PDF


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadReportToPDF")]
        public ActionResult DownloadReportDataAsPDF(FormCollection form)
        {
            string pdfhtml = "";
            string pdfTitle = "";
            string downloadFileName = "";
            string reportName = form["reportName"];
            ViewBag.ReportType = "PDF";
            var usersLoginReport = Session["usersLoginReport"] as List<Phoenix.Models.User>;

            switch (reportName)
            {
                case "UsersLoginReport":
                    {
                        pdfhtml = ControllerExtension.RenderPartialViewToString(this, "_UsersLogin", usersLoginReport);
                        pdfTitle = ResourceManager.GetString("Shared.Login.Logins");
                        downloadFileName = "UsersLoginReport.pdf";
                        break;
                    }
            }

            PdfDocument pdf = new PdfDocument();

            pdf = PdfGenerator.GeneratePdf(pdfhtml, PageSize.A4);
            pdf.Info.Title = pdfTitle;
            MemoryStream stream = new MemoryStream();
            pdf.Save(stream, false);
            byte[] file = stream.ToArray();
            stream.Write(file, 0, file.Length);
            stream.Position = 0;

            return File(stream, "application/pdf", downloadFileName);

        }

        #endregion

        #region Download Report Data As Excel

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DownloadReportToExcel")]
        public ActionResult DownloadReportDataAsExcelFile(FormCollection form)
        {
            string path = string.Empty;
            string downloadFileName = "";
            byte[] bytes;
            string reportName = form["reportName"];
            var usersLoginReport = Session["usersLoginReport"] as List<Phoenix.Models.User>;

            path = Server.MapPath("~/Content/Files/EmptyReportFile.xls");

            switch (reportName)
            {
                case "UsersLoginReport":
                    {
                        bytes = _schoolService.GenerateUsersLoginReportXLSFile(path, usersLoginReport);
                        downloadFileName = "UsersLoginReport.xls";
                        return File(bytes, "application/vnd.ms-excel", downloadFileName);
                    }
                default:
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        #endregion
    }
}