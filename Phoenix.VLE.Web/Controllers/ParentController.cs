﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using Phoenix.Models.Entities;
using Phoenix.Common;
using Phoenix.Common.Models;
using Phoenix.Common.Extensions;
using Phoenix.Common.Enums;
using Phoenix.Common.Logger;
using Phoenix.Common.Models.ViewModels;
using Phoenix.VLE.Web.Services.Setting.Contracts;

namespace Phoenix.VLE.Web.Controllers
{
    [RouteArea("Parent")]
    public class ParentController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IStudentService _studentService;
        private readonly IParentService _parentService;
        private readonly ILogInUserService _logInUserService;
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly IAssignmentService _assignmentService;
        private readonly ISchoolSpaceService _schoolSpaceService;
        private readonly IFileService _fileService;
        private readonly IObservationService _observationService;
        private readonly IRSSFeedService _rssFeedServices;
        private ILoggerClient _loggerClient;
        private readonly ISchoolBannerService _schoolBannerService;
        private readonly INotificationService _notificationService;
        private readonly IEventCategoryService _eventCategoryService;
        private readonly ISettingService _settingService;


        public ParentController(IUserService userService, IStudentService studentService, IParentService parentService, ILogInUserService logInUserService,
            ISchoolGroupService schoolGroupService, IAssignmentService assignmentService, ISchoolSpaceService schoolSpaceService, IFileService fileService,
            IObservationService observationService, IRSSFeedService rSSFeedService, ISchoolBannerService schoolBannerService, INotificationService notificationService, IEventCategoryService eventCategoryService
            , ISettingService _settingService)
        {
            _userService = userService;
            _studentService = studentService;
            _parentService = parentService;
            _logInUserService = logInUserService;
            _schoolGroupService = schoolGroupService;
            _assignmentService = assignmentService;
            _schoolSpaceService = schoolSpaceService;
            _fileService = fileService;
            _observationService = observationService;
            _rssFeedServices = rSSFeedService;
            _loggerClient = LoggerClient.Instance;
            _schoolBannerService = schoolBannerService;
            _notificationService = notificationService;
            _eventCategoryService = eventCategoryService;
            this._settingService = _settingService;
        }


        // GET: Parent
        public ActionResult Index()
        {
            return View();
        }
        [Route("Dashboard")]
        public ActionResult Dashboard()
        {
            var parentModel = new ParentView();

            try
            {
                parentModel = _parentService.GetStudentDetailsById(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId);
                var StudentDetails = (Phoenix.Common.ViewModels.StudentDetail)Session["CurrentSelectedStudent"];
                SessionHelper.CurrentSession.SchoolImage = StudentDetails.SchoolLogo;

                if (TempData["notificationData"] == null)
                {
                    string notificationKey = GetUserNotificationIdentifiers(out string identifier);
                    parentModel.Notifications =  _notificationService.GetAllNotifications(notificationKey, identifier, "CLASSROOM", string.Empty);
                    TempData["notificationData"] = parentModel.Notifications.OrderByDescending(n => n.DateTime).ToList();
                }
                else
                {
                    parentModel.Notifications = TempData["notificationData"] as List<StudentNotification>;
                    TempData.Keep("notificationData");
                }
                var profileImage = Constants.StudentImagesPath + SessionHelper.CurrentSession.CurrentSelectedStudent.StudentImage;
                Helpers.CommonHelper.AppendCookie("VirtualImgPath", "1", profileImage);

                List<SchoolBanner> lstSchoolBanners = new List<SchoolBanner>();
                lstSchoolBanners = _schoolBannerService.GetUserBanners(SessionHelper.CurrentSession.Id).ToList();

                List<SchoolBanner> lstStudentBanners = new List<SchoolBanner>();
                lstStudentBanners = _schoolBannerService.GetUserBanners(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId).ToList();

                lstSchoolBanners = lstStudentBanners.Union(lstStudentBanners).ToList();

                parentModel.SchoolBanners = lstSchoolBanners;
                //parentModel.UserFeelingStatus = Helpers.CommonHelper.GetUserFeeling();

                #region EventCategoryDropDown
                //comment the below code for new dashboard
                //var eventCategories = _eventCategoryService.GetAllEventCategoryBySchool().ToList();
                //List<EventCategoryView> lstEventCategory = new List<EventCategoryView>
                //{
                //    new EventCategoryView {  Name = "All Events", Description="All Events" },
                //    new EventCategoryView {  Name = "School Events", Description="School Events" },
                //    new EventCategoryView {  Name = "Student Timetable", Description="Student Timetable" },
                //    //new EventCategoryView {  Name = "Assignments", Description="Assignments" }
                //};

                //if (eventCategories.Count > 0)
                //    lstEventCategory.AddRange(eventCategories);

                //var categoryList = lstEventCategory.Select(i => new Phoenix.Common.Models.ListItem
                //{
                //    ItemId = (i.Id == 0) ? i.Name : i.Id.ToString(),
                //    ItemName = !string.IsNullOrEmpty(i.Name) ? i.Name : i.Description
                //});

                //ViewBag.lstCategory = categoryList;
                #endregion

                //disabling it as per requested by Vishal Mehta, Deepak Singh 18/Sept/2020
                //IEnumerable<UserActivityPointsView> lstUserActivityPointsView = _studentService.GetStudentTotalActivePoints(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId);
                //if (lstUserActivityPointsView.Count() > 0)
                //{
                //    parentModel.TotalActivePoints = lstUserActivityPointsView.Where(p => p.GroupName == "Overall").FirstOrDefault().Points;
                //}
            }
            catch (Exception ex)
            {
                _loggerClient.LogException("Error in parent dashboard: =>" + ex);
            }
            return View(parentModel);
        }

        [Route("LoadChildList")]
        public ActionResult LoadChildList()
        {
            var parent = new ParentView();
            var childDetail = _studentService.GetStudentByFamily(SessionHelper.CurrentSession.Id);
            foreach (var item in childDetail)
            {
                item.StudentImage = Constants.StudentImagesPath + item.StudentImage;
                parent.StudentDetails.Add(item);

            }
            Session["ChildCount"] = childDetail.Count();
            return PartialView("_ParentChidListPartial", parent);
        }

        public ActionResult GetSchoolGroupWithPagination(short pageIndex)
        {
            return PartialView("_DashboardGroups", GetSchoolGroupList(pageIndex));
        }

        private IEnumerable<SchoolGroup> GetSchoolGroupList(int pageIndex)
        {
            IEnumerable<SchoolGroup> schoolGroups = new List<SchoolGroup>();
            schoolGroups = _schoolGroupService.GetSchoolGroupWithPagination(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId, 12, pageIndex);
            return schoolGroups;
        }

        public ActionResult GetStudentDashboardAssignments()
        {
            IEnumerable<Assignment> assignments = new List<Assignment>();
            assignments = _assignmentService.GetDashboardAssignmentOverview(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId);
            return PartialView("_AssignmentOverview", assignments);
        }

        private string GetUserNotificationIdentifiers(out string identifier)
        {
            string _key = string.Empty;
            if (SessionHelper.CurrentSession.IsParent())
            {
                _key = "PARENT";
                identifier = SessionHelper.CurrentSession.UserName;
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                _key = "STUDENT";
                identifier = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
            }
            else identifier = string.Empty;
            return _key;
        }

        public ActionResult SetCurrentSelectedStudent(int studentId)
        {
            var studentDetail = SessionHelper.CurrentSession.FamilyStudentList.FirstOrDefault(x => x.UserId == studentId);
            Session["CurrentSelectedStudent"] = studentDetail;
            if (TempData["notificationData"] != null)
                TempData.Remove("notificationData");
            return Json(true, JsonRequestBehavior.DenyGet);
        }

        [Route("Assignments")]
        public ActionResult Assignments(string studentId, int pageIndex = 1)
        {
            var model = new Pagination<AssignmentStudentDetails>();
            var assignmentList = _assignmentService.GetAssignmentByStudentId(pageIndex, 2, Convert.ToInt64(SessionHelper.CurrentSession.CurrentSelectedStudent.UserId), String.Empty).ToList();
            if (assignmentList.Any())
                model = new Pagination<AssignmentStudentDetails>(pageIndex, 2, assignmentList, assignmentList.FirstOrDefault().TotalCount);
            return View(model);
        }

        private void ResetNotificationCookie(string studentNumber)
        {
            string Key = string.Empty, Identifier = string.Empty, Source = string.Empty, Type = string.Empty;
            Key = "STUDENT";
            Identifier = studentNumber;
            Source = "CLASSROOM";
            Type = "";
            Helpers.CommonHelper.RemoveCookie("NotificationExist");

            var allNotifications = _notificationService.GetAllNotifications(Key, Identifier, Source, Type);
            var unreadNotifications = allNotifications.Where(x => x.IsRead == false).ToList();

            Helpers.CommonHelper.AppendCookie("NotificationExist", "exist", unreadNotifications.Any().ToString().ToLower());
        }

        [Route("initSetting")]
        [HttpGet]
        public ActionResult initSetting()
        {
            var model = new NotificationSettings();
            model.NotificationList = _settingService.GetEnableDisableNotificationListForParent(SessionHelper.CurrentSession.Id).ToList();
            return View(model);
        }

        [HttpPost]
        [Route("SaveSettings")]
        public void SaveSettings(NotificationSettings model)
        {
            var records = _settingService.SaveNotificationSettingForParent(model, SessionHelper.CurrentSession.Id);

        }
    }
}
