﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Phoenix.Common;
using Phoenix.Common.Enums;

namespace Phoenix.VLE.Web.Controllers
{
    public class SchoolManagementController : BaseController
    {
        // GET: SIMS
        private ITimeTableService _TimeTableService;
        private ISIMSCommonService _SIMSCommonService;
        private IAssessmentService _AssessmentService;

        public SchoolManagementController(ITimeTableService timeTableService, ISIMSCommonService SIMSCommonService, IAssessmentService AssessmentService)
        {
            _TimeTableService = timeTableService;
            _SIMSCommonService = SIMSCommonService;
            _AssessmentService = AssessmentService;
        }

        public ActionResult Index()
        {
            Session["LoginType"] = (Session["LoginType"] != null && Session["LoginType"].ToString() == StringEnum.GetStringValue(MainModuleCodes.AdminPanel)) ? Session["LoginType"].ToString() :  StringEnum.GetStringValue(MainModuleCodes.SIMS);
            
            //if (Session["SIMSCurrentCurriculum"] == null)
            //{
                var curriculamList = _SIMSCommonService.GetCurriculum(SessionHelper.CurrentSession.SchoolId).FirstOrDefault();
            curriculamList.SchoolId = SessionHelper.CurrentSession.SchoolId;
                Session["SIMSCurrentCurriculum"] = curriculamList;
            //}
     
            LoadGrades();
            return View("~/Areas/SMS/Views/TimeTable/Index.cshtml");
        }
        [NonAction]
        public void LoadGrades()
        {
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _SIMSCommonService.GetGradesAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access).ToList();
            ViewBag.Grades = list;
        }
        [HttpGet]
        public JsonResult FetchSection(string id)
        {
            //var Section = new SelectList(SelectSIMSListHelper.GetSelectListData(ListItems.Section, id), "ItemId", "ItemName");
            //return Json(Section, JsonRequestBehavior.AllowGet);
            var schoolid = SessionHelper.CurrentSession.SchoolId;
            var username = SessionHelper.CurrentSession.UserName;
            var curr_role = _SIMSCommonService.GetUserCurriculumRole(schoolid.ToString(), username);
            var isSuperUser = (curr_role.IsSuperUser ? "Y" : "N");
            var grd_access = (int)curr_role.GSA_ID;
            var list = _AssessmentService.GetSectionAccess(username, isSuperUser, Helpers.CommonHelper.GetCurrentCurriculum().ACD_ID, Convert.ToInt32(schoolid), grd_access, id);
            var sectionList = new SelectList(list, "SCT_ID", "SCT_DESCR");
            return Json(sectionList, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult LoadTimeTableLayout(DateTime dateTime, string type, string grade = null, string section = null)
        {
            var currentUser = Helpers.CommonHelper.GetLoginUser();
            var TimeTableCombined = new TimeTableCombined();
            List<ClassTiming> objClassTiming = new List<ClassTiming>();
            List<WeekRange> objweekRange = new List<WeekRange>();

            DateTime startOfWeek = //DateTime.Today
                                 dateTime.AddDays(
                                 (int)System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek -
                                 (int)dateTime.DayOfWeek);

                                 string result = string.Join("," + Environment.NewLine, Enumerable
                                 .Range(0, 7)
                                 .Select(i => startOfWeek
                                 .AddDays(i)
                                 .ToString("dd-MMMM-yyyy")));

            var NextSunday = Next(dateTime, DayOfWeek.Sunday);
            var PreviousSunday = Previous(dateTime, DayOfWeek.Sunday);
           
            List<string> WeekRange = result.Replace("\r\n", string.Empty).Split(',').ToList();

            foreach(var item in WeekRange)
            {
                var Weekdate = Convert.ToDateTime(item);
                objweekRange.Add(new WeekRange {
                    Date = Weekdate,
                    Day = Weekdate.DayOfWeek.ToString()

                });
            }
            var username = currentUser.UserName;
            var days = _TimeTableService.GetCurrentMonth();
            TimeTableCombined.Calendardays = days;

            var timetable = _TimeTableService.GetTimeTablesByUserandDate(username, dateTime, type, grade, section);
            TimeTableCombined.TimetableLayout = timetable;
            var startendtime = TimeTableCombined.TimetableLayout.GroupBy(e => new { e.StartTime, e.EndTime, e.TT_Sequence })
                                                                 .Select(f => new { f.Key.StartTime, f.Key.EndTime, f.Key.TT_Sequence }).OrderBy(f => f.TT_Sequence);

            foreach (var item in startendtime)
            {

                objClassTiming.Add(new ClassTiming { StartTime = item.StartTime, EndTime = item.EndTime, TT_Sequence = item.TT_Sequence });
            }

            ViewBag.ClassTiming = objClassTiming;
            ViewBag.WeekRang = objweekRange;
            ViewBag.NextSunday = NextSunday;
            ViewBag.PreviousSunday = PreviousSunday;
            return PartialView("_TimeTableDetails", TimeTableCombined);
        }

        public DateTime Next(DateTime from, DayOfWeek dayOfWeek)
        {
            int start = (int)from.DayOfWeek;
            int target = (int)dayOfWeek;
            if (target <= start)
                target += 7;
            return from.AddDays(target - start);
        }

        public DateTime Previous(DateTime from, DayOfWeek dayOfWeek)
        {
            int start = (int)from.DayOfWeek;
            int target = (int)dayOfWeek;
            if (target <= start)
                target -= 7;
            return from.AddDays(target - start);
        }

        public PartialViewResult GetCurriculum(long schoolId)
        {
            var result = _SIMSCommonService.GetCurriculum(schoolId);
            return PartialView("_CurriculumDropdownList", result);
        }

        public ActionResult ChangeCurriculum(string curriculumId)
        {
            var selectedCurriculum = _SIMSCommonService.GetCurriculum(SessionHelper.CurrentSession.SchoolId, Convert.ToInt32(curriculumId)).FirstOrDefault();
            selectedCurriculum.SchoolId = SessionHelper.CurrentSession.SchoolId;
            Session["SIMSCurrentCurriculum"] = selectedCurriculum;
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}