﻿using Phoenix.Common;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Controllers
{
    [Authorize]
    public class TeacherController : BaseController
    {
        private readonly ITeacherDashboardService _teacherDashboardService;
        public TeacherController(ITeacherDashboardService teacherDashboardService)
        {
            _teacherDashboardService = teacherDashboardService;
        }
        // GET: Teacher
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SchoolGroups(int pageIndex = 1)
        {
            var teacherlist = _teacherDashboardService.GetTeacherDashboard();
            var objPage = new Pagination<SchoolGroup>(pageIndex, 5, teacherlist.ClassGroups,11);
            return View(objPage);
        }
    }
}