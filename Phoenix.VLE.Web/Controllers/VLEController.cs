﻿using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Controllers
{
    public class VLEController : Controller
    {
        UserService _userService = new UserService();
        // GET: VLE
        public ActionResult Index()
        {
            string viewName = "StudentDashboard";//Make it "" after testing
            StudentDetail studentDetail = new StudentDetail();
            //if (SessionHelper.CurrentSession.IsStudent())
            if (true)
            {
                IStudentService _studentService = new StudentService();

                studentDetail = _studentService.GetStudentByUserId(SessionHelper.CurrentSession.Id);
                viewName = "StudentDashboard";
            }
            if (SessionHelper.CurrentSession.IsTeacher())
            {
                viewName = "TeacherDashboard";
            }
            return View(viewName, studentDetail);
        }

    }
}