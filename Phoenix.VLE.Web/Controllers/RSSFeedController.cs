﻿using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Controllers
{
    public class RSSFeedController : Controller
    {
        private IRSSFeedService _rssFeedServices;

        public RSSFeedController(IRSSFeedService rssFeedServices)
        {
            _rssFeedServices = rssFeedServices;
        }
        public ActionResult Index()
        {
            return View();
        }

      
        //added for rssfeed
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveRSSFeed(RSSFeedEdit objRSSFeed)
        {
            try
            {
                var result = _rssFeedServices.AddUpdateRSSFeed(objRSSFeed);
                return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace);

            }
            finally
            {

            }

        }
    }
}