﻿using Saml;
using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using Phoenix.Common.Extensions;
using System.Web;
using System.Web.Script.Serialization;
using Phoenix.Common.Helpers;
using Phoenix.Common;
using Phoenix.Models;
using System.Linq;
using Phoenix.VLE.Web.Helpers;
using System.IO;
using System.Web.Configuration;
using Phoenix.Common.Logger;
using System.Collections.Generic;
using Phoenix.Common.ViewModels;
using System.Text;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using System.Security.Principal;
using Microsoft.Owin.Security.Cookies;
using System.Security.Claims;
using System.Threading.Tasks;
using Phoenix.VLE.Web.Utils;
using Phoenix.VLE.Web.Models;
using System.Net;
using System.Threading;

namespace Phoenix.VLE.Web.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        private readonly ILogInUserService _logInUserService;
        string samlSignout = ConfigurationManager.AppSettings["SamlSignOut"];
        private ILoggerClient _loggerClient;
        private readonly ICensorFilterService _censorFilterService;
        private readonly IStudentService _studentService;
        private readonly IThemesService _themesServiceService;
        private ITemplateLoginService _templateLogInService;
        //private readonly INotificationService _notificationService;
        private readonly IPhoenixTokenService _phoenixTokenService;
        private readonly IUserPermissionService _userPermissionService;
        double SessionTimeOut = Convert.ToDouble(ConfigurationManager.AppSettings["SessionTimeOut"]);

        public AccountController(ILogInUserService logInUserService, ICensorFilterService censorFilterService, IStudentService studentService, IThemesService ThemesService/*, INotificationService notificationService*/, IPhoenixTokenService phoenixTokenService, IUserPermissionService userPermissionService)
        {
            _logInUserService = logInUserService;
            _loggerClient = LoggerClient.Instance;
            _censorFilterService = censorFilterService;
            _studentService = studentService;
            _themesServiceService = ThemesService;
            //_notificationService = notificationService;
            _phoenixTokenService = phoenixTokenService;
            _userPermissionService = userPermissionService;
        }

        public ActionResult Index()
        {
            if (Helpers.CommonHelper.GetLoginUser() != null)
            {
                return RedirectToAction("Index", "Home");
            }

            // Signout from FormsAuthentication
            Session.Clear();
            FormsAuthentication.SignOut();
            return Redirect(PhoenixConfiguration.Instance.LoginPageUrl);
        }

        [AllowAnonymous]
        public void SignIn()
        {
            //specify the SAML provider url here, aka "Endpoint"
            var samlEndpoint = ConfigurationManager.AppSettings["SamlEndpoint"];

            string appIdUrl, samlConsumeUrl;
            if (ConfigurationManager.AppSettings["IsSandbox"].ToString() == "true")
            {
                appIdUrl = ConfigurationManager.AppSettings["AppIDUrlSandbox"];
                samlConsumeUrl = ConfigurationManager.AppSettings["SamlConsumeUrlSandbox"];
            }
            else
            {
                appIdUrl = ConfigurationManager.AppSettings["AppIDUrl"];
                samlConsumeUrl = ConfigurationManager.AppSettings["SamlConsumeUrl"];
            }

            var request = new AuthRequest(
                appIdUrl, //put your app's "unique ID" here
                samlConsumeUrl //assertion Consumer Url - the redirect URL where the provider will send authenticated users
                );

            //generate the provider URL
            string url = request.GetRedirectUrl(samlEndpoint);

            _loggerClient.LogWarning("samlEndpoint redirectd: " + samlEndpoint);
            //then redirect your user to the above "url" var
            //for example, like this:
            Response.Redirect(url);
        }

        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult EnterClassroom(string returnUrl)
        {
            //commented to clear cookies on login-By Rohan P 10/09/2020
            //if (SessionHelper.IsSessionActive())
            //{
            //    if (SessionHelper.CurrentSession.UserTypeId == 5)
            //    {
            //        return Redirect(PhoenixConfiguration.Instance.AdminHomePageUrl);
            //    }
            //    return Redirect(PhoenixConfiguration.Instance.HomePageUrl);
            //}
            ViewBag.ReturnUrl = returnUrl;
            SessionHelper.LogOffUser();
            return View("Login");
        }

        [AllowAnonymous]
        public ActionResult Enter(string returnUrl)
        {
            return View("SignIn");
        }


        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnUrl)
        {
            //commented to clear cookies on login-By Rohan P 10/09/2020
            //if (SessionHelper.IsSessionActive())
            //{
            //    if (SessionHelper.CurrentSession.UserTypeId == 5)
            //    {
            //        return Redirect(PhoenixConfiguration.Instance.AdminHomePageUrl);
            //    }
            //    return Redirect(PhoenixConfiguration.Instance.HomePageUrl);
            //}
            ViewBag.ReturnUrl = returnUrl;
            SessionHelper.LogOffUser();
            return View("Login2");
        }


        public UserSessionModel getUserSessionModel(LogInUser user)
        {
            UserSessionModel userSession = new UserSessionModel();
            //Assign value to user session data object
            userSession.SchoolName = user.SchoolBusinessUnitName;
            userSession.SchoolEmail = user.SchoolBusinessUnitEmail;
            userSession.SchoolCode = user.SchoolBusinessUnitCode;
            userSession.BusinessUnitTypeId = user.SchoolBusinessUnitTypeId;
            userSession.BusinessUnitType = user.SchoolBusinessUnitType;
            userSession.UserTypeName = user.UserTypeName;
            userSession.RoleId = user.RoleId;
            userSession.RoleName = user.RoleName;
            userSession.SchoolImage = user.SchoolImage;
            userSession.ProfilePhoto = user.ProfilePhoto;
            userSession.UserAvatar = user.UserAvatar;
            userSession.IsGEMSBU = user.IsGEMSBU;
            userSession.GoogleDriveIntegrationType = user.GoogleDriveIntegrationType;
            userSession.GoogleDriveClientKey = user.GoogleDriveClientKey;
            userSession.GoogleDriveSecretKey = user.GoogleDriveSecretKey;
            userSession.PhoneNumber = user.PhoneNumber;
            userSession.IsMultilingual = user.IsMultilingual;

            return userSession;
        }

        public UserPrincipalSerializeModel getserializeModel(LogInUser user)
        {
            UserPrincipalSerializeModel serializeModel = new UserPrincipalSerializeModel();
            serializeModel.Id = user.Id;
            string firstName = string.IsNullOrEmpty(user.FirstName) ? "" : user.FirstName;
            string lastName = string.IsNullOrEmpty(user.LastName) ? "" : user.LastName;
            serializeModel.FirstName = firstName;
            serializeModel.LastName = lastName;
            //serializeModel.FullName = $"{user.FirstName} {user.LastName}";                    
            serializeModel.FullName = firstName + " " + lastName;
            serializeModel.OldUserId = user.OldUserId;
            serializeModel.Email = user.Email;
            serializeModel.UserName = user.UserName;
            serializeModel.SchoolId = user.SchoolId;
            serializeModel.LanguageId = user.LanguageId;
            serializeModel.IsAdmin = user.IsSuperAdmin;
            serializeModel.UserTypeId = user.UserTypeId;
            return serializeModel;
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            bool IsVerified = false;
            var user = new LogInUser();
            var visitorInfo = new VisitorInfo();
            var userLogin = _logInUserService.CheckLoginUser(model);
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();
            string IpDetails = IpAddress + ":" + HostIpAddress;
            if (userLogin.success == "true")
            {
                IsVerified = true;
            }
            // for Parent Check
            if (IsVerified == true && (!string.IsNullOrEmpty(model.UserName)) && model.userType == (int)UserTypes.Parent)
            {
                user = _logInUserService.GetLoginUserByUserName(model.UserName, IpDetails);
                UserPrincipalSerializeModel serializeModel = getserializeModel(user);
                UserSessionModel userSession = getUserSessionModel(user);
                SetToken(user.Token); //Setting API token
                SetPhoenixToken();//Setting Phoenix API Token

                if (user.UserTypeId == (int)UserTypes.Parent)
                {
                    var studentList = _studentService.GetStudentByFamily(user.Id);
                    Session["FamilyStudentList"] = studentList;
                    if (studentList.Any())
                    { 
                        Session["CurrentSelectedStudent"] = studentList.FirstOrDefault();
                        DisplayTeamsAndConditionPopUp(studentList);
                    }
                    SetNotificationCookie("PARENT", user.UserName);

                    //get set  SystemLanguge Cookie
                    SetPreferedLanguage(user.Id, user.LanguageId);
                    //timezone Offset cookie
                    Helpers.CommonHelper.AppendCookie("TimeZone", "IsSet", "false");
                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    string userData = serializer.Serialize(serializeModel);

                    //Store session data in cookie
                    string userSessionData = serializer.Serialize(userSession);
                    SetUserSessionData(userSessionData);

                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                             1,
                             $"{user.FirstName} {user.LastName}",
                             DateTime.Now,
                             DateTime.Now.AddMinutes(SessionTimeOut),
                             false,
                             userData);
                    FormsAuthentication.SetAuthCookie(user.UserName, false);

                    string encTicket = FormsAuthentication.Encrypt(authTicket);

                    int maxByteSize = 4000;
                    if (System.Text.ASCIIEncoding.ASCII.GetByteCount(encTicket) > maxByteSize)
                    {
                        _loggerClient.LogWarning("Login cookie hash value exceeded max value of 4000");
                    }


                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    Response.Cookies.Add(faCookie);
                    string redirectUrl = returnUrl;
                    var syncContext = SynchronizationContext.Current;
                    SynchronizationContext.SetSynchronizationContext(null);
                    var isPermissionAssigned = !string.IsNullOrWhiteSpace(redirectUrl) && !redirectUrl.Contains("?") ? _userPermissionService.IsPermissionAssigned(user.Id, redirectUrl, user.UserTypeId).Result : true;
                    SynchronizationContext.SetSynchronizationContext(syncContext);
                    string rdUrl = GetEncodedQueryParams(redirectUrl).Replace("+", "%2b");

                    if (!string.IsNullOrEmpty((rdUrl)) && isPermissionAssigned) return Redirect(rdUrl);
                    return Redirect(PhoenixConfiguration.Instance.HomePageUrl);
                }
                else
                {
                    ModelState.AddModelError("", ResourceManager.GetString("Shared.Login.InvalidAttempt"));
                    return View("Login2", model);
                }
            }
            else
            {
                ModelState.AddModelError("", ResourceManager.GetString("Shared.Login.InvalidAttempt"));
                return View("Login2", model);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult EnterClassroom(LoginViewModel model, string returnUrl)
        {
            bool IsVerifed = false;
            var user = new LogInUser();
            var visitorInfo = new VisitorInfo();
            //var userLogin = _logInUserService.CheckLoginUser(model);
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();
            string IpDetails = IpAddress + ":" + HostIpAddress;
            //if (userLogin.success == "success")
            //{
            //    IsVerifed = true;
            //}
            if (IsVerifed)
            {
                user = _logInUserService.GetLoginUserByUserName(model.UserName, IpDetails);
            }
            else
            {
                user = _logInUserService.GetLoginUserByUserNamePassword(model.UserName, model.Password, IpDetails);
            }

            if (!string.IsNullOrEmpty(user.UserName))
            {
                UserPrincipalSerializeModel serializeModel = new UserPrincipalSerializeModel();
                UserSessionModel userSession = new UserSessionModel();

                serializeModel.Id = user.Id;
                string firstName = string.IsNullOrEmpty(user.FirstName) ? "" : user.FirstName;
                string lastName = string.IsNullOrEmpty(user.LastName) ? "" : user.LastName;
                serializeModel.FirstName = firstName;
                serializeModel.LastName = lastName;
                //serializeModel.FullName = $"{user.FirstName} {user.LastName}";                    
                serializeModel.FullName = firstName + " " + lastName;
                serializeModel.OldUserId = user.OldUserId;
                serializeModel.Email = user.Email;
                serializeModel.UserName = user.UserName;
                serializeModel.SchoolId = user.SchoolId;
                serializeModel.LanguageId = user.LanguageId;
                serializeModel.IsAdmin = user.IsSuperAdmin;
                serializeModel.UserTypeId = user.UserTypeId;

                //Assign value to user session data object
                userSession.SchoolName = user.SchoolBusinessUnitName;
                userSession.SchoolEmail = user.SchoolBusinessUnitEmail;
                userSession.SchoolCode = user.SchoolBusinessUnitCode;
                userSession.BusinessUnitTypeId = user.SchoolBusinessUnitTypeId;
                userSession.BusinessUnitType = user.SchoolBusinessUnitType;
                userSession.UserTypeName = user.UserTypeName;
                userSession.RoleId = user.RoleId;
                userSession.RoleName = user.RoleName;
                userSession.SchoolImage = user.SchoolImage;
                userSession.ProfilePhoto = user.ProfilePhoto;
                userSession.UserAvatar = user.UserAvatar;
                userSession.IsGEMSBU = user.IsGEMSBU;
                userSession.GoogleDriveIntegrationType = user.GoogleDriveIntegrationType;
                userSession.GoogleDriveClientKey = user.GoogleDriveClientKey;
                userSession.GoogleDriveSecretKey = user.GoogleDriveSecretKey;
                userSession.PhoneNumber = user.PhoneNumber;
                userSession.ZoomSessionEnabled = user.ZoomSessionEnabled;
                userSession.TeamsSessionEnabled = user.TeamsSessionEnabled;
                userSession.IsMultilingual = user.IsMultilingual;
                SetToken(user.Token); //Setting API token
                SetPhoenixToken();//Setting Phoenix API Token

                if (user.UserTypeId == (int)UserTypes.Student)
                {
                    var studentList = _studentService.GetStudentByUserId(user.Id);
                    if (studentList == null)
                    {
                        return Redirect("/Error/Nomapping");
                    }
                    DisplayStudentAccessInstructionPopUp(studentList);
                    serializeModel.ParentId = studentList.ParentId;
                    serializeModel.ParentUsername = studentList.ParentUserName;
                    Session["CurrentSelectedStudent"] = studentList;
                    var SchoolThemes = _themesServiceService.GetStudentGradeTheme(user.Id);
                    if (SchoolThemes.Count() > 0)
                        serializeModel.StudentTheme = SchoolThemes.FirstOrDefault().ThemeCssFileName;
                    SetNotificationCookie("STUDENT", user.OldUserId.ToString());
                }
                if (user.UserTypeId == (int)UserTypes.Parent)
                {
                    var studentList = _studentService.GetStudentByFamily(user.Id);
                    DisplayTeamsAndConditionPopUp(studentList);
                    Session["FamilyStudentList"] = studentList;
                    if (studentList.Any())
                        Session["CurrentSelectedStudent"] = studentList.FirstOrDefault();
                    SetNotificationCookie("PARENT", user.UserName);
                }
                if (user.UserTypeId != (int)UserTypes.Parent)
                {
                    var bannedWords = _censorFilterService.GetBannedWords(Convert.ToInt32(user.SchoolId));
                    Session["BannedWords"] = bannedWords;
                    SessionHelper.CurrentSession.BannedWords = new List<BannedWord>();
                    SessionHelper.CurrentSession.BannedWords = bannedWords.ToList();
                }

                if (user.UserTypeId >= (int)UserTypes.Teacher)
                {
                    //Session["IsSmartSchoolActive"] = user.IsSmartSchoolActive;
                    Helpers.CommonHelper.AppendCookie("sms", "act", user.IsSmartSchoolActive.ToString());
                }
                //get set  SystemLanguge Cookie
                SetPreferedLanguage(user.Id, user.LanguageId);
                //timezone Offset cookie
                Helpers.CommonHelper.AppendCookie("TimeZone", "IsSet", "false");
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                string userData = serializer.Serialize(serializeModel);

                //Store session data in cookie
                string userSessionData = serializer.Serialize(userSession);
                SetUserSessionData(userSessionData);

                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                         1,
                         $"{user.FirstName} {user.LastName}",
                         DateTime.Now,
                         DateTime.Now.AddMinutes(SessionTimeOut),
                         false,
                         userData);
                FormsAuthentication.SetAuthCookie(user.UserName, false);

                string encTicket = FormsAuthentication.Encrypt(authTicket);

                int maxByteSize = 4000;
                if (System.Text.ASCIIEncoding.ASCII.GetByteCount(encTicket) > maxByteSize)
                {
                    _loggerClient.LogWarning("Login cookie hash value exceeded max value of 4000");
                }


                HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                faCookie.Secure = !Request.IsLocal;
                Response.Cookies.Add(faCookie);

                //commented to redirect admin to Classroom - Rohan P

                //user.UserTypeId == 4 || => commened this logic, as school admin will also redirect to classroom - Hatims
                string redirectUrl = returnUrl;
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var isPermissionAssigned = !string.IsNullOrWhiteSpace(redirectUrl) && !redirectUrl.Contains("?") ? _userPermissionService.IsPermissionAssigned(user.Id, redirectUrl, user.UserTypeId).Result : true;
                SynchronizationContext.SetSynchronizationContext(syncContext);
                string rdUrl = GetEncodedQueryParams(redirectUrl).Replace("+", "%2b");

                if (!string.IsNullOrEmpty((rdUrl)) && isPermissionAssigned) return Redirect(rdUrl);
                if (user.UserTypeId == 5)
                {
                    return Redirect(PhoenixConfiguration.Instance.AdminHomePageUrl);
                }
                return Redirect(PhoenixConfiguration.Instance.HomePageUrl);
            }
            else
            {
                ModelState.AddModelError("", ResourceManager.GetString("Shared.Login.InvalidAttempt"));
                return View("Login", model);
            }
        }

        private string GetEncodedQueryParams(string uri)
        {
            var finalString = string.Empty;
            var queryUrl = new Uri("https://gemseducation.com" + uri);
            var queryDictionary = System.Web.HttpUtility.ParseQueryString(queryUrl.Query);
            foreach (string key in queryDictionary)
            {
                finalString = (string.IsNullOrEmpty(finalString) ? "?" : "&") + $"{key}=" + WebUtility.UrlEncode(queryDictionary.GetValues(key).FirstOrDefault());
            }
            return queryUrl.AbsolutePath.Replace("https://gemseducation.com", "") + finalString;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult SignIn(LoginViewModel model)
        {
            _templateLogInService = new TemplateLoginService();
            var visitorInfo = new VisitorInfo();
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();
            string IpDetails = IpAddress + ":" + HostIpAddress;
            bool isValidUser = _templateLogInService.IsValidUser(model.UserName, model.Password);
            if (isValidUser)
            {
                var user = _logInUserService.GetLoginUserByUserName(model.UserName, IpDetails);

                if (!string.IsNullOrEmpty(user.UserName))
                {
                    UserPrincipalSerializeModel serializeModel = new UserPrincipalSerializeModel();
                    UserSessionModel userSession = new UserSessionModel();

                    serializeModel.Id = user.Id;
                    string firstName = string.IsNullOrEmpty(user.FirstName) ? "" : user.FirstName;
                    string lastName = string.IsNullOrEmpty(user.LastName) ? "" : user.LastName;
                    serializeModel.FirstName = firstName;
                    serializeModel.LastName = lastName;
                    //serializeModel.FullName = $"{user.FirstName} {user.LastName}";                    
                    serializeModel.FullName = firstName + " " + lastName;
                    serializeModel.OldUserId = user.OldUserId;
                    serializeModel.Email = user.Email;
                    serializeModel.UserName = user.UserName;
                    serializeModel.SchoolId = user.SchoolId;
                    serializeModel.LanguageId = user.LanguageId;
                    serializeModel.IsAdmin = user.IsSuperAdmin;
                    serializeModel.UserTypeId = user.UserTypeId;

                    //Assign value to user session data object
                    userSession.SchoolName = user.SchoolBusinessUnitName;
                    userSession.SchoolEmail = user.SchoolBusinessUnitEmail;
                    userSession.SchoolCode = user.SchoolBusinessUnitCode;
                    userSession.BusinessUnitTypeId = user.SchoolBusinessUnitTypeId;
                    userSession.BusinessUnitType = user.SchoolBusinessUnitType;
                    userSession.UserTypeName = user.UserTypeName;
                    userSession.RoleId = user.RoleId;
                    userSession.RoleName = user.RoleName;
                    userSession.SchoolImage = user.SchoolImage;
                    userSession.ProfilePhoto = user.ProfilePhoto;
                    userSession.UserAvatar = user.UserAvatar;
                    userSession.IsGEMSBU = user.IsGEMSBU;
                    userSession.GoogleDriveIntegrationType = user.GoogleDriveIntegrationType;
                    userSession.GoogleDriveClientKey = user.GoogleDriveClientKey;
                    userSession.GoogleDriveSecretKey = user.GoogleDriveSecretKey;
                    userSession.PhoneNumber = user.PhoneNumber;
                    //serializeModel.AccessToken = string.Empty; //user.Token;

                    SetToken(user.Token);

                    if (user.UserTypeId == (int)UserTypes.Student)
                    {
                        var studentList = _studentService.GetStudentByUserId(user.Id);
                        if (studentList == null)
                        {
                            return Redirect("/Error/Nomapping");
                        }
                        serializeModel.ParentId = studentList.ParentId;
                        serializeModel.ParentUsername = studentList.ParentUserName;
                        var SchoolThemes = _themesServiceService.GetAllSchoolThemes(Convert.ToInt32(user.SchoolId));
                        if (SchoolThemes.Count() > 0)
                            serializeModel.StudentTheme = SchoolThemes.FirstOrDefault().ThemeCssFileName;

                        SetNotificationCookie("STUDENT", user.OldUserId.ToString());
                    }
                    if (user.UserTypeId == (int)UserTypes.Parent)
                    {
                        var studentList = _studentService.GetStudentByFamily(user.Id);
                        DisplayTeamsAndConditionPopUp(studentList);
                        Session["FamilyStudentList"] = studentList;
                        if (studentList.Any())
                            Session["CurrentSelectedStudent"] = studentList.FirstOrDefault();

                        SetNotificationCookie("PARENT", user.UserName);
                    }
                    if (user.UserTypeId != (int)UserTypes.Parent)
                    {
                        var bannedWords = _censorFilterService.GetBannedWords(Convert.ToInt32(user.SchoolId));
                        Session["BannedWords"] = bannedWords;
                        SessionHelper.CurrentSession.BannedWords = new List<BannedWord>();
                        SessionHelper.CurrentSession.BannedWords = bannedWords.ToList();
                    }
                    if (user.UserTypeId >= (int)UserTypes.Teacher)
                    {
                        Helpers.CommonHelper.AppendCookie("sms", "act", user.IsSmartSchoolActive.ToString());
                    }
                    //add SystemLanguge Cookie
                    SystemLanguageHelper.SetCurrentSystemLanguageCookie(SessionHelper.CurrentSession.LanguageId.ToString());
                    //timezone Offset cookie
                    Helpers.CommonHelper.AppendCookie("TimeZone", "IsSet", "false");
                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    string userData = serializer.Serialize(serializeModel);
                    //Store session data in cookie
                    string userSessionData = serializer.Serialize(userSession);
                    SetUserSessionData(userSessionData);

                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                             1,
                             $"{user.FirstName} {user.LastName}",
                             DateTime.Now,
                             DateTime.Now.AddMinutes(SessionTimeOut),
                             false,
                             userData);
                    FormsAuthentication.SetAuthCookie(user.UserName, false);

                    string encTicket = FormsAuthentication.Encrypt(authTicket);

                    int maxByteSize = 4000;
                    if (System.Text.ASCIIEncoding.ASCII.GetByteCount(encTicket) > maxByteSize)
                    {
                        _loggerClient.LogWarning("Login cookie hash value exceeded max value of 4000");
                    }


                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    Response.Cookies.Add(faCookie);


                    //commented to redirect admin to Classroom - Rohan P
                    //if (user.UserTypeId == 4 || user.UserTypeId == 5)
                    //{
                    //    return Redirect(PhoenixConfiguration.Instance.AdminHomePageUrl);
                    //}
                    return Redirect(PhoenixConfiguration.Instance.HomePageUrl);
                }
            }

            return View("SignIn", model);
        }

        [AllowAnonymous]
        public ActionResult SamlConsume()
        {
            //specify the certificate that your SAML provider has given to you
            string samlCertificate = System.IO.File.ReadAllText(Server.MapPath("/Content/X509Certificate/X509.txt"));

            Saml.Response samlResponse = new Response(samlCertificate);

            samlResponse.LoadXmlFromBase64(Request.Form["SAMLResponse"]); //SAML providers usually POST the data into this var
            var visitorInfo = new VisitorInfo();
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();
            string IpDetails = IpAddress + ":" + HostIpAddress;
            if (samlResponse.IsValid())
            {
                string username, email, firstname, lastname;
                try
                {
                    email = samlResponse.GetEmail();
                    firstname = samlResponse.GetFirstName();
                    lastname = samlResponse.GetLastName();
                    username = samlResponse.GetNameID();
                    var user = _logInUserService.GetLoginUserByUserName(username, IpDetails);
                    if (!string.IsNullOrEmpty(user.UserName))
                    {

                        UserPrincipalSerializeModel serializeModel = new UserPrincipalSerializeModel();
                        UserSessionModel userSession = new UserSessionModel();

                        serializeModel.Id = user.Id;
                        string firstName = string.IsNullOrEmpty(user.FirstName) ? "" : user.FirstName;
                        string lastName = string.IsNullOrEmpty(user.LastName) ? "" : user.LastName;
                        serializeModel.FirstName = firstName;
                        serializeModel.LastName = lastName;
                        //serializeModel.FullName = $"{user.FirstName} {user.LastName}";                    
                        serializeModel.FullName = firstName + " " + lastName;
                        serializeModel.OldUserId = user.OldUserId;
                        serializeModel.Email = user.Email;
                        serializeModel.UserName = user.UserName;
                        serializeModel.SchoolId = user.SchoolId;
                        serializeModel.LanguageId = user.LanguageId;
                        serializeModel.IsAdmin = user.IsSuperAdmin;
                        serializeModel.UserTypeId = user.UserTypeId;

                        //Assign value to user session data object
                        userSession.SchoolName = user.SchoolBusinessUnitName;
                        userSession.SchoolEmail = user.SchoolBusinessUnitEmail;
                        userSession.SchoolCode = user.SchoolBusinessUnitCode;
                        userSession.BusinessUnitTypeId = user.SchoolBusinessUnitTypeId;
                        userSession.BusinessUnitType = user.SchoolBusinessUnitType;
                        userSession.UserTypeName = user.UserTypeName;
                        userSession.RoleId = user.RoleId;
                        userSession.RoleName = user.RoleName;
                        userSession.SchoolImage = user.SchoolImage;
                        userSession.ProfilePhoto = user.ProfilePhoto;
                        userSession.UserAvatar = user.UserAvatar;
                        userSession.IsGEMSBU = user.IsGEMSBU;
                        userSession.GoogleDriveIntegrationType = user.GoogleDriveIntegrationType;
                        userSession.GoogleDriveClientKey = user.GoogleDriveClientKey;
                        userSession.GoogleDriveSecretKey = user.GoogleDriveSecretKey;
                        userSession.PhoneNumber = user.PhoneNumber;

                        SetToken(user.Token); //Setting API token

                        if (user.UserTypeId == (int)UserTypes.Student)
                        {

                            var studentList = _studentService.GetStudentByUserId(user.Id);

                            if (studentList == null)
                            {
                                return Redirect("/Error/Nomapping");
                            }
                            serializeModel.ParentId = studentList.ParentId;
                            serializeModel.ParentUsername = studentList.ParentUserName;
                            var SchoolThemes = _themesServiceService.GetAllSchoolThemes(Convert.ToInt32(user.SchoolId));
                            if (SchoolThemes.Count() > 0)
                                serializeModel.StudentTheme = SchoolThemes.FirstOrDefault().ThemeCssFileName;

                            SetNotificationCookie("STUDENT", user.OldUserId.ToString());
                        }
                        if (user.UserTypeId == (int)UserTypes.Parent)
                        {

                            var studentList = _studentService.GetStudentByFamily(user.Id);
                            Session["FamilyStudentList"] = studentList;
                            if (studentList.Any())
                                Session["CurrentSelectedStudent"] = studentList.FirstOrDefault();

                            SetNotificationCookie("PARENT", user.UserName);
                        }
                        if (user.UserTypeId != (int)UserTypes.Parent)
                        {
                            var bannedWords = _censorFilterService.GetBannedWords(Convert.ToInt32(user.SchoolId));
                            Session["BannedWords"] = bannedWords;
                            SessionHelper.CurrentSession.BannedWords = new List<BannedWord>();
                            SessionHelper.CurrentSession.BannedWords = bannedWords.ToList();
                        }
                        if (user.UserTypeId >= (int)UserTypes.Teacher)
                        {
                            Helpers.CommonHelper.AppendCookie("sms", "act", user.IsSmartSchoolActive.ToString());
                        }

                        //get set  SystemLanguge Cookie
                        SetPreferedLanguage(user.Id, user.LanguageId);
                        //timezone Offset cookie
                        Helpers.CommonHelper.AppendCookie("TimeZone", "IsSet", "false");
                        JavaScriptSerializer serializer = new JavaScriptSerializer();

                        string userData = serializer.Serialize(serializeModel);

                        //Store session data in cookie
                        string userSessionData = serializer.Serialize(userSession);
                        SetUserSessionData(userSessionData);

                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                                 1,
                                 $"{user.FirstName} {user.LastName}",
                                 DateTime.Now,
                                 DateTime.Now.AddMinutes(SessionTimeOut),
                                 false,
                                 userData);
                        FormsAuthentication.SetAuthCookie(user.UserName, false);

                        string encTicket = FormsAuthentication.Encrypt(authTicket);
                        int maxByteSize = 4000;
                        if (System.Text.ASCIIEncoding.ASCII.GetByteCount(encTicket) > maxByteSize)
                        {
                            _loggerClient.LogWarning("Login cookie hash value exceeded max value of 4000");
                        }

                        HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                        Response.Cookies.Add(faCookie);


                        //commented to redirect admin to Classroom - Rohan P
                        //if (user.UserTypeId == 4 || user.UserTypeId == 5)
                        //{
                        //    return Redirect(PhoenixConfiguration.Instance.AdminHomePageUrl);
                        //}
                        //_loggerClient.LogWarning("Redirecting to home page");
                        return Redirect(PhoenixConfiguration.Instance.HomePageUrl);
                    }
                    return Redirect(samlSignout);
                }
                catch (Exception ex)
                {
                    _loggerClient.LogWarning("Error in samlconsume: " + ex);
                    return null;
                }

                //user has been authenticated, put your code here, like set a cookie or something...
                //or call FormsAuthentication.SetAuthCookie() or something

            }
            return Redirect("~/Home/Index");
        }

        [AllowAnonymous]
        public async Task<ActionResult> MsSignout()
        {
            if (SessionHelper.CurrentSession.IsSSOLogin)
            {
                await MsalAppBuilder.ClearUserTokenCache();

                // Send an OpenID Connect sign-out request.
                HttpContext.GetOwinContext().Authentication.SignOut(OpenIdConnectAuthenticationDefaults.AuthenticationType, CookieAuthenticationDefaults.AuthenticationType);
            }
            return SignOut();
        }

        [AllowAnonymous]
        public ActionResult SignOut()
        {
            string isSandbox = ConfigurationManager.AppSettings["IsSandbox"];
            SessionHelper.LogOffUser();
            if (isSandbox == "false")
            {
                return Redirect(samlSignout);
            }
            return Redirect(PhoenixConfiguration.Instance.LoginPageUrl);
        }

        [AllowAnonymous]
        public ActionResult SignOutCallback()
        {
            if (Request.IsAuthenticated)
            {
                // Redirect to home page if the user is authenticated.
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        public ActionResult ExtendSession()
        {
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View("ForgotPassword");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult ForgotPassword(LoginViewModel model)
        {
            try
            {
                var visitorInfo = new VisitorInfo();
                string IpAddress = visitorInfo.GetIpAddress();
                string HostIpAddress = visitorInfo.GetClientIPAddress();
                string IpDetails = IpAddress + ":" + HostIpAddress;
                string EmailBody = "";
                string toEmailAddrees = model.Email;
                var user = _logInUserService.GetLoginUserByUserName(model.UserName, IpDetails);
                if (user.Email != model.Email)
                {
                    ViewBag.ErrorMsg = ResourceManager.GetString("Shared.Login.EmailNotFound");
                    return View("ForgotPassword", model);
                }
                ICommonService _commonService = new CommonService();
                StreamReader reader = new StreamReader(Server.MapPath(Constants.ForgotPasswordEmailTemplate), Encoding.UTF8);
                EmailBody = reader.ReadToEnd();
                var encryptUserName = EncryptDecryptHelper.Encrypt(model.UserName);
                string ActivationLink = "//" + Request.Url.Host + "/Account/ResetPassword/" + encryptUserName;
                string ResetLink = "<a href='" + ActivationLink + "'>Password Reset Link</a>";
                EmailBody = EmailBody.Replace("{Link}", ResetLink);
                EmailBody = EmailBody.Replace("@@SchoolLogo", Constants.SchoolImageDir + SessionHelper.CurrentSession.SchoolImage);
                EmailDetail sendEmailNotificationView = new EmailDetail();
                //SendMailView sendEmailNotificationView = new SendMailView();
                //        public string EmailType { get; set; }
                //public string ToEmail { get; set; }
                //public string CCEmail { get; set; }
                //public string Subject { get; set; }
                //public string Message { get; set; }
                /// sendEmailNotificationView.FROM_EMAIL = SessionHelper.CurrentSession.Email;
                sendEmailNotificationView.ToEmail = toEmailAddrees;
                sendEmailNotificationView.CCEmail = "";
                sendEmailNotificationView.EmailType = "FORGOT_PASSWORD";
                sendEmailNotificationView.Subject = "Classroom Password Reset Link";
                sendEmailNotificationView.Message = EmailBody;
                //sendEmailNotificationView.LOG_USERNAME = SessionHelper.CurrentSession.UserName;
                //sendEmailNotificationView.STU_ID = "11111";
                //sendEmailNotificationView.LOG_PASSWORD = "tt";
                //sendEmailNotificationView.PORT_NUM = "123";
                //sendEmailNotificationView.EmailHostNew = "false";
                var operationalDetails = _commonService.SendEmailCirculation(sendEmailNotificationView);



                if (operationalDetails.Success)
                {
                    ViewBag.Msg = ResourceManager.GetString("Shared.Login.EmailSuccessMessage");
                }
                else
                 if (!operationalDetails.Success)
                {
                    ViewBag.Msg = ResourceManager.GetString("Shared.Login.EmailFailedMessage");
                }
                return View("ForgotPassword");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMsg = ResourceManager.GetString("Shared.Login.EmailErrorMessage");
                return View("ForgotPassword");
            }



        }
        [AllowAnonymous]
        public ActionResult ResetPassword(string id)
        {
            var userName = EncryptDecryptHelper.Decrypt(id);
            LoginViewModel model = new LoginViewModel();
            model.UserName = userName;
            return View("ResetPassword", model);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult ResetPassword(LoginViewModel model)
        {
            _templateLogInService = new TemplateLoginService();
            var returnValue = _templateLogInService.ResetPassword(model.UserName, model.Password);
            return View("ResetPassword");
        }
        private void SetPreferedLanguage(long userId, int languageId)
        {
            var cookieLangId = Helpers.CommonHelper.GetCookieValue("SystemLanguageCookie", "SystemLanguageId").ToInteger();
            if (cookieLangId != 0)
            {
                var _commonService = new CommonService();
                var result = _commonService.SetUserCurrentLanguage(cookieLangId, userId);
                if (result)
                {
                    Session["SystemLangaugeId"] = cookieLangId;
                }
            }
            else
                SystemLanguageHelper.SetCurrentSystemLanguageCookie(languageId.ToString());
        }
        [AllowAnonymous]
        public ActionResult ChangeLanguage(string Name, int languageId)
        {
            //add SystemLanguge Cookie
            SystemLanguageHelper.SetCurrentSystemLanguageCookie(languageId.ToString());
            LocalizationHelper.CurrentSystemLanguage = null;
            return RedirectToAction(Name);
        }

        private void SetToken(string token)
        {
            Helpers.CommonHelper.AppendCookie("st", "tk", token);
        }

        private void SetPhoenixToken()
        {
            var phoenixToken = _phoenixTokenService.GetAuthorizationToken();
            Helpers.CommonHelper.AppendCookie("phst", "tk", phoenixToken);
        }

        private void SetNotificationCookie(string key, string identifier)
        {
            string Source = string.Empty, Type = string.Empty;
            Source = "CLASSROOM";
            Type = "";

            Helpers.CommonHelper.AppendCookie("ShowNotification", "isShown", "true");

            //var allNotifications = _notificationService.GetAllNotifications(key, identifier, Source, Type);
            //var unreadNotifications = allNotifications.Where(x => x.IsRead == false).ToList();
            //TempData.Add("notificationData", unreadNotifications);

            Helpers.CommonHelper.AppendCookie("NotificationExist", "exist", "false");
        }


        private void DisplayStudentAccessInstructionPopUp(StudentDetail studentList)
        {
            if (studentList != null && !studentList.IsAccessAssigned)
            {
                Helpers.CommonHelper.AppendCookie("ShowStudentTermandCondition", "isShown", "true");
                Helpers.CommonHelper.AppendCookie("StudentTermandConditionExist", "exist", "true");
            }
            else
            {
                Helpers.CommonHelper.AppendCookie("ShowStudentTermandCondition", "isShown", "false");
                Helpers.CommonHelper.AppendCookie("StudentTermandConditionExist", "exist", "false");
            }
        }
        private void DisplayTeamsAndConditionPopUp(IEnumerable<StudentDetail> studentList)
        {
            if (studentList != null && studentList.Count() > 0)
            {
                int totalstudent = studentList.Count();
                int PermissionGrantedstudentCount = studentList.Where(x => x.IsAccessAssigned == true).Count();
                if (PermissionGrantedstudentCount != totalstudent)
                {
                    Helpers.CommonHelper.AppendCookie("ShowTermandCondition", "isShown", "true");
                    Helpers.CommonHelper.AppendCookie("TermandConditionExist", "exist", "true");
                }
                else
                {
                    Helpers.CommonHelper.AppendCookie("ShowTermandCondition", "isShown", "false");
                    Helpers.CommonHelper.AppendCookie("TermandConditionExist", "exist", "false");
                }
            }
        }
        private void SetUserSessionData(string userSessionData)
        {
            var userSessionEncryptData = EncryptDecryptHelper.Encrypt(userSessionData);
            Helpers.CommonHelper.AppendCookie("usd", "d", userSessionEncryptData);
        }

        [AllowAnonymous]
        public ActionResult ADLogin(string redirectUrl)
        {
            if (!Request.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(redirectUrl)) Helpers.CommonHelper.AppendCookie("redirect", "url", redirectUrl);
                HttpContext.GetOwinContext().Authentication.Challenge(
                    new AuthenticationProperties { RedirectUri = "/account/authcallback" },
                    OpenIdConnectAuthenticationDefaults.AuthenticationType);
                return Content("");
            }
            else
            {
                if (SessionHelper.CurrentSession.UserTypeId == 5)
                {
                    return Redirect(PhoenixConfiguration.Instance.AdminHomePageUrl);
                }
                return Redirect(PhoenixConfiguration.Instance.HomePageUrl);
            }
        }

        [AllowAnonymous]
        public ActionResult AuthCallback(string code)
        {
            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(SessionHelper.CurrentSession.GoogleDriveRedirectUri))
            {
                return RedirectToAction("OnOneDriveAuthorization", "Files", new { area = "Files", code = code });
            }

            // string signedInUserID = ClaimsPrincipal.Current.FindFirst(System.IdentityModel.Claims.ClaimTypes.NameIdentifier).Value;
            string preferredUsername = Session["SSOPreferredUserName"]?.ToString();
            // Uri oauthCodeProcessingPath = new Uri(Request.Url.GetLeftPart(UriPartial.Authority).ToString());

            FormsAuthentication.SignOut();
            System.Web.HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            string email = preferredUsername != null ? preferredUsername : string.Empty;
            string username = email.Split('@').First();
            var user = new LogInUser();
            var visitorInfo = new VisitorInfo();
            string IpAddress = visitorInfo.GetIpAddress();
            string HostIpAddress = visitorInfo.GetClientIPAddress();
            string IpDetails = IpAddress + ":" + HostIpAddress;
            user = _logInUserService.GetLoginUserByUserName(username, IpDetails);
            if (!string.IsNullOrEmpty(user.UserName))
            {
                UserPrincipalSerializeModel serializeModel = new UserPrincipalSerializeModel();
                UserSessionModel userSession = new UserSessionModel();

                serializeModel.Id = user.Id;
                string firstName = string.IsNullOrEmpty(user.FirstName) ? "" : user.FirstName;
                string lastName = string.IsNullOrEmpty(user.LastName) ? "" : user.LastName;
                string middleName = string.IsNullOrEmpty(user.MiddleName) ? "" : user.MiddleName;
                serializeModel.FirstName = firstName;
                serializeModel.LastName = lastName;
                serializeModel.FullName = firstName + " " + middleName + " " + lastName;
                serializeModel.OldUserId = user.OldUserId;
                serializeModel.Email = user.Email;
                serializeModel.UserName = user.UserName;
                serializeModel.SchoolId = user.SchoolId;
                serializeModel.LanguageId = user.LanguageId;
                serializeModel.IsAdmin = user.IsSuperAdmin;
                serializeModel.UserTypeId = user.UserTypeId;

                //Assign value to user session data object
                userSession.SchoolName = user.SchoolBusinessUnitName;
                userSession.SchoolEmail = user.SchoolBusinessUnitEmail;
                userSession.SchoolCode = user.SchoolBusinessUnitCode;
                userSession.BusinessUnitTypeId = user.SchoolBusinessUnitTypeId;
                userSession.BusinessUnitType = user.SchoolBusinessUnitType;
                userSession.GoogleDriveIntegrationType = user.GoogleDriveIntegrationType;
                userSession.GoogleDriveClientKey = user.GoogleDriveClientKey;
                userSession.GoogleDriveSecretKey = user.GoogleDriveSecretKey;
                userSession.UserTypeName = user.UserTypeName;
                userSession.RoleId = user.RoleId;
                userSession.RoleName = user.RoleName;
                userSession.SchoolImage = user.SchoolImage;
                userSession.ProfilePhoto = user.ProfilePhoto;
                userSession.UserAvatar = user.UserAvatar;
                userSession.IsSSOLogin = true;
                userSession.ZoomSessionEnabled = user.ZoomSessionEnabled;
                userSession.TeamsSessionEnabled = user.TeamsSessionEnabled;
                userSession.IsMultilingual = user.IsMultilingual;
                SetToken(user.Token); //Setting API token
                SetPhoenixToken();//Setting Phoenix API Token

                if (user.UserTypeId == (int)UserTypes.Student)
                {
                    var studentList = _studentService.GetStudentByUserId(user.Id);

                    if (studentList == null)
                    {
                        return Redirect("/Error/Nomapping");
                    }
                    DisplayStudentAccessInstructionPopUp(studentList);
                    serializeModel.ParentId = studentList.ParentId;
                    serializeModel.ParentUsername = studentList.ParentUserName;
                    Session["CurrentSelectedStudent"] = studentList;
                    var SchoolThemes = _themesServiceService.GetStudentGradeTheme(user.Id);
                    if (SchoolThemes.Count() > 0)
                        serializeModel.StudentTheme = SchoolThemes.FirstOrDefault().ThemeCssFileName;

                    SetNotificationCookie("STUDENT", user.OldUserId.ToString());
                }
                if (user.UserTypeId == (int)UserTypes.Parent)
                {
                    var studentList = _studentService.GetStudentByFamily(user.Id);
                    Session["FamilyStudentList"] = studentList;
                    if (studentList.Any())
                        Session["CurrentSelectedStudent"] = studentList.FirstOrDefault();

                    SetNotificationCookie("PARENT", user.UserName);
                }
                if (user.UserTypeId != (int)UserTypes.Parent)
                {
                    var bannedWords = _censorFilterService.GetBannedWords(Convert.ToInt32(user.SchoolId));
                    Session["BannedWords"] = bannedWords;
                    //SessionHelper.CurrentSession.BannedWords = new List<BannedWord>();
                    //SessionHelper.CurrentSession.BannedWords = bannedWords.ToList();
                }

                if (user.UserTypeId >= (int)UserTypes.Teacher)
                {
                    Helpers.CommonHelper.AppendCookie("sms", "act", user.IsSmartSchoolActive.ToString());
                }
                SetPreferedLanguage(user.Id, user.LanguageId);
                //timezone Offset cookie
                Helpers.CommonHelper.AppendCookie("TimeZone", "IsSet", "false");
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                string userData = serializer.Serialize(serializeModel);

                //Store session data in cookie
                string userSessionData = serializer.Serialize(userSession);
                SetUserSessionData(userSessionData);

                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                         1,
                         $"{user.FirstName} {user.LastName}",
                         DateTime.Now,
                         DateTime.Now.AddMinutes(SessionTimeOut),
                         false,
                         userData);
                FormsAuthentication.SetAuthCookie(user.UserName, false);

                string encTicket = FormsAuthentication.Encrypt(authTicket);

                int maxByteSize = 4000;
                if (System.Text.ASCIIEncoding.ASCII.GetByteCount(encTicket) > maxByteSize)
                {
                    _loggerClient.LogWarning("Login cookie hash value exceeded max value of 4000");
                }
                HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                faCookie.Secure = !Request.IsLocal;
                Response.Cookies.Add(faCookie);
                //commented to redirect admin to Classroom - Rohan P
                //if (user.UserTypeId == 4 || user.UserTypeId == 5)
                //{
                //    return Redirect(PhoenixConfiguration.Instance.AdminHomePageUrl);
                //}
                string redirectUrl = Helpers.CommonHelper.GetCookieValue("redirect", "url");
                var syncContext = SynchronizationContext.Current;
                SynchronizationContext.SetSynchronizationContext(null);
                var isPermissionAssigned = !string.IsNullOrWhiteSpace(redirectUrl) && !redirectUrl.Contains("?") ?_userPermissionService.IsPermissionAssigned(user.Id, redirectUrl, user.UserTypeId).Result: true;
                SynchronizationContext.SetSynchronizationContext(syncContext);
                string rdUrl = GetEncodedQueryParams(redirectUrl).Replace("+", "%2b");

                if (!string.IsNullOrEmpty((rdUrl)) && isPermissionAssigned) return Redirect(rdUrl);
                return Redirect(PhoenixConfiguration.Instance.HomePageUrl);
            }
            else
            {
                ModelState.AddModelError("", ResourceManager.GetString("Shared.Login.InvalidAttempt"));
                // Remove Micorosft Login Cookies if successully but user not exists in classroom
                //HttpCookie currentUserCookie = Request.Cookies[".AspNet.Cookies"];
                //Response.Cookies.Remove(".AspNet.Cookies");
                //currentUserCookie.Expires = DateTime.Now.AddDays(-10);
                //currentUserCookie.Value = null;
                //Response.SetCookie(currentUserCookie);
                Request.GetOwinContext().Authentication.SignOut();
                return Redirect(PhoenixConfiguration.Instance.LoginPageUrl);
            }
        }
        #region Elite Forgot Password
        [AllowAnonymous]
        [HttpPost]
        public ActionResult PostForgetUserName(ForgotPasswordStudentDetail Model)
        {
            var phoenixToken = _phoenixTokenService.GetAuthorizationToken();
            var response = _logInUserService.PostForgetUserName(Model.StudentId, Model.DateOfBirth, phoenixToken);
            return Json(response);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetValidateUserName(string ParentUserName, string otp)
        {
            var phoenixToken = _phoenixTokenService.GetAuthorizationToken();
            var response = _logInUserService.GetValidateUserName(ParentUserName, otp, phoenixToken);
            return Json(response);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult PasswordChange(string Password, string UserName)
        {
            var phoenixToken = _phoenixTokenService.GetAuthorizationToken();
            ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest() { NewPassword = Password, UserName = UserName };
            var response = _logInUserService.PasswordChange(changePasswordRequest, phoenixToken);
            return Json(response);
        }
        #endregion
    }
}