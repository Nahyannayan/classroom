﻿using System.Web.Mvc;

namespace Phoenix.VLE.Web.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index()
        {
            return View("Error");
        }


        public ActionResult NotFound()
        {
            return View("NotAuthorize");
        }

        public ActionResult HttpError500()
        {
            return View();
        }

        public ActionResult HttpError404()
        {
            return View();
        }

        public  ActionResult NoPermission()
        {
            return View();
        }
        public ActionResult NoMapping()
        {
            return View();
        }
    }
}
