﻿using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Logger;
using Phoenix.Common.Models;
using Phoenix.Common.Models.ViewModels;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models.EditModels;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IStudentService _studentService;
        private readonly IParentService _parentService;
        private readonly ITeacherDashboardService _teacherDashboardService;
        private ILoggerClient _loggerClient;
        private readonly ISchoolGroupService _schoolGroupService;
        private readonly IFileService _fileService;
        private readonly ISchoolBannerService _schoolBannerService;
        private readonly INotificationService _notificationService;
        private readonly IAssignmentService _assignmentService;
        private readonly IBlogService _blogService;
        private readonly IEventCategoryService _eventCategoryService;
        private readonly IStudentListService _studentListService;

        public HomeController(IUserService userService, IStudentService studentService, IParentService parentService, ITeacherDashboardService teacherDashboardService,
            ISchoolGroupService schoolGroupService, IFileService fileService, ISchoolBannerService schoolBannerService, INotificationService notificationService,
            IAssignmentService assignmentService, IBlogService blogService, IEventCategoryService eventCategoryService, IStudentListService StudentListService)
        {
            _userService = userService;
            _studentService = studentService;
            _parentService = parentService;
            _teacherDashboardService = teacherDashboardService;
            _schoolGroupService = schoolGroupService;
            _loggerClient = LoggerClient.Instance;
            _fileService = fileService;
            _schoolBannerService = schoolBannerService;
            _notificationService = notificationService;
            _assignmentService = assignmentService;
            _blogService = blogService;
            _eventCategoryService = eventCategoryService;
            _studentListService = StudentListService;
        }



        public ActionResult Index()
        {
            if (Session["LoginType"] != null && Session["LoginType"].ToString() != StringEnum.GetStringValue(MainModuleCodes.Classroom))
            {
                Session["LoginType"] = StringEnum.GetStringValue(MainModuleCodes.Classroom);
            }
            else
            {
                Session["LoginType"] = StringEnum.GetStringValue(MainModuleCodes.Classroom);
            }

            #region EventCategoryDropDown
            // comment the below code for new dashboard
            //List<EventCategoryView> eventCategories = _eventCategoryService.GetAllEventCategoryBySchool().ToList();
            //List<EventCategoryView> lstEventCategory = new List<EventCategoryView>
            //    {
            //        new EventCategoryView {  Name = "All Events", Description="All Events" },
            //        new EventCategoryView {  Name = "School Events", Description="School Events" },
            //        new EventCategoryView {  Name = "Student Timetable", Description="Student Timetable" },
            //        //new EventCategoryView {  Name = "Assignments", Description="Assignments" }
            //    };

            //if (eventCategories.Count > 0)
            //    lstEventCategory.AddRange(eventCategories);

            //var categoryList = lstEventCategory.Select(i => new Phoenix.Common.Models.ListItem
            //{
            //    ItemId = (i.Id == 0) ? i.Name : i.Id.ToString(),
            //    ItemName = !string.IsNullOrEmpty(i.Name) ? i.Name : i.Description
            //});

            //ViewBag.lstCategory = categoryList;
            #endregion

            if (SessionHelper.CurrentSession.IsStudent())
            {
                StudentUserViewModel model = new StudentUserViewModel();
                model.StudentDashboard = _studentService.GetStudentDashboard(SessionHelper.CurrentSession.Id);
                model.StudentDashboard.Banners = _schoolBannerService.GetUserBanners(SessionHelper.CurrentSession.Id).ToList();
                model.UserFeelingStatus = Helpers.CommonHelper.GetUserFeeling();

                if (TempData["notificationData"] == null)
                {
                    string notificationKey = GetUserNotificationIdentifiers(out string identifier);
                    model.Notifications = _notificationService.GetAllNotifications(notificationKey, identifier, "CLASSROOM", string.Empty).OrderByDescending(n => n.DateTime).ToList();
                    TempData["notificationData"] = model.Notifications;
                }
                else
                {
                    model.Notifications = TempData["notificationData"] as List<StudentNotification>;
                    TempData.Keep("notificationData");
                }

                //disabling it as per requested by Vishal Mehta, Deepak Singh 18/Sept/2020
                //IEnumerable<UserActivityPointsView> lstUserActivityPointsView = _studentService.GetStudentTotalActivePoints(SessionHelper.CurrentSession.Id);
                //if (lstUserActivityPointsView.Count() > 0)
                //{
                //    model.TotalActivePoints = lstUserActivityPointsView.Where(p => p.GroupName == "Overall").FirstOrDefault().Points;
                //}
                return View("StudentDashboard", model);
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                TeacherViewModel teacherViewModel = new TeacherViewModel();
                //var user = _userService.GetUserById(SessionHelper.CurrentSession.Id);
                //ViewBag.UserModel = user;
                teacherViewModel.TeacherDashboard = _teacherDashboardService.GetTeacherDashboard();

                if (TempData["notificationData"] == null)
                {
                    string notificationKey = GetUserNotificationIdentifiers(out string identifier);
                    teacherViewModel.Notifications = _notificationService.GetAllNotifications(notificationKey, identifier, "CLASSROOM", string.Empty).OrderByDescending(n => n.DateTime).ToList();
                    TempData["notificationData"] = teacherViewModel.Notifications;
                }
                else
                {
                    teacherViewModel.Notifications = TempData["notificationData"] as List<StudentNotification>;
                    TempData.Keep("notificationData");
                }
                return View("TeacherDashboard", teacherViewModel);
            }
            else if (SessionHelper.CurrentSession.IsParent())
            {
                //var parent = new ParentView();
                if (SessionHelper.CurrentSession.HasMultipleChilds() && SessionHelper.CurrentSession.CurrentSelectedStudent == null)
                {
                    //List<SchoolBanner> lstStudentBanners = new List<SchoolBanner>();
                    //lstStudentBanners = _schoolBannerService.GetUserBanners(SessionHelper.CurrentSession.Id).ToList();
                    //ViewBag.Banners = lstStudentBanners;

                    VLE.Web.Helpers.CommonHelper.AppendCookie("VirtualImgPath", "1", "");
                    return View("ParentDashboard");
                }
                else
                {
                    return RedirectToAction("Dashboard", "Parent");
                }
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult GetChatterDetailsForStudent(int timeZoneOffset)
        {
            StudentUserViewModel model = new StudentUserViewModel();
            model.BlogStudentDash = _blogService.GetStudentChatterBlogs(SessionHelper.CurrentSession.SchoolId, SessionHelper.CurrentSession.Id);
            model.OffSet = timeZoneOffset;
            return PartialView("_StudentDashboardChatter", model);
        }


        [HttpPost]
        public ActionResult GetChatterDetailsForTeacher(int timeZoneOffset)
        {
            TeacherViewModel model = new TeacherViewModel();
            List<SelectListGroup> lstSelectListGroup = new List<SelectListGroup>();
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.ClassGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.HomeTutorGroup"),
                Disabled = false
            });
            lstSelectListGroup.Add(new SelectListGroup
            {
                Name = ResourceManager.GetString("Shared.Groups.OtherGroups"),
                Disabled = false
            });
            var lstUserGroup = _schoolGroupService.GetSchoolGroupsByUserId(SessionHelper.CurrentSession.Id, SessionHelper.CurrentSession.IsTeacher()).ToList();
            var groupedOptions = lstUserGroup.Select(x => new SelectListItem
            {
                Value = x.SchoolGroupId.ToString(),
                Text = x.SchoolGroupName,
                Group = (x.GroupingName == "Class Group" ? lstSelectListGroup[0] : (x.GroupingName == "Home Tutor Group" ? lstSelectListGroup[1] : lstSelectListGroup[2])),
            }).ToList();

            ViewBag.lstSchoolGroup = groupedOptions;
            model.SchoolGroupId = 0;
            model.BlogTeacherDash = _blogService.GetTeacherChatterBlogs(SessionHelper.CurrentSession.SchoolId, SessionHelper.CurrentSession.Id);
            model.OffSet = timeZoneOffset;
            return PartialView("_TeacherDashboardChatter", model);
        }

        public ActionResult SendSampleEmail()
        {
            var toEmailAddress = "patil.rohit14@gmail.com";
            var subject = "Sample Email";
            var operationalDetails = EmailHelper.SendEmail(this, toEmailAddress, subject);
            //Default template
            return Json(operationalDetails, JsonRequestBehavior.AllowGet);
        }


        #region USERFEELING
        //Set User Feeling

        public ActionResult SaveUserFeeling(UserFeelingView userFeelingView)
        {
            UserFeelingView objFellingView = new UserFeelingView();
            if (!string.IsNullOrEmpty(Request["hdnFeelId"]))
            {
                var hdnFeelId = Request["hdnFeelId"].ToString() == "" ? 0 : Convert.ToInt32(Request["hdnFeelId"].ToString());
                userFeelingView.FeelId = hdnFeelId;
                userFeelingView.UserId = SessionHelper.CurrentSession.Id;
                userFeelingView.UserTypeId = SessionHelper.CurrentSession.UserTypeId;
                userFeelingView.CurrentStatus = Request["hdnCurrentStatus"].ToString();
                var result = _userService.UpdateUserFeeling(userFeelingView);
                var userDetails = _userService.GetUserById(SessionHelper.CurrentSession.Id);

                var userFeelings = _userService.GetUserFeelings(LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
                var UserFeel = userFeelings.Where(e => e.FeelId == userDetails.FeelId).FirstOrDefault();
                objFellingView.FeelId = userDetails.FeelId;
                objFellingView.FeelingType = UserFeel == null ? "" : UserFeel.FeelingType;
                objFellingView.Logo = UserFeel.Logo;
                objFellingView.UserId = userDetails.Id;
                objFellingView.UserTypeId = userDetails.UserTypeId;
                objFellingView.CurrentStatus = UserFeel == null ? "" : UserFeel.CurrentStatus;
                return Json(objFellingView, JsonRequestBehavior.AllowGet);
            }
            return Json(objFellingView, JsonRequestBehavior.AllowGet);
        }

        //[ChildActionOnly]
        public ActionResult ListUserFeeling()
        {
            UserFeelingView userFeelingView = new UserFeelingView();
            var userDetails = _userService.GetUserById(SessionHelper.CurrentSession.Id);
            userFeelingView.CurrentStatus = userDetails.CureentStatus;

            userFeelingView.FeelingList = _userService.GetUserFeelings(LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);

            return PartialView("_AddUserFeeling", userFeelingView);
        }

        public ActionResult GetUserFeelingsById()
        {
            var userFeelings = _userService.GetUserFeelings(LocalizationHelper.CurrentSystemLanguage.SystemLanguageId);
            var userDetails = _userService.GetUserById(SessionHelper.CurrentSession.Id);
            UserFeelingView objFellingView = new UserFeelingView();
            var UserFeel = userFeelings.Where(e => e.FeelId == userDetails.FeelId).FirstOrDefault();
            objFellingView.FeelId = userDetails.FeelId;
            objFellingView.FeelingType = UserFeel == null ? "" : UserFeel.FeelingType;
            objFellingView.Logo = UserFeel == null ? "" : UserFeel.Logo;
            objFellingView.UserId = userDetails.Id;
            objFellingView.UserTypeId = userDetails.UserTypeId;
            objFellingView.CurrentStatus = UserFeel == null ? "" : UserFeel.CurrentStatus;

            return Json(objFellingView, JsonRequestBehavior.AllowGet);
        }
        #endregion
        [HttpGet]
        public ActionResult LoadProfileAvatar()
        {
            var userProfileView = new UserProfileView();
            userProfileView.UserProfileList = _userService.GetProfileAvatars();
            return PartialView("_ListProfileAvatar", userProfileView);
        }

        public ActionResult SendErrorLog(string type)
        {
            ErrorLogger objErrorLogger = new ErrorLogger();
            List<VLEFileType> _fileTypeList = _fileService.GetFileTypes().ToList();
            if (type.Equals("I", StringComparison.OrdinalIgnoreCase))
            {
                ViewBag.FileExtension = _fileTypeList.Select(r => r.Extension).ToList();
                ViewBag.ReportIssuesType = new SelectList(SelectListHelper.GetSelectListData(ListItems.ReportIssueTypes), "ItemId", "ItemName");
            }

            ViewBag.FormType = type;
            return PartialView("_ErrorLogger");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveErrorLog(ErrorLoggerEdit objErrorLog)
        {
            if (!ModelState.IsValid)
            {
                return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.ValidationFailedMessage), JsonRequestBehavior.AllowGet);
            }

            objErrorLog.UserId = SessionHelper.CurrentSession.Id;
            objErrorLog.ModuleUrl = SessionHelper.CurrentSession.CurrentModuleURL;
            objErrorLog.UserIpAddress = Request.UserHostAddress;
            objErrorLog.WebServerIpAddress = Request.ServerVariables["LOCAL_ADDR"];
            if (Session["ErrorFiles"] is null)
            {
                objErrorLog.lstTaskFiles = new List<TaskFile>();
            }
            else
            {
                objErrorLog.lstTaskFiles = Session["ErrorFiles"] as List<TaskFile>;
            }

            Session["ErrorFiles"] = null;
            var sourceModel = new ErrorLogger();
            EntityMapper<ErrorLoggerEdit, ErrorLogger>.Map(objErrorLog, sourceModel);
            sourceModel.ErrorDateTime = sourceModel.ErrorDateTime.ConvertLocalToUTC().Value;
            bool isSuccess = _userService.SendErrorLog(sourceModel);
            ICommonService _commonService = new CommonService();
            if (isSuccess)
            {
                string _VleErrorReportingMailAddress = ConfigurationManager.AppSettings["ErrorReportingMailAddress"];
                List<DBLogDetails> dbLogs = _userService.GetDBLogDetails().ToList();
                string emailBody = CreateIssueReportEmailTemplate(objErrorLog);

                EmailDetail sendEmailNotificationView = new EmailDetail();
                sendEmailNotificationView.ToEmail = _VleErrorReportingMailAddress;
                sendEmailNotificationView.CCEmail = "";
                sendEmailNotificationView.EmailType = "Phoenix_Classroom_Issue";
                sendEmailNotificationView.Subject = "Phoenix Classroom Issue Report - " + objErrorLog.ReportIssueTitle;
                sendEmailNotificationView.Message = emailBody;
                var operationalDetails = _commonService.SendEmailCirculation(sendEmailNotificationView);
            }
            var op = new OperationDetails(isSuccess);
            if (isSuccess) op.Message = ResourceManager.GetString("Shared.ErrorLogger.IssueSubmittedMessage");
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        private string CreateIssueReportEmailTemplate(ErrorLoggerEdit model)
        {
            string emailBody = "";
            string imagesTag = "";
            StreamReader reader = new StreamReader(Server.MapPath(Constants.IssueReportEmailTemplate), Encoding.UTF8);
            emailBody = reader.ReadToEnd();
            emailBody = emailBody.Replace("{Name}", SessionHelper.CurrentSession.FullName);
            emailBody = emailBody.Replace("{UserName}", SessionHelper.CurrentSession.UserName);
            emailBody = emailBody.Replace("{UserType}", SessionHelper.CurrentSession.UserTypeName);
            emailBody = emailBody.Replace("{SchoolName}", SessionHelper.CurrentSession.SchoolName);
            emailBody = emailBody.Replace("{Server}", model.WebServerIpAddress);
            emailBody = emailBody.Replace("{UserIpAddress}", model.UserIpAddress);
            emailBody = emailBody.Replace("{UserComment}", model.ErrorMessage);
            emailBody = emailBody.Replace("{ErrorType}", model.ReportIssueTitle);
            emailBody = emailBody.Replace("{ErrorOccuredOn}", model.ErrorDateTime.ToString("dd-MMM-yyyy hh:mm tt") + " UTC time");
            emailBody = emailBody.Replace("{ErrorFrequency}", model.ErrorFrequency);


            var imageExtensions = _fileService.GetFileTypes().Where(e => e.DocumentType == "Image").ToList();
            foreach (var item in model.lstTaskFiles)
            {

                var fileExt = Path.GetExtension(item.FileName).Replace(".", "");

                if (imageExtensions.Any(e => e.Extension == fileExt))
                {
                    imagesTag += "<a href=\"" + item.ShareableLinkFileURL + " \">"
                              + " <img src =\" " + item.ShareableLinkFileURL + "\">"
                              + " </a>";
                }
                else
                {
                    imagesTag += $"<br/><a href='{item.ShareableLinkFileURL}'>View Attachment</a>";
                }
            }
            emailBody = emailBody.Replace("{Images}", imagesTag);

            if (SessionHelper.CurrentSession.IsParent())
            {
                emailBody = emailBody.Replace("{ModeOfContact}", $"<strong>Mode Of Contact: </strong> {model.ModeOfContact}");
                emailBody = emailBody.Replace("{ContactDetail}", $"<strong>Contact Detail: </strong> {model.ContactDetail}");
            }
            else
            {
                emailBody = emailBody.Replace("{ModeOfContact}", "");
                emailBody = emailBody.Replace("{ContactDetail}", "");
            }


            return emailBody;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveUserFeedback(UserFeedback model)
        {
            model.CreatedBy = SessionHelper.CurrentSession.Id;
            bool result = await _userService.SaveUserFeedback(model);
            //if (result)
            //{

            //    var userIpAddress = Request.UserHostAddress;
            //    var webServerIpAddress = Request.ServerVariables["LOCAL_ADDR"];
            //    string _VleErrorReportingMailAddress = ConfigurationManager.AppSettings["FeedbackReportingMailAddress"];
            //    List<DBLogDetails> dbLogs = _userService.GetDBLogDetails().ToList();
            //    string sreverStatus = "<br/><u><strong>User Name :" + SessionHelper.CurrentSession.UserName + "</strong></u>";
            //    sreverStatus += "<br/><u><strong>Server :" + webServerIpAddress + "</strong></u>";
            //    sreverStatus += "<br/><u><strong>User IP Address : " + userIpAddress + "</strong></u>";
            //    sreverStatus += "<br/><u><strong>Experience : " + model.FeedbackRating + "</strong></u>";
            //    if (!string.IsNullOrEmpty(model.Description))
            //        sreverStatus += "<br/><u><strong>Description : " + model.Description + "</strong></u>";
            //    sreverStatus += GetWebServerStatus();
            //    sreverStatus += GetDBProcessDetails(dbLogs);
            //    SendMailView sendEmailNotificationView = new SendMailView();
            //    sendEmailNotificationView.FROM_EMAIL = SessionHelper.CurrentSession.Email;
            //    sendEmailNotificationView.TO_EMAIL = _VleErrorReportingMailAddress;
            //    sendEmailNotificationView.LOG_TYPE = "Phoenix Classroom Feedback";
            //    sendEmailNotificationView.SUBJECT = "Phoenix Classroom Feedback";
            //    sendEmailNotificationView.MSG = sreverStatus;
            //    sendEmailNotificationView.LOG_USERNAME = SessionHelper.CurrentSession.UserName;
            //    sendEmailNotificationView.STU_ID = "11111";
            //    ICommonService _commonService = new CommonService();
            //    var operationalDetails = _commonService.SendEmail(sendEmailNotificationView);
            //}
            var op = new OperationDetails(result);
            if (result)
                op.Message = ResourceManager.GetString("Shared.ErrorLogger.FeedbackSubmittedMessage");
            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> UploadErrorScreenShot()
        {
            List<TaskFile> fileNames = new List<TaskFile>();
            string relativePath = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    // Get all files from Request object
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        var uploadedFile = await UploadSharePointFile(file);
                        fileNames.Add(new TaskFile
                        {
                            FileName = uploadedFile.ActualFileName,
                            UploadedFileName = uploadedFile.SharepointUploadedFileURL,
                            ShareableLinkFileURL = uploadedFile.SharepointUploadedFileURL

                        });
                    }
                }
                catch (Exception ex)
                {
                }
                if (Session["ErrorFiles"] != null)
                {
                    List<TaskFile> lstTaskFiles = Session["ErrorFiles"] as List<TaskFile>;
                    lstTaskFiles.AddRange(fileNames);
                }
                else
                {
                    Session["ErrorFiles"] = fileNames;
                }


            }
            return Json(new OperationDetails(true), JsonRequestBehavior.AllowGet);
        }

        private async Task<SharePointFileView> UploadSharePointFile(HttpPostedFileBase file)
        {
            SharePointFileView fileDetails = new SharePointFileView();
            string sharePTUploadedFilePath = string.Empty;
            try
            {
                if (Phoenix.VLE.Web.Helpers.FunctionalConfig.IsAzureBlobStorage)
                {
                    AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
                    await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
                    fileDetails = await azureHelper.UploadStudentFilesAsPerModuleAsync(FileModulesConstants.ErrorLog, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                }
                else
                {
                    SharePointHelper sh = new SharePointHelper();
                    Microsoft.SharePoint.Client.ClientContext cx = sh.LoginToSharepoint(SessionHelper.CurrentSession.SchoolCode);
                    fileDetails = SharePointHelper.UploadStudentFilesAsPerModule(ref cx, FileModulesConstants.ErrorLog, SessionHelper.CurrentSession.OldUserId.ToString(), file);
                }
            }
            catch (Exception ex)
            {
                return fileDetails;
            }
            return fileDetails;
        }
        #region RSSFeed
        //public ActionResult RssFeedAddEdit(int? id)
        //{
        //    RSSFeedEdit model = new RSSFeedEdit();
        //    if (id.HasValue)
        //    {
        //        //var userRole = _userRoleService.GetUserRoleById(id.Value);
        //        //EntityMapper<UserRole, UserRoleEdit>.Map(userRole, model);
        //    }
        //    else
        //    {

        //    }

        //    return PartialView("_AddEditRSSFeed", model);
        //}
        //added for rssfeed
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult SaveRSSFeed(RSSFeedEdit objRSSFeed)
        //{
        //    //return RedirectToAction("SaveRSSFeed", "RSSFeed", objRSSFeed);
        //    try
        //    {
        //        var result = _rssFeedServices.AddUpdateRSSFeed(objRSSFeed);
        //        return Json(new OperationDetails(result), JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.StackTrace);

        //    }
        //    finally
        //    {

        //    }

        //}

        //[HttpGet]
        //public ActionResult GetRSSFeedList()
        //{
        //    List<RSSFeedEdit> objRSSFeedList = new List<RSSFeedEdit>();
        //    foreach (var rssFeedList in _rssFeedServices.GetAllRSSFeed())
        //    {
        //        objRSSFeedList.Add(new RSSFeedEdit
        //        {
        //            Id = rssFeedList.Id,
        //            RSSTitle = rssFeedList.RSSTitle,
        //            RSSLink = rssFeedList.RSSLink,

        //            RSSDescription = rssFeedList.RSSDescription



        //        });


        //    }
        //    return Json(objRSSFeedList, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult GetRSSFeedById(int Id = 0)
        //{
        //    var objGetRSSFeed = _rssFeedServices.GetRSSFeedById(Id);

        //    return View();
        //}
        #endregion

        #region UserStatus
        [HttpGet]
        public ActionResult UserStatus()
        {
            UserFeelingView userFeelingView = new UserFeelingView();
            var userDetails = _userService.GetUserById(SessionHelper.CurrentSession.Id);
            userFeelingView.CurrentStatus = userDetails.CureentStatus;
            userFeelingView.UserId = userDetails.Id;
            userFeelingView.FeelId = userDetails.FeelId;
            userFeelingView.FeelingType = userDetails.FeelType;
            return PartialView("_AddUserStatus", userFeelingView);
        }

        public ActionResult SaveUserStatus(UserFeelingView objUserFeelingStatusView)
        {
            var result = _userService.UpdateUserFeeling(objUserFeelingStatusView);
            var userDetails = _userService.GetUserById(SessionHelper.CurrentSession.Id);
            return Json(userDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetUserStatus()
        {
            UserFeelingView userFeelingView = new UserFeelingView();
            var userDetails = _userService.GetUserById(SessionHelper.CurrentSession.Id);
            userFeelingView.CurrentStatus = userDetails.CureentStatus;
            userFeelingView.UserId = userDetails.Id;
            userFeelingView.FeelId = userDetails.FeelId;
            userFeelingView.FeelingType = userDetails.FeelType;
            return Json(userFeelingView, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SchoolGroups(int pageIndex = 1)
        {
            var teacherlist = _teacherDashboardService.GetTeacherDashboard();
            var objPage = new Pagination<SchoolGroup>(pageIndex, 5, teacherlist.ClassGroups, 11);
            //studentId = "99327";//use for test only use decrept for syudentId.
            return View(objPage);
        }

        #endregion
        #region UserProfile
        [HttpPost]
        public ActionResult SaveUserProfile(UserProfileView userProfile, HttpPostedFileBase ProfilePhoto)
        {
            userProfile.UserId = Convert.ToInt32(SessionHelper.CurrentSession.Id);
            userProfile.ProfilePhoto = userProfile.ProfilePhoto == "undefined" ? null : userProfile.ProfilePhoto;
            if (ProfilePhoto != null)
            {
                userProfile.ProfilePhoto = UploadProfilePhoto(ProfilePhoto);
            }
            else
            {

                Session["UpdatedAvatarName"] = userProfile.AvatarLogo;

            }
            var result = _userService.UpdateUserProfile(userProfile);
            var loginSetting = Phoenix.VLE.Web.Helpers.CommonHelper.GetUserFeeling();
            return PartialView("_ProfilePartial", loginSetting);
        }

        private string UploadProfilePhoto(HttpPostedFileBase ProfilePhoto)
        {
            //string strFile = SessionHelper.CurrentSession.UserName + Path.GetExtension(ProfilePhoto.FileName);
            string GUID = Guid.NewGuid().ToString();
            string strFile = String.Format("{0}{1}", GUID, Path.GetExtension(ProfilePhoto.FileName));
            // string filePath = SessionHelper.CurrentSession.IsParent()? Constants.ParentProfilePath: Constants.TeacherProfilePath;

            string resourceDir = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir;
            string content = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir + "/Content";
            string profilePhoto = PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir + "/Content" + "/VLE" + "/ProfilePhotos";

            //string resourceDir =  Constants.ResourcesDir;
            //string content =  Constants.ResourcesDir + "/Content";
            //string profilePhoto =  Constants.ResourcesDir + "/Content" + "/ProfilePhotos";

            string filePath = SessionHelper.CurrentSession.IsParent() ? PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir + "/Content" + "/ProfilePhotos" + "/Parents/" : PhoenixConfiguration.Instance.WriteFilePath + Constants.ResourcesDir + "/Content" + "/ProfilePhotos" + "/Teachers/";
            //string filePath = SessionHelper.CurrentSession.IsParent() ? Constants.ResourcesDir + "/Content" + "/ProfilePhotos" + "/Parents/" : Constants.ResourcesDir + "/Content" + "/ProfilePhotos" + "/Teachers/";


            Common.Helpers.CommonHelper.CreateDestinationFolder(resourceDir);
            Common.Helpers.CommonHelper.CreateDestinationFolder(content);
            Common.Helpers.CommonHelper.CreateDestinationFolder(profilePhoto);
            Common.Helpers.CommonHelper.CreateDestinationFolder(filePath);
            // Common.Helpers.CommonHelper.CreateDestinationFolder(Teachers);


            //bool exists = System.IO.Directory.Exists(Server.MapPath(filePath));
            //if (!exists)
            //    System.IO.Directory.CreateDirectory(Server.MapPath(filePath));

            // filePath += strFile;
            filePath = Path.Combine(filePath, strFile);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            ProfilePhoto.SaveAs(filePath);
            return strFile;
        }
        #endregion

        #region Performance Log
        private string GetAllProcessStatus()
        {
            string ServerStatus = "";
            string ProcessStatus = "";
            ServerStatus = "<table border=\"1\"><tr><td>Process Name</td><td>Memory used (MB)</td><td>CPU Usage (%)</td></tr>";
            try
            {
                System.Diagnostics.Process[] MyProcess;
                DataTable processDT = new DataTable();
                processDT.Columns.Add("ProcessName", typeof(string));
                processDT.Columns.Add("MemoryUsed", typeof(decimal));
                processDT.Columns.Add("CPUUsage", typeof(decimal));
                MyProcess = System.Diagnostics.Process.GetProcesses();
                foreach (System.Diagnostics.Process p in MyProcess)
                {
                    ProcessStatus = "";
                    System.Diagnostics.PerformanceCounter pc = new System.Diagnostics.PerformanceCounter();
                    pc.CategoryName = "Process";
                    pc.CounterName = "Virtual Bytes";
                    pc.InstanceName = p.ProcessName;
                    DataRow mRow;
                    mRow = processDT.NewRow();
                    mRow["ProcessName"] = p.ProcessName;
                    mRow["MemoryUsed"] = (pc.RawValue / (double)(1024 * 1024));
                    mRow["CPUUsage"] = GetCPUPercentage(pc);
                    processDT.Rows.Add(mRow);
                }

                foreach (var iRow in processDT.Select("", "MemoryUsed"))
                {
                    ProcessStatus = "<tr><td>" + iRow["ProcessName"] + "</td><td>" + iRow["MemoryUsed"] + "</td><td>" + iRow["CPUUsage"] + "</td></tr>";
                    if (!ServerStatus.Contains(ProcessStatus))
                        ServerStatus += ProcessStatus;
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                ServerStatus += "</table>";

            }
            return ServerStatus;
        }

        private string GetServerPerformanceCounters()
        {
            var oPerfCounter = new System.Diagnostics.PerformanceCounter();
            string serverStatus;
            // ''''''''''''''''''''''''''''''PROCESSOR''''''''''''''''''''''''''''
            serverStatus = "<br /><u><strong> Processor Status</strong></u><br />";
            oPerfCounter.CategoryName = "Processor";
            oPerfCounter.CounterName = "% Processor Time";
            oPerfCounter.InstanceName = "_Total";
            serverStatus += "CPU Usage :  " + oPerfCounter.NextValue().ToString() + "%" + "<br />";



            oPerfCounter.CategoryName = "Processor";
            oPerfCounter.CounterName = "% Privileged Time";
            oPerfCounter.InstanceName = "_Total";
            serverStatus += "Privileged Time :  " + oPerfCounter.NextValue().ToString() + "<br />";




            oPerfCounter.CategoryName = "Processor";
            oPerfCounter.CounterName = "% Interrupt Time";
            oPerfCounter.InstanceName = "_Total";
            serverStatus += "Interrupt Time :  " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "System";
            oPerfCounter.CounterName = "Processor Queue Length";
            oPerfCounter.InstanceName = ""; // "_Total"
            serverStatus += "Processor Queue Length: " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "System";
            oPerfCounter.CounterName = "Context Switches/sec";
            oPerfCounter.InstanceName = ""; // "_Total"
            serverStatus += "Context Switches/sec :  " + oPerfCounter.NextValue().ToString() + " per sec " + "<br />";



            // ''''''''''''''''''''''''''''''MEMORY''''''''''''''''''''''''''''
            serverStatus += "<u><strong> Memory Status</strong></u><br />";
            long bytes = GC.GetTotalMemory(false);
            serverStatus += "Total Memory Used : " + bytes.ToString() + " Bytes" + "<br />";




            oPerfCounter.CategoryName = "Memory";
            oPerfCounter.CounterName = "Available Mbytes";
            oPerfCounter.InstanceName = "";
            serverStatus += "Available Mbytes :  " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "Memory";
            oPerfCounter.CounterName = "Page Reads/sec";
            oPerfCounter.InstanceName = "";
            serverStatus += "Page Reads/sec :  " + oPerfCounter.NextValue().ToString() + "<br />";




            oPerfCounter.CategoryName = "Memory";
            oPerfCounter.CounterName = "Pages/sec";
            oPerfCounter.InstanceName = "";
            serverStatus += "Pages/sec :  " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "Memory";
            oPerfCounter.CounterName = "Pool Nonpaged Bytes";
            oPerfCounter.InstanceName = ""; // "_Total"
            serverStatus += "Pool Nonpaged Bytes :  " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "Memory";
            oPerfCounter.CounterName = "Cache Bytes";
            oPerfCounter.InstanceName = ""; // "_Total"
            serverStatus += "Cache Bytes :  " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "Memory";
            oPerfCounter.CounterName = "Cache Faults/sec";
            oPerfCounter.InstanceName = ""; // "_Total"
            serverStatus += "Cache Faults/sec :  " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "Cache";
            oPerfCounter.CounterName = "MDL Read Hits %";
            oPerfCounter.InstanceName = ""; // "_Total"
            serverStatus += "MDL Read Hits % :  " + oPerfCounter.NextValue().ToString() + " %" + "<br />";



            // ''''''''''''''''''''''''''''''Disk I/O''''''''''''''''''''''''''''
            serverStatus += "<u><strong>Disk I/O</strong></u><br />";
            oPerfCounter.CategoryName = "PhysicalDisk";
            oPerfCounter.CounterName = "Avg. Disk Queue Length";
            oPerfCounter.InstanceName = "_Total";
            serverStatus += "Avg. Disk Queue Length :  " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "PhysicalDisk";
            oPerfCounter.CounterName = "Avg. Disk Read Queue Length";
            oPerfCounter.InstanceName = "_Total";
            serverStatus += "Avg. Disk Read Queue Length :  " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "PhysicalDisk";
            oPerfCounter.CounterName = "Avg. Disk Write Queue Length";
            oPerfCounter.InstanceName = "_Total";
            serverStatus += "Avg. Disk Write Queue Length :  " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "PhysicalDisk";
            oPerfCounter.CounterName = "Avg. Disk sec/Read";
            oPerfCounter.InstanceName = "_Total";
            serverStatus += "Avg. Disk sec/Read :  " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "PhysicalDisk";
            oPerfCounter.CounterName = "Avg. Disk sec/Transfer";
            oPerfCounter.InstanceName = "_Total";
            serverStatus += "Avg. Disk sec/Transfer :  " + oPerfCounter.NextValue().ToString() + "<br />";



            oPerfCounter.CategoryName = "PhysicalDisk";
            oPerfCounter.CounterName = "Disk Writes/sec";
            oPerfCounter.InstanceName = "_Total";
            serverStatus += "Disk Writes/sec :  " + oPerfCounter.NextValue().ToString() + "<br />";



            // ''''''''''''''''''''''''''''''Network I/O''''''''''''''''''''''''''''
            serverStatus += "<u><strong>Network I/O</strong></u><br />";
            System.Diagnostics.PerformanceCounterCategory pc = new System.Diagnostics.PerformanceCounterCategory("Network Interface");

            foreach (var instance in pc.GetInstanceNames())
            {
                serverStatus += "<u><strong>" + instance + "</strong></u><br />";
                oPerfCounter.CategoryName = "Network Interface";
                oPerfCounter.CounterName = "Bytes Total/sec";
                oPerfCounter.InstanceName = instance;
                serverStatus += "Bytes Total/sec :  " + oPerfCounter.NextValue().ToString() + "<br />";



                oPerfCounter.CategoryName = "Network Interface";
                oPerfCounter.CounterName = "Bytes Received/sec";
                oPerfCounter.InstanceName = instance;
                serverStatus += "Bytes Received/sec :  " + oPerfCounter.NextValue().ToString() + "<br />";



                oPerfCounter.CategoryName = "Network Interface";
                oPerfCounter.CounterName = "Bytes Sent/sec";
                oPerfCounter.InstanceName = instance;
                serverStatus += "Bytes Sent/sec :  " + oPerfCounter.NextValue().ToString() + "<br />";
            }
            return serverStatus;

        }


        private string GetCPUPercentage(System.Diagnostics.PerformanceCounter pc)
        {
            string CPUUsage = "";
            try
            {
                pc.CategoryName = "Process";
                pc.CounterName = "% Processor Time";
                pc.NextValue();
                CPUUsage = pc.NextValue().ToString();
            }
            catch
            {
            }
            finally
            {

            }
            return CPUUsage;
        }

        private string GetWebServerStatus()
        {
            string ServerStatus = "";
            try
            {
                try
                {
                    ServerStatus += GetServerPerformanceCounters();
                }
                catch (Exception ex)
                {

                }

                try
                {
                    ServerStatus += " <br /><a style=\"font-weight: bold; text-decoration: underline\"> All process Memory Usage </a><br />";
                    //ServerStatus += " <br /><a style=\"font-weight: bold; text-decoration: underline\"> All process Memory Usage </a><br />" + GetAllProcessStatus() + ",";
                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            return ServerStatus;
        }

        private string GetDBProcessDetails(List<DBLogDetails> dbLogs)
        {
            string DBStatus = "";
            DBStatus = "<table border=\"1\"><tr><th>SPID</th><th>BLOCKBY</th><th>ELAPSED_MS</th>"
                      + "<th>CPU</th><th>IOREADS</th><th>IOWRITES</th><th>EXECUTIONS</th>"
                       + "<th>COMMAND_TYPE</th><th>SQL_STATEMENT</th><th>STATUS</th><th>LOGIN</th>"
                      + "<th>HOST</th><th>DBName</th><th>LastWaitType</th><th>StartTime</th>"
                      + "<th>Protocol</th><th>Transaction_Isolation</th><th>ConnectionWrites</th><th>ConnectionReads</th>"
                       + "<th>ClientAddress</th><th>Authentication</th>"
                       + "</tr>";
            foreach (var item in dbLogs)
            {
                DBStatus += "<tr><th>" + item.SPID + "</th><th>" + item.BlkBy + "</th><th>" + item.ElapsedMS + "</th><th>" + item.CPU + "</th><th>" + item.IOReads + "</th>"
                           + "<th>" + item.IOWrites + "</th><th>" + item.Executions + "</th><th>" + item.CommandType + "</th><th>" + item.ObjectName + "</th>"
                           + "<th>" + item.SQLStatement + "</th><th>" + item.STATUS + "</th><th>" + item.Login + "</th><th>" + item.Host + "</th>"
                           + "<th>" + item.DBName + "</th><th>" + item.LastWaitType + "</th><th>" + item.StartTime + "</th><th>" + item.Protocol + "</th>"
                           + "<th>" + item.transaction_isolation + "</th><th>" + item.ConnectionWrites + "</th><th>" + item.ConnectionReads + "</th><th>" + item.ClientAddress + "</th>"
                           + "<th>" + item.Authentication + "</th>";
            }
            DBStatus += "</table>";
            return DBStatus;
        }
        #endregion 
        #region UserNotification
        [HttpGet]
        public ActionResult Notifications()
        {
            return View();
        }
        public ActionResult AllNotifications()
        {
            string studentNumber = string.Empty;
            string Key = string.Empty, Identifier = string.Empty, Source = string.Empty, Type = string.Empty;
            Key = GetUserNotificationIdentifiers(out Identifier);
            Source = "CLASSROOM";
            Type = "";

            var allNotifications = _notificationService.GetAllNotifications(Key, Identifier, Source, Type);
            TempData["notificationData"] = allNotifications;
            return PartialView("_UserNotificationsPartial", allNotifications);
        }

        private string GetUserNotificationIdentifiers(out string identifier)
        {
            string _key = string.Empty;
            if (SessionHelper.CurrentSession.IsParent())
            {
                _key = "PARENT";
                identifier = SessionHelper.CurrentSession.UserName;
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                _key = "STUDENT";
                identifier = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
            }
            else if (SessionHelper.CurrentSession.IsTeacher() || SessionHelper.CurrentSession.IsAdmin)
            {
                _key = "STAFF";
                identifier = Convert.ToString(SessionHelper.CurrentSession.UserName);
            }
            else identifier = string.Empty;
            return _key;
        }

        [HttpPost]
        public ActionResult PushNotificationLogs(string notifiactionType, string sourceId)
        {
            string studentNumber = string.Empty;
            string Key = string.Empty, Identifier = string.Empty, Source = string.Empty, Type = string.Empty;

            int notificationId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(sourceId));
            IEnumerable<UserNotificationView> lstUserNotifications = new List<UserNotificationView>();
            long userId = SessionHelper.CurrentSession.Id;
            var result = _userService.PushNotificationLogs(notifiactionType, notificationId, userId);

            if (SessionHelper.CurrentSession.IsParent())
            {
                Identifier = SessionHelper.CurrentSession.UserName;
                Key = "PARENT";
            }
            else if (SessionHelper.CurrentSession.IsStudent())
            {
                Identifier = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                Key = "STUDENT";
            }
            else if (SessionHelper.CurrentSession.IsTeacher())
            {
                Identifier = SessionHelper.CurrentSession.UserName;
                Key = "STAFF";
            }

            Source = "CLASSROOM";
            Type = "";

            var allNotifications = _notificationService.GetAllNotifications(Key, Identifier, Source, Type);
            var unreadNotifications = allNotifications.Where(x => x.IsRead == false).ToList();

            return PartialView("_UserNotification", unreadNotifications);
        }
        public ActionResult LoadNotificationPopup()
        {
            string studentNumber = string.Empty;
            string Key = string.Empty, Identifier = string.Empty, Source = string.Empty, Type = string.Empty;
            if (TempData["notificationData"] == null)
            {
                if (SessionHelper.CurrentSession.IsParent())
                {
                    Identifier = SessionHelper.CurrentSession.UserName;
                    Key = "PARENT";
                }
                else if (SessionHelper.CurrentSession.IsStudent())
                {
                    Identifier = Convert.ToString(SessionHelper.CurrentSession.OldUserId);
                    Key = "STUDENT";
                }

                Source = "CLASSROOM";
                Type = "";

                var allNotifications = _notificationService.GetAllNotifications(Key, Identifier, Source, Type);
                var unreadNotifications = allNotifications.Where(x => x.IsRead == false).ToList();
                return PartialView("_LoadNotificationPopup", unreadNotifications);
            }
            else
            {
                var model = TempData["notificationData"] as List<StudentNotification>;
                return PartialView("_LoadNotificationPopup", model);
            }
        }

        [HttpPost]
        public ActionResult CheckIfTheResourceDeleted(string sourceId, string notificationType, bool isTeacherNotification)
        {
            bool isResourceDeleted = false;
            int _sourceId = 0;
            if(!string.IsNullOrEmpty(sourceId))
            {
                if (notificationType == "MGRP" || notificationType == "CHAT")
                {
                    _sourceId = (int)Helpers.CommonHelper.GetDecryptedIdFromEncryptedString(sourceId);
                }
                else if (notificationType == "ASN" || notificationType == "TSK")
                {
                    _sourceId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(sourceId));
                }

                var result = _studentService.CheckIfTheResourceDeleted(_sourceId, notificationType);
                if (result > 0)
                    isResourceDeleted = true;
            }

            return Json(new OperationDetails(isResourceDeleted, "", null), JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadStuddentTermsAndConditionPopUp()
        {
            return PartialView("_StuddentPermissionInstructionPopUp");
        }


        public ActionResult LoadTermsAndConditionPopUp()
        {
            return PartialView("_TermsAndConditionPopUp");
        }
        public ActionResult GetPrivacyHtmlPage()
        {
            return View();
        }
        public ActionResult GetTermsAndConditionHtmlPage()
        {
            return View();
        }
        public ActionResult GetElitePrivacyHtmlPage()
        {
            return View();
        }
        public ActionResult GetEliteTermsAndConditionHtmlPage()
        {
            return View();
        }
        public ActionResult AssignPermission()
        {
            string StudentIdList = string.Join(",", SessionHelper.CurrentSession.FamilyStudentList.Select(x => x.StudentId));
            _parentService.AssignAccessPermission(StudentIdList);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public ActionResult IsNotificationExist()
        {
            bool result = false;
            var _userService = new UserService();
            IEnumerable<UserNotificationView> lstUserNotifications = new List<UserNotificationView>();
            lstUserNotifications = _userService.GetUserNotifications();
            if (lstUserNotifications.Any())
                result = true;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Dashboard Data
        public ActionResult GetSchoolGroupWithPagination(short pageIndex)
        {
            return PartialView("_DashboardGroups", GetSchoolGroupList(pageIndex));
        }

        public ActionResult GetStudentDashboardAssignments()
        {
            IEnumerable<Assignment> assignments = new List<Assignment>();
            assignments = _assignmentService.GetDashboardAssignmentOverview(SessionHelper.CurrentSession.Id);
            return PartialView("_AssignmentOverview", assignments);
        }

        private IEnumerable<SchoolGroup> GetSchoolGroupList(int pageIndex)
        {
            IEnumerable<SchoolGroup> schoolGroups = new List<SchoolGroup>();
            schoolGroups = _schoolGroupService.GetSchoolGroupWithPagination(SessionHelper.CurrentSession.Id, 12, pageIndex);
            return schoolGroups;
        }

        public ActionResult GetStudentExemplerWallData(int pageIndex = 1)
        {
            IEnumerable<Blog> _blogs = new List<Blog>();
            _blogs = _blogService.GetUserExemplerWallData(SessionHelper.CurrentSession.Id, pageIndex, 4);
            if (_blogs == null) _blogs = new List<Blog>();
            return PartialView("_ExemplarWalls", _blogs);
        }


        public ActionResult GetStudentChatterBlogs(long? schoolId, long? studentId)
        {
            IEnumerable<BlogStudentDash> _blogs = new List<BlogStudentDash>();
            _blogs = _blogService.GetStudentChatterBlogs(SessionHelper.CurrentSession.SchoolId, SessionHelper.CurrentSession.Id);
            //return PartialView("_ExemplarWalls", _blogs);
            return PartialView("", _blogs);
        }

        public ActionResult GetTeacherChatterBlogs(long? schoolId, long? studentId)
        {
            IEnumerable<BlogTeacherDash> _blogs = new List<BlogTeacherDash>();
            _blogs = _blogService.GetTeacherChatterBlogs(SessionHelper.CurrentSession.SchoolId, SessionHelper.CurrentSession.Id);
            //return PartialView("_ExemplarWalls", _blogs);
            return PartialView("", _blogs);
        }

        public JsonResult GetLeadBoardData(long? schoolId, long? studentId, int? range, int? gradeId, DateTime? date)
        {
            string overAllGroup = "GroupOverall";

            var activity = _studentService.GetStudentTotalActivePoints(studentId.Value);

            int overallPoints = activity.Any(x => x.GroupID.Equals(0)) ? activity.FirstOrDefault(x => x.GroupID.Equals(0)).Points : 0;

            int videoPoints = activity.Any(x => x.GroupID.Equals(1) && x.SummaryType.Equals(overAllGroup)) ?
                activity.FirstOrDefault(x => x.GroupID.Equals(1) && x.SummaryType.Equals(overAllGroup)).Points :
                0;

            int physicalPoints = activity.Any(x => x.GroupID.Equals(2) && x.SummaryType.Equals(overAllGroup)) ?
                activity.FirstOrDefault(x => x.GroupID.Equals(2) && x.SummaryType.Equals(overAllGroup)).Points :
                0;

            int artsPoints = activity.Any(x => x.GroupID.Equals(3) && x.SummaryType.Equals(overAllGroup)) ?
                activity.FirstOrDefault(x => x.GroupID.Equals(3) && x.SummaryType.Equals(overAllGroup)).Points :
                0;
            artsPoints += activity.Any(x => x.GroupID.Equals(4) && x.SummaryType.Equals(overAllGroup)) ?
                activity.FirstOrDefault(x => x.GroupID.Equals(4) && x.SummaryType.Equals(overAllGroup)).Points :
                0;

            var leadBoards = this.GetLeadBoard(schoolId, studentId, range, gradeId, date);

            var relatedHtml = Phoenix.Common.Helpers.Extensions.ControllerExtension.RenderPartialViewToString(this, "_GetLeaderBoard", leadBoards);

            return Json(new
            {
                overallPoints,
                videoPoints,
                physicalPoints,
                artsPoints,
                RelatedHtml = relatedHtml
            }, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        public List<LeadBoard> GetLeadBoard(long? schoolId, long? studentId, int? range, int? gradeId, DateTime? date)
        {
            List<LeadBoard> result = _studentService.GetLeaderBoard(schoolId, studentId, range, gradeId, date).ToList();
            result.ForEach(x => x.RecordType = range.Value);
            if (result.Any())
            {
                var studentDetails = _studentListService.GetStudentDetailsByIds(string.Join(",", result.Select(x => x.StudentID)));
                studentDetails.ToList().ForEach(x =>
                {
                    var leadDetails = result.FirstOrDefault(a => a.StudentID == x.StudentId);
                    if (leadDetails != null)
                    {
                        leadDetails.StudentImage = Constants.StudentImagesPath + x.StudentImage;
                        leadDetails.StudentName = string.Concat($"{x.FirstName} ", string.IsNullOrEmpty(x.MiddleName) ? x.LastName : $"{x.MiddleName} {x.LastName}");
                    }
                });
            }
            else
            {
                result.Add(new LeadBoard { RecordType = range.Value });
            }
            return result;
        }

        #endregion

    }
}
