﻿using Phoenix.Common;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Controllers;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Phoenix.VLE.Web.Extensions;
using System.Net;
using System.Globalization;
using Phoenix.Common.Logger;

namespace Phoenix.VLE.Web.Controllers
{
    public class FileDownlodController : Controller
    {
        // GET: FileDownlod
        
        private readonly ISchoolService _schoolService;
        

        public FileDownlodController(ISchoolService schoolService)
        {
           
            _schoolService = schoolService;
        }
        public ActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> DownloadBlobFile(string FileName)
        {
            AzureBlobStorageHelper azureHelper = new AzureBlobStorageHelper();
            await azureHelper.Connect(SessionHelper.CurrentSession.SchoolCode);
            var blobFileInfo = await azureHelper.DownloadFilesAsStreamAsync(FileName);
            try
            {
                return File(blobFileInfo.Content, blobFileInfo.ContentType, blobFileInfo.FileName.Split('/')[2]);
            }
            catch (Exception ex)
            {
                ILoggerClient _loggerClient = LoggerClient.Instance;
                _loggerClient.LogWarning("Azure Blob Storage Downloading Exception");
                _loggerClient.LogException(ex);
                _loggerClient.LogException(ex.InnerException);
                return File(blobFileInfo.Content, blobFileInfo.ContentType, blobFileInfo.FileName);
            }
        }
        public ActionResult DownloadFile(string id = "", string strType = "")
        {

            var FileType = strType;

            switch ((ModuleName)Enum.Parse(typeof(ModuleName),FileType))
            {

                case ModuleName.Assignment:

                    if (!string.IsNullOrWhiteSpace(id))
                    {
                        var fileId = Convert.ToInt32(id);
                        var file = _schoolService.GetModuleFileDetails(fileId, ModuleName.Assignment.ToString());
                        if (file != null && !string.IsNullOrWhiteSpace(file.FilePath))
                        {
                          
                            string path = PhoenixConfiguration.Instance.ReadFilePath + file.FilePath;

                            WebClient myWebClient = new WebClient();
                            byte[] myDataBuffer = myWebClient.DownloadData(path);
                            Response.BufferOutput = true;

                            return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, file.FileName);
                        }
                    }
                    return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.FileNotFound), JsonRequestBehavior.AllowGet);


                case ModuleName.Files:

                    if (!string.IsNullOrWhiteSpace(id))
                    {
                        var fileId = Convert.ToInt32(EncryptDecryptHelper.DecryptUrl(id));
                        var file = _schoolService.GetModuleFileDetails(fileId, ModuleName.Assignment.ToString());
                        if (file != null && !string.IsNullOrWhiteSpace(file.FilePath))
                        {
                            string path = PhoenixConfiguration.Instance.ReadFilePath + file.FilePath;

                            WebClient myWebClient = new WebClient();
                            byte[] myDataBuffer = myWebClient.DownloadData(path);
                            Response.BufferOutput = true;
                            

                            return File(myDataBuffer, System.Net.Mime.MediaTypeNames.Application.Octet, file.FileName);
                        }
                    }
                    return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.FileNotFound), JsonRequestBehavior.AllowGet);
            }
            return Json(new OperationDetails(false, Common.Localization.LocalizationHelper.FileNotFound), JsonRequestBehavior.AllowGet);

        }


    }
}