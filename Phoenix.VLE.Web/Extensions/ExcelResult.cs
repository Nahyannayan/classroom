﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace System.Web.Mvc
{
    public class ExcelResult : ActionResult
    {
        private readonly DataTable _dtExcel;
        private readonly string _fileName;
        public ExcelResult(DataTable dtExcel, String fileName)
        {
            _dtExcel = dtExcel;
            _fileName = fileName;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("We need a context!");
            }
            var response = context.HttpContext.Response;
            StringWriter strWriter = new StringWriter();
            HtmlTextWriter txtWriter = new HtmlTextWriter(strWriter);

            DataGrid dtGrid = new DataGrid();
            dtGrid.DataSource = _dtExcel;
            dtGrid.HeaderStyle.Font.Bold = true;
            dtGrid.DataBind();
            dtGrid.RenderControl(txtWriter);

            response.ContentType = "application/vnd.ms-excel";
            response.AppendHeader("Content-Disposition", "attachment; filename=" + _fileName + "");
            response.Write(strWriter.ToString());
            response.End();
        }
    }

    public static class ConvertToDataTable
    {
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
        public static DataTable ToDataTable<T>(this IEnumerable<T> data, string dataTableName)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable(dataTableName);
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
        public static DataTable EnumToDataTable<T>(this IEnumerable<T> l_oItems, string dataTableName)
        {
            var firstItem = l_oItems.FirstOrDefault();
            if (firstItem == null)
                return new DataTable(dataTableName);

            var oReturn = new DataTable(dataTableName);
            object[] a_oValues;
            int i;

            var properties = TypeDescriptor.GetProperties(firstItem);

            foreach (PropertyDescriptor property in properties)
                oReturn.Columns.Add(property.Name, BaseType(property.PropertyType));

            //#### Traverse the l_oItems
            foreach (var oItem in l_oItems)
            {
                //#### Collect the a_oValues for this loop
                a_oValues = new object[properties.Count];

                //#### Traverse the a_oProperties, populating each a_oValues as we go
                for (i = 0; i < properties.Count; i++)
                    a_oValues[i] = properties[i].GetValue(oItem);

                //#### .Add the .Row that represents the current a_oValues into our oReturn value
                oReturn.Rows.Add(a_oValues);
            }

            //#### Return the above determined oReturn value to the caller
            return oReturn;
        }
        public static Type BaseType(Type oType)
        {
            //#### If the passed oType is valid, .IsValueType and is logicially nullable, .Get(its)UnderlyingType
            if (oType != null &&
                oType.IsValueType &&
                oType.IsGenericType &&
                oType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                return Nullable.GetUnderlyingType(oType);
            }

            //#### Else the passed oType was null or was not logicially nullable, so simply return the passed oType
            return oType;
        }
        //public class DataRecordDynamicWrapper : DynamicObject, ICustomTypeDescriptor
        //{
        //    private readonly IDataRecord _dataRecord;
        //    private PropertyDescriptorCollection _properties;

        //    public DataRecordDynamicWrapper(IDataRecord dataRecord)
        //    {
        //        _dataRecord = dataRecord;
        //    }

        //    public override bool TryGetMember(GetMemberBinder binder, out object result)
        //    {
        //        result = _dataRecord[binder.Name];
        //        return result != null;
        //    }

        //    ComponentModel.AttributeCollection ICustomTypeDescriptor.GetAttributes()
        //    {
        //        return ComponentModel.AttributeCollection.Empty;
        //    }

        //    string ICustomTypeDescriptor.GetClassName()
        //    {
        //        return _dataRecord.GetType().Name;
        //    }

        //    string ICustomTypeDescriptor.GetComponentName()
        //    {
        //        return _dataRecord.GetType().Name;
        //    }

        //    TypeConverter ICustomTypeDescriptor.GetConverter()
        //    {
        //        return new TypeConverter();
        //    }

        //    EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        //    {
        //        return null;
        //    }

        //    PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        //    {
        //        return null;
        //    }

        //    object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
        //    {
        //        throw new NotSupportedException();
        //    }

        //    EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        //    {
        //        return EventDescriptorCollection.Empty;
        //    }

        //    EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
        //    {
        //        return EventDescriptorCollection.Empty;
        //    }

        //    PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        //    {
        //        if (_properties == null)
        //            _properties = GenerateProperties();
        //        return _properties;
        //    }

        //    private PropertyDescriptorCollection GenerateProperties()
        //    {
        //        var count = _dataRecord.FieldCount;
        //        var properties = new PropertyDescriptor[count];

        //        for (var i = 0; i < count; i++)
        //            properties[i] = new DataRecordProperty(i, _dataRecord.GetName(i), _dataRecord.GetFieldType(i));

        //        return new PropertyDescriptorCollection(properties);
        //    }

        //    PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(Attribute[] attributes)
        //    {
        //        if (attributes != null && attributes.Length == 0)
        //            return ((ICustomTypeDescriptor)this).GetProperties();
        //        return PropertyDescriptorCollection.Empty;
        //    }

        //    object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
        //    {
        //        return _dataRecord;
        //    }

        //    private sealed class DataRecordProperty : PropertyDescriptor
        //    {
        //        private static readonly Attribute[] NoAttributes = new Attribute[0];

        //        private readonly int _ordinal;
        //        private readonly Type _type;

        //        public DataRecordProperty(int ordinal, string name, Type type)
        //            : base(name, NoAttributes)
        //        {
        //            _ordinal = ordinal;
        //            _type = type;
        //        }

        //        public override bool CanResetValue(object component)
        //        {
        //            return false;
        //        }

        //        public override object GetValue(object component)
        //        {
        //            var wrapper = ((DataRecordDynamicWrapper)component);
        //            return wrapper._dataRecord.GetValue(_ordinal);
        //        }

        //        public override void ResetValue(object component)
        //        {
        //            throw new NotSupportedException();
        //        }

        //        public override void SetValue(object component, object value)
        //        {
        //            throw new NotSupportedException();
        //        }

        //        public override bool ShouldSerializeValue(object component)
        //        {
        //            return true;
        //        }

        //        public override Type ComponentType
        //        {
        //            get { return typeof(IDataRecord); }
        //        }

        //        public override bool IsReadOnly
        //        {
        //            get { return true; }
        //        }

        //        public override Type PropertyType
        //        {
        //            get { return _type; }
        //        }
        //    }
        //}
    }
}