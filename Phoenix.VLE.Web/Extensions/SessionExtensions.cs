﻿using Newtonsoft.Json;
using System.Web;


public static class SessionExtensions
{
    public static void SetObject(string key, object value) =>
      HttpContext.Current.Session[key] = JsonConvert.SerializeObject(value);

    public static T GetObject<T>(string key)
    {
        var value = System.Convert.ToString(HttpContext.Current.Session[key]);
        //Ok Crrecvt
        return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
    }

    public static void RemoveObject(string key)
    {
        if (HttpContext.Current.Session[key].ToString() != null)
        {
            HttpContext.Current.Session.Remove(key);
        }
    }
}

