﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Extensions
{
    public static class Paging
    {
        public static PagedData<T> PagedResult<T>(this List<T> list, int PageNumber, int PageSize, int TotalCount = 8) where T : class
        {
            var result = new PagedData<T>();
            result.Data = list.ToList();
            result.TotalPages = Convert.ToInt32(Math.Ceiling((double)TotalCount / PageSize));
            result.CurrentPage = PageNumber;
            return result;
        }
    }
}