﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models.EditModels;

namespace Phoenix.VLE.Web.ViewModels
{
    public class MyFilesViewModel
    {
        public int ModuleId { get; set; }
        public int ParentFolderId { get; set; }
        public int SectionId { get; set; }
        public List<string> FileExtension { get; set; }
        public IEnumerable<FileEdit> Files { get; set; }
        public IEnumerable<FolderEdit> Folders { get; set; }
        public IEnumerable<FolderTree> FolderTree { get; set; }
        public IEnumerable<GroupUrl> GroupUrl { get; set; }
        public IEnumerable<GroupQuiz> GroupQuiz { get; set; }
        public IEnumerable<QuizView> GroupForm { get; set; }
        public IEnumerable<Assignment> Assignments { get; set; }
        public IEnumerable<AssignmentStudent> AssignmentStudents { get; set; }
        public MyFilesViewModel()
        {
            Files = new List<FileEdit>();
            Folders = new List<FolderEdit>();
            FolderTree = new List<FolderTree>();
            FileExtension = new List<string>();
            Forms = new List<QuizView>();
            GroupUrl = new List<GroupUrl>();
            GroupQuiz = new List<GroupQuiz>();
            AssignmentStudents = new List<AssignmentStudent>();
        }
        //forms
        public IEnumerable<QuizView> Forms { get; set; }
        public IEnumerable<Bookmark> lstBookmarks { get; set; }
        public int GroupStudentCount { get; set; }
    }
}