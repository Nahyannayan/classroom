﻿using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phoenix.Models;
using Phoenix.Models.Entities;

namespace Phoenix.VLE.Web.ViewModels
{
    public class YoYoBlogViewModel
    {
        public BlogEdit BlogEdit { get; set; }
        public BlogCommentEdit BlogCommentEdit { get; set; }
        public List<SchoolGroup> GroupsList { get; set; }
        public List<Blog> BlogList { get; set; }
        public List<ExemplarUserDetails> ExemplarList { get; set; }
        public List<Chatter> Chatters { get; set; }
        public int? SchoolGroupId { get; set; }

        public List<string> AllowedFileExtension { get; set; }
        public List<string> AllowedImageExtension { get; set; }

        public List<Chat> RecentChatUserList { get; set; }
        public List<LogInUser> MyContactsList { get; set; }

        public List<Chat> MySearchContactsList { get; set; }

        public List<Student> StudentList { get; set; }

        public List<SchoolLevel> SchoolLevelList { get; set; }
        public List<SchoolDepartment> SchoolDepartmentList { get; set; }
        public List<Course> SchoolCourseList { get; set; }

        public ExemplarWallModel ExemplarWallModel { get; set; }
        public List<RecentUserActivity> RecentUserActivity { get; set; }

        public YoYoBlogViewModel()
        {
            BlogEdit = new BlogEdit();
            BlogCommentEdit = new BlogCommentEdit();
            GroupsList = new List<SchoolGroup>();
            BlogList = new List<Blog>();
            ExemplarList = new List<ExemplarUserDetails>();
            Chatters = new List<Chatter>();
            AllowedFileExtension = new List<string>();
            AllowedImageExtension = new List<string>();
            StudentList = new List<Student>();//deependra
            SchoolLevelList = new List<SchoolLevel>();//deependra
            SchoolDepartmentList = new List<SchoolDepartment>();//deependra
            SchoolCourseList = new List<Course>();//deependra
            RecentChatUserList = new List<Chat>();
            MyContactsList = new List<LogInUser>();
            MySearchContactsList = new List<Chat>();
            ExemplarWallModel = new ExemplarWallModel();
            RecentUserActivity = new List<RecentUserActivity>();
        }
    }
}