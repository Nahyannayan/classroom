﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class GalleryViewModel
    {
        public List<GalleryFile> GalleryFolders { get; set; }
        public List<GalleryFile> GalleryFiles { get; set; }
        public List<int> GalleryFilesItemCount { get; set; }
    }
}