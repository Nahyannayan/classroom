﻿using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class ArchiveAssignmentViewModel
    {
        public IEnumerable<ListItem> UnAssignedMemberList { get; set; }
        public IEnumerable<GroupMemberMapping> AssignedMemberList { get; set; }
    }
}