﻿using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.ViewModels
{
    public class StudentViewModel
    {
        public List<StudentView> StudentList { get; set; }

        public StudentViewModel()
        {
            StudentList = new List<StudentView>();
        }
    }
}
