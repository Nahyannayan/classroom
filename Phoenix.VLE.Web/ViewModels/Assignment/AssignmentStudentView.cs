﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Models;
namespace Phoenix.VLE.Web.ViewModels
{
    public class Pagination<T> {
        public Pagination()
        {
            PageRecords = new List<T>();
        }

        public Pagination(int _pageIndex, int _pageSize,List<T> _pageRecords,int _totalCount)
        {
            //PageIndex = _pageIndex <= 0 ? 1 : _pageIndex;
            //PageSize = _pageSize;
            PageRecords = _pageRecords;
            //TotalPages = (int)Math.Ceiling((decimal)_totalCount / (decimal)_pageSize);
            Pager = new Pager(_totalCount, _pageIndex, _pageSize);
        }
        public int TotalPages { get; set; }
        public int PageIndex { get; set; }
        public int RecordCount { get; set; }
        public Pager Pager { get; set; }
        public List<T> PageRecords { get; set; }
        public string LoadPageRecordsUrl { get; set; }
        public string SearchString { get; set; }
        public string SortBy { get; set; }
        public bool CreatedByMeOnly { get; set; }
    }
    public class Pager
    {
        public Pager(int totalItems, int? page, int pageSize = 6)
        {
            // calculate total, start and end pages
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);
            var currentPage = page != null ? (int)page : 1;
            var startPage = currentPage - 5;
            var endPage = currentPage + 4;
            var startRecord = ((currentPage - 1) * pageSize+1);
            var endRecord = startRecord + (pageSize - 1);
            if (totalItems - ((currentPage - 1) * pageSize)<pageSize)
             endRecord = totalItems - ((currentPage - 1) * pageSize) +((currentPage - 1) * pageSize);
            if (startPage <= 0)
            {
                endPage -= (startPage - 1);
                startPage = 1;
            }
            if (endPage > totalPages)
            {
                endPage = totalPages;
                if (endPage > 10)
                {
                    startPage = endPage - 9;
                }
            }

            TotalItems = totalItems;
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalPages = totalPages;
            StartPage = startPage;
            EndPage = endPage;
            PaginationInformation = string.Format(ResourceManager.GetString("Shared.Labels.ShowingEntries"), currentPage, totalPages, totalItems);
        }

        public int TotalItems { get; private set; }
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public int TotalPages { get; private set; }
        public int StartPage { get; private set; }
        public int EndPage { get; private set; }
        public string PaginationInformation { get; private set; }
    }


}