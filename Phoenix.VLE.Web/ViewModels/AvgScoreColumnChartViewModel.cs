﻿using DevExpress.CodeParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class AvgScoreColumnChartViewModel
    {
        
        public List<PercentageValue> LabelNameValue { get; set; }
        public List<string> categories { get; set; }
    }

    public class PercentageValue
    {
        public string name { get; set; }
        public List<double?> data { get; set; }
    }

    public class StudentCountViewModel
    {
        public string category { get; set; }
        public double value { get; set; }
    }
}