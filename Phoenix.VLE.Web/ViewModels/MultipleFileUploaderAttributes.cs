﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class MultipleFileUploaderAttributes
    {
        public MultipleFileUploaderAttributes()
        {
            SaveFileToDisk = true;
        }
        public string FileUploaderId
        {
            get;
            set;
        }
        public string FileUploaderName
        {
            get;
            set;
        }
        public string FileName
        {
            get;
            set;
        }
        public string FieldName
        {
            get;
            set;
        }
        public int? ExternalCategoryId { get; set; }
        public IDictionary<string, object> Attributes
        {
            get;
            set;
        }
        public string UserFolderPath
        {
            get;
            set;
        }

        public int? RegistrationID
        {
            get;
            set;
        }
        public IEnumerable<FileDataView> FileList { get; set; }
        public bool SaveFileToDisk { get; set; }
    }
}
