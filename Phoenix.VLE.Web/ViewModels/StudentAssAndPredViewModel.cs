﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class StudentAssAndPredViewModel
    {
        public List<StudentPercentageValue> LabelNameValue { get; set; }
        public List<string> categories { get; set; }
    }

    public class StudentPercentageValue
    {
        public string name { get; set; }
        public List<double?> data { get; set; }
    }

    public class PredictedScoreViewModel
    {
        public string category { get; set; }
        public double value { get; set; }
    }
}