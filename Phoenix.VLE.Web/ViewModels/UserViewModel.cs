﻿using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class UserViewModel
    {
        public List<TeacherView> TeacherList { get; set; }
    }
}