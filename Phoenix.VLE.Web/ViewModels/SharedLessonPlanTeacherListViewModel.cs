﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class SharedLessonPlanTeacherListViewModel
    {
        public int PlanSchemeDetailId { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserImageFilePath { get; set; }

    }
}