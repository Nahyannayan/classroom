﻿using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phoenix.Models;

namespace Phoenix.VLE.Web.ViewModels
{
    public class ChatViewModel
    {
        public List<User> RecentUserList { get; set; }
        public List<SchoolGroup> SchoolGroups { get; set; }
        public List<string> SchoolGroupId { get; set; }
        public List<string> AllowedFileExtension { get; set; }
        public List<string> AllowedImageExtension { get; set; }
        public List<Chat> UserList  { get; set; }
        public Chat Chat { get; set; }
        public string ProfileImage { get; set; }
        public List<Chat> MyContactsList { get; set; }
        public List<Chat> GetUserList { get; set; }
        public long ChatNowUserId { get; set; }
        public string ChatNowUserName { get; set; }

        public bool IsGroup { get; set; }

        public List<UserChatPermission> ChatPermissionUser { get; set; }

    }
}