﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public partial class OneDriveFilesView
    {
        [JsonProperty("@odata.context")]
        public Uri OdataContext { get; set; }
        [JsonProperty("createdDateTime")]
        public DateTimeOffset CreatedDateTime { get; set; }

        [JsonProperty("eTag")]
        public string ETag { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("lastModifiedDateTime")]
        public DateTimeOffset LastModifiedDateTime { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("webUrl")]
        public Uri WebUrl { get; set; }

        [JsonProperty("cTag")]
        public string CTag { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }
        [JsonProperty("parentReference")]
        public ParentReference ParentReference { get; set; }

        [JsonProperty("value")]
        public Value[] Value { get; set; }
    }

    public class PackageType
    {
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class ParentReference
    {
        [JsonProperty("driveId")]
        public string DriveId { get; set; }
        [JsonProperty("driveType")]
        public string DriveType { get; set; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("path")]
        public string Path { get; set; }
    }

    public partial class Value
    {
        [JsonProperty("createdDateTime")]
        public DateTimeOffset CreatedDateTime { get; set; }

        [JsonProperty("eTag")]
        public string ETag { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("lastModifiedDateTime")]
        public DateTimeOffset LastModifiedDateTime { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("webUrl")]
        public Uri WebUrl { get; set; }

        [JsonProperty("cTag")]
        public string CTag { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }

        [JsonProperty("createdBy")]
        public OneDriveFileUserInfo CreatedBy { get; set; }

        [JsonProperty("lastModifiedBy")]
        public OneDriveFileUserInfo LastModifiedBy { get; set; }

        [JsonProperty("fileSystemInfo")]
        public OneDriveFileSystemInfo FileSystemInfo { get; set; }

        [JsonProperty("folder", NullValueHandling = NullValueHandling.Ignore)]
        public OneDriveFolder Folder { get; set; }

        [JsonProperty("@microsoft.graph.downloadUrl", NullValueHandling = NullValueHandling.Ignore)]
        public Uri MicrosoftGraphDownloadUrl { get; set; }

        [JsonProperty("file", NullValueHandling = NullValueHandling.Ignore)]
        public OneDriveFileMimeType File { get; set; }
        [JsonProperty("package")]
        public PackageType PackageType { get; set; }
        [JsonProperty("parentReference")]
        public ParentReference ParentInfo { get; set; }
    }

    public partial class OneDriveFileUserInfo
    {
        [JsonProperty("application", NullValueHandling = NullValueHandling.Ignore)]
        public OneDriveApplicationUser Application { get; set; }

        [JsonProperty("user", NullValueHandling = NullValueHandling.Ignore)]
        public OneDriveUserInfo User { get; set; }
    }

    public partial class OneDriveApplicationUser
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }
    }

    public partial class OneDriveUserInfo
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }
    }


    public partial class OneDriveFileMimeType
    {
        [JsonProperty("mimeType")]
        public string MimeType { get; set; }

        [JsonProperty("hashes")]
        public OneDriveFileHashes Hashes { get; set; }
    }

    public partial class OneDriveFileHashes
    {
        [JsonProperty("quickXorHash")]
        public string QuickXorHash { get; set; }
    }

    public partial class OneDriveFileSystemInfo
    {
        [JsonProperty("createdDateTime")]
        public DateTimeOffset CreatedDateTime { get; set; }

        [JsonProperty("lastModifiedDateTime")]
        public DateTimeOffset LastModifiedDateTime { get; set; }
    }

    public partial class OneDriveFolder
    {
        [JsonProperty("childCount")]
        public long ChildCount { get; set; }
    }

}