﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class CloudFileListView
    {
        public string ParentFolderId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public long? Size { get; set; }
        public long? Version { get; set; }
        public DateTime? CreatedTime { get; set; }
        public IList<string> Parents { get; set; }
        public long FolderFilesCount { get; set; }
        public string MimeType { get; set; }
        public bool IsFolder { get; set; }
        public string WebContentLink { get; set; }
        public string WebViewLink { get; set; }
        public string DownloadUrl { get; set; }
        public string ThumbnailLink { get; set; }
        public string FileExtension { get; set; }
        public string IconLink { get; set; }
        public string DriveId { get; set; }
        public short ResourceFileTypeId { get; set; }

    }
}