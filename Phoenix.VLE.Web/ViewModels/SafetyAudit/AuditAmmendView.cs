﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class AuditAmmendView
    {
        public int AuditAmmendID { get; set; }
        public string VersionName { get; set; }
        public IEnumerable<AuditQuestionAnswerView> AuditQuestionAnswerList { get; set; }
    }
}