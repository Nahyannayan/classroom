﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class AuditQuestionAnswerView
    {
        public string QuestionName { get; set; }
        public string QuestionDescription { get; set; }
        public int SequenceNo { get; set; }
        public string Severity { get; set; }
        public int PositiveValue { get; set; }

        public int NegativeValue { get; set; }
        public string ControlProfileName { get; set; }
        public long AuditSectionID { get; set; }
        public long AuditAmmendID { get; set; }
        

    }
}