﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class AuditQuestionView
    {
        public IEnumerable<AuditAmmend> AuditAmmendList { get; set; }
        public IEnumerable<AuditQuestionAnswer> AuditQuestionAnswerList { get; set; }
        public IEnumerable<AuditSection> AuditSectionList { get; set; }
    }
}