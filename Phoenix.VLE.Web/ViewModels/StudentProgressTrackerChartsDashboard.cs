﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class StudentProgressTrackerChartsDashboard
    {
        public StudentProgressTrackerChartsDashboard()
        {
            this.StudentProgressFilter = new StudentProgressFilter();
            this.FilterRes = new FilterRes();
            this.AssWiseColumnChartVMs = new List<AvgScoreColumnChartViewModel>();
            this.AvgScoreColumnChartVMs = new List<AvgScoreColumnChartViewModel>();
            this.StudentCountDonutChartVMs = new List<StudentCountDonutChartViewModel>();
            this.ChartRes = new ChartRes();
        }

        public StudentProgressFilter StudentProgressFilter { get; set; }
        public FilterRes FilterRes { get; set; }
        public List<AvgScoreColumnChartViewModel> AssWiseColumnChartVMs { get; set; }
        public List<AvgScoreColumnChartViewModel> AvgScoreColumnChartVMs { get; set; }
        public List<StudentCountDonutChartViewModel> StudentCountDonutChartVMs { get; set; }
        public ChartRes ChartRes { get; set; }
    }

    
}