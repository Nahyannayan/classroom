﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class HttpPostedFileViewModel
    {

        public  int ContentLength { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public Stream InputStream { get; set; }

    }
}