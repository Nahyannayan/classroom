﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class PlanSchemeViewModel
    {
        public Int64 SchemeId { get; set; }
        public string SchemeName { get; set; }
        public string CreateDate { get; set; }
        public string PlanTemplateName { get; set; }
        public string StringStatus { get; set; }
        public int IntStatus { get; set; }

        public string PreviewActionsUrl { get; set; }
        public string SendForApprovalUrl { get; set; }
        public string EditActionUrl { get; set; }
        public string SharedDocumentUrl { get; set; }
        public string DeletActionUrl { get; set; }
        public string CreatedByName { get; set; }
        public List<Phoenix.Models.Entities.SharedLessonPlanTeacherList> SharedLessonPlanTeacherList { get; set; }
        public bool CanApprovePlan { get; set; }

        public long CreatedById { get; set; }

        public bool IsSharedWithMe { get; set; }
        public bool IsAdminApprovalRequired { get; set; }
    }
}