﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class FileDataView
    {
        public FileDataView()
        {
            FileContent = new byte[0];
        }
        public int RegistrationId { get; set; }
        public long? AttachmentId { get; set; }
        public int CategoryId { get; set; }
        public string AttachmentName { get; set; }
        public bool IsProcessed { get; set; }
        public HttpPostedFileBase AttachmentFile { get; set; }
        public byte[] FileContent { get; set; }
    }
}
