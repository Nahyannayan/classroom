﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.ViewModels
{
    public class ChatUsers
    {
        public long SchoolId { get; set; }
       
        public long GroupId { get; set; }
        public string SelectedStudentIDs { get; set; }
        public string SelectedTeacherIDs { get; set; }
        public List<SelectListItem> GroupList { get; set; }
        public List<Student> StudentList { get; set; }
        public List<User> UserList { get; set; }
       
        public List<SelectListItem> SelectedStudentList { get; set; }
        public List<TeacherList> TeacherList { get; set; }
        public List<SelectListItem> SelectedTeacherList { get; set; }
        public bool UserTypeId { get; set; }
       


        public ChatUsers()
        {
            GroupList = new List<SelectListItem>();
            StudentList = new List<Student>();
            SelectedStudentList = new List<SelectListItem>();
            SelectedTeacherList = new List<SelectListItem>();
        }
    }
}