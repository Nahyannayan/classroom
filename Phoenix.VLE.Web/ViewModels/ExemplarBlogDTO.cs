﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web.ViewModels
{
    public class ExemplarBlogDTO
    {

        public long? ExemplarWallId { get; set; }
        public long? SchoolId { get; set; }
        public string PostTitle { get; set; }
        public string PostDescription { get; set; }
        public string TaggedGroup { get; set; }
        public string TaggedStudent { get; set; }
        public string WinnerStudent { get; set; }
        public bool? IsDepartmentWall { get; set; }
        public bool? IsCourseWall { get; set; }
        public string SchoolLevelId { get; set; }
        public string ReferenceLink { get; set; }
        public string EmbededVideoLink { get; set; }
        public string AdditionalDocPath { get; set; }
        public string ParentSharableLink { get; set; }
        public string FileNames { get; set; }

        public Nullable<long> CreatedBy { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<DateTime> UpdatedOn { get; set; }
        public Nullable<long> DeletedBy { get; set; }
        public Nullable<DateTime> DeletedOn { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<long> UserId { get; set; }

        public int SortOrder { get; set; }

        public int IsPublish { get; set; }

        public int BlogTypeId { get; set; }

        public string DeleteReason { get; set; }
        public long? CourseId { get; set; }


        public HttpFileCollectionBase PostedImage { get; set; }
    
  

    }
}