﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class DevExpressEditorSettings
    {
        public DevExpressEditorSettings()
        {
        }
        public DevExpressEditorSettings(File file)
        {
            string path = PhoenixConfiguration.Instance.WriteFilePath + file.FilePath;
            FileName = file.FileName;
            FilePath = file.FilePath;
            ServerPath = path;
            FileExtension = System.IO.Path.GetExtension(path);
        }
        public DevExpressEditorSettings(File file, bool isEdit, bool isRead)
        {
            string path = PhoenixConfiguration.Instance.WriteFilePath + file.FilePath;
            FileName = file.FileName;
            FilePath = file.FilePath;
            ServerPath = path;
            FileExtension = System.IO.Path.GetExtension(path);
            IsEdit = isEdit;
            IsRead = isRead;
        }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string ServerPath { get; set; }
        public bool IsEdit { get; set; }
        public bool IsRead { get; set; }
        public string FileExtension { get; set; }
        public byte[] byteArray { get; set; }
    }
}