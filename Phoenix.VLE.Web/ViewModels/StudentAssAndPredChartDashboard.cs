﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class StudentAssAndPredChartDashboard
    {
        public StudentAssAndPredChartDashboard()
        {
            this.StudentAssAndPredFilter = new StudentAssAndPredFilter();
            this.StudentFilterRes = new StudentFilterRes();
            this.AvgScoreClassPredColumnChartVMs = new List<StudentAssAndPredViewModel>();
            this.PredictionScoreColumnChartVMs = new List<StudentAssAndPredViewModel>();
            this.AssVsPredColumnChartVMs = new List<StudentAssAndPredViewModel>();
            
            this.StudentCountPieChartVMs = new List<StudentCountPieChartViewModel>();
            this.StudentChartRes = new StudentChartRes();
            this.FileStudents = new List<FilStudent>();
            this.FileStudent = new FilStudent();
            this.AssessmentDataByStudent = new List<AssessmentDataByStudent>();
        }

        public StudentAssAndPredFilter StudentAssAndPredFilter { get; set; }
        public StudentFilterRes StudentFilterRes { get; set; }
        public List<StudentAssAndPredViewModel> AvgScoreClassPredColumnChartVMs { get; set; }
        public List<StudentAssAndPredViewModel> PredictionScoreColumnChartVMs { get; set; }
        public List<StudentAssAndPredViewModel> AssVsPredColumnChartVMs { get; set; }
        public List<StudentCountPieChartViewModel> StudentCountPieChartVMs { get; set; }
        public StudentChartRes StudentChartRes { get; set; }
        public List<FilStudent> FileStudents { get; set; }

        public FilStudent FileStudent { get; set; }
        public List<AssessmentDataByStudent> AssessmentDataByStudent { get; set; }
    }
}