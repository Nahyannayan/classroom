﻿using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class TeacherViewModel
    {
        public TeacherDashboard TeacherDashboard { get; set; }

        public List<StudentNotification> Notifications { get; set; }

        public List<BlogTeacherDash> BlogTeacherDash { get; set; }

        public int? SchoolGroupId { get; set; }
        public int OffSet { get; set; }
        public TeacherViewModel()
        {
            BlogTeacherDash = new List<BlogTeacherDash>();
        }
    }
}