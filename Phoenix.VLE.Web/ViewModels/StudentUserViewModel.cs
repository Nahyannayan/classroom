﻿using Phoenix.Common.Models;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class StudentUserViewModel
    {
        public StudentDashboard StudentDashboard { get; set; }
        public List<StudentNotification> Notifications { get; set; }
        public StudentDetail StudentDetail { get; set; }

        public List<TimeTableEvent> TimeTableEvent { get; set; }

        public List<BlogStudentDash> BlogStudentDash { get; set; }

        public UserProfileView UserFeelingStatus { get; set; }

        public StudentUserViewModel()
        {
            TimeTableEvent = new List<TimeTableEvent>();
            BlogStudentDash = new List<BlogStudentDash>();
        }

        public int TotalActivePoints { get; set; }
        public int OffSet { get; set; }
    }

    public class StudentDashboardBadgesViewModel
    {
        public string UserFeelingLogo { get; set; }
        public string UserFeelingType { get; set; }
        public string UserDisplayName { get; set; }
        public int TotalActivePoints { get; set; }
        public List<SchoolBadge> Badges { get; set; }
    }
}