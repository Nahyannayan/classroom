﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.ViewModels
{
    public class DocumentView
    {
        public List<Document> DocumentList { get; set; }

        public List<Catagory> CatagoryList { get; set; }
    }
}