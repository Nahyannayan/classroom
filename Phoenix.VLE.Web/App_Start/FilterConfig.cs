﻿using System.Web;
using System.Web.Mvc;

namespace Phoenix.VLE.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new AuthenticateAttribute());
        }
    }
}
