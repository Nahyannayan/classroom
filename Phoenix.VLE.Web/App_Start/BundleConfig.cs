﻿using Phoenix.Common.Helpers;
using System.Web;
using System.Web.Optimization;

namespace Phoenix.VLE.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Script files

            //Added on 9th July 2019 by Deepak Singh
            // HSE layout js files
            var topLayoutJsBundleVLE = new ScriptBundle("~/bundles/toplayoutjs");
            topLayoutJsBundleVLE.Transforms.Add(new FileHashVersionBundleTransform());

            topLayoutJsBundleVLE.Include(
                "~/Content/js/jquery-3.3.1.min.js",
                //"~/Scripts/jquery-migrate-3.0.0.min.js",incompatible js  added by TTM
                "~/Content/js/jquery-ui.min.js",
                "~/Content/js/popper.min.js",
                 "~/Content/js/bootstrap.min.js",
                 "~/Content/js/jquery.unobtrusive-ajax.min.js",
                 "~/Content/js/mdb.min.js",
                 "~/Content/VLE/js/addons/bootstrap-select.js",
                 "~/Content/VLE/js/addons/moment-with-locales.js",
                 "~/Content/VLE/js/addons/bootstrap-datetimepicker.min.js",
                 //"~/Content/VLE/js/addons/bootstrap-hijri-datetimepicker.js",
                 "~/Content/VLE/js/addons/jquery.minicolors.min.js",
                "~/Content/js/noty/jquery.noty.min.js",
                "~/Content/VLE/js/addons/datatables.min.js",
                "~/Content/VLE/js/addons/datatables-select.js",
                "~/Content/VLE/js/addons/dataTables.rowOrder.min.js",
                "~/Content/VLE/js/addons/particles.min.js",
                "~/Content/VLE/js/addons/fileinput/fileinput.js",
                "~/Content/VLE/js/addons/fileinput/piexif.min.js",
                "~/Content/VLE/js/addons/fileinput/sortable.min.js",
                "~/Content/VLE/js/addons/fileinput/ar.js",
                "~/Content/VLE/js/addons/fileinput/fas-theme.js",
                "~/Content/VLE/js/addons/fileinput/explorer-fas-theme.js",
                "~/Content/VLE/js/file-uploader-config.js",
                "~/Content/VLE/js/addons/jquery.justifiedGallery.min.js",
                 "~/Content/VLE/js/jquery.mCustomScrollbar.concat.min.js",
                "~/Content/VLE/js/addons/moment.js",
                "~/Content/js/noty/themes/default.js",
                "~/Content/js/noty/layouts/topCenter.js",
                "~/Content/js/noty/layouts/bottomRight.js",
                "~/Content/js/noty/layouts/topRight.js",
                "~/Content/js/common/global.js",
                "~/Content/VLE/js/custom.js",
                "~/Content/js/custom/Users/UserFeelingStatus.js",
                "~/Content/VLE/audiojs/audio.min.js",
                "~/Content/VLE/js/addons/owl.carousel.min.js",
                 "~/Content/VLE/js/all-carousels.js",
                 "~/Content/js/addons/defaults-en_US.js",
                 "~/Content/js/addons/defaults-ar_AR.js");
            bundles.Add(topLayoutJsBundleVLE);


            var calendarBundlejs = new ScriptBundle("~/bundles/calendarBundlejs-student");
            calendarBundlejs.Transforms.Add(new FileHashVersionBundleTransform());
            calendarBundlejs.Include(
                "~/Content/Student/calendar-packages/core/main.js",
                "~/Content/Student/calendar-packages/daygrid/main.js",
                "~/Content/Student/calendar-packages/interaction/main.js",
                 "~/Content/Student/calendar-packages/bootstrap/main.js"
                //, "~/Content/Student/js/calendar.js"
                );
            bundles.Add(calendarBundlejs);

            var calendarBundlejsTeacher = new ScriptBundle("~/bundles/calendarBundlejs-teacher");
            calendarBundlejsTeacher.Transforms.Add(new FileHashVersionBundleTransform());
            calendarBundlejsTeacher.Include(
                "~/Content/VLE/calendar-packages/core/main.js",
                "~/Content/VLE/calendar-packages/daygrid/main.js",
                "~/Content/VLE/calendar-packages/interaction/main.js",
                 "~/Content/VLE/calendar-packages/bootstrap/main.js"
                //, "~/Content/VLE/js/calendar.js"
                );
            bundles.Add(calendarBundlejsTeacher);


            //Added on 9th July 2019 by Deepak Singh
            var bottomLayoutJsBundleVLE = new ScriptBundle("~/bundles/bottomlayoutjs");
            bottomLayoutJsBundleVLE.Transforms.Add(new FileHashVersionBundleTransform());
            bottomLayoutJsBundleVLE.Include(
                "~/Content/js/common/plugins.js",
                "~/Content/js/common/DynamicGrid.js",
                "~/Content/js/common/jquery-pagination.js",
                "~/content/Vle/js/addons/lightgallery-all.min.js",
                "~/content/Vle/js/addons/jquery.mousewheel.min.js",
                "~/Content/js/Main.js",
                 "~/Content/js/scrollpagination.js");
            bundles.Add(bottomLayoutJsBundleVLE);

            //Added on 15th July 2020 by Deepak Singh
            var dataPageLibrariesJsBundleVLE = new ScriptBundle("~/bundles/dataPagelibrariesjs");
            dataPageLibrariesJsBundleVLE.Transforms.Add(new FileHashVersionBundleTransform());
            dataPageLibrariesJsBundleVLE.Include(
                "~/Content/js/jquery.validate.min.js",
                "~/Content/js/jquery.validate.unobtrusive.min.js",
                "~/Content/js/addons/bootstrap-select.js",
                 "~/Content/js/addons/datatables.min.js",
                 "~/Content/js/addons/datatables-select.min.js"
                );
            bundles.Add(dataPageLibrariesJsBundleVLE);

            #region StudentBundles

            var topLayoutStudentJsBundleVLE = new ScriptBundle("~/bundles/toplayoutStudentjs");
            topLayoutStudentJsBundleVLE.Transforms.Add(new FileHashVersionBundleTransform());

            topLayoutStudentJsBundleVLE.Include(
                "~/Content/js/jquery-3.3.1.min.js",
                //"~/Scripts/jquery-migrate-3.0.0.min.js",incompatible js added by TTM
                "~/Content/js/jquery.validate.min.js",
                "~/Content/js/jquery.validate.unobtrusive.min.js",
                 "~/Content/js/jquery.unobtrusive-ajax.min.js",
                "~/Content/js/jquery-ui.min.js",
                "~/Content/js/popper.min.js",
                 "~/Content/js/bootstrap.min.js",
                 "~/Content/js/mdb.min.js",
                 "~/Content/Student/js/addons/bootstrap-select.js",
                 "~/Content/Student/js/addons/moment-with-locales.js",
                  "~/Content/Student/js/addons/moment.js",
                 "~/Content/Student/js/addons/bootstrap-datetimepicker.min.js",
                 //"~/Content/Student/js/addons/bootstrap-hijri-datetimepicker.js",
                 "~/Content/Student/js/addons/jquery.minicolors.min.js",
                "~/Content/js/noty/jquery.noty.min.js",
                "~/content/student/js/addons/datatables.min.js",
                "~/content/student/js/addons/datatables-select.js",
                "~/Content/Student/js/addons/fileinput/fileinput.js",
                "~/Content/Student/js/addons/fileinput/piexif.min.js",
                "~/Content/Student/js/addons/fileinput/sortable.min.js",
                "~/Content/Student/js/addons/fileinput/ar.js",
                "~/Content/Student/js/addons/fileinput/fas-theme.js",
                "~/Content/Student/js/addons/fileinput/explorer-fas-theme.js",
                "~/Content/Student/js/file-uploader-config.js",
                 "~/Content/Student/js/jquery.mCustomScrollbar.concat.min.js",
                 "~/Content/Student/js/addons/jquery.justifiedGallery.min.js",
                 "~/Content/Student/js/addons/owl.carousel.min.js",
                 "~/Content/js/all-carousels.js",
                //"~/Content/js/custom.js",

                "~/Content/js/noty/themes/default.js",
                "~/Content/js/noty/layouts/topCenter.js",
                "~/Content/js/noty/layouts/bottomRight.js",
                "~/Content/js/noty/layouts/topRight.js",
                //"~/Content/js/common/paginate.js",
                "~/Content/Student/js/custom.js",
                "~/Content/js/common/global.js");
            bundles.Add(topLayoutStudentJsBundleVLE);

            #endregion

            //Added on 20 SEP 2020 by Hatim
            var loginJsBundle = new ScriptBundle("~/bundles/loginjs");
            loginJsBundle.Transforms.Add(new FileHashVersionBundleTransform());
            loginJsBundle.Include(
                "~/Content/VLE/js/jquery-3.3.1.min.js",
                "~/Content/VLE/js/popper.min.js",
                "~/Content/VLE/js/bootstrap.min.js",
                "~/Content/VLE/js/mdb.min.js",
                "~/Scripts/jquery.validate.js"
                );
            bundles.Add(loginJsBundle);

            #endregion

            #region Style files

            bundles.Add(new StyleBundle("~/Content/materialcss").Include(
                  "~/Content/css/main.css",
                  "~/Content/css/custom/loader.css"));

            // VLE CSS
            bundles.Add(new StyleBundle("~/Content/bootstrapcore").Include(
                    //commented for multilingual, added in _layout//"~/Content/VLE/css/bootstrap.min.css",
                    "~/Content/VLE/css/addons/bootstrap-datetimepicker-standalone.css",
                    "~/Content/VLE/css/addons/bootstrap-datetimepicker.css",
                   "~/Content/VLE/css/addons/jquery.minicolors.css"
                  //"~/Content/VLE/css/addons/filetype-icons.css",
                  //"~/Content/VLE/css/addons/menu-icon.css"
                  ));

            bundles.Add(new StyleBundle("~/Content/datatablecss").Include(
                   "~/Content/css/addons/datatables.min.css",
                   "~/Content/css/addons/datatables-select.min.css",
                   "~/Content/css/addons/rowReorder.dataTables.min.css"));

            bundles.Add(new StyleBundle("~/Content/main-teacher").Include(
                  "~/Content/VLE/css/addons/fileinput/fileinput.css",
                  "~/Content/VLE/css/addons/fileinput/explorer-fas-theme.css",
                  "~/Content/VLE/css/addons/justifiedGallery.css",
                  "~/Content/VLE/css/addons/jquery.mCustomScrollbar.css",
                  "~/Content/VLE/css/addons/loader.css",
                    //"~/Content/VLE/css/main-common.css",
                    "~/Content/VLE/css/main-teacher.css",
                  "~/content/vle/css/addons/lightgallery.css",
                  "~/content/css/addons/video-js.css",
                  "~/Content/VLE/css/addons/owl.carousel.min.css",
                  "~/Content/VLE/addons/owl.theme.default.min.css"
                  ));

            bundles.Add(new StyleBundle("~/Content/main-admin").Include(
               "~/Content/VLE/css/addons/fileinput/fileinput.css",
               "~/Content/VLE/css/addons/fileinput/explorer-fas-theme.css",
               "~/Content/VLE/css/addons/justifiedGallery.css",
               "~/Content/VLE/css/addons/jquery.mCustomScrollbar.css",
               "~/Content/VLE/css/addons/loader.css",
                 //"~/Content/VLE/css/main-common.css",
                 "~/Content/VLE/css/main-admin.css",
               "~/content/vle/css/addons/lightgallery.css",
               "~/content/css/addons/video-js.css",
               "~/Content/VLE/css/addons/owl.carousel.min.css",
               "~/Content/VLE/addons/owl.theme.default.min.css"
               ));

            bundles.Add(new StyleBundle("~/Content/main-common").Include(
                  "~/Content/VLE/css/addons/fileinput/fileinput.css",
                  "~/Content/VLE/css/addons/fileinput/explorer-fas-theme.css",
                  "~/Content/VLE/css/addons/justifiedGallery.css",
                  "~/Content/VLE/css/addons/jquery.mCustomScrollbar.css",
                  "~/Content/VLE/css/addons/loader.css",
                  "~/Content/VLE/css/main-teacher.css",
                  "~/content/vle/css/addons/lightgallery.css",
                  "~/content/css/addons/video-js.css",
                  "~/Content/VLE/css/addons/owl.carousel.min.css",
                  "~/Content/VLE/addons/owl.theme.default.min.css"
                  ));

            bundles.Add(new StyleBundle("~/Content/main-parent").Include(
                  "~/Content/VLE/css/addons/fileinput/fileinput.css",
                  "~/Content/VLE/css/addons/fileinput/explorer-fas-theme.css",
                  "~/Content/VLE/css/addons/jquery.mCustomScrollbar.css",
                  "~/Content/VLE/css/addons/loader.css",
                  "~/Content/VLE/css/main-parent.css",
                  "~/content/vle/css/addons/lightgallery.css",
                  "~/content/css/addons/video-js.css",
                   "~/Content/VLE/css/addons/owl.carousel.min.css",
                    "~/Content/VLE/addons/owl.theme.default.min.css"
                  //,"~/Content/VLE/css/main-teacher.css"
                  ));


            //vle style as per student
            #region studentCss
            bundles.Add(new StyleBundle("~/Content/bootstrapcoreStudent").Include(
                   //"~/Content/Student/css/bootstrap.min.css",
                   "~/Content/Student/css/addons/bootstrap-datetimepicker-standalone.css",
                   "~/Content/Student/css/addons/bootstrap-datetimepicker.css",
                  "~/Content/Student/css/addons/jquery.minicolors.css"
                  //"~/Content/VLE/css/addons/filetype-icons.css",
                  //"~/Content/VLE/css/addons/menu-icon.css"
                  ));

            bundles.Add(new StyleBundle("~/Content/datatablecssStudent").Include(
                   "~/Content/Student/css/addons/datatables.min.css",
                   "~/Content/Student/css/addons/datatables-select.min.css"));

            bundles.Add(new StyleBundle("~/Content/main-student").Include(
                  "~/Content/Student/css/addons/fileinput/fileinput.css",
                  "~/Content/Student/css/addons/fileinput/explorer-fas-theme.css",
                  "~/Content/Student/css/addons/justifiedGallery.css",
                  "~/Content/Student/css/addons/jquery.mCustomScrollbar.css",
                  "~/Content/Student/css/addons/loader.css",
                  "~/content/vle/css/addons/lightgallery.css",
                  "~/Content/Student/css/addons/owl.carousel.min.css",
                  "~/Content/Student/css/addons/owl.theme.default.min.css",
                  "~/Content/Student/css/main-student.css"
                 ));

            bundles.Add(new StyleBundle("~/Content/skin-1").Include(
                 "~/Content/Student/css/skin-1.css"
                ));

            bundles.Add(new StyleBundle("~/Content/skin-default").Include(
                 "~/Content/Student/css/skin-default.css"
                ));

            bundles.Add(new StyleBundle("~/Content/calendar").Include(
                "~/Content/Student/calendar-packages/core/main.css",
                  "~/Content/Student/calendar-packages/daygrid/main.css",
                    "~/Content/Student/calendar-packages/bootstrap/main.css"
               ));
            #endregion
            //end of calendr css for student



            #region SIMS BUNDLES

            // SIMS Css
            bundles.Add(new StyleBundle("~/Content/bootstrapcore-sims").Include(
                     "~/Content/VLE/css/bootstrap.min.css",
                     "~/Content/VLE/css/addons/bootstrap-datetimepicker-standalone.css",
                     "~/Content/VLE/css/addons/bootstrap-datetimepicker.css",
                    "~/Content/VLE/css/addons/jquery.minicolors.css"));

            bundles.Add(new StyleBundle("~/Content/datatablecss-sims").Include(
                    "~/Content/VLE/css/addons/datatables.min.css",
                    "~/Content/VLE/css/addons/datatables-select.min.css"));

            bundles.Add(new StyleBundle("~/Content/main-sims").Include(
                   "~/Content/VLE/css/main-teacher.css",
                   "~/Content/VLE/css/addons/loader.css",
                   "~/Content/VLE/css/addons/jquery.mCustomScrollbar.css",
                   "~/Content/css/custom/SIMS/custom.css"
                   ));

            // SIMS layout js files
            var topLayoutJsBundleSIMS = new ScriptBundle("~/bundles/toplayoutjs-sims");
            topLayoutJsBundleSIMS.Transforms.Add(new FileHashVersionBundleTransform());
            topLayoutJsBundleSIMS.Include(
                "~/Content/js/jquery-3.3.1.min.js",
                "~/Content/js/jquery-ui.min.js",
                "~/Content/js/popper.min.js",
                 "~/Content/js/bootstrap.min.js",
                 "~/Content/js/jquery.unobtrusive-ajax.min.js",
                 "~/Content/js/mdb.min.js",
                 "~/Content/js/addons/bootstrap-select.js",
                 "~/Content/js/addons/moment-with-locales.js",
                 "~/Content/js/addons/bootstrap-datetimepicker.min.js",
                 "~/Content/js/addons/jquery.minicolors.min.js",
                "~/Content/js/noty/jquery.noty.min.js",
                "~/Content/js/addons/datatables.min.js",
                "~/Content/js/addons/datatables-select.js",
                "~/Content/VLE/js/addons/particles.min.js",
                "~/Content/VLE/js/addons/fileinput/fileinput.js",
                "~/Content/VLE/js/addons/fileinput/piexif.min.js",
                "~/Content/VLE/js/addons/fileinput/sortable.min.js",
                "~/Content/VLE/js/addons/fileinput/ar.js",
                "~/Content/VLE/js/addons/fileinput/fas-theme.js",
                "~/Content/VLE/js/addons/fileinput/explorer-fas-theme.js",
                "~/Content/VLE/js/file-uploader-config.js",
                 "~/Content/VLE/js/jquery.mCustomScrollbar.concat.min.js",
                //"~/Content/js/custom.js",
                "~/Content/js/noty/themes/default.js",
                "~/Content/js/noty/layouts/topCenter.js",
                "~/Content/js/noty/layouts/topRight.js",
                 "~/Content/js/noty/layouts/bottomRight.js",
                "~/Content/js/common/global.js",
                 "~/Content/VLE/js/custom.js");
            bundles.Add(topLayoutJsBundleSIMS);

            //Added on 9th July 2019 by Deepak Singh
            // HSE layout js files
            var bottomLayoutJsBundleSIMS = new ScriptBundle("~/bundles/bottomlayoutjs-sims");
            bottomLayoutJsBundleSIMS.Transforms.Add(new FileHashVersionBundleTransform());
            bottomLayoutJsBundleSIMS.Include(
                "~/Content/js/common/plugins.js",
                "~/Content/js/common/DynamicGrid.js",
            "~/Content/js/common/jquery-pagination.js",
            "~/Content/js/custom/SIMS/SIMSlayout/SIMSlayout.js");
            bundles.Add(bottomLayoutJsBundleSIMS);

            #endregion

            #region BDS Bundles

            //#region BDS  bundle bootstrap core

            //bundles.Add(new StyleBundle("~/Content/bds_bootstrapcore").Include(
            //        "~/Content/BDS/css/bootstrap.min.css",
            //       "~/Content/BDS/css/addons/jquery.minicolors.css"));

            //#endregion

            //#region BDS calendar css
            //bundles.Add(new StyleBundle("~/Content/bds_calendar").Include(
            //    "~/Content/BDS/calendar-packages/core/main.css",
            //    "~/Content/BDS/calendar-packages/daygrid/main.css",
            //    "~/Content/BDS/calendar-packages/bootstrap/main.css"
            //    ));
            //#endregion

            //#region BDS file upload css
            //bundles.Add(new StyleBundle("~/Content/bds_fileupload").Include(
            //  "~/Content/BDS/css/addons/fileinput/fileinput.css",
            //  "~/Content/BDS/css/addons/fileinput/explorer-fas-theme.css"
            //  ));

            //#endregion

            //#region BDS material design bootstrap css for teacher

            //bundles.Add(new StyleBundle("~/Content/bds_materialdesignbootstrapforteacher").Include(
            //"~/Content/BDS/css/addons/jquery.mCustomScrollbar.css",
            //"~/Content/BDS/css/addons/loader.css",
            //"~/Content/BDS/css/addons/owl.carousel.min.css",
            //"~/Content/BDS/css/addons/owl.theme.default.min.css",
            //"~/Content/BDS/css/main-teacher.css"
            //));

            //#endregion

            //#region BDS jquery / bootstrap bundle

            //bundles.Add(new ScriptBundle("~/bundles/bds_jquerybootstap").Include
            //    ("~/Content/BDS/js/jquery-3.3.1.min.js",
            //     "~/Content/BDS/js/popper.min.js",
            //     "~/Content/BDS/js/bootstrap.min.js",
            //     "~/Content/BDS/js/mdb.min.js",
            //     "~/Content/BDS/js/addons/bootstrap-select.js",
            //     "~/Content/BDS/js/addons/moment.js",
            //     "~/Content/BDS/js/jquery.mCustomScrollbar.concat.min.js",
            //     "~/Content/BDS/js/addons/bootstrap-datetimepicker.min.js",
            //     "~/Content/BDS/js/addons/jquery.minicolors.min.js",
            //     "~/Content/BDS/js/addons/owl.carousel.min.js",
            //     "~/Content/BDS/js/all-carousels.js"                 
            //    ));

            //#endregion

            //#region BDS ckeditor
            ////bundles.Add(new ScriptBundle("~/bundles/bds_jquerybootstap").Include(
            ////    "~/Content/BDS/js/addons/ckeditor/ckeditor.js",

            ////    ));       

            //#endregion
            #endregion

            #endregion

            BundleTable.EnableOptimizations = true;
        }
    }
}
