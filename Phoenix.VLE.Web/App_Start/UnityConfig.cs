using System.Web.Mvc;
using Unity;
using Unity.Mvc5;
using Phoenix.VLE.Web.Services;
using Phoenix.VLE.Web.Services.Skill.Contracts;
using Phoenix.VLE.Web.Services.Skill;
using SMS.Web.Services;
using Phoenix.VLE.Web.Services.SelectSubject.Contracts;
using Phoenix.VLE.Web.Services.SelectSubject;
using Phoenix.VLE.Web.Services.ParentCorner;
using Phoenix.VLE.Web.Services.ParentCorner.Contracts;
using Phoenix.VLE.Web.Services.Setting.Contracts;
using Phoenix.VLE.Web.Services.Setting;
using Phoenix.VLE.Web.Services.PTM.Contracts;
using Phoenix.VLE.Web.Services.PTM;

namespace Phoenix.VLE.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            //Common
            container.RegisterType<ISelectListService, SelectListService>();
            container.RegisterType<IModuleStructureService, ModuleStructureService>();


            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IStudentService, StudentService>();
            container.RegisterType<ISafetyCategoriesService, SafetyCategoriesService>();
            container.RegisterType<IGradingTemplateService, GradingTemplateService>();
            container.RegisterType<IGradingTemplateItemService, GradingTemplateItemService>();
            container.RegisterType<ISubjectService, SubjectService>();
            container.RegisterType<IContactInfoService, ContactInfoService>();

            container.RegisterType<ICountryService, CountryService>();
            container.RegisterType<ICensorFilterService, CensorFilterService>();
            container.RegisterType<IUserRoleService, UserRoleService>();
            container.RegisterType<ILogInUserService, LogInUserService>();
            container.RegisterType<ISchoolService, SchoolService>();
            container.RegisterType<ILogInUserService, LogInUserService>();
            container.RegisterType<IMarkingSchemeService, MarkingSchemeService>();
            container.RegisterType<ISchoolSpaceService, SchoolSpaceService>();
            container.RegisterType<IAbuseDelegateService, AbuseDelegateService>();
            container.RegisterType<ISchoolGroupService, SchoolGroupService>();
            container.RegisterType<IAssignmentService, AssignmentService>();
            container.RegisterType<IUserPermissionService, UserPermissionService>();
            container.RegisterType<IUserLocationMapService, UserLocationMapService>();
            container.RegisterType<IBlogService, BlogService>();
            container.RegisterType<IBlogCommentService, BlogCommentService>();
            container.RegisterType<IFolderService, FolderService>();
            container.RegisterType<IFileService, FileService>();
            container.RegisterType<IObservationService, ObservationService>();
            container.RegisterType<IGroupUrlService, GroupUrlService>();
            container.RegisterType<IGroupQuizService, GroupQuizService>();
            container.RegisterType<ISuggestionCategoryService, SuggestionCategoryService>();
            container.RegisterType<ISchoolSkillSetService, SchoolSkillSetService>();
            container.RegisterType<IPTMCategoryService, PTMCategoryService>();
            container.RegisterType<IPTMDeclineReasonService, PTMDeclineReasonService>();
            container.RegisterType<ISuggestionService, SuggestionService>();
            container.RegisterType<IContentLibraryService, ContentLibraryService>();
            container.RegisterType<IEventService, EventService>();
            container.RegisterType<IStudentSkillSetService, StudentSkillSetService>();
            container.RegisterType<IStudentCertificateService, StudentCertificateService>();
            container.RegisterType<IStudentAcademicService, StudentAcademicService>();
            container.RegisterType<IStudentGoalService, StudentGoalService>();
            container.RegisterType<IStudentAcheivementService, StudentAcheivementService>();
            container.RegisterType<ISchoolNotificationService, SchoolNotificationService>();

            container.RegisterType<ITemplateService, TemplateService>();
            container.RegisterType<IPlanSchemeDetailService, PlanSchemeDetailService>();
            container.RegisterType<IGroupCourseTopicService, GroupCourseTopicService>();
            container.RegisterType<IExemplarWallServices, ExemplarWallService>();

            container.RegisterType<IRSSFeedService, RSSFeedService>();

            //Async Lesson
            container.RegisterType<IAsyncLessonService, AsyncLessonService>();

            container.RegisterType<IStudentListService, StudentListService>();
            //Users
            container.RegisterType<IUserService, UserService>();
            //Notifications
            container.RegisterType<INotificationService, NotificationService>();


            //SIMS
            container.RegisterType<ITimeTableService, TimeTableService>();
            container.RegisterType<IAttendanceService, AttendanceService>();
            container.RegisterType<IClassListService, ClassListService>();
            container.RegisterType<IBehaviourService, BehaviourService>();
            container.RegisterType<IAssessmentService, AssessmentService>();
            container.RegisterType<ISIMSCommonService, SIMSCommonService>();
            container.RegisterType<IAttendanceSettingService, AttendanceSettingService>();


            container.RegisterType<ISENService, SENService>();
            container.RegisterType<IProgressTrackerService, ProgressTrackerService>();
            container.RegisterType<ISubjectSettingService, SubjectSettingService>();

            //Behaviour
            container.RegisterType<IBehaviourService, BehaviourService>();

            //Assessment
            container.RegisterType<IAssessmentService, AssessmentService>();

            //Report
            container.RegisterType<IIncidentReportService, IncidentReportService>();

            //Reminder
            //container.RegisterType<IReminderRegistrationService, ReminderRegistrationService>();
            //container.RegisterType<ICustomReminderService, CustomReminderService>();

            //Parent
            container.RegisterType<IParentService, ParentService>();
            container.RegisterType<IAssignmentService, AssignmentService>();
            container.RegisterType<ISchoolSpaceService, SchoolSpaceService>();
            container.RegisterType<IFileService, FileService>();

            //Teacher
            container.RegisterType<ITeacherDashboardService, TeacherDashboardService>();

            //Quiz
            container.RegisterType<IQuizService, QuizService>();
            container.RegisterType<IQuizQuestionsService, QuizQuestionsService>();

            container.RegisterType<IAbuseDelegateService, AbuseDelegateService>();

            //Common Service
            container.RegisterType<ICommonService, CommonService>();

            //Themes
            container.RegisterType<IThemesService, ThemesService>();
            //Bookmarks
            container.RegisterType<IBookmarkService, BookmarkService>();
            //Bookmarks
            container.RegisterType<ISchoolBannerService, SchoolBannerService>();
            //SchoolBanner
            // container.RegisterType<IStudentBannerService, StudentBannerService>();
            //planner
            container.RegisterType<IMyPlannerService, MyPlannerService>();
            container.RegisterType<IEventCategoryService, EventCategoryService>();
            //School Badges
            container.RegisterType<ISchoolBadgeService, SchoolBadgeService>();
            container.RegisterType<ITemplateLoginService, TemplateLoginService>();
            container.RegisterType<ITemplateUploadService, TemplateUploadService>();

            container.RegisterType<IAssessmentConfigService, AssessmentConfigService>();
            container.RegisterType<ICalendarService, CalendarService>();
            container.RegisterType<IPhoenixTokenService, PhoenixTokenService>();
            //School Marking Policy
            // container.RegisterType<IMarkingPolicyService, MarkingPolicyService>();
            #region Course
            container.RegisterType<ICourseService, CourseService>();
            container.RegisterType<IUnitService, UnitService>();
            //container.RegisterType<ILessonService, LessonService>();
            container.RegisterType<IStandardService, StandardService>();
            container.RegisterType<IStandardDetailsService, StandardDetailsService>();
            container.RegisterType<ILessonService, LessonService>();
            container.RegisterType<IAttachmentService, AttachmentService>();
            container.RegisterType<IAssessmentEvidenceService, AssessmentEvidenceService>();
            container.RegisterType<IAssessmentLearningService, AssessmentLearningService>();
            #endregion

            #region Beahviour
            container.RegisterType<IIncidentService, IncidentService>();
            #endregion

            #region Beahviour Setup
            container.RegisterType<IBehaviourSetupService, BehaviourSetupService>();
            #endregion

            #region Grade Book
            container.RegisterType<IGradeBookService, GradeBookService>();
            container.RegisterType<IGradeBookSetupService, GradeBookSetupService>();
            #endregion

            #region Chat
            container.RegisterType<IChatService, ChatService>();
            container.RegisterType<IChatterPermissionService, ChatterPermissionService>();
            #endregion

            #region Course Catalogue
            container.RegisterType<ICourseCatalogueService, CourseCatalogueService>();
            #endregion


            #region Parent Corner
            //Leave
            container.RegisterType<ILeaveService, LeaveService>();
            container.RegisterType<IAssessmentReport, AssessmentServiceReport>();
            container.RegisterType<IEnrollActivitiesService, EnrollActivitiesService>();
            container.RegisterType<ISelectSubjectService, SelectSubjectService>();
            container.RegisterType<IGetStudentInfoService, GetStudentInfoService>();
            container.RegisterType<IGetRewardService, GetRewardService>();
            container.RegisterType<IStudentUpdateDetailsService, StudentUpdateDetailsService>();

            //FeePayments
            container.RegisterType<IFeePaymentsService, FeePaymentsService>();
            //Set subject for exam
            container.RegisterType<ISetSubjectService, SetSubjectService>();
            //Common payment service
            container.RegisterType<IPaymentService, PaymentService>();

            //Tc request
            container.RegisterType<ITCRequestService, TCRequestService>();
            //Hifz tracker
            container.RegisterType<IHifzTrackerService, HifzTrackerService>();

            //PhoenixAPI token
            container.RegisterType<IPhoenixAPIParentCornerService, PhoenixAPIParentCornerService>();

            #endregion

            container.RegisterType<ISetSubjectService, SetSubjectService>();
            #region Division
            container.RegisterType<IDivisionService, DivisionService>();
            #endregion
            container.RegisterType<ISharepointService, SharePointService>();
            container.RegisterType<IAssessmentConfigurationService, AssessmentConfigurationService>();
            #region SchoolReport
            container.RegisterType<ISchoolReportService, SchoolReportService>();
            //container.RegisterType<ISelectCourseTestService, SelectCourseTestService>();
            #endregion

            #region Charts Dashboard
            container.RegisterType<IStudentProgressTrackerService, StudentProgressTrackerService>();
            container.RegisterType<IStudentAssAndPredAnalysisService, StudentAssAndPredAnalysisService>();
            #endregion

            #region Setting
            container.RegisterType<ISettingService, SettingService>();
            #endregion

            #region Survey
            container.RegisterType<ISurveyService, SurveyService>();
            #endregion

            #region MigrationJob
            container.RegisterType<IMigrationJobService, MigrationJobService>();
            #endregion


            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}
