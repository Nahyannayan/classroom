﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;

namespace Phoenix.VLE.Web.Services
{
    public interface IStudentSkillSetService
    {
        List<StudentSkills> GetStudentSkillsData(long id);
        OperationDetails UpdateStudentSkillsData(int studentId, int skillId, TransactionModes mode);
        OperationDetails InsertStudentSkill(StudentSkillEdit model);
        bool UpdateStudentSkillEndorsementDetails(SkillEndorsementEdit model);
        List<SkillEndorsement> GetAllStudentEndorsedSkills(long userId, int pageIndex, int v, string searchString = "");
        bool UpdateEndorseSkillStatusRating(SkillEndorsementEdit editModel);
        bool UpdateAllEndorsedSkillStatus(List<SkillEndorsementEdit> lstEditModel);
        bool DeleteEndorsedSkill(SkillEndorsement model);
        SkillEndorsement GetStudentEndorsedSkillById(int skillEndorsementId);
        bool UpdateStudentEndorsementStatus(SkillEndorsement model);
    }
}
