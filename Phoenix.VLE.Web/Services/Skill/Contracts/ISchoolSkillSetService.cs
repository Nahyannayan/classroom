﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services.Skill.Contracts
{
    public interface ISchoolSkillSetService
    {
        IEnumerable<SchoolSkillSet> GetSchoolSkillSet(int SchoolGradeId);
        SchoolSkillSet GetSchoolSkillSetById(int id);
        int UpdateSchoolSkillSet(SchoolSkillSetEdit model);
        int DeleteSchoolSkillSet(int id);
    }
}
