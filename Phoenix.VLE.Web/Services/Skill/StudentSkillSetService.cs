﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Newtonsoft.Json;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public class StudentSkillSetService : IStudentSkillSetService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });

        // TODO: put this is a base class if needed
        readonly string _baseUrl = Constants.PhoenixAPIUrl;

        readonly string _path = "api/v1/StudentSkillSet";
        #endregion

        public StudentSkillSetService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public List<StudentSkills> GetStudentSkillsData(long id)
        {
            var lstStudentSkills = new List<StudentSkills>();
            var uri = API.StudentInformation.GetStudentSkillsData(_path, id);

            //To set authorization header 
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataStatus = response.Content.ReadAsStringAsync().Result;
                lstStudentSkills = JsonConvert.DeserializeObject<List<StudentSkills>>(jsonDataStatus);
            }

            return lstStudentSkills;
        }

        public OperationDetails InsertStudentSkill(StudentSkillEdit model)
        {
            var op = new OperationDetails();
            HttpResponseMessage response = _client.PostAsJsonAsync(API.StudentInformation.InsertStudentSkill(_path), model).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataStatus = response.Content.ReadAsStringAsync().Result;
                op = JsonConvert.DeserializeObject<OperationDetails>(jsonDataStatus);
            }

            return op;
        }
        public OperationDetails UpdateStudentSkillsData(int studentId, int skillId, TransactionModes mode)
        {
            var op = new OperationDetails();
            var studentSkillEdit = new StudentSkillEdit() { StudentId = studentId, SkillId = skillId, Mode = mode, CreatedUserId = SessionHelper.CurrentSession.Id };
            var uri = API.StudentInformation.UpdateStudentSkillsData(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, studentSkillEdit).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataStatus = response.Content.ReadAsStringAsync().Result;
                op = JsonConvert.DeserializeObject<OperationDetails>(jsonDataStatus);
            }

            return op;
        }


        #region Student Skill Endorsement
        public bool UpdateStudentSkillEndorsementDetails(SkillEndorsementEdit model)
        {
            var skillEndorsementModel = new SkillEndorsement();
            EntityMapper<SkillEndorsementEdit, SkillEndorsement>.Map(model, skillEndorsementModel);
            var uri = API.StudentInformation.UpdateStudentSkillEndorsementDetails(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, skillEndorsementModel).Result;
            return responseMessage.IsSuccessStatusCode;
        }

        public List<SkillEndorsement> GetAllStudentEndorsedSkills(long userId, int pageIndex, int pageSize, string searchString = "")
        {
            var lstStudentSkills = new List<SkillEndorsement>();
            var uri = API.StudentInformation.GetAllStudentEndorsedSkills(_path, userId, pageIndex, pageSize, searchString, SessionHelper.CurrentSession.Id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataStatus = response.Content.ReadAsStringAsync().Result;
                lstStudentSkills = JsonConvert.DeserializeObject<List<SkillEndorsement>>(jsonDataStatus);
            }

            return lstStudentSkills;
        }

        public bool UpdateAllEndorsedSkillStatus(List<SkillEndorsementEdit> listEditModel)
        {
            var lstModel = new List<SkillEndorsement>();
            foreach(var EditModel in listEditModel)
            {
                var Model = new SkillEndorsement();
                EntityMapper<SkillEndorsementEdit, SkillEndorsement>.Map(EditModel, Model);
                lstModel.Add(Model);
            }
            
            var uri = API.StudentInformation.UpdateAllEndorsedSkillStatus(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, lstModel).Result;
            return responseMessage.IsSuccessStatusCode;
        }

        public bool UpdateEndorseSkillStatusRating(SkillEndorsementEdit editModel)
        {
            var model = new SkillEndorsement();
            EntityMapper<SkillEndorsementEdit, SkillEndorsement>.Map(editModel, model);
            var uri = API.StudentInformation.UpdateEndorseSkillStatusRating(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, model).Result;
            return responseMessage.IsSuccessStatusCode;
        }

        public bool DeleteEndorsedSkill(SkillEndorsement model)
        {
            var uri = API.StudentInformation.DeleteEndorsedSkill(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public SkillEndorsement GetStudentEndorsedSkillById(int skillEndorsementId)
        {
            var model = new SkillEndorsement();
            var uri = API.StudentInformation.GetStudentEndorsedSkillById(_path, skillEndorsementId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataStatus = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<SkillEndorsement>(jsonDataStatus);
            }
            return model;

        }

        public bool UpdateStudentEndorsementStatus(SkillEndorsement model)
        {
            var uri = API.StudentInformation.UpdateStudentEndorsementStatus(_path);
            HttpResponseMessage message = _client.PostAsJsonAsync(uri, model).Result;
            return message.IsSuccessStatusCode;
        }


        #endregion
    }
}