﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Localization;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services.Skill.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class SchoolSkillSetService : ISchoolSkillSetService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/SchoolSkillSet";
        #endregion

        public SchoolSkillSetService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods
       

        public IEnumerable<SchoolSkillSet> GetSchoolSkillSet(int SchoolGradeId)
        {
            int languageId = LocalizationHelper.CurrentSystemLanguageId;
            var uri = API.SchoolSkillSet.GetSchoolSkillSet(_path, languageId, SchoolGradeId);
            IEnumerable<SchoolSkillSet> categories = new List<SchoolSkillSet>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                categories = EntityMapper<string, IEnumerable<SchoolSkillSet>>.MapFromJson(jsonDataProviders);
            }
            return categories;
        }

        public SchoolSkillSet GetSchoolSkillSetById(int id)
        {
            var category = new SchoolSkillSet();
            var uri = API.SchoolSkillSet.GetSchoolSkillSetById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                category = EntityMapper<string, SchoolSkillSet>.MapFromJson(jsonDataProviders);
            }
            return category;
        }

        public int UpdateSchoolSkillSet(SchoolSkillSetEdit model)
        {
            int result = 0;
            var uri = string.Empty;
            var sourceModel = new SchoolSkillSet();
            EntityMapper<SchoolSkillSetEdit, SchoolSkillSet>.Map(model, sourceModel);
            sourceModel.CreatedBy = (int)SessionHelper.CurrentSession.Id;
            //sourceModel.SchoolGradeId = (int)SessionHelper.CurrentSession.SchoolId;
            if (model.IsAddMode)
                uri = API.SchoolSkillSet.InsertSchoolSkillSet(_path);
            else
                uri = API.SchoolSkillSet.UpdateSchoolSkillSet(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int DeleteSchoolSkillSet(int id)
        {
            var uri = API.SchoolSkillSet.DeleteSchoolSkillSet(_path, id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            int result = Convert.ToInt32(jsonDataProviders);
            return result;
        }
        #endregion


    }
}