﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Services.Setting.Contracts
{
    public interface ISettingService
    {
        IEnumerable<NotificationTrigger> GetEnableDisableNotificationListForParent(long parentId);
        int SaveNotificationSettingForParent(NotificationSettings model, long parentId);
    }
}