﻿using Phoenix.Common.Helpers;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services.Setting.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Phoenix.VLE.Web.Services.Setting
{
    public class SettingService : ISettingService
    {
        readonly string _path = "api/v1/Setting";
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        public SettingService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

            }
        }

        public IEnumerable<NotificationTrigger> GetEnableDisableNotificationListForParent(long parentId)
        {
            var uri = API.Setting.GetEnableDisableNotificationListForParent(_path, parentId);
            IEnumerable<NotificationTrigger> notificationTriggerList = new List<NotificationTrigger>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                notificationTriggerList = EntityMapper<string, IEnumerable<NotificationTrigger>>.MapFromJson(jsonDataProviders);
            }
            return notificationTriggerList;
        }

        public int SaveNotificationSettingForParent(NotificationSettings model, long parentId) {
            var uri = API.Setting.SaveNotificationSettingForParent(_path, parentId);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            //  if (response.IsSuccessStatusCode)
            //{
            //    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            //    notificationTriggerList = EntityMapper<string, IEnumerable<NotificationTrigger>>.MapFromJson(jsonDataProviders);
            //}
            //return notificationTriggerList;
            return 0;
        }
    }
}