﻿using Newtonsoft.Json;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Net.Http.Headers;
using Phoenix.Models;
using Phoenix.Common.Helpers;

namespace Phoenix.VLE.Web.Services
{
    public class ParentService: IParentService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });

        // TODO: put this is a base class if needed
        readonly string _baseUrl = Constants.PhoenixAPIUrl;

        readonly string _path = "api/v1/parent";
        #endregion
        public ParentService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public ParentView GetStudentDetailsById(long id)
        {
            var parentView = new ParentView();
            var uri = API.Parent.GetStudentDetailsById(_path, id);

            //To set authorization header 
            var token = Helpers.CommonHelper.GetCookieValue("st", "tk");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataStatus = response.Content.ReadAsStringAsync().Result;
                parentView = JsonConvert.DeserializeObject<ParentView>(jsonDataStatus);
            }

            return parentView;
        }

        public int AssignAccessPermission(string StudentIdList) {
            var NoOfRowUpdated = 0;
            var uri = API.Parent.AssignAccessPermission(_path, StudentIdList);
            var token = Helpers.CommonHelper.GetCookieValue("st", "tk");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataStatus = response.Content.ReadAsStringAsync().Result;
                NoOfRowUpdated = Convert.ToInt32(jsonDataStatus);
            }

            return NoOfRowUpdated;
        }
    }
}