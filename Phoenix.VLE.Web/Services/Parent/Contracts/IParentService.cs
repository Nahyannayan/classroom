﻿using Phoenix.Common.Models;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public interface IParentService
    {
        ParentView GetStudentDetailsById(long id);

        int AssignAccessPermission(string StudentIdList);
    }
}