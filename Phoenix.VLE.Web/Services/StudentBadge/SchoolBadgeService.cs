﻿using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Web;
using System.Collections.Generic;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;

namespace Phoenix.VLE.Web.Services
{
    public class SchoolBadgeService : ISchoolBadgeService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/SchoolBadge";
        #endregion

        public SchoolBadgeService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public OperationDetails UpdateBadgeData(SchoolBadgeEdit model)
        {

            var uri = string.Empty;
            var sourceModel = new SchoolBadge();
            OperationDetails op = new OperationDetails();
            EntityMapper<SchoolBadgeEdit, SchoolBadge>.Map(model, sourceModel);

            if (model.IsAddMode)
                uri = API.SchoolBadge.InsertSchoolBadge(_path);
            else
                uri = API.SchoolBadge.UpdateSchoolBadge(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }

        public OperationDetails DeleteBadgeData(SchoolBadge schoolBadge)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.SchoolBadge.DeleteSchoolBadge(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, schoolBadge).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }

        public IEnumerable<SchoolBadge> GetSchoolBadges(long userId, int schoolId)
        {
            var uri = API.SchoolBadge.GetSchoolBadges(_path, userId, schoolId);
            IEnumerable<SchoolBadge> lstSchoolBadge = new List<SchoolBadge>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstSchoolBadge = EntityMapper<string, IEnumerable<SchoolBadge>>.MapFromJson(jsonDataProviders);
            }
            return lstSchoolBadge;
        }
        public IEnumerable<string> GetBadgeSchoolIds(long BadgeId)
        {
            var uri = API.SchoolBadge.GetBadgeSchoolIds(_path, BadgeId);
            IEnumerable<string> lstSchoolIds = new List<string>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstSchoolIds = EntityMapper<string, IEnumerable<string>>.MapFromJson(jsonDataProviders);
            }
            return lstSchoolIds;
        }

        public SchoolBadge GetSchoolBadgeDetails(long schoolBadgeId, long userId)
        {
            var schoolBadgeDetails = new SchoolBadge();
            var uri = API.SchoolBadge.GetSchoolBadgeDetails(_path, schoolBadgeId, userId);
            IEnumerable<string> lstSchoolIds = new List<string>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                schoolBadgeDetails = EntityMapper<string, SchoolBadge>.MapFromJson(jsonDataProviders);
            }
            return schoolBadgeDetails;
        }
        public int GetTopOrderForDisplayBadge(long userId)
        {
            int toporder = 1;
            var schoolBadgeDetails = new SchoolBadge();
            var uri = API.SchoolBadge.GetTopOrderForDisplayBadge(_path, userId);

            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                toporder = int.Parse(jsonDataProviders);
            }
            return toporder;
        }

        public IEnumerable<SchoolBadge> GetUserBadges(long userId)
        {
            IEnumerable<SchoolBadge> lstUserBadges = new List<SchoolBadge>();
            var uri = API.SchoolBadge.GetUserBadges(_path, userId);
            IEnumerable<string> lstSchoolIds = new List<string>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstUserBadges = EntityMapper<string, IEnumerable<SchoolBadge>>.MapFromJson(jsonDataProviders);
            }
            return lstUserBadges;
        }

        public bool SaveStudentBadges(SchoolBadge studentBadges)
        {
            var uri = API.SchoolBadge.SaveStudentBadges(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, studentBadges).Result;
            return responseMessage.IsSuccessStatusCode;
        }

        public IEnumerable<SchoolBadge> GetBadgesBySchoolId(int schoolId)
        {
            var uri = API.SchoolBadge.GetBadgesBySchoolId(_path, schoolId);
            IEnumerable<SchoolBadge> lstSchoolBadge = new List<SchoolBadge>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstSchoolBadge = EntityMapper<string, IEnumerable<SchoolBadge>>.MapFromJson(jsonDataProviders);
            }
            return lstSchoolBadge;
        }

        public bool SaveGroupWiseStudentBadges(StudentBadge studentBadge)
        {
            var uri = API.SchoolBadge.SaveGroupWiseStudentBadges(_path);
            HttpResponseMessage responseMessage = _client.PostAsJsonAsync(uri, studentBadge).Result;
            return responseMessage.IsSuccessStatusCode;
        }

        public IEnumerable<StudentProfileSections> GetStudentProfileSections(int studentId)
        {
            var uri = API.StudentInformation.GetStudentProfileSections(_path, studentId);
            IEnumerable<StudentProfileSections> lstStudentProfileSections = new List<StudentProfileSections>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstStudentProfileSections = EntityMapper<string, IEnumerable<StudentProfileSections>>.MapFromJson(jsonDataProviders);
            }
            return lstStudentProfileSections;
        }
    }
}
