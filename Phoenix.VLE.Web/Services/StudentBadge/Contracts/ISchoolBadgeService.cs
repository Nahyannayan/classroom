﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Phoenix.VLE.Web.Services
{
    public interface ISchoolBadgeService
    {
        OperationDetails UpdateBadgeData(SchoolBadgeEdit model);
        OperationDetails DeleteBadgeData(SchoolBadge assignment);
        IEnumerable<SchoolBadge> GetSchoolBadges(long userId, int schoolId);
        IEnumerable<string> GetBadgeSchoolIds(long BadgeId);
        SchoolBadge GetSchoolBadgeDetails(long SchoolBadgeId,long UserId);
        int GetTopOrderForDisplayBadge(long userId);
        IEnumerable<SchoolBadge> GetUserBadges(long userId);
        bool SaveStudentBadges(SchoolBadge studentBadges);
        IEnumerable<SchoolBadge> GetBadgesBySchoolId(int schoolId);
        bool SaveGroupWiseStudentBadges(StudentBadge studentBadge);
    }
}
