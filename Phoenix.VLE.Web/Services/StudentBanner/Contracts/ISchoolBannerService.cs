﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Phoenix.VLE.Web.Services
{
    public interface ISchoolBannerService
    {
        OperationDetails UpdateBannerData(SchoolBannerEdit model);
        OperationDetails DeleteBannerData(SchoolBanner assignment);
        IEnumerable<SchoolBanner> GetSchoolBanners(long userId, int schoolId);
        IEnumerable<string> GetBannerSchoolIds(long bannerId);
        SchoolBanner GetSchoolBannerDetails(long SchoolBannerId,long UserId);
        int GetTopOrderForDisplayBanner(long userId);
        IEnumerable<SchoolBanner> GetUserBanners(long userId);
    }
}
