﻿using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Web;
using System.Collections.Generic;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;

namespace Phoenix.VLE.Web.Services
{
    public class SchoolBannerService : ISchoolBannerService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/SchoolBanner";
        #endregion

        public SchoolBannerService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);

            }
        }

        public OperationDetails UpdateBannerData(SchoolBannerEdit model)
        {
            bool result = false;
            var uri = string.Empty;
            var sourceModel = new SchoolBanner();
            OperationDetails op = new OperationDetails();
            EntityMapper<SchoolBannerEdit, SchoolBanner>.Map(model, sourceModel);
                       
            if (model.IsAddMode)
                uri = API.SchoolBanner.InsertSchoolBanner(_path);
            else
                uri = API.SchoolBanner.UpdateSchoolBanner(_path);
            if (SessionHelper.CurrentSession.UserTypeId != 5)
            {
                sourceModel.Schools = SessionHelper.CurrentSession.SchoolId.ToString();
            }
            else {
                sourceModel.Schools = string.Join(",", model.SchoolIds);
            }
            
            sourceModel.UserTypes = string.Join(",", model.UserTypeIds);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            op = EntityMapper<string,OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }

        public OperationDetails DeleteBannerData(SchoolBanner schoolBanner)
        {
            OperationDetails op = new OperationDetails();
            var uri = API.SchoolBanner.DeleteSchoolBanner(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, schoolBanner).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            op = EntityMapper<string, OperationDetails>.MapFromJson(jsonDataProviders);
            return op;
        }

        public IEnumerable<SchoolBanner>  GetSchoolBanners(long userId, int schoolId)
        {
            var uri = API.SchoolBanner.GetSchoolBanners(_path,userId, schoolId);
            IEnumerable<SchoolBanner> lstSchoolBanner = new List<SchoolBanner>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstSchoolBanner = EntityMapper<string, IEnumerable<SchoolBanner>>.MapFromJson(jsonDataProviders);
            }
            return lstSchoolBanner;
        }
        public IEnumerable<string> GetBannerSchoolIds(long bannerId)
        {
            var uri = API.SchoolBanner.GetBannerSchoolIds(_path, bannerId);
            IEnumerable<string> lstSchoolIds = new List<string>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstSchoolIds = EntityMapper<string, IEnumerable<string>>.MapFromJson(jsonDataProviders);
            }
            return lstSchoolIds;
        }

        public SchoolBanner GetSchoolBannerDetails(long schoolBannerId,long userId)
        {
            var schoolBannerDetails = new SchoolBanner();
            var uri = API.SchoolBanner.GetSchoolBannerDetails(_path,schoolBannerId, userId);
            IEnumerable<string> lstSchoolIds = new List<string>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                schoolBannerDetails = EntityMapper<string, SchoolBanner>.MapFromJson(jsonDataProviders);
            }
            return schoolBannerDetails;
        }
        public int GetTopOrderForDisplayBanner(long userId)
        {
            int toporder = 1;
            var schoolBannerDetails = new SchoolBanner();
            var uri = API.SchoolBanner.GetTopOrderForDisplayBanner(_path, userId);
           
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                toporder =int.Parse(jsonDataProviders);
            }
            return toporder;
        }

        public IEnumerable<SchoolBanner> GetUserBanners( long userId)
        {
            IEnumerable<SchoolBanner> lstUserBanners = new List<SchoolBanner>();
            var uri = API.SchoolBanner.GetUserBanners(_path, userId);
            IEnumerable<string> lstSchoolIds = new List<string>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstUserBanners = EntityMapper<string, IEnumerable<SchoolBanner>>.MapFromJson(jsonDataProviders);
            }
            return lstUserBanners;
        }

    }
}
