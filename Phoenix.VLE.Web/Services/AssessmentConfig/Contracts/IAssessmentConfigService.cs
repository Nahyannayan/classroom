﻿using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IAssessmentConfigService
    {
        bool AddEditAssessmentConfig(List<GradeTemplate> obj, string Description,string MasterId, long GTM_Id, long SchoolId);
        IEnumerable<GradeTemplate> GetAssessmentConfigList(long Acd_Id);
        IEnumerable<GradeTemplate> GetAssessmentConfigMasterList(long SchoolId);
        
        IEnumerable<GradeSlab> GetGradeSlabList(long Acd_Id);


        IEnumerable<GradeSlabMaster> GetGradeSlabMasterList(long SchoolId);

        bool AddEditGradeSlab(List<GradeSlab> obj, string Description,string MasterIds, long GradeSlabMasterId, long Acd_Id);
    }
}
