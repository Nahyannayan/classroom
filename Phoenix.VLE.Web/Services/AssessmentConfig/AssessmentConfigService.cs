﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System.Text;
using Phoenix.VLE.Web.Areas.Course.Models;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Services
{
    public class AssessmentConfigService : IAssessmentConfigService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/AssessmentConfig";
        #endregion
        public AssessmentConfigService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public bool AddEditAssessmentConfig(List<GradeTemplate> obj, string Description, string MasterId, long GTM_Id, long SchoolId)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.AssessmentConfig.AddEditAssessmentConfig(_path, Description, MasterId, GTM_Id, SchoolId), obj).Result;
            return response.IsSuccessStatusCode;

        }
        public bool AddEditGradeSlab(List<GradeSlab> obj, string Description,string MasterId, long GradeSlabMasterId, long Acd_Id)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.AssessmentConfig.AddEditGradeSlab(_path, Description, MasterId, GradeSlabMasterId, Acd_Id), obj).Result;
            return response.IsSuccessStatusCode;

        }

        public IEnumerable<GradeTemplate> GetAssessmentConfigList(long Acd_Id)
        {
            var uri = API.AssessmentConfig.GetAssessmentConfigList(_path, Acd_Id);
            IEnumerable<GradeTemplate> Lesson = new List<GradeTemplate>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Lesson = EntityMapper<string, IEnumerable<GradeTemplate>>.MapFromJson(jsonDataProviders);
            }
            return Lesson;
        }
        public IEnumerable<GradeTemplate> GetAssessmentConfigMasterList(long SchoolId)
        {
            var uri = API.AssessmentConfig.GetAssessmentConfigMasterList(_path, SchoolId);
            IEnumerable<GradeTemplate> Lesson = new List<GradeTemplate>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Lesson = EntityMapper<string, IEnumerable<GradeTemplate>>.MapFromJson(jsonDataProviders);
            }
            return Lesson;
        }
        public IEnumerable<GradeSlab> GetGradeSlabList(long Acd_Id)
        {
            var uri = API.AssessmentConfig.GetGradeSlabList(_path, Acd_Id);
            IEnumerable<GradeSlab> Lesson = new List<GradeSlab>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Lesson = EntityMapper<string, IEnumerable<GradeSlab>>.MapFromJson(jsonDataProviders);
            }
            return Lesson;
        }

        public IEnumerable<GradeSlabMaster> GetGradeSlabMasterList(long SchoolId)
        {
            var uri = API.AssessmentConfig.GetGradeSlabMasterList(_path, SchoolId);
            IEnumerable<GradeSlabMaster> Lesson = new List<GradeSlabMaster>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Lesson = EntityMapper<string, IEnumerable<GradeSlabMaster>>.MapFromJson(jsonDataProviders);
            }
            return Lesson;
        }


    }
}