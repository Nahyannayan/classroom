﻿using System;
using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.VLE.Web.EditModels;
using Phoenix.Models.Entities;

namespace Phoenix.VLE.Web.Services
{
    public interface IEnrollActivitiesService
    {
        List<EnrolllActivityModel> GetListOfActivities(string studId);
        RequestEnroll RequestEnrollInfo(int alD_ID, string Studentnumber);
        IEnumerable<EnrolllActivityModel> GetActivity(string StudentNo, int ActivityId, int activityenrollmentId = 0);
        bool SendRequest(string stu_no, string alD_ID,int studentCount, string radioselect, int id, string comment);
        bool PostActivityFeedback(ActiveFeedback feebback);
        bool PostUnsubscribeActivity(int APD_ID);


    }
}

