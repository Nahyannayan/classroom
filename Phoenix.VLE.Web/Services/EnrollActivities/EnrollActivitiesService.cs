﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models.EditModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class EnrollActivitiesService : IEnrollActivitiesService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        private ILoggerClient _loggerClient;
        private IPhoenixAPIParentCornerService _phoenixAPIParentCornerService;
        private int ParentCornerAPITraceEnabled = Convert.ToInt16(ConfigurationManager.AppSettings["ParentCornerAPITraceEnabled"]);

        public EnrollActivitiesService(IPhoenixAPIParentCornerService phoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _loggerClient = LoggerClient.Instance;
            }
            _phoenixAPIParentCornerService = phoenixAPIParentCornerService;
        }
        #endregion



        public List<EnrolllActivityModel> GetListOfActivities(string studId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            string json = "";
            EnrolllActivityRoot enrolllactivityRoot = new EnrolllActivityRoot();
            List<EnrolllActivityModel> EnrolllActivityList = new List<EnrolllActivityModel>();
            HttpRequestMessage httpRequestMessage = null;
            try
            {
                var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();
                if (token != null)
                {
                    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                    {
                        string AuthorizedToken = token.token_type + " " + token.access_token;
                        httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.GET_LIST_OF_ACTIVITIES);


                        httpRequestMessage.Headers.Add("stuno", studId.ToString());
                        httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                        using (response = _client.SendAsync(httpRequestMessage).Result)
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                JavaScriptSerializer se = new JavaScriptSerializer();
                                json = response.Content.ReadAsStringAsync().Result;

                                enrolllactivityRoot = new JavaScriptSerializer().Deserialize<EnrolllActivityRoot>(json);

                                if (enrolllactivityRoot.data != null && enrolllactivityRoot.data.Count > 0)
                                    EnrolllActivityList = enrolllactivityRoot.data;
                            }
                        }

                    }
                }
                return EnrolllActivityList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in EnrollActivitiesService.GetListOfActivities()" + ex.Message);
                return EnrolllActivityList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, EnrolllActivityList, response, SessionHelper.CurrentSession.UserName);
            }
        }
        public RequestEnroll RequestEnrollInfo(int alD_ID, string Studentnumber)
        {
            string json = "";
            RequestEnrollRoot requestEnrollRoot = new RequestEnrollRoot();
            RequestEnroll requestEnroll = new RequestEnroll();
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            try
            {
                var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();
                if (token != null)
                {
                    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                    {
                        string AuthorizedToken = token.token_type + " " + token.access_token;
                        httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.GET_ACTIVITY_SHORT_INFO);


                        httpRequestMessage.Headers.Add("stuno", Studentnumber);
                        httpRequestMessage.Headers.Add("activityid", Convert.ToString(alD_ID));
                        httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                        using (response = _client.SendAsync(httpRequestMessage).Result)
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                JavaScriptSerializer se = new JavaScriptSerializer();
                                json = response.Content.ReadAsStringAsync().Result;
                                requestEnrollRoot = new JavaScriptSerializer().Deserialize<RequestEnrollRoot>(json);
                                if (requestEnrollRoot.data != null && requestEnrollRoot.data.Count > 0)
                                {
                                    requestEnroll = requestEnrollRoot.data[0];
                                }
                            }
                        }

                    }
                }
                return requestEnroll;
            }
            catch (Exception ex)
            {
                _loggerClient.LogException("Error in EnrollActivitiesService.RequestEnrollInfo()" + ex.Message);
                return requestEnroll;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, requestEnroll, response, SessionHelper.CurrentSession.UserName);
            }
        }
        public bool SendRequest(string stu_no, string alD_ID, int couponCount, string radioselect, int id, string comment)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            bool Json = false;
            try
            {
                var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();
                if (token != null)
                {
                    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                    {
                        string AuthorizedToken = token.token_type + " " + token.access_token;

                        using (httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.PostActivityRegisterDetail))
                        {
                            couponCount = couponCount > 0 ? couponCount : 1;
                            var data = "{\"ActivityID\":\"" + alD_ID + "\",\"STU_NO\":\"" + stu_no + "\",\"STU_COUNT\":\"" + couponCount
                                + "\",\"ScheduleID\":\"" + radioselect + "\",\"ScheduleDescription\":\"" + id + "\"}";

                            HttpContent content = new StringContent(data, Encoding.UTF8, "application/json");

                            httpRequestMessage.Content = content;
                            httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                            using (response = _client.SendAsync(httpRequestMessage).Result)
                            {
                                if (response.IsSuccessStatusCode)
                                {
                                    JavaScriptSerializer se = new JavaScriptSerializer();
                                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                                    JasonResultFileter result = new JavaScriptSerializer().Deserialize<JasonResultFileter>(jsonDataProviders);
                                    Json = Convert.ToBoolean(result.success);
                                }

                            }
                        }
                    }
                }
                return Json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogException("Error in EnrollActivitiesService.SendRequest()" + ex.Message);
                return Json;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, Json, response, SessionHelper.CurrentSession.UserName);
            }
        }
        public IEnumerable<EnrolllActivityModel> GetActivity(string StudentNo, int ActivityId=0, int activityenrollmentId=0)
        {
            string json = "";
            IEnumerable<EnrolllActivityModel> obj = new List<EnrolllActivityModel>();
            EnrolllActivityRoot activityDetails = new EnrolllActivityRoot();
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            try
            {
                var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();

                if (token != null)
                {
                    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                    {
                        //string AuthorizedToken = token.token_type + " " + token.access_token;
                        httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.GetActivityById);
                        httpRequestMessage.Headers.Add("stuno", StudentNo.ToString());
                        httpRequestMessage.Headers.Add("activityId", Convert.ToString(ActivityId)); 
                        httpRequestMessage.Headers.Add("activityenrollmentId", Convert.ToString(ActivityId));
                        httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);
                       ///httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                        using (response = _client.SendAsync(httpRequestMessage).Result)
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                JavaScriptSerializer se = new JavaScriptSerializer();
                                json = response.Content.ReadAsStringAsync().Result;

                                activityDetails = new JavaScriptSerializer().Deserialize<EnrolllActivityRoot>(json);

                                if (activityDetails.data != null && activityDetails.data.Count > 0)
                                    obj = activityDetails.data;
                            }
                        }

                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                _loggerClient.LogException("Error in EnrollActivitiesService.GetActivity()" + ex.Message);
                return obj;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, obj, response, SessionHelper.CurrentSession.UserName);
            }
        }


        public bool PostActivityFeedback(ActiveFeedback feedback)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            bool Json = false;
            try
            {
                var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();
                if (token != null)
                {

                    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                    {
                        string AuthorizedToken = token.token_type + " " + token.access_token;

                        using (httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.PostActivityFeedbackAPI))
                        {

                            var data = JsonConvert.SerializeObject(feedback);

                            HttpContent content = new StringContent(data, Encoding.UTF8, "application/json");

                            httpRequestMessage.Content = content;
                            httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                            using (response = _client.SendAsync(httpRequestMessage).Result)
                            {
                                if (response.IsSuccessStatusCode)
                                {
                                    JavaScriptSerializer se = new JavaScriptSerializer();
                                    var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                                    JasonResultFileter result = new JavaScriptSerializer().Deserialize<JasonResultFileter>(jsonDataProviders);
                                    Json = Convert.ToBoolean(result.success);
                                }

                            }
                        }
                    }
                }
                return Json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogException("Error in EnrollActivitiesService.PostActivityFeedback()" + ex.Message);
                return Json;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, Json, response, SessionHelper.CurrentSession.UserName);
            }
        }
        public bool PostUnsubscribeActivity(int APD_ID)
        {
            bool Json = false;
            string json = "";
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            try
            {
                
                var token = _phoenixAPIParentCornerService.GetAuthorizationTokenAsync();               
                if (token != null)
                {

                    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                    {
                        string AuthorizedToken = token.token_type + " " + token.access_token;
                        httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, _phoenixAPIParentCornerService.GetParentCornerBaseUrl + _phoenixAPIParentCornerService.PostUnsubscribeActivityAPI);
                        httpRequestMessage.Headers.Add("APD_ID", APD_ID.ToString());
                        httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                        using (response = _client.SendAsync(httpRequestMessage).Result)
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                JavaScriptSerializer se = new JavaScriptSerializer();
                                json = response.Content.ReadAsStringAsync().Result;

                                JasonResultFileter result = new JavaScriptSerializer().Deserialize<JasonResultFileter>(json);

                                Json = Convert.ToBoolean(result.success);
                            }
                        }
                    }
                }
                return Json;
            }
            catch (Exception ex)
            {
                _loggerClient.LogException("Error in EnrollActivitiesService.PostUnsubscribeActivity()" + ex.Message);
                return Json;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, Json, response, SessionHelper.CurrentSession.UserName);
            }
        }
    }
}
