﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;

namespace Phoenix.VLE.Web.Services
{
    public interface ITemplateService
    {
        IEnumerable<Template> GetAll();
        Template GetById(int id);
        int Insert(Template entity);
        bool Delete(int id);
        int Update(Template entityToUpdate);

        IEnumerable<Template> GetTemplatesBySchoolId(int schoolId, int? userId, bool isActive, string TemplateType,int? status);
        Template GetTemplateDetail(int? templateId, int? schoolId, string templateType, string period, int? userId, bool? isActive, bool? includeTemplateField);
        TemplateField GetTemplateFieldById(int templateFieldId);
        IEnumerable<TemplateField> GetTemplateFieldByTemplateId(int templateId, long UserId,int groupid, bool isActive);
        bool SaveTemplateImageData(TemplateEdit templateData);
        IEnumerable<Student> GetCertificateMappingStudents(int id, int pageIndex, int v, int templateId, string schoolGroupIds, string searchString);
        bool SaveTemplateStudentMapping(StudentCertificateMapping model);
        bool SaveCertificateAssignedColumns(List<TemplateField> templateFieldList);
        IEnumerable<Template> GetStudentAssignedCertificates(long userId, PlanTemplateTypes templateTypes);
        IEnumerable<TemplateFieldMapping> GetTemplateFieldData(int templateId, long userId);
        bool SaveTemplateCertificateStatus(TemplateEdit model);
        IEnumerable<CertificateTeacherMapping> GetSchoolTemplateApproverDelegates(long schoolId, PlanTemplateTypes templateType, long? departmentId);
        bool SaveCertificateApprovalTeacher(CertificateTeacherMapping model);
        IEnumerable<TemplateFieldMapping> GetTemplatePreviewData(int templateId, long schoolId);
        IEnumerable<CertificateColumns> GetCertificateColumns(int certificateColumnType);
        IEnumerable<TemplateCollectionList> GetTimeTableList(long UserId, string SelectDate);
        IEnumerable<TemplateCollectionList> GetGradeList(long Groupid,long SchoolId);
        IEnumerable<TemplateFieldMapping> GetTemplateFieldData(int templateId, List<long> userIds, string dynamicParameter = null);
        IEnumerable<CertificateReportView> GetCertificateReportData();
        byte[] GenerateCertificateReportFile(string filePath);
    }
}
