﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;


namespace Phoenix.VLE.Web.Services
{
    public interface IPlanSchemeDetailService
    {
        IEnumerable<PlanSchemeDetail> GetAll();
        PlanSchemeDetail GetById(int id);
        int Insert(PlanSchemeDetail entity);
        int SavePlanSchemeFields(List<PlanSchemeField> planSchemeField);
        IEnumerable<PlanSchemeField> GetPlanSchemeFields(long PlanSchemeId);
        bool Delete(int id);
        int Update(PlanSchemeDetail entityToUpdate);
        int UpdatePlanSchemeStatus(PlanSchemeDetail entityToUpdate);

        IEnumerable<PlanSchemeDetail> GetPlanSchemeBySchoolId(int schoolId, int? userId, int? status, bool? isActive,
            bool? SharedWithMe = null, bool? CreatedByMe = null,
            string MISGroupId = "", string OtherGroupId = "", string CourseId = "", string GradeId = "",
            int? pageIndex=null,int? PageSize=null,string searchStrin="",string sortBy="");
        IEnumerable<PlanSchemeDetail> GetPlanSchemeDetail(int? planSchemeDetailId, int? templateId, int? schoolId, string templateType, int? status, int? userId, bool? isActive);

        long UpdateSharedLessonPlanDetial(PlanSchemeDetail planSchemeDetail, long UserId, string Operation);
        IEnumerable<SharedLessonPlanTeacherList> GetSharedLessonPlanTeacherList(string PlanSchemeId);

        IEnumerable<TopicSubTopicStructure> GetUnitStructure(string groupIds);

        IEnumerable<LessonPlanFilterModule> GetLessonPlanFilterModule(long SchoolId, long TeacherId);
        IEnumerable<PlanSchemeDetail> GetPendingForApprovalLessonPlan(long SchoolId);

        IEnumerable<PlanSchemeDetail> GetPlanSchemesByUnitId(long unitId, int? status);
        IEnumerable<SchoolGroup> GetUserGroups(long Userid);


    }
}
