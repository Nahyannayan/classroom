﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Phoenix.Common.Enums;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public class TemplateService : ITemplateService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/template";
        #endregion

        public TemplateService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool SaveTemplateCertificateStatus(TemplateEdit model)
        {
            var template = new Template();
            EntityMapper<TemplateEdit, Template>.Map(model, template);
            var uri = API.Template.SaveTemplateCertificateStatus(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, template).Result;
            return response.IsSuccessStatusCode;
        }

        public bool Delete(int id)
        {
            var uri = API.Template.DeleteTemplate(_path, id, (int)SessionHelper.CurrentSession.Id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<Template> GetAll()
        {
            var result = new List<Template>();
            var uri = API.Template.GetTemplates(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Template>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public Template GetById(int id)
        {
            var result = new Template();
            var uri = API.Template.GetTemplateById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, Template>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Student> GetCertificateMappingStudents(int id, int pageIndex, int pageSize, int templateId, string schoolGroupIds, string searchString)
        {
            IEnumerable<Student> studentList = new List<Student>();
            var uri = API.Template.GetCertificateMappingStudents(_path, id, pageIndex, pageSize, templateId, schoolGroupIds, searchString);
            HttpResponseMessage httpResponseMessage = _client.GetAsync(uri).Result;
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var jsonDataProviders = httpResponseMessage.Content.ReadAsStringAsync().Result;
                studentList = EntityMapper<string, IEnumerable<Student>>.MapFromJson(jsonDataProviders);
            }
            return studentList;
        }

        public IEnumerable<Template> GetStudentAssignedCertificates(long userId, PlanTemplateTypes templateType)
        {
            IEnumerable<Template> templateList = new List<Template>();
            var uri = API.Template.GetStudentAssignedCertificates(_path, userId, (short)templateType);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                templateList = EntityMapper<string, IEnumerable<Template>>.MapFromJson(jsonDataProviders);
            }
            return templateList;
        }

        public IEnumerable<CertificateTeacherMapping> GetSchoolTemplateApproverDelegates(long schoolId, PlanTemplateTypes templateType, long? departmentId)
        {
            IEnumerable<CertificateTeacherMapping> teacherList = new List<CertificateTeacherMapping>();
            var uri = API.Template.GetSchoolTemplateApproverDelegates(_path, schoolId, (short)templateType, departmentId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                teacherList = EntityMapper<string, IEnumerable<CertificateTeacherMapping>>.MapFromJson(jsonDataProviders);
            }
            return teacherList;
        }

        public Template GetTemplateDetail(int? templateId, int? schoolId, string templateType, string period, int? userId, bool? isActive, bool? includeTemplateField)
        {
            var result = new Template();
            var uri = API.Template.GetTemplateDetail(_path, templateId, schoolId, templateType, period, userId, isActive, includeTemplateField);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, Template>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public TemplateField GetTemplateFieldById(int templateFieldId)
        {
            var result = new TemplateField();
            var uri = API.Template.GetTemplateFieldById(_path, templateFieldId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, TemplateField>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<TemplateField> GetTemplateFieldByTemplateId(int templateId, long UserId, int groupid, bool isActive)
        {
            var result = new List<TemplateField>();
            var uri = API.Template.GetTemplateFieldByTemplateId(_path, templateId, isActive, UserId, groupid);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<TemplateField>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<TemplateFieldMapping> GetTemplateFieldData(int templateId, long userId)
        {
            IEnumerable<TemplateFieldMapping> result = new List<TemplateFieldMapping>();
            var uri = API.Template.GetTemplateFieldData(_path, templateId, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, IEnumerable<TemplateFieldMapping>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<Template> GetTemplatesBySchoolId(int schoolId, int? userId, bool isActive, string templateType, int? status)
        {
            var result = new List<Template>();
            var uri = API.Template.GetTemplatesBySchoolid(_path, schoolId, userId, isActive, templateType, status);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<Template>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int Insert(Template entity)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.Template.AddTemplate(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public bool SaveCertificateApprovalTeacher(CertificateTeacherMapping model)
        {
            var uri = API.Template.SaveCertificateApprovalTeacher(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
        }

        public bool SaveCertificateAssignedColumns(List<TemplateField> templateFieldList)
        {
            var uri = API.Template.SaveCertificateAssignedColumns(_path);
            HttpResponseMessage httpResponseMessage = _client.PostAsJsonAsync(uri, templateFieldList).Result;
            return httpResponseMessage.IsSuccessStatusCode;
        }

        public bool SaveTemplateImageData(TemplateEdit templateData)
        {
            var templateModel = new Template();
            EntityMapper<TemplateEdit, Template>.Map(templateData, templateModel);
            var uri = API.Template.SaveTemplateImageData(_path);
            HttpResponseMessage httpResponseMessage = _client.PostAsJsonAsync(uri, templateModel).Result;
            return httpResponseMessage.IsSuccessStatusCode;
        }

        public bool SaveTemplateStudentMapping(StudentCertificateMapping model)
        {
            var uri = API.Template.SaveTemplateStudentMapping(_path);
            HttpResponseMessage httpResponseMessage = _client.PostAsJsonAsync(uri, model).Result;
            return httpResponseMessage.IsSuccessStatusCode;
        }

        public int Update(Template entityToUpdate)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.Template.UpdateTemplate(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entityToUpdate).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public IEnumerable<TemplateFieldMapping> GetTemplatePreviewData(int templateId, long schoolId)
        {
            var result = new List<TemplateFieldMapping>();
            var uri = API.Template.GetTemplatePreviewData(_path, templateId, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<TemplateFieldMapping>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<CertificateColumns> GetCertificateColumns(int certificateColumnType)
        {
            var result = new List<CertificateColumns>();
            var uri = API.Template.GetCertificateColumns(_path, certificateColumnType);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<CertificateColumns>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<TemplateCollectionList> GetTimeTableList(long UserId, string SelectDate)
        {
            var result = new List<TemplateCollectionList>();
            var uri = API.Template.GetTimeTableList(_path, UserId, SelectDate);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<TemplateCollectionList>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<TemplateCollectionList> GetGradeList(long Groupid, long SchoolId)
        {
            var result = new List<TemplateCollectionList>();
            var uri = API.Template.GetGradeList(_path, Groupid, SchoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<TemplateCollectionList>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<TemplateFieldMapping> GetTemplateFieldData(int templateId, List<long> userIds, string dynamicParameter = null)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.Template.GetTemplateFieldData(_path), new TemplateMappingData { TemplateId = templateId, UserIds = userIds, DynamicParameters = dynamicParameter }).Result;
            return response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync().Result.FromJsonToType<List<TemplateFieldMapping>>() : new List<TemplateFieldMapping>();
        }

        public IEnumerable<CertificateReportView> GetCertificateReportData()
        {
            IEnumerable<CertificateReportView> lst = new List<CertificateReportView>();
            var uri = API.Template.GetCertificateReportData(_path, SessionHelper.CurrentSession.SchoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lst = EntityMapper<string, IEnumerable<CertificateReportView>>.MapFromJson(jsonDataProviders);
            }
            return lst;
        }

        public byte[] GenerateCertificateReportFile(string filePath)
        {
            var certificateList = GetCertificateReportData();
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(filePath, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();
                    WorkbookStylesPart stylesPart = workbookPart.AddNewPart<WorkbookStylesPart>();
                    stylesPart.Stylesheet = GenerateStyleSheet();
                    stylesPart.Stylesheet.Save();
                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();
                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Certificate Report" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());
                    
                    // Constructing header
                    Row row = new Row();

                    row.Append(
                       new Cell() { CellValue = new CellValue("Certificate Title"), DataType = CellValues.String, StyleIndex= 1 },
                       new Cell() { CellValue = new CellValue("Certificate Description"), DataType = CellValues.String, StyleIndex = 1 },
                       new Cell() { CellValue = new CellValue("Requester Name"), DataType = CellValues.String, StyleIndex = 1 },
                       new Cell() { CellValue = new CellValue("Sent for approval (date)"), DataType = CellValues.String , StyleIndex = 1 },
                       new Cell() { CellValue = new CellValue("Approver/Rejector Name"), DataType = CellValues.String, StyleIndex = 1 },
                       new Cell() { CellValue = new CellValue("Status"), DataType = CellValues.String, StyleIndex = 1 },
                       new Cell() { CellValue = new CellValue("Approval/Rejection date"), DataType = CellValues.String, StyleIndex = 1 }
                    );
                    
                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    // Inserting each value
                    foreach (var c in certificateList)
                    {
                        row = new Row();

                        row.Append(
                            new Cell() { CellValue = new CellValue(c.Title), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue(c.Description), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue(c.RequesterName), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue(c.ApprovalSentOn), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue(c.FullName), DataType = CellValues.String},
                            new Cell() { CellValue = new CellValue(c.TemplateStatus), DataType = CellValues.String },
                            new Cell() { CellValue = new CellValue(c.StatusChangedDate), DataType = CellValues.String }
                         );

                        sheetData.AppendChild(row);
                    }

                    worksheetPart.Worksheet.Save();
                }

                var data = System.IO.File.ReadAllBytes(filePath);

                System.IO.File.Delete(filePath);

                return data;
            }
            catch
            {
                throw;
            }
        }

        private static Stylesheet GenerateStyleSheet()
        {
            return new Stylesheet(
                new Fonts(
                    new Font(                                                               // Index 0 - The default font.
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(                                                               // Index 1 - The bold font.
                        new Bold(),
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(                                                               // Index 2 - The Italic font.
                        new Italic(),
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(
                        new FontSize() { Val = 16 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Times New Roman" })
                ),
                new Fills(
                    new Fill(
                        new PatternFill() { PatternType = PatternValues.None }),
                    new Fill(
                        new PatternFill() { PatternType = PatternValues.Gray125 }),
                    new Fill(
                        new PatternFill(
                            new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "FFFFFF00" } }
                        )
                        { PatternType = PatternValues.Solid })
                ),
                new Borders(
                    new Border(                                                         // Index 0 - The default border.
                        new LeftBorder(),
                        new RightBorder(),
                        new TopBorder(),
                        new BottomBorder(),
                        new DiagonalBorder()),
                    new Border(                                                         // Index 1 - Applies a Left, Right, Top, Bottom border to a cell
                        new LeftBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new RightBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new TopBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new BottomBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                ),
                new CellFormats(
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 0 },                          // Index 0 - The default cell style.  If a cell does not have a style index applied it will use this style combination instead
                    new CellFormat() { FontId = 1, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 1 - Bold 
                    new CellFormat() { FontId = 2, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 2 - Italic
                    new CellFormat() { FontId = 3, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 3 - Times Roman
                    new CellFormat() { FontId = 0, FillId = 2, BorderId = 0, ApplyFill = true },       // Index 4 - Yellow Fill
                    new CellFormat(                                                                   // Index 5 - Alignment
                        new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center }
                    )
                    { FontId = 0, FillId = 0, BorderId = 0, ApplyAlignment = true },
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }      // Index 6 - Border
                )
            );
        }
    }
}