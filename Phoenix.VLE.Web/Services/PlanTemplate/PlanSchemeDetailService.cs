﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public class PlanSchemeDetailService : IPlanSchemeDetailService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/planschemedetail";
        #endregion

        public PlanSchemeDetailService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public bool Delete(int id)
        {
            var uri = API.PlanSchemeDetail.DeletePlanSchemeDetail(_path, id, (int)SessionHelper.CurrentSession.Id);
            HttpResponseMessage response = _client.DeleteAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<PlanSchemeDetail> GetAll()
        {
            var result = new List<PlanSchemeDetail>();
            var uri = API.PlanSchemeDetail.GetAllPlanSchemeDetail(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<PlanSchemeDetail>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public PlanSchemeDetail GetById(int id)
        {
            var result = new PlanSchemeDetail();
            var uri = API.PlanSchemeDetail.GetPlanSchemeDetailById(_path, id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, PlanSchemeDetail>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<PlanSchemeDetail> GetPlanSchemeBySchoolId(int schoolId, int? userId, int? status, bool? isActive,
            bool? SharedWithMe = null, bool? CreatedByMe = null,
            string MISGroupId = null, string OtherGroupId = null, string CourseId = null, string GradeId = null,
            int? pageIndex = null, int? PageSize = null, string searchString = "", string sortBy = "")
        {
            var result = new List<PlanSchemeDetail>();
            var uri = API.PlanSchemeDetail.GetPlanSchemeDetailBySchoolId
                (_path, schoolId, userId, status, isActive, SharedWithMe, CreatedByMe, MISGroupId, OtherGroupId, CourseId, GradeId,
                pageIndex, PageSize, searchString, sortBy);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<PlanSchemeDetail>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<PlanSchemeDetail> GetPlanSchemeDetail(int? planSchemeDetailId, int? templateId, int? schoolId, string templateType, int? status, int? userId, bool? isActive)
        {
            var result = new List<PlanSchemeDetail>();
            var uri = API.PlanSchemeDetail.GetPlanSchemeDetail(_path, planSchemeDetailId, templateId, schoolId, templateType, status, userId, isActive);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<PlanSchemeDetail>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int Insert(PlanSchemeDetail entity)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.PlanSchemeDetail.AddPlanSchemeDetail(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entity).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int Update(PlanSchemeDetail entityToUpdate)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.PlanSchemeDetail.UpdatePlanSchemeDetail(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entityToUpdate).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public int UpdatePlanSchemeStatus(PlanSchemeDetail entityToUpdate)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.PlanSchemeDetail.UpdatePlanSchemeStatus(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, entityToUpdate).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public long UpdateSharedLessonPlanDetial(PlanSchemeDetail planSchemeDetail, long UserId, string Operation)
        {
            long result = 0;
            var uri = string.Empty;
            HttpResponseMessage response;
            string jsonDataProviders;
            if (Operation == "update")
            {
                uri = API.PlanSchemeDetail.UpdateSharedLessonPlanDetial(_path, planSchemeDetail.PlanSchemeDetailId, planSchemeDetail.SelectedTeacherIds, UserId, Operation);
                response = _client.GetAsync(uri).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = planSchemeDetail.PlanSchemeDetailId;
                }

                return result;
            }

            uri = API.PlanSchemeDetail.UpdateSharedLessonPlanDetial(_path, planSchemeDetail.PlanSchemeDetailId, planSchemeDetail.SelectedTeacherIds, UserId, Operation);
            response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                //jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = planSchemeDetail.PlanSchemeDetailId;
            }
            return result;
        }
        public IEnumerable<SharedLessonPlanTeacherList> GetSharedLessonPlanTeacherList(string PlanSchemeId)
        {
            var result = new List<SharedLessonPlanTeacherList>();
            var uri = API.PlanSchemeDetail.GetSharedLessonPlanTeacherList(_path, PlanSchemeId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SharedLessonPlanTeacherList>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<TopicSubTopicStructure> GetUnitStructure(string groupIds)
        {
            var result = new List<TopicSubTopicStructure>();
            var uri = API.PlanSchemeDetail.GetUnitStructure(_path, groupIds);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<TopicSubTopicStructure>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<LessonPlanFilterModule> GetLessonPlanFilterModule(long SchoolId, long TeacherId)
        {
            var result = new List<LessonPlanFilterModule>();
            var uri = API.PlanSchemeDetail.GetLessonPlanFilterModule(_path, SchoolId, TeacherId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<LessonPlanFilterModule>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<PlanSchemeDetail> GetPendingForApprovalLessonPlan(long SchoolId)
        {
            var result = new List<PlanSchemeDetail>();
            var uri = API.PlanSchemeDetail.GetPendingForApprovalLessonPlan(_path, SchoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<PlanSchemeDetail>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public IEnumerable<PlanSchemeField> GetPlanSchemeFields(long PlanSchemeId)
        {
            var result = new List<PlanSchemeField>();
            var uri = API.PlanSchemeDetail.GetPlanSchemeFields(_path, PlanSchemeId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<PlanSchemeField>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }

        public int SavePlanSchemeFields(List<PlanSchemeField> planSchemeField)
        {
            var result = 0;
            var uri = string.Empty;
            uri = API.PlanSchemeDetail.SavePlanSchemeFields(_path);

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, planSchemeField).Result;
            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
            result = Convert.ToInt32(jsonDataProviders);
            return result;
        }

        public IEnumerable<PlanSchemeDetail> GetPlanSchemesByUnitId(long unitId, int? status)
        {
            var result = new List<PlanSchemeDetail>();
            var uri = API.PlanSchemeDetail.GetPlanSchemesByUnitId
                (_path, unitId, status);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<PlanSchemeDetail>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<SchoolGroup> GetUserGroups(long Userid)
        {
            var result = new List<SchoolGroup>();
            var uri = API.PlanSchemeDetail.GetUserGroups
                (_path, Userid);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = EntityMapper<string, List<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return result;
        }
    }
}