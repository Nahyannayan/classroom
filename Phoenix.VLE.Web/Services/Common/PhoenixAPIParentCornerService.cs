﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models.HSEObjects;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class PhoenixAPIParentCornerService : IPhoenixAPIParentCornerService
    {
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        private ILoggerClient _loggerClient;

        public PhoenixAPIParentCornerService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
            _loggerClient = LoggerClient.Instance; 
        }



        
      
       // readonly string _path = "api/v1/TimeTable";
        readonly string tokenAPI = ConfigurationManager.AppSettings["TokenAPI"];
        readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"];

        #region Common declaration
        readonly string strPhoenixAPIParentCornerBaseUrl = ConfigurationManager.AppSettings["PhoenixBaseUriParentCorner"];
        public string GetParentCornerBaseUrl { get { return strPhoenixAPIParentCornerBaseUrl; } }
        #endregion

        #region Set subjects for examination
        readonly string strGeExamPaperListAPI = "api/CurriculumRep/GetExamPaperSelectionList";
        readonly string strGetSubjectWiseAPI = "api/CurriculumRep/GetSubjectwisePaperDetailList";
        readonly string strSaveExamPaperAPI = "api/CurriculumRep/SaveExamPaperDetails";
        readonly string strRemoveExamPaperAPI = "api/CurriculumRep/RemovePaperFromSelection";
        readonly string strExamApprovalRequestAPI = "api/CurriculumRep/ExamSetupRequestForApproval";
        readonly string strGetSelectedPaperDetailsAPI = "api/CurriculumRep/GetSelectedPaperDetailList";
       
        public string GetExamPaperListAPI { get { return strGeExamPaperListAPI; } }
        public string GetSubjectWiseAPI { get { return strGetSubjectWiseAPI; } }
        public string SaveExamPaperAPI { get { return strSaveExamPaperAPI; } }
        public string RemoveExamPaperAPI { get { return strRemoveExamPaperAPI; } }
        public string ExamApprovalRequestAPI { get { return strExamApprovalRequestAPI; } }
        public string GetSelectedPaperDetailsAPI { get { return strGetSelectedPaperDetailsAPI; } }
        #endregion

        #region Hifz tracker
        readonly string strGetHifzTrackerList = "api/StudentInfo/GetHifzTrackerList";
        readonly string strSaveHifzTrackerAPI = "api/StudentInfo/SaveHifzTracker";
        public string GetHifzTrackerListAPI { get { return strGetHifzTrackerList; } }
        public string SaveHifzTrackerAPI { get { return strSaveHifzTrackerAPI; } }
        #endregion

        #region Student UpdateDetails
        readonly string strMastersAPI = "api/masters";
        readonly string strGET_PARENT_CONTACT = "api/StudentInfo/GET_PARENT_CONTACT";
        readonly string strGET_STUDENT_HEALTH_RESTRICTIONS = "api/StudentInfo/GET_STUDENT_HEALTH_RESTRICTIONS";
        readonly string strGET_STUDENT_DISEASES = "api/StudentInfo/GET_STUDENT_DISEASES";
        readonly string strGET_STUDENT_DISEASES_DETAILS = "api/StudentInfo/GET_STUDENT_DISEASES_DETAILS";
        readonly string strUPDATE_STUDENT = "api/StudentInfo/UPDATE_STUDENT";
        readonly string strPOST_PARENT_INFO = "api/StudentInfo/POST_PARENT_INFO";
        readonly string strUPDATE_STUDENT_HEALTH = "api/StudentInfo/UPDATE_STUDENT_HEALTH";
        readonly string strPOST_STUDENT_CONSENT_INFO = "api/StudentInfo/POST_STUDENT_CONSENT_INFO";

        public string MastersAPI { get { return strMastersAPI; } }
        public string GET_PARENT_CONTACT { get { return strGET_PARENT_CONTACT; } }
        public string GET_STUDENT_HEALTH_RESTRICTIONS { get { return strGET_STUDENT_HEALTH_RESTRICTIONS; } }
        public string GET_STUDENT_DISEASES { get { return strGET_STUDENT_DISEASES; } }
        public string GET_STUDENT_DISEASES_DETAILS { get { return strGET_STUDENT_DISEASES_DETAILS; } }
        public string UPDATE_STUDENT { get { return strUPDATE_STUDENT; } }
        public string POST_PARENT_INFO { get { return strPOST_PARENT_INFO; } }
        public string UPDATE_STUDENT_HEALTH { get { return strUPDATE_STUDENT_HEALTH; } }
        public string POST_STUDENT_CONSENT_INFO { get { return strPOST_STUDENT_CONSENT_INFO; } }
        #endregion
        #region
        readonly string strGetCoursesWithCatgoryList = "api/CurriculumRep/GetCoursesWithCatgoryList";
        readonly string strSaveCourseSelection = "api/CurriculumRep/SaveCourseSelection";
        readonly string StrTwsOptionGuidelines = "https://oasis.gemseducation.com/curriculum/TwsOptionGuidelines.pdf";
        #endregion

        #region Enroll Activity
        readonly string strGET_LIST_OF_ACTIVITIES = "api/Activity/GET_LIST_OF_ACTIVITIES";
        readonly string strGET_ACTIVITY_SHORT_INFO = "api/Activity/GET_ACTIVITY_SHORT_INFO";
        readonly string strPostActivityRegisterDetail = "api/StudentInfo/PostActivityRegisterDetail";
        readonly string strPostActivityFeedbackAPI = "api/Activity/PostActivityFeedback";
        readonly string strPostUnsubscribeActivityAPI = "api/StudentInfo/PostUnsubscribeActivity";
        readonly string strallLeaveDetailsAPI = "api/leave/GET_LEAVE";
        readonly string straddLeaveRequestAPI = "api/leave/LEAVE_REQUEST";
        readonly string strGetActivityById = "api/Activity/GET_ACTIVITY";
        public string GET_LIST_OF_ACTIVITIES { get { return strGET_LIST_OF_ACTIVITIES; } }
        public string GET_ACTIVITY_SHORT_INFO { get { return strGET_ACTIVITY_SHORT_INFO; } }
        public string PostActivityRegisterDetail { get { return strPostActivityRegisterDetail; } }
        public string PostActivityFeedbackAPI { get { return strPostActivityFeedbackAPI; } }
        public string PostUnsubscribeActivityAPI { get { return strPostUnsubscribeActivityAPI; } }
        public string AllLeaveDetailsAPI { get { return strallLeaveDetailsAPI; } }
        public string AddLeaveRequestAPI { get { return straddLeaveRequestAPI; } }
        public string GetActivityById { get { return strGetActivityById; } }
        #endregion
        #region Select Subject
        public string GetCoursesWithCatgoryList { get { return strGetCoursesWithCatgoryList; } }
        public string SaveCourseSelection { get { return strSaveCourseSelection; } }
        public string TwsOptionGuidelines { get { return StrTwsOptionGuidelines; } }
        
        #endregion


        #region Get Autherization token
        public TokenResult GetAuthorizationTokenAsync()
        {
            TokenResult JsonDeserilize = new TokenResult();
            JsonDeserilize.access_token = SessionHelper.CurrentSession.PhoenixAccessToken;
            JsonDeserilize.token_type = "Bearer";
            //if (HttpContext.Current.Session["AccessToken"] != null)
            //{
            //    JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
            //}
            //else
            //{
            //    _client.DefaultRequestHeaders.Accept.Clear();
            //    var uri = tokenAPI;
            //    string _ContentType = "application/x-www-form-urlencoded";
            //    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //    var nvc = new List<KeyValuePair<string, string>>();
            //    nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
            //    nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
            //    nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
            //    nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

            //    using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
            //    {
            //        using (var result = _client.SendAsync(test).Result)
            //        {
            //            if (result.IsSuccessStatusCode)
            //            {
            //                var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
            //                try
            //                {
            //                    JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
            //                }
            //                finally
            //                {
            //                    result.Dispose();
            //                }
            //            }
            //        }
            //    }

            //    HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
            //}
            return JsonDeserilize;
        }
        #endregion

        #region Error Log
        /// <summary>
        /// Author:Tejalben
        /// Summery:Add log details
        /// Date:15 Aug 200
        /// </summary>
        /// <param name="inputMode"></param>
        /// <param name="outputModel"></param>
        /// <param name="response"></param>
        /// <param name="baseUrl"></param>
        /// <returns></returns>
        public void InsertLogDetails(HttpRequestMessage httpRequestMessage, dynamic outputModel, HttpResponseMessage httpResponseMessage, string userName)
        {
            HttpResponseMessage responses = new HttpResponseMessage();
            try
            {
                //if (_client.BaseAddress == null)
                //{
                //    _client.BaseAddress = new Uri(Constants.PhoenixAPIUrl);
                //}

                //httpRequestMessage.Headers.Remove("Authorization");
                //httpRequestMessage.Headers.Remove("Accept");
                LogDetails logDetails = new LogDetails();
                logDetails.InputJson = JsonConvert.SerializeObject(httpRequestMessage.Headers);
                logDetails.OutputJson = JsonConvert.SerializeObject(outputModel);
                logDetails.IsSuccessStatusCode = httpResponseMessage.IsSuccessStatusCode;
                logDetails.StatusCode = Convert.ToString(httpResponseMessage.StatusCode);
                logDetails.Header = JsonConvert.SerializeObject(httpRequestMessage.Headers);
                logDetails.BaseUrl = Convert.ToString(httpResponseMessage.RequestMessage.RequestUri);
                logDetails.userName = userName;
                var uri = API.Users.InsertLogDetails("api/v1/Users");
                var content = new StringContent(logDetails.ToString(), Encoding.UTF8, "application/json");
                responses = _client.PostAsJsonAsync(uri, logDetails).Result;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning(ex.Message);
            }
            finally
            {
                _loggerClient.LogWarning("Add log details:" + responses.RequestMessage);
            }
        }
        #endregion

    }
}