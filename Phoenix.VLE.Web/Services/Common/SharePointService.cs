﻿using Phoenix.Common.Logger;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class SharePointService : ISharepointService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        SharepointTokenrequest apiCredentials;
        private ILoggerClient _loggerClient;
        public SharePointService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.

                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
            apiCredentials = new SharepointTokenrequest()
            {
                SharepointTokenUrl = ConfigurationManager.AppSettings["SharepointTokenUrl"],
                grant_type = ConfigurationManager.AppSettings["grant_type"],
                resource = ConfigurationManager.AppSettings["resource"],
                client_id = ConfigurationManager.AppSettings["client_id"],
                client_secret = ConfigurationManager.AppSettings["client_secret"],

            };
        }
        #endregion

        public SharepointTokenResult GetSharepointTokenOld()
        {
            SharepointTokenResult JsonDeserilize = new SharepointTokenResult();
            //added httpclient in scope for Authorization Token By Rohan P-27/08/2020
            HttpClient _phoneixapiclient = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
            _phoneixapiclient.DefaultRequestHeaders.Accept.Clear();
            string _ContentType = "application/x-www-form-urlencoded";
            _phoneixapiclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            var nvc = new List<KeyValuePair<string, string>>();
            nvc.Add(new KeyValuePair<string, string>("grant_type", apiCredentials.grant_type));
            nvc.Add(new KeyValuePair<string, string>("resource", apiCredentials.resource));
            nvc.Add(new KeyValuePair<string, string>("client_id", apiCredentials.client_id));
            nvc.Add(new KeyValuePair<string, string>("client_secret", apiCredentials.client_secret));

            using (var test = new HttpRequestMessage(HttpMethod.Post, apiCredentials.SharepointTokenUrl) { Content = new FormUrlEncodedContent(nvc) })
            {
                using (var result = _phoneixapiclient.SendAsync(test).Result)
                {
                    if (result.IsSuccessStatusCode)
                    {
                        var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
                        try
                        {
                            JsonDeserilize = new JavaScriptSerializer().Deserialize<SharepointTokenResult>(jsonDataStatus);
                        }
                        finally
                        {
                            result.Dispose();
                        }
                    }
                }
            }
            HttpContext.Current.Session["SharepointAccesstoken"] = JsonDeserilize;
            return JsonDeserilize;
        }

        //public static DateTime UnixTimeStampToDateTime(double expires_on)
        //{
        //    // Unix timestamp is seconds past epoch
        //    System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        //    dtDateTime = dtDateTime.AddSeconds(expires_on).ToUniversalTime();
        //    return dtDateTime;
        //}
    }

    public sealed class SharepointTokenHelper
    {
        private static string _token;
        private static DateTime? Expiration;
        public string Token { get { return _token; } }
        
        private SharepointTokenHelper()
        {
          
        }
        private static readonly Lazy<SharepointTokenHelper> lazy = new Lazy<SharepointTokenHelper>(() => new SharepointTokenHelper());
        public static SharepointTokenHelper Instance
        {

            get
            {
                if (_token == null || _token.Equals(string.Empty) || Expiration == null || !Expiration.HasValue || Expiration <= DateTime.Now.AddMinutes(-10).ToUniversalTime())
                {
                    SharepointTokenResult shr = new SharepointTokenResult();                  
                    shr=GetSharepointToken();
                    Expiration = UnixTimeStampToDateTime(shr.expires_on);
                    _token = shr.access_token;


                }
                return lazy.Value;
            }

        }

        private static SharepointTokenResult GetSharepointToken()
        {
            SharepointTokenrequest apiCredentials = new SharepointTokenrequest()
            {
                SharepointTokenUrl = ConfigurationManager.AppSettings["SharepointTokenUrl"],
                grant_type = ConfigurationManager.AppSettings["grant_type"],
                resource = ConfigurationManager.AppSettings["resource"],
                client_id = ConfigurationManager.AppSettings["client_id"],
                client_secret = ConfigurationManager.AppSettings["client_secret"],
            };
            SharepointTokenResult JsonDeserilize = new SharepointTokenResult();
            //added httpclient in scope for Authorization Token By Rohan P-27/08/2020
            HttpClient _phoneixapiclient = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
            _phoneixapiclient.DefaultRequestHeaders.Accept.Clear();
            string _ContentType = "application/x-www-form-urlencoded";
            _phoneixapiclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            var nvc = new List<KeyValuePair<string, string>>();
            nvc.Add(new KeyValuePair<string, string>("grant_type", apiCredentials.grant_type));
            nvc.Add(new KeyValuePair<string, string>("resource", apiCredentials.resource));
            nvc.Add(new KeyValuePair<string, string>("client_id", apiCredentials.client_id));
            nvc.Add(new KeyValuePair<string, string>("client_secret", apiCredentials.client_secret));

            using (var test = new HttpRequestMessage(HttpMethod.Post, apiCredentials.SharepointTokenUrl) { Content = new FormUrlEncodedContent(nvc) })
            {
                using (var result = _phoneixapiclient.SendAsync(test).Result)
                {
                    if (result.IsSuccessStatusCode)
                    {
                        var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
                        try
                        {
                            JsonDeserilize = new JavaScriptSerializer().Deserialize<SharepointTokenResult>(jsonDataStatus);
                        }
                        finally
                        {
                            result.Dispose();
                        }
                    }
                }
            }
            HttpContext.Current.Session["SharepointAccesstoken"] = JsonDeserilize;
            return JsonDeserilize;
        }
        private static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime.ToUniversalTime();
        }


    }


}