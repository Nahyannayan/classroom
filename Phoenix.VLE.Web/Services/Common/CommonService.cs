﻿
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Logger;
using Phoenix.Common.Models;
using Phoenix.Common.Models.ViewModels;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using CommonHelper = Phoenix.VLE.Web.Helpers.CommonHelper;


namespace Phoenix.VLE.Web.Services
{
    public class CommonService : ICommonService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Common";
        readonly string _PhoenixAPiBaseUri = ConfigurationManager.AppSettings["PhoenixAPIBaseUri"];
        readonly string SendEmailNotificationApi = ConfigurationManager.AppSettings["SendEmailNotification"];
        readonly string SendEmailApi = ConfigurationManager.AppSettings["SendEmailApi"];
        readonly string SendEmailApiCiruclation = ConfigurationManager.AppSettings["SendEmailApiCirculation"];

        ApiCredentials apiCredentials;
        private ILoggerClient _loggerClient;
        public CommonService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
            apiCredentials = new ApiCredentials()
            {
                AppId = ConfigurationManager.AppSettings["AppId"],
                Password = ConfigurationManager.AppSettings["APIPassword"],
                UserName = ConfigurationManager.AppSettings["APIUsername"],
                GrantType = ConfigurationManager.AppSettings["APIGrant_type"],
                TokenApiUrl = CommonHelper.GetPhoenixBaseURI() + ConfigurationManager.AppSettings["TokenAPI"]

            };
        }
        #endregion
        public EmailSettingsView GetEmailSettings()
        {
            var emailSettingsView = new EmailSettingsView();

            var uri = API.Common.GetEmailSettings(_path);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                emailSettingsView = EntityMapper<string, EmailSettingsView>.MapFromJson(jsonDataProviders);
            }
            return emailSettingsView;
        }

        public SystemLanguage GetSchoolCurrentLanguage(int schoolId)
        {
            var systemLanguage = new SystemLanguage();

            var uri = API.Common.GetSchoolCurrentLanguage(_path, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                systemLanguage = EntityMapper<string, SystemLanguage>.MapFromJson(jsonDataProviders);
            }
            return systemLanguage;
        }

        public bool SetSchoolCurrentLanguage(int languageId, int schoolId)
        {
            var result = false;
            var uri = API.Common.SetSchoolCurrentLanguage(_path, languageId, schoolId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToBoolean(jsonDataProviders);
            }
            return result;
        }

        public TokenResult GetAuthorizationTokenAsync(ApiCredentials apiCredentials)
        {
            TokenResult JsonDeserilize = new TokenResult();
            JsonDeserilize.access_token = SessionHelper.CurrentSession.PhoenixAccessToken;
            JsonDeserilize.token_type = "Bearer";
            //if (HttpContext.Current.Session["AccessToken"] != null)
            //{
            //    JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
            //}
            //else
            //{
            //    _client.DefaultRequestHeaders.Accept.Clear();
            //    string _ContentType = "application/x-www-form-urlencoded";
            //    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //    var nvc = new List<KeyValuePair<string, string>>();
            //    nvc.Add(new KeyValuePair<string, string>("AppId", apiCredentials.AppId));
            //    nvc.Add(new KeyValuePair<string, string>("password", apiCredentials.Password));
            //    nvc.Add(new KeyValuePair<string, string>("username", apiCredentials.UserName));
            //    nvc.Add(new KeyValuePair<string, string>("grant_type", apiCredentials.GrantType));

            //    using (var test = new HttpRequestMessage(HttpMethod.Post, apiCredentials.TokenApiUrl) { Content = new FormUrlEncodedContent(nvc) })
            //    {
            //        using (var result = _client.SendAsync(test).Result)
            //        {
            //            if (result.IsSuccessStatusCode)
            //            {
            //                var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
            //                try
            //                {
            //                    JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
            //                }
            //                finally
            //                {
            //                    result.Dispose();
            //                }
            //            }
            //        }
            //    }

            //    HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
            //}

            return JsonDeserilize;

        }
        public OperationDetails SendEmail(SendMailView sendMailView)
        {
            string postBody = JsonConvert.SerializeObject(sendMailView);
            var operationDetails = new OperationDetails();
            var token = GetAuthorizationTokenAsync(apiCredentials);
            if (token != null)
            {
                if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.token_type))
                {
                    string AuthorizedToken = token.token_type + " " + token.access_token;
                    try
                    {
                        using (HttpClient client = new HttpClient())
                        {
                            client.BaseAddress = new Uri(_PhoenixAPiBaseUri);
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                            client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                            client.DefaultRequestHeaders.Add("Authorization", AuthorizedToken);
                            HttpResponseMessage userResp = client.PostAsJsonAsync(SendEmailApi, sendMailView).Result;
                            operationDetails.Success = userResp.IsSuccessStatusCode;
                        }
                    }
                    catch (Exception ex)
                    {
                        _loggerClient = LoggerClient.Instance;
                        _loggerClient.LogException(ex);
                    }
                }
            }
            return operationDetails;
        }
        public bool SendEmailNotifications(SendEmailNotificationView sendEmailNotificationView)
        {
            bool isSent = false;

            var token = GetAuthorizationTokenAsync(apiCredentials);

            if (token != null)
            {
                if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.token_type))
                {
                    string AuthorizedToken = token.token_type + " " + token.access_token;

                    try 
                    {
                        using (HttpClient client = new HttpClient())
                        {
                            client.BaseAddress = new Uri(_PhoenixAPiBaseUri);
                            // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Authorization", AuthorizedToken);
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                            client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                            client.DefaultRequestHeaders.Add("Authorization", AuthorizedToken);
                            HttpResponseMessage userResp = client.PostAsJsonAsync(SendEmailNotificationApi, sendEmailNotificationView).Result;
                            isSent = userResp.IsSuccessStatusCode;

                        }
                    }
                    catch (Exception ex)
                    {
                        _loggerClient = LoggerClient.Instance;
                        _loggerClient.LogException(ex);
                    }
                    
                }
            }

            return isSent;
        }
        public OperationDetails SendEmailCirculation(EmailDetail sendMailView)
        {
            string postBody = JsonConvert.SerializeObject(sendMailView);
            var operationDetails = new OperationDetails();
            var token = GetAuthorizationTokenAsync(apiCredentials);
            if (token != null)
            {
                if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.token_type))
                {
                    string AuthorizedToken = token.token_type + " " + token.access_token;
                    try
                    {
                        using (HttpClient client = new HttpClient())
                        {
                            client.BaseAddress = new Uri(_PhoenixAPiBaseUri);
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                           // client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                            client.DefaultRequestHeaders.Add("Authorization", AuthorizedToken);
                            HttpResponseMessage userResp = client.PostAsJsonAsync(SendEmailApiCiruclation, sendMailView).Result;
                            operationDetails.Success = userResp.IsSuccessStatusCode;
                        }
                    }
                    catch (Exception ex)
                    {
                        _loggerClient = LoggerClient.Instance;
                        _loggerClient.LogException(ex);
                    }
                }
            }
            return operationDetails;
        }

        public bool SetUserCurrentLanguage(int languageId, long userId)
        {
            var result = false;
            var uri = API.Common.SetUserCurrentLanguage(_path, languageId, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToBoolean(jsonDataProviders);
            }
            return result;
        }
        public SystemLanguage GetUserCurrentLanguage(int languageId)
        {
            var systemLanguage = new SystemLanguage();

            var uri = API.Common.GetUserCurrentLanguage(_path, languageId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                systemLanguage = EntityMapper<string, SystemLanguage>.MapFromJson(jsonDataProviders);
            }
            return systemLanguage;
        }
        public long OperationAuditCU(OperationAudit operationAudit)
        {
            long result = 0;
            var uri = API.Common.OperationAuditCU(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, operationAudit).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToInt64(jsonDataProviders);
            }
            return result;
        }
        public IEnumerable<SystemImageViewModel> GetSystemImageList()
        {
            HttpResponseMessage responseMessage = _client.GetAsync(API.Common.GetSystemImageList(_path)).Result;
            return responseMessage.IsSuccessStatusCode ? responseMessage.Content.ReadAsStringAsync().Result.FromJsonToType<IEnumerable<SystemImageViewModel>>() : default(IEnumerable<SystemImageViewModel>);
        }


        /// <summary>
        /// Convert plain password to EncryptSHA512
        /// </summary>
        /// <param name="PlainText"></param>
        /// <returns></returns>

        public string EncryptSHA512(string PlainText)
        {
            try
            {
                SHA512 hasher = SHA512.Create();
                byte[] HashValue = hasher.ComputeHash(Encoding.ASCII.GetBytes(PlainText));
                string strHex = "";
                foreach (byte b in HashValue)
                {
                    strHex += b.ToString("x2");
                }
                return strHex;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}