﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class GetStudentInfoService : IGetStudentInfoService
    {
        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = ConfigurationManager.AppSettings["PhoenixAPIBaseUri"];
        //readonly string _path = "api/v1/TimeTable";

        //readonly string tokenAPI = ConfigurationManager.AppSettings["TokenAPI"];
        //readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        //readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        //readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        //readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"];
        readonly string GET_STUDENT = ConfigurationManager.AppSettings["GET_STUDENT"];

        private ILoggerClient _loggerClient;
        private readonly PhoenixAPIParentCornerService _phoenixAPIParentCornerService;
        public GetStudentInfoService(PhoenixAPIParentCornerService phoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                //_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                _loggerClient = LoggerClient.Instance;
            }
            _phoenixAPIParentCornerService = phoenixAPIParentCornerService;
        }
        #endregion
        public TokenResult GetAuthorizationTokenAsync()
        {
            TokenResult JsonDeserilize = new TokenResult();
            JsonDeserilize.access_token = SessionHelper.CurrentSession.PhoenixAccessToken;
            JsonDeserilize.token_type = "Bearer";
            //if (HttpContext.Current.Session["AccessToken"] != null)
            //{
            //    JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
            //}
            //else
            //{
            //    _client.DefaultRequestHeaders.Accept.Clear();
            //    var uri = tokenAPI;
            //    string _ContentType = "application/x-www-form-urlencoded";
            //    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //    var nvc = new List<KeyValuePair<string, string>>();
            //    nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
            //    nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
            //    nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
            //    nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

            //    using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
            //    {
            //        using (var result = _client.SendAsync(test).Result)
            //        {
            //            if (result.IsSuccessStatusCode)
            //            {
            //                var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
            //                try
            //                {
            //                    JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
            //                }
            //                finally
            //                {
            //                    result.Dispose();
            //                }
            //            }
            //        }
            //    }

            //    HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
            //}
            return JsonDeserilize;
        }
        public StudentInfo GetStudentInfo(string studentnumebr, string Language, string Source)
        {
            string json = "";
            StudentInfoRoot studentinforoot = new StudentInfoRoot();
            StudentInfo getstudentinfo = new StudentInfo();
            HttpResponseMessage response = new HttpResponseMessage();
            //var token = GetAuthorizationTokenAsync();

            //if (token != null)
            //{
            //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
            //    {
            //        string AuthorizedToken = token.token_type + " " + token.access_token;
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GET_STUDENT);
            try
            {


                //HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GET_STUDENT);


                httpRequestMessage.Headers.Add("stuId", studentnumebr);
                httpRequestMessage.Headers.Add("language", Language);
                httpRequestMessage.Headers.Add("source", Source);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;

                        studentinforoot = new JavaScriptSerializer().Deserialize<StudentInfoRoot>(json);
                        // requestEnrollRoot.data.academicYear = result.data[]''

                        if (studentinforoot.data != null && studentinforoot.data.Count > 0)
                        {
                            getstudentinfo = studentinforoot.data[0];
                        }
                        //requestEnroll = requestEnrollRoot.data;
                    }
                }
                return getstudentinfo;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in StudentUpdateDetailsService.GetMasterList()" + ex.Message);
                return getstudentinfo;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, getstudentinfo, response, SessionHelper.CurrentSession.UserName);
            }
        }
    }
}