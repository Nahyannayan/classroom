﻿using Phoenix.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IPhoenixAPIParentCornerService
    {
        TokenResult GetAuthorizationTokenAsync();
        void InsertLogDetails(HttpRequestMessage httpRequestMessage, dynamic outputModel, HttpResponseMessage httpResponseMessage, string userName);
        string GetExamPaperListAPI { get; }
        string GetParentCornerBaseUrl { get; }
        string GetSubjectWiseAPI { get; }
        string SaveExamPaperAPI { get; }
        string RemoveExamPaperAPI { get; }
        string ExamApprovalRequestAPI { get; }
        string GetSelectedPaperDetailsAPI { get; }
        string GetHifzTrackerListAPI { get; }
        string SaveHifzTrackerAPI { get; }
        string MastersAPI { get; }
        string GET_PARENT_CONTACT { get; }
        string GET_STUDENT_HEALTH_RESTRICTIONS { get; }
        string GET_STUDENT_DISEASES { get; }
        string UPDATE_STUDENT { get; }
        string POST_PARENT_INFO { get; }
        string UPDATE_STUDENT_HEALTH { get; }
        string POST_STUDENT_CONSENT_INFO { get; }
        string GET_STUDENT_DISEASES_DETAILS { get; }

        string GET_LIST_OF_ACTIVITIES { get; }
        string GET_ACTIVITY_SHORT_INFO { get; }
        string PostActivityRegisterDetail { get; }
        string PostActivityFeedbackAPI { get; }
        string PostUnsubscribeActivityAPI { get; }
        string AllLeaveDetailsAPI { get; }
        string AddLeaveRequestAPI { get; }
        string GetActivityById { get; }
        string TwsOptionGuidelines { get; }
        string GetCoursesWithCatgoryList { get; }
        string SaveCourseSelection { get; }
    }
}
