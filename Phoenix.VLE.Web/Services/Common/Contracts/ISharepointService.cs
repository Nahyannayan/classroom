﻿using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public interface ISharepointService
    {
        SharepointTokenResult GetSharepointTokenOld();
    }
}