﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IPaymentService
    {
        OnlinePayResponse SubmitOnlinePaymentRequest( string PaymentTo, IEnumerable<CommonStudentFee> LstStudentsFee, string ApdId ,string feeCollType, string BackUrl = "");
        IEnumerable<PaymentHistory> GetPaymentHistory(string StudentNo, string FromDate, string ToDate, string Source);
        IEnumerable<AccountStatementDetail> GetAccountDetails(string StudentNo, string FromDate, string ToDate, string Source);
        IEnumerable<AccountStatementSummary> GetAccountSummary(string StudentNo, string FromDate, string ToDate, string Source);
        OnlineFeePayment SendEmailStatementOfAccounts(string StudentNo, string FromDate, string ToDate, string StatementType);
        PaymentSummary GetPaymentSummary(string type, string PaymentRefNo, string paymentTo);
        PaymentSummary GetPaymentSummaryTransport(string type, string PaymentRefNo, string paymentTo);
        PaymentSummary GetPaymentSummaryActivity(string type, string PaymentRefNo, string paymentTo,string feeCollType);
        OnlinePayResponse SubmitOnlinePaymentRequestMulti(string PaymentTo, IEnumerable<CommonStudentFee> LstStudentsFee, string BackUrl = "", string feeCollType = "");
        OnlinePayResponse SubmitOnlinePaymentRequestExam(string PaymentTo, IEnumerable<CommonStudentFee> LstStudentsFee, string BackUrl = "");
    }
}
