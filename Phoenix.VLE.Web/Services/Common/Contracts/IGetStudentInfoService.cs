﻿using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
   public  interface IGetStudentInfoService
    {
        StudentInfo GetStudentInfo(string studentnumebr, string Language, string Source);
    }
}
