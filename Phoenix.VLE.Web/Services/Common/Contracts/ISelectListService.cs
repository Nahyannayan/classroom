﻿using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ISelectListService
    {
        IEnumerable<ListItem> GetSelectListItems(string listCode, string whereCondition, object whereConditionParamValues);
        IEnumerable<SubjectListItem> GetSubjectsByUserId(int userId);
        IEnumerable<AcademicYearList> GetAcademicYearByUserId(int userId);
        IEnumerable<ListItem> GetAssignmentCategoryMasterList();
        IEnumerable<AssignmentCategory> GetAssignmentCategoryMappingListBySchoolId(long SchoolId);
    }
}
