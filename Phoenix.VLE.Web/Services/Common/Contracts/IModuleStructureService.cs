﻿using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IModuleStructureService
    {
        IEnumerable<ModuleStructure> GetPhoenixModuleStructure(int systemLanguageId,
            long userId, string applicationCode, string traverseDirection, string moduleUrl, string moduleCode, bool excludeParent,
            string excludeModuleCodes, bool? showInMenu);
    }
}
