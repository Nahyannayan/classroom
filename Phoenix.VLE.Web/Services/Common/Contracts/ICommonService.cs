﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Models.ViewModels;
using Phoenix.Common.ViewModels;
using Phoenix.VLE.Web.Areas.SMS.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface ICommonService
    {
        EmailSettingsView GetEmailSettings();
        SystemLanguage GetSchoolCurrentLanguage(int schoolId);
        bool SetSchoolCurrentLanguage(int languageId, int schoolId);
        bool SendEmailNotifications(SendEmailNotificationView sendEmailNotificationView);
        OperationDetails SendEmail(SendMailView sendMailView);
        OperationDetails SendEmailCirculation(EmailDetail sendMailView);

        bool SetUserCurrentLanguage(int languageId, long userId);
        SystemLanguage GetUserCurrentLanguage(int languageId);
        long OperationAuditCU(OperationAudit operationAudit);
        IEnumerable<SystemImageViewModel> GetSystemImageList();
    }
}
