﻿using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class PaymentService : IPaymentService
    {

        #region private variables
        private static HttpClient _client = new HttpClient();
        readonly string _baseUrl = ConfigurationManager.AppSettings["PhoenixAPIBaseUri"];
        // readonly string _path = "api/v1/TimeTable";


        //readonly string tokenAPI = ConfigurationManager.AppSettings["TokenAPI"];
        //readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        //readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        //readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        //readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"];
        readonly string APIPaymentOrigin = ConfigurationManager.AppSettings["PaymentOrigin"];
        readonly string GetPaymentHistoryAPI = ConfigurationManager.AppSettings["GetPaymentHistoryAPI"];
        readonly string GetAccountDetailsAPI = ConfigurationManager.AppSettings["GetAccountDetailsAPI"];
        readonly string GetAccountSummaryAPI = ConfigurationManager.AppSettings["GetAccountSummaryAPI"];
        readonly string SendMailAccountStatementAPI = ConfigurationManager.AppSettings["SendMailAccountStatementAPI"];
        readonly string GetPaymentSummaryAPI = ConfigurationManager.AppSettings["GetPaymentSummaryAPI"];
        readonly string GetPaymentSummaryActivityAPI = ConfigurationManager.AppSettings["GetPaymentSummaryActivityAPI"];
        readonly string GetPaymentSummaryTrasportAPI = ConfigurationManager.AppSettings["GetPaymentSummaryTrasportAPI"];
        readonly string AddOnlinePayment = ConfigurationManager.AppSettings["AddFeeDetailsAPI"];
        readonly string AddFeeDetailsMultiAPI = ConfigurationManager.AppSettings["AddFeeDetailsMultiAPI"];
        readonly string AddOnlinePaymentExamAPI = ConfigurationManager.AppSettings["AddOnlinePaymentExamAPI"];

        private ILoggerClient _loggerClient;
        private IPhoenixAPIParentCornerService _phoenixAPIParentCornerService;
        public PaymentService(IPhoenixAPIParentCornerService phoenixAPIParentCornerService)
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                //  _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);               
            }
            _loggerClient = LoggerClient.Instance;
            _phoenixAPIParentCornerService = phoenixAPIParentCornerService;
        }
        #endregion
        public OnlinePayResponse SubmitOnlinePaymentRequest(string PaymentTo, IEnumerable<CommonStudentFee> LstStudentsFee, string apdId = "", string BackUrl = "", string feeCollType = "")
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string PaymentOrigin = APIPaymentOrigin;
            OnlinePayResponse onlPayResponse = new OnlinePayResponse();
            try
            {
                var payload = string.Empty;
                foreach (var item in LstStudentsFee)
                {
                    string jsonFeeDetails = JsonConvert.SerializeObject(item.StudFeeDetails.Select(m => new { m.BlockPayNow, m.AdvancePaymentAvailable, m.AdvanceDetails, m.DiscAmount, m.DueAmount, m.PayAmount, m.OriginalAmount, m.FeeDescription, m.FeeID }));
                    string jsonDiscountDetails = JsonConvert.SerializeObject(item.DiscountDetails);
                    jsonFeeDetails = jsonFeeDetails == "null" ? "[]" : jsonFeeDetails;
                    jsonDiscountDetails = jsonDiscountDetails == "null" ? "[]" : jsonDiscountDetails;

                    payload += "," + "{\"STU_NO\":\"" + item.STU_NO + "\",\"OnlinePaymentAllowed\":\"" + item.OnlinePaymentAllowed
                            + "\",\"UserMessageforOnlinePaymentBlock\":\"" + item.UserMessageforOnlinePaymentBlock
                            + "\",\"IpAddress\":\"" + item.IpAddress + "\",\"PaymentTypeID\":\"" + item.PaymentTypeID
                            + "\",\"PaymentProcessingCharge\":\"" + item.PaymentProcessingCharge
                            + "\",\"PayingAmount\":\"" + item.PayingAmount + "\",\"PayMode\":\"" + item.PayMode
                            + "\",\"FeeDetail\":" + Convert.ToString(jsonFeeDetails) + ",\"DiscountDetails\":" + Convert.ToString(jsonDiscountDetails) + "}";
                }
                payload = payload.Substring(1);
                payload = string.Format("[{0}]", payload);
                var baseUrl = HttpContext.Current.Request.Url.Host;
                if (baseUrl == "localhost")
                {
                    baseUrl = "http://" + baseUrl + ":7761";
                }
                else
                {
                    baseUrl = "https://" + baseUrl;
                }
                //BackUrl = baseUrl + BackUrl;
                _loggerClient.LogWarning(baseUrl);
                _loggerClient.LogWarning(Constants.PCPaymentGatewayRedirection);

                // var token = GetAuthorizationTokenAsync();
                // if (token != null)
                // {
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //     {
                string json = string.Empty;
                //string AuthorizedToken = token.token_type + " " + token.access_token;
                using (httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, AddOnlinePayment))
                {
                    HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
                    httpRequestMessage.Content = content;
                    httpRequestMessage.Headers.Add("paymentTo", PaymentTo);
                    httpRequestMessage.Headers.Add("paymentOrigin", PaymentOrigin);
                    httpRequestMessage.Headers.Add("apdId", apdId);
                    httpRequestMessage.Headers.Add("activityFeeCollType", feeCollType);
                    httpRequestMessage.Headers.Add("data", payload);
                    httpRequestMessage.Headers.Add("redirectURL", Convert.ToString(baseUrl + Constants.PCPaymentGatewayRedirection));
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                    //httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                    using (response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            JavaScriptSerializer se = new JavaScriptSerializer();
                            json = response.Content.ReadAsStringAsync().Result;
                            onlPayResponse = new JavaScriptSerializer().Deserialize<OnlinePayResponse>(json);

                            SessionExtensions.SetObject("PCPayGatewayRefId", Convert.ToString(onlPayResponse.PaymentRefNo));
                            SessionExtensions.SetObject("PCPayGateWaySource", Convert.ToString(PaymentTo));
                            SessionExtensions.SetObject("PCPayGateWayReturnTo", Convert.ToString(BackUrl));

                        }
                    }
                }
                // }
                //}
                return onlPayResponse;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PaymentService.SubmitOnlinePaymentRequest()" + ex.Message);
                return onlPayResponse;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, onlPayResponse, response, SessionHelper.CurrentSession.UserName);
            }

        }

        public OnlinePayResponse SubmitOnlinePaymentRequestMulti(string PaymentTo, IEnumerable<CommonStudentFee> LstStudentsFee, string BackUrl = "", string feeCollType = "")
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.Append("Payment service Step1: Start SubmitOnlinePaymentRequestMulti:");
            _loggerClient.LogWarning(errorMessage.ToString());


            string PaymentOrigin = APIPaymentOrigin;
            OnlinePayResponse onlPayResponse = new OnlinePayResponse();
            try
            {
                var payload = string.Empty;
                foreach (var item in LstStudentsFee)
                {
                    string jsonFeeDetails = JsonConvert.SerializeObject(item.StudFeeDetails.Select(m => new { m.BlockPayNow, m.AdvancePaymentAvailable, m.AdvanceDetails, m.DiscAmount, m.DueAmount, m.PayAmount, m.OriginalAmount, m.FeeDescription, m.FeeID, m.ActivityRefID }));
                    string jsonDiscountDetails = JsonConvert.SerializeObject(item.DiscountDetails);
                    jsonFeeDetails = jsonFeeDetails == "null" ? "[]" : jsonFeeDetails;
                    jsonDiscountDetails = jsonDiscountDetails == "null" ? "[]" : jsonDiscountDetails;

                    payload += "," + "{\"STU_NO\":\"" + item.STU_NO + "\",\"OnlinePaymentAllowed\":\"" + item.OnlinePaymentAllowed
                            + "\",\"UserMessageforOnlinePaymentBlock\":\"" + item.UserMessageforOnlinePaymentBlock
                            + "\",\"IpAddress\":\"" + item.IpAddress + "\",\"PaymentTypeID\":\"" + item.PaymentTypeID
                            + "\",\"PaymentProcessingCharge\":\"" + item.PaymentProcessingCharge
                            + "\",\"PayingAmount\":\"" + item.PayingAmount + "\",\"PayMode\":\"" + item.PayMode
                            + "\",\"FeeDetail\":" + Convert.ToString(jsonFeeDetails) + ",\"DiscountDetails\":" + Convert.ToString(jsonDiscountDetails) + "}";
                }
                payload = payload.Substring(1);
                payload = string.Format("[{0}]", payload);

                errorMessage.Append("Payment service Step2: payload:" + payload);
                _loggerClient.LogWarning(errorMessage.ToString());
                var baseUrl = HttpContext.Current.Request.Url.Host;
                if (baseUrl == "localhost")
                {
                    baseUrl = "https://" + baseUrl + ":7761";
                }
                else
                {
                    baseUrl = "https://" + baseUrl;
                }
                errorMessage.Append("Payment service Step3: baseUrl:" + baseUrl);
                _loggerClient.LogWarning(errorMessage.ToString());
                //BackUrl = baseUrl + BackUrl;
                _loggerClient.LogWarning(baseUrl);
                _loggerClient.LogWarning(Constants.PCPaymentGatewayRedirection);

                //var token = GetAuthorizationTokenAsync();
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                string json = string.Empty;
                //string AuthorizedToken = token.token_type + " " + token.access_token;
                using (httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, AddFeeDetailsMultiAPI))
                {
                    HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
                    httpRequestMessage.Content = content;
                    httpRequestMessage.Headers.Add("paymentTo", PaymentTo);
                    httpRequestMessage.Headers.Add("paymentOrigin", PaymentOrigin);
                    //httpRequestMessage.Headers.Add("apdId", apdId);
                    httpRequestMessage.Headers.Add("activityFeeCollType", feeCollType);
                    httpRequestMessage.Headers.Add("data", payload);
                    httpRequestMessage.Headers.Add("redirectURL", Convert.ToString(baseUrl + Constants.PCPaymentGatewayRedirection));
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                    //httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);

                    errorMessage.Append("Payment service Step4: header:" + httpRequestMessage.Headers.ToString());
                    _loggerClient.LogWarning(errorMessage.ToString());
                    using (response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        errorMessage.Append("Payment service Step5: response :" + response.RequestMessage + " headers" + response.Headers.ToString());
                        _loggerClient.LogWarning(errorMessage.ToString());
                        if (response.IsSuccessStatusCode)
                        {
                            errorMessage.Append("Payment service Step6: response :" + response.RequestMessage + " headers" + response.Headers.ToString());
                            _loggerClient.LogWarning(errorMessage.ToString());
                            JavaScriptSerializer se = new JavaScriptSerializer();
                            json = response.Content.ReadAsStringAsync().Result;
                            onlPayResponse = new JavaScriptSerializer().Deserialize<OnlinePayResponse>(json);

                            SessionExtensions.SetObject("PCPayGatewayRefId", Convert.ToString(onlPayResponse.PaymentRefNo));
                            SessionExtensions.SetObject("PCPayGateWaySource", Convert.ToString(PaymentTo));
                            SessionExtensions.SetObject("PCPayGateWayReturnTo", Convert.ToString(BackUrl));
                            errorMessage.Append("Payment service Step6: json :" + json.ToString());
                            _loggerClient.LogWarning(errorMessage.ToString());
                        }
                    }
                }
                //}
                // }
                return onlPayResponse;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in  PaymentService.SubmitOnlinePaymentRequestMulti()" + ex.Message);
                return onlPayResponse;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, onlPayResponse, response, SessionHelper.CurrentSession.UserName);
            }

        }

        public OnlinePayResponse SubmitOnlinePaymentRequestExam(string PaymentTo, IEnumerable<CommonStudentFee> LstStudentsFee, string BackUrl = "")
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string PaymentOrigin = APIPaymentOrigin;
            OnlinePayResponse onlPayResponse = new OnlinePayResponse();
            try
            {
                var payload = string.Empty;
                foreach (var item in LstStudentsFee)
                {
                    string jsonFeeDetails = JsonConvert.SerializeObject(item.StudFeeDetails.Select(m => new { m.BlockPayNow, m.AdvancePaymentAvailable, m.AdvanceDetails, m.DiscAmount, m.DueAmount, m.PayAmount, m.OriginalAmount, m.FeeDescription, m.FeeID, m.ActivityRefID }));
                    string jsonDiscountDetails = JsonConvert.SerializeObject(item.DiscountDetails);
                    jsonFeeDetails = jsonFeeDetails == "null" ? "[]" : jsonFeeDetails;
                    jsonDiscountDetails = jsonDiscountDetails == "null" ? "[]" : jsonDiscountDetails;

                    payload += "," + "{\"STU_NO\":\"" + item.STU_NO + "\",\"OnlinePaymentAllowed\":\"" + item.OnlinePaymentAllowed
                            + "\",\"UserMessageforOnlinePaymentBlock\":\"" + item.UserMessageforOnlinePaymentBlock
                            + "\",\"IpAddress\":\"" + item.IpAddress + "\",\"PaymentTypeID\":\"" + item.PaymentTypeID
                            + "\",\"PaymentProcessingCharge\":\"" + item.PaymentProcessingCharge
                            + "\",\"PayingAmount\":\"" + item.PayingAmount + "\",\"PayMode\":\"" + item.PayMode
                            + "\",\"FeeDetail\":" + Convert.ToString(jsonFeeDetails) + ",\"DiscountDetails\":" + Convert.ToString(jsonDiscountDetails) + "}";
                }
                payload = payload.Substring(1);
                payload = string.Format("[{0}]", payload);
                var baseUrl = HttpContext.Current.Request.Url.Host;
                if (baseUrl == "localhost")
                {
                    baseUrl = "http://" + baseUrl + ":7761";
                }
                else
                {
                    baseUrl = "https://" + baseUrl;
                }
                //BackUrl = baseUrl + BackUrl;
                _loggerClient.LogWarning(baseUrl);
                _loggerClient.LogWarning(Constants.PCPaymentGatewayRedirection);

                //var token = GetAuthorizationTokenAsync();
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                string json = string.Empty;
                //string AuthorizedToken = token.token_type + " " + token.access_token;
                using (httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, AddOnlinePaymentExamAPI))
                {
                    HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
                    httpRequestMessage.Content = content;
                    httpRequestMessage.Headers.Add("paymentTo", PaymentTo);
                    httpRequestMessage.Headers.Add("paymentOrigin", PaymentOrigin);
                    //httpRequestMessage.Headers.Add("apdId", apdId);
                    //httpRequestMessage.Headers.Add("activityFeeCollType", feeCollType);
                    httpRequestMessage.Headers.Add("data", payload);
                    httpRequestMessage.Headers.Add("redirectURL", Convert.ToString(baseUrl + Constants.PCPaymentGatewayRedirection));
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                    // httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", AuthorizedToken);
                    using (response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            JavaScriptSerializer se = new JavaScriptSerializer();
                            json = response.Content.ReadAsStringAsync().Result;
                            onlPayResponse = new JavaScriptSerializer().Deserialize<OnlinePayResponse>(json);

                            SessionExtensions.SetObject("PCPayGatewayRefId", Convert.ToString(onlPayResponse.PaymentRefNo));
                            SessionExtensions.SetObject("PCPayGateWaySource", Convert.ToString(PaymentTo));
                            SessionExtensions.SetObject("PCPayGateWayReturnTo", Convert.ToString(BackUrl));
                            SessionExtensions.SetObject("PCPayGateWayStudentNo", Convert.ToString(LstStudentsFee.Select(m => m.STU_NO).FirstOrDefault()));
                        }
                    }
                }
                //    }
                //}
                return onlPayResponse;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PaymentService.SubmitOnlinePaymentRequestExam()" + ex.Message);
                return onlPayResponse;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, onlPayResponse, response, SessionHelper.CurrentSession.UserName);
            }

        }

        public PaymentSummary GetPaymentSummary(string type, string PaymentRefNo, string paymentTo)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            PaymentSummaryRoot root = new PaymentSummaryRoot();
            PaymentSummary onlPayResponse = new PaymentSummary();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //    if (token != null)
                //    {
                //        if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //        {
                //            string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetPaymentSummaryAPI);
                httpRequestMessage.Headers.Add("type", type);
                httpRequestMessage.Headers.Add("PaymentRefNo", PaymentRefNo);
                httpRequestMessage.Headers.Add("paymentTo", paymentTo);
                httpRequestMessage.Headers.Add("parentid", Convert.ToString(SessionHelper.CurrentSession.UserName));
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        root = new JavaScriptSerializer().Deserialize<PaymentSummaryRoot>(json);
                        if (root.data != null)
                            onlPayResponse = root.data;
                    }
                }
                //    }
                //}
                return onlPayResponse;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PaymentService.GetPaymentSummary()" + ex.Message);
                return onlPayResponse;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, onlPayResponse, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public PaymentSummary GetPaymentSummaryTransport(string type, string PaymentRefNo, string paymentTo)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            PaymentSummaryRoot root = new PaymentSummaryRoot();
            PaymentSummary onlPayResponse = new PaymentSummary();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetPaymentSummaryTrasportAPI);
                httpRequestMessage.Headers.Add("type", type);
                httpRequestMessage.Headers.Add("PaymentRefNo", PaymentRefNo);
                httpRequestMessage.Headers.Add("paymentTo", paymentTo);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        root = new JavaScriptSerializer().Deserialize<PaymentSummaryRoot>(json);
                        if (root.data != null)
                            onlPayResponse = root.data;
                    }
                }
                //    }
                //}
                return onlPayResponse;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PaymentService.GetPaymentSummaryTransport()" + ex.Message);
                return onlPayResponse;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, onlPayResponse, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public PaymentSummary GetPaymentSummaryActivity(string type, string PaymentRefNo, string paymentTo, string feeCollType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            PaymentSummaryRoot root = new PaymentSummaryRoot();
            PaymentSummary onlPayResponse = new PaymentSummary();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetPaymentSummaryActivityAPI);
                httpRequestMessage.Headers.Add("type", type);
                httpRequestMessage.Headers.Add("PaymentRefNo", PaymentRefNo);
                httpRequestMessage.Headers.Add("paymentTo", paymentTo);
                httpRequestMessage.Headers.Add("feeCollType", feeCollType);
                httpRequestMessage.Headers.Add("parentid", Convert.ToString(SessionHelper.CurrentSession.UserName));
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        root = new JavaScriptSerializer().Deserialize<PaymentSummaryRoot>(json);
                        if (root.data != null)
                            onlPayResponse = root.data;
                    }
                }
                //    }
                //}
                return onlPayResponse;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PaymentService.GetPaymentSummaryActivity()" + ex.Message);
                return onlPayResponse;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, onlPayResponse, response, SessionHelper.CurrentSession.UserName);
            }

        }

        public IEnumerable<PaymentHistory> GetPaymentHistory(string StudentNo, string FromDate, string ToDate, string Source)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            _loggerClient.LogWarning("GetPaymentHistory:");
            PaymentHistoryRoot payHistRoot = new PaymentHistoryRoot();
            List<PaymentHistory> payHistoryList = new List<PaymentHistory>();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetPaymentHistoryAPI);
                httpRequestMessage.Headers.Add("stuId", StudentNo);
                httpRequestMessage.Headers.Add("fromDate", FromDate.ToString());
                httpRequestMessage.Headers.Add("toDate", ToDate.ToString());
                httpRequestMessage.Headers.Add("source", Source.ToString());
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);
                //_loggerClient.LogWarning("StudentNo:" + StudentNo);
                //_loggerClient.LogWarning("FromDate:" + FromDate);
                //_loggerClient.LogWarning("ToDate:" + ToDate);
                //_loggerClient.LogWarning("source:" + Source);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        _loggerClient.LogWarning("jsonPH:" + json);
                        //payHistRoot = new JavaScriptSerializer().Deserialize<PaymentHistoryRoot>(json);
                        if (json != "" && json != "[]")
                        {
                            payHistRoot.data = new JavaScriptSerializer().Deserialize<List<PaymentHistory>>(json);
                        }
                        if (payHistRoot.data != null && payHistRoot.data.Count > 0)
                            payHistoryList = payHistRoot.data;
                    }
                }
                //    }
                //}
                return payHistoryList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PaymentService.GetPaymentHistory()" + ex.Message);
                return payHistoryList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, payHistoryList, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public IEnumerable<AccountStatementDetail> GetAccountDetails(string StudentNo, string FromDate, string ToDate, string Source)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            _loggerClient.LogWarning("GetAccountDetails:");
            AccountStatDetailRoot payHistRoot = new AccountStatDetailRoot();
            List<AccountStatementDetail> payHistoryList = new List<AccountStatementDetail>();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetAccountDetailsAPI);
                httpRequestMessage.Headers.Add("stuId", StudentNo);
                httpRequestMessage.Headers.Add("fromDate", FromDate.ToString());
                httpRequestMessage.Headers.Add("toDate", ToDate.ToString());
                httpRequestMessage.Headers.Add("source", Source.ToString());
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);
                //_loggerClient.LogWarning("StudentNo:" + StudentNo);
                //_loggerClient.LogWarning("FromDate:" + FromDate);
                //_loggerClient.LogWarning("ToDate:" + ToDate);
                //_loggerClient.LogWarning("source:" + Source);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        _loggerClient.LogWarning("jsonAD:" + json);
                        if (json != "" && json != "[]")
                        {
                            //payHistRoot = new JavaScriptSerializer().Deserialize<AccountStatDetailRoot>(json);
                            payHistRoot.data = new JavaScriptSerializer().Deserialize<List<AccountStatementDetail>>(json);
                        }
                        //else
                        //{
                        //    string strJson = "[{\"date\":\"2020-02-14T00:00:00\",\"refNo\":\"OPENING\",\"feeDescription\":\"Catering Fee (School)\",\"narration\":\"OPENING\",\"debit\":0,\"credit\":27.55},{\"date\":\"2020-02-14T00:00:00\",\"refNo\":\"OPENING\",\"feeDescription\":\"TUITION FEE\",\"narration\":\"OPENING\",\"debit\":963.25,\"credit\":0}]";
                        //    payHistRoot.data = new JavaScriptSerializer().Deserialize<List<AccountStatementDetail>>(strJson);
                        //}
                        if (payHistRoot.data != null && payHistRoot.data.Count > 0)
                            payHistoryList = payHistRoot.data;
                    }
                    //    }
                    //}
                }
                return payHistoryList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PaymentService.GetAccountDetails()" + ex.Message);
                return payHistoryList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, payHistoryList, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public IEnumerable<AccountStatementSummary> GetAccountSummary(string StudentNo, string FromDate, string ToDate, string Source)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            _loggerClient.LogWarning("GetAccountSummary:");
            AccountStatSummaryRoot payHistRoot = new AccountStatSummaryRoot();
            List<AccountStatementSummary> payHistoryList = new List<AccountStatementSummary>();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, GetAccountSummaryAPI);
                httpRequestMessage.Headers.Add("stuId", StudentNo);
                httpRequestMessage.Headers.Add("fromDate", FromDate.ToString());
                httpRequestMessage.Headers.Add("toDate", ToDate.ToString());
                httpRequestMessage.Headers.Add("source", Source.ToString());
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                //_loggerClient.LogWarning("stuId:" + StudentNo);
                //_loggerClient.LogWarning("FromDate:" + FromDate);
                //_loggerClient.LogWarning("ToDate:" + ToDate);
                //_loggerClient.LogWarning("source:" + Source);
                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        if (json != "" && json != "[]")
                        {
                            //payHistRoot = new JavaScriptSerializer().Deserialize<AccountStatSummaryRoot>(json);
                            payHistRoot.data = new JavaScriptSerializer().Deserialize<List<AccountStatementSummary>>(json);
                        }
                        //else
                        //{
                        //    string strJson = "[{\"academicYear\":\"2019-2020\",\"feeDescription\":\"REGISTRATION FEE\",\"debit\":0,\"credit\":500},{\"academicYear\":\"2019-2020\",\"feeDescription\":\"TUITION FEE\",\"debit\":53498.75,\"credit\":53498.75}]";
                        //    payHistRoot.data = new JavaScriptSerializer().Deserialize<List<AccountStatementSummary>>(strJson);
                        //}

                        if (payHistRoot.data != null && payHistRoot.data.Count > 0)
                            payHistoryList = payHistRoot.data;
                    }
                }
                //    }
                //}
                return payHistoryList;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PaymentService.GetAccountSummary()" + ex.Message);
                return payHistoryList;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, payHistoryList, response, SessionHelper.CurrentSession.UserName);
            }

        }

        //send email of for statement of account 
        public OnlineFeePayment SendEmailStatementOfAccounts(string StudentNo, string FromDate, string ToDate, string StatementType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            HttpRequestMessage httpRequestMessage = null;
            string json = "";
            OnlineFeePayment onlPayResponse = new OnlineFeePayment();
            //var token = GetAuthorizationTokenAsync();
            try
            {
                //if (token != null)
                //{
                //    if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                //    {
                //        string AuthorizedToken = token.token_type + " " + token.access_token;
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, SendMailAccountStatementAPI);
                httpRequestMessage.Headers.Add("stuid", StudentNo);
                httpRequestMessage.Headers.Add("fromDate", FromDate);
                httpRequestMessage.Headers.Add("toDate", ToDate);
                httpRequestMessage.Headers.Add("statementType", StatementType);
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                //httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                using (response = _client.SendAsync(httpRequestMessage).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        JavaScriptSerializer se = new JavaScriptSerializer();
                        json = response.Content.ReadAsStringAsync().Result;
                        onlPayResponse = new JavaScriptSerializer().Deserialize<OnlineFeePayment>(json);
                    }
                }
                //    }
                //}
                return onlPayResponse;
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Error in PaymentService.SendEmailStatementOfAccounts()" + ex.Message);
                return onlPayResponse;
            }
            finally
            {
                _phoenixAPIParentCornerService.InsertLogDetails(httpRequestMessage, onlPayResponse, response, SessionHelper.CurrentSession.UserName);
            }

        }
        public TokenResult GetAuthorizationTokenAsync()
        {
            TokenResult JsonDeserilize = new TokenResult();
            JsonDeserilize.access_token = SessionHelper.CurrentSession.PhoenixAccessToken;
            JsonDeserilize.token_type = "Bearer";
            //if (HttpContext.Current.Session["AccessToken"] != null)
            //{
            //    JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
            //}
            //else
            //{
            //    //added httpclient in scope for Authorization Token By Rohan P-27/08/2020
            //    HttpClient _phoneixapiclient = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
            //    _phoneixapiclient.DefaultRequestHeaders.Accept.Clear();
            //    var uri = tokenAPI;
            //    string _ContentType = "application/x-www-form-urlencoded";
            //    _phoneixapiclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //    var nvc = new List<KeyValuePair<string, string>>();
            //    nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
            //    nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
            //    nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
            //    nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

            //    using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
            //    {
            //        using (var result = _phoneixapiclient.SendAsync(test).Result)
            //        {
            //            if (result.IsSuccessStatusCode)
            //            {
            //                var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
            //                try
            //                {
            //                    JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
            //                }
            //                finally
            //                {
            //                    result.Dispose();
            //                }
            //            }
            //        }
            //    }

            //    HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
            //}
            return JsonDeserilize;
        }
    }
}