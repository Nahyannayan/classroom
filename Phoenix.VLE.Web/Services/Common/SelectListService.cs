﻿
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.Helpers.Extensions;
using Phoenix.Common.Localization;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public class SelectListService : ISelectListService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/selectlist";
        #endregion

        public SelectListService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        #region Methods
        public IEnumerable<ListItem> GetSelectListItems(string listCode, string whereCondition, object whereConditionParamValues)
        {
            var uri = API.SelectList.GetSelectListItems(_path, listCode, whereCondition, whereConditionParamValues);
            IEnumerable<ListItem> list = new List<ListItem>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<ListItem>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }

        public IEnumerable<AssignmentCategory> GetAssignmentCategoryMappingListBySchoolId(long SchoolId)
        {
            var uri = API.SelectList.GetAssignmentCategoryMappingListBySchoolId(_path, SchoolId);
            IEnumerable<AssignmentCategory> list = new List<AssignmentCategory>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<AssignmentCategory>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }

        public IEnumerable<ListItem> GetAssignmentCategoryMasterList()
        {
            var uri = API.SelectList.GetAssignmentCategoryMasterList(_path);
            IEnumerable<ListItem> list = new List<ListItem>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<ListItem>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }

        public IEnumerable<SubjectListItem> GetSubjectsByUserId(int userId)
        {
            int languageId = LocalizationHelper.CurrentSystemLanguageId;
            var uri = API.SelectList.GetSubjectListItems(_path, languageId, userId);
            IEnumerable<SubjectListItem> subjectsList = new List<SubjectListItem>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjectsList = EntityMapper<string, IEnumerable<SubjectListItem>>.MapFromJson(jsonDataProviders);
            }
            return subjectsList;
        }

        public IEnumerable<AcademicYearList> GetAcademicYearByUserId(int userId)
        {

            var uri = API.SelectList.GetAcademicYearListItem(_path, userId);
            IEnumerable<AcademicYearList> yearList = new List<AcademicYearList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                yearList = EntityMapper<string, IEnumerable<AcademicYearList>>.MapFromJson(jsonDataProviders);
            }
            return yearList;
        }


        #endregion
    }
}
