﻿using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace Phoenix.VLE.Web.Services
{
    public class ChatterPermissionService : IChatterPermissionService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/ChatterPermission";
        #endregion
        public ChatterPermissionService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }

        public IEnumerable<ChatUsers> GetUsersInGroup(int groupId)
        {
            var uri = API.Chatter.GetUsersInGroup(_path, groupId);
            IEnumerable<ChatUsers> lstUsers = new List<ChatUsers>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstUsers = EntityMapper<string, IEnumerable<ChatUsers>>.MapFromJson(jsonDataProviders);
            }
            return lstUsers;
        }

        public bool UpdateChatterPermission(ChatPermission chats)
        {
            var model = new ChatPermission();
            model.SchoolId = chats.SchoolId;
            model.UserIds = chats.UserIds;
            model.PermissionId = chats.PermissionId;
            model.Flag = chats.Flag;
            var uri = API.Chatter.UpdateChatterPermission(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, model).Result;
            return response.IsSuccessStatusCode;
            
        }
    }
}