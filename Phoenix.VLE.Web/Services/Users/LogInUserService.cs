﻿using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Web;
using System.Collections.Generic;
using Phoenix.VLE.Web.ViewModels;
using Phoenix.Common.ViewModels;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Text;
using CommonHelper = Phoenix.VLE.Web.Helpers.CommonHelper;
using Phoenix.VLE.Web.Models;
using Phoenix.Common.Logger;

namespace Phoenix.VLE.Web.Services
{
    public class LogInUserService : ILogInUserService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/loginuser";

        readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"];
        readonly string tokenAPI = CommonHelper.GetPhoenixBaseURI() + ConfigurationManager.AppSettings["TokenAPI"];
        readonly string userLoginAPI = ConfigurationManager.AppSettings["UserLoginAPI"];
        private ILoggerClient _loggerClient = LoggerClient.Instance;
        #endregion
        #region Elite Private Variables

        private readonly string ForgotPasswordBaseUrl = ConfigurationManager.AppSettings["PhoenixBaseUriParentCorner"];

        #endregion Elite Private Variables

        public LogInUserService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

            }
        }

        public LogInUser GetLoginUserByUserName(string userName, string ipDetails)
        {
            var loginUser = new LogInUser();
            var encryptUserName = EncryptDecryptHelper.EncryptUrl(userName);
            var uri = API.LogInUser.GetLoginUserByUserName(_path, encryptUserName, ipDetails);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                loginUser = EntityMapper<string, LogInUser>.MapFromJson(jsonDataProviders);
            }
            return loginUser;
        }


        public LoginUser CheckLoginUser(LoginViewModel model)
        {
            string json = "";
            var objLoginUser = new LoginUser();
            var _commonService = new CommonService();
            var token = GetAuthorizationTokenAsync(new APIAuthorization() { });
            if (token != null)
            {
                if (!string.IsNullOrEmpty(token.token_type) && !string.IsNullOrEmpty(token.access_token))
                {
                    string AuthorizedToken = token.token_type + " " + token.access_token;
                    var EncryptedPassword = _commonService.EncryptSHA512(model.Password);
                    HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, userLoginAPI);
                    httpRequestMessage.Headers.Add("Username", model.UserName);
                    httpRequestMessage.Headers.Add("Password", EncryptedPassword);
                    httpRequestMessage.Headers.Add("Source", "Classroom");
                    httpRequestMessage.Headers.Add("Authorization", AuthorizedToken);

                    using (var response = _client.SendAsync(httpRequestMessage).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            JavaScriptSerializer se = new JavaScriptSerializer();
                            var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                            objLoginUser = new JavaScriptSerializer().Deserialize<LoginUser>(jsonDataProviders);
                            if(objLoginUser.Message == "User is valid")
                            {
                                objLoginUser.success = "true";
                            }
                            else
                            {
                                objLoginUser.success = "false";
                            }
                        }
                    }
                }
            }
            return objLoginUser;
        }

        //Old token API
        public TokenResult GetAuthorizationTokenAsync(APIAuthorization calendarAuthorization)
        {
            //_loggerClient.LogWarning("GetAuthorizationToken");
            TokenResult JsonDeserilize = new TokenResult();
            //if (HttpContext.Current.Session["AccessToken"] != null)
            //{
            //    JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
            //}
            //else
            //{
                _client.DefaultRequestHeaders.Accept.Clear();
                var uri = tokenAPI;
               // string _ContentType = "application/x-www-form-urlencoded";
                //_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
                var nvc = new List<KeyValuePair<string, string>>();
                nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
                nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
                nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
                nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

                using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
                {
                    using (var result = _client.SendAsync(test).Result)
                    {
                        if (result.IsSuccessStatusCode)
                        {
                            var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
                            try
                            {
                                JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
                            }
                            finally
                            {
                                result.Dispose();
                            }
                        }
                    }
                }

            //    HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
            //}
            return JsonDeserilize;
        }
        
        public IEnumerable<LogInUser> GetUserList(int schoolId)
        {
            var uri = API.LogInUser.GetUserList(_path, schoolId);
            IEnumerable<LogInUser> userList = new List<LogInUser>();

            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                userList = EntityMapper<string, IEnumerable<LogInUser>>.MapFromJson(jsonDataProviders);
            }
            return userList;
        }
        public LogInUser GetLoginUserByUserNamePassword(string userName, string passowrd, string ipDetails)
        {
            var loginUser = new LogInUser();
            var encryptUserName = EncryptDecryptHelper.EncryptUrl(userName);
            var encryptPassowrd = EncryptDecryptHelper.EncryptUrl(passowrd);
            var uri = API.LogInUser.GetLoginUserByUserNamePassword(_path, encryptUserName, encryptPassowrd, ipDetails);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                loginUser = EntityMapper<string, LogInUser>.MapFromJson(jsonDataProviders);
            }
            return loginUser;
        }
        #region ForgotPassword Elite

        public PostForgetUsernameResponse PostForgetUserName(string StudentId, string dob, string token)
        {
            var postForgetUsernameResponse = new PostForgetUsernameResponse();
            var uri = API.ForgotPasswordUrl.GetPostForgetUserNameURI(ForgotPasswordBaseUrl);
            string JsonResponse = string.Empty;
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    client.DefaultRequestHeaders.Add("studentid", StudentId);
                    client.DefaultRequestHeaders.Add("dob", dob);
                    client.DefaultRequestHeaders.Add("source", "SCHOOL");
                    HttpResponseMessage response = client.PostAsync(uri, null).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        JsonResponse = response.Content.ReadAsStringAsync().Result;
                        postForgetUsernameResponse = EntityMapper<string, PostForgetUsernameResponse>.MapFromJson(JsonResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Forgot Password: PostForgetUserName Method Exception");
                _loggerClient.LogException(ex);
                _loggerClient.LogException(ex.InnerException);
            }

            return postForgetUsernameResponse;
        }

        public ValidateUserNameResponse GetValidateUserName(string ParentUserName, string otp, string Token)
        {
            var validateUserNameResponse = new ValidateUserNameResponse();
            string JsonResponse = string.Empty;
            var uri = API.ForgotPasswordUrl.GetValidateUserNameURI(ForgotPasswordBaseUrl);
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                    client.DefaultRequestHeaders.Add("username", ParentUserName);
                    client.DefaultRequestHeaders.Add("otp", otp);
                    client.DefaultRequestHeaders.Add("source", "SCHOOL");
                    HttpResponseMessage response = client.GetAsync(uri).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        JsonResponse = response.Content.ReadAsStringAsync().Result;
                        validateUserNameResponse = EntityMapper<string, ValidateUserNameResponse>.MapFromJson(JsonResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Forgot Password: GetValidateUserName Method Exception");
                _loggerClient.LogException(ex);
                _loggerClient.LogException(ex.InnerException);
            }
            return validateUserNameResponse;
        }

        public ChangePasswordResponse PasswordChange(ChangePasswordRequest changePasswordRequest, string Token)
        {
            var changePasswordResponse = new ChangePasswordResponse();
            var uri = API.ForgotPasswordUrl.PasswordChangeURI(ForgotPasswordBaseUrl);
            string JsonResponse = string.Empty;
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                    HttpResponseMessage response = client.PostAsJsonAsync(uri, changePasswordRequest).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        JsonResponse = response.Content.ReadAsStringAsync().Result;
                        changePasswordResponse = EntityMapper<string, ChangePasswordResponse>.MapFromJson(JsonResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                _loggerClient.LogWarning("Forgot Password: PasswordChange Method Exception");
                _loggerClient.LogException(ex);
                _loggerClient.LogException(ex.InnerException);
            }
            return changePasswordResponse;
        }

        #endregion ForgotPassword Elite
    }
}
