﻿using Phoenix.Models;
using Phoenix.VLE.Web.Services;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using System.Web;
using System.Collections.Generic;

namespace Phoenix.VLE.Web.Services
{
    public class UserPermissionService : IUserPermissionService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/userpermission";
        #endregion

        public UserPermissionService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }

        public bool AddMenuItem(MenuItem menuItem)
        {
            var uri = API.UserPermission.AddMenuItem(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, menuItem).Result;
            return response.IsSuccessStatusCode;
        }

        public async Task<PagePermission> GetUserPermissions(long userId, string moduleUrl, string moduleCode, int userTypeId)
        {
            var pagePermission = new PagePermission();
            var encryptModuleUrl = EncryptDecryptHelper.EncryptUrl(moduleUrl);
            var encryptModuleCode =  EncryptDecryptHelper.EncryptUrl(moduleCode);
            var uri = API.UserPermission.GetUserPermission(_path, userId, encryptModuleUrl, encryptModuleCode, userTypeId);
            HttpResponseMessage response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                pagePermission = EntityMapper<string, PagePermission>.MapFromJson(jsonDataProviders);
            }
            return pagePermission;
        }

        public async Task<bool> IsPermissionAssigned(long userId, string moduleUrl, int userTypeId)
        {
            var result = false;
            var encryptModuleUrl = EncryptDecryptHelper.EncryptUrl(moduleUrl);
            var uri = API.UserPermission.CheckUserPermission(_path, userId, encryptModuleUrl, userTypeId);
            HttpResponseMessage response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToBoolean(jsonDataProviders);
            }
            return result;
        }
        public async Task<bool> IsCustomPermissionAssigned(int userId, string permissionCodes)
        {
            var result = false;
            var uri = API.UserPermission.IsCustomPermissionAssigned(_path, userId, permissionCodes);
            HttpResponseMessage response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                result = Convert.ToBoolean(jsonDataProviders);
            }
            return result;
        }
    }
}