﻿using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace Phoenix.VLE.Web.Services
{
    public class PhoenixTokenService : IPhoenixTokenService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = ConfigurationManager.AppSettings["PhoenixAPIBaseUri"];
        //readonly string _path = "api/v1/ChatterPermission";
        readonly string tokenAPI = ConfigurationManager.AppSettings["TokenAPI"];
        readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"];
        #endregion
        public PhoenixTokenService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                
            }
        }

        //New code to get token from Pheonix for user and store in cookie: Deepak Singh on 14/Sept/2020
        public string GetAuthorizationToken()
        {
            //_loggerClient.LogWarning("GetAuthorizationToken");
            TokenResult JsonDeserilize = new TokenResult();
            var uri = tokenAPI;
            var nvc = new List<KeyValuePair<string, string>>();
            nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
            nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
            nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
            nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

            using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
            {
                using (var result = _client.SendAsync(test).Result)
                {
                    if (result.IsSuccessStatusCode)
                    {
                        var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
                        try
                        {
                            JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
                        }
                        finally
                        {
                            result.Dispose();
                        }
                    }
                }
            }
            return JsonDeserilize.access_token;
        }
    }
}