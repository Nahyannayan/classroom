﻿using Phoenix.Common.Helpers;
using Phoenix.Common.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.ViewModels;
using System.Web.Script.Serialization;
using Phoenix.Common.Models;
using System.Threading.Tasks;
using System.Net;
using System.Text;
using CommonHelper = Phoenix.VLE.Web.Helpers.CommonHelper;

namespace Phoenix.VLE.Web.Services
{
    public class NotificationService : INotificationService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = ConfigurationManager.AppSettings["PhoenixAPIBaseUri"];
        //readonly string _path = "api/v1/TimeTable";

        //readonly string tokenAPI = CommonHelper.GetPhoenixBaseURI() + ConfigurationManager.AppSettings["TokenAPI"];
        readonly string allNotificationAPI = ConfigurationManager.AppSettings["GetNotificationAPI"];
        readonly string updateNotificationAPI = ConfigurationManager.AppSettings["UpdateNotificationAPI"];

        //readonly string AppId = ConfigurationManager.AppSettings["AppId"];
        //readonly string APIPassword = ConfigurationManager.AppSettings["APIPassword"];
        //readonly string APIUsername = ConfigurationManager.AppSettings["APIUsername"];
        //readonly string APIGrant_type = ConfigurationManager.AppSettings["APIGrant_type"];

        private ILoggerClient _loggerClient;

        public NotificationService()
        {
            if (_client.BaseAddress == null)
            {
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                //_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.PhoenixAccessToken);
                _loggerClient = LoggerClient.Instance;
            }
        }

        #endregion


        public List<StudentNotification> GetAllNotifications(string key, string identifier, string source, string type)
        {
            GetNotificationParams getNotificationParams = new GetNotificationParams()
            {
                Key = key,
                Identifier = identifier,
                Source = source,
                Type = type
            };
            List<StudentNotification> notificationsList = new List<StudentNotification>();



            HttpResponseMessage response = _client.PostAsJsonAsyncExt(allNotificationAPI, getNotificationParams,SessionHelper.CurrentSession.PhoenixAccessToken).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var studentNotificationRoot = EntityMapper<string, StudentNotificationRoot>.MapFromJson(jsonDataProviders);
                if (studentNotificationRoot.data != null)
                {
                    //filter according to selected student
                    if (SessionHelper.CurrentSession.IsParent())
                    {
                        notificationsList = studentNotificationRoot.data.Where(x=>x.StudentId==SessionHelper.CurrentSession.CurrentSelectedStudent.StudentNumber.ToString()).ToList();
                    }
                    else
                    {
                        notificationsList = studentNotificationRoot.data;
                    }
                }
            }
            return notificationsList;
        }

        public bool UpdateNotificationMarkAsRead(string key, string identifier, string source, long notificationId = 0, List<StudentNotification> notificationList = null, bool IsMarked = true)
        {
            int MarkedStatus = 1;
            if (IsMarked)
                MarkedStatus = 1;
            else
                MarkedStatus = 0;
            PostNotificationParams postNotificationParams = new PostNotificationParams();
            string result = string.Empty;
            if (notificationList == null)
            {
                postNotificationParams = new PostNotificationParams()
                {
                    Key = key,
                    Identifier = identifier,
                    Source = source,
                    NotificationDetails = new List<NotificationDetails>()
                {
                    new NotificationDetails() { NotificationID = notificationId, ReadFlag = MarkedStatus, FavoriteFlag = 0, DeleteFlag = 0 }
                }
                };
            }
            else
            {
                List<NotificationDetails> lst = new List<NotificationDetails>();
                foreach (var id in notificationList.Select(x => x.NotificationID).ToList())
                {
                    lst.Add(new NotificationDetails()
                    {
                        NotificationID = Convert.ToInt64(id),
                        ReadFlag = MarkedStatus,
                        FavoriteFlag = 0,
                        DeleteFlag = 0
                    });
                }
                postNotificationParams = new PostNotificationParams()
                {
                    Key = key,
                    Identifier = identifier,
                    Source = source,
                    NotificationDetails = lst
                };
            }

            HttpResponseMessage response = _client.PostAsJsonAsyncExt(updateNotificationAPI, postNotificationParams, SessionHelper.CurrentSession.PhoenixAccessToken).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                var studentNotificationRoot = EntityMapper<string, StudentNotificationRoot>.MapFromJson(jsonDataProviders);
                if (studentNotificationRoot != null)
                    result = studentNotificationRoot.success;
            }
            if (result == "true")
                return true;
            else
                return false;
        }

        //public TokenResult GetAuthorizationTokenAsync()
        //{
        //    TokenResult JsonDeserilize = new TokenResult();
        //    if (HttpContext.Current.Session["AccessToken"] != null)
        //    {
        //        JsonDeserilize = (TokenResult)HttpContext.Current.Session["AccessToken"];
        //    }
        //    else
        //    {
        //        _client.DefaultRequestHeaders.Accept.Clear();
        //        var uri = tokenAPI;
        //        string _ContentType = "application/x-www-form-urlencoded";
        //        _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
        //        var nvc = new List<KeyValuePair<string, string>>();
        //        nvc.Add(new KeyValuePair<string, string>("AppId", AppId));
        //        nvc.Add(new KeyValuePair<string, string>("password", APIPassword));
        //        nvc.Add(new KeyValuePair<string, string>("username", APIUsername));
        //        nvc.Add(new KeyValuePair<string, string>("grant_type", APIGrant_type));

        //        using (var test = new HttpRequestMessage(HttpMethod.Post, uri) { Content = new FormUrlEncodedContent(nvc) })
        //        {
        //            using (var result = _client.SendAsync(test).Result)
        //            {
        //                if (result.IsSuccessStatusCode)
        //                {
        //                    var jsonDataStatus = result.Content.ReadAsStringAsync().Result;
        //                    try
        //                    {
        //                        JsonDeserilize = new JavaScriptSerializer().Deserialize<TokenResult>(jsonDataStatus);
        //                    }
        //                    finally
        //                    {
        //                        result.Dispose();
        //                    }
        //                }
        //            }
        //        }

        //        HttpContext.Current.Session["AccessToken"] = JsonDeserilize;
        //    }
        //    return JsonDeserilize;
        //}

    }
}