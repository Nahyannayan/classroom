﻿using Phoenix.Models;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Phoenix.VLE.Web.Services
{
    public interface ILogInUserService
    {
        LogInUser GetLoginUserByUserName(string userName,string ipDetails);
        IEnumerable<LogInUser> GetUserList(int schoolId);
        LoginUser CheckLoginUser(LoginViewModel model);
        LogInUser GetLoginUserByUserNamePassword(string userName,string password,string ipDetails);
        PostForgetUsernameResponse PostForgetUserName(string StudentId, string dob, string Token);
        ValidateUserNameResponse GetValidateUserName(string ParentUserName, string otp, string Token);
        ChangePasswordResponse PasswordChange(ChangePasswordRequest changePasswordRequest, string Token);
    }
}
