﻿using Phoenix.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface INotificationService
    {
        List<StudentNotification> GetAllNotifications(string key, string identifier, string source, string type);
        bool UpdateNotificationMarkAsRead(string key, string identifier, string source, long notificationId, List<StudentNotification> notificationList, bool IsMarked = true);
    }
}
