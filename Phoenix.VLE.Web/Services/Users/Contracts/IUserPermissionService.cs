﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IUserPermissionService
    {
        Task<bool> IsPermissionAssigned(long userId, string moduleUrl, int userTypeId);
        Task<PagePermission> GetUserPermissions(long userId, string moduleUrl, string moduleCode, int userTypeId);

        bool AddMenuItem(MenuItem menuItem);
        Task<bool> IsCustomPermissionAssigned(int userId, string permissionCodes);


    }
}
