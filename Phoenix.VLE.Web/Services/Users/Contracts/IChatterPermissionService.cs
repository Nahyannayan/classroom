﻿using Phoenix.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IChatterPermissionService
    {
        bool UpdateChatterPermission(ChatPermission chats);
        IEnumerable<ChatUsers> GetUsersInGroup(int groupId);
    }
}
