﻿using Phoenix.Common.ViewModels;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Phoenix.VLE.Web.Services
{
    public interface IUserRoleService
    {
        IEnumerable<UserRole> GetUserRoles();
        IEnumerable<UserRoleMapping> GetAllUserRoleMappingData(int userid);
        IEnumerable<ModuleStructure> GetModuleList(int systemlanguageid, string modulecode);
        IEnumerable<ModuleStructure> GetModuleStructureList(int systemlanguageid,int? moduleid, string modulecode);
        IEnumerable<PermissionTypeView> GetAllPermissionData(int userroleId, int userId, int moduleId, bool loadcustomepermission, int schoolId);
        bool UpdatePermissionTypeDataCUD(List<CustomPermissionEdit> mappingDetails, string operationType, short? userId, short userRoleId);
        UserRole GetUserRoleById(int id);
        bool UpdateUserRoleData(UserRoleEdit model);
        bool DeleteUserRoleData(int id);
        object CheckUserRoleMapping(long? userId, short? roleId);
        bool InsertUserRoleMappingData(long userId, short roleId);
        bool DeleteUserRoleMappingData(long userId, short roleId);

        IEnumerable<UserRole> GetUserRolesBySchoolId(int schoolId);
        IEnumerable<User> GetUsersForRole(int roleId, int schoolId);
    }
}
