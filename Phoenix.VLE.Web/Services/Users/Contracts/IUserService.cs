﻿using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IUserService
    {
        IEnumerable<User> GetAllUsers();
        IEnumerable<UserFeelingView> GetUserFeelings(int SystemLanguageId);
        IEnumerable<UserProfileView> GetProfileAvatars();
        bool UpdateUserFeeling(UserFeelingView userFeelingView);
        bool UpdateUserProfile(UserProfileView userProfileView);
        IEnumerable<UserNotificationView> GetUserNotifications();
        bool PushNotificationLogs(string notificationType, int sourceId, long userId);
        IEnumerable<User> GetUserBySchool(long? schoolId, int userTypeId);
        User GetUserById(long userId);
        IEnumerable<User> SearchUserByName(string name, long schoolId = 0, int userTypeId = 0);
        IEnumerable<User> GetUsersByRolesLocatonAllowed(string roles);
        bool SendErrorLog(ErrorLogger errorLogger);
        IEnumerable<DBLogDetails> GetDBLogDetails();
        IEnumerable<User> GetUserLog(int? schoolId, DateTime startDate, DateTime endDate, string loginType);
        IEnumerable<UserErrorLogs> GetErrorLogs(int? schoolId, DateTime startDate, DateTime endDate, string loginType);
        IEnumerable<ErrorFiles> GetErrorLogFiles(long ErrorLogId);
        IEnumerable<User> GetTeachersListBySchoolId(long schoolId = 0);

        bool IsNotificationExist(int userId, int userTypeId, int CurrentLoginUserId);
        Task<bool> SaveUserFeedback(UserFeedback model);
    }
}
