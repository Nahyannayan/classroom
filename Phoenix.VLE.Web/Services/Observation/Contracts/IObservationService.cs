﻿using Phoenix.Models;
using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IObservationService
    {
        bool UpdateObservationData(ObservationEdit model, List<Objective> lstObjective);
        bool DeleteObservationData(Observation observation);
        Observation GetObservationById(int id);
        IEnumerable<ObservationFile> GetObservationFiles(int observationId);
        IEnumerable<ObservationStudent> GetObservationStudentById(int observationId);
        IEnumerable<Objective> GetObservationObjectives(int observationId);
        IEnumerable<SchoolGroup> GetSelectedSchoolGroupsByObservationID(int observationId);
        IEnumerable<ObservationStudent> GetStudentsByObservationId(int observationId);
        IEnumerable<ObservationFile> GetObservationMyFiles(long fileId, bool isFolder);
        IEnumerable<Observation> GetObservationTeacherPaging(int pageNumber, int pageSize, long teacherId,string schoolGroupIds, string searchString,string sortBy);
        IEnumerable<ObservationStudent> GetObservationStudentPaging(int pageNumber, int pageSize, long teacherId, string searchString,string sortBy);
        bool DeleteUploadedFiles(int fileId,int ObservationId,long UserId);
        IEnumerable<Subject> GetObservationSubjects(int observationId);
        File GetObservationFilebyObservationFileId(long id);
        IEnumerable<Observation> GetArchivedObservationTeacherPaging(int pageNumber, int pageSize, long teacherId, string searchString = "");
        Observation GetObservations(int teacherId);
        bool UpdateActiveObservation(long observationId);
        IEnumerable<Course> GetSchoolCoursesByObservation(int observationId);
    }
}
