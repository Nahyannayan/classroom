﻿using Phoenix.VLE.Web.EditModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Models;
using Phoenix.Common.Helpers;
using Phoenix.VLE.Web.Helpers;
using Newtonsoft.Json;

namespace Phoenix.VLE.Web.Services
{
    public class ObservationService : IObservationService
    {
        #region private variables
        private static HttpClient _client =  new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/Observation";
        #endregion
        public ObservationService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SessionHelper.CurrentSession.Token);
            }
        }
        public bool UpdateObservationData(ObservationEdit model, List<Objective> lstObjectives)
        {
            bool result = false;
            var uri = string.Empty;
            var sourceModel = new Observation();
            EntityMapper<ObservationEdit, Observation>.Map(model, sourceModel);
            sourceModel.lstObservationFiles = new List<ObservationFile>();
            sourceModel.lstObjectives = lstObjectives;
            if (model.SchoolGroupIds != null)
            {
                sourceModel.SchoolGroups = string.Join(",", model.SchoolGroupIds);
            }
            if (model.CourseIds != null)
            {
                sourceModel.Courses = string.Join(",", model.CourseIds);
            }
            if (model.StudentIds != null)
            {
                sourceModel.StudentIdsToAdd = string.Join(",", model.StudentIds);
            }

            if (model.lstDocuments != null)
            {
                sourceModel.lstObservationFiles = model.lstDocuments;
            }

            if (model.IsAddMode)
                uri = API.Observation.InsertObservation(_path);
            else
                uri = API.Observation.UpdateObservation(_path);
            string data = JsonConvert.SerializeObject(sourceModel);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, sourceModel).Result;
            return response.IsSuccessStatusCode;
        }
        public bool DeleteObservationData(Observation observation)
        {
            var uri = API.Observation.DeleteObservation(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, observation).Result;
            return response.IsSuccessStatusCode;
        }

        public Observation GetObservationById(int id)
        {
            var observation = new Observation();
            var uri = API.Observation.GetObservationById(_path, id, SessionHelper.CurrentSession.Id);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                observation = EntityMapper<string, Observation>.MapFromJson(jsonDataProviders);
            }
            return observation;
        }

        public IEnumerable<ObservationFile> GetObservationFiles(int observationId)
        {
            var uri = API.Observation.getObservationFilesById(_path, observationId);
            IEnumerable<ObservationFile> ObservationFiles = new List<ObservationFile>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                ObservationFiles = EntityMapper<string, IEnumerable<ObservationFile>>.MapFromJson(jsonDataProviders);
            }
            return ObservationFiles;
        }
        public IEnumerable<ObservationStudent> GetObservationStudentById(int observationId)
        {
            var uri = API.Observation.GetObservationStudentById(_path, observationId);
            IEnumerable<ObservationStudent> observationStudent = new List<ObservationStudent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                observationStudent = EntityMapper<string, IEnumerable<ObservationStudent>>.MapFromJson(jsonDataProviders);
            }
            return observationStudent;
        }

        public IEnumerable<Objective> GetObservationObjectives(int observationId)
        {
            var uri = API.Observation.GetObservationObjectives(_path, observationId);
            IEnumerable<Objective> lstObjectives = new List<Objective>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstObjectives = EntityMapper<string, IEnumerable<Objective>>.MapFromJson(jsonDataProviders);
            }
            return lstObjectives;
        }
        public IEnumerable<SchoolGroup> GetSelectedSchoolGroupsByObservationID(int ObservationId)
        {
            var uri = API.Observation.GetSchoolGroupsByObservationId(_path, ObservationId);
            IEnumerable<SchoolGroup> lstSchoolGroups = new List<SchoolGroup>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstSchoolGroups = EntityMapper<string, IEnumerable<SchoolGroup>>.MapFromJson(jsonDataProviders);
            }
            return lstSchoolGroups;
        }
        public IEnumerable<ObservationStudent> GetStudentsByObservationId(int observationId)
        {
            var uri = API.Observation.GetObservationStudent(_path, observationId);
            IEnumerable<ObservationStudent> lstObservation = new List<ObservationStudent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstObservation = EntityMapper<string, IEnumerable<ObservationStudent>>.MapFromJson(jsonDataProviders);
            }
            return lstObservation;
        }

        public IEnumerable<ObservationFile> GetObservationMyFiles(long fileId, bool isFolder)
        {
            var uri = API.Observation.GetObservationMyFiles(_path, fileId, isFolder);
            IEnumerable<ObservationFile> lstMyFiles = new List<ObservationFile>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstMyFiles = EntityMapper<string, IEnumerable<ObservationFile>>.MapFromJson(jsonDataProviders);
            }
            return lstMyFiles;
        }

        public IEnumerable<Observation> GetObservationTeacherPaging(int pageNumber, int pageSize, long teacherId, string schoolGroupIds, string searchString,string sortBy)
        {
            var uri = API.Observation.GetObservationTeacherPaging(_path, pageNumber, pageSize, teacherId, searchString, schoolGroupIds, sortBy);
            IEnumerable<Observation> lstObservations = new List<Observation>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstObservations = EntityMapper<string, IEnumerable<Observation>>.MapFromJson(jsonDataProviders);
            }
            return lstObservations;
        }
        public IEnumerable<ObservationStudent> GetObservationStudentPaging(int pageNumber, int pageSize, long studentId, string searchString,string sortBy)
        {
            var uri = API.Observation.GetObservationStudentPaging(_path, pageNumber, pageSize, studentId, searchString, sortBy);
            IEnumerable<ObservationStudent> lstObservationStudent = new List<ObservationStudent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstObservationStudent = EntityMapper<string, IEnumerable<ObservationStudent>>.MapFromJson(jsonDataProviders);
            }
            return lstObservationStudent;
        }
        public bool DeleteUploadedFiles(int fileId, int observationId, long userId)
        {
            var uri = API.Observation.DeleteObservationFile(_path, fileId, observationId, userId);
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<Subject> GetObservationSubjects(int observationId)
        {
            var uri = API.Observation.GetObservationSubjects(_path, observationId);
            IEnumerable<Subject> subjects = new List<Subject>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                subjects = EntityMapper<string, IEnumerable<Subject>>.MapFromJson(jsonDataProviders);
            }
            return subjects;
        }

        public File GetObservationFilebyObservationFileId(long id)
        {
            var uri = API.Observation.GetObservationFilebyObservationFileId(_path, id);
            File lstMyFiles = new File();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstMyFiles = EntityMapper<string, File>.MapFromJson(jsonDataProviders);
            }
            return lstMyFiles;
        }
        public IEnumerable<Observation> GetArchivedObservationTeacherPaging(int pageNumber, int pageSize, long teacherId, string searchString)
        {
            var uri = API.Observation.GetArchivedObservationTeacherPaging(_path, pageNumber, pageSize, teacherId, searchString);
            IEnumerable<Observation> lstObservations = new List<Observation>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                lstObservations = EntityMapper<string, IEnumerable<Observation>>.MapFromJson(jsonDataProviders);
            }
            return lstObservations;
        }
        public Observation GetObservations(int teacherId)
        {
            var uri = API.Observation.GetObservations(_path, teacherId);
            Observation observation = new Observation();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                observation = EntityMapper<string, Observation>.MapFromJson(jsonDataProviders);
            }
            return observation;
        }
        public bool UpdateActiveObservation(long observationId)
        {
            var uri = API.Observation.UpdateActiveObservation(_path, observationId);
            Assignment assignments = new Assignment();

            HttpResponseMessage response = _client.PostAsJsonAsync(uri, observationId).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<Course> GetSchoolCoursesByObservation(int observationId)
        {
            var uri = API.Observation.GetSchoolCoursesByObservation(_path, observationId);
            IEnumerable<Course> courses = new List<Course>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                courses = EntityMapper<string, IEnumerable<Course>>.MapFromJson(jsonDataProviders);
            }
            return courses;
        }

    }
}