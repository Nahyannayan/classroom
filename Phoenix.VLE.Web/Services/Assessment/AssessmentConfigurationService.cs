﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Phoenix.Common.Helpers;
using Phoenix.Models;
using Phoenix.VLE.Web.Helpers;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Services
{
    public class AssessmentConfigurationService : IAssessmentConfigurationService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/AssessmentConfiguration";
        #endregion

        public AssessmentConfigurationService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }

        #region Assessment Configuration

        public IEnumerable<AssessmentConfigSetting> GetAssessmentConfigDetail(long AssessmentMasterId, long schoolId)
        {
            var uri = API.AssessmentConfiguration.GetAssessmentConfigDetail(_path, AssessmentMasterId, schoolId);
            IEnumerable<AssessmentConfigSetting> assessmentSettingsList = new List<AssessmentConfigSetting>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assessmentSettingsList = EntityMapper<string, IEnumerable<AssessmentConfigSetting>>.MapFromJson(jsonDataProviders);
            }
            return assessmentSettingsList;
        }

        public IEnumerable<AssessmentConfigSetting> GetAssessmentConfigPagination(long schoolId, int pageNum, int pageSize, string searchString)
        {
            var uri = API.AssessmentConfiguration.GetAssessmentConfigPagination(_path, schoolId, pageNum, pageSize, searchString);
            IEnumerable<AssessmentConfigSetting> assessmentSettingsList = new List<AssessmentConfigSetting>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assessmentSettingsList = EntityMapper<string, IEnumerable<AssessmentConfigSetting>>.MapFromJson(jsonDataProviders);
            }
            return assessmentSettingsList;
        }

        public IEnumerable<AssessmentColumn> GetAssessmentColumnDetail(long AssessmentMasterId)
        {
            var uri = API.AssessmentConfiguration.GetAssessmentColumnDetail(_path, AssessmentMasterId);
            IEnumerable<AssessmentColumn> list = new List<AssessmentColumn>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<AssessmentColumn>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }

        public bool SaveAssessmentConfigDetail(AssessmentConfigSetting assessmentConfig)
        {
            var uri = API.AssessmentConfiguration.SaveAssessmentConfigDetail(_path);
            HttpResponseMessage response = _client.PostAsJsonAsync(uri, assessmentConfig).Result;
            return response.IsSuccessStatusCode;
        }
        public IEnumerable<Course> GetCourseListByGrade(string SchoolGradeIds)
        {
            var uri = API.AssessmentConfiguration.GetCourseListByGrade(_path, SchoolGradeIds);
            IEnumerable<Course> list = new List<Course>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                list = EntityMapper<string, IEnumerable<Course>>.MapFromJson(jsonDataProviders);
            }
            return list;
        }
        #endregion

    }
}