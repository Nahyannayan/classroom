﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Helpers;
using System.Text;
using Phoenix.VLE.Web.Models;
using Phoenix.VLE.Web.Areas.Assessment.Model;


namespace Phoenix.VLE.Web.Services
{
    public class ProgressTrackerService : IProgressTrackerService
    {
        #region private variables
        private static HttpClient _client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
        readonly string _baseUrl = Constants.PhoenixAPIUrl;
        readonly string _path = "api/v1/ProgressTracker";
        #endregion

        public ProgressTrackerService()
        {
            if (_client.BaseAddress == null)
            {
                // Initializing our HttpClient temporarly here, try to move into some generic class.
                _client.BaseAddress = new Uri(_baseUrl);
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }
        public IEnumerable<CourseTopicsEdit> GetTopicsByCourseId(long Id)
        {
            var uri = API.ProgressTracker.GetTopicsByCourseId(_path, Id);
            IEnumerable<CourseTopicsEdit> objCourseTopics = new List<CourseTopicsEdit>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objCourseTopics = EntityMapper<string, IEnumerable<CourseTopicsEdit>>.MapFromJson(jsonDataProviders);
            }
            return objCourseTopics;
        }
        public IEnumerable<StudentList> GetStudentList(string GRD_ID, Int32 ACD_ID, Int32 SGR_ID, Int32 SCT_ID)
        {
            var uri = API.ProgressTracker.GetStudentList(_path, GRD_ID, ACD_ID, SGR_ID, SCT_ID);
            IEnumerable<StudentList> studentList = new List<StudentList>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentList = EntityMapper<string, IEnumerable<StudentList>>.MapFromJson(jsonDataProviders);
            }
            return studentList;
        }
        public IEnumerable<ProgressTrackerHeader> GET_PROGRESS_TRACKER_HEADERS(long SBG_ID, string TOPIC_ID, long AGE_BAND_ID, string STEPS, long TSM_ID)
        {
            var uri = API.ProgressTracker.GET_PROGRESS_TRACKER_HEADERS(_path, SBG_ID, TOPIC_ID, AGE_BAND_ID, STEPS, TSM_ID);
            IEnumerable<ProgressTrackerHeader> ProgressTrackerHeaderlist = new List<ProgressTrackerHeader>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                ProgressTrackerHeaderlist = EntityMapper<string, IEnumerable<ProgressTrackerHeader>>.MapFromJson(jsonDataProviders);
            }
            return ProgressTrackerHeaderlist;
        }
        public IEnumerable<ProgressTrackerData> GET_PROGRESS_TRACKER_DATA(long SBG_ID, string TOPIC_ID, long TSM_ID, long SGR_ID)
        {
            var uri = API.ProgressTracker.GET_PROGRESS_TRACKER_DATA(_path, SBG_ID, TOPIC_ID, TSM_ID, SGR_ID);
            IEnumerable<ProgressTrackerData> ProgressTrackerDatalist = new List<ProgressTrackerData>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                ProgressTrackerDatalist = EntityMapper<string, IEnumerable<ProgressTrackerData>>.MapFromJson(jsonDataProviders);
            }
            return ProgressTrackerDatalist;
        }
        public IEnumerable<ProgressTrackerDropdown> BindProgressTrackerDropdown(long BSU_ID, string GRD_ID)
        {
            var uri = API.ProgressTracker.BindProgressTrackerDropdown(_path, BSU_ID, GRD_ID);
            IEnumerable<ProgressTrackerDropdown> ProgressTrackerDropdownlist = new List<ProgressTrackerDropdown>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                ProgressTrackerDropdownlist = EntityMapper<string, IEnumerable<ProgressTrackerDropdown>>.MapFromJson(jsonDataProviders);
            }
            return ProgressTrackerDropdownlist;
        }
        public ProgressTrackerSettingMaster BindProgressTrackerMasterSetting(long ACD_ID, long BSU_ID, string GRD_ID)
        {
            var uri = API.ProgressTracker.BindProgressTrackerMasterSetting(_path, ACD_ID, BSU_ID, GRD_ID);
            ProgressTrackerSettingMaster objTrackerSettingMaster = new ProgressTrackerSettingMaster();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objTrackerSettingMaster = EntityMapper<string, ProgressTrackerSettingMaster>.MapFromJson(jsonDataProviders);
            }
            return objTrackerSettingMaster;
        }

        public bool AddEditProgressSetUP(long teacherId,ProgressTrackerSetup model)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.ProgressTracker.AddEditProgressSetUP(_path, teacherId), model).Result;
            return response.IsSuccessStatusCode;

        }
        public IEnumerable<ProgressTrackerSetup> GetProgressSetup(long schoolId)
        {
            var uri = API.ProgressTracker.GetProgressSetup(_path, schoolId);
            IEnumerable<ProgressTrackerSetup> Lesson = new List<ProgressTrackerSetup>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                Lesson = EntityMapper<string, IEnumerable<ProgressTrackerSetup>>.MapFromJson(jsonDataProviders);
            }
            return Lesson;
        }

        public IEnumerable<CourseGradeDisplay> GetCourseGradeDisplay(long schoolId, string courseIds)
        {
            var uri = API.ProgressTracker.GetCourseGradeDisplay(_path, schoolId, courseIds);
            IEnumerable<CourseGradeDisplay> coursegradeList = new List<CourseGradeDisplay>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                coursegradeList = EntityMapper<string, IEnumerable<CourseGradeDisplay>>.MapFromJson(jsonDataProviders);
            }
            return coursegradeList;
        }

        public ProgressTrackerSetup GetProgressSetupDetailsById(long Id)
        {
            var uri = API.ProgressTracker.GetProgressSetupDetailsById(_path, Id);
            ProgressTrackerSetup progressSetup = new ProgressTrackerSetup();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                progressSetup = EntityMapper<string, ProgressTrackerSetup>.MapFromJson(jsonDataProviders);
            }
            return progressSetup;
        }

        public IEnumerable<GradingTemplateItem> GetProgressLessonGrading(long courseId, long schoolGroupId)
        {
            var uri = API.ProgressTracker.GetProgressLessonGrading(_path, courseId, schoolGroupId);
            IEnumerable<GradingTemplateItem> gradingtemplateList = new List<GradingTemplateItem>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                gradingtemplateList = EntityMapper<string, IEnumerable<GradingTemplateItem>>.MapFromJson(jsonDataProviders);
            }
            return gradingtemplateList;
        }

        public IEnumerable<AssessmentStudent> StudentProgressTracker(long schoolGroupId)
        {
            var uri = API.ProgressTracker.StudentProgressTracker(_path,  schoolGroupId);
            IEnumerable<AssessmentStudent> studentList = new List<AssessmentStudent>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentList = EntityMapper<string, IEnumerable<AssessmentStudent>>.MapFromJson(jsonDataProviders);
            }
            return studentList;
        }

        public IEnumerable<AssessmetLessons> GetLessonObjectivesByUnitIds(string subSyllabusIds)
        {
            var uri = API.ProgressTracker.GetLessonObjectivesByUnitIds(_path, subSyllabusIds);
            IEnumerable<AssessmetLessons> studentList = new List<AssessmetLessons>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                studentList = EntityMapper<string, IEnumerable<AssessmetLessons>>.MapFromJson(jsonDataProviders);
            }
            return studentList;
        }

        public bool StudentProgressMapper(long userId, long groudId, List<AssessmentStudent> studentProgressMapper)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.ProgressTracker.StudentProgressMapper(_path, userId, groudId), studentProgressMapper).Result;
            return response.IsSuccessStatusCode;
        }

        public bool SaveProgressTrackerEvidence(List<Phoenix.Models.Entities.Attachment> attachments)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(API.ProgressTracker.SaveProgressTrackerEvidence(_path), attachments).Result;
            return response.IsSuccessStatusCode;
        }

        public IEnumerable<Phoenix.Models.Entities.Attachment> GetProgressTrackerEvidence(long id, string key)
        {
            var uri = API.ProgressTracker.GetProgressTrackerEvidence(_path, id, key);
            IEnumerable<Phoenix.Models.Entities.Attachment> attachmentList = new List<Phoenix.Models.Entities.Attachment>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                attachmentList = EntityMapper<string, IEnumerable<Phoenix.Models.Entities.Attachment>>.MapFromJson(jsonDataProviders);
            }
            return attachmentList;
        }

        public IEnumerable<AssignmentObjectiveGrading> GetObjectiveAssignmentGrading(long lessonId, long studentId)
        {
            var uri = API.ProgressTracker.GetObjectiveAssignmentGrading(_path, lessonId, studentId);
            IEnumerable<AssignmentObjectiveGrading> assignmentObjectiveGradings = new List<AssignmentObjectiveGrading>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                assignmentObjectiveGradings = EntityMapper<string, IEnumerable<AssignmentObjectiveGrading>>.MapFromJson(jsonDataProviders);
            }
            return assignmentObjectiveGradings;
        }

        public IEnumerable<ProgressTrackerSetup> ValidateProgressSetupCourseGrade(string courseIds, string gradeIds)
        {
            var uri = API.ProgressTracker.ValidateProgressSetupCourseGrade(_path, courseIds, gradeIds);
            IEnumerable<ProgressTrackerSetup> objValidateProgressSetupCourseGrade = new List<ProgressTrackerSetup>();
            HttpResponseMessage response = _client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonDataProviders = response.Content.ReadAsStringAsync().Result;
                objValidateProgressSetupCourseGrade = EntityMapper<string, IEnumerable<ProgressTrackerSetup>>.MapFromJson(jsonDataProviders);
            }
            return objValidateProgressSetupCourseGrade;
        }
    }
}