﻿using Phoenix.Models;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.VLE.Web.Services
{
    public interface IAssessmentConfigurationService
    {
        #region Assessment Configuration Setting
        IEnumerable<Course> GetCourseListByGrade(string SchoolGradeIds);
        IEnumerable<AssessmentColumn> GetAssessmentColumnDetail(long AssessmentMasterId);
        IEnumerable<AssessmentConfigSetting> GetAssessmentConfigDetail(long AssessmentMasterId, long schoolId);
        IEnumerable<AssessmentConfigSetting> GetAssessmentConfigPagination(long schoolId, int pageNum, int pageSize, string searchString);
        bool SaveAssessmentConfigDetail(AssessmentConfigSetting assessmentConfig);
        #endregion
    }
}
