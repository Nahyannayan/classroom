﻿using Phoenix.Common.Helpers;
using Phoenix.Common.ViewModels;
using Phoenix.Models;
using Phoenix.Models.Entities;
using Phoenix.VLE.Web.EditModels;
using Phoenix.VLE.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.VLE.Web.Areas.Assessment.Model;

namespace Phoenix.VLE.Web.Services
{
    public interface IProgressTrackerService
    {
        IEnumerable<CourseTopicsEdit> GetTopicsByCourseId(long Id);
        IEnumerable<StudentList> GetStudentList(string GRD_ID, Int32 ACD_ID, Int32 SGR_ID, Int32 SCT_ID);
        IEnumerable<ProgressTrackerHeader> GET_PROGRESS_TRACKER_HEADERS(long SBG_ID, string TOPIC_ID, long AGE_BAND_ID, string STEPS, long TSM_ID);
        IEnumerable<ProgressTrackerData> GET_PROGRESS_TRACKER_DATA(long SBG_ID, string TOPIC_ID, long TSM_ID, long SGR_ID);
        IEnumerable<ProgressTrackerDropdown> BindProgressTrackerDropdown(long BSU_ID, string GRD_ID);
        ProgressTrackerSettingMaster BindProgressTrackerMasterSetting(long ACD_ID, long BSU_ID, string GRD_ID);

        bool AddEditProgressSetUP(long teacherId,ProgressTrackerSetup model);

        IEnumerable<ProgressTrackerSetup> GetProgressSetup(long schoolId);

        IEnumerable<CourseGradeDisplay> GetCourseGradeDisplay(long schoolId, string courseIds);

         ProgressTrackerSetup GetProgressSetupDetailsById(long Id);

        IEnumerable<GradingTemplateItem> GetProgressLessonGrading(long courseId, long schoolGroupId);
        IEnumerable<AssessmentStudent> StudentProgressTracker(long schoolGroupId);
        IEnumerable<AssessmetLessons> GetLessonObjectivesByUnitIds(string subSyllabusIds);

        bool StudentProgressMapper(long userId, long groudId, List<AssessmentStudent> studentProgressMapper);
        bool SaveProgressTrackerEvidence(List<Phoenix.Models.Entities.Attachment> attachments);
        IEnumerable<Phoenix.Models.Entities.Attachment> GetProgressTrackerEvidence(long id, string key);
        IEnumerable<AssignmentObjectiveGrading> GetObjectiveAssignmentGrading(long lessonId, long studentId);
        IEnumerable<ProgressTrackerSetup> ValidateProgressSetupCourseGrade(string courseIds, string gradeIds);
    }
}
