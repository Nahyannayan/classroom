﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Common.Enums;
using Phoenix.Common.Models;
using Phoenix.Models;
using Phoenix.VLE.Web.Models;

namespace Phoenix.VLE.Web.Services
{
    public interface IAssessmentService
    {
        IEnumerable<StudentList> GetStudentList(string GRD_ID, Int32 ACD_ID, Int32 SGR_ID, Int32 SCT_ID);

        IEnumerable<ReportHeader> GetReportHeaders(string GRD_ID, Int32 ACD_ID, Int32 SBG_ID, Int32 RPF_ID,
            Int32 RSM_ID, string prv, ReportHeaderType isGradeBook = ReportHeaderType.GradeEntry);
        IEnumerable<ReportHeaderDropDown> GetReportHeadersDropdowns(Int32 RSM_ID, Int32 SBG_ID, Int32 RSD_ID);
        IEnumerable<AssessmentData> GetAssessmentData(Int32 ACD_ID, Int32 SBG_ID, Int32 RPF_ID, Int32 RSM_ID, string prv);
        bool InsertAssessmentData(string student_xml, string username, int bEdit);
        IEnumerable<MarkEntry> GetAssessmentActivityList(long ACD_ID = 0, long CAM_ID = 0, string GRD_ID = "", long STM_ID = 0, long TRM_ID = 0, long SGR_ID = 0, long SBG_ID = 0, int GRADE_ACCESS = 0, string Username = "", string SuperUser = "");

        IEnumerable<MarkEntryAOLData> GetMarkEntryAOLData(long CAS_ID);

        bool InsertMarkEntryAOLData(List<MarkEntryAOLData> lstmarkEntryAOLData, string Username = "", bool bWithoutSkill = false, long CAS_ID = 0);

        IEnumerable<MarkEntryData> GetMarkEntryData(long CAS_ID, double MIN_MARK, double MAX_MARK);

        bool InsertMarkEntryData(List<MarkEntryData> lstmarkEntryData, long SlabId, string entryType, long CAS_ID);

        IEnumerable<AssessmentComments> GetAssessmentComments(int CAT_ID, long STU_ID);
        IEnumerable<GetHeaderBySubjectCategory> GetHeaderBySubjectCategory(long SGRP_ID);
        
        IEnumerable<AssessmentCategory> GetAssessmentCategories(long CAT_BSU_ID, string CAT_GRD_ID);
        IEnumerable<SectionAccess> GetSectionAccess(string USERNAME, string IsSuperUser, long ACD_ID, long BSU_ID, int GRD_ACCESS, string GRD_ID);
        IEnumerable<HeaderOptional> GetReportHeaderOptional(string AOD_IDs);
        IEnumerable<AssessmentDataOptional> GetAssessmentDataOptional(long ACD_ID, long RPF_ID, long RSM_ID, long SBG_ID, long SGR_ID, string GRD_ID, long SCT_ID, string AOD_IDs);
        IEnumerable<AssessmentPreviousSchedule> GetAssessmentPreviousSchedule(long ACD_ID, string GRD_ID);
        IEnumerable<AssessmentOptionalList> GetAssessmentOptionList(long BSU_ID, long ACD_ID);
        bool IsReportPublish(long RPP_RSM_ID, long RPP_RPF_ID, long RPP_ACD_ID, string RPP_GRD_ID, long RPP_SCT_ID, long RPP_TRM_ID);
        IEnumerable<ListItem> GetReportSchedule(long RSM_ID);
        bool UpdateMarkAttendance(List<MarkAttendance> lstMarkAttendance, long CAS_ID);

        #region Grade Book Setup
        IEnumerable<GradeBookGradeScale> GetGradeScaleList(long BSU_ID, long ACD_ID, long TEACHER_ID);
        IEnumerable<GradeBookGradeScaleDetail> GetGradeScaleDetailList(long GSM_ID);
        bool SaveGradeScaleAndDetail(GradeBookGradeScale gradeBookGradeScale, string DATAMODE);
        bool SaveGradeBookSetup(GradeBookSetup gradeBookSetup, string DATAMODE);
        IEnumerable<GradeBookSetup> GetGradeBookSetupList(GradeBookSetup gradeBookSetup);
        IEnumerable<ReportHeaderModel> GetReportHeaderByRSMID(long RSM_ID);
        bool SaveProcessingRuleSetup(ProcessingRuleSetup processingRuleSetup, string DATAMODE);
        IEnumerable<ProcessingRuleSetup> GetProcessingRuleSetupList(long PRS_RSD_ID);
        IEnumerable<GradeBookDetail> GetGradebookDetail(long RSD_ID);
        #endregion

        #region Grade Book Entry
        int GradeBookCUD(GradeBookEntryListModel gradeBookEntry);
        #endregion

        IEnumerable<Phoenix.VLE.Web.Models.Subjects> GetSubjectsForReportWriting(long acdId, long studentId, string IsSuperUser, long employeeId);
        IEnumerable<StudentReportWriting> GetSavedRecordsOfReportWriting(long rpfId, long studentId);
        bool ReportWritingCU(List<AssessmentData> assessmentDatas);
    }
}
